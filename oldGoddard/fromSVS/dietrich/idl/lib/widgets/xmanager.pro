; Copyright (c) 1991, Research Systems, Inc.  All rights reserved.
;	Unauthorized reproduction prohibited.
PRO XUnregister, corpse

	;This procedure removes a dead widget from the Xmanagers common
	;block.  

COMMON MANAGED, names, ids, handlers, $
		nummanaged, inuseflag, $
		backroutines, backroutineids, backnumber, nbacks, $
		cleanups

janitor = 0						;check to see if the
victim = WHERE(ids EQ corpse, janitor)			;dying widget has a
IF(janitor NE 0) THEN BEGIN				;cleanup routine and
  IF(STRLEN(cleanups(victim(0))) NE 0) THEN $		;call it if so.
    CALL_PROCEDURE, cleanups(victim(0)), corpse
ENDIF

remainingindexes = WHERE(ids NE corpse, count)		;find living indices
IF(count NE 0) THEN BEGIN
  names = names(remainingindexes)			;managed widgets, just
  handlers = handlers(remainingindexes)			;remove the one found
  ids = ids(remainingindexes)				;one less to manage
  cleanups = cleanups(remainingindexes)	
  IF(KEYWORD_SET(backroutines)) THEN BEGIN		;find living background
    remainingbacks = WHERE(backroutineids NE corpse, nbacks)
    IF(nbacks LE 0) THEN BEGIN
      backroutines = 0
      backroutineids = 0
      backnumber = 0
      nbacks = 0 
    ENDIF ELSE BEGIN
      backroutines = backroutines(remainingbacks)	;remove the background
      backroutineids = backroutineids(remainingbacks)	;tasks for dead widget
      backnumber = 0
    ENDELSE
  ENDIF
  nummanaged = nummanaged - 1
ENDIF ELSE BEGIN					;else there are no 
  names = 0						;healthy indices so 
  ids = 1L						;just clean the common
  handlers = 0						;block
  nummanaged = 0
  inuseflag = 0
  backroutines = 0
  backroutineids = 0
  backnumber = 0
  nbacks = 0
ENDELSE

END		;end of XUnregister procedure

;-----------------------------------------------------------------------------

PRO XGetManagerEvent, newevent, ID, BAD

	;This procedure retrieves the next event for the widgets to
	;process.  If there are widgets registered that have 
	;background procedures that need to be called, this routine
	;calls them one at a time in between checking for events.
	;If no widgets have background tasks, this routine just 
	;waits for the next event without doing anything else

COMMON MANAGED, names, ids, handlers, $
		nummanaged, inuseflag, $
		backroutines, backroutineids, backnumber, nbacks, $
		cleanups

IF(nbacks NE 0) THEN BEGIN
  newevent = WIDGET_EVENT(ids, BAD_ID = BAD, /NOWAIT)	;get event and check
  WHILE((newevent.id EQ 0) AND $
	(BAD EQ 0) AND $
	(nbacks NE 0)) DO BEGIN		;for dead widget
    IF(backnumber GE nbacks) THEN backnumber = 0	;call the background
    CALL_PROCEDURE, backroutines(backnumber), $
		    backroutineids(backnumber)
    backnumber = backnumber + 1				;increment the counter
    newevent = WIDGET_EVENT(ids, BAD_ID = BAD, /NOWAIT)	;that keeps track of
  ENDWHILE						;the background tasks
  IF((newevent.id EQ 0) AND (BAD EQ 0)) THEN $
    newevent = WIDGET_EVENT(ids, BAD_ID = BAD)		;no backgrounds so get
ENDIF ELSE $

  newevent = WIDGET_EVENT(ids, BAD_ID = BAD)		;no backgrounds so get
							;event and check
							;for dead widget

END		;end of XGetManagerEvent procedure

;-----------------------------------------------------------------------------

PRO XManager, NAME, ID, $
		BACKGROUND = BACKGROUND, $
		CLEANUP = CLEANUP, $
		EVENT_HANDLER = EVENT_HANDLER, $
		GROUP_LEADER = GROUP_LEADER, $
		JUST_REG = JUST_REG, $
		MODAL = MODAL
;+
; NAME:  
;	XManager
; PURPOSE: 
;	Provides main event loop and management for widgets created using IDL
; CATEGORY:
;	Widgets
; CALLING SEQUENCE:
;	Xmanager [,NAME, ID]
; OPTIONAL INPUTS:
;	NAME = The string name of the routine that creates the widget.  
;	ID = The Widget id of the base of the new widget.
; KEYWORD PARAMETERS:
;	BACKGROUND = This keyword contains a string that is the name of a
;		background task procedure that is to be called when the event
;		loop is idle.  The background task procedure has only one 
;		parameter.  The parameter is the widget ID of the top level 
;		base widget associated with the background task.  The 
;		background task should be very short and quick in execution.  
;		Long, complicated routines will hinder the smoothness of 
;		event processing.
;	CLEANUP = This keyword contains a string that is the name of the
;		routine called when the widget dies.  If not specified,
;		no routine is called.  The cleanup routine must accept one 
;		parameter which is the widget id of the dying widget.
;	EVENT_HANDLER = The name of the routine that is to be called when a
;		widget event occurs in the widget being registered.  If this 
;		keyword is not supplied, the Xmanager will try to process the
;		event by calling the routine name with "_event" appended.  See
;		below for a more detailed explanation.
;	GROUP_LEADER = The widget id of the group leader for the
;		widget being processed.  When the leader dies either by the 
;		users actions or some other routine, all widgets that have that
;		leader will also die.
;		  For example, a widget that views a help file for a demo 
;		  widget would have that demo widget as it's leader.  When
;		  the help widget is registered, it sets the keyword 
;		  GROUP_LEADER to the widget id of the demo widget. If 
;		  the demo widget is destroyed, the help widget led by 
;		  the it would be killed by the Xmanager.
;	JUST_REG = This keyword tells the manager to just register the widget
;		but not to start doing the event processing.  This is useful
;		when you want to register a group of related top level widgets
;		before beginning processing.
;	MODAL = When this keyword is set, the widget that is being registered
;		traps all events and desensitizes all the other widgets.  It
;		is usefull when input from the user is necessary to continue.
;		Once the modal widget dies, the others are resensitized and the
;		normal event processing is restored.
; OUTPUTS:
;	No explicit outputs.
; COMMON BLOCKS:
;	MANAGED - common block used for widget maintenance.  This common block
;	is internal and should not be referenced outside the IDL supplied 
;	routines.  It is subject to change without notice.
; SIDE EFFECTS:
;	Takes control of event processing and locks out the IDL prompt until
;	all widgets have been destroyed.
; RESTRICTIONS:
;	Widgets being registered with the Xmanager must provide at least two 
;	routines. The first routine creates the widget and registers it with
;	the manager and the second routine processes the events that occur 
;	within that widget.  Optionally, a third routine can is used for the
;	processing of background tasks while awaiting input.  An example 
;	widget is suplied below that uses only two routines.  For more 
;	information on using the background tasks, see the Xexample routine
;	that is found in this same library.  It demonstrates background 
;	routines.
;
;   !!!!! WARNING !!!!!!!!! WARNING !!!!!!!!! WARNING !!!!!!!!! WARNING !!!!!
;
;	Although this is a library routine, it may change in the future in
;	its internal implementation.  For future upgradability, it is best
;	not to modify or even worry about what this routine does internally.
;
; EXAMPLE USE:
;	To create widget named Example that is just a base widget with a done
;	button using the Xmanager you would do the following:
;
;
;	;------ first - the event handler routine ------;
;
;     PRO example_event, ev			;this is the routine that 
;						;deals with the events in the 
;						;example widget.
;	
;	WIDGET_CONTROL, ev.id, GET_UVALUE = uv	;the uservalue is retrieved 
;						;from the widget where the 
;						;event occured
;
;	if(uv eq "DONE") then $			;if the event occured in the
;	  WIDGET_CONTROL, ev.top, /DESTROY	;done button then kill the 
;     END					;widget example
;
;
;	;------ second - the main routine ------;
;
;     PRO example				;this is the main routine
;						;that builds the widget and
;						;registers it with the Xmanager
;	
;	base = WIDGET_BASE(TITLE = "Example")	;first the base is created
;	
;	done = WIDGET_BUTTON(base, $		;next the done button is 
;			     TITLE = "DONE", $	;created and it's user value
;			     UVALUE = "DONE")	;set to "DONE"
;
;	WIDGET_CONTROL, base, /REALIZE		;the widget is realized
;
;	XManager, "example", base		;finally the example widget
;						;is registered with the 
;						;Xmanager
;     END
;
;	notes:	First the event handler routine is listed.  The handler
;		routine has the same name as the main routine with the 
;		characters "_event" added.  If you would like to use another
;		event handler name, you would need to pass it's name in as
;		a string to the EVENT_HANDLER keyword.  Also notice that the
;		event routine is listed before the main routine.  This is 
;		because the compiler will not compile the event routine if
;		it was below the main routine.  This is only needed if both
;		routines reside in the same file and the file name is the same
;		as the main routine name with the ".pro" extension added.
;
;
; PROCEDURE:
;	When the first widget is registered, initialize the lists and then 
;	start processing events.  Continue registering widgets and dispatching
;	events until all the widgets have been destroyed.  When a widget is 
;	killed, destroy all widgets that list the destroyed widget as their 
;	leader, if any.
;
; RELATED FUNCTIONS AND PROCEDURES:
;	XRegistered, XManagerTool, XBackRegister
;
; MODIFICATION HISTORY: Written by Steve Richards, November, 1990
;	SMR, March, 1991.     Added a cleanup routine keyword to allow dying
;				widgets to clean themselves up when dying.
;	SMR, May, 1991.	      Fixed a bug found by Diane Parchomchuk where 
;				an error occured when registering a widget 
;				right after destroying another.
;	SMR & ACY, July, 1991 Fixed a bug found by Debra Wolkovitch where
;				lone widgets being destroyed and new ones 
;				created caused problems.
;	SMR, Sept, 1991	      Changed cleanup to use the new WIDGET_INFO
;				routine.
;	SMR and ACY, Oct, '91 Fixed a bug where a background event that 
;				unregistered itself after a time would result
;				in an XMANAGER error.
;-

COMMON MANAGED, names, ids, handlers, $
		nummanaged, inuseflag, $
		backroutines, backroutineids, backnumber, nbacks, $
		cleanups

; The MANAGED common block contains the lists of widget names and 
; ids being managed as well as the number of widgets presently being 
; managed.  The common block is easily thought of as a table with each widget 
; registered being a row in the table.  Pictorally:
;
;	 table   widget's   widget's     widget's	widget's 
;	 index     name       id       event handler  cleanup rtn
;
;	   0     MyWidget   538809568  MyWidget_event MyWidget_cln
;	   1     Sample     548402073  Sample_event   Sample_cln
;	   2     Another    447504503  Another_event  Another_cln
;
; The MANAGED common block also contains a nummanaged variable that is the 
; number of widgets currently being managed.  In addition, a backroutines
; and backroutineids array is maintained to contain the background routines 
; their owner widgets and a counter named backnumber that indicates the 
; current background routine.  Finally, there is a variable called nbacks that
; maintains the number of background tasks

doeventloop = 0						;flag set if the main
							;event loop is to be
							;used

IF(N_PARAMS() EQ 0) THEN BEGIN				;no parameters.
  IF (NOT(KEYWORD_SET(nummanaged))) THEN RETURN
  WIDGET_INFO, WIDGET_ID = ids, VALID = validids
  badids = WHERE((validids EQ 0), deaduns)
  IF(deaduns NE 0) THEN $
    FOR i = 0, deaduns-1 do $
      XUnRegister, ids(badids(i))			;event loop again
  IF(KEYWORD_SET(ids)) THEN doeventloop = 1
  IF(NOT(KEYWORD_SET(nummanaged))) THEN $
    MESSAGE, "No Widgets Are Currently being managed"
ENDIF ELSE IF(N_PARAMS() EQ 1) THEN BEGIN		;Cluster registration
  MESSAGE, "Cannot be called with one argument."
ENDIF ELSE IF(N_PARAMS() NE 2) THEN BEGIN
  MESSAGE, "Wrong Number of Arguments, Usage = Xmanager, [name, id]"

ENDIF ELSE BEGIN					;register a new widget

  IF (KEYWORD_SET(IDS)) THEN BEGIN			;make sure that the new
    position = WHERE(ID EQ IDS, count)			;widget ID is not
    IF (count NE 0) THEN $				;currently in the IDS
      XUnregister, ID					;common block.
  ENDIF							;This is only
							;true when a widget is
							;killed and another is
							;created before
							;events are processed

  IF(NOT KEYWORD_SET(EVENT_HANDLER)) THEN $		;if no event handler 
    EVENT_HANDLER = NAME + "_event"			;was specified, assume
							;that the widget name +
							;"_event" exists

  IF(KEYWORD_SET(BACKGROUND)) THEN $			;make sure the window
		WIDGET_CONTROL, id, /DELAY_DESTROY	;manager will not kill
							;a widget that has a
							;background in progress

  IF(NOT KEYWORD_SET(CLEANUP)) THEN $			;specify a null cleanup
		CLEANUP = ""				;if keyword not set

  IF(keyword_set(GROUP_LEADER)) THEN $
    WIDGET_CONTROL, ID, GROUP_LEADER = GROUP_LEADER

  IF(NOT KEYWORD_SET(nummanaged)) THEN BEGIN		;if not managing any
							;widgets then go ahead
    names = NAME
    ids = ID
    handlers = EVENT_HANDLER
    IF(KEYWORD_SET(BACKGROUND)) THEN BEGIN
      backroutines = BACKGROUND
      backroutineids = ID
      nbacks = 1
    ENDIF ELSE nbacks = 0
    cleanups = CLEANUP
    nummanaged = 1
    backnumber = 0

    IF(NOT(keyword_set(JUST_REG))) THEN $		;if just registering
      doeventloop = 1					;the widget, do not
							;start the event loop

  ENDIF ELSE BEGIN					;else not the first 
							;widget

    handlers = [handlers, EVENT_HANDLER]

    names = [names, NAME]
    ids = [ids, ID]

    IF(KEYWORD_SET(BACKGROUND)) THEN BEGIN
      IF(nbacks GT 0) THEN BEGIN
	backroutines = [backroutines, BACKGROUND]
	backroutineids = [backroutineids, ID]
        nbacks = nbacks + 1
      ENDIF ELSE BEGIN
        backroutines = BACKGROUND
        backroutineids = ID
        nbacks = 1
      ENDELSE
    ENDIF

    cleanups = [cleanups, CLEANUP]

    nummanaged = nummanaged + 1				;set the number managed

    IF(NOT(KEYWORD_SET(inuseflag)) AND $		;if this is one in a 
       NOT(KEYWORD_SET(JUST_REG))) THEN doeventloop = 1	;series that are just
							;being registered, do
  ENDELSE						;not do event loop

  IF(KEYWORD_SET(MODAL)) THEN BEGIN			;when modal, do the
    FOR i = 0, nummanaged - 2 DO $			;event processing for 
      WIDGET_CONTROL, ids(i), SENSITIVE = 0		;just this new widget
							;desensitizing others
    XGetManagerEvent, newevent, ID, BAD

    WHILE(BAD EQ 0) DO BEGIN				;keep looking for 
      i = WHERE(ids EQ newevent.top, count)		;events in the modal
      IF(count le 0) THEN $				;widget while calling
        MESSAGE, "Unmanaged Widget returned event"	;the background
      CALL_PROCEDURE, handlers(i(0)), newevent		;routines
      XGetManagerEvent, newevent, ID, BAD
    ENDWHILE
							;remove modal widget
    XUnregister, BAD					;if it has not already
							;been removed

    FOR i = 0, nummanaged - 1 DO BEGIN			;resensitize the lower
      WIDGET_INFO, WIDGET_ID = ids(i), valid = val
      IF(val EQ 1) THEN $
	WIDGET_CONTROL, ids(i), /SENSITIVE		;widgets
    ENDFOR
  ENDIF
ENDELSE

IF(doeventloop NE 0) THEN BEGIN				;if not modal and the
  WHILE(KEYWORD_SET(nummanaged)) DO BEGIN		;event loop should be
							;started, do it!

    inuseflag = 1					;this flag says that
							;the main event loop
							;is being used

    XGetManagerEvent, newevent, ID, BAD			;get next event

    IF(BAD NE 0) THEN $					;if there a dead widget
      XUnregister, BAD $
    ELSE BEGIN						;else a valid widget 
      i = WHERE(ids EQ newevent.top, count)		;was hit so call its
      IF(count le 0) THEN $				;event_handler passing
        MESSAGE, "Unmanaged Widget returned event"	;along the event for
      CALL_PROCEDURE, handlers(i(0)), newevent		;processing
    ENDELSE
  ENDWHILE

  inuseflag = 0						;done so unset the 
ENDIF							;inuse flag

END
;----------------------- end of procedure XManager ----------------------------

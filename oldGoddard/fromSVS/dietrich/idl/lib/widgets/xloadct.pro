; Copyright (c) 1991, Research Systems, Inc.  All rights reserved.
;	Unauthorized reproduction prohibited.
PRO xloadct_event, event
COMMON colors, r_orig, g_orig, b_orig, r_curr, g_curr, b_curr
COMMON xloadct_com, r0, g0, b0, r1, g1, b1

WIDGET_CONTROL, GET_UVALUE = instance, $		;first get hold of a
		event.top				;copy of the instance
							;structure of this 
							;widget

WIDGET_CONTROL, event.id, $
		GET_UVALUE = eventval

gamma = 1.0
abstop = !D.TABLE_SIZE -1
WIDGET_CONTROL, instance.top, $
		GET_VALUE = thetop
WIDGET_CONTROL, instance.bot, $
		GET_VALUE = thebot

CASE eventval OF
    "TOP":    BEGIN
		if instance.lock ne 0 then begin
			thebot = (thetop - instance.lock) > 0 < 100
			widget_control, instance.bot, SET_VALUE=thebot
			endif
		GOTO, set_gamma
	      END

    "BOTTOM": BEGIN
		if instance.lock ne 0 then begin
			thetop = (thebot + instance.lock) > 0 < 100
			widget_control, instance.top, SET_VALUE=thetop
			ENDIF
   set_gamma:
		STRETCH, thebot*abstop/100, thetop*abstop/100, gamma,  $
			CHOP = instance.chop
		WIDGET_CONTROL, instance.gamma, GET_VALUE = test
		IF(test NE 50) THEN BEGIN
		  WIDGET_CONTROL, instance.gamma, SET_VALUE = 50
		  WIDGET_CONTROL, instance.g_lbl, SET_VALUE = STRING(1.0)
		ENDIF
	      ENDCASE

    "GAMMA": BEGIN
		WIDGET_CONTROL, instance.gamma, GET_VALUE = gamma
		gamma = 10^((gamma/50.) - 1)
		WIDGET_CONTROL, instance.g_lbl, SET_VALUE = STRING(gamma)
		STRETCH, thebot*abstop/100, thetop*abstop/100, gamma,  $
			CHOP = instance.chop
	     ENDCASE

    "LOCK" : BEGIN
	if instance.lock eq 0 then $
		instance.lock = thetop - thebot $
	else instance.lock = 0
	for i=0,1 do WIDGET_CONTROL, instance.lock_button(i), $
		SENSITIVE = i eq (instance.lock ne 0)
	WIDGET_CONTROL, event.top, SET_UVALUE = instance  ;Update status
        ENDCASE
    "CHOP" : BEGIN
	instance.chop = 1-instance.chop
	for i=0,1 do $
	  WIDGET_CONTROL, instance.chop_button(i), $
		SENSITIVE = instance.chop eq i
	WIDGET_CONTROL, event.top, SET_UVALUE = instance  ;Update status
	goto, set_gamma		;And redraw
	ENDCASE

    "HELP" : XDisplayFile, FILEPATH("xloadct.txt", subdir='help'), $
		TITLE = "Xloadct Help", $
		GROUP = event.top, $
		WIDTH = 55, $
		HEIGHT = 16

    "RESTORE" : BEGIN			;Restore the original tables
	r_curr = r0
	g_curr = g0
	b_curr = b0
	r_orig = r1
	g_orig = g1
	b_orig = b1
	tvlct, r0, g0, b0
	ENDCASE

    "DONE": BEGIN
	WIDGET_CONTROL, event.top, /DESTROY
	r0 = 0 & g0 = 0 & b0 = 0  ;Free common
	r1 = 0 & g1 = 0 & b1 = 0
	ENDCASE

    ELSE: BEGIN
	    i = FIX(eventval)
  	    LOADCT, silent=instance.silent, i
	    WIDGET_CONTROL, instance.top, GET_VALUE = thetop
	    WIDGET_CONTROL, instance.bot, GET_value = thebot
	    Stretch, thebot*abstop/100, thetop*abstop/100, gamma, $
		CHOP = instance.chop
	  END
ENDCASE

END


;+
; NAME:
;	XLOADCT
; PURPOSE:
;	A graphical interface to the LOADCT user library procedure.
;	XLOADCT displays the current color map and provides
;	an array of buttons, one per availible predefined color
;	table. Using the mouse to press these buttons causes
;	the corresponding color map to be loaded.
; CATEGORY:
;	Widgets
; CALLING SEQUENCE:
;	XLOADCT
; INPUTS:
;	None.
; KEYWORDS:
;	GROUP = The widget ID of the widget that calls XLoadct.  When 
;		this ID is specified, a death of the caller results in a 
;		death of XLoadct
;	PICK_ONE - Normally, XLOADCT remains running until the user
;		presses the "QUIT" button. If PICK_ONE is present and 
;		non-zero, the "QUIT" button is not displayed, and
;		XLOADCT quits after a single selection.
;	SILENT - Normally, no informational message is printed when
;		a color map is loaded. If this keyword is present and
;		zero, this message is printed.
; OUTPUTS:
;	None.
; COMMON BLOCKS:
;	None.
; SIDE EFFECTS:
;	One of the predefined color maps may be loaded.
; RESTRICTIONS:
;	This routine uses the LOADCT user library procedure to
;	do the actual work.
; MODIFICATION HISTORY:
;	24, August, 1990, Written by AB, RSI.
;-

PRO XLoadct, PICK_ONE=PICK_ONE, SILENT=SILENT, GROUP = GROUP

COMMON colors, r_orig, g_orig, b_orig, r_curr, g_curr, b_curr
COMMON xloadct_com, r0, g0, b0, r1, g1, b1

IF(XRegistered("xloadct") NE 0) THEN GOTO, DROPOUT

xload_str = {xloadstr, silent:0, top:0L, bot:0L, gamma:0L, g_lbl:0L, $
	lock : 0, chop: 0, lock_button : [0L,0L], chop_button : [ 0L,0L] }


IF(KEYWORD_SET(SILENT)) THEN xload_str.silent = SILENT $
ELSE xload_str.silent = 1
OPENR,lun, FILEPATH('colors.tbl'), /BLOCK,/GET_LUN
aa=ASSOC(lun, BYTARR(32))       ;Get name

w_height = 50			;Height of ramp
cur_win = !D.WINDOW
xload_str.lock = 0
xload_str.chop = 0

base = WIDGET_BASE(TITLE="XLoadct", /COLUMN)


show = WIDGET_DRAW(base, YSIZE=w_height, XSIZE=256, /FRAME, RETAIN = 2)
IF(NOT(KEYWORD_SET(pick_one))) THEN BEGIN
  junk = WIDGET_BASE(base, /ROW)
  done = WIDGET_BUTTON(junk, VALUE=' Done ', UVALUE = "DONE")
  junk1 = WIDGET_BUTTON(junk, VALUE=' Help ', UVALUE = "HELP")
  junk1 = WIDGET_BUTTON(junk, VALUE=' Options ', /MENU)
  xload_str.lock_button(0) = WIDGET_BUTTON(junk1, VALUE='Gang Sliders', $
		UVALUE = "LOCK")
  xload_str.lock_button(1) = WIDGET_BUTTON(junk1, VALUE='Ungang Sliders', $
		UVALUE = "LOCK")
  xload_str.chop_button(0) = WIDGET_BUTTON(junk1, VALUE='Chop Top', $
		UVALUE="CHOP")
  xload_str.chop_button(1) = WIDGET_BUTTON(junk1, VALUE='Clip Top', $
		UVALUE="CHOP")
  junk = WIDGET_BUTTON(junk1, VALUE='Restore Original Table', $
		UVALUE="RESTORE")
  WIDGET_CONTROL, xload_str.chop_button(1), SENSITIVE = 0
  WIDGET_CONTROL, xload_str.lock_button(1), SENSITIVE = 0
  sbase=WIDGET_BASE(base, /COLUMN)
  xload_str.bot = WIDGET_SLIDER(sbase, TITLE = "Stretch Bottom", MINIMUM = 0, $
	MAXIMUM = 100, VALUE = 0, /DRAG, UVALUE = "BOTTOM", xsize=256)
  xload_str.top = WIDGET_SLIDER(sbase, TITLE = "Stretch Top", MINIMUM = 0, $
	MAXIMUM = 100, VALUE = 100, /DRAG, UVALUE = "TOP", xsize=256)
  xload_str.g_lbl = WIDGET_LABEL(sbase, VALUE = STRING(1.0))
  xload_str.gamma = WIDGET_slider(sbase, TITLE = "Gamma Correction", $
	MINIMUM = 0, MAXIMUM = 100, VALUE = 50, UVALUE = "GAMMA", $
	/SUPPRESS_VALUE, xsize=256)
ENDIF ELSE BEGIN
  done = 0
ENDELSE

rowcol = WIDGET_BASE(base, /FRAME, /EXCLUSIVE ,/COLUMN, xsize=256)
FOR i = 0, 15 DO junk = WIDGET_BUTTON(rowcol, VALUE=STRING(aa(i)), $
	UVALUE = STRING(i), /NO_RELEASE)
FREE_LUN, lun
WIDGET_CONTROL, base, /REALIZE

IF(N_ELEMENTS(r_orig) LE 0)THEN LOADCT, 0
r0 = r_curr		;Save original colors
g0 = g_curr
b0 = b_curr
r1 = r_orig
g1 = g_orig
b1 = b_orig

WIDGET_CONTROL, show, GET_VALUE=show_win
WSET, show_win
TVSCL, INDGEN(256) # REPLICATE(1, w_height)
IF(cur_win NE -1) THEN WSET, cur_win

WIDGET_CONTROL, base, SET_UVALUE = xload_str

XManager, "xloadct", base, GROUP_LEADER = GROUP

DROPOUT:

END










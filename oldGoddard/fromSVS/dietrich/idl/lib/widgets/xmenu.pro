; Copyright (c) 1991, Research Systems, Inc.  All rights reserved.
;	Unauthorized reproduction prohibited.
;+
; NAME:
;	XMENU
; PURPOSE:
;	Simplifies setting up widget menus. XMENU accepts a string
;	array of menu labels, creates a widget base, and populates
;	the base with buttons containing the specified labels.
; CALLING SEQUENCE:
;	XMENU, VALUES [, PARENT]
; INPUTS:
;	VALUES - An array of labels for the butons. If VALUES is of
;		type string, then it is a 1-D array of labels. If it
;		is of type byte, it is a 3-D array of bitmaps, where
;		the 1st 2 dimensions are the width and height of each
;		bitmap.
;	PARENT - Widget ID of parent base widget. If this argument is
;		omitted, the menu base is a top-level base.
; KEYWORDS:
;	BASE: A named variable to recieve the widget id of the created base.
;	BUTTONS: A named variable to recieve the widget id of the created
;		buttons. This return value is a longword array, with each
;		element matching the corresponding element in VALUES.
;	COLUMN: Specifies the buttons should be layed out in columns. The
;		value specified gives the number of columns desired.
;	EXCLUSIVE: Specifies that the buttons should have both selected
;		and unselected states, and that only one button at a time
;		is allowed to be selected.
;	FONT: A string specifying the font for the button labels.
;	FRAME: A frame should be drawn around the base.
;	NONEXCLUSIVE: Specifies that the buttons should have both selected
;		and unselected states, and that more than one button at a time
;		is allowed to be selected.
;	NO_RELEASE: When set, the release events from buttons are ignored.
;	ROW: Specifies the buttons should be layed out in rows. The
;		value specified gives the number of rows desired.
;	SCROLL: The base should have scrollbars to allow a large number
;		of buttons to be viewed in a small region.
;	SPACE: The space, in pixels, to be left around the edges of the base.
;	TITLE: If PARENT is not specified, TITLE will specify the MENU title.
;		IF PARENT is specified, a framed base will be created and a
;		label with the value TITLE will be added before the menu. 
;	XPAD: The horizontal space, in pixels, to be left between the buttons.
;	YPAD: The vertical space, in pixels, to be left between the buttons.
;	UVALUE: An array of user values to be set into the UVALUE of the
;		buttons. This array must have the same number of elements
;		as VALUES.
;	X_SCROLL_SIZE: The width of the scrolling viewport. This keyword
;		implies SCROLL.
;	Y_SCROLL_SIZE: The height of the scrolling viewport. This keyword
;		implies SCROLL.
;	
; OUTPUTS:
;	None.
; COMMON BLOCKS:
;	None.
; SIDE EFFECTS:
;	A widget base containing buttons has been created, but not realized.
; MODIFICATION HISTORY:
;	16 January 1991, AB, RSI
;	5 September 1991, SMR, RSI   Fixed bug where titles were ignored when
;				     no base specified.
;	21 January 1992, ACY, RSI    Added FONT keyword.
;-
PRO XMENU, VALUES, PARENT, BASE=BASE, BUTTONS=BUTTONS, COLUMN=COLUMN, $
	EXCLUSIVE=EXCLUSIVE, FONT=FONT, FRAME=FRAME, $
	NONEXCLUSIVE=NONEXCLUSIVE, ROW=ROW, SCROLL=SCROLL, SPACE=SPACE, $
	XPAD=XPAD, YPAD=YPAD, UVALUE=UVALUE, X_SCROLL_SIZE=X_SCROLL_SIZE, $
	Y_SCROLL_SIZE=Y_SCROLL_SIZE, TITLE = TITLE, NO_RELEASE = NO_RELEASE

  ; Error check the plain arguments
  s = size(parent)
  if (s(s(0) + 1) eq 0) then begin
    ; No parent is specified.
    parent = 0
    if (not keyword_set(TITLE)) then TITLE = 'Menu'
  endif else begin
    if (s(0) ne 0) then message, 'PARENT must be a scalar value."
    if (s(1) ne 3) then message, 'PARENT must be a long integer."
  endelse
  s = size(VALUES)
  value_type = s(s(0) + 1)
  if ((value_type ne 1) and (value_type ne 7)) then $
    message, 'VALUES must be a string vector or 3-D byte array.`
  if (value_type eq 1) then begin
    if (s(0) ne 3) then message, 'Type Byte VALUES must be 3-D'
    n_buttons = s(3)
  endif else begin
    if (s(0) ne 1) then message, 'Type string VALUES must be a vector."
    n_buttons = s(1)
  endelse

  ; Sort out the keywords
  if ((not keyword_set(row)) and (not keyword_set(column))) then column=1
  if (not keyword_set(COLUMN)) then COLUMN=0
  if (not keyword_set(FONT)) then FONT = ''
  if (not keyword_set(ROW)) then ROW=0
  if (not keyword_set(EXCLUSIVE)) then EXCLUSIVE=0
  if (not keyword_set(NONEXCLUSIVE)) then NONEXCLUSIVE=0
  if (keyword_set(scroll) or keyword_set(x_scroll_size) or $
      keyword_set(y_scroll_size)) then begin
    scroll = 1;
    if (not keyword_set(x_scroll_size)) then x_scroll_size=0
    if (not keyword_set(y_scroll_size)) then y_scroll_size=0
  endif else begin
    scroll=0
  endelse
  if (not keyword_set(frame)) then frame = 0
  if (not keyword_set(space)) then space = 0
  if (not keyword_set(xpad)) then xpad = 0
  if (not keyword_set(ypad)) then ypad = 0
  if (not keyword_set(uvalue)) then begin
    uvalue=lindgen(n_buttons)
  endif else begin
    s = size(uvalue)
    if (s(s(0) + 2) ne n_buttons) then $
      message, 'UVALUE must have the same number of elements as VALUES'
  endelse

  ; Create the base
  if (parent eq 0) then begin
    if (scroll) then $
      base = widget_base(COLUMN=COLUMN, EXCLUSIVE=EXCLUSIVE, $
	  FRAME=FRAME, NONEXCLUSIVE=NONEXCLUSIVE, ROW=ROW, SCROLL=SCROLL, $
	  SPACE=SPACE, XPAD=XPAD, YPAD=YPAD, X_SCROLL_SIZE=X_SCROLL_SIZE, $
	  Y_SCROLL_SIZE=Y_SCROLL_SIZE, TITLE = TITLE) $
    else $
      base = widget_base(COLUMN=COLUMN, EXCLUSIVE=EXCLUSIVE, $
	  FRAME=FRAME, NONEXCLUSIVE=NONEXCLUSIVE, ROW=ROW, $
	  SPACE=SPACE, XPAD=XPAD, YPAD=YPAD, TITLE = TITLE)
  endif else begin
    if (KEYWORD_SET(TITLE)) THEN BEGIN
      theparent = widget_base(parent, /COLUMN, /FRAME)
      thelabel = widget_label(theparent, value = title)
    ENDIF ELSE theparent = parent
    if (scroll) then $
      base = widget_base(theparent, COLUMN=COLUMN, EXCLUSIVE=EXCLUSIVE, $
	  FRAME=FRAME, NONEXCLUSIVE=NONEXCLUSIVE, ROW=ROW, SCROLL=SCROLL, $
	  SPACE=SPACE, XPAD=XPAD, YPAD=YPAD, X_SCROLL_SIZE=X_SCROLL_SIZE, $
	  Y_SCROLL_SIZE=Y_SCROLL_SIZE) $
    else $
      base = widget_base(theparent, COLUMN=COLUMN, EXCLUSIVE=EXCLUSIVE, $
	  FRAME=FRAME, NONEXCLUSIVE=NONEXCLUSIVE, ROW=ROW, $
	  SPACE=SPACE, XPAD=XPAD, YPAD=YPAD)
  endelse

  ; Create the buttons
  buttons = lindgen(n_buttons)
  if (value_type eq 1) then begin
    for i = 0, n_buttons-1 do $
      buttons(i) = WIDGET_BUTTON(base, $
		value=values(*, *, i), $
		no_release = no_release, $
		uvalue=uvalue(i), $
		FONT = FONT)
  endif else begin
    for i = 0, n_buttons-1 do $
      buttons(i) = WIDGET_BUTTON(base, $
		value=values(i), $
		no_release = no_release, $
		uvalue=uvalue(i), $
		FONT = FONT)
  endelse

end

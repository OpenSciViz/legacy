; Copyright (c) 1991, Research Systems, Inc.  All rights reserved.
;	Unauthorized reproduction prohibited.
function XRegistered, NAME, NOSHOW = NOSHOW

;+
; NAME: 
;	XRegistered
; PURPOSE:
;	Returns true if a widget named NAME is currently registered with the
;	Xmanager as an exclusive widget, otherwise this routine returns false.
; CATEGORY:
;	Widgets
; CALLING SEQUENCE:
;	return_value = XRegistered(NAME)
; INPUTS:
;	NAME - The string name of the widget in question.
; KEYWORD PARAMETERS:
;	NOSHOW - If the widget in question is registered, it will be brought
;		to the front of all the other windows as a default.  Setting
;		the NOSHOW parameter inhibits the forcing of the window to
;		the front.
; OUTPUTS:
;	Returns 0 if not registered and the number of widgets registered with
;	that name if there are any.
; COMMON BLOCKS:
;	MANAGED 
; SIDE EFFECTS:
;	Brings the widget to the front of the desktop if it finds one.
; RESTRICTIONS:
;	None
; PROCEDURE:
;	Searches the list of exclusive widget names and if a match is found
;	with the one in question, the return value is modified.
;
; MODIFICATION HISTORY: Written by Steve Richards, November, 1990
;			Jan, 92 - SMR, fixed a bug where an invalid widget
;					was being referenced with 
;					widget_control and the /show keyword
;-



COMMON MANAGED, names, ids, handlers, $
		nummanaged, inuseflag, $
		backroutines, backroutineids, backnumber, nbacks, $
		cleanups

answer = 0

IF(KEYWORD_SET(nummanaged)) THEN $
  registered = WHERE(names EQ NAME, answer)

IF(answer NE 0) THEN BEGIN			;if registered
  WIDGET_INFO, WIDGET_ID = ids(registered), VALID = validids
  IF((validids(0) EQ 1) AND (NOT(KEYWORD_SET(NOSHOW)))) THEN $
    WIDGET_CONTROL, ids(registered(0)), /SHOW
ENDIF

RETURN, answer

END



; Copyright (c) 1991, Research Systems, Inc.  All rights reserved.
;	Unauthorized reproduction prohibited.
PRO XDispFile_evt, event

WIDGET_CONTROL, GET_UVALUE = retval, event.id

IF(retval EQ "EXIT") THEN WIDGET_CONTROL, event.top, /DESTROY

END


PRO XDisplayFile, FILENAME, TITLE = TITLE, GROUP = GROUP, WIDTH = WIDTH, $
		HEIGHT = HEIGHT, TEXT = TEXT
;+
; NAME: 
;	XDisplayFile
; PURPOSE:
;	Displays an ascii file using widgets and the widget manager.
; CATEGORY:
;	Widgets
; CALLING SEQUENCE:
;	XDisplayFile, FILENAME
; INPUTS:
;	FILENAME = A string variable that contains the filename of the file
;		to display.  This could include a path to that file.
; KEYWORD PARAMETERS:
;	GROUP = A widget ID of the group leader of the widget.  Specifying the
;		group leader means that if the group leader is closed, this
;		instance of XDisplayFile will also automatically close.
;	HEIGHT = The number of characters tall the widget should be.  If not
;		specified, 24 characters is the default.
;	TEXT = A string or string array that is to be displayed in the widget
;		instead of the contents of a file.  This keyword supercedes
;		the FILENAME input parameter.
;	TITLE = A string to use as the title rather than the file name or 
;		XDisplayFile.
;	WIDTH = The number of characters wide the widget should be.  If not
;		specified, 80 characters is the default.
; OUTPUTS:
;	Creates a Xwidget for file viewing
; SIDE EFFECTS:
;	Triggers the Xmanager if it is not already in use.
; RESTRICTIONS:
;	None
; PROCEDURE: Open a file and create a widget to display its contents.
; MODIFICATION HISTORY: Written By Steve Richards, December 1990
;-

IF(NOT(KEYWORD_SET(TITLE))) THEN TITLE = FILENAME	;use the defaults if
IF(NOT(KEYWORD_SET(HEIGHT))) THEN HEIGHT = 24		;the keywords were not
IF(NOT(KEYWORD_SET(WIDTH))) THEN WIDTH = 80		;passed in

filebase = WIDGET_BASE(TITLE = TITLE, $			;create the base
		/COLUMN, $
		SPACE = 20, $
		XPAD = 20, $
		YPAD = 20)

IF(NOT(KEYWORD_SET(TEXT)))THEN BEGIN
  OPENR, unit, FILENAME, /GET_LUN			;open the file and then
  a = strarr(1000)					;Maximum # of lines
  i = 0
  c = ''
  while not eof(unit) do begin
	readf,unit,c
	a(i) = c
	i = i + 1
	endwhile
  a = a(0:i-1)
  FREE_LUN, unit					;free the file unit.
ENDIF ELSE a = TEXT

filequit = WIDGET_BUTTON(filebase, $			;create a Done Button
		VALUE = "Done with " + TITLE, $
		UVALUE = "EXIT")

filetext = WIDGET_TEXT(filebase, $			;create a text widget
		XSIZE = WIDTH, $			;to display the file's
		YSIZE = HEIGHT, $			;contents
		/SCROLL, $
		VALUE = a)

WIDGET_CONTROL, filebase, /REALIZE			;instantiate the widget

Xmanager, "XDisplayFile", $				;register it with the
		filebase, $				;widget manager
		GROUP_LEADER = GROUP, $
		EVENT_HANDLER = "XDispFile_evt" 

END  ;--------------------- procedure XDisplayFile ----------------------------

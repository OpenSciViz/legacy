; Copyright (c) 1991, Research Systems, Inc.  All rights reserved.
;	Unauthorized reproduction prohibited.

PRO XBackRegister, NAME, ID, UNREGISTER = UNREGISTER

;+
; NAME: 
;	XBackRegister
; PURPOSE:
;	Registers a background task with a currently managed widget 
;	application.
; CATEGORY:
;	Widgets
; CALLING SEQUENCE:
;	XBackRegister, NAME, ID
; INPUTS:
;	NAME - The string name of the background routine to be registered.
;	ID - The top level base widget ID of the widget application the 
;		background routine is associated with.
; KEYWORD PARAMETERS:
;	UNREGISTER - If the widget in question is registered as a background
;		task, it will be unregistered if this keyword is set.  If not,
;		the routine will be registered.
; COMMON BLOCKS:
;	MANAGED 
; SIDE EFFECTS:
;	Brings the widget to the front of the desktop if it finds one.
; RESTRICTIONS:
;	Allows only one instance of a given background task for any one widget
;	ID.
; PROCEDURE:
;	Inserts and deletes background events from the XManagers common block
;
; MODIFICATION HISTORY: Written by Steve Richards, January, 1991
;-


COMMON MANAGED, names, ids, handlers, $
		nummanaged, inuseflag, $
		backroutines, backroutineids, backnumber, nbacks, $
		cleanups


IF(N_PARAMS() NE 2) THEN MESSAGE, "Incorrect number of parameters"
IF(NOT(KEYWORD_SET(inuseflag))) THEN MESSAGE, "XManager not in use"
IF(NOT(KEYWORD_SET(backroutineids))) THEN $
  alreadythere = -1 $
ELSE $
  alreadythere = WHERE(backroutineids EQ ID)

IF(KEYWORD_SET(UNREGISTER)) THEN BEGIN			;background task is to
  IF(alreadythere(0) NE -1) THEN BEGIN			;be removed
    remainingbacks = WHERE((backroutines NE NAME) AND $
			   (backroutineids NE ID), nbacks)
    IF(remainingbacks(0) NE -1) THEN BEGIN
      backroutines = backroutines(remainingbacks)
      backroutineids = backroutineids(remainingbacks)
    ENDIF ELSE BEGIN
      backroutines = 0
      backroutineids = 0
    ENDELSE
    backnumber = 0
  ENDIF
ENDIF ELSE BEGIN					;background task is to
  IF(alreadythere(0) NE -1) THEN BEGIN			;be added
    routinelist = WHERE(backroutines(alreadythere) EQ NAME)
    IF(routinelist(0) NE -1) THEN $
      MESSAGE, "Routine already registered"
  ENDIF
  IF(nbacks GT 0) THEN BEGIN
    newbackroutines = STRARR(nbacks + 1)		;add the new widget's
    newbackroutines(0:nbacks - 1) = backroutines	;background to the list
    newbackroutines(nbacks) = NAME			;of background routines
    backroutines = newbackroutines
    newbackroutids = LONARR(nbacks + 1, /NOZERO)
    newbackroutids(0:nbacks - 1) = backroutineids
    newbackroutids(nbacks) = ID
    backroutineids = newbackroutids      
    nbacks = nbacks + 1
  ENDIF ELSE BEGIN
    backroutines = NAME
    backroutineids = ID
    nbacks = 1
  ENDELSE
  WIDGET_CONTROL, ID, /DELAY_DESTROY

ENDELSE

END

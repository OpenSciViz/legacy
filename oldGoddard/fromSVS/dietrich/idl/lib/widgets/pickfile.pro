;
; Copyright (c) 1991, Research Systems, Inc.  All rights reserved.
;	Unauthorized reproduction prohibited.
;+
; NAME:
;	PICKFILE
; PURPOSE:
;	This routine allows the user to interactively pick a file.  This file
; 	can be in the current directory or in other directories.
; CATEGORY:
;	Widgets
; CALLING SEQUENCE:
;	filename = Pickfile()
; KEYWORD PARAMETERS:
;	GROUP = The widget ID of the widget that calls Pickfile.  When this
;		ID is specified, a death of the caller results in a death of
;		Pickfile
;	READ = When set, the title of Pickfile is "Select File to Read"
;	WRITE = When set, the title of Pickfile is "Select File to Write"
;	PATH = The initial path that the files should be selected from.  When
;		this is not set, the current directory is used.
;	FILTER = A string array of extensions that will be selectable.  This
;		lets the user select only files of a certain type.
;	TITLE = When set, the string is used for the window title.
; OUTPUTS:
;	Returns either a string name of the file selected.
; COMMON BLOCKS:
;	picker = COMMON block that maintains state for the widget.
; SIDE EFFECTS:
;	Initiates the XManager if it is not already running.
; RESTRICTIONS:
;	This routine is known to work on Suns (Openlook), MIPS, RS/6000, 
;		VAX/VMS and SGI machines.
;	Only one instance of the Pickfile widget may be running at one time.
;	Does not recognize symbolic links to other files in UNIX.
; PROCEDURE:
;	Create and register the widget and then exit returning the filename
;	that was picked.
; MODIFICATION HISTORY:
; 	Written by: Steve Richards,	April, 1991
;	Modified:	July, 1991 - Added a FILTER keyword to allow users
;				to select files with a given extension or 
;				extensions.
;	Modified:	August, 1991 - Fixed bugs caused by differences between
;				spawned ls commands on different machines.
;	Modified;	September, 1991 - Made Myfindfile so only one pass was
;				necessary to find files and directories.
;-


;------------------------------------------------------------------------------
;	procedure MYFINDFILE
;------------------------------------------------------------------------------
; This routine finds the files or directories at the current directory level.
; It must be called with either files or directories as a keyword.
;------------------------------------------------------------------------------

pro myfindfile, PATH, $
		PARENT = PARENT, $
		FILES = FILES, $
		DIRECTORIES = DIRECTORIES, $
		FILTER = FILTER

results = 0
files = ""
directories = ""

CD, CURRENT = PARENT					;store the parent into

IF (!version.os EQ "vms") THEN BEGIN			;version is VMS which 
  results = findfile()					;directories have an
  IF(NOT(KEYWORD_SET(results))) THEN RETURN		;extension of ".dir"
  index = 0
  endpath = STRPOS(results(0), "]", 0) + 1
  numfound = N_ELEMENTS(results)
  types = bytarr(numfound)
  FOR i = 0, numfound - 1 DO BEGIN			;strip off the path
    results(i) = STRMID(results(i), endpath, 100)	;that precedes the
    found = STRPOS(results(i), ".DIR", 0)		;filename
    IF (found EQ -1) THEN $
      types(i) = 2 ELSE types(i) = 1
  ENDFOR

  IF(KEYWORD_SET(FILTER)) THEN $
    FILTER = STRUPCASE(FILTER)

ENDIF ELSE BEGIN					;version is not VMS

  IF (N_ELEMENTS(PATH) EQ 0) THEN $			;make sure path is
    PATH = "./" $					;valid
  ELSE BEGIN
    IF(STRPOS(PATH, "/", STRLEN(PATH)) EQ -1) THEN $
      PATH = PATH + "/"
  ENDELSE

  SPAWN, "ls -l " + PATH, results

  numfound = N_ELEMENTS(results)

  IF(NOT(KEYWORD_SET(results))) THEN RETURN		;extension of ".dir"

  types = BYTARR(numfound)

  spaceindices = WHERE(BYTE(results(numfound-1)) EQ 32)
  spaceindex = spaceindices(N_ELEMENTS(spaceindices)-1)

  FOR i = 1, numfound-1 DO BEGIN
    IF(STRPOS(results(i), "d", 0) EQ 0) THEN $		;it is a directory
      types(i) = 1 $
    ELSE IF(STRPOS(results(i), "D", 0) EQ 0) THEN $	;it is a directory(IBM)
      types(i) = 1 $
    ELSE IF(STRPOS(results(i), "-", 0) EQ 0) THEN $	;it is a file
      types(i) = 2 $
    ELSE IF(STRPOS(results(i), "F", 0) EQ 0) THEN $	;it is a file (IBM)
      types(i) = 2 $
    ELSE types(i) = 0
    results(i) = STRMID(results(i), spaceindex + 1, 100)
  ENDFOR

ENDELSE

fileindices = WHERE(types EQ 2, found)
IF(found NE 0) THEN $
  files = results(fileindices)
dirindices = WHERE(types EQ 1, found)
IF(found NE 0) THEN $
  directories = results(dirindices)

IF (KEYWORD_SET(FILTER)) THEN BEGIN
  numleft = N_ELEMENTS(files)
  count = 0
  striped = STRARR(numleft)
  FOR i = 0, numleft - 1 DO BEGIN
    dot = STRPOS(files(i), ".")
    ext = STRMID(files(i), dot + 1, 100)
    IF (!version.os EQ "vms") THEN $
      ext = STRMID(ext, 0, STRPOS(ext, ";"))
    checkext = WHERE(ext EQ FILTER, validext)
    IF validext THEN BEGIN
      striped(count) = files(i)
      count = count + 1
    ENDIF
  ENDFOR
  IF (count GT 0) THEN $
    files = striped(0:count - 1) $
  ELSE files = ""
ENDIF

END


;------------------------------------------------------------------------------
;	procedure Pickfile_ev
;------------------------------------------------------------------------------
; This procedure processes the events being sent by the XManager.
;------------------------------------------------------------------------------
PRO Pickfile_ev, event

COMMON picker, pkstr, directories, files, here, backchars, separator, $
	dirlist, filelist, pathid, pathchars, selecttxt, selection, thefile, $
	thefilter

WIDGET_CONTROL, event.id, GET_UVALUE = eventval		;find the user value
							;of the widget where
							;the event occured
CASE eventval OF

  "CANCEL": WIDGET_CONTROL, event.top, /DESTROY		;There is no need to
							;"unregister" a widget
							;application.  The
							;XManager will clean
							;the dead widget from
							;its list.

  "DIRSEL": BEGIN
      IF (!version.os EQ "vms") THEN BEGIN
	found = STRPOS(directories(event.index), ".", 0)
	CD, STRMID(directories(event.index), 0, found)
      ENDIF ELSE CD, directories(event.index)
      CD, CURRENT = here
      WIDGET_CONTROL, pathid, $
		SET_VALUE = here
      myfindfile, here, FILES = files, $
		DIRECTORIES = directories, $
		FILTER = thefilter
      WIDGET_CONTROL, filelist, $
		SET_VALUE = files
      WIDGET_CONTROL, dirlist, $
		SET_VALUE = directories
    END

  "BACKUP": BEGIN
      CD, backchars
      CD, CURRENT = here
      WIDGET_CONTROL, pathid, $
		SET_VALUE = here
      myfindfile, here, FILES = files, $
		DIRECTORIES = directories, $
		FILTER = thefilter
      WIDGET_CONTROL, filelist, $
		SET_VALUE = files
      WIDGET_CONTROL, dirlist, $
		SET_VALUE = directories

    END

  "FILESEL": BEGIN
      selection = event.index
      thefile = here + files(selection)
      WIDGET_CONTROL, selecttxt, $
		SET_VALUE =  thefile
      WIDGET_CONTROL, event.top, /DESTROY
    END

  "OK": BEGIN
      WIDGET_CONTROL, selecttxt, $
		GET_VALUE = temp
      thefile = temp(0)
      WIDGET_CONTROL, event.top, /DESTROY
    END

  "SELECTTXT": BEGIN
      WIDGET_CONTROL, selecttxt, $
		GET_VALUE = temp
      thefile = temp(0)
      WIDGET_CONTROL, event.top, /DESTROY
    END

  "HELP": XDISPLAYFILE, "", $
		GROUP = event.top, $
		TITLE = "File Selection Help", $
		WIDTH = 50, $
		HEIGHT = 12, $
		TEXT = [ $
			"    The file selection widget lets you pick a ", $
			"file.  The files are shown on the right.  You can", $
			"pick a file by clicking on it with the mouse.  ", $
			"Pressing the 'OK' button will accept the choice", $
			"and the Cancel button will not.  To move to other",$
			"directories from here, you can back up the tree", $
			"of directories by clicking on the 'Up One", $
			"Directory Level' button.  To move into a ", $
			"subdirectory, click on its name in the left ", $
			"directory list.  You can also type in a file name", $
			"in the Selection area of the widget."]

  ELSE: MESSAGE, "Event User Value Not Found"		;When an event occurs
							;in a widget that has
							;no user value in this
							;case statement, an
							;error message is shown
ENDCASE

END ;============= end of Pickfile event handling routine task =============



;------------------------------------------------------------------------------
;	procedure Pickfile
;------------------------------------------------------------------------------
;  This is the actual routine that creates the widget and registers it with the
;  Xmanager.  It also determines the operating system and sets the specific
;  file designators for that operating system.
;------------------------------------------------------------------------------
FUNCTION Pickfile, GROUP = GROUP, $
		PATH = PATH, $
		READ = READ, $
		WRITE = WRITE, $
		FILTER = FILTER, $
		TITLE = TITLE

COMMON picker, pkstr, directories, files, here, backchars, separator, $
	dirlist, filelist, pathid, pathchars, selecttxt, selection, thefile, $
	thefilter

IF(XRegistered("Pickfile")) THEN RETURN, 0		;only one instance of
							;the Pickfile widget
							;is allowed.  If it is
							;already managed, do
							;nothing and return

IF (!version.os EQ "vms") THEN BEGIN			;set the characters 
  backchars = "[-]"					;for the file separator
  separator = ""					;depending on the O.S.
ENDIF ELSE BEGIN
  backchars = ".."
  separator = "/"
ENDELSE

thefile = ""
selection = -1

IF (KEYWORD_SET(PATH)) THEN CD, PATH			;if the user selected
							;a path then use it
CD, CURRENT = here
dirsave = here

IF (KEYWORD_SET(FILTER)) THEN $				;if no filter is given
  thefilter = FILTER $					;use the default which
ELSE $							;is to accept all
  thefilter = ""					;files

myfindfile, here, FILES = files, $			;determine the initial
		DIRECTORIES = directories, $		;files
		FILTER = thefilter

IF (NOT (KEYWORD_SET(TITLE))) THEN $			;build up the title
  TITLE = "Please Select a File"			;based on the keywords
IF (KEYWORD_SET(READ)) THEN $
  TITLE = TITLE + " for Reading"
IF (KEYWORD_SET(WRITE)) THEN $
  TITLE = TITLE + " for Writing"

Pickfilebase = WIDGET_BASE(TITLE = TITLE, $		;create the main base
		/COLUMN)

widebase = WIDGET_BASE(Pickfilebase, /ROW)
label = WIDGET_LABEL(widebase, VALUE = "Path:")
pathid = WIDGET_LABEL(widebase, $
		VALUE = here)

backup = WIDGET_BUTTON(Pickfilebase, $
		VALUE = "Up One Directory Level", $
		UVALUE = "BACKUP")

selections = WIDGET_BASE(Pickfilebase, $
		/ROW, $
		SPACE = 30)

dirs = WIDGET_BASE(selections, $
		/COLUMN, $
		/FRAME)

lbl = WIDGET_LABEL(dirs, $
		VALUE = "Subdirectories          ")

dirlist = WIDGET_LIST(dirs, $
		VALUE = directories, $
		YSIZE = 8, $
		UVALUE = "DIRSEL")

fls = WIDGET_BASE(selections, $
		/COLUMN, $
		/FRAME)

lbl = WIDGET_LABEL(fls, $
		VALUE = "Files                   ")

filelist = WIDGET_LIST(fls, $
		VALUE = files, $
		YSIZE = 8, $
		UVALUE = "FILESEL")

widebase = WIDGET_BASE(Pickfilebase, /ROW)
label = WIDGET_LABEL(widebase, VALUE = "Selection:")
selecttxt = WIDGET_TEXT(widebase, $
		VALUE = "", $
		XSIZE = 42, $
		YSIZE = 1, $
		/FRAME, $
		UVALUE = "SELECTTXT", $
		/EDITABLE)

rowbase = WIDGET_BASE(Pickfilebase, $
		SPACE = 20, $
		/ROW)

ok = WIDGET_BUTTON(rowbase, $
		VALUE = "    Ok    ", $
		UVALUE = "OK")

cancel = WIDGET_BUTTON(rowbase, $
		VALUE = "  Cancel  ", $
		UVALUE = "CANCEL")

help = WIDGET_BUTTON(rowbase, $
		VALUE = "   Help   ", $
		UVALUE = "HELP")

WIDGET_CONTROL, Pickfilebase, /REALIZE			;create the widgets
							;that are defined

XManager, "Pickfile", Pickfilebase, $			;register the widgets
		EVENT_HANDLER = "Pickfile_ev", $	;with the XManager
		GROUP_LEADER = GROUP, $			;and pass through the
		/MODAL					;group leader if this
							;routine is to be 
							;called from some group
							;leader.
CD, dirsave
thefilter = ""

return, thefile						;else return the string
							;name of the file
							;selected

END ;================ end of Pickfile background task =====================




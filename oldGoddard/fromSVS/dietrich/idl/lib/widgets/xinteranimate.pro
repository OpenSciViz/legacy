; Copyright (c) 1991, Research Systems, Inc.  All rights reserved.
;	Unauthorized reproduction prohibited.
;-----------------------------------------------------------------------------
;   procedure xintanim_bck
;-----------------------------------------------------------------------------
; This procedure is the background task for the XInterAnimate Procedure.  The
; XInterAnimate Widget has a speed control that controls the length of the
; time between updates of the animation. If the current time is earlier
; than the the expiration time, this routine simply returns.
; When the current time later than the expiration time, this routine
; increments or decrements the frame pointer and then displays the apropriate
; frame.
;-----------------------------------------------------------------------------

PRO xintanim_bck, TOSS

COMMON XInterAnimate_com, pwin, swin, nframes, xs, ys, $
	animateframe, animatebase, animatepause, animatereverse, $
	animatespeed, animateforward, displayload, $
	framedelta, curframe, delay, currentframe, t0, animatedisplay, $
	t00, rateframe

t = systime(1)						;Current time
IF t ge t0 THEN BEGIN		  			;if it is time to draw
  t0 = t + delay					;Next frame time
  IF framedelta EQ 0 THEN RETURN			;Paused?
  curframe = curframe + framedelta			;New frame
  WHILE curframe LT 0 DO curframe = curframe + nframes	;Into range
  WHILE curframe GE nframes DO curframe = curframe - nframes
  if curframe eq 0 then begin				;Display rate?
	r = nframes / float(t-t00)			;Rate in Frames/Sec
	WIDGET_CONTROL, rateframe, SET_VALUE = $
	string(r, format='(f5.1, " Frames/Sec")')
	t00 = t
	ENDIF
  WSET, swin						;Set to the drawing
							;window
  IF pwin(curframe) ge 0 then $				;Next frame
	DEVICE, COPY = [0, 0, xs, ys, 0, 0, pwin(curframe)] 
  EMPTY
ENDIF

END ;------------------ end of procedure xintanim_bck ------------------------


;-----------------------------------------------------------------------------
;   procedure xintanim_clean
;-----------------------------------------------------------------------------
; This procedure cleans up the variables when the widget dies.
;-----------------------------------------------------------------------------
PRO xintanim_clean, TOSS

COMMON XInterAnimate_com, pwin, swin, nframes, xs, ys, $
	animateframe, animatebase, animatepause, animatereverse, $
	animatespeed, animateforward, displayload, $
	framedelta, curframe, delay, currentframe, t0, animatedisplay, $
	t00, rateframe


FOR i=0,N_ELEMENTS(pwin)-1 DO $
  IF (pwin(i) GE 0) THEN WDELETE,pwin(i)
pwin = -1
nframes = 0

END ;----------------- end of procedure xintanim_clean -----------------------


;-----------------------------------------------------------------------------
;   procedure xintanim_event
;-----------------------------------------------------------------------------
; This procedure processes the events being sent by the XManager.  It adjusts
; the settings for the animation as well as getting and displaying the help.
;-----------------------------------------------------------------------------

PRO xintanim_event, event

COMMON XInterAnimate_com, pwin, swin, nframes, xs, ys, $
	animateframe, animatebase, animatepause, animatereverse, $
	animatespeed, animateforward, displayload, $
	framedelta, curframe, delay, currentframe, t0, animatedisplay, $
	t00, rateframe


  WIDGET_CONTROL, GET_UVALUE = retval, event.id
  CASE retval OF
    "ANIMATESPEED" : BEGIN		;New rate
			WIDGET_CONTROL, animatespeed, GET_VALUE = temp
			if temp eq 100 then delay=0. else delay= 2./(1.+temp)
			t0 = systime(1) + delay		;Reset expire time
  	       	     END

    "ANIMATEPAUSE" : BEGIN
			WIDGET_CONTROL, animatespeed, SENSITIVE = 0
 			XBackRegister, "xintanim_bck", $
				animatebase, $
				/UNREGISTER
			framedelta = 0
			currentframe = curframe
			WIDGET_CONTROL, animateframe, SET_VALUE = currentframe
			WIDGET_CONTROL, animateframe, /SENSITIVE
		     END

    "ANIMATEFRAME" : BEGIN
		       WIDGET_CONTROL, animateframe, GET_VALUE = curframe
    		       IF (curframe NE currentframe) THEN BEGIN
			 currentframe = curframe
			 DEVICE, COPY = [0, 0, xs, ys, 0, 0, pwin(curframe)] 
       			 EMPTY
       		       ENDIF
		     END

    "ANIMATEFORWARD" : BEGIN
			WIDGET_CONTROL, animateframe, SENSITIVE = 0
			IF(framedelta EQ 0) THEN $
 			  XBackRegister, "xintanim_bck", $
				animatebase
			framedelta = 1
			WIDGET_CONTROL, animatespeed, /SENSITIVE
		       END

    "ANIMATEREVERSE" : BEGIN
			WIDGET_CONTROL, animateframe, SENSITIVE = 0
			IF(framedelta EQ 0) THEN $
			  XBackRegister, "xintanim_bck", $
				animatebase
			framedelta = -1
			WIDGET_CONTROL, animatespeed, /SENSITIVE
		       END

    "ANIMHELP" : xdisplayfile, "animatedemo.hlp", $
			TITLE = "Animation Help", $
			GROUP = event.top, $
			WIDTH = 55, $
			HEIGHT = 16, $
			TEXT = [					$
	"                                                       ", 	$
	"      Xinteranimate is used for displaying a series of", 	$
	"images created with IDL as an animation.  The user can", 	$
	"select the speed, direction or specific frames in the", 	$
	"animation.", 							$
	"      The top slider is used to control the speed of", 	$
	"the animation.  Moving it to the far right is one hundred", 	$
	"percent, as fast as the animation can go.  If there are", 	$
	"other IDL widget applications using background tasks,", 	$
	"they can slow down the animation.  Closing the other", 	$
	"applications can speed up the animation.  When the", 		$
	"animation is paused, the speed slider is inactive.", 		$
	"      The middle three buttons are reverse play, pause", 	$
	"and forward play.  Use them to select a direction or to", 	$
	"pause the animation and view specific frames.", 		$
	"      Finally, the bottom slider is used to view", 		$
	"certain frames from the animation.  The animation must", 	$
	"be paused to use the frame selection slider."	]

    "ANIMPALADJ" : XLOADCT, GROUP = event.top

    "ANIMEXIT": XInterAnimate, /close

    ELSE:
  ENDCASE

END ;----------------- end of procedure xintanim_event -----------------------



PRO XInterAnimate, RATE, SET = SET, IMAGE = IMAGE, FRAME = FRAME, $
			 ORDER = ORDER, CLOSE = CLOSE, TITLE = TITLE, $
			 SHOWLOAD = SHOWLOAD, GROUP = GROUP, WINDOW = WINDOW, $
			 XOFFSET = XOFFSET, YOFFSET = YOFFSET 
;+
; NAME:
;		XInterAnimate
; PURPOSE:
;	Display an animated sequence of images using Xwindows Pixmaps.
;	The speed and direction of the display can be adjusted using 
;	the widget interface.
; CATEGORY:
;	Image display, Widgets
; CALLING SEQUENCE:
;   To initialize:
;	XInterAnimate, SET = [ Sizex, Sizey, Nframes ]
;   To load a single image:
;	XInterAnimate, IMAGE = Image, FRAME = frame_index
;   To load a single image that is already displayed in an existing window:
;	XANIMATE, FRAME = Frame_index, WINDOW = [ Window [, X0, Y0, Sx, Sy]]
;	  (This is much faster than reading back from the window)
;   After all the images have been loaded, To display:
;	XInterAnimate, speed (here speed is a percentage of maximum speed)
;   To close and deallocate the pixmap / buffer (this takes place
;		automatically when the user presses the "Done With Animation"
;		button or closes the window with the window manager:
;	XInterAnimate, /CLOSE
; INPUTS:
;	RATE =  Optional rate of display.  The rate is a value between
;		0 and 100 that gives the speed that the animation is displayed.
;		The fastest animation is with a value of 100 and the slowest
;		is with a value of 0.  The default value is 100 if not
;		specified.  The animation must be initialized using the SET
;		keyword before calling Xinteranimate with a rate value.
; KEYWORD PARAMETERS:
;	CLOSE = Deletes offscreen pixwins and Widget, freeing storage.
;	FRAME = The frame number when loading frames (only takes effect 
;		when used in conjunction with the SET keyword).
;	GROUP = The widget ID of the widget that calls Xinteranimate.  When 
;		this ID is specified, a death of the caller results in a 
;		death of Xinteranimate
;	IMAGE = Loads a single image at the animation position given by FRAME.
;		FRAME must be in the range of 0 to Nframes-1.
;		The keyword parameter FRAME must also be specified.
;	ORDER = Set if images run from top down, omitted or zero if
;		images go from bottom to top.  Only used when loading
;		images.
;	SHOWLOAD = This keyword can be used in conjunction with the
;		SET keyword to display each frame and update the frame 
;		slider as frames are loaded.
;	SET = Initializes XInterAnimate.  SET should be equated to a
;		3 element integer vector  containing the following parameters:
;		  Sizex, Sizey = the size of the images to be displayed, in
;				  pixels.
;		  Nframes = number of frames in animated sequence (since 
;			this creates an animation, Nframes must be at
;			least 2 frames).
;	TITLE = String to set the title of the Widget.  When not specified, 
;		the title is set to "XInterAnimate" (only takes effect when 
;		used in conjunction with the SET keyword).
;	WINDOW = Copies from an existing window to the pixmap.  When
;		using X windows, this is much faster than reading
;		from the display and then calling XANIMATE with a 2D array.
;		The value of this parameter is either a simple window
;		index (in which case the entire window is copied),
;		or a vector containing the window index and
;		rectangular bounds of the area to be copied.
;		[ Xsrc, Ysrc, Nx, Ny, Xdst, Ydst]
;	XOFFSET = The horizontal offset in pixels of image in the destination
;		window.
;	YOFFSET = The vertical offset(from the bottom of the frame) in pixels
;		the image will be displayed in the destination window.
; OUTPUTS:
;	No explicit outputs.
; COMMON BLOCKS:
;	XInterAnimate_COM - private common block.
; SIDE EFFECTS:
;	A pixmap and Widget are created.
; RESTRICTIONS:
;	An animation may not have more than approximately 127 frames.
;
; PROCEDURE:
;	When initialized this procedure creates an approximately square
;	pixmap or memory buffer, large enough to contain Nframes of
;	the requested size.  Once the images are loaded, by using the 
;	IMAGE and FRAME keywords, they are displayed by copying the images 
;	from the pixmap or buffer to the visible draw widget.
; MODIFICATION HISTORY:
;	DMS, April, 1990.
;	SMR, December, 1990.  Modified the XANIMATE code to work
;				 interactively with widgets.
;	DMS, March, 1991.     Modified the routine to use individual pixmaps
;				 for each frame of the animation.  Also added
;				 the ability to read in from current IDL 
;				 windows directly into offscreen bitmap.
;	SMR, March, 1991.     Modified to use new XMANAGER keyword CLEANUP
;				 to clean up the offscreen pixmaps when
;				 dying.
;	SMR, Jan, 1992.	      Modified the /close portion to check for a valid
;				 widget before using WIDGET_CONTROL and /DESTR.
;-

COMMON XInterAnimate_com, pwin, swin, nframes, xs, ys, $
	animateframe, animatebase, animatepause, animatereverse, $
	animatespeed, animateforward, displayload, $
	framedelta, curframe, delay, currentframe, t0, animatedisplay, $
	t00, rateframe


initialspeed = 100		;Top speed

;--------------------- CLOSE Portion of Xinteranimate -------------------------

IF KEYWORD_SET(CLOSE) THEN BEGIN		;when close keyword set, 
  XBackRegister, "xintanim_bck", $
		animatebase, $
		/UNREGISTER 
  i = N_ELEMENTS(pwin)
  if i le 1 THEN RETURN
  FOR j=0, i-1 DO IF pwin(j) GE 0 THEN WDELETE, pwin(j) ;Delete the windows
  pwin = -1		;Show nothing there
  WIDGET_INFO, WIDGET_ID = animatebase, VALID = validids
  IF(validids(0) EQ 1) THEN $
	WIDGET_CONTROL, animatebase, /DESTROY
  RETURN
ENDIF

IF xregistered("XInterAnimate") THEN RETURN	;Don't allow two copies of
						;xinternimate to run at once

;----------------------  SET Portion of Xinteranimate ------------------------

IF KEYWORD_SET(SET) THEN BEGIN
;This is the first call to xinteranimate.  Here the pixmap is
;	created and the widgets are initialized.

  old_window = !D.WINDOW			;Save old window

  IF N_ELEMENTS(pwin) GT 0 THEN BEGIN		;If old pixmap exists, delete 
    FOR i=0,N_ELEMENTS(pwin)-1 do if pwin(i) ge 0 then wdelete,pwin(i)
    pwin = -1
  ENDIF

  xs = SET(0)					;Determine the xsize and ysize
  ys = SET(1)					;of the display window and
  nframes = SET(2)				;number of frames
  IF (nframes LE 1) THEN $
	MESSAGE, "animations must have 2 or more frames"
  pwin = replicate(-1, nframes)			;Array of window indices


  IF NOT(KEYWORD_SET(TITLE)) THEN $		;Create the widgets used for
	  	TITLE = "XInterAnimate"		;the application
  animatebase = WIDGET_BASE(/ROW, $
		TITLE = TITLE)
  animatecntl = WIDGET_BASE(animatebase, $
		/COLUMN, $
		/FRAME, $
		XPAD = 10, $
		YPAD =30)
  XPdMenu, [	'"End Animation"	ANIMEXIT',	$
		'"Adjust Color Palette"	ANIMPALADJ',	$
		'"Help"			ANIMHELP'],	$
		animatecntl
  animatespeed = WIDGET_SLIDER(animatecntl, $
		XSIZE = 240, $
		UVALUE = "ANIMATESPEED", $
		/DRAG, $
		VALUE = initialspeed, $
		MAXIMUM = 100, $
		MINIMUM = 0, $
		TITLE = "Animation Speed")
  animatebuts = WIDGET_BASE(animatecntl, $
		/ROW)
  animatereverse = widget_button(animatebuts, $
		VALUE = "      <      ", $
		UVALUE = "ANIMATEREVERSE")
  animatepause = WIDGET_BUTTON(animatebuts, $
		VALUE = "      []      ", $
		UVALUE = "ANIMATEPAUSE")
  animateforward = WIDGET_BUTTON(animatebuts, $
		VALUE = "      >      ", $
		UVALUE = "ANIMATEFORWARD")
  animateframe = WIDGET_SLIDER(animatecntl, $
		XSIZE = 240, $
		UVALUE = "ANIMATEFRAME", $
		/DRAG, $
		VALUE = 0, $
		MAXIMUM = nframes - 1, $
		TITLE = "Animation Frame", $
		MINIMUM = 0)
  rateframe = WIDGET_TEXT(animatecntl, $
		xsize = 15, ysize=1)
  i = WIDGET_BASE(animatebase, /COLUMN)	;To prevent stretching
  animatedisplay = WIDGET_DRAW(i, $
		XSIZE = xs, $
		YSIZE = ys, $
		XOFFSET = 280, $
		YOFFSET = 20, $
		RETAIN = 0)
  WIDGET_CONTROL, animatebase, SENSITIVE = 0

  IF KEYWORD_SET(SHOWLOAD) THEN BEGIN		;Finally, if the SHOWLOAD
    displayload = 1				;keyword was set, display
    WIDGET_CONTROL, animatebase, $		;the widgets
		/REALIZE
    EMPTY
    WIDGET_CONTROL, animatedisplay, $
		GET_VALUE = swin
    WSET, swin					;Return with new window set
    xs = !d.x_vsize				;Maybe window was not req size
    ys = !d.y_vsize
  ENDIF ELSE BEGIN
    displayload = 0
    IF (old_window GE 0) THEN WSET, old_window
  ENDELSE
  RETURN
ENDIF

;-----------------  IMAGE Loading Portion of Xinteranimate --------------------

old_window = !D.WINDOW				;Save old window

IF (N_ELEMENTS(pwin) LE 1) THEN $		;Here, windows must exist
  MESSAGE,"Not initialized"			;to do the loading so exit

IF (N_ELEMENTS(FRAME)) GT 0 THEN BEGIN		;Check frame parameter and make
  IF (FRAME LT 0) OR (FRAME GE nframes) THEN $	;sure it in in a valid range
    MESSAGE, "Frame number must be" + $
		" from 0 to nframes -1."
ENDIF

IF (N_ELEMENTS(YOFFSET) EQ 0) THEN YOFFSET = 0	;are zero unless specified.
IF (N_ELEMENTS(XOFFSET) EQ 0) THEN XOFFSET = 0	;The defaults for the offsets

j = N_ELEMENTS(WINDOW)				;check to see if WINDOW was set

IF (j GT 0) THEN BEGIN				;Copy image from window?
  IF (j LT 5) THEN BEGIN			;If coords not spec, use all
    WSET, WINDOW(0)
    p = [ WINDOW(0), 0, 0, !D.X_VSIZE, !D.Y_VSIZE ]  ;Get size of window
  ENDIF ELSE p = WINDOW

  if pwin(frame) lt 0 then BEGIN		;Create pixwin?
	WINDOW, /FREE,      $
		xsize = xs, $
		ysize = ys, $
		/pixmap
	pwin(frame) = !d.window
  endif

  IF (p(3) GT xs) OR (p(4) GT ys) THEN $
    MESSAGE, "Window parameter larger than setup"

  IF displayload THEN BEGIN		;Load display window
    WSET, swin				;Show it?
    IF swin ne p(0) THEN $		;Copy to show window?
        DEVICE, COPY = [ p(1), p(2), p(3), p(4), xoffset, yoffset, p(0)]
    WSET, pwin(frame)			;Pixmap destination
		;Copy from display window to pixmap
    DEVICE, COPY = [ xoffset, yoffset, p(3), p(4), xoffset, yoffset, swin ]
  ENDIF ELSE BEGIN			;load / no show
	wset, pwin(frame)
	DEVICE, COPY = [ p(1), p(2), p(3), p(4), xoffset, yoffset, p(0)]
  ENDELSE

  empty
  IF (N_ELEMENTS(swin) EQ 0) THEN swin = -1

  IF (old_window GE 0) THEN WSET, old_window

  IF displayload THEN $				;When displayload is set, the
    WIDGET_CONTROL, animateframe, $		;frame slider should be updated
		SET_VALUE = FRAME		;to show frame number
  RETURN
ENDIF						;So WINDOW was not set.

IF N_ELEMENTS(IMAGE) NE 0  THEN BEGIN		;Make sure image was set.

  IF (displayload NE 0) THEN BEGIN		;When displayload is set, the
    WIDGET_CONTROL, animateframe, $		;draw widget should be updated
		SET_VALUE = FRAME		;to show the new frame being
    WSET, swin					;loaded and the frame slider
    TV, IMAGE					;should be set correspondingly
    EMPTY
  ENDIF

  s = SIZE(IMAGE)				;Make sure the image is of a
  if ((s(0) NE 2) OR $				;valid size and report if not.
      (s(1) GT xs) OR $
      (s(2) GT ys)) THEN $
    MESSAGE, "Image parameter must be 2D " + $
	"of size" + STRING(xs)+ STRING(ys)

  IF N_ELEMENTS(ORDER) EQ 0 THEN ORDER = 0  	;Default order
  if pwin(frame) lt 0 then BEGIN
		WINDOW, /FREE, xsize = xs, ysize = ys, /pixmap
		pwin(frame) = !d.window
  endif else WSET, pwin(frame)
  TV, IMAGE, xoffset, yoffset, ORDER = ORDER
  EMPTY
  IF (old_window GE 0) THEN WSET, old_window
  RETURN
ENDIF						;End of "if IMAGE was set".

;---------------  Register and Run Portion of Xinteranimate -------------------

IF (displayload EQ 0)THEN BEGIN			;If display load was not 
  WIDGET_CONTROL, animatebase, $		;register all the widgets
		/REALIZE			;now since all the frames
  WIDGET_CONTROL, animatedisplay, $		;have been loaded
		GET_VALUE = swin
ENDIF

EMPTY

WIDGET_CONTROL, animatebase, /SENSITIVE 
WIDGET_CONTROL, animateframe, SENSITIVE = 0

curframe = 0					;Set up the initial values
framedelta = 1					;used by the background task

IF N_ELEMENTS(RATE) EQ 0 THEN RATE = INITIALSPEED  ;Default rate
RATE = 0 > RATE < 100				;for the animation making sure
WIDGET_CONTROL, animatespeed, SET_VALUE = RATE
if rate eq 100 then delay=0.0 else delay = 2./(1.+rate)
delay = (100-rate) * 0.01			;To 100ths of a sec
t00 = systime(1)				;Start of loop time
t0 =  t00 + delay				;Expiration time

Xmanager, "XInterAnimate", animatebase, $	;Register with the manager and
	  EVENT_HANDLER = "xintanim_event", $	;let the background task do the
	  BACKGROUND = "xintanim_bck", $	;frame update and the event
	  GROUP_LEADER = GROUP, $		;routine handle the events.
	  CLEANUP = "xintanim_clean"
END

; Copyright (c) 1991, Research Systems, Inc.  All rights reserved.
;	Unauthorized reproduction prohibited.
PRO XManTool_bck, topid

COMMON MANAGED, names, ids, handlers, $
		nummanaged, inuseflag, $
		backroutines, backroutineids, backnumber, nbacks, $
		cleanups
COMMON MYMANTOOL, mylast, toollist, toolbase, backlist, mynum, selectedwid, $
		litcontbase, enabler, disabler, disablelist, selectedback, $
		ritcontbase, backonoff, lastback, mybacknum

IF((mylast NE names(nummanaged - 1)) OR $
   (nummanaged NE mynum) OR $
   (lastback NE backroutines(nbacks -1)) OR $
   (nbacks NE mybacknum)) THEN BEGIN
  WIDGET_CONTROL, toollist, $
		SET_VALUE = names
  filler = "                                           "
  fullnames = STRARR(N_ELEMENTS(backroutines))
  IF(KEYWORD_SET(backroutines)) THEN BEGIN
    backonoff = STRARR(N_ELEMENTS(backroutines))
    FOR i = 0, N_ELEMENTS(backroutines) - 1 DO BEGIN
      length = STRLEN(backroutines(i))
      ownername = names(WHERE(ids EQ backroutineids(i)))
      fullnames(i) = backroutines(i) + $
        STRMID(filler, 0, 25-length) + ownername
    ENDFOR
  WIDGET_CONTROL, backlist, $
		SET_VALUE = fullnames
  ENDIF
  mylast = names(nummanaged - 1)
  mynum = nummanaged
  lastback = backroutines(nbacks - 1)
  mybacknum = nbacks
ENDIF
END

;-----------------------------------------------------------------------------

PRO XManTool_event, event

COMMON MANAGED, names, ids, handlers, $
		nummanaged, inuseflag, $
		backroutines, backroutineids, backnumber, nbacks, $
		cleanups
COMMON MYMANTOOL, mylast, toollist, toolbase, backlist, mynum, selectedwid, $
		litcontbase, enabler, disabler, disablelist, selectedback, $
		ritcontbase, backonoff, lastback, mybacknum

WIDGET_CONTROL, event.id, GET_UVALUE = evntval

CASE evntval OF
  "COLADJ": xloadct, GROUP = event.top

  "TOOLLIST" : BEGIN
		 selectedwid = event.index
		 WIDGET_CONTROL, litcontbase, $
			/SENSITIVE
	       END

  "DISABLER" : BEGIN
		 backroutines(selectedwid) = ""
		 backindexes = WHERE(STRLEN(backroutines), nbacks)
	       END

  "KILLER" : BEGIN
	       WIDGET_CONTROL, litcontbase, $
			SENSITIVE = 0
	       WIDGET_CONTROL, ids(selectedwid), /DESTROY
	     END

  "SHOWSTER" : BEGIN
	       WIDGET_CONTROL, litcontbase, $
			SENSITIVE = 0
	       WIDGET_CONTROL, ids(selectedwid), /SHOW
	     END

  "DONE": WIDGET_CONTROL, event.top, /DESTROY

  ELSE:
ENDCASE

END
;-----------------------------------------------------------------------------

PRO XManagerTool, GROUP = GROUP

;+
; NAME:
;	XManagerTool
; PURPOSE:
;	Provides a tool for viewing Widgets currently being managed
;	by the XManager.
; CATEGORY:
;	Widgets
; CALLING SEQUENCE:
;	XManager
; KEYWORD PARAMETERS:
;	GROUP = The widget id of the group leader that the XManagerTool 
;		is to live under.  If the group is destroyed, then
;		the XManagerTool will also be destroyed.
; COMMON BLOCKS:
;	MYMANTOOL = common block that keeps track of the state of the XManager
;		the last time the XManagerTool background routine was called
; SIDE EFFECTS:
;	Creates a widget that has the capacity to destroy other widgets being
;	managed by the XManager.
; RESTRICTIONS:
;	Only one instance of the XManagerTool can run at one time.
; PROCEDURE:
;	Initiate the widget and then let the background routine update the
;	lists as the widgets being managed by the XManager are changed.
; MODIFICATION HISTORY:
;	Written by Steve Richards,	Dec, 1990
;-

COMMON MANAGED, names, ids, handlers, $
		nummanaged, inuseflag, $
		backroutines, backroutineids, backnumber, nbacks, $
		cleanups
COMMON MYMANTOOL, mylast, toollist, toolbase, backlist, mynum, selectedwid, $
		litcontbase, enabler, disabler, disablelist, selectedback, $
		ritcontbase, backonoff, lastback, mybacknum

IF(XRegistered("XManagerTool")) THEN RETURN

toolbase = WIDGET_BASE(TITLE = "Xmanager Tool", $
		/COLUMN, $
		SPACE = 10)
buttonbase = WIDGET_BASE(toolbase, /ROW)
tooldone = WIDGET_BUTTON(buttonbase, $
		VALUE = "Exit Xmanager Tool", $
		UVALUE = "DONE")
coladjust = WIDGET_BUTTON(buttonbase, $
		VALUE = " Adjust Color Palette", $
		UVALUE = "COLADJ")
abase = WIDGET_BASE(toolbase, $
		/ROW, $
		SPACE = 20)
litbase = WIDGET_BASE(abase, $
		/COLUMN, $
		/FRAME, $
		SPACE = 10)
toollabel = WIDGET_LABEL(litbase, $
		VALUE = "Registered Widgets")
toollist = WIDGET_LIST(litbase, $
		VALUE = "XmanagerTool      ", $
		YSIZE = 10, $
		UVALUE = "TOOLLIST")
litcontbase = WIDGET_BASE(litbase, $
		/ROW)
showster = WIDGET_BUTTON(litcontbase, $
		VALUE = "Bring To Front", $
		UVALUE = "SHOWSTER")
killer = WIDGET_BUTTON(litcontbase, $
		VALUE = "Kill Widget", $
		UVALUE = "KILLER")
ritbase = WIDGET_BASE(abase, $
		/COLUMN, $
		/FRAME, $
		SPACE = 10)
backlabel = WIDGET_LABEL(ritbase, $
		VALUE = "Background Tasks         Owners            ")
backlist = WIDGET_LIST(ritbase, $
		VALUE = "                                           ", $
		YSIZE = 10, $
		UVALUE = "BACKLIST")

WIDGET_CONTROL, litcontbase, SENSITIVE = 0
WIDGET_CONTROL, toolbase, /REALIZE

IF(KEYWORD_SET(nummanaged)) THEN BEGIN
  mylast = ''
  mynum = nummanaged
ENDIF ELSE BEGIN
  mylast = ''
  mynum = 0
ENDELSE

lastback = ''
mybacknum = 0

IF KEYWORD_SET(nbacks) THEN backonoff = strarr(nbacks) $
ELSE backonoff = 0

Xmanager, "XManagerTool", $
		toolbase, $
		BACKGROUND = "XManTool_bck", $
		EVENT_HANDLER = "XManTool_event", $
		GROUP_LEADER = GROUP

END


; Copyright (c) 1991, Research Systems, Inc.  All rights reserved.
;	Unauthorized reproduction prohibited.
;+
; NAME:
;	SLIDE_IMAGE
; PURPOSE:
;	Create a scrolling graphics window for examining large images.
;	By default, 2 draw widgets are used; The leftmost widget shows
;	a reduced version of the complete image, while the rightmost
;	widget contains the actual image with scrollbars that allow sliding
;	the visible window.
; CALLING SEQUENCE:
;	SLIDE_IMAGE [, Image]
; INPUTS:
;	Image: If present, this is the image to be displayed. If no
;		image is present, no image is displayed. The FULL_WINDOW
;		and SCROLL_WINDOW keywords can be used to obtain the window
;		numbers of the 2 draw widgets in order to draw into them
;		at a later point.
; KEYWORDS:
;	CONGRID: Normally, the image is processed with the CONGRID
;		procedure before it is written to the fully visible
;		window on the left. Specifying CONGIRD=0 will force
;		the image to be drawn as is.
;	FULL_WINDOW: If this keyword is present, FULL_IMAGE will store the
;		IDL window number of the non-sliding window in the named
;		variable it specifies. This can be used with the WSET
;		procedure to draw to the scrolling window at a later point.
;		If SHOW_FULL is zero, this keyword returns -1 to indicate
;		that no such window exists.
;	GROUP:	This keyword tells the XMANAGER to destroy the SLIDE_IMAGE
;		widget when the routine registering it dies.  The base 
;		widget ID of the SLIDE_IMAGE caller should be passed as
;		the GROUP keyword.
;	ORDER: This keyword is passed directly to the TV procedure
;		to control the order in which the images are drawn.
;	REGISTER: The basic widgets used in this procedure do not generate
;		widget events, so it is not necessary to process events
;		in an event loop. The default is therefore to simply create
;		the widgets and return. This frees the user to type
;		commands to the "IDL>" prompt which use the widgets.
;		However, if REGISTER is present and non-zero, a "Done"
;		button is added to the widgets, and they are registered
;		with the XMANAGER procedure.
;	RETAIN: This keyword is passed directly to the WIDGET_DRAW
;		function, and controls the type of backing store
;		used for the draw windows. If not present, a value of
;		1 is used, which requests backing store from the window
;		system.
;	SHOW_FULL: If this keyword is present and zero, the leftmost window
;		containing the fully visible image will not be created,
;		leaving only a single scrolling window for the full image.
;	SLIDE_WINDOW: If this keyword is present, SLIDE_IMAGE will store the
;		IDL window number of the sliding window in the named variable
;		it specifies. This can be used with the WSET procedure
;		to draw to the scrolling window at a later point.
;	TITLE - The title to be used on the window frame.
;	TOP_ID - If present, the base widget ID of this widget hierarchy
;		is stored in the named variable provided. This
;		ID can be used to kill the hierarchy:
;			SLIDE_IMAGE, BASE_ID=base, ...
;			.
;			.
;			.
;			WIDGET_CONTROL, /DESTROY, base
;	XSIZE - The maximum width of the image that can be displayed by
;		the scrolling window. This should not be confused with the
;		visible size of the image, which is controlled by the XVISIBLE
;		keyword. If XSIZE is not present: If IMAGE is specified,
;		its width is used, otherwise 256.
;	XVISIBLE: The width of the viewport on the scrolling window. If
;		this keyword is not present, 256 is used.
;	YSIZE - The maximum height of the image that can be displayed by
;		the scrolling window. This should not be confused with the
;		visible size of the image, which is controlled by the YVISIBLE
;		keyword. If YSIZE is not present: If IMAGE is specified,
;		its height is used, otherwise 256.
;	YVISIBLE: The height of the viewport on the scrolling window. If
;		this keyword is not present, 256 is used.
; OUTPUTS:
;	None.
; COMMON BLOCKS:
;	None.
; SIDE EFFECTS:
;	Widgets for displaying a very large image are created.
;	The user typically uses the window manager to destroy
;	the window, although the TOP_ID keyword can also be used to
;	obtain the widget id to use in destroying it via WIDGET_CONTROL.
; RESTRICTIONS:
;	Scrolling windows don't work if backing store is not provided.
;	They work best with window system provided backing store
;	(RETAIN=1), but are also usable with IDL provided
;	backing store (RETAIN=2).
;
;	Various machines place different restrictions on the size of the
;	actual image that can be handled.
; MODIFICATION HISTORY:
;	7 August, 1991, Written by AB, RSI.
;-


pro SLIDE_IMG_EVENT, ev
  WIDGET_CONTROL, ev.top, /DESTROY
end







pro slide_image, image, CONGRID=USE_CONGRID, ORDER=ORDER, REGISTER=REGISTER, $
	RETAIN=RETAIN, SHOW_FULL=SHOW_FULL, SLIDE_WINDOW=SLIDE_WINDOW, $
	XSIZE=XSIZE, XVISIBLE=XVISIBLE, YSIZE=YSIZE, YVISIBLE=YVISIBLE, $
	TITLE=TITLE, TOP_ID=BASE, FULL_WINDOW=FULL_WINDOW, GROUP = GROUP

  if (n_params() ne 0) then begin
    image_size = SIZE(image)
    if (image_size(0) ne 2) then message,'Image must be a 2-D array'
    if (n_elements(XSIZE) eq 0) then XSIZE = image_size(1)
    if (n_elements(YSIZE) eq 0) then YSIZE = image_size(2)
  endif else begin
    image_size=bytarr(1)
    if (n_elements(XSIZE) eq 0) then XSIZE = 256
    if (n_elements(YSIZE) eq 0) then YSIZE = 256
  endelse
  if (n_elements(xvisible) eq 0) then XVISIBLE=256
  if (n_elements(Yvisible) eq 0) then YVISIBLE=256
  if(not KEYWORD_SET(SHOW_FULL)) THEN SHOW_FULL = 1
  if(not KEYWORD_SET(ORDER)) THEN ORDER = 0
  if(not KEYWORD_SET(USE_CONGRID)) THEN USE_CONGRID = 1
  if(n_elements(RETAIN) eq 0) THEN RETAIN = 1
  if(n_elements(TITLE) eq 0) THEN TITLE='Slide Image'
  if(not KEYWORD_SET(REGISTER)) THEN REGISTER = 0

  if (REGISTER) then begin
    base = WIDGET_BASE(title=title, /COLUMN)
    junk = WIDGET_BUTTON(WIDGET_BASE(base), value='Done')
    ibase = WIDGET_BASE(base, /ROW)
  endif else begin
    base = WIDGET_BASE(title=title, /ROW)
    ibase = base
  endelse

  if (SHOW_FULL) then begin
      fbase = WIDGET_BASE(ibase, /COLUMN, /FRAME)
        junk = WIDGET_LABEL(fbase, value='Full Image')
        all = widget_draw(fbase,retain=retain,xsize=xvisible,ysize=yvisible)
      sbase = WIDGET_BASE(ibase, /COLUMN, /FRAME)
        junk = WIDGET_LABEL(sbase, value='Full Resolution')
        scroll = widget_draw(sbase, retain=retain,xsize=xsize,ysize=ysize, $
		/scroll, x_scroll_size=xvisible, y_scroll_size=yvisible)
    WIDGET_CONTROL, /REAL, base
    WIDGET_CONTROL, get_value=FULL_WINDOW, all
  endif else begin
    scroll = widget_draw(ibase, retain=retain, xsize=xsize, ysize=ysize, $
	/frame, /scroll, x_scroll_size=xvisible, y_scroll_size=yvisible)
    WIDGET_CONTROL, /REAL, base
    FULL_WINDOW=-1
  endelse

  WIDGET_CONTROL, get_value=SLIDE_WINDOW, scroll

  ; Show the image(s) if one is present
  if (image_size(0) ne 0) then begin
    cur_win = !d.window
    if (SHOW_FULL) then begin
      WSET, FULL_WINDOW
      if (use_congrid) then begin
	TV, congrid(image, xvisible, xvisible), ORDER=ORDER
      endif else begin
	TV, image, ORDER=ORDER
      endelse
    endif
  WSET, SLIDE_WINDOW
  TV, image, ORDER=ORDER
  if (cur_win ne -1) then WSET, cur_win
  endif

  if (REGISTER) then XMANAGER, 'SLIDE_IMAGE', base, event='SLIDE_IMG_EVENT', $
	GROUP_LEADER = GROUP

end

; Copyright (c) 1991, Research Systems, Inc.  All rights reserved.
;	Unauthorized reproduction prohibited.
;+
; NAME:		XPALETTE
; PURPOSE:	Interactive creation of color tables based on
;		the RGB color system using the mouse, three sliders,
;		and a cell for each color index.  Single colors may be
;		defined or multiple color indices between two endpoints
;		may be interpolated.
; CATEGORY:	Color tables, widgets
; CALLING SEQUENCE:
;	PALETTE
; INPUTS:
;	No explicit inputs.  The current color table is used as a starting
;	point.
; KEYWORD PARAMETERS:
;	None.
; OUTPUTS:
;	colors_out = optional output parameter.  Will contain
;	  (Number_colors, 3) color values of final color table.
;	  Floating point array.
; COMMON BLOCKS:
;	COLORS - contains the current RGB color tables.
;	XP_COM - Private to this module.
; SIDE EFFECTS:
;	XPALETTE uses two colors from the current color table as
;	drawing foreground and background colors. These are used
;	for the RGB plots on the left, and the current index marker on
;	the right. This means that if the user set these two colors
;	to the same value, the XPALETTE display could become unreadable
;	(like writing on black paper with black ink). XPALETTE minimizes
;	this possibility by noting changes to the color map and always
;	using the brightest available color for the foreground color
;	and the darkest for the background. Thus, the only way
;	to make XPALETTE's display unreadable is to set the entire color
;	map to a single color, which is highly unlikely. The only side
;	effect of this policy is that you may notice XPALETTE redrawing
;	the entire display after you've modified the current color.
;	This simply means that the change has made XPALETTE pick new
;	drawing colors.
;
;	The new color tables are saved in the COLORS common and loaded
;	to the display.
; PROCEDURE:
;	The display consists of the following sections:
;
;		Left: Three plots showing the current Red, Green,
;		    and blue vectors.
;
;		Center (top): A status region containing:
;		    1) The total number of colors.
;		    2) The current color. XPALETTE allows changing
;		       one color at a time. This color is known as
;		       the "current color" and is indicated in the
;		       color spectrum display with a special marker.
;		    3) The current mark index. The mark is used to
;		       remember a color index. It is established by
;		       pressing the "Set Mark Button" while the current
;		       color index is the desired mark index.
;		    4) The current color. The special marker used in
;		       color spectrum display prevents the user from seeing
;		       the color of the current index, but it is visible
;		       here.
;
;		Center (middle): A panel of control buttons, which do
;		    the following when pressed:
;
;		        Done: Exits XPALETTE.
;		        Predefined: Starts XLOADCT to allow selection of
;			    one of the predefined color tables. Note that
;			    when you change the color map via XLOADCT, XPALETTE
;			    is not always able to keep its display accurate.
;			    This can be overcome by pressing the XPALETTE
;			    "Redraw" button after changing things via XLOADCT.
;		        Help: Starts XDL (A graphical user interface version
;			    of DOC_LIBRARY) with this documentation as its
;			    initial selection
;			Redraw: Completely redraws the display using the
;			    current state of the color map.
;			Set Mark: Set the value of the mark index to the
;			    current index.
;			Switch Mark: Exchange the mark and the current index.
;			Copy Current: Every color lying between the current
;			    index and the mark index (inclusive) is given
;			    the current color.
;			Interpolate: The colors lying between the current
;			    index and the mark index are interpolated linearly
;			    to lie between the colors of two endpoints.
;
;		Center (bottom): Three sliders (R, G, and B) which
;		    allow the user to modify the current color.
;
;		Right: A display which shows the current color map as
;		    a series of squares. Color index 0 is at the upper
;		    left. The color index increases monotonically by
;		    rows going left to right and top to bottom.
;		    The current color index is indicated by a special
;		    marker symbol. There are 4 ways to change the
;		    current color:
;		        1) Press any mouse button while the mouse
;			   pointer is over the color map display.
;			2) Use the "By Index" slider to move to
;			   the desired color index.
;			3) Use the "Row" Slider to move the marker
;			   vertically.
;			4) Use the "Column" Slider to move the marker
;			   horizontally..
; MODIFICATION HISTORY:
;						July 1990, AB,
;	Based on the PALETTE procedure, which does similar things
;	using only basic IDL graphics commands.
;	7 January 1991, Re-written for general use.
;-

pro XP_SET_SLIDERS, type, cur_x, cur_y, idx
; Set the three position sliders according to the supplied values.
; Move the mark to the new location.
; entry:
;	type - Controls operation of procedure. Can have the following
;		values:
;		0 - Set all three sliders from IDX. state.x and state.y
;		    are updated.
;		1 - Set Row and Column sliders from IDX. Update
;		    state.x and state_y.
;		2 - Set Column and Index sliders from CUR_X and CUR_Y.
;		3 - Set Row and Index sliders from CUR_X and CUR_Y.
  common xp_com, xpw, state

  change_x = 0
  change_y = 0
  change_idx = 0
  nc = !D.table_size

  if (type lt 2) then begin		; From IDX
    if (idx ge nc) then idx = nc -1
    if (type eq 0) then change_idx = 1	; Update slider to match
    ; Calculate current Y and see if slider value needs changing
    tmp = idx / 16
    if (idx ne cur_y) then begin
      cur_y = tmp
      change_y = 1
    endif
    ; Calculate current X and see if slider value needs changing
    tmp = idx - (cur_y * 16)
    if (tmp ne cur_x) then begin
      cur_x = tmp
      change_x = 1
    endif
  endif else begin			; From CUR_X and CUR_Y
    if (type eq 2) then begin
      tmp = cur_x
      while (((cur_y * 16) + tmp) ge nc) do tmp = tmp - 1
      if (tmp ne cur_x) then begin
        cur_x = tmp
        change_x = 1
      endif
    endif else begin
      tmp = cur_y
      while (((tmp * 16) + cur_x) ge nc) do tmp = tmp - 1
      if (tmp ne cur_y) then begin
        cur_y = tmp
        change_y = 1
      endif
    endelse
    IDX = (cur_y * 16B) + cur_x
    change_idx = 1
  endelse

  if (change_x) then WIDGET_CONTROL, set_value=cur_x, xpw.column
  if (change_y) then WIDGET_CONTROL, set_value=cur_y, xpw.row
  if (change_idx) then WIDGET_CONTROL, set_value=idx, xpw.by_index
  if (type lt 3) then begin
    state.x = cur_x
    state.y = cur_y
  endif
  XP_MVMARK, idx

end







function XP_NEW_COLORS, set_marker=set_marker
; Choose the best foreground and background colors for the current
; color maps and set !P appropriately. Returns 1 if the colors changed,
; 0 otherwise.
  common xp_com, xpw, state
  common colors, r_orig, g_orig, b_orig, r_curr, g_curr, b_curr

  res = 0
  luminance= (.3 * r_curr) + (.59 * g_curr) + (.11 * b_curr)
  bright_col = max(luminance, min=dark_col)
  bright_col = (byte(where(luminance eq bright_col)))(0)
  dark_col = (byte(where(luminance eq dark_col)))(0)

  if (bright_col ne !p.color) then begin
    !p.color = bright_col
    res = 1
  endif

  if (dark_col ne !p.background) then begin
    !p.background = dark_col
    res = 1
  endif

  if (res or keyword_set(set_marker)) then begin
    marker = bytarr(20, 20) + !p.background
    marker(2, 2) = bytarr(16, 16) + !p.color
    marker(4, 4) = bytarr(12, 12) + !p.background
    marker(6, 6) = bytarr(8, 8) + !p.color
    marker(8, 8) = bytarr(4, 4) + !p.background
    state.marker = marker

    marker = bytarr(20, 20) + !p.background
    strip=bytarr(20, 2) + !p.color
    marker(*, 2:3) = strip
    marker(*, 6:7) = strip
    marker(*, 10:11) = strip
    marker(*, 14:15) = strip
    marker(*, 18:19) = strip
    marker(0:1, 2:17) = bytarr(2, 16) + !p.background
    state.unreach = marker
 
    marker = bytarr(20, 20) + !p.background
    marker(1:18, 2:17) = bytarr(18, 16) + !p.color

;    marker = bytarr(20, 20) + !p.background
;    marker(2:17, 4:15) = bytarr(16, 12) + !p.color

;    strip=bytarr(20, 2) + !p.color
;    marker(*, 2:3) = strip
;    marker(*, 6:7) = strip
;    marker(*, 10:11) = strip
;    marker(*, 14:15) = strip
;    marker(*, 18:19) = strip
;    marker(0:1, 2:17) = bytarr(2, 16) + !p.background
    state.unreach = marker
 
  endif

  return, res
end







pro XP_REPLOT, color_index, type
; Re-draw the RGB plots. Type has the following possible values.
;;	- 'D': Draw the data part of all three plots
;	- 'F': draw all three plots
;	- 'R': Draw the data part of the Red plot
;	- 'G': Draw the data part of the Green plot
;	- 'B': Draw the data part of the Blue plot

  common xp_com, xpw, state
  common colors, r_orig, g_orig, b_orig, r_curr, g_curr, b_curr
  common pscale, r_x_s, r_y_s, g_x_s, g_y_s, b_x_s, b_y_s

  ; Update the plots of RGB
  wset, state.plot_win
  save_p_region = !p.region
  save_x_margin = !x.margin
  save_y_margin = !y.margin
  save_x_s = !x.s
  save_y_s = !y.s

  !y.margin= [2, 2]
  !x.margin= [6, 2]

  if (type eq 'F') then begin
    !p.region = [0,.6667, 1, 1]
    plot,xstyle=2, ystyle=3, yrange=[0, 260], r_curr, title='Red'
    r_x_s = !x.s
    r_y_s = !y.s

    !p.region = [0,.333, 1, .6667]
    plot,/noerase, xstyle=2,ystyle=3, yrange=[0, 260], g_curr, title='Green'
    g_x_s = !x.s
    g_y_s = !y.s

    !p.region = [0,0, 1, .333]
    plot,/noerase, xstyle=2,ystyle=3, yrange=[0, 260], b_curr, title='Blue'
    b_x_s = !x.s
    b_y_s = !y.s
  endif else begin
    if ((type eq 'D') or (type eq 'R')) then begin
      !p.region = [0,.6667, 1, 1]
      !x.s = r_x_s
      !y.s = r_y_s
      oplot, r_curr, color=color_index
    endif
    if ((type eq 'D') or (type eq 'G')) then begin
      !p.region = [0,.333, 1, .6667]
      !x.s = g_x_s
      !y.s = g_y_s
      oplot, g_curr, color=color_index
    endif
    if ((type eq 'D') or (type eq 'B')) then begin
      !p.region = [0,0, 1, .333]
      !x.s = b_x_s
      !y.s = b_y_s
      oplot, b_curr, color=color_index
    endif
  endelse

  !p.region = save_p_region
  !x.margin = save_x_margin
  !y.margin = save_y_margin
  !x.s = save_x_s
  !y.s = save_y_s

end




pro XP_UNREACH
; Draw the unreachable squares
  common xp_com, xpw, state

    wset, state.spectrum_win
    tmp = state.unreach
    for i = !d.n_colors, 255 do tv, tmp, i
end





pro XP_CHANGE_COLOR, type, value
; Change current color. Type has the following possible values.
;	- 'R': Change the R part of the current color
;	- 'G': ...
;	- 'B': ...
  common xp_com, xpw, state
  common colors, r_orig, g_orig, b_orig, r_curr, g_curr, b_curr


  cur_idx = state.cur_idx

  XP_REPLOT, !p.background, type

  if (type eq 'R') then r_curr(cur_idx) = value;
  if (type eq 'G') then g_curr(cur_idx) = value;
  if (type eq 'B') then b_curr(cur_idx) = value;

  tvlct, r_curr(cur_idx), g_curr(cur_idx), b_curr(cur_idx), cur_idx

  if (XP_NEW_COLORS()) then begin
    ; Highlight the current position using the marker
    XP_MVMARK, cur_idx
    XP_REPLOT, !p.color, 'F'
    XP_UNREACH
  endif else begin
    XP_REPLOT, !p.color, type
  endelse

end



pro XP_RESPEC, always
; Re-draw the spectrum window.

  common xp_com, xpw, state

  if (XP_NEW_COLORS(set_marker=always) or always) then begin
    ; Draw spectrum widgets
    wset, state.spectrum_win
    tmp=bytarr(20,20)
    for i = 0, !d.n_colors-1 do begin
      tv, tmp, i
      tmp = tmp + 1B
    endfor
    XP_UNREACH

    ; Highlight the current position using the marker
    XP_MVMARK, state.cur_idx

    ; Update the plots of RGB
    XP_REPLOT, !p.color, 'F'
  endif

end




pro XP_MVMARK, to_index
; Move the marker and update the current color square.
  common xp_com, xpw, state
  common colors, r_orig, g_orig, b_orig, r_curr, g_curr, b_curr

  cur_idx = state.cur_idx

  new_pos = to_index ne cur_idx

  ; Restore current square
  wset, state.spectrum_win
  if (new_pos) then begin
    tv, bytarr(20, 20) + byte(cur_idx), cur_idx
    state.cur_idx = (cur_idx = to_index)
    WIDGET_CONTROL, xpw.idx_label, $
	set_value=strcompress(cur_idx, /REMOVE_ALL)
  endif
  tv, state.marker, cur_idx

  ; Mark new square
  wset, state.cur_color_win
  erase, color=cur_idx

  ; Update the RBG sliders
  if (new_pos) then begin
    WIDGET_CONTROL, xpw.r, set_value=r_curr(cur_idx)
    WIDGET_CONTROL, xpw.g, set_value=g_curr(cur_idx)
    WIDGET_CONTROL, xpw.b, set_value=b_curr(cur_idx)
  endif

end







pro XP_EVENT, event

  common xp_com, xpw, state
  common colors, r_orig, g_orig, b_orig, r_curr, g_curr, b_curr

  case (event.id) of

  xpw.done: begin
    if state.old_window ge 0 then wset, state.old_window
      empty
      device,set_write=state.old_mask		;Restore old write mask
      r_orig = r_curr & g_orig = g_curr & b_orig = b_curr ;new orig color tbl
      WIDGET_CONTROL, /DESTROY, event.top
      !p = state.old_p
    end

  xpw.predefined: xloadct, /silent, group=xpw.base

  xpw.help: IF(DEMO_MODE()) THEN $
		MESSAGE, "Help is not available in the Demo Mode", $
 			/CONTINUE $	
	    ELSE $
		XDL, 'XPALETTE', group=xpw.base

  xpw.redraw: XP_RESPEC, 1

  xpw.r: XP_CHANGE_COLOR, "R", event.value

  xpw.g: XP_CHANGE_COLOR, "G", event.value

  xpw.b: XP_CHANGE_COLOR, "B", event.value

  xpw.by_index: begin
    set_slide_idx:
      XP_SET_SLIDERS, 1, state.x, state.y, event.value
    end

  xpw.column: begin
    state.x = byte(event.value)
    XP_SET_SLIDERS, 2, state.x, state.y, tmp
    end

  xpw.row : begin
    state.y = byte(event.value)
    XP_SET_SLIDERS, 3, state.x, state.y, tmp
    end

  xpw.switch_mark : if (state.mark_idx ne state.cur_idx) then begin
    tmp = state.mark_idx
    state.mark_idx = state.cur_idx
    XP_SET_SLIDERS, 0, state.x, state.y, tmp
    WIDGET_CONTROL, xpw.mark_label, $
		    set_value=strcompress(state.mark_idx, /REMOVE)
    endif

  xpw.set_mark: begin
    change_mark:
      state.mark_idx = state.cur_idx
      WIDGET_CONTROL, xpw.mark_label, $
		  set_value=strcompress(state.mark_idx, /REMOVE)
    endif

  xpw.interp: goto, do_copy

  xpw.copy : begin
    do_copy:
      cur_idx = state.cur_idx
      if (state.mark_idx le cur_idx) then begin
	s = state.mark_idx
	e = cur_idx
      endif else begin
	s = cur_idx
	e = state.mark_idx
      endelse
      n = e-s+1
      XP_REPLOT, !p.background, 'D'
      if (event.id eq xpw.copy) then begin
        r_curr(s:e) = r_curr(cur_idx)
	g_curr(s:e) = g_curr(cur_idx)
	b_curr(s:e) = b_curr(cur_idx)
      endif else begin			; Interpolate
	scale = findgen(n)/float(n-1)
	r_curr(s:e) = r_curr(s) + (fix(r_curr(e)) - fix(r_curr(s))) * scale
	g_curr(s:e) = g_curr(s) + (fix(g_curr(e)) - fix(g_curr(s))) * scale
	b_curr(s:e) = b_curr(s) + (fix(b_curr(e)) - fix(b_curr(s))) * scale
      endelse
      tvlct, r_curr(s:e), g_curr(s:e), b_curr(s:e), s
      if (XP_NEW_COLORS()) then begin
        ; Highlight the current position using the marker
	XP_MVMARK, cur_idx
	XP_REPLOT, !p.color, 'F'
        XP_UNREACH
      endif else begin
        XP_REPLOT, !p.color, 'D'
      endelse
      end

    xpw.spectrum: begin
      if (event.press ne 0) then begin
	tmp = ((319 - event.y) / 20 ) * 16 + (event.x / 20)
	XP_SET_SLIDERS, 0, state.x, state.y, tmp
      endif
      end
    else:
    endcase

end







pro XPALETTE, group=group

  common xp_com, xpw, state
  common colors, r_orig, g_orig, b_orig, r_curr, g_curr, b_curr

  xpw = { xp_widgets, base:0L, done:0L, predefined:0L, help:0L, redraw:0L, $
	set_mark:0L, switch_mark:0L, copy:0L, interp:0L, $
	r:0L, g:0L, b:0L, by_index:0L, row:0L, column:0L, spectrum:0L, $
	mark_label:0L, idx_label:0L, button_base:0L, rgb_base:0L}

  state = {xp_state, old_window:0L, $	; Original window index
	   old_mask:0L, $		; Original mask
	   old_p:!p, $			; Original value of !P
	   x:0l, y:0l, $		; Current X and Y of marker in spectrum
	   mark_idx:0, $		; Current mark index
	   cur_idx:0, $			; Current index
	   marker:bytarr(20,20,/nozero), $	; Byte array used as marker
	   unreach:bytarr(20,20,/nozero), $	; Image for unreachable colors
	   spectrum_win:0, $		; Spectrum draw window index
	   cur_color_win:0, $		; Current Color draw window index
	   plot_win:0 }			; RGB plot draw window index

  if (XREGISTERED('XPALETTE')) then return	; Only one copy at a time

  on_error,2              ;Return to caller if an error occurs

  device,get_write=tmp,set_write=255 ;Enable all bits
  state.old_mask=tmp

  nc = !d.table_size		;# of colors avail
  if nc eq 0 then message, "Device has static color tables.  Can't modify."
  if (nc eq 2) then message, 'Unable to work with monochrome system.'

  state.old_p = !p		;Save !p
  !p.noclip = 1			;No clipping
  !p.color = nc -1		;Foreground color
  !p.font = 0			;Hdw font
  state.old_window = !d.window	;Previous window

  if (n_elements(r_curr) eq 0) then begin
    r_orig = bytscl(indgen(nc)) & g_orig = r_orig & b_orig = r_orig
    r_curr = r_orig & g_curr = r_orig & b_curr = r_orig
  endif

  ; Create widgets
  xpw.base=WIDGET_BASE(title='XPalette', /ROW, space=30)

  plot_frame = WIDGET_DRAW(xpw.base, xsize=200, ysize=450)

  c1 = WIDGET_BASE(xpw.base, /COLUMN, space=20)
    status = WIDGET_BASE(c1, /COLUMN, /FRAME)
      ncw = WIDGET_LABEL(WIDGET_BASE(status))
      junk = WIDGET_BASE(status, /ROW)
        j2 =  WIDGET_LABEL(junk, value = 'Current Index: ')
        xpw.idx_label = WIDGET_TEXT(junk, value = '0', ysize=1, xsize=20)
      junk = WIDGET_BASE(status, /row)
        j2 =  WIDGET_LABEL(junk, value = 'Mark Index:    ')
        xpw.mark_label = WIDGET_TEXT(junk, value = '0', ysize=1, xsize=20)
      c1_1 = widget_base(status, /ROW)
        junk = WIDGET_LABEL(c1_1, value="Current Color: ")
          cur_color = WIDGET_DRAW(c1_1, xsize = 125, ysize=50, /frame)
    xpw.button_base = WIDGET_BASE(c1, COLUMN=3, /FRAME)
      xpw.done = WIDGET_BUTTON(xpw.button_base, value='Done')
      xpw.predefined = WIDGET_BUTTON(xpw.button_base, value='Predefined')
      xpw.help = WIDGET_BUTTON(xpw.button_base, value='Help')
      xpw.redraw = WIDGET_BUTTON(xpw.button_base, value='Redraw')
      xpw.set_mark = WIDGET_BUTTON(xpw.button_base, value='Set Mark')
      xpw.switch_mark = WIDGET_BUTTON(xpw.button_base, value='Switch Mark')
      xpw.copy = WIDGET_BUTTON(xpw.button_base, value='Copy Current')
      xpw.interp = WIDGET_BUTTON(xpw.button_base, value='Interpolate')
    xpw.rgb_base = WIDGET_BASE(c1, /COLUMN, /FRAME)
      xpw.r = WIDGET_SLIDER(xpw.rgb_base, max=255, title='R', /drag, xsize=256)
      xpw.g = WIDGET_SLIDER(xpw.rgb_base, max=255, title='G', /drag, xsize=256)
      xpw.b = WIDGET_SLIDER(xpw.rgb_base, max=255, title='B', /drag, xsize=256)



  newer_motif = (!version.arch eq 'hp_pa') or (!version.os eq 'IRIX') or $
	(!version.os eq 'RISC/os')

  s_base = WIDGET_BASE(xpw.base)
    if (newer_motif) then begin
        xpw.row = WIDGET_SLIDER(s_base, min=15, max=0, xoff = 325, yoff= 75, $
	    /vert, val=0, title='Row', ysize=320)
    endif else begin
        xpw.row = WIDGET_SLIDER(s_base, min=15, max=0, xoff = 325, yoff= 75, $
	    /vert, val=0, title='Row', ysize=320, xsize=75)
    endelse
    xpw.column = WIDGET_SLIDER(s_base, max=15, title='Column', $
	xsize=320, yoff=400)
    xpw.spectrum=WIDGET_DRAW(s_base, xsize=320, ysize=320, /frame, /button, $
	yoff=75)
     xpw.by_index = WIDGET_SLIDER(s_base, max=nc-1, $
	title = 'By Index', xsize=320)


  state.cur_idx = 0
  state.mark_idx = 0
  state.x = 0
  state.y = 0

  WIDGET_CONTROL, /REAL, xpw.base
  WIDGET_CONTROL, ncw, $
	set_value='Number Of Colors: ' + strcompress(!d.n_colors, /REMOVE_ALL)
  WIDGET_CONTROL, get_value=tmp, cur_color
  state.cur_color_win = tmp
  WIDGET_CONTROL, get_value=tmp, xpw.spectrum
  state.spectrum_win = tmp
  WIDGET_CONTROL, get_value=tmp, plot_frame
  state.plot_win = tmp

  ; Paint the contents of the spectrum widget including the marker
  XP_RESPEC, 1

  XMANAGER, 'XPALETTE', xpw.base, event_handler='XP_EVENT', group=group
end


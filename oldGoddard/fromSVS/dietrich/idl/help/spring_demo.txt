
The Spring_Demo example is a dynamic simulation of a
rectangular grid of weights connected by springs.  The
results may be visualized by a number of methods,
illustrating some of the many ways of displaying data
with IDL.

Pressing the GO! button starts the simulation from an
initial starting grid.  The initial Z position of the
weights may be edited with an editing tool.  The X and
Y position of the weights is fixed.  The strength of
the springs may be varied, as well as a number of
viewing and simulation parameters.

One or two rendering methods may be displayed at the
same time allowing comparison.

Hints:  Use the "Adjust Color Palette" button of the
animation window to optimize the color tables for the
rendering method you select.

WARNING: this demo can easily exhaust the memory / swap
space resources of small or improperly configured
machines.


**** CONTROLS ****

DONE button: exits the simulation.

GO! button: starts the simulation from the starting
grid.  Each frame is rendered and the XINTERANIMATE
procedure is called to animate the results.

STOP button: stops the animation and removes the
animated display.  The same effect is obtained by
pressing the "End Animation" button in the Animation
window. 

EDIT button: edits the starting grid.  A window is
displayed with a grid on the left and a mesh surface
display of the current starting grid on the right.  To
edit: set the top slider to the new Z value (range:
-100 to 100), and double click on the cell's position
in the left hand grid.  The RESET button sets all cells
to zero.  The DONE button exits the editor.

HELP button:  Displays this text.


Rendering Methods box:  One or two methods may be
selected.  Of course, the amount of display memory
required is doubled when viewing two methods.  To
select a method, click on it's button.  Click the
button again to deselect the method.  Initially,
nothing is selected for the right display.

Spring Strength slider: Sets the spring coefficient.  A
higher setting makes the springs stronger in relation
to the weights.  Higher settings result in more
movement; lower settings result in smoother
simulations. 

X axis rotation slider: Controls the rotation about the
X axis for the 3D displays.

Z axis rotation slider: Controls the rotation about the
Z axis for 3D displays.

View area size: The size of the viewing area, in
pixels.  The amount of display memory is proportional
to the square of this number.

Number of frames: The number of animation frames in the
animation.  The amount of display memory is
proportional to this number.  WARNING: this demo can
easily exhaust the memory / swap space resources of
small machines.

Grid size: the size of the simulation grid.  Does not
affect the amount of display memory used.


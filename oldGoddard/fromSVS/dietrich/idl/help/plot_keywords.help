      86
AX             0
AZ             1105
BACKGROUND     1347
BOTTOM         1709
CHANNEL        1916
CHARSIZE       2165
CHARTHICK      2535
CLIP           3028
COLOR          3691
C_ANNOTATION   4014
C_CHARSIZE     4790
C_COLORS       5248
C_LABELS       5917
C_LINESTYLE    6900
C_THICK        7964
DATA           8420
DEVICE         8629
FOLLOW         8771
FONT           9988
HORIZONTAL     10699
LEVELS         10995
LINESTYLE      11484
LOWER_ONLY     11852
MAX_VALUE      12305
NLEVELS        12694
NOCLIP         13073
NODATA         13553
NOERASE        13853
NORMAL         14079
NSUM           14311
OVERPLOT       14907
PATH_FILENAME  15283
POLAR          16251
POSITION       16719
PSYM           18473
SAVE           19844
SKIRT          20999
SPLINE         22065
SUBTITLE       23065
SYMSIZE        23226
T3D            23451
THICK          24243
TICKLEN        24781
TITLE          26393
UPPER_ONLY     26893
XAXIS          921
XCHARSIZE      2766
XMARGIN        11998
XMINOR         12600
XRANGE         19594
XSTYLE         21519
XTICKLEN       24431
XTICKNAME      25315
XTICKS         25460
XTICKV         25768
XTICK_GET      25995
XTITLE         26683
XTYPE          26785
YAXIS          921
YCHARSIZE      2766
YMARGIN        11998
YMINOR         12600
YNOZERO        27077
YRANGE         19594
YSTYLE         21519
YTICKLEN       24431
YTICKNAME      25315
YTICKS         25460
YTICKV         25768
YTICK_GET      25995
YTITLE         26683
YTYPE          26785
ZAXIS          27535
ZCHARSIZE      2766
ZMARGIN        11998
ZMINOR         12600
ZRANGE         19594
ZSTYLE         21519
ZTICKLEN       24431
ZTICKNAME      25315
ZTICKS         25460
ZTICKV         25768
ZTICK_GET      25995
ZTITLE         26683
ZTYPE          26785
ZVALUE         28393
;+
    PLOTTING KEYWORD:
      AX [SURFACE]:
          Specifies the angle of  rotation,  about  the  X  axis,  in
         degrees  towards the viewer.  This keyword is effective only
         if !P.T3D  is  not  set.   If  !P.T3D  is  set,  the  three-
         dimensional  to  two-dimensional transformation used by SUR-
         FACE is contained in the 4 by 4 array !P.T.

         The surface represented  by  the  two-dimensional  array  is
         first rotated, AZ (see below) degrees about the Z axis, then
         by AX degrees about the X axis, tilting the surface  towards
         the viewer (AX  > 0), or away from the viewer.

         The AX and AZ keyword parameters default to +30  degrees  if
         omitted and !P.T3D is 0.

         The  three-dimensional  to  two-dimensional   transformation
         represented  by AX and AZ, can be saved in !P.T by including
         the SAVE keyword.
;-
;+

    PLOTTING KEYWORD:
      [XY]AXIS [AXIS]:
         The XAXIS and YAXIS keywords indicate which type of axis  is
         to be drawn by the AXIS procedure and its placement.
;-
;+

    PLOTTING KEYWORD:
      AZ [SURFACE]:
         Specifies the counterclockwise angle of rotation about the Z
         axis.   This keyword is effective only if !P.T3D is not set.
         The order of rotation is AZ first, then AX.
;-
;+

    PLOTTING KEYWORD:
      BACKGROUND [All]:
          The background color index to which the screen is set  when
         erasing.   Not all devices support erasing the background to
         a color index.

         To produce a black plot with a white background on  a  color
         display:

                        PLOT, Y, BACK = 255, COLOR = 0
;-
;+

    PLOTTING KEYWORD:
      BOTTOM [SURFACE]:
          The color index used to draw the  bottom  surface.  If  not
         specified,  the  bottom  is drawn with the same color as the
         top.
;-
;+

    PLOTTING KEYWORD:
      CHANNEL [All]:
         The channel index or mask for the operation.  This parameter
         is  ignored  when used with devices without multiple display
         channels.  !P.CHANNEL contains the default value.
;-
;+

    PLOTTING KEYWORD:
      CHARSIZE [All]:
          The overall character size for the annotation.  A  CHARSIZE
         of  1.0  is  normal.  The size of the annotation on the axes
         may be set, relative to CHARSIZE, with xCHARSIZE, where x is
         X, Y, or Z.  The main title is written with a character size
         of 1.25 times this parameter.
;-
;+

    PLOTTING KEYWORD:
      CHARTHICK [All]:
         The line thickness of the vector drawn font  characters.This
         keyword  has  no  effect  when  used with the hardware drawn
         fonts.  Default value is 1.0.
;-
;+

    PLOTTING KEYWORD:
      [XYZ]CHARSIZE [All]:
         The size of the characters used to annotate the axis and its
         title.  This  field  is a scale factor applied to the global
         scale factor set by !P.CHARSIZE or the keyword CHARSIZE.
;-
;+

    PLOTTING KEYWORD:
      CLIP [All]:
          The coordinates of a rectangle used to  clip  the  graphics
         output.   The rectangle is specified as a vector of the form
         [ X_0, Y_0, X_1, Y_1 ] , giving  coordinates  of  the  lower
         left and upper right corners, respectively.  Coordinates are
         specified in data  coordinate  units  unless  an  overriding
         coordinate  unit specification keyword is present, i.e. NOR-
         MAL or DEVICE.

         Note: even though no clipping rectangle is explicitly speci-
         fied, IDL will clip to the plotting window unless the NOCLIP
         keyword is specified.
;-
;+
    PLOTTING KEYWORD:
      COLOR [All]:
          The color index or indices used to draw the data, axes, and
         annotation.

         When used with the  PLOTS,  POLYFILL,  or  the  XYOUTS  pro-
         cedures,  this  keyword  parameter may be set to a vector to
         specify more than one color index.
;-
;+

    PLOTTING KEYWORD:
      C_ANNOTATION [CONTOUR]:
          The label that will be drawn on each contour.
          Usually, contours are labeled with their value. This param-
         eter,  a vector of strings, allows any text to be specified.
         The first label is used for the first contour drawn, and  so
         forth.  If  the LEVELS keyword is specified, the elements of
         C_ANNOTATION correspond directly to  the  levels  specified,
         otherwise,  they  correspond to the default levels chosen by
         the CONTOUR procedure.  If there  are  more  contour  levels
         than  elements  in  C_ANNOTATION,  the  remaining levels are
         labeled with their values.

         Use of this keyword implies use of the FOLLOW keyword.
;-
;+

    PLOTTING KEYWORD:
      C_CHARSIZE [CONTOUR]:
         The size of the characters used to annotate contour  labels.
         Normally,  contour  labels are drawn at 3/4 of the size used
         for the axis labels (specified by the  CHARSIZE  keyword  or
         !P.CHARSIZE  system  variable.  This keyword allows the con-
         tour label size to be specified independently.

         Use of this keyword implies use of the FOLLOW keyword.
;-
;+

    PLOTTING KEYWORD:
      C_COLORS [CONTOUR]:
          The color index used to draw each contour.  This  parameter
         is  a  vector,  converted  to integer type if necessary.  If
         there are more contour levels than elements in C_COLORS, the
         elements of the color vector are cyclically repeated.
         If C_COLORS contains three elements,  and  there  are  seven
         contour levels to be drawn, the colors
          c_0, c_1, c_2, c_0, c_1, c_2, c_0  will  be  used  for  the
         seven  levels.   To  call  CONTOUR  and  set  the  colors to
         [100,150,200]:

                     CONTOUR, Z, C_COLORS = [100,150,200]
;-
;+

    PLOTTING KEYWORD:
      C_LABELS [CONTOUR]:
          Specifies which contour levels should be labeled.
          By default, every other contour level is labeled.  C_LABELS
         allows  you  to override this default and explicitly specify
         the levels to label. This parameter is a  vector,  converted
         to  integer  type  if  necessary.  If  the LEVELS keyword is
         specified, the elements of C_LABELS correspond  directly  to
         the  levels  specified,  otherwise,  they  correspond to the
         default levels chosen by the CONTOUR procedure.  Setting  an
         element  of  the vector to zero causes that contour label to
         not be labeled.  A non-zero value forces labeling.

         To produce a contour plot with four levels where all but the
         third level is labeled:

         CONTOUR, Z, LEVELS = [0.0, 0.25, 0.75, 1.0], C_LABELS = [1, 1, 0, 1]

         Use of this keyword implies use of the FOLLOW keyword.
;-
;+

    PLOTTING KEYWORD:
      C_LINESTYLE [CONTOUR]:
          The  line  style  used  to  draw  each  contour.   As  with
         C_COLORS, C_LINESTYLE is a vector of line style indices.  If
         there are more contour levels than  line  styles,  the  line
         styles are cyclically repeated.  See  Table -style-table for
         the available styles.

          Note:  The cell drawing contouring algorithm draws all  the
         contours in each cell, rather than following contours. Since
         an entire contour is not drawn as a  single  operation,  the
         appearance  of  the more complicated linestyles will suffer.
         Use of the contour following method (selected with the  FOL-
         LOW keyword) will give better looking results in such cases.

         To produce a contour plot, with the contour levels  directly
         specified  in  a  vector V, with all negative contours drawn
         with dotted lines, and with positive levels in solid lines:

                CONTOUR, Z, LEVELS = V, C_LINESTYLE = V LT 0.0
;-
;+

    PLOTTING KEYWORD:
      C_THICK [CONTOUR]:
         The line used to draw each contour level.  As with C_COLORS,
         C_THICK  is  a vector of line thickness values, although the
         values are floating point.  If there are more contours  than
         thickness  elements, elements are repeated.  If omitted, the
         overall line thickness specified by the THICK keyword param-
         eter or !P.THICK is used for all contours.
;-
;+

    PLOTTING KEYWORD:
      DATA [All]:
          A keyword flag, which if present, indicates that the  clip-
         ping coordinates are specified in data coordinates.  This is
         the default.
;-
;+

    PLOTTING KEYWORD:
      DEVICE [All]:
          Specifies that the POSITION and  CLIP  coordinates  are  in
         device units.
;-
;+

    PLOTTING KEYWORD:
      FOLLOW [CONTOUR]:
         If present and non-zero, forces the CONTOUR procedure to use
         the  line  following  method  instead  of  the  cell drawing
         method.
          CONTOUR can  draw  contours  using  one  of  two  different
         methods:

         The first algorithm, used by default,  examines  each  array
         cell  and draws all contours emanating from that cell before
         proceeding to the next cell. This  method  is  efficient  in
         terms  of  computer  resources  but  does  not allow contour
         labeling.

         The second method searches for each contour  line  and  then
         follows the line until it reaches a boundary or closes. This
         method gives better looking results with dashed line styles,
         and  allows  contour  labeling,  but  requires more computer
         time. It is used if any of the following keywords is  speci-
         fied:   C_ANNOTATION,   C_CHARSIZE,   C_LABELS,  FOLLOW,  or
         PATH_FILENAME.

         Although these two methods both draw correct  contour  maps,
         differences  in their algorithms can cause small differences
         in the resulting graph,
;-
;+

    PLOTTING KEYWORD:
      FONT [All]:
          An integer that specifies the  graphics  text  font  index.
         Font number -1 selects the Hershey character fonts which are
         drawn using vectors.  Font 0 uses the hardware font  of  the
         output device.

         The Hershey fonts are selected by command strings  beginning
         with  the exclamation mark character within the graphic text
         string.  See Chapter 12 for a complete  description  of  the
         vector drawn fonts.

         When using three-dimensional transformations, always use the
         vector  drawn  fonts, as the hardware fonts are not properly
         projected from three to two dimensions.
;-
;+

    PLOTTING KEYWORD:
      HORIZONTAL [SURFACE]:
          A keyword flag which if set causes  SURFACE  to  only  draw
         lines  across  the  plot perpendicular to the line of sight.
         The default is for SURFACE to draw both across the plot  and
         from front to back.
;-
;+

    PLOTTING KEYWORD:
      LEVELS [CONTOUR]:
          Specifies a vector containing the  contour levels drawn  by
         the  CONTOUR  procedure. A contour is drawn at each level in
         LEVELS.

         To draw a contour plot with levels  at  1,  100,  1000,  and
         10000:
                  CONTOUR, Z, LEVELS = [1, 100, 1000, 10000]

         To draw a contour plot with levels at 50, 60, ..., 90, 100:

                  CONTOUR, Z, LEVELS = FINDGEN(6) * 10 + 50
;-
;+

    PLOTTING KEYWORD:
      LINESTYLE [OPLOT, PLOT, SURFACE]:
          Determines the line style of the lines used to connect  the
         data points in PLOT and OPLOT.

         The line style index is an integer with one of the following
         values: (0) Solid, (1) Dotted, (2) Dashed, (3) Dash Dot, (4)
         Dash Dot Dot Dot, and (5) Long Dashes.
;-
;+

    PLOTTING KEYWORD:
      LOWER_ONLY [SURFACE]:
          Indicates that only the lower surface of the object  is  to
         be drawn.
;-
;+

    PLOTTING KEYWORD:
      [XYZ]MARGIN [All]:
          A 2-element array specifying the margin on the  left  (bot-
         tom)  and  right (top) sides of the plot window, in units of
         character size.  Default margins are 10  and  3  for  the  X
         axis, and 4 and 2 for the Y axis.
;-
;+

    PLOTTING KEYWORD:
      MAX_VALUE [CONTOUR]:
          Data points with values equal to or above  this  value  are
         ignored  when  contouring.   Cells  containing   one or more
         corners with values above MAX_VALUE will  have  no  contours
         drawn through them.
;-
;+

    PLOTTING KEYWORD:
      [XYZ]MINOR [All]:
         The number of minor tick marks.
;-
;+

    PLOTTING KEYWORD:
      NLEVELS [CONTOUR]:
          The number of equally spaced contour levels that  are  pro-
         duced by CONTOUR.  If the LEVELS parameter, which explicitly
         specifies the value of the contour levels, is  present  this
         keyword  has  no  effect.   If neither parameter is present,
         approximately six levels are drawn.
;-
;+

    PLOTTING KEYWORD:
      NOCLIP [CONTOUR, OPLOT, PLOT]:
          A keyword flag, which if present,  suppresses  clipping  of
         the  plot.  By default, the plot is clipped within the plot-
         ting window.  To  disable  clipping  include  the  parameter
         NOCLIP = 1 or /NOCLIP in the call.

         Note: the default value is clipping disabled for  PLOTS  and
         XYOUTS.   For  all  other routines, the default is to enable
         clipping.
;-
;+

    PLOTTING KEYWORD:
      NODATA [All]:
          If this keyword is set, only the axes, titles, and  annota-
         tion are drawn.  No data points are plotted.

         To draw an empty set of axes between some given values:

                   PLOT, [XMIN, XMAX],[YMIN, YMAX], /NODATA
;-
;+

    PLOTTING KEYWORD:
      NOERASE [All]:
          Specifies that the screen or page is not to be erased.   By
         default,  the  screen  is  erased,  or  a new page is begun,
         before a plot is produced.
;-
;+

    PLOTTING KEYWORD:
      NORMAL [All]:
          A keyword flag, which if present, indicates that  the  CLIP
         and/or POSITION coordinates are in the normalized coordinate
         system and range from 0.0 to 1.0.
;-
;+
    PLOTTING KEYWORD:
      NSUM [PLOT, OPLOT]:
          Indicates the number of data points to average  when  plot-
         ting.   If NSUM is larger than 1, every group of NSUM points
         is averaged to produce one plotted point.  If  there  are  m
         data  points, then m /  NSUM points are displayed.  On loga-
         rithmic axes a geometric average is performed.

         It is convenient to use NSUM  when  there  is  an  extremely
         large  number  of data points to plot because it plots fewer
         points, the graph is less cluttered, and it is quicker.
;-
;+

    PLOTTING KEYWORD:
      OVERPLOT [CONTOUR]:
         Indicates the the CONTOUR procedure is to overplot.  No axes
         are  drawn and the previously established scaling remains in
         effect.  You must explicitly specify the values of the  con-
         tour  levels when using this option, unless geographics map-
         ping coordinates are in effect.
;-
;+

    PLOTTING KEYWORD:
      PATH_FILENAME [CONTOUR]:
         Specifies the name of a file to contain  the  contour  posi-
         tions.  If  PATH_FILENAME  is present, CONTOUR does not draw
         the contours, but  rather,  opens  the  specified  file  and
         writes  the  positions,  in normalized coordinates, into it.
         The file consists of a series of logical records  containing
         binary data.

         The format is described in the User's Guide.

         The POLYCONTOUR User Library Procedure  can  be  used  along
         with  this  file  to fill the closed contours with specified
         colors.  A typical application is to use  CONTOUR  with  the
         PATH_FILENAME  keyword  to  get  the  path  information, use
         POLYCONTOUR to fill the closed contours, and then  use  CON-
         TOUR with the NOERASE keyword to overplot the contours.

         Use of this keyword implies use of the FOLLOW keyword.
;-
;+

    PLOTTING KEYWORD:
      POLAR [PLOT, OPLOT]:
          Polar plots are produced when this keyword is  present  and
         non-zero.  The X and Y vector parameters, both of which must
         be present, are first  converted  from  polar  to  cartesian
         coordinates.   The  first  parameter  is the radius, and the
         second is theta, expressed in radians.

         To make a polar plot:

                            PLOT, /POLAR, R, THETA
;-
;+

    PLOTTING KEYWORD:
      POSITION [All]:
          Allows direct specification of the plot window. POSITION is
         a  4-element  vector  giving,  in  order, the coordinates  [
         (X_0, Y_0), (X_1, Y_1) ] , of the lower left and upper right
         corners  of  the  data window.  Coordinates are expressed in
         normalized units ranging from 0.0 to 1.0, unless the keyword
         /DEVICE  is present, in which case they are in actual device
         units.

         When setting the position of the window, be  sure  to  allow
         space  for the annotation, which resides outside the window.
         IDL outputs the message " Warning: Plot truncated."  if  the
         plot  region  is  larger  than the screen or page size.  The
         plot region is the rectangle enclosing the plot  window  and
         the annotation.

         When plotting in three dimensions, the POSITION keyword is a
         6-element vector with the first four elements describing, as
         above, the XY position, and with the last two elements  giv-
         ing the minimum and maximum Z coordinates.  The Z specifica-
         tion is always in normalized coordinate units.

         When making more than one plot per  page  it  is  more  con-
         venient  to  set !P.MULTI than to manipulate the position of
         the plot directly with the POSITION keyword.

         The following statement produces a contour  plot  with  data
         plotted in only the upper left quarter of the screen:

                      CONTOUR, Z, POS=[0., 0.5, .5, 1.0]

         Because no space on the left or top edges  was  allowed  for
         the  axes  or  their annotation, the above described warning
         message results.
;-
;+
    PLOTTING KEYWORD:
      PSYM [All]:
          The symbol used to mark each data point.  Normally, PSYM is
         0,  data  points  are connected by lines, and no symbols are
         drawn to mark the points.   Specify  this  keyword,  or  set
         !P.PSYM,  to  the  symbol  index as given below to mark data
         points with symbols.  The keyword SYMSIZE is used to set the
         size of the symbols.

         Negative values of  PSYM  cause  the  symbol  designated  by
         |PSYM| to be plotted at each point with solid lines connect-
         ing the symbols.  For example, a value of -5 plots triangles
         at each data point and connects the points with lines.

         The allowed symbol indexes are: (1) Plus sign, (2) Asterisk,
         (3)  Period,  (4)  Diamond, (5) Triangle, (6) Square, (7) X,
         (8) User-defined, see USERSYM procedure, (9) Undefined, (10)
         Data  points  are plotted in the histogram mode.  Horizontal
         and vertical lines connect the plotted points, as opposed to
         the normal method of connecting points with straight lines.
;-
;+

    PLOTTING KEYWORD:
      [XYZ]RANGE [All]:
         The desired data range of the axis, a 2-element vector.  The
         first  element  is  the  axis minimum, and the second is the
         maximum.  IDL will frequently round this range.
;-
;+

    PLOTTING KEYWORD:
      SAVE [AXIS, SURFACE]:
         Saves the 3d to 2d transformation matrix established by SUR-
         FACE in the system variable field !P.T.
           Use this keyword when combining the output of SURFACE with
         the output of other routines in the same plot.

         When used with AXIS, the SAVE keyword  parameter  saves  the
         scaling  parameters  established  by  the  call  back in the
         appropriate axis system  variable,  !X,  !Y,  or  !Z.   This
         causes subsequent overplots to be scaled to the new axis.

         To display a two-dimensional array  using  SURFACE,  and  to
         then  superimpose  contours over the surface:  (This example
         assumes that !P.T3D is zero, its default value.)
              SURFACE, Z, /SAVE ; Surface, save transform.
              CONTOUR, Z, /NOERASE, /T3D ; Contours. Use saved transform.

         To  display  a  surface  and  to then display a flat contour
         plot, registered above the surface:

              SURFACE, Z, /SAVE ; Surface, save transform
              CONTOUR, Z, /NOERASE, /T3D, ZVALUE=1.0 ; Contour plot
;-
;+

    PLOTTING KEYWORD:
      SKIRT [SURFACE]:
          A skirt around the array at a given Z  value  is  drawn  if
         this keyword parameter is present.  The Z value is expressed
         in data units.

         If the skirt is drawn, each point on the four edges  of  the
         surface  is  connected to a point on the skirt which has the
         given Z value, and the same X  and  Y  values  as  the  edge
         point.  In addition, each point on the skirt is connected to
         its neighbor.
;-
;+

    PLOTTING KEYWORD:
      [XYZ]STYLE [All]:
         Allows specification of axis options  such  as  rounding  of
         tick  values  and  selection  of a box axis.  Each option is
         encoded in a bit.

          Setting bit 0 (value 1) forces exact  axis  range.   Bit  1
         (value   2)  extends  the  axis  range.   Bit  2  (value  4)
         suppresses the entire axis.  Bit 3 (value 8) suppresses  box
         style  axis.   Bit 4 (value 16) inhibits setting the  Y axis
         minimum value to 0.  (Y axis only.)
;-
;+

    PLOTTING KEYWORD:
      SPLINE [CONTOUR]:
         Specifies that contour paths are to  be  interpolated  using
         cubic  splines.   Use of this keyword implies the use of the
         FOLLOW keyword.  The appearance of contour plots  of  arrays
         with low resolution may be improved by using spline interpo-
         lation.   In  rare  cases,  contour  lines  that  are  close
         together  may cross because of interpolation.  Use of spline
         interpolation is not recommended when the  array  dimensions
         are more than approximately 15.

         You may specify the length of each  interpolated  line  seg-
         ment,  in  normalized  coordinates by including a value with
         this keyword.  The default value is 0.005 which is  obtained
         when  the parameter: /SPLINE is present.  Smaller values for
         this parameter yield smoother lines, up to the resolution of
         the output device, at the expense of more computations.
;-
;+

    PLOTTING KEYWORD:
      SUBTITLE [All]:
          Produces a subtitle underneath the X  axis  containing  the
         text in this string parameter.
;-
;+

    PLOTTING KEYWORD:
      SYMSIZE [All]:
          Specifies the size of the symbols drawn when PSYM  is  set.
         The  default  size of 1.0 produces symbols approximately the
         same size as a character.
;-
;+

    PLOTTING KEYWORD:
      T3D [All]:
          A keyword flag which, if present, indicates that  the  gen-
         eralized  transformation  matrix  in !P.T is to be used.  If
         not present, the user-supplied coordinates are simply scaled
         to  screen coordinates.  See the examples in the description
         of SAVE.

         Note: Since T3D uses the transformation matrix in  !P.T,  it
         is  important  that  !P.T  contain  a  valid  transformation
         matrix.  This can be achieved in several ways:

          Use the SAVE keyword to save the transformation matrix from
         an earlier graphics operation.
          Establish  a  transformation  matrix  using  the  T3D  user
         library procedure.
          Set the value of !P.T directly.
;-
;+

    PLOTTING KEYWORD:
      THICK [PLOT, OPLOT]:
          Controls the thickness of the lines connecting the  points.
         A thickness of 1.0 is normal, 2 is double wide, etc.
;-
;+

    PLOTTING KEYWORD:
      [XYZ]TICKLEN [All]:
         Tick lengths of the  individual  axes  expressed  in  normal
         coordinates.This  field,  if  nonzero,  overrides the global
         tick length specified in !P.TICKLEN, and/or the TICKLEN key-
         word  parameter,  which  is expressed in terms of the window
         size.
;-
;+

    PLOTTING KEYWORD:
      TICKLEN [All]:
          Controls the length of the axis tick marks, expressed as  a
         fraction  of  the  window  size.  The default value is 0.02.
         TICKLEN of 1.0 produces a grid,  while  a  negative  TICKLEN
         makes tick marks that extend outside the window, rather than
         inwards.

         To produce outward-going tick marks of the normal length:

                         PLOT, X, Y, TICKLEN = -0.02

         To provide a new default tick length, set !P.TICKLEN.
;-
;+

    PLOTTING KEYWORD:
      [XYZ]TICKNAME [All]:
         The annotation of each tick.  A string array  of  up  to  30
         elements.
;-
;+

    PLOTTING KEYWORD:
      [XYZ]TICKS [All]:
         The number of major tick intervals to draw for the axis.  If
         omitted  IDL  will  select from three to six tick intervals.
         Setting this field to n, where n >  0,  produces  exactly  n
         tick intervals, and n+1 tick marks.
;-
;+

    PLOTTING KEYWORD:
      [XYZ]TICKV [All]:
         The data values for each tick mark, an array  of  up  to  30
         elements.   Note:  if you specify n tick intervals, you must
         specify n+1 tick values.
;-
;+

    PLOTTING KEYWORD:
      [XYZ]TICK_GET [All]:
         Returns the values of the  tick  marks  for  the  designated
         axis.   The  result  is a floating point array with the same
         number of elements as ticks.
         To retrieve in the variable V the values of the  tick  marks
         selected by IDL for the Y axis:

                          PLOT, X, Y, YTICK_GET = V
;-
;+

    PLOTTING KEYWORD:
      TITLE [All]:
          Produces a main title centered above the plot window.   The
         text  size  of this main title is larger than the other text
         by a factor of 1.25.  For example:

                     PLOT, X, Y, TITLE = 'Final Results'
;-
;+

    PLOTTING KEYWORD:
      [XYZ]TITLE [All]:
         Produces a title on the specified axis.
;-
;+

    PLOTTING KEYWORD:
      [XYZ]TYPE [AXIS, PLOT]:
         Specifies logarithmic axis if non-zero.
;-
;+

    PLOTTING KEYWORD:
      UPPER_ONLY [SURFACE]:
          Indicates that only the upper surface of the object  is  to
         be drawn.  By default, both surfaces are drawn.
;-
;+

    PLOTTING KEYWORD:
      YNOZERO [AXIS, PLOT]:
         Inhibits setting the minimum Y axis value to zero when the Y
         data  are all positive and non-zero, and no explicit minimum
         Y value  is  specified  (using  YRANGE,  or  !Y.RANGE).   By
         default,  the  Y  axis  spans  the range of 0 to the maximum
         value of Y, in the case of positive Y  data.   Set  bit   in
         !Y.STYLE to make this option the default.
;-
;+

    PLOTTING KEYWORD:
      ZAXIS [AXIS, CONTOUR, SURFACE]:
          Specifies the existence of a Z axis for  CONTOUR,  and  the
         placement  of  the  Z axis for SURFACE.  For AXIS, the ZAXIS
         keyword indicates that a Z axis is to be drawn and where  it
         should be placed.
         CONTOUR draws no Z axis by default.  Include the ZAXIS  key-
         word  in  the  call to CONTOUR to draw a Z axis.  This is of
         use only if  a  three-dimensional  transformation  is  esta-
         blished.

         By default, SURFACE draws the  Z  axis  at  the  upper  left
         corner of the axis box.  To suppress the Z axis, use ZAXIS =
         -1 in the call.  The position of the Z  axis  is  determined
         from  ZAXIS as follows: 1 =  lower right, 2 =  lower left, 3
         =  upper left, and 4 = upper right.
;-
;+

    PLOTTING KEYWORD:
      ZVALUE [PLOT, OPLOT, CONTOUR]:
          Sets the Z coordinate, in  normalized  coordinates  in  the
         range  of  0  to  1,  of the axis and data output from PLOT,
         OPLOT, and CONTOUR.

         This has effect  only  if  !P.T3D  is  set  and  the  three-
         dimensional  to  two-dimensional transformation is stored in
         !P.T. If Z_VALUE is not specified, CONTOUR will output  each
         contour  at  its Z coordinate, and the axes and title at a Z
         coordinate of 0.0.
;-


	IDL, Interactive Data analysis Language, is a
complete package for the interactive reduction, analysis,
and visualization of scientific data and images.  Optimized
for the workstation environment, IDL integrates a responsive
array oriented language with numerous data analysis methods
and an extensive variety of two and three dimensional
displays into a powerful tool for researchers.
	IDL supports an extensive data import capability, publication
quality hard copy output, and user-defined Motif graphical user
interfaces.
	Users can create complex visualizations in hours
instead of weeks with the aid of IDL's high level capabilities
and interactive environment.
	IDL is useful in physics, astronomy, image and
signal processing, mapping, medical imaging, statistics,
and other technical disciplines requiring visualization of
large amounts of data.

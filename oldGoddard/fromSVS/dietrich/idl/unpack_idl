#!/bin/sh
# unpack_idl
#
# This script expects to find a directory containing IDL
# distribution compressed tar files. When executed, it
# unpacks the distribution and removes the tar files.
#
# It also performs some incidental processing (idl_setup, idl_demo,
# file ownership, etc...)
#

# Preserve the file modes on the tape
umask 000

SCRIPT=`basename $0`

if [ -f /hp-ux ]; then
  SYSV_ECHO=1
fi
if [ -f /bin/4d ]; then
  SYSV_ECHO=1
fi

if [ "$SYSV_ECHO" = 1 ]; then
  ECHO_NONL=echo
  ECHO_NONL_TAIL=\\c
else
  ECHO_NONL="echo -n"
  ECHO_NONL_TAIL=
fi

chmod o+rx-w .  # Make sure everyone can read.


# Uncompress any .Z files and delete them
NUM=0
for FILE in *.tar.Z; do
  if [ $FILE != "*.tar.Z" ]; then
    NUM=`expr $NUM + 1`
  fi
done
if [ $NUM != 0 ]; then
  for FILE in *.tar.Z; do
    echo "$SCRIPT: $FILE"
    zcat $FILE | tar xvoRf -
    if [ $? != 0 ]; then
      echo "$SCRIPT: error unpacking."
      exit 1
    fi
    rm $FILE
  done
fi


# logged in as root and files writable? Make files  belong to this account.
if [ -f /etc/chown ]; then
    CHOWN=/etc/chown
else
    CHOWN=/bin/chown
fi
if [ `whoami` = "root" -a -w . ]; then $CHOWN root * */* */*/*; fi


# Generate the idl_demo script for quick demos
if [ ! -f idl_demo ]; then
  bin/make_idl_demo
fi


# Generate idl_setup file, just in case they need it and forget to build one
if [ ! -f idl_setup ]; then
  bin/make_idl_setup
fi


# How many sets of binaries are there?
NUM=0
for FILE in bin.*; do
  if [ $FILE != "*.tar.Z" ]; then
    NUM=`expr $NUM + 1`
  fi
done

echo "$SCRIPT: This IDL distribution supports $NUM
        operating system/hardware combination(s).
"

# If this distribution supports multiple OS/hardware combinations, offer
# to remove some of them to save room.
if [ $NUM != 1 ]; then
  echo "
This distribution is able to support multiple hardware/operating
systems. This is done by providing multiple copies of the IDL binaries.
If you do not need all the combinations present, you can remove the
unwanted ones and save disk space. You will now be given a description
of each one and then asked if it should be deleted.
Answer with a LOWERCASE y for yes or n for no.
"
RESP=""
while [ "$RESP" != y -a "$RESP" != n ]
do
  $ECHO_NONL "Do you wish to remove excess binaries? (y/n): $ECHO_NONL_TAIL"
  read RESP
  if [ "$RESP" != y -a "$RESP" != n ]; then
    echo "    Please answer y for yes or n for n"
  fi
done
if [ "$RESP" = n ]; then
  exit 0
 fi

  for FILE in bin.*; do
    if [ -f $FILE/DESCRIP ]; then
      echo " "
      cat $FILE/DESCRIP
      RESP=""
      while [ "$RESP" != y -a "$RESP" != n ]
      do
        $ECHO_NONL "    Delete? (y/n): $ECHO_NONL_TAIL"
        read RESP
        if [ "$RESP" != y -a "$RESP" != n ]; then
	  echo "    Please answer y for yes or n for n"
        fi
      done
      if [ "$RESP" = y ]; then
        echo "    Removing " $FILE
        rm -r $FILE
      fi
    fi
  done
fi

#!/bin/sh
#
# This script determines the operating system and hardware combination
# and overlays itself with the correct binary for the desired program.
# The program is determined from the name by which the script is invoked.

APPLICATION=`basename $0`

# Determine the operating system, hardware architecture, and os release
OS=
ARCH=
OSVER=
if [ -f /bin/arch ]; then			# SunOS
	OS="sunos"
	ARCH=".`/bin/arch`"
	if [ -f /usr/lib/libdl.so* ]; then
                OSVER=".4.1"
	elif [ -f /usr/lib/ld.so ]; then
                OSVER=".4.0"
	else
		OSVER=".3.5"
	fi
elif [ -f /bin/machine ]; then			# DEC Ultrix (sometimes)
	OS="ultrix"
	ARCH=".`/bin/machine`"
elif [ -f /hp-ux ]; then			# HP-UX
	OS="hp"
	file /hp-ux | grep s800 > /dev/null
	if [ $? = 0 ]; then
		ARCH=".s700"
	else
		ARCH=".s300"
	fi
elif [ -f /bin/4d ]; then			# SGI Irix
	OS="sgi"
elif [ -f /dev/root ]; then			# Mips
	OS="mips"
elif [ -f /vmunix ]; then			# DEC Ultrix
	OS="ultrix"
	file /vmunix | grep mips > /dev/null
	if [ $? = 0 ]; then
	  ARCH=".mips"
	else
	  ARCH=".vax"
	fi
elif [ -x /bin/smit ]; then			# IBM
	OS="ibm"
elif [ -b /dev/root ]; then			# Mips
	OS="mips"
fi

# Find the main IDL directory
if [ "$IDL_DIR" = "" ]; then
    for DIR in /usr/local/lib/idl /usr/local/idl /usr/local/bin/idl . ./idl
	do
	if [ -d $DIR ]; then
	    if [ -f $DIR/hersh.chr ]; then
        	IDL_DIR=$DIR
		break
	    fi
        fi
    done
fi

if [ "$IDL_DIR" = "" ]; then
    echo "Unable to access $APPLICATION. You will have to
define the IDL_DIR environment variable to point at the main
IDL directory before it will work."
exit 1
fi

# If the LM_LICENSE_FILE environent variable is not set, set it
# to point at the IDL main directory.
if [ "$LM_LICENSE_FILE" = "" ]; then
	LM_LICENSE_FILE=$IDL_DIR/license.dat
	export LM_LICENSE_FILE
fi

# Users Libraries
if [ "$IDL_PATH" = "" ]; then
	IDL_PATH="+$IDL_DIR/lib"
fi

# Add the IDL bin directory to the path so that idltool will always find idl
PATH=$IDL_DIR/bin:$PATH
export PATH IDL_DIR IDL_PATH

exec $IDL_DIR/bin.$OS$OSVER$ARCH/$APPLICATION $*

# We shouldn't get here unless there was an error.
echo "$APPLICATION is not availible for this system ($OS/$ARCH)"
exit 1

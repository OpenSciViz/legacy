#!/bin/sh
# install_idl_links
#
# This script is called by install_idl, once per architecture
# to be installed. Arguments are:
#       $1 - The name that corresponds to /usr in this case.
#       $2 - The real path of the main IDL directory.
#	$3 - The architecture name if this is a Sun.
#
# It does (if possible) the following:
#	- Place symbolic link from /usr/local/lib to current directory
#       - Place symbolic links from /usr/local/bin to idl
#	  (and idltool for Suns).
#

LOCAL=$1/local/bin
BINDIR=$2/bin

if [ -f /hp-ux ]; then
  SYSV_ECHO=1
fi
if [ -f /bin/4d ]; then
  SYSV_ECHO=1
fi
if [ ! -f /vmunix ]; then		# Ultrix test(1) lacks -b
  if [ -b /dev/root ]; then
    SYSV_ECHO=1
  fi
fi

if [ "$SYSV_ECHO" = 1 ]; then
  ECHO_NONL=echo
  ECHO_NONL_TAIL=\\c
else
  ECHO_NONL="echo -n"
  ECHO_NONL_TAIL=
fi


echo " "
if [ -f /bin/arch ]; then			# SunOS
  echo "Architecture: $3"
fi

# Link directory to /usr/local/lib. If we are already there,
# the symbolic link will fall harmlessly inside the idl
# directory, so we'll just remove it.
if [ "$2" != "$1/local/lib/idl" ]; then	# This if-stmt is not foolproof
  bin/make_link $2 $1/local/lib/idl
  STATUS=$?
else
  STATUS=0
fi
if [ "$STATUS" = "0" ]; then
    if [ -w $2/idl ]; then
        rm -f $2/idl    # It wasn't needed
    fi
    bin/make_link $BINDIR/idl $LOCAL/idl
    STATUS=$?
    if [ "$STATUS" = "0" ]; then
      if [ -f /bin/arch  ]; then
	bin/make_link $BINDIR/idltool $LOCAL/idltool
        STATUS=$?
      fi
    fi
fi

if [ "$STATUS" != "0" ]; then
    echo "
	Here are your options:

	    1) Correct the problem that prevented me from executing
	       properly and then re-execute install_idl
	       (It is OK to execute install_idl more than once).

	    2) Create symbolic links using the commands:
		   ln -s $2 /usr/local/lib/idl
                   ln -s $2/idl LOCAL_DIR/idl
	       and if this is a Sun:
                   ln -s $2/idltool LOCAL_DIR/idltool
	       where LOCAL_DIR is the path of a directory on your system
               that commonly appears in users paths.

	    3) You can create a file named idl_setup by executing
	       the command bin/make_idl_setup from this directory and
	       each user of IDL can add the line

		    source $2/idl_setup

	       to their .cshrc files. In this case, no
	       symbolic links are necessary, but every user has
	       to modify their .cshrc file.

	" | more
    $ECHO_NONL Please press return to continue. $ECHO_NONL_TAIL
    read ANSWER
fi

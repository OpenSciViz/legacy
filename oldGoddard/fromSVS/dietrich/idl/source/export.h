/* export.h */

/*
 *  Copyright (c) 1990, Research Systems Inc.  All rights reserved.
 *   Reproduction by any means whatsoever is  prohibited without express
 *   written permission. 
*/

#ifndef export_X
#define export_X

#ifndef MSG_DEF
#define MSG_DEF

#define M_GENERIC           -1
#define M_NAMED_GENERIC     -2

#endif /* MSG_DEF */


/***** Definitions from config *****/

#ifndef config_DEF
#define config_DEF

#if defined(__STDC__) || defined(VMS) || defined(AIX)  /* Ansi prototypes */
#define DO_PROTO		/* Use ANSI C function prototypes */
#endif

#ifdef FALSE
#undef FALSE
#endif
#define FALSE	(0)

#ifdef TRUE
#undef TRUE
#endif
#define TRUE	(1)

/*
 * The following definitions are to be used in all modules. They
 * are used by cx to generate .x files. public is used for functions that
 * are used outside a module, but which are RSI private. export is used
 * for functions that people outside of RSI can use. private is for
 * functions that are private to a module.
*/
#define private	static		/* static really means private */

#ifdef VMS			/* Used for global data in files w/o code */

#define public noshare		/* public is C default scope */
#define export noshare		/* Useable outside of RSI */
#define global noshare globaldef
#define export_global noshare globaldef{"EXPORTED_DATA"} 
#define GLOBALREF globalref

#else				/* Under Unix, do the usual extern thing */

#define public			/* public is C default scope */
#define export			/* Useable outside of RSI */
#define global
#define export_global
#define GLOBALREF extern
#define noshare			/* Not used outside of VMS */

#endif

/*
 * cx, cx_export, and make_sysnames emit "EXTERN" in places where "extern"
 * is appropriate. Here, we define it for the compilation environment.
 *
*/
#ifdef VMS
#define EXTERN extern noshare
#else
#define EXTERN extern
#endif


#define CAN_USE_LMGR		/* True if network manager works */
#if (defined(vax) && defined(ultrix)) || defined(hp9000s800) || defined(MIPS) || defined(DGUX) || defined(CANT_USE_LMGR)
#undef CAN_USE_LMGR		/* Sorry, only genver is availible */
#endif

#ifdef MSDOS
#undef CAN_USE_LMGR
#endif

#ifdef sun			/* All Suns use SUN_VIEW */
				/* OS 4.1 is upwards compatible with 4.0 */
#if defined(SUN_OS_4_1) && !defined(SUN_OS_4)
#define SUN_OS_4
#endif
#define SUN_VIEW 1
#endif

/*
 * We assume that a signal catcher remains installed after a signal
 * in the style of BSD reliable signals unless RESET_SIGNALS is defined.
 */
#if (defined(mips) && !defined(ultrix)) || defined(hpux) || defined(VMS) || defined(MSDOS) || defined (DGUX)
#define RESET_SIGNALS
#endif

#ifdef irix			/* Use IRIS BSD sigvec facility  */
#define _BSD_SIGNALS
#define _BSD_COMPAT
#endif

#if defined(DGUX) && !defined(_BSD_SIGNAL_FLAVOR)
#define _BSD_SIGNAL_FLAVOR      /* Use BSD compatibility */
#endif


#ifdef _AIX
#define unix			/* AIX Doesn't think its unix */
#endif

#if defined(__unix__) && !defined(unix)
#define unix                    /* ANSI C doesn't define it this way */
#endif


#if DEBUGGING < 2
#define REGISTER register	/* Use explicit register declarations */
#else
#define REGISTER
#endif


/**** Maximum # of params allowed in a call ****
  NEVER make this > 127.  */
#define MAXPARAMS 64

/**** Maximum # of array dimensions ****/
#define MAX_ARRAY_DIM 8

/**** Characters in longest identifier ****/
#define MAXIDLEN 15		/* This should always be an odd number */

/**** Longest allowed file path specification ****/
#ifdef VMS
#define	MAX_PATH_LEN	264		/* That's what VMS allows (+ slop) */
#endif
#ifdef unix
#define	MAX_PATH_LEN	1024		/* That's what BSD allows */
#endif
#ifdef MSDOS
#define MAX_PATH_LEN 64
#endif


#ifdef VMS
/* Take advantage of better memory allocation routines availible under VMS */
#define malloc	VAXC$MALLOC_OPT
#define calloc	VAXC$CALLOC_OPT
#define free	VAXC$FREE_OPT
#define cfree	VAXC$CFREE_OPT
#define realloc	VAXC$REALLOC_OPT

/* Eventually, a good place for these definitions will be needed */
unsigned long htonl();
unsigned long ntohl();
unsigned short ntohs();
unsigned short htons();
#endif
#endif	/* config_DEF */




/***** Definitions from defs *****/

#ifndef defs_DEF
#define defs_DEF
#if !defined(__WATCOMC__) || !defined(PLTYPES)
typedef unsigned char UCHAR;	/* Unsigned character type */
#endif


/*
 * Define VARIABLE type values - Note that TYP_UNDEF is always 0 by definition.
 * It is correct to use the value 0 in place of TYP_UNDEF. It is not
 * correct to assume the value assigned to any other type - the preprocessor
 * definitions below must be used.
 */

#define	TYP_UNDEF		0
#define	TYP_BYTE		1
#define TYP_INT			2
#define TYP_LONG		3
#define TYP_FLOAT		4
#define TYP_DOUBLE		5
#define TYP_COMPLEX		6
#define TYP_STRING		7
#define TYP_STRUCT		8

#define MAX_SIMPLE_TYPE		7
#define NUM_SIMPLE_TYPES	8
#define MAX_TYPE		8
#define NUM_TYPES		9



/*
 * The above type codes each have a bit mask value associated with
 * them. The bit mask value is computed as (2**Type_code), but the
 * following definitions can also be used. Some routines request the
 * bit mask value instead of the type code value.
*/
#define TYP_B_SIMPLE		255
#define TYP_B_ALL		511

/* This macro turns it's argument into its bit mask equivalent.
 * The argument type_code should be one of the type codes defined
 * above.
*/ 
#define TYP_MASK(dim_code)	(1 << dim_code)



/***** Define VARIABLE flag values ********/

#define V_CONST		1
#define V_TEMP		2
#define V_ARR		4
#define V_FILE		8
#define V_DYNAMIC	16
#define V_STRUCT	32
#define V_NOT_SCALAR	(V_ARR | V_FILE | V_STRUCT)

/**** Define ARRAY flag values ****/
#define A_FILE		1	/* Array is a FILE variable (ASSOC) */
#define A_NO_GUARD	2	/* Indicates no data guards for array */

/**** Define STRUCTURE flag values ****/
#define S_SAVED		1	/* This struct definition has already been
				saved or restored in the current save file. */


/**** Define values which are not defined for the WATCOM C compiler(386) ****/
#ifdef __WATCOMC__
#define SIGPIPE 101
#endif



  /* **** Define commonly used structures: *** */

typedef struct {		/* Define complex structure */
  float r,i;
} COMPLEX;

typedef struct {		/* Define string descriptor */
  unsigned short slen;		/* Length of string, 0 for null */
  short stype;			/* type of string, static or dynamic */
  char *s;			/* Addr of string */
} STRING;

typedef struct {		/* Its important that this block
				   be an integer number of longwords
				   in length to ensure that array
				   data is longword aligned.  */
  long elt_len;			/* Length of element in char units */
  long arr_len;			/* Length of entire array (char) */
  long n_elts;			/* total # of elements */
  UCHAR *data;			/* ^ to beginning of array data */
  UCHAR n_dim;			/* # of dimensions used by array */
  UCHAR flags;			/* Array block flags */
  short file_unit;		/* # of assoc file if file var */
  long dim[MAX_ARRAY_DIM];	/* dimensions */
  long data_guard;		/* Guard longword */
} ARRAY;

typedef struct {		/* Reference to a structure */
  ARRAY	*arr;			/* ^ to array block containing data */
  struct structure *sdef;	/* ^ to structure definition */
} SREF;

/* ALLTYPES can be used to represent all VARIABLE types */
typedef union {
  UCHAR c;			/* Byte value */
  short i;			/* Integer short value */
  long l;			/* Long value */
  float f;			/* Floating value */
  double d;			/* Double value */
  COMPLEX cmp;			/* Complex value */
  STRING str;			/* String descriptor */
  ARRAY *arr;			/* ^ to array descriptor */
  SREF s;			/* Descriptor of structure */
} ALLTYPES;
typedef struct {		/* VARIABLE definition */
  UCHAR type;			/* Type byte */
  UCHAR flags;			/* Flags byte */
  ALLTYPES value;
} VARIABLE;

typedef VARIABLE *VPTR;		/* Pointer to VARIABLE */
typedef void (* PRO_PTR)();	/* ^ to interpreter procedure (ret is void) */
typedef VARIABLE *(* FUN_RET)();   /* ^ to interp. function (ret ^ to VAR) */
#endif				/* defs_DEF */




/***** Definitions from message *****/

#ifndef message_DEF
#define message_DEF
#ifdef VMS
extern volatile int noshare errno;
extern volatile int noshare sys_nerr;
extern volatile noshare char *sys_errlist[];
#endif

#ifdef unix
extern int errno;		/* System provided error number */
extern int sys_nerr;		/* # of entries in sys_errlist */
extern char *sys_errlist[];	/* System provided error messages */
#endif


/* Allowed codes for action parameter to message() */
#define	MSG_RET			0   /* Return to caller */
#define MSG_EXIT		1   /* Terminate process via exit(3) */
#define MSG_LONGJMP		2   /* General error. Obey the error handling
				       established by the ON_ERROR user
				       procedure. */
#define MSG_LONGJMP_NOTRACE	3   /* MSG_LONGJMP, but prevent the usual
				       traceback */
#define MSG_IO_LONGJMP		4   /* I/O error. Obey the error handling
				       established by the ON_IOERROR user
				       procedure. */
#define MSG_IO_LONGJMP_NOTRACE	5   /* MSG_IOLONGJMP, but prevent the usual
				       traceback */
#define MSG_INFO		6   /* Informational. Like MSG_RET, but won't
				       set !ERR or !ERR_STRING. Also,
				       inhibited by !QUIET */


/* Allowed attribute masks that can be OR'd into the action code */
#define MSG_ATTR_NOPRINT	0x00010000	/* Suppress the printing of
						   the error text to stderr,
						   but do everything else in
						   the normal way. */
#define MSG_ATTR_MORE	0x00020000   /* Use idl_more() instead of printf(3S)
					to output the message. The calling
					routine must worry about calling
					idl_more_reset(). A side effect of
					this is that the message goes to the
					file named in idl_more_reset(), not
					necessarily stderr. */
#define MSG_ATTR_NOPREFIX	0x00040000   /* Don't output the normal
						message (from !MSG_PREFIX),
						just the message text. */
#define MSG_ATTR_QUIET		0x00080000   /* If the message would normally
						be printed and !QUIET is
						non-zero, the printing
						is suppressed. Everything
						else is updated as expected. */

#endif	/* message_DEF */




/***** Definitions from macros *****/

#ifndef macros_DEF
#define macros_DEF

/* General math macros */
#ifndef MIN
#define MIN(x,y) (((x) < (y)) ? (x) : (y))
#endif
#ifndef MAX
#define MAX(x,y) (((x) > (y)) ? (x) : (y))
#endif
#ifndef ABS
#define ABS(x) (((x) >= 0) ? (x) : -(x))
#endif

/* Round x up modulo m. m must be a power of 2 : */
#define ROUND_UP(x,m) \
  (((x) + (m-1)) & (~(m-1)))
/**** Macro to cast the address of a variable to (char *) ****/
#define CHAR(x) ((char *) x)

/**** Macro to take the address of a variable and cast it to (char *) ****/
#define CHARA(x) ((char *) &x)

/**** Macro used to get pointer to a valid string from a STRING descriptor */
#define STRING_STR(desc) ((desc)->slen ? (desc)->s : "")

/* Check if var is a temp.  If so, delete it using deltmp  */
#define DELTMP(v) { if ((v->flags) & V_TEMP) deltmp(v); }

#define EXCLUDE_UNDEF(v) { if (!v->type) \
	var_error(M_UNDEFVAR, v, MSG_LONGJMP); }
#define EXCLUDE_CONST(v)	{ if (v->flags & V_CONST) \
	var_error(M_NOCONST, v, MSG_LONGJMP); }
#define EXCLUDE_EXPR(v)	 { if (v->flags & (V_CONST | V_TEMP)) \
	var_error(M_NOEXPR, v, MSG_LONGJMP); }
#define EXCLUDE_FILE(v)	{ if (v->flags & V_FILE) \
	var_error(M_NOFILE, v, MSG_LONGJMP); }
#define EXCLUDE_STRUCT(v)	{ if (v->flags & V_STRUCT) \
	var_error(M_NOSTRUCT, v, MSG_LONGJMP); }
#define EXCLUDE_COMPLEX(v)	{ if (v->type == TYP_COMPLEX) \
	var_error(M_NOCOMPLEX, v, MSG_LONGJMP); }
#define EXCLUDE_STRING(v)	{ if (v->type == TYP_STRING)  \
	var_error(M_NOSTRING, v, MSG_LONGJMP); }
#define EXCLUDE_SCALAR(v) { if (!(v->flags & V_NOT_SCALAR)) \
	var_error(M_NOSCALAR, v, MSG_LONGJMP);}


/**** Macros used to ensure that variables possess certain attributes ****/
#define ENSURE_ARRAY(v) { if (!(v->flags & V_ARR)) \
	var_error(M_NOTARRAY, v, MSG_LONGJMP); }
#define ENSURE_SCALAR(v) { if (v->flags & V_NOT_SCALAR) \
	var_error(M_NOTSCALAR, v, MSG_LONGJMP);}
#define ENSURE_STRING(v) { if (v->type != TYP_STRING)  \
	var_error(M_REQSTR, v, MSG_LONGJMP);}
#define ENSURE_SIMPLE(v) { if (v->flags & (V_FILE | V_STRUCT))\
  	var_error(M_SIMVARONLY, v, MSG_LONGJMP);}
#define ENSURE_STRUCTURE(v) { if (!(v->flags & V_STRUCT)) \
	var_error(M_STRUC_REQ, v, MSG_LONGJMP);}

     /* Check if var has a dynamic part. If so, delete it using delvar  */
#define DELVAR(v) { if ((v->flags) & V_DYNAMIC) delvar(v); }


/*
 * Under VMS, we define bcopy() and bzero() as macros that have the
 * same syntax as the Unix functions.
 */
#ifdef VMS
extern void OTS$MOVE3();	/* Move Data Without Fill */
extern void OTS$MOVE5();	/* Move Data With Fill */

#define bcopy(src, dest, length) /* Copies length bytes from src to dest */ \
  OTS$MOVE3((long) length, (unsigned char *) src, (unsigned char *) dest)

#define bzero(dest, length) /* Places length 0 bytes in the array dest */ \
  OTS$MOVE5(0L, (unsigned char *) 0, 0, (long) length, (unsigned char *) dest)
  
/* Check the VMS status code and issue error message */
#define CHECK_VMS_STAT(stat, action, msg) \
  { if (!(stat & 1)) vms_message(M_SYSERR, stat, 0, action, msg);}

#endif				/* VMS */

#if defined(__WATCOMC__) && !defined(bcopy)	/* Same defs for WATCOMC */
#include <string.h>
#define bcopy(src,dest,len)	(memcpy((dest), (src), (len)))
#define bzero(dest,len)		(memset((dest), 0, (len)))
#define bcmp(b1,b2,len)		(memcmp((b1), (b2), (len)))
#endif

#endif				/* macros_DEF */




/***** Definitions from crearr *****/

#ifndef create_array_DEF
#define create_array_DEF

/* The following define the valid values for the init arg to basic_array */
#define	BASICARR_INI_ZERO	0	/* Zero data area */
#define	BASICARR_INI_NOP	1	/* Don't do anything to data area */
#define	BASICARR_INI_INDEX	2	/* Put 1-D index into each elt. */

#endif	/* create_array_DEF */




/***** Definitions from ez *****/

#ifndef ez_DEF
#define ez_DEF

/* These constants can be ORd together to form the value for the
   access field of EZ_ARG */
#define EZ_ACCESS_R	1		/* Arg is readable */
#define EZ_ACCESS_W	2		/* Arg is writable */
#define EZ_ACCESS_RW	3		/* Arg is readable and writable */


/* This macro turns it's argument into a bit mask suitable for
 * the allowed_dims field of EZ_ARG. The argument dim_code should be
 * 0 for scalar, 1 for 1D, 2 for 2D, etc...
 */ 
#define EZ_DIM_MASK(dim_code)	(1 << dim_code)

/* Define type mask of all numeric types: */
#define TYP_NUMERIC TYP_MASK(TYP_INT)|TYP_MASK(TYP_LONG)| \
  TYP_MASK(TYP_FLOAT)| TYP_MASK(TYP_DOUBLE)| TYP_MASK(TYP_COMPLEX) |\
   TYP_MASK(TYP_BYTE)


/* These constants should be used instead of EZ_DIM_MASK when appropriate */
#define EZ_DIM_ARRAY	510		/* Allow all but scalar */
#define EZ_DIM_ANY	511		/* Allow anything */


/* These constants can be ORd together to form the value for the
   pre field of EZ_ARG. These actions are taken only if the argument
   has EZ_ACCESS_R. */
#define EZ_PRE_SQMATRIX		1	/* Arg must be a square matrix. */
#define EZ_PRE_TRANSPOSE	2	/* Transpose arg. This only happens
					   with read access. */


/* These constants can be ORd together to form the value for the
   post field of EZ_ARG. These actions are taken only if the argument
   has EZ_ACCESS_W.  If EZ_POST_WRITEBACK is not present, none of the
   other actions are considered, since that would imply wasted effort. */
#define EZ_POST_WRITEBACK	1	/* Transfer the contents of
					   uargv to the actual argument. */
#define EZ_POST_TRANSPOSE	2	/* Transpose uargv prior to writing. */


/*
 * EZ_ARG is the definition for the structure used by ez_call()
 * and ez_call_cleanup() to define the plain arguments being passed
 * to a routine.
 */
typedef struct {
    short	allowed_dims;	/* A bit mask that specifies the
				 allowed dimensions. Bit 0 means scalar,
				 bit 1 is 1D, etc. Use the EZ_DIM_* constants
				 defined in this file to specify this value. */
    short	allowed_types;	 /* This is a bit mask defining the
				  allowed data types for the argument.
				  To convert the TYP_* type codes defined
				  in defs.h to the appropriate bits,
				  use the formula 2**(type_code) or use
				  the TYP_B_* bit masks defined in defs.h
				  NOTE: If you specify a value for convert,
				  its a good idea to specify TYP_B_ALL or
				  TYP_B_SIMPLE here. The type conversion will
				  catch any problems and your routine will
				  be more flexible. */
    short	access;		/* Some combination of the EZ_ACCESS
				 constants defined above. */
    short	convert;	/* If non-zero, the TYP_* type code to
				   which the argument will be converted.
				   A value of zero means that no conversion
				   will be applied. */
    short	pre;		/* A bit mask that specifies special purpose
				   processing that should be performed on
				   the variable by ez_call(). These bits
				   are specified with the EZ_PRE_* constants.
				   This processing occurs *AFTER* any type
				   conversions specified by convert. */
    short	post;		/* A bit mask that specifies special purpose
				   processing that should be performed on
				   the variable by ez_call_cleanup(). These
				   bits are specified with the EZ_POST_*
				   constants. */
    VPTR	to_delete;	/* RESERVED TO EZ MODULE. DO NOT MAKE
				 USE OF OR CHANGE THIS FIELD. If EZ
				 allocated a temporary variable to satisfy
				 the conversion requirements given by the
				 convert field, the VPTR to that temp
				 is stashed here by ez_call for use by
				 ez_call_cleanup(). */
    VPTR	uargv;		/* After calling ez_call(), uargv contains
				   a pointer to the VARIABLE which is
				   the argument. */
    ALLTYPES	value;		/* This is a copy of the value field
				 of the variable pointed at by uargv.
				 For scalar variables, it contains the
				 value, for arrays it points at the
				 array block. */
    } EZ_ARG;

#endif	/* ez_DEF */




/***** Definitions from files *****/

#ifndef files_DEF
#define files_DEF
/**** Definition of bits in access field of FILE_DESC and FILE_STAT ****/
#define OPEN_R		1	/* Open file for reading */
#define OPEN_W		2	/* Open file for writing */
#define OPEN_NEW	4	/* Unix - Truncate old file contents.
				   VMS - Use a new file. */
#define OPEN_APND	8	/* File open with pointer at EOF */

/**** Definition of bits in flags field of FILE_DESC and FILE_STAT ****/
#define F_ISATTY	1	/* Is a terminal */
#define	F_NOCLOSE	2	/* Don't let user close */
#define	F_MORE		4	/* Use more(1) like pager for fmt output */
#define F_XDR		8	/* Is a XDR file */
#define F_DELETE	16	/* Delete on close */
#define F_SR		32	/* Is a SAVE/RESTORE file.` */
#define F_UNIX_F77	64	/* Unformatted f77(1) I/O */
#define	F_UNIX_PIPE	128	/* fptr is to a socketpair(2) */
#define F_UNIX_NOBUFFER	256	/* Don't do stdio buffering */
#define F_UNIX_SPECIAL  (1 << 9) /* It's a device/special file */
#define F_VMS_FIXED	(1 << 10)   /* Fixed length records */
#define F_VMS_VARIABLE	(1 << 11)   /* Variable length records */
#define F_VMS_SEGMENTED	(1 << 12)   /* FORTRAN segmented var len records */
#define F_VMS_STREAM	(1 << 13)   /* Stream file */
#define F_VMS_RMSBLK	(1 << 14)   /* RMS Block Mode access */
#define F_VMS_RMSBLKUDF	(1 << 15)   /* RMS block mode files are created
				       with FIXED length 512 byte records.
				       This bit indicates that the RMS
				       UNDEFINED record type should be used. */
#define F_VMS_INDEXED	(1 << 16)   /* Indexed file */
#define F_VMS_PRINT	(1 << 17)   /* Send to SYS$PRINT on close */
#define F_VMS_SUBMIT	(1 << 18)   /* Send to SYS$BATCH on close */
#define F_VMS_TRCLOSE	(1 << 19)   /* Truncate file allocation on close */
#define F_VMS_CCLIST	(1 << 20)   /* CR/LF carriage control */
#define F_VMS_CCFORTRAN	(1 << 21)   /* FORTRAN style carriage control */
#define F_VMS_CCNONE	(1 << 22)   /* Explicit carriage control */
#define F_VMS_SHARED	(1 << 23)   /* Shared access */
#define F_VMS_SUPERSEDE (1 << 24)   /* Supersede existing version on open */
#define F_DOS_TEXT	(1 << 25)   /* File is in text mode (^M^J) */
#define F_DOS_BINARY	(1 << 26)   /* File is in binary mode (^J) */
  
/* Macro that sets the F_NOCLOSE bit in a file descriptor */
#define FILE_NOCLOSE(unit) set_file_close((unit), FALSE)

/* Macro that removes the F_NOCLOSE bit in a file descriptor */
#define FILE_CLOSE(unit) set_file_close((unit), TRUE)

/**** File units that map to standard units ****/
#define STDIN_UNIT	0
#define STDOUT_UNIT	-1
#define STDERR_UNIT	-2
#define NON_UNIT	-100	/* Gauranteed to be an invalid unit */

/* Valid flags to bit-OR together for ensure_file_status() flags argument */
#define EFS_USER	1	/* Must be user unit (1 - MAX_USER_FILES) */
#define EFS_OPEN	2	/* Unit must be open */
#define EFS_CLOSED	4	/* Unit must be closed */
#define EFS_READ	8	/* Unit must be open for input */
#define EFS_WRITE	16	/* Unit must be open for output */
#define EFS_NOTTY	32	/* Unit cannot be a tty */
#define EFS_NOPIPE	64	/* Unit cannot be a pipe */
#define EFS_NOXDR	128	/* Unit cannot be a XDR file */
#define EFS_ASSOC	256	/* Unit can be assoc'd. This implies USER,
				   OPEN, NOTTY, NOPIPE, and NOXDR, in
				   addition to other OS specific concerns */


/**** Struct for global variable term, filled by init_files() ****/
typedef struct {
  char *name;			/* Name of terminal type */
  char is_tty;			/* True if stdin is a terminal */
  int lines;			/* Lines on screen */
  int columns;			/* Width of output */
} TERMINFO;


/**** Struct that is filled in by get_file_stat() ****/
typedef struct {
  char *name;
  short access;
  long flags;
  FILE *fptr;
  struct {
    unsigned short mrs;
  } rms;
} FILE_STAT;

#endif	/* files_DEF */




/***** Definitions from graphics *****/

#ifndef graphics_DEF
#define graphics_DEF

/*
 * DEVICE_CORE defines the core functions required by every
 * device driver. Most fields can be filled with
 * a NULL indicating the ability dosen't exist. draw and erase are
 * exceptions to this --- If you can't do that much, why bother with a driver?
*/
typedef struct {
  PRO_PTR draw;			/* ^ to draw routine */
  int (*text)();		/* text output, or NULL */
  PRO_PTR erase;		/* erase */
  PRO_PTR cursor;		/* cursor inquire and set, or NULL */
  PRO_PTR polyfill;		/* Fill irregular polygon, or NULL */
  PRO_PTR inter_exit;		/* Returning to interactive mode, or NULL */
  PRO_PTR flush;		/* Flush entry, or NULL */
  PRO_PTR load_color;		/* Load color tables, or NULL */
  PRO_PTR rw_pixels;		/* Pixel input/output, or NULL */
  PRO_PTR dev_specific;		/* Rout. to call from DEVICE proc, or NULL.*/
  PRO_PTR dev_help;		/* Rout. to call for HELP,/DEVICE, or NULL */
  PRO_PTR load_rtn;		/* Routine to call when driver is loaded */
} DEVICE_CORE;


/*
 * DEVICE_WINDOW contains pointers to functions that accomplish
 * window system operations. If the device is a window system,
 * every field in this struct must point at a valid function,
 * they're called without checking.
 */
typedef struct {		/* Procedures & functions for image device: */
  PRO_PTR window_create;	/* Create a window */
  PRO_PTR window_delete;	/* Delete a window */
  PRO_PTR window_show;		/* Expose a window */
  PRO_PTR window_set;		/* Set the current window */
  FUN_RET window_menu;		/* Menu function */
} DEVICE_WINDOW;

/*
 * DEVICE_DEF is the interface between a device driver and the rest
 * of the Structure defining a graphics device. Every field in this
 * structure must contain valid information --- it is used without
 * any error checking.
 */
typedef struct {		/* Device descriptor, mostly static attributes
				   and definitions: */
  STRING name;			/* STRING for name */
  int t_size[2];		/* Total size in device coordinates */
  int v_size[2];		/* Visible area size, device coords */
  int ch_size[2];		/* Default character sizes */
  float px_cm[2];		/* Device units / centimeter, x & y. */
  int n_colors;			/* # of possible simultaneous colors */
  int table_size;		/* # of color table elements */
  int fill_dist;		/* minimum line spacing for solid fill */
  int window;			/* Current window number */
  int unit;			/* Unit number of output file */
  int flags;			/* Advertise limitations and abilities */
  int origin[2];		/* Display XY (pan/scroll) origin */
  int zoom[2];			/* Display XY zoom factors */
  float aspect;			/* Aspect ratio, = v_size[0] / v_size[1]. */
  DEVICE_CORE core;		/* Core graphics */
  DEVICE_WINDOW winsys;		/* Window system. Only required if D_WINDOWS */
} DEVICE_DEF;


/* Define bits in DEVICE_DEF flags: */

#define D_SCALABLE_PIXELS 1	/* True if pixel size is variable (e.g. PS) */
#define D_ANGLE_TEXT (1 << 1)	/* True if device can output text at angles */
#define D_THICK (1 << 2)	/* True if line thickness can be set */
#define D_IMAGE (1 << 3)	/* True if capable of imaging */
#define D_COLOR (1 << 4)	/* True if device supports color */
#define D_POLYFILL (1 << 5)	/* True if device can do polyfills */
#define D_MONOSPACE (1<<6)	/* True if device has only monspaced text */
#define D_READ_PIXELS (1<<7)	/* True if device can read back pixels */
#define D_WINDOWS (1<<8)	/* True if device supports windows */
#define D_WHITE_BACKGROUND (1<<9)   /* True if device background is white, like
				       PostScript. */
#define D_NO_HDW_TEXT (1<<10)	/* True if device has no hardware text */
#define D_POLYFILL_LINE (1<<11)	  /* True to use device driver for line
				     style polyfills. */
#define D_HERSH_CONTROL (1<<12)	  /* True if device accepts hershey style
				     control characters. */
#define D_PLOTTER (1<<13)	/* True if pen plotter */
#define D_WORDS (1<<14)		/* True if device images can be words */
#define D_KANJI (1 << 15)	/* Device has Kanji characters */
#define D_WIDGETS (1 << 16)	/* Device supports graphical user interfaces */
#define D_Z (1 << 17)		/* Device is 3d */

/* *** Structure defining current device and parameters: *** */
#define MAX_TICKN 30		/* Max # of axis annotations */

#define COLOR_MAP_SIZE 256	/* Size of internal color map. */

#define NUM_LINESTYLES 6	/* # of line styles */
#define X0 0			/* Subscripts of fields for rect structures */
#define Y0 1
#define X1 2
#define Y1 3
#define Z0 4
#define Z1 5


#define AX_LOG 1		/* Axis type values */
#define AX_MAP 2

#define AX_EXACT 1		/* Axis style values: */
#define AX_EXTEND 2
#define AX_NONE 4
#define AX_NOBOX 8
#define AX_NOZERO 16

typedef struct {		/* System variable for axis */
  STRING title;			/* Axis title */
  int type;			/* 0 = normal linear, 1=log. */
  int style;			/* 0 = norm, AX_EXTEND, AX_EXACT, AX_NONE,
				   AX_NOBOX. */
  int nticks;			/* # of ticks, 0=auto, -1 = none */
  float ticklen;		/* Tick length, normalized */
  float thick;			/* Axis thickness */
  float range[2];		/* Min and max of endpoints */
  float crange[2];		/* Current min & max */
  float s[2];			/* Scale factors, screen = data*s[1]+s[0] */
  float margin[2];		/* Margin size, in char units. */
  float omargin[2];		/* Outer margin, in char units */
  float window[2];		/* data WINDOW coords, normal units */
  float region[2];		/* Plot region, normal units */
  float charsize;		/* Size of annotations */
  int minor_ticks;		/* Minor ticks */
  float tickv[MAX_TICKN];	/* Position of ticks */
  STRING annot[MAX_TICKN];	/* Annotation */
  /* After here, the elements are not accessible to the user via
   the system variables: */
  VPTR ret_values;		/* Returned tick values */
} AXIS;

typedef struct {
  int background;		/* Background color */
  float charsize;		/* Global Character size */
  float charthick;		/* Character thickness */
  int clip[6];			/* Clipping rectangle, normalized coords */
  int color;			/* Current color */
  int font;			/* Font */
  int linestyle;		/* Line style */
  int  multi[5];		/* Cnt, Cols/rows, major dir for multi plts. */
  int clip_off;			/* True if clipping is disabled */
  int noerase;			/* No erase flag */
  int nsum;			/* Number of points to sum */
  float position[4];		/* Default window */
  int psym;			/* Marker symbol */
  float region[4];		/* Default plotting region */
  STRING subtitle;		/* Plot subtitle */
  float t[16];			/* Matrix (4x4) of homogeneous transform */
  int  t3d_on;			/* True if 3d homo transform is on */
  float thick;			/* Line thickness */
  STRING title;			/* Main plot title */
  float ticklen;		/* Tick length */
  int chl;			/* Default channel */
  DEVICE_DEF *dev;		/* Current output device, not user accesible */
} PLOT_COM;



/* Define cursor function codes: */

#define CURS_SET 1		/* Set cursor */
#define CURS_RD  2		/* Read cursor pos */
#define CURS_RD_WAIT 3		/* Read cursor with wait */
#define CURS_HIDE 4		/* Disable cursor */
#define CURS_SHOW 5		/* Display cursor */
#define CURS_RD_MOVE 6		/* Read & wait for movement or button */
#define CURS_RD_BUTTON_UP 7	/* Wait for button up transition */
#define CURS_RD_BUTTON_DOWN 8	/* Wait for button down transition */
#define CURS_HIDE_ORIGINAL 9	/* Restore cursor to its original shape
				   (window systems) instead of blanking it. */

/* Define coordinate system types: */
#define COORD_DATA 0
#define COORD_DEVICE 1
#define COORD_NORMAL 2
#define COORD_MARGIN 3
#define COORD_IDEVICE 4

#define PX 0			/* Subscripts for each point member */
#define PY 1
#define PZ 2
#define PH 3

typedef union {			/* Describe a point: */
  struct {			/* Discrete point, ref by .x, .y, or .z: */
    float x,y,z,h;		/* Homogenous coordinates */
  } d;
  struct {			/* Integer discrete: */
    int x,y,z,h;
  } i;
  float p[4];			/* Point refered to by [0],[1],... for
				   x, y, etc.*/
  int ip[4];			/* Integer representation, only valid
				   for COORD_IDEVICE. */
} POINT;
    
typedef struct {		/* Attributes structure for points & lines */
  int color;			/* Specifys all that can go wrong w/ graphic */
  float thick;
  int linestyle;
  float *t;			/* NULL for no 3d transform, or pointer to
				   4 by 4 matrix. */
  int *clip;			/* NULL for no clipping or ^ to [2][2]
				   clipping rectangle in device coord. */
  AXIS *ax,*ay,*az;		/* Axis definitions */
  int chl;			/* For devices with multiple channels */
} ATTR_STRUCT;

typedef struct {		/* Graphic text attribute structure.
				   Passed to text routines. */
  int font;			/* 0 for hdw text, -1 for hershey */
  int axes;			/* Text axes, 0 = xy, 1 = xz, 2 =yz,
				   3 = yx, 4 = zx, 5 = zy */
  float size;			/* Text size, 1.0 = normal */
  float orien;			/* Orientation, degrees CCW from normal */
  float align;			/* Justification, 0.0 = left, 1.0 = right,
				   0.5  = centered. */
} TEXT_STRUCT;


/* Structure that defines secondary paramenters for imaging */
typedef struct {		/* Imaging attribute structure */
  short xsize_exp;		/* Non-0 if xsize is EXPlictily set by user */
  short ysize_exp;		/* Non-0 if ysize is EXPlictily set by user */
  long xsize, ysize;		/* Requested size of image (dev coords) */
  int chl;			/* Channel */
  int order;			/* Image order - 0 bottom to top */
  /* Three element array giving the stride between colors of the same
     pixel, adjacent pixels of the same color, and rows of the same color.
     color_stride[0] is non-zero for true color. */
  int color_stride[3];
  int image_is_scratch;		/* True if source image is a temp */
  int b_per_pixel;		/* # of bytes/pixel */
} TV_STRUCT;



typedef struct {		/* Structure defining polygon fills */
  enum {
    POLY_SOLID, POLY_PATTERN, POLY_IMAGE, POLY_GOURAUD, POLY_IMAGE3D
    } fill_type;
  ATTR_STRUCT *attr;		/* Graphics attribute structure */
  PRO_PTR routine;		/* Drawing routine to use */
  union {			/* Operation dependent params: */
    struct {			/* Image fill  */
      UCHAR *data;		/* Fill data for image fill */
      int d1, d2;		/* Dimensions of fill data */
      float *im_verts;		/* Image coords of verts */
      UCHAR interp;		/* TRUE to interpolate in image space */
      UCHAR transparent;	/* Transparency threshold, 0 for none */
    } image;
    struct {			/* Line-pattern fill: */
      float angle;		/* Fill orientation in degrees */
      int spacing;		/* Line spacing, in device units */
      float ct, st;		/* Cos / sin of rotation */
    } lines;
    int fill_style;		/* Hardware dependent fill style for
				   POLY_SOLID */
  } extra;
  struct {			/* Info used only with Z buffer device */
    float *z;			/* The Z values */
    int *shades;		/* Shading values at verts for POLY_GOURAUD */
  } three;
} POLYFILL_ATTR;


typedef struct {		/* Struct containing last mouse status */
  int x,y;			/* X & Y device coordinates */
  int button;			/* Button status bits */
  int time;			/* Time stamp,  not present in all devices */
} MOUSE_STRUCT;

#endif	/* graphics_DEF */




/***** Definitions from keyword *****/

#ifndef keyword_DEF
#define keyword_DEF

				/* Bit values of flags field: */
#define KW_ARRAY (1 << 12)	/* If specified array is required, otherwise
				   scalar required */
#define KW_OUT (1 << 13)	/* Indicates parameter is an output
				   parameter.  The address of the VARIABLE
				   is stored in the value field. No checking is
				   performed.  Special hint: to find out if
				   a KW_OUT parameter is specified, use
				   0 for the type, and KW_OUT | KW_ZERO for
				   the flags.  The value field will either
				   contain NULL or the pointer to the variable.
				   */
#define KW_ZERO (1 << 14)	/* If set, zero the parameter before parsing
				   the keywords.  I.e. if this bit is set,
				   and the parameter is not specified, the
				   value will always be 0. */
#define KW_VALUE (1 << 15)
/* If this bit is set and the keyword is present, and its value
   is non-zero, the low 12 bits of this field will be inclusive 'or'ed
   with the longword pointed to by KW_PAR.value.
   Be sure that the type field contains TYP_LONG.  The largest value
   that may be specified is (2^12)-1.  Negative values are not allowed.
   For example, if the KW_PAR struct contains:

   "DEVICE", TYP_LONG, 1, KW_VALUE | 4 | KW_ZERO, NULL, &(char *) xxx,
   "NORMAL", TYP_LONG, 1, KW_VALUE | 3, NULL, &(char *) xxx,

   then xxx will contain a 3 if /NORMAL, or NORMAL = (expr) is
   present, a 4 if /DEVICE is present, 7 if both are set, and 0 if neither.
   KW_ZERO can also be used in combination with this flag, use it only
   once for each KW_PAR.value.
   */

#define KW_VALUE_VALUE ((1 << 12) -1)	/* Mask for value part */

/* Use KW_FAST_SCAN as the first element of the keyword array if
   there are more than approximately 5 or 10 elements in the keyword
   array.  The KW_PAR structure defined by this macro is used to
   point to a list of elements to zero, and speeds processing of long
   keyword lists.  NEVER touch the contents of this structure.
   */
#define KW_FAST_SCAN { "", 0,0,0,0,0 }

typedef struct {
  char *keyword;		/* ^ to Keyword string, NULL terminated.
				   A NULL keyword string pointer value
				   terminates the keyword structure.  Strings
				   must be UPPER case and in LEXICAL order .*/
  UCHAR type;			/* Type of data required.  For scalars the
				   only allowable types are TYP_STRING,
				   TYP_LONG, TYP_FLOAT and TYP_DOUBLE.
				   For arrays, this may be any simple type
				   or 0 for no conversion. */
  UCHAR mask;			/* Enable mask.  This field is AND'ed with
				   the mask field in the call to GET_KW_PARAMS,
				   and if the result is non-zero the keyword
				   is used.  If it is 0, the keyword is
				   ignored.  */
  unsigned short flags;		/* Contains  flags as described above */
  int *specified;		/* Address of int to set on return if
				   param is specified.  May be null if this
				   information is not required. */
  char *value;			/* Address of value to return.  In the
				   case of arrays,  this value points to
				   the KW_ARRAY_DESC structure for the data
				   to be returned.
				   */
} KW_PAR;

typedef struct {		/* Descriptor for array's that are returned */
  char *data;			/* Address of array to receive data. */
  int nmin;			/* Minimum # of elements allowed. */
  int nmax;			/* Maximum # of elements allowed. */
  int n;			/* # present, (Returned value). */
} KW_ARRAY_DESC;


#define KW_CLEAN_ALL 0		/* Codes for kw_cleanup, clean all strings */
#define KW_MARK 1		/* Mark string stack before calling get_
				   kw_params.  */
#define KW_CLEAN 2		/* Clean strings created since last call
				   with KW_MARK. */

#endif	/* keyword_DEF */




/***** Definitions from os *****/

#ifndef os_DEF
#define os_DEF

/* Structure passed to get_user_info() */
typedef struct {
     char *logname;			/* Users login name */
     char host[64];			/* The machine name */
     char wd[MAX_PATH_LEN];		/* The current directory */
     char date[25];			/* The current date */
   } USER_INFO;

#ifdef VMS
#include <descrip.h>
#endif

#endif	/* os_DEF */




/***** Definitions from pout *****/

#ifndef pout_DEF
#define pout_DEF

/*** Mask values for flags argument to pout() ***/
#define POUT_SL		1	/* Start on a new line */
#define	POUT_FL		2	/* Finish current line */
#define POUT_NOSP	4	/* Don't add leading space */
#define POUT_NOBREAK	8	/* Don't start a new line if too long */
#define POUT_LEADING	16	/* Print leading text at start of line */

/*** Structure for control argument to pout() ***/
typedef struct pout_control {
  int unit;			/* LUN of open file */
  int curcol;			/* Current output column */
  int wrap;			/* # chars at which buf should flush */
  char *leading;		/* String to output at start of each line */
  int leading_len;		/* Length of leading w/o terminating null */
  char *buf;			/* ^ to output buffer. Must be max_len chars */
  int max_len;			/* Length of buffer */
} POUT_CNTRL;

#endif	/* pout_DEF */




/***** Definitions from raster *****/

#ifndef raster_DEF
#define raster_DEF


/*** Allowed values for dither_method field of RASTER_DEF struct ***/
#define DITHER_REVERSE		0 /* "Un-dither" back to bytes */
#define DITHER_THRESHOLD	1 /* Threshold dithering  */
#define DITHER_FLOYD_STEINBERG	2 /* Floyd Steinberg method */
#define DITHER_ORDERED		3 /* Ordered dither */

/* Values for flags field: */
#define DITHER_F_WHITE 1	/* Device has white background, dithering
				   module then sets the black bits. */

/*** Convenience values for the bit_tab array of RASTER_DEF struct ***/
#define RASTER_MSB_LEFT { 0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02, 0x01 }
#define RASTER_MSB_RIGHT { 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80 }

typedef struct {		/* Information that characterizes a raster */
  UCHAR *fb;			/* Address of frame buffer */
  int nx, ny;			/* Size of frame buffer in pixels */
  int bytes_line;		/* # of bytes per scan line, must be whole. */
  int byte_padding;		/* Pad lines to a multiple of this amount,
				   Must be a power of 2.  */
  int dot_width;		/* The length of a dot for the linestyles.
				   Default = 1. */
  int dither_method;		/* Dithering method code. */
  int dither_threshold;		/* Threshold value for threshold dither */
  UCHAR bit_tab[8];		/* Table of set bits, bit_tab[0] is leftmost,
				   bit_tab[7] is right most. */
  int flags;			/* Raster flags, see above */
} RASTER_DEF;

#endif	/* raster_DEF */




/***** Definitions from rline *****/

#ifndef rline_DEF
#define rline_DEF

/**** Flags to OR together for options parameter to rline() ****/
#define RLINE_OPT_NOSAVE	1   /* Don't save in recall buffer */
#define RLINE_OPT_NOJOURNAL	2   /* Don't journal */
#define RLINE_OPT_JOURCMT	4   /* Put a '; ' at start in journal */
#define RLINE_OPT_NOEDIT	8   /* Like (!EDIT_INPUT = 0) for one call */

/**** Flags for the rline_state global variable ****/
#define RLINE_STATE_ACTIVE	1   /* rline() sets this bit when it has the
				       terminal in a special state. */
#define RLINE_STATE_NOSAVE	2   /* Interpreter routines set this bit to
				       prevent rline() from putting the last
				       command in the recall buffer. */
#define RLINE_STATE_NOJOURNAL	4   /* Interp routines set this bit to prevent
				       rline() from journaling the last cmd */

#endif	/* rline_DEF */




/***** Definitions from sysnames *****/

#ifndef sysnames_DEF
#define sysnames_DEF
typedef struct {		/* System function definition */
  FUN_RET funct_addr;		/* Address of function, or procedure.  */
  char *name;			/* The name of the function */
  UCHAR arg_min, arg_max;	/* Min & maximum argument count */
} SYSFUN_DEF;

#define KW_ARGS 128		/* Bit set in argmin indicating kw's allowed */


/* Structure used for sysv_version global variable */
typedef struct {
  STRING arch;			/* Machine architecture */
  STRING os;			/* Operating System */
  STRING release;		/* Software release */
} SYS_VERSION;

#endif	/* sysnames_DEF */




/***** Definitions from sysvars *****/

#ifndef sysvars_DEF
#define sysvars_DEF
/* Structure of system variable descriptors */
typedef struct sysvardef {	/* System variable definition struct: */
  char name[MAXIDLEN + 1];	/* Name of sysvar */
  VARIABLE var;			/* Variable definition */
  char *address;		/* Address of actual value */
} SYSVARDEF;

#endif				/* sysvars_DEF */




/***** Definitions from ur_main *****/

#ifndef ur_main_DEF
#define ur_main_DEF

/* Define action codes for ur_main() */
#define MAIN_NORMAL 1		/* Run as a normal main program */
#define MAIN_EXECUTE 2		/* Execute strings & return */
#define MAIN_EXIT 3		/* Clean up and return */

#endif	/* main_DEF */





/* Forward declarations for all exported routines and data */

#ifdef DO_PROTO                         /* Ansi prototypes */

EXTERN VPTR ur_bytscl(int argc, VPTR *argv, char *argk);
EXTERN char *ur_malloc(unsigned int size, int action);
EXTERN int ur_free(char *p, int action);
EXTERN void idl_rgb_to_hsv(UCHAR *r, UCHAR *g, UCHAR *b, float *h,
	float *s, float *v, int n);
EXTERN void rgb_to_hls(UCHAR *r, UCHAR *g, UCHAR *b, float *h, float *l,
	float *s, int n);
EXTERN long long_scalar(REGISTER VPTR p);
EXTERN double double_scalar(REGISTER VPTR p);
GLOBALREF DEVICE_DEF *dev_list[];
EXTERN char *make_temp_array(int type, int n_dim, long dim[], int init,
	VPTR *var);
EXTERN void ez_call(int argc, VPTR argv[], EZ_ARG arg_struct[]);
EXTERN void ez_call_cleanup(int argc, VPTR argv[], EZ_ARG arg_struct[]);
EXTERN char *idl_filepath(char *pathbuf, char *file, char *ext,
	int nsubdir, char **subdir);
GLOBALREF TERMINFO term;
EXTERN int ensure_file_status(int action, int unit, int flags);
EXTERN void open_file(int argc, VPTR argv[], char *argk, int access_mode,
	int extra_flags);
EXTERN void close_file(int argc, VPTR argv[], char *argk);
EXTERN void flush_file_unit(int unit);
EXTERN void get_lun(int argc, VPTR argv[]);
EXTERN void free_lun(int argc, VPTR argv[]);
EXTERN int unit_eof(int unit);
EXTERN void get_file_stat(int unit, FILE_STAT *stat_blk);
EXTERN set_file_close(int unit, int allow);
GLOBALREF char *oform[];
GLOBALREF int oform_len[];
GLOBALREF long type_size[];
GLOBALREF char *type_name[];
GLOBALREF PLOT_COM plot_com;
GLOBALREF UCHAR color_map[];
EXTERN void polyfill_sfw(int *x, int *y, int n, POLYFILL_ATTR *s);
EXTERN double graph_text(POINT *p, ATTR_STRUCT *ga, TEXT_STRUCT *a,
	char *text);
EXTERN int get_kw_params(int argc, VPTR *argv, REGISTER char *argk,
	KW_PAR *kw_list, VPTR *plain_args, int mask);
EXTERN void keyword_cleanup(int fcn);
EXTERN void message(int code,...);
EXTERN void vms_message(int code,...);
EXTERN void ur_wait(int argc, VPTR argv[]);
EXTERN void declare_exit_handler(PRO_PTR proc);
EXTERN void get_user_info(USER_INFO *user_info);
EXTERN int get_kbrd(int should_wait);
EXTERN int get_kbrd(int should_wait);
EXTERN void terminal_raw(int to_from, int fnin);
EXTERN void terminal_raw(int to_from, int fnin);
EXTERN int get_kbrd(int should_wait);
EXTERN void pout( POUT_CNTRL *control, ...);
EXTERN void pout_raw(int unit, char *buf, int n);
GLOBALREF char *dither_method_names[];
EXTERN void raster_draw_thick(POINT *p0, POINT *p1, ATTR_STRUCT *a,
	PRO_PTR routine, int dot_width);
EXTERN void raster_polyfill(int *x, int *y, int n, POLYFILL_ATTR *p,
	RASTER_DEF *r);
EXTERN void raster_draw(POINT *p0, POINT *p1, ATTR_STRUCT *a,
	RASTER_DEF *r);
EXTERN void dither(UCHAR *data, int ncols, int nrows, RASTER_DEF *r,
	int x0, int y0, TV_STRUCT *secondary);
EXTERN void bitmap_landscape(RASTER_DEF *in, RASTER_DEF *out, int y0);
GLOBALREF int rline_state;
EXTERN char *idl_rline(char *s, int n, int unit, FILE *stream,
	char *prompt, int opt);
EXTERN void logit(char *s);
EXTERN void dup_str(REGISTER STRING *str, REGISTER long n);
EXTERN void del_str(STRING *str, long n);
EXTERN void str_store(STRING *s, char *fs);
EXTERN void str_ensure_length(STRING  *s, int  n);
EXTERN VPTR ret_str_as_STRING(char *s);
GLOBALREF SYS_VERSION sysv_version;
GLOBALREF char *program_name;
GLOBALREF char *program_name_lc;
GLOBALREF STRING sysv_dir;
GLOBALREF STRING ur_err_string;
GLOBALREF STRING ur_syserr_string;
GLOBALREF long err_code;
GLOBALREF long error_code;
GLOBALREF long *syserror_codes;
GLOBALREF long sys_order;
EXTERN int add_system_routine(SYSFUN_DEF *defs, int is_function,
	int cnt);
GLOBALREF short tapechl[];
EXTERN VPTR gettmp(void);
EXTERN void deltmp(REGISTER VPTR p);
EXTERN void freetmp(REGISTER VPTR p);
EXTERN char *getscratch(VPTR *p, long n_elts, long elt_size);
EXTERN VPTR basic_type_conversion(int argc, VPTR argv[],
	REGISTER int type);
EXTERN VPTR byte(int argc, VPTR argv[]);
EXTERN VPTR fix(int argc, VPTR argv[]);
EXTERN VPTR lng(int argc, VPTR argv[]);
EXTERN VPTR flt(int argc, VPTR argv[]);
EXTERN VPTR dbl(int argc, VPTR argv[]);
EXTERN VPTR ur_complex(int argc, VPTR argv[]);
EXTERN VPTR string(int argc, VPTR argv[], char *argk);
EXTERN int ur_main(int action, int argc, char *argv[]);
EXTERN int bail_out(int stop);
EXTERN void var_copy(REGISTER VPTR src, REGISTER VPTR dst);
EXTERN void store_scalar(VPTR dest, int type, ALLTYPES *value);
EXTERN VPTR get_var_addr1(char *name, int ienter);
EXTERN VPTR get_var_addr(char *name);
EXTERN VPTR import_array(int n_dim, long dim[], int type, UCHAR *data);
EXTERN void delvar(REGISTER VPTR var);

#else                                   /* Non-Ansi prototypes */

EXTERN VPTR ur_bytscl();
EXTERN char *ur_malloc();
EXTERN int ur_free();
EXTERN void idl_rgb_to_hsv();
EXTERN void rgb_to_hls();
EXTERN long long_scalar();
EXTERN double double_scalar();
GLOBALREF DEVICE_DEF *dev_list[];
EXTERN char *make_temp_array();
EXTERN void ez_call();
EXTERN void ez_call_cleanup();
EXTERN char *idl_filepath();
GLOBALREF TERMINFO term;
EXTERN int ensure_file_status();
EXTERN void open_file();
EXTERN void close_file();
EXTERN void flush_file_unit();
EXTERN void get_lun();
EXTERN void free_lun();
EXTERN int unit_eof();
EXTERN void get_file_stat();
EXTERN set_file_close();
GLOBALREF char *oform[];
GLOBALREF int oform_len[];
GLOBALREF long type_size[];
GLOBALREF char *type_name[];
GLOBALREF PLOT_COM plot_com;
GLOBALREF UCHAR color_map[];
EXTERN void polyfill_sfw();
EXTERN double graph_text();
EXTERN int get_kw_params();
EXTERN void keyword_cleanup();
EXTERN void message();
EXTERN void vms_message();
EXTERN void ur_wait();
EXTERN void declare_exit_handler();
EXTERN void get_user_info();
EXTERN int get_kbrd();
EXTERN int get_kbrd();
EXTERN void terminal_raw();
EXTERN void terminal_raw();
EXTERN int get_kbrd();
EXTERN void pout();
EXTERN void pout_raw();
GLOBALREF char *dither_method_names[];
EXTERN void raster_draw_thick();
EXTERN void raster_polyfill();
EXTERN void raster_draw();
EXTERN void dither();
EXTERN void bitmap_landscape();
GLOBALREF int rline_state;
EXTERN char *idl_rline();
EXTERN void logit();
EXTERN void dup_str();
EXTERN void del_str();
EXTERN void str_store();
EXTERN void str_ensure_length();
EXTERN VPTR ret_str_as_STRING();
GLOBALREF SYS_VERSION sysv_version;
GLOBALREF char *program_name;
GLOBALREF char *program_name_lc;
GLOBALREF STRING sysv_dir;
GLOBALREF STRING ur_err_string;
GLOBALREF STRING ur_syserr_string;
GLOBALREF long err_code;
GLOBALREF long error_code;
GLOBALREF long *syserror_codes;
GLOBALREF long sys_order;
EXTERN int add_system_routine();
GLOBALREF short tapechl[];
EXTERN VPTR gettmp();
EXTERN void deltmp();
EXTERN void freetmp();
EXTERN char *getscratch();
EXTERN VPTR basic_type_conversion();
EXTERN VPTR byte();
EXTERN VPTR fix();
EXTERN VPTR lng();
EXTERN VPTR flt();
EXTERN VPTR dbl();
EXTERN VPTR ur_complex();
EXTERN VPTR string();
EXTERN int ur_main();
EXTERN int bail_out();
EXTERN void var_copy();
EXTERN void store_scalar();
EXTERN VPTR get_var_addr1();
EXTERN VPTR get_var_addr();
EXTERN VPTR import_array();
EXTERN void delvar();

#endif                                  /* Non-Ansi prototypes */

#endif	/* export_X */

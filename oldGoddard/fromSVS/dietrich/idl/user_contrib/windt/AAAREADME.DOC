This directory contains IDL procedures and functions contributed by
David Windt of AT&T Bell Laboratories.

A brief summary of the routines follows:
------------------------------------------------------------------------------
annotate      
             Use the mouse to label a plot.
chisqr        
             Compute the Chi Square statistic of a function and a fit
             to the function.  ChiSquare=TOTAL((Y-Yfit)^2/SigmaY^2)
clear         
             clear the screen
cont_image    
             Overlay an image and a contour plot.
contour_rms   
             Determine the rms of the contour lines associated with an
             image, and create a contour/image plot showing the rms values.
curfit        
             Non-linear least squares fit to a function of an
             arbitrary number of parameters.
             Function may be any non-linear function where
             the partial derivatives are known or can be approximated.
dlib          
             Extract the documentation template of one or more procedures.
eplot         
             Plot a vector with error bars.
erom          
             Read one or more variables from a file written by MORE.
errorf_fit    
             fit y=f(x) where:
             f(x) = a0*errorf((x-a1)/a2))+a3+x*a4
expo_fit      
             Fit y=f(x) where:
             F(x) = a0*exp(-abs(x-a1)/a2)+a3
             a0 = height of exp, a1 = center of peak, a2 = 1/e width,
             Estimate the parameters a0,a1,a2,a3 and then call curfit.

fmovie       	Show a cyclic sequence of images stored in a 3D array 
             with axes drawn and labeled.
fractal_fit   
             Fit y=f(x) where:
             F(x) = a0/(x^a1) [+a2]
             Estimate the parameters a0,a1[,a2] and then call curfit.
fwhm          
             Measure the full-width-half-max of a region of curve that
             has been previously plotted.
fwhm_label    
             Label the previously measured full-width-half-max of a
             region of curve that has been previously plotted.
gaus2d_ave    
             Compute the gaussian-weighted average of a 2-D function,
             averaged along the column direction.
gaus_convol   
             Convolve a function with a gaussian.
gauss_fit     
             fit y=f(x) where:
             f(x) = a0*exp(-z^2/2) + a3
             and z=(x-a1)/a2
             a0 = height of gaussian, a1 = center of gaussian, a2 = 1/e width,
             a3 = background.
             Estimate the parameters a0,a1,a2,a3 and then call curfit.
gaussexpo_fit 
             Fit y=f(x) where:
             F(x) = a0*exp(-z^2/2)+a3*exp(-abs(x-a4)/a5)+a6
             and z=(x-a1)/a2
             a0 = height of gaussian, a1 = center of gaussian, a2 = 1/e width of
             gaussia, a3 = height of exponential, a4 = center of exponential,
             a5 = 1/e width of exponential, a6=background.
             Estimate the parameters a0,a1,a2,a3,a4,a5,a6 and then call curfit.
get_peak      
             Get the local maximum of a previously plotted curve.
get_pt        
             Digitize a point on a plot and find the closest point
             on the curve.
get_roi       
             Get a region of interest of a previously plotted curve.
inter_prof    
             Determine the average profile and its derivative along
             the vertical (horizontal) direction for a rectangular
             region of an image, and optionally compute the FWHM of
             features in the derivative.
             
label_data   	Label lines and plotting symbols on a plot. 
             The default is to draw lines of different linestyles
             and colors, putting a label next to each line.

lpq          	Spawn a process which executes the Unix 'lpq' command. 

lprint       	Close a PostScript file and print it. 

ls           	Spawn a process which executes the Unix 'ls' command. 

make_arrow   	Put a label and a line on a plot. 

make_latex_tbl
             Create a LaTeX format table.
mk4quads      
             Convert an array z(x,y) defined for x,y > 0 to
             znew(x,y) defined for all x,y.
more          
             Print one or more variables on the screen using the MORE keyword.

movie        	Show a cyclic sequence of images stored in a 3D array. 

movie2       	Show a cyclic sequence of 2 sets of images stored in 2 
             3D arrays.
movie3       	Show a cyclic sequence of 3 sets of images stored in 3 
             3D arrays.
oeplot        
             Overplot a vector with error bars.
oned2twod     
             Create a two-dimensional array whose elements are equal to the
             radial distance from the origin, given a one-dimensional vector.
rd_matrix     
             Read a data file created using MATRIX.
rd_mca_data   
             Read mca data files written with ~icl/norland5700/mca
read_contf    
             Read the contours defined by a path file created by CONTOUR.
recroi       	Define a rectangular region of interest of an image 
             using the image display system and the cursor/mouse.
rotation      
             Rotate two vectors by a specified amount.
shift_plot    
             Interactively slide a previously plotted array using
             the mouse.
show_ct       
             Make a window and show the first 32 colors in the current
             color table.
sinchsquare_fit
             Fit y=f(x) where:
             F(x) = a0*( sin(a1*(x-a2))/(a1*(x-a2)) )^2 + a3
             Estimate the parameters a0,a1,a2,a3 and then call curfit.
small_window  
             Make a 500x400 window.
sp             Execute Set_plot, and some handy default settings. 
symbols       
             Create custom plotting symbols
wdump         
             Dump the contents of the current plotting window to a PostScript
             printer.
whardcopy      Use WMENU to get Yes/No response to hardcopy query. 
write_contf    
             Create a contour-path data file from an image.
zoom_plot     
             Plot y versus x for a region of interest interactively
             defined by the user.
zrttozxy        
             Convert a 2d array from polar (r,theta) coordinates to
             cartesian (x,y) coordinates.

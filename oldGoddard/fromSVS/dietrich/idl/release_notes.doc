                            IDL Release Notes

   This document lists bug fixes as well as new features
   and enhancements of existing features to IDL starting with
   version 2.2.0. For earlier notes, see the files release_notes_2_0.doc
   and release_notes_2_1.doc.

------------------------------------------------------------------------------

8/6/91
	Version 2.2.0

8/8/91
	For IDL on Sun machines, IDL now sets the default graphics
	device to  SUN if SunView is running, otherwise it is set to 
	X Windows.

8/13/91
	Fixed bug in CALL_EXTERNAL that allowed the called external
	program to overwrite string constants in IDL in certain cases.
	This would manifest itself by causing more than one string
	variable to change value when an external program returned a
	string.

8/15/91
	Fixed a bug that occurred when reading (TVRD) from an X
	window that had no backing storage and that was not
	entirely on the screen.  In this case, an error message
	would result, no data was transferred, and sometimes a
	dump occurred.  Now, the portion of the window that is
	on the screen is correctly read.  If no backing storage
	is present (RETAIN is not set to 2) then undefined data
	result from reading the non-visible areas.

8/21/91
	New keyword to WIDGET_EVENT called YIELD_TO_TTY. THIS
	KEYWORD IS NOT SUPPORTED UNDER VMS. If YIELD_TO_TTY is
	set and the user enters input on stdin, WIDGET_EVENT
	will return immediately with the ID field of the returned
	event structure set to 0.

9/5/91
	Enhanced PLOTS and XYOUTS to accept either a scalar
	or vector for the COLOR keyword, allowing polylines
	and multiple strings to be drawn with different colors
	in the same call.

	
9/7/91
	Formatted input would not work properly on negative double
	precision numbers. For example, given a file with the
	line "-4.1d7", the following statements:
		openr, 1, 'test'
		b = 0d
		readf, 1, b
		print, b
	would print the result -4.1 instead of -41000000.0.
	This has been fixed.

	Under VMS, restoring a version 1 Save/Restore file
	would cause the spurious error:
		% Internal error: Unknown global junk in interpreter().
	This error did not cause the restore operation to fail, and
	can be safely ignored. It has been fixed. This bug may have been
	introduced while fixing the bug reported on 2/28/91 above, which
	makes 2.1.0 the version where it probably first appeared.

9/16/91
	POLYFILLV now returns a scalar with a value of -1, and an
	informational message when given a polygon that contains
	no points.

	Added a Z-buffer device driver.  This a pseudo device that
	draws 2D or 3D graphics in a buffer contained in
	memory.  This driver implements the classic Z buffer
	algorithm for hidden surface removal.  Although
	primarily used for 3D graphics, the Z-buffer driver
	can be used to create 2D objects in a frame buffer in
	memory.  The resolution of this device can be set by
	the user.

	Enhanced the TOTAL function by adding the ability to
	sum over a given dimension of multi-dimensional arrays.
	TOTAL is now defined as TOTAL(Array [, Dimension]), where
	Dimension is the dimension over which to sum.  The result
	is an array of one less dimension than the operand. For
	example, if Array has the dimensions (N1, N2, N3), and
	Dimension is 2, the result has dimensions (N1, N3) and
	element (I,J) is the sum of the elements Array(I, *, J).

	Added anonymous structures.  This allows structures to be
	defined without names, removing the restriction that all
	structures with a given name have the exactly the same
	tag definitions.  To define an anonymous structure:
		a = { Tag1: v1, Tag2: v2, ...., TagN, vN}
	All other structure operations remain unchanged.  The only
	restriction is that an anonymous structure may not contain
	another anonymous structure.

	The CALL_EXTERNAL Function now works with HP9000 Series 700
	machines.  See the note "hp_call_external.doc" in the 
	notes directory. LINKIMAGE does not work yet.

	Enhanced the FFT function by adding the ability to do "in
	place" transforms.   If the operand is a variable and the
	OVERWRITE keyword is set, the transform is done without
	copying the data.  For example:
		A = FFT(A, 1, /OVERWRITE)
	computes the FFT of A without allocating additional memory
	or moving the data.

9/20/91
	Widget changes:
	    - Internal changes make event processing and widget ID
	      verification much more efficient.
	    - The PEEK keyword to WIDGET_EVENT has been removed. It
	      prevented upcoming enhancements and did not provide
	      especially useful functionality.
	    - CLEAR_EVENTS keyword to WIDGET_EVENT now works on
	      entire subtree instead of just that widget.
	    - A new procedure named WIDGET_INFO has been added for getting
	      information about widgets. It accepts only keyword arguments:
		VALID - Returns a BYTE var of the same structure as WIDGET_ID
			full of 1's and 0s depending on whether the widget
			IDs are valid or not.
		VERSION - Gives back struct giving the toolkit name and
			version. This allows user library procedures to
			work around differences in toolkit versions.
		WIDGET_ID - Input scalar or array of widget IDs. Used
			by VALID.
	    - If you add a new widget to a realized parent, the new
	      child is automatically realized also. This closed a hole
	      in the widget initialization process.
	    - The "font not found" error from FONT keyword to the
	      widget creation functions no longer halts
	      execution. An error message is issued and things continue
	      with the default font.
	    - The MAP keyword to WIDGET_CONTROL did not work reliably. The
	      following changes and enhancements make it more useful:
		[] You can set mapping state prior to realization.
		[] WIDGET_BASE() now accepts a keyword named MAP that works
		   the same way as the WIDGET_CONTROL MAP keyword. This allows
		   setting the initial mapping state with a single statement.
		[] WIDGET_CONTROL, /MAP only works for base widgets.
		   If the widget is not a base, IDL goes up the widget
		   tree until one is found and that widget is used.
	    - Fixed a bud in the OPEN LOOK implementation (Sun workstations)
	      in which an exclusive base would not allow all of its toggle
	      buttons to be unset. The base would force the first button to
	      be automatically selected in such situations, leading to
	      unexpected results.
	    - The note for 6/25/91 above describes a problem where
	      blinking text widget cursors would cause all IDL widget
	      applications to freeze after some amount of time.
	      It turns out that OPEN LOOK suffers from the same problem.
	      Therefore OPEN LOOK text widgets now have their cursors
	      set to not blink, and the hanging no longer occurs.

	      If you are running an older version of IDL, the following line
	      in your ~/.Xdefaults file can be used to solve the problem:

		Idl*blinkRate:       0

9/23/91
	New keyword named GROUP_LEADER for WIDGET_CONTROL and
	the widget creation functions. This keyword allows setting
	up associations between widgets such that killing one causes
	the associated ones to be killed also. The XMANAGER user library
	procedure currently does this entirely in user level IDL code
	but in a future release it will be modified to use the new
	built in version.

9/27/91
	Added a new function TEMPORARY(V), which returns the value
	of the variable V as a temporary variable, and sets the
	variable V to undefined.  This function may be used to
	conserve memory when operating with large arrays, as it
	avoids making a new copy of temporary results.  For example,
	assume the variable A is a large array.  The statement:
		A = A + 1
	creates a new array for the result of the addition, 
	then assigns it to A, and then frees the old allocation
	of A.  Total storage required is twice the size of A.
	The statement:
		A = TEMPORARY(A) + 1
	requires no additional space.

10/1/91
	Version 2.2.1

10/21/91
		Under SunOS, the SGI, and presumably other System V
		conforming Unix implementations, the following satatements:

		OPENW,10,'tstr.dat'
		WRITEU,10,INDGEN(25)
		CLOSE,10

		dat = INTARR(10)
		OPENU, 11,n'tstr.dat'
		POINT_LUN, -11, pt
		PRINT, 'position = ', pt

		READU, 11, dat
		POINT_LUN, -11, now
		PRINT, 'position = ', now

		POINT_LUN, 11, pt
		POINT_LUN, -11, pt
		PRINT, 'position = ', pt

		WRITEU, 11, dat
		POINT_LUN, -11, pt
		PRINT, 'position = ', pt

		READU, 11, dat

	would fail on the final READU with the following error:

		% READU: Unable to push character back. Unit: 11
			 File: tstr.dat

	This error does not occur under older (true BSD I/O) SunOS
	or current Ultrix systems. It occurs in the final READU
	when it checks the files EOF status. The check for EOF was 
	rewritten differently to avoid the problem.

11/7/91
	- When using the STRING function with the format keyword, there
	  was a limit of 128 "lines" (array elements) in the array result.
	  Any additional lines were quietly dropped from the result.
	  Two changes have been made:

		- The limit has been increased to 256.

		- An error message informing the user of the output
		  truncation is issued. Execution continues as before.

	- When generating PostScript graphics in landscape mode, the
	  %%BoundingBox comments were wrong, although they were
	  correct in portrait mode. It now generates the correct
	  BoundingBox information in both cases.

11/8/91
	Repaired a bug in SAVE,/COMM in which variables in a common
	block would have their names and values switched on RESTORE if the
	variable names in the COMMON block were not in alphabetical order.

11/10/91
	Added four new keywords to the BYTEORDER procedure:
		DTOXDR - Convert double precision floating to XDR format.
		FTOXDR - Convert single precision floating to XDR format.
		XDRTOD - Convert XDR data to double precision floating.
		XDRTOF - Convert XDR data to single precision floating.

	These keywords allow translating data in memory between the
	machines native binary floating point format and the portable
	XDR format (which happens to be the same as IEEE floating).

	------------
	VAX WARNING:
	------------
	Vaxes generate an illegal instruction trap when the CPU
	encounters an illegal floating point values. Under VMS, the
	illegal value is automatically replaced with zero and execution
	continues. Under VAX Ultrix, the IDL program is terminated
	by the operating system.

	Some of the bit patterns resulting from conversion to XDR format
	are illegal VAX floating values. The resulting interactions
	can be difficult to diagnose. For example. the following
	series of statements work as expected:

		a = 1.0
		BYTEORDER, a, /FTOXDR
		BYTEORDER, a, /XDRTOF
		PRINT, a
 			1.0

	However, the following on a VMS system gives an unexpected result:

		a = 1.0
		BYTEORDER, a, /FTOXDR
		PRINT, a	; Illegal floating value is replaced by zero!
 			0
		BYTEORDER, a, /XDRTOF
		PRINT, a	; The wrong answer results.
 			0
	On an Ultrix system, IDL would have crashed.

	This problem can be avoided in two ways:

	    1) Avoid any operation that would result in the
	       data in the XDR format variable having its data
	       interpreted as floating point data. This is why the
	       first example above worked.

	    2) Keep the binary data in an integer array. For instance:

		a = LONG(1.0, 0, 1)
		BYTEORDER, a, /FTOXDR
		PRINT, a
			32831
		BYTEORDER, a, /XDRTOF
		PRINT, FLOAT(a, 0, 1)
			1.0
11/13/91
	The B argument to LUBKSB is supposed to return a solution vector.
	This was not happening if the input value of B did not have the
	same type as the desired output. This has been fixed.

11/21/91
	Fixed a bug in GET_KBRD(1) that only occured on	HP 9000/700
	series machines.  After a several second delay, it would
	return 255.

11/26/91
	The VMS IDL timer module was using the default event flag (0)
	for blocking timer requests (such as in the WAIT procedure)
	and explicitly allocating an event flag for non blocking requests
	(such as those made by the X Windows driver). This turned out to
	cause problems for code linked with LINKIMAGE that was also using 
	the default event flag. IDL has been modified to always allocate
	an exclusive event flag.

	On some Unix systems (DEC Ultrix and SGI IRIX but not SunOS)
	the following statements:
		IDL> READ, f & PRINT, f
		: 1087.87d 8.9d-2
	would result in the output:
		1.08787e+11
	This incorrect result was due do the C standard library sscanf(3)
	function continuing past the 'd' in 1087.87d looking for the
	exponent. IDL's use of sscanf(3) has been modified to avoid this
	case.

11/26/91
	Fixed a bug in COLOR_CONVERT, ...., /RGB_HLS (converting from
	RGB to HLS).  Incorrect results for S were returned.

	Increased the number of default tick marks when the style
	keyword is set to produce an exact range.  This fixes the
	problem of axes being produced when an exact range is set
	without enough tick marks to be properly interpreted.

	Fixed a bug in the Z-buffer driver that would occasionally
	insert 0's into the output image when polyfilling images
	(pattern specified) with interpolation turned on.

	Fixed a bug in the POLYSHADE function that prevented it
	from working when more than 65535 vertices were present.

12/10/91
	Xwindows driver: There is now no IDL imposed limit on the
	number of active windows or pixmaps.  Users should only explicitly
	create windows 0 to 31.  Window indices of 32 and higher are
	allocated by the WINDOW procedure when the FREE keyword is
	specified.

	Contour labels could be drawn outside the plot window
	when the plot window range was less than the range of
	the data being contoured.  Fixed.

12/26/91
	Motif Widgets: Added the ability for draw widgets to
	have a TrueColor or DirectColor visual type.

12/31/91
	Rewrote the low level binary operator routines (+,-,*,/,AND,
	OR, EQ, NE, LE, LT, GE, GT) and the temporary variable
	routines to maximize speed.  Performance for many basic
	operations has improved, most dramatically on VAX machines
	and by a fair amount on all other machines.  Improvement is
	greatest on VAXes because of their inefficient hardware level
	CALL instruction.  The CALL instructions on RISC machines is
	much more efficient, so the improvement is not so dramatic.

	One metric, the simple program:

		FOR i = 0L, 100000 DO a = i + i + i

	required the following times:
	
	Machine		 Approximate Time required (sec)
				Before		After	% improvement

	VAX 3100		21		11	90%
	Sparc 4/65 (1+)		5.0		3.6	38%
	IBM RS6000/320		4.0		3.0	33%

1/3/92
	Fixed a bug that prevented CURSOR, /CHANGE from working
	properly on draw widgets.

1/4/92
	Added a new procedure named READS that works like READ or READF
	except that the input is taken from an IDL variable instead of a file.
	This is similar to the standard C sscanf(3) function, and serves
	as a counterpart to the STRING function.

1/10/92
	Version 2.2.2

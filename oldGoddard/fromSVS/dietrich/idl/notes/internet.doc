Through the generosity of the University of Colorado, we (Research
Systems, Inc) make the current Unix IDL distribution available on the
internet via anonymous ftp (The VMS version is available via SPAN in
the directory ORION::DISKA:[IDL.V2]. ORION has the decnet address
33814.) Sites licensed for IDL can periodically update their copy from
this source. In addition, people wishing to sample IDL can ftp a copy
of the distribution and try it out (You should note that IDL will not
run on your system for more than 7 minutes per invocation unless you
call us and obtain a copy verification key which enables the trial
installation).

1.0 General Information

1.1 Our Network Policy

	The network distribution of IDL exists primarily for two classes
	of users:

		1) People who would like a demonstration of IDL.  There 
		   are two ways to demonstrate IDL: The Quick Demo Mode 
		   (section 5.0) and the Trial Installation Mode
		   (section 5.3).

		2) Existing customers under a current software support
		   agreement who wish to update their current installation
		   with the latest release.

	If you are a licensed IDL site, but you do not have a current
	software support agreement, you are still free to make use of
	this resource to update your installation. However, we ask that
	you do it in a self sufficient manner, without calling on us
	for help.

1.2 Form Of Distribution

	Every file in this directory (with the exception of this
	README file and the unpack_idl script) is a compressed tar file.
	This means that the Unix tar(1) program was used to produce it,
	and the compress(1) program was used to reduce its size for
	transfer across the internet.

	   To save space on the distribution machine and to keep
	the size of individual tar files down to a size which can
	be easily transferred, we have broken the distribution into
	different files. The machine specific instructions below
	explain which files you will need to take and how to unpack them.

1.3 The Files

	Here is a brief description of the files in the distribution:

	base.tar.Z - This file contains those files that are
		found in every IDL distribution. This includes
		things like the user library.
	bin.hp.tar.Z - The HP-UX version of IDL for the 9000 Series
		300, 400 and 700.
	bin.ibm.tar.Z - The IBM AIX version of IDL for the Risc 6000 series.
	bin.mips.tar.Z - The MIPS Risc/OS version of IDL.
	bin.sgi.tar.Z - The Silicon Graphics Iris version of IDL.
	bin.sun3.tar.Z - IDL for the Sun 3 under SunOS 4.0.* and 4.1.* .
	bin.sun4.tar.Z - IDL for the Sun 4 (SPARC) under SunOS 4.0.*
		and 4.1.* .
	bin.sunos.3.5.sun3.tar.Z - IDL for Sun 3 systems running SunOS 3.5.
		This is an old version and is no longer supported.
	bin.sunos.4.0.sun386.tar.Z - IDL for Sun 386i systems running
		SunOS 4.0.* . This is an old version and is no longer
		supported.
	bin.ultrix.tar.Z - IDL for DEC Ultrix (both Risc and VAX versions).
	images.tar.Z - Data files used by the demo programs. These files
		could logically go in base.tar.Z, but they are separate
		because the files are rather large, and since they
		rarely change, people who are updating an existing
		distribution can use the images subdirectory from
		their existing distribution. This speeds things up
		and eliminates the transfer of unneeded data
		across the internet.
	unpack_idl - A Bourne shell script (/bin/sh) that unpacks
		each compressed tar file.
	user_contrib.tar.Z - User contributed software. We distribute
		this directory, but on a strictly as is basis. RSI
		does not support this software and accepts no
		responsibility for it.
	xdemo.tar.Z - Files used by the widget demo. IDLwidgets are a
		new facility that allow the IDL programmer to create
		graphical user interfaces from IDL.

	There are also a series of tar files containing the string ".lm"
	in their names. An example is bin.sun4.lm.tar.Z. These
	contain the files used to support the network floating license
	manager for certain machines. You will need these files only
	if you are currently using floating licenses. NOTE: Trial
	copies of IDL do not require these files.

1.4 Supported Distributions

	HP 9000 Series 300, 400 (Bobcat) and 700 (Snake): This requires
                HP_UX release 7 to work properly.

        Silicon Graphics Iris 4D Series. SGI's X11
                implementation is still in development at this point
                and lacks a proper X windows window manager
                which reduces IDL's usefulness. However, it is still
                usable, and the Iris is a nice platform. IDLwidgets are
		included however, using a preliminary developers MOTIF
		library supplied by SGI.

	SunOS: We currently support the Sun3 and Sun4 (Sparc)
		architectures under version 4 and 4.1 of SunOS.

		In addition, we supply versions for Sun3 machines running
		SunOS 3.5 and 386i machines under SunOS 4. However,
		we have frozen support for these machines and will
		not be updating them in the future.

	Ultrix: We currently support DEC machines running Ultrix
		(both VAX and RISC architectures).


1.5 Installation Instructions
	We send purchasers of IDL printed installation instructions.
	The LaTeX files for these instructions can be found in the
	"misc" subdirectory of the IDL distribution once you've
	unpacked the tar files. The only difference in the
	installation procedure for those who obtain IDL across
	the internet is in how the distribution is obtained and
	unpacked. That is the subject of this document.


1.6 Locating The Distribution

1.6.1 Updating An Existing Installation
	If you are updating an existing installation, you have
	already dealt with the problem of deciding where to
	install the IDL distribution. To update your distribution
	you will need to move the old one out of the way. Assuming
	you used our recommended default of /usr/local/lib/idl,
	the following Unix commands should suffice. You should
	be logged in as root when you execute these commands:

		% cd /usr/local/lib
		% mv idl idl.old
		% mkdir idl


1.6.2 First Installation --- System Wide 
	If you are installing a trial version, simply create
	a directory for IDL where you place user accounts on your
	system. This is "/home" for SunOS, "/users" for Ultrix, etc...

	The name of the directory should be "idl". For example,
	assume that you usually put user directories in /home.
	The following Unix commands should suffice. You should
	be logged in as root when you execute these commands:

		% cd /home
		% mkdir idl


1.6.3 First Installation --- Single User
	If you are installing a trial version for your personal
	use, simply place the directory somewhere in your directory
	tree such as your login directory. Assuming you
	use your login directory, the following command should suffice:

		% cd $HOME
		% mkdir idl


2.0 Getting Tar Files Across The Internet
	The required files can be recovered via ftp over the Internet
	as follows:

        1) Use ftp to connect with the machine boulder.colorado.edu.
           The internet address for this machine is 128.138.240.1   .

        2) Log onto boulder:

                account: ftp
                password: anonymous

        3) Move to the IDL directory:

                ftp> cd pub/idl

        4) Put ftp into "binary" mode.

                ftp> binary

        5) Use the ftp "get" command to retrieve the desired file(s)

        6) Exit the ftp session using the "bye" command.


3.0 Getting Help:

	Research Systems, Inc.
	777 29th Street, Suite 302
	Boulder, CO 80303
	(303) 786-9900      (Voice)
	(303) 786-9909      (Fax)

	Email:
	    idl@boulder.colorado.edu             # Internet
	    ORION::IDL                           # SPAN
	    IDL@COLOLASP                         # Bitnet


4.0 Building The Distribution

	To build a distribution you will need to obtain the
	following files:

		- base.tar.Z
		- images.tar.Z only if you do not already have the
			images subdirectory from an existing distribution.
		- unpack_idl. You may need to make this file executable
			after you have obtained it:

				% chmod 755 unpack_idl
		- user_contrib.tar.Z only if you are interested in
			user contributed software.
		- xdemo.tar.Z if you are running IDL on a system
			that supports IDLwidgets. This file requires
			about 4 Mb of disk space when unpacked, but
			it demonstrates our new graphical user interface
			support as well as providing programming examples.
			At this time, Widgets are supported on all
			systems except Sun 3s running SunOS 4.0. Sun 3s
			running SunOS 4.1 are OK. On Suns, you need to
			have OPEN WINDOWS installed.

	In addition, you will need system dependent files:

		HP:
			- bin.hp.tar.Z

		Silicon Graphics:
			- bin.sgi.tar.Z

		Sun:
			- bin.sun3.tar.Z if you have Sun 3 systems.
			- bin.sun4.tar.Z if you have Sun 4 (SPARC) systems.
			- bin.sunos.3.5.sun3.tar.Z only if you still have
				Sun 3 machines running SunOS 3.5.
			- bin.sunos.4.0.sun386.tar.Z only if you have Sun
				386i machines.

		Ultrix:
			- ultrix.tar.Z

	Finally, if you are using the network floating license manager,
	you will need the appropriate "bin.*.lm.tar.Z" files for the
	systems you will be using IDL on.

	0) Use the suggestions in Section 1.5 to create an empty
	   directory named "idl" to hold the distribution. Move your
	   current default directory there. Depending on the location
	   you've picked, you may need to be logged on as root as you
	   execute these instructions:

	1) Move to the empty IDL directory

		% cd XXX/idl

	   where "XXX" is the path to the directory.

	2) Get the required compressed tar files from
	   across the internet as described in
	   earlier sections of this document.
		   
	3) Unpack the tar files.

		% unpack_idl

	4) If you intend to re-use the images subdirectory,
	   from your old distribution, use the Unix mv(1) command
	   to move it from your old distribution:

		% mv OLD_DIR/images .

	   where "OLD_DIR" is the path to the old distribution
	   on your system.

	5) If you are updating an old distribution, move your
	   existing validation files to the new location. For standard
	   installations:

		% mv OLD_DIR/idl.hosts .
		% mv OLD_DIR/idl.genver .

	   For network license manager installations:

		% mv OLD_DIR/license.dat .

	   If you are installing a new trial version, call us at
	   the phone number given in Section 3.0 for validation
	   instructions.

5.0 Quick IDL Demonstration Instructions

	You can run IDL without calling RSI for a trial version.  This
	will automatically put you in a Quick Demo Mode.  IDL will run
	for seven minutes at a time and can be re-run as often as needed.
	Follow the instructions for Building The Distribution (section 
	4.0 of this document) if you have not done so already.  The Quick 
	Demo Mode doesn't allow writing of files and is intended only for 
	quick demonstration of IDL.

5.1 Running The XDemo

	The XDemo requires X-windows (Openlook on Suns) and it illustrates 
	the type of applications that can be written with IDLWidgets.  These
	instructions assume you have already obtained and built the 
	distribution for version 2.1.0 or greater on your system.  To run 
	the demo, type the following:

     	Command			Explanation
     ------------------------------------------------------------------

	% cd /u/tom/idl 	Change directories to the main IDL 
				directory.  (Here we assume you have
				chosen "/u/tom/idl" as your main
				directory.)

	% ./idl_demo		Run IDL demo version.

	IDL> xdemo		Start the demo.

5.1.1 Hints On Using The XDemo

	This demo must be run while using X-windows.  Once the demo is
	running, you should see an IDL logo with buttons above it.  Choose
	examples from the Examples menu with the mouse.  For Image and Signal
	processing, try the Fourier Filtering Example.  Select Spring Demo to
	see a dynamic visualization generated with IDL.  The Widget Types
	Example demonstrates the types of Widgets currently available in IDL.
	The Animation Example displays medical animations.  Use XLoadct Color
	Table Chooser to adjust the colors and contrast used by IDL.

5.2 Another Demo

	Another IDL demonstration program illustrates some of the many ways
	that IDL can help to visualize data.  In this non-interactive demo,
	the IDL commands that generate the examples are shown.  Move the
	window you are using to the lower left of the screen so that it is not
	obscured by the demo.  Run this demo by typing the following:

	Command			Explanation
     ------------------------------------------------------------------

	% cd /u/tom/idl		Move into the IDL directory.

	% ./idl_demo		Run IDL demo version.

	IDL> .run demo		Start the other demo.

5.3 Trial Demonstration Mode

	To validate IDL for a trial period, first follow the above steps 
	(section 5.0) and make sure the Quick Demo Mode works.  You will
	then need to call Research Systems, Inc. (303-786-9900) while 
	running the genver program as described below:

	Command			Explanation
     ------------------------------------------------------------------

	% cd /u/tom/idl		Move into the IDL directory.

	% bin/genver		Run the validation program.






















							25 September 1991


Running the Sun version of IDL on a Non-OpenWindows Display Server

   IDL version 2.x on the Sun is usually run from OpenWindows. If IDL
   is run with a different X server, such as X11R4 from MIT,
   IDL Widget applications will not work correctly unless the OpenWindows
   fonts for buttons and scrollbars are available to the display server.
   This situation might occur if OpenWindows is replaced with a different
   X server or if a different machine without OpenWindows is used as the
   display server.

   This situation is complicated by the fact that different font formats
   are used by Sun (Sun X11/News), DEC (pcf) and the standard X11
   server from MIT (bdf). The process is the the same for converting
   to DecWindows or the MIT server, although the actual commands differ
   in name.

   The required fonts are supplied with OpenWindows. It is necessary to
   convert them to the correct format for your server. The procedure for
   converting the fonts to the appropriate format is described below.

   Many thanks to Dave Biron of MIT Lincoln Laboratories who came up with
   this approach and documented it.

   This description assumes that the OpenWindows distribution is installed
   in the directory OPENWINHOME. This is a Unix environment variable that
   contains the location of the root OpenWindows directory. On many
   systems, this is defined as:

		% setenv OPENWINHOME /usr/openwin

   You should make sure it is defined properly for your system before
   starting this procedure.

   On the machine running the non-OpenWindows server, we assume the
   location of the X11 installation is X11HOME. On many systems, this
   is defined as:

		% setenv X11HOME /usr/lib/X11

   You should make sure it is defined properly for your system before
   starting this procedure.


ON THE MACHINE WITH OPENWINDOWS:
--------------------------------

1. Find the fonts in the OpenWindows distribution.  They are usually in the
   $OPENWINHOME/lib/fonts directory, with names like "d12butto12.fb"
   or "d12sbarv12.fb".  The important files have extensions of ".fb"; don't
   worry about the ".ff" files.  There are 61 files whose names should match
   the list of ".snf" files shown below except for the ".fb" suffix.  Use
   the following command to list the files:

      ls $OPENWINHOME/lib/fonts/?12*.fb

   Use the command "xlsfonts" to list the actual names of the fonts:

      $OPENWINHOME/bin/xlsfonts | grep '[a-h]'12
    

2. Use the OpenWindows  utility "convertfont" to create ".bdf" format
   font files from the ".fb" files.  First make a subdirectory of the
   fonts directory, and create the new files there. This process must
   be done as superuser, unless it is done in a user's directory.

      % cd $OPENWINHOME/lib/fonts
      % mkdir bdf_fonts_for_widgets
      % cd !$
      % foreach name (../?12*.fb)
      ?   $OPENWINHOME/bin/convertfont -x -o `basename $name .fb` $name
      ? end


3. Copy the ".bdf" format font files to the machine that will run as the
   display server for IDL.  If only one machine is being used to run both
   OpenWindows and X11, skip this step.

      % cd /usr/lpp/fonts
      % mkdir openwin
      % cd !$
      % rcp mysun:"$OPENWINHOME/lib/fonts/bdf_fonts_for_widgets/*" .


ON THE MACHINE WITH X11:
------------------------

4. Convert the ".bdf format font files to the correct format for your
   server.

   FOR THE MIT SERVER:
      % cd /usr/lpp/fonts/openwin
      % foreach name (*.bdf)
      ?   echo $name
      ?   bdftosnf $name > `basename $name .bdf`.snf
      ? end

   FOR THE DecWindows SERVER:
      % cd /usr/lpp/fonts/openwin
      % foreach name (*.bdf)
      ?   echo $name
      ?   dxfc $name > `basename $name .bdf`.pcf
      ? end


5. Generate the file "fonts.dir". This file contains a line for each
   font file in the directory, and maps the actual name of each font
   to the file containing it:

   FOR THE MIT SERVER:
      % mkfontdir `pwd`

   FOR THE DecWindows SERVER:
      % dxmkfontdir `pwd`


6. Edit the "fonts.dir" file to make the font names (the second field on each
   line) match the OpenWindows font names.  On some servers, the font names
   end with "-10", "-12", or "-14", but the Sun version of IDL looks for
   fonts without that suffix.

   To make the font names in the file "fonts.dir" match the OpenWindows
   font names from step 1, simply edit the "fonts.dir" file and correct
   the names.  (It is probably possible to use a "fonts.alias" file, but
   editing the "fonts.dir" file seemed like the most simple approach.)
   For example, to use vi to remove the "-1?" suffixes, enter:

      vi fonts.dir
      :g/-..$/s///


7. Add the directory containing the fonts to the font path of the server
   and rehash the font path to add the fonts.

           xset +fp /usr/lpp/fonts/openwin
           xset fp rehash

   These "xset" commands could be placed in a user's startup file so that
   the fonts are added when the server is initialized.  On some systems,
   including the IBM RS6000 the commands could be put in the ".xinitrc"
   file.  For other systems, such as Ultrix machines, the commands should
   be put in the ".X11start" file.  On the SGI, the files were placed in
   an existing font directory, and then the "fonts.dir" file was updated
   to include the new fonts.  Use "xset q" to make sure that the font
   path has been modified as desired.  Use the same "xlsfonts" command
   shown in step one to make sure that the fonts are installed in the
   server.


ADDITIONAL NOTES
----------------

   After the fonts are installed, they can be viewed using the X11
   utility "xfd".

   The MIT X11 server can read ".bdf" format fonts but the ".snf" format
   is more compact.

   Some installations might be missing the utility "xset".  Usually the
   source can be found and compiled.  You might have to remove a call
   to XmuPrintDefaultErrorMessage to allow xset.c to compile.

   On Suns running MIT X11, many of these utility programs require that
   the environment variable LD_LIBRARY_PATH be set to the lib
   subdirectory of the X installation. For example, on the machine
   running OpenWindows, enter:
      setenv LD_LIBRARY_PATH $OPENWINHOME/lib
   On other machines which normally run X11, enter:
      setenv LD_LIBRARY_PATH /usr/lib
 
   IDL may also look for a set of pushpin bitmaps.  If your
   system has the OpenWindows distribution installed they should be available
   in $OPENWINHOME/lib/bitmaps.  Simply define the environment variable
   OPENWINHOME and the toolkits should then be able to access them. 
   On other machines which normally run X11 copy the bitmaps to a bitmaps
   subdirectory of the lib directory of the X installation.  Then enter:
      setenv OPENWINHOME $X11HOME/lib

      


LIST OF FONT FILES
------------------
The following is a list of the converted fonts and their OpenWindows font
names for Open Windows 2.0.  (It is actually the file "fonts.dir".)
Some of these may not be required, but if all are converted IDL Widgets
will run.

61
a12biluc12.snf     a12biluc
a12bluci10.snf     a12bluci
a12butto12.snf     a12butto
a12iluci12.snf     a12iluci
a12lucid10.snf     a12lucid
a12sbarh12.snf     a12sbarh
a12sbarv12.snf     a12sbarv
a12sldrh12.snf     a12sldrh
a12sldrv12.snf     a12sldrv
b12bluci10.snf     b12bluci
b12butto12.snf     b12butto
b12lucid12.snf     b12lucid
b12sbarh12.snf     b12sbarh
b12sbarv12.snf     b12sbarv
b12sldrh12.snf     b12sldrh
b12sldrv12.snf     b12sldrv
c12bluci12.snf     c12bluci
c12butto12.snf     c12butto
c12iluci12.snf     c12iluci
c12lucid12.snf     c12lucid
c12sbarh12.snf     c12sbarh
c12sbarv12.snf     c12sbarv
c12sldrh12.snf     c12sldrh
c12sldrv12.snf     c12sldrv
d12bluci14.snf     d12bluci
d12butto12.snf     d12butto
d12iluci14.snf     d12iluci
d12lucid14.snf     d12lucid
d12sbarh12.snf     d12sbarh
d12sbarv12.snf     d12sbarv
d12sldrh12.snf     d12sldrh
d12sldrv12.snf     d12sldrv
e12bluci10.snf     e12bluci
e12butto12.snf     e12butto
e12lucid10.snf     e12lucid
e12sbarh12.snf     e12sbarh
e12sbarv12.snf     e12sbarv
e12sldrh12.snf     e12sldrh
e12sldrv12.snf     e12sldrv
f12bluci10.snf     f12bluci
f12butto12.snf     f12butto
f12lucid12.snf     f12lucid
f12sbarh12.snf     f12sbarh
f12sbarv12.snf     f12sbarv
f12sldrh12.snf     f12sldrh
f12sldrv12.snf     f12sldrv
g12bluci10.snf     g12bluci
g12butto12.snf     g12butto
g12lucid10.snf     g12lucid
g12sbarh12.snf     g12sbarh
g12sbarv12.snf     g12sbarv
g12sldrh12.snf     g12sldrh
g12sldrv12.snf     g12sldrv
h12bluci12.snf     h12bluci
h12butto12.snf     h12butto
h12iluci12.snf     h12iluci
h12lucid12.snf     h12lucid
h12sbarh12.snf     h12sbarh
h12sbarv12.snf     h12sbarv
h12sldrh12.snf     h12sldrh
h12sldrv12.snf     h12sldrv

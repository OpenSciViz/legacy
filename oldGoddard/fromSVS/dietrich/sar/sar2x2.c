#include <stdio.h>
#include <math.h>
#include "complex.h"

/* grid of harmonic oscillators scattering/emitting spherical waves of fixed 
polarization (z axis) in phase, with different amplitudes Aij */
#define N 2 /* number of scattering cells */
#define M 2
#define H 10000.0 /* altitude of sensor ~ 10,000 meters */
#define PI 3.1415926
#define C 300000000.0 /* speed of light in m/s */
#define PULSE_LN 0.000002 /* 2 micro-sec pulse length */
#define PRF 1000.0 /* 1 KHz */
#define INTERPULSE_LN 1.0/PRF /* 1 milli-sec between each pulse */
#define GRND_SP 100.0 /* 100 m/sec */
#define P_BAND 450000000.0 /* 450.0 MHz */
#define COMP_RATIO 200.0 
#define BAND_WIDTH 40000000.0 /* 40 MHz */ 
#define MAX_PULSE 1024 /*  seconds worth using 1 milli-sec "inter-pulse period */
#define DIGITIZE 16384
#define SIGNAL 1.0
#define SWATH_WIDTH C*PULSE_LN/1.4142

/* note this sim. uses a range gate period == uncompressed pulse period */
double w_min=9999999999.9, w_max= -9999999999.9, w_pulse;
float width, look[3];
float cell[N][M][3];
double Freq_Rate = -BAND_WIDTH/(PULSE_LN/2.0);
double T0 = 1.4142136*H/C;
double Doppler_Freq_Rate;

main(argc,argv,env)
int argc; char **argv, **env;
{
 static float A0[2][N][M], q0[2][N][M], refl[2][N][M];
 static double w_chrp, t_chrp, w0, t, del_t, h, vel[3], pos[3]; 
 static float x, x0= -GRND_SP/2*MAX_PULSE*INTERPULSE_LN; /* ~ 1/2 neg & 1/2 pos x vals */
 static float y, y0= -H; /* 45 deg. inclination to target */
 static fcomplex sig[DIGITIZE], car[DIGITIZE], pulse[DIGITIZE];
 float doppler[MAX_PULSE], ref[DIGITIZE/16];
 fcomplex tmp, collect();
 long seed=12345678;
 int fdr, fdd, i=0, j=0, k, n_pulse;
 float locate_1, locate_2;

/*  srand48(seed); */

  if( argc > 1 )
  {
    vel[0] = atof(argv[1]);
    argc--;
    argv++;
  }
  else
    vel[0] = GRND_SP; /* default vel. in meters/sec */
  fprintf(stderr,"sar> vel= %f\n",vel[0]);

  if( argc > 1 )
  {
    locate_1 = atof(argv[1]) * SWATH_WIDTH;
    argc--;
    argv++;
  }
  else
    locate_1 = -0.25 * SWATH_WIDTH;
  fprintf(stderr,"sar> target #1= %f\n",locate_1);

  if( argc > 1 )
  {
    locate_2 = atof(argv[1]) * SWATH_WIDTH;
    argc--;
    argv++;
  }
  else
    locate_2 = 0.25 * SWATH_WIDTH;
  fprintf(stderr,"sar> target #2= %f\n",locate_2);

  if( argc > 1 )
    width = cos(atof(argv[1]) / 57.3);
  else
    width = cos(5.0 / 57.3);

  look[0] = 0.0; /* this can change with squint ang. and pitch of plane */
  look[1] = 1.0/sqrt(2.0); /* these can change with roll */
  look[2] = look[1];

  x0 = -vel[0]/2*MAX_PULSE*INTERPULSE_LN;
  if( (N == 1) && (M == 1) )
  {
    cell[0][0][0] = 0.0;
    cell[0][0][1] = locate_1; 
    cell[0][0][2] = 0.0;
  }
  else if( (N == 1) && (M == 2) )
  {
    cell[0][0][0] = 0.0;
    cell[0][0][1] = locate_1; 
    cell[0][0][2] = 0.0;
    cell[0][1][0] = 0.0;
    cell[0][1][1] = locate_2; 
    cell[0][1][2] = 0.0;
  }
  else if( (N == 2) && (M == 1) )
  {
    cell[0][0][0] = locate_1;
    cell[0][0][1] = 0.0; 
    cell[0][0][2] = 0.0;
    cell[0][1][0] = locate_2;
    cell[0][1][1] = 0.0; 
    cell[0][1][2] = 0.0;
  }
  else if( (N == 2) && (M == 2) )
  {
    cell[0][0][0] = locate_1;
    cell[0][0][1] = 0.0; 
    cell[0][0][2] = 0.0;
    cell[0][1][0] = locate_2;
    cell[0][1][1] = 0.0; 
    cell[0][1][2] = 0.0;
    cell[0][2][0] = 0.0;
    cell[0][2][1] = locate_1; 
    cell[0][2][2] = 0.0;
    cell[0][3][0] = 0.0;
    cell[0][3][1] = locate_2; 
    cell[0][3][2] = 0.0;
  }
  else
  {
/* divide x values by 2 to keep targets in 1/2 of the 2 second swath */  
    for( i = 0, x = x0*N/(N+1)/2; i < N; x = x + -2.0*x0/N/2, i++ )
    {
      for( j = 0, y = -SWATH_WIDTH/2.0*M/(M+1); j < M; y += SWATH_WIDTH/M , j++ )
      {
        cell[i][j][0] = x;
        cell[i][j][1] = y;
        cell[i][j][2] = 0.0;
	fprintf(stderr,"cell (%d,%d):  x= %f  y= %f\n",i,j,x,y);
      }
    }
  }

  h = H;
  del_t = PULSE_LN / DIGITIZE;
  w0= 2.0*PI*P_BAND;
  Doppler_Freq_Rate = 2*vel[0]*vel[0]*(P_BAND/C)/(1.4142136*H)/(4*PI);
  fprintf(stderr,"sar> Range_Freq_Rate= %f\n",Freq_Rate);
  fprintf(stderr,"sar> Doppler_Freq_Rate= %f\n",Doppler_Freq_Rate);
/*  read(0,A0,2*4*N*M); */
  for( i=0; i<N; i++ )
  {
    for( j=0; j<M; j++ )
    {
      A0[0][i][j] = SIGNAL;
    }
  }

  fprintf(stderr,"start x: %f\n",x0);

/* generate time reference carrier and pulses: */
  t_chrp = PULSE_LN/(-2.0); /* reset time var. for carrier */ 
  for( k = 0; k < DIGITIZE; k++ )
  {
/* a chirp that goes from low to high: */
    w_chrp = w0 + 2*PI*Freq_Rate*t_chrp; 
    pulse[k] = Cexp(Complex(0.0,w_chrp*t_chrp));
    car[k] = Cexp(Complex(0.0,w0*t_chrp)); 
    t_chrp = t_chrp + del_t;
  }
  for( i = 0; i < DIGITIZE; i += 16 )
  {
    tmp = Cdiv(pulse[i],car[i]);
    ref[i/16] = tmp.r; /* reference signal is simply the transmit pulse "mixed" with carrier */
  }
  write(1,ref,4*DIGITIZE/16);

  for( t = 0.0, n_pulse = 0; n_pulse < MAX_PULSE; n_pulse++, t += INTERPULSE_LN )
    doppler[n_pulse] = cos(2*PI*Doppler_Freq_Rate*
			  (t-INTERPULSE_LN*MAX_PULSE/2.0)*
			  (t-INTERPULSE_LN*MAX_PULSE/2.0));
  write(1,doppler,4*MAX_PULSE);

  for( t = 0.0, n_pulse = 0; n_pulse < MAX_PULSE; n_pulse++ )
  {
    t_chrp = PULSE_LN/(-2.0); /* reset time var. for carrier */ 
    for( k = 0; k < DIGITIZE; k++ )
    {
      pos[0] = x0 + vel[0]*t;
      pos[1] = y0 + vel[1]*t;
      pos[2] = h + vel[2]*t;

/* just do one component for now, and since q0 is all 0s, all in phase
      for( i = 0; i < N; i++ )
        for( j = 0; j < M; j++ )
        {
          refl[0][i][j] = A0[0][i][j]*cos(q0[0][i][j]);
	} 
*/
/* calculate time-delayed dopplar signal: */ 
      sig[k] = collect(w0,t_chrp,vel,pos,A0);
      t = t + del_t;
      t_chrp = t_chrp + del_t;
    }
/* write result to standard output */
    mix_and_store(DIGITIZE,1,car,sig);
    t = t + INTERPULSE_LN;
  }

  fprintf(stderr,"stop x: %f\n",pos[0]);
  fprintf(stderr,"w0, w_min, w_max: %f  %f  %f\n",w0,w_min,w_max);
}

fcomplex collect(w0,t,vel,pos,rad)
double w0, t, vel[3], pos[3];
float rad[2][N][M];
/* time, freq., velocity of detector, position, and scatterd radiation */
{
 double dot, range, w_prm, w_chrp, t_delay, t0;
 float tmp, pointing[3];
 int i, j;
 fcomplex noise, signal, amp, antenna_beam();
  

  signal.r = 0.0;
  signal.i = 0.0;
/* each scatterer "sees" the chirp at a different point in it's frequency modulation,
   this difference amounts to the difference in time-delays. use t0 at a nominal
   position, say the point of closest approach to the swath and caclulate the
   difference in time delays */ 

  for( i = 0; i < N; i++ )
  {
    for( j = 0; j < M; j++ )
    {
      pointing[0] = cell[i][j][0] - pos[0];
      pointing[1] = cell[i][j][1] - pos[1];
      pointing[2] = cell[i][j][2] - pos[2];
      range = sqrt( pointing[0]*pointing[0] + pointing[1]*pointing[1] +
      		    pointing[2]*pointing[2] );
      dot = pointing[0]*vel[0] + pointing[1]*vel[1] + pointing[2]*vel[2];
      dot = dot / range;

/* relative time-delay scatterers "see" pulse (for evaluation of chirp): */
      t_delay = t + (T0 - range/C);  

/* a chirp that goes from low to high: */
      w_chrp = w0 + 2*PI*Freq_Rate*t_delay;

      w_prm = w_chrp * (1.0 + dot/C) * (1.0 + dot/C);  /* galilean (double) doppler shift */

      if( w_prm > w_max ) w_max = w_prm;
      if( w_prm < w_min ) w_min = w_prm;

      amp = RCmul(rad[0][i][j],Cexp(Complex(0.0,w_prm*t_delay)));  

      signal = Cadd(signal,antenna_beam(pointing,amp));
    }
  }

/*  fprintf(stderr,"n_pulse= %d \t w_prm= %f\n",n_pulse,w_prm); 
  tmp = sqrt(SIGNAL/10.0 * drand48()); 
  noise = Complex(tmp,tmp);
  signal = Cadd(signal,noise); */

  return( signal );
}

fcomplex antenna_beam(vec,amp)
float vec[3];
fcomplex amp;
{
  float dot;
  fcomplex val;

  dot = vec[0]*vec[0] + vec[1]*vec[1] + vec[2]*vec[2];
  
  dot = (look[0]*vec[0] + look[1]*vec[1] + look[2]*vec[2]) / dot; 

  if( dot < width )
  {
    val.r = amp.r;
    val.i = amp.i;
  }
  else
    val.r = val.i = 0.0;

  return( val );
}

mix_and_store(n,fd,c,s)
int n, fd;
fcomplex c[], s[];
{
  float mix[1024];
  fcomplex tmp;
  int i;
  for( i = 0; i < n; i += 16 )
  {
    tmp = Cdiv(s[i],c[i]); 
    mix[i/16] = tmp.r; /* "mix" the received pulse (signal) with carrier */
  }
  write(fd,mix,4*n/16);

  return;
}

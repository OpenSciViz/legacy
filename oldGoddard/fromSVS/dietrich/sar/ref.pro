; IDL Version 2.1.0 (IRIX mipseb)
; Journal File for xrhon@amelia
; Working directory: /usr/okeeffe/data/sirb
; Date: Wed Aug 28 16:23:04 1991
 
pi=3.1415926
f0=7.2*1000000.0
bw=12.0*1000000.0
tp=30.4/1000000.0
help
t=tp/4096.0*indgen(4096)
ref=cos(2*pi*((f0+bw/2)*t - bw/2/tp*t^2))
window,xs=1024,ys=256,ret=2
plot,ref
plot,ref(0:100)
plot,ref(0:1024)
plot,ref(3000:4000)

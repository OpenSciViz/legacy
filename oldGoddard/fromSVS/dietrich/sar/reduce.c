#include <stdio.h>
#include <math.h>
#include "nrutil.h"
#include "complex.h" /* from numerical recipes */

/* the following parameters presumably reflect SIRB specs.: */
#define DIGITIZE 4096 /* actually only 3270 */
#define MAX_PULSE 4096
#define A 0.231 /* hamming window parameters */
#define B 0.538
#define PRF 1824.1046 
#define INTERPULSE_LN 1.0/PRF /* 1 milli-sec between each pulse */
#define PULSE_LN 0.0000304 /* 30.4 microsec. */
#define BAND_WIDTH 12000000.0 /* 12.0 MHz */
#define OFFSET_FRQ 7200000.0 /* 7.2 MHz */
#define H 233797.0 /* altitude of sensor ~ 233,797 meters */
#define PI 3.1415926
#define C 300000000.0 /* speed of light in m/s */
#define P_BAND 450000000.0 /* 450.0 MHz */
#define L_BAND 1282000000.0 /* 1282.0 MHz */
#define SWATH_WIDTH C*PULSE_LN/1.4142 /* for 45 deg. inc. */
#define INC_ANG 48.6078 /* degrees */
#define DOPPLER_FRQ 1321.44 /* Hz */
#define DOPPLER_BAND_WIDTH (1357.19 - 1300.06) /* Hz */
#define SPEED 7769.0 /* m/s */

unsigned byt_sig[DIGITIZE*MAX_PULSE];
float r_ref[DIGITIZE], a_ref[MAX_PULSE], sig[DIGITIZE*MAX_PULSE], image[1024*1024];
fcomplex ftr_ref[DIGITIZE], fta_ref[MAX_PULSE],
         rtmp[DIGITIZE], atmp[MAX_PULSE],
         fts[DIGITIZE*MAX_PULSE];
int fd= 0; /* standard input */
double Doppler_Freq_Rate, Freq_Rate, R0, del_R= SWATH_WIDTH/DIGITIZE;

main(argc,argv,env)
int argc;
char **argv, **env;
{
int i, j;
int Ndim[2];
static float t, vel=2000.0, r_hamm[DIGITIZE], a_hamm[MAX_PULSE];
Ndim[0] = MAX_PULSE;
Ndim[1] = DIGITIZE;

if( argc > 1 ) 
  vel = atof(argv[1]);
else
  vel = SPEED;
fprintf(stderr,"reduce> vel= %f\n",vel);

/* assuming everything is in one file: 
read(fd,r_ref,4*DIGITIZE); 
read(fd,a_ref,4*MAX_PULSE);
*/
/* or generate the reference functions on the fly */ 
Freq_Rate = BAND_WIDTH / PULSE_LN;
for( t = 0.0, j = 0; j < DIGITIZE; j++, t += PULSE_LN/DIGITIZE )
    a_ref[j] = cos(2*PI*(OFFSET_FRQ + BAND_WIDTH/2)*t - Freq_Rate/2*t*t);
R0 = H / cos(INC_ANG);
Doppler_Freq_Rate = 2.0*vel*vel*(L_BAND/C)/R0/(MAX_PULSE/PRF);
/* Doppler_Freq_Rate = Doppler_Freq_Rate / (4*PI); broader central lobe */ 
for( t = 0.0, j = 0; j < MAX_PULSE; j++, t += INTERPULSE_LN )
    a_ref[j] = cos(2*PI*(DOPPLER_BAND_WIDTH/2*t + Doppler_Freq_Rate/2*t*t));

/* now read in the raw (byte) data: */
read(fd,byt_sig,DIGITIZE*MAX_PULSE);
for( i = 0; i < DIGITIZE*MAX_PULSE; i++ )
  sig[i] = 1.0*byt_sig[i]; /* assume a constant calibration of 1.0 */

/* init the hamming window coeffs.: */
for( i=0; i<DIGITIZE; i++ )
  r_hamm[i] = pow(B + 2.0*A*cos(3.1415926/(DIGITIZE/2)*(i-DIGITIZE/2)),1.0);

for( i=0; i<MAX_PULSE; i++ )
  a_hamm[i] = pow(B + 2.0*A*cos(3.1415926/(MAX_PULSE/2)*(i-MAX_PULSE/2)),1.0);

/* save the ref. and window coeff. for later inspection: */
fd = creat("ref.dat",0644);
write(fd,r_ref,4*DIGITIZE);
write(fd,r_hamm,4*DIGITIZE);
write(fd,a_ref,4*MAX_PULSE);
write(fd,a_hamm,4*MAX_PULSE);
close(fd);
fd = 0;
/* convert to complex array and generate the comp. conj. of FT: 
   1. range ref., 2. azimuth ref. */
for( i=0; i<DIGITIZE; i++ )
  ftr_ref[i] = Complex(r_hamm[i]*r_ref[i],0.0);
four1((&ftr_ref[0].r-1),DIGITIZE,-1);
for( i=0; i<DIGITIZE; i++ )
  ftr_ref[i] = Conjg(ftr_ref[i]);

for( i=0; i<MAX_PULSE; i++ )
  fta_ref[i] = Complex(a_hamm[i]*a_ref[i],0.0);
four1((&fta_ref[0].r-1),DIGITIZE,-1);
for( i=0; i<MAX_PULSE; i++ )
  fta_ref[i] = Conjg(fta_ref[i]);

/* range compression: */
fprintf(stderr,"reduce> range compression...\n");
for( i=0; i<MAX_PULSE; i++ )
{
/* put a range (row) into rtmp and fft it: */
  for( j=0; j<DIGITIZE; j++ )
    rtmp[j] = Complex(sig[i*DIGITIZE + j],0.0); /*Complex(r_hamm[j]*sig[i*DIGITIZE + j],0.0);*/
  four1((&rtmp[0].r-1),DIGITIZE,-1);

/* multiply by range ref. and store in fts */
  for( j=0; j<DIGITIZE; j++ )
    fts[i*DIGITIZE + j] = Cmul(rtmp[j],ftr_ref[j]);

/* take the inverse fft: */
  four1((&fts[i*DIGITIZE].r-1),DIGITIZE,1);
}

/* azimuth compression: */
fprintf(stderr,"reduce> azimuth compression...\n");
for( i=0; i<DIGITIZE; i++ )
{
/* put an azimuth (column) into atmp and fft it: */
  for( j=0; j<MAX_PULSE; j++)
    atmp[j] = fts[i + j*DIGITIZE]; /* RCmul(a_hamm[j],fts[i + j*DIGITIZE]); */
  four1((&atmp[0].r-1),MAX_PULSE,-1);

/* multiply by azimuth ref., redefining ref. for each range: 
  R0 = sqrt(H*H + (H + (i-DIGITIZE/2)*del_R)*(H + (i-DIGITIZE/2)*del_R));
  Doppler_Freq_Rate = 2.0*vel*vel*(L_BAND/C)/R0;
  Doppler_Freq_Rate = Doppler_Freq_Rate / (2*PI);  
  for( t = -INTERPULSE_LN*MAX_PULSE/2.0, j = 0; j < MAX_PULSE; j++, t += INTERPULSE_LN )
    a_ref[j] = cos(2*PI*Doppler_Freq_Rate*t*t);
  for( j=0; j<MAX_PULSE; j++ )
    fta_ref[j] = Complex(a_hamm[j]*a_ref[j],0.0);
  four1((&fta_ref[0].r-1),DIGITIZE,-1);
  for( j=0; j<MAX_PULSE; j++ )
    fta_ref[j] = Conjg(fta_ref[j]);
*/
  for( j=0; j<MAX_PULSE; j++)
    atmp[j] = Cmul(atmp[j],fta_ref[j]);

/* and eval. the inverse fft: */
  four1((&atmp[0].r-1),MAX_PULSE,1);

/* store the result back in fts: */
  for( j=0; j<MAX_PULSE; j++)
    fts[i + j*DIGITIZE] = atmp[j];
}

/* output the result as power, while shuffling the 4 quadrants: */
  for( i=0; i<MAX_PULSE/2; i++ )
    for(j=0; j<DIGITIZE/2; j++ )
    {
      sig[i*DIGITIZE +j] = Camp(fts[(i+MAX_PULSE/2)*DIGITIZE + j + DIGITIZE/2]);
      sig[(i+MAX_PULSE/2)*DIGITIZE + j + DIGITIZE/2] = Camp(fts[i*DIGITIZE+j]);
      sig[i*DIGITIZE + j + DIGITIZE/2] = Camp(fts[(i+MAX_PULSE/2)*DIGITIZE + j]);
      sig[(i+MAX_PULSE/2)*DIGITIZE + j] = Camp(fts[i*DIGITIZE + j + DIGITIZE/2]);
    }

  write(1,sig,4*MAX_PULSE*DIGITIZE);
/* also create a 1024x1024 sub-sampled image */
  for( i=0; i < 1024; i++ )
    for( j=0; j < 1-24; j++ )
      image[i*1024 + j] = sig[4*i*1024 + 4*j];
  fd=creat("sirb1024.image",0644);
  write(fd,image,4*1024*1024);
      
}


  

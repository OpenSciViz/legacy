#include <fcntl.h>

#include <stdio.h>
#include <gl.h>
#include <gl/device.h>
#include <audio.h>



typedef signed short SAMPLE;
#define MAXBUFSIZE 1000000
#define CPYBUFSIZE 200000
#define BUFCHUNKSIZE 2000
#define NTRACKS 2
#define SIGLIMIT 34000

#define MIN(A,B)  ( (A) < (B) ? (A) : (B)  )
#define MAX(A,B)  ( (A) > (B) ? (A) : (B)  )
/* Define the magic number */

SAMPLE inbuf[NTRACKS*MAXBUFSIZE];
SAMPLE copybuf[CPYBUFSIZE];




int nsamps,screenx0,screeny0,screenwidth,screenheight;
int firstvisible,nvisible;
int nselected, firstselected, lastselected;
int BytesPerSample = 2;
float leftedge,rightedge;
int wid;	/* our window id */
int eightbit;
int copysize;
ALconfig recordconf, playconf;

main(argc,argv)
int argc;
char *argv[];
{
	
	int temp,i;
	long dev,qsize;
	short data;
	int mainmenu, puprequest;
	char filename[120];

	copysize = 0;

	recordconf = ALnewconfig();
	ALsetwidth(recordconf,2);
	qsize = ALgetqueuesize(recordconf);

	playconf = ALnewconfig();
	ALsetwidth(playconf,2);

	for (i=0;i< MAXBUFSIZE; i++) inbuf[i] = 0;
	nsamps = 1024;
	nvisible = nsamps;
	firstvisible = 0;
	leftedge=0;
	rightedge=nsamps-1;
	firstselected = 0;
	nselected = nsamps;

	prefposition(200,1200, 300,700);
	wid = winopen(argv[1]);
	winconstraints();
	ortho2(0,nsamps-1,-128,127);
    	eightbit = getplanes() < 12;
    	if (!eightbit) {
		overlay(2);
		gconfig();
		drawmode(OVERDRAW);
		mapcolor(1, 255,0,0);
		drawmode(NORMALDRAW);
	}
	qdevice(LEFTMOUSE);
	qdevice(MIDDLEMOUSE);
	qdevice(RIGHTMOUSE);
	qdevice(SPACEKEY);
/*
	tie(LEFTMOUSE,MOUSEX,0);
	tie(MIDDLEMOUSE,MOUSEX,0);
*/
	refresh();
#ifdef NOTDEF
	speak(&astate);
#endif
	mainmenu = 
  defpup("Clipit %t| play | record  | zoom in | zoom out |quit");
	while (TRUE) {
	  /* while(qtest()) */ {
		dev=qread(&data);
		if(dev == LEFTMOUSE && data == 1) {
			data = make_new_space(LEFTMOUSE,TRUE);
			/* leftedge = ((nsamps-1) * (data-screenx0+0.0))/ screenwidth; */
			refresh();
			speak();
		} else if(dev == MIDDLEMOUSE && data == 1) {
			qread(&data);
			rightedge = ((nsamps-1) * (data-screenx0)+0.0)/screenwidth;
			refresh();
			speak();
		} else if(dev == RIGHTMOUSE && data == 1) {
			puprequest = dopup(mainmenu);
			switch ( puprequest ) {
			case 1:
				speak();
				break;
			case 2:

				nsamps = recordbuffer(inbuf,MAXBUFSIZE*NTRACKS);
				firstvisible = 0;
				nvisible = nsamps;
				leftedge=0;
				rightedge=nsamps-1;
    				firstselected = leftedge;
    				lastselected = rightedge;
    				nselected = lastselected-firstselected; 
  				refresh();
				
				break;

			case 3:
				if( nselected < screenwidth) 
					{
					/*
					firstvisible = firstvisible +
					  (screenwidth-nselected)/2);
					*/
					break;
					}
				firstvisible = firstvisible+firstselected;
				firstselected = 0;
				nvisible = nselected;
				refresh();
				break;
			case 4: 
				firstselected += firstvisible;
				firstvisible = 0;
				nvisible = nsamps;
				refresh();
				break;
			case 5:
				exit(0);
			default:
				qreset();
				break;
			}
			
		} else if(dev == REDRAW) {
			refresh();
			}
		
		}
		
	}
}

speak() 
{
	int l,r,w,noutputsamps,firstsamp;
	int ntoplay,nplayed,nleft,duration;
	short val,dev;
	int audio;
	SAMPLE dummy = 0;
	register SAMPLE *p,*q,tmp;
	ALport port;
	port = ALopenport("clipit play", "w", playconf);
	if ( port ==  (ALport) 0) 
		{
		fprintf(stderr, " Could not open Audio Port \n");
		exit(1);
		}

	w=nselected;
	    noutputsamps = w+1;
	p = inbuf+(NTRACKS*(firstselected+firstvisible));
        nleft = nselected;
	nleft *= NTRACKS;
	while (nleft > 0)
		{
		ntoplay = (BUFCHUNKSIZE > nleft) ? nleft : BUFCHUNKSIZE;


		ALwritesamps(port, (void *)p, ntoplay);
		nplayed = ntoplay;
		p += nplayed;
		nleft -= nplayed;
		if (qtest()) 
			{
			dev = qread(&val);
			if (dev == SPACEKEY || dev == RIGHTMOUSE || dev == LEFTMOUSE || dev == MIDDLEMOUSE)
			  {  qread(&val); break;}
			}
		}

	while(ALgetfilled(port))
		sginap(3);
	ALcloseport(port);
	return(1);
}

refresh()
{
	int i,j,k,l;
	int sampsperpixel;
	int min,max;
	long dev;
	register SAMPLE *p;
	reshapeviewport();
	getorigin(&screenx0,&screeny0);
	getsize(&screenwidth,&screenheight);
	ortho2(0,nvisible-1,-(SIGLIMIT<<1),(SIGLIMIT<<1));
	color(8);
	clear();
	color(43);
	rectf(firstselected,-(SIGLIMIT<<1)+(SIGLIMIT>>3),firstselected+nselected,(SIGLIMIT<<1)-(SIGLIMIT>>3));
	color(3);
	sampsperpixel = nvisible/screenwidth;
	if (sampsperpixel < 1 ) sampsperpixel = 1;
	ortho2(0,screenwidth-1,-(SIGLIMIT<<1),(SIGLIMIT<<1));
	p = inbuf+(firstvisible*NTRACKS);
	l = screenwidth;
	if (screenwidth > nvisible) l = nvisible;
	move2i(0,SIGLIMIT);
	for (j=0;j<l;j++)
		{
		max = -32000;
		min = 32000;
		for(k=0;k<sampsperpixel;k++,p+=NTRACKS)
			{
			if (*p > max) max = *p;
			if (*p < min) min = *p;
			}
		if ( sampsperpixel > 1) move2i(j,min+SIGLIMIT);
		draw2i(j,max+SIGLIMIT);
	}
	p = inbuf+(firstvisible*NTRACKS)+1;
	l = screenwidth;
	if (screenwidth > nvisible) l = nvisible;
	move2i(0,-SIGLIMIT);
	color(2);
	for (j=0;j<l;j++)
		{
		max = -32000;
		min = 32000;
		for(k=0;k<sampsperpixel;k++,p+=NTRACKS)
			{
			if (*p > max) max = *p;
			if (*p < min) min = *p;
			}
		if ( sampsperpixel > 1) move2i(j,min-SIGLIMIT);
		draw2i(j,max-SIGLIMIT);
	}
	ortho2(0,nvisible-1,-(SIGLIMIT),(SIGLIMIT));
}


int recordbuffer(buf,size) SAMPLE *buf; int size;
	{
	int  ntorecord, ntotal, nleft, nbufs =0, nwaiting;
	short val;
	int audio;
	ALport port;
	char *bp;
	long dev;
	nleft = size;
	ntotal =0;
	qreset();
	port = ALopenport("clipit rrecord", "r", recordconf);
	if ( port == (ALport) 0) 
		{
		fprintf(stderr, " Could not open Audio Port \n");
		exit(1);
		}
	ortho2(0,MAXBUFSIZE*NTRACKS,-(SIGLIMIT<<1),(SIGLIMIT<<1));
	color(8);
	clear();
	color(43);
	while (nleft > 0)
		{
		nwaiting = ALgetfilled(port);
		ntorecord = MIN(nleft, MAX(BUFCHUNKSIZE,nwaiting));
		ALreadsamps(port, (void *) buf, ntorecord);
		
		nbufs++;
		buf += ntorecord;
		nleft -= ntorecord;
		ntotal += ntorecord;
		if (qtest () ) 
			{
			dev = qread(&val);
			if (dev == SPACEKEY || dev == RIGHTMOUSE || dev == LEFTMOUSE || dev == MIDDLEMOUSE)
				 {qread(&val); break;}
			}
	 	if( nleft < BUFCHUNKSIZE ) break;
		rectf(0,-(SIGLIMIT>>3),ntotal,(SIGLIMIT>>3));
		}
	nwaiting = ALgetfilled(port);
	ntorecord = MIN(nleft, MAX(BUFCHUNKSIZE,nwaiting));
	ALreadsamps(port, (void *) buf, ntorecord);
	ntotal += ntorecord;
	ALcloseport(port);
	return(ntotal/NTRACKS);
	}

make_new_space(button,maintainaspect) short button; int maintainaspect;
{

    long            mx, omx,  cx,  bx,  obx;
    int fgrndcolor;
    float tmp;
    short dev,val;
    if (eightbit) {
	drawmode(PUPDRAW);
	mapcolor(1, 255, 0, 0);
    } else
	drawmode(OVERDRAW);

    fgrndcolor = 1;
    qreset();

    omx = mx = getvaluator(MOUSEX) - screenx0;
 
    obx = bx = cx = mx;

    color(0);
    clear();


    do {
	while (!qtest()) {

		mx = getvaluator(MOUSEX) - screenx0;

		if (mx != omx ) {

		bx = mx;
		color(0);

		leftedge = ((nvisible-1) * (cx+0.0))/ screenwidth; 
		rightedge = ((nvisible-1) * (obx+0.0))/ screenwidth; ;
		rect(leftedge, -SIGLIMIT, rightedge, SIGLIMIT);

		obx = bx;
		omx = mx;

		leftedge = ((nvisible-1) * (cx+0.0))/ screenwidth; 
		rightedge = ((nvisible-1) * (bx+0.0))/ screenwidth; ;

		color(fgrndcolor);
		rect (leftedge, -SIGLIMIT, rightedge, SIGLIMIT);
		}
    	}
    } while ((dev = qread(&val)) != button );
    color(0);
    clear();

    drawmode(NORMALDRAW);
    if ( leftedge > rightedge)
		{
		tmp =leftedge;
		leftedge =rightedge;
		rightedge = tmp;
		}
  
    firstselected = leftedge;
    lastselected = rightedge;
    nselected = lastselected-firstselected+1;   
    return(val);
}



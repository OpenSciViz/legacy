/** raw byte array output */
extern int ufSendAgent(const char* agent, const unsigned char* sndbuf, int nb);
extern int ufSendShortAgent(const char* agent, short val); /* htons */
extern int ufSendShortsAgent(const char* agent, short* vals, int cnt); /* htons */
extern int ufSendIntAgent(const char* agent, int val); /* htonl */
extern int ufSendIntsAgent(const char* agent, int* vals, int cnt); /* htonl */
extern int ufSendFloatAgent(const char* agent, float val); /* htonl */
extern int ufSendFloatsAgent(const char* agent, float* vals, int cnt); /* htonl */

/** raw byte array input */
extern int ufRecvAgent(const char* agent, unsigned char* rbuf, int len);
extern unsigned short ufRecvShortAgent(const char* agent); /* ntohs */
extern int ufRecvShortsAgent(const char* agent, short* vals, int cnt); /* ntohs */
extern int ufRecvIntAgent(const char* agent); /* ntohl */
extern int ufRecvIntsAgent(const char* agent, int* vals, int cnt); /* ntohl */
extern float ufRecvFloatAgent(const char* agent); /* ntohl */
extern int ufRecvFloatsAgent(const char* agent, float* vals, int cnt); /* ntohl */

extern int ufRecvObsConf(const char* agent, ufObsConfig** ufop);

/*** convenience func. to req & recv both config. objects ***/
extern int ufFetchConfig( const char* agent, ufFrmConfig* uffc,
                                             ufObsConfig** ufop );


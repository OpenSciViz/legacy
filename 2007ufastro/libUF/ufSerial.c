#if !defined(__UFSerial_c__)
#define __UFSerial_c__ "$Name:  $ $Id: ufSerial.c,v 0.0 2002/06/03 17:42:20 hon beta $"
static const char rcsIdUFSerial[] = __UFSerial_c__;

#include "ufSerial.h"
__UFSerial_H__(Serial_c);

#include "ufLog.h"
__UFLog_H__(Serial_c);

#include "stdlib.h"
#include "string.h"
#include "unistd.h"
#include "stdio.h"
#include "dirent.h"
#include "errno.h"
#include "limits.h"
#include "strings.h"
#include "fcntl.h"
#include "time.h"
#include "termio.h"
#include "signal.h"
#include "sys/uio.h"
#include "sys/stat.h"

static char _UFerrmsg[MAXNAMLEN + 1];

/**************************************** annex "network" serial ports 7002 - 7008 ****************/
/* static FILE* _fileptrs[FD_SETSIZE]; */

static pid_t _child;
static char* _fifo;

int ufCloseAnnexFifo(const char* fifoname, pid_t pid) {
  char *sendname, *recvname;
  /*
  FILE* f = _fileptrs[fd];
  if( f != 0 ) {
    sprintf(_UFerrmsg,"ufCloseAnnexFifo> closing %s, pipe fd= %d", fifoname, fd);
    _uflog(_UFerrmsg);
    pclose(f);
    _fileptrs[fd] = 0;
  }
  */
  kill(pid, SIGTERM); /* terminate the annexfifo child */
  /* remove the fifo(s) */
  sendname = calloc(1+strlen(fifoname)+strlen("Send"), sizeof(char));
  strcat(sendname, fifoname); strcat(sendname, "Send");
  remove(sendname);
  recvname = calloc(1+strlen(fifoname)+strlen("Recv"), sizeof(char));
  strcat(recvname, fifoname); strcat(recvname, "Recv");
  remove(recvname);

  return 0;
}

/* if user exits idl without closing annex fifo child, try to close it here */
static void _ufshutdown(void) {
  ufCloseAnnexFifo(_fifo, _child);
}

pid_t ufOpenAnnexFifo(const char* fifoname, int port) {
  /*
  int fd= -1;
  FILE* f= 0;
  */
  pid_t child=0;
  int cnt= 0;
  char* cmd= 0;
  /* char* annex = "/usr/local/uf2000/bin/annex"; */
  char* annex = "annex";
  char* argv[7];
  argv[0] = annex;
  argv[1] = "-port";
  argv[2] = "7008";
  argv[3] = "-sendfifo";
  argv[4] = calloc(1+strlen(fifoname)+strlen("Send"), sizeof(char));
  strcat(argv[4], fifoname); strcat(argv[4], "Send");
  argv[5] = "-recvfifo";
  argv[6] = calloc(1+strlen(fifoname)+strlen("Recv"), sizeof(char));
  strcat(argv[6], fifoname); strcat(argv[6], "Recv");

  /* allocate sufficient space for command string using default port number */
  cmd = calloc(1+strlen(argv[0]) +
	       1+strlen(argv[1]) +
	       1+strlen(argv[2]) +
	       1+strlen(argv[3]) +
	       1+strlen(argv[4]) +
	       1+strlen(argv[5]) +
	       1+strlen(argv[6]), 
	       sizeof(char));
  while( cnt < 7 )
    strcat(cmd, argv[cnt++]); strcat(cmd, " ");

  /*
  sprintf(_UFerrmsg,"ufOpenAnnexFifo> default cmd= %s", cmd);
  _uflog(_UFerrmsg);
  */
  /* replace default port number */
  if( port >= 7002 && port <= 7008 ) { /* annex only has 7 available ports! */
    sprintf(cmd, "%s %04d %s%s%s %s%s%s", "/usr/local/uf/bin/annex -port", port,
	    "-sendfifo ", fifoname, "Send", "-recvfifo ", fifoname, "Recv");
  }
  /*
  sprintf(_UFerrmsg,"ufOpenAnnexFifo> cmd= %s", cmd);
  _uflog(_UFerrmsg);
  */
  /* fork, create pipe, exec; save the FILE* for pclose call
  f = popen(cmd, "r");
  if( f != 0 ) {
    fd = fileno(f);
    _fileptrs[fd] = f; 
  }
  return fd;
  */

  /* instead of popen, use fork & exec; returm child pid */
  child = fork();
  if( child == 0 )
    execvp(annex, argv); /* child overlays new process with annex app */

  /* save fifo & child pid for atexit ... */
  _fifo = calloc(1+strlen(fifoname), sizeof(char)); strcpy(_fifo, fifoname);
  _child = child;
  atexit(_ufshutdown);
  return child; /* parent gets child pod back */
}

/******************************** serial port i/o ***********************************/
#include "sys/uio.h"
#include "termio.h"
static int _fdtty[2] = { -1, -1 };
struct termio _orig[2];
struct termio _raw;

#if defined(sun) || defined(solaris)|| defined(Solaris) || defined(SOLARIS)
static const char* _tty[2] = { "/dev/term/a", "/dev/term/b" };
static const int defaultBaud = B9600;
#endif
#if defined(LINUX)
static const char* _tty[2] = { "/dev/ttyS0", "/dev/ttyS1" };
static const int defaultBaud = B9600;
#endif
#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx) 
static const char* _tty[2] = { "/tyCo/0", "/tyCo/1" }; /* PPC mv2700 */
static const defaultBaud = 9600;
#endif

static const int _CR = 0x0d;
static const int _NL = 0x0a;

int ufttyOpen(int portNo, int baud) {
  _fdtty[portNo] = open( _tty[portNo], O_RDWR, 0 );
#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx) 
  _orig[portNo] = ioctl(_fdtty[portNo], FIOGETOPTIONS, 0 );
  ioctl( _fdtty[portNo], FIOBAUDRATE, baud );
  ioctl( _fdtty[portNo], FIOSETOPTIONS, OPT_RAW );
#else
  ioctl( _fdtty[portNo], TCGETA , &_orig[portNo]);
  ioctl( _fdtty[portNo], TCGETA, &_raw); 
  /* note B0, B50, ..., B460800 are in /usr/include/sys/termios.h */
  _raw.c_cflag = baud | CS8 | CLOCAL | CREAD;
  _raw.c_lflag &= ~ICANON;
  _raw.c_lflag &= ~ECHO;
  _raw.c_cc[VMIN] = 1; /* present each character as soon as it shows up */
  _raw.c_cc[VTIME] = 1; /* 0.1 second for timeout */
  ioctl( _fdtty[portNo], TCSETAF, &_raw);
#endif
  return _fdtty[portNo];
} /* ttyopen */

int ufttyClose(int portNo) {
#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx) 
  ioctl( _fdtty[portNo], FIOSETOPTIONS, _orig[portNo] );
#else
  ioctl( _fdtty[portNo], TCSETAF, &_orig[portNo] );
#endif
  return close( _fdtty[portNo] );
} /* ttyclose */

/* 0 == ttya, 1 == ttyb */
char ufttySend1(int portNo, char c) {
  if( _fdtty[portNo] < 0 ) ufttyOpen(portNo, defaultBaud);
  write(_fdtty[portNo], &c, 1);
  return c;
}

char* ufttySend(int portNo, char* s) {
  int len, cnt;
  if( _fdtty[portNo] < 0 ) ufttyOpen(portNo, defaultBaud);
  len = strlen(s); /* assume null terminated string? */
  cnt = write(_fdtty[portNo], s, len);
  return s;
}

char ufttyRecv1(int portNo) {
  char c;
  if( _fdtty[portNo] < 0 )
    ufttyOpen(portNo, defaultBaud);
  read(_fdtty[portNo], &c, 1);
  return c;
}

char* ufttyRecv(int portNo) {
  int len, cnt;
  static char s[MAXNAMLEN + 1];

  memset(s, 0, sizeof(s));
  if( _fdtty[portNo] < 0 )
    ufttyOpen(portNo, defaultBaud);
  len = strlen(s); /* assume null terminated string? */
  cnt = read(_fdtty[portNo], s, len);
  return s;
}
  
char* ufttyGatirSend(int ttyNo, char* s) {
  char c = '#';
  static char echo[MAXNAMLEN + 1];
  int cnt = 0, len = strlen(s); /* assume null terminated string */

  memset(echo, 0, sizeof(echo));

  if( _fdtty[ttyNo] == -1 ) /* need to open */
    ufttyOpen(ttyNo, defaultBaud); 

  /* first send the string and get its echo */
  do {
    c = s[cnt];
    c = ufttySend1(ttyNo, c);
    echo[cnt] = ufttyRecv1(ttyNo);
  } while( echo[cnt] != '>' && c != echo[cnt]  && ++cnt < len );

  /* then look for Gatir prompt '>' */
  cnt = 0;
  while( c != '>' && ++cnt < MAXNAMLEN )
    c = ufttyRecv1(ttyNo);

  if(  c != '>' ) {
    sprintf(_UFerrmsg,"ufttySend> echo = %s, cnt= %d", echo, cnt);
    _uflog(_UFerrmsg);
  }

  return echo;
}

char* ufttyGatirRecv(int ttyNo) {
  /* return each "CRLF" terminated as a null terminated string */
  static char buf[MAXNAMLEN + 1];
  char c = '#', pc = '#';
  int cnt = 0, len = sizeof(buf) - 1;

  memset(buf, 0, sizeof(buf));

  if( _fdtty[ttyNo] == -1 ) /* need to open */
    ufttyOpen(ttyNo, defaultBaud); 

  /* return each "CRLF" terminated string or hte prompt */
  do {
    pc = c;
    c = buf[cnt] = ufttyRecv1(ttyNo);
  } while( pc != _CR && c != _NL && c != '>' && ++cnt < len );

  if( c != _NL && c != '>' ) {
    sprintf(_UFerrmsg,"_ufttysend> final c = %c, cnt= %d", c, cnt);
    _uflog(_UFerrmsg);
  }

  return buf;
}

#endif /* __UFSerial_c__ */

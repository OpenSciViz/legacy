#if !defined(__UFLog_h__)
#define __UFLog_h__ "$Name:  $ $Id: ufLog.h,v 0.0 2002/06/03 17:42:20 hon beta $"
#define __UFLog_H__(arg) const char arg##Log_h__rcsId[] = __UFLog_h__;
#include "sys/types.h"

/* log interface */
#if !defined(_IDL_)
extern void _uflog(const char* msg);
#endif /* _IDL_ */
#endif /* __UFLog_h__ */

#if !defined(__UFSADFITS_cc__)
#define __UFSADFITS_cc__ "$Id: UFSADFITS.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFSADFITS_cc__;

#include "UFSADFITS.h"
__UFSADFITS_H__(__UFSADFITS_cc);

// globals: 
string* UFSADFITS::_instrum= 0;
std::map< string, string >* UFSADFITS::_sad= 0;
std::vector< string >* UFSADFITS::_obssad= 0;
std::vector< string >* UFSADFITS::_ccsad= 0;
std::vector< string >* UFSADFITS::_dcsad= 0;
std::vector< string >* UFSADFITS::_ecls218sad= 0;
std::vector< string >* UFSADFITS::_ecls33xsad= 0;
std::vector< string >* UFSADFITS::_ecpfvacsad= 0;
std::string* UFSADFITS::_ecplcsad= 0;
std::vector< string >* UFSADFITS::_eclvdtsad= 0;
std::vector< string >* UFSADFITS::_ccsymbarcdsad= 0;

// public static:
int UFSADFITS::initSADHash(std::map< string, string >& sad, const string& instrum) {
  if( _instrum == 0 ) { // ctor not yet used?
    if( instrum.empty() )
      _instrum = new string("flam");
    else
      _instrum = new string(instrum);
  }

  if( _sad == 0 ) // not called from ctor?
    _sad = new std::map< string, string >;

  if( _sad->size() == 0 ) {
    _ccsad = new std::vector< string >;
    _ccsymbarcdsad = new std::vector< string >;
    _dcsad = new std::vector< string >;
    _obssad = new std::vector< string >;
    _ecls218sad = new std::vector< string >;
    _ecls33xsad = new std::vector< string >;
    _eclvdtsad = new std::vector< string >;
    _ecpfvacsad = new std::vector< string >;
    _ecplcsad = new string; // only 1 PLC FITS SAD
  }

  _sad->clear(); 
  // via arg
  if( sad.size() != 0 ) { // use this to (re)init _sad
    clog<<"UFSADFITS::initSADHash> using provided sad table size: "<<sad.size()<<endl;
    std::map< string, string >::iterator it = sad.begin();
    while( it != sad.end() ) {
      string k = it->first;
      (*_sad)[k] = sad[k];
      ++it;
    }
    return (int)_sad->size();
  }
  // or via default:

  // note that all sad channel names should be congruent with FITS keywords
  // ufflam2epicsd -l >& flam.db; grep ':sad:' flam.db| tr "[:lower:]" "[:upper:]" | sort -u 
  // FITS keyword <= 8 uppercase characters!

  string sadrec = *_instrum + ":sad:";
  clog<<"UFSADFITS::initSADHash> sad table: "<<sadrec<<endl;

  // put the datum count at the start of the sad table
  string sadchan = sadrec + "DATUMCNT"; (*_sad)[sadchan] = "-1";
  // CC: put the datum counter in the cc list first:
  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "DCKERPOS"; (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "DCKRSTEP"; (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "FILTER";   (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "FILT1POS"; (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "FIL1STEP"; (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "FILT2POS"; (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "FIL2STEP"; (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "FOCUSPOS"; (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "FOCUSTEP"; (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "GRSMPOS";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "GRSMSTEP"; (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "LYOTPOS";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "LYOTSTEP"; (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "MOSPOS";   (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "MOSSTEP";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "WINPOS";   (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "WINSTEP";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);

  sadchan = sadrec + "WINDOWIV";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "WINDOWSV";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "WINDOWAC";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "WINDOWDC";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "WINDOWHC";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "WINDOWRC";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "WINDOWDS";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);

  sadchan = sadrec + "MOSIV";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "MOSSV";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "MOSAC";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "MOSDC";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "MOSHC";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "MOSRC";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "MOSDS";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);

  sadchan = sadrec + "DECKERIV";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "DECKERSV";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "DECKERAC";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "DECKERDC";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "DECKERHC";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "DECKERRC";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "DECKERDS";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);

  sadchan = sadrec + "FILT1IV";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "FILT1SV";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "FILT1AC";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "FILT1DC";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "FILT1HC";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "FILT1RC";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "FILT1DS";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);

  sadchan = sadrec + "LYOTIV";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "LYOTSV";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "LYOTAC";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "LYOTDC";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "LYOTHC";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "LYOTRC";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "LYOTDS";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);

  sadchan = sadrec + "FILT2IV";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "FILT2SV";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "FILT2AC";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "FILT2DC";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "FILT2HC";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "FILT2RC";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "FILT2DS";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);

  sadchan = sadrec + "GRISMIV";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "GRISMSV";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "GRISMAC";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "GRISMDC";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "GRISMHC";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "GRISMRC";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "GRISMDS";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);

  sadchan = sadrec + "FOCUSIV";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "FOCUSSV";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "FOCUSAC";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "FOCUSDC";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "FOCUSHC";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "FOCUSRC";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);
  sadchan = sadrec + "FOCUSDS";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsad->push_back(sadchan);


  sadchan = sadrec + "MOSBARCD"; (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsymbarcdsad->push_back(sadchan);
  sadchan = sadrec + "MOSCIRC1"; (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsymbarcdsad->push_back(sadchan);
  sadchan = sadrec + "MOSCIRC2"; (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsymbarcdsad->push_back(sadchan);
  sadchan = sadrec + "MOSPLTYP"; (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsymbarcdsad->push_back(sadchan);
  sadchan = sadrec + "MOSPLT01"; (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsymbarcdsad->push_back(sadchan);
  sadchan = sadrec + "MOSPLT02"; (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsymbarcdsad->push_back(sadchan);
  sadchan = sadrec + "MOSPLT03"; (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsymbarcdsad->push_back(sadchan);
  sadchan = sadrec + "MOSPLT04"; (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsymbarcdsad->push_back(sadchan);
  sadchan = sadrec + "MOSPLT05"; (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsymbarcdsad->push_back(sadchan);
  sadchan = sadrec + "MOSPLT06"; (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsymbarcdsad->push_back(sadchan);
  sadchan = sadrec + "MOSPLT07"; (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsymbarcdsad->push_back(sadchan);
  sadchan = sadrec + "MOSPLT08"; (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsymbarcdsad->push_back(sadchan);
  sadchan = sadrec + "MOSPLT09"; (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsymbarcdsad->push_back(sadchan);
  sadchan = sadrec + "MOSPLT10"; (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsymbarcdsad->push_back(sadchan);
  sadchan = sadrec + "MOSPLT11"; (*_sad)[sadchan] = sad[sadchan] = "null"; _ccsymbarcdsad->push_back(sadchan);

  // EC:
  sadchan = sadrec + "INTERLCK"; (*_sad)[sadchan] = sad[sadchan] = "0"; *_ecplcsad = sadchan;
  sadchan = sadrec + "LVDTDISP"; (*_sad)[sadchan] = sad[sadchan] = "null"; _eclvdtsad->push_back(sadchan);
  sadchan = sadrec + "LVDTVLTS"; (*_sad)[sadchan] = sad[sadchan] = "null"; _eclvdtsad->push_back(sadchan);
  sadchan = sadrec + "LOGHOST"; (*_sad)[sadchan] = sad[sadchan] = "null";
  sadchan = sadrec + "LOGFILE"; (*_sad)[sadchan] = sad[sadchan] = "null";
  sadchan = sadrec + "LOGSIZE"; (*_sad)[sadchan] = sad[sadchan] = "0";
  
  // Camera cryostat vacuum
  sadchan = sadrec + "CAMVAC";   (*_sad)[sadchan] = sad[sadchan] = "null"; _ecpfvacsad->push_back(sadchan);

  // MOS cryostat vacuum
  sadchan = sadrec + "MOSVAC";   (*_sad)[sadchan] = sad[sadchan] = "null"; _ecpfvacsad->push_back(sadchan);
  
  // Camera cryostat channel A and B heater power and set points:
  // note that heater power is manual % and that only channel A supports Range selections
  sadchan = sadrec + "CAMARANG"; (*_sad)[sadchan] = sad[sadchan] = "null"; _ecls33xsad->push_back(sadchan);
  sadchan = sadrec + "CAMAHTPW"; (*_sad)[sadchan] = sad[sadchan] = "null"; _ecls33xsad->push_back(sadchan);
  sadchan = sadrec + "CAMBHTPW"; (*_sad)[sadchan] = sad[sadchan] = "null"; _ecls33xsad->push_back(sadchan);
  sadchan = sadrec + "CAMASETP"; (*_sad)[sadchan] = sad[sadchan] = "null"; _ecls33xsad->push_back(sadchan);
  sadchan = sadrec + "CAMBSETP"; (*_sad)[sadchan] = sad[sadchan] = "null"; _ecls33xsad->push_back(sadchan);
  sadchan = sadrec + "CAMAPID"; (*_sad)[sadchan] = sad[sadchan] = "null"; _ecls33xsad->push_back(sadchan);
  sadchan = sadrec + "CAMBPID"; (*_sad)[sadchan] = sad[sadchan] = "null"; _ecls33xsad->push_back(sadchan);

  sadchan = sadrec + "CAMBAPWR"; (*_sad)[sadchan] = sad[sadchan] = "null"; _ecls33xsad->push_back(sadchan);
  sadchan = sadrec + "CAMAAPWR"; (*_sad)[sadchan] = sad[sadchan] = "null"; _ecls33xsad->push_back(sadchan);
  sadchan = sadrec + "LOCK"; (*_sad)[sadchan] = sad[sadchan] = "null"; _ecls33xsad->push_back(sadchan);
  sadchan = sadrec + "ANALOG"; (*_sad)[sadchan] = sad[sadchan] = "null"; _ecls33xsad->push_back(sadchan);

  // A [0], B [1] , etc. in agent _StatRecs[] vec.
  // Camera cryostat bench temperature in kelvin (LS332 chan. A)
  sadchan = sadrec + "CAMABENC"; (*_sad)[sadchan] = sad[sadchan] = "null"; _ecls33xsad->push_back(sadchan);

  // Camera cryostat detector temperature in kelvin (LS332 chan. B)
  sadchan = sadrec + "CAMBDETC"; (*_sad)[sadchan] = sad[sadchan] = "null"; _ecls33xsad->push_back(sadchan);

  // c1-8 [0] - [7] in agent _StatRecs[] vec.
  // MOS cryostat coldhead temperature in kelvin
  sadchan = sadrec + "MOSCOLD1";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ecls218sad->push_back(sadchan);
  //sadchan = sadrec + "LS218K1";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ecls218sad->push_back(sadchan);

  // MOS cryostat bench temperature in kelvin
  sadchan = sadrec + "MOSBENC2";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ecls218sad->push_back(sadchan);
  //sadchan = sadrec + "LS218K2";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ecls218sad->push_back(sadchan);

  sadchan = sadrec + "MOSBENC3";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ecls218sad->push_back(sadchan);
  //sadchan = sadrec + "LS218K3";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ecls218sad->push_back(sadchan);

  sadchan = sadrec + "MOSBENC4";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ecls218sad->push_back(sadchan);
  //sadchan = sadrec + "LS218K4";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ecls218sad->push_back(sadchan);

  // Camera cryostat coldhead temperature in kelin
  sadchan = sadrec + "CAMCOLD5";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ecls218sad->push_back(sadchan);
  //sadchan = sadrec + "LS218K5";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ecls218sad->push_back(sadchan);

  // Camera cryostat bench temperature in kelvin
  sadchan = sadrec + "CAMBENC6";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ecls218sad->push_back(sadchan);
  //sadchan = sadrec + "LS218K6";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ecls218sad->push_back(sadchan);

  sadchan = sadrec + "CAMBENC7";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ecls218sad->push_back(sadchan);
  //sadchan = sadrec + "LS218K7";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ecls218sad->push_back(sadchan);

  sadchan = sadrec + "CAMBENC8";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ecls218sad->push_back(sadchan);
  //sadchan = sadrec + "LS218K8";  (*_sad)[sadchan] = sad[sadchan] = "null"; _ecls218sad->push_back(sadchan);

   // DC:
  sadchan = sadrec + "EXPTIME";  (*_sad)[sadchan] = sad[sadchan] = "0"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "CDSREADS"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "CYCLETYP"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "PIXBSCLK";  (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  // biases and preamps, known F2 biases: "BiasVCC", "BiasPWR", "BiasGATE", "BiasVRESET"
  sadchan = sadrec + "BIASVCC"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "BIASPWR"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "BIASGATE"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "BIASVRES"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  /*
  sadchan = sadrec + "DACBIA00"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACBIA01"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACBIA02"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACBIA03"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACBIA04"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACBIA05"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACBIA06"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACBIA07"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACBIA08"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACBIA09"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACBIA10"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACBIA11"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACBIA12"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACBIA13"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACBIA14"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACBIA15"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACBIA16"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACBIA17"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACBIA18"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACBIA19"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACBIA20"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACBIA21"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACBIA22"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACBIA23"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACBIA24"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACBIA25"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACBIA26"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACBIA27"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACBIA28"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACBIA29"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACBIA30"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACBIA31"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  */
  sadchan = sadrec + "DACPRE00"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACPRE01"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACPRE02"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACPRE03"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACPRE04"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACPRE05"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACPRE06"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACPRE07"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACPRE08"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACPRE09"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACPRE10"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACPRE11"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACPRE12"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACPRE13"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACPRE14"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACPRE15"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACPRE16"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACPRE17"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACPRE18"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACPRE19"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACPRE20"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACPRE21"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACPRE22"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACPRE23"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACPRE24"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACPRE25"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACPRE26"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACPRE27"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACPRE28"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACPRE29"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACPRE30"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "DACPRE31"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  // Science == 1 frame; Engineering == 2 frames
  sadchan = sadrec + "FRMMODE";  (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "WIDTH";    (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "HEIGHT";   (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "MILLISEC"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "PIXBSCLK"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "POSTSETS"; (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "PRESETS";  (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  sadchan = sadrec + "SECONDS";  (*_sad)[sadchan] = sad[sadchan] = "null"; _dcsad->push_back(sadchan);
  // OBS:
  sadchan = sadrec + "EDTACTN"; (*_sad)[sadchan] = sad[sadchan] = "null"; _obssad->push_back(sadchan);
  sadchan = sadrec + "EDTFRAME"; (*_sad)[sadchan] = sad[sadchan] = "null"; _obssad->push_back(sadchan);
  sadchan = sadrec + "EDTTOTAL"; (*_sad)[sadchan] = sad[sadchan] = "null"; _obssad->push_back(sadchan);
  sadchan = sadrec + "AIRMASS";  (*_sad)[sadchan] = sad[sadchan] = "null"; _obssad->push_back(sadchan);
  sadchan = sadrec + "DHSLABEL"; (*_sad)[sadchan] = sad[sadchan] = "null"; _obssad->push_back(sadchan);
  sadchan = sadrec + "OBJNAME";  (*_sad)[sadchan] = sad[sadchan] = "null"; _obssad->push_back(sadchan);
  sadchan = sadrec + "OBSID";    (*_sad)[sadchan] = sad[sadchan] = "null"; _obssad->push_back(sadchan);
  sadchan = sadrec + "OBSNAME";  (*_sad)[sadchan] = sad[sadchan] = "null"; _obssad->push_back(sadchan);
  // Imaging or Spectroscopy:
  sadchan = sadrec + "OBSMODE";  (*_sad)[sadchan] = sad[sadchan] = "null"; _obssad->push_back(sadchan);
  sadchan = sadrec + "OBSPROGM"; (*_sad)[sadchan] = sad[sadchan] = "null"; _obssad->push_back(sadchan);
  sadchan = sadrec + "OBSERVER"; (*_sad)[sadchan] = sad[sadchan] = "null"; _obssad->push_back(sadchan);
  sadchan = sadrec + "OBSRA";    (*_sad)[sadchan] = sad[sadchan] = "null"; _obssad->push_back(sadchan);
  sadchan = sadrec + "OBSDEC";   (*_sad)[sadchan] = sad[sadchan] = "null"; _obssad->push_back(sadchan);
  sadchan = sadrec + "TELESCOP"; (*_sad)[sadchan] = sad[sadchan] = "Gemini"; _obssad->push_back(sadchan);
  sadchan = sadrec + "UTSTART";  (*_sad)[sadchan] = sad[sadchan] = "null"; _obssad->push_back(sadchan);
  sadchan = sadrec + "UTEND";    (*_sad)[sadchan] = sad[sadchan] = "null"; _obssad->push_back(sadchan);

  //clog<<"UFSADFITS::initSADHash> sad table size: "<<sad.size()<<" / "<<_sad->size()<<endl;
  return (int) _sad->size();
}

int UFSADFITS::fetchAllSAD(std::map< string, string >& sad, const string& instrum) {
  clog<<"UFSADFITS::fetchAllSAD> "<<ends; system("date +%Y:%j:%H:%M:%S");
  if( _sad == 0 ) initSADHash(sad, instrum); 

  int ns = (int)_sad->size();
  if( ns == 0 || sad.size() == 0 ) ns = initSADHash(sad, instrum);

  clog<<"UFSADFITS::fetchAllSAD> nsad: "<<sad.size()<<endl;

  // iterate key == sad channel name, val == caget of record.val
  std::map< string, string >::iterator it = sad.begin();
  for( int n= 0; it != sad.end(); ++n, ++it ) {
    string pvname = it->first;
    string val = "null"; // = it->second;
    chid chan = 0;
    chan = get(&pvname, &val);
    if( chan <= 0 ) {
      clog<<"UFSADFITS::fetchAllSAD> caget failed, pvname: "<<pvname<<endl;
    }
    else {
      sad[pvname] = val;
      //clog<<"UFSADFITS::fetchAllSAD> "<<n<<" : pvname: "<<pvname<<", value: "<<val<<endl;
    }
  }
  clog<<"UFSADFITS::fetchAllSAD> "<<ends; system("date +%Y:%j:%H:%M:%S");
  return ns;
}

UFStrings* UFSADFITS::fitsHeader(std::map< string, string >& sad, const string& datalabel) {
  int ns = fetchAllSAD(sad);
  if( ns <= 0 )
    return 0;

  //clog<<"UFSADFITS::fitsHeader> nsad: "<<sad.size()<<endl;
  string comment = "EpicsSAD: ";
  // iterate key == sad channel name, val == caget of record.val
  string pvname, key, val;
  std::map< string, string >::iterator it = sad.begin();
  while( it != sad.end() ) {
    key = it->first;
    val = it->second;
    pvname = comment + key;
    //clog<<"UFSADFITS::fitsHeader> pv/chan name: "<<key<<", value: "<<val<<endl;
    size_t pos = key.rfind(":");
    if( pos != string::npos ) key = key.substr(++pos);
    //add(key, val, comment);
    replace(key, val, pvname);
    ++it;
  }
  //UFStrings ufs = new UFStrings(datalabel);
  return Strings(datalabel); // allocates new UFStrings...
}

// any agent may use this to set a default list of all sads:
int UFSADFITS::allSAD(std::vector< string >& sad, const string& instrum) {
  sad.clear();
  std::map< string, string > allsad;
  if( _sad == 0 ) {
    int ns = initSADHash(allsad, instrum);
    if( ns <= 0 )
      return 0;
  }
  std::map< string, string >::iterator it = _sad->begin();
  while( it != _sad->end() ) {
    string sadchan = it->first;
    sad.push_back(sadchan);
    ++it;
  }
  return (int) sad.size();
}

// any agent may use this to set a default list of observation sads:
int UFSADFITS::obsSAD(std::vector< string >& sad, const string& instrum) {
  sad.clear();
  std::map< string, string > allsad;
  if( _sad == 0 ) {
    int ns = initSADHash(allsad, instrum);
    if( ns <= 0 )
      return 0;
    if( _obssad == 0 )
      return 0;
  }
  for( size_t i= 0; i < _obssad->size(); ++i ) {
    sad.push_back((*_obssad)[i]);
  }
  return (int) sad.size();
}

// portescap motor indexor agent may use this to set its default list of sads:
int UFSADFITS::ccSAD(std::vector< string >& sad, const string& instrum) {
  sad.clear();
  std::map< string, string > allsad;
  if( _sad == 0 ) {
    int ns = initSADHash(allsad, instrum);
    if( ns <= 0 )
      return 0;
    if( _ccsad == 0 )
      return 0;
  }
  for( size_t i= 0; i < _ccsad->size(); ++i ) {
    sad.push_back((*_ccsad)[i]);
  }
  return (int) sad.size();
}

// mce4 detector control agent may use this to set its default list of sads:
int UFSADFITS::dcSAD(std::vector< string >& sad, const string& instrum) {
  sad.clear();
  std::map< string, string > allsad;
  if( _sad == 0 ) {
    int ns = initSADHash(allsad, instrum);
    if( ns <= 0 )
      return 0;
    if( _dcsad == 0 )
      return 0;
  }
  for( size_t i= 0; i < _dcsad->size(); ++i ) {
    sad.push_back((*_dcsad)[i]);
  }
  return (int) sad.size();
}


// ec status is distributed across multiple agents; agentname disambiguates lists
int UFSADFITS::ecSAD(std::vector< string >& sad, const string& agentname, const string& instrum) {
  sad.clear();
  std::map< string, string > allsad;
  if( _sad == 0 ) {
    int ns = initSADHash(allsad, instrum);
    if( ns <= 0 )
      return 0;
  }
  // env. log file sads:
  string sadchan, sadrec = *_instrum + ":ec:";
  sadchan = sadrec + "LOGHOST"; sad.push_back(sadchan);
  sadchan = sadrec + "LOGFILE"; sad.push_back(sadchan);
  sadchan = sadrec + "LOGSIZE"; sad.push_back(sadchan);

  std::vector< string >* vec;
  string ecname = agentname; UFRuntime::lowerCase(ecname);
  if( ecname.find("all") != string::npos || ecname.find("ls218") != string::npos ) {
    vec = _ecls218sad;
    if( vec != 0 ) {
      for( size_t i= 0; i < vec->size(); ++i )
        sad.push_back((*vec)[i]);
    }
    if( ecname.find("all") == string::npos ) // all done
      return (int) sad.size();
  }

  if( ecname.find("all") != string::npos || ecname.find("ls33") != string::npos ) {
    vec = _ecls33xsad;
    if( vec != 0 && vec->size() > 0 ) {
      for( size_t i= 0; i < vec->size(); ++i )
        sad.push_back((*vec)[i]);
    }
    if( ecname.find("all") == string::npos ) // all done
      return (int) sad.size();
  }

  if( ecname.find("all") != string::npos || ecname.find("plc") != string::npos ) {
    /*
    vec = _ecplcsad;
    if( vec != 0 && vec->size() > 0 ) {
      for( size_t i= 0; i < vec->size(); ++i )
        sad.push_back((*vec)[i]);
    }
    */
    sad.push_back(*_ecplcsad);
    if( ecname.find("all") == string::npos ) // all done
      return (int) sad.size();
  }

  if( ecname.find("all") != string::npos || ecname.find("vac") != string::npos ) {
    vec = _ecpfvacsad;
    if( vec != 0 && vec->size() > 0 ) {
      for( size_t i= 0; i < vec->size(); ++i )
        sad.push_back((*vec)[i]);
    }
    if( ecname.find("all") == string::npos ) // all done
      return (int) sad.size();
  }

  if( ecname.find("all") != string::npos || ecname.find("lvdt") != string::npos ) {
    vec = _eclvdtsad;
    if( vec != 0 && vec->size() > 0 ) {
      for( size_t i= 0; i < vec->size(); ++i )
        sad.push_back((*vec)[i]);
    }
    if( ecname.find("all") == string::npos ) // all done
      return (int) sad.size();
  }

  if( ecname.find("all") != string::npos || ecname.find("barcd") != string::npos ) {
    vec = _ccsymbarcdsad;
    if( vec != 0 && vec->size() > 0 ) {
      for( size_t i= 0; i < vec->size(); ++i )
        sad.push_back((*vec)[i]);
    }
    if( ecname.find("all") == string::npos ) // all done
      return (int) sad.size();
  }

  return (int) sad.size();
}

#endif // __UFSADFITS_cc__

#if !defined(__UFCAwrap_cc__)
#define __UFCAwrap_cc__ "$Id: UFCAwrap.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFCAwrap_cc__;

#include "UFCAwrap.h"
__UFCAwrap_H__(__UFCAwrap_cc__);

#include "stdio.h"
#include "stdlib.h"

// epics alarms
#if defined(__cplusplus)
extern "C" {
#endif
#include "alarmString.h" /* for severity strings */
#if defined(__cplusplus)
}
#endif

/////////////////////////////// helpers //////////////////////////////////////
static struct timeval fifteen_sec = {15, 0};
static struct timeval one_sec =     {0, 999999};

/////////////////////////////// protected statics ////////////////////////////
fdctx* UFCAwrap::_pfdctx = 0;
double UFCAwrap::_pendio = 1.0;
bool   UFCAwrap::_verbose = false;

void UFCAwrap::_caFDCallBack(void *pUser) {
  ::ca_pend_event(1.0e-9);
  return;
}

void UFCAwrap::_fd_register(void *pfdctx, int fd, int opened) {
  if(opened) {
    fdmgr_add_callback((fdctx*) pfdctx, fd, fdi_read, UFCAwrap::_caFDCallBack, 0);
  }
  else {
    fdmgr_clear_callback((fdctx*) pfdctx, fd, fdi_read);
  }
  return;
}

void UFCAwrap::_capollfunc(void *pParam) {
  /* pend for handlers, then issues next timeout */
  ::ca_pend_event(1.0e-9);
  fdmgr_add_timeout(UFCAwrap::_pfdctx, &fifteen_sec, UFCAwrap::_capollfunc, 0);
  return;
}

/////////////////////////////// public statics  ///////////////////////////////
string UFCAwrap::usage("usage: ufcatest -g[et] -p[ut] [name=val]");

void UFCAwrap::defaultMonEventHandler(struct event_handler_args args)  {
  int i;
  /* union db_access_val defined in db_access.h */
  union db_access_val *pBuf;

  /* cast pointer to appropriate type or member of db_access_val */
  pBuf = (union db_access_val *)args.dbr;
  clog<<"UFCAwrap::_defaultMonEventHandler> channel: "<<ca_name(args.chid)<<endl;
  switch (args.type) {
    case DBR_STS_STRING:
      clog<<"UFCAwrap::_defaultMonEventHandler> alarm status, alarm severity = "
          <<alarmStatusString[pBuf->sstrval.status]<<", "
          <<alarmSeverityString[pBuf->sstrval.severity]<<endl;
      
      clog<<"UFCAwrap::_defaultMonEventHandler> current "
	  <<(args.count > 1? "values":"value")<<endl;
      
      for (i = 0; i < args.count; i++) {
        clog<<"UFCAwrap::_defaultMonEventHandler> \t"<< *(&(pBuf->sstrval.value) + i)<<endl;
        if ((i+1)%6 == 0) clog<<"UFCAwrap::_defaultMonEventHandler> "<<endl;
      }
      clog<<"UFCAwrap::_defaultMonEventHandler> "<<endl;
      break;
    case DBR_STS_SHORT:
      clog<<"UFCAwrap::_defaultMonEventHandler> alarm status, alarm severity = "
          <<alarmStatusString[pBuf->sshrtval.status]<<", "
          <<alarmSeverityString[pBuf->sshrtval.severity]<<endl;
      clog<<"UFCAwrap::_defaultMonEventHandler> current: "<<(args.count>1?"values":"value")<<endl;
      for (i = 0; i < args.count; i++){
        clog<<"UFCAwrap::_defaultMonEventHandler> "<<(long)*(&(pBuf->sshrtval.value) + i)<<endl;
        if ((i+1)%8 == 0) clog<<"UFCAwrap::_defaultMonEventHandler> "<<endl;
      }
      clog<<"UFCAwrap::_defaultMonEventHandler> "<<endl;
      break;
    case DBR_STS_FLOAT:
      clog<<"UFCAwrap::_defaultMonEventHandler> alarm status, alarm severity= "
          <<alarmStatusString[pBuf->sfltval.status]<<", "
          <<alarmStatusString[pBuf->sfltval.severity]<<endl;
      clog<<"UFCAwrap::_defaultMonEventHandler> current: "<<(args.count>1?"values":"value")<<endl;
      for (i = 0; i < args.count; i++){
	  clog<<"UFCAwrap::_defaultMonEventHandler> "<<(double)*(&(pBuf->sfltval.value) + i)<<endl;
      
        if ((i+1)%8 == 0) clog<<"UFCAwrap::_defaultMonEventHandler> "<<endl;
      }
      clog<<"UFCAwrap::_defaultMonEventHandler> "<<endl;
      break;
    case DBR_STS_ENUM:
      clog<<"UFCAwrap::_defaultMonEventHandler> alarm status, alarm severity = "
          <<alarmStatusString[pBuf->senmval.status]<<", "
          <<alarmSeverityString[pBuf->senmval.severity]<<endl;
      clog<<"UFCAwrap::_defaultMonEventHandler> current: "<<(args.count>1?"values":"value")<<endl;
      for (i = 0; i < args.count; i++){
        clog<<"UFCAwrap::_defaultMonEventHandler> "<<(long)*(&(pBuf->senmval.value) + i)<<endl;
      }
      clog<<"UFCAwrap::_defaultMonEventHandler> "<<endl;
      break;
    case DBR_STS_CHAR:
      clog<<"UFCAwrap::_defaultMonEventHandler> alarm status, alarm severity = "
          <<alarmStatusString[pBuf->schrval.status]<<", "
          <<alarmSeverityString[pBuf->schrval.severity]<<endl;
      clog<<"UFCAwrap::_defaultMonEventHandler> current: "<<(args.count>1?"values":"value")<<endl;
      for (i = 0; i < args.count; i++){
        clog<<"UFCAwrap::_defaultMonEventHandler> "<<(long) *(&(pBuf->schrval.value) + i)<<endl;
        if ((i+1)%15 == 0) clog<<"UFCAwrap::_defaultMonEventHandler> "<<endl;
      }
      clog<<"UFCAwrap::_defaultMonEventHandler> "<<endl;
      break;
    case DBR_STS_LONG:
      clog<<"UFCAwrap::_defaultMonEventHandler> alarm status, alarm severity = "
          <<alarmStatusString[pBuf->slngval.status]<<", "
          <<alarmSeverityString[pBuf->slngval.severity]<<endl;
      clog<<"UFCAwrap::_defaultMonEventHandler> current: "<<(args.count >1?"values":"value")<<endl;
      for (i = 0; i < args.count; i++){
      clog<<"UFCAwrap::_defaultMonEventHandler> "<<(long)*(&(pBuf->slngval.value)+ i)<<endl;

        if((i+1)%5 == 0) clog<<"UFCAwrap::_defaultMonEventHandler> "<<endl;
      }
      clog<<"UFCAwrap::_defaultMonEventHandler> "<<endl;
      break;
    case DBR_STS_DOUBLE:
      clog<<"UFCAwrap::_defaultMonEventHandler> alarm status, alarm severity = "
          <<alarmStatusString[pBuf->sdblval.status]<<", "
          <<alarmSeverityString[pBuf->sdblval.severity]<<endl;
      clog<<"UFCAwrap::_defaultMonEventHandler> current: "<<(args.count>1?"values":"value")<<endl;
      for (i = 0; i < args.count; i++){
        clog<<"UFCAwrap::_defaultMonEventHandler> "<<(double)*(&(pBuf->sdblval.value) + i)<<endl;
      }
      clog<<"UFCAwrap::_defaultMonEventHandler> "<<endl;
      break;
    default:
      clog<<"UFCAwrap::_defaultMonEventHandler> data type was not dbr_sts_nnn."<<endl;
      break;
  }
  return;
}

// static convenience function for creating a pipe to
// ufcaput child proc.: 
FILE* UFCAwrap::pipeToCAPut() {
  // try to fund ufcaput in a few obvious places:
  // this assumes csh/tcsh is used by popen
  string env = "/usr/bin/env LD_LIBRARY_PATH=/usr/local/lib:/usr/local/epics/lib ";
  string ufcaput;
  char* ufe = getenv("UFINSTALL");
  if( ufe )
    ufcaput = env + ufe + "/bin/ufcaput";
  else
    ufcaput = env + "/usr/local/uf/bin/ufcaput"; // dynamically linked binary

  FILE* ret= 0;
  ret = popen(ufcaput.c_str(), "w");
  if( ret == 0 )
    clog << "FCAwrap::pipeToCAPut> failed to exec. "<<ufcaput<<endl;
  else
    return ret;

  if( ufe )
    ufcaput = env + ufe + "/sbin/ufcaput";
  else
    ufcaput = env + "/usr/local/uf/sbin/ufcaput"; // try static binary
  ret = popen(ufcaput.c_str(), "w");
  if( ret == 0 )
    clog << "FCAwrap::pipeToCAPut> failed to exec. "<<ufcaput<<endl;

  return ret;
}

// static convenience function for writting a line to
// ufcaput (pipe) child proc.: 
int UFCAwrap::writeToCAPut(const FILE* fp, const string& s) {
  string line = s;
  if( line.rfind("\n") == string::npos ) // be sure include new-line
    line += "\n";

  clog<<"UFCAwrap::writeToCAPut> "<<line<<endl;
  return fputs(line.c_str(), (FILE*) fp);
}

///////////////////////////// protected //////////////////////////////
string UFCAwrap::_getCurrent(chid chan, const string& name) {
  memset(_cabuff, 0, sizeof(_cabuff));
  int status = ::ca_get(DBR_STRING, chan, (void *)_cabuff);
  SEVCHK(status, "Error in call to ca_get" );
  status = ::ca_pend_io(_pendio);
  SEVCHK(status, "Get Timed Out?" );
  if (status != ECA_NORMAL) {
    clog<<"Get Timed Out? name= "<<name<<endl;
    return string("Get Timed Out?");
  }

  string retval(_cabuff);
  //clog<<string<<endl;
  //clog<<"Current channel value for "<<name<<" = "<<retval<<endl;
  return retval;
}

int UFCAwrap::_getCurrentArray(chid chan, const string& name, vector< string >& array) {
  int cnt = (int)array.size();
  int ecnt = ca_element_count(chan);
  //clog<<"UFCAwrap::_getCurrentArray> edbname: "<<name<<", ca_elem_cnt: "<<ecnt<<endl;
  if( cnt < ecnt ) cnt = ecnt;
  array.clear();
  dbr_string_t *pdbs = new dbr_string_t[cnt];
  int status = ::ca_array_get(DBR_STRING, cnt, chan, pdbs);
  SEVCHK(status, "Error in call to ca_array_get" );
  status = ::ca_pend_io(_pendio);
  SEVCHK(status, "Get Timed Out?" );
  if (status != ECA_NORMAL) {
    clog<<"Get Timed Out? name= "<<name<<endl;
    return -1;
  }
  for( int i =0; i<cnt; ++i ) {
    //array.push_back(&cabuff[i*(1+MAX_STRING_SIZE)]);
    if( *pdbs[0] != '\0' )
      array.push_back(*pdbs++); 
    else if( pdbs ) {
      //clog<<"UFCAwrap::_getCurrentArray> edbname: "<<name<<", val: "<<pdbs<<endl;
      array.push_back("OK?"); 
    }
  }
  return (int)array.size();
}

////////////////////////////// public ////////////////////////////////
UFCAwrap::UFCAwrap(int argc, char** argv) : _argc(argc), _argv(argv),
                                        _loop(false), hasErrors(0) {
  memset(_cabuff, 0 ,sizeof(_cabuff));
  // ca initialization
  int status;
#if defined(EPICS314)
  status = ::ca_context_create(ca_disable_preemptive_callback); // single thread
  if( status != ECA_NORMAL ) {
    hasErrors++;
    SEVCHK(::ca_context_create(ca_disable_preemptive_callback), "Failed first time ca context create");  
  }
#endif
  status = ::ca_task_initialize();
  if( status != ECA_NORMAL ) {
    hasErrors++;
    SEVCHK(::ca_task_initialize(), "Unable to initialize channel access");  
  }
}

UFCAwrap::~UFCAwrap() {
  chidmap::iterator i=_chids.begin();
  while( i != _chids.end() ) {
    string nm = i->first;
    chid ch = i->second;
    ::ca_clear_channel(ch);
    i++;
  }

  _chids.clear();
  ::ca_task_exit();
}

int UFCAwrap::checkOpt(char* opt) {
  string s(_argv[0]);
 
  //clog<<"UFCAwrap::checkOpt> "<<s<<endl;
 
  if( s.find("loop") != string::npos ) _loop = true;
 
  for(int i=1; i < _argc; i++ ) {
    string arg(_argv[i]);
    if( arg == opt )
      return 1;
  }
  return 0;
}

// from line, return argcnt # beyond -p or -g or -m:
/*
int UFCAwrap::checkOpt(char* opt, const string& line) {
  int optpos = line.find(opt);
  if( optpos == (int)string::npos )
    return 0;

  int retval= 0;
  // count white spaces between args 
  int wpos = line.find(" ", optpos);
  int wprvpos = wpos;
  while( wpos != (int) string::npos ) {
    if( wpos - wprvpos > 1 )
      retval++;
    wprvpos = wpos;
    wpos = line.find(" ", 1+wpos);
  }
  return retval;
}
*/

// from line, return count of options ( -p or -g or -m):
int UFCAwrap::checkOpt(char* opt, const string& line) {
  int optpos = line.find(opt);
  if( optpos == (int)string::npos )
    return 0;

  int cnt = 1;
  do {
    optpos = line.find(opt, 1+optpos);
    if( optpos == (int)string::npos )
      break;
    ++cnt;
  } while( true );
  return cnt;
}

// from argv:
// cnt supports array dbvals
// vcnt should really be fetched from ca_element_count(chanId)...
int UFCAwrap::cmdLine(string*& name, string*& val, int vcnt) {
  name=0;
  val=0;
  int numname = 0;
  // bool loop = false;
  //string* putval=0;
  // algorithm always assume single character option -[A-z] followed by "epicsdbrec.field"
  int numget = checkOpt("-g");
  int nummon = checkOpt("-m");
  int numput = checkOpt("-p");
  numname = numget + nummon + numput;
  name = new string[numname];

  if( checkOpt("-g") || checkOpt("-get") || checkOpt("-m") || checkOpt("-mon") ) {
    if( _argc > 2 ) {
      name = new string[numname];
      if( vcnt > 0 ) val = new string[vcnt];
      for(int i=1; i < _argc; i++ ) {
        string a(_argv[i]);
	if( a == "-g" || a == "-get" || a == "-m" || a == "-mon" ) continue;
        name[numname++] = a;
      }
    }
  }
  else if( checkOpt("-p") || checkOpt("-put")) {
    if( _argc > 2 ) {
      name = new string[numname];
      if( vcnt < numname ) vcnt = numname;
      val = new string[vcnt];
      for(int i=1; i < _argc; i++ ) {
        string a(_argv[i]);
        if( a == "-help" ) {
          clog<<usage<<endl;
	  return numname;
	}
	if( a == "-p" || a == "-put" ) continue;
        int p = a.find("=");
	if( p == (int)string::npos ) {
          clog<<"need = !"<<usage<<endl;
	  return numname;
	}
	name[numname] = a.substr(0, p);
	for( int i = 0; i < vcnt; ++i )
	  val[numname*vcnt+i] = a.substr(p+1, a.length() - p);
	numname++;
      }
    }
    else {
      clog<<"cmdLine> use test default record for put..."<<endl;
    }
  }
  else
    clog<<"no -g or -p ! "<<usage<<endl;

  return numname;
}

// line is expected to look like:
// "-g dbrec1 -g dbrec2 -p dbrec3=something -m dbrec2 ..."
// .i.e. args. MUST be in pairs...
// from line, return argcnt # beyond -p or -g or -m:
int UFCAwrap::cmdLine(const string& cmdline, string*& name, string*& val, int vcnt) {
  int numname= 0;
  name=0;
  val=0;
  //clog<<"UFCAwrap::cmdLine> "<<line<<endl;

  // algorithm always assume single character option -[A-z] followed by "epicsdbrec.field"
  int numget = checkOpt("-g", cmdline);
  int nummon = checkOpt("-m", cmdline);
  int numput = checkOpt("-p", cmdline);
  numname = numget + nummon + numput;
  name = new string[numname];
  if( vcnt > numname )
    val = new string[vcnt];
  else
    val = new string[numname];
    
  if( numget || nummon) {
    for(int i= 0; i<numname; ++i) {
      name[i] = cmdline.substr(2+cmdline.find("-", cmdline.find(" ")));
      char *cs = (char*)name[i].c_str();
      while( *cs == ' ' ) ++cs; // eliminate leading spaces
      name[i] = cs;
    }
    return numname;
  }

  // algorithm below looks for spaces after each 'word', so be sure line ends with space-dash:
  string line = cmdline + " -";

  if( numput <= 0 ) {
    clog<<"no -p or -g or -m ! "<<usage<<endl;
    return 0;
  }

  int wpos= 2 + line.find("-p"); // assume "-p "
  int len= 0, valpos= 0;
  for(int i= 0; i<numput; ++i) {
    valpos = line.find("=", wpos);
    if( valpos == (int)string::npos ) {
      clog<<"UFCAwrap::cmdLine> i= "<<i<<", find '=' failed!"<<endl;
      return 0;
    }
    ++valpos; // first char after =
    len = valpos - wpos - 2; // length of name
    name[i] = line.substr(1+wpos, len);
    wpos = line.find(" -", valpos); // next space+dash
    if( wpos == (int)string::npos ) {
      clog<<"UFCAwrap::cmdLine> i= "<<i<<", find ' -' failed!"<<endl;
      return i;
    }
    len = wpos - valpos; // length of value
    val[i] = line.substr(valpos, len);
    //clog<<"UFCAwrap::cmdLine> i="<<i<<", name="<<name[i]<<", val="<<val[i]<<endl;
  }

  return numname;
}

chid UFCAwrap::searchAndConnect(const string& name) {
  //clog<<"UFCAwrap::searchAndConnect> req. chan name: "<<name<<endl;
  chid chan= 0;
  if( _chids.size() == 0 ) { // need initial context
    //clog<<"UFCAwrap::searchAndConnect> one-time context create..."<<endl;
    int stat = ::ca_context_create(ca_disable_preemptive_callback); // new epics style?
    if( stat != ECA_NORMAL ) {
      clog<<"connectPV> context create failed: "<<name<<endl;
      return chan;
    }
  }
 
  chidmap::iterator i= _chids.find(name);
  bool connected = false;
  if( i != _chids.end() ) { // already connected?
    chan = i->second;
    // double check connection 
    if( ca_state( chan ) != cs_conn ) {
      clog<<"UFCAwrap::searchAndConnect> no longer connected: "<< name<<", reconnnecting..."<<endl;
      _chids.erase(name);
      chan = 0;
    }
    else
      connected = true;
  }
  //  find the specified channel to (re)connect
  if( !connected ) {
    //int stat = ::ca_search_and_connect((char*)name.c_str(), &chan, 0, 0); // old style
    //clog<<"UFCAwrap::searchAndConnect> (ca_create_channel -- new style) connecting: "<<name<<endl;
    int stat = ::ca_create_channel((char*)name.c_str(), 0, 0, 0, &chan); // new epics
    if( stat != ECA_NORMAL ) {
      SEVCHK(stat, "Unable to connect to channel");
    }
    else {
      //::ca_flush_io();
      //clog<<"UFCAwrap::searchAndConnect> ca_pend_io: "<<name<<endl;
      ::ca_pend_io(_pendio); // synchronous completion of search
      if( ca_state( chan ) == cs_conn ) {
        _chids.insert(chidmap::value_type(name, chan));
        connected = true;
      }
    }
  }
  if( connected && _verbose ) {
    clog<<"UFCAwrap::searchAndConnect> succeeded with name: "<<ca_name(chan)
	<<", type: "<<ca_field_type(chan)<<", count: "<<ca_element_count(chan)<<endl;
  }
  else if( !connected && _verbose ) {
    clog<<"UFCAwrap::searchAndConnect> failed with name: "<<ca_name(chan)
	<<", type: "<<ca_field_type(chan)<<", count: "<<ca_element_count(chan)<<endl;
    chan = 0;
  }
  return chan;
}

chid UFCAwrap::get(string* name, string* val, int num) {
  if( name == 0 || val == 0 ) {
    clog<<"UFCAwrap::get> bad parameters..."<<endl;
    return 0;
  }
  chid chan=0;
  string dbrec;
  for( int i = 0; i<num; i++ ) {
    if( name ) dbrec = name[i];
    //  find the specified channel 
    chan = searchAndConnect(dbrec);
    val[i] = _getCurrent(chan, dbrec);
    //clog<<dbrec<<" = "<<val<<endl;
  }

  return chan;
}

int UFCAwrap::getArray(const string& name, vector <string>& vals) {
  chid chan=0;
  // find the specified channel 
  chan = searchAndConnect(name);
  if( chan == 0 ) {
    if( _verbose )
      clog<<" UFCAwrap::getArray> failed to connect to "<<name<<endl;
    return 0;
  }

  return _getCurrentArray(chan, name, vals);
}

chid UFCAwrap::put(string* name_vec, string* put_vec, int num) {
  int status;
  chid chan=0;
  string name;
  string putval;

  for( int i = 0; i<num; i++ ) {
    name = name_vec[i];
    putval = put_vec[i];
    int ppos= 0;
    int rpos = putval.rfind("&&");
    int pos = putval.find("&&");
    if( pos != (int)string::npos ) { // parse into array
      vector< string > v;
      int len = pos - ppos;
      while( pos <= rpos ) {
	if( pos == rpos ) 
	  len = putval.length() - pos;
	else
	  len = pos - ppos;
        v.push_back(putval.substr(ppos, len));
	ppos = 2+pos; pos = putval.find("&&", ppos);
      }
      putArray(name, v);
      continue;
    }
    chan = searchAndConnect(name);
    short etyp = ca_field_type(chan); 
    int len = putval.length();
    len = (len < (int)sizeof(_cabuff)) ? len : (int) sizeof(_cabuff)-1;
    memset(_cabuff, 0, sizeof(_cabuff));
    strncpy(_cabuff, putval.c_str(), len);
    if( dbr_type_is_STRING(etyp) ) { // all genSub inputs
      status = ::ca_put(DBR_STRING, chan, _cabuff);
      //clog<<"UFCAwrap::put> (string): "<<_cabuff<<endl;  
    }
    else if( dbr_type_is_LONG(etyp) || dbr_type_is_SHORT(etyp)) { // heartbeats
      dbr_long_t val = atoi(_cabuff);
      status = ::ca_put(DBR_LONG, chan, &val);
      //clog<<"UFCAwrap::put> (int): "<<val<<endl;  
    }
    else if( dbr_type_is_DOUBLE(etyp) || dbr_type_is_FLOAT(etyp) ) { // anolog
      dbr_double_t val = atof(_cabuff);
      status = ::ca_put(DBR_DOUBLE, chan, &val);
      //clog<<"UFCAwrap::put> (double): "<<val<<endl;  
    }
    else if( dbr_type_is_ENUM(etyp) ) { // binary i/o or CAD enumeration?
      dbr_enum_t val = 0; 
      if( isdigit(_cabuff[0]) )
	val = atoi(_cabuff);
      else if( (_cabuff[0] == 't' || _cabuff[0] == 'T') && 
	       (_cabuff[1] == 'r' || _cabuff[1] == 'R') ) 
	val = 1;
      status = ::ca_put(DBR_ENUM, chan, &val);
      //clog<<"UFCAwrap::put> (enum): "<<(int)val<<endl;  
    }
    SEVCHK(status, "UFCAwrap::put> invalid request");
    //::ca_flush_io();
    status = ::ca_pend_io(_pendio);
    if( status == ECA_TIMEOUT )
      clog<<"UFCAwrap::put> failed."<<endl;  
  }
  return chan;
}
  
int UFCAwrap::putArray(const string& name, vector< string >& vals) {
  chid chan=0;
  // find the specified channel 
  int cpos = name.find(":");
  if( cpos == (int)string::npos ) {
    clog<<"UFCAwrap::putArray> bad db chan name: " <<name<<endl;
    return -1;
  }

  chan = searchAndConnect(name);
  if( chan == 0 ) {
    clog<<" UFCAwrap::getArray> failed to connect to "<<name<<endl;
    return 0;
  }
  int cnt = (int)vals.size(); // just a hint
  short etyp = ca_field_type(chan); 
  int ecnt = ca_element_count(chan);
  if( ecnt <= 0 ) {
    if( _verbose )
      clog<<"UFCAwrap::putArray> element count returned for db chan name: " <<name<<" is = "<<ecnt<<endl;
    return -1;
  }
  if( cnt > ecnt ) {
    cnt = ecnt;
    if( _verbose )
      clog<<"UFCAwrap::putArray> truncating array to fit, epicscnt= "<<ecnt<<", cnt= "<<cnt<<endl;
    return -1;
  }
  dbr_string_t *pdbs = new dbr_string_t[cnt];
  int maxlen = sizeof(dbr_string_t);
  int status;
  for( int i= 0; i < cnt; ++i ) {
    char* cs = (char*) vals[i].c_str();
    int slen = strlen(cs);
    slen = (maxlen <= slen) ? (maxlen-1) : slen; // insure proper length (must be < maxlen-1)
    memset(pdbs[i], 0, maxlen); // insure null-terminated strings
    if( slen > 0 )
      strncpy(pdbs[i], cs, slen); // copy and truncate if necessary
    else {
      clog<<"UFCAwrap::putArray> empty string at index: " <<i<<endl;
      strncpy(pdbs[i], "000", strlen("000"));
    }
  }
  if( dbr_type_is_STRING(etyp) ) { // all genSub inputs
    if( _verbose ) {
      for( int i= 0; i < cnt; ++i )
        clog<<"UFCAwrap::putArray> (string) i, pdbl[i]: "<<i<<", "<<pdbs[i]<<endl;
    }
    status = ::ca_array_put(DBR_STRING, cnt, chan, pdbs);
  }
  else if( dbr_type_is_LONG(etyp) || dbr_type_is_SHORT(etyp) ) { // heartbeats
    dbr_long_t *pdbl = new dbr_long_t[cnt];
    for( int i= 0; i < cnt; ++i ) {
      pdbl[i] = atoi(pdbs[i]);
      if( _verbose )
        clog<<"UFCAwrap::putArray> (int) i, pdbl[i]: "<<i<<", "<<pdbl[i]<<endl;
    }
    status = ::ca_array_put(DBR_LONG, cnt, chan, pdbl);
    delete [] pdbl; // free edb longs
  }
  else if( dbr_type_is_DOUBLE(etyp) || dbr_type_is_FLOAT(etyp) ) { // anologs
    dbr_double_t *pdbd = new dbr_double_t[cnt];
    for( int i= 0; i < cnt; ++i ) {
      pdbd[i] = atof(pdbs[i]);
      if( _verbose )
	clog<<"UFCAwrap::putArray> (double) pdbd[i]: "<<pdbd[i]<<endl;
    }
    status = ::ca_array_put(DBR_DOUBLE, cnt, chan, pdbd);
    delete [] pdbd; // free edb doubles
  }
  else if( dbr_type_is_ENUM(etyp) ) { // binary (bitfield)
    dbr_enum_t *penum = new dbr_enum_t[cnt]; 
    for( int i= 0; i < cnt; ++i ) {
      penum[i] = 0;
      if( isdigit(pdbs[i][0]) )
	penum[i] = atoi(pdbs[i]);
      else if( (pdbs[i][0] == 't' || pdbs[i][0] == 'T') && (pdbs[i][1] == 'r' || pdbs[i][0] == 'R') )
        penum[i] = 1;
      if( _verbose )
        clog<<"UFCAwrap::putArray> (enum) pdbd[i]: "<<(int)penum[i]<<endl;
    }
    status = ::ca_array_put(DBR_ENUM, cnt, chan, penum);
    delete [] penum; // free edb enums
  }

  SEVCHK(status, "Error in call to ca_array_put" );
  status = ::ca_pend_io(_pendio);
  SEVCHK(status, "PutArray Timed Out?" );
  if (status != ECA_NORMAL) {
    clog<<"Get Timed Out? name= "<<name<<endl;
    cnt = -1;
  }

  delete [] pdbs; // free edb strings
  return cnt;
}

void UFCAwrap::mon(string* name_val, int num, monEventHandler handler) {
  /* Set up file descriptor mngr(fdmgr) */
  UFCAwrap::_pfdctx =  fdmgr_init();
  if (!UFCAwrap::_pfdctx) {
    printf("fdmgr_init failed.\n");
    exit(-1);
  }
  /* Get CA file descriptor for fdmgr */
  SEVCHK(::ca_add_fd_registration(UFCAwrap::_fd_register, UFCAwrap::_pfdctx), "fd registration failed.\n");

  fdmgr_add_timeout(UFCAwrap::_pfdctx, &one_sec, _capollfunc, (void *)NULL);

  //int status;
  chid chan=0;
  string name("LakeshoreStatus.VAL");
  for( int i = 0; i<num; i++ ) {
    name = name_val[i];
    chan = searchAndConnect(name);
    chtype type = ca_field_type(chan);
    /* install monitor */
    SEVCHK(ca_add_array_event(dbf_type_to_DBR_STS(type),
           ca_element_count(chan),
           chan,
           handler,
           NULL,
           0.0, 0.0, 0.0,
           (evid *)NULL),
	   NULL);
  }

  while( true ) {
    /* wait for an event or 15 second timeout */
    fdmgr_pend_event(_pfdctx, &fifteen_sec);
    clog<<"UFCAwrap::mon> 15 second timeout in event loop, continueing..."<<endl;
  }

  return;
}

#endif // __UFCAwrap_cc__

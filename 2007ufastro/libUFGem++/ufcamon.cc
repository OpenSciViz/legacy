#if !defined(__UFCAMON_cc__)
#define __UFCAMON_h__ "$Id: ufcamon.cc 14 2008-06-11 01:49:45Z hon $"
static const char rcsId[] = __UFCAMON_h__;

#include "UFCAwrap.h"
__UFCAwrap_H__(__UFCAwrap_cc__)
//#include "alarmString.h" // for severity strings, included in wrapper above */

#include "stdio.h"
#include "cadef.h"
#include "alarmString.h"
#include "sys/time.h"

#define FALSE 0
#define TRUE 1

/* some time values for timeouts */
static struct timeval fifteen_seconds = {15, 0};
static struct timeval one_second      = { 0, 999999};

chid my_chid; /* channel access id */
fdctx *pfdctx; /* file descriptor manager id */


void caFDCallBack(void *pUser) {
  ca_pend((1e-12), FALSE);
  return;
}

void fd_register(void *pfdctx, int fd, int opened) {
  if(opened){
    fdmgr_add_callback((fdctx*) pfdctx, fd, fdi_read, caFDCallBack, NULL);
  }
  else{
    fdmgr_clear_callback((fdctx*) pfdctx, fd, fdi_read);
  }
  return;
}

static void capollfunc(void *pParam) {
  /* pend for handlers, then issues next timeout */
  ca_pend((1e-12), FALSE);
  fdmgr_add_timeout(pfdctx, &fifteen_seconds, capollfunc, (void *)NULL);
  return;
}

void caEventHandler(struct event_handler_args args) {
  int i;
  /* union db_access_val defined in db_access.h */
  union db_access_val *pBuf;

  /* cast pointer to appropriate type or member of db_access_val */
  pBuf = (union db_access_val *)args.dbr;
  printf("channel %s: ", ca_name(args.chid));
  switch (args.type) {
    case DBR_STS_STRING:
      printf("alarm status = %s, alarm severity = %s\n",
          alarmStatusString[pBuf->sstrval.status],
          alarmSeverityString[pBuf->sstrval.severity]);
      printf("current %s:\n", args.count > 1?"values":"value");
      for (i = 0; i < args.count; i++){
        printf("%s\t", *(&(pBuf->sstrval.value) + i));
        if ((i+1)%6 == 0) printf("\n");
      }
      printf("\n");
      break;
    case DBR_STS_SHORT:
      printf("alarm status = %s, alarm severity = %s\n",
          alarmStatusString[pBuf->sshrtval.status],
          alarmSeverityString[pBuf->sshrtval.severity]);
      printf("current %s:\n", args.count > 1?"values":"value");
      for (i = 0; i < args.count; i++){
        printf("%-10d", *(&(pBuf->sshrtval.value) + i));
        if ((i+1)%8 == 0) printf("\n");
      }
      printf("\n");
      break;
    case DBR_STS_FLOAT:
      printf("alarm status = %s, alarm severity = %s\n",
             alarmStatusString[pBuf->sfltval.status],
             alarmStatusString[pBuf->sfltval.severity]);
      printf("current %s:\n", args.count > 1?"values":"value");
      for (i = 0; i < args.count; i++) {
        printf("%-10.4f", *( &(pBuf->sfltval.value) + i ) );
        if ((i+1)%8 == 0) printf("\n");
      }
      printf("\n");
      break;
    case DBR_STS_ENUM:
      printf("alarm status = %s, alarm severity = %s\n",
          alarmStatusString[pBuf->senmval.status],
          alarmSeverityString[pBuf->senmval.severity]);
      printf("current %s:\n", args.count > 1?"values":"value");
      for (i = 0; i < args.count; i++){
        printf("%d ", *(&(pBuf->senmval.value) + i));
      }
      printf("\n");
      break;
    case DBR_STS_CHAR:
      printf("alarm status = %s, alarm severity = %s\n",
          alarmStatusString[pBuf->schrval.status],
          alarmSeverityString[pBuf->schrval.severity]);
      printf("current %s:\n", args.count > 1?"values":"value");
      for (i = 0; i < args.count; i++){
        printf("%-5d", *(&(pBuf->schrval.value) + i));
        if ((i+1)%15 == 0) printf("\n");
      }
      printf("\n");
      break;
    case DBR_STS_LONG:
      printf("alarm status = %s, alarm severity = %s\n",
          alarmStatusString[pBuf->slngval.status],
          alarmSeverityString[pBuf->slngval.severity]);
      printf("current %s:\n", args.count > 1?"values":"value");
      for (i = 0; i < args.count; i++){
        printf("%-15d", *(&(pBuf->slngval.value) + i));
        if((i+1)%5 == 0) printf("\n");
      }
      printf("\n");
      break;
    case DBR_STS_DOUBLE:
      printf("alarm status = %s, alarm severity = %s\n",
          alarmStatusString[pBuf->sdblval.status],
          alarmSeverityString[pBuf->sdblval.severity]);
      printf("current %s:\n", args.count > 1?"values":"value");
      for (i = 0; i < args.count; i++){
        printf("%-15.4f", *(&(pBuf->sdblval.value) + i));
      }
      printf("\n");
      break;
    default:
      printf("data type was not dbr_sts_nnn.\n");
      break;
  }
  return;
}

int main(int argc, char **argv) {
  unsigned short new_set;
  char *name;
  chtype type;
  int status, cnt;

  /* initialize channel access */
  SEVCHK(ca_task_initialize(), "ca_task_initialize: CA init failure.\n");

  /* Set up file descriptor mngr(fdmgr) */
  pfdctx =  fdmgr_init();
  if (!pfdctx) {
    printf("fdmgr_init failed.\n");
    exit(-1);
  }

  /* Get CA file descriptor for fdmgr */
  SEVCHK(ca_add_fd_registration(fd_register, pfdctx), "fd registration failed.\n");

  fdmgr_add_timeout(pfdctx, &one_second, capollfunc, (void *)NULL);

  new_set = 1;
  if(argc != 2) {
    printf("usage: ufcamon <channel_name>\n");
    exit (-1);
  }
  name = argv[1];

  /* loop indefinitely until user terminates process */
  for(;;) {
    if (new_set) {
    /* establish connection */
    SEVCHK(ca_search(name, &my_chid), NULL);
    status = ca_pend_io(0.5);
    //status = ca_pend_event(0.5);
    if (status == ECA_TIMEOUT) {
      printf("not connected\n");
      //exit(-1);
    }
    else {
      type = ca_field_type(my_chid); cnt = ca_element_count(my_chid);
      printf("%s: connected, type= %d, cnt= %d\n", ca_name(my_chid), (int)type, cnt);
      // install monitor
      SEVCHK(ca_add_array_event(dbf_type_to_DBR_STS(type),
            1, // ca_element_count(my_chid),
            my_chid,
            caEventHandler,
            NULL,
            0.0, 0.0, 0.0,
            (evid *)NULL),
            NULL);
    }
    new_set = 0;
    }
    /* wait for an event or 15 second timeout */
    fdmgr_pend_event(pfdctx, &fifteen_seconds);
  }
  return 0;
}

#endif // __UFCAMON_H__

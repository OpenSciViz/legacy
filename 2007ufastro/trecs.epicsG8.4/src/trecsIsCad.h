
/* INDENT OFF */

/*+
 *
 * FILENAME
 * -------- 
 * trecsIsCad.h
 *
 * PURPOSE
 * -------
 * Declare public CAD record support functions
 * 
 * FUNCTION NAME(S)
 * ----------------
 * 
 * DEPENDENCIES
 * ------------
 *
 * LIMITATIONS
 * -----------
 * 
 * AUTHOR
 * ------
 * William Rambold (wrambold@gemini.edu)
 * 
 * HISTORY
 * -------
 * 2000/12/11  WNR  Initial coding
 */

/* INDENT ON */

/* ===================================================================== */


/*
 *  Module level definitions
 */

#define TRX_MAX_QL_STREAMS       14


/*
 *
 *  Public function prototypes
 *
 */

long trecsIsAbortProcess (cadRecord * pcr);
long trecsIsDataModeProcess (cadRecord * pcr);
long trecsIsDatumProcess (cadRecord * pcr);
long trecsIsDebugProcess (cadRecord * pcr);
long trecsIsInitProcess (cadRecord * pcr);
long trecsIsInsSetupProcess (cadRecord * pcr);
long trecsIsNullInit (cadRecord * pcr);
long trecsIsObsSetupProcess (cadRecord * pcr);
long trecsIsObserveProcess (cadRecord * pcr);
long trecsIsParkProcess (cadRecord * pcr);
long trecsIsRebootInit (cadRecord * pcr);
long trecsIsRebootProcess (cadRecord * pcr);
long trecsIsSetDhsInfoProcess (cadRecord * pcr);
long trecsIsSetWcsProcess (cadRecord * pcr);
long trecsIsStopProcess (cadRecord * pcr);
long trecsIsTestProcess (cadRecord * pcr);

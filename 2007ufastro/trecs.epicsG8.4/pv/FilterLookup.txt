#
#  T-ReCS filter name to wheel position translation table, with default Window
#


# Filter Name		Filter 1	Filter 2		Window

Si1-7.9um		Si1-7.9um	Open			ZnSe  
Si2-8.8um		Si2-8.8um	Open			ZnSe
Si3-9.7um		Si3-9.7um	Open			ZnSe
Si4-10.4um		Si4-10.4um	Open			ZnSe
Si5-11.7um		Si5-11.7um	Open			ZnSe
Si6-12.3um		Si6-12.3um	Open			ZnSe
PAH1-8.6um		PAH1-8.6um	Open			ZnSe
PAH2-11.3um		PAH2-11.3um	Open			ZnSe
ArIII-9.0um		ArIII-9.0um	Open			ZnSe
K			Open		K			ZnSe
L			Open		L			ZnSe     
M			Open		M			ZnSe      
N			Open		N			ZnSe 
NeII-12.8um	 	Open		NeII-12.8um		ZnSe 
NeII_ref2-13.1um 	Open		NeII_ref2-13.1um	ZnSe 
SIV-10.5um		Open		SIV-10.5um		ZnSe 
Qs-18.3um		Open		Qs-18.3um		KBr
Qone-17.8um		Open		Qone-17.8um		KBr
Ql-24.5um		Open		Ql-24.5um		KRS-5
Qw-20.8um		Qw-20.8um	Open			KRS-5
Align-Spot		Align-Spot	Align-Spot		ZnSe
Block			Block		Block			ZnSe

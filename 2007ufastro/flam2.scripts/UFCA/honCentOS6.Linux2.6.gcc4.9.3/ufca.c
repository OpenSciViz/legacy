#if !defined(__UFCA_C__)
#define __UFCA_C__ "$Name:  $ $Id: ufca.c,v 0.41 2007/02/01 19:18:45 hon Exp $"
const char rcsId[] = __UFCA_C__;

/* #define __USE_UNIX98 1 */
#define _XOPEN_SOURCE 500
#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "pthread.h"
#include "signal.h"
#include "errno.h"

#include "ufca.h"
#include "db_access.h"

#define _UFCAMAXMON_ 10000
#ifndef PTHREAD_THREADS_MAX
#define PTHREAD_THREADS_MAX 64
#endif

/* fix problem on linux? */
#ifndef PTHREAD_MUTEX_RECURSIVE
#define PTHREAD_MUTEX_RECURSIVE PTHREAD_MUTEX_RECURSIVE_NP
#endif


static double _Timeout = 1.0;
static size_t _Subscriptions = 0;
/* chid of pv monitor[i], 0 <= i < UFCAMAXMON */
static chid _monchan[_UFCAMAXMON_];
/* number of times monitor has triggered since last get of value*/
static int _moncnt[_UFCAMAXMON_];
static int _CAContext= -1;
//static int locks=0, unlocks=0;
static int _threadIDs[PTHREAD_THREADS_MAX];
static char _threadMEM[PTHREAD_THREADS_MAX][40];


/* brute-force thread-saftey via single mutex */
static pthread_mutex_t _mutex, *_pmutex= 0;

static int _lock(pthread_mutex_t* pmutex) {
#if !defined(_REENTRANT)
  return 0;
#endif
  if( pmutex == 0 ) return 0;
  /* printf("UFCA::_lock> lock count: %d\n", ++locks); */
  return pthread_mutex_lock(pmutex);
}

static int _trylock(pthread_mutex_t* pmutex) {
#if !defined(_REENTRANT)
  return 0;
#endif
  if( pmutex == 0) return 0;
  return pthread_mutex_trylock(pmutex);
}

static int _unlock(pthread_mutex_t* pmutex) {
  int retval = 0;
#if !defined(_REENTRANT)
  return retval;
#endif
  if( pmutex == 0 ) return retval;
  /* printf("UFCA::_unlock> unlock count: %d\n",++unlocks); */
  retval = pthread_mutex_unlock(pmutex);
  if( retval != 0 ) {
    printf("UFCA::_unlock> unlock retval: %s\n", strerror(errno));
  }
  return retval;
}

static int _idxOfThread(pthread_t threadId) {
  int stat, idx = 0;
  stat = _lock(_pmutex);
  if( stat != 0 ) {
    fprintf(stderr,"UFCA> mutex lock failed, thread: %d\n", (int)pthread_self());
    return -1;
  }
  /* try to find idx for currently active thread */
  while (idx < PTHREAD_THREADS_MAX) {
    if (_threadIDs[idx] == threadId) {
      if (_unlock(_pmutex) != 0){
	printf("UFCA::_idxOfThread> Unlock failed!\n");
      }
      return idx;
    }
    idx++;
  }
  idx = 0;
  /* must be a new thread, find a place for it */
  while (idx < PTHREAD_THREADS_MAX) {
    if (_threadIDs[idx] == 0){
      _threadIDs[idx] = threadId;
      if (_unlock(_pmutex) != 0) {
	printf("UFCA::_idxOfThread> Unlock failed!\n");
      }
      return idx;
    }
    /* test thread to see if its still alive */
    /* using 0 will only test thread, won't actually */
    /* send the kill signal */
    stat = pthread_kill(_threadIDs[idx],SIGCONT);
    if (stat != 0) {
      /* we found a thread that no longer exists, lets */
      /* steal his place in line */
      _threadIDs[idx] = threadId;
      if (_unlock(_pmutex)!= 0) {
	printf("UFCA::_idxOfThread> Unlock failed!\n");
      }
      return idx;
    }
    idx++;
  }
  if (_unlock(_pmutex)!= 0)
    printf("UFCA::_idxOfThread> Unlock failed!\n");

  return -1;
}

static int _idxOfChan(chid chan) {
  int stat, idx = 0;

  /* printf ("_idxOfChan>  chan: %d\n",(int)chan); */
  if( chan == 0 )
    return -1;

  /* printf("locking...\n"); */
  stat = _lock(_pmutex);
  /* printf("locked!\n"); */
  if( stat != 0 ) {
    fprintf(stderr,"UFCA> mutex lock failed, thread: %d\n", (int)pthread_self());
    return 0;
  }

  if( _Subscriptions <= 0 ) {
    if (_unlock(_pmutex)!= 0) {
      printf("UFCA::_idxOfChan> Unlock failed!\n");
    }
    return -1;
  }
  while( idx < _Subscriptions ) {
    /* printf("_idxOfChan> idx: %d, chan: %d, _monchan[idx]: %d, subscriptions: %d\n",
               idx,(int)chan,(int)_monchan[idx],_Subscriptions); */
    if( chan == _monchan[idx] ) {
      /* printf("_idxOfChan> done, breaking.\n"); */
      break;
    }
    ++idx;
  }
  if( idx >= _Subscriptions ) {
    /* printf("_idxOfChan> subscriptions: %d, no channel idx...\n", _Subscriptions); */
    if (_unlock(_pmutex)!= 0) {
      printf("UFCA::_idxOfChan> Unlock failed!\n");
    }
    return -1;
  }
  
  /* printf("_idxOfChan> subscriptions: %d, channel idx: %d, name: %s\n",
            _Subscriptions, idx, getPVName(chan)); */
  
  if (_unlock(_pmutex)!= 0)
    printf("UFCA::_idxOfChan> Unlock failed!\n");

  return idx;
}

char* getPVName(chid chan) {
  char* pvname= 0;
  int stat= 0, _pidx = _idxOfThread(pthread_self());
  if (_pidx == -1) {
    fprintf(stderr,"UFCA> unable to grab mem for thread\n");
    return pvname;
  }
  pvname = _threadMEM[_pidx];
  stat = _lock(_pmutex);
  memset(_threadMEM[_pidx], 0, sizeof(_threadMEM[_pidx])); 
  if( stat != 0 ) {
    fprintf(stderr,"UFCA> mutex lock failed, thread: %d\n", (int)pthread_self());
    return pvname;
  }

  if( chan == 0 ) {
    if (_unlock(_pmutex)!= 0) {
      printf("UFCA::getPVName> Unlock failed!\n");
    }
    return pvname;
  }

  pvname = (char*) ca_name(chan);
  if( pvname ) {
    memset(_threadMEM[_pidx], 0, sizeof(_threadMEM[_pidx])); 
    strncpy(_threadMEM[_pidx], pvname, sizeof(_threadMEM[_pidx]));
  }
  if (_unlock(_pmutex)!= 0)
    printf("UFCA::getPVName> Unlock failed!\n");

  return pvname;
}

int eventMon(chid chan) {
  int stat= 0, idx = 0, m= -1;
  /*
  int stat = _lock(_pmutex);
  if( stat != 0 ) {
    fprintf(stderr,"UFCA> mutex lock failed, thread: %d\n", (int)pthread_self());
    return -1;
    }
  */
  /* printf("ca_polling...\n"); */
  stat = ca_poll();
 
  /* _unlock(_pmutex); */
  idx = _idxOfChan(chan);
  m = -1;
  if( idx < 0  ) {
    fprintf(stderr,"UFCA.eventMon> Negative idx. Returning -1\n");
    return m;
  }

  stat = _lock(_pmutex);
  if( stat != 0 ) {
    fprintf(stderr,"UFCA> mutex lock failed, thread: %d\n", (int)pthread_self());
    return m;
  }

  m = _moncnt[idx];
  if (_unlock(_pmutex) != 0)
    printf("UFCA::eventMon> Unlock failed!\n");

  return m;
}
 
int nullPV (chid chan) {
  return ( chan == 0 ? 1 : 0 );
}

chid connectPV(const char* name, double timeout) {
  chid chan= 0;
  int stat= -1;
  char* caname= 0;
  static pthread_mutexattr_t mattr; /* one time usage on mutex init... */

  /*  printf("connectPV> channel name: %s\n", name); */
  /* #if _REENTRANT > 0 */
#if defined(_REENTRANT) 
  if( _pmutex == 0 ) {
    pthread_mutexattr_settype(&mattr, PTHREAD_MUTEX_RECURSIVE | PTHREAD_PROCESS_SHARED );
    pthread_mutexattr_setpshared(&mattr, PTHREAD_PROCESS_SHARED);
    _pmutex = &_mutex; /* faster and safer than heap alloc */
    pthread_mutex_init(_pmutex, &mattr);
  }
#endif
  stat = _lock(_pmutex);  
  if( stat != 0 ) {
    fprintf(stderr,"connectPV> mutex lock failed: %s, thread: %d\n", name, (int)pthread_self());
    return 0;
  }
  if( _CAContext < 0 ) {
    stat = ca_context_create(ca_disable_preemptive_callback);
    if( stat != ECA_NORMAL ) {
      fprintf(stderr,"connectPV> context create failed: %s\n", name);
      _unlock(_pmutex);
      return 0;
    }
    _CAContext++;
  }
  stat = ca_create_channel(name, 0, 0, 0, &chan);
  if( stat != ECA_NORMAL ) {
    fprintf(stderr,"connectPV> failed: %s.  CPtr value: %d\n", name,(int)chan);
    /* if (chan) ca_clear_channel(chan); */
    _unlock(_pmutex);
    return 0;
  }

  caname = (char*) ca_name(chan);
  _Timeout = timeout; /* allow reset of timeout on each connect */
  /* unlock mutex here to avoid monitor callback deadlocks */
  if (_unlock(_pmutex) != 0){
    printf("UFCA::connectPV> Unlock failed!\n");
  }
  stat = ca_pend_io(timeout);
  if( stat != ECA_NORMAL ) {
    fprintf(stderr,"connectPV> pend_io failed: %s. CPtr value: %d\n", name,(int)chan);
    /*if (chan) ca_clear_channel(chan);*/
    /* _unlock(_pmutex); */
    return 0;
  }
  
  /* printf("connect> chan: %x, name: %s\n", (int)chan, caname); */
  
  /* _unlock(_pmutex); */
  return chan;
}

void disconnectPV(chid chan) {
  int stat= 0;
  if( chan == 0 )
    return;

  /* printf("disconnectPV> channel name: %s\n", name); */
  stat = ca_clear_channel(chan);
  if( stat != ECA_NORMAL ) {
    fprintf(stderr,"disconnectPV> failed\n");
    return;
  }
  stat = ca_pend_io(_Timeout);
  if( stat != ECA_NORMAL ) {
    fprintf(stderr,"disconnectPV> failed\n");
    return;
  }

  return;
}

static void _monitor(struct event_handler_args event) {
  /* printf("_monitor> starting\n"); */
  int stat= 0, idx = _idxOfChan(event.chid);
  if( idx < 0  ) {
    fprintf(stderr,"UFCA._monitor> negative idx, chan not connected\n");
    return;
  }
  stat = _lock(_pmutex);
  
  if( stat != 0 ) {
    fprintf(stderr,"connectPV> mutex lock failed: %d, thread: %d\n",stat , (int)pthread_self());
    return;
  }
  
  _moncnt[idx] = 1 + _moncnt[idx];
  
  /* printf("_monitor> moncnt: %d, for chan: %s\n", _moncnt[idx], getPVName(_monchan[idx])); */
  
  if (_unlock(_pmutex) != 0)
    printf("UFCA::_monitor> Unlock failed!\n");

  return;
}

chid subscribe(chid chan) {
  double timeout = _Timeout;
  int stat, idx= _idxOfChan(chan);
  char* name= getPVName(chan);

  /* printf("subscribe> chan pointer: %d\n",(int)chan); */

  if( idx >= 0 ) { /* already subscribed to this channel */
    /* printf("subscribe> subscribed: %s idx: %d\n pointer: %d", name,idx,(int)chan); */
    return chan;
  }
  stat = _lock(_pmutex);
  if( stat != 0 ) {
    fprintf(stderr,"UFCA> mutex lock failed: %s, thread: %d\n", name, (int)pthread_self());
    return 0;
  }

  idx = _Subscriptions++;
  if( _Subscriptions >= _UFCAMAXMON_ ) {
    fprintf(stderr,"subscribe> sorry maxed out subscriptions!\n");
    _unlock(_pmutex);
    return 0;
  }

  stat = ca_create_subscription(DBR_INT, 0, chan, DBE_VALUE|DBE_ALARM, _monitor, 0, 0);
  if( stat != ECA_NORMAL ) {
    fprintf(stderr,"subscribe> failed: %s\n", name);
    _unlock(_pmutex);
    return 0;
  }

  _moncnt[idx] = 0;
  _monchan[idx] = chan;

  /* unlock mutex here to avoid monitor callback deadlocks */
  if (_unlock(_pmutex)!= 0) {
    printf("UFCA::subscribe> Unlock failed!\n");
  }

  stat = ca_pend_io(timeout);
  if( stat != ECA_NORMAL ) {
    fprintf(stderr,"subscribe> pend_io failed: %s\n", name);
    /* _unlock(_pmutex); */
    return 0;
  }

  /* printf("subscribe> %s, current number of subscriptions: %d\n", name, _Subscriptions); */
  /* _unlock(_pmutex); */
  return chan;
}

int unsubscribe(chid chan) {
  /* ca_clear_subscription(*PEVID);*/
  return 0;
}

int getType(chid chan) {
  double timeout= _Timeout;
  int stat= 0, typ= ca_field_type(chan);
  if( chan == 0 )
    return 0;

  timeout = _Timeout;
  typ = ca_field_type(chan);
  if( typ == TYPENOTCONN ) {
    fprintf(stderr,"getType> failed\n");
    return 0;
  }
   
  stat = ca_pend_io(timeout);
  if( stat != ECA_NORMAL ) {
    fprintf(stderr,"getType> pend_io failed\n");
    return 0;
  }
  /* printf("getType> chan: %s, type (enum): %d, %s \n", ca_name(chan), typ, dbr_type_to_text(typ)); */
  return typ;
}

char* getTypeStr(chid chan) {
  double timeout = _Timeout;
  static char es[40], ee[40];
  const char* styp;
  int typ= 0, stat= _lock(_pmutex);
  memset(ee, 0, sizeof(ee));
  if( stat != 0 ) {
    fprintf(stderr,"UFCA> mutex lock failed, thread: %d\n", (int)pthread_self());
    return (char*) ee;
  }
  if( chan == 0 ) {
    if (_unlock(_pmutex)!= 0) {
      printf("UFCA::getTypeStr> Unlock failed!\n");
    }
    return (char*) ee;
  }

  typ = ca_field_type(chan);
  if( typ == TYPENOTCONN ) {
    _unlock(_pmutex);
    fprintf(stderr,"getType> failed\n");
    return (char*) ee;
  }
  
  /* unlock mutex here to avoid monitor callback deadlocks */
  if (_unlock(_pmutex)!= 0) {
    printf("UFCA::getTypeStr> Unlock failed!\n");
  }
  stat = ca_pend_io(timeout);
  if( stat != ECA_NORMAL ) {
    fprintf(stderr,"getType> pend_io failed\n");
    return (char*) ee;
  }

  stat = _lock(_pmutex);
  if( stat != 0 ) {
    fprintf(stderr,"UFCA> mutex lock failed, thread: %d\n", (int)pthread_self());
    return (char*) ee;
  }

  styp = dbr_type_to_text(typ);
  memset(es, 0, sizeof(es));
  strcpy(es, styp);
  /* printf("getType> chan: %s, type (enum): %d, %s \n", ca_name(chan), typ, styp); */
  if (_unlock(_pmutex)!= 0)
    printf("UFCA::getTypeStr> Unlock failed!\n");

  return (char*) es;
}

int getElemCnt(chid chan) {
  double timeout = _Timeout;
  int stat, cnt = ca_element_count(chan);
  if( chan == 0 )
    return 0;

  if( cnt == 0 ) {
    fprintf(stderr,"getElemCnt> failed: %s\n", ca_name(chan));
    return 0;
  }
   
  stat = ca_pend_io(timeout);
  if( stat != ECA_NORMAL ) {
    fprintf(stderr,"getElemCnt> pend_io failed: %s\n", ca_name(chan));
    return 0;
  }
  /* printf("getElemCnt> chan: %s, elements: %d\n", ca_name(chan), cnt); */
  return cnt;
}

char* getString(chid chan) {
  double timeout = _Timeout;
  int _pidx = -1;
  int idx= 0, stat= 0;

  _pidx = _idxOfThread(pthread_self());
  if (_pidx == -1) {
    fprintf(stderr,"UFCA::getString> failed to get thread mem\n");
    return 0;
  }

  stat = _lock(_pmutex);
  memset(_threadMEM[_pidx], 0, sizeof(_threadMEM[_pidx])); 
  if( stat != 0 ) {
    fprintf(stderr,"UFCA> mutex lock failed, thread: %d\n", (int)pthread_self());
    return _threadMEM[_pidx];
  }
  if (_unlock(_pmutex)!= 0) {
    printf("UFCA::getString> Unlock failed!\n");
  }
  if( chan == 0 ) {
    return _threadMEM[_pidx];
  }
  idx= _idxOfChan(chan);
  stat = _lock(_pmutex);
  if( stat != 0 ) {
    fprintf(stderr,"UFCA> mutex lock failed, thread: %d\n", (int)pthread_self());
    return _threadMEM[_pidx];
  }
  if( idx >= 0 )
    _moncnt[idx] = 0; /* clear monitor counter */

  memset(_threadMEM[_pidx], 0, sizeof(_threadMEM[_pidx])); 
  stat = ca_array_get(DBR_STRING, 1u, chan, _threadMEM[_pidx]);
  if( stat != ECA_NORMAL ) {
    fprintf(stderr,"getString> failed! chan: %s, val: %s\n", ca_name(chan), _threadMEM[_pidx]);
    _unlock(_pmutex);
    return _threadMEM[_pidx];
  }
  /* unlock mutex here to avoid ca monitor callback deadlocks */
  if (_unlock(_pmutex)!= 0) {
    printf("UFCA::getString> Unlock failed!\n");
  }
  stat = ca_pend_io(timeout);
  if( stat != ECA_NORMAL ) {
    fprintf(stderr,"getString> pend_io failed! chan: %s, val: %s\n", ca_name(chan), _threadMEM[_pidx]);
    /* _unlock(_pmutex); */
    return _threadMEM[_pidx];
  }

  /* printf("getString> chan: %s, val: %s\n", ca_name(chan), es); */
  /* _unlock(_pmutex); */
  return _threadMEM[_pidx];
}

int getInt(chid chan) {
  double timeout = _Timeout;
  int stat, ival= 0, idx= 0, typ= 0;
  char* sval= 0;
  if( chan == 0 )
    return 0;

  //_unlock(_pmutex);
  idx = _idxOfChan(chan);
  typ= getType(chan);

  stat= _lock(_pmutex);
  if( stat != 0 ) {
    fprintf(stderr,"UFCA> mutex lock failed, thread: %d\n", (int)pthread_self());
    return ival;
  } 
  if( idx >= 0 )
    _moncnt[idx] = 0; /* clear monitor counter */
  _unlock(_pmutex);

  switch(typ) {
  case DBF_FLOAT:
  case DBF_DOUBLE: 
    ival = (int) getDouble(chan);
    return ival;
    break;
  case DBF_STRING:
    sval = getString(chan);
    ival = atoi(sval);
    /* printf("getInt> chan: %s, ival: %d, sval: %s\n", ca_name(chan), ival, sval); */
    return ival;
    break;
  default: break;
  }
  /* must be native int */
  stat = ca_array_get(DBR_INT, 1u, chan, &ival);
  if( stat != ECA_NORMAL ) {
    fprintf(stderr,"getInt> failed! chan: %s, val: %d\n", ca_name(chan), ival);
    return ival;
  }
  stat = ca_pend_io(timeout);
  if( stat != ECA_NORMAL ) {
    /* _unlock(_pmutex); */
    fprintf(stderr,"getInt> pend_io failed! chan: %s, val: %d\n", ca_name(chan), ival);
    return ival;
  }
  /* printf("getInt> chan: %s, val: %d\n", ca_name(chan), i); */
  return ival;
}

double getDouble(chid chan) {
  double dval= 0, timeout = _Timeout;
  int stat= 0, idx= 0, typ= 0;
  char* sval= 0;
  if( chan == 0 )
    return dval;

  
  idx= _idxOfChan(chan);
  typ= getType(chan);

  stat= _lock(_pmutex);
  if( stat != 0 ) {
    fprintf(stderr,"UFCA> mutex lock failed, thread: %d\n", (int)pthread_self());
    return dval;
  }
  if( idx >= 0 )
    _moncnt[idx] = 0; /* clear monitor counter */
  _unlock(_pmutex);

  switch(typ) {
  case DBF_STRING:
    sval = getString(chan);
    _lock(_pmutex);
    dval = strtod(sval, 0);
    /* printf("getDouble> chan: %s, dval: %f, sval: %s\n", ca_name(chan), dval, sval); */
    _unlock(_pmutex);
    return dval;
    break;
  case DBF_CHAR:
  case DBF_INT:
  case DBF_LONG:
    dval = (double) getInt(chan);
    return dval;
    break;
  default: break;
  }

  /* must be native double */
  stat = ca_array_get(DBR_DOUBLE, 1u, chan, &dval);
  if( stat != ECA_NORMAL ) {
    fprintf(stderr,"getDouble> failed! chan: %s, val: %f\n", ca_name(chan), dval);
    return 0.0;
  }
  stat = ca_pend_io(timeout);
  if( stat != ECA_NORMAL ) {
    fprintf(stderr,"getDouble> pend_io failed! chan: %s, val: %f\n", ca_name(chan), dval);
    /* _unlock(_pmutex); */
    return 0.0;
  }
  /* printf("getDouble> chan: %s, val: %f\n", ca_name(chan), d); */
  /* _unlock(_pmutex);*/
  return dval;
}

char* getAsString(chid chan) {
  int stat= 0, typ= 0, i=0;
  int _pidx = -1;
  double d = 0;
  if( chan == 0 )
    return 0;
  _pidx = _idxOfThread(pthread_self());
  if (_pidx == -1) {
    fprintf(stderr,"UFCA::getAsString> failed to get thread mem\n");
    return 0;
  }

  typ= getType(chan);
  stat= _lock(_pmutex);
  if( stat != 0 ) {
    fprintf(stderr,"UFCA> mutex lock failed, thread: %d\n", (int)pthread_self());
    return _threadMEM[_pidx]; 
  }
  memset(_threadMEM[_pidx], 0, sizeof(_threadMEM[_pidx]));

  switch(typ) {
  case DBF_ENUM:
  case DBF_STRING:
    _unlock(_pmutex);
    return getString(chan);
    break;
  case DBF_FLOAT:
  case DBF_DOUBLE: 
    memset(_threadMEM[_pidx], 0, sizeof(_threadMEM[_pidx]));
    _unlock(_pmutex);
    d = getDouble(chan);
    _lock(_pmutex);
    sprintf(_threadMEM[_pidx], "%f", d);
    break;
  case DBF_CHAR:
  case DBF_INT:
  case DBF_LONG:
    memset(_threadMEM[_pidx], 0, sizeof(_threadMEM[_pidx]));
    _unlock(_pmutex);
    i = getInt(chan);
    _lock(_pmutex);
    sprintf(_threadMEM[_pidx], "%d",i);
    break;
  default: 
    memset(_threadMEM[_pidx], 0, sizeof(_threadMEM[_pidx]));
    sprintf(_threadMEM[_pidx], "%d", typ);
    break;
  }

  _unlock(_pmutex);
  return _threadMEM[_pidx];    
}

int putInt(chid chan, int i) { 
  char es[40];
  double timeout = _Timeout, d= i;
  int stat= 0, typ= 0;
  char* caname= 0;
  if( chan == 0 )
    return 0;


  caname = (char*) ca_name(chan);
  typ = getType(chan);

  stat = _lock(_pmutex);
  if( stat != 0 ) {
    fprintf(stderr,"UFCA> mutex lock failed, thread: %d\n", (int)pthread_self());
     return 0;
  }

  switch(typ) {
  case DBF_CHAR:
  case DBF_INT:
  case DBF_LONG:
    stat = ca_array_put(DBR_INT, 1u, chan, &i);
    if( stat != ECA_NORMAL ) {
      fprintf(stderr,"putInt> failed! chan: %s, val: %d\n", caname, i);
      _unlock(_pmutex);
      return 0;
    }
    break;
  case DBF_FLOAT:
  case DBF_DOUBLE: 
    stat = ca_array_put(DBR_DOUBLE, 1u, chan, &d);
    if( stat != ECA_NORMAL ) {
      fprintf(stderr,"putDouble> failed! chan: %s, val: %f\n", caname, d);
      _unlock(_pmutex);
      return 0;
    }
    break;
  case DBF_STRING:
  default: 
    memset(es, 0, sizeof(es));
    sprintf(es, "%d", i);
    stat = ca_array_put(DBR_STRING, 1u, chan, (char*)es);
    if( stat != ECA_NORMAL ) {
      fprintf(stderr,"putDouble> failed! chan: %s, val: %f\n", caname, d);
      _unlock(_pmutex);
      return 0;
    }
    break;
  }
  _unlock(_pmutex);

  stat = ca_pend_io(timeout);
  if( stat != ECA_NORMAL ) {
    fprintf(stderr,"putInt> pend_io failed! chan: %s, val: %d\n", caname, i);
    return 0;
  }
  
  /*
  printf("putInt> chan: %s, val: %d\n", caname, i);
  */
  return 1;
}

int putDouble(chid chan, double d) {
  char es[40];
  double timeout = _Timeout;
  int stat= 0, typ= 0, i= (int)d;
  char* caname= 0;
  if( chan == 0 )
    return 0;

  typ = getType(chan);
  caname = (char*) ca_name(chan);

  stat = _lock(_pmutex);
  if( stat != 0 ) {
    fprintf(stderr,"UFCA> mutex lock failed, thread: %d\n", (int)pthread_self());
    return 0;
  }

  switch(typ) {
  case DBF_CHAR:
  case DBF_INT:
  case DBF_LONG:
    stat = ca_array_put(DBR_INT, 1u, chan, &i);
    if( stat != ECA_NORMAL ) {
      fprintf(stderr,"putInt> failed! chan: %s, val: %d\n", caname, i);
      _unlock(_pmutex);
      return 0;
    }
    break;
  case DBF_FLOAT:
  case DBF_DOUBLE: 
    stat = ca_array_put(DBR_DOUBLE, 1u, chan, &d);
    if( stat != ECA_NORMAL ) {
      fprintf(stderr,"putDouble> failed! chan: %s, val: %f\n", caname, d);
      _unlock(_pmutex);
      return 0;
    }
    break;
  case DBF_STRING:
  default: 
    memset(es, 0, sizeof(es));
    sprintf(es, "%f", d);
    stat = ca_array_put(DBR_STRING, 1u, chan, (char*)es);
    if( stat != ECA_NORMAL ) {
      fprintf(stderr,"putDouble> failed! chan: %s, val: %f\n", ca_name(chan), d);
      _unlock(_pmutex);
      return 0;
    }
    break;
  }
  _unlock(_pmutex);

  stat = ca_pend_io(timeout);
  if( stat != ECA_NORMAL )
    fprintf(stderr,"putDouble> pend_io failed! chan: %s, val: %f\n", caname, d);
  /* 
  printf("putDouble> chan: %s, val: %f\n", caname, d);
  */
  return 1;
}

int putString(chid chan, const char* s) {
  char es[40];
  double timeout= _Timeout;
  int stat= 0;
  char* caname= 0;
  if( chan == 0 )
    return 0;

  caname = (char*) ca_name(chan);
  stat = _lock(_pmutex);
  if( stat != 0 ) {
    fprintf(stderr,"UFCA> mutex lock failed, thread: %d\n", (int)pthread_self());
    return 0;
  }
  memset(es, 0, sizeof(es));
  strncpy(es, s, sizeof(es));
  stat = ca_array_put(DBR_STRING, 1u, chan, (char*)es);
  if( stat != ECA_NORMAL ) {
    fprintf(stderr,"putString> failed! chan: %s, val: %s\n", caname, es);
    _unlock(_pmutex);
    return 0;
  }
  _unlock(_pmutex);

  stat = ca_pend_io(timeout);
  if( stat != ECA_NORMAL ) {
    fprintf(stderr,"putString>> pend_io failed! chan: %s, val: %s\n", caname, es);
    return 0;
  }
  /*
  printf("putString> chan: %s, val: %s\n", ca_name(chan), es);
  */
  return 1;
}

#endif /* __UFCA_C__ */

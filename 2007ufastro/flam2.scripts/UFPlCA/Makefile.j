# RCS: 
#
# Macros:

SHELL := /bin/tcsh -f

ifndef GCCID
  GCCID = $(shell ../../.gccv)
endif

include ../../.makerules

ifndef ($JAVA_DIR)
  JAVA_DIR = /usr/local/java
endif
ifndef ($EPICS_BASE)    
  EPICS_BASE = /gemini/epics/base
endif

JNI_INC = -I$(JAVA_DIR)/include -I$(JAVA_DIR)/include/linux -I$(EPICS_BASE)/include -I$(EPICS_BASE)/include/os/Linux -I$(JAVA_DIR)/include/solaris -I$(EPICS_BASE)/include/os/solaris

#CFLAGS += -shared
CFLAGS += -shared -D_REENTRANT=1

LDFLAGS += -L$(EPICS_BASE)/lib/linux-x86 -L$(EPICS_BASE)/lib/solaris-sparc-gnu

LDSYSLIB += -lca -lcas

ifneq ($(AMD64), )
  CFLAGS += -fPIC
  LDFLAGS += -L$(EPICS_BASE)/lib/linux-x86_64
endif

PKG = ufjca
SRC_HDR = ufca.h
SRC = $(patsubst %.h, %.c, $(SRC_HDR)) 
SRC_WRAP = $(patsubst %.h, %_wrap.c, $(SRC_HDR))
OBJ = $(patsubst %.c, %.o, $(SRC))
TARGETOBJ = $(patsubst %.c, %.o, $(SRC_WRAP))
LIBNAME = lib$(PKG).so

#CC = g++
LD = g++ -shared
ifneq ($(AMD64), )
  CC = gcc
  LD = gcc -shared
endif
ifeq ($(OS),SOLARIS)
  LD = ld -G
endif
SWIG = swig -java -package ufjca
RM = rm -f
JAVAC = $(JAVA_DIR)/bin/javac -d ./
JAR = $(JAVA_DIR)/bin/jar
JAR_FLAGS = cvf

JAVASRC = UFCAToolkit.java


all: .start .swig .shared jar

install: .start .swig .shared jar .install

.start:
	mkdir -p $(TARGET)/java

.install:
	mkdir -p $(UFINSTALL)/lib
	mkdir -p $(UFINSTALL)/javalib
	cp -f $(TARGET)/$(LIBNAME) $(UFINSTALL)/lib 
	cp -f $(TARGET)/java/$(PKG).jar $(UFINSTALL)/javalib

jar:
	cp -f $(JAVASRC) $(TARGET)/java
	cd $(TARGET)/java; $(JAVAC)  *.java
	cp -f $(TARGET)/$(LIBNAME) $(TARGET)/java
	cd $(TARGET)/java; $(JAR) $(JAR_FLAGS) $(PKG).jar $(PKG) $(LIBNAME)

.swig: $(TARGET)/ufca.o
	cp -f $(SRC_HDR) $(SRC) $(TARGET)
	cd $(TARGET); $(SWIG) $(SRC_HDR)
	cd $(TARGET); cp -f *.java java/

.shared: $(TARGET)/$(OBJ) 
	cd $(TARGET); $(CC) -fPIC -shared $(CFLAGS) $(JNI_INC) -c -o ufca_wrap.o ufca_wrap.c ;
	cd $(TARGET); $(LD) $(OBJ) $(TARGETOBJ) -o $(LIBNAME) $(LDFLAGS) -lc $(LDSYSLIB)

clean:
	$(RM) -r $(TARGET)
	$(RM) ufca.o
	$(RM) ufca_wrap.o

install_noepics: .start .swig jar .install

%.o : %.c
	cd $(TARGET); $(CC) -fPIC -shared $(CFLAGS) $(JNI_INC) -c -o $@ $< ;  

$(TARGET)/%.o : $(TARGET)/%.c
	cd $(TARGET); $(CC) -fPIC -shared $(CFLAGS) $(JNI_INC) -c -o $@ $< ;  
	

#include ../../.maketargets

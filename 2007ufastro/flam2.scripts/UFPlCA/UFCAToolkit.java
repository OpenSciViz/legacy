package ufjca;

import java.util.Vector;
import java.util.Hashtable;

public class UFCAToolkit {

    static {
	System.loadLibrary("ufjca");
    }

    public interface MonitorListener {
	public void monitorChanged(String value);
	public void reconnect(String dbname);
    }

    static Vector monVec;
    static Hashtable listenerHash;
    public static boolean keepGoing = false;
    static double TIMEOUT = 1.0;

    static int count = 0;

    static Vector lisVec;
    static Vector namVec;

    public static boolean addingMonitors = false;

    public static Object mutex = new Object();

    public synchronized static String get(String pvName) {
	//return "0";/*
	//if (keepGoing) return "0";
	System.out.println("Start getting");
	String val = "Error";
	synchronized (mutex) {
	    SWIGTYPE_p_chid pv = UFCA.connectPV(pvName,TIMEOUT);
	    if (pv != null && UFCA.nullPV(pv)==0) {
		if (SWIGTYPE_p_chid.getCPtr(pv) != 0) {
		    //System.out.println("UFCAToolkit.get> "+pvName+"--> CPtr: "+SWIGTYPE_p_chid.getCPtr(pv));
		    val = UFCA.getAsString(pv);
		    if (val == null || val.trim()=="") {
			System.err.println("UFCAToolkit.get> Could not get value for pv");
			val = "Error";
		    }
		    else {
			UFCA.disconnectPV(pv);		    
		    }
		} else 
		    System.err.println("UFCAToolkit.get> Could not connect to PV");
	    } else
		System.err.println("UFCAToolkit.get> "+pvName+": NULL pv.  Could not connect, bad pv name?");
	}
	if (val.indexOf(".")!=-1&&val.indexOf("+")!=-1&&val.indexOf("e")!=-1) val = val.toUpperCase();
	try {
	    if ((int)(Double.parseDouble(val)) == Integer.MIN_VALUE) val = "null";
	    //else System.out.println(val+": "+Double.parseDouble(val)+": "+(int)(Double.parseDouble(val))+": "+Integer.MIN_VALUE);
	} catch (NumberFormatException nfe) { }
	System.out.println("Done getting");
	return val;
	//*/
    }

    public synchronized static void put(String pvName, String val) {
	
	//if (keepGoing) return;
	//	/*
	synchronized (mutex) {
	    SWIGTYPE_p_chid pv = UFCA.connectPV(pvName,TIMEOUT);
	    if (pv != null && SWIGTYPE_p_chid.getCPtr(pv) != 0) {
		if (UFCA.putString(pv,val) == 0) {
		    System.err.println("UFCAToolkit.put> Put failed! Recname: "+pvName+" Value: "+val);
		}
		UFCA.disconnectPV(pv);
	    }
	}	
	//*/
    }

    public synchronized static boolean addMonitor(String pvName, MonitorListener listen) {
	if (namVec == null || lisVec == null) {
	    namVec = new Vector(); lisVec = new Vector();
	}
	namVec.add(pvName); lisVec.add(listen);
	if (namVec.size() == lisVec.size()) return true;
	else return false;
    }
    
    private synchronized static boolean _addMonitor(String pvName, MonitorListener listen) {	
	//if (count > 3) return true;
	if (monVec == null) monVec = new Vector();
	if (listenerHash == null) listenerHash = new Hashtable();
	//if (pvName.indexOf("CdSteps") != -1) return false;
	String s = pvName.trim();
	Vector v = (Vector)listenerHash.get(s);
	if (v != null) return v.add(listen);
	else {
	    v = new Vector();
	    v.add(listen);
	    listenerHash.put(s,v);
	    synchronized (mutex) {
		//System.out.println("Connecting: "+pvName);
		SWIGTYPE_p_chid p = UFCA.connectPV(pvName,TIMEOUT);
		//System.out.println("Connected: "+pvName);
		//System.out.println("Subscribing, count = "+count++);
		if (p != null && SWIGTYPE_p_chid.getCPtr(p) != 0 && UFCA.nullPV(p)==0) {
		    //System.out.println("Subscribing: "+pvName);
		    UFCA.subscribe(p);
		    //System.out.println("Subscribed: "+pvName);
		    monVec.add(p);
		}
	    }
	}
	return true;
    }
    
    public static void removeAllMonitors() {
	if (listenerHash != null)
	    listenerHash.clear(); 
	if (monVec != null)
	    monVec.clear(); 
	if (namVec != null)
	    namVec.clear();
    }

    public static boolean removeMonitor(String pvName,MonitorListener listen) {
	String s = pvName.trim();
	if (listenerHash == null) return true;
	Vector v = (Vector)listenerHash.get(s);
	if (v != null) return v.remove(listen);
	return false;
    }

    public static void removeAllConnections() {
	System.out.println("UFCAToolkit.removeAllConnections> Removing..."); 
	for (int i=0; i<monVec.size(); i++) {
	    System.out.println("UFCAToolkit.removeAllConnections> Unsubscribing");
	    SWIGTYPE_p_chid pv = (SWIGTYPE_p_chid)monVec.elementAt(i);
	    UFCA.unsubscribe(pv);
	    UFCA.disconnectPV(pv);
	    System.out.println("UFCAToolkit.removeAllConnections> Unsubscribed");
	}
	
    }

    public static void startMonitorLoop() {
	//allow external entities a machanism by which to tell when the main loop has actually
	//started (ie, when keepGoing becomes true)
	keepGoing = false;
	if (listenerHash == null) listenerHash = new Hashtable();
	if (monVec == null) monVec = new Vector();
	if (namVec!= null && lisVec != null && 
	    (namVec.size() == lisVec.size())) {
	    int x = namVec.size();
	    //System.out.println("namVec size: "+x);
	    for (int i=0; i<x; i++) {
		//System.out.println("adding monitor number "+i+", "+namVec.elementAt(0));
		_addMonitor((String)namVec.remove(0),
			    (MonitorListener)lisVec.remove(0));
	    }
	}
	System.out.println("UFCAToolkit.startMonitorLoop> Done setting up pre-registered monitors, entering main loop.");
	keepGoing = true;
	//try{while (System.in.read()==-1);}catch(Exception e){}
	while (keepGoing) {
	    try {
		if (namVec!= null && lisVec != null && 
		    (namVec.size() == lisVec.size())) {
		    int x = namVec.size();
		    //System.out.println("namVec size: "+x);
		    addingMonitors = true;
		    for (int i=0; i<x; i++) 
			_addMonitor((String)namVec.remove(0),
				    (MonitorListener)lisVec.remove(0));
		    addingMonitors = false;
		}
		for (int i=0; i<monVec.size(); i++) {
		    SWIGTYPE_p_chid pv = (SWIGTYPE_p_chid)monVec.elementAt(i);
		    int x = 0;
		    synchronized (mutex) {
			//System.out.println("UFCAToolkit.mainLoop> Trying eventMon...");
			x = UFCA.eventMon(pv);
			//System.out.println("UFCAToolkit.mainLoop> eventMon returned with x = "+x);
		    }
		    if (x>0) {
			//System.out.println("Got monitor!");
			String s=null; Vector v=null;
			synchronized (mutex) {
			    //try {
			        s = UFCA.getAsString(pv);
				try {
				  if ((int)(Double.parseDouble(s)) <= Integer.MIN_VALUE+4000) s = "null";
				    //else System.out.println(s+": "+Double.parseDouble(s)+": "+(int)(Double.parseDouble(s))+": "+Integer.MIN_VALUE);
				} catch (NumberFormatException nfe) { }

				v = (Vector)listenerHash.get(UFCA.getPVName(pv));
				//} catch (Exception e) {
				//System.err.println("UFCAToolkit.startMonitorLoop> "+e.toString());
				//}
			}		
			if (v != null) 
			    for (int j=0; j<v.size(); j++)
				((MonitorListener)v.elementAt(j)).monitorChanged(s);
			else {
			    System.err.println("UFCAToolkit.startMonitorLoop> Null MonitorListener vector for "+
					       UFCA.getPVName(pv));
			}
		    } 
		}
		Thread.sleep(100);
		//try{while(System.in.read()==-1);}catch(Exception e){}
	    } catch (Exception e) {
		keepGoing = false;
		System.err.println("UFCAToolkit.startMonitorLoop> "+e.toString());
		return;
	    }
	}
	removeAllConnections();
	// wait for pend_io to complete?
	//try { Thread.sleep(2000); } catch (Exception e) {}
	System.out.println("UFCAToolkit.startMonitorLoop> Now exiting monitor loop");
    }
    
    private synchronized static void _stop() {
	synchronized(mutex) {
	    keepGoing = false;
	}
    }

    public static void restart() {
    }

    public synchronized static void stop() {
	_stop();
    }


    /*
    private class PVMonitor {
	private String name;
	private SWIGTYPE_p_chid pv;
	private Vector listeners;
	public String name() {return name;}
	public PVMonitor(String pvname, MonitorListener listen) {
	    name = new String(pvname.trim());
	    pv = UFCA.connectPV(name,1.0);
	    pv = UFCA.subscribe(pv);
	    listeners = new Vector();
	    listeners.add(listen);
	}
	public boolean connected() { return (pv != null);}
	public void addListener(MonitorListener ml) {
	    listeners.add(ml);
	}
	public void removeListener(MonitorListener ml) {
	    listeners.remove(ml);
	}
	public boolean empty() {
	    return listeners.size()==0;
	}
	public void clear() {
	    UFCA.disconnectPV(pv);
	}
	public boolean changed() {
	    if (UFCA.eventMon(pv)>0) return true;
	    else {
		System.out.println(UFCA.getPVName(pv)+": "+UFCA.getAsString(pv)+": "+UFCA.eventMon(pv));
		return false;
	    }
	}
	public void fireEvents() {
	    String s = UFCA.getAsString(pv);
	    //UFCA.subscribe(pv);
	    for (int i=0; i<listeners.size(); i++) {
		System.out.println("Firing Events");
		((MonitorListener)listeners.elementAt(i)).monitorChanged(s);
	    }
	}
    }

    private boolean keepGoing = false;
    private static UFMonitorThread instance;
    private static Vector subscribers;


    public static boolean addMonitor(String pvName, MonitorListener listen) {
	if (instance == null) {
	    System.err.println("UFMonitorThread.addMonitor> Thread hasn't "+
			       "been started yet!  You must call "+
			       "UFMonitorThread.start() before adding "+
			       "monitors");
	    return false;
	}
	return (instance._addMonitor(pvName,listen));
    }
    public static boolean removeMonitor(String pvName,MonitorListener listen) {
	if (instance == null) {
	    System.err.println("UFMonitorThread.removeMonitor> Thread hasn't "+
			       "been started yet!  You must call "+
			       "UFMonitorThread.start() before removing "+
			       "monitors");
	    return false;
	}
	return (instance._removeMonitor(pvName,listen));
    }

    public static void start() {
	if (instance != null) {
	    System.err.println("UFMonitorThread.start> Instance already "+
			       "running! If you want to restart, use "+
			       "UFMonitorThread.restart() instead");
	    return;
	}
	instance = new UFMonitorThread();
    }

    public static void restart() {
	stop();
	instance = new UFMonitorThread();
    }

    public static void stop() {
	if (instance != null) { 
	    instance.keepGoing = false;
	    try {
		//sleep to allow thread to exit
		Thread.sleep(1000);
	    } catch (Exception e) {
		System.err.println("UFMonitorThread.restart> Problem sleeping,"
				   +" thread may not have stopped correctly: "+
				   e.toString());
	    }
	}
    }

    public UFMonitorThread() {
	keepGoing = true;
	subscribers = new Vector();
	new Thread() {
	    public void run() {
		while (keepGoing) {
		    //PVMonitor [] pvmons=new PVMonitor[1];
		    synchronized (subscribers) {
			if (subscribers.size() > 0) {
			    //synchronized (subscribers) {
			    //pvmons = (PVMonitor[])subscribers.toArray();
			    //}
			    //for (int i=0; i<pvmons.length; i++) {
			    for (int i=0; i<subscribers.size(); i++){
				PVMonitor pvmon = 
				    (PVMonitor)(subscribers.elementAt(i));
				if (pvmon.changed()) 
				    pvmon.fireEvents();
				    
			    }
			}
		    }
		    try {Thread.sleep(100);}
		    catch (Exception e){}
		}
	    }
	}.start();
    }

    private boolean _addMonitor(String pvName, MonitorListener listen) {
	String s = pvName.trim();
	synchronized (subscribers) {
	    boolean found = false;
	    for (int i=0; i<subscribers.size(); i++) {
		PVMonitor pvmon = (PVMonitor)subscribers.elementAt(i);
		if (pvmon.name().equals(pvName)) {
		    found = true;
		    pvmon.addListener(listen);
		    System.out.println("Added monitor: "+pvName);
		    break;
		}
	    }
	    if (!found) {
		PVMonitor p = new PVMonitor(pvName,listen);
		if (!p.connected()) return false;
		System.out.println("Added, eventMon: "+p.changed());
		subscribers.add(p);
		//p.fireEvents();
	    }
	}
	return true;
    }
    private boolean _removeMonitor(String pvName, MonitorListener listen) {
	String s = pvName.trim();
	synchronized (subscribers) {
	    boolean found = false;
	    for (int i=0; i<subscribers.size(); i++) {
		PVMonitor pvmon = (PVMonitor)subscribers.elementAt(i);
		if (pvmon.name().equals(pvName)) {
		    found = true;
		    pvmon.removeListener(listen);
		    if (pvmon.empty()) {
			pvmon.clear();
			subscribers.removeElementAt(i);
		    }
		    break;
		}
	    }
	    if (!found) {
		return false;
	    }
	}
	return true;
    }
    */
}

#!/usr/local/bin/perl
$rcsId = '$Name:  $ $Id: datum.pl 14 2008-06-11 01:49:45Z hon $';
use UFCA;
######################### main #########################
$Idle = 0;
$Clear = 1;
$Busy = 2;
$Start = 3;
$to = 1.0;

$instrum= "flam";
$subsys = "all"; # "cc,ec,dc"

if( @ARGV > 0 ) {
  $argval = shift;
  if( $argval eq "-h" || $argval eq "-help" ) {
    die "datum.pl [-h[elp]] [instrum] [subsys: all/cc,ec,dc]\n";
  }
  $instrum = $argval;
  if( @ARGV > 0 ) { $subsys = shift; }
}
print "datum> $instrum, subsys: $subsys\n";

$apply = UFCA::connectPV("$instrum:apply.DIR", $to);
$applyC = UFCA::connectPV("$instrum:applyC", $to);
$sysD = UFCA::connectPV("$instrum:datum.DIR", $to);
$sysDM = UFCA::connectPV("$instrum:datum.MARK", $to);
$sysDsub = UFCA::connectPV("$instrum:datum.subsys", $to);
$sysDC = UFCA::connectPV("$instrum:datumC", $to);
$pv = UFCA::getPVName($apply);
print "datum> clear system: $pv\n";
$p = UFCA::putInt($apply, $Clear);
$c = UFCA::getInt($applyC);
#$p = UFCA::putInt($sysD, $Mark);
$pv = UFCA::getPVName($sysDsub);
$p = UFCA::putString($sysDsub, $subsys);
$m = UFCA::getInt($sysDM);
if( $m <= 0 ) { die  "datum> mark failed for $pv ...\n"; }
print "datum> set $pv to subsys $subsys. $instrum:datum marked; applying start...\n";
$p = UFCA::putInt($apply, $Start);
$c = UFCA::getInt($sysDC);
$ca = UFCA::getInt($applyC);
$pv = UFCA::getPVName($sysDC);
$pva = UFCA::getPVName($apply);
$cnt = 10;
while(  $c == $Busy && $cnt-- > 0 ) {
  print "datum> $pv: $c, $pva: $ca\n";
  sleep 1;
  $c = UFCA::getInt($sysDC);
  $ca = UFCA::getInt($applyC);
}
$cnt = 10;
while( $ca == $Busy && $cnt-- > 0 ) {
  print "datum> $pv: $c, $pva: $ca\n";
  sleep 1;
  $c = UFCA::getInt($sysDC);
  $ca = UFCA::getInt($applyC);
}
print "datum> $pv: $c, $pva: $ca\n";
exit;


#!/usr/local/bin/perl
$rcsId = '$Name:  $ $Id: engobserve.pl 14 2008-06-11 01:49:45Z hon $to6 20:33:37 hon Exp hon $';
use UFCA;
######################### main #########################
#dbprint();
$Start = 3;
$Clear = 1;
$Idle = 0;
$to = 1.0;
$frmcnt = 1;
if( @ARGV > 0 ) { 
  $frmcnt = shift;
}
if( $frmcnt > 1 ) { 
  print "engobserve> EngMode frmcnt == $frmcnt\n";
}
else {
  print "engobserve> SciMode frmcnt == $frmcnt\n";
}
$obsC = UFCA::connectPV("flam:eng:observeC", $to);
$obsI = UFCA::connectPV("flam:eng:observeC.ival", $to);
$g = UFCA::getAsString($obsC);
if( $g ne $Idle ) {
  print "flam:eng:observeC not Idle, force Idle? [y/n]";
  $a = getc;
  if( $a eq 'n' ) { exit; }
  $p = UFCA::putInt($obsI, $Idle);
}
$apply = UFCA::connectPV("flam:eng:apply.dir", $to);
$applyC = UFCA::connectPV("flam:eng:applyC", $to);
$dhslabel = UFCA::connectPV("flam:eng:observe.DHSLabel", $to);
$edtinit = UFCA::connectPV("flam:eng:observe.EDTInit", $to);
$ds9 = UFCA::connectPV("flam:eng:observe.DS9", $to);
$lut = UFCA::connectPV("flam:eng:observe.LUT", $to);
$fits = UFCA::connectPV("flam:eng:observe.FITSFile", $to);
$frmmode = UFCA::connectPV("flam:eng:observe.FrameMode", $to);
$sim = UFCA::connectPV("flam:eng:observe.SimMode", $to);
$exptime = UFCA::connectPV("flam:eng:observe.ExpTime", $to);
$p = UFCA::putDouble($exptime, 2.0);
$p = UFCA::putString($dhslabel,"RichardElston");
$p = UFCA::putString($fits,"RichardElston.fits");
$p = UFCA::putString($lut, "true");
$p = UFCA::putInt($frmmode, $frmcnt);
$p = UFCA::putInt($ds9, 2);
$p = UFCA::putInt($apply, $Clear);
$p = UFCA::putInt($apply, $Start);
exit;

############################## subs #####################
sub dbprint {
  my $cc = `ufflam2epicsd -lvv |grep 'cc:setup'|grep -v 'setupC'|grep -v 'O,'|cut -d' ' -f4`;
  my @dball = split /\n/,$cc;
  my $db = shift @dball;
  foreach $db (@dball) {
    chomp $db;
    print "$db\n";
  }
  $dc = `ufflam2epicsd -lvv | grep 'dc:setup'| grep -v 'setupC'| grep -v 'O,'| cut -d' ' -f4`;
  @dball = split /\n/,$dc;
  $db = shift @dball;
  foreach $db (@dball) {
    chomp $db;
    print "$db\n";
  }
  $ec = `ufflam2epicsd -lvv | grep 'ec:setup'| grep -v 'setupC'| grep -v 'O,'| cut -d' ' -f4`;
  @dball = split /\n/,$ec;
  $db = shift @dball;
  foreach $db (@dball) {
    chomp $db;
    print "$db\n";
  }
  $eng = `ufflam2epicsd -lvv | grep 'eng:observe'| grep -v 'observeC'| grep -v 'O,'| cut -d' ' -f4`;
  @dball = split /\n/,$eng;
  $db = shift @dball;
  foreach $db (@dball) {
    chomp $db;
    print "$db\n";
  }
}


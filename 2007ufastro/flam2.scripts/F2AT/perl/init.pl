#!/usr/local/bin/perl
$rcsId = '$Name:  $ $Id: init.pl 14 2008-06-11 01:49:45Z hon $';
#
# load the Swig generated Epics channel access module:
use UFCA;
#
######################### main #########################
#
$Idle = 0;
$Clear = 1;
$Busy = 2;
$Start = 3;
$to = 1.0;

$instrum= "flam";
$host = `hostname`;
chomp $host;

if( @ARGV > 0 ) {
  $argval = shift;
  if( $argval eq "-h" || $argval eq "-help" ) {
    die "init.pl [-h[elp]] [instrum] [agenthost]\n";
  }
  $instrum = $argval;
  if( @ARGV > 0 ) { $host = shift; }
}

print "init> $instrum@$host, device agents expect connections on $host\n";

$apply = UFCA::connectPV("$instrum:apply.DIR", $to);
$applyC = UFCA::connectPV("$instrum:applyC", $to);
$sysI = UFCA::connectPV("$instrum:init.DIR", $to);
$sysIM = UFCA::connectPV("$instrum:init.MARK", $to);
$sysIhost = UFCA::connectPV("$instrum:init.host", $to);
$sysIC = UFCA::connectPV("$instrum:initC", $to);
$pv = UFCA::getPVName($apply);
print "init> clear system: $pv\n";
$p = UFCA::putInt($apply, $Clear);
$c = UFCA::getInt($applyC);
$pv = UFCA::getPVName($sysIhost);
#$p = UFCA::putInt($sysI, $Mark);
$p = UFCA::putString($sysIhost, $host);
$m = UFCA::getInt($sysIM);
if( $m <= 0 ) { die  "init> mark failed for $pv ...\n"; }
print "set $pv to host $host. $instrum:init marked; applying start...\n";
$p = UFCA::putInt($apply, $Start);
$c = UFCA::getInt($sysIC);
$pv = UFCA::getPVName($sysIC);
$ca = UFCA::getInt($applyC);
$pva = UFCA::getPVName($applyC);
$cnt = 10;
while(  $c == $Busy && $cnt-- > 0 ) {
  print "init> $pv: $c, $pva: $ca\n";
  sleep 1;
  $c = UFCA::getInt($sysIC);
  $ca = UFCA::getInt($applyC);
}
$cnt = 10;
while(  $ca == $Busy && $cnt-- > 0 ) {
  print "init> $pv: $c, $pva: $ca\n";
  sleep 1;
  $c = UFCA::getInt($sysIC);
  $ca = UFCA::getInt($applyC);
}
print "init> $pv: $c, $pva: $ca\n";
exit;


#!/usr/local/bin/perl
$rcsId = '$Name:  $ $Id: dcinit.pl 14 2008-06-11 01:49:45Z hon $';
#
# load the Swig generated Epics channel access module:
use UFCA;
#
######################### main #########################
#
$Idle = 0;
$Clear = 1;
$Busy = 2;
$Start = 3;
$to = 1.0;

$instrum= "flam";
$host = `hostname`;
chomp $host;

if( @ARGV > 0 ) {
  $argval = shift;
  if( $argval eq "-h" || $argval eq "-help" ) {
    die "init.pl [-h[elp]] [instrum] [agenthost]\n";
  }
  $instrum = $argval;
  if( @ARGV > 0 ) { $host = shift; }
}

print "init> $instrum@$host, device agents expect connections on $host\n";

$apply = UFCA::connectPV("$instrum:apply.DIR", $to);
$applyC = UFCA::connectPV("$instrum:applyC", $to);
$dcI = UFCA::connectPV("$instrum:dc:init.DIR", $to);
$dcIM = UFCA::connectPV("$instrum:dc:init.MARK", $to);
$dcIC = UFCA::connectPV("$instrum:dc:initC", $to);
$dcIhost = UFCA::connectPV("$instrum:dc:init.host", $to);
$pv = UFCA::getPVName($apply);
print "init> clear system: $pv\n";
$p = UFCA::putInt($apply, $Clear);
$c = UFCA::getInt($applyC);
$pv = UFCA::getPVName($dcIhost);
$p = UFCA::putString($dcIhost, $host);
$m = UFCA::getInt($dcIM);
if( $m <= 0 ) { die  "init> mark failed for $pv ...\n"; }
print "set $pv to host $host. $instrum:init marked; applying start...\n";
$p = UFCA::putInt($apply, $Start);
$c = UFCA::getInt($dcIC);
$pv = UFCA::getPVName($dcIC);
$ca = UFCA::getInt($applyC);
$pva = UFCA::getPVName($applyC);
$cnt = 10;
while(  $c == $Busy && $cnt-- > 0 ) {
  print "init> $pv: $c, $pva: $ca\n";
  sleep 1;
  $c = UFCA::getInt($dcIC);
  $ca = UFCA::getInt($applyC);
}
$cnt = 10;
while(  $ca == $Busy && $cnt-- > 0 ) {
  print "init> $pv: $c, $pva: $ca\n";
  sleep 1;
  $c = UFCA::getInt($dcIC);
  $ca = UFCA::getInt($applyC);
}
print "init> $pv: $c, $pva: $ca\n";
exit;


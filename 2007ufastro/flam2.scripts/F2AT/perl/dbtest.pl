#!/usr/local/bin/perl
use UFCA;
$PVString = 0;
$PVInt = 1;
$PVDouble = 6;
#
#$db = "flam:cc:heartbeat.val";
$db = shift;
$val = shift;
print "PV == $db, val == $val\n";
$to = 0.5;
$chan = UFCA::connectPV($db, $to);
$inpv = UFCA::getPVName($chan);
$typ = UFCA::getType($chan, $to);
print "$inpv type == $typ\n";
if( $typ == $PVString ) { $i = UFCA::getString($chan, $to); }
if( $typ == $PVInt ) { $i = UFCA::getInt($chan, $to); }
if( $typ == $PVDouble ) { $i = UFCA::getDouble($chan, $to); }
print "$inpv == $i\n";
if( $val eq "" ) {
  exit;
}
#
@dbrec = split /./,$db;
print "$dbrec[0]\n";
$valchan = UFCA::connectPV($dbrec[0], $to);
print "$dbrec[0] chid: $valchan\n";
$outpv = UFCA::getPVName($valchan);
print "$outpv chid: $valchan\n";
if( $typ == $PVInt ) {
  UFCA::putInt($chan, $val, $to);
  $i = UFCA::getInt($chan, $to);
  $o = UFCA::getInt($valchan, $to);
}
if( $typ == $PVDouble ) {
  UFCA::putDouble($chan, $val, $to);
  $i = UFCA::getDouble($chan, $to);
  $o = UFCA::getDouble($valchan, $to);
}
if( $typ == $PVString ) {
  UFCA::putString($chan, $val, $to);
  $i = UFCA::getString($chan, $to);
  $o = UFCA::getString($valchan, $to);
}

print "input $inpv is $i ==> output $outpv is $o\n";

exit;


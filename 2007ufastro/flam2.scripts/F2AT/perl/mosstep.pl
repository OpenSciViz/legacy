#!/usr/local/bin/perl
$rcsId = '$Name:  $ $Id: mosstep.pl 14 2008-06-11 01:49:45Z hon $';
use UFCA;
# timeout:
$to = 0.75;
#
# directives:
$Mark = 0;
$Clear = 1;
$Preset = 2;
$Start = 3;
#
# CAR states:
$Idle= 0;
$Busy = 2;
$Error = 3;
#
# data types:
$PVString = 0;
$PVInt = 1;
$PVDouble = 6;
#
$apply = UFCA::connectPV("flam:eng:apply.dir", $to);
$applyR = UFCA::connectPV("flam:eng:applyC", $to);
$setupR = UFCA::connectPV("flam:cc:setupC", $to);
$mosR = UFCA::connectPV("flam:cc:setupMOSC", $to);
$stepR = UFCA::connectPV("flam:cc:setupC", $to);
$sad = UFCA::connectPV("flam:sad:MOSSteps", $to);
#print "connected to Barcode SAD I/O...\n";
#
sub setPV {
  my $argc = @_;
  my $ch = shift @_;
  my $val = shift @_;
  my $pv = UFCA::getPVName($ch);
  my $typ = UFCA::getType($ch, $to);
  my $styp = UFCA::getTypeStr($ch, $to);
  my $i, $o;
  if( $argc >= 3 ) { print "PV == $pv, type == $styp, val == $val\n"; }
  if( $typ == $PVInt ) {
    $i = UFCA::getInt($ch, $to); UFCA::putInt($ch, $val, $to); $o = UFCA::getInt($ch, $to);
  }
  if( $typ == $PVDouble ) {
    $i = UFCA::getDouble($ch, $to); UFCA::putDouble($ch, $val, $to); $o = UFCA::getDouble($ch, $to);
  }
  if( $typ == $PVString ) {
    $i = UFCA::getString($ch, $to); UFCA::putString($ch, $val, $to); $o = UFCA::getString($ch, $to);
  }
  if( $argc >= 3 ) { print "initial $pv was $i ==> new $pv is $o\n"; }
}
#
sub getPV {
  my $argc = @_;
  my $ch = shift @_;
  my $pv = UFCA::getPVName($ch);
  my $typ = UFCA::getType($ch, $to);
  my $styp = UFCA::getTypeStr($ch, $to);
  my $val;
  if( $typ == $PVInt ) {
    $val = UFCA::getInt($ch, $to);
  }
  if( $typ == $PVDouble ) {
    $val = UFCA::getDouble($ch, $to);
  }
  if( $typ == $PVString ) {
    $val = UFCA::getString($ch, $to);
  }
  if( $argc >= 2 ) { print "$pv type == $styp, val == $val\n"; }
  return $val;
}

sub monPV {
  my $ch1 = shift @_;
  my $ch2 = shift @_;
  my $pv1 = UFCA::getPVName($ch1);
  my $pv2 = UFCA::getPVName($ch2);
  UFCA::subscribe($ch1, $to);
  UFCA::subscribe($ch2, $to);
  my $m1 = 0; $m2 = 0;
  while( $m1 == 0 || $m2 == 0 ) {
    $m1 = UFCA::eventMon($ch1); $m2 = UFCA::eventMon($ch2);
    sleep 1;
  }
  $val1 = getPV($ch1);
  $val2 = getPV($ch2);
  print "$pv1 == $val1; $pv2 == $val2\n";
}

# main:
$stepcnt = 100;
$argc = @ARGV;
#print "ARGV: ";
#foreach $a (@ARGV) { print "$a, "; }
#print "\n";
if( $argc > 0 ) { $stepcnt = shift @ARGV; }

# step to each barcode position and process barcode capture:
# set step parms?
$bacc = UFCA::connectPV("flam:cc:setup.MOSBarCdAcc", $to);
$bdec = UFCA::connectPV("flam:cc:setup.MOSBarCdDec", $to);
$bbl = UFCA::connectPV("flam:cc:setup.MOSBarCdBacklash", $to);
$bivel = UFCA::connectPV("flam:cc:setup.MOSBarCdInitVel", $to);
$brunc = UFCA::connectPV("flam:cc:setup.MOSBarCdRunc", $to);
$bvel = UFCA::connectPV("flam:cc:setup.MOSBarCdSlewVel", $to);
$bdir = UFCA::connectPV("flam:cc:setup.MOSBarCdDirection", $to);
$acc = UFCA::connectPV("flam:cc:setup.MOSAcc", $to);
$dec = UFCA::connectPV("flam:cc:setup.MOSDec", $to);
$bl = UFCA::connectPV("flam:cc:setup.MOSBacklash", $to);
$ivel = UFCA::connectPV("flam:cc:setup.MOSInitVel", $to);
$runc = UFCA::connectPV("flam:cc:setup.MOSRunc", $to);
$vel = UFCA::connectPV("flam:cc:setup.MOSSlewVel", $to);
$dir = UFCA::connectPV("flam:cc:setup.MOSDirection", $to);
print "connected to all pv channels ... \n";
setPV($bivel, 100);
setPV($bvel, 100);
setPV($bacc, 200);
setPV($bbl, 0);
setPV($brunc, 50);
setPV($bdir, "+");
setPV($ivel, 100);
setPV($vel, 100);
setPV($acc, 200);
setPV($bl, 0);
setPV($runc, 50);
setPV($dir, "+");
#$step = UFCA::connectPV("flam:cc:setup.MOSBarCdStep", $to);
$step = UFCA::connectPV("flam:cc:setup.MOSStep", $to);
setPV($mosR, $Idle);
setPV($stepR, $Idle);
setPV($applyR, $Idle);
setPV($step, $stepcnt, 1);
setPV($apply, $Start);
print "stepping $stepcnt ... \n";
$pos = getPV($sad, 1);
$newpos = $pos;
$diff = abs($newpos - $pos);
$abstep = abs($stepcnt);
while( $diff < $abstep ) {
  $newpos = getPV($sad);
  $diff = abs($newpos - $pos);
  print "orig. pos == $pos, new pos. == $newpos, diff == $diff\n";
  sleep 1;
}
exit;

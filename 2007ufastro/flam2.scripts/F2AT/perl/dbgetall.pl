#!/usr/local/bin/perl
$rcsId = '$Name:  $ $Id: dbgetall.pl 14 2008-06-11 01:49:45Z hon $';
use UFCA;
$PVString = 0;
$PVInt = 1;
$PVDouble = 6;
#
sub dbget {
  $db = shift;
  $to = 0.5;
  $chan = UFCA::connectPV($db, $to);
  if( $chan == 0 ) { exit; }
  $pv = UFCA::getPVName($chan);
  $typ = UFCA::getType($chan, $to);
  $styp = UFCA::getTypeStr($chan, $to);
#  print "$pv type == $typ\n";
  if( $typ == $PVString ) { $i = UFCA::getString($chan, $to); }
  if( $typ == $PVInt ) { $i = UFCA::getInt($chan, $to); }
  if( $typ == $PVDouble ) { $i = UFCA::getDouble($chan, $to); }
  print "$pv == $i, type == $styp\n";
}
# main:
print "getting full list of db fields, this will take a moment..\n";
$dball = `sh -c 'ufflam2epicsd -lvv 2>&1' | grep pvname | cut -d ',' -f2`;
#print "$dball\n";
@dball = split / /,$dball;
# first element seems to be empty line (blank?)
shift @dball;
foreach $db (@dball) {
  chomp $db;
#  print "$db ...\n";
  dbget($db);
}
#
exit;

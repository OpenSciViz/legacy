#!/usr/local/bin/perl
$rcsId = '$Name:  $ $Id: dcdatum.pl 14 2008-06-11 01:49:45Z hon $';
use UFCA;
######################### main #########################

$instrum= "flam";
$Idle = 0;
$Clear = 1;
$Busy = 2;
$Start = 3;
$to = 1.0;

#${instrum}:dc:datum.VAL
#${instrum}:dc:datum.VALAllBias
#${instrum}:dc:datum.VALAllPreamp
#${instrum}:dc:datum.VALBitDepth
#${instrum}:dc:datum.VALCDSReads
#${instrum}:dc:datum.VALExpCnt
#${instrum}:dc:datum.VALHeight
#${instrum}:dc:datum.VALLabDef
#${instrum}:dc:datum.VALMilliSec
#${instrum}:dc:datum.VALPixelBaseClock
#${instrum}:dc:datum.VALPostsets
#${instrum}:dc:datum.VALPresets
#${instrum}:dc:datum.VALSeconds
#${instrum}:dc:datum.VALWidth
#${instrum}:dc:datum.VALtimeout

if( @ARGV > 0 ) {
  $argval = shift;
  if( $argval eq "-h" || $argval eq "-help" ) {
    die "dcdatum.pl [-h[elp]] [instrum]\n";
  }
  $instrum = $argval;
  if( @ARGV > 0 ) { $subsys = shift; }
}
print "dcdatum> $instrum\n";

$apply = UFCA::connectPV("$instrum:apply.DIR", $to);
$applyC = UFCA::connectPV("$instrum:applyC", $to);
$dcD = UFCA::connectPV("$instrum:dc:datum.DIR", $to);
$dcDLab = UFCA::connectPV("$instrum:dc:datum.LabDef", $to);
$dcDC = UFCA::connectPV("$instrum:dc:datumC", $to);
$dcDM = UFCA::connectPV("$instrum:dc:datum.marked", $to);
$pv = UFCA::getPVName($apply);
print "dcdatum> clear: $pv\n";
$p = UFCA::putInt($apply, $Clear);
$c = UFCA::getInt($applyC);
#$p = UFCA::putInt($sysD, $Mark);
$pv = UFCA::getPVName($dcDLab);
$p = UFCA::putInt($dcDLab, 1);
$m = UFCA::getInt($dcDM);
if( $m <= 0 ) { die  "dcdatum> mark failed for $pv ...\n"; }
print "dcdatum> set $pv to 1. $instrum:datum marked; applying start...\n";
$p = UFCA::putInt($apply, $Start);
$c = UFCA::getInt($dcDC);
$pv = UFCA::getPVName($dcDC);
$ca = UFCA::getInt($applyC);
$pva = UFCA::getPVName($apply);
$cnt = 10;
while(  $c == $Busy && $cnt-- > 0 ) {
  print "dcdatum> $pv: $c, $pva: $ca\n";
  sleep 1;
  $c = UFCA::getInt($dcDC);
  $ca = UFCA::getInt($applyC);
}
$cnt = 10;
while( $ca == $Busy && $cnt-- > 0 ) {
  print "dcdatum> $pv: $c, $pva: $ca\n";
  sleep 1;
  $c = UFCA::getInt($dcDC);
  $ca = UFCA::getInt($applyC);
}
print "dcdatum> $pv: $c, $pva: $ca\n";
exit;


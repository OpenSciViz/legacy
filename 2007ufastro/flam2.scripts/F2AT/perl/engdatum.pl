
#!/usr/local/bin/perl
$rcsId = '$Name:  $ $Id: engdatum.pl 14 2008-06-11 01:49:45Z hon $';
use UFCA;

sub dbprint {
  my $cc = `sh -c 'ufflam2epicsd -lvv 2>&1'|grep 'cc:datum'|grep -v 'datumC'|grep -v 'O,'|cut -d' ' -f4`;
  my @dball = split /\n/,$cc;
  my $db = shift @dball;
  foreach $db (@dball) {
    chomp $db;
    print "$db\n";
  }
  $dc = `sh -c 'ufflam2epicsd -lvv 2>&1'| grep 'dc:datum'| grep -v 'datumC'| grep -v 'O,'| cut -d' ' -f4`;
  @dball = split /\n/,$dc;
  $db = shift @dball;
  foreach $db (@dball) {
    chomp $db;
    print "$db\n";
  }
  $ec = `sh -c 'ufflam2epicsd -lvv 2>&1'| grep 'ec:datum'| grep -v 'datumC'| grep -v 'O,'| cut -d' ' -f4`;
  @dball = split /\n/,$ec;
  $db = shift @dball;
  foreach $db (@dball) {
    chomp $db;
    print "$db\n";
  }
}

# main
dbprint();




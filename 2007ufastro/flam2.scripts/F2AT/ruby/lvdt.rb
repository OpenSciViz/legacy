#!/usr/local/bin/ruby
$rcsId = '$Name:  $ $Id: lvdt.rb 14 2008-06-11 01:49:45Z hon $';
require 'UFCA';
######################### main #########################
# globals
$instrum= "flam"
$Idle = 0
$Clear = 1
$Busy = 2
$Start = 3

def sethandler( sig )
  trap( sig ) {
    print "sig = ",sig,"\n";
    if(  sig == "SIGTERM" || sig == "SIGINT" || sig == "SIGABRT" || sig == "SIGTSTP" )
      print "terminating...\n";  exit; 
    end
  }
end

def cmdLVDT( onoff )
  if onoff == "off" then p = UFCA.putInt($ecDLVDT, 0); else p = UFCA.putInt($ecDLVDT, 1); end
  m = UFCA.getInt($ecDM)
  if m <= 0 then print "lvdt> mark failed for ",pv," ...\n"; exit; end
  l = UFCA.getInt($ecDLVDT)
  print "lvdt> set ",$pvL," to ",l,", ",$instrum,":setup marked...applying start...\n"
  p = UFCA.putInt($apply, $Start)
  c = UFCA.getInt($ecDC)
  pv = UFCA.getPVName($ecDC)
  ca = UFCA.getInt($applyC)
  pva = UFCA.getPVName($apply)
  cnt = 10
  while ca == $Busy && --cnt > 0
    print "lvdt> ",pv,": ",c,", ",pva,": ",ca,"\n"
    sleep 1
    c = UFCA.getInt($ecDC); ca = UFCA.getInt($applyC)
  end
  print "lvdt> ",pv,": ",c," ",pva,": ",ca,"\n"
end

def offLVDT( time )
  cmdLVDT( "off" )
  print "sleep for ",time/60," min.\n"
  sleep time
end

def onLVDT( time )
  cmdLVDT( "on" )
  s = 0;
  while ++s <= time 
    volts = UFCA.getDouble($ecDLVDTV)
    t = Time.new
    print "lvdt> ",t," , ",$pvLstat," , ",volts,"\n"
    sleep 1
  end
end

if( ARGV.length > 0 )
  $instrum = argval = ARGV.shift
  if argval == "-h" || argval == "-help" then print "lvdt.rb [-h[elp]] [instrum]\n"; exit; end
end
print "lvdt> ",$instrum,"\n"
sethandler("SIGINT"); sethandler("SIGABRT"); sethandler("SIGTSTP"); sethandler("SIGTERM")

to = 1.0 # timeout
# globals:
$apply = UFCA.connectPV($instrum+":apply.DIR", to)
$applyC = UFCA.connectPV($instrum+":applyC", to)
$ecD = UFCA.connectPV($instrum+":ec:setup.DIR", to)
$ecDC = UFCA.connectPV($instrum+":ec:setupC", to)
$ecDM = UFCA.connectPV($instrum+":ec:setup.marked", to)

$ecDLVDT = UFCA.connectPV($instrum+":ec:setup.LVDTOnOff", to)
$ecDLVDTV = UFCA.connectPV($instrum+":sad:LVDTVLTS.VAL", to)

$pvL = UFCA.getPVName($ecDLVDT)
$pvLstat = UFCA.getPVName($ecDLVDTV)

pv = UFCA.getPVName($apply)
print "lvdt> clear: pv\n"
p = UFCA.putInt($apply, $Clear)
c = UFCA.getInt($applyC)

time = 300 # 5 minutes

# do this until terminated..
while true
  5.times do
    onLVDT( time ) # turn on lvdt and monitor reading time seconds before returning
    offLVDT( time ) # turn off the lvdt and sleep for 5 min. before returning
  end
  time += 300 # increase duration of sample
end

exit


#!/bin/env python
rcsId = '$Name:  $ $Id: cctest.py 14 2008-06-11 01:49:45Z hon $'
import os, signal, sys, time
import UFCA, UFF2

def handler(signum, frame):
  print 'Signal handler called with signal', signum
  sys.stdout.write("exit? [n]: ")
  rep = "y"; rep = sys.stdin.readline()
  if rep[0] == 'y' :
    sys.exit()
#end def
  
signal.signal(signal.SIGINT, handler)

arg = app = sys.argv.pop(0)
instrum = "flam"

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  if arg == "-h" or arg == "-help" :
    print "no help in sight"
    exit
  else:
    instrum = arg
    print "instrum == " + instrum
  #end if
#end if

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  print "arg == " + arg
#end if

# CAR states:
Idle= 0
Busy = 2
Error = 3
# apply/cad directives:
Idle = 0
Clear = 1
Busy = 2
Start = 3
# channel connection timeout:
to = 0.5
#
instrum = "flam"
mech = "all"
#
  #end
#end
apply = UFCA.connectPV(instrum+":eng:apply.DIR", to)
applyC = UFCA.connectPV(instrum+":eng:applyC", to)
test = UFCA.connectPV(instrum+":cc:test.DIR", to)
testC = UFCA.connectPV(instrum+":cc:testC", to)
all = UFCA.connectPV(instrum+":cc:test.All", to)
dck = UFCA.connectPV(instrum+":cc:test.Decker", to)
sdck = UFCA.connectPV(instrum+":sad:DeckerSteps", to)
f1 = UFCA.connectPV(instrum+":cc:test.Filter1", to)
sf1 = UFCA.connectPV(instrum+":sad:Filter1Steps", to)
f2 = UFCA.connectPV(instrum+":cc:test.Filter2", to)
sf2 = UFCA.connectPV(instrum+":sad:Filter2Steps", to)
foc = UFCA.connectPV(instrum+":cc:test.Focus", to)
sfoc = UFCA.connectPV(instrum+":sad:FocusSteps", to)
grsm = UFCA.connectPV(instrum+":cc:test.Grism", to)
sgrsm = UFCA.connectPV(instrum+":sad:GrismSteps", to)
lyot = UFCA.connectPV(instrum+":cc:test.Lyot", to)
slyot = UFCA.connectPV(instrum+":sad:LyotSteps", to)
mos = UFCA.connectPV(instrum+":cc:test.MOS", to)
smos = UFCA.connectPV(instrum+":sad:MOSSteps", to)
win = UFCA.connectPV(instrum+":cc:test.Window", to)
swin = UFCA.connectPV(instrum+":sad:WindowSteps", to)
#icid = UFCA.connectPV(instrum+":cc:test.icid", to)
#mess = UFCA.connectPV(instrum+":cc:test.mess", to)
#ocid = UFCA.connectPV(instrum+":cc:test.ocid", to)
tmo = UFCA.connectPV(instrum+":cc:test.timeout", to)
val = UFCA.connectPV(instrum+":cc:test.val", to)
#
# check current state of setup CAR:
c = UFCA.getInt(testC) pv = UFCA.getPVName(testC) print "pv == c\n"

# clear:
p = UFCA.putInt(apply, Clear)
rall = rindex mech, "all"
rwin = rindex mech, "win"
rdck = rindex mech, "deck"
rmos = rindex mech, "mos"
rf1 = rindex mech, "f1"
rlyot = rindex mech, "lyot"
rf2 = rindex mech, "f2"
rgrsm = rindex mech, "grism"
rfoc = rindex mech, "foc"
if rall >= 0   : p = UFCA.putInt(all, Preset) #end
if rwin >= 0   : p = UFCA.putInt(win, Preset) #end
if rdck >= 0   : p = UFCA.putInt(dck, Preset) #end
if rmos >= 0   : p = UFCA.putInt(mos, Preset) #end
if rf1 >= 0     : p = UFCA.putInt(f1, Preset) #end
if rlyot >= 0  : p = UFCA.putInt(lyot, Preset) #end
if rf2 >= 0    : p = UFCA.putInt(f2, Preset) #end
if rgrsm >= 0  : p = UFCA.putInt(grsm, Preset) #end
if rfoc >= 0   : p = UFCA.putInt(foc, Preset) #end
p = UFCA.putInt(apply, Start)

waitCAR(testC)

printCCSAD()

exit

#################################### subs ############################
sub waitCAR :
  my c = UFCA.getInt(car)
  my pv = UFCA.getPVName(car)
  my ca = UFCA.getInt(applyC)
  my pva = UFCA.getPVName(applyC)
  my cnt = 10
  while  c == Busy and cnt-- > 0  :
    print "waitCAR> pv: c, pva: ca\n"
    time.sleep 1
    c = UFCA.getInt(car)
    ca = UFCA.getInt(applyC)
  #end
  cnt = 10
  while  ca == Busy and cnt-- > 0  :
    print "waitCAR> pv: c, pva: ca\n"
    time.sleep 1
    c = UFCA.getInt(car)
    ca = UFCA.getInt(applyC)
  #end
  print "waitCAR> pv: c, pva: ca\n"
#end

sub printCCSAD :
  my s = UFCA.getInt(swin)
  print "StepCnt -- Window: s, "
  my s = UFCA.getInt(sdck)
  print "Decker: s, "
  my s = UFCA.getInt(smos)
  print "MOS: s, "
  my s = UFCA.getInt(sf1)
  print "Filt1: s, "
  my s = UFCA.getInt(slyot)
  print "Lyot: s, "
  my s = UFCA.getInt(sf2)
  print "Filt2: s, "
  my s = UFCA.getInt(sgrsm)
  print "Grism: s, "
  my s = UFCA.getInt(sfoc)
  print "DetFocus: s\n"
#end

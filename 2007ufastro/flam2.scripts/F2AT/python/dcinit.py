#!/bin/env python
rcsId = '$Name:  $ $Id: dcinit.py 14 2008-06-11 01:49:45Z hon $'
import os, signal, sys, time
import UFCA, UFF2

def handler(signum, frame):
  print 'Signal handler called with signal', signum
  sys.stdout.write("exit? [n]: ")
  rep = "y"; rep = sys.stdin.readline()
  if rep[0] == 'y' :
    sys.exit()
#end def
  
signal.signal(signal.SIGINT, handler)

arg = app = sys.argv.pop(0)
instrum = "flam"

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  if arg == "-h" or arg == "-help" :
    print "no help in sight"
    exit
  else:
    instrum = arg
    print "instrum == " + instrum
  #end if
#end if

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  print "arg == " + arg
#end if

#
# load the Swig generated Epics channel access module:
#
######################### main #########################
#
Idle = 0
Clear = 1
Busy = 2
Start = 3
to = 1.0

instrum= "flam"
host = `hostname`

  #end
#end

print "init> instrum@host, device agents expect connections on host\n"

apply = UFCA.connectPV(instrum+":apply.DIR", to)
applyC = UFCA.connectPV(instrum+":applyC", to)
dcI = UFCA.connectPV(instrum+":dc:init.DIR", to)
dcIM = UFCA.connectPV(instrum+":dc:init.MARK", to)
dcIC = UFCA.connectPV(instrum+":dc:initC", to)
dcIhost = UFCA.connectPV(instrum+":dc:init.host", to)
pv = UFCA.getPVName(apply)
print "init> clear system: pv\n"
p = UFCA.putInt(apply, Clear)
c = UFCA.getInt(applyC)
pv = UFCA.getPVName(dcIhost)
p = UFCA.putString(dcIhost, host)
m = UFCA.getInt(dcIM)
print "set pv to host host. instrum:init marked applying start...\n"
p = UFCA.putInt(apply, Start)
c = UFCA.getInt(dcIC)
pv = UFCA.getPVName(dcIC)
ca = UFCA.getInt(applyC)
pva = UFCA.getPVName(applyC)
cnt = 10
while  c == Busy and cnt-- > 0  :
  print "init> pv: c, pva: ca\n"
  time.sleep 1
  c = UFCA.getInt(dcIC)
  ca = UFCA.getInt(applyC)
#end
cnt = 10
while  ca == Busy and cnt-- > 0  :
  print "init> pv: c, pva: ca\n"
  time.sleep 1
  c = UFCA.getInt(dcIC)
  ca = UFCA.getInt(applyC)
#end
print "init> pv: c, pva: ca\n"
exit


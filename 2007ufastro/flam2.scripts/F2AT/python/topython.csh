#!/bin/csh -f
set list = `ls -1 ../perl/*.pl ../ruby/*.rb`
if( "$1" != "" ) set list = $argv[1-]
foreach i ( $list )
  set f = $i:t ; set f = $f:r ; set f = $f'.py'
  cat $i |sed '/perl/d'|sed '/ruby/d'|sed 's/\$//g'|sed 's/||/or/g'|sed 's/sub/def/'|sed 's/then/:/'|sed 's/;//g'|sed 's/end/\#end/g'|sed 's/"instrum/instrum+"/g'|sed 's/"flam:/instrum+":/g'|sed 's/{/:/g'|sed 's/}/\#end/g'|sed 's/::/./g'|sed 's/&&/and/g'|sed 's/sleep 1/time.sleep(1)/'|sed '/require/d'|sed '/use/d'|sed '/rcs/d'|sed '/shift/d'|sed '/chomp/d'|sed '/ARG/d'|sed '/die/d'|sed '/argval/d' > .$f
#  cat $i |sed -e '/perl/d;/ruby/d;s/\$//g;s/end/\#end/;s/"instrum/instrum+"/;s/"flam:/instrum+":/;s/{/:/;s/}/\#end/;s/::/./;s/( / /;s/ )/ /;s/&&/and/g;s/sleep/time.sleep/;/require/d;/use/d;/rcs/d;shift/d;/chomp/d;/ARG/d;/die/d;/argval/d' > .$f
  cat arg.py0 .$f > $f
  rm .$f
end

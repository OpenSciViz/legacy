#!/bin/env python
rcsId = '$Name:  $ $Id: interrupt.py 14 2008-06-11 01:49:45Z hon $'
import os, signal, sys, time

def handler(signum, frame):
  print 'Signal handler called with signal', signum
  sys.stdout.write("exit? [n]: ")
  rep = "y"; rep = sys.stdin.readline()
  if rep[0] == 'y' :
    sys.exit()
  
signal.signal(signal.SIGINT, handler)


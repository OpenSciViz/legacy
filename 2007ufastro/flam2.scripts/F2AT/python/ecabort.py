#!/bin/env python
rcsId = '$Name:  $ $Id: ecabort.py 14 2008-06-11 01:49:45Z hon $'
import os, signal, sys, time
import UFCA, UFF2

def handler(signum, frame):
  print 'Signal handler called with signal', signum
  sys.stdout.write("exit? [n]: ")
  rep = "y"; rep = sys.stdin.readline()
  if rep[0] == 'y' :
    sys.exit()
#end def
  
signal.signal(signal.SIGINT, handler)

arg = app = sys.argv.pop(0)
instrum = "flam"

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  if arg == "-h" or arg == "-help" :
    print "no help in sight"
    exit
  else:
    instrum = arg
    print "instrum == " + instrum
  #end if
#end if

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  print "arg == " + arg
#end if

# timeout:
to = 0.75
#
# directives:
Mark = 0
Clear = 1
Preset = 2
Start = 3
#
# CAR states:
Idle= 0
Busy = 2
Error = 3
#
# data types:
PVString = 0
PVInt = 1
PVDouble = 6
#
apply = UFCA.connectPV(instrum+":eng:apply.dir", to)
applyR = UFCA.connectPV(instrum+":eng:applyC", to)
init = UFCA.connectPV(instrum+":ec:init.dir", to)
initR = UFCA.connectPV(instrum+":ec:initC", to)
datumR = UFCA.connectPV(instrum+":ec:datumC", to)
camP = UFCA.connectPV(instrum+":ec:datum.CamP", to)
camI = UFCA.connectPV(instrum+":ec:datum.CamI", to)
camD = UFCA.connectPV(instrum+":ec:datum.CamD", to)
camR = UFCA.connectPV(instrum+":ec:datum.CamRange", to)
camK = UFCA.connectPV(instrum+":ec:datum.CamKelvin", to)
mosP = UFCA.connectPV(instrum+":ec:datum.MOSP", to)
mosI = UFCA.connectPV(instrum+":ec:datum.MOSI", to)
mosD = UFCA.connectPV(instrum+":ec:datum.MOSD", to)
mosR = UFCA.connectPV(instrum+":ec:datum.MOSRange", to)
mosK = UFCA.connectPV(instrum+":ec:datum.MOSKelvin", to)
tmo = UFCA.connectPV(instrum+":ec:datumC.timeout", to)
sadmos = UFCA.connectPV(instrum+":sad:MOSKelvin", to)
sadcam = UFCA.connectPV(instrum+":sad:CamKelvin", to)
#
sub getPV :
  my pv = UFCA.getPVName(ch)
  my typ = UFCA.getType(ch, to)
  my styp = UFCA.getTypeStr(ch, to)
  my val
  if typ == PVInt  :
    val = UFCA.getInt(ch, to)
  #end
  if typ == PVDouble  :
    val = UFCA.getDouble(ch, to)
  #end
  if typ == PVString  :
    val = UFCA.getString(ch, to)
  #end
  print "pv type == styp, val == val\n"
  return val
#end
# main:
getPV(sadmos)
getPV(sadcam)
# init:
setPV(initR, Idle)
setPV(init, Preset)
setPV(applyR, Idle)
setPV(apply, Start)
done = 0
while  done == 0  :
  ir = getPV(initR)
  ar = getPV(applyR)
  if ir == Error || ar == Error  : done = -1 #end
  if ir == Idle and ar == Idle  : done = 1 #end
  time.sleep 1
#end
# datum:
k1 = 71.1
k2 = 72.2
setPV(datumR, Idle)
setPV(mosP, 500)
setPV(mosI, 100)
setPV(mosD, 500)
setPV(mosR, 3)
setPV(mosD, k1)
setPV(camP, 500)
setPV(camI, 100)
setPV(camD, 500)
setPV(camR, 3)
setPV(camD, k2)
setPV(applyR, Idle)
setPV(apply, Start)
done = 0
while  done == 0  :
  ir = getPV(initR)
  ar = getPV(applyR)
  if ir == Error || ar == Error  : done = -1 #end
  if ir == Idle and ar == Idle  : done = 1 #end
  time.sleep 1
#end
#
exit

#!/bin/env python
rcsId = '$Name:  $ $Id: initdatum.py 14 2008-06-11 01:49:45Z hon $'
import os, signal, sys, time
import UFCA, UFF2

def handler(signum, frame):
  print 'Signal handler called with signal', signum
  sys.stdout.write("exit? [n]: ")
  rep = "y"; rep = sys.stdin.readline()
  if rep[0] == 'y' :
    sys.exit()
#end def
  
signal.signal(signal.SIGINT, handler)

arg = app = sys.argv.pop(0)
instrum = "flam"

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  if arg == "-h" or arg == "-help" :
    print "no help in sight"
    exit
  else:
    instrum = arg
    print "instrum == " + instrum
  #end if
#end if

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  print "arg == " + arg
#end if

######################### main #########################
#dbprint()
Idle = 0
Clear = 1
Busy = 2
Start = 3
to = 1.0
init = 0
limit = 0
stepcnt = 0
init = 1
datum = 1
sys = "all"

    print "focus.pl [-help] [-init[only]] [-datum[only] [subsys]]\n"
    exit
  #end
    init = 1 datum = 0
  #end
    datum = 1 init = 0
  #end
  else :
  #end
#end
#end
print "initdatum> init: init, datum: datum, sys: sys\n"

apply = UFCA.connectPV(instrum+":apply.DIR", to)
applyC = UFCA.connectPV(instrum+":applyC", to)
sysI = UFCA.connectPV(instrum+":init.DIR", to)
sysIC = UFCA.connectPV(instrum+":initC", to)
sysD = UFCA.connectPV(instrum+":datum.DIR", to)
sysDC = UFCA.connectPV(instrum+":datumC", to)
if init == 1  :
  p = UFCA.putInt(sysI, Start)
  c = UFCA.getInt(sysIC)
  print "sys init car: c\n"
  cnt = 3
  while  c == Busy and cnt-- > 0  :
  print "sys init car: c\n"
    time.sleep 1
    c = UFCA.getInt(sysIC)
  #end
#end
if datum == 1  :
  p = UFCA.putInt(sysD, Start)
  c = UFCA.getInt(sysDC)
  while  c == Busy and cnt-- > 0  :
  print "sys datum car: c\n"
    time.sleep 1
    c = UFCA.getInt(sysDC)
  #end
#end

exit

############################## subs #####################
sub dbprint :
  my cc = `ufflam2epicsd -lvv |grep 'cc:focus'|grep -v 'focusC'|grep -v 'O,'|cut -d' ' -f4`
  my @dball = split /\n/,cc
  foreach db (@dball) :
    print "db\n"
  #end
  dc = `ufflam2epicsd -lvv | grep 'dc:focus'| grep -v 'focusC'| grep -v 'O,'| cut -d' ' -f4`
  @dball = split /\n/,dc
  foreach db (@dball) :
    print "db\n"
  #end
  ec = `ufflam2epicsd -lvv | grep 'ec:focus'| grep -v 'focusC'| grep -v 'O,'| cut -d' ' -f4`
  @dball = split /\n/,ec
  foreach db (@dball) :
    print "db\n"
  #end
  eng = `ufflam2epicsd -lvv | grep 'eng:focus'| grep -v 'focusC'| grep -v 'O,'| cut -d' ' -f4`
  @dball = split /\n/,eng
  foreach db (@dball) :
    print "db\n"
  #end
#end


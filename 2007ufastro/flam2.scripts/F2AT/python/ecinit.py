#!/usr/bin/env python
rcsId = '$Name:  $ $Id: ecinit.py 14 2008-06-11 01:49:45Z hon $'
import os, signal, socket, sys, time
from subprocess import *
ufpy = os.getenv("UFINSTALL") + "/python"
if not (ufpy == None) :
  print "ufpy= "+ufpy
else:
  print "UFINSTALL is not defined, abort..."
  sys.exit()
ldpath = os.getenv("LD_LIBRARY_PATH")
if not (ldpath == None) :
  print "ldpath= "+ldpath
else:
  print "LD_LIBRARY_PATH is not defined, abort..."
  sys.exit()
pypath = os.getenv("PYTHONPATH")
if not (pypath == None) :
  print "pypath= "+pypath
else:
  print "PYTHONPATH is not defined, abort..."
  sys.exit()

import UFCA
from UFF2 import *

#UFF2.ufF2printAllNamedPositions()
ufF2printAllNamedPositions()

def handler(signum, frame):
  print 'Signal handler called with signal', signum
  sys.stdout.write("exit? [n]: ")
  rep = "y"; rep = sys.stdin.readline()
  if rep[0] == 'y' :
    sys.exit()
#end def
  
signal.signal(signal.SIGINT, handler)

arg = app = sys.argv.pop(0)
instrum = "fu" #"flam"
#host = `/usr/bin/env hostname`
#host = Popen(["/usr/bin/env", "hostname"], stdout=PIPE).communicate()[0]
host = socket.gethostname()
print "host= "+host+", argv0= "+arg+", instrum= "+instrum

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  if arg == "-h" or arg == "-help" :
    print "no help in sight"
    exit
  else:
    instrum = arg
    print "instrum= " + instrum
  #end if
#end if

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  print "arg= " + arg
#end if
Idle = 0
Clear = 1
Busy = 2
Start = 3
to = 1.0

print "ecinit> ",instrum," device agent connections on ",host,""
apply = UFCA.connectPV(instrum+":apply.DIR", to)
applyC = UFCA.connectPV(instrum+":applyC", to)
ecI = UFCA.connectPV(instrum+":ec:init.DIR", to)
ecIM = UFCA.connectPV(instrum+":ec:init.MARK", to)
ecIC = UFCA.connectPV(instrum+":ec:initC", to)
ecIhost = UFCA.connectPV(instrum+":ec:init.host", to)
pv = UFCA.getPVName(apply)
print "ecinit> clear system: ",pv,""
p = UFCA.putInt(apply, Clear)
c = UFCA.getInt(applyC)
pv = UFCA.getPVName(ecIhost)
p = UFCA.putString(ecIhost, host)
m = UFCA.getInt(ecIM)
if m <= 0 : print "ecinit> mark failed for",pv,"" #end
print "ecinit> set ",pv," to host ",host," , ",instrum,":init marked, applying start...",""
p = UFCA.putInt(apply, Start)
c = UFCA.getInt(ecIC)
pv = UFCA.getPVName(ecIC)
ca = UFCA.getInt(applyC)
pva = UFCA.getPVName(applyC)
cnt = 10
while c == Busy or ca == Busy and --cnt > 0 :
  print "ecinit> ",pv,": ", c, ", ",pva,": ",ca,""
  time.sleep(1)
  c = UFCA.getInt(ecIC)
  ca = UFCA.getInt(applyC)
#end
print "ecinit> ",pv,": ", c, ", ",pva,": ",ca,""
sys.exit()

#!/bin/env python
rcsId = '$Name:  $ $Id: reboot.py 14 2008-06-11 01:49:45Z hon $'
import os, signal, sys, time
import UFCA, UFF2

def handler(signum, frame):
  print 'Signal handler called with signal', signum
  sys.stdout.write("exit? [n]: ")
  rep = "y"; rep = sys.stdin.readline()
  if rep[0] == 'y' :
    sys.exit()
#end def
  
signal.signal(signal.SIGINT, handler)

arg = app = sys.argv.pop(0)
instrum = "flam"

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  if arg == "-h" or arg == "-help" :
    print "no help in sight"
    exit
  else:
    instrum = arg
    print "instrum == " + instrum
  #end if
#end if

if len(sys.argv) > 0 :
  arg = sys.argv.pop(0)
  print "arg == " + arg
#end if

######################### main #########################
#dbprint()
Idle = 0
Clear = 1
Busy = 2
Start = 3
to = 1.0
init = 0
reboot = 1
sys = "all"

    print "focus.pl [-help] [-init[only]] [-reboot[only] [subsys]]\n"
    exit
  #end
    init = 1 reboot = 0
  #end
    reboot = 1 init = 0
  #end
  else :
  #end
#end
#end
print "initreboot> init: init, reboot: reboot, sys: sys\n"

apply = UFCA.connectPV(instrum+":apply.DIR", to)
applyC = UFCA.connectPV(instrum+":applyC", to)
sysI = UFCA.connectPV(instrum+":init.DIR", to)
sysIC = UFCA.connectPV(instrum+":initC", to)
sysR = UFCA.connectPV(instrum+":reboot.DIR", to)
sysRC = UFCA.connectPV(instrum+":rebootC", to)
sysRsub = UFCA.connectPV(instrum+":reboot.subsys", to)
sysRF2 = UFCA.connectPV(instrum+":reboot.F2host", to)
sysRWFS = UFCA.connectPV(instrum+":reboot.F2OIWFS", to)
#
name = UFCA.getPVName(sysRC)
c = UFCA.getInt(sysRC)
print "sys reboot car: name, VAL: c\n"

if init == 1  :
  p = UFCA.putInt(sysI, Start)
  c = UFCA.getInt(sysIC)
  print "sys init car: c\n"
  c = Busy cnt = 3
  while  c == Busy and cnt-- > 0  :
  print "sys init car: c\n"
    time.sleep 1
    c = UFCA.getInt(sysIC)
  #end
#end
if reboot == 1  :
#  p = UFCA.putString(sysRsub, "all")
#  p = UFCA.putString(sysRF2, "irflam2a")
#  p = UFCA.putString(sysRWFS, "f2:wfs:reboot")
  subsys = UFCA.getString(sysRsub)
  f2host = UFCA.getString(sysRF2)
  wfs = UFCA.getString(sysRWFS)
  p = UFCA.putInt(sysR, Start)
  print "sys reboot F2host, subsys, wfs: f2host, subsys, wfs\n"
  c = Busy cnt = 3
  c = UFCA.getInt(sysRC)
  while  c == Busy and cnt-- > 0  :
  print "sys reboot car: c\n"
    time.sleep 1
    c = UFCA.getInt(sysRC)
  #end
#end

exit

############################## subs #####################
sub dbprint :
  my cc = `ufflam2epicsd -lvv |grep 'cc:focus'|grep -v 'focusC'|grep -v 'O,'|cut -d' ' -f4`
  my @dball = split /\n/,cc
  foreach db (@dball) :
    print "db\n"
  #end
  dc = `ufflam2epicsd -lvv | grep 'dc:focus'| grep -v 'focusC'| grep -v 'O,'| cut -d' ' -f4`
  @dball = split /\n/,dc
  foreach db (@dball) :
    print "db\n"
  #end
  ec = `ufflam2epicsd -lvv | grep 'ec:focus'| grep -v 'focusC'| grep -v 'O,'| cut -d' ' -f4`
  @dball = split /\n/,ec
  foreach db (@dball) :
    print "db\n"
  #end
  eng = `ufflam2epicsd -lvv | grep 'eng:focus'| grep -v 'focusC'| grep -v 'O,'| cut -d' ' -f4`
  @dball = split /\n/,eng
  foreach db (@dball) :
    print "db\n"
  #end
#end


#!/usr/local/bin/perl
use UFCA;
######################### main #########################
$Idle = 0;
$Clear = 1;
$Busy = 2;
$Start = 3;
$to = 1.0;

$epicsDB = "flam2";
$pollPeriod = 5;
open(FD, ">&STDOUT");
$filename = "STDOUT";
$useFile1 = 0;
$useFile2 = 0;
#################Handle comand line arguments############
$numArgs = $#ARGV + 1;
for ($argnum=0; $argnum < $numArgs; $argnum++) {
    $argval = $ARGV[$argnum];
    if ($argval eq "-h" || $argval eq "-help" || $argval eq "--help") {
	print "Usage: uftemperaturelogger [OPTION]...[FILE]\n";
	print "Logs temperature values from lakeshores to [FILE] (default is STDOUT)\n";
	print "\n";
	print "Arguments:\n";
	print "  -epics <name>\tEpics DB name\n";
	print "  -pollperiod <time>\tPoll Period in seconds\n";
	exit;
    }elsif ($argval eq "-epics") {
	if ($argval lt $numArgs-1) {
	    $argnum++;
	    $epicsDB = $ARGV[$argnum];
	}
    }elsif ($argval eq "-pollperiod") {
	if ($argval lt $numArgs-1) {
	    $argnum++;
	    $pollPeriod = $ARGV[$argnum];
	}
    }elsif ($argval eq "-dir1") {
	    if ($argval lt $numArgs-1) {
		    $argnum++;
		    $dir1 = $ARGV[$argnum];
		    $useFile1 = 1;
	    }
    }elsif ($argval eq "-dir2") {
	    if ($argval lt $numArgs-1) {
		    $argnum++;
		    $dir2 = $ARGV[$argnum];
		    $useFile2 = 1;
	    }
    }elsif ($argval eq "-dir3") {
	    if ($argval lt $numArgs-1) {
		    $argnum++;
		    $dir3 = $ARGV[$argnum];
		    $useFile3 = 1;
	    }
    }else{
	close(FD);
	open(FD, ">$argval") 
	    or die "Cannot open file $argval for writing\n";
	$filename = $argval;
    }
}

close(FD);

$recs[0] =  UFCA::connectPV($epicsDB.":sad:MOSVAC",$to);
$recs[1] =  UFCA::connectPV($epicsDB.":sad:CAMVAC",$to);
$recs[2] =  UFCA::connectPV($epicsDB.":sad:MOSCOLD1",$to);
$recs[3] =  UFCA::connectPV($epicsDB.":sad:MOSBENC2",$to);
$recs[4] =  UFCA::connectPV($epicsDB.":sad:MOSBENC3",$to);
$recs[5] =  UFCA::connectPV($epicsDB.":sad:MOSBENC4",$to);
$recs[6] =  UFCA::connectPV($epicsDB.":sad:CAMCOLD5",$to);
$recs[7] =  UFCA::connectPV($epicsDB.":sad:CAMBENC6",$to);
$recs[8] =  UFCA::connectPV($epicsDB.":sad:CAMBENC7",$to);
$recs[9] =  UFCA::connectPV($epicsDB.":sad:CAMBENC8",$to);
$recs[10] =  UFCA::connectPV($epicsDB.":sad:CAMABENC",$to);
$recs[11] =  UFCA::connectPV($epicsDB.":sad:CAMBDETC",$to);
$recs[12] =  UFCA::connectPV($epicsDB.":sad:CAMASETP",$to);
$recs[13] =  UFCA::connectPV($epicsDB.":sad:CAMBSETP",$to);
$recs[14] =  UFCA::connectPV($epicsDB.":sad:LVDTVLTS",$to);
$recs[15] =  UFCA::connectPV($epicsDB.":sad:FocusSteps.VAL",$to);
$recs[16] =  UFCA::connectPV($epicsDB.":sad:CAMAPID",$to);
$recs[17] =  UFCA::connectPV($epicsDB.":sad:CAMAHTPW",$to);
$recs[18] =  UFCA::connectPV($epicsDB.":sad:CAMAAPWR",$to);
$recs[19] =  UFCA::connectPV($epicsDB.":sad:CAMBPID",$to);
$recs[20] =  UFCA::connectPV($epicsDB.":sad:CAMBHTPW",$to);
$recs[21] =  UFCA::connectPV($epicsDB.":sad:CAMBAPWR",$to);

$logfile = UFCA::connectPV($epicsDB.":sad:LOGFILE",$to); 
$logsize = UFCA::connectPV($epicsDB.":sad:LOGSIZE",$to);
$loghost = UFCA::connectPV($epicsDB.":sad:LOGHOST",$to);

$timestamp = time,dc;
my ($s,$m,$h,$md,$mo,$y,$wd,$yd,$isd) = localtime($timestamp);
$startDay = $md;
$y = $y + 1900;
$mo = $mo + 1;

$cday = `date +%d`;
$cmo = `date +%m`;
$cy = `date +%Y`;

chomp($cday);
chomp($cmo);
chomp($cy);

$currTime = `date +%H:%M:%S`;
chomp($currTime);

$file1Exists = 0;
$file2Exists = 0;
if ($useFile1) {
	$filename = $dir1 . "/f2Log${cmo}-${cday}-${cy}";
	if (-e $filename) {
		$file1Exists = 1;
	}
}
open(FD, ">>${filename}");
if ($useFile2) {
	$filename2 = $dir2 . "/f2Log${cmo}-${cday}-${cy}";
	if (-e $dir2) {
		if (-e $filename2) {
			$file2Exists = 1;
		}
		open(FDB, ">>${filename2}");
	} else {
		print "Directory ${dir2} does not exist!";
		$useFile2 = 0;
	}
}
if ($useFile3) {
	$filename3 = $dir3 . "/f2Log${cmo}-${cday}-${cy}";
	if (-e $dir3) {
		if (-e $filename3) {
			$file3Exists = 1;
		}
		open(FDC, ">>${filename3}");
	} else {
		print "Directory ${dir3} does not exist!";
		$useFile3 = 0;
	}
}
UFCA::putString($logfile,$filename);
UFCA::putString($loghost,`hostname`);
#$line =  "BEGIN TIMESTAMP: $timestamp $mo-$md-$y $h:$m:$s";
#$line =  "BEGIN TIMESTAMP: $timestamp $mo-$md-$y $currTime";

#Get GMT time
($s, $m, $h, $md, $mo, $y, $wd, $yd, $isd) = gmtime(time);
if ($h < 10) { $h = "0$h"; }
if ($m < 10) { $m = "0$m"; }
if ($s < 10) { $s = "0$s"; }
@weekday = ( 'Sun.', 'Mon.', 'Tue.', 'Wed.', 'Thu.', 'Fri.', 'Sat.' );
@month = ( 'Jan.', 'Feb.', 'Mar.', 'Apr.', 'May.', 'Jun.', 'Jul.', 'Aug.', 'Sep.', 'Oct.', 'Nov.', 'Dec.' );
$y += 1900; $yd++;
$gmt = "$h:$m:$s $weekday[$wd] $month[$mo] $md $y";
$line = "BEGIN TIMESTAMP: $timestamp GMT: $gmt";

if (not $file1Exists) {
	print FD "$line\n";
	#print FD "DATE, TIME OFFSET, CAMBENCA, CAMDETCB, MOSVAC, CAMVAC, MOSCOLD1, MOSBENC2, MOSBENC3, MOSBENC4, CAMCOLD5, CAMBENC6, CAMBENC7, CAMBENC8\n";
        print FD "DATE,\tTIME OFFSET,\tMOSVAC,\tCAMVAC,\tMOSCOLD1,\tMOSBENC2,\tMOSBENC3,\tMOSBENC4,\tCAMCOLD5,\tCAMBENC6,\tCAMBENC7,\tCAMBENC8,\tCAMBENCA,\tCAMDETCB,\tCAMASETPNT,\tCAMBSETPNT,\tLVDTVLTS,\tFocusSteps\tCAMA P,   I,   D,\tCAMAHTPW,\tCAMAAPWR,\tCAMB P,   I,   D,\tCAMBHTPW,\tCAMBAPWR\n";
}
if ($useFile2 and not $file2Exists) {
	print FDB "$line\n";
	#print FDB "DATE, TIME OFFSET, CAMBENCA, CAMDETCB, MOSVAC, CAMVAC, MOSCOLD1, MOSBENC2, MOSBENC3, MOSBENC4, CAMCOLD5, CAMBENC6, CAMBENC7, CAMBENC8\n";  
        print FDB "DATE,\tTIME OFFSET,\tMOSVAC,\tCAMVAC,\tMOSCOLD1,\tMOSBENC2,\tMOSBENC3,\tMOSBENC4,\tCAMCOLD5,\tCAMBENC6,\tCAMBENC7,\tCAMBENC8,\tCAMBENCA,\tCAMDETCB,\tCAMASETPNT,\tCAMBSETPNT,\tLVDTVLTS,\tFocusSteps\tCAMA P,   I,   D,\tCAMAHTPW,\tCAMAAPWR,\tCAMB P,   I,   D,\tCAMBHTPW,\tCAMBAPWR\n";
}
if ($useFile3 and not $file3Exists) {
	print FDC "$line\n";
	#print FDC "DATE, TIME OFFSET, CAMBENCA, CAMDETCB, MOSVAC, CAMVAC, MOSCOLD1, MOSBENC2, MOSBENC3, MOSBENC4, CAMCOLD5, CAMBENC6, CAMBENC7, CAMBENC8\n";
        print FDC "DATE,\tTIME OFFSET,\tMOSVAC,\tCAMVAC,\tMOSCOLD1,\tMOSBENC2,\tMOSBENC3,\tMOSBENC4,\tCAMCOLD5,\tCAMBENC6,\tCAMBENC7,\tCAMBENC8,\tCAMBENCA,\tCAMDETCB,\tCAMASETPNT,\tCAMBSETPNT,\tLVDTVLTS,\tFocusSteps\tCAMA P,   I,   D,\tCAMAHTPW,\tCAMAAPWR,\tCAMB P,   I,   D,\tCAMBHTPW,\tCAMBAPWR\n";
}
$oldsize = 0;
$currDay = $startDay;
while (1) {
    my ($sec,$min,$hour,$mday,$mon,$year,
	$wday,$yday,$isdst) = localtime time;
    #Get GMT time
    ($s, $m, $h, $md, $mo, $y, $wd, $yd, $isd) = gmtime(time);
    if ($h < 10) { $h = "0$h"; }
    if ($m < 10) { $m = "0$m"; }
    if ($s < 10) { $s = "0$s"; }
    @weekday = ( 'Sun.', 'Mon.', 'Tue.', 'Wed.', 'Thu.', 'Fri.', 'Sat.' );
    @month = ( 'Jan.', 'Feb.', 'Mar.', 'Apr.', 'May.', 'Jun.', 'Jul.', 'Aug.', 'Sep.', 'Oct.', 'Nov.', 'Dec.' );
    $y += 1900; $yd++;
    $gmt = "$h:$m:$s $weekday[$wd] $month[$mo] $md $y";

    $currDay = $mday;
    $mon = $mon + 1;
    $newtime = time,dc;
    #$line = $newtime - $timestamp;
    $line = $newtime;
    $line = $line.",\t";
    for ($count=0; $count<22; $count++) {
	if ($count == 16 || $count == 19) {
	   $line = $line.UFCA::getString($recs[$count]).",\t";
	} else {
	   $line = $line.UFCA::getDouble($recs[$count]).",\t";
	}
    }
    $line = substr($line,0,length($line)-2);
    print FD "$mon/$mday,\t$line,\t$gmt\n";
    if ($useFile2) {
	    print FDB "$mon/$mday,\t$line,\t$gmt\n";
    }
    if ($useFile3) {
	    print FDC "$mon/$mday,\t$line,\t$gmt\n";
    }
    select((select(FD), $| = 1)[0]);
    select((select(FDB), $| = 1)[0]);
    select((select(FDC), $| = 1)[0]);
    $newsize = -s "$filename";
    UFCA::putInt($logsize,$newsize);
    if ($newsize - $oldsize > 1000) {
      #print STDERR "Filesize: $newsize\n";
      $oldsize = $newsize;
    }
    sleep($pollPeriod);
}
close (FD);
if ($useFile2) {
	close(FDB);
}
if ($useFile3) {
	close(FDC);
}
exit;


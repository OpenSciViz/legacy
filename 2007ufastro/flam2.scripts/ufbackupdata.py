#!/usr/bin/python
import os, sys, glob;
from datetime import datetime;
sourcedir = ""
if (len(sys.argv) == 1):
   print "Usage: ufbackupdata.py source_dir [-gzip] dest_dir1 [-gzip] dest_dir2... [-subdirs]";
else:
   sourcedir = sys.argv[1];
if (sourcedir == "-help"):
   print "Usage: ufbackupdata.py source_dir [-gzip] dest_dir1 [-gzip] dest_dir2... [-subdirs]";
n = 0;
narg = 2;
dogzip = []
destdir = []
subdirs = False;
while (len(sys.argv) > narg and os.access(sourcedir, os.F_OK)):
   if (sys.argv[narg] == '-subdirs'):
      narg+=1;
      subdirs = True;
   elif (sys.argv[narg] == '-gzip'): 
      narg+=1;
      if (len(sys.argv) > narg):
	dogzip.append(True);
	destdir.append(sys.argv[narg]);
	narg+=1;
	n+=1;
   else:
      dogzip.append(False);
      destdir.append(sys.argv[narg]);
      narg+=1;
      n+=1;
for j in range(n):
   starttime = str(datetime.today());
   if (not os.access(destdir[j]+"backuplogs", os.F_OK)):
      os.mkdir(destdir[j]+"backuplogs");
   logfile = destdir[j]+"backuplogs/backuplog_"+starttime.replace(' ','_');
   print "Syncing "+sourcedir+" to "+destdir[j];
   if (not os.access(sourcedir, os.R_OK)):
      print "ERROR: "+sourcedir+" not found!";
      f = open(logfile,'ab');
      f.write("ERROR: "+sourcedir+" not found!\n");
      f.close();
      continue
   if (subdirs):
      ssubdirs = glob.glob(sourcedir+'/*');
   else:
      ssubdirs = [sourcedir];
   dsubdirs = [];
   for l in range(len(ssubdirs)):
      if (not os.path.isdir(ssubdirs[l])):
	dsubdirs.append('')
	continue
      if (not ssubdirs[l].endswith('/')):
	ssubdirs[l] += '/';
      dsubdirs.append(ssubdirs[l].replace(sourcedir, destdir[j]));
      if (subdirs and not os.access(dsubdirs[l]+'sync', os.F_OK)):
	isSync = False
      else:
	isSync = True
      if (subdirs and os.access(dsubdirs[l]+'sync', os.F_OK)):
	gz = glob.glob(dsubdirs[l]+'*.gz')
	if (len(gz) == 0):
	   isSync = False
	   try:
	      os.unlink(dsubdirs[l]+'sync')
	   except Exception:
	      print 'Unable to remove file '+dsubdirs[l]+'sync'
      #if (subdirs and not os.access(dsubdirs[l]+'sync', os.F_OK)):
      if (not isSync):
        print "Syncing "+ssubdirs[l]+" to "+dsubdirs[l];
	f = open(logfile,'ab');
	f.write("Syncing "+ssubdirs[l]+" to "+dsubdirs[l]+"\n");
	f.close();
	os.system("rsync -Cauvbt "+ssubdirs[l]+" "+dsubdirs[l]+" >> "+logfile);
	if (dogzip[j]):
	   os.system("gzip -rf "+dsubdirs[l]);
	os.system("touch "+dsubdirs[l]+"sync");
      elif (not subdirs):
	os.system("rsync -Cauvbt "+ssubdirs[l]+" "+dsubdirs[l]+" >> "+logfile);
        if (dogzip[j]):
           os.system("gzip -rf "+dsubdirs[l]);
   #os.system("rsync -Cauvzb "+sourcedir+" "+destdir[j]+" > "+logfile); 
   f = open(logfile,'ab');
   f.write('Started: '+starttime+'\n');
   f.write('Finished: '+str(datetime.today())+'\n');
   f.close();
   #if (dogzip[j]):
      #os.system("gzip -r "+destdir[j]);

#!/usr/local/bin/perl
use POSIX;
use UFCA;
######################### main #########################
$Idle = 0;
$Clear = 1;
$Busy = 2;
$Start = 3;
$to = 1.0;

$epicsDB = "flam";


#@names = ('Window', 'MOS', 'Decker', 'Filter1', 'Lyot', 'Filter2', 'Grism', 'Focus');

#foreach  $i (@names) {
#    print "$i\n";
#}

#for ($i=0; $i<@names; $i++) {
#    print "$names[$i]\n";
#    print "$i\n";
#}


$numArgs = $#ARGV + 1;
for ($argnum=0; $argnum < $numArgs; $argnum++) {
    $argval = $ARGV[$argnum];
    if ($argval eq "-h" || $argval eq "-help" || $argval eq "--help") {
	die "starttemperatureloggers.pl [-h[elp]] [-epics [instrum]]\n";
    }elsif ( $argval eq "-noepics") {
	$epicsDB = "-noepics";
    }elsif ($argval eq "-epics") {
	if ($argval lt $numArgs-1) {
	    $argnum++;
	    $epicsDB = $ARGV[$argnum];
	}
    }
}


###################################################
############## Window Params ######################
###################################################

putVal($epicsDB.":cc:setup.WindowInitVel",    25);
putVal($epicsDB.":cc:setup.WindowSlewVel",   100);
putVal($epicsDB.":cc:setup.WindowAcc",       255);
putVal($epicsDB.":cc:setup.WindowDec",       255);
putVal($epicsDB.":cc:setup.WindowRunCur",       25);
putVal($epicsDB.":cc:setup.WindowBacklash",    0);
putVal($epicsDB.":cc:datum.WindowDirection",   0);
putVal($epicsDB.":cc:datum.WindowVel",       100);



###################################################
################# MOS Params ######################
###################################################

putVal($epicsDB.":cc:setup.MOSInitVel",       25);
putVal($epicsDB.":cc:setup.MOSSlewVel",      100);
putVal($epicsDB.":cc:setup.MOSAcc",          255);
putVal($epicsDB.":cc:setup.MOSDec",          255);
putVal($epicsDB.":cc:setup.MOSRunCur",          25);
putVal($epicsDB.":cc:setup.MOSBacklash",       0);
putVal($epicsDB.":cc:datum.MOSDirection",      0);
putVal($epicsDB.":cc:datum.MOSVel",          100);


###################################################
############### MOS Barcd Params ##################
###################################################

putVal($epicsDB.":cc:setup.MOSBarCdInitVel",       25);
putVal($epicsDB.":cc:setup.MOSBarCdSlewVel",      100);
putVal($epicsDB.":cc:setup.MOSBarCdAcc",          255);
putVal($epicsDB.":cc:setup.MOSBarCdDec",          255);
putVal($epicsDB.":cc:setup.MOSBarCdRunCur",          25);
putVal($epicsDB.":cc:setup.MOSBarCdBacklash",       0);
putVal($epicsDB.":cc:datum.MOSBarCdDirection",      0);
putVal($epicsDB.":cc:datum.MOSBarCdVel",          100);



###################################################
############## Decker Params ######################
###################################################

putVal($epicsDB.":cc:setup.DeckerInitVel",    25);
putVal($epicsDB.":cc:setup.DeckerSlewVel",   100);
putVal($epicsDB.":cc:setup.DeckerAcc",       255);
putVal($epicsDB.":cc:setup.DeckerDec",       255);
putVal($epicsDB.":cc:setup.DeckerRunCur",       25);
putVal($epicsDB.":cc:setup.DeckerBacklash",    0);
putVal($epicsDB.":cc:datum.DeckerDirection",   0);
putVal($epicsDB.":cc:datum.DeckerVel",       100);



###################################################
############## Filter1 Params #####################
###################################################

putVal($epicsDB.":cc:setup.Filter1InitVel",   25);
putVal($epicsDB.":cc:setup.Filter1SlewVel",  600);
putVal($epicsDB.":cc:setup.Filter1Acc",      255);
putVal($epicsDB.":cc:setup.Filter1Dec",      255);
putVal($epicsDB.":cc:setup.Filter1RunCur",      25);
putVal($epicsDB.":cc:setup.Filter1Backlash",   0);
putVal($epicsDB.":cc:datum.Filter1Direction",  0);
putVal($epicsDB.":cc:datum.Filter1Vel",      600);



###################################################
################# Lyot Params #####################
###################################################

putVal($epicsDB.":cc:setup.LyotInitVel",      25);
putVal($epicsDB.":cc:setup.LyotSlewVel",     200);
putVal($epicsDB.":cc:setup.LyotAcc",         255);
putVal($epicsDB.":cc:setup.LyotDec",         255);
putVal($epicsDB.":cc:setup.LyotRunCur",         25);
putVal($epicsDB.":cc:setup.LyotBacklash",      0);
putVal($epicsDB.":cc:datum.LyotDirection",     0);
putVal($epicsDB.":cc:datum.LyotVel",         200);



###################################################
############## Filter2 Params #####################
###################################################

putVal($epicsDB.":cc:setup.Filter2InitVel",   25);
putVal($epicsDB.":cc:setup.Filter2SlewVel",  600);
putVal($epicsDB.":cc:setup.Filter2Acc",      255);
putVal($epicsDB.":cc:setup.Filter2Dec",      255);
putVal($epicsDB.":cc:setup.Filter2RunCur",      25);
putVal($epicsDB.":cc:setup.Filter2Backlash",   0);
putVal($epicsDB.":cc:datum.Filter2Direction",  0);
putVal($epicsDB.":cc:datum.Filter2Vel",      600);



###################################################
################ Grism Params #####################
###################################################

putVal($epicsDB.":cc:setup.GrismInitVel",     25);
putVal($epicsDB.":cc:setup.GrismSlewVel",    600);
putVal($epicsDB.":cc:setup.GrismAcc",        255);
putVal($epicsDB.":cc:setup.GrismDec",        255);
putVal($epicsDB.":cc:setup.GrismRunCur",        25);
putVal($epicsDB.":cc:setup.GrismBacklash",     0);
putVal($epicsDB.":cc:datum.GrismDirection",    0);
putVal($epicsDB.":cc:datum.GrismVel",        600);



###################################################
################ Focus Params #####################
###################################################

putVal($epicsDB.":cc:setup.FocusInitVel",     25);
putVal($epicsDB.":cc:setup.FocusSlewVel",    100);
putVal($epicsDB.":cc:setup.FocusAcc",        255);
putVal($epicsDB.":cc:setup.FocusDec",        255);
putVal($epicsDB.":cc:setup.FocusRunCur",        25);
putVal($epicsDB.":cc:setup.FocusBacklash",     0);
putVal($epicsDB.":cc:datum.FocusDirection",    0);
putVal($epicsDB.":cc:datum.FocusVel",        100);


putVal($epicsDB.":cc:initC.IVAL", POSIX::INT_MIN);
putVal($epicsDB.":cc:initC.VAL", POSIX::INT_MIN);
putVal($epicsDB.":dc:initC.IVAL", POSIX::INT_MIN);
putVal($epicsDB.":dc:initC.VAL", POSIX::INT_MIN);
putVal($epicsDB.":ec:initC.IVAL", POSIX::INT_MIN);
putVal($epicsDB.":ec:initC.VAL", POSIX::INT_MIN);



sleep(2.0);

sub putVal {
    my $ch = shift @_;
    my $val = shift @_;
    my $chan = UFCA::connectPV($ch,$to);
    UFCA::putString($chan,$val);
}

exit;


#!/usr/local/bin/perl

$month = `date | cut -f 2 -d " "`;
$day = `date +%d`;

$mo = `date +%m`;

$y = `date +%Y`;

chomp($month);
chomp($day);
chomp($mo);
chomp($y);

$epicsDB = "flam";




$numArgs = $#ARGV + 1;
for ($argnum=0; $argnum < $numArgs; $argnum++) {
    $argval = $ARGV[$argnum];
    if ($argval eq "-h" || $argval eq "-help" || $argval eq "--help") {
	die "starttemperatureloggers.pl [-h[elp]] [-epics [instrum]]\n";
    }elsif ( $argval eq "-noepics") {
	$epicsDB = "-noepics";
    }elsif ($argval eq "-epics") {
	if ($argval lt $numArgs-1) {
	    $argnum++;
	    $epicsDB = $ARGV[$argnum];
	}
    }
}


if ( $epicsDB eq "-noepics") {
    $origFileName= "/share/data/environment/F2Log${date}_noepics";
    $tmpFileName = $origFileName;
    while ( -e "$tmpFileName") {
	$tmpFileName = $tmpFileName.".bak";
    }
    if ( -e "$origFileName") {
	`mv $origFileName $tmpFileName`;
    }
    $origFileName= "/nfs/irdoradus/share/data/environment/F2Log${date}_noepics";
    $tmpFileName = $origFileName;
    while ( -e "$tmpFileName") {
	$tmpFileName = $tmpFileName.".bak";
    }
    if ( -e "$origFileName") {
	`mv $origFileName $tmpFileName`;
    }
    `f2envlogger.csh | tee /share/data/environment/F2Log${date}_noepics > /nfs/irdoradus/share/data/environment/F2Log${date}_noepics &`;
    `rm -f /nfs/irdoradus/share/data/environment/current_noepics`;
    `cd /nfs/irdoradus/share/data/environment; ln -s f2Log${mo}-${day}-${y}_noepics current_noepics`;
} else {
    $origFileName= "/share/data/environment/f2log${date}";
    $tmpFileName = $origFileName;
    #while ( -e "$tmpFileName") {
        #$tmpFileName = $tmpFileName.".bak";
    #}
    #if ( -e "$origFileName") {
	#`mv $origFileName $tmpFileName`;
    #}
    $origFileName= "/nfs/irdoradus/share/data/environment/f2log${date}";
    $tmpFileName = $origFileName;
    #while ( -e "$tmpFileName") {
	#$tmpFileName = $tmpFileName.".bak";
    #}
    #if ( -e "$origFileName") {
	#`mv $origFileName $tmpFileName`;
    #}
    #`uftemperaturelogger.pl -epics $epicsDB | tee -a /share/data/environment/f2log${date} >> /nfs/irdoradus/share/data/environment/f2log${date} &`;
    $user = `whoami`;
    chomp($user);
    if ($user eq "flam") {
	system("uftemperaturelogger.pl -epics $epicsDB -dir1 /usr/tmp -dir2 /share/data/environment -dir3 /nfs/irdoradus/share/data/environment &");
    	#`uftemperaturelogger.pl -epics $epicsDB -dir1 /usr/tmp -dir2 /share/data/environment -dir3 /nfs/irdoradus/share/data/environment &`;
	if (not -e "/nfs/irdoradus/share/data/environment/f2Log${mo}-${day}-${y}") {
	    `rm -f /nfs/irdoradus/share/data/environment/current`;
	    #`cd /nfs/irdoradus/share/data/environment; ln -s f2log$date current`;
	    `cd /nfs/irdoradus/share/data/environment; ln -s f2Log${mo}-${day}-${y} current`;
	}
    }
}


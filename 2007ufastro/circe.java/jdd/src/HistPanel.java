package ufjdd;

// "Java Tech"
//  Code provided with book for educational purposes only.
//  No warranty or guarantee implied.
//  This code freely available. No copyright claimed.
//  2003


import java.awt.*;
import javax.swing.*;

/**
 *  Display the histogram data.
 */
public class HistPanel extends PlotPanel
{
  // Histogram access
  protected NewHistogram histogram;

  protected Color barLineColor    = Color.DARK_GRAY;
  protected Color barFillColor    = Color.PINK;
  int gap = 2;    // 2 pixels between bars

  // Fractional margin between highest bar and top frame
  double topMargin  = 0.05;
  // Fractional margnin between side bars and frame
  double sideMargin = 0.01;

  // Numbers to use for plotting axes scale values
  double [] xScaleValue;
  double [] yScaleValue;
  int numYScaleValues = 2;
  int numXScaleValues = 5;

  /**
   * Create the panel with the histogram.
   */
  public HistPanel(NewHistogram histogram)
  {
    this.histogram = histogram;
    getScaling();
  }

  /**
   * Switch to a new histogram
   */
  public void setHistogram(NewHistogram histogram)
  {
    this.histogram = histogram;
    getScaling();
    repaint();
  }

  /**
   * Get the values for putting scaling numbers on
   * the plot axes.
   */
  void getScaling()
  {

    yScaleValue = new double[numYScaleValues];
    // Use lowest value of 0;
    yScaleValue[0] = 0.0;

    xScaleValue = new double[numXScaleValues];
    // First get the low and high values;
    xScaleValue[0] = histogram.getLo();
    xScaleValue[numXScaleValues-1] = histogram.getHi();

    // Then calculate the difference between the values
    // (assumes linear scale)
    double range = xScaleValue[numXScaleValues-1] -
                        xScaleValue[0];
    double dx = range/(numXScaleValues-1);

    // Now set the intermediate scale values.
    for(int i=1; i < (numXScaleValues-1); i++)
    {
        xScaleValue[i] = i*dx + xScaleValue[0];
    }

  }

  /**
   * Optional bar color settings
   */
  public void setBarColors(Color line, Color fill)
  {
    if( line != null) barLineColor = line;
    if( fill != null) barFillColor = fill;
  }


  /**
   * Overrides the abstract method in PlotPanel superclass.
   * Draw the vertical bars that represent the bin contents.
   * Draw also the numbers for the scales on the axes.
   */
  void paintContents(Graphics g)
  {
    // Get the histogram max value and bin data
    int maxDataValue = histogram.getMax();
    int [] bins = histogram.getBins();

    if(maxDataValue == 0) return;

    Color oldColor=g.getColor();  // remember color for later

    // Dimensions of the drawing area for the bars
    int sideSpace = (int)(frameWidth*sideMargin);
    int drawWidth= frameWidth - 2*sideSpace-(bins.length-1)*gap;

    int drawHeight= (int)(frameHeight*(1.0 - topMargin));

    // To avoid build up of round off errors, the bar
    // positions will be calculated from a FP value.
    float stepWidth=(float)drawWidth/(float)bins.length;
    int barWidth = Math.round(stepWidth);
    stepWidth += gap;

    // Scale the bars to the maximum bin value.
    float scaleFactor=(float)drawHeight/maxDataValue;

    int startX = frameX + sideSpace;
    int startY = frameY+frameHeight;

    for(int i=0; i < bins.length; i++)
    {
        int barHeight = (int)(bins[i]*scaleFactor);

        int barX = (int)(i*stepWidth) + startX;

        // Bar drawn from top left corner
        int barY= startY-barHeight;

        g.setColor(barLineColor);
        g.drawRect(barX,barY, barWidth ,barHeight);

        g.setColor(barFillColor);
        g.fillRect(barX+1,barY+1, barWidth-2, barHeight-1);

    }

    // Find the scale value of the full frame height.
    yScaleValue[1] = (double)(frameHeight/scaleFactor);

    // Draw the numbers along the axes.
    drawAxesNumbers( g, xScaleValue, yScaleValue);

    g.setColor(oldColor); //reset original color
  }

  // Methods overriding those in PlotPanel
  String getTitle()
  {  return histogram.getTitle();}

  String getXLabel()
  {  return histogram.getXLabel();}

}


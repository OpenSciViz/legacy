/*
 * TelescopePanel.java
 *
 * Created on June 17, 2004, 2:33 PM
 */

package ufjdd;

/**
 *
 * @author  ziad
 */
import javaUFLib.*;
import java.awt.*;
import javax.swing.*;
import java.awt.image.*;
import javax.swing.*;
import java.awt.geom.*;

public class StatisticsPanel {
    
    UFPlot ufp;
    /** Creates new form TelescopePanel */
    public StatisticsPanel() {
       ufp = new UFPlot();
    }
    
    /** Exit the Application */
    private void exitForm(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_exitForm
        System.exit(0);
    }//GEN-LAST:event_exitForm
    
    public void update(int[][] data, int x1, int y1, int x2, int y2, int scalefactor) {
        
        System.out.println("x1=" + x1 + " y1= " + y1 + " x2 = " + x2 + " y2 = " + y2);
        int[][] statData = UFArrayOps.divArrays(UFArrayOps.extractValues(data, y1, y2, x1, x2), scalefactor);
        float mean = UFArrayOps.avgValue(statData);
	float stddev = UFArrayOps.stddev(statData);
	float sqnpts = (float)Math.sqrt((x2-x1)*(y2-y1));
	int nbins = 10*(int)(Math.floor(sqnpts/42)+1);
	int[] histY = ufp.hist(statData, "*nbins="+nbins+", *xticks=5, *xtitle=Data, *ytitle=Frequency, *title=Mean: " +mean+"     Std Dev: " + stddev);
	int max = (int)UFArrayOps.maxValue(statData);
	int min = (int)UFArrayOps.minValue(statData);
	float[] x = new float[100];
	for (int j = 0; j < 100; j++) x[j] = min+j*(max-min)/100f;
        float[] z =UFArrayOps.divArrays(UFArrayOps.subArrays(x, mean), stddev);
	float[] y = new float[100];
	int ymax = UFArrayOps.maxValue(histY); 
	for (int j = 0; j < 100; j++)
	   y[j] = (float)(ymax*Math.exp(-0.5*Math.pow(z[j],2)));
	for (int j = 0; j < 100; j++) x[j] = j*nbins/100f;
	ufp.overplot(x, y, ""); 
    }
    
}

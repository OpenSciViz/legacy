package ufjdd;

import java.util.*;
import java.text.*;

/**
    This class provides a static method getFormatted that
    returns a double value as a string in decimal or scientific
    notation.<br><br>
 */


public class PlotFormat
{
  final static DecimalFormat df1 = new DecimalFormat("0.0");
  final static DecimalFormat df2 = new DecimalFormat("0.00");
  final static DecimalFormat df3 = new DecimalFormat("0.000");
  final static DecimalFormat df4 = new DecimalFormat("0.0000");

  final static DecimalFormat sf1 = new DecimalFormat("0.0E0");
  final static DecimalFormat sf2 = new DecimalFormat("0.00E0");
  final static DecimalFormat sf3 = new DecimalFormat("0.000E0");
  final static DecimalFormat sf4 = new DecimalFormat("0.0000E0");

 /**
  * The options include 1 to 3 decimal places. Values below
  * decimalLimit use decimal notation; above this use scientific
  * notation.
  * @param input value
  * @param upper limit before changing to scientific notation
  * @param lower limit before changing to scientific notation
  * @param number of decimal places in the output.
  */
  public static String getFormatted( double val,
                                     double decimalHiLimit,
                                     double decimalLoLimit,
                                     int decimalPlaces)
  {
  // If less than decimalLimit, or equal to zero, use decimal style
    if( val == 0.0 ||
        (Math.abs(val) <= decimalHiLimit &&
        Math.abs(val) > decimalLoLimit) )
    {
     switch (decimalPlaces)
     {
         case 1 : return df1.format(val);
         case 2 : return df2.format(val);
         case 3 : return df3.format(val);
         case 4 : return df4.format(val);
         default: return df1.format(val);
     }

    }else
    {
      // Create the format for Scientific Notation with E
      switch (decimalPlaces)
      {
         case 1 : return sf1.format(val);
         case 2 : return sf2.format(val);
         case 3 : return sf3.format(val);
         case 4 : return sf4.format(val);
         default: return sf1.format(val);
      }
    }

  }
}
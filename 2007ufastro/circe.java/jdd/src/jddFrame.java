package ufjdd;

/**
 * Title:        Java Data Display  (JDD) main Frame.
 * Version:      (see rcsID)
 * Copyright:    Copyright (c) 2004
 * Author:       Frank Varosi, Ziad Saleh
 * Company:      University of Florida
 * Description:  For quick-look image display and analysis of Infrared Camera data stream.
 */

import CCS.*;
import DAF.*;
import javaUFLib.*;
import javaUFProtocol.*;
import TelescopeServer.*;

import java.io.*;
import java.net.*;
import java.util.*;
import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

public class jddFrame extends JFrame {
    
    public static final
	String rcsID = "$Name:  $ $Id: jddFrame.java,v 1.43 2004/10/05 23:08:41 varosi Exp $";

    final int X_SIZE = 1260, Y_SIZE = 950;
    String hostname;
    int port;
    public int globalindex = -1;
    IndexColorModel colorModel;
    int XLOC, YLOC;
    JMenuBar menuBar;
    JMenu menuFile;
    JMenuItem menuFileExit;
    JMenu menuHelp;
    JMenuItem menuHelpAbout;
    ZoomPanel zoomPanel;
    AdjustZoomPanel adjZoomPanel;
    ImagePanel imgPanelArray[];
    DisplayPanel displayPanel;
    UFobsMonitor ufobsMonitor;
    Histogram histogram ;
    Socket socket ;
    FrameBuffer frameBuffer;
    String bufferNames[] ;
    String ZoomFactors[] = { "", "2", "3", "4", "5", "7"};
    String SmoothFactors[] = { "0", "1", "2", "3", "4", "5", "8", "9", "10", "11", "12", "13" };
    
    String[] LnF = {"Motif Look","Metal Look"};
    public JComboBox zoomFactor ;
    public JComboBox smoothFactor ;
    public JLabel overFlowLabel, underFlowLabel, pixelDataLabel;
    JToggleButton histnormButton;
    JButton colorMapButton;
    ColorBar colorBar;
    ColorMapDialog colorMapDialog;

    JButton telescopeOffsetControl;
    TelescopePanel telescopePanel;
    
    public jddFrame(String hostName, int port, String tcshost, String tcsport, String args[]) {
        this.hostname = hostName;
        this.port = port;
        System.out.println("Initializing JDD... ");
        System.out.println("Connecting to the Data Acq. Server @ " + hostName + ":" + port);
        
        try {
            socket = new Socket(hostName, port);
            System.err.println("Socket Connection Established ");
            
        } catch (IOException ioe) {
            System.err.println("Can't connect to the server \n" );
            ioe.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
        telescopePanel = new TelescopePanel(tcshost, tcsport, args);
        
        frameBuffer = new FrameBuffer(socket);
        bufferNames = frameBuffer.readBufferNames();
        System.out.println("Initializing components...");
        
        jddInit();

        System.out.println("Read and Display default frame buffers...");
        imgPanelArray[0].controlPanel.displayBuffer("dif1",false);
        imgPanelArray[1].controlPanel.displayBuffer("dif2",false);
        imgPanelArray[2].controlPanel.displayBuffer("sig",false);
        imgPanelArray[3].controlPanel.displayBuffer("sig",true);
    }
    
    /**
     * Component initialization
     */
    private void jddInit() {
        this.setTitle("JDD:  Java  Data  Display  Tool  (version 2004/09/09)"); // + jddVersion.version + ")");
        menuBar = new JMenuBar();
        menuFile = new JMenu("File");
        menuFileExit = new JMenuItem("Exit");
        menuFile.add(menuFileExit);
        menuHelp = new JMenu("Help");
        menuHelpAbout = new JMenuItem("About");
        menuHelp.add(menuHelpAbout);
        
        menuFileExit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
		System.out.println("JDD exiting...");
                System.exit(0);
            }
        });
        
        JMenu lnfMenu = new JMenu("Look & Feel");
        
        for( int i=0; i < LnF.length; i++ ) {
            JMenuItem menuItem = new JMenuItem(LnF[i]);
            lnfMenu.add(menuItem);
            menuItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) { change_look(e); }
            });
        }
        
        colorMapButton = new JButton("Color Map Control");
        colorMapDialog = new ColorMapDialog(this);
        colorModel  = colorMapDialog.colorModel;
        
        colorMapButton.addActionListener(new ActionListener()  {
            public void actionPerformed(ActionEvent e) {
                colorMapDialog.show();
            }
        });
        
        menuHelpAbout.addActionListener(new ActionListener()  {
            public void actionPerformed(ActionEvent e) {
                helpAbout_action(e);
            }
        });

        menuBar.add(menuFile);
        menuBar.add(lnfMenu);
        menuBar.add(menuHelp);
        this.setJMenuBar( menuBar );
        
        zoomFactor = new JComboBox(ZoomFactors);
        smoothFactor = new JComboBox(SmoothFactors);
        
        String sliderColors[] = {"Red", "Green", "Blue"};
        
        histogram = new Histogram(this);
        
        zoomPanel = new ZoomPanel(this, smoothFactor, colorModel, histogram);
        
        imgPanelArray = new ImagePanel[4];
        for(int i=0; i < imgPanelArray.length; i++)
            imgPanelArray[i] = new ImagePanel(i, this, frameBuffer, bufferNames, zoomFactor, colorModel, zoomPanel);

        JPanel tempPanel = new JPanel();
        tempPanel.setMinimumSize(new Dimension(680, 800));
        tempPanel.setLayout(new GridLayout(2,2));
        
        for(int i=0; i < imgPanelArray.length; i++)
            tempPanel.add(imgPanelArray[i]);
        
        JScrollPane scrollPane = new JScrollPane(tempPanel);

        colorBar = new ColorBar(colorModel,30);
        
        adjZoomPanel = new AdjustZoomPanel(zoomPanel);
        
        underFlowLabel = new JLabel("UnderFlow");
        underFlowLabel.setBorder(BorderFactory.createLoweredBevelBorder());
        overFlowLabel  = new JLabel("OverFlow");
        overFlowLabel.setBorder(BorderFactory.createLoweredBevelBorder());
        pixelDataLabel = new JLabel("Pixel Data");
        pixelDataLabel.setBorder(BorderFactory.createLoweredBevelBorder());
        
        histnormButton = new JToggleButton("Apply Histogram Eq");

        telescopeOffsetControl = new JButton("Telescope  Offset  Control");
        
        telescopeOffsetControl.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                telescopePanel.show();
            }
        });
        
        histnormButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                
                String buttonText = histnormButton.getText();
                if(buttonText.equalsIgnoreCase("Apply Histogram Eq") ){
                    histnormButton.setText("Undo Hist Eq");
                    int [] normHist = new int[256];
                    normHist = histogram.normalize();
                    IndexColorModel normColorModel = colorMapDialog.normColorModel(normHist);
                    zoomPanel.updateColorMap(normColorModel);
                    colorBar.updateColorMap(normColorModel);
                }
                else {
                    histnormButton.setText("Apply Histogram Eq");
                    histogram.applyOldSettings();
                    zoomPanel.updateColorMap(colorModel);
                    colorBar.updateColorMap(colorModel);
                }
            }
        });
        
        displayPanel = new DisplayPanel(this, imgPanelArray, frameBuffer);
        ufobsMonitor = new UFobsMonitor(hostname, port, displayPanel);
        ufobsMonitor.setBounds( 0, 730, X_SIZE-14, Y_SIZE-730-62 );

        scrollPane.setBounds(0, 0, 650, 730);
        JLabel ZFLabel = new JLabel("Choose Zoom Factor");
	ZFLabel.setBounds(690, 50, 150, 40);
        zoomFactor.setBounds(690, 90, 100, 30);
        JLabel SFLabel = new JLabel("Choose Smooth Factor");
	SFLabel.setBounds(860, 50, 150, 40);
        smoothFactor.setBounds(860, 90 , 100, 30);        
        telescopeOffsetControl.setBounds(710, 10, 230, 40);
        histnormButton.setBounds(1020, 10, 200, 30);
        colorMapButton.setBounds(1020, 50, 200, 30);
        zoomPanel.setBounds(650, 130, 420, 420);
        pixelDataLabel.setBounds(650, 550, 420, 20);
        adjZoomPanel.setBounds(650, 570, 420, 100);
        colorBar.setBounds(1080, 130, 30, 512);
        histogram.setBounds(1120, 130, 120, 512);
        underFlowLabel.setBounds(1120, 642, 120, 20);
        overFlowLabel.setBounds(1120, 110, 120, 20);

        getContentPane().setLayout(null);
        getContentPane().add(scrollPane);
        getContentPane().add(ZFLabel);
        getContentPane().add(zoomFactor);
        getContentPane().add(SFLabel);
        getContentPane().add(smoothFactor);
        getContentPane().add(telescopeOffsetControl);
        getContentPane().add(histnormButton);
        getContentPane().add(colorMapButton);
        getContentPane().add(zoomPanel);
        getContentPane().add(pixelDataLabel);
        getContentPane().add(adjZoomPanel);
        getContentPane().add(colorBar);
        getContentPane().add(histogram);
        getContentPane().add(underFlowLabel);
        getContentPane().add(overFlowLabel);
        getContentPane().add(ufobsMonitor);

        this.setLocation(5, 5);
        this.setSize(X_SIZE, Y_SIZE);
        this.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
//-------------------------------------------------------------------------------

    public void helpAbout_action(ActionEvent e) {
        // Do nothing right now
        // To be added later
    }
    
    public void changeColorModel(IndexColorModel colorModel) {
        //System.out.println("Color Model updating...");
        this.colorModel = colorModel;
        for(int i=0; i < imgPanelArray.length; i++) imgPanelArray[i].imgDisplayPanel.updateImage(colorModel);
        zoomPanel.updateColorMap(colorModel);
        colorBar.updateColorMap(colorModel);
    }

    void change_look(ActionEvent e) {
        if (e.getActionCommand()=="Motif Look")
            try {
                UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
                SwingUtilities.updateComponentTreeUI(this);
            }
            catch (Exception exc) {}
        
        if (e.getActionCommand()=="Metal Look")
            try {
                UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
                SwingUtilities.updateComponentTreeUI(this);
            }
            catch (Exception exc) {}
    }
}
//-------------------------------------------------------------------------------

class DisplayPanel extends UFLibPanel {
    
    FrameBuffer frmBuffer;
    ImagePanel[] imgPanel;
    jddFrame frame;
    
    public DisplayPanel(jddFrame frame, ImagePanel imgPanels[], FrameBuffer frmBuffer) {
        
        this.frmBuffer = frmBuffer;
        this.imgPanel = imgPanels;
        this.frame = frame;
    }
    
    public void updateFrames(javaUFProtocol.UFFrameConfig ufFrameConfig) {
        
        String framesUpdated = ufFrameConfig.name();
        String temp[] = framesUpdated.split("\\^");
        
        for(int i=0; i < imgPanel.length; i++) {
            ImagePanel  panel = imgPanel[i];
            String bufName = panel.imgDisplayPanel.frameName;
            
            for(int j=0; j < temp.length; j++) {
                if(bufName.equalsIgnoreCase(temp[j])) {
                    ImageBuffer imgBuffer = frmBuffer.readFrameBuffer(bufName);
                    
                    if (imgBuffer == null )
                        System.out.println("There is no image at the buffer Reg " + bufName  );
                    else {
                        panel.imgDisplayPanel.createImage(imgBuffer);
                        panel.adjPanel.updateMinMaxVal(imgBuffer.min, imgBuffer.max, imgBuffer.scaleFactor);  
                        panel.adjPanel.reset();
                        frame.adjZoomPanel.reset();
                        frame.colorBar.updateColorMap(frame.colorModel);
                        ButtonModel model = frame.histnormButton.getModel();
                        model.setSelected(false);
                        frame.histnormButton.setText("Apply Histogram Eq");
                    }
                }
            }
        }        
    }
}



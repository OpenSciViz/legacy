/*
 * ZoomPanel.java
 *
 * Created on March 23, 2004, 4:35 PM
 */

package ufjdd;

/**
 * @author  ziad & Frank
 */

import java.awt.*;
import javax.swing.*;
import java.awt.image.*;
import java.awt.geom.*;
import java.awt.event.*;
import javaUFLib.*;

public class ZoomPanel extends javax.swing.JPanel {
    
    IndexColorModel model;
    int pixel[];
    int pixels[];
    int width = 320;
    int height = 240;
    final int dimension = 420;
    boolean erase = false;
    Image img = null;
    String coordinates = "";
    int XLOC, YLOC;
    int range;
    int start_x, start_y;
    int new_x, new_y;
    Histogram histogram;
    int initialData[][];
    int origData[][], smoothData[][];
    int selectedData[][], selSmoothData[][];
    int zoomedData[][];
    int min, max;
    int newMin, newMax;
    int smoothFactor = 0;
    int startx, starty, scalefactor, zoomfactor;
    jddFrame frm;
    final JPopupMenu menu ;
    // Create and add menu items
    JMenuItem item1, item2, item3, item4, item5 ;
    StatisticsPanel statPanel;
    LineCutPanel linecutpanel;
    String mode = "";
    JComboBox jComboBox;
    /** Creates a new instance of ZoomPanel */
    public ZoomPanel(jddFrame jddfrm, JComboBox comb, IndexColorModel model, Histogram hgrm) {
        
        this.frm = jddfrm;
        this.model = model;
        this.histogram = hgrm;
        this.jComboBox = comb;
        this.setPreferredSize(new Dimension(dimension, dimension));
        this.setBorder(BorderFactory.createLineBorder(Color.black, 1));
        menu = new JPopupMenu();
        item1 = new JMenuItem("Statisctics");
        item2 = new JMenuItem("Line Cut");
        item3 = new JMenuItem("Clear");
        
        menu.add(item1);
        menu.add(item2);
        menu.add(item3);
        
        MyListener myListener = new MyListener();
        jComboBox.addActionListener(myListener);
        
        statPanel = new StatisticsPanel();
        linecutpanel = new LineCutPanel();
        
        
        item1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                mode = "stat";
                erase = false;
                //System.out.println( mode + " Event source " + evt.getActionCommand());
                
            }
        });
        
        item2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                mode = "lncut";
                erase = false;
                //System.out.println( mode +" Event source " + evt.getActionCommand());
                
            }
        });
        
        
        item3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                mode = "clear";
                erase = true;
                repaint();
                //System.out.println( mode + " Event source " + evt.getActionCommand());
                
            }
        });
        
        addMouseMotionListener(new java.awt.event.MouseMotionListener() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                if( zoomfactor == 0) {
                }
                else {
                    XLOC = startx + evt.getX()/zoomfactor;
                    YLOC = 239 - (int)Math.round(starty + evt.getY()/zoomfactor);
                    int XPOS = evt.getX();
                    int YPOS = evt.getY();
                    frm.pixelDataLabel.setText("X : " + XLOC +  "  Y : " + YLOC +  "  Data Val :" + zoomedData[YPOS][XPOS]/scalefactor );
                }
            }
            
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                new_x = evt.getX();
                new_y = evt.getY();
                repaint();
            }
            
            
        });
        
        
        addMouseListener(new java.awt.event.MouseListener() {
            
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                
            };
            
            public void mousePressed(java.awt.event.MouseEvent evt) {
                
                if(( evt.getModifiers() & InputEvent.BUTTON1_MASK) != 0) {
                    new_x = start_x = evt.getX();
                    new_y = start_y = evt.getY();
                }
                
                if(( evt.getModifiers() & InputEvent.BUTTON2_MASK) != 0) {
                    
                }
                
                if(( evt.getModifiers() & InputEvent.BUTTON3_MASK) != 0) {
                    if(evt.isPopupTrigger()) {
                        menu.show(evt.getComponent(), evt.getX(), evt.getY());
                    }
                }
                
            };
            
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                if(( evt.getModifiers() & InputEvent.BUTTON1_MASK) != 0) {
                    
                    new_x = evt.getX();
                    new_y = evt.getY();
                    if(mode.equalsIgnoreCase("stat")) {
                        // Pass the parameters  to plot the graph
                        int x1 = startx + (int)Math.floor(start_x/zoomfactor);
                        int y1 = starty + (int)Math.floor(start_y/zoomfactor); 
                        int x2 = startx + (int)Math.floor(new_x/zoomfactor);
                        int y2 = starty + (int)Math.floor(new_y/zoomfactor);
                        System.out.println("(x1, y1) => " + x1 + "," + y1 + "(x2, y2) => " + x2 + "," + y2);
			if (smoothFactor == 0) {
                           statPanel.update(origData, x1, y1, x2, y2, scalefactor);
			} else {
			   statPanel.update(smoothData, x1, y1, x2, y2, scalefactor);
			}
                        repaint();
                    }
                    else if(mode.equalsIgnoreCase("lncut")) {
                       // Pass the parameters  to plot the graph
                       int x1 = startx + (int)Math.floor(start_x/zoomfactor);
                       int y1 = starty + (int)Math.floor(start_y/zoomfactor);
                       int x2 = startx + (int)Math.floor(new_x/zoomfactor);
                       int y2 = starty + (int)Math.floor(new_y/zoomfactor);
                       System.out.println("(x1, y1) => " + x1 + "," + y1 + "(x2, y2) => " + x2 + "," + y2);
                       if (smoothFactor == 0) {
			   linecutpanel.update( origData, x1, y1, x2, y2,scalefactor); 
		       } else {
			   linecutpanel.update(smoothData, x1, y1, x2, y2, scalefactor);
		       }
                       repaint();
                    }
                    else
                        erase = true;
                    
                    
                   
                    
                }
                
                if(( evt.getModifiers() & InputEvent.BUTTON2_MASK) != 0) {
                    
                }
                
                if(( evt.getModifiers() & InputEvent.BUTTON3_MASK) != 0) {
                    if(evt.isPopupTrigger()) {
                        menu.show(evt.getComponent(), evt.getX(), evt.getY());
                    }
                }
                
            };
            
            public void mouseEntered(java.awt.event.MouseEvent evt) {
            };
            
            public void mouseExited(java.awt.event.MouseEvent evt) {
            };
            
        });
        
        
        pixel = new int[dimension*dimension];
        pixels = new int[dimension*dimension];
        zoomedData   = new int[dimension][dimension];
        selectedData = new int[height][width];
        
        
    }
    
    public void updateImage(int origData[][], IndexColorModel model, int startX, int startY, int range, int scaleFactor, int zoomFactor)  {
        
        //this.initialData = data;
        this.range = range;
        this.startx = startX;
        this.starty = startY;
        this.scalefactor = scaleFactor;
        this.zoomfactor = zoomFactor;
        this.origData = origData;
        initialData = new int[range][range];
	selSmoothData = new int[range][range];

        // copy the selected area
        for( int i=0; i < range; i++)
            for (int j=0; j < range; j++) {
                initialData[i][j] = origData[i + starty][j + startx];
            }
        
	if (scaleFactor == 0) {
           minmax(initialData);
        
           scaleArray(min, max);
           frm.adjZoomPanel.minValLabel.setText("" + min/scaleFactor );
           frm.adjZoomPanel.maxValLabel.setText("" + max/scaleFactor );
           histogram.updateHisto(initialData, min, max);
           img = createImage(new MemoryImageSource(dimension, dimension, model, pixel, 0, dimension));
	} else {
           int[][] w = UFArrayOps.where(selSmoothData, "!=", 0);
           if (w[0][0] == -1 && w[0][1] == -1) {
               smooth(origData, 3, smoothFactor);
	   }
           minmax(selSmoothData);
           scaleArray(min, max);
           frm.adjZoomPanel.minValLabel.setText("" + min/scalefactor );
           frm.adjZoomPanel.maxValLabel.setText("" + max/scalefactor );
           histogram.updateHisto(selSmoothData, min, max);
           img = createImage(new MemoryImageSource(dimension, dimension, model, pixel, 0, dimension));
	}

        repaint();
        
    }
    
    public void updateColorMap(IndexColorModel model) {
        
        this.model = model;
        img = createImage(new MemoryImageSource(dimension, dimension, model, pixel, 0, dimension));
        
        repaint();
    }
    
    public void paintComponent( Graphics g) {
        super.paintComponent(g);
        
        Graphics2D g2d = (Graphics2D)g;
        
        if (img == null)
            g.drawString("Image not Available", 200, 200);
        else {
            if(erase) {
		g.drawImage(img, 0, 0, this);
            } else if(mode.equalsIgnoreCase("stat")) {
                g.drawImage(img, 0, 0, this);
                // Paint a rectangle with a translucent color
                g.setColor(new Color(128, 255, 128, 56));
                g.fillRect(Math.min(start_x, new_x), Math.min(start_y, new_y), Math.abs(new_x - start_x), Math.abs(new_y - start_y));
                
                // Paint a solid black rectangular outline
                g.setColor(Color.WHITE);
                g.drawRect(Math.min(start_x, new_x), Math.min(start_y, new_y), Math.abs(new_x - start_x), Math.abs(new_y - start_y));
            } else if(mode.equalsIgnoreCase("lncut"))  {
                if(smoothFactor == 0) {
                    g.drawImage(img, 0, 0, this);
                    g.setColor(Color.GREEN);
                    g.drawLine(start_x, start_y, new_x, new_y);
                } else {
                    
		    histogram.updateHisto(selSmoothData, min, max);
		    g.drawImage(img, 0, 0, this);
                    g.setColor(Color.GREEN);
                    g.drawLine(start_x, start_y, new_x, new_y);
                }
                //g.drawImage(img, 0, 0, this);
                
            } else {
		g.drawImage(img, 0, 0, this);
            }
            
        }
    }
    
    
    
    private void minmax( int array[][]) {
        min = array[0][0];
        max = array[0][0];
        
        
        // scale the array by a zoomFactor
        for(int i=0; i < array.length; i++)
            for (int j=0; j < array.length; j++)
                for(int k=0; k < zoomfactor; k++)
                    for( int s=0; s < zoomfactor; s++) {
                        zoomedData[zoomfactor*i + k][zoomfactor*j + s] = array[i][j];
                    }
        
        //Convert back from two dimensional into one dimensional array
        for ( int index=0, i=0; i < zoomedData.length; i++)
            for (int j=0; j < zoomedData.length; j++) {
                pixels[index++] = zoomedData[i][j];
                if( zoomedData[i][j] < min )
                    min = zoomedData[i][j];
                if( zoomedData[i][j] > max )
                    max = zoomedData[i][j];
            }
        
        //System.out.println("Min > " + min + " Max > " + max);
    }
    
    public void drawOriginalImage() {
        
        scaleArray(min, max);
        histogram.updateHisto(initialData, min, max );
        img = createImage(new MemoryImageSource(dimension, dimension, model, pixel, 0, dimension));
        repaint();
    }
    
    public void applyNewMinMax(int new_min, int new_max) {
        scaleArray(new_min*scalefactor, new_max*scalefactor);
        histogram.updateHisto(initialData, new_min*scalefactor, new_max*scalefactor );
        img = createImage(new MemoryImageSource(dimension, dimension, model, pixel, 0, dimension));
        repaint();
    }
    
    public void scaleArray(int minv, int maxv ) {
        float range;
        float f;
        range = maxv - minv;
        
        for(int i=0; i < pixels.length; i++) {
            f = (float)(pixels[i] - minv)/(float) range;
            if(f<0) {
                pixel[i] = 0;
            }
            else if(f>1) {
                pixel[i] = 255;
            }
            else
                pixel[i] = Math.round(f*255);
        }
        
    }
    
    public void applyLogScale(float threshold) {
        
        double scaleFactor = Math.log(10);
        
        double temp_double[] = new double[dimension*dimension];
        
        int temp[] = new int[dimension*dimension];
        
        for ( int i=0; i < temp.length; i++) {
            if(pixels[i] < threshold)
                temp_double[i] = Math.log(threshold);
            else
                temp_double[i] = Math.log((double)pixels[i])/scaleFactor;
        }
        
        double mn = Math.log(threshold)/scaleFactor;
        double mx = Math.log((double)max)/scaleFactor;
        
        double range = mx - mn;
        
        System.out.println(" mn " + mn + " mx " + mx + " range " + range);
        
        for(int i=0; i < temp.length; i++) {
            double f = (temp_double[i] - mn)/range;
            if(f<0) {
                temp[i] = 0;
            }
            else if(f>1) {
                temp[i] =255;
            }
            else
                temp[i] = Math.round((float)f*255);
        }
        
        img = createImage(new MemoryImageSource(dimension, dimension, model, temp, 0, dimension));
        repaint();
    }

   private void smooth(int[][] data, int width, int niter) {
      smoothData = UFImageOps.smooth(data, width, niter);
      for( int i=0; i < range; i++) {
	for (int j=0; j < range; j++) {
	   selSmoothData[i][j] = smoothData[i + starty][j + startx];
	}
      }
   }
    
/*
    private void blur(int data[][], int n_times) {
        
        //int range = data.length;
        int blured[][] = new int[height][width];
        int tempArray[][] = new int[height][width];
        int temp;
        
        for(int index=0; index < n_times; index++) {
            for(int i=0; i < height; i++)
                for(int j=0; j < width; j++)
                    if((i==0) && (j==0)) {
                        temp = data[i][j] + data[i][j+1] + data[i+1][j] + data[i+1][j+1];
                        blured[i][j] = (int) (temp/ 4.0f);
                    } else if( (i==0) && (j==(width-1))) {
                        temp = data[i][j] + data[i][j-1] + data[i+1][j] + data[i+1][j-1];
                        blured[i][j] = (int) (temp/ 4.0f);
                    } else if((i==(height-1) && (j==0))) {
                        temp = data[i][j] + data[i][j+1] + data[i-1][j] + data[i-1][j+1];
                        blured[i][j] = (int) (temp/ 4.0f);
                    } else if((i==(height-1)) && (j==(width-1))) {
                        temp = data[i][j] + data[i][j-1] + data[i-1][j] + data[i-1][j-1];
                        blured[i][j] = (int) (temp/ 4.0f);
                    } else if((i==0) && ((j!=0) || (j!=(width-1)))) {
                        temp = data[i][j-1] + data[i][j] + data[i][j+1]
                        + data[i+1][j-1] + data[i+1][j] + data[i+1][j+1];
                        blured[i][j] = (int) (temp/ 6.0f);
                    } else if((i==(height-1)) && ((j!=0) || (j!=(width-1)))) {
                        temp = data[i-1][j-1] + data[i-1][j] + data[i-1][j+1]
                        + data[i][j-1] + data[i][j] + data[i][j+1];
                        blured[i][j] = (int) (temp/ 6.0f);
                    } else if((j==0) && ((i!=0) || (i!=(height-1)))) {
                        temp = data[i-1][j] + data[i-1][j+1]
                        + data[i][j] + data[i][j+1]
                        + data[i+1][j] + data[i+1][j+1];
                        blured[i][j] = (int) (temp/ 6.0f);
                    } else if((j==(width-1)) && ((i!=0) || (i!=(height-1)))) {
                        temp = data[i-1][j-1] + data[i-1][j]
                        + data[i][j-1] + data[i][j]
                        + data[i+1][j-1] + data[i+1][j];
                        blured[i][j] = (int) (temp/ 6.0f);
                    } else {
                        temp = data[i-1][j-1] + data[i-1][j] + data[i-1][j+1]
                        + data[i][j-1] + data[i][j] + data[i][j+1]
                        + data[i+1][j-1] + data[i+1][j] + data[i+1][j+1];
                        blured[i][j] = (int) (temp/ 9.0f);
                    }
            
            data = blured;
        }
        
        bluredData = blured;
        // copy the selected area
        for( int i=0; i < range; i++)
            for (int j=0; j < range; j++) {
                selBluredData[i][j] = bluredData[i + starty][j + startx];
            }
        //repaint();
    }
*/
    
    class MyListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String comboitem = (String) jComboBox.getSelectedItem();
            if(comboitem.length() == 0 ) {
                smoothFactor = 0;
            } else {
                smoothFactor =  Integer.parseInt(comboitem);
                //mode = "smooth";
                erase = false;
		smooth(origData, 3, smoothFactor);
                minmax(selSmoothData);
                scaleArray(min, max);
                frm.adjZoomPanel.minValLabel.setText("" + min/scalefactor );
                frm.adjZoomPanel.maxValLabel.setText("" + max/scalefactor );
                histogram.updateHisto(selSmoothData, min, max);
                img = createImage(new MemoryImageSource(dimension, dimension, model, pixel, 0, dimension));
	    }
            repaint();
        }
    }
}

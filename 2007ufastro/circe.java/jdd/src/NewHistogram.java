// "Java Tech"
//  Code provided with book for educational purposes only.
//  No warranty or guarantee implied.
//  This code freely available. No copyright claimed.
//  2003
//
/**
 * This class provides the bare essentials for a histogram.
 */
package ufjdd;

class NewHistogram
{
  protected String title = "Histogram";
  protected String xLabel = "Data";

  protected int [] bins;
  protected int numBins;
  protected int underflows;
  protected int overflows;

  protected double lo;
  protected double hi;
  protected double range;

  // The constructor will create an array of a given
  // number of bins. The range of the histogram given
  // by the upper and lower limit values.
  public NewHistogram(int numBins, double lo, double hi)
  {
    // Check for bad range values.
    // Could throw an exception but will just
    // use default values;
    if( hi < lo)
    {   lo = 0.0;
        hi = 1.0;
    }
    if( numBins <= 0) numBins = 1;

    this.numBins = numBins;

    bins = new int[numBins];

    this.lo = lo;
    this.hi = hi;
    range = hi - lo;
  }

  // This constructor includes the title and horizontal
  // axis label.
  public NewHistogram(String title, String xLabel, int numBins, double lo, double hi)
  {
    this(numBins, lo, hi);// Invoke overloaded constructor
    this.title = title;
    this.xLabel = xLabel;
  }

//--- Histogram description --------------------------------
  /**
   * Get to title string.
   */
  public String getTitle()
  { return title; }

  /**
   * Set the title.
   */
  public void setTitle(String title)
  { this.title = title; }

  /**
   * Get to the horizontal axis label
   */
  public String getXLabel()
  { return xLabel; }

  /**
   * Set the horizontal axis label.
   */
  public void setXLabel(String xLabel)
  { this.xLabel = xLabel; }

//--- Bin info access --------------------------------------
   /**
    * Get the low end of the range.
    */
  public double getLo()
  { return lo; }

  /**
   * Get the high end of the range.
   */
  public double getHi()
  { return hi; }

  /**
   * Get the number of entries in the largest bin.
   */
  public int getMax()
  {
    int max = 0;

    for( int i=0; i < numBins;i++)
         if( max < bins[i]) max = bins[i];

    return max;
  }

  /**
   * This method returns a reference to the bins.
   * Note that this means the values of the histogram
   * could be altered by the caller object.
   */
  public int [] getBins()
  {
    return bins;
  }

  /**
   * Get the number of entries in the smallest bin.
   */
  public int getMin()
  {
    int min = getMax();

    for(int i=0; i < numBins;i++)
        if( min > bins[i]) min = bins[i];

    return min;
  }

  /**
   * Get the total number of entries not counting
   * overflows and underflows.
   */
  public int getTotal()
  {
      int total = 0;
      for( int i=0; i < numBins;i++)
           total += bins[i];

      return total;
    }

  /**
   * Add an entry to a bin.
   * @param x double value added if it is in the range:<br>
   *   lo <= x < hi
   */
  public void add(double x)
  {
    if( x >= hi) overflows++;
    else
    if( x < lo) underflows++;
    else
    {
      double val = x - lo;

      // Casting to int will round off to lower
      // integer value.
      int bin = (int)(numBins * (val/range) );

      // Increment the corresponding bin.
      bins[bin]++;
    }
  }

  /**
   * Clear the histogram bins and the over and under flows.
   */
  public void clear()
  {
    for( int i=0; i < numBins; i++)
    {
      bins[i] = 0;
      overflows = 0;
      underflows= 0;
    }
  }

  /**
   * Provide access to the bin values.
   * Return the underflows if bin value negative,
   * Return the overflows if bin value more than
   * the number of bins.
   */
  public int getValue(int bin)
  {
    if( bin < 0)
        return underflows;
    else
    if( bin >= numBins)
        return overflows;
    else
        return bins[bin];
  }


  /**
   * Get the average and standard deviation of the
   * distribution of entries.
   * @return double array
   */
  public double [] getStats()
  {
    int total = 0;

    double wtTotal = 0;
    double wtTotal2 = 0;
    double [] stat = new double[2];
    double binWidth = range/numBins;

    for( int i=0; i < numBins;i++)
    {
      total += bins[i];

      double binMid = (i - 0.5) * binWidth + lo;
      wtTotal  += bins[i]*binMid;
      wtTotal2 += bins[i]*binMid*binMid;
    }

    if( total > 0)
    {
      stat[0] = wtTotal/total;
      double av2 = wtTotal2/total;
      stat[1] = Math.sqrt(av2 - stat[0]*stat[0]);
    }else
    {
      stat[0] = 0.0;
      stat[1] = -1.0;
    }

    return stat;
  }

 /**
  * Create the histogram from a user derived array along with the
  * under and overflow values.
  * The low and high range values that the histogram
  * corresponds to must be in passed as well.
  *
  * @param userBins array of int values.
  * @param under number of underflows.
  * @param over number of overflows.
  * @param lo value of the lower range limit.
  * @param hi value of the upper range limit.
  */
  public void pack(int [] userBins,
                   int under, int over,
                   double lo, double hi)
  {
    numBins = userBins.length;
    bins = new int[numBins];
    for( int i = 0; i < numBins; i++)
    {
      bins[i] = userBins[i];
    }

    this.lo = lo;
    this.hi = hi;
    range = hi-lo;
    underflows = under;
    overflows = over;
  }
}
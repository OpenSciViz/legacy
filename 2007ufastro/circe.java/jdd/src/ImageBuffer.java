package ufjdd;

import javaUFProtocol.*;

public class ImageBuffer {
    
    UFFrameConfig frameConfig;
    UFInts frameInts ;
    public int width;
    public int height;
    public int min;
    public int max;
    public int scaleFactor;
    public int new_min=0;
    public int new_max=0;
    public int pixels[];

    public ImageBuffer( UFFrameConfig frameConfig, UFInts frameInts )
    {
	frameInts.flipFrame( frameConfig );
        this.frameConfig = frameConfig;
        this.frameInts = frameInts;
        this.width = frameConfig.width;
        this.height = frameConfig.height;
        this.min = frameInts.minVal();
        this.max = frameInts.maxVal();
        this.scaleFactor = frameConfig.chopCoadds*frameConfig.frameCoadds;
        new_min = min;
        new_max = max;
        pixels = frameInts.values();
    }
}




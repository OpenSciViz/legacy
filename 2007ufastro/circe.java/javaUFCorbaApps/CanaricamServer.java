import CCS.*;
import DAF.*;
import TelescopeServer.*;

import java.io.*;
import java.net.*;
import javaUFProtocol.*;
import org.omg.CosNaming.*;
import org.omg.CosNaming.NamingContextPackage.*;
import org.omg.CORBA.*;
import org.omg.PortableServer.*;

public class CanaricamServer {

    public static void main(String [] args) {
	new CanaricamServer(args);
    }

    public CanaricamServer(String [] args) {
	try {
	    // Initialize the ORB.
	    String host = "kepler";
	    int port = 12344;
	    String DCAgentHost = "";
	    int DCAgentPort = -1;
	    String DASHost = "";
	    int DASPort = -1;
	    int listenPort = 53000;
	    if (args.length == 0) {
		args = new String[2];
		args[0] = "-ORBInitRef";
		args[1] = "NameService=corbaloc:iiop:"+host+":"+port+
		    "/NameService";
	    } else {
		if (CMD.findArg(args,"-host")) {
		    host = CMD.getArg(args,"-host");
		    args = CMD.stripArg(args,"-host",true);
		}
		if (CMD.findArg(args,"-port")) {
		    port = Integer.parseInt(CMD.getArg(args,"-port"));
		    args = CMD.stripArg(args,"-port",true);
		}
		if (CMD.findArg(args,"-ORBInitRef","NameService")) {
		    args = CMD.stripArg(args,"-ORBInitRef","NameService");
		}
		args = CMD.addArg(args,"-ORBInitRef","NameService=corbaloc:"+
				  "iiop:"+host+":"+port+"/NameService");
		if (CMD.findArg(args,"-DChost")) {
		    DCAgentHost = CMD.getArg(args,"-DChost");
		    args = CMD.stripArg(args,"-DChost",true);
		}
		if (CMD.findArg(args,"-DCport")) {
		    DCAgentPort = Integer.parseInt(CMD.getArg(args,"-DCport"));
		    args = CMD.stripArg(args,"-DCport",true);
		}
		if (CMD.findArg(args,"-DAShost")) {
		    DASHost = CMD.getArg(args,"-DAShost");
		    args = CMD.stripArg(args,"-DAShost",true);
		}
		if (CMD.findArg(args,"-DASport")) {
		    DASPort = Integer.parseInt(CMD.getArg(args,"-DASport"));
		    args = CMD.stripArg(args,"-DASport",true);
		}
		if (CMD.findArg(args,"-listenport")) {
		    listenPort = Integer.parseInt(CMD.getArg(args,"-listenport"));
		    args = CMD.stripArg(args,"-listenport",true);
		}
	    }
	    org.omg.CORBA.ORB orb = org.omg.CORBA.ORB.init(args,null);
	    
	    // get reference to rootpoa & activate the POAManager
	    POA rootpoa = 
		POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
	    rootpoa.the_POAManager().activate();
	    
	    org.omg.CORBA.Object objRef,ref;
	    NamingContextExt ncRef;
	    NameComponent path[];

	    // Create the CountImpl object
	    CanaricamImpl theCanaricam = new CanaricamImpl();
	    if (DCAgentHost != "") theCanaricam.setDCAgentHost(DCAgentHost);
	    if (DCAgentPort != -1) theCanaricam.setDCAgentPort(DCAgentPort);
	    if (DASHost != "") theCanaricam.setDASHost(DASHost);
	    if (DASPort != -1) theCanaricam.setDASPort(DASPort);
	    // get object reference from the servant
	    ref = rootpoa.servant_to_reference(theCanaricam);
	    Canaricam cref = CanaricamHelper.narrow(ref);

	    // get the root naming context
	    // NameService invokes the name service
	    objRef = orb.resolve_initial_references("NameService");
	    // User NamingContextExt which is part of the 
	    // Interoperable Naming Service (INS) specification
	    ncRef = NamingContextExtHelper.narrow(objRef);

	    // bind the Object Refernce in Naming
	    String name = "Canaricam.CCS";
       
	    path = ncRef.to_name(name);
	    ncRef.rebind(path,cref);


	    (new ListenThread(listenPort,args)).start();
	    System.out.println("Canaricam server ready and waiting ... ");

	    // wait for invocations from clients
	    orb.run();
	} catch (Exception e) {
	    System.out.println(e.toString());
	    e.printStackTrace(System.out);
	}
    }
    private class ListenThread extends Thread {
	int lp;
	String [] argz;
	public ListenThread(int listenPort, String [] args) {
	    lp = listenPort;
	    argz = args;
	}
	public void run() {
	    try {
		ServerSocket ss = new ServerSocket(lp);
		while (true) {	  
		    Socket s = ss.accept();
		    //String [] argz = new String[2];
		    //argz[0] = "-ORBInitRef";
		    //argz[1] = "NameService=corbaloc:iiop:"+host+":"+port+
		    //    "/NameService";
		    System.out.println("CanaricamServer::ListenThread::run> " +
				       "Got new connection");
		    AThread at = new AThread(s,TelescopeClient.getTelescopeServerReference(argz));
		    at.start();
		}
	    } catch (Exception e) {
		System.err.println("CanaricamServer::ListenThread::run> "+e.toString());
		return;
	    }
	    
	}
    }
    private class AThread extends Thread {
	Socket so;
	Telescope_ifce tele;

	public AThread(Socket s,Telescope_ifce telifce) { this.so = s; tele = telifce;}
	
	public void run() {
	    while (true) {
		UFProtocol ufp = UFProtocol.createFrom(so);
		if (ufp == null) return;
		String name;
		if (ufp instanceof UFTimeStamp) {
		    UFTimeStamp ufs = (UFTimeStamp)ufp;
		    name = ufs.name();
		} else if (ufp instanceof UFStrings) {
		    UFStrings ufs = (UFStrings)ufp;
		    name = ufs.name();
		} else {
		    System.err.println("CanaricamServer::AThread::run> Unexpexted UFProtocol object.");
		    String [] strs = new String[1];
		    strs[0] = "Unexpected UFProtocol object.  Not a UFTimeStamp "+
			" or a UFStrings object";
		    UFStrings ufss = new UFStrings("ERROR",strs);
		    ufss.sendTo(so);
		    return;
		}
		try {
		    System.out.println(name);
		    if (name.equals("getChopStabilizeTime")) {
			float f = tele.getChopStabilizeTime();
			String [] strs = new String[1];
			strs[0] = f+"";
			UFStrings ufss = new UFStrings("OK",strs);
			ufss.sendTo(so);
		    } else if (name.equals("getChopDutyCycle")) {
			float f = tele.getChopDutyCycle();
			String [] strs = new String[1];
			strs[0] = f+"";
			UFStrings ufss = new UFStrings("OK",strs);
			ufss.sendTo(so);
		    } else if (name.equals("setChopperParameters")) {
			
		    } else if (name.equals("setChopperHardwareMode")) {
			tele.setChopperHardwareMode();
			UFStrings ufss = new UFStrings("OK");
			ufss.sendTo(so);
		    } else if (name.equals("setChopperPosition")) {
			
		    } else if (name.equals("setChopperHome")) {
			tele.setChopperHome();
			UFStrings ufss = new UFStrings("OK");
			ufss.sendTo(so);
		    } else if (name.equals("requestBeamSwitch")) {
			tele.requestBeamSwitch();
			String [] strs = new String[1];
			strs[0] = "dummy";
			UFStrings ufss = new UFStrings("OK",strs);
			ufss.sendTo(so);
		    } else if (name.equals("requestTelescopeOffset")) {
			
		    } else if (name.equals("getCurrentBeam")) {
			String [] strs = new String[1];
			strs[0] = tele.getCurrentBeam();
			UFStrings ufss = new UFStrings("OK",strs);
			ufss.sendTo(so);
		    } else if (name.equals("getCurrentRotatorAngle")) {
			float f = tele.getCurrentRotatorAngle();
			String [] strs = new String[1];
			strs[0] = f+"";
			UFStrings ufss = new UFStrings("OK",strs);
			ufss.sendTo(so);
		    } else if (name.equals("getM1Temperature")) {
			String [] strs = new String[1];
			strs[0] = tele.getM1Temperature()+"";
			UFStrings ufss = new UFStrings("OK",strs);
			ufss.sendTo(so);
		    } else if (name.equals("getOverallEmissivity")) {
			String [] strs = new String[1];
			strs[0] = tele.getOverallEmissivity()+"";
			UFStrings ufss = new UFStrings("OK",strs);
			ufss.sendTo(so);
		    } else if (name.equals("getHumidity")) {
			String [] strs = new String[1];
			strs[0] = tele.getHumidity()+"";
			UFStrings ufss = new UFStrings("OK",strs);
			ufss.sendTo(so);
		    } else if (name.equals("getAirMass")) {
			String [] strs = new String[1];
			strs[0] = tele.getAirMass()+"";
			UFStrings ufss = new UFStrings("OK",strs);
			ufss.sendTo(so);
		    } else if (name.equals("getFITSHeader")) {
			String [] strs = new String[1];
			strs[0] = tele.getFITSHeader();
			UFStrings ufss = new UFStrings("OK",strs);
			ufss.sendTo(so);
		    } else if (name.equals("completedObservation")) {
			
		    } else if (name.equals("sendAlarm")) {
			
		    } else if (name.equals("sendLog")) {
			
		    } else if (name.equals("getTime")) {
			
		    } else {
			String [] strs = new String[1];
			strs[0] = "Unknown command: "+name;
			UFStrings ufss = new UFStrings("ERROR",strs);
			ufss.sendTo(so);
		    }
		} catch (GCSException gcse) {
		    String [] strs = new String[1];
		    strs[0] = gcse.toString();
		    UFStrings ufss = new UFStrings("ERROR",strs);
		    ufss.sendTo(so);
		}
	    }
	}

    }
   
}

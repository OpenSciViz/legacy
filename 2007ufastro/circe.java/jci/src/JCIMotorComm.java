//Title:        JCIMotorComm class for Java Control Interface (JCI) using UFLib Protocol communications.
//Version:      (see rcsID)
//Copyright:    Copyright (c) 2003
//Author:       David Rashkin and Frank Varosi
//Company:      University of Florida
//Description:  for control and monitor of motors in CanariCam infrared camera system.

package ufjci;
import java.net.*;
import java.io.*;
import java.util.*;
import javaUFProtocol.*;
                                                                               
/** 
 * Motor communication class  -- contains parameters and methods 
 * associated with communication with the motor agent
 */ 
public class JCIMotorComm extends JCIMotorParameters {
    public static final
	String rcsID = "$Name:  $ $Id: JCIMotorComm.java,v 1.1 2004/10/07 21:18:18 amarin Exp $";

    public boolean verbose = false;
    protected Socket comSoc;
    protected Socket statusSoc;
    protected String agentHost;
    protected String LocalHost;
    protected int agentPort;
    protected int socTimeout;
    private static final int DEFAULT_TIMEOUT = 7000;
    private static final int CONNECT_TIMEOUT = 1200;
    private static final int HANDSHAKE_TIMEOUT = 2300;

    public JCIMotorComm(String host, int port) {
	setHostAndPort(host,port);
	//set timeouts to default milliseconds
	socTimeout = DEFAULT_TIMEOUT;
    }

    public JCIMotorComm(String name, char Indexor, String host, int port) {
	super(name,Indexor);
	setHostAndPort(host,port);
	//set timeouts to default milliseconds
	socTimeout = DEFAULT_TIMEOUT;
    }

    public JCIMotorComm(String name, char Indexor, String host, int port, 
			int is, int ts, int acc, int dec, int dc, int hc, int ds,
			int fds, int ddir, int bl, int [] positions, 
			String[] positionNames) {
	super(is,ts,acc,dec,dc,hc,ds,fds,ddir,bl,name,Indexor,positions,positionNames);
	setHostAndPort(host,port);
	//set timeouts to default milliseconds
	socTimeout = DEFAULT_TIMEOUT;
    }

    //////////////////////////////////
    //Connection methods for sockets//
    //////////////////////////////////

    public boolean createStatusSocket() {
	// try to close connection if one exists
	if (statusSoc != null) {
	    try {
		statusSoc.close();
	    } catch (IOException ioe) {
		System.err.println("JCIMotorComm-"+Indexor+".createStatusSocket> "+ioe.toString());
	    }
	    try { Thread.sleep(100);} catch ( Exception _e) {}
	}
	try {
	    InetSocketAddress agentIPsoca = new InetSocketAddress( agentHost, agentPort );
	    statusSoc = new Socket();
	    statusSoc.connect( agentIPsoca, CONNECT_TIMEOUT );
      	    //must send agent a timestamp with the client name in it
	    statusSoc.setSoTimeout(HANDSHAKE_TIMEOUT);
	    UFTimeStamp uft = new UFTimeStamp("ufjci."+Indexor+"-status");
	    if (uft.sendTo(statusSoc) <= 0) {
		System.err.println("JCIMotorComm-"+Indexor+".createStatusSocket> Write Error");
		statusSoc.close();
		statusSoc = null;
		return false;
	    }
	    UFProtocol ufp = null;
	    //get response from agent
	    if ( (ufp = UFProtocol.createFrom(statusSoc)) == null) {
		System.err.println("JCIMotorComm-"+Indexor+".createStatusSocket> Read Error");
		statusSoc.close();
		statusSoc = null;
		return false;
	    }
	    System.out.println("JCIMotorComm-"+Indexor+".createStatusSocket> " + ufp.name() );
	    statusSoc.setSoTimeout(0);
	    return true;
	}
	catch (Exception e) {
	    statusSoc = null;
	    System.err.println("JCIMotorComm-"+Indexor+".createStatusSocket> "+e.toString());
	    return false;
	}
    }

    public void connectCmdSocket() {
	// try to close connection if one exists
	if (comSoc != null) 
	    try {
		comSoc.close();
	    } catch (IOException ioe) {
		System.err.println("JCIMotorComm-"+Indexor+".connectCmdSocket> "+ioe.toString());
	    }
	try {
	    InetSocketAddress agentIPsoca = new InetSocketAddress( agentHost, agentPort );
	    comSoc = new Socket();
	    comSoc.connect( agentIPsoca, CONNECT_TIMEOUT );
	    //must send agent a timestamp with the client name in it
	    comSoc.setSoTimeout(HANDSHAKE_TIMEOUT);
	    UFTimeStamp uft = new UFTimeStamp("ufjci."+Indexor+"-commander");
	    if (uft.sendTo(comSoc) <= 0) {
		System.err.println("JCIMotorComm-"+Indexor+".connectCmdSocket> Write Error");
		return;
	    }
	    UFProtocol ufp = null;
	    //get response from agent
	    if ( (ufp = UFProtocol.createFrom(comSoc)) == null) {
		System.err.println("JCIMotorComm-"+Indexor+".connectCmdSocket> Read Error");
		return;
	    }
	    System.out.println("JCIMotorComm-"+Indexor+".connectCmdSocket> " + ufp.name() );
	    comSoc.setSoTimeout(socTimeout);
	    InetAddress LocalInet = comSoc.getLocalAddress();
	    LocalHost = LocalInet.getHostName();
	} catch (Exception e) {
	    comSoc = null;
	    System.err.println("JCIMotorComm-"+Indexor+".connectCmdSocket> "+e.toString());
	}
    }

    /////////////////////////////////////////////////////////////
    //Accessor and mutator methods for communication components//
    /////////////////////////////////////////////////////////////
 
    public void setHostAndPort(String host, int port) {
	this.agentHost = new String(host);
	this.agentPort = port;
    }
    public void setHost(String hosty) {
	setHostAndPort(hosty,agentPort);
    }
    public void setPort(int porty) {
	setHostAndPort(agentHost,porty);
    }
    public String getHost() {
	return new String(agentHost);
    }
    public int getPort() {
	return agentPort;
    }

    public void setSocTimeout(int timeout) {
	socTimeout = timeout;
	try {
	    comSoc.setSoTimeout(socTimeout);
	} catch (SocketException se) {
	    System.err.println("JCIMotorComm-"+Indexor+".setSocTimeout> "+se.toString());
	}
    }

    ///////////////////////////////
    //Motor communication methods//
    ///////////////////////////////
    
    //Convenience function to send a query/status request to the agent, parse the
    //reply, and return the position of the motor as reported by the agent

    public String getCurrentLocation( boolean queryIndexor )
    {
	String[] valdez = new String[2];

	if( queryIndexor ) {
	    valdez[0] = Indexor + " raw";
	    valdez[1] = Indexor + " Z";
	}
	else {  //ask MC agent for last known location.
	    valdez[0] = "status";
	    valdez[1] = Indexor + " Location";
	}

	UFProtocol ufp = sendCommand( valdez );
	if( ufp == null ) return null;

	try {
	    if( ufp instanceof UFStrings ) {
		StringTokenizer st = new StringTokenizer( ((UFStrings)ufp).valData(0) );
		int ntoks = st.countTokens();
		//normally the third token is the position, but otherwise return last token:
		if( ntoks > 3 ) ntoks = 3;
		for( int i=1; i<ntoks; i++ ) st.nextToken();
		return st.nextToken();
	    }
	    else return ufp.name();
	}
	catch (Exception e) {
	    jciError.show("JCIMotorComm-"+Indexor+".getCurrentLocation> " + e.toString());
	    return null;
	}
    }

    public synchronized UFProtocol sendCommand(String[] s)
    {
	//construct UFStrings object to send to agent
	UFStrings ufs = new UFStrings( LocalHost+":ufjci."+Indexor+"-cmd", s );

	//declare UFProtocol object to receive response from agent
	UFProtocol ufp = null;

	try {
	    //send command to agent
	    if( ufs.sendTo( comSoc ) <= 0 ) {
		System.err.println("JCIMotorComm-"+Indexor+".sendCommand> Write Error");
		return null;
	    }
	    //get response from agent
	    if ( (ufp = UFProtocol.createFrom( comSoc )) == null) {
		System.err.println("JCIMotorComm-"+Indexor+".sendCommand> Read Error");
		return null;
	    }
	    echo(ufp);
	    return ufp;
     	}
	catch (Exception e) {
	    System.err.println("JCIMotorComm-"+Indexor+".sendCommand> "+e.toString());
	    return null;
	}
    }

    public UFProtocol sendParams() {
	String [] s = new String[12];
	s[0] = new String(Indexor + "raw");
	s[1] = new String(Indexor + "E " + "0.00");// settle time
	s[2] = new String(Indexor + "raw");
	s[3] = new String(Indexor + "B " + "0 0"); // jog speed
	s[4] = new String(Indexor + "raw");
	s[5] = new String(Indexor + "I " + getInitialSpeed());
	s[6] = new String(Indexor + "raw");
	s[7] = new String(Indexor + "V " + getTerminalSpeed());
	s[8] = new String(Indexor + "raw");
	s[9] = new String(Indexor + "K " + getAcceleration() + " " + getDeceleration());
	s[10] = new String(Indexor + "raw");
	s[11] = new String(Indexor + "Y " + getHoldCurrent() + " " + getDriveCurrent());
	return sendCommand(s);
    }

    public UFProtocol sendMove(int stepsToMove) {
	String [] s = new String[2];
	String moveChar = "";
	if (stepsToMove >= 0) moveChar = "+"; 
	s[0] = new String(Indexor + "raw");
	s[1] = new String(Indexor + moveChar + stepsToMove);
	return sendCommand(s);
    }

    public UFProtocol sendHiMove(int steps) {
	String [] s = new String[4];
	String moveChar = "";
	steps -= getBackLash();
	s[0] = new String(Indexor + "raw");
	s[1] = new String(Indexor + "N" + steps);
	if (getBackLash() >= 0) moveChar = "+";
	else moveChar = "";
	s[2] = new String(Indexor + "raw");
	s[3] = new String(Indexor + moveChar +  getBackLash());
	return sendCommand(s);	
    }

    private void echo(UFProtocol ufp) {
	if( ufp == null ) return;
	if( ufp instanceof UFStrings ) {
	    if( verbose || ufp.name().indexOf("ERROR") >= 0 ) {
		System.out.println( ufp.name() );
		for (int i=0; i < ((UFStrings)ufp).numVals(); i++)
		    System.out.println( ((UFStrings)ufp).valData(i) );
	    }
	}
	else System.out.println( ufp.description() + ".name=" + ufp.name()  );
    }

    public UFProtocol sendDatum() {
	String [] s = new String[6];
	String moveChar = "";
	if (getBackLash() <= 0) moveChar = "+";
	s[0] = new String(Indexor + "raw");
	s[1] = new String(Indexor + "F " + getDatumSpeed()+ " " + getDatumDirection());
	s[2] = new String(Indexor + "raw");
	s[3] = new String(Indexor + moveChar + (-2 * getBackLash()));
	s[4] = new String(Indexor + "raw");
	s[5] = new String(Indexor + "F " + getFinalDatumSpeed()+ " " + getDatumDirection());
	return sendCommand(s);
    }

    public UFProtocol sendAbort() {
	String [] s = new String[2];
	s[0] = new String(Indexor + "raw");
	s[1] = new String(Indexor + " @");
	return sendCommand(s);
    }

    public UFProtocol sendOrigin() {
	String [] s = new String[2];
	s[0] = new String(Indexor + "raw");
	s[1] = new String(Indexor + " o");  //lowercase is same as uppercase for origin cmd.
	return sendCommand(s);
    }

    public UFProtocol queryStatus() {
	String [] s = new String[2];
	s[0] = new String(Indexor + "raw");
	s[1] = new String(Indexor + " ^");
	return sendCommand(s);
    }

    public String getMotorStatus(Socket statusSoc) {
	UFProtocol ufp = UFProtocol.createFrom(statusSoc);
	if( ufp == null ) {
	    System.err.println("JCIMotorComm-"+Indexor+"::getMotorStatus> null UFObject.");
	    return "ERROR: getMotorStatus>  recvd  NULL  object";
	}
	else if( ufp instanceof UFStrings ) {
	    try {
		String name = ufp.name();
		if (name.charAt(0) == Indexor)
		    if (name.toLowerCase().indexOf("status") != -1)
			return ((UFStrings)ufp).valData(0);
		    else if (name.toLowerCase().indexOf("motion finish")!=-1)
			return ((UFStrings)ufp).valData(0);
		    else
			return "ERROR: Unknown reply";
		else 
		    return "NotMe";
	    }
	    catch (Exception e) {
		System.err.println("JCIMotorComm-"+Indexor+".getMotorStatus> "+e.toString());
		return "ERROR: getMotorStatus> "+ e.toString();
	    }
	}
	else {
	    System.err.println("JCIMotorComm-"+Indexor+"::getMotorStatus> Wrong UFObject. " +
			       "Expexted UFStrings. Received " + ufp.description());
	    return "ERROR: Expected UFStrings. Recvd " + ufp.description();
	}
    }
}

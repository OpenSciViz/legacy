//Version:      (see rcsID)
//Copyright:    Copyright (c) 2003
//Author:       Frank Varosi and David Rashkin
//Company:      University of Florida
//Description:  for control and monitor of CanariCam infrared camera system.

package ufjci;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.tree.*;
import java.io.*;
import java.util.*;
import javaUFLib.*;

//===============================================================================
/**
 * Panel container of a tree model for saving/restoring high-level configuration
 * of instrument optics and observation parameters, used by JPanelMaster of JCI.
 */
public class JCIconfigTree extends JPanel
{
    public static final
	String rcsID = "$Name:  $ $Id: JCIconfigTree.java,v 1.1 2004/11/01 19:04:00 amarin Exp $";

    JTree cfgTree = new JTree();
    DefaultTreeModel treeModel;

    String cfg_file_name = "CC.cfg";
    boolean ok_to_edit = true;

    JButton buttonLoadCfg = new JButton("Load Cfg Set");
    JButton buttonEditCfg = new JButton("Enable Cfg Edits");
    JButton buttonDoConfig = new JButton("Do Selected Cfg");
    JButton buttonUseActive = new JButton("Use Active");
    JButton buttonDelCfg = new JButton("Delete Config");
    JButton buttonMoveCfgUp = new JButton("Move Up");
    JButton buttonMoveCfgDown = new JButton("Move Down");
    JButton buttonSaveConfig = new JButton("Save Config");
    JButton buttonSaveCfgSet = new JButton("Save Cfg Set");
    JButton buttonBlankAll = new JButton("Blank all inputs");

    //these are pointers to combo-boxes and text-fields in JPanelMaster:
    LinkedHashMap chosenModesMap;
    LinkedHashMap chosenOpticsMap;
    LinkedHashMap enteredParmsMap;

    public JCIconfigTree(LinkedHashMap modes, LinkedHashMap parms, LinkedHashMap optics)
    {
	chosenModesMap = modes;
	enteredParmsMap = parms;
	chosenOpticsMap = optics;

	//Configuration Management Buttons actions:
	buttonDoConfig.addActionListener(new ActionListener()
	    { public void actionPerformed(ActionEvent e) { buttonDoConfig_action(); } });
	buttonBlankAll.addActionListener(new ActionListener()
	    { public void actionPerformed(ActionEvent e) { buttonBlankAll_action(); } });
	buttonLoadCfg.addActionListener(new ActionListener()
	    { public void actionPerformed(ActionEvent e) { buttonLoadCfg_action(); } });
	buttonEditCfg.addActionListener(new ActionListener()
	    { public void actionPerformed(ActionEvent e) { buttonEditCfg_action(); } });
	buttonSaveConfig.addActionListener(new ActionListener()
	    { public void actionPerformed(ActionEvent e) { buttonSaveConfig_action(); } });
	buttonUseActive.addActionListener(new ActionListener()
	    { public void actionPerformed(ActionEvent e) { buttonUseActive_action(); } });
	buttonDelCfg.addActionListener(new ActionListener()
	    { public void actionPerformed(ActionEvent e) { buttonDelCfg_action(); } });
	buttonMoveCfgUp.addActionListener(new ActionListener()
	    { public void actionPerformed(ActionEvent e) { buttonMoveCfgUp_action(); } });
	buttonMoveCfgDown.addActionListener(new ActionListener()
	    { public void actionPerformed(ActionEvent e) { buttonMoveCfgDown_action(); } });
	buttonSaveCfgSet.addActionListener(new ActionListener()
	    { public void actionPerformed(ActionEvent e) { buttonSaveCfgSet_action(); } });

	JScrollPane jScrollPane = new JScrollPane();
	jScrollPane.getViewport().add( cfgTree, null );
	JPanel configScroll  = new JPanel();
	configScroll.setLayout(new BorderLayout());
	configScroll.add(new JLabel("Saved Configurations"), BorderLayout.NORTH);
	configScroll.add( jScrollPane, BorderLayout.CENTER );

	JPanel configCmds = new JPanel();
	configCmds.setLayout(new GridLayout(0,1));
	configCmds.add(buttonDoConfig, null);
	configCmds.add(buttonLoadCfg, null);
	configCmds.add(buttonEditCfg, null);
	configCmds.add(buttonSaveCfgSet, null);
	configCmds.add(buttonUseActive, null);
	configCmds.add(buttonSaveConfig, null);
	configCmds.add(buttonDelCfg, null);
	configCmds.add(buttonMoveCfgUp, null);
	configCmds.add(buttonMoveCfgDown, null);
	//	configCmds.add(buttonBlankAll, null);

	this.setLayout(new BorderLayout());
	this.setBorder(new EtchedBorder(0));
	this.add( configCmds, BorderLayout.EAST );
	this.add( configScroll, BorderLayout.CENTER );
    }
//-------------------------------------------------------------------------------
    /**
     *JCIconfigTree#Save configuration button action performed
     */
    void buttonSaveCfgSet_action()
    {
	JFileChooser chooser = new JFileChooser(jci.data_path);
	ExtensionFilter cfg_type = new ExtensionFilter("Configuration Files", new String[] {".cfg"});
	chooser.setFileFilter(cfg_type); // Initial filter setting
	chooser.setSelectedFile(new File(cfg_file_name));
	chooser.setDialogTitle("Save configuration tree to");
	chooser.setApproveButtonText("Save");
	int returnVal = chooser.showOpenDialog(this);
	if( returnVal == JFileChooser.CANCEL_OPTION ) return;

	if( returnVal == JFileChooser.APPROVE_OPTION )
	    {
		cfg_file_name = chooser.getSelectedFile().getName();
		if (!cfg_file_name.endsWith(".cfg")) cfg_file_name = cfg_file_name + ".cfg";
	    }

	TextFile out_file = new TextFile(jci.data_path + cfg_file_name, TextFile.OUT) ;
	Object root = treeModel.getRoot();

	if( !out_file.ok() ) {
	    jciError.show("error opening file: "+out_file.toString());
	    return;
	}

	for (int i = 0; i < treeModel.getChildCount(root); i++)
	    {
		Object cfg = treeModel.getChild(root, i);

		if (!treeModel.isLeaf(cfg))
		    {
			if (!out_file.ok())
			    jciError.show("error writing to file: "+out_file.toString());
			else out_file.println(cfg.toString());

			for (int j = 0; j < treeModel.getChildCount(cfg); j++) {
			    Object child = treeModel.getChild(cfg, j);
			    if (treeModel.isLeaf(child)) {
				//System.out.println("." + child);
				if (!out_file.ok())
				    jciError.show("error writing to file: "+out_file.toString());
				else out_file.println("." + child);
			    }
			}
		    }
	    }
	out_file.close();
    } //end of buttonSaveCfgSet_action()
//-------------------------------------------------------------------------------
    /**
     *JCIconfigTree#Load configuration button action performed
     */
    void buttonLoadCfg_action() {
	JFileChooser chooser = new JFileChooser(jci.data_path);
	ExtensionFilter cfg_type = new ExtensionFilter("Config files", new String[] {".cfg"});
	chooser.setFileFilter(cfg_type); // Initial filter setting
	chooser.setSelectedFile(new File(cfg_file_name));
	chooser.setDialogTitle("Load configuration tree from");
	chooser.setApproveButtonText("Load");
	int returnVal = chooser.showOpenDialog(this);
	if (returnVal == JFileChooser.CANCEL_OPTION ) return;
	if(returnVal == JFileChooser.APPROVE_OPTION) {
	    cfg_file_name = chooser.getSelectedFile().getName();
	    if (!cfg_file_name.endsWith(".cfg")) cfg_file_name = cfg_file_name + ".cfg";
	}

	LoadCfgTree();
    }
//-------------------------------------------------------------------------------
    /**
     * Loads the configuration tree from file: jci.data_path + cfg_file_name
     */
    void LoadCfgTree() {
	DefaultMutableTreeNode root = new DefaultMutableTreeNode(cfg_file_name);
	String line;
	DefaultMutableTreeNode parent = null;
	TextFile in_file = new TextFile(jci.data_path + cfg_file_name, TextFile.IN) ;

	// read the file
	while( (line = in_file.readLine()) != null && in_file.ok() ) {
	    if( !line.trim().equals("") ) {
		if (line.charAt(0) == '.') { // add child
		    if (parent == null) {
			jciError.show("reading from " + cfg_file_name + " - no parent for child");
			return;
		    }
		    else parent.add(new DefaultMutableTreeNode(line.substring(1)));
		}
		else { // add parent
		    parent = new DefaultMutableTreeNode(line);
		    root.add(parent);
		}
	    }
	}

	in_file.close();
	treeModel = new DefaultTreeModel(root);
	cfgTree.setModel(treeModel);
    }
//-------------------------------------------------------------------------------
    /**
     *JCIconfigTree#Delete configuration button action performed
     */
    void buttonDelCfg_action() {
	TreePath path = cfgTree.getSelectionPath();
	if (path == null) {
	    jciError.show("nothing selected to delete");
	}
	else {
	    MutableTreeNode treeNode = (MutableTreeNode)path.getPathComponent(1);
	    if( JOptionPane.showConfirmDialog(null,
					      "DELETE the selected configutation> "
					      + treeNode + " ?") == JOptionPane.YES_OPTION )
		{
		    treeModel.removeNodeFromParent(treeNode);
		}
	}
    }
//-------------------------------------------------------------------------------
    /**
     *JCIconfigTree#Add configuration button action performed
     */
    void buttonSaveConfig_action()
    {
	int index;
	DefaultMutableTreeNode root = (DefaultMutableTreeNode)treeModel.getRoot();
	TreePath path = cfgTree.getSelectionPath();

	if (path == null) {
	    index = root.getChildCount();
	}
	else {
	    MutableTreeNode treeNode = (MutableTreeNode)path.getPathComponent(1);
	    index = root.getIndex(treeNode) + 1;
	}

	String cfg_name = JOptionPane.showInputDialog("Enter name of new configuration:");
	if( cfg_name == null ) return;

	DefaultMutableTreeNode parent = new DefaultMutableTreeNode(cfg_name);
	String val;

	Set keys = chosenModesMap.keySet();
	Iterator keyit = keys.iterator();

	while( keyit.hasNext() ) {
	    String key = (String)keyit.next();
	    UFComboPanel chooseMode = (UFComboPanel)chosenModesMap.get( key );
	    if( chooseMode != null ) {
		val = chooseMode.getSelectedItem();
		if( val.trim().length() > 0 )
		    parent.add(new DefaultMutableTreeNode(key + " : " + val));
	    }
	}

	keys = enteredParmsMap.keySet();
	keyit = keys.iterator();

	while( keyit.hasNext() ) {
	    String key = (String)keyit.next();
	    UFTextPanel enteredParm = (UFTextPanel)enteredParmsMap.get( key );
	    if( enteredParm != null ) {
		val = enteredParm.desiredField.getText();
		if( val.trim().length() > 0 )
		    parent.add(new DefaultMutableTreeNode(key + " : " + val));
	    }
	}

	keys = chosenOpticsMap.keySet();
	keyit = keys.iterator();

	while( keyit.hasNext() ) {
	    String key = (String)keyit.next();
	    UFComboPanel chooseOptic = (UFComboPanel)chosenOpticsMap.get( key );
	    if( chooseOptic != null ) {
		if( chooseOptic.isSelected() ) {
		    val = chooseOptic.getSelectedItem();
		    if( val.trim().length() > 0 )
			parent.add(new DefaultMutableTreeNode(key + " : " + val));
		}
	    }
	}

	treeModel.insertNodeInto(parent, root, index);
	cfgTree.setSelectionPath(path);
    } //end of buttonSaveConfig_action
//-------------------------------------------------------------------------------
    /**
     *JCIconfigTree#Use current instrument configuration and then add to config tree.
     */
    void buttonUseActive_action()
    {
	String val;

	Set keys = chosenModesMap.keySet();
	Iterator keyit = keys.iterator();

	while( keyit.hasNext() ) {
	    String key = (String)keyit.next();
	    UFComboPanel chooseMode = (UFComboPanel)chosenModesMap.get( key );
	    if( chooseMode != null )
		chooseMode.setDesiredSel( chooseMode.getActive() );
	}

	keys = enteredParmsMap.keySet();
	keyit = keys.iterator();

	while( keyit.hasNext() ) {
	    String key = (String)keyit.next();
	    UFTextPanel enteredParm = (UFTextPanel)enteredParmsMap.get( key );
	    if( enteredParm != null )
		enteredParm.setDesired( enteredParm.getActive() );
	}

	keys = chosenOpticsMap.keySet();
	keyit = keys.iterator();

	while( keyit.hasNext() ) {
	    String key = (String)keyit.next();
	    UFComboPanel chooseOptic = (UFComboPanel)chosenOpticsMap.get( key );
	    if( chooseOptic != null ) {
		if( chooseOptic.isSelected() )
		    chooseOptic.setDesiredSel( chooseOptic.getActive() );
	    }
	}

	//buttonSaveConfig_action();

    } //end of buttonUseActive_action
//-------------------------------------------------------------------------------
    /**
     *JCIconfigTree#Move configuration up button action performed
     */
    void buttonMoveCfgUp_action() {
	TreePath path = cfgTree.getSelectionPath();
	if (path == null) {
	    jciError.show("nothing selected to move");
	    return;
	}
	MutableTreeNode treeNode = (MutableTreeNode)path.getPathComponent(1);
	DefaultMutableTreeNode root = (DefaultMutableTreeNode)treeModel.getRoot();
	int i = root.getIndex(treeNode);
	if (i > 0) {
	    treeModel.removeNodeFromParent(treeNode);
	    treeModel.insertNodeInto(treeNode, root, i-1);
	    cfgTree.setSelectionPath(path);
	}
    }
//------------------------------------------------------------------------------
    /**
     * JCIconfigTree#Move configuration down button action performed
     */
    void buttonMoveCfgDown_action() {
	TreePath path = cfgTree.getSelectionPath();
	if (path == null) {
	    jciError.show("nothing selected to move");
	    return;
	}
	MutableTreeNode treeNode = (MutableTreeNode)path.getPathComponent(1);
	DefaultMutableTreeNode root = (DefaultMutableTreeNode)treeModel.getRoot();
	int i = root.getIndex(treeNode);
	if (i < root.getChildCount()-1) {
	    treeModel.removeNodeFromParent(treeNode);
	    treeModel.insertNodeInto(treeNode, root, i+1);
	    cfgTree.setSelectionPath(path);
	}
    }
//-------------------------------------------------------------------------------
    /**
     *JCIconfigTree#Do configuration button action performed
     */
    void buttonDoConfig_action()
    {
	TreePath path = cfgTree.getSelectionPath();

	if (path == null) {
	    jciError.show("nothing selected to do");
	    return;
	}

	MutableTreeNode treeNode = (MutableTreeNode)path.getPathComponent(1);
	String tname = treeNode.toString() + ".";
	System.out.println(tname);
	Object cfg = treeNode;
	if( treeModel.isLeaf(cfg) ) return;

	for( int j = 0; j < treeModel.getChildCount(cfg); j++ )
	    {
		Object child = treeModel.getChild(cfg, j);

		if( treeModel.isLeaf(child) )
		    {
			System.out.println(tname + child.toString());
			String[] wordpair = child.toString().split(":");
			String key = wordpair[0].trim();
			String val = wordpair[1].trim();
			UFComboPanel chooseMode = (UFComboPanel)chosenModesMap.get( key );

			if( chooseMode != null )
			    chooseMode.setDesiredSel( val );
			else {
			    UFTextPanel enteredParm = (UFTextPanel)enteredParmsMap.get( key );
			    if( enteredParm != null )
				enteredParm.setDesired( val );
			    else {
				UFComboPanel chooseOptic = (UFComboPanel)chosenOpticsMap.get( key );
				if( chooseOptic != null )
				    chooseOptic.setDesiredSel( val );
			    }
			}
		    }
	    }
	System.out.println("end.");
    }
//-------------------------------------------------------------------------------
    /**
     *JCIconfigTree#Blank all button action performed (clears all input fields and selections)
     */
    void buttonBlankAll_action()
    {
	if( JOptionPane.showConfirmDialog(null,"Are you sure you want to CLEAR all selections ?")
	    == JOptionPane.YES_OPTION )
	    {
		Set keys = chosenModesMap.keySet();
		Iterator keyit = keys.iterator();

		while( keyit.hasNext() )
		    {
			String key = (String)keyit.next();
			UFComboPanel chooseMode = (UFComboPanel)chosenModesMap.get( key );
			if( chooseMode != null )
			    chooseMode.setDesiredSel("");
		    }

		keys = enteredParmsMap.keySet();
		keyit = keys.iterator();

		while( keyit.hasNext() )
		    {
			String key = (String)keyit.next();
			UFTextPanel enteredParm = (UFTextPanel)enteredParmsMap.get( key );
			if( enteredParm != null )
			    enteredParm.setDesired(" ");
		    }

		keys = chosenOpticsMap.keySet();
		keyit = keys.iterator();

		while( keyit.hasNext() )
		    {
			String key = (String)keyit.next();
			UFComboPanel chooseOptic = (UFComboPanel)chosenOpticsMap.get( key );
			if( chooseOptic != null ) {
			    if( chooseOptic.isSelected() ) chooseOptic.setDesiredSel("");
			}
		    }
	    }
    } //end of jButtonBlankAll_action
//-------------------------------------------------------------------------------
    /**
     *JCIconfigTree#Edit configuration button action performed
     */
    void buttonEditCfg_action() {
	if (ok_to_edit) {
	    ok_to_edit = false;
	    buttonEditCfg.setText("Enable Edits");
	    buttonBlankAll.setEnabled(false);
	    buttonSaveConfig.setEnabled(false);
	    buttonUseActive.setEnabled(false);
	    buttonDelCfg.setEnabled(false);
	    buttonMoveCfgUp.setEnabled(false);
	    buttonMoveCfgDown.setEnabled(false);
	    buttonSaveCfgSet.setEnabled(false);
	}
	else {
	    ok_to_edit = true;
	    buttonEditCfg.setText("Disable Edits");
	    buttonBlankAll.setEnabled(true);
	    buttonSaveConfig.setEnabled(true);
	    buttonUseActive.setEnabled(true);
	    buttonDelCfg.setEnabled(true);
	    buttonMoveCfgUp.setEnabled(true);
	    buttonMoveCfgDown.setEnabled(true);
	    buttonSaveCfgSet.setEnabled(true);
	}
    }
} //end of JCIconfigTree

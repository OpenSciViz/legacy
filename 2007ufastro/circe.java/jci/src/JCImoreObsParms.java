//Title:        JCImoreObsParms for Java Control Interface (JCI) of CanariCam
//Version:      (see rcsID)
//Copyright:    Copyright (c) 2004
//Author:       Frank Varosi
//Company:      University of Florida
//Description:  for control and monitoring of CanariCam infrared camera system.

package ufjci;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.util.*;
import javaUFLib.*;

//===============================================================================
/* 
 * This class creates a window for overriding/modifying additional observation parameters.
 */

class JCImoreObsParms extends JFrame
{
    UFComboPanel chooseReadoutMode;
    UFComboPanel chooseNodPattern;

    LinkedHashMap chosenModesMap = new LinkedHashMap(2);
    LinkedHashMap enteredParmsMap = new LinkedHashMap(6);

    UFTextPanel enterFrameTime   = new UFTextPanel(false, "Frame Time (ms):");
    UFTextPanel enterSaveFreq    = new UFTextPanel(false, "Save Frequency (Hz):");
    UFTextPanel enterNodDwell    = new UFTextPanel(false, "Nod Dwell Time (s):");

    UFTextPanel enterEmissivity  = new UFTextPanel(false, "Mirror Emissivity :");
    UFTextPanel enterTemperature = new UFTextPanel(false, "Temperature (C):");
//-----------------------------------------------------------------------------------

    public JCImoreObsParms()
    {
	super("Additional Observation Parameters");

	String[] readoutModes = {"S1R3", "S1", "Automatic"};
	chooseReadoutMode = new UFComboPanel("Readout Mode :", false, readoutModes );

	String[] NodPatterns = {"A-B-A-B","A-BB-A"};
	chooseNodPattern = new UFComboPanel("Nod Pattern :", false, NodPatterns );

	chosenModesMap.put( "ReadoutMode", chooseReadoutMode );
	chosenModesMap.put( "NodPattern", chooseNodPattern );

	enteredParmsMap.put( "FrameTime", enterFrameTime );
	enteredParmsMap.put( "SaveFrequency", enterSaveFreq );
	enteredParmsMap.put( "NodDwellTime", enterNodDwell );

	enteredParmsMap.put( "TelescopeEmissivity", enterEmissivity );
	enteredParmsMap.put( "AmbientTemperature", enterTemperature );

//	LeftPanel.add(new JLabel("Humidity (real)"));
//	LeftPanel.add(new JLabel("Air Mass (real)"));

//	EPICSTextField Humidity_TF = new EPICSTextField(tcs + "sad:currentRH", "Humidity");
//	EPICSTextField AirMass_TF = new EPICSTextField(tcs + "sad:airMassNow", "Environment");

	JPanel parmsPanel = new JPanel();
	parmsPanel.setLayout(new GridLayout(0,1));
	parmsPanel.setBorder(new EtchedBorder(0));
	JPanel title = new JPanel();
	title.setLayout(new RatioLayout());
	JLabel label = new JLabel("more Parameters:");
	title.add("0.00,0.0;0.41,1.0", label);
	label = new JLabel("Desired");
	title.add("0.42,0.0;0.28,1.0", label);
	label = new JLabel("Active");
	title.add("0.70,0.0;0.28,1.0", label);

	parmsPanel.add( title );
	parmsPanel.add(chooseReadoutMode);
	parmsPanel.add(chooseNodPattern);
	parmsPanel.add(enterFrameTime);
	parmsPanel.add(enterSaveFreq);
	parmsPanel.add(enterNodDwell);
	parmsPanel.add(enterEmissivity);
	parmsPanel.add(enterTemperature);

	UFLabel metaFrameTime = new UFLabel("meta-FrameTime___=");
	UFLabel monDutyCycle = new UFLabel("Duty Cycle________=");
	UFLabel monFrameCoadds = new UFLabel("Frame Coadds_____=");
	UFLabel monSkipFrames = new UFLabel("Skip Frames______=");
	UFLabel monWellDepth = new UFLabel("Well Depth_______=");

	JPanel BottomPanel = new JPanel();
	BottomPanel.setLayout(new GridLayout(5,1));
	BottomPanel.add( metaFrameTime );
	BottomPanel.add( monDutyCycle );
	BottomPanel.add( monFrameCoadds );
	BottomPanel.add( monSkipFrames );
	BottomPanel.add( monWellDepth );

	this.getContentPane().setLayout(new RatioLayout());
	this.getContentPane().add("0.01,0.00;0.99,0.69",parmsPanel);
	this.getContentPane().add("0.05,0.71;0.60,0.28",BottomPanel);

	JButton buttonDONE = new JButton ("DONE");
	this.getContentPane().add("0.70,0.90;0.30,0.10",buttonDONE);

	buttonDONE.addActionListener(new java.awt.event.ActionListener()
	    { public void actionPerformed(ActionEvent e) { buttonDONE_action(); } });
    }
//--------------------------------------------------------
    void buttonDONE_action() { this.dispose(); }

}//end of class JCImoreObsParms******************************************

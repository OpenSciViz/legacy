//Title:        JPanelBias for Java Control Interface (JCI) using UFLib Protocol communications.
//Version:      (see rcsID)
//Copyright:    Copyright (c) 2003
//Authors:      Ziad Saleh and Frank Varosi
//Company:      University of Florida
//Description:  for control and monitoring of CanariCam infrared camera system.

package ufjci ;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;
import javax.swing.SwingConstants;
import javax.swing.border.*;
import java.text.DecimalFormat;
import javaUFProtocol.*;
import javaUFLib.*;

/**
 * Custom panel for setting Det. Bias voltages via communication with Detector Control Agent 
 * @authors Ziad Saleh and Frank Varosi
 */

//===============================================================================
// Panel for voltage title headings:

class JPanelTitleV extends JPanel {
    public JPanelTitleV()
    {
	setLayout(new RatioLayout());
	this.add("0.01,0.01;0.20,0.98", new JLabel("Desired",JLabel.CENTER));
	this.add("0.25,0.01;0.22,0.98", new JLabel("Active",JLabel.CENTER));
	this.add("0.47,0.01;0.40,0.98", new JLabel("Name",JLabel.CENTER));
    }
}

//===============================================================================
// Panel for showing desired/active bias voltages, and methods to access/modify:

class JPanelVoltage extends JPanel
{  
    public  String Name;
    public  int Register ;
    public  int LatchOrder;
    public  double BiasVoltage ;
    public  double DacScaleFactor ;
    public  int DacOffset;
    public  int DacValue;
    UFTextPanel BiasVoltPanel;

    public JPanelVoltage(String name ,String register, String latchorder,
			 String biasvoltage, String dacscalefactor, String dacoffset)
    {
	Name = name ;
	Register = Integer.parseInt( register );
	LatchOrder= Integer.parseInt( latchorder );
	BiasVoltage = Double.valueOf( biasvoltage.trim() ).doubleValue();
	DacScaleFactor = Double.valueOf( dacscalefactor.trim() ).doubleValue();
	DacOffset = Integer.parseInt( dacoffset );
	BiasVoltPanel = new UFTextPanel( name, true ); //true for name on right side.
	DacValue = computeDAC( BiasVoltage );
	BiasVoltPanel.setDesired( biasvoltage, DacValue );
	BiasVoltPanel.desiredField.setEditable(true);
	this.setLayout(new GridLayout(1,1));
	this.add( BiasVoltPanel );
    }

    public int computeDAC( double Voltage )
    {
	return (int)Math.round( Voltage*DacScaleFactor + DacOffset );
    }

    public double computeVolts( int DACval )
    {
	return( ( DACval - DacOffset )/DacScaleFactor );
    }

    public void setVolts( String DACval )
    {
	DacValue = Integer.parseInt(DACval);
	double Voltage = computeVolts( DacValue );
	String volts = Double.toString( Voltage );
	int ndigits = volts.indexOf(".") + 4;
	if( ndigits < 4 ) ndigits = 4;
	if( ndigits > volts.length() ) ndigits = volts.length();
	BiasVoltPanel.setActive( volts.substring(0,ndigits), DacValue );
    }

    public String getVolts() { return BiasVoltPanel.getActive(); }

    public String getVdefault() { return BiasVoltPanel.getDesired(); }

    public String get_Name() { return BiasVoltPanel.name(); }

} //end of Class JPanelVoltage.

//===============================================================================
// Main Tabbed Pane for controlling Detector Bias voltages:

public class JPanelBias  extends UFLibPanel
{
    public static final
	String rcsID = "$Name:  $ $Id: JPanelBias.java,v 1.1 2004/11/01 19:04:00 amarin Exp $";

    public static final int NvBias=24; //must be divisible by 3, for equal panels: Left, Middle, Right.
    final private JPanelVoltage jPanelVoltage[];
    private JPanel LeftPanel, MidPanel, RightPanel;
    private JButton readAllButton, resetButton, LatchButton;
    private RatioLayout  JPanelBiasLayout;
    private JButton jButtonPassword = new JButton("Send  Password");
    JPasswordField jTextPassword = new JPasswordField();
    public boolean sentPassword = false;
    public JButton powerStatus = new JButton();
    public JButton powerON = new JButton("ON");
    public JButton powerOFF = new JButton("OFF");
    JLabel powerLabel = new JLabel("POWER : ",JLabel.RIGHT);
    JLabel jLabelPassword = new JLabel("Password: ",JLabel.RIGHT);

    String [] items_Well = {"Shallow","Deep"}; //orientation Vertical=true, and useIndex=true:
    public UFComboPanel chooseWellDepth = new UFComboPanel("Detector Well Depth", items_Well, true,true );

    String [] items_Bias = {"OFF","Low","Medium","High"};
    public UFComboPanel chooseBiasLevel = new UFComboPanel("V_DetGrv Bias Level", items_Bias, true,true );

    private JButton applyBiasLevDepth = new JButton("APPLY Well Depth and Bias Level");

    int powerState ;
    JPanelDetector DetectorPanel;

    //Transaction info Labels (added at bottom of frame):
    UFLabel statusAction   = new UFLabel("Action___:");
    UFLabel statusResponse = new UFLabel("Response:");

    public JPanelBias( JPanelDetector DetectorPanel )
    {
	this.DetectorPanel = DetectorPanel;
	JButton readAllButton = new JButton("READ ALL Voltages");
	readAllButton.setDefaultCapable(false);
	JButton resetButton = new JButton("RESET to MCE defaults");
	resetButton.setDefaultCapable(false);
	JButton LatchButton = new JButton("LATCH");
	LatchButton.setDefaultCapable(false);

	JButton applyBiasVoltageButton = new JButton("APPLY Desired Bias Voltages");
	applyBiasVoltageButton.setDefaultCapable(false);
	jPanelVoltage = new JPanelVoltage[NvBias];

	int ixv = 0;
	String record = null ;

	//read file of parameters and create buttons for each bias voltage:
	try {
	    FileReader fin = new FileReader(jci.data_path + "dc_bias_param.txt");
	    BufferedReader bin = new BufferedReader(fin);
	    LeftPanel = new JPanel();
	    LeftPanel.setBorder(new EtchedBorder(0));
	    LeftPanel.setLayout(new GridLayout(9,1));
	    LeftPanel.add(new JPanelTitleV());
	    record = bin.readLine();
	    while( record.indexOf("V_") < 0 ) record = bin.readLine(); //read until first voltage.	 

	    for(int i=1 ; i <= NvBias/3; i++) 
		{
		    StringTokenizer tokens = new StringTokenizer(record);
		    jPanelVoltage[ixv] = new JPanelVoltage(tokens.nextToken(),tokens.nextToken(),
							   tokens.nextToken(),tokens.nextToken(),
							   tokens.nextToken(),tokens.nextToken());
		    LeftPanel.add(jPanelVoltage[ixv++]);
		    record =  bin.readLine();
		}
		    
	    MidPanel = new JPanel();
	    MidPanel.setBorder(new EtchedBorder(0));
	    MidPanel.setLayout(new GridLayout(9,1));
	    MidPanel.add(new JPanelTitleV());

	    for(int i=1 ; i <= NvBias/3; i++) 
		{
		    StringTokenizer tokens = new StringTokenizer(record);
		    jPanelVoltage[ixv] = new JPanelVoltage(tokens.nextToken(),tokens.nextToken(),
							   tokens.nextToken(),tokens.nextToken(),
							   tokens.nextToken(),tokens.nextToken());
		    MidPanel.add(jPanelVoltage[ixv++]);
		    record =  bin.readLine();
		}
		    
	    RightPanel = new JPanel();
	    RightPanel.setBorder(new EtchedBorder(0));
	    RightPanel.setLayout(new GridLayout(9,1));
	    RightPanel.add(new JPanelTitleV());

	    for(int i=1 ; i <= NvBias/3; i++) 
		{
		    StringTokenizer tokens = new StringTokenizer(record); 
		    jPanelVoltage[ixv] = new JPanelVoltage(tokens.nextToken(),tokens.nextToken(),
							   tokens.nextToken(),tokens.nextToken(),
							   tokens.nextToken(),tokens.nextToken());
		    RightPanel.add(jPanelVoltage[ixv++]);
		    record =  bin.readLine();
		}
	    bin.close();  //done reading file and creating buttons for each bias voltage.
	}
	catch(Exception e) { JOptionPane.showMessageDialog(null,e,"Error",JOptionPane.ERROR_MESSAGE); }    
	    
	applyBiasVoltageButton.addActionListener( new ActionListener() {
		public void actionPerformed(ActionEvent e) { setDesiredVoltages(); } } );

	resetButton.addActionListener( new ActionListener() {
		public void actionPerformed( ActionEvent e) { resetAll_action(); } } );

	readAllButton.addActionListener( new ActionListener() {
		public void actionPerformed( ActionEvent e) { readAll_action(); } } );					    
	jButtonPassword.addActionListener( new ActionListener() {
		public void actionPerformed(ActionEvent e) { jButtonPassword_action(e); } } );

	LatchButton.addActionListener( new ActionListener() {
		public void actionPerformed(ActionEvent e) { LatchButton_action(e); } } );

	powerON.addActionListener( new ActionListener() { 
		public void actionPerformed(ActionEvent e) { powerON_action(e); } } );

	powerOFF.addActionListener( new ActionListener() { 
		public void actionPerformed(ActionEvent e) { powerOFF_action(e); } } );

	applyBiasLevDepth.addActionListener( new ActionListener() {
		public void actionPerformed(ActionEvent e) { biasLevelWell_action(); } } );

	setLayout(new RatioLayout());
	this.add("0.01,0.03;0.25,0.17",chooseWellDepth);
	this.add("0.27,0.03;0.31,0.17",chooseBiasLevel);
	this.add("0.60,0.02;0.10,0.07",powerLabel);
	this.add("0.70,0.02;0.05,0.07",powerStatus);
	this.add("0.76,0.02;0.09,0.07",powerON);
	this.add("0.86,0.02;0.09,0.07",powerOFF);
	this.add("0.60,0.12;0.30,0.07",applyBiasLevDepth) ;
	this.add("0.01,0.22;0.32,0.55",LeftPanel);
	this.add("0.34,0.22;0.32,0.55",MidPanel);
	this.add("0.67,0.22;0.32,0.55",RightPanel);
	this.add("0.01,0.8;0.45,0.07",applyBiasVoltageButton);
	this.add("0.01,0.9;0.21,0.07",readAllButton);
	this.add("0.25,0.9;0.21,0.07",resetButton);
	this.add("0.50,0.8;0.12,0.07",LatchButton);
	this.add("0.60,0.8;0.10,0.06",jLabelPassword);
	this.add("0.70,0.8;0.10,0.06",jTextPassword);
	this.add("0.80,0.8;0.15,0.06",jButtonPassword) ;

	this.add("0.50,0.91;0.54,0.04", statusAction);
	this.add("0.50,0.95;0.54,0.04", statusResponse);

    } //end of method JPanelBias().
//-------------------------------------------------------------------------------
    /**
     * JPanelBias#Password button action performed
     *@param e not used
     */

    void jButtonPassword_action(ActionEvent e)
    {
	String passwd = new String( jTextPassword.getPassword() );
	String Command[] = { "MCE Password" , "ARRAY_PW_BIAS  " + passwd };
	UFStrings ufsDCcmds = new UFStrings(Command[0], Command);
	commandAgent( ufsDCcmds );
	//Clear the password field
	jTextPassword.setText("");
    }
//-------------------------------------------------------------------------------

    void resetAll_action()  
    { 
	int no = JOptionPane.showConfirmDialog(null,"RESET All Bias Voltages to MCE defaults ?","Warning",
					       JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
	if( no != 1 ) {
	    String Command[] = {"RESET All Bias Voltages", "ARRAY_INIT_STATE_BIAS"};
	    UFStrings ufsDCcmds = new UFStrings(Command[0], Command);
	    commandAgent( ufsDCcmds );
	}

	// Read all Bias voltages quietly:
	readAll_action(false);
    }
//-------------------------------------------------------------------------------
	
    void readAll_action() { readAll_action(true); }

    void readAll_action( boolean showActionResponse )
    { 
	// Construct the command that reads all the voltages
	String Command[] = {"READ All Bias Voltages", "ARRAY_DAC_READBACK_BIAS"};
	UFStrings ufsDCcmds = new UFStrings(Command[0], Command);
	commandAgent( ufsDCcmds, showActionResponse );
    }
//-------------------------------------------------------------------------------

    void LatchButton_action( ActionEvent e )  
    { 
	String Command[] = {"LATCH All Bias Voltages", "DAC_LAT_ALL_BIAS"};
	UFStrings ufsDCcmds = new UFStrings(Command[0], Command);
	commandAgent( ufsDCcmds );
    }
//-------------------------------------------------------------------------------
    //For powerON/OFF actions the actual status of power will be reported by DC agent
    // to the StatusReceiver thread in JCIobservStatus JPanelObsStatus,
    // which then invokes the public powerIndicator() method, see below.

    void powerON_action( ActionEvent e) 
    {
	String Command[] = {"Bias Power=ON","ARRAY_POWER_UP_BIAS","Vgate=ON","ARRAY_VGATE  1"};
	UFStrings ufsDCcmds = new UFStrings(Command[0], Command);
	commandAgent( ufsDCcmds );
    }
//-------------------------------------------------------------------------------

    void powerOFF_action( ActionEvent e) 
    {
	String Command[] = {"Bias Power=OFF","ARRAY_POWER_DOWN_BIAS"};
	UFStrings ufsDCcmds = new UFStrings(Command[0], Command);
	commandAgent( ufsDCcmds );
    }
//-------------------------------------------------------------------------------

    void biasLevelWell_action() 
    {
	String Command[] = new String[4];
	Command[0] = "Set Well Depth";
	Command[1] = "ARRAY_WELL " + chooseWellDepth.desiredSelect.getSelectedIndex();
	Command[2] = "Set Bias Level";
	Command[3] = "ARRAY_SET_DET_BIAS " + chooseBiasLevel.desiredSelect.getSelectedIndex();
	UFStrings ufsDCcmds = new UFStrings("Set Bias Level & Well Depth", Command);
	commandAgent( ufsDCcmds );
    }
//-------------------------------------------------------------------------------

    public void powerIndicator( boolean powerON ) 
    {
	if( powerON )
	    powerStatus.setBackground( Color.green );
	else
	    powerStatus.setBackground( Color.red );
    }
//-------------------------------------------------------------------------------
    //This method is invoked by JPanelObsStatus upon recving status from DC agent:

    public void powerIndicator( String powerState ) 
    {
	if( powerState.trim().toUpperCase().equals("ON") )
	    powerIndicator(true);
	else if( powerState.trim().toUpperCase().equals("OFF") )
	    powerIndicator(false);
	else
	    powerStatus.setBackground( Color.yellow );
    }
//-------------------------------------------------------------------------------

    public void setNewStatus( UFStrings newStatus )
    {
	String status = newStatus.name();

	if( status.indexOf("Power") > 0 ) {
	    powerIndicator( newStatus.valData(0) );
	}
	else if( status.indexOf("Level") > 0 ) {
	    chooseBiasLevel.setActive( newStatus.valData(0) );
	}
	else if( status.indexOf("Well") > 0 ) {
	    chooseWellDepth.setActive( newStatus.valData(0) );
	}
	else System.out.println(className+"::setNewStatus> Bias Status: "
				+ status + ", value = " + newStatus.valData(0));
    }
//-------------------------------------------------------------------------------

    void setDesiredVoltages()
    {
	//StringBuffer cmd = new StringBuffer("set all desired voltages for the Bias Panel \n");
	String errMsg = "";
	String Command[] = new String[NvBias*2];
	    
	// Read all the desired voltage values and convert them into Dac values
	for ( int i=0; i < NvBias; i++) {
	       
	    String voltName = jPanelVoltage[i].get_Name();
	    String voltValue = jPanelVoltage[i].BiasVoltPanel.desiredField.getText();

	    try {
		double Val = Double.parseDouble( voltValue ) ;
		// Convert from voltage to DAC value
		int DacVal = jPanelVoltage[i].computeDAC( Val );
		Command[2*i] = "Set Bias DAC";
		Command[2*i +1] = "DAC_SET_SINGLE_BIAS  " + i  + "  " + DacVal ;
		jPanelVoltage[i].BiasVoltPanel.setDesired( voltValue, DacVal );
	    }
	    catch ( NumberFormatException nfe ) {
		errMsg = "ERR: " + voltName + " has invalid (double) value: " + voltValue;
		break;
	    }
	}
	    
	if( errMsg.length() > 0 ) {
	    statusResponse.setText(errMsg);
	    return;
	} else {
	    UFStrings ufsDCcmds = new UFStrings("Set Bias DACs", Command);
	    commandAgent( ufsDCcmds );
	}

	readAll_action(false); //do not show action-response since it will overwrite.
    }
//-------------------------------------------------------------------------------
    /**
     * JPanelBias#commandAgent
     *@param agentRequest : UFStrings object containing request to send to DC agent.
     */

    public void commandAgent( UFStrings agentRequest )
    {
	commandAgent( agentRequest, true );
    }

    public void commandAgent( UFStrings agentRequest, boolean showActionResponse )
    {
	final boolean ShowAR = showActionResponse;
	final String panel = "JCI:JPanelBias:";
	final String action = agentRequest.name();
	agentRequest.rename( panel + action );
	final UFStrings AgentRequest = agentRequest;
	String msg = " sending " + action + " directive to DC agent...";
	System.out.println( panel + msg );

	if( showActionResponse ) {
	    statusAction.setText( msg );
	    statusResponse.setText(" waiting for response... ");
	}

	Runnable cmd_DC_agent = new Runnable() {
		public void run() {
		    try {
			String task = action + " >  ";
			UFStrings agentReply = DetectorPanel.sendRecvAgent( AgentRequest, panel+task );
			if( agentReply == null )
			    statusResponse.setText( task + "transaction ERROR: recvd NO response ?");
			else {
			    String statmsg = decodeAgentResponse( agentReply );

			    if( statmsg == null ) statmsg = "WARN: failed decoding response ?";
			    System.out.println( panel + task + statmsg );
			    if( ShowAR )
				statusResponse.setText( task + statmsg );
			}
		    }
		    catch( Exception x ) { x.printStackTrace(); }
		}
	    };

	SwingUtilities.invokeLater( cmd_DC_agent );
    }
//-------------------------------------------------------------------------------
    /**
     * JPanelBias#decodeAgentResponse
     *@param agentReply : UFStrings object containing reply from DC agent.
     */
    public String decodeAgentResponse( UFStrings agentReply )
    {
	String errMsg = "";
	boolean verbose = false;
	String msg = "DC agent response OK." ;
	    
	if( agentReply == null ) return("ERROR: agent did not respond ?");

	if( verbose ) System.out.println( agentReply.toString() );

	if( agentReply.valData(0).indexOf("Channel #") >= 0 ) {
	    for( int i=0; i < agentReply.numVals(); i++ )
		{
		    String param = agentReply.valData(i);
		    // Reteive the name of the element
		    String Vname = param.substring(param.indexOf(':') + 1, param.indexOf('=')).trim();
		    String Val = param.substring(param.indexOf('=') + 1, param.indexOf('(')).trim() ;
		    if (Val != null ) jPanelVoltage[i].setVolts(Val);

		    if( !Vname.equalsIgnoreCase( jPanelVoltage[i].Name ) )
			errMsg = "ERR: name mismatch " + Vname + " original: " + jPanelVoltage[i].Name ;
		}
	}
	else if( agentReply.valData(0).indexOf("simulation") >= 0 )
	    msg = "DC Agent in simulation mode ";
	else
	    errMsg = "ERR: unknown response from agent!";
	    
	if( errMsg.length() > 0 ) 
	    return errMsg;
	else 
	    return msg;
    }

} //end of Class JPanelBias.

//Title:        JPanelDetector class for Java Control Interface (JCI) using UFLib Protocol communications.
//Version:      (see rcsID)
//Copyright:    Copyright (c) 2003
//Author:       Frank Varosi and David Rashkin
//Company:      University of Florida
//Description:  for control and monitor of CanariCam infrared camera system.

package ufjci;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.io.*;
import java.net.*;
import java.util.*;
import javaUFProtocol.*;
import javaUFLib.*;

/**
 * Custom panel for communication and status monitor with Detector Control Agent.
 * Extends UFLibPanel, which contains basic components and agent communication methods.
 * @see javax.swing.JPanel
 * @authors Frank Varosi and David Rashkin 
 */

//===============================================================================
/**
 * Detector Control Tabbed pane
 */
public class JPanelDetector extends UFLibPanel
{
    public static final
	String rcsID = "$Name:  $ $Id: JPanelDetector.java,v 1.1 2004/11/01 19:04:00 amarin Exp $";

    // Text field ordering for input and adjusted physical MCE parameters:
//    public static final int FRMTIME = 0;
//    public static final int OBJTIME = 1;
//    public static final int SAVEFREQ = 2;
//    public static final int CHOPFREQ = 3;
//    public static final int CHOPDUTY = 4;
//    public static final int PREVALID = 5;
//    public static final int NODTIME = 6;
//    public static final int NODDUTY = 7;   //this and following are in adjusted parms. only
//    public static final int TOTALTIME = 8;
    
    public static final int FRMTIME = 0;
    public static final int OBJTIME = 1;
    public static final int NODTIME = 2;
    public static final int NODDELAY = 3;
    public static final int EFF_NOD = 4;
    public static final int EFF_FRM = 5;
    public static final int FRMCOADD = 6;
    public static final int NODSETTR = 7;
//    public static final int NODSETS = 8;
    public static final int PIXCLOCK = 8;
    
    boolean needConfigure = false;
    public static final int NinParms = 7;   // # of input parms
    public static final int NadjParms = 9; // # of adjusted parms
    public static final int NCTcnts = 12;   // # of CT counters
    // Text field ordering for MCE CT counters:
//    public static final int ctiPixClock = 0;
//    public static final int ctiSaveCoadds = 1;
//    public static final int ctiFrameCoadds = 2;
//    public static final int ctiChopSettleFrms = 4;
//    public static final int ctiChopCoadds = 3;
//    public static final int ctiPreValidChops = 5;
//    public static final int ctiNodSets = 7;
//    public static final int ctiSaveSets = 6;
//    public static final int ctiTotalFrames = 8;

    public static final int ctiFrameCoadds = 0;
    public static final int ctiNodSettleReads = 1;
    public static final int ctiNods = 2;
    public static final int ctiPixClock = 3;
    public static final int ctiTotalFrames = 4; 
    public static final int ctiFrameTime = 5;
    public static final int ctiOnSourceTime = 6;
    public static final int ctiElapsedTime = 7;
    public static final int ctiNodDwellTime = 8;
    public static final int ctiNodSettleTime = 9;
    public static final int ctiNodDutyCycle = 10;
    public static final int ctiFrameDutyCycle = 11;

    //map to hold PhysParmAdj objects:
    HashMap DCparamsMap = new HashMap( NadjParms + NCTcnts );

    //map to hold choose[*]Mode objects:
    HashMap DCmodesMap = new HashMap( 3 );

    //created by chooseModes():
    UFComboPanel chooseObsMode;
    UFComboPanel chooseReadoutMode;
    UFComboPanel chooseNodPattern;
    UFComboPanel chooseDataMode;
    UFComboPanel chooseAcqMode;
    UFComboPanel chooseNodConfirm; //nod confirm is no longer used, always true.

    //created by inputParameters():
    UFTextField[] PhysParmIn = new UFTextField[NinParms];   // Input Parameters

    //created by adjustedParameters():
    UFTextPanel[] PhysParmAdj = new UFTextPanel[NadjParms]; // Adjusted Parameters

    //created by CTcounters():
    UFTextPanel[] CTcounters = new UFTextPanel[NCTcnts];    // CT counters
    JCheckBox jCheckBoxCTLock = new JCheckBox("Locked",true);
    JButton applyCTcntsButton = new JButton("APPLY");

    //created by controlButtons():
    JButton computeButton = new JButton("COMPUTE parameters");
    JButton configureButton = new JButton("CONFIGURE > MCE");
    JButton efficButton = new JButton("Efficiency ?");
    JButton startButton = new JButton("START");
    JButton abortButton = new JButton("ABORT");
    JButton refreshButton = new JButton("Refresh Params");
    JButton testButton = new JButton("TEST");

    //created by moreButtons():
    JButton fileButton = new JButton("Read Meta-Config file");
    JButton queryMCE = new JButton("query MCE timing");
    JButton parkButton = new JButton("PARK");

    Font thinFont;
    //component of jPanelObsNote:
    UFTextField enterObsNote = new UFTextField("Obs. note for FITS header");

    //for pop-up window action of Efficiency button:
    ObsEfficiency efficWindow = new ObsEfficiency();
//-------------------------------------------------------------------------------
    /**
     * JPanelDetector default constructor.
     * Calls private method, createDetPanel(), which does all component creation.
     */
    public JPanelDetector()
    {
	super( "kepler", 52008 ); //creates the standard UFLib connectPanel.
	try {
	    createDetPanel();
	    connectToAgent();
	}
	catch (Exception e) { e.printStackTrace(); }
    }
//-------------------------------------------------------------------------------
    public JPanelDetector( String host )
    {
	super( host, 52008 ); //creates the standard UFLib connectPanel.
	try {
	    createDetPanel();
	    connectToAgent();
	}
	catch (Exception e) { e.printStackTrace(); }
    }
//-------------------------------------------------------------------------------
    public JPanelDetector( String host, int port )
    {
	super( host, port ); //creates the standard UFLib connectPanel.
	try {
	    createDetPanel();
	    connectToAgent();
	}
	catch (Exception e) { e.printStackTrace(); }
    }
//-------------------------------------------------------------------------------
    /**
     *Component initialization
     */
    private void createDetPanel() throws Exception
    {
	//thinFont is used by all the component sub-panel creation methods:
	thinFont = new Font( this.getFont().getName(),
			     this.getFont().getStyle(),
			     this.getFont().getSize()+2 );

	//this is currently not used but kept for future possibilites:
	JPanel jPanelObsNote = new JPanel();
	jPanelObsNote.setLayout( new RatioLayout() );
	jPanelObsNote.add("0.0,0.0;0.2,1.0", new JLabel("Note:"));
	jPanelObsNote.add("0.2,0.0;0.8,1.0", enterObsNote);

	// connectPanel (connectButton & hostPortPanel) are defined in super class UFLibPanel:
	// along with methods connectToAgent(), commandAgent(), etc.

	this.setLayout(new RatioLayout());
	this.add("0.00,0.02;1.00,0.14", paneChooseModes() );
	this.add("0.01,0.16;0.29,0.43", paneInputParams());
	this.add("0.02,0.61;0.27,0.13", connectPanel );
	this.add("0.31,0.16;0.37,0.54", paneAdjustedParams() );
	this.add("0.69,0.16;0.30,0.54", paneCTcounters() );
	this.add("0.00,0.78;1.00,0.12", paneControlButtons() );
	this.add("0.50,0.90;0.50,0.10", paneMoreButtons() );

	// DC agent Transaction info (components defined in super class UFLibPanel):
	this.add("0.01,0.90;0.49,0.05", statusAction);
	this.add("0.01,0.95;0.49,0.05", statusResponse);

    } //end of createDetPanel().
//-------------------------------------------------------------------------------
    /**
     * Create the choose modes UFComboPanels:
     */
    private JPanel paneChooseModes()
    {
//	String[] obsModes = {"chop-nod", "chop", "nod", "stare"};
	String[] obsModes = {"chop-nod", "stare"};
	chooseObsMode = new UFComboPanel("ObsMode:", obsModes, true ); //orientation Vertical=true.
	chooseObsMode.compareWithSelection(false);

	String[] readoutModes = {"S1R3", "S1", "S1R1", "S1R1_CR",
				 "S1R3_raw", "S1R1_raw","S1R1_CR_raw" };
	chooseReadoutMode = new UFComboPanel("ReadoutMode:", readoutModes, true );
	chooseReadoutMode.compareWithSelection(false);

	String[] DataModes = {"SAVE","SAVE+SEND","DISCARD"};
	chooseDataMode = new UFComboPanel("DataMode:", DataModes, true );

	String[] trueFalse = {"TRUE", "FALSE"};
	chooseNodConfirm = new UFComboPanel("NodConfirm:", trueFalse, true );

//	String[] NodPatterns = {"A-B-A-B","A-BB-A"};
//	chooseNodPattern = new UFComboPanel("NodPattern:", NodPatterns, true );

	String[] acqModes = {"normal","FAST"};
	chooseAcqMode = new UFComboPanel("AcqMode:", acqModes, true );
	chooseAcqMode.setAlarmValue("FAST"); //selection of FAST will be indicated with orange/red.

	DCmodesMap.put( chooseObsMode.name(), chooseObsMode );
	DCmodesMap.put( chooseReadoutMode.name(), chooseReadoutMode );
	DCmodesMap.put( chooseDataMode.name(), chooseDataMode );
//	DCmodesMap.put( chooseNodPattern.name(), chooseNodPattern );
	DCmodesMap.put( chooseNodConfirm.name(), chooseNodConfirm );
	DCmodesMap.put( chooseAcqMode.name(), chooseAcqMode );

	JPanel chooseModes= new JPanel();
	chooseModes.setLayout(new GridLayout(1,0));
	chooseModes.add( chooseObsMode );
	chooseModes.add( chooseReadoutMode );
//	chooseModes.add( chooseNodPattern ); //nod pattern must be CONFIGURED before START.
	chooseModes.add( chooseDataMode );
	chooseModes.add( chooseAcqMode );
	return chooseModes;
    }
//-------------------------------------------------------------------------------
    /**
     * Create the input parameter's UFTextFields:
     */
    private JPanel paneInputParams()
    {
	JLabel[] Labels = new JLabel[NinParms];
//	Labels[FRMTIME] = new JLabel("FrameTime (ms):");
//	Labels[SAVEFREQ] = new JLabel("SaveFrequency (Hz):");
//	Labels[OBJTIME] = new JLabel("OnSourceTime (min):");
//	Labels[CHOPFREQ] = new JLabel("ChopFrequency (Hz):");
//	Labels[CHOPDUTY] = new JLabel("SCSDutyCycle (max %):");
//	Labels[PREVALID] = new JLabel("preValidChopTime (sec):");
//	Labels[NODTIME] = new JLabel("NodDwellTime (sec):");

	Labels[FRMTIME]  =  new JLabel("FrameTime (ms):");
	Labels[OBJTIME]  =  new JLabel("OnSourceTime (min):");
	Labels[NODTIME]  =  new JLabel("NodDwellTime (sec):");
	Labels[NODDELAY] =  new JLabel("NodSettleTime (sec):");
	Labels[EFF_NOD]  =  new JLabel("NodDutyCycle (%):");
	Labels[EFF_FRM]  =  new JLabel("FrameDutyCycle (%):");
	Labels[FRMCOADD] =  new JLabel("FrameCoadds:");
//	Labels[NODSETTR] =  new JLabel("NodSettleReads:");
//	Labels[NODSETS]  =  new JLabel("Nods:");
//	Labels[PIXCLOCK] =  new JLabel("PixClock:");

//	PhysParmIn[FRMTIME]  = new UFTextField( Labels[FRMTIME].getText() );
//	PhysParmIn[SAVEFREQ] = new UFTextField( Labels[SAVEFREQ].getText() );
//	PhysParmIn[OBJTIME]  = new UFTextField( Labels[OBJTIME].getText() );
//	PhysParmIn[CHOPFREQ] = new UFTextField( Labels[CHOPFREQ].getText() );
//	PhysParmIn[CHOPDUTY] = new UFTextField( Labels[CHOPDUTY].getText() );
//	PhysParmIn[PREVALID] = new UFTextField( Labels[PREVALID].getText() );
//	PhysParmIn[NODTIME]  = new UFTextField( Labels[NODTIME].getText() );

      PhysParmIn[FRMTIME ]  = new UFTextField( Labels[FRMTIME].getText() );
      PhysParmIn[OBJTIME ]  = new UFTextField( Labels[OBJTIME].getText() );
      PhysParmIn[NODTIME ]  = new UFTextField( Labels[NODTIME].getText() );
      PhysParmIn[NODDELAY]  = new UFTextField( Labels[NODDELAY].getText() );
      PhysParmIn[EFF_NOD ]  = new UFTextField( Labels[EFF_NOD].getText() );
      PhysParmIn[EFF_FRM ]  = new UFTextField( Labels[EFF_FRM].getText() );
      PhysParmIn[FRMCOADD]  = new UFTextField( Labels[FRMCOADD].getText() );
//      PhysParmIn[NODSETTR]  = new UFTextField( Labels[NODSETTR].getText() );
//      PhysParmIn[NODSETS ]  = new UFTextField( Labels[NODSETS].getText() );
//      PhysParmIn[PIXCLOCK]  = new UFTextField( Labels[PIXCLOCK].getText() );

	JPanel inputParams = new JPanel();
	inputParams.setLayout(new GridLayout(0,2));
	inputParams.setBorder(new EtchedBorder(0));

	JLabel RightLabel = new JLabel("Input ",JLabel.RIGHT);
	JLabel LeftLabel = new JLabel("Parameters",JLabel.LEFT);

	RightLabel.setFont(thinFont);
	LeftLabel.setFont(thinFont);
	inputParams.add(RightLabel);
	inputParams.add(LeftLabel);

	for (int i=0; i<NinParms; i++) {
	    inputParams.add( Labels[i] );
	    inputParams.add( PhysParmIn[i] );
	}

	return inputParams;
    }
//-------------------------------------------------------------------------------
    /**
     * Create the adjusted parameter's UFTextPanels:
     */
    private JPanel paneAdjustedParams()
    {
//	PhysParmAdj[FRMTIME]   = new UFTextPanel("FrameTime (ms):");
//	PhysParmAdj[SAVEFREQ]  = new UFTextPanel("SaveFrequency (Hz):");
//	PhysParmAdj[OBJTIME]   = new UFTextPanel("OnSourceTime (min):");
//	PhysParmAdj[CHOPFREQ]  = new UFTextPanel("ChopFrequency (Hz):");
//	PhysParmAdj[CHOPDUTY]  = new UFTextPanel("ChopDutyCycle (%):");
//	PhysParmAdj[NODTIME]   = new UFTextPanel("NodDwellTime (sec):");
//	PhysParmAdj[NODDUTY]   = new UFTextPanel("NodDutyCycle (%):");
//	PhysParmAdj[PREVALID]  = new UFTextPanel("preValidChopTime (sec):");
//	PhysParmAdj[TOTALTIME] = new UFTextPanel("ElapsedTime (min):");

      PhysParmAdj[FRMTIME]  = new UFTextPanel("FrameTime (ms):");
      PhysParmAdj[OBJTIME]  = new UFTextPanel("OnSourceTime (min):");
      PhysParmAdj[NODTIME]  = new UFTextPanel("NodDwellTime (sec):");
      PhysParmAdj[NODDELAY] = new UFTextPanel("NodSettleTime (sec):");
      PhysParmAdj[EFF_NOD]  = new UFTextPanel("NodDutyCycle (%):");
      PhysParmAdj[EFF_FRM]  = new UFTextPanel("FrameDutyCycle (%):");
      PhysParmAdj[FRMCOADD] = new UFTextPanel("FrameCoadds:");
      PhysParmAdj[NODSETTR] = new UFTextPanel("NodSettleReads:");
//      PhysParmAdj[NODSETS]  = new UFTextPanel("Nods:");
      PhysParmAdj[PIXCLOCK] = new UFTextPanel("PixClock:");


	JPanel adjustedParams = new JPanel();
	adjustedParams.setLayout(new GridLayout(0,1));
	adjustedParams.setBorder(new EtchedBorder(0));
	JPanel title = new JPanel();
	title.setLayout(new RatioLayout());
	JLabel label = new JLabel("Adjusted Parameters:");
	label.setFont(thinFont);
	title.add("0.00,0.0;0.41,1.0", label);
	label = new JLabel("Desired");
	label.setFont(thinFont);
	title.add("0.42,0.0;0.28,1.0", label);
	label = new JLabel("Active");
	label.setFont(thinFont);
	title.add("0.70,0.0;0.28,1.0", label);
	adjustedParams.add( title );

	for (int i=0; i<NadjParms; i++) {
	    adjustedParams.add( PhysParmAdj[i] );
	    DCparamsMap.put( PhysParmAdj[i].name(), PhysParmAdj[i] );
	}

	return adjustedParams;
    }
//-------------------------------------------------------------------------------
    /**
     * Create the CT counter parameter's UFTextPanels:
     */
    private JPanel paneCTcounters()
    {
//	CTcounters[ctiSaveCoadds] =     new UFTextPanel("SaveCoadds:");
//	CTcounters[ctiFrameCoadds] =    new UFTextPanel("FrameCoadds:");
//	CTcounters[ctiChopSettleFrms] = new UFTextPanel("ChopSettleFrms:");
//	CTcounters[ctiChopCoadds] =     new UFTextPanel("ChopCoadds:");
//	CTcounters[ctiSaveSets] =       new UFTextPanel("SaveSets:");
//	CTcounters[ctiNodSets] =        new UFTextPanel("NodSets:");
//	CTcounters[ctiPixClock] =       new UFTextPanel("PixClock:");
//	CTcounters[ctiPreValidChops] =  new UFTextPanel("PreValidChops:");
//	CTcounters[ctiTotalFrames] =    new UFTextPanel("TotalFrames:");

	CTcounters[ctiFrameCoadds] =	 new UFTextPanel("FrameCoadds:");
	CTcounters[ctiNodSettleReads] =	 new UFTextPanel("NodSettleReads:");
	CTcounters[ctiNods] = new UFTextPanel("Nods:");
	CTcounters[ctiPixClock] =	 new UFTextPanel("PixClock:");
	CTcounters[ctiTotalFrames] =	 new UFTextPanel("TotalFrames:");
	CTcounters[ctiFrameTime] =	 new UFTextPanel("FrameTime:");
	CTcounters[ctiOnSourceTime] =  new UFTextPanel("OnSourceTime:");
	CTcounters[ctiElapsedTime] =	 new UFTextPanel("ElapsedTime:");
	CTcounters[ctiNodDwellTime] =	 new UFTextPanel("NodDwellTime:");
	CTcounters[ctiNodSettleTime] =	 new UFTextPanel("NodSettleTime:");
	CTcounters[ctiNodDutyCycle] =  new UFTextPanel("NodDutyCycle:");
	CTcounters[ctiFrameDutyCycle] =	 new UFTextPanel("FrameDutyCycle:");

	JPanel CTpanel = new JPanel();
	CTpanel.setLayout(new GridLayout(0,1));
	CTpanel.setBorder(new EtchedBorder(0));
	JPanel applyCTcnts = new JPanel();
	applyCTcnts.setLayout(new RatioLayout());
	applyCTcntsButton.setDefaultCapable(false);
	applyCTcntsButton.setEnabled(false);
	JLabel CTcLabel = new JLabel("MCE-CT counters:");	
	CTcLabel.setFont(thinFont);
	applyCTcnts.add("0.00,0.0;0.41,1.0", CTcLabel );
	applyCTcnts.add("0.42,0.0;0.28,1.0", jCheckBoxCTLock );
	applyCTcnts.add("0.70,0.0;0.28,1.0", applyCTcntsButton );
	CTpanel.add( applyCTcnts );
	Color grey = new Color (240,230,220);

	for (int i=0; i<NCTcnts; i++) {
	    CTcounters[i].desiredField.setBackground( grey );
	    CTcounters[i].desiredField.setEditable(false);
	    CTpanel.add( CTcounters[i] );
	    DCparamsMap.put( CTcounters[i].name(), CTcounters[i] );
	}

	applyCTcntsButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) { action_applyCTcntsButton(e); } });

	jCheckBoxCTLock.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    Color color;
		    boolean editable;
		    if (jCheckBoxCTLock.isSelected()) {
			color = new Color(240,230,220);
			editable = false;
			applyCTcntsButton.setEnabled(false);
		    }
		    else {
			color = new Color(255,255,255);
			editable = true;
			applyCTcntsButton.setEnabled(true);
		    }
		    for (int i=0; i<NCTcnts; i++) {
			CTcounters[i].desiredField.setBackground(color);
			CTcounters[i].desiredField.setEditable(editable);
		    }
		}
	    });

	return CTpanel;
    }
//-------------------------------------------------------------------------------
    /**
     * Create the control buttons panel
     */
    private JPanel paneControlButtons()
    {
	computeButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) { action_computeButton(e); } });

	configureButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) { action_configureButton(e); } });

	efficButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) { action_efficButton(e); } });

	startButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) { action_Obs_Start(e); } });

	abortButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) { action_Obs_Abort(e); } });

	testButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) { action_testButton(); } });

	refreshButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) { getNewParams(clientName); } });


	startButton.setBackground(Color.green);
	abortButton.setBackground(Color.red);
	abortButton.setForeground(Color.white);
	configureButton.setBackground(Color.cyan);

	JPanel controlButtons = new JPanel();
	controlButtons.setLayout( new FlowLayout() );
	controlButtons.add( computeButton );
	controlButtons.add( configureButton );
	controlButtons.add( efficButton );
	controlButtons.add( startButton );
	controlButtons.add( abortButton );
	controlButtons.add( refreshButton );
	controlButtons.add( testButton );
	return controlButtons;
    }
//-------------------------------------------------------------------------------
    /**
     * Create more control buttons panel
     */
    private JPanel paneMoreButtons()
    {
	parkButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) { action_parkButton(e); } });

	fileButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) { action_fileButton(e); } });

	queryMCE.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) { action_queryMCE(); } });

	JPanel moreButtons = new JPanel();
	moreButtons.setLayout( new FlowLayout() );
	moreButtons.add( fileButton );
	moreButtons.add( queryMCE );
	moreButtons.add( parkButton );
	return moreButtons;
    }
//-------------------------------------------------------------------------------

    boolean setDefaultInputs()
    {
	//set input text fields to default values.
	boolean defaultSet = false;

	for (int i=0; i<NinParms; i++) {
	    String parm = PhysParmIn[i].getText();
	    if( parm.equals("-1.00") || parm.equals("string") || parm.equals("") ) {
		defaultSet = true;
//		if( i == CHOPDUTY )
//		    PhysParmIn[i].setNewState("90");
//		else {
		    String ppa = PhysParmAdj[i].getDesired();
		    if( ppa.length() > 0 && !ppa.equals("0") ) {
			int nc = ppa.length();
			if( nc > 5 ) nc = 5;
			PhysParmIn[i].setNewState( ppa.substring(0,nc) );
		    } else {
			if     ( i == FRMTIME  )  PhysParmIn[i].setNewState("1");
			else if( i == OBJTIME  )  PhysParmIn[i].setNewState("1");
			else if( i == NODTIME  )  PhysParmIn[i].setNewState("1");
			else if( i == NODDELAY )  PhysParmIn[i].setNewState("1");
			else if( i == EFF_NOD  )  PhysParmIn[i].setNewState("1");
			else if( i == EFF_FRM  )  PhysParmIn[i].setNewState("1");
			else if( i == FRMCOADD )  PhysParmIn[i].setNewState("1");
//			else if( i == NODSETTR )  PhysParmIn[i].setNewState("1");
//			else if( i == NODSETS  )  PhysParmIn[i].setNewState("1");
//			else if( i == PIXCLOCK )  PhysParmIn[i].setNewState("1");
			else PhysParmIn[i].setNewState("1");
		    }
//		}
	    }
	}
	return defaultSet;
    }
//-------------------------------------------------------------------------------
    /**
     * JPanelDetector#compute button action performed
     * Directs the Detector Control Agent to compute adjusted physical params
     * based on user input parameters and electronic/mechanical limitations of instrument.
     *@param e not used
     */
    void action_computeButton(ActionEvent e)
    {
	if( setDefaultInputs() ) return;

	for (int i=0; i<NinParms; i++) {
	    if( PhysParmIn[i].getText().trim().equals("") ) {
		JOptionPane.showMessageDialog(null,"Need to specify all Input Parameters!",
					      "ERROR", JOptionPane.ERROR_MESSAGE);
		return;
	    }
	}

	if( needObsModeSelect() ) return;

	String[] DCparams = new String[2*NinParms+6];
	DCparams[0] = "PARM";
	DCparams[1] = "HARD";
	DCparams[2] = "ObsMode";
	DCparams[3] = chooseObsMode.getSelection();
	if( DCparams[3].length() < 3 ) DCparams[3] = chooseObsMode.getActive();
	DCparams[4] = "ReadoutMode";
	DCparams[5] = chooseReadoutMode.getSelection();
	if( DCparams[5].length() < 2 ) DCparams[5] = chooseReadoutMode.getActive();

	for (int i=0; i<NinParms; i++) {
	    DCparams[2*i+6] = PhysParmIn[i].name();
	    DCparams[2*i+7] = PhysParmIn[i].getText();
	    PhysParmIn[i].setNewState();
	}

	UFStrings ufsDCparams = new UFStrings("COMPUTE desired physical params.", DCparams);
	commandAgent( ufsDCparams );

	//indicate to method getNewParams() that this JCI has params,
	// because the statusReceiver method of UFobsMonitor class
	// will get msg from DC agent to refresh params (other JCIs will refresh).
	gotParams = true;
	needConfigure = true;
    }//end of action_computeButton
//-------------------------------------------------------------------------------
    /**
     * JPanelDetector#needCompute
     */
    boolean needCompute()
    {
	boolean needComp = false;

	for (int i=0; i<NinParms; i++) {
	    String prev_text = PhysParmIn[i].prev_text.trim();
	    String curr_text = PhysParmIn[i].getText().trim();
	    //if( prev_text.equals("") || curr_text.equals("") ) needComp = true;
	    //if( Float.parseFloat( prev_text ) != Float.parseFloat( curr_text ) ) needComp = true;
	}

	String newObsMode = chooseObsMode.getSelection();
	String desObsMode = chooseObsMode.getDesired();
	if( newObsMode.length() > 0 && !desObsMode.equals( newObsMode ) ) needComp = true;

	String newReadMode = chooseReadoutMode.getSelection();
	String desReadMode = chooseReadoutMode.getDesired();
	if( newReadMode.length() > 0 && !desReadMode.equals( newReadMode ) ) needComp = true;

	if( needComp ) {
	    if( JOptionPane.NO_OPTION ==
		JOptionPane.showConfirmDialog(null,"Input parameters changed: continue without COMPUTE ?",
					      "Warning",
					      JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE) )
		{
		    JOptionPane.showMessageDialog(null,"Input parameters changed:  press COMPUTE button !",
						  "ERROR", JOptionPane.ERROR_MESSAGE);
		    return needComp;
		}
	    else return false;
	}
	else return false;
    }
//-------------------------------------------------------------------------------
    /**
     * JPanelDetector#needConfigure
     */
    boolean needConfigure()
    {
	boolean needConf = false;
	needConf = true;

//	String newNodPattern = chooseNodPattern.getSelection();
//	String desNodPattern = chooseNodPattern.getDesired();
//	if( newNodPattern.length() > 0 && !desNodPattern.equals( newNodPattern ) ) needConf = true;

//	if( needConf )
//	    JOptionPane.showMessageDialog(null,"Nod Pattern changed:  press CONFIGURE button !",
//					  "ERROR", JOptionPane.ERROR_MESSAGE);
	return needConf;
    }
//-------------------------------------------------------------------------------
    /**
     * JPanelDetector#needObsModeSelect
     */
    boolean needObsModeSelect()
    {
	boolean needSelect = false;

	String newObsMode = chooseObsMode.getSelection();
	String actObsMode = chooseObsMode.getActive();
	if( newObsMode.equals("") && actObsMode.equals("") ) needSelect = true;

	String newReadMode = chooseReadoutMode.getSelection();
	String actReadMode = chooseReadoutMode.getActive();
	if( newReadMode.equals("") && actReadMode.equals("") ) needSelect = true;

	if( needSelect ) {
	    JOptionPane.showMessageDialog(null,"Need to select Obs mode and Readout mode !",
					  "ERROR", JOptionPane.ERROR_MESSAGE);
	    return true;
	} else
	    return false;
    }
//-------------------------------------------------------------------------------
    /**
     *JPanelDetector#configure button action performed
     * send ACQ => CONFIG directive, with current NodPattern, to det. control. agent
     * instructing DC agent to send configuration to MCE4 and FrameAcqServer.
     *@param e not used
     */
    void action_configureButton(ActionEvent e)
    {
	if( needObsModeSelect() ) return;
//	if( needNodPatternSelect() ) return;
	if( needCompute() ) return;

//	String[] DCconfig = new String[4];
//	DCconfig[0] = "ACQ";
//	DCconfig[1] = "CONFIG";
//	DCconfig[2] = "NodPattern";
//	String nodPattern = chooseNodPattern.getSelection();
//	if( nodPattern.length() < 6 ) nodPattern = chooseNodPattern.getActive();
//	DCconfig[3] = nodPattern;

//	UFStrings ufsDCconfig = new UFStrings("CONFIGURE  MCE & FAS", DCconfig);
//	commandAgent( ufsDCconfig );

	//indicate to method getNewParams() that this JCI has params,
	// because the statusReceiver method of UFobsMonitor class
	// will get msg from DC agent to refresh params (other JCIs will refresh).
	gotParams = true;
	needConfigure = false;
    }
//-------------------------------------------------------------------------------
    /**
     * JPanelDetector#needNodPatternSelect
     */
    boolean needNodPatternSelect()
    {
	String newNodPattern = chooseNodPattern.getSelection();
	String actvNodPattern = chooseNodPattern.getActive();

	if( actvNodPattern.length() < 6 && newNodPattern.length() < 6 )
	    {
		JOptionPane.showMessageDialog(null,"Need to select Nod Pattern !",
					      "ERROR", JOptionPane.ERROR_MESSAGE);
		return true;
	    }
	else return false;
    }
//-------------------------------------------------------------------------------
    /**
     * JPanelDetector#needNodConfirmSelect
     */
    boolean needNodConfirmSelect()
    {
	String newNodConfirm = chooseNodConfirm.getSelection();
	String actvNodConfirm = chooseNodConfirm.getActive();

	if( actvNodConfirm.length() < 4 && newNodConfirm.length() < 4 ) //must be either TRUE or FALSE.
	    {
		JOptionPane.showMessageDialog(null,"Need to select Nod Confirm status !",
					      "ERROR", JOptionPane.ERROR_MESSAGE);
		return true;
	    }
	else return false;
    }
//-------------------------------------------------------------------------------
    /**
     * JPanelDetector#start button action performed
     * Checks if COMPUTE or CONFIGURE needs to be pressed,
     *  if all ok, sends observation START directive to detector controller agent.
     *@param e not used
     */
    void action_Obs_Start(ActionEvent e)
    {
	if( needObsModeSelect() ) return;
	if( needCompute() ) return;
	if( needConfigure() ) return;

	if( needConfigure ) {
	    JOptionPane.showMessageDialog(null,"Press CONFIGURE button to send new configuration!",
					  "ERROR", JOptionPane.ERROR_MESSAGE);
	    return;
	}

	if( checkArchvFileStatus() )
	    {
		Vector cmdStrings = new Vector();
		cmdStrings.add("ACQ");
		cmdStrings.add("START");

		String AcqMode = chooseAcqMode.getSelection();
		if( AcqMode.length() > 3 ) {
		    cmdStrings.add("AcqMode");
		    cmdStrings.add( AcqMode );
		}

		String DataMode = chooseDataMode.getSelection();
		if( DataMode.length() > 3 ) {
		    cmdStrings.add("DataMode");
		    cmdStrings.add( DataMode );
		}

		String Comment = enterObsNote.getText();
		if( Comment.length() > 0 ) {
		    cmdStrings.add("Comment");
		    cmdStrings.add( Comment );
		}

		String[] DCstart = new String[ cmdStrings.size() ];
		cmdStrings.copyInto( DCstart );
		UFStrings ufsDCstart = new UFStrings("START observation", DCstart);
		commandAgent( ufsDCstart );
	    }
    } //end of action_Obs_Start
//-------------------------------------------------------------------------------
    /**
     *JPanelDetector#checkArchvFileStatus
     * Gets the current Archive File status, and if TRUE returns true.
     */
    boolean checkArchvFileStatus()
    {
	String DataMode = chooseDataMode.getActive();
	String newDataMode = chooseDataMode.getSelection();
	if( newDataMode.length() > 3 ) DataMode = newDataMode;

	if( DataMode.length() < 4 )
	    {
		JOptionPane.showMessageDialog(null,"Need to select Data Mode !",
					      "ERROR", JOptionPane.ERROR_MESSAGE);
		return false;
	    }

	if( DataMode.trim().toUpperCase().equals("SAVE") )
	    return true;
	else {
	    if( JOptionPane.NO_OPTION ==
		JOptionPane.showConfirmDialog(null,"Data will NOT be saved, START obs anyway ?","Warning",
					      JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE) )
		return false;
	    else
		return true;
	}
    }
//-------------------------------------------------------------------------------
    /**
     * JPanelDetector#abort button action performed
     * send ACQ => ABORT directive to detector control agent.
     *@param e not used
     */
    void action_Obs_Abort(ActionEvent e)
    {
	if(JOptionPane.showConfirmDialog(null,"ABORT the observation ?") == JOptionPane.YES_OPTION)
	    {
		String[] DCabort = new String[2];
		DCabort[0] = "ACQ";
		DCabort[1] = "ABORT";
		UFStrings ufsDCabort = new UFStrings("ABORT observation", DCabort);
		commandAgent( ufsDCabort );
	    }
    }
//-------------------------------------------------------------------------------
    /**
     * JPanelDetector#park button action performed
     * send SETUP => PARK directive to detector control agent.
     * which causes it to disconnect from other agents,
     * make sure FITS file is closed (if obs is done), and power OFF the det.bias voltages.
     *@param e not used
     */
    void action_parkButton(ActionEvent e)
    {
	if(JOptionPane.showConfirmDialog(null,"Power OFF the detector Bias ?") == JOptionPane.YES_OPTION)
	    {
		String[] DCpark = new String[2];
		DCpark[0] = "SETUP";
		DCpark[1] = "PARK";
		UFStrings ufsDCpark = new UFStrings("PARK MCE4", DCpark);
		commandAgent( ufsDCpark );
	    }
    }
//-------------------------------------------------------------------------------
    /**
     * JPanelDetector#file button action performed
     * send SETUP => FILE directive to Detector Control agent,
     * which causes it to re-connect to Data Acquisition Server and re-send the Pixel Maps,
     * and also causes the DC agent to re-connect to other agents.
     *@param e not used
     */
    void action_fileButton(ActionEvent e)
    {
	if( JOptionPane.showConfirmDialog(null, "Make DC Agent re-Read Meta-Config file ?")
	    == JOptionPane.YES_OPTION )
	    {
		String[] DCfile = new String[2];
		DCfile[0] = "SETUP";
		DCfile[1] = "FILE";
		UFStrings ufsDCfile = new UFStrings("Read Meta-Config file", DCfile);
		commandAgent( ufsDCfile );
	    }
    }
//-------------------------------------------------------------------------------
    /**
     * JPanelDetector#queryMCE button action performed
     * send ACQ => TEST directive to Detector Control agent,
     * which forwards TEST to data Acq.Server and MCE4.
     */
    void action_testButton()
    {
	String[] DCtest = new String[2];
	DCtest[0] = "ACQ";
	DCtest[1] = "TEST";
	UFStrings ufsDCtest = new UFStrings("TEST", DCtest);
	commandAgent( ufsDCtest );
    }
//-------------------------------------------------------------------------------
    /**
     * JPanelDetector#queryMCE button action performed
     * send SETUP => TIMING directive to Detector Control agent,
     * which causes it to re-connect to Data Acquisition Server and re-send the Pixel Maps,
     * and if OK, causes it to re-query the MCE for basic frame times.
     */
    void action_queryMCE()
    {
	if( JOptionPane.showConfirmDialog(null,
	      "query MCE for basic frame times (takes 2 mins) ?") == JOptionPane.YES_OPTION )
	    {
		String[] DCtiming = new String[2];
		DCtiming[0] = "SETUP";
		DCtiming[1] = "TIMING";
		UFStrings ufsDCtiming = new UFStrings("query-MCE-timing", DCtiming);
		commandAgent( ufsDCtiming );
	    }
    }
//-------------------------------------------------------------------------------
    /**
     * JPanelDetector# apply CT counters button action performed
     *@param e not used
     */
    void action_applyCTcntsButton(ActionEvent e)
    {
	String[] DCparams = new String[2*NCTcnts+2];
	DCparams[0] = "PARM";
	DCparams[1] = "HARDWARE";
	DCparams[2] = "ReadoutMode";
	DCparams[3] = chooseReadoutMode.getSelection();
	if( DCparams[3].length() < 2 ) DCparams[3] = chooseReadoutMode.getActive();

	for (int i=0; i<NCTcnts-1; i++) //note: last CT-counter is not sent (not needed).
	    {
		DCparams[2*i+4] = CTcounters[i].name();
		DCparams[2*i+5] = CTcounters[i].getDesired();
	    }

	UFStrings ufsDCparams = new UFStrings("LOAD desired Hardware params.", DCparams);
	commandAgent( ufsDCparams );
	needConfigure = true;
    }
//-------------------------------------------------------------------------------
    /**
     * JPanelDetector#decodeAgentResponse
     *@param agentReply : UFStrings object containing reply from DC agent.
     *@retval = String, either OK or ERR.
     *
     * Parses reponse for DESIRED/ACTIVE params/modes and
     * uses HashMaps DCparamsMap and DCmodesMap to update corresponding GUI objects.
     */
    public String decodeAgentResponse( UFStrings agentReply )
    {
	if( agentReply == null ) return("ERROR: agent did not respond ?");
	boolean verbose = false;
	if( verbose ) System.out.println( agentReply.toString() );
	boolean Desired = false;
	boolean Active = false;
	boolean status = false;

	for( int i=0; i<agentReply.numVals(); i++ )
	    {
		String paramVal = agentReply.valData(i);
		int eqpos = 0;

		if( paramVal.toUpperCase().indexOf("DESIRED") >= 0 ) {
		    Desired = true;
		    if( paramVal.toUpperCase().indexOf("ACTIVE") >= 0 )
			Active = true;
		    else
			Active = false;
		}
		else if( paramVal.toUpperCase().indexOf("ACTIVE") >= 0 ) {
		    Active = true;
		    Desired = false;
		}

		if( (eqpos = paramVal.indexOf("==")) > 0 )
		    {
			String param = paramVal.substring(0,eqpos).trim();
			String pVal = paramVal.substring( eqpos+2, paramVal.length() ).trim();
			if( verbose ) System.out.println( i + ":" + param + "=" + pVal );
			UFTextPanel paramField = (UFTextPanel)DCparamsMap.get( param );

			if( paramField != null ) {
			    if( Desired ) paramField.setDesired( pVal );
			    if( Active )  paramField.setActive( pVal );
			    status = true;
			}
			else {
			    UFComboPanel modeSel = (UFComboPanel)DCmodesMap.get( param );
			    if( modeSel != null ) {
				if( Desired ) modeSel.setDesired( pVal );
				if( Active )  modeSel.setActive( pVal );
				status = true;
			    }
			}
		    }
	    }

	if( agentReply.name().toUpperCase().indexOf("ERR") >= 0 )
	    {
		if( agentReply.numVals() > 0 )
		    return agentReply.valData(0);
		else
		    return("ERR: invalid DC agent response ?");
	    }
	else {
	    if( status )
		return("DC agent response OK.");
	    else
		return("ERR: invalid DC agent response ?");
	}
    }
//-------------------------------------------------------------------------------
    /**
     *Pops up the efficiency window using class ObsEfficiency
     *@param e not used
     */
    void action_efficButton(ActionEvent e)
    {
	Point loc = getLocation();
	Dimension frmSize = new Dimension( 250, 250 );
	efficWindow.setSize( frmSize );
	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	efficWindow.setLocation( (int)(screenSize.getWidth() - frmSize.getWidth()*2),
				     (int)(screenSize.getHeight() - frmSize.getHeight()) - 100 );
	efficWindow.show();
	efficWindow.setState( Frame.NORMAL );
	efficWindow.action_compEffButton();
    }
//===============================================================================
    /**
     * Handles the detector efficiency dialog box
     */
    public class ObsEfficiency extends JFrame
    {
	UFTextField frameDC = new UFTextField("frameDC");
	UFTextField chopDC = new UFTextField("chopDC");
	UFTextField nodDC = new UFTextField("nodDC");
	UFTextField photDC = new UFTextField("");
	UFTextField srcDC = new UFTextField("");
	JPanel labelpanel = new JPanel();
	JPanel textpanel = new JPanel();
	JPanel buttonpanel = new JPanel();
	JButton compEffButton = new JButton ("Compute Efficiency");
	JButton doneButton = new JButton ("DONE");
//-------------------------------------------------------------------------------
	/**
	 *Constructor
	 */
	ObsEfficiency() {
	    try  {
		createEffBox();
	    }
	    catch(Exception x) { x.printStackTrace(); }
	}
//-------------------------------------------------------------------------------
	/**
	 *Component initialization
	 */
	private void createEffBox()
	{
	    doneButton.addActionListener(new ActionListener()
		{ public void actionPerformed(ActionEvent e) { action_doneButton(); } });

	    compEffButton.addActionListener(new ActionListener()
		{ public void actionPerformed(ActionEvent e) { action_compEffButton(); } });

	    labelpanel.setLayout(new GridLayout(0,1,0,10));
	    textpanel.setLayout(new GridLayout(0,1,0,10));
	    buttonpanel.setLayout(new FlowLayout());
	    this.getContentPane().setLayout(new BorderLayout());
	    frameDC.setEditable(false);
	    frameDC.setColumns(7); 
	    frameDC.setNewState("100");
	    nodDC.setEditable(false);
	    nodDC.setColumns(7);
	    chopDC.setEditable(false);
	    chopDC.setColumns(7);
	    photDC.setBackground(Color.white);
	    photDC.setEditable(false);
	    photDC.setColumns(7);
	    srcDC.setBackground(Color.white);
	    srcDC.setEditable(false);
	    srcDC.setColumns(7);
	    labelpanel.add(new JLabel(""));
	    labelpanel.add(new JLabel("Frame Duty Cycle (%):"));
	    labelpanel.add(new JLabel("Chop Duty Cycle (%):"));
	    labelpanel.add(new JLabel("Nod Duty Cycle (%):"));
	    labelpanel.add(new JLabel("Photon Duty Cycle (%):"));
	    labelpanel.add(new JLabel("Source Duty Cycle (%):"));
	    labelpanel.add(new JLabel(""));
	    textpanel.add(new JLabel(""));
	    textpanel.add(frameDC);
	    textpanel.add(chopDC);
	    textpanel.add(nodDC);
	    textpanel.add(photDC);
	    textpanel.add(srcDC);
	    textpanel.add(new JLabel(""));
	    buttonpanel.add(compEffButton);
	    buttonpanel.add(doneButton);
	    this.setTitle("Detector Efficiency");
	    this.getContentPane().add(labelpanel,BorderLayout.WEST);
	    this.getContentPane().add(textpanel,BorderLayout.EAST);
	    this.getContentPane().add(buttonpanel,BorderLayout.SOUTH);
	    doneButton.grabFocus();
	    this.setSize(new Dimension(250,250));
	}
//--------------------------------------------------------

	void action_doneButton() { this.dispose(); }

//--------------------------------------------------------
	void action_compEffButton()
	{
	    try {
//		chopDC.setNewState( PhysParmAdj[CHOPDUTY].getDesired() );
//		nodDC.setNewState( PhysParmAdj[NODDUTY].getDesired() );
		nodDC.setNewState( PhysParmAdj[EFF_NOD].getDesired() );
		float pDC = 100
		    * Float.parseFloat( frameDC.getText() )/100
		    * Float.parseFloat( chopDC.getText() )/100
		    * Float.parseFloat( nodDC.getText() )/100;
		String duty = Float.toString( pDC );
		photDC.setText( duty.substring(0,4) );
		duty = Float.toString( pDC/2 );
		srcDC.setText( duty.substring(0,4) );
	    }
	    catch(Exception x) { x.printStackTrace(); System.out.println( x.toString() ); }
	}
    } // end of class ObsEfficiency.

} //end of class JPanelDetector

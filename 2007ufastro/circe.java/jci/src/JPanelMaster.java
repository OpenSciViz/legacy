//Title:        JPanelMaster for Java Control Interface (JCI) using UFLib Protocol communications.
//Version:      (see rcsID)
//Copyright:    Copyright (c) 2004
//Author:       Frank Varosi, David Rashkin and Antonio Marin-Franch
//Company:      University of Florida
//Description:  for control and monitor of CIRCE near-infrared camera system for GTC.

package ufjci;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.io.*;
import java.util.*;
import javaUFProtocol.*;
import javaUFLib.*;

//===============================================================================
/**
 * Master Panel tabbed pane:
 * for high-level configuration of instrument optics and observation parameters,
 * and start/abort of an obs.
 */
public class JPanelMaster extends UFLibPanel
{
    public static final
	String rcsID = "$Name:  $ $Id: JPanelMaster.java,v 1.1 2004/11/18 20:40:19 amarin Exp $";

    JCIMotor[] jciMotors;
    JPanelDetector DetectorPanel;
    JCImoreObsParms MoreObsParms = new JCImoreObsParms(); //this class is a JFrame (window).
    JCIopticalPath showOpticalPath;

    //instrumentSetup:
    UFComboPanel chooseCameraMode;
    JCIMotorGUI chooseSlit;
    JCIMotorGUI chooseFilter;
    JCIMotorGUI chooseHWP;
    JCIMotorGUI chooseHWPangle;
    JCIMotorGUI chooseGrism;
    
    //obsParams:
    UFComboPanel chooseDataMode;
    UFComboPanel chooseWaterVapour;
    UFComboPanel chooseAirmass;
    UFTextPanel enterOnSrcTime;
    UFTextPanel enterOffsetPosAngle;

    //Revisar:
    LinkedHashMap chosenModesMap = new LinkedHashMap(4);
    LinkedHashMap chosenOpticsMap = new LinkedHashMap(5);
    LinkedHashMap enteredParmsMap = new LinkedHashMap(2);

    boolean obsInProgress = false;
    boolean needConfigure = false;

    float selecOpticsThroughput;
    float configOpticsThroughput;

    // Obs. note for FITS header:
    UFTextField enterObsNote = new UFTextField("Obs. note for FITS header");

    // DC agent Transaction info:
    UFLabel statusAction   = new UFLabel("Action:");
    UFLabel statusResponse = new UFLabel("Response:");
//------------------------------------------------------------------------------------------

    public JPanelMaster( JPanelDetector DetectorPanel, JCIMotor[] jciMotors )
    {
	this.jciMotors = jciMotors;
	this.DetectorPanel = DetectorPanel;

	JButton buttonMoreObsParms = new JButton("More Obs. Parameters");
	JButton buttonOpticalPath =  new JButton("Show Optical Configuration");
	JButton buttonApplyConfig =  new JButton("APPLY configuration");

	buttonApplyConfig.setBackground(Color.cyan);
	buttonApplyConfig.addActionListener(new ActionListener()
	    { public void actionPerformed(ActionEvent e) { action_ApplyConfig(); } });

	buttonOpticalPath.setBackground(new Color(200,255,200));
	buttonOpticalPath.addActionListener(new ActionListener()
	    { public void actionPerformed(ActionEvent e) { action_ShowOpticalPath(); } });

	buttonMoreObsParms.setBackground(new Color(200,255,200));
	buttonMoreObsParms.addActionListener(new ActionListener()
	    { public void actionPerformed(ActionEvent e) { action_MoreObsParms(); } });

	JCIconfigTree configTree = new JCIconfigTree( chosenModesMap, enteredParmsMap, chosenOpticsMap );
	
	JPanel jPanelObsNote = new JPanel();
	jPanelObsNote.setLayout( new RatioLayout() );
	jPanelObsNote.add("0.0,0.0;0.2,1.0", new JLabel("Note:"));
	jPanelObsNote.add("0.2,0.0;0.8,1.0", enterObsNote);

	setLayout(new RatioLayout());
	this.add("0.01,0.01;0.35,0.52", panelOpticalSetup() );
	this.add("0.01,0.54;0.35,0.45", panelObsParams() );
	this.add("0.40,0.70;0.30,0.15", jPanelObsNote );
	this.add("0.37,0.05;0.37,0.57", configTree );
	this.add("0.76,0.05;0.23,0.11", buttonMoreObsParms );
	this.add("0.76,0.24;0.23,0.28", panelButtons() );
	this.add("0.76,0.60;0.23,0.14", buttonApplyConfig );
	this.add("0.76,0.82;0.23,0.09", buttonOpticalPath );

	// DC agent Transaction info:
	this.add("0.40,0.88;0.35,0.04", statusAction);
	this.add("0.40,0.93;0.35,0.04", statusResponse);

	configTree.LoadCfgTree();
	configTree.buttonEditCfg_action();

	if(chooseCameraMode.getActive().indexOf("Imag") >= 0) 
	    chooseHWP.setActive("Open");
	else {
	    if(chooseCameraMode.getActive().indexOf("Pola") >= 0)
		chooseHWP.setActive("HWP");
	    else
	        chooseHWP.setActive("Open");
	}

	getNewParams();
	try { Thread.sleep(700);} catch ( Exception _e) {}
	DetectorPanel.setDefaultInputs();
    }
//------------------------------------------------------------------------------------------

    private JPanel panelOpticalSetup()
    {

	String[] CameraModes = {"Imaging","Spectroscopy","Polarimetry"};
	chooseCameraMode = new UFComboPanel("Camera Mode :", CameraModes);
	
	chooseSlit     = new JCIMotorGUI("Slit wheel:",   jciMotors[getMotorNum("Slit")]);
	chooseFilter   = new JCIMotorGUI("Filter wheel:", jciMotors[getMotorNum("Filter")]);
	chooseHWP      = new JCIMotorGUI("HWP wheel :",   jciMotors[getMotorNum("HWP")]);
	chooseHWPangle = new JCIMotorGUI("HWP angle :",   jciMotors[getMotorNum("HWP-Angle")]);
	chooseGrism    = new JCIMotorGUI("Grism wheel:",  jciMotors[getMotorNum("Grism")]);


	chosenModesMap.put("CameraMode", chooseCameraMode);
	chosenOpticsMap.put("Slit",      chooseSlit);
	chosenOpticsMap.put("HWP",       chooseHWP);
	chosenOpticsMap.put("HWPangle",  chooseHWPangle);
	chosenOpticsMap.put("Filter",    chooseFilter);
	chosenOpticsMap.put("Grism",     chooseGrism);

	JPanel panel = new JPanel();
	panel.setLayout(new GridLayout(0,1));
	panel.setBorder(new EtchedBorder(0));
	JPanel title = new JPanel();
	title.setLayout(new GridLayout(0,3));
	title.add(new JLabel("  Optical Setup:"));
	title.add(new JLabel("Desired:"));
	title.add(new JLabel("     Active:"));
	panel.add( title );
	panel.add( chooseCameraMode );
	panel.add( chooseSlit );
	panel.add( chooseFilter );
	panel.add( chooseHWP );
	panel.add( chooseHWPangle );
	panel.add( chooseGrism );
	return panel;
    }
//------------------------------------------------------------------------------------------

    private JPanel panelObsParams()
    {

	String[] dataModes = {"SAVE","SAVE+SEND","DISCARD"};
	chooseDataMode = new UFComboPanel("Data Mode :", dataModes );

	String[] mmsWater = {"1","2","3","4","5","6","7"};
	chooseWaterVapour = new UFComboPanel("Water Vapour (mm):", mmsWater);

	String[] airMasses = {"1.0","1.5","2.0","2.5","3.0"};
	chooseAirmass = new UFComboPanel("Airmass :", airMasses);

	chosenModesMap.put("DataMode", chooseDataMode);
	chosenModesMap.put("AirMass", chooseAirmass);
	chosenModesMap.put("WaterVapour", chooseWaterVapour);

	enterOnSrcTime =    new UFTextPanel("On Source Time (min):");
	enterOffsetPosAngle = new UFTextPanel("Offset Pos. Angle (deg):");

	enterOnSrcTime.desiredField.setEditable(true);
	enterOffsetPosAngle.desiredField.setEditable(true);

	enteredParmsMap.put("OnSourceTime", enterOnSrcTime);
	enteredParmsMap.put("OffsetPosAngle", enterOffsetPosAngle);

	JPanel panel = new JPanel();
	panel.setLayout(new GridLayout(0,1));
	panel.setBorder(new EtchedBorder(0));
	JPanel title = new JPanel();
	title.setLayout(new GridLayout(0,3));
	title.add(new JLabel("  Obs. Params. :"));
	title.add(new JLabel("Desired:"));
	title.add(new JLabel("     Active:"));
	panel.add( title );
	panel.add( chooseDataMode );
	panel.add( enterOnSrcTime );
	panel.add( enterOffsetPosAngle );
	panel.add( chooseWaterVapour );
	panel.add( chooseAirmass );
	return panel;
    }
//------------------------------------------------------------------------------------------

    int getMotorNum( String motorName )
    {
	for( int i=0; i<jciMotors.length; i++ )
	{
	    String name = jciMotors[i].getName().toUpperCase();
	    if( name.indexOf( motorName.toUpperCase() ) >= 0 ) return i;
	}

	System.err.println("JPanelMaster::getMotorNum>");
	System.err.println("Did not find motor name <"+motorName+"> in jciMotors array!!!");
	return(-1);
    }
//------------------------------------------------------------------------------------------
    
    void action_MoreObsParms() {
	Dimension frmSize = new Dimension(400,300);
	MoreObsParms.setSize( frmSize );
	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	MoreObsParms.setLocation( 10, (int)(screenSize.getHeight() - frmSize.getHeight()) - 7 );
	MoreObsParms.show();
	MoreObsParms.setState( Frame.NORMAL );
    }
//------------------------------------------------------------------------------------------

    public void action_ApplyConfig()
    {
	if( ObsInProgress() ) return;

	if( MotorsMoving() ) return;

	if( !checkOpticsConfig() ) return;

	//if desired configuration checks out OK then form Det.Cont. Meta-params config bundle
	// from user choices & entries, and send it to DC agent:

	Vector cmdStrings = new Vector();
	cmdStrings.add("PARM");
	cmdStrings.add("META");

	Set keys = chosenModesMap.keySet();
	Iterator keyit = keys.iterator();

	while( keyit.hasNext() ) {
	    String key = (String)keyit.next();
	    UFComboPanel chooseMode = (UFComboPanel)chosenModesMap.get( key );
	    if( chooseMode != null ) {
		String value = chooseMode.getSelection();
		if( value.trim().length() > 0 ) {
		    cmdStrings.add(key);
		    cmdStrings.add(value);
		}
	    }
	}

	keys = enteredParmsMap.keySet();
	keyit = keys.iterator();

	while( keyit.hasNext() ) {
	    String key = (String)keyit.next();
	    UFTextPanel enteredParm = (UFTextPanel)enteredParmsMap.get( key );
	    if( enteredParm != null ) {
		String value = enteredParm.desiredField.getText();
		if( value.trim().length() > 0 ) {
		    cmdStrings.add(key);
		    cmdStrings.add(value);
		}
	    }
	}

	keys = chosenOpticsMap.keySet();
	keyit = keys.iterator();

	while( keyit.hasNext() ) {
	    String key = (String)keyit.next();
	    UFComboPanel chooseOptic = (UFComboPanel)chosenOpticsMap.get( key );
	    if( chooseOptic != null ) {
		if( chooseOptic.isSelected() ) {
		    String value = chooseOptic.getSelection();
		    if( value.trim().length() > 0 ) {
			cmdStrings.add(key);
			cmdStrings.add(value);
		    }
		}
	    }
	}
	cmdStrings.add("TelescopeEmissivity");
	cmdStrings.add("0.08");
	cmdStrings.add("AmbientTemperature");
	cmdStrings.add("7");

	cmdStrings.add("Throughput");
	cmdStrings.add( Float.toString( selecOpticsThroughput ) );

	String[] DCmeta = new String[ cmdStrings.size() ];
	cmdStrings.copyInto( DCmeta );
	UFStrings ufsDCmeta = new UFStrings("META-config parameters", DCmeta);

//	String[] DCconfig = new String[4];
//	DCconfig[0] = "ACQ";
//	DCconfig[1] = "CONFIG";
//	DCconfig[2] = "NodPattern";
//	String nodPattern = DetectorPanel.chooseNodPattern.getSelection();
//	if( nodPattern.length() < 6 ) nodPattern = "A-BB-A";
//	DCconfig[3] = nodPattern;
//	UFStrings ufsDCconfig = new UFStrings("CONFIGURE  MCE & DAS", DCconfig);

//	UFStrings[] ufsMetaConfig = new UFStrings[2];
	UFStrings[] ufsMetaConfig = new UFStrings[1];
	ufsMetaConfig[0] = ufsDCmeta;
//	ufsMetaConfig[1] = ufsDCconfig;
	commandAgent( ufsMetaConfig );

	chooseWaterVapour.setActive( chooseWaterVapour.getSelection() );
	chooseAirmass.setActive( chooseAirmass.getSelection() );
	needConfigure = false;

	applyOpticsConfig();
    }
//------------------------------------------------------------------------------------------

    public boolean checkOpticsConfig()
    {
	//set defaults:
	if( !chooseCameraMode.isSelected() ) chooseCameraMode.setSelectedItem("Imaging");
	//if( !chooseSlit.isSelected() ) chooseSlit.setSelectedItem("Open");
	//if( !chooseFilter.isSelected() ) chooseFilter.setSelectedItem("J");
	//if( !chooseHWP.isSelected() ) chooseHWP.setSelectedItem("Open");
	//if( !chooseHWPangle.isSelected() ) chooseHWPangle.setSelectedItem(" ");
	//if( !chooseGrism.isSelected() ) chooseGrism.setSelectedItem("Open");

	String cameraMode = chooseCameraMode.getSelection().toLowerCase();
	String slit       = chooseSlit.getSelection().toLowerCase();
	String filter     = chooseFilter.getSelection().toLowerCase();
	String hwp        = chooseHWP.getSelection().toLowerCase();
	String hwpangle   = chooseHWPangle.getSelection().toLowerCase();
	String grism      = chooseGrism.getSelection().toLowerCase();


	if( cameraMode.indexOf("imag") == 0 )
	    {
	      if( slit.indexOf("open") < 0 ) {
		  checkOpticsErrMsg("Slit wheel position");
		  return false;
	      }
	      if( filter.indexOf("open") == 0 ) {
		  checkOpticsErrMsg("Filter wheel position");
		  return false;
	      }
	      if( hwp.indexOf("open") != 0 ) {
		  checkOpticsErrMsg("HWP wheel position");
		  return false;
	      }
	      if( !chooseHWPangle.isSelected() ) chooseHWPangle.setSelectedItem("Park");
	      if( grism.indexOf("open") != 0 ) {
		  checkOpticsErrMsg("Grism wheel position");
		  return false;
	      }	      
	    }
	    
	if( cameraMode.indexOf("spec") == 0 )
	    {
	      if( slit.indexOf("slit") != 0 ) {
		  checkOpticsErrMsg("Slit wheel position");
		  return false;
	      }
	      if( filter.indexOf("open") != 0 ) {
		  checkOpticsErrMsg("Filter wheel position");
		  return false;
	      }
	      if( hwp.indexOf("open") != 0 ) {
		  checkOpticsErrMsg("HWP wheel position");
		  return false;
	      }
	      if( !chooseHWPangle.isSelected() ) chooseHWPangle.setSelectedItem("Park");
	      if( grism.indexOf("open") == 0 || grism.indexOf("closed") == 0 ) {
		  checkOpticsErrMsg("Grism wheel position");
		  return false;
	      }	      
	    }
	    
	if( cameraMode.indexOf("pola") == 0 )
	    {
	      if( slit.indexOf("half") != 0 ) {
		  checkOpticsErrMsg("Slit wheel position");
		  return false;
	      }
	      if( filter.indexOf("open") == 0 ) {
		  checkOpticsErrMsg("Filter wheel position");
		  return false;
	      }
	      if( hwp.indexOf("hwp") != 0 ) {
		  checkOpticsErrMsg("HWP wheel position");
		  return false;
	      }
	      if( !chooseHWPangle.isSelected() ) chooseHWPangle.setSelectedItem("Park");
	      if( grism.indexOf("open") != 0 ) {
		  checkOpticsErrMsg("Grism wheel position");
		  return false;
	      }	      
	    }
	
      selecOpticsThroughput =
	  chooseFilter.selectedThroughput()   *
	  chooseSlit.selectedThroughput()     *
	  chooseGrism.selectedThroughput()    *
	  chooseHWP.selectedThroughput();
      System.out.println("checkOpticsConfig> selected optics Throughput = " + selecOpticsThroughput);
      return true;
    }
//------------------------------------------------------------------------------------------

    public void checkOpticsErrMsg( String item )
    {
	statusAction.setText("WARN: invalid " + item + ".");
	statusResponse.setText("ERR: cannot configure...");
	System.err.println("checkOpticsConfig> must select valid " + item + ".");
    }
//------------------------------------------------------------------------------------------

    public void applyOpticsConfig()
    {
	chooseFilter.moveToPosition();
	chooseSlit.moveToPosition();
	chooseGrism.moveToPosition();
	chooseHWP.moveToPosition();

	String cameraMode = chooseCameraMode.getSelection().toLowerCase();
	String HWplate = chooseHWP.getSelection().toUpperCase();
	//in Polarimetry mode the Half-wave Plate angle mech. can only be datuumed
	// when Half-wave Plate is in position, so perform datum in a wait thread:

	chooseHWPangle.moveToPositionWhenReady( chooseHWP.Motor );
    }
//------------------------------------------------------------------------------------------

    private JPanel panelButtons()
    {
	JButton buttonDatum =   new JButton("DATUM");
	JButton buttonPark =    new JButton("PARK");
	JButton buttonObserve = new JButton("OBSERVE");
	JButton buttonAbort =   new JButton("ABORT");

	buttonDatum.addActionListener(new ActionListener()
	    { public void actionPerformed(ActionEvent e) { action_ButtonDatum(); } });

	buttonPark.addActionListener(new ActionListener()
	    { public void actionPerformed(ActionEvent e) { action_ButtonPark(); } });

	buttonObserve.setBackground(Color.green);
	buttonObserve.addActionListener(new ActionListener()
	    { public void actionPerformed(ActionEvent e) { action_Obs_Start(); } });

	buttonAbort.setBackground(Color.red);
	buttonAbort.setForeground(Color.white);
	buttonAbort.addActionListener(new ActionListener()
	    { public void actionPerformed(ActionEvent e) { action_Obs_Abort(); } });

	JPanel panel = new JPanel();
	panel.setLayout(new GridLayout(2,2));
	panel.setBorder(new EtchedBorder(0));
	panel.add( buttonDatum );
	panel.add( buttonPark );
	panel.add( buttonObserve );
	panel.add( buttonAbort );
	return panel;
    }
//------------------------------------------------------------------------------------------

    public void action_ButtonDatum()
    {
	if( ObsInProgress() ) return;
	if( MotorsMoving() ) return;

	if(JOptionPane.showConfirmDialog(null,"DATUM all mechanisms ?") == JOptionPane.YES_OPTION)
	    {
		for( int i=0; i<jciMotors.length; i++ ) jciMotors[i].doDatumThread();
	    }
    }
//------------------------------------------------------------------------------------------

    public void action_ButtonPark()
    {
	if( ObsInProgress() ) return;
	if( MotorsMoving() ) return;

	if(JOptionPane.showConfirmDialog(null,"PARK all mechanisms ?") == JOptionPane.YES_OPTION)
	    {
		for( int i=0; i<jciMotors.length; i++ ) {
		    jciMotors[i].namedLocationBox.setSelectedItem("Park");
		    jciMotors[i].doHiMoveThread();
		}
	    }
    }
//-------------------------------------------------------------------------------

    boolean ObsInProgress()
    {
	if( obsInProgress ) {
	    statusAction.setText("WARN: observation in progress!");
	    statusResponse.setText("ERR: cannot move mechanisms!");
	    System.err.println(clientName+"> may NOT change configuration during an observation!");
	    return true;
	}
	else return false;
    }
//-------------------------------------------------------------------------------
    //this method is called by the observation monitor thread (UFobsMonitor):

    public void updateFrames( UFFrameConfig ofc )
    {
	if( ofc.frameProcCnt < ofc.frameObsTotal && ofc.frameProcCnt >= 0 )
	    {
		obsInProgress = true;
		jciFrame.camStatus.setText("OBSERVING   ");
	    }
	else {
	    obsInProgress = false;
	    if( jciFrame.motorStatus.getText().indexOf("MOVING") < 0 )
		jciFrame.camStatus.setText("IDLE        ");
	}
    }
//-------------------------------------------------------------------------------

    boolean MotorsMoving()
    {
	if( jciFrame.motorStatus.getText().indexOf("MOVING") > 0 ) {
	    statusAction.setText("WARN: Motors are MOVING...");
	    statusResponse.setText("ERR: cannot re-command mechanisms!");
	    System.err.println(clientName+"> may NOT change configuration or observe while moving!");
	    return true;
	}
	else return false;
    }
//-------------------------------------------------------------------------------
    /**
     *JPanelMaster#checkDataMode
     * Checks the current data mode, and if TRUE returns if SAVE or user OK-s it.
     */
    boolean checkDataMode()
    {
	String DataMode = chooseDataMode.getActive();
	String newDataMode = chooseDataMode.getSelection();
	if( newDataMode.length() > 3 ) DataMode = newDataMode;

	if( DataMode.length() < 4 )
	    {
		JOptionPane.showMessageDialog(null,"Need to select Data Mode !",
					      "ERROR", JOptionPane.ERROR_MESSAGE);
		return false;
	    }

	if( DataMode.trim().toUpperCase().equals("SAVE") )
	    return true;
	else {
	    if( JOptionPane.NO_OPTION ==
		JOptionPane.showConfirmDialog(null,"Data will NOT be saved, START obs anyway ?","Warning",
					      JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE) )
		return false;
	    else
		return true;
	}
    }
//-------------------------------------------------------------------------------
    /**
     * JPanelMaster#start button action performed
     * Checks if configuration needs to be applied or if motors are moving, and verifies data mode,
     * and if all ok, sends observation ACQ => START directive to detector controller agent.
     */
    void action_Obs_Start()
    {
	if( needConfigure ) return;

	if( MotorsMoving() ) {
	    statusResponse.setText("ERR: cannot start observation");
	    return;
	}

	if( checkDataMode() )
	    {
		Vector cmdStrings = new Vector();
		cmdStrings.add("ACQ");
		cmdStrings.add("START");

		cmdStrings.add("AcqMode");
		cmdStrings.add("normal");

		String DataMode = chooseDataMode.getSelection();
		if( DataMode.length() > 3 ) {
		    cmdStrings.add("DataMode");
		    cmdStrings.add( DataMode );
		}

		String Comment = enterObsNote.getText();
		if( Comment.length() > 0 ) {
		    cmdStrings.add("Comment");
		    cmdStrings.add( Comment );
		}

		String[] DCstart = new String[ cmdStrings.size() ];
		cmdStrings.copyInto( DCstart );
		UFStrings ufsDCstart = new UFStrings("START observation", DCstart);
		commandAgent( ufsDCstart );
	    }
    } //end of action_Obs_Start_action
//-------------------------------------------------------------------------------
    /**
     * JPanelMaster#abort button action performed
     * send ACQ => ABORT directive to detector control agent.
     * or if motors are moving, gives user option to abort all motion.
     */
    void action_Obs_Abort()
    {
	if( jciFrame.motorStatus.getText().indexOf("MOVING") > 0 )
	    {
		if(JOptionPane.showConfirmDialog(null,"ABORT all motor motion ?") == JOptionPane.YES_OPTION)
		    {
			for( int i=0; i<jciMotors.length; i++ ) jciMotors[i].doAbortThread();
		    }
	    }

	if(JOptionPane.showConfirmDialog(null,"ABORT the observation ?") == JOptionPane.YES_OPTION)
	    {
		String[] DCabort = new String[2];
		DCabort[0] = "ACQ";
		DCabort[1] = "ABORT";
		UFStrings ufsDCabort = new UFStrings("ABORT observation", DCabort);
		commandAgent( ufsDCabort );
	    }
    }
//--------------------------------------------------------
    
    void action_ShowOpticalPath()
    {
	if( showOpticalPath == null )
	    showOpticalPath = new JCIopticalPath( jciMotors );
	else
	    showOpticalPath.updatePathLines( jciMotors );

	showOpticalPath.show();
	showOpticalPath.setState( Frame.NORMAL );
    }
//------------------------------------------------------------------------------------------
    /**
     * JPanelMaster#commandAgent
     *@param agentRequest : UFStrings object array containing multiple requests to send to DC agent.
     */
    public void commandAgent( UFStrings agentRequest  ) {
	commandAgent( agentRequest, clientName );
    }
    public void commandAgent( UFStrings[] agentRequest  ) {
	commandAgent( agentRequest, clientName );
    }

    public void commandAgent( UFStrings agentRequest, String callerName )
    {
	UFStrings[] agentReqs = new UFStrings[1];
	agentReqs[0] = agentRequest;
	commandAgent( agentReqs, callerName );
    }

    public void commandAgent( UFStrings[] agentRequest, String callerName )
    {
	final String panel = callerName + ":";
	final UFStrings AgentRequest[] = agentRequest;
	String action = agentRequest[0].name();
	String msg = "sending " + action + " directive to DC agent...";
	System.out.println( panel + msg );
	statusAction.setText( msg );
	statusResponse.setText(" waiting for response... ");
	DetectorPanel.statusAction.setText( msg );
	DetectorPanel.statusResponse.setText(" waiting for response... ");

	//create new task and put on the event thread queue
	// so it will be invoked after current action completes (so above setText will be repainted):

	Runnable cmd_DC_agent = new Runnable() {
		public void run() {
		    try {
			for( int i=0; i<AgentRequest.length; i++ ) {
			    gotParams = true;
			    String action = AgentRequest[i].name();
			    AgentRequest[i].rename( panel + action );

			    if( i > 0 ) {
				String msg = "sending " + action + " directive to DC agent...";
				System.out.println( panel + msg );
				statusAction.setText( msg );
				statusResponse.setText(" waiting for response... ");
				DetectorPanel.statusAction.setText( msg );
				DetectorPanel.statusResponse.setText(" waiting for response... ");
			    }

			    String[] words = action.split(" ");
			    String task = words[0] + " >  ";
			    UFStrings agentReply = DetectorPanel.sendRecvAgent( AgentRequest[i], panel+task );

			    if( agentReply == null ) {
				String statmsg = "transaction ERROR: recvd NO response ?";
				statusResponse.setText( task + statmsg );
				DetectorPanel.statusResponse.setText( task + statmsg );
			    }
			    else {
				String statmsg = DetectorPanel.decodeAgentResponse( agentReply );

				if( statmsg == null ) statmsg = "WARN: failed decoding response ?";
				DetectorPanel.statusResponse.setText( task + statmsg );
				System.out.println( "JPanelDetector:" + task + statmsg );

				statmsg = decodeAgentResponse( agentReply );

				if( statmsg == null ) statmsg = "WARN: failed decoding response ?";
				statusResponse.setText( task + statmsg );
				System.out.println( panel + task + statmsg );
			    }
			}
		    }
		    catch( Exception x ) { x.printStackTrace(); }
		}
	    };

	SwingUtilities.invokeLater( cmd_DC_agent );
    }
//-------------------------------------------------------------------------------
    /**
     * JPanelMaster#decodeAgentResponse
     *@param agentReply : UFStrings object containing reply from DC agent.
     *@retval = String, either OK or ERR.
     */
    public String decodeAgentResponse( UFStrings agentReply )
    {
	if( agentReply == null ) return("ERROR: agent did not respond ?");
	boolean verbose = false;
	if( verbose ) System.out.println( agentReply.toString() );
	boolean Desired = false;
	boolean Active = false;
	boolean status = false;

	for( int i=0; i<agentReply.numVals(); i++ )
	    {
		String paramVal = agentReply.valData(i);
		int eqpos = 0;

		if( paramVal.toUpperCase().indexOf("DESIRED") >= 0 ) {
		    Desired = true;
		    if( paramVal.toUpperCase().indexOf("ACTIVE") >= 0 )
			Active = true;
		    else
			Active = false;
		}
		else if( paramVal.toUpperCase().indexOf("ACTIVE") >= 0 ) {
		    Active = true;
		    Desired = false;
		}

		if( (eqpos = paramVal.indexOf("==")) > 0 )
		    {
			String param = paramVal.substring(0,eqpos).trim();
			String pVal = paramVal.substring( eqpos+2, paramVal.length() ).trim();
			if( verbose ) System.out.println( i + ":" + param + "=" + pVal );
			UFTextPanel paramField = (UFTextPanel)enteredParmsMap.get( param );

			if( paramField != null ) {
			    if( Desired ) paramField.setDesired( pVal );
			    if( Active )  paramField.setActive( pVal );
			    status = true;
			}
			else {
			    UFComboPanel modeSel = (UFComboPanel)chosenModesMap.get( param );
			    if( modeSel != null ) {
				if( Desired ) modeSel.setDesired( pVal );
				if( Active )  modeSel.setActive( pVal );
				status = true;
			    }
			    else {
				paramField = (UFTextPanel)MoreObsParms.enteredParmsMap.get( param );
				if( paramField != null ) {
				    if( Desired ) paramField.setDesired( pVal );
				    if( Active )  paramField.setActive( pVal );
				    status = true;
				}
				else {
				    modeSel = (UFComboPanel)MoreObsParms.chosenModesMap.get( param );
				    if( modeSel != null ) {
					if( Desired ) modeSel.setDesired( pVal );
					if( Active )  modeSel.setActive( pVal );
					status = true;
				    }
				}
			    }
			}
		    }
	    }

	if( agentReply.name().toUpperCase().indexOf("ERR") >= 0 )
	    {
		if( agentReply.numVals() > 0 )
		    return agentReply.valData(0);
		else
		    return("ERROR");
	    }
	else {
	    if( status )
		return("OK");
	    else
		return("ERROR");
	}
    }
} //end of JPanelMaster

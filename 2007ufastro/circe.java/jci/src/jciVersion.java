package ufjci;

/**
 *Handles the current version of the package
 *Date is hard coded in the version field
 *Date is six digits starting with the year then month then day
 *This string is used for the name in the title bar
 */
public class jciVersion {
    public static final
	String rcsID = "$Name:  $ $Id: jciVersion.java,v 1.1 2004/11/01 19:04:00 amarin Exp $";
    public static final String version = "2004/10/12";
}

package ufjci;

import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.net.*;
import java.applet.*;
import java.io.*;
import java.util.*;
import java.util.Date.*;
import java.sql.Timestamp;
import javaUFProtocol.*;
import javaUFLib.*;

//===============================================================================
/**
 *This Class handles the Temperature tabbed pane
 */
public class JPanelTemperature extends JPanel {
  public static final String rcsID = "$Name:  $ $Id: JPanelTemperature.java,v 1.1 2004/11/01 19:04:00 amarin Exp $";
  TextFile graphDataFile;
  public String fileName;                // file containing temperature sensors config.
  public String tgd_filename = "cc.tgd"; // default temperature graph data filename is cc.tgd
  public String hostnameLs218 = "kepler";
  public String hostnameLs331 = "kepler";
  public String hostnamePrAgent = "kepler";
  int portLs218 = 52002;
  int portLs331 = 52003;
  int portPrAgent = 52004;

    UFHostPortPanel hostPortPanLs218 = new UFHostPortPanel(hostnameLs218,portLs218);
    UFHostPortPanel hostPortPanLs331 = new UFHostPortPanel(hostnameLs331,portLs331);
    UFHostPortPanel hostPortPanPrAg = new UFHostPortPanel(hostnamePrAgent,portPrAgent);

  JTextField set_Point = new JTextField();
  JTextField p_Text = new JTextField();
  JTextField i_Text = new JTextField();
  JTextField d_Text = new JTextField();
  JLabel p_Label = new JLabel("     P =");
  JLabel i_Label = new JLabel("     I =");
  JLabel d_Label = new JLabel("     D =");

  JButton jButtonConnectLs218 = new JButton("Connect to Ls 218 Agent");
  JButton jButtonConnectLs331 = new JButton("Connect to Ls 331 Agent");
  JButton jButtonConnectPrAg = new JButton("Connect to Pressure Agent");
  JButton jButtonPID = new JButton("Hide  PID");
  JButton jButtonHeaterOff = new JButton("Emergency Heater OFF");
  JButton jButtonApplyPID = new JButton("Apply PID");
  JButton jButtonQueryPID = new JButton("Query PID");
  JButton jButtonApplyHeater = new JButton("Apply Heater");
  JButton jButtonQueryHeater = new JButton("Query Heater");  
  JButton jButtonApplySetP = new JButton("Apply Set Point");
  JButton jButtonQuerySetP = new JButton("Query Set Point");
  JPanel jPanelPIDButtons = new JPanel(new GridLayout(1,0));
  JTextField actualTdet = new JTextField();
  JCIsensorParameters SensorParms[];
  GridLayout gridLayout1 = new GridLayout();
  GridLayout gridLayout2 = new GridLayout();
  GridLayout gridLayout3 = new GridLayout();
  JPanel jPanelLeft = new JPanel();
  JPanel jColorPanel = new JPanel();
  JPanel jSetPUnitPanel = new JPanel();
  JPanel jPanelSensorLabel = new JPanel();
  JPanel jPanelSensorTemp = new JPanel();
  JPanel jPanelSensorUnits = new JPanel();
  JPanel jPanelTempControl = new JPanel();
  JPanel jRatePanel = new JPanel();
  JPanel jKeyPanel = new JPanel();
  JPanel jButtonPanel = new JPanel();
  JPanel jPanelHeater = new JPanel();
  JPanel jRateUnitPanel = new JPanel();
  JPanel jCheckBoxPanel = new JPanel();
  JPanel jGraphButtonPanel = new JPanel();
  JGraphPanel jGraphPanel;
  JButton jGraphStartButton = new JButton("Start Graph");
  JButton jGraphParamButton = new JButton("Graph Parameters");
  JButton jGraphClearButton = new JButton("Clear Graph Data");
  JButton jGraphLoadButton = new JButton("Load Graph Data");
  GraphThread graphThread;
  JComboBox heaterComboBox;
  Thread monitorLs218;
  Thread monitorLs331;
  Thread monitorPressure;
  Socket cmdSocLs331Agent;
  Socket cmdSocLs218;
  Socket cmdSocPressAgent;
  JCIsensorParameters PressureSensor = new JCIsensorParameters("Pressure");
  JDialogGraphParameters dialogBox; // create dialog box after getting
                                    // graph parameters from file or UFlib
  JCheckBox jCheckBoxGraphGrid = new JCheckBox("Show Grid");
  UFLabel errMsg = new UFLabel("msg:");

  private final int CONNECT_TIMEOUT =  3000;
  private final int DEFAULT_TIMEOUT =  9000;
  private final int MONITOR_TIMEOUT = 13000;
//-------------------------------------------------------------------------------
  /**
   *Constructs the frame
   *@param fileName string that contains the file name
   */
  public JPanelTemperature(String fileName) {
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    try  {
      this.fileName = fileName;
      CreatePanel();
    }
    catch(Exception e) {
      e.printStackTrace();
    }
  }
//-------------------------------------------------------------------------------
  /**
   *Constructs the frame
   *@param fileName string that contains the file name
   *@param globalHostName hostname for both Ls 218 and 331 agents
   */
  public JPanelTemperature(String fileName, String globalHostName) {
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    try  {
      this.fileName = fileName;
      this.hostnameLs218 = globalHostName;
      this.hostnameLs331 = globalHostName;
      setHost( globalHostName ); //put it also in host fields.
      CreatePanel();
    }
    catch(Exception e) {
      e.printStackTrace();
    }
  }
//-------------------------------------------------------------------------------
  /**
   *Component initialization
   */
  private void CreatePanel() throws Exception
  {
    this.setLayout(new RatioLayout());

    String [] items = {"Off (0)","Low (1)","Medium (2)","High (3)"};
    heaterComboBox = new JComboBox( items );
    ReadFile( fileName );
    PressureSensor.hotThreshold = 1.e-4;
    PressureSensor.coldThreshold = 1.3e-6;
    graphThread = new GraphThread();
    dialogBox = new JDialogGraphParameters();

    actualTdet.setEditable(false);
    actualTdet.setBackground(new Color(233,233,233));
    this.setMaximumSize(new Dimension(870, 470));
    this.setMinimumSize(new Dimension(870, 470));
    this.setPreferredSize(new Dimension(870, 470));
    gridLayout1.setColumns(1);
    gridLayout1.setRows(0);
    gridLayout1.setVgap(0);
    gridLayout2.setColumns(1);
    gridLayout2.setRows(0);
    gridLayout2.setVgap(0);
    gridLayout3.setColumns(1);
    gridLayout3.setRows(0);
    gridLayout3.setVgap(0);
    jPanelLeft.setLayout(gridLayout1);
    jSetPUnitPanel.setLayout(gridLayout1);
    jColorPanel.setLayout(gridLayout2);
    jPanelSensorLabel.setLayout(gridLayout2);
    jPanelSensorTemp.setLayout(gridLayout2);
    jPanelSensorUnits.setLayout(gridLayout2);
    jPanelTempControl.setLayout(gridLayout1);
    jRateUnitPanel.setLayout(gridLayout2);
    jCheckBoxPanel.setLayout(gridLayout2);
    jRatePanel.setLayout(gridLayout2);
    jKeyPanel.setLayout(gridLayout2);
    jGraphPanel.setLayout(gridLayout1);
    jButtonPanel.setLayout(gridLayout3);

    jButtonConnectLs218.addActionListener(new java.awt.event.ActionListener()
	{
	    public void actionPerformed(ActionEvent ae) { connectToLs218Agent(); }
	});
    jButtonConnectLs331.addActionListener(new java.awt.event.ActionListener()
	{
	    public void actionPerformed(ActionEvent ae) { connectToLs331Agent(); }
	});
    jButtonConnectPrAg.addActionListener(new java.awt.event.ActionListener()
	{
	    public void actionPerformed(ActionEvent ae) { connectToPressureAgent(); }
	});

    jButtonQueryPID.addActionListener(new java.awt.event.ActionListener() {
	    public void actionPerformed(ActionEvent e) { jButtonQueryPID_action(e); } });

    jButtonApplySetP.addActionListener(new java.awt.event.ActionListener() {
	    public void actionPerformed(ActionEvent e) { jButtonApplySetP_action(e); } });

    jButtonQuerySetP.addActionListener(new java.awt.event.ActionListener() {
	    public void actionPerformed(ActionEvent e) { jButtonQuerySetP_action(e); } });

    jButtonPID.addActionListener( new java.awt.event.ActionListener() {
	    public void actionPerformed(ActionEvent e) { jButtonPID_action(); } });

    jGraphLoadButton.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) { jGraphLoadButton_action(e); } });

    jGraphStartButton.addActionListener(new java.awt.event.ActionListener() {
	    public void actionPerformed(ActionEvent e) { jGraphStartButton_action(e); } });

    jGraphClearButton.addActionListener(new java.awt.event.ActionListener() {
	    public void actionPerformed(ActionEvent e) {     
		if (graphThread.isAlive()) 
		    jGraphStartButton_action(e); // stop the graph thread before clearing data! 
		jGraphPanel.eraseGraphData(); 
	    }
	});

    jGraphParamButton.addActionListener(new java.awt.event.ActionListener() {
	    public void actionPerformed(ActionEvent e) { jGraphParamButton_action(e); } });

    jButtonApplyPID.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) { jButtonApplyPID_action(e); } });

    jButtonApplyHeater.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) { jButtonApplyHeater_action(e); } });

    jButtonQueryHeater.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) { jButtonQueryHeater_action(e); } });

    jButtonHeaterOff.addActionListener(new java.awt.event.ActionListener() {
	    public void actionPerformed(ActionEvent e) { jButtonHeaterOff_action(e); } });

    jCheckBoxGraphGrid.setSelected(true);
    jCheckBoxGraphGrid.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) { repaint(); } });

    jColorPanel.add(new JLabel(""));
    jPanelSensorLabel.add(new JLabel("Sensor Name"));
    jPanelSensorTemp.add(new JLabel("Temperature"));
    jPanelSensorUnits.add(new JLabel(""));
    jRatePanel.add(new JLabel(" Rate"));
    jRateUnitPanel.add(new JLabel(""));
    jCheckBoxPanel.add(new JLabel(""));

    for (int i=0; i<SensorParms.length; i++) {
      jColorPanel.add(SensorParms[i].statusColor);
      jPanelSensorLabel.add(SensorParms[i].sName);
      jPanelSensorTemp.add(SensorParms[i].sValue);
      jPanelSensorUnits.add(SensorParms[i].units);
      jRatePanel.add(SensorParms[i].graphElement.rate);
      jRateUnitPanel.add(SensorParms[i].graphElement.units);
      jCheckBoxPanel.add(SensorParms[i].graphElement.showme);
    }

    jColorPanel.add(PressureSensor.statusColor);
    PressureSensor.units.setText("Torr");
    jPanelSensorLabel.add(PressureSensor.sName);
    jPanelSensorTemp.add(PressureSensor.sValue);
    jPanelSensorUnits.add(PressureSensor.units);
    jRatePanel.add( new JLabel());
    jRateUnitPanel.add(new JLabel());
    jCheckBoxPanel.add(new JLabel());

    jKeyPanel.add(new JLabel("Red == Warm, OK to open"));
    jKeyPanel.add(new JLabel("Yellow == In Transition"));
    jKeyPanel.add(new JLabel("Blue == Cold, OK for observation"));

    jPanelLeft.add(new JLabel()); //add dummy components for spacing
    jPanelTempControl.add(new JLabel("Detector  T"));
    jSetPUnitPanel.add(new JLabel()); // add dummy components for spacing

    jPanelLeft.add(new JLabel("Set Point ="));
    jPanelTempControl.add(set_Point);
    jSetPUnitPanel.add(new JLabel("K"));

    jPanelLeft.add(new JLabel("Actual T ="));
    jPanelTempControl.add(actualTdet);
    jSetPUnitPanel.add(new JLabel("K"));

    jPanelLeft.add(p_Label);
    jPanelTempControl.add(p_Text);

    jPanelLeft.add(i_Label);
    jPanelTempControl.add(i_Text);

    jPanelLeft.add(d_Label);
    jPanelTempControl.add(d_Text);

    jPanelHeater.add(new JLabel("Heater Power:"));
    jPanelHeater.add(heaterComboBox);
    jPanelHeater.add(jButtonHeaterOff);

    jButtonPanel.add(jButtonApplySetP);
    jButtonPanel.add(jButtonQuerySetP);
    jButtonPanel.add(jButtonApplyHeater);
    jButtonPanel.add(jButtonQueryHeater);
    jButtonPanel.add(jButtonPID);
    jButtonPanel.add(jPanelPIDButtons);
    jGraphButtonPanel.setLayout(new GridLayout(0,1));
    jGraphButtonPanel.add(jGraphStartButton);
    jGraphButtonPanel.add(jGraphParamButton);
    jGraphButtonPanel.add(jGraphClearButton);
    jGraphButtonPanel.add(jGraphLoadButton);

    for (int h=0; h<3; h++) jSetPUnitPanel.add(new JLabel()); //add dummy components for spacing
    this.add("0.01,0.02;0.08,0.40", jPanelLeft);
    this.add("0.09,0.02;0.08,0.40", jPanelTempControl);
    this.add("0.17,0.02;0.02,0.40", jSetPUnitPanel);
    this.add("0.01,0.44;0.20,0.18", jPanelHeater);
    this.add("0.01,0.62;0.20,0.34", jButtonPanel);
    this.add("0.22,0.02;0.03,0.59", jColorPanel);
    this.add("0.26,0.02;0.10,0.59", jPanelSensorLabel);
    this.add("0.36,0.02;0.07,0.59", jPanelSensorTemp);
    this.add("0.44,0.02;0.03,0.59", jPanelSensorUnits);
    this.add("0.47,0.02;0.02,0.59", jCheckBoxPanel);
    this.add("0.50,0.02;0.49,0.73", jGraphPanel);
    this.add("0.23,0.61;0.21,0.09", jKeyPanel);
    this.add("0.25,0.71;0.22,0.06", jButtonConnectLs331);
    this.add("0.24,0.77;0.24,0.06", hostPortPanLs331);
    this.add("0.25,0.84;0.22,0.06", jButtonConnectLs218);
    this.add("0.24,0.90;0.24,0.06", hostPortPanLs218);
    this.add("0.50,0.84;0.22,0.06", jButtonConnectPrAg);
    this.add("0.49,0.90;0.24,0.06", hostPortPanPrAg);
    this.add("0.65,0.78;0.10,0.05", jCheckBoxGraphGrid);
    this.add("0.77,0.78;0.18,0.20", jGraphButtonPanel);
    this.add("0.01,0.95;0.72;0.05", errMsg);

    p_Label.setVisible(false);
    i_Label.setVisible(false);
    d_Label.setVisible(false);
    p_Text.setVisible(false);
    i_Text.setVisible(false);
    d_Text.setVisible(false);
    jButtonPID.setText("Show PID");

    connectToLs331Agent();
    connectToLs218Agent();
    connectToPressureAgent();
    checkConnection();
  } //end of jbInit

//-------------------------------------------------------------------------------

    void setHostLs218(String newHost) {
	hostPortPanLs218.setHost( newHost );
    }

    void setHostLs331(String newHost) {
	hostPortPanLs331.setHost( newHost );
    }

    void setHostPrAgent(String newHost) {
	hostPortPanPrAg.setHost( newHost );
    }

    void setHost(String newHost) {
	setHostLs331( newHost );
	setHostLs218( newHost );
	setHostPrAgent( newHost );
    }
//-------------------------------------------------------------------------------

    Thread createLs218Monitor() {
	return new Thread() {
		public synchronized void run() {
		    try {
			JCIsensorParameters [] jsp = SensorParms;
			errMsg.setText("starting Ls218 Monitor Thread....");
			System.out.println("JPanelTemperature.createLs218Monitor> port=" + portLs218 +
					   " @ host = " + hostnameLs218 );
			InetSocketAddress agentIPsoca = new InetSocketAddress( hostnameLs218, portLs218 );
			Socket s = new Socket();
			s.connect( agentIPsoca, CONNECT_TIMEOUT );
			s.setSoTimeout(DEFAULT_TIMEOUT);
			UFTimeStamp uft = new UFTimeStamp("Status");
			uft.sendTo(s);
			UFProtocol ufp = UFProtocol.createFrom(s);

			if (ufp == null) {
			    String msg = "Ls218 Monitor Thread> did not recv response Tmon Agent.";
			    jciError.show(msg);
			    errMsg.setText("ERR: "+msg);
			    s.close();
			    setName("Stop");
			    jciFrame.jPanelTemperature.checkConnection();
			    return;
			}

			errMsg.setText("Ls218 Monitor Thread is running....");
			setName("KeepGoing");
			System.out.println("JPanelTemperature.createLs218Monitor> " + ufp.name() );
			s.setSoTimeout(MONITOR_TIMEOUT);

			while(getName().equals("KeepGoing")) {
			    ufp = UFProtocol.createFrom(s);
			    if (ufp instanceof UFStrings) {
				String timeStamp = ufp.timeStamp();
				String name = ufp.name().toLowerCase();
				if( name.equals("autovals") || name.equals("status") ) {
				    StringTokenizer st = new StringTokenizer(((UFStrings)ufp).valData(0),"+,");
				    String[] vals = new String[st.countTokens()];
				    int i=0;
				    while(st.hasMoreTokens()) vals[i++] = st.nextToken();
				    for (i=0; i<jsp.length; i++) 
					if(jsp[i].modelNumber == 218) {
					    jsp[i].timeStamp = timeStamp;
					    jsp[i].sValue.setText( vals[Integer.parseInt(jsp[i].channel)-1] );
					}
				}
				else if( name.equals("error") ) errMsg.setText("Ls218> "+name);
			    } else {
				jciError.show("Ls218 Monitor Thread> object recvd is NOT UFStrings!");
				if( ufp != null ) jciError.show( ufp.description() + ".name=" + ufp.name() );
				String msg = "Ls218 Monitor Thread> stopping  (try reconnect).";
				jciError.show(msg);
				errMsg.setText("ERR: "+msg);
				setName("Stop");
			    }
			}
			s.close();
		    }
		    catch (Exception e) {
			String msg = "Ls218 Monitor Thread> " + e.toString();
			jciError.show(msg);
			errMsg.setText("ERR: "+msg);
			setName("Stop");
		    }

		    if( jciFrame.jPanelTemperature != null )
			jciFrame.jPanelTemperature.checkConnection();
		}
	    };
    }
//-------------------------------------------------------------------------------

    Thread createLs331Monitor() {
	return new Thread () {
		public synchronized void run() {
		    try {
			JCIsensorParameters[] jsp = SensorParms;
			errMsg.setText("starting Ls331 Monitor Thread....");
			System.out.println("JPanelTemperature.createLs331Monitor> port=" + portLs331 +
					   " @ host = " + hostnameLs331 );
			InetSocketAddress agentIPsoca = new InetSocketAddress( hostnameLs331, portLs331 );
			Socket s = new Socket();
			s.connect( agentIPsoca, CONNECT_TIMEOUT );
			s.setSoTimeout(DEFAULT_TIMEOUT);
			UFTimeStamp uft = new UFTimeStamp("Status");
			uft.sendTo(s);
			UFProtocol ufp = UFProtocol.createFrom(s);

			if (ufp == null) {
			    String msg = "Ls331 Monitor Thread> did not recv response from Tcon Agent.";
			    jciError.show(msg);
			    errMsg.setText("ERR: "+msg);
			    s.close();
			    setName("Stop");
			    jciFrame.jPanelTemperature.checkConnection();
			    return;
			}

			errMsg.setText("Ls331 Monitor Thread is running....");
			setName("KeepGoing");
			System.out.println("JPanelTemperature.createLs331Monitor> " + ufp.name() );
			s.setSoTimeout(MONITOR_TIMEOUT);

			while(getName().equals("KeepGoing")) {
			    ufp = UFProtocol.createFrom(s);
			    if (ufp instanceof UFStrings) {
				String timeStamp = ufp.timeStamp();
				String name = ufp.name().toLowerCase();
				if( name.indexOf("status") > 0 || name.indexOf("update") > 0 )
				    for (int i=0; i<jsp.length; i++)
					if (jsp[i].modelNumber == 331 ) {
					    jsp[i].timeStamp = timeStamp;
					    if( jsp[i].channel.equals("A") )
						jsp[i].sValue.setText( ((UFStrings)ufp).valData(0) );
					    else if( jsp[i].channel.equals("B") )
						jsp[i].sValue.setText( ((UFStrings)ufp).valData(1) );
					}
			    } else {
				jciError.show("Ls331 Monitor Thread: object recvd is NOT UFStrings!");
				if( ufp != null ) jciError.show( ufp.description() + ".name=" + ufp.name() );
				String msg = "Ls331 Monitor Thread> stopping  (try reconnect).";
				jciError.show(msg);
				errMsg.setText("ERR: "+msg);
				setName("Stop");
			    }
			}
			s.close();
		    }
		    catch (Exception e) {
			String msg = "Ls331 Monitor Thread> " + e.toString();
			jciError.show(msg);
			errMsg.setText("ERR: "+msg);
			setName("Stop");
		    }

		    if( jciFrame.jPanelTemperature != null )
			jciFrame.jPanelTemperature.checkConnection();
		}
	    };
    }
//-------------------------------------------------------------------------------

    Thread createPressureMonitor() {
	return new Thread () {
		public synchronized void run() {
		    try {
			errMsg.setText("starting Pressure Monitor Thread....");
			System.out.println("JPanelTemperature.createPressureMonitor> port=" + portPrAgent +
					   " @ host = " + hostnamePrAgent );
			InetSocketAddress agentIPsoca = new InetSocketAddress( hostnamePrAgent, portPrAgent );
			Socket s = new Socket();
			s.connect( agentIPsoca, CONNECT_TIMEOUT );
			s.setSoTimeout(DEFAULT_TIMEOUT);
			UFTimeStamp uft = new UFTimeStamp("Status");
			uft.sendTo(s);
			UFProtocol ufp = UFProtocol.createFrom(s);

			if (ufp == null) {
			    String msg = "Pressure Monitor Thread: did not recv response from Pressure Agent.";
			    jciError.show(msg);
			    errMsg.setText("ERR: "+msg);
			    s.close();
			    setName("Stop");
			    jciFrame.jPanelTemperature.checkConnection();
			    return;
			}

			errMsg.setText("Pressure Monitor Thread is running....");
			setName("KeepGoing");
			System.out.println("JPanelTemperature.createPressureMonitor> " + ufp.name() );
			s.setSoTimeout(MONITOR_TIMEOUT);

			while(getName().equals("KeepGoing")) {
			    ufp = UFProtocol.createFrom(s);
			    if (ufp instanceof UFStrings) {
				String name = ufp.name().toLowerCase();
				if( name.indexOf("status") > 0 || name.indexOf("update") > 0 ) {
				    PressureSensor.timeStamp = ufp.timeStamp();
				    String pressure = ((UFStrings)ufp).valData(0);
				    int z = pressure.indexOf("0E");
				    if( z > 0 ) {
					int e = pressure.indexOf("E");
					int zz = pressure.indexOf("00E");
					if( zz > 0 ) z = zz;
					pressure = pressure.substring(0,z) + pressure.substring(e);
				    }
				    PressureSensor.sValue.setText( pressure );
				}
			    } else {
				jciError.show("Pressure Monitor Thread: object recvd is NOT UFStrings!");
				if( ufp != null ) jciError.show( ufp.description() + ".name=" + ufp.name() );
				String msg = "Pressure Monitor Thread> stopping  (try reconnect).";
				jciError.show(msg);
				errMsg.setText("ERR: "+msg);
				setName("Stop");
			    }
			}
			s.close();
		    }
		    catch (Exception e) {
			String msg = "Pressure Monitor Thread> " + e.toString();
			jciError.show(msg);
			errMsg.setText("ERR: "+msg);
			setName("Stop");
		    }

		    if( jciFrame.jPanelTemperature != null )
			jciFrame.jPanelTemperature.checkConnection();
		}
	    };
    }
//-------------------------------------------------------------------------------

    void reconnect() {
	connectToLs331Agent();
	connectToLs218Agent();
	connectToPressureAgent();
    }

    void connectToLs331Agent() {
	try {
	    hostnameLs331 = hostPortPanLs331.getHostField();
	    portLs331 = hostPortPanLs331.getPortField();
	    if (monitorLs331 != null) 
		monitorLs331.setName("Stop");
	    monitorLs331 = createLs331Monitor();
	    monitorLs331.setName("KeepGoing");
	    monitorLs331.start();
	    if( cmdSocLs331Agent != null ) try{ cmdSocLs331Agent.close(); } catch(Exception ee) {}
	    cmdSocLs331Agent = connectCmdLs331(hostnameLs331,portLs331);
	}
	catch(Exception e) {
	    jciError.show("JPanelTemperature::connectToLs331Agent> "+e.toString());
	}
	checkConnection();
    }

    void connectToLs218Agent() {
	try {
	    hostnameLs218 = hostPortPanLs218.getHostField();
	    portLs218 = hostPortPanLs218.getPortField();
	    if (monitorLs218 != null)
		monitorLs218.setName("Stop");
	    monitorLs218 = createLs218Monitor();
	    monitorLs218.setName("KeepGoing");
	    monitorLs218.start();
	}
	catch(Exception e) {
	    jciError.show("JPanelTemperature::connectToLs218Agent> "+e.toString());
	}
	checkConnection();
    }

    void connectToPressureAgent() {
	try {
	    hostnamePrAgent = hostPortPanPrAg.getHostField();
	    portPrAgent = hostPortPanPrAg.getPortField();
	    if (monitorPressure != null)
		monitorPressure.setName("Stop");
	    monitorPressure = createPressureMonitor();
	    monitorPressure.setName("KeepGoing");
	    monitorPressure.start();
	}
	catch(Exception e) {
	    jciError.show("JPanelTemperature::connectToPressureAgent> "+e.toString());
	}
	checkConnection();
    }
//-------------------------------------------------------------------------------

    Socket connectCmdLs331 (String host, int port) {
	try {
	    System.out.println("JPanelTemperature.connectCmdLs331> port=" + port + " @ host = " + host );
	    InetSocketAddress agentIPsoca = new InetSocketAddress( host, port );
	    Socket s = new Socket();
	    s.connect( agentIPsoca, CONNECT_TIMEOUT );
	    s.setSoTimeout(DEFAULT_TIMEOUT);
	    UFTimeStamp uft = new UFTimeStamp("Command");
	    uft.sendTo(s);
	    UFProtocol ufp = UFProtocol.createFrom(s);
	    if (ufp == null) {
		jciError.show("JPanelTemperature.connectCmdLs331> Did not recv handshake from 331 Agent.");
		return null;
	    }
	    System.out.println("JPanelTemperature.connectCmdLs331> " + ufp.name() );
	    return s;
	} catch (Exception e) {
	    jciError.show("Error connecting to 331 agent: " + e.toString());
	    return null;
	}
    }
//-------------------------------------------------------------------------------

    void checkConnection()
    {
	if( monitorLs218 == null || !monitorLs218.getName().equals("KeepGoing") ) 
	    { 
		jButtonConnectLs218.setBackground(Color.red);
		jButtonConnectLs218.setForeground(Color.white);
		jButtonConnectLs218.setText("Connect to Ls 218 Agent");
	    }
	else {
	    jButtonConnectLs218.setBackground(Color.green);
	    jButtonConnectLs218.setForeground(Color.black);
	    jButtonConnectLs218.setText("Connected to Ls 218 Agent");
	}

	if( monitorLs331==null || !monitorLs331.getName().equals("KeepGoing") || cmdSocLs331Agent==null )
	    { 
		jButtonConnectLs331.setBackground(Color.red);
		jButtonConnectLs331.setForeground(Color.white);
		jButtonConnectLs331.setText("Connect to Ls 331 Agent");
	    }
	else {
	    jButtonConnectLs331.setBackground(Color.green);
	    jButtonConnectLs331.setForeground(Color.black);
	    jButtonConnectLs331.setText("Connected to Ls 331 Agent");
	}

	if( monitorPressure == null || !monitorPressure.getName().equals("KeepGoing") ) 
	    { 
		jButtonConnectPrAg.setBackground(Color.red);
		jButtonConnectPrAg.setForeground(Color.white);
		jButtonConnectPrAg.setText("Connect to Pressure Agent");
	    }
	else {
	    jButtonConnectPrAg.setBackground(Color.green);
	    jButtonConnectPrAg.setForeground(Color.black);
	    jButtonConnectPrAg.setText("Connected to Pressure Agent");
	}
    }
//-------------------------------------------------------------------------------
  /**
   *Common code to send a command string to agent <br>
   *Sets errMsgLabel to response from agent
   *@param command Raw command to send to agent 
   */
  String sendCommand(String command) {
    if (cmdSocLs331Agent == null) {
      jciError.show("Not connected to 331 Agent");
      errMsg.setText("ERR: not connected to 331 Agent");
      return "";
    }
    String []cmdstr = new String[2];
    cmdstr[0] = "RAW";
    cmdstr[1] = command;
    UFStrings ufs = new UFStrings("RAW");
    ufs.setNameAndVals("RAW",cmdstr);
    ufs.sendTo(cmdSocLs331Agent);
    UFProtocol ufp = UFProtocol.createFrom(cmdSocLs331Agent);
    if (ufp instanceof UFStrings) 
      if (((UFStrings)ufp).valData(0).toLowerCase().indexOf("err") != -1)
	  errMsg.setText(((UFStrings)ufp).valData(0));
      else {
	  errMsg.setText("");
	  return ((UFStrings)ufp).valData(0);
      }
    else errMsg.setText("ERR: bad response from agent");
    return "";
  }

//-------------------------------------------------------------------------------
  /**
   *Event handler for jButtonApplyHeater
   *JPanelTemperature Apply Heater button action performed
   *@param e not used
   */
  void jButtonApplyHeater_action (ActionEvent e) {
    int value = heaterComboBox.getSelectedIndex();
    sendCommand("RANGE "+value);
  } //end of jButtonHeater
//-------------------------------------------------------------------------------
  /**
   *Event handler for jButtonQueryHeater
   *JPanelTemperature Apply Heater button action performed
   *@param e not used
   */
  void jButtonQueryHeater_action (ActionEvent e) {
    String ret = sendCommand("RANGE?");
    try {
	heaterComboBox.setSelectedIndex(Integer.parseInt(ret.trim()));
    } catch (Exception ee) {
	System.err.println("JPanelTemperature::jButtonQueryHeater_action> "+
			   "Bad response from agent:  "+ ret);
    }
  } //end of jButtonHeater

//-------------------------------------------------------------------------------
  /**
   *Event handler for jButtonHeaterOff
   *JPanelTemperature emergency heater off button action performed
   *@param e not used
   */
  void jButtonHeaterOff_action (ActionEvent e) {
      heaterComboBox.setSelectedIndex(0);
      jButtonApplyHeater_action(e);
  } //end of jButtonHeater Off
//-------------------------------------------------------------------------------
  /**
   *Event handler for jButtonApplySetP
   *JPanelTemperature apply setp button action performed
   *@param e not used
   */
  void jButtonApplySetP_action (ActionEvent e) {
    sendCommand ("SETP 1," + set_Point.getText());
  } //end of jButtonHeater Off
//-------------------------------------------------------------------------------
  /**
   *Event handler for jButtonQuerySetP
   *JPanelTemperature apply setp button action performed
   *@param e not used
   */
  void jButtonQuerySetP_action (ActionEvent e) {
    set_Point.setText(sendCommand ("SETP? 1"));
  } //end of jButtonHeater Off

//-------------------------------------------------------------------------------
  /**
   *Event handler for jButtonApplyPID
   *JPanelTemperature Apply PID button action performed
   *@param e not used
   */
  void jButtonApplyPID_action (ActionEvent ae) {
    sendCommand("PID 1,"+p_Text.getText()+","+i_Text.getText()+","+d_Text.getText());
  } //end of jButtonApplyPID

//-------------------------------------------------------------------------------
  /**
   *Event handler for jButtonQueryPID
   *JPanelTemperature Query PID button action performed
   *@param e not used
   */
  void jButtonQueryPID_action (ActionEvent ae) {
      try {
	  String ret = sendCommand("PID? 1");
	  StringTokenizer st = new StringTokenizer(ret,",+-");
	  if (st.hasMoreTokens()) p_Text.setText(st.nextToken());
	  if (st.hasMoreTokens()) i_Text.setText(st.nextToken());
	  if (st.hasMoreTokens()) d_Text.setText(st.nextToken());
      } catch (Exception e) {
	  jciError.show("While attempting to query PID values: "+e.toString());
      }
  } //end of jButtonQueryPID
//-------------------------------------------------------------------------------
  /**
   *Reads the temperature parameter file
   *@param filename String: contains the name of the file name used
   */
  public void ReadFile(String filename) {

    TextFile in_file = new TextFile(jci.data_path + filename,TextFile.IN);

    try {
      String line;
      StringTokenizer st;

      line = in_file.nextUncommentedLine();
      int numSensors=0; int maxLabelSize = 0;
      try {
	  numSensors = Integer.parseInt(line);
	  SensorParms = new JCIsensorParameters[numSensors];
      }
      catch (Exception e) { jciError.show("Bad number of sensors parameter"); }

      for (int i=0; i<numSensors; i++) {
        line = in_file.nextUncommentedLine();
        st = new StringTokenizer(line," ");
        // assume that all words are part of the sensor name
        // except the last four, which should be the Hot/Cold Threshold Values, channel name
	// and model number (LakeShore 331 or 218)
        int numTokens = st.countTokens();
        String s = "";
        for (int j=0; j<numTokens-4; j++)
          s += st.nextToken() + " ";
        s = s.substring(0,s.length()-1); // strip off the last space
        if (s.length() > maxLabelSize) maxLabelSize = s.length();
        SensorParms[i] = new JCIsensorParameters (s);
        try { SensorParms[i].hotThreshold = Double.parseDouble(st.nextToken()); }
        catch (Exception e) { jciError.show("Bad Hot Threshold Value: "+e.toString()); }
        try { SensorParms[i].coldThreshold = Double.parseDouble(st.nextToken()); }
        catch (Exception e) { jciError.show("Bad Cold Threshold Value: "+e.toString()); }
	try { SensorParms[i].channel = st.nextToken(); }
	catch (Exception e) { jciError.show("Could not read channel name for sensor "+i+": "+e.toString());}
	try { SensorParms[i].modelNumber = Integer.parseInt(st.nextToken()); }
	catch (Exception e) { jciError.show("Bad model number: " + e.toString()); }
	if (SensorParms[i].modelNumber == 331 && SensorParms[i].channel.equals("A"))
	  SensorParms[i].sValue2 = actualTdet;
      }

      //graph parameters
      line = in_file.nextUncommentedLine();
      st = new StringTokenizer(line," ");
      // create the graph panel object
      jGraphPanel = new JGraphPanel();

      try {
        jGraphPanel.timeMin = Double.parseDouble(st.nextToken());
        jGraphPanel.timeMax = Double.parseDouble(st.nextToken());
        jGraphPanel.tempMin = Double.parseDouble(st.nextToken());
        jGraphPanel.tempMax = Double.parseDouble(st.nextToken());
        jGraphPanel.xTickDelta = Double.parseDouble(st.nextToken());
        jGraphPanel.xLabelDelta = Double.parseDouble(st.nextToken());
        jGraphPanel.yTickDelta = Double.parseDouble(st.nextToken());
        jGraphPanel.yLabelDelta = Double.parseDouble(st.nextToken());
        jGraphPanel.pollPeriod = Integer.parseInt(st.nextToken());
        jGraphPanel.maxDataPoints = Integer.parseInt(st.nextToken());
      } catch (Exception e) {jciError.show("Bad graph Parameters"); }

      in_file.close();
    }
    catch (Exception e) {
      jciError.show("Couldn't read temperature data file: "+e.toString());
    }
  } //end of ReadFile

//-------------------------------------------------------------------------------
 /**
  *Event Handler for jGraphLoadButton
  *JPanelTemperature Graph load button action performed
  *@param e not used
  */
 void jGraphLoadButton_action (ActionEvent e) {
    int counter =0;
    try {
      JFileChooser fc = new JFileChooser(new File(jci.data_path));
      ExtensionFilter t_graph_type = new ExtensionFilter("Temperature graph data files", new String[]{".tgd"});
      fc.setFileFilter(t_graph_type); // Initial filter setting
      fc.setDialogTitle("Load graph data from");
      fc.setSelectedFile(new File(tgd_filename));
      fc.setApproveButtonText("Load");
      int returnVal = fc.showOpenDialog(null);
      if (returnVal == JFileChooser.APPROVE_OPTION) {
        String t_file_name = fc.getSelectedFile().getAbsolutePath();
      	if( !t_file_name.endsWith(".tgd") ) t_file_name += ".tgd";
        TextFile inpGraphData = new TextFile(t_file_name,TextFile.IN);
        jGraphPanel.pollPeriod = Integer.parseInt( inpGraphData.nextUncommentedLine() );
        counter = 0;
        jGraphPanel.size = 0;
        String line = "";
        for (int i=0; i<SensorParms.length; i++) {
	    jGraphPanel.xValues[i].clear();
	    jGraphPanel.yValues[i].clear();
	}
        while((line = inpGraphData.nextUncommentedLine()) != null) {
	  if (line.trim().equals("")) break;
          StringTokenizer stY = new StringTokenizer(line);
	  String timeStamp = stY.nextToken();
          for (int i=0; i<SensorParms.length; i++) {
            if( jGraphPanel.size > jGraphPanel.maxDataPoints ) {
		jGraphPanel.yValues[i].removeFirst();
		jGraphPanel.xValues[i].removeLast();
            }
	    String nextok = stY.nextToken();
	    if( nextok.indexOf(":") > 0 ) nextok = stY.nextToken(); //skip another timestamp.
	    jGraphPanel.yValues[i].addLast( new Integer((int)(Double.parseDouble( nextok )*100)) );
	    jGraphPanel.xValues[i].addFirst( new Integer( jGraphPanel.pollPeriod*counter*100 ) );
          }
          counter++;
          jGraphPanel.size++;
        }
        inpGraphData.close();
        tgd_filename = t_file_name;
	repaint();
      }
    }
    catch (Exception ee) {
       //jciFrameErrorLog.addError("JPanelTemperature","jGraphLoadButton_action()",ee.toString());
       jciError.show("problem reading temperature data file : " + counter + " " +ee.toString());
    }
  } //end of jGraphLoadButton_action

//-------------------------------------------------------------------------------
  /**
   *Event handler for JGraphParamButton
   *JPanelTemperature Graph parameters button action performed
   *@param e not used
   */
  void jGraphParamButton_action (ActionEvent e) {
    Point origin = getLocation();
    Dimension totalSize = getSize();
    dialogBox.graphParams[8].setText(jGraphPanel.pollPeriod + "");
    dialogBox.graphParams[9].setText(jGraphPanel.maxDataPoints + "");
    dialogBox.setLocation((int)(origin.getX() + totalSize.getWidth()/2),
                           (int)(origin.getY() + totalSize.getHeight()/2));
    dialogBox.setVisible(true);
    repaint();
  } //end of jGraphParamButton

//-------------------------------------------------------------------------------
  /**
   *Sounds the alarm
   *@param e not used
   */
  void alarm_action(ActionEvent e){
    SensorParms[1].soundAlarm();
  } //end of alarm_action

//-------------------------------------------------------------------------------
  /**
   *Event handler for jButtonPID
   *JPanelTemperature PID button action performed
   *@param e not used
   */
  void jButtonPID_action() {
    if (p_Label.isShowing()){
      p_Label.setVisible(false);
      i_Label.setVisible(false);
      d_Label.setVisible(false);
      p_Text.setVisible(false);
      i_Text.setVisible(false);
      d_Text.setVisible(false);
      jButtonPID.setText("Show PID");
      jPanelPIDButtons.remove(jButtonApplyPID);
      jPanelPIDButtons.remove(jButtonQueryPID);
      jPanelPIDButtons.repaint();
    }
    else {
      p_Label.setVisible(true);
      i_Label.setVisible(true);
      d_Label.setVisible(true);
      p_Text.setVisible(true);
      i_Text.setVisible(true);
      d_Text.setVisible(true);
      jButtonPID.setText("Hide  PID");
      jPanelPIDButtons.add(jButtonApplyPID);
      jPanelPIDButtons.add(jButtonQueryPID);
      jPanelPIDButtons.repaint();
    }
    jPanelLeft.repaint();
    jPanelTempControl.repaint();
  } //end of jButtonPID_action

//-------------------------------------------------------------------------------
    /**
   *Event handler for jGraphStartButton
   *JPanelTemperature start button action performed
   *@param e not used
   */
    void jGraphStartButton_action(ActionEvent e) {
    if (graphThread.isAlive()) {
      graphThread.keepRunning = false;
      jGraphStartButton.setText("Start Graph");
      graphDataFile.close();
    }
    else {
      try {
        JFileChooser fc = new JFileChooser(jci.data_path);
        ExtensionFilter t_graph_type = new ExtensionFilter("Temperature graph data files", new String[] {".tgd"});
        fc.setFileFilter(t_graph_type); // Initial filter setting
        fc.setDialogTitle("Save graph data to");
        fc.setSelectedFile(new File(tgd_filename));
        fc.setApproveButtonText("Save");
        int returnVal = fc.showSaveDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
          graphThread = new GraphThread();
          String t_file_name = fc.getSelectedFile().getAbsolutePath();
          if( !t_file_name.endsWith(".tgd") ) t_file_name += ".tgd";
          graphDataFile = new TextFile(t_file_name,TextFile.OUT);
          graphDataFile.println("; Temperature Graph Data file");
          graphDataFile.println(";");
          graphDataFile.println("; Poll Rate");
          graphDataFile.println(jGraphPanel.pollPeriod+"");
          graphDataFile.println(";");
	  String title = "; Channels:";
	  for( int i=0; i<SensorParms.length; i++ )
	      title += ("     " + SensorParms[i].modelNumber + "-" + SensorParms[i].channel);
          graphDataFile.println( title + "    Pressure");
          graphDataFile.println(";");
          tgd_filename = t_file_name;

          graphThread.start();
          jGraphStartButton.setText("Stop Graph");
        }
          /*for (int i=0; i<SensorParms.length; i++) {
            ListIterator lit = jGraphPanel.xValues[i].listIterator(0);
            String s = "";
            while (lit.hasNext()) {
              s += ((Integer)lit.next()).toString();
              if (lit.hasNext()) s += " ";
            }
            graphDataFile.println(s);
            lit = jGraphPanel.yValues[i].listIterator(0);
            s = "";
            while (lit.hasNext()) {
              s += ((Integer)lit.next()).toString();
              if (lit.hasNext()) s += " ";
            }
            graphDataFile.println(s);
          }
          graphDataFile.close();
        }   */
      }
      catch (Exception ee) {
        //jciFrameErrorLog.addError("JPanelTemperature","jGraphStartButton_action()",ee.toString());
      }
    }
  } //end of jGraphStartButton_action

//===============================================================================
    /**
     *Object storing and handling temperature sensor parameters
     */
    public class JCIsensorParameters
    {
	JTextField sValue;
	JTextField sValue2;
	JLabel sName;
	JLabel units = new JLabel("K");
	JTextField statusColor = new JTextField("",2);
	GraphElement graphElement = new GraphElement();
	double currentTemp;
	double hotThreshold;
	double coldThreshold;
	String channel;
	String timeStamp;
	long milliSeconds;
	int modelNumber;
//-------------------------------------------------------------------------------
	/**
	 *Constructor for JCIsensorParameters
	 */
	JCIsensorParameters( String name ) {
	    sName = new JLabel(name);
	    sValue = new JTextField();
	    sValue2 = null;
	    statusColor.setBackground(new Color(255,0,0));
	    statusColor.setEditable(false);
	    sValue.setEditable(false);
	    sValue.setBackground(new Color(233,233,233));

	    sValue.getDocument().addDocumentListener(new DocumentListener() {
		    public void insertUpdate(DocumentEvent e) {
			setTemperature (e);
		    }
		    public void removeUpdate(DocumentEvent e) {
			//setTemperature (e);
		    }
		    public void changedUpdate(DocumentEvent e) {
			//setTemperature (e);
		    }
		});

	    currentTemp = 0;
	    hotThreshold = 0;
	    coldThreshold = 0;
	    channel = "0";
	    modelNumber = 0;
	}
//-------------------------------------------------------------------------------
	/**
	 *Sets the temperature
	 *@param e not used
	 */
	public void setTemperature(DocumentEvent e) {
	    String s = sValue.getText();
	    if (sValue2 != null) sValue2.setText(s);
	    if( s.trim().length() > 0 ) {
		try { currentTemp = Double.parseDouble(s); }
		catch (Exception ex) { } //jciError.show("Bad Temperature Reading"); }
		checkThreshold();
	    }
	}
//-------------------------------------------------------------------------------

	public void checkThreshold()
	{
	    if (currentTemp > hotThreshold)
		statusColor.setBackground( Color.red );
	    else {
		if (currentTemp < coldThreshold)
		    statusColor.setBackground( Color.blue );
		else
		    statusColor.setBackground( Color.yellow );
	    }
	}
//-------------------------------------------------------------------------------
	/**
	 *Sounds the Alarm
	 */
	public void soundAlarm () {
	    URL codeBase;
	    try {
		codeBase = new URL("file:" + System.getProperty("user.dir") + "/alert.au");
		AudioClip aa = Applet.newAudioClip(codeBase);
		aa.play();
	    }
	    catch (MalformedURLException e) {
		System.err.println(e.getMessage());
	    }
	} //end of soundAlarm
    } //end of class JCIsensorParameters

//===============================================================================

  public class GraphThread extends Thread {
    Timestamp timestamp;
    boolean keepRunning = true;
    int sleepamount = jGraphPanel.pollPeriod;
    int counter = 0;

//-------------------------------------------------------------------------------
    /**
     *Displays line on the graph when start graph is clicked
     *logs the graph data in the .tgd file
     *
     */
    public void run() {
      counter = 0;
      while( keepRunning && (counter<32768) )
        try {
	    graphDataFile.print( SensorParms[0].timeStamp.substring(0,21) + " ");
	    int modnum = SensorParms[0].modelNumber;
	    if (jGraphPanel.size < jGraphPanel.maxDataPoints)
		jGraphPanel.size++;
	    for( int i=0; i<SensorParms.length; i++ ) {
		if( i > 0 && modnum != SensorParms[i].modelNumber ) {
		    modnum = SensorParms[i].modelNumber;
		    graphDataFile.print( SensorParms[i].timeStamp.substring(0,21) + " ");
		}
		graphDataFile.print( SensorParms[i].currentTemp + " ");
		if (jGraphPanel.size == jGraphPanel.maxDataPoints && counter >= jGraphPanel.size) {
		    //jGraphPanel.xValues[i].removeLast();
		    jGraphPanel.yValues[i].removeFirst();
		}
		// Add x values in reverse order because of strip chart style graph
		// Add Integers since linkedlists can only hold objects, not primitives.
		// Scale by 100 in order to preserve precision
                if (jGraphPanel.size > counter)
		  jGraphPanel.xValues[i].addFirst( new Integer( sleepamount*counter*100 ) );
		jGraphPanel.yValues[i].addLast( new Integer((int)(SensorParms[i].currentTemp*100)) );
	    }
	    graphDataFile.print( PressureSensor.timeStamp.substring(0,21) + " ");
	    graphDataFile.println( PressureSensor.sValue.getText().trim() );
	    //    timestamp = new Timestamp((new Date()).getTime());
	    //    graphDataFile.println( timestamp.toString() );
	    repaint();
	    if( sleepamount <= 0 )
		keepRunning = false;
	    else
		this.sleep(sleepamount*1000);
	    counter++;
	    //System.out.println(jGraphPanel.size+"");
	}
      catch (Exception e) {
	  System.err.println("JPanelTemperature::GraphThread::run> "+e.toString());
      }
    }
  } //end of class GraphThread

//===============================================================================
  /**
   *Handles the Graph on the JPanelTemperature
   */
  class JGraphPanel extends JPanel {
    // array of linked lists to store data points for the sensors.
    LinkedList [] xValues = new LinkedList[SensorParms.length];
    LinkedList [] yValues = new LinkedList[SensorParms.length];
    //number of points currently stored.
    int size;
    //maximum number of points
    int maxDataPoints;
    int panelWidth, panelHeight, panelMinX, panelMinY;
    double timeMin, timeMax, tempMin, tempMax;
    double xTickDelta, xLabelDelta, yTickDelta, yLabelDelta;
    int pollPeriod;
//-------------------------------------------------------------------------------
    /**
     *Constructor Function for JGraphPanel
     */
    JGraphPanel() {
      size = 0; maxDataPoints = 2000;
      xTickDelta = 60; yTickDelta = 10;
      xLabelDelta = 120; yLabelDelta = 20;
      panelMinX = 10; panelMinY = 10;
      tempMin = 0; tempMax = 100;
      timeMin = 0; timeMax = 600;
      pollPeriod = 5;
      for (int i=0; i<SensorParms.length; i++) {
        xValues[i] = new LinkedList();
        yValues[i] = new LinkedList();
      }
    } //end of JGraphPanel constructor fn

//-------------------------------------------------------------------------------
      /**
       *Clears the xValues array and the yValues array for the graph data
       */
      public void eraseGraphData () {
         size = 0;
         for (int i=0; i<SensorParms.length; i++) {
           xValues[i].clear();
           yValues[i].clear();
         }
         jciFrame.jPanelTemperature.repaint();
      } //end of eraseGraphData

//-------------------------------------------------------------------------------
      /**
       *reverses ??
       *@param arrayit TBD
       */
      public int[] reverse (int [] arrayit) {
        int [] temper = new int[arrayit.length];
        int k = arrayit.length-1;
        for (int i=0; i<arrayit.length; i++) {
            temper[k--] = arrayit[i];
        }
        return temper;
      } //end of reverse

//-------------------------------------------------------------------------------
      /**
       *TBD
       *@param x double x coordinate
       *@param y double y coordinate
       */
      public Point xform(double x, double y) {
         final double scr_org_x = panelWidth;
         final double scr_org_y = panelHeight;
         final double scr_x_per_x = -( panelWidth-panelMinX) / (timeMax-timeMin);
         final double scr_y_per_y = -( panelHeight-panelMinY)/ (tempMax-tempMin);
         y -= tempMin; x -= timeMin;
         Point scr_loc = new Point();
         scr_loc.x = Math.max((int)(scr_org_x + x * scr_x_per_x), panelMinX);
         scr_loc.y = Math.max((int)(scr_org_y + y * scr_y_per_y), panelMinY);
         scr_loc.y = Math.min( scr_loc.y, panelHeight );
         return scr_loc;
      } //end of xform

//-------------------------------------------------------------------------------
      /**
       *TBD
       *@param x double x coordinate
       *@param y double y coordinate
       */
      public Point xform2(double x, double y)
      {
         final double scr_org_x = 0;
         final double scr_org_y = panelHeight;
         final double scr_x_per_x = (panelWidth) / (timeMax-timeMin);
         final double scr_y_per_y = -( panelHeight-panelMinY) / (tempMax-tempMin);
         y -= tempMin;// x += timeMin;
         Point scr_loc = new Point();
         scr_loc.x = (int)(scr_org_x + x * scr_x_per_x);
         scr_loc.y = Math.max((int)(scr_org_y + y * scr_y_per_y), panelMinY);
         scr_loc.y = Math.min( scr_loc.y, panelHeight );
         return scr_loc;
      } //end of xform2

//-------------------------------------------------------------------------------
      /**
       *TBD
       *@param g Graphics TBD
       */
      public synchronized void paint(Graphics g) {
        panelWidth = (int)this.getWidth() - 38;
        panelHeight = (int)this.getHeight() - 18;
        g.setFont(new Font(g.getFont().getName(),g.getFont().getStyle(),10));
        g.drawLine( panelMinX, panelHeight, panelWidth, panelHeight );
        g.drawLine( panelWidth, panelMinY, panelWidth, panelHeight );

        double d = timeMin;
        while (d <= timeMax) {
	    Point p1 = xform(d,1); Point p2 = xform(d,-1);
	    if( jCheckBoxGraphGrid.isSelected() ) {
		g.setColor( Color.gray );
		g.drawLine( p1.x, panelHeight, p2.x, 0 );
	    }
	    g.setColor( Color.black );
	    g.drawLine( p1.x, panelHeight+1, p2.x, panelHeight-1 );
	    d+=xTickDelta;
        }

        d = timeMin;
        while (d <=timeMax) {
          Point p1 = xform(d,-15);
	  g.drawString( String.valueOf(-d), p1.x-9, panelHeight+15 );
          d+=xLabelDelta;
        }

        double y = tempMin;
        while (y <= tempMax) {
	    if( jCheckBoxGraphGrid.isSelected() ) {
		g.setColor( Color.gray );
		g.drawLine( panelMinX, xform2(panelMinX+1,y).y, panelWidth, xform2(panelMinX-1,y).y );
	    }
	    g.setColor( Color.black );
	    g.drawLine( panelWidth-1, xform2(0,y).y, panelWidth+1, xform2(0,y).y );
	    y+=yTickDelta;
        }

        y = tempMin;
        while (y <= tempMax) {
          g.drawString( String.valueOf(y), panelWidth+6, xform2(0,y).y+3 );
          y+=yLabelDelta;
        }

        int []tempx = new int[size];
        int []tempy = new int[size];
	Point srcLoc;
	// Compute screen Locations and use polylines to draw the graphs.

        for (int i=0; i<SensorParms.length; i++) {
          if (SensorParms[i].graphElement.showme.isSelected()) {
             for (int j=0; j<size; j++){
                srcLoc = xform( ((Integer)xValues[i].get(j)).intValue()/100.0,
				((Integer)yValues[i].get(j)).intValue()/100.0 );
                tempx[j] = srcLoc.x;
                tempy[j] = srcLoc.y;
             }
	     g.setColor( SensorParms[i].statusColor.getBackground() );
             g.drawPolyline(tempx,tempy,size);
          }
        }

	g.setColor( Color.black );
      } //end of paint
   } //end of class JGraphPanel

//===============================================================================
   /**
    *TBD
    */
   public class GraphElement {
     JTextField rate = new JTextField ("0",5);
     JLabel units = new JLabel ("K/min");
     JCheckBox showme = new JCheckBox ();

//-------------------------------------------------------------------------------
     /**
      *GraphElement constructor
      */
     GraphElement () {
       showme.setSelected(false);
       rate.setEditable(false);
       rate.setBackground(new Color(175,175,175));
       showme.addActionListener(
           new java.awt.event.ActionListener() {
              public void actionPerformed(ActionEvent e) {
                 repaint();
           }
       });

     } //end of GraphElement

   } //end of class GraphElement

//===============================================================================
   /**
    *TBD
    */
  public class JDialogGraphParameters extends JDialog {
    JPanel labelpanel = new JPanel();
    JPanel textpanel = new JPanel();
    JPanel buttonpanel = new JPanel();
    JButton buttonOK = new JButton ("OK");
    JButton buttonCancel = new JButton ("Cancel");
    JTextField [] graphParams = new JTextField[10];
    double timeMin,timeMax,tempMin,tempMax;
    double xTickDelta,xLabelDelta,yTickDelta,yLabelDelta;

//-------------------------------------------------------------------------------
      /**
       *Default Constructor
       */
      JDialogGraphParameters () {
        super();
        enableEvents(AWTEvent.WINDOW_EVENT_MASK);
        this.setModal(true);
        try  {
          jbInit();
        }
        catch(Exception e) {
          e.printStackTrace();
        }
      } //end of JDialogGraphParameters
//-------------------------------------------------------------------------------
      /**
       *Component Initialization
       */
      private void jbInit () {
        buttonOK.addActionListener(new java.awt.event.ActionListener() {
          public void actionPerformed(ActionEvent e) {
             buttonOK_action(e);
          }
        });
        buttonCancel.addActionListener(new java.awt.event.ActionListener() {
          public void actionPerformed(ActionEvent e) {
             buttonCancel_action(e);
          }
        });
        labelpanel.setLayout(new GridLayout(0,1,0,10));
        textpanel.setLayout(new GridLayout(0,1,0,10));
        buttonpanel.setLayout(new FlowLayout());
        this.getContentPane().setLayout (new BorderLayout());
        labelpanel.add(new JLabel());
        labelpanel.add(new JLabel("Min Time"));
        labelpanel.add(new JLabel("Max Time"));
        labelpanel.add(new JLabel("Min Temp"));
        labelpanel.add(new JLabel("Max Temp"));
        labelpanel.add(new JLabel("x Tick Delta"));
        labelpanel.add(new JLabel("x Label Delta"));
        labelpanel.add(new JLabel("y Tick Delta"));
        labelpanel.add(new JLabel("y Label Delta"));
        labelpanel.add(new JLabel("Graph Poll Period"));
        labelpanel.add(new JLabel("Max Data Points"));
        textpanel.add(new JLabel());
        for (int i=0; i<10; i++) {
           graphParams[i] = new JTextField("0.0",5);
           textpanel.add(graphParams[i]);
        }
        graphParams[0].setText(jGraphPanel.timeMin + "");
        graphParams[1].setText(jGraphPanel.timeMax + "");
        graphParams[2].setText(jGraphPanel.tempMin + "");
        graphParams[3].setText(jGraphPanel.tempMax + "");
        graphParams[4].setText(jGraphPanel.xTickDelta + "");
        graphParams[5].setText(jGraphPanel.xLabelDelta + "");
        graphParams[6].setText(jGraphPanel.yTickDelta + "");
        graphParams[7].setText(jGraphPanel.yLabelDelta + "");
        graphParams[8].setText(jGraphPanel.pollPeriod + "");
        graphParams[9].setText(jGraphPanel.maxDataPoints + "");
        buttonpanel.add(buttonOK);
        buttonpanel.add(buttonCancel);
        this.getContentPane().add(labelpanel,BorderLayout.WEST);
        this.getContentPane().add(textpanel,BorderLayout.EAST);
        this.getContentPane().add(buttonpanel,BorderLayout.SOUTH);
        this.setTitle("Graph Parameters");
        buttonOK.grabFocus();
        this.setSize(new Dimension(200,350));
      } //end of jbInit
//-------------------------------------------------------------------------------
      /**
       *Event handler for buttonOK
       *JDialogGraphParameters ok button action performed
       *@param e not used
       */
      void buttonOK_action(ActionEvent e) {
        int oldRate = jGraphPanel.pollPeriod;
        int newRate = Integer.parseInt(graphParams[8].getText());
        int oldMaxPoints = jGraphPanel.maxDataPoints;
        int newMaxPoints = Integer.parseInt(graphParams[9].getText());
        int i = -1;
        if (oldRate != newRate || oldMaxPoints != newMaxPoints) {
          i = JOptionPane.showConfirmDialog(null,"These changes will erase graph data",
					    "Warning",JOptionPane.OK_CANCEL_OPTION);
          if (i == JOptionPane.CANCEL_OPTION) return;
          else jGraphPanel.eraseGraphData();
        }
        jGraphPanel.timeMin = Double.parseDouble(graphParams[0].getText());
        jGraphPanel.timeMax = Double.parseDouble(graphParams[1].getText());
        jGraphPanel.tempMin = Double.parseDouble(graphParams[2].getText());
        jGraphPanel.tempMax = Double.parseDouble(graphParams[3].getText());
        jGraphPanel.xTickDelta = Double.parseDouble(graphParams[4].getText());
        jGraphPanel.xLabelDelta = Double.parseDouble(graphParams[5].getText());
        jGraphPanel.yTickDelta = Double.parseDouble(graphParams[6].getText());
        jGraphPanel.yLabelDelta = Double.parseDouble(graphParams[7].getText());
        jGraphPanel.pollPeriod = Integer.parseInt(graphParams[8].getText());
        jGraphPanel.maxDataPoints = Integer.parseInt(graphParams[9].getText());
        this.setVisible(false);
      } //end of buttonOK_action
//-------------------------------------------------------------------------------
      /**
       *Event handler for buttonCancel
       *JDialogGraphParameters cancel button action performed
       *@param e not used
       */
      void buttonCancel_action(ActionEvent e) {
        this.setVisible(false);
        graphParams[0].setText(jGraphPanel.timeMin + "");
        graphParams[1].setText(jGraphPanel.timeMax + "");
        graphParams[2].setText(jGraphPanel.tempMin + "");
        graphParams[3].setText(jGraphPanel.tempMax + "");
        graphParams[4].setText(jGraphPanel.xTickDelta + "");
        graphParams[5].setText(jGraphPanel.xLabelDelta + "");
        graphParams[6].setText(jGraphPanel.yTickDelta + "");
        graphParams[7].setText(jGraphPanel.yLabelDelta + "");
        graphParams[8].setText(jGraphPanel.pollPeriod + "");
        graphParams[9].setText(jGraphPanel.maxDataPoints + "");
      } //end of buttonCancel_action
//-------------------------------------------------------------------------------

      protected void processWindowEvent(WindowEvent e) {
        if(e.getID() == WindowEvent.WINDOW_CLOSING) {
          this.dispose();
        }
        super.processWindowEvent(e);
      } //end of processWindowEvent

   } //end of class JDialogGraphParameters

} //end of class JPanelTemperature






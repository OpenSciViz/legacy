//Title:        UFHostPortPanel for Java Control Interface (JCI)
//Version:      (see rcsID)
//Copyright:    Copyright (c) 2003
//Author:       Frank Varosi
//Company:      University of Florida
//Description:  for control and monitor of CanariCam infrared camera system.

package javaUFLib;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.JTextField;

//===============================================================================
/**
 * Creates 2 text fields with Labels on left for entering and storing Hostname and Port #,
 * all in one Panel, with methods for accessing and setting the values.
 */

public class UFHostPortPanel extends JPanel
{
    public static final
	String rcsID = "$Name:  $ $Id: UFHostPortPanel.java,v 1.7 2004/03/30 04:42:26 varosi Exp $";

    protected String host = "";
    protected int port = 0;

    UFTextField hostName;
    UFTextField portNum;

    JCheckBox checkBoxHost = new JCheckBox("Host :",false);
    JCheckBox checkBoxPort = new JCheckBox("Port :",false);

    boolean userEvent = false;
//-------------------------------------------------------------------------------
    /**
     *Default Constructor
     */
    public UFHostPortPanel() {
	try  {
	    jbInit();
	}
	catch(Exception ex) {
	    System.out.println("Error creating UFHostPortPanel: " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor with values.
     */
    public UFHostPortPanel( String hostname, int portnum ) {
	try  {
	    this.host = hostname;
	    this.port = portnum;
	    jbInit();
	}
	catch(Exception ex) {
	    System.out.println("Error creating UFHostPortPanel: " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     *Component initialization
     */
    private void jbInit() throws Exception
    {
	hostName = new UFTextField( "Host", this.host, 4 );
	portNum = new UFTextField( "Port", Integer.toString( this.port ), 4 );

	hostName.setEditable(false);
	hostName.setBackground(new Color(240,230,220));
	portNum.setEditable(false);
	portNum.setBackground(new Color(240,230,220));

	checkBoxHost.addActionListener(new java.awt.event.ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    if (checkBoxHost.isSelected()) {
			hostName.setBackground(Color.white);
			hostName.setEditable(true);
		    }
		    else {
			hostName.setEditable(false);
			hostName.setNewState();
			hostName.setBackground(new Color(240,230,220));
		    }
		}
	    });

	checkBoxPort.addActionListener(new java.awt.event.ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    if (checkBoxPort.isSelected()) {
			portNum.setBackground(Color.white);
			portNum.setEditable(true);
		    }
		    else {
			portNum.setEditable(false);
			portNum.setNewState();
			portNum.setBackground(new Color(240,230,220));
		    }
		}
	    });

	this.setLayout(new RatioLayout());
	this.add("0.00,0.1;0.24,0.8", checkBoxHost );
	this.add("0.24,0.1;0.24,0.8", hostName );
	this.add("0.51,0.1;0.24,0.8", checkBoxPort );
	this.add("0.75,0.1;0.24,0.8", portNum );
    }
//-------------------------------------------------------------------------------

    public void setHostAndPort( String host, int port ) {
	setHost( host );
	setPort( port );
    }

    public void setHost(String host) {
	if( host.trim().length() > 0 ) {
	    this.host = host.trim();
	    hostName.setText( this.host );
	}
    }

    public void setPort(int port) {
	if( port > 0 ) {
	    this.port = port;
	    portNum.setText( Integer.toString( this.port ) );
	}
    }

    public String getHost() { return this.host; }
    public int getPort() { return this.port; }

    public String getHostField() {
	this.host = hostName.getText().trim();
	return this.host;
    }

    public int getPortField() {
	this.port = Integer.parseInt( portNum.getText().trim() );
	return this.port;
    }
}

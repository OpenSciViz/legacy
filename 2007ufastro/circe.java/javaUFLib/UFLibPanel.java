//Title:        UFLibPanel
//Version:      (see rcsID)
//Copyright:    Copyright (c) 2003
//Author:       Frank Varosi and David Rashkin
//Company:      University of Florida
//Description:  generic extension of JPanel, used by other UF tools and Java Control Interface (JCI)

package javaUFLib;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;
import java.net.*;
import java.util.*;
import javaUFProtocol.*;

//===============================================================================
/**
 * Generic extension of JPanel, so that certain methods/buttons can be used by UFLib components,
 * or by other UF tools and the Java Control Interface (JCI) for CanariCam.
 * @author Frank Varosi
 */

public class UFLibPanel extends JPanel
{
    public static final
	String rcsID = "$Name:  $ $Id: UFLibPanel.java,v 1.11 2004/07/13 01:20:16 varosi Exp $";

    protected static final int DEFAULT_TIMEOUT = 9000;
    protected static final int CONNECT_TIMEOUT = 1700;
    protected static final int HANDSHAKE_TIMEOUT = 2300;
    protected int socTimeout = DEFAULT_TIMEOUT;

    protected Socket agentSocket;
    protected String agentHost = "";
    protected int agentPort = 0;

    protected String className = this.getClass().getName();
    protected String clientName = className;
    protected String LocalHost;

    public JPanel connectPanel = new JPanel();
    public JButton connectButton = new JButton("Connect");
    UFHostPortPanel hostPortPanel = new UFHostPortPanel( agentHost , agentPort );

    //Used to indicate to getNewParams() that this object already has new params,
    // if object gets notified of new params available.
    public boolean gotParams = false;

    // agent Transaction info (added at bottom of frame):
    public UFLabel statusAction   = new UFLabel("Action___:");
    public UFLabel statusResponse = new UFLabel("Response:");
//-------------------------------------------------------------------------------
    /**
     * UFLibPanel default constructor.
     * Calls private method, createPanel(), which does all component initialization.
     */
    public UFLibPanel() {
	try {
	    createPanel();
	}
	catch (Exception e) { e.printStackTrace(); }
    }
//-------------------------------------------------------------------------------
    public UFLibPanel( String host ) {
	try {
	    setHost( host );
	    createPanel();
	}
	catch (Exception e) { e.printStackTrace(); }
    }
//-------------------------------------------------------------------------------
    public UFLibPanel( String host, int port ) {
	try {
	    setHost( host );
	    setPort( port );
	    createPanel();
	}
	catch (Exception e) { e.printStackTrace(); }
    }
//-------------------------------------------------------------------------------
    /**
     *Component creation
     */
    protected void createPanel() throws Exception
    {
	connectButton.addActionListener(new ActionListener()
	    {
		public void actionPerformed(ActionEvent e) {
		    if( connectToAgent() ) getNewParams();
		}
	    });

	connectPanel.setLayout(new GridLayout(2,1));
	connectPanel.add( connectButton );  
	connectPanel.add( hostPortPanel );
    }
//-------------------------------------------------------------------------------
    /**
     * UFLibPanel#Connect button action performed
     *  Connect via socket to the Detector Control Agent,
     *  allowing direct engineering access to detector control parameters.
     */
    public boolean connectToAgent()
    {
	connectButton.setText("Connecting");

	if( agentSocket != null ) { // close connection if one exists
	    try {
		agentSocket.close();
		indicateConnectStatus(false);
	    }
	    catch (IOException ioe) {
		String message = "connectToAgent> " + ioe.toString();
		statusAction.setText( message );
		System.err.println(className + "::" + message);
	    }
	}

	try {
	    agentHost = getHostField();
	    agentPort = getPortField();
	    String message = "connectToAgent> port=" + agentPort + " @ host = " + agentHost;
	    statusAction.setText( message );
	    System.out.println( className + "::" + message );
	    InetSocketAddress agentIPsoca = new InetSocketAddress( agentHost, agentPort );
	    agentSocket = new Socket();
	    agentSocket.connect( agentIPsoca, CONNECT_TIMEOUT );
	    agentSocket.setSoTimeout( HANDSHAKE_TIMEOUT );

	    //must send agent a timestamp with the client name in it
	    UFTimeStamp uft = new UFTimeStamp(clientName);

	    if( uft.sendTo(agentSocket) <= 0 ) {
	        connectError("Handshake Send");
		return false;
	    }

	    //get response from agent
	    UFProtocol ufp = null;

	    if( (ufp = UFProtocol.createFrom(agentSocket)) == null ) {
	        connectError("Handshake Read");
		return false;
	    }

	    message = "Agent handshake> " + ufp.name();
	    statusResponse.setText( message );
	    System.out.println( message );
	    indicateConnectStatus(true);
	    InetAddress LocalInet = agentSocket.getLocalAddress();
	    LocalHost = LocalInet.getHostName();
	    //set normal timeout for socket
	    agentSocket.setSoTimeout( socTimeout );
	    return true;
	}
	catch (Exception x) {
	    indicateConnectStatus(false);
	    agentSocket = null;
	    String message = "connectToAgent> " + x.toString();
	    statusAction.setText( message );
	    System.err.println(className + "::" + message);
	    Toolkit.getDefaultToolkit().beep();
	    return false;
	}
    }
//-------------------------------------------------------------------------------

    public void connectError( String errmsg )
    {
	String message = "connectToAgent> " + errmsg + " ERROR";
	statusResponse.setText( message );
	System.err.println(className + "::" + message);
	Toolkit.getDefaultToolkit().beep();
	indicateConnectStatus(false);
	try {
	    agentSocket.close();
	    agentSocket = null;
	}
	catch (IOException ioe) {
	    message = "connectToAgent> " + ioe.toString();
	    statusAction.setText( message );
	    System.err.println(className + "::" + message);
	}
    }
//-------------------------------------------------------------------------------

    public void indicateConnectStatus( boolean connStatus )
    {
	if( connStatus ) {
	    connectButton.setBackground(Color.green);
	    connectButton.setForeground(Color.black);
	    connectButton.setText("Connected to Agent");
	}
	else { 
	    connectButton.setBackground(Color.red);
	    connectButton.setForeground(Color.white);
	    connectButton.setText("Re-Connect to Agent");
	}
    }
//-------------------------------------------------------------------------------

    public void setHostAndPort( String host, int port ) {
	setHost( host );
	setPort( port );
    }

    public void setHost(String host) {
	agentHost = host;
	hostPortPanel.setHost( host );
    }

    public void setPort(int port) {
	agentPort = port;
	hostPortPanel.setPort( port );
    }

    public String getHost() { return agentHost; }
    public int getPort() { return agentPort; }

    public String getHostField() {
	agentHost = hostPortPanel.getHostField();
	return agentHost;
    }

    public int getPortField() {
	agentPort = hostPortPanel.getPortField();
	return agentPort;
    }

    public void setSocTimeout(int timeout) {
	socTimeout = timeout;
	try {
	    agentSocket.setSoTimeout( socTimeout );
	} catch (SocketException se) {
	    System.err.println(className+"setSocTimeout> "+se.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     * UFLibPanel#getNewParams
     * send STATUS => FULL directive to agent,
     */
    public void getNewParams()
    {
	String[] DCstatus = new String[2];
	DCstatus[0] = "STATUS";
	DCstatus[1] = "FULL";
	UFStrings ufsDCstatus = new UFStrings( "STATUS", DCstatus );
	commandAgent( ufsDCstatus );
    }
//-------------------------------------------------------------------------------
    /**
     * UFLibPanel#getNewParams
     * send STATUS => FULL directive to agent, if this client has not done it yet.
     */
    public void getNewParams( String callerName )
    {
	if( gotParams ) {     //this object already has new params.
	    gotParams = false;
	    if( callerName != clientName ) return; //skip if not this clients request.
	}

	String[] DCstatus = new String[2];
	DCstatus[0] = "STATUS";
	DCstatus[1] = "FULL";
	UFStrings ufsDCstatus = new UFStrings( "STATUS", DCstatus );
	commandAgent( ufsDCstatus, callerName );
    }
//-------------------------------------------------------------------------------
    /**
     * UFLibPanel#commandAgent
     *@param agentRequest : UFStrings object containing request to send to DC agent.
     */
    public void commandAgent( UFStrings agentRequest )
    {
	commandAgent( agentRequest, clientName );
    }
//-------------------------------------------------------------------------------
    /**
     * UFLibPanel#commandAgent
     *@param agentRequest : UFStrings object containing request to send to DC agent.
     *@param callerName   : String = name of client invoking this method.
     */
    public void commandAgent( UFStrings agentRequest, String callerName )
    {
	final String panel = callerName + ":";
	final String action = agentRequest.name();
	agentRequest.rename( panel + action );
	final UFStrings AgentRequest = agentRequest;
	String msg = " sending " + action + " directive to DC agent...";
	System.out.println( panel + msg );
	statusAction.setText( msg );
	statusResponse.setText(" waiting for response... ");

	//create new task an put on the event thread queue
	// so it will be invoked after current action completes (so above setText will be repainted):

	Runnable cmd_agent_task = new Runnable() {
		public void run() {
		    try {
			String[] words = action.split(" ");
			String task = words[0] + " >  ";
			UFStrings agentReply = sendRecvAgent( AgentRequest, panel+task );

			if( agentReply == null ) {
			    String statmsg = "transaction ERROR: recvd NO response ?";
			    statusResponse.setText( task + statmsg );
			}
			else {
			    String statmsg = decodeAgentResponse( agentReply );

			    if( statmsg == null ) statmsg = "WARN: failed decoding response ?";
			    statusResponse.setText( task + statmsg );
			    System.out.println( panel + task + statmsg );
			}
		    }
		    catch( Exception x ) { x.printStackTrace(); }
		}
	    };

	SwingUtilities.invokeLater( cmd_agent_task );
    }
//-------------------------------------------------------------------------------
    /**
     * UFLibPanel#sendRecvAgent
     *@param agentRequest : UFStrings object containing request to send to DC agent.
     *@param caller : String = name of calling method.
     *@param errmsg : String = error message if something bad happened.
     *@retval = UFStrings reply from DC agent.
     */
    public UFStrings sendRecvAgent( UFStrings agentRequest, String caller )
    {
	agentRequest.rename( LocalHost + ":" + agentRequest.name() );

	if( agentRequest.sendTo( agentSocket ) <= 0 ) {
	    String errmsg = "Send ERROR.";
	    System.err.println( caller + errmsg );
	    System.out.println( caller + errmsg );
	    indicateConnectStatus(false);
	    return null;
	}

	UFStrings agentReply = null;
	//recv response from agent:

	if( (agentReply = (UFStrings)UFProtocol.createFrom( agentSocket )) == null ) {
	    String errmsg = "Transaction ERROR: nothing recvd, trying again...";
	    System.err.println( caller + errmsg );
	    System.out.println( caller + errmsg );
	    if( (agentReply = (UFStrings)UFProtocol.createFrom( agentSocket )) == null ) {
		errmsg = "Transaction ERROR: nothing recvd, giving up.";
		System.err.println( caller + errmsg );
		System.out.println( caller + errmsg );
		indicateConnectStatus(false);
		return null;
	    }
	}

	return agentReply;
    }
//------------------------------------------------------------------------
    /**
     * UFLibPanel#decodeAgentResponse: virtual method to be overriden.
     *
     *@param agentReply : UFStrings object containing reply from DC agent.
     *@retval = String, either OK or ERR.
     */
    public String decodeAgentResponse( UFStrings agentReply )
    {
	return("OK");
    }
//-------------------------------------------------------------------------------

    public void setNewStatus( UFStrings newStatus )
    {
    }
//-------------------------------------------------------------------------------

    public void updateFrames( UFFrameConfig frameConfig )
    {
    }
}

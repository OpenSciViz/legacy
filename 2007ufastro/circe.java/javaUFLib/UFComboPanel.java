//Title:        UFComboPanel
//Version:      2.0
//Copyright:    Copyright (c) 2003
//Author:       Frank Varosi, 2003
//Company:      University of Florida
//Description:  Combine UFComboBox and UFTextField in one panel (with labels)

package javaUFLib;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

//===============================================================================
/**
 * Combine UFComboBox and UFTextField in one panel (with labels),
 * with vertical (default) or horizontal arrangement,
 * together dealing with desired and active selections, and changes color depending state.
 */

public class UFComboPanel extends JPanel
{
    public static final
	String rcsID = "$Name:  $ $Id: UFComboPanel.java,v 1.24 2004/08/20 23:53:25 varosi Exp $";

    public UFComboBox desiredSelect;
    public UFTextField activeField;

    JLabel paramName;
    String Name, Units;

    JCheckBox paramEnable;
    boolean _checkBox = false; //if true then Label will be a JCheckBox for enabling/disabling selection.
    boolean _checkBoxState = false; //initially the JCheckBox will NOT be checked (selected).

    boolean _Vertical = false;
    boolean _useIndex = false;
    boolean _compareWithSelection = true; //default is to compare activeItem with desiredSelect
                                          // but if false, then compare with desiredItem directly.
    String desiredItem = "";
    String activeItem = "";
    String alarm_item = null;
    String[] _items;
//-------------------------------------------------------------------------------
    /**
     * Default Constructor
     */
    public UFComboPanel()
    {
	try { createPanel( "", null ); }
	catch(Exception x) { System.out.println("Error creating UFComboPanel: " + x.toString()); }
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param description String: Information regarding the field
     *@param items       Array of Strings: list of values to be passed to the record field
     */
    public UFComboPanel( String description, String[] items )
    {
	try {
	    createPanel( description, items );
	}
	catch(Exception x) { System.out.println("Error creating UFComboPanel: " + x.toString()); }
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param description String: Information regarding the field
     *@param items       Array of Strings: list of values to be passed to the record field
     */
    public UFComboPanel( String description, String[] items1, String[] items2 )
    {
	try {
	    String[] items = new String[ items1.length + items2.length ];
	    for( int i=0; i < items1.length; i++ ) items[i] = items1[i];
	    for( int i=0; i < items2.length; i++ ) items[i+items1.length] = items2[i];
	    createPanel( description, items );
	}
	catch(Exception x) { System.out.println("Error creating UFComboPanel: " + x.toString()); }
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param description   String: Information regarding the field
     *@param items         Array of Strings: list of values to be passed to the record field
     *@param Vertical      boolean: set true for Vertical orientation (default is Horizontal).
     */
    public UFComboPanel( String description, String[] items, boolean Vertical )
    {
	try {
	    _Vertical = Vertical;
	    createPanel( description, items );
	}
	catch(Exception x) { System.out.println("Error creating UFComboPanel: " + x.toString()); }
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param description   String: Information regarding the field
     *@param items         Array of Strings: list of values to be passed to the record field
     *@param Vertical      boolean: set true for Vertical orientation (default is Horizontal).
     */
    public UFComboPanel( String description, String[] items, boolean Vertical, boolean useIndex )
    {
	try {
	    _useIndex = useIndex;
	    _Vertical = Vertical;
	    createPanel( description, items );
	}
	catch(Exception x) { System.out.println("Error creating UFComboPanel: " + x.toString()); }
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param description   String: Information regarding the field
     *@param checkBoxState boolean: if present Label is JCheckBox with init check state specified.
     *@param items         Array of Strings: list of values to be passed to the record field
     */
    public UFComboPanel( String description, boolean checkBoxState, String[] items )
    {
	try {
	    _checkBox = true;
	    _checkBoxState = checkBoxState;
	    createPanel( description, items );
	}
	catch(Exception x) { System.out.println("Error creating UFComboPanel: " + x.toString()); }
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param description   String: Information regarding the field
     *@param checkBoxState boolean: if present Label is JCheckBox with init check state specified.
     *@param items         Array of Strings: list of values to be passed to the record field
     *@param Vertical      boolean: set true for Vertical orientation (default is Horizontal).
     */
    public UFComboPanel( String description, boolean checkBoxState, String[] items, boolean Vertical )
    {
	try {
	    _checkBox = true;
	    _checkBoxState = checkBoxState;
	    _Vertical = Vertical;
	    createPanel( description, items );
	}
	catch(Exception x) { System.out.println("Error creating UFComboPanel: " + x.toString()); }
    }
//-------------------------------------------------------------------------------
    /**
     *Component creation -- returns no arguments
     *@param description   String: Information regarding the field, used for Label and Name.
     *@param items         Array of Strings: list of items in the combo box
     */
    protected void createPanel( String descrip, String[] items ) throws Exception
    {
	if( descrip.indexOf("(") > 0 ) {
	    int pos = descrip.indexOf("(");
	    this.Name = descrip.substring( 0, pos ).trim();
	    this.Units = descrip.substring( pos+1, descrip.indexOf(")") ).trim();
	}
	else if( descrip.indexOf(":") > 0 )
	    this.Name = descrip.substring( 0, descrip.indexOf(":") ).trim();
	else
	    this.Name = descrip.trim();

	if( _useIndex ) {
	    _items = items;
	    desiredSelect = new UFComboBox( items );
	    desiredSelect.setIndexMethod();
	}
	else {//make copy of items and set first item to be always a blank:	
	    _items = new String[items.length+1];
	    _items[0] = "";
	    for( int i=0; i < items.length; i++ ) _items[i+1] = items[i];
	    desiredSelect = new UFComboBox( _items );
	}

	activeField = new UFTextField( descrip );
	activeField.setBackground(Color.white);
	activeField.setEditable(false);
	setToolTipText();
	setLayout(new RatioLayout());

	if( _checkBox )
	    {
		paramEnable = new JCheckBox( descrip );
		paramEnable.setSelected(_checkBoxState);
		desiredSelect.setEnabled(_checkBoxState);

		paramEnable.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			    if( paramEnable.isSelected() ) {
				desiredSelect.setEnabled(true);
			    }
			    else {
				desiredSelect.setEnabled(false);
			    }
			}
		    });
	    }
	else paramName = new JLabel( descrip + " ", JLabel.RIGHT );

	if( _Vertical )
	    {
		if( _checkBox )
		    this.add("0.0,0.02;0.5,0.47", paramEnable);
		else
		    this.add("0.0,0.02;0.5,0.47", paramName);
		this.add("0.5,0.02;0.5,0.47", desiredSelect );
		this.add("0.0,0.52;0.5,0.40", new JLabel("Active Value= ",JLabel.RIGHT) );
		this.add("0.5,0.52;0.5,0.40", activeField );
	    }
	else
	    {	
		if( _checkBox )
		    this.add("0.00,0.02;0.32,0.96", paramEnable);
		else
		    this.add("0.00,0.02;0.32,0.96", paramName);
		this.add("0.32,0.02;0.37,0.96", desiredSelect );
		this.add("0.70,0.02;0.29,0.96", activeField );
	    }
    }
//-------------------------------------------------------------------------------
    /**
     *UFComboPanel#setAlarmValue
     *@param alarmValue       String: item value for which red color should be indicated if selected.
     */
    public void setAlarmValue( String alarmValue )
    {
	if( alarmValue != null ) {
	    alarm_item = alarmValue;
	    desiredSelect.setAlarmValue( alarmValue );
	    activeField.setAlarmValue( alarmValue );
	}
    }
//-------------------------------------------------------------------------------

    public boolean isSelected()
    {
	if( _checkBox )
	    return paramEnable.isSelected();
	else
	    return true;
    }
//-------------------------------------------------------------------------------

    void setToolTipText() {
	this.setToolTipText( Name + ": Desired=" + desiredItem + ", Active=" + activeItem );
    }
//-------------------------------------------------------------------------------

    public void compareWithSelection( boolean compare ) { _compareWithSelection = compare; }

    public String getActive() {	return activeItem.trim(); }
    public String getDesired() { return desiredItem.trim(); }
    public String getSelection() { return desiredSelect.getSelection(); }
    public String getSelectedItem() { return (String)desiredSelect.getSelectedItem(); }
    public void setSelectedIndex(int index) { desiredSelect.setSelectedIndex(index); }
    public void setSelectedItem(String item) { desiredSelect.setSelectedItem(item); }

//-------------------------------------------------------------------------------

    public void setActive( String newItem )
    {
	if( desiredSelect.indexMethod() )
	    {
		if( newItem.length() > 0 ) {
		    int newIndex = Integer.parseInt( newItem );
		    activeItem = _items[newIndex];
		}
		else activeItem = "?";
	    }
	else activeItem = newItem.trim();

	setToolTipText();
	compareItems();

	if( ! activeField.getText().equals( activeItem ) )
	    activeField.setNewState( activeItem );
    }
//-------------------------------------------------------------------------------

    public void setDesired( String newItem )
    {
	if( desiredSelect.indexMethod() )
	    {
		if( newItem.length() > 0 ) {
		    int newIndex = Integer.parseInt( newItem );
		    desiredItem = _items[newIndex];
		}
		else desiredItem = "?";
	    }
	else desiredItem = newItem.trim();

	desiredSelect.setNewState( desiredItem );
	setToolTipText();
	compareItems();
    }
//-------------------------------------------------------------------------------

    public void setDesiredSel( String newItem )
    {
	setDesired( newItem );
	desiredSelect.setSelectedItem( newItem );
    }
//-------------------------------------------------------------------------------

    void compareItems()
    {
	if( _compareWithSelection )
	    {
		desiredItem = (String)desiredSelect.getSelectedItem();
		if( desiredItem.length() > 0 ) setColor();
	    }
	else setColor(); //compare activeItem directly to desiredItem (from setDesired).
    }

//-------------------------------------------------------------------------------

    void setColor()
    {
	if( desiredItem.toUpperCase().equals( activeItem.toUpperCase() ) )
	    desiredSelect.setBackground( Color.white );
	else
	    desiredSelect.setBackground( Color.yellow );

	if( alarm_item != null ) {
	    if( desiredItem.toUpperCase().equals( alarm_item.toUpperCase() ) )
		desiredSelect.setBackground( Color.red );
	}
    }
//-------------------------------------------------------------------------------

    public String name() { return Name; }
    public String nameUp() { return Name.toUpperCase(); }
    public String nameLow() { return Name.toLowerCase(); }

} //end of class UFComboPanel

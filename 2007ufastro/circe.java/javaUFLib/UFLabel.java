//Title:        UFLabel.java
//Version:      2.0
//Copyright:    Copyright (c) Frank Varosi
//Author:       Frank Varosi, 2003
//Company:      University of Florida
//Description:  Extension of JLabel class.

package javaUFLib;

import java.awt.*;
import java.awt.event.*;
import java.awt.Toolkit;
import javax.swing.JLabel;

//===============================================================================
/**
 * Creates text Labels with always same prefix text and color blue.
 * Beeps and changes color to red if text "ERR" or "WARN" is displayed.
 */
public class UFLabel extends JLabel
{
    public static final
	String rcsID = "$Name:  $ $Id: UFLabel.java,v 1.8 2004/06/17 22:46:05 varosi Exp $";

    String _prefix = "";
    Color _color = Color.blue;
    boolean _doBeep = true;
//-------------------------------------------------------------------------------
    /**
     * Basic Constructor
     *@param prefix String: Text to preceed record value in the label text
     */
    public UFLabel(String prefix) {
	try  {
	    super.setText(prefix);
	    _prefix = prefix;
	}
	catch(Exception ex) {
	    System.out.println("Error creating UFLabel with prefix "+prefix+": " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param prefix String: Text to preceed record value in the label text
     */
    public UFLabel( String prefix, Color color ) {
	try  {
	    super.setText(prefix);
	    _prefix = prefix;
	    _color = color;
	}
	catch(Exception ex) {
	    System.out.println("Error creating UFLabel with prefix "+prefix+": " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------

    public void newPrefix( String prefix ) { _prefix = prefix; }
    public void newColor( Color color ) { _color = color; }

//-------------------------------------------------------------------------------
  /**
   * Override setText method to always use _prefix.
   */
    public void setText( String text )
    {
	super.setText( _prefix + "  " + text );

	if( text.indexOf("ERR") >= 0 || text.indexOf("WARN") >= 0 ) {
	    super.setForeground( Color.red );
	    if( _doBeep )
		Toolkit.getDefaultToolkit().beep();
	}
	else if( text.indexOf("FAIL") >= 0 || text.indexOf("Fail") >= 0 )
	    super.setForeground( Color.red );
	else
	    super.setForeground( _color );
    }
//-------------------------------------------------------------------------------

    public void setText( String text, boolean noBeep )
    {
	if( noBeep ) _doBeep = false;
	setText( text );
	_doBeep = true;
    }
//-------------------------------------------------------------------------------

    public String getSubText()
    {
	String text = super.getText();
	if( _prefix == null ) return text;
	return text.substring( _prefix.length() ).trim();
    }
} //end of class UFLabel


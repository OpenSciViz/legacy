package javaUFLib;

import java.math.*;
import java.io.*;
import java.util.*;

public class UFImageOps {

   public static float[] getCentroid(int[][] img, int x, int y, int fwhm) {
      int[] ax = {x};
      int[] ay = {y};
      float[][] cen = getCentroid(img, ax, ay, fwhm);
      return UFArrayOps.map2DByRow(cen);
   }

   public static float[][] getCentroid(int[][] img, int[] x, int[] y, int fwhm) {
      float[][] fimg = new float[img.length][img[0].length];
      for (int j = 0; j < img.length; j++) {
	for (int l = 0; l < img[0].length; l++) {
	   fimg[j][l] = (float)img[j][l];
	} 
      }
      float[] fx = new float[x.length];
      float[] fy = new float[y.length];
      for (int j = 0; j < x.length; j++) {
	fx[j]=(float)x[j];
        fy[j]=(float)y[j];
      }
      float ffwhm = (float)fwhm; 
      return getCentroid(fimg, fx, fy, ffwhm);
   }

   public static float[][] getCentroid(float[][] img, float[] x, float[] y, float fwhm) {
      int xsize = img.length;
      int ysize = img[0].length;
      int nhalf = (int)Math.floor(0.637*fwhm);
      if (nhalf < 2) nhalf = 2;
      int nbox = 2*nhalf+1;
      int nhalfbig = nhalf+3;
      int nbig = nbox+6;
      int npts = x.length;
      float[][] cen = new float[npts][2];
      int[] ix = new int[npts];
      int[] iy = new int[npts];
      String pos;
      float[][] bigbox, strbox, deriv;
      float[] dd, w, derivtot, ddsq;
      float mx, sumc, sumd, sumxd, sumxsq, dx, dy;
      int[][] mx_pos;
      int[] temp_idx, temp_idy;
      int Nmax, idx, idy, xmax, ymax, ir;

      for (int j = 0; j < npts; j++) {
	ix[j] = (int)Math.floor(x[j] + 0.5);
	iy[j] = (int)Math.floor(y[j] + 0.5);
      }
      
      for (int j = 0; j < npts; j++) {
	pos = "" + x[j] + " " + y[j];
	if ((ix[j] < nhalfbig) || ((ix[j] + nhalfbig) > xsize-1) || (iy[j] < nhalfbig) || ((iy[j] + nhalfbig > ysize-1))) {
	   System.out.println("Position " + pos + " too near edge of image");
	   cen[j][0] = -1;
	   cen[j][1] = -1;
	   continue;
	}
	bigbox = UFArrayOps.extractValues(img, ix[j]-nhalfbig, ix[j]+nhalfbig, iy[j]-nhalfbig, iy[j]+nhalfbig);
	mx = UFArrayOps.maxValue(bigbox);
	mx_pos = UFArrayOps.where(bigbox, "==", mx);
	Nmax = UFArrayOps.countN(bigbox, mx);
	temp_idx = new int[mx_pos.length];
        temp_idy = new int[mx_pos.length];
        for (int l=0; l < mx_pos.length; l++) {
	   temp_idx[l] = mx_pos[l][0];
	   temp_idy[l] = mx_pos[l][1];
	}
	if (Nmax > 1) {
	   idx = Math.round(UFArrayOps.avgValue(temp_idx));
	   idy = Math.round(UFArrayOps.avgValue(temp_idy));
	} else {
	   idx = temp_idx[0];
	   idy = temp_idy[0];
	}
	xmax = ix[j] - (nhalf+3) + idx;
	ymax = iy[j] - (nhalf+3) + idy;

	if ((xmax < nhalf) || ((xmax + nhalf) > xsize-1) || (ymax < nhalf) || ((ymax + nhalf) > ysize-1)) {
           System.out.println("Position " + pos + " too near edge of image");
           cen[j][0] = -1;
           cen[j][1] = -1;
           continue;
	}

	strbox = UFArrayOps.extractValues(img, xmax-nhalf, xmax+nhalf, ymax-nhalf, ymax+nhalf);  

	ir = Math.max(nhalf-1, 1);
	dd = new float[nbox-1];
	for (int l=0; l < dd.length; l++) dd[l] = (float)(l+0.5-nhalf);
	w = new float[nbox-1];
	for (int l=0; l < w.length; l++)
	   w[l] = (float)(1.0 - 0.5 * (Math.abs(dd[l])-0.5) / (nhalf-0.5));
	sumc = UFArrayOps.totalValue(w);

	deriv = UFArrayOps.shift(strbox,-1,0);
	deriv = UFArrayOps.subArrays(deriv, strbox);
	deriv = UFArrayOps.extractValues(deriv, 0, nbox-2, nhalf-ir, nhalf+ir);
	derivtot = UFArrayOps.totalValue(deriv, 1);
	sumd = UFArrayOps.totalValue(UFArrayOps.multArrays(w, derivtot));
	sumxd = UFArrayOps.totalValue(UFArrayOps.multArrays(UFArrayOps.multArrays(w, dd), derivtot)); 
	ddsq = new float[dd.length];
	for (int l = 0; l < dd.length; l++) ddsq[l] = dd[l]*dd[l];
	sumxsq = UFArrayOps.totalValue(UFArrayOps.multArrays(w, ddsq));

	if (sumxd > 0) {
	   System.out.println("Unable to compute X centroid around position" + pos);
	   cen[j][0] = -1;
	   cen[j][1] = -1;
	   continue;
	}
	dx = sumxsq*sumd/(sumc*sumxd);
	if (Math.abs(dx) > nhalf) {
	   System.out.println("Computed X centroid for position " + pos + " out of range");
	   cen[j][0] = -1;
           cen[j][1] = -1;
           continue;
	}	
	cen[j][0] = xmax-dx;

        deriv = UFArrayOps.shift(strbox,0,-1);
        deriv = UFArrayOps.subArrays(deriv, strbox);
        deriv = UFArrayOps.extractValues(deriv, nhalf-ir, nhalf+ir, 0, nbox-2);
        derivtot = UFArrayOps.totalValue(deriv, 2);
        sumd = UFArrayOps.totalValue(UFArrayOps.multArrays(w, derivtot));
        sumxd = UFArrayOps.totalValue(UFArrayOps.multArrays(UFArrayOps.multArrays(w, dd), derivtot));
        ddsq = new float[dd.length];
        for (int l = 0; l < dd.length; l++) ddsq[l] = dd[l]*dd[l];
        sumxsq = UFArrayOps.totalValue(UFArrayOps.multArrays(w, ddsq));

        if (sumxd > 0) {
           System.out.println("Unable to compute Y centroid around position" + pos);
           cen[j][0] = -1;
           cen[j][1] = -1;
           continue;
        }
        dy = sumxsq*sumd/(sumc*sumxd);
        if (Math.abs(dy) > nhalf) {
           System.out.println("Computed Y centroid for position " + pos + " out of range");
           cen[j][0] = -1;
           cen[j][1] = -1;
           continue;
        }
        cen[j][1] = ymax-dy;
      }
      return cen; 
   }
   
   public static int[][] smooth(int[][] data, int width, int niter) {
      if (niter == 0) return data;
      int m = data.length;
      int n = data[0].length;
      if (width % 2 == 0) width++;
      int w = width/2;
      int[][] tot = new int[m][n];
      int[][] sdata = new int[m][n];
      int a = 0, b = 0, c = 0, d = 0; 
      float pts;
      for (int i = 0; i < niter; i++) {
	for (int j = 0; j < m; j++) {
	   for (int l = 0; l < n; l++) {
	      if (l > 0) tot[j][l] = tot[j][l-1] + data[j][l];
	      else tot[j][l] = data[j][l];
	   }
	}
        for (int j = m-2; j >=0 ; j--) {
           for (int l = 0; l < n; l++) {
              tot[j][l]+=tot[j+1][l];
           }
        }
	for (int j = 0; j < m; j++) {
	   for (int l = 0; l < n; l++) {
	      if (j-w >=0 && l+w < n) a = tot[j-w][l+w];
	      if (j+w+1 < m && l+w < n) b = tot[j+w+1][l+w];
	      if (j-w >= 0 && l-(w+1) >= 0) c = tot[j-w][l-(w+1)];
	      if (j+w+1 < m && l-(w+1) >= 0) d = tot[j+w+1][l-(w+1)];
	      if (l-(w+1) < 0) {
		c = 0;
		d = 0;
	      }
	      if (j+w+1 >= m) {
		b = 0;
		d = 0;
	      }
	      if (l+w >= n || j-w < 0) {
		a = tot[Math.max(j-w,0)][Math.min(l+w,n-1)];
		if (j+w+1 < m) b = tot[j+w+1][Math.min(l+w,n-1)];
		if (l-(w+1) >= 0) c = tot[Math.max(j-w,0)][l-(w+1)];
	      }
	      pts = (Math.min(j+w+1,m)-Math.max(j-w,0))*(Math.min(l+w,n-1)-Math.max(l-(w+1),-1));
	      sdata[j][l] = (int)Math.round((a-b-c+d)/pts);
	   }
	}
	data = sdata;
      }
      return sdata;
   }

   public static void main(String[] args) {
      float[][] a = new float[530][530];
      String s;
      try {
        BufferedReader r = new BufferedReader(new FileReader("NGC3031.txt"));
        for (int j = 0; j < 530; j++) {
           for (int l = 0; l < 530; l++) {
              s = r.readLine();
              a[j][l] = Float.parseFloat(s);
           }
        }
      } catch(IOException e) {}
      float[] x = {440};
      float[] y = {39};
      float[][] c = getCentroid(a, x, y, 4);
      System.out.println(UFArrayOps.arrayToString(c));
      int[][] data = {{0,1,2,3,4},{5,6,7,8,9},{10,17,12,13,14},{15,16,17,25,19},{20,21,22,23,24,25}};
      System.out.println(UFArrayOps.arrayToString(smooth(data, 3, 1)));
   }

}

package javaUFProtocol;

import java.io.*;

public class UFFloats extends UFTimeStamp
{
    public static final
	String rcsID = "$Name:  $ $Id: UFFloats.java,v 1.6 2004/09/14 22:18:57 varosi Exp $";

    protected float[] _values=null;

    public UFFloats() {
	_type=MsgTyp._Floats_;
    }

    public UFFloats(int length) {
	_length = length;
	_type=MsgTyp._Floats_;
    }

    public UFFloats(String name, float[] vals) {
	_name = new String(name);
	_type=MsgTyp._Floats_;
	_elem=vals.length;
	_values = vals;
	_length = _minLength() + 4*vals.length;
    }

    // all methods declared abstract by UFProtocol can be defined here:

    public String description() { return new String("UFFloats"); }
 
    // return size of an element's value:
    public int valSize(int elemIdx) { 
	if( elemIdx >= _values.length )
	    return 0;
	else
	    return 4;
    }

    public float[] values() { return _values; }
    public float valData(int index) { return _values[index]; }

    public float maxVal() {
	float max=-1.0e38f;
	for( int i=0; i<_values.length; i++ )
	    if( _values[i] > max ) max = _values[i];
	return max;
    }

    public float minVal() {
	float min=1.0e38f;
	for( int i=0; i<_values.length; i++ )
	    if( _values[i] < min ) min = _values[i];
	return min;
    }

    public int numVals() {
	if (_values != null)
	    return _values.length;
	else
	    return 0;
    }

    protected void _copyNameAndVals(String s, float[] vals) {
	_name = new String(s);
	_elem = vals.length;
	_length = _minLength();
	_values = new float[vals.length];
	for( int i=0; i<_elem; i++ ) {
	    _values[i] = vals[i];
	    _length += 4;
	}
    }
 
    public void setNameAndVals(String s, float[] vals) {
	_name = new String(s);
	_elem = vals.length;
	_length = _minLength();
	_values = new float[vals.length];
	for( int i=0; i<_elem; i++ ) {
	    _values[i] = vals[i];
	    _length += 4;
	}
    }
 
    // recv data values (length, type, and header have already been read)

    public int recvData(DataInputStream inp) {
	int retval=0;
	try {
	    _values = new float[_elem];

	    for( int elem=0; elem<_elem; elem++ ) {
		_values[elem] = inp.readFloat();
		retval += 4;
	    }
	}
	catch(EOFException eof) {
	    System.err.println("UFFloats::recvData> "+eof.toString());
	}
	catch(IOException ioe) {
	    System.err.println("UFFloats::recvData> "+ioe.toString());
	}
	catch( Exception e ) {
	    System.err.println("UFFloats::recvData> "+e.toString());
	}
	return retval;
    }

    // send data values (header already sent):

    public int sendData(DataOutputStream out) {
	int retval=0;
	try {
	    if (_values != null && _values.length > 0) {
		ByteArrayOutputStream outArray = new ByteArrayOutputStream(_elem*4);
		DataOutputStream outToArray = new DataOutputStream(outArray);
		for( int elem=0; elem<_elem; elem++ ) {
		    outToArray.writeFloat(_values[elem]);
		    retval += 4;
		}
		outArray.writeTo(out); // write all floats as bytes in one chunk
	    }
	}
	catch(EOFException eof) {
	    System.err.println("UFFloats::sendData> "+eof.toString());
	} 
	catch(IOException ioe) {
	    System.err.println("UFFloats::sendData> "+ioe.toString());
	}
	catch( Exception e ) {
	    System.err.println("UFFloats::sendData> "+e.toString());
	}
	return retval;
    }

    // additional methods (beyond base class's):
    // flip the image frame by reversing the order of rows:

    public void flipFrame( UFFrameConfig fc )
    {
	int nrow = fc.height;
	int ncol = fc.width;

	for( int irow=0; irow < (nrow/2); irow++ )
	    {
		int iflip = nrow - irow - 1;
		int kp = irow * ncol;
		int kf = iflip * ncol;

		for( int k=0; k<ncol; k++ ) {
		    float pixval = _values[kp];
		    _values[kp++] = _values[kf];
		    _values[kf++] = pixval;
		}
	    }
    }

    public void plusEquals(UFFloats rhs) {
	if (rhs._elem == 1)
	    for (int i=0; i<this._elem; i++) 
		this._values[i] += rhs._values[0];
	else
	    for(int i=0; i<Math.min(this._elem,rhs._elem); i++) 
		this._values[i] += rhs._values[i];
    }

    public void minusEquals(UFFloats rhs) {
	if (rhs._elem == 1)
	    for (int i=0; i<this._elem; i++) 
		this._values[i] -= rhs._values[0];
	else
	    for(int i=0; i<Math.min(this._elem,rhs._elem); i++) 
		this._values[i] -= rhs._values[i];
    }

    public void plusEquals(float rhs) {
	for (int i=0; i<this._elem; i++) this._values[i] += rhs;
    }

    public void minusEquals(float rhs) {
	for (int i=0; i<this._elem; i++) this._values[i] -= rhs;
    }

    public int sum(UFFloats Left, UFFloats Right) {
	if( Left._values == null || 
	    Right._values == null ) return 0;

	float[] viL = Left._values;
	float[] viR = Right._values;
	int elem = Math.min(Left._elem, Right._elem);

	if( this._values == null || _elem <= 0 ) this._values = new float[elem];

	for (int i=0; i<elem; i++) _values[i] = viL[i] + viR[i];

	return elem;
    }

    public int diff(UFFloats Left, UFFloats Right) {
	if( Left._values == null || 
	    Right._values == null ) return 0;

	float[] viL = Left._values;
	float[] viR = Right._values;
	int elem = Math.min(Left._elem, Right._elem);

	if( this._values == null || _elem <= 0 ) this._values = new float[elem];

	for (int i=0; i<elem; i++) _values[i] = viL[i] - viR[i];

	return elem;
    }
}

package javaUFProtocol;

import java.io.*;
import java.net.*;
import java.util.*; 

public abstract class UFProtocol extends Object
{    
    // This _MinLength_ should be congruent with _minlength() for empty name string:
    public static int _MinLength_=48;
    
    // All protocol message packets have following protected attributes,
    // in following sequence, followed by an array of Data Values (if _elem > 0) :

    // total length of message (byte count)
    protected int _length = _MinLength_;
    
    // type must be one enumation MsgTyp
    protected int _type = 0;
    
    // number of elements (values): _elem == 0 for Timestamp, _elem >= 1 for others
    protected int _elem = 0;
    
    // Sequence count allows grouping of multiple protocol objects 
    protected short _seqCnt = 1; 
    protected short _seqTot = 1; 

    // duration of data in seconds (optional)
    protected float _duration = 0;

    // timesamp: "yyyy:ddd:hh:mm:ss.uuuuuu" format is 24 char string
    protected String _timestamp="yyyy:ddd:hh:mm:ss.uuuuuu";

    // optional name string (note that in C++ code _name is currently limited to 83 bytes).
    protected String _name="";

    // default ctor needed for containers and "virtual creation"
    public UFProtocol() { }
    
    protected UFProtocol(String name, int elem) {
	_elem = elem;
	_name = new String(name);
    }
    
    // run-time typing for client side:
    // since enum's are not part of the java language, use inner classes

    public static class MsgTyp {
	static final int _MsgError_=-1;
	static final int _TimeStamp_=0;
	static final int _Strings_=1;
	static final int _Bytes_=2;
        static final int _Shorts_=3;
	static final int _Ints_=4;
	static final int _Floats_=5;
        static final int _ObsConfig_=8;
	static final int _FrameConfig_=9;
	static final int _ByteFrames_=11;
	static final int _IntFrames_=13;
	static final int _FloatFrames_=14;
    }
    
    // client default is push
    public static class ClientTyp {
	static int _Push_=0;
	static int _Pull_=1;
    }
    
    public static class LengthAndType {
	int length;
	int type;
	LengthAndType() { length = type = 0; }
    }
    
    // _length must be >= _minlength!
    // sizeof(namelen) + strlen(_name) (could be 0) +  
    // strlen(_timestamp) + sizeof(_type) + sizeof(_elem) + sizeof(_length)+... 
    // = 4+0+24+4+4+4+2+2+4 = 48

    protected int _minLength() {
	int minlength = 6*4 + _timestamp.length() + _name.length();
	return minlength;
    }
    
    // return size of the element's value (not it's name!)
    // either string length, or sizeof(float), sizeof(frame):
    public abstract int valSize(int elemIdx); 
    
    // fetch type
    public int typeId() { return _type; }
    // fetch description
    public abstract String description();
    // fetch name
    public String name() { return _name; };
    // fetch timestamp
    public String timeStamp() { return _timestamp; };
    // fetch element count
    public int elements() { return _elem; }
    // total length of the message:
    public int length() { return _length; }

    public String rename(String newname) {
	String orig = _name;
	_length -= _name.length();
	_name = new String(newname);
	_length += _name.length();
	return orig;
    }
    
    // can support bundling a bunch of messages into one send/recv?:
    public static int sizeOf(Vector v) {
	int retval=0;
	for( Enumeration e = v.elements(); e.hasMoreElements(); ) {
	    retval += ((UFProtocol)e).length();
	}
	return retval;
    }
    
    // recv length and type from file DataInputStream
    // (then used for both socket and file):

    public static UFProtocol.LengthAndType recvLengthAndType(DataInputStream inpStrm)
    {
	try { //try reading input stream first to see if exceptions trigger, then returning null...
	    int length = inpStrm.readInt();
	    int type = inpStrm.readInt();
	    UFProtocol.LengthAndType LTobj = new UFProtocol.LengthAndType();
	    LTobj.length = length;
	    LTobj.type = type;
	    return LTobj;
	}
	catch(EOFException eof) {
	    System.err.println("UFProtocol::recvLengthAndType> " +eof.toString());
	} 
	catch(IOException ioe) {
	    System.err.println("UFProtocol::recvLengthAndType> "+ioe.toString());
	}
	catch( Exception e ) {
	    System.err.println("UFProtocol::recvLengthAndType> " +e.toString());
	}
	return null;
    }
    
    // read length and type from file descriptor:

    public static UFProtocol.LengthAndType readLengthAndType(File f)
    {
	UFProtocol.LengthAndType LTobj=null;
	try {
	    DataInputStream inpStrm = new DataInputStream(new FileInputStream(f));
	    LTobj = recvLengthAndType(inpStrm);
	} 
	catch(IOException ioe) {
	    System.err.println("UFProtocol::readLengthAndType(file)> "+ioe.toString());
	}
	catch( Exception e ) {
	    System.err.println("UFProtocol::readLengthAndType(file)> "+e.toString());
	}
	return LTobj;
    }
    
    // recv length and type from socket:

    public static UFProtocol.LengthAndType recvLengthAndType(Socket soc)
    {
	UFProtocol.LengthAndType LTobj=null;
	try {
	    DataInputStream inpStrm = new DataInputStream(soc.getInputStream());
	    LTobj = recvLengthAndType(inpStrm);
	}
	catch(IOException ioe) {
	    System.err.println("UFProtocol::recvLengthAndType(sock)> "+ ioe.toString());
	}
	catch( Exception e ) {
	    System.err.println("UFProtocol::recvLengthAndType(sock)> "+e.toString());
	}
	return LTobj;
    }

    // recv rest of protocol header, assuming length & type have already been recvd:

    protected int recvHeader(DataInputStream inpStrm) {
	int nbytes=0;
	try {
	    //length and type have already been read
	    _elem = inpStrm.readInt(); // should be 0
	    nbytes += 4;
	    _seqCnt = inpStrm.readShort(); // seqcnt and seqtot (both short ints)
	    _seqTot = inpStrm.readShort();
	    nbytes += 4;
	    _duration = inpStrm.readFloat(); // read in 'duration'
	    nbytes += 4;
	    byte[] tbuf = new byte[_timestamp.length()];
	    inpStrm.readFully(tbuf);
	    nbytes+=_timestamp.length();
	    _timestamp = new String(tbuf);
	    int namelen = inpStrm.readInt();
	    nbytes += 4;
	    if( namelen > 0 ) {
		byte[] namebuf = new byte[namelen];
		inpStrm.readFully(namebuf);
		nbytes +=  namelen;
		_name = new String(namebuf);
	    }
	}
	catch(EOFException eof) {
	    System.err.println("UFProtocol::recvHeader> "+eof.toString());
	}
	catch(IOException ioe) {
	    System.err.println("UFProtocol::recvHeader> "+ioe.toString());
	}
	catch( Exception e ) {
	    System.err.println("UFProtocol::recvHeader> "+e.toString());
	}
	return nbytes;
    }

    // Following abstract methods must be defined in each type of Protocol object
    // to handle the data values of that object (UFTimeStamp has no data).

    // recv data from stream
    public abstract int recvData(DataInputStream inpStrm);
    // send data out stream
    public abstract int sendData(DataOutputStream outpStrm);

    // creates a new instance of UFProtocol object on heap, reading from a socket,

    public static UFProtocol createFrom(Socket soc)
    {
	UFProtocol ufpObj = create( recvLengthAndType(soc) );
	if( ufpObj != null ) ufpObj.recvFrom(soc);
	return ufpObj;
    }
    
    // recv rest of object header and data values from socket
    //(assumes that recvLengthAndType(soc) was already invoked):

    public int recvFrom(Socket soc) {
	try {
	    DataInputStream inpStrm = new DataInputStream(soc.getInputStream());
	    return recvFrom(inpStrm);
	}
	catch(IOException ioe) {
	    System.err.println("UFProtocol::recvFrom(Socket)> "+ioe.toString());
	    return 0;
	}
    }

    public int recvFrom(DataInputStream inpStrm)
    {
	int nbHead = recvHeader(inpStrm) + 8;

	if( nbHead < _MinLength_ ) {
	    System.err.println("UFProtocol::recvFrom> failed reading header info!");
	    return nbHead;
	}

	if( _elem > 0 ) {
	    int numRead = recvData(inpStrm);
	    int nbToRead = _length - nbHead;
	    if (numRead == 0) {
		System.err.println("UFProtocol.recvFrom> for object: "+description());
		System.err.println("UFProtocol.recvFrom> no data values available:"+
				   " Expected: " + _elem + " values = " + nbToRead + " bytes.");
	    }
	    else if( numRead > nbToRead ) {
		System.err.println("UFProtocol.recvFrom> for object: "+description());
		System.err.println("UFProtocol.recvFrom> Read more than length specified in header:"
				   +" Bytes Read = "+numRead+". Expected: "+nbToRead);
	    }
	    else if( numRead < nbToRead ) {
		System.err.println("UFProtocol.recvFrom> for object: "+description());
		System.err.println("UFProtocol.recvFrom> Bytes Read = "+numRead+". Expected: "
				   + nbToRead + ". Discarding the extraneous data.");
		try {
		    byte[] byteStream = new byte[nbToRead];
		    inpStrm.readFully( byteStream );
		}
		catch (IOException ioe) {
		    System.err.println("UFProtocol.recvFrom> error trying to read more: "
				       +ioe.toString());
		}
	    }
	    return nbHead + numRead;
	}
	else return nbHead;
    }

    // creates a new instance of UFProtocol object from a file,
    // returns null on failure:

    public static UFProtocol createFrom(File f)
    {
      	UFProtocol ufpObj = create( readLengthAndType(f) );
	if( ufpObj != null ) ufpObj.readFrom(f);
	return ufpObj;
    }
  
    protected int readFrom(File f) {
	try {
	    DataInputStream inpStrm = new DataInputStream(new FileInputStream(f));
	    return recvFrom(inpStrm);
	}
	catch( Exception e ) {
	    System.err.println("UFProtocol::readFrom(File)> "+e.toString());
	    return 0;
	}
    }

    //send protocol header attributes (including length & type) to output stream:

    protected int sendHeader(DataOutputStream outpStrm) {
	int nbsent=0;
	try {
	    outpStrm.writeInt(_length);
	    nbsent += 4;
	    outpStrm.writeInt(_type);
	    nbsent += 4;
	    outpStrm.writeInt(_elem);
	    nbsent += 4;
	    outpStrm.writeShort(_seqCnt);//2 shorts -- seqcnt, seqtot
	    nbsent += 2;
	    outpStrm.writeShort(_seqTot);
	    nbsent += 2;
	    outpStrm.writeFloat(_duration);//duration
	    nbsent += 4;
	    outpStrm.writeBytes(_timestamp);
	    nbsent += _timestamp.length();
	    outpStrm.writeInt(_name.length());
	    nbsent += 4;
	    if( _name.length() > 0 ) {
		outpStrm.writeBytes(_name);
		nbsent += _name.length();
	    }
	    outpStrm.flush();
	}
	catch(EOFException eof) {
	    System.out.println("UFProtocol::sendHeader> "+eof);
	} 
	catch(IOException ioe) {
	    System.out.println("UFProtocol::sendHeader> "+ioe);
	}
	catch( Exception e ) {
	    System.out.println("UFProtocol::sendHeader> "+e);
	}
	return nbsent;
    }

    // send msg out to socket, returns 0 on failure, num. bytes on success

    public int sendTo(DataOutputStream outpStrm)
    {
	int nbHead = sendHeader(outpStrm);

	if( nbHead < _MinLength_ ) {
	    System.err.println("UFProtocol::sendTo> failed sending header info!");
	    return 0;
	}

	if( _elem > 0 ) {
	    int numSent = sendData(outpStrm);
	    int nbToSend = _length - nbHead;
	    if (numSent == 0) {
		System.err.println("UFProtocol.sendTo> for object: "+description());
		System.err.println("UFProtocol.sendTo> no data values sent:"+
				   " Expected: " + _elem + " values = " + nbToSend + " bytes.");
	    }
	    else if( numSent > nbToSend ) {
		System.err.println("UFProtocol.sendTo> for object: "+description());
		System.err.println("UFProtocol.sendTo> Sent more than length specified in header:"
				   +" Bytes Sent = "+numSent+" > "+nbToSend);
	    }
	    else if( numSent < nbToSend ) {
		System.err.println("UFProtocol.sendTo> for object: "+description());
		System.err.println("UFProtocol.sendTo> Sent less than length specified in header:"
				   +" Bytes Sent = "+numSent+" < "+nbToSend);
	    }
	    return nbHead + numSent;
	}
	else return nbHead;
    }

    public int sendTo(Socket soc) {
	try {
	    DataOutputStream outpStrm = new DataOutputStream(soc.getOutputStream());
	    return sendTo(outpStrm);
	}
	catch(IOException ioe) {
	    System.err.println("UFProtocol::sendTo(Socket)> "+ioe.toString());
	    ioe.printStackTrace();
	    return 0;
	}
	catch(Exception x) {
	    System.err.println("UFProtocol::sendTo(Socket)> "+x.toString());
	    x.printStackTrace();
	    return(-1);
	}
    }

    // write (serialize) msg out to file, returns 0 on failure, num. bytes output on success

    public int writeTo(File f) {
	try {
	    DataOutputStream outpStrm = new DataOutputStream(new FileOutputStream(f));
	    return sendTo(outpStrm);
	}
	catch( Exception e ) {
	    System.err.println("UFProtocol::writeTo(File)> "+e.toString());
	    return 0;
	}
    }

    // must have knowledge of all possible descendents in hierarchy:

    protected static UFProtocol create(UFProtocol.LengthAndType LTobj)
    {
	if( LTobj == null ) return null;
	UFProtocol ufpObj=null;

	switch(LTobj.type)
	    {
	    case MsgTyp._Strings_: ufpObj = new UFStrings(LTobj.length);
		break;
	    
	    case MsgTyp._Bytes_: ufpObj = new UFBytes(LTobj.length);
		break;
	    
	    case MsgTyp._Ints_: ufpObj = new UFInts(LTobj.length);
		break;
	    
	    case MsgTyp._Floats_: ufpObj = new UFFloats(LTobj.length);
		break;

	    case MsgTyp._FrameConfig_: ufpObj = new UFFrameConfig(LTobj.length);
		break;

	    case MsgTyp._ObsConfig_: ufpObj = new UFObsConfig(LTobj.length);
		break;    

	    case MsgTyp._TimeStamp_: // default
	    
	    default: ufpObj = new UFTimeStamp(LTobj.length);
		break;
	    }
	
	return ufpObj;
    }
} // UFProtocol



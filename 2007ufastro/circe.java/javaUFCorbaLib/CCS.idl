// This interface shall be provided by the CCS instrument Team
// Language: OMG-IDL

#include "GCS.idl"

module CCS  // CanariCam Control System
{
  interface Canaricam
  {
    // Structure giving number of chop beams, nod beams, save sets
    // and node sets in currently configured observation.
    struct ObsConfig
    {
	long chopBeams;      // # of chop beams (1 or 2).
  	long saveSets;       // # of save sets per nod set.
	long nodBeams;       // # of nod beams (1 or 2)
  	long nodSets;        // total # of nod sets.
  	long coaddsPerFrm;   // coadds done by MCE for each sent frame.
  	string readoutMode; // one of 7 possible detector readout modes. 
  	string dataLabel;   // label of current obs. data.
    };

    // readout modes:
    // S1          = single sample.
    // S1R1        = 2 samples with detector output clamp ref.
    // S1R1_raw    = same, just display and save raw samples.
    // S1R1_CR     = 2 samples with detector column clamp ref.
    // S1R1_CR_raw = same, just display and save raw samples.
    // S1R3        = 4 samples with 2 output clamps & 1 column clamp.
    // S1R3_raw    = same, just display and save raw samples.

    // Each frame of data acq. from MCE has associated parameters:
    struct FrameConfig
    {
      long w;      //width of each frame.
      long h;      //height...
      long d;      //depth in bits, of each frame.
      long DMAcnt;         //count # of DMAs to EDT fiber interface.
      long frameGrabCnt;   //count # of frames grabbed from EDT.
      long frameObsSeqNum; // # of frames processed
      long frameObsSeqTot; // total # of frames in obs.
      long chopBeam;       // current chop beam: 0 or 1.
      long saveSetCnt;     // current # of save (chop) sets.
      long nodBeam;        // current nod beam: 0 or 1.
      long nodSetCnt;      // current # of nod sets.
      long coadds;         // # of frames coadded in current nod set.
      long frameWriteCnt;  // # of frames written to FITS file.
      long frameSendCnt;   // # of frames sent to data out stream.
      long bg_ADUs;        // average # AD units in background.
      float bg_Well;      // average percent filling of det. wells.
      float sigmaFrameNoise; // std. Dev. Of noise in background.
      long off_ADUs;       // # AD units read when det. is clamped OFF.
      float off_Well;     // percent filling of wells when det. OFF.
      float sigmaReadNoise;  // std. Dev. read noise when det. is OFF.
    };

    // Returns the ObsConfig struct
    ObsConfig getObsConfig() 
      raises (DAF::GCSException);

    // Returns the FrameConfig struct
    FrameConfig getFrameConfig() 
      raises (DAF::GCSException);

    // Method for GTC to notify CCS that requested switch beam is
    // completed
    void beamSwitchDone() 
      raises (DAF::GCSException);

    // Returns true if an observation is in progress.
    boolean isObservationInProgress() 
      raises (DAF::GCSException);

    // Returns name of last written observation raw data FITS file.
    string getRawDataFileName() 
      raises (DAF::GCSException);

    // Returns the name of last written processed data FITS file.
    string getProcessedDataFileName() 
      raises (DAF::GCSException);

    // String in FITS format containing all instrument information.
    string instrumentFITSHeader() 
      raises (DAF::GCSException);

    // Returns the server name, in this case: CANARICAM
    string name() 
      raises (DAF::GCSException);

    // Returns the operational state of the CANARICAM instrument as
    // a whole. E.g. Exposing, setting mechanical elements.
    string operationalState() 
      raises (DAF::GCSException);

    // Returns a report of the instrument status and configuration. 
    // It should be the same information of the methods getObsConfig()
    // and getFrameConfig() but in string form.
    // detailLevel defines the detail of the information provided if 
    // there are different levels. TBC in the case of CanariCam.
    string report(in long detailLevel) 
      raises (DAF::GCSException);

    // Performs the startup procedure. E.g. initialiting
    // communication with attached hardware, so the instrument is
    // ready to receive any command. The combination of shutdown and
    // start provides the reset functionality.
    void start() 
      raises (DAF::GCSException);

    // Allows Canaricam to receive/send messages through interface.
    void enable() 
      raises (DAF::GCSException);


    // Canaricam shall not accept or send any command except the
    // enable or shutdown command.
    void disable() 
      raises (DAF::GCSException);

    // Aborts any operation
    void abort() 
      raises (DAF::GCSException);

    // Used for checking CORBA communication
    oneway void ping();
  };
};  // module CCS

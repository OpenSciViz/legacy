#include "GCS.idl"

module TelescopeServer
{
  interface Telescope_ifce
  {
    // Time in seconds from start of chopping until the chopper duty
    // cycle has reached the designated stable value;
    float getChopStabilizeTime() 
      raises (DAF::GCSException);

    // Percentage of time that the chopper is not in motion, i.e.
    // stable and pointing at either the source or reference beam
    float getChopDutyCycle() 
      raises (DAF::GCSException);

    // Method to configure the chopper with the desired chop throw and
    // position angle
    // NOTE: changed from throw to throw_offset to avoid conflicts
    //  with C++'s "throw" keyword
    // throw_offset: Relative offset in arcseconds between chop beam
    // (source and reference positions)
    // angle: Position angle in degrees of the line segment between
    // chop beams relative to north of standard astronomical 
    // coordinate system (FK5 epoc 2000).
    void setChopperParameters(in float throw_offset, in float angle) 
      raises (DAF::GCSException);

    // Command the chopper to accept chop trigger signal from hardware 
    // TTL input. This shall be invoked by any instrument that wants 
    // to use this mode prior to observation.
    void setChopperHardwareMode() 
      raises (DAF::GCSException);

    // Command the chopper to move to any of the positions
    //  in the chop profile definition.
    void setChopperPosition(in long positionNumber) 
      raises (DAF::GCSException);

    // Command the chopper to move back to the starting position.
    void setChopperHome() 
      raises (DAF::GCSException);

    // Method to request a telescope beam switch to be performed.
    void requestBeamSwitch() 
      raises (DAF::GCSException);

    // Method to request a telescope pointing offset from the current
    // position.
    // offsetRA
    // offsetDEC
    void requestTelescopeOffset(in double offsetRA, in double offsetDEC) 
      raises (DAF::GCSException);

   // Return the current right ascention of the telescope
    double getRightAscention()
      raises (DAF::GCSException);

    // Return the current declination of the telescope 
    double getDeclination()
      raises (DAF::GCSException);

    // Return the current angle from zenith of telescope
    double getZenithAngle()
      raises (DAF::GCSException);

    // Return the current azimuth angle of telescope.
    double getAzimuthAngle()
      raises (DAF::GCSException);

    // Return the current nod beam in which telescope is pointing (A 
    // or B) Dependent on chopper configuration: A = normal, 
    // B = telescope offset so that source is in reference beam of chopper.
    string getCurrentBeam() 
      raises (DAF::GCSException);

    // Return the current angle of instrument rotator on telescope.
    // Rotator Mechanical Angle in radians
    float getCurrentRotatorAngle() 
      raises (DAF::GCSException);

    // Returns the temperature of the M1 mirror in Kelvin degrees
    float getM1Temperature() 
      raises (DAF::GCSException);

    // Returns the overall emissivity in percentage
    float getOverallEmissivity() 
      raises (DAF::GCSException);

    // Returns the average relative humidity measurement in the 
    // telescope enclosure in percentage
    float getHumidity() 
      raises (DAF::GCSException);

    // Returns the airmass meassurement
    float getAirMass() 
      raises (DAF::GCSException);

    // String in FITS format containing all information relevant to 
    // the observation about the telescope and observatory
    string getFITSHeader() 
      raises (DAF::GCSException);

    // Method to signal GTC that observation is completed and data 
    // resides in specified files.
    void completedObservation(in string fileNameRawData, 
                              in string fileNameProcData) 
      raises (DAF::GCSException);

    // Allows the Instrument Control System to send an alarm to the 
    // GCS
    void sendAlarm(in ALARM::Event alarmMessage) 
      raises (DAF::GCSException);

    // Allows the Instrument Control System to send a log message to 
    // the GCS
    void sendLog(in LOG::Record logMessage) 
      raises (DAF::GCSException);

    // Allow the Instrument Control System to get the TAI time from 
    // the GCS. 
    DAF::TimeValue getTime() 
      raises (DAF::GCSException);

    // Used to issue a CORBA shutdown
    oneway void shutdown() ;

    // Used to verify CORBA communication
    oneway void ping() ;
  };
};

#if !defined(__annex_cc__)
#define __annex_cc__ "$Name:  $ $Id: annex.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __annex_cc__;

#include "UFAnnexIO.h"
__UFAnnexIO_H__(annex_cc);

int main(int argc, char** argv) {
  // just call UFAnnexIO main:
  return UFAnnexIO::main(argc, argv);
}

#endif // __annex_cc__

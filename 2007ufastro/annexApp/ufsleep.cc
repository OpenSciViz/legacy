#include "stdlib.h"
#include "UFPosixRuntime.h"

int main(int argc, char** argv) {
  float sec = 0.5;
  if( argc > 1 )
    sec = atof(argv[1]);

  return UFPosixRuntime::sleep(sec);
}

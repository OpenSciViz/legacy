#if !defined(__UFDaemon_h__)
#define __UFDaemon_h__ "$Name:  $ $Id: UFDaemon.h,v 0.2 2004/09/17 19:19:08 hon Exp $"
#define __UFDaemon_H__(arg) const char arg##Daemon_h__rcsId[] = __UFDaemon_h__;

#include "string"

#include "UFRuntime.h"
#include "UFRingBuff.h"
#include "UFProtocol.h"
#include "UFInts.h"

#include "map"
using std::map;

// allow any daemon to optionally archive files (raw or fits):
// this needs to be moved out of here... 
struct UFArchive {
  int fd;
  string outfile; // output filename (hint)
  string fitsfile; // fits template filename
  string fitsHdr; // contents of template
  string slutfile; // sort lut filename
  UFProtocol* sortbuf;
  UFInts* sortLUT;
  static int applySort(const UFInts& sortLUT, const UFProtocol& in, UFProtocol& out);
  // numvals <= ifp->numVals (when only using fraction of UFprotocol buffer)
  static int writeFile(UFArchive& arc, UFProtocol* ufp, int numvals= 0, int filecnt= -1, int modulo= 1);
  inline UFArchive() : fd(-1), sortbuf(0), sortLUT(0) {}
};

class UFDaemon : public UFRuntime {
protected:
  string _name, _usage;
  static UFRuntime::Argv* _args;
  static char** _envp;

public:
  enum { _Process_=0, _Thread_=1 };
  typedef union { pid_t p; pthread_t t; } daemon_t; // store a pid_t or pthread_t
  // keep track of all threads/or child procs:
  typedef vector< UFDaemon* > DaemonVec;
  // ditto, keyed with pid or thread_id:
  typedef std::map< UFDaemon::daemon_t*, UFDaemon* > DaemonTable;
  // named buffers can be either heap or shared memory based:
  typedef std::map< string, UFRingBuff* > RingBuffTable;
 
  // if this it used, it must be the first thing executed by exec,
  // turn self into daemon via the Steven's example:
  static int daemonize();

  // return path to the executable file of this process ("which" on myself):
  // this can be used for finding co-located binaries that are meant to
  // be run together in a self-consistent fashion
  string pathToSelf();

  UFDaemon(const string& name= "Anonymous", char** envp= 0);
  UFDaemon(int argc, char** argv, char** envp= 0);
  UFDaemon(const string& name, int argc, char** argv, char** envp= 0);
  UFDaemon(const string& name,UFRuntime::Argv* args,char** envp= 0);
    
  inline virtual ~UFDaemon() { delete _args; }

  // find specific arg: 
  inline virtual string findArg(char *arg) { if ( _args != 0 )  return argVal(arg, *_args); return "false"; }
  // return _args ptr
  inline static UFRuntime::Argv* getArgs() { return _args; }
  // return envp
  inline static char** envp() { return _envp; }

  virtual string description() const = 0;
  // create shared resource(s) to be used by this daemon by
  // call/delegate-to the specific daemon's create function:
  virtual int createSharedResources() { return 0; }
  // delete all resources
  virtual int deleteSharedResources() { return 0; }

  inline virtual string name() const { return UFDaemon::_name; }
  inline virtual string rename(const string& name) { UFDaemon::_name= name; return UFDaemon::_name; }
  virtual string heartbeat();

  // this is the key virtual function that must be overriden
  virtual void* exec(void* p=0);

  // convenience function for use with UFPosixRuntime::newThread():
  static void* execThread(void* p=0);

};

// try this for pthread_create?
//void* (UFDaemon::*DaemonExecPtr)(void *);

#endif // __UFDaemon_h__

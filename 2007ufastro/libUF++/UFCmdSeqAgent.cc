#if !defined(__UFCmdSeqAgent_cc__)
#define __UFCmdSeqAgent_cc__ "$Name:  $ $Id: UFCmdSeqAgent.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFCmdSeqAgent_cc__;

#include "UFCmdSeqAgent.h"
__UFCmdSeqAgent_H__(__UFCmdSeqAgent_cc);

#include "time.h"

// default behavior of virtuals:
string UFCmdSeqAgent::heartbeat() {
  string sl = currentTime("local");
  string sg = currentTime("gmt");
  string t = sl + " (" + sg + " gmt)";
  return t;
}

void* UFCmdSeqAgent::exec(void* p) {
  // this is the main function for the Image Server CmdSeqAgent
  // a unit test would simply call this directly from main
  // the system executive should call this immediately upon
  // creation of the child process
  do {
    clog<<"UFCmdSeqAgent::heartbeat at t= "<<heartbeat()<<endl;
    sleep(600); // 10 min.
  } while( true );
}

#endif // __UFCmdSeqAgent_cc__

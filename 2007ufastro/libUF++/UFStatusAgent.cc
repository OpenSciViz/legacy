#if !defined(__UFStatusAgent_cc__)
#define __UFStatusAgent_cc__ "$Name:  $ $Id: UFStatusAgent.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFStatusAgent_cc__;

#include "UFStatusAgent.h"
__UFStatusAgent_H__(__UFStatusAgent_cc);

#include "time.h"

// default behavior of virtuals:
string UFStatusAgent::heartbeat() {
  string sl = currentTime("local");
  string sg = currentTime("gmt");
  string t = sl + " (" + sg + " gmt)";
  return t;
}

void* UFStatusAgent::exec(void* p) {
  // this is the main function for the Image Server StatusAgent
  // a unit test would simply call this directly from main
  // the system executive should call this immediately upon
  // creation of the child process
  do {
    clog<<"UFStatusAgent::heartbeat at t= "<<heartbeat()<<endl;
    sleep(600); // 10 min.
  } while( true );
}

#endif // __UFStatusAgent_cc__

#if !defined(__UFOCSClient_h__)
#define __UFOCSClient_h__ "$Name:  $ $Id: UFOCSClient.h,v 0.1 2004/09/17 19:19:11 hon Exp $"
#define __UFOCSClient_H__(arg) const char arg##OCSClient_h__rcsId[] = __UFOCSClient_h__;

#include "UFDaemon.h"
#include "string"

class UFOCSClient : public UFDaemon {
public:
  inline UFOCSClient(const string& name) : UFDaemon(name) {}
  inline UFOCSClient(int argc, char** argv) : UFDaemon(argc, argv) {}
  inline UFOCSClient(const string& name, int argc, char** argv) : UFDaemon(name, argc, argv) {}
  inline virtual ~UFOCSClient() {}

  inline virtual string description() const { return __UFOCSClient_h__; }

  virtual string heartbeat();

  // this is the key virtual function that must be overriden
  virtual void* exec(void* p= 0);
};

#endif // __UFOCSClient_h__

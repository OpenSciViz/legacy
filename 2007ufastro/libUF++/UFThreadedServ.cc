#if !defined(__UFThreadedServ_cc__)
#define __UFThreadedServ_cc__ "$Name:  $ $Id: UFThreadedServ.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFThreadedServ_cc__;

#include "UFThreadedServ.h"
__UFThreadedServ_H__(__UFThreadedServ_cc);

#include "new"
#include "string"
#include "map"
#include "list"
#include "vector"

#include "pthread.h"

#include "ctime"
#include "cstring"
#include "cerrno"

#include "UFSocket.h"
#include "UFServerSocket.h"

using std::string ;
using std::map ;
using std::list ;
using std::vector ;

UFThreadedServ::UFThreadedServ( const string& name )
  : UFDaemon( name )
{
  init() ;
}

UFThreadedServ::UFThreadedServ( int argc, char** argv )
  : UFDaemon( argc, argv )
{
  init() ;
}

UFThreadedServ::UFThreadedServ(const string& name, int argc, char** argv)
  : UFDaemon( name, argc, argv )
{
  init() ;
}

UFThreadedServ::~UFThreadedServ()
{
  // Notify threads of server shutdown
  keepRunning = false ; 

  // Make sure to clear the listeners first
  ::pthread_mutex_lock( &listenThreadListMutex ) ;
  for( listenThreadListType::iterator ptr = listenThreadList.begin() ;
       ptr != listenThreadList.end() ; ++ptr )
    {
      if( ::pthread_join( ptr->first, 0 ) != 0 )
	{
	  clog << "~UFThreadedServ> Failed to join listen thread: "
	       << strerror( errno ) << endl ;
	}
      delete ptr->second ;
    }
  listenThreadList.clear() ;
  ::pthread_mutex_unlock( &listenThreadListMutex ) ;

  // Stop all client threads
  ::pthread_mutex_lock( &clientThreadListMutex ) ;
  for( clientThreadListType::iterator ptr = clientThreadList.begin() ;
       ptr != clientThreadList.end() ; ++ptr )
    {
      if( ::pthread_join( ptr->first, 0 ) != 0 )
	{
	  clog << "~UFThreadedServ> Failed to join client thread: "
	       << strerror( errno ) << endl ;
	}
      delete ptr->second ;
    }
  clientThreadList.clear() ;
  ::pthread_mutex_unlock( &clientThreadListMutex ) ;

  // Finally, join() any dead threads
  ::pthread_mutex_lock( &deadThreadListMutex ) ;
  for( deadThreadListType::iterator ptr = deadThreadList.begin() ;
       ptr != deadThreadList.end() ; ++ptr )
    {
      if( ::pthread_join( *ptr, 0 ) != 0 )
	{
	  clog << "~UFThreadedServ> Failed to join dead thread: "
	       << strerror( errno ) << endl ;
	}
    }
  ::pthread_mutex_unlock( &deadThreadListMutex ) ;

  ::pthread_mutex_destroy( &clientThreadListMutex ) ;
  ::pthread_mutex_destroy( &listenThreadListMutex ) ;
  ::pthread_mutex_destroy( &deadThreadListMutex ) ;

}

void UFThreadedServ::init()
{
  keepRunning = true ;

  // initialize mutexes
  ::pthread_mutex_init( &clientThreadListMutex, 0 ) ;
  ::pthread_mutex_init( &listenThreadListMutex, 0 ) ;
  ::pthread_mutex_init( &deadThreadListMutex, 0 ) ;
}

// default behavior of virtuals:
string UFThreadedServ::heartbeat() {
  string sl = currentTime("local");
  string sg = currentTime("gmt");
  string t = sl + " (" + sg + " gmt)";
  return t;
}

void* UFThreadedServ::exec(void* p) {
  // this is the main function for the Image Server ThreadedServ
  // a unit test would simply call this directly from main
  // the system executive should call this immediately upon
  // creation of the child process

  // Responsibilities:
  // Handling dead threads
  // Checking all other threads to make sure they are still
  // running; add any stopped threads to the dead thread list

  deadThreadListType localDeadList ;

  while( keepRunning )
    {

      ::usleep( 10000 ) ;
      localDeadList.clear() ;

      if( !deadThreadList.empty() )
	{
	  ::pthread_mutex_lock( &deadThreadListMutex ) ;
	  for( deadThreadListType::iterator ptr = deadThreadList.begin() ;
	       ptr != deadThreadList.end() ; ++ptr )
	    {
	      localDeadList.push_back( *ptr ) ;
	    }
	  deadThreadList.clear() ;
	  ::pthread_mutex_unlock( &deadThreadListMutex ) ;
	}

      // Run through both of the listenThreadList and clientThreadList
      // checking for stopped threads; add any to the deadThreadList
      ::pthread_mutex_lock( &listenThreadListMutex ) ;
      for( listenThreadListType::iterator ptr = listenThreadList.begin() ;
	   ptr != listenThreadList.end() ; ++ptr )
	{
	  if( !ptr->second->keepRunning )
	    {
	      // The thread has been shut down
	      localDeadList.push_back( ptr->first ) ;
	      delete ptr->second ;
	      listenThreadList.erase( ptr ) ;
	    }
	}
      ::pthread_mutex_unlock( &listenThreadListMutex ) ;


      ::pthread_mutex_lock( &clientThreadListMutex ) ;
      for( clientThreadListType::iterator ptr = clientThreadList.begin() ;
	   ptr != clientThreadList.end() ; ++ptr )
	{
	  if( !ptr->second->keepRunning )
	    {
	      localDeadList.push_back( ptr->first ) ;
	      delete ptr->second ;
	      clientThreadList.erase( ptr ) ;
	    }
	}
      ::pthread_mutex_unlock( &clientThreadListMutex ) ;

      if( !localDeadList.empty() )
	{
	  // Each of the dead threads has already deallocated its own
	  // information, and removed itself from its list (listenThreadList
	  // or clientThreadList).  All we have to do here is join()
	  for( deadThreadListType::iterator ptr = localDeadList.begin() ;
	       ptr != localDeadList.end() ; ++ptr )
	    {
	      if( ::pthread_join( *ptr, 0 ) != 0 )
		{
		  clog << "UFThreadedServ::exec> Failed to join thread: "
		       << strerror( errno ) << endl ;
		  continue ;
		}
	    }
	  localDeadList.clear() ;

	}

    } // while( keepRunning )
  return 0 ;
} // exec()

bool UFThreadedServ::addListener( const unsigned short int& whichPort )
{
  ::pthread_mutex_lock( &listenThreadListMutex ) ;

  // First make sure that we're not already listening on this
  // port.
  for( listenThreadListType::const_iterator ptr = listenThreadList.begin() ;
       ptr != listenThreadList.end() ; ++ptr )
    {
      if( whichPort == ptr->second->whichPort )
	{
	  // Already have a listener for this port
	  clog << "UFThreadedServ::addListener> Already listening on "
	       << "port " << whichPort << endl ;
	  ::pthread_mutex_unlock( &listenThreadListMutex ) ;
	  return false ;
	}
    }

  // Not currently listening on whichPort
  UFServerSocket* ss = new (nothrow) UFServerSocket ;
  if( NULL == ss )
    {
      clog << "UFThreadedServ::addListener> Memory allocation failure\n" ;
      ::pthread_mutex_unlock( &listenThreadListMutex ) ;
      return false ;
    }

  if( ss->listen( whichPort ).fd < 0 )
    {
      clog << "UFThreadedServ::addListener> Failed to listen on port "
	   << whichPort << " because: " << strerror( errno ) << endl ;
      ss->close() ;
      delete ss ; ss = 0 ;
      ::pthread_mutex_unlock( &listenThreadListMutex ) ;
      return false ;
    }
  
  threadInfo* tInfo = new (nothrow) threadInfo( ss,
						whichPort ) ;
  bootStrap* bs = new (nothrow) bootStrap( this, tInfo ) ;
  if( NULL == tInfo || NULL == bs )
    {
      clog << "UFThreadedServ::addListener> Memory allocation failure\n" ;
      ss->close() ;
      delete ss ; ss = 0 ;
      ::pthread_mutex_unlock( &listenThreadListMutex ) ;

      delete tInfo ; tInfo = 0 ;
      delete bs ; bs = 0 ;
      return false ;
    }

  pthread_t threadID = 0 ;
  if( ::pthread_create( &threadID,
			0,
			reinterpret_cast< void* (*)(void*) >( listenThreadStub ),
			static_cast< void* >( bs ) ) != 0 )
    {
      clog << "UFThreadedServ::addListener> Failed to spawn listener "
	   << "thread because: " << strerror( errno ) << endl ;
      ss->close() ;
      delete ss ; ss = 0 ;
      delete bs ; bs = 0 ;
      delete tInfo ; tInfo = 0 ;
      ::pthread_mutex_unlock( &listenThreadListMutex ) ;
      return false ;
    }

  listenThreadList.insert( listenThreadListType::value_type( threadID,
							     tInfo ) ) ;

  ::pthread_mutex_unlock( &listenThreadListMutex ) ;

  return true ;
}

bool UFThreadedServ::removeListener( const unsigned short int& whichPort )
{
  ::pthread_mutex_lock( &listenThreadListMutex ) ;

  listenThreadListType::iterator ptr = listenThreadList.begin() ;
  for( ; ptr != listenThreadList.end() ; ++ptr )
    {
      if( whichPort == ptr->second->whichPort )
	{
	  // Found it
	  break ;
	}
    }

  if( ptr == listenThreadList.end() )
    {
      // Couldn't find it
      clog << "UFThreadedServ::removeListener> Failed to find listener "
	   << "for port " << whichPort << endl ;
      ::pthread_mutex_unlock( &listenThreadListMutex ) ;
      return false ;
    }

  // Found it, ask the thread to terminate itself
  // The thread will close and deallocate its socket
  ptr->second->keepRunning = false ;

  pthread_t id = ptr->first ;

  // Remove from the listenThreadList
  listenThreadList.erase( ptr ) ;

  // Make sure to remove the lock on listenThreadListMutex
  // BEFORE locking any other mutexes
  ::pthread_mutex_unlock( &listenThreadListMutex ) ;

  // Add to deadThreadList for exec() to clean up
  ::pthread_mutex_lock( &deadThreadListMutex ) ;
  deadThreadList.push_back( id ) ;
  ::pthread_mutex_unlock( &deadThreadListMutex ) ;

  return true ;
}

void* UFThreadedServ::listenThreadStub( void* data )
{
  bootStrap* bs = static_cast< bootStrap* >( data ) ;
  if( NULL == bs )
    {
      clog << "UFThreadedServ::listenThreadStub> Cast failure\n" ;
      return 0 ;
    }
  threadInfo* tInfo = bs->tInfo ;
  UFThreadedServ* callBack = bs->callback ;

  delete bs ; bs = 0 ;

  callBack->listenThread( tInfo ) ;
  return 0 ;
}

void* UFThreadedServ::clientThreadStub( void* data )
{
  bootStrap* bs = static_cast< bootStrap* >( data ) ;
  if( NULL == bs )
    {
      clog << "UFThreadedServ::clientThreadStub> Cast failure\n" ;
      return 0 ;
    }
  threadInfo* tInfo = bs->tInfo ;
  UFThreadedServ* callBack = bs->callback ;

  delete bs ; bs = 0 ;

  callBack->clientThread( tInfo ) ;
  return 0 ;
}

void UFThreadedServ::listenThread( threadInfo* info )
{
  UFServerSocket* ss = dynamic_cast< UFServerSocket* >( info->sock ) ;
  if( NULL == ss )
    {
      clog << "UFThreadedServ::listenThread> Socket cast failed\n" ;
      return ;
    }

  while( info->keepRunning && keepRunning )
    {

      ::usleep( 10000 ) ;

      int readable = ss->readable() ;
      if( readable < 0 )
	{
	  clog << "UFThreadedServ::listenThread> Socket error: "
	       << strerror( errno ) << endl ;
	  info->keepRunning = false ;
	  continue ;
	}
      else if( 0 == readable )
	{
	  // No data available, no problem.
	  continue ;
	}

      // Data is available, try to accept a new client
      UFSocket* newSock = new (nothrow) UFSocket ;
      if( NULL == newSock )
	{
	  clog << "UFThreadedServ::listenThread> Memory allocation "
	       << "failure " << endl ;
	  // Hope that it fixes itself
	  continue ;
	}

      // Call the blocking accept()
      if( ss->accept( *newSock ) < 0 )
	{
	  clog << "UFThreadedServ::listenThread> Failed to receive "
	       << "new client connection: " << strerror( errno )
	       << endl ;
	  newSock->close() ;
	  delete newSock ; newSock = 0 ;
	  continue ;
	}

      // Got a new client connection
      // Spawn the new thread here to reduce bloat in exec()
      threadInfo* tInfo = new (nothrow) threadInfo( newSock,
						    info->whichPort ) ;
      if( NULL == tInfo )
	{
	  clog << "UFThreadedServ::listenThread> Memory allocation "
	       << "failure\n" ;
	  newSock->close() ;
	  delete newSock ; newSock = 0 ;
	  continue ;
	}

      pthread_t threadID = 0 ;
      if( ::pthread_create( &threadID,
			    0,
			    reinterpret_cast< void* (*)(void*) >( clientThreadStub ),
			    static_cast< void* >( tInfo ) ) != 0 )
	{
	  clog << "UFThreadedServ::listenThread> Failed to spawn client "
	       << "thread: " << strerror( errno ) << endl ;
	  delete tInfo ; tInfo = 0 ;
	  newSock->close() ;
	  delete newSock ; newSock = 0 ;
	  continue ;
	}

      // Add to client list
      ::pthread_mutex_lock( &clientThreadListMutex ) ;
      clientThreadList.insert( clientThreadListType::value_type( threadID,
								 tInfo ) ) ;
      ::pthread_mutex_unlock( &clientThreadListMutex ) ;

    } // while()

  // Clean up
  ss->close() ;
  delete ss ;
 
  ss = 0 ;
  info->sock = 0 ;

  // exec() will see that this thread is stopped and will join()
  info->keepRunning = false ;

} // listenThread

void UFThreadedServ::clientThread( threadInfo* info )
{

  while( info->keepRunning && keepRunning )
    {

      ::usleep( 10000 ) ;

      int readable = info->sock->readable() ;
      if( readable < 0 )
	{
	  clog << "UFThreadedServ::clientThread> Socket error: "
	       << strerror( errno ) << endl ;
	  info->keepRunning = false ;
	  continue ;
	}
      else if( 0 == readable )
	{
	  continue ;
	}

      UFProtocol* ufp = UFProtocol::createFrom( *info->sock ) ;
      if( NULL == ufp )
	{
	  clog << "UFThreadedServ::clientThread> Read error: "
	       << strerror( errno ) << endl ;
	  info->keepRunning = false ;
	  continue ;
	}

      // Call the pure virtual handler method
      vector< UFProtocol* > reply = handleObject( ufp ) ;

      // Deallocate the object read from the socket
      delete ufp ;

      for( vector< UFProtocol* >::iterator ptr = reply.begin() ;
	   ptr != reply.end() ; ++ptr )
	{
	  if( info->sock->send( *ptr ) < 0 )
	    {
	      clog << "UFThreadedServ::clientThread> Write error: "
		   << strerror( errno ) << endl ;
	      info->keepRunning = false ;
	      break ;
	    }
	}

      for( vector< UFProtocol* >::iterator ptr = reply.begin() ;
	   ptr != reply.end() ; ++ptr )
	{
	  delete *ptr ;
	}
      reply.clear() ;

    }

  // When a client shuts down, it can just set it's keepRunning to
  // false for exec() to handle
  info->sock->close() ;
  delete info->sock ; info->sock = 0 ;

  // Main will see that this thread is no longer running and clean up
}

bool UFThreadedServ::removeClient( const pthread_t& whichClient )
{
  ::pthread_mutex_lock( &clientThreadListMutex ) ;
  clientThreadListType::iterator ptr = clientThreadList.find( whichClient ) ;
  if( ptr == clientThreadList.end() )
    {
      clog << "UFThreadedServ::removeClient> Unable to find client: "
	   << whichClient << endl ;
      ::pthread_mutex_unlock( &clientThreadListMutex ) ;
      return false ;
    }

  // Remove the thread from the clientThreadList
  clientThreadList.erase( ptr ) ;

  ::pthread_mutex_unlock( &clientThreadListMutex ) ;

  // Ask the thread to shutdown
  ptr->second->keepRunning = false ;

  // Add the dead thread to the deadThreadList
  ::pthread_mutex_lock( &deadThreadListMutex ) ;
  deadThreadList.push_back( ptr->first ) ;
  ::pthread_mutex_unlock( &deadThreadListMutex ) ;

  return true ;
}

pthread_t UFThreadedServ::findClientID( const threadInfo* info )
{
  // Protected method, don't need to assert info != NULL
  pthread_t id = 0 ;
  ::pthread_mutex_lock( &clientThreadListMutex ) ;

  clientThreadListType::const_iterator ptr = clientThreadList.begin() ;
  for( ; ptr != clientThreadList.end() ; ++ptr )
    {
      if( info == ptr->second )
	{
	  // Found it
	  id = ptr->first ;
	  break ;
	}
    }

  ::pthread_mutex_unlock( &clientThreadListMutex ) ;
  return id ;
}

pthread_t UFThreadedServ::findListenID( const threadInfo* info )
{
  pthread_t id = 0 ;
  ::pthread_mutex_lock( &listenThreadListMutex ) ;

  for( listenThreadListType::const_iterator ptr = listenThreadList.begin() ;
       ptr != listenThreadList.end() ; ++ptr )
    {
      if( info == ptr->second )
	{
	  // Found it
	  id = ptr->first ;
	  break ;
	}
    }

  ::pthread_mutex_unlock( &listenThreadListMutex ) ;
  return id ;

}

#endif // __UFThreadedServ_cc__

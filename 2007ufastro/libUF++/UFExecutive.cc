#if !defined(__UFExecutive_cc__)
#define __UFExecutive_cc__ "$Name:  $ $Id: UFExecutive.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFExecutive_cc__;
#include "UFExecutive.h"
__UFExecutive_H__(__UFExecutive_cc);

#include "unistd.h"
#include "UFFrameGrab.h"
#include "UFImgPreProcess.h"
#include "UFImgServ.h"
#include "UFCmdSeqAgent.h"

UFExecutive::DaemonVec* UFExecutive::_daemonvec= 0;
UFExecutive::DaemonTable* UFExecutive::_daemontable= 0;
UFExecutive::RingBuffTable* UFExecutive::_ringbuffs= 0;

void UFExecutive::_sigHandler(int signum) {
  clog<<"UFAiresExecutive::_sigHandler> signum= "<<signum<<endl;
  setInterrupt(signum);
  switch(signum) {
    case SIGPIPE: // client broke connection, figure out which client thread to exit...
      clog<<"UFAiresExecutive::_sigHandler> SIGPIPE lost client connection?"<<endl;
      break;

    case SIGINT:
      clog<<"UFAiresExecutive::_sigHandler> SIGINT -- exit"<<endl;
      exit(0);
      break;

    default:
      UFPosixRuntime::defaultSigHandler(signum);
      break;
  }
}

// for external use:
int UFExecutive::main(int argc, char** argv) {
  UFExecutive* theInstance = new UFExecutive(argc, argv);
  UFDaemon::Argv* args =  theInstance->getArgs(); // init args from _argc, _argv
  clog<<"UFExecutive::main> argc= "<<args->size()<<", start-time= "<<currentTime()<<endl;
  theInstance->exec((void*) args);
  return 0;
}

void* UFExecutive::exec(void* p) {
  // this is the main entry function for the executive Daemon
  // a unit test would simply call this directly from main
  // the system executive creates all shared memory and
  // semaphore and any other system IPC elements. then
  // the system executive should instance each daemon
  // process object and spawn a child process that
  // runs that object's exec function
  UFRuntime::Argv* args = (UFRuntime::Argv*) p;
   
  if( threaded()) {
    pthread_t sigthread = sigWaitThread(_sigHandler);
    if( sigthread < 0 ) {
      clog<<"UFExecutive> Unable to create signale handler thread"<<endl;
      exit(1);
    }
  }
  else {  // init the signal handler: (non-threaded mechanism!)
    setSignalHandler(_sigHandler);
  }
  
  int status= 0;
  UFFrameGrab* fgb= 0;
  UFImgPreProcess* ipp= 0;
  UFImgServ* isv= 0;

  // this is solely responsible for using the Edt device: 
  if( findArg("-fgrb") != "false" ) {
    fgb = new UFFrameGrab("UFFrameGrab", args);
    status= fgb->createSharedResources();
    _ringbuffs->insert(RingBuffTable::value_type(fgb->_theDMARingBuff->getRingName(), fgb->_theDMARingBuff));
    _daemonvec->push_back(fgb);
  }

  // archive raw frames to disk, sort pixels, diff. and/or do-add into separate buffers: 
  if( findArg("-imgpp") != "false" ) {
    ipp  = new UFImgPreProcess("UFImgPreProcess", args);
    status= ipp->createSharedResources();
    //_ringbuffs->insert(RingBuffTable::value_type(ipp->_theArchiveBuff->getRingName(), fgb->_theArchiveBuff));
    _daemonvec->push_back(ipp);
  }

  // make buffers available to clients:
  if( findArg("-imgsrv") != "false" ) {
    isv = new UFImgServ("UFImgServ", args);
    status= isv->createSharedResources();
    //_ringbuffs->insert(RingBuffTable::value_type(ipp->_theArchiveBuff->getRingName(), fgb->_theArchiveBuff));
    _daemonvec->push_back(isv);
  }

  if( status < 0 ) {
    clog<<"UFExecutive> Unable to create (all) shared resources"<<endl;
    if( fgb ) fgb->deleteSharedResources();
    if( ipp ) ipp->deleteSharedResources();
    if( isv ) isv->deleteSharedResources();
    _daemonvec->clear();
    return p;
  }

  int dcnt = startDaemons();

  // finally, enter into command agent event loop:
  if( argVal("-cmd", *args) != "false" ) {
    UFCmdSeqAgent* cmd = new UFCmdSeqAgent("UFCmdAgent", args);
    if( cmd ) {
      // CmdAgent can recreate shared resource & restart all daemons if needed
      return cmd->exec((void*) this);
    }
  }

  // if we get here there is no command agent requested?    
  do {
    clog<<"UFExecutive heartbeat at t= "<<heartbeat()<<endl;
    sleep(600);
  } while( dcnt > 0 );

  return p;
}

// this starts all daemons, except the command agent,
// which is now simply run from the executive's main (exec func):
// default behavior for virtual multiProcs func:
bool UFExecutive::startDaemons() {
  // since this can also be used to re-start the daemons, it should reset the demon-id table;
  // check that _daemons have been cleared?
  if( _daemontable != 0 && _daemontable->size() > 0 ) {
    clog<<"UFExecutive::startDaemons> ? daemons->size() == "<<_daemontable->size()<<endl;
    killDaemons(); // shutdown threads/procs & clear table
  }

  for( int i = 0; i < (int) _daemonvec->size(); i++ ) {
    UFDaemon::daemon_t* id= new UFDaemon::daemon_t;
    UFDaemon* dp =  (*_daemonvec)[i];
    if( threaded() ) {
      id->p = createChild(dp);
    }
    else {
      //FrameGrabExecPtr = &UFFrameGrab::exec;
      //UFPosixRuntime::ThreadStart fp = reinterpret_cast< UFPosixRuntime::ThreadStart > (FrameGrabExecPtr);
      //d->t = newThread(fp, fgb);
      id->t = newThread(dp->execThread, (void*) dp);
    }
    _daemontable->insert(DaemonTable::value_type(id, dp));
    yield();
  }

  return (_daemontable->size() == _daemonvec->size());
}

#endif // __UFExecutive_cc__

#if !defined(__UFFrameGrab_cc__)
#define __UFFrameGrab_cc__ "$Id: UFFrameGrab.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFFrameGrab_cc__;

#include "UFFrameGrab.h"
__UFFrameGrab_H__(__UFFrameGrab_cc);

#include "UFEdtDMA.h"
__UFEdtDMA_H__(__UFFrameGrab_cc);

#include "UFRingBuff.h"
__UFRingBuff_H__(__UFFrameGrab_cc);

#include "UFDaemon.h"
__UFDaemon_H__(__UFFrameGrab_cc);

#include "UFBytes.h"
__UFBytes_H__(__UFFrameGrab_cc);

#include "UFInts.h"
__UFInts_H__(__UFFrameGrab_cc);

#include "iostream"
#include "unistd.h"
#include "limits.h"
#include "cstdlib"
     
#include "sys/types.h"
#include "sys/stat.h"
#include "fcntl.h"

#ifdef _NO_EDT_
// internal testing:
static unsigned char* _testVals= 0;
#endif

//static attributes
UFRingBuff* UFFrameGrab::_theDMARingBuff= 0;
UFEdtDMA::Conf* UFFrameGrab::_currentConf= 0;
UFEdtDMA::DevPtr UFFrameGrab::_edtp= 0;
int UFFrameGrab::_waitCnt= 1;

FILE* UFFrameGrab::_statusLog(const char* logmsg) {
  static FILE* log= 0;
  if( log == 0 )
    log = fopen("/tmp/UFFrameGrab.log", "w+");

  strstream s;
  s<<"UFFrameGrab> "<<UFRuntime::currentTime()<<" -- "<<logmsg<<ends;
  fprintf(log,"%s\n",s.str());
  clog<<s.str()<<endl;

  delete s.str();
  return log;
}     

int UFFrameGrab::_refCmp(unsigned char* buff, int elem, bool newRef) {
  static unsigned char* ref=0;
  static int badcnt;

  // first frame defines reference
  if( ref == 0 || newRef ) {
    delete ref;
    badcnt = 0;
    ref = new unsigned char[elem];
    memcpy(ref, buff, elem);
    strstream s;
    s<<"UFFrameGrab::_refCmp> badcnt= "<<badcnt<<ends;
    _statusLog(s.str()); delete [] s.str();
  }
  if( memcmp(ref, buff, elem) != 0 ) {
    badcnt++;
    strstream s;
    s<<"UFFrameGrab::_refCmp> badcnt= "<<badcnt<<ends;
    _statusLog(s.str()); delete [] s.str();
    // reset the reference frame
    memcpy(ref, buff, elem);
  }
  return badcnt;
}
/*  
void* UFFrameGrab::mainThread(void* p) {
  UFFrameGrab* instance = static_cast<UFFrameGrab*>(p);
  if( instance != 0 ) {
    instance->exec(p);
    return p;
  }
  else {
    clog<<"UFFrameGrab::mainThread> cast void* to UFFrameGrab* failed! can't exec."<<endl;
    return 0;
  }
}
*/  
/*
void UFFrameGrab::_sigHandler(int signum) {
  clog<<"UFFrameGrab::_sigHandler> signum= "<<signum<<endl;
  switch(signum) {
    case SIGPIPE:
    default: UFPosixRuntime::defaultSigHandler(signum);
  }
}
*/

// this is potentially usable in its own thread:
void UFFrameGrab::_saveToDisk(int cnt, UFArchive* arc) {
  int index= -1;
  UFProtocol* ufp = _theDMARingBuff->prevActiveBuf(index);
  if( ufp <= 0 ) {
    clog<<"UFFrameGrab::_saveToDisk>?  bad buffer pointe, cannot save..."<<endl;
    return;
  }
  int numvals = _currentConf->w * _currentConf->h;
  int nb = UFArchive::writeFile(*arc, ufp, numvals, cnt);
  if( nb <= 0 )
    clog<<"UFFrameGrab::_saveToDisk> failed to write "<<arc->outfile<<endl;
} 

void* UFFrameGrab::exec(void* p) {
  pid_t mypid = getpid();
 
  string arg;
  if( (arg = findArg("-?")) != "false" || (arg = findArg("-help")) != "false") {
    _usage = (*_args)[0] + _usage;
    _usage += " [-bytes] [-grabcnt cnt] [-grabfile filename_hint] [-fits fitstemplatefile] [-sort sortLUTfile]";
    cout<<_usage<<endl;
    return p;
  }

  UFFrameGrab::_currentConf = UFEdtDMA::getConf();
  if( _currentConf == 0 ) {
    clog<<"UFFrameGrab> unable to get EDT configuration! aborting..."<<endl;
    exit(1);
  }
  clog<<"UFFrameGrab> current config: "<<_currentConf->w<<"x"<<_currentConf->h<<"x"
      <<_currentConf->d<<endl;
  // simple cmdLine stuff here for now:
  int grabCnt= 0;
  bool useBytes= false;
  UFArchive* arc= 0;
  // ints are the default
  if( (arg = findArg("-bytes")) != "false" ) {
    useBytes = true;
    clog<<"UFFrameGrab> ("<<mypid<<") use Bytes"<<endl;
  }
  if( (arg = findArg("-chop")) != "false" || (arg = findArg("-pair")) != "false" ) {
    _waitCnt = 2;
    clog<<"UFFrameGrab> ("<<mypid<<") use Bytes"<<endl;
  }
  if( (arg = findArg("-grabcnt")) != "false" ) {
    grabCnt = atoi(arg.c_str());
    clog<<"UFFrameGrab> ("<<mypid<<") grabCnt = "<<grabCnt<<endl;
  }
  
  if( (arg = findArg("-grabfile")) != "false" ) {
    // grab and write to file(s)
    strstream s;
    s << arg <<"."<<_currentConf->w<<"x"<<_currentConf->h<<ends;
    arc = new UFArchive;
    arc->outfile = s.str(); delete s.str();
    arc->fd = 0;
    clog<<"UFFrameGrab> ("<<mypid<<") save frames to files: "<<arg<<endl;
    if( (arg = findArg("-fits")) != "false" ) {
      arc->fitsfile = arg;
      clog<<"UFFrameGrab> ("<<mypid<<") fits template file = "<<arg<<endl;
      arc->fitsHdr.reserve(2880);
      int fd = ::open(arc->fitsfile.c_str(), O_RDONLY);
      if( fd <= 0 ) {
	clog<<"UFFrameGrab> failed to open fits header template file "<<arc->fitsfile<<endl;
	exit(1);
      }
      int nb = ::read(fd, (char*)arc->fitsHdr.data(), 2880);
      if( nb <= 0 ) clog<<"UFFrameGrab> failed to read fits header template"<<endl;
      ::close(fd);
    }
    if( (arg = findArg("-sort")) != "false" ) {
      arc->slutfile = arg;
      clog<<"UFFrameGrab> ("<<mypid<<") sort LUT file = "<<arg<<endl;
      if( arg != "true" ) {// must be file-name
        int fd = ::open(arc->slutfile.c_str(), O_RDONLY);
        if( fd <= 0 ) {
	  clog<<"UFFrameGrab> failed to open sort LUT  file "<<arc->slutfile<<endl;
	  exit(1);
        }
        int elem = _currentConf->w * _currentConf->h;
        int *lut = new int[elem];
        arc->sortLUT = new UFInts("PixelSortLUT", (const int*) lut, elem);
        int nb = ::read(fd, (char*)arc->sortLUT->valData(), elem*sizeof(int));
        if( nb <= 0 ) clog<<"UFFrameGrab> failed to sort LUT file"<<endl;
	::close(fd);
	arc->sortLUT->rename(arc->slutfile); // and should set timestamp according to file date...
      }
    }

    // grab outfile option assumes this is not being run as a child thread of the Executive,
    // ringbuff object must be intitilized here for either UFBytes or UFInts
    int rings = createSharedResources();
    if( rings <= 0 )
      clog<<"UFFrameGrab> unable to create ring buff..."<<endl;
  }
  // create separate thread for signal handling:
  /*
  _signalThreadId = sigWaitThread(UFFrameGrab::_sigHandler);
  if( _signalThreadId == 0 ) {
    clog<<"UFFrameGrab> unable to create (posix) signal thread. errno="<<errno<<endl;
  }
  yield(); // let the signal thread start

  // create separate thread for archiving?
  // ...
  */
  // and finally enter the grab event loop;
  // grab 0 frames indicates forever (never return):
  // if arc != 0, saves to disk...
  int frmCnt = grabFrames(grabCnt, arc);
  if( frmCnt < grabCnt )
    clog<<"UFFrameGrab> missed some frames, got:" <<frmCnt<<", expected: "<<grabCnt<<endl;

  return p;
}

int UFFrameGrab::grabFrames(int numFrm, UFArchive* arc) {
  bool locked;
  int activeBufIdx = _theDMARingBuff->activeIndex(locked);
  clog<<"UFFrameGrab> active buff index= "<<activeBufIdx<<", locked= "<<locked<<endl;

#if !defined(_NO_EDT_)
  // init & start DMAs
  int numbufs= _theDMARingBuff->size();
  unsigned char** buffers = _theDMARingBuff->bufValPtrs();
  clog<<"UFFrameGrab> ("<<pthread_self()<<") start EDTDMA..."<<endl;
  int dma_cnt = UFEdtDMA::startDMA(numbufs, buffers, numFrm);
#else
  int dma_cnt= 0;
  _testVals = new unsigned char[frameSize(*_currentConf)];
  int intelem = frameSize(*_currentConf) / sizeof(int);
  for( int i = 0; i< intelem; i++ )
    ((unsigned int*)_testVals)[i] = i;
#endif
  int frmCnt= dma_cnt;
  int actIdx= 0;
  // enter dma buffer loop
  bool alive= true;
  clog<<"UFFrameGrab> ("<<pthread_self()<<") enter dma loop, edt dma done count= "<<dma_cnt
      <<", frmCnt= "<<frmCnt<<endl;
  do {
    ++frmCnt;
    // lock the active ring buffer to prevent other processes from reading, let 'em sleep on it
    UFProtocol* ufp = _theDMARingBuff->lockActiveBuf(actIdx);
    // double check the active buf. index and lock state:
    activeBufIdx = _theDMARingBuff->activeIndex(locked);
#if !defined(_NO_EDT_)
    clog<<"UFFrameGrab> ("<<pthread_self()<<") start pdv_wait_image: edt dma done count= "<<dma_cnt<<endl;
    unsigned char* bufptr = UFEdtDMA::waitImages(_waitCnt); // one at a time is all this can handle now!
    if( bufptr == 0 )
      clog<<"UFFrameGrab> pdv_wait_images failed?"<<endl;
    // after the above returns, the buffer has new values, set timestamp
    string t = UFRuntime::currentTime();
    dma_cnt = UFEdtDMA::doneCnt();
    strstream s;
    s << "EDTpdv DMA # " << dma_cnt <<", w:="<<_currentConf->w<<", h:="
      <<_currentConf->h<< ", d:="<<_currentConf->d<<ends;
    ufp->rename(s.str()); delete[] s.str();
    ufp->stampTime(t);
#if defined(sparc)
    int nint = UFEdtDMA::little2BigEnd(bufptr, (unsigned char*) ufp->valData(), _currentConf);
    if( nint <= 0 )
      clog<<"UFFrameGrab> ? endian conversion error?"<<endl;
#endif
#else  // put (NonEDT) test data into the buf
    string t = UFRuntime::currentTime();
    strstream s;
    s << "Sim DMA # " << dma_cnt <<", w:="<<_currentConf->w<<", h:="
      <<_currentConf->h<< ", d:="<<_currentConf->d<<ends;
    ++dma_cnt;
    // this is a cheap way to reset the shmem buff header:
    if( ufp->typeId() == UFProtocol::_Bytes_ ) {
      UFBytes bytes(s.str(), (const unsigned char*)_testVals, frameSize(*_currentConf));
      delete[] s.str();
      ufp->shallowCopy(bytes);
    }
    if( ufp->typeId() == UFProtocol::_Ints_ ) {
      UFInts ints(s.str(), (const unsigned char*)_testVals, frameSize(*_currentConf));
      delete[] s.str();
      ufp->shallowCopy(ints);
    }
#endif

    int ielems= 1;
    // assume byte array contains ints
    if( ufp->typeId() == UFProtocol::_Bytes_ )
      ielems = ufp->elements()/sizeof(int);

    int* ivals = (int*) ufp->valData();

    strstream logmsg;
    logmsg<<"test frame: "<<ivals[0]<<" "<<ivals[ielems/2]
	  <<" "<<ivals[ielems - 1]<<" "<<ufp->timestamp()<<" "<<ufp->name()<<ends;
    _statusLog(logmsg.str()); delete logmsg.str();

    _theDMARingBuff->nextActiveBuf(); // increment bufIdx and unlock
    activeBufIdx = _theDMARingBuff->activeIndex(locked); // check for proper icrement!
    
    clog<<"UFFrameGrab> done dma, new active buff index= "<<activeBufIdx<<", locked= "<<locked<<endl;
    if( arc )
      _saveToDisk(dma_cnt, arc);

    if( frmCnt != dma_cnt ) 
      clog<<"UFFrameGrab> edt dma done count= "<<dma_cnt<<" != frmCnt= "<<frmCnt<<endl;
#ifdef _NO_EDT_
    sleep(0.5);
#else
    //sleep(2);
    yield();
#endif
    if( frmCnt >= numFrm || dma_cnt >= numFrm )
      alive = false;

  } while( alive ); 

  clog << "UFFrameGrab> edt dma done count= "<<dma_cnt<<", frmCnt= "<<frmCnt<<endl;

#ifndef _NO_EDT_
  UFEdtDMA::closeDev();
#else
  delete[] _testVals;
#endif

  return frmCnt;
}

#endif // __UFFrameGrab_cc__

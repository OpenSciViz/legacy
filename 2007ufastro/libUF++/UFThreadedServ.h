#if !defined(__UFThreadedServ_h__)
#define __UFThreadedServ_h__ "$Name:  $ $Id: UFThreadedServ.h,v 0.1 2004/09/17 19:19:13 hon Exp $"
#define __UFThreadedServ_H__(arg) const char arg##ThreadedServ_h__rcsId[] = __UFThreadedServ_h__;

#include "UFDaemon.h"
#include "string"
#include "map"
#include "list"
#include "vector"

#include "pthread.h"

#include "UFSocket.h"
#include "UFServerSocket.h"

using std::string;
using std::map;
using std::list;
using std::vector;

/**
 * A multithreaded server framework.
 */
class UFThreadedServ : public UFDaemon
{
public:

  /**
   * Construct given a name.
   */
  UFThreadedServ(const string& name) ;

  /**
   * Construct given the command line arguments.
   */
  UFThreadedServ(int argc, char** argv) ;

  /**
   * Construct given the command line arguments and a name.
   */
  UFThreadedServ(const string& name, int argc, char** argv) ;

  /**
   * Shutdown the server.
   */
  virtual ~UFThreadedServ() ;

  /**
   * Return a description of this object.
   */  
  inline virtual string description() const
    { return __UFThreadedServ_h__; }

  /**
   * Send out a heartbeat.
   */
  virtual string heartbeat();

  /**
   * The main method of the server.
   */
  virtual void* exec(void* p=0);

  /**
   * Pure virtual handler method to be overloaded by clients of
   * this class.  This method is called each time a UFProtocol
   * object is read from a client connection.
   * TODO: Put an intermediate method in between this method
   * and clientThread that performs mutex locking.
   */
  virtual vector< UFProtocol* > handleObject( UFProtocol* ) = 0 ;

  /**
   * Request that a listener be added for client connections
   * on the given port.
   */
  virtual bool addListener( const unsigned short int& whichPort ) ;

  /**
   * Request that a listener be removed for the given port.
   */
  virtual bool removeListener( const unsigned short int& whichPort ) ;

  /**
   * Remove a client given it's thread ID.
   */
  virtual bool removeClient( const pthread_t& ) ;

  virtual int createSharedResources() { return 0 ; }
  virtual int deleteSharedResources() { return 0 ; }

 protected:

  /**
   * Perform initialization routines.
   */
  void init() ;

  /**
   * C style stub function for pthread_create() to run.
   */
  static void* listenThreadStub( void* ) ;

  /**
   * C style stub function for pthread_create() to run.
   */
  static void* clientThreadStub( void* ) ;

  /**
   * A structure containing information about a server thread.
   */
  struct threadInfo
  {
    UFSocket* sock ;
    unsigned short int whichPort ;
    bool keepRunning ;

    threadInfo( UFSocket* _sock,
		      const unsigned short int& _whichPort )
      : sock( _sock ), whichPort( _whichPort ), keepRunning( true )
    {}
    threadInfo( const threadInfo& rhs )
      : sock( rhs.sock ), whichPort( rhs.whichPort ),
      keepRunning( rhs.keepRunning )
    {}
  } ;

  /**
   * A small structure to pass into the stub functions to enable
   * callbacks into the current object.
   */
  struct bootStrap
  {
    UFThreadedServ* callback ;
    threadInfo* tInfo ;
    bootStrap( UFThreadedServ* _callback, threadInfo* _tInfo )
      : callback( _callback ), tInfo( _tInfo )
    {}
    bootStrap( const bootStrap& rhs )
      : callback( rhs.callback ), tInfo( rhs.tInfo )
    {}
  } ;

  /**
   * Each listener thread runs inside of this method.
   */
  virtual void listenThread( threadInfo* ) ;

  /**
   * Each client thread runs inside of this method.
   */
  virtual void clientThread( threadInfo* ) ;

  /**
   * Find a client's thread ID given its threadInfo.
   * This method is non-const because pthread_mutex_lock()
   * receives non-const pointer to pthread_mutex_t.
   */
  virtual pthread_t findClientID( const threadInfo* ) ;

  /**
   * Find a listener thread's ID given its threadInfo.
   * This method is non-const because pthread_mutex_lock()
   * receives non-const pointer to pthread_mutex_t.
   */
  virtual pthread_t findListenID( const threadInfo* ) ;

  /**
   * All threads must halt when this variable becomes false.
   * No threads may block.
   */
  volatile bool keepRunning ;

  /**
   * The type used to store information about client threads.
   */
  typedef std::map< pthread_t, threadInfo* > clientThreadListType ;

  /**
   * The structure used to store information about client threads.
   */
  clientThreadListType clientThreadList ;

  /**
   * The mutex used to synchronize access to the clientThreadList.
   */
  pthread_mutex_t clientThreadListMutex ;

  /**
   * The type used to store information about listener threads.
   */
  typedef std::map< pthread_t, threadInfo* > listenThreadListType ;

  /**
   * The structure used to store information about the listener
   * threads.
   */
  listenThreadListType listenThreadList ;

  /**
   * The mutex used to synchronize access to the listenThreadList.
   */
  pthread_mutex_t listenThreadListMutex ;

  /**
   * The type used to store information about threads that are no
   * longer currently running and need to be deallocated.
   */
  typedef list< pthread_t > deadThreadListType ;

  /**
   * The structure used to store information about threads that are
   * no longer currently running and need to be deallocated.
   */
  deadThreadListType deadThreadList ;

  /**
   * The mutex used to synchronize access to the deadThreadList.
   */
  pthread_mutex_t deadThreadListMutex ;

};

#endif // __UFThreadedServ_h__

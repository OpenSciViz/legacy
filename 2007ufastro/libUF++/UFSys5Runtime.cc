#if !defined(__UFSys5Runtime_cc__)
#define __UFSys5Runtime_cc__ "$Name:  $ $Id: UFSys5Runtime.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFSys5Runtime_cc__;

#include "UFSys5Runtime.h"
__UFSys5Runtime_H__(__UFSys5Runtime_cc);

#include "UFPosixRuntime.h"
__UFPosixRuntime_H__(__UFSys5Runtime_cc);

#include "iostream"

//#define  __PRAGMA_REDEFINE_EXTNAME
#include "dirent.h"
#include "sys/types.h"
#include "sys/stat.h"
#include "fcntl.h"
#include "signal.h"

#if defined(__GNU_LIBRARY__) && !defined(_SEM_SEMUN_UNDEFINED)
// union semun is defined by including <sys/sem.h> 
#else
// according to X/OPEN we have to define it ourselves
union semun {
  int val;                    // value for SETVAL 
  struct semid_ds *buf;       // buffer for IPC_STAT, IPC_SET
  unsigned short int *array;  // array for GETALL, SETALL
  struct seminfo *__buf;      // buffer for IPC_INFO
};
#endif

#ifndef MAXNAMLEN
#define MAXNAMLEN 255
#endif

// loosely based on  Stevens' Original Net Programming text pg 142-151
const struct sembuf _lock[2] = { { 0, 0, 0 }, // wait for sem to become 0
				 { 0, 1, SEM_UNDO } }; // then increment by 1
const struct sembuf _trylock[2] = {
		{ 0, 0, IPC_NOWAIT }, // don't wait for sem to become 0
		{ 0, 1, SEM_UNDO } }; // if 0, increment by 1 ?

const struct sembuf _unlock = { 0, -1, SEM_UNDO | IPC_NOWAIT }; // decrement by 1

static string _keyname(string& name) {
  if(  name.find_first_of( "/usr/tmp/") != 0 )
    name = "/usr/tmp/" + name;

  return name;
}

// protected:

// statics attributes:
std::map< string, UFSys5Runtime::sem5* > UFSys5Runtime::_keyTable;

// functions:
key_t UFSys5Runtime::_ftok(string& name){
  // POSIX semaphore format:
  // first char must be a '/'
  // there must be exactly one '/' in name
  /*
  if( 0 != name.find_first_of( "/" ) || (name.find_first_of( "/" ) != name.find_last_of( "/" )) ) {
    clog << "Invalid format"<<endl;
    return (key_t ) -1;
  }
  */
  key_t key = sem5key(name);
  if( key >= 0 )
    return key;

  name = _keyname(name);
  const char* nm = name.c_str();
  struct stat statbuf;
  int status = ::stat(nm, &statbuf);
  if( status < 0 )  // create it
    ::creat( nm, S_IRWXU | S_IRWXG | S_IRWXO );

  key_t k = ::ftok(nm, 0);
  if( k >= 0 ) {
    sem5* sm = new sem5;
    sm->key = k;
    _keyTable.insert( pair< string, UFSys5Runtime::sem5* > (name, sm) );
  }

  return k;
}

// public
int UFSys5Runtime::sem5id(const string& name) {
  int semid = -1;
  std::map< string, sem5* >::iterator i = _keyTable.find(name);
  if( i != _keyTable.end() )
    semid = i->second->semid; // _ketTable[name];

  return semid;
}

key_t UFSys5Runtime::sem5key(const string& name) {
  key_t k = -1;
  std::map< string, sem5* >::iterator i = _keyTable.find(name);
  if( i != _keyTable.end() )
    k = i->second->key; // _ketTable[name];

  return k;
}

UFSys5Runtime::sem5* UFSys5Runtime::find(const string& name) {
  sem5* s5 = 0;
  std::map< string, sem5* >::iterator i = _keyTable.find(name);
  if( i != _keyTable.end() )
    s5 = i->second; // _ketTable[name];

  return s5;
}

int UFSys5Runtime::sem5Attach(string& name) {
  key_t key = _ftok(name);
  sem5* sm = find(name);
  sigset_t prev_sigset;

  // enter critical section, mask all signals o prevent interrupts
  UFPosixRuntime::sigMask(&prev_sigset);

  int semid = sm->semid = semget( key, 1, 0 );
  if( semid < 0 ) {
    clog<<"UFSys5Runtime::sem5Attach> error attaching to: "<<name<<", errno= "<<errno<<endl;
    _keyTable.erase(name);
  }

  // must wait for the semaphore to be initialized?
  /*
  union semun arg ;
  struct semid_ds seminfo ;
  arg.buf = &seminfo ;

  for( int i = 0 ; i < 10 ; i++ ) {
    if( semctl( semid, 0, IPC_STAT, arg ) < 0 )
      return -1 ;
    if( arg.buf->sem_otime != 0 ) // it's initialized
      return semid ;

    sleep( 1 ) ;
  }
  */

  // exti critical section
  UFPosixRuntime::sigUnMask(&prev_sigset);
  return semid;
}

int UFSys5Runtime::sem5Create(string& name) {
  key_t key = _ftok( name ) ;
  if( key < 0 )
    return key ;

  sem5* sm = find(name);

  // prevent signals from being delivered
  sigset_t prev_sigset;
  UFPosixRuntime::sigMask(&prev_sigset);
  // attempt to create the semaphore set (with just one sem)
  int semid = sm->semid = semget( key, 1, IPC_CREAT | S_IRWXU | S_IRWXG | S_IRWXO );

  // Did we create the semaphore?
  if( semid < 0 ) {
    _keyTable.erase(name);
    UFPosixRuntime::sigUnMask(&prev_sigset);
    return semid;
  }

  // initialize
  union semun arg;
  arg.val = 0;
  int ret = semctl(sm->semid, 0, SETVAL, arg) ;
  if( ret < 0 ) {
    // semctl() failed for some reason
    // remove the newly created semaphore
    // and return the error
    sem5Delete(name);
    UFPosixRuntime::sigUnMask(&prev_sigset);
    semid = -1; ;
  }
  else {
    // all is well, return the index to the new semaphore
    UFPosixRuntime::sigUnMask(&prev_sigset);
  }

  return semid ;
}


int UFSys5Runtime::sem5Value(const string& name) {
  int semid = sem5id(name);
  if( semid < 0 ) 
    return semid;

  union semun arg;
  arg.val = 0;
  semid_ds seminfo;
  arg.buf = &seminfo;

  // Get this semaphore's value
  int semval = semctl(semid, 0, GETVAL, arg);
  if( semval < 0 )
    clog<<"UFSys5Runtime::sem5Value> unable to get value of sem5: "<<name<<endl;

  return  semval;
}

int UFSys5Runtime::sem5Release(const string& name){
  // find the semaphore
  int semid = sem5id(name);
  if( semid < 0 ) 
    return semid;

  int stat = semop(semid, (sembuf *)&_unlock, 1);

  return stat;
}

int UFSys5Runtime::sem5TakeNoWait(const string& name) {
  // find the semaphore
  int semid = sem5id(name);
  if( semid < 0 ) 
    return semid;

  return semop(semid, (sembuf *)&_trylock, 2) ;
}

int UFSys5Runtime::sem5Take(const string& name) {
  // find the semaphore
  int semid = sem5id(name);
  if( semid < 0 ) 
    return semid;

  return semop(semid, (sembuf *)&_lock, 2);
}

int UFSys5Runtime::sem5Delete(const string& name) {
  // find the semaphore
  int semid = sem5id(name);
  if( semid < 0 ) 
    return semid;

  sigset_t prev_sigset;
  UFPosixRuntime::sigMask(&prev_sigset);

  int stat = semctl( semid, 0, IPC_RMID );
  if( stat < 0 )
    clog<<"UFSys5Runtime::sem5Delete> error deleting: "<<name<<", errno= "<<errno<<endl;
  else
    _keyTable.erase(name);

  UFPosixRuntime::sigUnMask(&prev_sigset);
  return stat;
}

#endif // __UFSys5Runtime_cc__

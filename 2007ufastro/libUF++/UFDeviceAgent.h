#if !defined(__UFDeviceAgent_h__)
#define __UFDeviceAgent_h__ "$Name:  $ $Id: UFDeviceAgent.h,v 0.11 2006/02/10 22:05:18 hon Exp $"
#define __UFDeviceAgent_H__(arg) const char arg##UFDeviceAgent_h__rcsId[] = __UFDeviceAgent_h__;

#include "string"
#include "vector"
#include "deque"
#include "map"
#include "iostream"

#include "UFRndRobinServ.h"
#include "UFStrings.h"
#include "UFObsConfig.h"
#include "UFFITSheader.h"
#include "UFDeviceConfig.h"

// abstract base class for all UF device server/agents
// transactions are UFStrings
class UFDeviceAgent: public UFRndRobinServ {
public:
  enum { TReCS= 0, Flamingos1= 1, Flamingos2= 2, CanariCam= 3}; // supported instruments
  inline virtual int instrumentId() { return UFDeviceAgent::TReCS; }
 
  // keep history of commands processed, and there final dispositions
  struct CmdInfo {
    string clientinfo; // "client@connect_time~hostname~hostIP"
    string time_received; // "yyyy:ddd:hh:mm:ss:uuuuuu" i.e. standard UFtime string
    string time_submitted; // identical or very nearly to cmd_time
    string time_completed; // i.e. time of device reply
    string status_cmd; // "rejected", "active", "succeeded", "failed"
    string status_config; // "idle-UnConfigured", "idle-DefaultConfig", "idle-<ConfigName>", "busy-<ConfigName>"
    string cmd_time; // (nearly)current or past times indicate immediate actions; future indicates queued actions
    // allow ctor to parse multi-element UFStrings into multi-cmd "bundle"
    // format should be "raw" or enumerated: "CmdName@Device", e.g. "sim@mce4"
    vector<string> cmd_name; 
    // params for raw cmds should be complete cmd, e.g. "sim 3 128 128 1000" for raw invocation of "sim@mce4"
    // params for enumerations like "sim@mce4" would look like "3 128 128 1000"
    vector<string> cmd_params; 
    // all command implementations must be terminated with new-line.
    vector<string> cmd_impl; // parsed(cmd_name) + parsed(cmd_params) + \n"
    vector<string> cmd_reply; // device erply concatinated with optinal supplemental info.
    // allocate UFStrings object and set with CmdInfo
    UFStrings* ufStrings(const string& name);
    // allocate UFStrings and initialize with CmdInfo + vs
    UFStrings* ufStrings(const string& name, const vector<string>& vs);
    CmdInfo(); // default c-tor should be null/no-op command, or default configure?
    CmdInfo(const UFStrings& ufs, const UFDeviceConfig* devconf= 0); // 
    // convenience func. to assemble deque< string > from CmdInfo -- cmd_name & cmd_params
    int cmdDeque(deque< string >& cmds);

  protected:
    string _remWhites(const string& s);
  };

  // key= "command time", val= CmdInfo*
  typedef map< string, UFDeviceAgent::CmdInfo* > CmdList;

  static int main(int argc, char** argv, char** envp= 0);
  UFDeviceAgent(int argc, char** argv, char** envp= 0);
  UFDeviceAgent(const string& name, int argc, char** argv, char** envp= 0);
  inline virtual ~UFDeviceAgent() {}

  // Note: the virtual funcs: init, action() & query() are the key virtuals 
  // that any subclass of UFDeviceAgent should override.
  // clients may submit a series/sequence of command transactions

  // this should establish some sort of UFDeviceConfig object that holds
  // command tables & config names, and if the device relies on a terminal
  // server like the annex or iocomm, connect to it...
  virtual UFTermServ* init(const string& host= "", int port= 0);

  // perform device action, set cmd/dev reply if any, append _completed list, return a reply
  virtual UFProtocol* action(UFDeviceAgent::CmdInfo* a);
  // return multiple (heterogeneous) replies
  virtual int action(UFDeviceAgent::CmdInfo* a, vector< UFProtocol* >*& replies);

  // create the basic FITS header from agent attributes
  virtual UFFITSheader* basicFITSheader();
  // create the basic FITS header from agent and obs attributes
  virtual UFFITSheader* basicFITSheader(UFObsConfig* oc);

  // some actions my be queries that may or may not require device access:
  // this can be optionally called from action:
  virtual int query(const string& q, vector<string>& reply);

  // override this/these UFRndRobinServ virtuals:
  virtual void startup();
  virtual int options(string& servlist);

  // future cmds are queued, all others immediately perform action or query
  virtual int servImmed();

  // check if it's time to deal with a queued (action) cmd, if so remove it
  // from the queued list and call action.
  virtual int servQueued();

  // hibernate can be overrided to perform sleep for different durations
  // depending on queued contents.
  //virtual void hibernate();

  // need to put this here for lack of anywhere else;
  // this is a kludge to help simplify linkage issues
  // and obviate the need to re-organize some of the device
  // class hiearchy:
  inline virtual int getValue(const string& name, string& val) { val = name;  return 0; }
  inline virtual int getValue(const string& name, int& val) { val = 0;  return 0; }
  inline virtual int getValue(const string& name, double& val) { val = 0.0;  return 0; }

  static bool _verbose, _sim;

protected:
  UFDeviceAgent::CmdInfo* _active; // or latest action command to be executed
  UFDeviceAgent::CmdList _completed; // full history
  UFDeviceConfig* _config;
  float _flush;
  deque < string > _DevHistory;
  const size_t _DevHistSz; // max.
  inline virtual void _setDefaults(const string& instrum= "") { _sim = false; }
};

#endif // __UFDeviceAgent_h__


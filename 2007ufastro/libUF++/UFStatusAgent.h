#if !defined(__UFStatusAgent_h__)
#define __UFStatusAgent_h__ "$Name:  $ $Id: UFStatusAgent.h,v 0.1 2004/09/17 19:19:12 hon Exp $"
#define __UFStatusAgent_H__(arg) const char arg##StatusAgent_h__rcsId[] = __UFStatusAgent_h__;

#include "UFDaemon.h"
#include "UFRndRobinServ.h"
#include "string"

class UFStatusAgent : public UFDaemon {
public:
  inline UFStatusAgent(const string& name, char** envp= 0) : UFDaemon(name, envp) {}
  inline UFStatusAgent(int argc, char** argv, char** envp= 0) : UFDaemon(argc, argv, envp) {}
  inline UFStatusAgent(const string& name, int argc, char** argv, char** envp= 0) : UFDaemon(name, argc, argv, envp) {}
  inline UFStatusAgent(const string& name, UFRuntime::Argv* args, char** envp= 0) : UFDaemon(name, args, envp) {}
  inline virtual ~UFStatusAgent() {}

  inline virtual string description() const { return __UFStatusAgent_h__; }

  virtual string heartbeat();

  // this is the key virtual function that must be overriden
  virtual void* exec(void* p= 0);

  // stubs for now, eventually have these manage queued commands:  
  inline virtual int createSharedResources() { return 0; }
  inline virtual int deleteSharedResources() { return 0; }
};

#endif // __UFStatusAgent_h__

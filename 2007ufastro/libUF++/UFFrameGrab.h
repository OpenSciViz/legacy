#if !defined(__UFFrameGrab_h__)
#define __UFFrameGrab_h__ "$Id: UFFrameGrab.h,v 0.2 2004/09/21 19:45:32 hon Exp $";
#define __UFFrameGrab_H__(arg) const char arg##FrameGrab_h__rcsId[] = __UFFrameGrab_h__;

#include "UFDaemon.h"
#include "UFEdtDMA.h"
#include "UFHeapRingBuff.h"
//#include "UFShMemRingBuff.h"


class UFFrameGrab : public UFDaemon {
public:
  static UFRingBuff* _theDMARingBuff;

  // the frame grabber uses the edt libraires which force it to be threaded:
  inline UFFrameGrab(const string& name, char** envp= 0) : UFDaemon(name, envp), _configs(0) {
    UFPosixRuntime::_threaded = true;
  }
  inline UFFrameGrab(int argc, char** argv, char** envp= 0) : UFDaemon(argc, argv, envp), _configs(0) {
    UFPosixRuntime::_threaded = true;
  }
  inline UFFrameGrab(const string& name, int argc, char** argv, char** envp= 0) : UFDaemon(name, argc, argv, envp), _configs(0) {
    UFPosixRuntime::_threaded = true;
  }
  inline UFFrameGrab(const string& name, UFRuntime::Argv* args, char** envp= 0) : UFDaemon(name, args, envp), _configs(0) {
    UFPosixRuntime::_threaded = true;
  }

  inline virtual string description() const { return __UFFrameGrab_h__; }

  //inline static int main(int argc, char** argv) { UFFrameGrab proc(argc, argv); proc.exec(); return 0;}

  // total bytes in image frame:
  inline static int frameSize(const UFEdtDMA::Conf& c) { return c.w * c.h * c.d / 8; }

  // if instrument has more than one imaging array of different dimensions:
  // search the configvec for max bufsize
  inline static int maxFrameSize(const UFEdtDMA::ConfVec& cv) {
    if( cv.size() == 0 ) return 0;
    int tmp= 0, max= frameSize(*(cv[0]));
    for( int i = 1; i < (int)cv.size(); i++ ) {
      tmp = frameSize(*(cv[i]));
      if( tmp > max ) max = tmp;
    }
    return max;
  }
    
  // if instrument has more than one imaging array of different dimensions:
  // search the configvec for min bufsize
  inline static int minFrameSize(const UFEdtDMA::ConfVec& cv) {
    if( cv.size() == 0 ) return 0;
    int tmp= 0, min= frameSize(*(cv[0]));
    for( int i = 1; i < (int)cv.size(); i++ ) {
      tmp = frameSize(*(cv[i]));
      if( tmp < min ) min = tmp;
    }
    return min;
  }

  // return all known/possible configs for system (each instrument will differ)
  // this virtual can/should be overriden by instrument sub-class.
  // default behavior is to simply provide 1 (the current) config:
  inline virtual int allConfs(UFEdtDMA::ConfVec& cv) {
    cv.clear();
    cv.push_back(UFEdtDMA::getConf());

    return cv.size();
  }

  // this can be overriden 
  inline virtual int maxFrameSize() {
    UFEdtDMA::ConfVec cv;
    int nc = allConfs(cv);
    if( nc > 0 )
      return maxFrameSize(cv);
    else 
      return nc;
  }

  // delegated here for theDMARingBuff
  inline virtual int createSharedResources() {
    // this is the RingBuff for use by the EDTpdv device driver
    vector< string > names;
    int bufsize = maxFrameSize();
    int type;
    if( _currentConf->d / 8 == 1 ) type = UFProtocol::_Bytes_;
    if( _currentConf->d / 16 == 1 ) type = UFProtocol::_Shorts_;
    if( _currentConf->d / 32 == 1 ) type = UFProtocol::_Ints_;
    _theDMARingBuff = new UFHeapRingBuff("EdtDMA");
    _theDMARingBuff->init(names, bufsize, type);

    return 1; // created one shared resource (ringbuff)
  }

  inline virtual int deleteSharedResources() { delete _theDMARingBuff; return 0; }

  // override this UFDaemon virtual: 
  virtual void* exec(void* p= 0);

  // to make use of this (for running without the pre-processor):
  // numFrm == 0 implies grab forever
  // if pg->sortbuf is not null copy DMA buff to it
  // if arc.sortLUT is provided, apply the sort LUT to the copy
  virtual int grabFrames(int numFrm= 0, UFArchive* arc= 0); 

protected:
  UFEdtDMA::ConfVec* _configs;
  static UFEdtDMA::Conf* _currentConf;
  static UFEdtDMA::DevPtr _edtp; // only one per daemon!
  static int _waitCnt; // arguement to pdv_wait_image 

  // local log
  static FILE* _statusLog(const char* logmsg);

  // for comparison against reference frame:
  static int UFFrameGrab::_refCmp(unsigned char* buff, int elem, bool newRef= false);

  // static functions for archiving thread, optionally prepending FITS Header:
  static void _saveToDisk(int cnt, UFArchive* arc);
};

// try this for pthread_create?
//void* (UFFrameGrab::*FrameGrabExecPtr)(void *);

#endif // __UFFrameGrab_h__

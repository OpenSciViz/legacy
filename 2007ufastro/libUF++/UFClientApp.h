#if !defined(__UFClientApp_h__)
#define __UFClientApp_h__ "$Name:  $ $Id: UFClientApp.h,v 0.3 2005/06/09 19:27:51 hon Exp $";
#define __UFClientApp_H__(arg) const char arg##UFClientApp_h__rcsId[] = __UFClientApp_h__;

#if defined(sun) || defined(solaris)|| defined(Solaris) || defined(SOLARIS)
#include "sys/uio.h"
#endif
#include "termio.h"
#include "unistd.h"

// c++
using namespace std;
#include "string"
#include "vector"

#include "UFDaemon.h"
#include "UFClientSocket.h"
#include "UFStrings.h"

// although technically not a daemon, a client app. can make use 
// of the daemon runtime funcs:
class UFClientApp : public UFDaemon, public UFClientSocket {
public:
  UFClientApp(const string& name, int argc, char** argv, char** envp, int port= -1);
  virtual ~UFClientApp();

  static struct termio setRawIO(int fd= 0); // default is stdin, return orig termio;
  static int restoreIO(struct termio& orig, int fd= 0);

  // parse mutil-cmd line assuming format "cmd1 & cmd2 & cmd3...
  // inserting into string vector"
  static int parse(string& req, vector < string >& cmds);

  // each subclass should use its own version of
  // a static main to define accepable user input:
  static int main(int argc, char** argv, char** envp);

  // return 0 on connection failure:
  virtual FILE* init(const string& host, int port);

  // submit string (only), return immediately without fetching reply
  // flush output and sleep flush sec.
  virtual int submit(const string& key, const string& val, float flush= -1.0); 

  // submit string, recv reply and return reply as string
  // flush output and sleep flush sec.
  virtual int submit(const string& key, const string& val,
		     UFStrings*& reply, float flush= -1.0);

  // submit bundle of string vals (all of same key type),
  // flush output and sleep flush sec. before returning
  virtual int submit(const string& key, const vector< string >& val, float flush= -1.0);
  // submit bundle of string vals (all of same key type), recv reply and 
  // return reply as ufstrings
  virtual int submit(const string& key, const vector< string >& val,
		     UFStrings*& reply, float flush= -1.0);

  // fetch next request from stdin, or re-use historical request:
  virtual string getLine(const string& prompt, vector< string > history);

  inline virtual string description() const { return __UFClientApp_h__; }

protected:
  FILE* _fs; // for flushing ... this should really go into UFSocket someday

  static bool _quiet;
  static struct termio _orig;
};


#endif // __UFClientApp_h__

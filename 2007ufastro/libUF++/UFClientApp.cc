#if !defined(__UFClientApp_cc__)
#define __UFClientApp_cc__ "$Name:  $ $Id: UFClientApp.cc 14 2008-06-11 01:49:45Z hon $";

#include "UFClientApp.h"

bool UFClientApp::_quiet = false;
struct termio UFClientApp::_orig;

UFClientApp::UFClientApp(const string& name,
			 int argc, char** argv, char** envp, 
			 int port) : UFDaemon(name, argc, argv, envp),
				         UFClientSocket(port), _fs(0) {
  rename("UFClientApp@" + hostname());
}

UFClientApp::~UFClientApp() {
  if( _fs ) {
    close(); 
    fclose(_fs);
    _fs = 0;
  }
}

struct termio UFClientApp::setRawIO(int fd) {
  struct termio orig, raw;
  memset(&orig, 0, sizeof(orig));
  raw = orig;
#if defined(LINUX) || defined(SOLARIS)
  ::ioctl(fd, TCGETA , &orig);
  ::ioctl(fd, TCGETA, &raw); 
  raw.c_lflag &= ~ECHO;
  raw.c_lflag &= ~ICANON;
  raw.c_cc[VMIN] = 1; // present each character as soon as it shows up
  raw.c_cc[VTIME] = 1; // 0.1 second for timeout
  ::ioctl(fd, TCSETAF, &raw);
#endif
  return orig;
}

int UFClientApp::restoreIO(struct termio& orig, int fd) {
#if defined(LINUX) || defined(SOLARIS)
  return ::ioctl(fd, TCSETAF, &orig);
#else
  return -1;
#endif
}

int UFClientApp::parse(string& line, vector< string >& cmds) {
  // parse '&' separated elements into indivicual commands
  // eliminte any white spaces at the start of line:
  char* lstrt = (char*)line.c_str();
  while( *lstrt == ' ' || *lstrt == '\t' ) ++lstrt;
  line = lstrt; 
  cmds.clear();
  if( !_quiet )
    clog<<"UFClientApp::parse> line: "<<line<<endl;

  int len= 0, pos= 0, posA= -1; // ++posA starts at 0
  do {
    pos = ++posA;
    posA = line.find("&", pos);
    if( posA != (int)string::npos ) {
      len = posA - pos;
    }
    else {
      len = line.size() - pos;
    }
    string s = line.substr(pos, len);
    // eliminate any leading spaces, insure first char is motor/indexor name:
    char *cs = (char*)s.c_str();
    while( *cs == ' ' || *cs == '\t' )
      ++cs;

    s = cs;
    cmds.push_back(s);
  } while( posA != (int)string::npos );

  return cmds.size();
}

int UFClientApp::main(int argc, char** argv, char** envp) {
  UFClientApp cap("UFClientApp", argc, argv, envp);
  string arg, host(hostname());
  string raw = "true"; // default is raw command mode
  string status = ""; 
  int port= 52003;
  float flush= -1.0;

  arg = cap.findArg("-flush");
  if( arg != "false" )
    flush = atof(arg.c_str());

  arg = cap.findArg("-raw");
  if( arg != "false" && arg != "true" ) // explicit cmd string 
    raw = arg;

  arg = cap.findArg("-host");
  if( arg != "false" )
    host = arg;

  arg = cap.findArg("-port");
  if( arg != "false" && arg != "false" )
    port = atoi(arg.c_str());

  arg = cap.findArg("-q");
  if( arg != "false" ) {
    _quiet = true;
    if( arg != "true" ) 
      raw = arg;
  }

  arg = cap.findArg("-stat");
  if( arg != "false" ) { // full or fits
    _quiet = true;
    raw = "false";
    if( arg != "true" ) 
      status = arg;
    else
      status = "All";
      //status = "FITS"; // only two allowed values 
  }

  arg = cap.findArg("-status");
  if( arg != "false" ) { // full or fits
    _quiet = true;
    raw = "false";
    if( arg != "true" ) 
      status = arg;
    else
      status = "All";
      //status = "FITS"; // only two allowed values 
  }

  if( port <= 0 || host.empty() ) {
    clog<<"UFClientApp> usage: 'UFClientApp -flush sec. -host host -port port -q raw-command'"<<endl;
    return 0;
  }

  FILE* f  = cap.init(host, port);
  if( f == 0 ) {
    clog<<"UFClientApp> unable to connect to LakeShore command server/agent..."<<endl;
    return -1;
  }

  UFStrings* reply_p;;
  if( raw != "true" && raw != "false" ) {
    // raw (explicit) command should be executed once
    int ns = cap.submit("raw", raw, reply_p, flush);
    if( ns <= 0 )
      clog << "UFClientApp> failed to submit raw= "<<raw<<endl;

    UFStrings& reply = *reply_p;
    //cout<<reply.timeStamp()<<reply.name()<<endl;
    for( int i=0; i < reply.elements(); ++i )
      cout<<reply[i]<<endl;

    delete reply_p; reply_p = 0;
    return 0;
  }

  if( status.length() > 0 ) {
    // raw (explicit) command should be executed once
    int ns = cap.submit("status", status, reply_p, flush);
    if( ns <= 0 )
      clog << "UFClientApp> failed to submit raw= "<<raw<<endl;

    UFStrings& reply = *reply_p;
    //cout<<reply.timeStamp()<<reply.name()<<endl;
    for( int i=0; i < reply.elements(); ++i )
      cout<<reply[i]<<endl;

    delete reply_p; reply_p = 0;
    return 0;
  }

  // enter command loop
  // first reset keyboard input behavior
  _orig = cap.setRawIO();
  string line, prompt = "UFClientApp> ";
  vector< string > history;
  while( true ) {
    line = cap.getLine(prompt, history);
    
    if( line == "exit" || line == "quit" || line == "q" )
      break;

    if( line.rfind("hist") != string::npos ||
	line.rfind("Hist") != string::npos ||
	line.rfind("HIST") != string::npos ) { // print history
      for( int i = 0; i < (int)history.size(); ++i )
	clog<<i<<": "<<history[i]<<endl;
      continue;
    }
    else {
      history.push_back(line);
    }

    int ns= 0;
    size_t spos = line.rfind("stat");
    if( spos == string::npos )
      spos = line.rfind("Stat");
    if( spos == string::npos )
      spos = line.rfind("STAT");
    if( spos != string::npos ) {
      status = line.substr(1+line.find(" ", spos));
      if( status.length() > 0 )
        ns = cap.submit("status", status, reply_p, flush);
    }
    else {
      raw = line;
      ns = cap.submit("raw", raw, reply_p, flush);
      if( ns <= 0 )
        clog << "UFClientApp> failed to submit raw= "<<raw<<endl;
    }
    if( ns > 0 ) {
      UFStrings& reply = *reply_p;
      cout<<"\n"<<reply.timeStamp()<<reply.name()<<endl;
      for( int i=0; i < reply.elements(); ++i )
        cout<<reply[i]<<endl;
      delete reply_p; reply_p = 0;
    }
  } // end while

  cap.restoreIO(_orig);
  return 0; 
}

string UFClientApp::getLine(const string& prompt, vector< string > history) {
 clog<<prompt<<ends;
  setRawIO();
  string line= "";
  int delcnt= 0;
  int c = ::getc(stdin);
  if( c != 27 ) {
    ::ungetc(c, stdin); ::putc(c, stderr);
    restoreIO(_orig);
    getline(cin, line); // mixing stdin & iostream funcs. may not work
    return line;
  } // up arrow?
  else {
    c = ::getc(stdin);
    if( c != 91 ) {
      ::ungetc(c, stdin); ::putc(c, stderr);
      restoreIO(_orig);
      getline(cin, line); // mixing stdin & iostream funcs. may not work
      return line;
    } // up arrow?
    else {
      c = ::getc(stdin);
      char cc = (char)c;
      if( cc != 51 && cc != 'A' && cc != 'B' && cc != 'C' && cc != 'D') {
	 // 51del, 65up, 66down, 67right, 68left
        ::ungetc(c, stdin); ::putc(c, stderr);
        restoreIO(_orig);
        getline(cin, line); // mixing stdin & iostream funcs. may not work
        return line;
      }
      if( cc == 51 ) {
        c = ::getc(stdin);
	if( c == 126 )
	  delcnt++;
      }
    }
  }
  // up arrow, count number of up arrows
  int cnt = 1;
  int hs = (int) history.size();
  int hi = hs - cnt;
  if( hi < 0 ) 
    hi = 0;
  else if( hi >= hs) 
    hi = hs - 1;

  string hcmd = history[hi];
  hcmd = hcmd.substr(0, hcmd.length()-delcnt);
  clog<<hcmd<<ends;

  do {
    delcnt = 0;
     c = getc(stdin);
     if( c == 27 ) {
       c = getc(stdin);
       if( c == 91 )
	 c = getc(stdin);
         if( c == 'A' )
	   ++cnt;
	 else if( c == 'B' )
	   --cnt;
	 else
	   delcnt++;
     }
     hi = hs - cnt;
     if( hi < 0 ) 
       hi = 0;
     else if( hi >= hs ) 
       hi = hs - 1;


     hcmd = history[hi];
     hcmd = hcmd.substr(0, hcmd.length()-delcnt);

     // blank the line first:
     clog<<"\r                                                                                "<<ends;
     clog<<"\rUFClientApp> "<<hcmd<<ends;
  } while( c != '\n' );

  line = history[hi];
  return line;
}   

// public static func:
// return < 0 on connection failure:
FILE* UFClientApp::init(const string& host, int port) {
  int fd = connect(host, port);
  if( fd <= 0 ) {
    return 0;
  }
  _fs = fdopen(fd, "w");
  if( !_quiet )
    clog<<"UFClientApp> connected to: "<<host<<" on port: "<<port<<endl;

  // after accetping connection, server/agent will expect client to send
  // a UFProtocol object identifying itself, and echos it back (slightly
  // modified) 
  UFTimeStamp greet(name());
  int ns = send(greet);
  ns = recv(greet);
  if( !_quiet )
    clog<<"UFClientApp> greeting: "<< greet.name() << " " << greet.timeStamp() <<endl;

  return _fs;
}

// submit string (only), return immediately without fetching reply
int UFClientApp::submit(const string& k, const string& s, float flush) {
  vector<string> cmd;
  cmd.push_back(k);
  cmd.push_back(s);
  UFStrings ufs(name(), cmd);
  int ns = send(ufs);
  // try flushing the socket output stream
  // this is the only way i can think to do it:
  int stat = fflush(_fs);
  if( stat != 0 ) clog<<"UFClientApp::submit> socket flush failed? ns= "<<ns<<endl;

  if( flush > 0.0 ) // optionally sleep after the flush
    UFPosixRuntime::sleep(flush);

  return ns;
}

// submit string, recv reply
int UFClientApp::submit(const string& k, const string& s, UFStrings*& r, float flush) {
  int ns = submit(k, s, flush);
  if( ns <= 0 )
    return ns;

  r = dynamic_cast<UFStrings*> (UFProtocol::createFrom(*this));
  if( r == 0 )
    return 0;
  return r->elements();
}

// submit strings (only), return immediately without fetching reply
int UFClientApp::submit(const string& cmdtyp, const vector< string >& vs, float flush) {
  vector<string> cmds;
  for( int i = 0; i < (int)vs.size(); ++i ) {
    cmds.push_back(cmdtyp);
    cmds.push_back(vs[i]);
  }
  UFStrings ufs(name(), cmds);
  int ns = send(ufs);
  // try flushing the socket output stream
  // this is the only way i can think to do it:
  int stat = fflush(_fs);
  if( stat != 0 ) clog<<"ufmotor::submit> socket flush failed? ns= "<<ns<<endl;

  if( flush > 0.0 ) // optionally sleep after the flush
    UFPosixRuntime::sleep(flush);

  return ns;
}

// submit strings, recv reply
int UFClientApp::submit(const string& cmdtyp, const vector< string >& vs, UFStrings*& r, float flush) {
  int ns = submit(cmdtyp, vs, flush);
  if( ns <= 0 )
    return 0;

  r = dynamic_cast<UFStrings*> (UFProtocol::createFrom(*this));
  if( r == 0 )
    return 0;

  return r->elements();
}


#endif // __UFClientApp_cc__

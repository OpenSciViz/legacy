#if !defined(__UFAcqCtrl_h__)
#define __UFAcqCtrl_h__ "$Name:  $ $Id: UFAcqCtrl.h,v 0.5 2006/04/27 17:34:50 hon Exp $";
#define __UFAcqCtrl_H__(arg) const char arg##UFAcqCtrl_h__rcsId[] = __UFAcqCtrl_h__;

// c++
using namespace std;
#include "string"
#include "deque"
#include "map"
#include "multimap.h"

// uflib
#include "UFDeviceConfig.h"
#include "UFDeviceAgent.h"
#include "UFEdtDMA.h"
#include "UFClientSocket.h"
#include "UFStrings.h"
#include "UFFlamObsConf.h"
#include "UFFITSClient.h"
#include "UFFITSheader.h"
//
// this support a 'control & status' client to the ufedtd, an epics FITS reader/monitor, and an optional agent FITS client
// detector agent should use this to connect to edtd and commanf it (on behalf of sequencer) to acquire frames.
// dhs daemon should use this to fetch fits header values from epics db or agents, and connect to edtd as
// replicant client and receive obsconfs and frames. 
// edtd should use this to construct flam2obsconf from detector agent command bundle and send obsconf to dhs client, etc.
//
class UFAcqCtrl : public UFClientSocket {
  // for use by any daemon that wishes to command the ufedtd
  // used by the F2 detector agent to coordinate frame output
  // by mce4 with frame capture/acquitision by ufedtd
  // additional support for FITS headers (primary and extesion)
  // via static funcs. that use either epics sad or agent fits req.
  // it would be most efficient to use the fits sutff in the edtdma post acq.
  // function or better yet in the dhs client observation thread...
public:
  struct Observe {
    // support obsetup command
    string _exptime, _expcnt, _dhslabel, _datamode, _dhsqlook, _nfshost, _datadir, _fitsfile, _lutfile, _index,
          _usrinfo, _comment, _ds9, _pngjpegfile; // archive
    UFFlamObsConf *_obscfg;
    inline string name() { if( _obscfg == 0 ) return ""; return _obscfg->name(); }
    // helper to formulate cmd bundle to edtd or status from it
    int asStrings(deque< string >& text, const string& directive= "AcqStart");
    // ctor from UFDeviceAgent::CmdInfo (which is formulated from UFStrings transaction)
    Observe(UFDeviceAgent::CmdInfo* cmds);
#if defined(sparc) // bigendian
    Observe(float exptime= 1.0, int expcnt= 1, const string& dhslabel= "Test", 
	    const string& datamode= "SaveAll", const string& dhsqlook= "All",
	    const string& usrinfo= "RichardElston", const string& comment= "RichardElston",
	    const string& fits= "RichardElston", const string& lut= "UFFlamingos.lutBE", const string& fileIdx= "0000",
	    const string& datadir= "/data/flam2/", const string& nfshost= "localhost",
	    const string& ds9= "0", const string& pngjpeg= "");
#else // assume intel littleendian
    Observe(float exptime= 1.0, int expcnt= 1, const string& dhslabel= "Test", 
	    const string& datamode= "SaveAll", const string& dhsqlook= "All",
	    const string& usrinfo= "RichardElston", const string& comment= "RichardElston",
	    const string& fits= "RichardElston", const string& lut= "UFFlamingos.lutLE", const string& fileIdx= "0000",
	    const string& datadir= "/data/flam2/", const string& nfshost= "localhost",
	    const string& ds9= "0", const string& pngjpeg= "");
#endif
    inline ~Observe() {}
  };

  static float _timeout;
  static bool _sim, _verbose, _observatory; // simulation may include observatory info?
  static UFAcqCtrl::Observe* _obssetup; // current observation setup, if exposing (not idle)
  static map< string, UFAcqCtrl::Observe* > _Observations; // history of observations?

  UFAcqCtrl(const string& agenthost, const string& framehost);
  UFAcqCtrl(const string& agenthost, const string& framehost, map< string, int>& portmap);
  inline virtual ~UFAcqCtrl() {}

  UFAcqCtrl::Observe* obsSetup(UFDeviceAgent::CmdInfo* act);
  // additional (optional?) attributes of the aquisition (ACQ) directive
  UFAcqCtrl::Observe* UFAcqCtrl::attributes(UFDeviceAgent::CmdInfo* act);

  int defaultPorts(map< string, int>& portmap);
  string submitEdtd(const string& directive= "AcqStart"); // submit acq directive(s) vi UFStrings to edtd
  static string submitDhsc(Observe* obssetup, UFSocket* replicant); // send flam2obsconf to dhsclient (replicant client)
  int connectFITS(string& agenthost, const string& exclude= "");
  int connectEdtd(const string& clientname, string& frmhost, int frmport= 52001, bool block= false);
  UFInts* simAcq(const string& name, int index= 0);

  // these should return the datalabel and local archive file name:
  // start observation (connect to ufedtd if necessary), and set FITS
  // prmary and extension headers
  string startObs(UFDeviceConfig* dc); // mce4d sends acq action to edt then starts mce4
  // for unit tests and for edtd send of obssetup to dhs (replican) client
  static string startObs(UFSocket* soc, float exptime= 1.0, int expcnt= 1, const string& dhslabel= "Test",
			 UFAcqCtrl::Observe*& obssetup= 0, UFStrings*& obsmsg= 0); 
  static string startObs(UFSocket* soc, float exptime= 1.0, int expcnt= 1, const string& dhslabel= "Test",
			 UFFlamObsConf*& obsconf= 0);
  // start a (test) discard observation 
  string discardObs(UFDeviceConfig* dc); // mce4d sends acq action to edt then starts mce4
  static string discardObs(UFSocket* soc); // edtd sends obssetup to dhs (replican) client
  // stop observation (connect to ufedtd if necessary)
  // data should be saved
  string stopObs(UFDeviceConfig* dc); // mce4d sends acq stop action to edt then stops mce4
  static string stopObs(UFSocket* soc); // for unit tests and for mce4edtd sends acq stop action to dhsclient
  // abort observation (connect to ufedtd if necessary)
  // data should be discarded
  string abortObs(UFDeviceConfig* dc); // mce4d sends acq stop action to edt then stops mce4
  static string abortObs(UFSocket* soc); // for unit tests and for mce4edtd sends acq stop action to dhsclient

  // return file decr. &  FITS header from opened file, seeking to start of first data frame
  int openFITS(const string& filename, UFStrings*& fitsHdr, int& w, int& h, int& frmtotal);
  // return FITS data from open file, seeking to next data frame
  UFInts* seekFITSData(const int fd, const int w= 2048, const int h= 2048);

  // allocate & fetch/set FITS header for obs. datalabel, using agents (default) or epics sad:
  // primary header (initial or final vals)
  UFStrings* fitsHdrPrm(const string& datalabel, UFFITSheader& fh, const string& epics= "");
  UFStrings* fitsHdrExt(const string& datalabel= "", const string& epics= ""); // extension header

  // extract all headers for the given datalabel from the _FITSheaders history
  // (for real DHS datalabels will likely be one primay with one or more extensions,
  // but for Test datalabels, who knows):
  //int fitsHdrs(const string& datalabel, deque< UFStrings* >& fits);

  // integer key value
  static int fitsInt(const string& key, char* fitsHdr);
  // float/double key value
  static double fitsFlt(const string& key, char* fitsHdr);
  // string key value
  static string fitsStr(const string& key, char* fitsHdr);

  // just a reminder that this is the actuall ufedtd frame socket client connection:
  inline UFClientSocket* getFrmSoc() { return this; }

protected:
  // mce dc agent and dhsclient daemon should use this acqctrl class to connect to edtd
  // and optionally to device agents for fits transactions
  bool _connectedEdt, _connectedFITS;
  string _agthost, _edtfrmhost;
  UFFITSClient* _clientFITS;
  UFSocket::ConnectTable _fitsoc;
  map< string, int > _portmap; // 'agent@host', portNo
  //multimap< string, UFStrings* > _FITSheaders; // datalabel, FITS header (primary & extensions)
};

#endif // __UFAcqCtrl_h__

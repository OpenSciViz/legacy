#if !defined(__UFImgServ_cc__)
#define __UFImgServ_cc__ "$Name:  $ $Id: UFImgServ.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFImgServ_cc__;

#include "UFImgServ.h"
__UFImgServ_H__(__UFImgServ_cc);

#include "time.h"

// default behavior of virtuals:
string UFImgServ::heartbeat() {
  string sl = currentTime("local");
  string sg = currentTime("gmt");
  string t = sl + " (" + sg + " gmt)";
  return t;
}

void* UFImgServ::exec(void* p) {
  // this is the main function for the Image Server ImgServ
  // a unit test would simply call this directly from main
  // the system executive should call this immediately upon
  // creation of the child process
  do {
    clog<<"UFImgServ::heartbeat at t= "<<heartbeat()<<endl;
    sleep(600); // 10 min.
  } while( true );
}

#endif // __UFImgServ_cc__

/* UFStringTokenizer.cc
 */

#ifndef __UFStringTokenizer_cc__
#define __UFStringTokenizer_cc__ "$Name:  $ $Id: UFStringTokenizer.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFStringTokenizer_cc__ ;

#include	<vector>
#include	<string>
#include	<stdexcept>
#include	<iostream>

#include	<cassert>

#include	"UFStringTokenizer.h"
__UFStringTokenizer_H__(__UFStringTokenizer_cc__) ;

using std::string ;
using std::vector ;

UFStringTokenizer::UFStringTokenizer( const string& buf, char _delimiter )
	: original( buf ), delimiter( _delimiter )
{
Tokenize( original ) ;
}

UFStringTokenizer::~UFStringTokenizer()
{ /* No heap space allocated */ }

/* getToken()
 * Return an individual token to the calling
 * method.
 * Throws an exception if the requested index
 * is out of range
 */
const string& UFStringTokenizer::getToken( const size_type& sub ) const
{
#ifndef NDEBUG
	// Dump core
	assert( validSubscript( sub ) ) ;
#endif
return array[ sub ] ;
}

/* Tokenize()
 * Break the string into elements delimited
 * by the given delimiter.
 * Fill the array with individual tokens
 */
void UFStringTokenizer::Tokenize( const string& buf )
{

// Make sure that there is something
// worth tokenizing
if( buf.empty() )
	{
	return ;
	}

string addMe ;
string::const_iterator currentPtr = buf.begin() ;
string::const_iterator endPtr = buf.end() ;

for( ; currentPtr != endPtr ; ++currentPtr )
	{
	if( delimiter == *currentPtr )
		{
		if( !addMe.empty() )
			{
			array.push_back( addMe ) ;

			// GNU doesn't implement the std::string::clear() method.
			addMe.erase( addMe.begin(), addMe.end() ) ;
			}
		}
	else
		{
		addMe += *currentPtr ;
		}
	}

// currentPtr == endPtr
if( !addMe.empty() )
	{
	array.push_back( addMe ) ;
	}

}

/* assemble()
 * Assemble into a string all tokens starting from index
 * start
 */
string UFStringTokenizer::assemble( const size_type& start ) const
{

// check if the beginning index is valid
if( !validSubscript( start ) )
	{
	return string() ;
	}

string retMe ;

// continue while there are more tokens to concatenate
for( size_type i = start ; i < size() ; i++ )
	{
	retMe += array[ i ] ;

	// If there is another token, put a delimiter here
	if( (i + 1) < size() )
		{
		retMe += delimiter ;
		}

	} // close for

return retMe ;

}

#endif // __UFStringTokenizer_cc__

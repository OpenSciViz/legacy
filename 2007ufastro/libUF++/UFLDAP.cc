#if !defined(__UFLDAP_cc__)
#define __UFLDAP_cc__ "$Name:  $ $Id: UFLDAP.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFLDAP_cc__;

#include "UFLDAP.h"
__UFLDAP_H__(__UFLDAP_cc);

#include "time.h"

// default behavior of virtuals:
string UFLDAP::heartbeat() {
  string sl = currentTime("local");
  string sg = currentTime("gmt");
  string t = sl + " (" + sg + " gmt)";
  return t;
}

void* UFLDAP::exec(void* p) {
  // this is the main function for the Image Server LDAP
  // a unit test would simply call this directly from main
  // the system executive should call this immediately upon
  // creation of the child process
  do {
    clog<<"UFLDAP::heartbeat at t= "<<heartbeat()<<endl;
    sleep(600); // 10 min.
  } while( true );
}

#endif // __UFLDAP_cc__

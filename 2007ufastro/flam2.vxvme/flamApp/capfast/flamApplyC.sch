[schematic2]
uniq 254
[tools]
[detail]
w 498 43 100 0 n#252 hwin.hwin#249.in 96 32 960 32 960 800 1344 800 egenSubE.egenSubE#78.INPN
w 482 107 100 0 n#251 hwin.hwin#248.in 96 96 928 96 928 832 1344 832 egenSubE.egenSubE#78.INPM
w 466 171 100 0 n#247 hwin.hwin#245.in 96 160 896 160 896 864 1344 864 egenSubE.egenSubE#78.INPL
w 450 235 100 0 n#246 hwin.hwin#244.in 96 224 864 224 864 896 1344 896 egenSubE.egenSubE#78.INPK
w 450 1611 100 0 n#243 hwin.hwin#240.in 96 1600 864 1600 864 928 1344 928 egenSubE.egenSubE#78.INPJ
w 466 1675 100 0 n#242 hwin.hwin#241.in 96 1664 896 1664 896 960 1344 960 egenSubE.egenSubE#78.INPI
w 226 299 100 0 n#238 hwin.hwin#215.in 96 288 416 288 416 608 512 608 egenSubE.egenSubE#37.INPT
w 210 363 100 0 n#237 hwin.hwin#216.in 96 352 384 352 384 640 512 640 egenSubE.egenSubE#37.INPS
w 514 -21 100 0 n#239 hwin.hwin#225.in 96 -32 992 -32 992 768 1344 768 egenSubE.egenSubE#78.INPO
w 530 -85 100 0 n#231 hwin.hwin#226.in 96 -96 1024 -96 1024 736 1344 736 egenSubE.egenSubE#78.INPP
w 546 -149 100 0 n#230 hwin.hwin#228.in 96 -160 1056 -160 1056 704 1344 704 egenSubE.egenSubE#78.INPQ
w 562 -213 100 0 n#253 hwin.hwin#227.in 96 -224 1088 -224 1088 672 1344 672 egenSubE.egenSubE#78.INPR
w 562 2059 100 0 n#224 hwin.hwin#81.in 96 2048 1088 2048 1088 1152 1344 1152 egenSubE.egenSubE#78.INPC
w 546 1995 100 0 n#223 hwin.hwin#82.in 96 1984 1056 1984 1056 1120 1344 1120 egenSubE.egenSubE#78.INPD
w 530 1931 100 0 n#222 hwin.hwin#79.in 96 1920 1024 1920 1024 1088 1344 1088 egenSubE.egenSubE#78.INPE
w 514 1867 100 0 n#221 hwin.hwin#80.in 96 1856 992 1856 992 1056 1344 1056 egenSubE.egenSubE#78.INPF
w 498 1803 100 0 n#220 hwin.hwin#84.in 96 1792 960 1792 960 1024 1344 1024 egenSubE.egenSubE#78.INPG
w 482 1739 100 0 n#219 hwin.hwin#83.in 96 1728 928 1728 928 992 1344 992 egenSubE.egenSubE#78.INPH
w 1746 859 100 0 n#214 estringouts.estringouts#124.SDIS 1856 848 1696 848 1696 1152 junction
w 1714 1163 100 0 n#214 elongouts.elongouts#123.SDIS 1856 1152 1632 1152 egenSubE.egenSubE#78.VALC
w 1762 891 100 0 n#213 egenSubE.egenSubE#78.FLNK 1632 480 1728 480 1728 880 1856 880 estringouts.estringouts#124.SLNK
w 1762 923 100 0 n#212 egenSubE.egenSubE#78.VALB 1632 1184 1728 1184 1728 912 1856 912 estringouts.estringouts#124.DOL
w 1714 1227 100 0 n#211 egenSubE.egenSubE#78.VALA 1632 1216 1856 1216 elongouts.elongouts#123.DOL
w 994 491 100 0 n#178 egenSubE.egenSubE#37.FLNK 800 480 1248 480 1248 512 1344 512 egenSubE.egenSubE#78.SLNK
w 1042 1195 100 0 n#166 egenSubE.egenSubE#37.VALB 800 1184 1344 1184 egenSubE.egenSubE#78.INPB
w 1042 1227 100 0 n#165 egenSubE.egenSubE#37.VALA 800 1216 1344 1216 egenSubE.egenSubE#78.INPA
w 2098 1403 100 0 n#147 hwin.hwin#146.in 2048 1392 2208 1392 2208 1120 2304 1120 ecars.ecars#34.ICID
w 2130 875 100 0 n#145 estringouts.estringouts#124.OUT 2112 864 2208 864 2208 1088 2304 1088 ecars.ecars#34.IMSS
w 2178 1163 100 0 n#144 elongouts.elongouts#123.OUT 2112 1152 2304 1152 ecars.ecars#34.IVAL
w 1922 1003 100 0 n#129 estringouts.estringouts#124.FLNK 2112 896 2144 896 2144 992 1760 992 1760 1184 1856 1184 elongouts.elongouts#123.SLNK
w 200 427 100 0 n#77 egenSubE.egenSubE#37.INPR 512 672 352 672 352 416 96 416 hwin.hwin#57.in
w 184 491 100 0 n#76 egenSubE.egenSubE#37.INPQ 512 704 320 704 320 480 96 480 hwin.hwin#56.in
w 376 747 100 0 n#73 egenSubE.egenSubE#37.INPP 512 736 288 736 288 544 96 544 hwin.hwin#53.in
w 360 779 100 0 n#72 egenSubE.egenSubE#37.INPO 512 768 256 768 256 608 96 608 hwin.hwin#52.in
w 344 811 100 0 n#71 egenSubE.egenSubE#37.INPN 512 800 224 800 224 672 96 672 hwin.hwin#50.in
w 328 843 100 0 n#70 egenSubE.egenSubE#37.INPM 512 832 192 832 192 736 96 736 hwin.hwin#51.in
w 312 875 100 0 n#69 egenSubE.egenSubE#37.INPL 512 864 160 864 160 800 96 800 hwin.hwin#49.in
w 296 907 100 0 n#68 egenSubE.egenSubE#37.INPK 512 896 128 896 128 864 96 864 hwin.hwin#48.in
w 296 939 100 0 n#67 hwin.hwin#46.in 96 960 128 960 128 928 512 928 egenSubE.egenSubE#37.INPJ
w 312 971 100 0 n#66 hwin.hwin#47.in 96 1024 160 1024 160 960 512 960 egenSubE.egenSubE#37.INPI
w 328 1003 100 0 n#65 hwin.hwin#45.in 96 1088 192 1088 192 992 512 992 egenSubE.egenSubE#37.INPH
w 344 1035 100 0 n#64 hwin.hwin#44.in 96 1152 224 1152 224 1024 512 1024 egenSubE.egenSubE#37.INPG
w 360 1067 100 0 n#63 hwin.hwin#42.in 96 1216 256 1216 256 1056 512 1056 egenSubE.egenSubE#37.INPF
w 376 1099 100 0 n#62 hwin.hwin#43.in 96 1280 288 1280 288 1088 512 1088 egenSubE.egenSubE#37.INPE
w 184 1355 100 0 n#61 hwin.hwin#41.in 96 1344 320 1344 320 1120 512 1120 egenSubE.egenSubE#37.INPD
w 200 1419 100 0 n#60 hwin.hwin#40.in 96 1408 352 1408 352 1152 512 1152 egenSubE.egenSubE#37.INPC
w 216 1483 100 0 n#59 egenSubE.egenSubE#37.INPB 512 1184 384 1184 384 1472 96 1472 hwin.hwin#39.in
w 232 1547 100 0 n#58 egenSubE.egenSubE#37.INPA 512 1216 416 1216 416 1536 96 1536 hwin.hwin#38.in
s 2016 2064 100 1792 B
s 2240 2064 100 1792 Add pause and continue
s 2480 2064 100 1792 WNR
s 2624 2064 100 1792 2002/11/11
s 2240 -128 100 0 FLAMINGOS
s 2096 -176 200 1792 FLAMINGOS
s 2432 -192 100 256 Flamingos ApplyC Generator
s 2320 -240 100 1792 Rev: B
s 2096 -240 100 1792 2002/11/11
s 2096 -272 100 1792 Author: WNR
s 2512 -240 100 1792 flamApplyC.sch
s 2016 2064 100 1792 A
s 2240 2064 100 1792 Initial Layout
s 2480 2064 100 1792 WNR
s 2624 2064 100 1792 2000/12/14
[cell use]
use hwin -96 119 100 0 hwin#245
xform 0 0 160
p -368 160 100 0 -1 val(in):$(top)dc:applyC.OMSS
use hwin -96 183 100 0 hwin#244
xform 0 0 224
p -352 224 100 0 -1 val(in):$(top)dc:applyC.VAL
use hwin -96 -9 100 0 hwin#249
xform 0 0 32
p -368 32 100 0 -1 val(in):$(top)cc:applyC.OMSS
use hwin -96 55 100 0 hwin#248
xform 0 0 96
p -352 96 100 0 -1 val(in):$(top)cc:applyC.VAL
use hwin -96 1623 100 0 hwin#241
xform 0 0 1664
p -304 1664 100 0 -1 val(in):$(top)pauseC.VAL
use hwin -96 1559 100 0 hwin#240
xform 0 0 1600
p -320 1600 100 0 -1 val(in):$(top)pauseC.OMSS
use changeBar 1984 2023 100 0 changeBar#236
xform 0 2336 2064
use changeBar 1984 2023 100 0 changeBar#235
xform 0 2336 2064
use hwin -96 -201 100 0 hwin#228
xform 0 0 -160
p -352 -160 100 0 -1 val(in):$(top)continueC.VAL
use hwin -96 -265 100 0 hwin#227
xform 0 0 -224
p -368 -224 100 0 -1 val(in):$(top)continueC.OMSS
use hwin -96 -137 100 0 hwin#226
xform 0 0 -96
p -368 -96 100 0 -1 val(in):$(top)ec:applyC.OMSS
use hwin -96 -73 100 0 hwin#225
xform 0 0 -32
p -352 -32 100 0 -1 val(in):$(top)ec:applyC.VAL
use hwin -96 1495 100 0 hwin#38
xform 0 0 1536
p -304 1536 100 0 -1 val(in):$(top)initC.VAL
use hwin -96 1431 100 0 hwin#39
xform 0 0 1472
p -320 1472 100 0 -1 val(in):$(top)initC.OMSS
use hwin -96 1367 100 0 hwin#40
xform 0 0 1408
p -304 1408 100 0 -1 val(in):$(top)testC.VAL
use hwin -96 1303 100 0 hwin#41
xform 0 0 1344
p -320 1344 100 0 -1 val(in):$(top)testC.OMSS
use hwin -96 1175 100 0 hwin#42
xform 0 0 1216
p -336 1216 100 0 -1 val(in):$(top)datumC.OMSS
use hwin -96 1239 100 0 hwin#43
xform 0 0 1280
p -320 1280 100 0 -1 val(in):$(top)datumC.VAL
use hwin -96 1111 100 0 hwin#44
xform 0 0 1152
p -304 1152 100 0 -1 val(in):$(top)parkC.VAL
use hwin -96 1047 100 0 hwin#45
xform 0 0 1088
p -320 1088 100 0 -1 val(in):$(top)parkC.OMSS
use hwin -96 919 100 0 hwin#46
xform 0 0 960
p -336 960 100 0 -1 val(in):$(top)debugC.OMSS
use hwin -96 983 100 0 hwin#47
xform 0 0 1024
p -320 1024 100 0 -1 val(in):$(top)debugC.VAL
use hwin -96 823 100 0 hwin#48
xform 0 0 864
p -320 864 100 0 -1 val(in):$(top)rebootC.VAL
use hwin -96 759 100 0 hwin#49
xform 0 0 800
p -336 800 100 0 -1 val(in):$(top)rebootC.OMSS
use hwin -96 631 100 0 hwin#50
xform 0 0 672
p -448 672 100 0 -1 val(in):$(top)instrumentSetupC.OMSS
use hwin -96 695 100 0 hwin#51
xform 0 0 736
p -432 736 100 0 -1 val(in):$(top)instrumentSetupC.VAL
use hwin -96 567 100 0 hwin#52
xform 0 0 608
p -352 608 100 0 -1 val(in):$(top)dataModeC.VAL
use hwin -96 503 100 0 hwin#53
xform 0 0 544
p -368 544 100 0 -1 val(in):$(top)dataModeC.OMSS
use hwin -96 439 100 0 hwin#56
xform 0 0 480
p -368 480 100 0 -1 val(in):$(top)setDhsInfoC.VAL
use hwin -96 375 100 0 hwin#57
xform 0 0 416
p -384 416 100 0 -1 val(in):$(top)setDhsInfoC.OMSS
use hwin -96 1879 100 0 hwin#79
xform 0 0 1920
p -448 1920 100 0 -1 val(in):$(top)observationSetupC.VAL
use hwin -96 1815 100 0 hwin#80
xform 0 0 1856
p -464 1856 100 0 -1 val(in):$(top)observationSetupC.OMSS
use hwin -96 2007 100 0 hwin#81
xform 0 0 2048
p -368 2048 100 0 -1 val(in):$(top)observeCmdC.VAL
use hwin -96 1943 100 0 hwin#82
xform 0 0 1984
p -384 1984 100 0 -1 val(in):$(top)observeCmdC.OMSS
use hwin -96 1687 100 0 hwin#83
xform 0 0 1728
p -320 1728 100 0 -1 val(in):$(top)abortC.OMSS
use hwin -96 1751 100 0 hwin#84
xform 0 0 1792
p -304 1792 100 0 -1 val(in):$(top)abortC.VAL
use hwin 1856 1351 100 0 hwin#146
xform 0 1952 1392
p 1632 1392 100 0 -1 val(in):$(top)apply.CLID
use hwin -96 247 100 0 hwin#215
xform 0 0 288
p -320 288 100 0 -1 val(in):$(top)stopC.OMSS
use hwin -96 311 100 0 hwin#216
xform 0 0 352
p -304 352 100 0 -1 val(in):$(top)stopC.VAL
use estringouts 1856 807 100 0 estringouts#124
xform 0 1984 880
p 1920 768 100 0 1 OMSL:closed_loop
p 2048 800 100 1024 1 name:$(top)carMessage
use elongouts 1856 1095 100 0 elongouts#123
xform 0 1984 1184
p 1696 1198 100 0 0 DISV:1
p 1920 1056 100 0 1 OMSL:closed_loop
p 2032 1088 100 1024 1 name:$(top)carState
p 2112 1152 75 768 -1 pproc(OUT):PP
use egenSubE 512 423 100 0 egenSubE#37
xform 0 656 848
p 289 197 100 0 0 FTA:LONG
p 289 197 100 0 0 FTB:STRING
p 289 165 100 0 0 FTC:LONG
p 289 133 100 0 0 FTD:STRING
p 289 101 100 0 0 FTE:LONG
p 289 37 100 0 0 FTF:STRING
p 289 37 100 0 0 FTG:LONG
p 289 5 100 0 0 FTH:STRING
p 289 -27 100 0 0 FTI:LONG
p 289 -59 100 0 0 FTJ:STRING
p 289 197 100 0 0 FTK:LONG
p 289 197 100 0 0 FTL:STRING
p 289 165 100 0 0 FTM:LONG
p 289 133 100 0 0 FTN:STRING
p 289 101 100 0 0 FTO:LONG
p 289 37 100 0 0 FTP:STRING
p 289 37 100 0 0 FTQ:LONG
p 289 5 100 0 0 FTR:STRING
p 289 -27 100 0 0 FTS:LONG
p 289 -59 100 0 0 FTT:STRING
p 289 197 100 0 0 FTVA:LONG
p 289 197 100 0 0 FTVB:STRING
p 480 384 100 0 1 INAM:flamIsNullGInit
p 480 320 100 0 1 SCAN:.1 second
p 480 352 100 0 1 SNAM:flamCmdCombineGProcess
p 624 416 100 1024 1 name:$(top)cmdCombineG1
use egenSubE 1344 423 100 0 egenSubE#78
xform 0 1488 848
p 1121 197 100 0 0 FTA:LONG
p 1121 197 100 0 0 FTB:STRING
p 1121 165 100 0 0 FTC:LONG
p 1121 133 100 0 0 FTD:STRING
p 1121 101 100 0 0 FTE:LONG
p 1121 37 100 0 0 FTF:STRING
p 1121 37 100 0 0 FTG:LONG
p 1121 5 100 0 0 FTH:STRING
p 1121 -27 100 0 0 FTI:LONG
p 1121 -59 100 0 0 FTJ:STRING
p 1121 197 100 0 0 FTK:LONG
p 1121 197 100 0 0 FTL:STRING
p 1121 165 100 0 0 FTM:LONG
p 1121 133 100 0 0 FTN:STRING
p 1121 101 100 0 0 FTO:LONG
p 1121 37 100 0 0 FTP:STRING
p 1121 37 100 0 0 FTQ:LONG
p 1121 5 100 0 0 FTR:STRING
p 1121 -27 100 0 0 FTS:LONG
p 1121 -59 100 0 0 FTT:STRING
p 1121 197 100 0 0 FTVA:LONG
p 1121 197 100 0 0 FTVB:STRING
p 1121 165 100 0 0 FTVC:LONG
p 1312 384 100 0 1 INAM:flamIsNullGInit
p 1312 352 100 0 1 SNAM:flamCmdCombineGProcess
p 1456 416 100 1024 1 name:$(top)cmdCombineG2
use ecars 2304 871 100 0 ecars#34
xform 0 2464 1040
p 2480 864 100 1024 1 name:$(top)applyC
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamApplyC.sch,v 0.0 2007/02/02 21:09:44 aaguayo Developmental $

[schematic2]
uniq 355
[tools]
[detail]
w 354 923 100 0 n#353 hwin.hwin#354.in 288 912 480 912 480 928 608 928 egenSubD.egenSubD#259.INPJ
w 354 987 100 0 n#349 hwin.hwin#350.in 288 976 480 976 480 992 608 992 egenSubD.egenSubD#259.INPH
w 354 1019 100 0 n#347 hwin.hwin#348.in 288 1008 480 1008 480 1024 608 1024 egenSubD.egenSubD#259.INPG
w 354 1051 100 0 n#345 hwin.hwin#346.in 288 1040 480 1040 480 1056 608 1056 egenSubD.egenSubD#259.INPF
w 1650 1067 100 0 n#309 estringouts.estringouts#307.OUT 1600 1040 1600 1056 1760 1056 1760 1136 1856 1136 hwout.hwout#285.outp
w 1090 1099 100 0 n#306 estringouts.estringouts#307.DOL 1344 1088 896 1088 egenSubD.egenSubD#259.VALC
w 1090 1067 100 0 n#305 estringouts.estringouts#307.SLNK 1344 1056 896 1056 egenSubD.egenSubD#259.OUTC
w 1698 635 100 0 n#303 estringouts.estringouts#302.OUT 1600 624 1856 624 hwout.hwout#304.outp
w 1090 683 100 0 n#301 estringouts.estringouts#302.DOL 1344 672 896 672 egenSubD.egenSubD#259.OUTI
w 1090 651 100 0 n#300 estringouts.estringouts#302.SLNK 1344 640 896 640 egenSubD.egenSubD#259.VALJ
w 1090 779 100 0 n#299 estringouts.estringouts#297.SLNK 1344 768 896 768 egenSubD.egenSubD#259.VALH
w 1090 811 100 0 n#298 estringouts.estringouts#297.DOL 1344 800 896 800 egenSubD.egenSubD#259.OUTG
w 1698 763 100 0 n#296 estringouts.estringouts#297.OUT 1600 752 1856 752 hwout.hwout#295.outp
w 1010 1195 100 0 n#289 estringouts.estringouts#287.SLNK 1344 1152 1184 1152 1184 1184 896 1184 egenSubD.egenSubD#259.OUTA
w 1090 1227 100 0 n#288 estringouts.estringouts#287.DOL 1344 1184 1344 1216 896 1216 egenSubD.egenSubD#259.VALA
w 1634 1115 100 0 n#286 estringouts.estringouts#287.OUT 1600 1136 1600 1104 1728 1104 1728 1040 1856 1040 hwout.hwout#308.outp
w 1698 923 100 0 n#264 estringouts.estringouts#260.OUT 1600 912 1856 912 hwout.hwout#263.outp
w 1090 939 100 0 n#262 estringouts.estringouts#260.SLNK 1344 928 896 928 egenSubD.egenSubD#259.OUTE
w 1090 971 100 0 n#261 estringouts.estringouts#260.DOL 1344 960 896 960 egenSubD.egenSubD#259.VALE
w 354 1083 100 0 n#247 hwin.hwin#225.in 288 1072 480 1072 480 1088 608 1088 egenSubD.egenSubD#259.INPE
w 418 1131 100 0 n#246 hwin.hwin#215.in 288 1120 608 1120 egenSubD.egenSubD#259.INPD
w 354 1291 100 0 n#245 hwin.hwin#81.in 288 1280 480 1280 480 1216 608 1216 egenSubD.egenSubD#259.INPA
w 338 1243 100 0 n#244 hwin.hwin#82.in 288 1232 448 1232 448 1184 608 1184 egenSubD.egenSubD#259.INPB
w 490 1163 100 0 n#243 hwin.hwin#216.in 288 1184 432 1184 432 1152 608 1152 egenSubD.egenSubD#259.INPC
s 2240 -128 100 0 FLAMINGOS
s 2096 -176 200 1792 FLAMINGOS
s 2432 -192 100 256 Flamingos ObserveC Generator
s 2320 -240 100 1792 Rev: B
s 2096 -240 100 1792 2003/05/28
s 2096 -272 100 1792 Author: WNR
s 2512 -240 100 1792 flamObserveC.sch
s 2016 2064 100 1792 A
s 2240 2064 100 1792 Initial Layout
s 2480 2064 100 1792 WNR
s 2624 2064 100 1792 2002/11/11
s 2016 2032 100 1792 B
s 2240 2032 100 1792 Corrected nod complete link
s 2480 2032 100 1792 WNR
s 2624 2032 100 1792 2003/05/28
[cell use]
use hwin 96 871 100 0 hwin#354
xform 0 192 912
p 64 912 100 512 -1 val(in):$(top)setDhsInfoC.VAL
use hwout 1856 1095 100 0 hwout#285
xform 0 1952 1136
p 1952 1127 100 0 -1 val(outp):$(sad)health.VAL PP NMS
use hwin 96 935 100 0 hwin#350
xform 0 192 976
p 64 976 100 512 -1 val(in):$(top)dataModeC.VAL
use hwin 96 967 100 0 hwin#348
xform 0 192 1008
p 64 1008 100 512 -1 val(in):$(top)instrumentSetupC.VAL
use hwin 96 999 100 0 hwin#346
xform 0 192 1040
p 64 1040 100 512 -1 val(in):$(top)rebootC.VAL
use hwin 96 1031 100 0 hwin#225
xform 0 192 1072
p 64 1072 100 512 -1 val(in):$(top)debugC.VAL
use hwin 96 1239 100 0 hwin#81
xform 0 192 1280
p 64 1280 100 512 -1 val(in):$(top)initC.VAL
use hwin 96 1191 100 0 hwin#82
xform 0 192 1232
p 64 1232 100 512 -1 val(in):$(top)testC.VAL
use hwin 96 1079 100 0 hwin#215
xform 0 192 1120
p 64 1120 100 512 -1 val(in):$(top)parkC.VAL
use hwin 96 1143 100 0 hwin#216
xform 0 192 1184
p 64 1184 100 512 -1 val(in):$(top)datumC.VAL
use hwout 1856 999 100 0 hwout#308
xform 0 1952 1040
p 1952 1031 100 0 -1 val(outp):$(sad)state.VAL PP NMS
use hwout 1856 583 100 0 hwout#304
xform 0 1952 624
p 1952 615 100 0 -1 val(outp):$(sad)parKed.VAL PP NMS
use hwout 1856 711 100 0 hwout#295
xform 0 1952 752
p 1952 743 100 0 -1 val(outp):$(sad)datummed.VAL PP NMS
use hwout 1856 871 100 0 hwout#263
xform 0 1952 912
p 1952 903 100 0 -1 val(outp):$(sad)initialized.VAL PP NMS
use estringouts 1344 983 100 0 estringouts#307
xform 0 1472 1056
p 1456 976 100 1024 0 name:$(top)state
use estringouts 1344 567 100 0 estringouts#302
xform 0 1472 640
p 1456 560 100 1024 0 name:$(top)parKed
use estringouts 1344 695 100 0 estringouts#297
xform 0 1472 768
p 1456 688 100 1024 0 name:$(top)datummed
use estringouts 1344 1079 100 0 estringouts#287
xform 0 1472 1152
p 1456 1072 100 1024 0 name:$(top)health
use estringouts 1344 855 100 0 estringouts#260
xform 0 1472 928
p 1456 848 100 1024 0 name:$(top)initialized
use egenSubD 608 423 100 0 egenSubD#259
xform 0 752 848
p 385 197 100 0 0 FTA:LONG
p 385 197 100 0 0 FTB:LONG
p 385 165 100 0 0 FTC:LONG
p 385 133 100 0 0 FTD:LONG
p 385 101 100 0 0 FTE:LONG
p 385 37 100 0 0 FTF:LONG
p 385 37 100 0 0 FTG:LONG
p 385 5 100 0 0 FTH:LONG
p 385 -27 100 0 0 FTI:LONG
p 385 -59 100 0 0 FTJ:LONG
p 385 197 100 0 0 FTK:LONG
p 385 197 100 0 0 FTL:LONG
p 385 165 100 0 0 FTM:LONG
p 385 133 100 0 0 FTN:LONG
p 385 101 100 0 0 FTO:LONG
p 385 37 100 0 0 FTP:LONG
p 385 37 100 0 0 FTQ:LONG
p 385 5 100 0 0 FTR:LONG
p 385 -27 100 0 0 FTS:LONG
p 385 -59 100 0 0 FTT:LONG
p 385 -59 100 0 0 FTU:LONG
p 385 197 100 0 0 FTVA:STRING
p 385 197 100 0 0 FTVB:LONG
p 385 165 100 0 0 FTVC:STRING
p 385 133 100 0 0 FTVD:LONG
p 385 101 100 0 0 FTVE:STRING
p 385 37 100 0 0 FTVF:LONG
p 385 37 100 0 0 FTVG:STRING
p 385 5 100 0 0 FTVH:LONG
p 385 -27 100 0 0 FTVI:STRING
p 385 -59 100 0 0 FTVJ:LONG
p 320 830 100 0 0 INAM:flamIsNullGInit
p 320 1118 100 0 0 SCAN:.1 second
p 320 798 100 0 0 SNAM:flamStateCombineGProcess
p 720 416 100 1024 0 name:$(top)stateCombineG
use changeBar 1984 2023 100 0 changeBar#235
xform 0 2336 2064
use changeBar 1984 1991 100 0 changeBar#258
xform 0 2336 2032
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamState3.sch,v 0.0 2007/02/02 21:09:44 aaguayo Developmental $

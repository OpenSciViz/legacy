[schematic2]
uniq 4
[tools]
[detail]
[cell use]
use esirs 96 -377 100 0 esirs#3
xform 0 304 -224
p 208 -384 100 1024 1 name:$(top)generic4
use esirs -480 -377 100 0 esirs#2
xform 0 -272 -224
p -368 -384 100 1024 1 name:$(top)generic3
use esirs 96 7 100 0 esirs#1
xform 0 304 160
p 208 0 100 1024 1 name:$(top)generic2
use esirs -464 7 100 0 esirs#0
xform 0 -256 160
p -352 0 100 1024 1 name:$(top)generic1
[comments]

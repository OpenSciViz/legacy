[schematic2]
uniq 16
[tools]
[detail]
[cell use]
use esirs 160 39 100 0 esirs#3
xform 0 368 192
p 384 80 100 0 1 HHSV:MAJOR
p 256 176 100 0 1 HIGH:100
p 256 144 100 0 1 HIHI:200
p 400 176 100 0 1 HSV:MINOR
p 352 336 100 1024 -1 name:$(top)TIME:health
use estringins 320 -185 100 0 estringins#2
xform 0 448 -112
p 448 -32 100 1024 -1 name:$(top)logrecord
use ebos -256 -217 100 0 ebos#1
xform 0 -128 -128
p -192 -256 100 0 1 ONAM:True
p -32 -224 100 0 1 OSV:MINOR
p -192 -224 100 0 1 ZNAM:False
p -64 -48 100 1024 -1 name:$(top)TIME:intSimulate
[comments]

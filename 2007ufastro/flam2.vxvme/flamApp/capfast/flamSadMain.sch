[schematic2]
uniq 57
[tools]
[detail]
s 2624 2032 100 1792 2001/01/25
s 2480 2032 100 1792 WNR
s 2240 2032 100 1792 Added EC status
s 2016 2032 100 1792 B
s 2624 2064 100 1792 2000/11/18
s 2480 2064 100 1792 WNR
s 2240 2064 100 1792 Initial Layout
s 2016 2064 100 1792 A
s 2512 -240 100 1792 flamSadMain.sch
s 2096 -272 100 1792 Author: NWR
s 2096 -240 100 1792 2001/01/25
s 2320 -240 100 1792 Rev: B
s 2432 -192 100 256 Flamingos SAD Overview
s 2096 -176 200 1792 FLAMINGOS
s 2240 -128 100 0 FLAMINGOS
[cell use]
use flamSadBcrConfig 2048 1287 100 0 flamSadBcrConfig#56
xform 0 2192 1440
use flamSadEcStatus 1088 1287 100 0 flamSadEcStatus#55
xform 0 1232 1440
use flamSadCcEngineering 608 39 100 0 flamSadCcEngineering#53
xform 0 752 192
p 628 12 100 0 1 setDev:dev Window
use flamSadCcEngineering 1088 39 100 0 flamSadCcEngineering#52
xform 0 1232 192
p 1108 12 100 0 1 setDev:dev Focus
use flamSadCcEngineering 128 39 100 0 flamSadCcEngineering#51
xform 0 272 192
p 148 12 100 0 1 setDev:dev Grism
use flamSadCcEngineering 1088 455 100 0 flamSadCcEngineering#50
xform 0 1232 608
p 1108 428 100 0 1 setDev:dev Lyot
use flamSadCcEngineering 128 455 100 0 flamSadCcEngineering#49
xform 0 272 608
p 148 428 100 0 1 setDev:dev Filter2
use flamSadCcEngineering -352 39 100 0 flamSadCcEngineering#48
xform 0 -208 192
p -332 12 100 0 1 setDev:dev Filter1
use flamSadCcEngineering -352 455 100 0 flamSadCcEngineering#47
xform 0 -208 608
p -332 428 100 0 1 setDev:dev MOS
use flamSadCcEngineering 608 455 100 0 flamSadCcEngineering#34
xform 0 752 608
p 628 428 100 0 1 setDev:dev Decker
use flamSadEcEngineering 1088 871 100 0 flamSadEcEngineering#42
xform 0 1232 1024
use flamSadEcConfig 1088 1703 100 0 flamSadEcConfig#40
xform 0 1232 1856
use changeBar 1984 1991 100 0 changeBar#39
xform 0 2336 2032
use changeBar 1984 2023 100 0 changeBar#38
xform 0 2336 2064
use flamSadDcStatus 1568 1287 100 0 flamSadDcStatus#36
xform 0 1712 1440
use flamSadCcStatus 608 1287 100 0 flamSadCcStatus#35
xform 0 752 1440
use flamSadCcConfig 608 1703 100 0 flamSadCcConfig#33
xform 0 752 1856
use flamSadDcEngineering 1568 871 100 0 flamSadDcEngineering#32
xform 0 1712 1024
use flamSadDcConfig 1568 1703 100 0 flamSadDcConfig#31
xform 0 1712 1856
use flamSadInsStatus -352 1287 100 0 flamSadInsStatus#30
xform 0 -208 1440
use flamSadObsStatus 128 1287 100 0 flamSadObsStatus#29
xform 0 272 1440
use flamSadInsConfig -352 1703 100 0 flamSadInsConfig#28
xform 0 -208 1856
use flamSadObsConfig 128 1703 100 0 flamSadObsConfig#27
xform 0 272 1856
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamSadMain.sch,v 0.0 2007/02/02 21:09:44 aaguayo Developmental $

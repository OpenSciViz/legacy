/** \file flam2BcScan.st
 *  \brief This sequence will scan all masks that have SIR records
 *  \author William Rambold
 *  \date 24-10-2006
 *
 */
program flam2BcScan("top=flam2:")

%%static char rcsid[]="$Id";

option +r;   /* Re-entrant */

#include "aomSeqMacros.h"

%%#include "menuCarstates.h"
%%#include "applyRecord.h"
%%#include <string.h>
%%#include <taskLib.h>

/* Local variables */

int mask;
char sirName[64];
char steps [64];

/* SNC assignments */

MONVAR (int,    startScan,   "{top}ec:startScan");
ASSVAR (int,    scanState,   "{top}ec:scanMasksC.IVAL");
ASSVAR (int,    requestDatum, "{top}cc:MOS:datum.DIR");
ASSVAR (int,    startMove,   "{top}:cc:apply.DIR");
MONVAR (int,    moveState,   "{top}:cc:MOS:motorC");
ASSVAR (int,    startRead,   "{top}ec:readBc.DIR");
MONVAR (int,    readState,   "{top}ec:readBcC");
ASSVAR (string, scanMessage, "{top}ec:scanMasksC.IMSS");
ASSVAR (string, rawSteps,    "{top}cc:MOS:steps.A");
ASSVAR (string, maskName,    "{top}ec:readBc.A");
ASSVAR (string, mask1Name,   "{top}ec:readBcG.VALA");
ASSVAR (string, mask2Name,   "{top}ec:readBcG.VALB");
ASSVAR (string, mask3Name,   "{top}ec:readBcG.VALC");
ASSVAR (string, mask4Name,   "{top}ec:readBcG.VALD");
ASSVAR (string, mask5Name,   "{top}ec:readBcG.VALE");
ASSVAR (string, mask6Name,   "{top}ec:readBcG.VALF");
ASSVAR (string, mask7Name,   "{top}ec:readBcG.VALG");
ASSVAR (string, mask8Name,   "{top}ec:readBcG.VALH");
ASSVAR (string, mask9Name,   "{top}ec:readBcG.VALI");
ASSVAR (string, mask10Name,  "{top}ec:readBcG.VALJ");
ASSVAR (string, mask11Name,  "{top}ec:readBcG.VALK");
ASSVAR (string, mask12Name,  "{top}ec:readBcG.VALL");
ASSVAR (string, mask13Name,  "{top}ec:readBcG.VALM");
ASSVAR (string, mask14Name,  "{top}ec:readBcG.VALN");
ASSVAR (string, mask15Name,  "{top}ec:readBcG.VALO");
ASSVAR (string, mask16Name,  "{top}ec:readBcG.VALP");
ASSVAR (string, mask17Name,  "{top}ec:readBcG.VALQ");
ASSVAR (string, mask18Name,  "{top}ec:readBcG.VALR");
ASSVAR (string, mask19Name,  "{top}ec:readBcG.VALS");
ASSVAR (string, mask20Name,  "{top}ec:readBcG.VALT");
ASSVAR (int, masks,          "{top}ec:readBcG.VALU");

/* State program starts here */

ss InitialiseScan
{
  state init
  {
    when ()
    {
printf ("barcode scanning sequence waiting for trigger\n");
    } state waitingForTrigger
  }

  state waitingForTrigger
  {
    when (startScan == 1)
    {
printf ("barcode scanning trigger received\n");
      startScan = 0;
      pvPut(startScan);
      scanState = 2;
      pvPut(scanState);
      requestDatum = 0;
      pvPut(requestDatum);    
      startMove = 3;
      pvPut(startMove);    
    } state datumDelay
  }

  state datumDelay
  {
    when (moveState == 2)
    { 
printf ("MOS wheel datum started\n");
    } state datumming

    when (delay(60.0))
    {
      strcpy (scanMessage,  "MOS wheel datum did not start");
      pvPut(scanMessage);
    } state error
  }

  state datumming
  {
    when (moveState == 0)
    {
printf ("MOS wheel datum completed\n");
      mask = 1;
      pvGet (masks);
      if (mask >= masks)
      {
        mask = 0;
      }
    } state scanning

    when (moveState == 3)
    {
      strcpy (scanMessage, "MOS wheel datum failed");
      pvPut(scanMessage);
    }  state error

    when (delay(500.0))
    {
      strcpy (scanMessage, "MOS wheel datum timed out");
      pvPut(scanMessage);
    } state error
  }

  state scanning
  {
    when (mask == 0)
    {
printf ("mask scan complete\n");
    } state endScan
    
    when (mask == 1)
    {
      pvGet(mask1Name);
      sscanf (mask1Name, "%s %s", sirName, steps);
      strcpy (rawSteps, steps);
      pvPut(rawSteps);
      strcpy (maskName, sirName);
      pvPut(maskName);
      startMove=3;
      pvPut(startMove);
printf ("moving mask 1 to %s\n", steps);
    } state startMotion

    when (mask == 2)
    {
      pvGet(mask2Name);
      strcpy (rawSteps, mask2Name);
      pvPut(rawSteps);
      strcpy (maskName, mask2Name);
      pvPut(maskName);
      startMove=3;
      pvPut(startMove);
    } state startMotion

    when (mask == 3)
    {
      pvGet(mask3Name);
      strcpy (rawSteps, mask3Name);
      pvPut(rawSteps);
      strcpy (maskName, mask3Name);
      startMove=3;
      pvPut(startMove);
    } state startMotion

    when (mask == 4)
    {
      pvGet(mask4Name);
      strcpy (rawSteps, mask4Name);
      pvPut(rawSteps);
      strcpy (maskName, mask4Name);
      pvPut(maskName);
      startMove=3;
      pvPut(startMove);
    } state startMotion

    when (mask == 5)
    {
      pvGet(mask5Name);
      strcpy (rawSteps, mask5Name);
      pvPut(rawSteps);
      strcpy (maskName, mask5Name);
      pvPut(maskName);
      startMove=3;
      pvPut(startMove);
    } state startMotion

    when (mask == 6)
    {
      pvGet(mask6Name);
      strcpy (rawSteps, mask6Name);
      pvPut(rawSteps);
      strcpy (maskName, mask6Name);
      pvPut(maskName);
      startMove=3;
      pvPut(startMove);
    } state startMotion
    
    when (mask == 7)
    {
      pvGet(mask7Name);
      strcpy (rawSteps, mask7Name);
      pvPut(rawSteps);
      strcpy (maskName, mask7Name);
      pvPut(maskName);
      startMove=3;
      pvPut(startMove);
    } state startMotion

    when (mask == 8)
    {
      pvGet(mask8Name);
      strcpy (rawSteps, mask8Name);
      pvPut(rawSteps);
      strcpy (maskName, mask8Name);
      pvPut(maskName);
      startMove=3;
      pvPut(startMove);
    } state startMotion

    when (mask == 9)
    {
      pvGet(mask9Name);
      strcpy (rawSteps, mask9Name);
      pvPut(rawSteps);
      strcpy (maskName, mask9Name);
      pvPut(maskName);
      startMove=3;
      pvPut(startMove);
    } state startMotion

    when (mask == 10)
    {
      pvGet(mask10Name);
      strcpy (rawSteps, mask10Name);
      pvPut(rawSteps);
      strcpy (maskName, mask10Name);
      pvPut(maskName);
      startMove=3;
      pvPut(startMove);
    } state startMotion

    when (mask == 11)
    {
      pvGet(mask11Name);
      strcpy (rawSteps, mask11Name);
      pvPut(rawSteps);
      strcpy (maskName, mask11Name);
      pvPut(maskName);
      startMove=3;
      pvPut(startMove);
    } state startMotion

    when (mask == 12)
    {
      pvGet(mask12Name);
      strcpy (rawSteps, mask12Name);
      pvPut(rawSteps);
      strcpy (maskName, mask12Name);
      pvPut(maskName);
      startMove=3;
      pvPut(startMove);
    } state startMotion

    when (mask == 13)
    {
      pvGet(mask13Name);
      strcpy (rawSteps, mask13Name);
      pvPut(rawSteps);
      strcpy (maskName, mask13Name);
      pvPut(maskName);
      startMove=3;
      pvPut(startMove);
    } state startMotion

    when (mask == 14)
    {
      pvGet(mask14Name);
      strcpy (rawSteps, mask14Name);
      pvPut(rawSteps);
      strcpy (maskName, mask14Name);
      pvPut(maskName);
      startMove=3;
      pvPut(startMove);
    } state startMotion

    when (mask == 15)
    {
      pvGet(mask15Name);
      strcpy (rawSteps, mask15Name);
      pvPut(rawSteps);
      strcpy (maskName, mask15Name);
      pvPut(maskName);
      startMove=3;
      pvPut(startMove);
    } state startMotion

    when (mask == 16)
    {
      pvGet(mask16Name);
      strcpy (rawSteps, mask16Name);
      pvPut(rawSteps);
      strcpy (maskName, mask16Name);
      pvPut(maskName);
      startMove=3;
      pvPut(startMove);
    } state startMotion

    when (mask == 17)
    {
      pvGet(mask17Name);
      strcpy (rawSteps, mask17Name);
      pvPut(rawSteps);
      strcpy (maskName, mask17Name);
      pvPut(maskName);
      startMove=3;
      pvPut(startMove);
    } state startMotion

    when (mask == 18)
    {
      pvGet(mask18Name);
      strcpy (rawSteps, mask18Name);
      pvPut(rawSteps);
      strcpy (maskName, mask18Name);
      pvPut(maskName);
      startMove=3;
      pvPut(startMove);
    } state startMotion

    when (mask == 19)
    {
      pvGet(mask19Name);
      strcpy (rawSteps, mask19Name);
      pvPut(rawSteps);
      strcpy (maskName, mask19Name);
      pvPut(maskName);
      startMove=3;
      pvPut(startMove);
    } state startMotion

    when (mask == 20)
    {
      pvGet(mask20Name);
      strcpy (rawSteps, mask20Name);
      pvPut(rawSteps);
      strcpy (maskName, mask20Name);
      pvPut(maskName);
      startMove=3;
      pvPut(startMove);
    } state startMotion
    
    when (delay(1))
    {
      strcpy (scanMessage, "Scan sequence timeout");
      pvPut (scanMessage);
    } state error
  }

  state startMotion
  {
    when (moveState == 2)
    {
printf ("MOS wheel motion started\n");
    } state moving

    when (moveState == 3)
    {
      strcpy (scanMessage, "MOS wheel motion error");
      pvPut (scanMessage);
    } state error
  
    when (delay (60.0))
    {
      strcpy (scanMessage, "MOS wheel did not start");
      pvPut (scanMessage);
    } state error
  }
  
 
  state moving
  {
    when (moveState == 0)
    {
printf ("MOS wheel motion finished\n");
      startRead = 3;
      pvPut(startRead);
    } state startReading
      
    when (moveState == 3)
    {
      strcpy (scanMessage, "Motion error");
      pvPut (scanMessage);
    } state error
    
    when (delay (500.0))
    {
      strcpy (scanMessage, "Motion timeout");
      pvPut (scanMessage);
    } state error
  }
  
  state startReading
  {
    when (readState == 2)
    {
printf ("barcode scan started\n");
    } state reading
    
    when (readState == 3)
    {
      strcpy (scanMessage, "Barcode reading error");
      pvPut(scanMessage);
    } state error
    
    when (delay(500.0))
    {
      strcpy (scanMessage, "Barcode reading timeout");
      pvPut(scanMessage);
    } state error 
  }
    
  state reading
  {
    when (readState == 0)
    {
printf ("barcode scan finished\n");
      pvGet (masks);
      mask++;
      if (mask >= masks)
      {
        mask = 0;
      }
    } state scanning
    
    when (readState == 3)
    {
      strcpy (scanMessage, "Barcode reading error");
      pvPut(scanMessage);
    } state error
    
    when (delay(500.0))
    {
      strcpy (scanMessage, "Barcode reading timeout");
      pvPut(scanMessage);
    } state error
  }
  
  state error
  {
    when()
    {
      scanState = 3;
      pvPut(scanState);
    } state waitingForTrigger
  }
  
  state endScan
  {
    when()
    {
      scanState = 0;
      pvPut(scanState);
    } state waitingForTrigger
  }
}

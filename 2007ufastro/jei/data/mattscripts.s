~;Kisko's scripts
~test1
PUT  trecs:instrumentSetup.B imaging ;select item #, 0 based
PUT  trecs:observationSetup.A chop ;select item #, 0 based
~test add 
ADD_REC trecs:setDhsInfo.A&& in, STRING, TBD, Commanded DHS quick-look stream ID&&Custom List 2
ADD_REC trecs:setDhsInfo.VALA&& out, TBD, TBD, Current DHS quick-look stream ID&&Custom List 2
~;Matt's Scripts
~add is1
ADD_REC trecs:dataMode.A&& in, STRING, <save|discard>, Commanded data disposition mode&&Custom List 1
ADD_REC trecs:dataMode.VALA&& out, STRING, <save|discard>, Current data disposition&&Custom List 1
ADD_REC trecs:debug.A&& in, STRING, <NONE|MIN|FULL>, Commanded debuggin output level&&Custom List 1
ADD_REC trecs:debug.VALA&& out, STRING, <NONE|MIN|FULL>, Current debugging output level&&Custom List 1
ADD_REC trecs:init.A&& in, STRING, <NONE|VSM|FAST|FULL>, Commanded simulation mode&&Custom List 1
ADD_REC trecs:init.VALA&& out, STRING, <NONE|VSM|FAST|FULL>, Current simulation mode&&Custom List 1
ADD_REC trecs:observationSetup.A&& in, STRING, <chop-nod|stare|chop|nod>, Commanded observing mode&&Custom List 1
ADD_REC trecs:observationSetup.B&& in, STRING, TBD[TBD], Commnaded source photon collectin time&&Custom List 1
ADD_REC trecs:observationSetup.C&& in, STRING, <nominal|high>, Commanded flux background setting&&Custom List 1
ADD_REC trecs:observationSetup.D&& in, STRING, <nominal|high>, Commanded flux noise setting&&Custom List 1
ADD_REC trecs:observationSetup.VALA&& out, STRING, <chop-nod|stare|chop|nod>, Commanded observing mode&&Custom List 1
ADD_REC trecs:observationSetup.VALB&& out, STRING, TBD[TBD], Current source photon collecting time&&Custom List 1
ADD_REC trecs:observationSetup.VALC&& out, STRING, <nominal|high>, Current flux background setting&&Custom List 1
ADD_REC trecs:observationSetup.VALD&& out, STRING, <nominal|high>, Current flux noise setting&&Custom List 1
ADD_REC trecs:observe.A&& in, TBD, TBD, Commanded data label&&Custom List 1
ADD_REC trecs:observe.VALA&& out, TBD, TBD, Current data label&&Custom List 1
ADD_REC trecs:setDhsInfo.A&& in, STRING, TBD, Commanded DHS quick-look stream ID&&Custom List 1
ADD_REC trecs:setDhsInfo.VALA&& out, TBD, TBD, Current DHS quick-look stream ID&&Custom List 1
~add is2
ADD_REC trecs:instrumentSetup.A&& in, STRING, imaging|spectroscopy, Commanded camera mode&&Custom List 1
ADD_REC trecs:instrumentSetup.B&& in, STRING, <field|window|pupil>, Commanded imaging mode&&Custom List 1
ADD_REC trecs:instrumentSetup.C&& in, STRING, TBD, Commanded aperture name&&Custom List 1
ADD_REC trecs:instrumentSetup.D&& in, STRING, TBD, Commanded filter name&&Custom List 1
ADD_REC trecs:instrumentSetup.E&& in, STRING, loRes10|hiRes10|loRes20, Commanded grating name&&Custom List 1
ADD_REC trecs:instrumentSetup.F&& in, STRING, TBD, Commanded central wavelength&&Custom List 1
ADD_REC trecs:instrumentSetup.G&& in, STRING, TBD, Commanded lens name&&Custom List 1
ADD_REC trecs:instrumentSetup.H&& in, STRING, TBD, Commanded Lyot stop name&&Custom List 1
ADD_REC trecs:instrumentSetup.I&& in, STRING, TBD, Commanded sector name&&Custom List 1
ADD_REC trecs:instrumentSetup.J&& in, STRING, TBD, Commanded slit width&&Custom List 1
ADD_REC trecs:instrumentSetup.K&& in, STRING, TBD, Commanded window setting&&Custom List 1
ADD_REC trecs:instrumentSetup.L&& in, STRING, TRUE|FALSE, Override automatic aperture selection&&Custom List 1
ADD_REC trecs:instrumentSetup.M&& in, STRING, TRUE|FALSE, Override automatic filter selection&&Custom List 1
ADD_REC trecs:instrumentSetup.N&& in, STRING, TRUE|FALSE, Override automatic lens selection&&Custom List 1
ADD_REC trecs:instrumentSetup.O&& in, STRING, TRUE|FALSE, Override automatic window selection&&Custom List 1
ADD_REC trecs:instrumentSetup.VALA&& out, STRING, imaging|spectroscopy, Current camera mode&&Custom List 1
ADD_REC trecs:instrumentSetup.VALB&& out, STRING, <field|window|pupil>, Current imaging mode&&Custom List 1
ADD_REC trecs:instrumentSetup.VALC&& out, STRING, [0..1], Optical throughput&&Custom List 1
ADD_REC trecs:instrumentSetup.VALE&& out, STRING, TBD, Current filter name&&Custom List 1
ADD_REC trecs:instrumentSetup.VALF&& out, STRING, TBD, Current central wavelength&&Custom List 1
ADD_REC trecs:instrumentSetup.VALG&& out, STRING, TBD, Current aperture name&&Custom List 1
ADD_REC trecs:instrumentSetup.VALH&& out, STRING, TBD, Current cold clamp name&&Custom List 1
ADD_REC trecs:instrumentSetup.VALI&& out, STRING, TBD, Current filter 1 name&&Custom List 1
ADD_REC trecs:instrumentSetup.VALJ&& out, STRING, TBD, Current filter 2 name&&Custom List 1
ADD_REC trecs:instrumentSetup.VALK&& out, STRING, loRes10|hiRes10|loRes20, Current grating name&&Custom List 1
ADD_REC trecs:instrumentSetup.VALL&& out, STRING, TBD, Current Lyot stop name&&Custom List 1
ADD_REC trecs:instrumentSetup.VALM&& out, STRING, TBD, Current pupil name&&Custom List 1
ADD_REC trecs:instrumentSetup.VALN&& out, STRING, TBD, Current sector name&&Custom List 1
ADD_REC trecs:instrumentSetup.VALO&& out, STRING, TBD, Current slit width&&Custom List 1
ADD_REC trecs:instrumentSetup.VALP&& out, STRING, TBD, Current window setting&&Custom List 1
~add envCont1
ADD_REC trecs:envCont:tempMonG.A&& in, STRING, TBA, cold1 upper limit override&&Custom List 1
ADD_REC trecs:envCont:tempMonG.B&& in, STRING, TBA, cold2 upper limit override&&Custom List 1
ADD_REC trecs:envCont:tempMonG.C&& in, STRING, TBA, active upper limit override&&Custom List 1
ADD_REC trecs:envCont:tempMonG.D&& in, STRING, TBA, passive upper limit override&&Custom List 1
ADD_REC trecs:envCont:tempMonG.E&& in, STRING, TBA, window upper limit override&&Custom List 1
ADD_REC trecs:envCont:tempMonG.F&& in, STRING, TBA, strap upper limit override&&Custom List 1
ADD_REC trecs:envCont:tempMonG.G&& in, STRING, TBA, edge upper limit override&&Custom List 1
ADD_REC trecs:envCont:tempMonG.H&& in, STRING, TBA, middle upper limit override&&Custom List 1
ADD_REC trecs:envCont:tempMonG.J&& in, STRING, TBA, input array&&Custom List 1
ADD_REC trecs:envCont:tempMonG.VALA&& out, STRING, TBA, cold1&&Custom List 1
ADD_REC trecs:envCont:tempMonG.VALB&& out, STRING, TBA, cold2&&Custom List 1
ADD_REC trecs:envCont:tempMonG.VALC&& out, STRING, TBA, active&&Custom List 1
ADD_REC trecs:envCont:tempMonG.VALD&& out, STRING, TBA, passive&&Custom List 1
ADD_REC trecs:envCont:tempMonG.VALE&& out, STRING, TBA, window&&Custom List 1
ADD_REC trecs:envCont:tempMonG.VALF&& out, STRING, TBA, strap&&Custom List 1
ADD_REC trecs:envCont:tempMonG.VALG&& out, STRING, TBA, edge&&Custom List 1
ADD_REC trecs:envCont:tempMonG.VALH&& out, STRING, TBA, middle&&Custom List 1
ADD_REC trecs:envCont:tempMonG.VALI&& out, STRING, TRUE|FALSE, cold1 OK&&Custom List 1
ADD_REC trecs:envCont:tempMonG.VALJ&& out, STRING, TRUE|FALSE, cold2 OK&&Custom List 1
ADD_REC trecs:envCont:tempMonG.VALK&& out, STRING, TRUE|FALSE, active OK&&Custom List 1
ADD_REC trecs:envCont:tempMonG.VALL&& out, STRING, TRUE|FALSE, passive OK&&Custom List 1
ADD_REC trecs:envCont:tempMonG.VALM&& out, STRING, TRUE|FALSE, window OK&&Custom List 1
ADD_REC trecs:envCont:tempMonG.VALN&& out, STRING, TRUE|FALSE, strap OK&&Custom List 1
ADD_REC trecs:envCont:tempMonG.VALO&& out, STRING, TRUE|FALSE, edge OK&&Custom List 1
ADD_REC trecs:envCont:tempMonG.VALP&& out, STRING, TRUE|FALSE, middle OK&&Custom List 1
~add envCont2
ADD_REC trecs:envCont:pressMonG.A&& in, STRING, TBA, pressure upper&&Custom List 1
ADD_REC trecs:envCont:pressMonG.J&& in, STRING, TBD, input array&&Custom List 1
ADD_REC trecs:envCont:pressMonG.VALA&& out, STRING, TBA, cryostat pressure&&Custom List 1
ADD_REC trecs:envCont:pressMonG.VALB&& out, STRING, TBA, cryostat pressure OK to cool instrument&&Custom List 1
ADD_REC trecs:envCont:arrayG.A&& in, STRING, n/a, set point(VALC from tempSetG)&&Custom List 1
ADD_REC trecs:envCont:arrayG.B&& in, STRING, TBA, array upper&&Custom List 1
ADD_REC trecs:envCont:arrayG.C&& in, STRING, TBA, array lower&&Custom List 1
ADD_REC trecs:envCont:arrayG.D&& in, STRING, TBA, finger upper&&Custom List 1
ADD_REC trecs:envCont:arrayG.VALA&& out, STRING, TBA, Detector (current)(to sad:tSDetector.VAL)&&Custom List 1
ADD_REC trecs:envCont:arrayG.VALB&& out, STRING, TBA, finger(to sad:tSFinger.VAL)&&Custom List 1
ADD_REC trecs:envCont:arrayG.VALC&& out, STRING, TBA, Detector OK(to sad:DetectorOK.VAL)&&Custom List 1
ADD_REC trecs:envCont:arrayG.VALD&& out, STRING, TBA, finger OK(to sad:FingerOK.VAL)&&Custom List 1
ADD_REC trecs:envCont:configG.VALA&& out, STRING, n/a, status gensubs&&Custom List 1
ADD_REC trecs:envCont:configG.VALB&& out, STRING, n/a, status update&&Custom List 1
ADD_REC trecs:envCont:configG.VALC&& out, STRING, n/a, heartbeat records&&Custom List 1
~add envCont3
ADD_REC trecs:envCont:tempSetG.A&& in, LONG, n/a, command mode(OUT from envCont:simLevelWrite)&&Custom List 1
ADD_REC trecs:envCont:tempSetG.B&& in, DOUBLE, n/a, set point(OUT from envCont:setPointWrite)&&Custom List 1
ADD_REC trecs:envCont:tempSetG.C&& in, LONG, n/a, heater power(OUT from envCont:powerWrite)&&Custom List 1
ADD_REC trecs:envCont:tempSetG.D&& in, DOUBLE, n/a, P value&&Custom List 1
ADD_REC trecs:envCont:tempSetG.E&& in, DOUBLE, n/a, I value&&Custom List 1
ADD_REC trecs:envCont:tempSetG.F&& in, DOUBLE, n/a, D value&&Custom List 1
ADD_REC trecs:envCont:tempSetG.G&& in, LONG, n/a, auto tune&&Custom List 1
ADD_REC trecs:envCont:tempSetG.H&& in, STRING, n/a, n/a(from envCont:debug.OUT)&&Custom List 1
ADD_REC trecs:envCont:tempSetG.SLNK&& in, STRING, n/a, n/a(FLNK from envCont:apply)&&Custom List 1
ADD_REC trecs:envCont:tempSetG.VALA&& out, STRING, n/a, command mode&&Custom List 1
ADD_REC trecs:envCont:tempSetG.VALB&& out, STRING, n/a, socket number&&Custom List 1
ADD_REC trecs:envCont:tempSetG.VALC&& out, STRING, n/a, set point(INPA to envCont:arrayG&&Custom List 1
ADD_REC trecs:envCont:tempSetG.VALD&& out, STRING, n/a, heater power&&Custom List 1
ADD_REC trecs:envCont:tempSetG.VALE&& out, STRING, n/a, P value&&Custom List 1
ADD_REC trecs:envCont:tempSetG.VALF&& out, STRING, n/a, I value&&Custom List 1
ADD_REC trecs:envCont:tempSetG.VALG&& out, STRING, n/a, D value&&Custom List 1
ADD_REC trecs:envCont:tempSetG.VALH&& out, STRING, n/a, auto tune&&Custom List 1
ADD_REC trecs:envCont:apply.A&& in, STRING, n/a, n/a(from envCont:init.VAL)&&Custom List 1
ADD_REC trecs:envCont:apply.B&& in, STRING, n/a, n/a(from envCont:tempSet.VAL)&&Custom List 1
ADD_REC trecs:envCont:apply.FLNK&& out, STRING, n/a, n/a(to envCont:tempSetG.SLNK)&&Custom List 1
ADD_REC trecs:envCont:apply.VALA&& out, STRING, n/a, n/a(to envCont:init.DIR)&&Custom List 1
ADD_REC trecs:envCont:apply.VALB&& out, STRING, n/a, n/a(to envCont:tempSet.DIR)&&Custom List 1
~add envCont4
ADD_REC trecs:envCont:init.VALA&& out, STRING, n/a, n/a(to envCont:heartbeatCalc.INPE)&&Custom List 1
ADD_REC trecs:envCont:init.VALB&& out, STRING, n/a, n/a(to envCont:simLevelWrite.DOL)&&Custom List 1
ADD_REC trecs:envCont:tempSet.A&& in, STRING, n/a, set point&&Custom List 1
ADD_REC trecs:envCont:tempSet.B&& in, STRING, n/a, heater power&&Custom List 1
ADD_REC trecs:envCont:tempSet.VALA&& out, STRING, n/a, n/a(to envCont:setPointWrite.DOL)&&Custom List 1
ADD_REC trecs:envCont:tempSet.VALB&& out, STRING, n/a, n/a(to envCont:powerWrite.DOL)&&Custom List 1
ADD_REC trecs:envCont:tempSet.VALG&& out, STRING, n/a, set point&&Custom List 1
ADD_REC trecs:envCont:tempSet.VALH&& out, STRING, n/a, heater power&&Custom List 1
ADD_REC trecs:envCont:applyC.IVAL&& in, STRING, TBD, TBD&&Custom List 1
ADD_REC trecs:envCont:applyC.IMSS&& in, STRING, TBD, TBD&&Custom List 1
ADD_REC trecs:envCont:applyC.VAL&& out, STRING, TBD, TBD&&Custom List 1
ADD_REC trecs:envCont:applyC.OMSS&& out, STRING, TBD, TBD&&Custom List 1
ADD_REC trecs:envCont:debug.DOL&& in, STRING, TBD, TBD&&Custom List 1
ADD_REC trecs:envCont:debug.OUT&& out, STRING, TBD, TBD&&Custom List 1
ADD_REC trecs:envCont:ls340Heartbeat.INP&& in, STRING, n/a, n/a&&Custom List 1
ADD_REC trecs:envCont:ls340Heartbeat.VAL&& out, STRING, n/a, n/a(to envCont:heartbeat.SLNK)&&Custom List 1
ADD_REC trecs:envCont:ls218Heartbeat.INP&& in, STRING, n/a, n/a&&Custom List 1
ADD_REC trecs:envCont:ls218Heartbeat.VAL&& out, STRING, n/a, n/a(to envCont:heartbeat.SLNK)&&Custom List 1
ADD_REC trecs:envCont:ls354Heartbeat.INP&& in, STRING, n/a, n/a&&Custom List 1
ADD_REC trecs:envCont:ls354Heartbeat.VAL&& out, STRING, n/a, n/a(to envCont:heartbeat.SLNK)&&Custom List 1
ADD_REC trecs:envCont:heartbeat.SLNK&& in, STRING, n/a, n/a(from the 3 longins above)&&Custom List 1
ADD_REC trecs:envCont:heartbeat.OUT&& out, STRING, n/a, n/a(to sad:ecHeartbeat.VAL&&Custom List 1
~add motor Sector Wheel 1
ADD_REC trecs:cc:sectWhl:motorApply.A&& in, STRING, n/a, n/a(from cc:sectWhl:test.VAL)&&Sector Wheel 1
ADD_REC trecs:cc:sectWhl:motorApply.B&& in, STRING, n/a, n/a(from cc:sectWhl:init.VAL)&&Sector Wheel 1
ADD_REC trecs:cc:sectWhl:motorApply.C&& in, STRING, n/a, n/a(from cc:sectWhl:datum.VAL)&&Sector Wheel 1
ADD_REC trecs:cc:sectWhl:motorApply.D&& in, STRING, n/a, n/a(from cc:sectWhl:namedPos.VAL)&&Sector Wheel 1
ADD_REC trecs:cc:sectWhl:motorApply.E&& in, STRING, n/a, n/a(from cc:sectWhl:steps.VAL)&&Sector Wheel 1
ADD_REC trecs:cc:sectWhl:motorApply.F&& in, STRING, n/a, n/a(from cc:sectWhl:abort.VAL)&&Sector Wheel 1
ADD_REC trecs:cc:sectWhl:motorApply.G&& in, STRING, n/a, n/a(from cc:sectWhl:stop.VAL)&&Sector Wheel 1
ADD_REC trecs:cc:sectWhl:motorApply.VALA&& out, STRING, n/a, n/a(to cc:sectWhl:test.DIR)&&Sector Wheel 1
ADD_REC trecs:cc:sectWhl:motorApply.VALB&& out, STRING, n/a, n/a(to cc:sectWhl:init.DIR)&&Sector Wheel 1
ADD_REC trecs:cc:sectWhl:motorApply.VALC&& out, STRING, n/a, n/a(to cc:sectWhl:datum.DIR)&&Sector Wheel 1
ADD_REC trecs:cc:sectWhl:motorApply.VALD&& out, STRING, n/a, n/a(to cc:sectWhl:namedPos.DIR)&&Sector Wheel 1
ADD_REC trecs:cc:sectWhl:motorApply.VALE&& out, STRING, n/a, n/a(to cc:sectWhl:steps.DIR)&&Sector Wheel 1
ADD_REC trecs:cc:sectWhl:motorApply.VALF&& out, STRING, n/a, n/a(to cc:sectWhl:abort.DIR)&&Sector Wheel 1
ADD_REC trecs:cc:sectWhl:motorApply.VALG&& out, STRING, n/a, n/a(to cc:sectWhl:stop.DIR)&&Sector Wheel 1
ADD_REC trecs:cc:sectWhl:test.A&& in, STRING, n/a, test directive&&Sector Wheel 1
ADD_REC trecs:cc:sectWhl:test.VALA&& out, LONG, n/a, n/a(internal)&&Sector Wheel 1
ADD_REC trecs:cc:sectWhl:init.A&& in, STRING, n/a, init directive&&Sector Wheel 1
ADD_REC trecs:cc:sectWhl:init.VALA&& out, LONG, n/a, n/a&&Sector Wheel 1
ADD_REC trecs:cc:sectWhl:datum.A&& in, STRING, n/a, datum directive&&Sector Wheel 1
ADD_REC trecs:cc:sectWhl:datum.VALA&& out, LONG, n/a, n/a&&Sector Wheel 1
~add motor Sector Wheel 2
ADD_REC trecs:cc:sectWhl:namedPos.A&& in, STRING, n/a, Position number&&Sector Wheel 2
ADD_REC trecs:cc:sectWhl:namedPos.VALA&& out, LONG, n/a, n/a&&Sector Wheel 2
ADD_REC trecs:cc:sectWhl:namedPos.VALB&& out, DOUBLE, n/a, n/a&&Sector Wheel 2
ADD_REC trecs:cc:sectWhl:namedPos.VALC&& out, TBD, n/a, Position number(to sad:sectWhl:Pos.VAL)&&Sector Wheel 2
ADD_REC trecs:cc:sectWhl:stop.A&& in, STRING, n/a, stop message&&Sector Wheel 2
ADD_REC trecs:cc:sectWhl:stop.VALA&& out, STRING, n/a, n/a&&Sector Wheel 2
ADD_REC trecs:cc:sectWhl:abort.A&& in, STRING, n/a, abort message&&Sector Wheel 2
ADD_REC trecs:cc:sectWhl:abort.VALA&& out, STRING, n/a, n/a&&Sector Wheel 2
ADD_REC trecs:cc:sectWhl:steps.A&& in, STRING, n/a, steps&&Sector Wheel 2
ADD_REC trecs:cc:sectWhl:steps.VALA&& out, DOUBLE, n/a, n/a&&Sector Wheel 2
ADD_REC trecs:cc:sectWhl:debug.DOL&& in, STRING, n/a, n/a&&Sector Wheel 2
ADD_REC trecs:cc:sectWhl:debug.OUT&& out, STRING, n/a, n/a&&Sector Wheel 2
ADD_REC trecs:cc:sectWhl:motorC.IVAL&& in, STRING, n/a, n/a&&Sector Wheel 2
ADD_REC trecs:cc:sectWhl:motorC.IMSS&& in, STRING, n/a, n/a&&Sector Wheel 2
ADD_REC trecs:cc:sectWhl:motorC.VAL&& out, STRING, n/a, n/a&&Sector Wheel 2
ADD_REC trecs:cc:sectWhl:motorC.OMSS&& out, STRING, n/a, n/a&&Sector Wheel 2
~add motor Sector Wheel 3
ADD_REC trecs:cc:sectWhl:motorG.A&& in, LONG, n/a, perform_test&&Sector Wheel 3
ADD_REC trecs:cc:sectWhl:motorG.C&& in, LONG, n/a, command mode&&Sector Wheel 3
ADD_REC trecs:cc:sectWhl:motorG.E&& in, LONG, n/a, datum_motor&&Sector Wheel 3
ADD_REC trecs:cc:sectWhl:motorG.G&& in, DOUBLE, n/a, num_steps&&Sector Wheel 3
ADD_REC trecs:cc:sectWhl:motorG.I&& in, STRING, n/a, stop_mess&&Sector Wheel 3
ADD_REC trecs:cc:sectWhl:motorG.K&& in, STRING, n/a, abort_mess&&Sector Wheel 3
ADD_REC trecs:cc:sectWhl:motorG.M&& in, STRING, n/a, debug mode&&Sector Wheel 3
ADD_REC trecs:cc:sectWhl:motorG.N&& in, DOUBLE, n/a, init_velocity&&Sector Wheel 3
ADD_REC trecs:cc:sectWhl:motorG.O&& in, DOUBLE, n/a, slew_velocity&&Sector Wheel 3
ADD_REC trecs:cc:sectWhl:motorG.P&& in, DOUBLE, n/a, acceleration&&Sector Wheel 3
ADD_REC trecs:cc:sectWhl:motorG.Q&& in, DOUBLE, n/a, deceleration&&Sector Wheel 3
ADD_REC trecs:cc:sectWhl:motorG.R&& in, DOUBLE, n/a, drive_current&&Sector Wheel 3
ADD_REC trecs:cc:sectWhl:motorG.S&& in, DOUBLE, n/a, datum_speed&&Sector Wheel 3
ADD_REC trecs:cc:sectWhl:motorG.T&& in, LONG, n/a, datum_direction&&Sector Wheel 3
ADD_REC trecs:cc:sectWhl:motorG.VALA&& out, LONG, n/a, command mode&&Sector Wheel 3
ADD_REC trecs:cc:sectWhl:motorG.VALB&& out, LONG, n/a, socket number&&Sector Wheel 3
ADD_REC trecs:cc:sectWhl:motorG.VALC&& out, STRING, n/a, current position&&Sector Wheel 3
ADD_REC trecs:cc:sectWhl:motorG.VALD&& out, DOUBLE, n/a, init_velocity&&Sector Wheel 3
ADD_REC trecs:cc:sectWhl:motorG.VALE&& out, DOUBLE, n/a, slew_velocity&&Sector Wheel 3
ADD_REC trecs:cc:sectWhl:motorG.VALF&& out, DOUBLE, n/a, acceleration&&Sector Wheel 3
ADD_REC trecs:cc:sectWhl:motorG.VALG&& out, DOUBLE, n/a, deceleration&&Sector Wheel 3
ADD_REC trecs:cc:sectWhl:motorG.VALH&& out, DOUBLE, n/a, drive_current&&Sector Wheel 3
ADD_REC trecs:cc:sectWhl:motorG.VALI&& out, DOUBLE, n/a, datum_speed&&Sector Wheel 3
ADD_REC trecs:cc:sectWhl:motorG.VALJ&& out, LONG, n/a, datum_direction&&Sector Wheel 3
~add motor Window Changer 1
ADD_REC trecs:cc:winChngr:motorApply.A&& in, STRING, n/a, n/a(from cc:winChngr:test.VAL)&&Window Changer 1
ADD_REC trecs:cc:winChngr:motorApply.B&& in, STRING, n/a, n/a(from cc:winChngr:init.VAL)&&Window Changer 1
ADD_REC trecs:cc:winChngr:motorApply.C&& in, STRING, n/a, n/a(from cc:winChngr:datum.VAL)&&Window Changer 1
ADD_REC trecs:cc:winChngr:motorApply.D&& in, STRING, n/a, n/a(from cc:winChngr:namedPos.VAL)&&Window Changer 1
ADD_REC trecs:cc:winChngr:motorApply.E&& in, STRING, n/a, n/a(from cc:winChngr:steps.VAL)&&Window Changer 1
ADD_REC trecs:cc:winChngr:motorApply.F&& in, STRING, n/a, n/a(from cc:winChngr:abort.VAL)&&Window Changer 1
ADD_REC trecs:cc:winChngr:motorApply.G&& in, STRING, n/a, n/a(from cc:winChngr:stop.VAL)&&Window Changer 1
ADD_REC trecs:cc:winChngr:motorApply.VALA&& out, STRING, n/a, n/a(to cc:winChngr:test.DIR)&&Window Changer 1
ADD_REC trecs:cc:winChngr:motorApply.VALB&& out, STRING, n/a, n/a(to cc:winChngr:init.DIR)&&Window Changer 1
ADD_REC trecs:cc:winChngr:motorApply.VALC&& out, STRING, n/a, n/a(to cc:winChngr:datum.DIR)&&Window Changer 1
ADD_REC trecs:cc:winChngr:motorApply.VALD&& out, STRING, n/a, n/a(to cc:winChngr:namedPos.DIR)&&Window Changer 1
ADD_REC trecs:cc:winChngr:motorApply.VALE&& out, STRING, n/a, n/a(to cc:winChngr:steps.DIR)&&Window Changer 1
ADD_REC trecs:cc:winChngr:motorApply.VALF&& out, STRING, n/a, n/a(to cc:winChngr:abort.DIR)&&Window Changer 1
ADD_REC trecs:cc:winChngr:motorApply.VALG&& out, STRING, n/a, n/a(to cc:winChngr:stop.DIR)&&Window Changer 1
ADD_REC trecs:cc:winChngr:test.A&& in, STRING, n/a, test directive&&Window Changer 1
ADD_REC trecs:cc:winChngr:test.VALA&& out, LONG, n/a, n/a(internal)&&Window Changer 1
ADD_REC trecs:cc:winChngr:init.A&& in, STRING, n/a, init directive&&Window Changer 1
ADD_REC trecs:cc:winChngr:init.VALA&& out, LONG, n/a, n/a&&Window Changer 1
ADD_REC trecs:cc:winChngr:datum.A&& in, STRING, n/a, datum directive&&Window Changer 1
ADD_REC trecs:cc:winChngr:datum.VALA&& out, LONG, n/a, n/a&&Window Changer 1
~add motor Window Changer 2
ADD_REC trecs:cc:winChngr:namedPos.A&& in, STRING, n/a, Position number&&Window Changer 2
ADD_REC trecs:cc:winChngr:namedPos.VALA&& out, LONG, n/a, n/a&&Window Changer 2
ADD_REC trecs:cc:winChngr:namedPos.VALB&& out, DOUBLE, n/a, n/a&&Window Changer 2
ADD_REC trecs:cc:winChngr:namedPos.VALC&& out, TBD, n/a, Position number(to sad:winChngr:Pos.VAL)&&Window Changer 2
ADD_REC trecs:cc:winChngr:stop.A&& in, STRING, n/a, stop message&&Window Changer 2
ADD_REC trecs:cc:winChngr:stop.VALA&& out, STRING, n/a, n/a&&Window Changer 2
ADD_REC trecs:cc:winChngr:abort.A&& in, STRING, n/a, abort message&&Window Changer 2
ADD_REC trecs:cc:winChngr:abort.VALA&& out, STRING, n/a, n/a&&Window Changer 2
ADD_REC trecs:cc:winChngr:steps.A&& in, STRING, n/a, steps&&Window Changer 2
ADD_REC trecs:cc:winChngr:steps.VALA&& out, DOUBLE, n/a, n/a&&Window Changer 2
ADD_REC trecs:cc:winChngr:debug.DOL&& in, STRING, n/a, n/a&&Window Changer 2
ADD_REC trecs:cc:winChngr:debug.OUT&& out, STRING, n/a, n/a&&Window Changer 2
ADD_REC trecs:cc:winChngr:motorC.IVAL&& in, STRING, n/a, n/a&&Window Changer 2
ADD_REC trecs:cc:winChngr:motorC.IMSS&& in, STRING, n/a, n/a&&Window Changer 2
ADD_REC trecs:cc:winChngr:motorC.VAL&& out, STRING, n/a, n/a&&Window Changer 2
ADD_REC trecs:cc:winChngr:motorC.OMSS&& out, STRING, n/a, n/a&&Window Changer 2
~add motor Window Changer 3
ADD_REC trecs:cc:winChngr:motorG.A&& in, LONG, n/a, perform_test&&Window Changer 3
ADD_REC trecs:cc:winChngr:motorG.C&& in, LONG, n/a, command mode&&Window Changer 3
ADD_REC trecs:cc:winChngr:motorG.E&& in, LONG, n/a, datum_motor&&Window Changer 3
ADD_REC trecs:cc:winChngr:motorG.G&& in, DOUBLE, n/a, num_steps&&Window Changer 3
ADD_REC trecs:cc:winChngr:motorG.I&& in, STRING, n/a, stop_mess&&Window Changer 3
ADD_REC trecs:cc:winChngr:motorG.K&& in, STRING, n/a, abort_mess&&Window Changer 3
ADD_REC trecs:cc:winChngr:motorG.M&& in, STRING, n/a, debug mode&&Window Changer 3
ADD_REC trecs:cc:winChngr:motorG.N&& in, DOUBLE, n/a, init_velocity&&Window Changer 3
ADD_REC trecs:cc:winChngr:motorG.O&& in, DOUBLE, n/a, slew_velocity&&Window Changer 3
ADD_REC trecs:cc:winChngr:motorG.P&& in, DOUBLE, n/a, acceleration&&Window Changer 3
ADD_REC trecs:cc:winChngr:motorG.Q&& in, DOUBLE, n/a, deceleration&&Window Changer 3
ADD_REC trecs:cc:winChngr:motorG.R&& in, DOUBLE, n/a, drive_current&&Window Changer 3
ADD_REC trecs:cc:winChngr:motorG.S&& in, DOUBLE, n/a, datum_speed&&Window Changer 3
ADD_REC trecs:cc:winChngr:motorG.T&& in, LONG, n/a, datum_direction&&Window Changer 3
ADD_REC trecs:cc:winChngr:motorG.VALA&& out, LONG, n/a, command mode&&Window Changer 3
ADD_REC trecs:cc:winChngr:motorG.VALB&& out, LONG, n/a, socket number&&Window Changer 3
ADD_REC trecs:cc:winChngr:motorG.VALC&& out, STRING, n/a, current position&&Window Changer 3
ADD_REC trecs:cc:winChngr:motorG.VALD&& out, DOUBLE, n/a, init_velocity&&Window Changer 3
ADD_REC trecs:cc:winChngr:motorG.VALE&& out, DOUBLE, n/a, slew_velocity&&Window Changer 3
ADD_REC trecs:cc:winChngr:motorG.VALF&& out, DOUBLE, n/a, acceleration&&Window Changer 3
ADD_REC trecs:cc:winChngr:motorG.VALG&& out, DOUBLE, n/a, deceleration&&Window Changer 3
ADD_REC trecs:cc:winChngr:motorG.VALH&& out, DOUBLE, n/a, drive_current&&Window Changer 3
ADD_REC trecs:cc:winChngr:motorG.VALI&& out, DOUBLE, n/a, datum_speed&&Window Changer 3
ADD_REC trecs:cc:winChngr:motorG.VALJ&& out, LONG, n/a, datum_direction&&Window Changer 3
~add motor Aperture Wheel 1
ADD_REC trecs:cc:aprtrWhl:motorApply.A&& in, STRING, n/a, n/a(from cc:aprtrWhl:test.VAL)&&Aperture Wheel 1
ADD_REC trecs:cc:aprtrWhl:motorApply.B&& in, STRING, n/a, n/a(from cc:aprtrWhl:init.VAL)&&Aperture Wheel 1
ADD_REC trecs:cc:aprtrWhl:motorApply.C&& in, STRING, n/a, n/a(from cc:aprtrWhl:datum.VAL)&&Aperture Wheel 1
ADD_REC trecs:cc:aprtrWhl:motorApply.D&& in, STRING, n/a, n/a(from cc:aprtrWhl:namedPos.VAL)&&Aperture Wheel 1
ADD_REC trecs:cc:aprtrWhl:motorApply.E&& in, STRING, n/a, n/a(from cc:aprtrWhl:steps.VAL)&&Aperture Wheel 1
ADD_REC trecs:cc:aprtrWhl:motorApply.F&& in, STRING, n/a, n/a(from cc:aprtrWhl:abort.VAL)&&Aperture Wheel 1
ADD_REC trecs:cc:aprtrWhl:motorApply.G&& in, STRING, n/a, n/a(from cc:aprtrWhl:stop.VAL)&&Aperture Wheel 1
ADD_REC trecs:cc:aprtrWhl:motorApply.VALA&& out, STRING, n/a, n/a(to cc:aprtrWhl:test.DIR)&&Aperture Wheel 1
ADD_REC trecs:cc:aprtrWhl:motorApply.VALB&& out, STRING, n/a, n/a(to cc:aprtrWhl:init.DIR)&&Aperture Wheel 1
ADD_REC trecs:cc:aprtrWhl:motorApply.VALC&& out, STRING, n/a, n/a(to cc:aprtrWhl:datum.DIR)&&Aperture Wheel 1
ADD_REC trecs:cc:aprtrWhl:motorApply.VALD&& out, STRING, n/a, n/a(to cc:aprtrWhl:namedPos.DIR)&&Aperture Wheel 1
ADD_REC trecs:cc:aprtrWhl:motorApply.VALE&& out, STRING, n/a, n/a(to cc:aprtrWhl:steps.DIR)&&Aperture Wheel 1
ADD_REC trecs:cc:aprtrWhl:motorApply.VALF&& out, STRING, n/a, n/a(to cc:aprtrWhl:abort.DIR)&&Aperture Wheel 1
ADD_REC trecs:cc:aprtrWhl:motorApply.VALG&& out, STRING, n/a, n/a(to cc:aprtrWhl:stop.DIR)&&Aperture Wheel 1
ADD_REC trecs:cc:aprtrWhl:test.A&& in, STRING, n/a, test directive&&Aperture Wheel 1
ADD_REC trecs:cc:aprtrWhl:test.VALA&& out, LONG, n/a, n/a(internal)&&Aperture Wheel 1
ADD_REC trecs:cc:aprtrWhl:init.A&& in, STRING, n/a, init directive&&Aperture Wheel 1
ADD_REC trecs:cc:aprtrWhl:init.VALA&& out, LONG, n/a, n/a&&Aperture Wheel 1
ADD_REC trecs:cc:aprtrWhl:datum.A&& in, STRING, n/a, datum directive&&Aperture Wheel 1
ADD_REC trecs:cc:aprtrWhl:datum.VALA&& out, LONG, n/a, n/a&&Aperture Wheel 1
~add motor Aperture Wheel 2
ADD_REC trecs:cc:aprtrWhl:namedPos.A&& in, STRING, n/a, Position number&&Aperture Wheel 2
ADD_REC trecs:cc:aprtrWhl:namedPos.VALA&& out, LONG, n/a, n/a&&Aperture Wheel 2
ADD_REC trecs:cc:aprtrWhl:namedPos.VALB&& out, DOUBLE, n/a, n/a&&Aperture Wheel 2
ADD_REC trecs:cc:aprtrWhl:namedPos.VALC&& out, TBD, n/a, Position number(to sad:aprtrWhl:Pos.VAL)&&Aperture Wheel 2
ADD_REC trecs:cc:aprtrWhl:stop.A&& in, STRING, n/a, stop message&&Aperture Wheel 2
ADD_REC trecs:cc:aprtrWhl:stop.VALA&& out, STRING, n/a, n/a&&Aperture Wheel 2
ADD_REC trecs:cc:aprtrWhl:abort.A&& in, STRING, n/a, abort message&&Aperture Wheel 2
ADD_REC trecs:cc:aprtrWhl:abort.VALA&& out, STRING, n/a, n/a&&Aperture Wheel 2
ADD_REC trecs:cc:aprtrWhl:steps.A&& in, STRING, n/a, steps&&Aperture Wheel 2
ADD_REC trecs:cc:aprtrWhl:steps.VALA&& out, DOUBLE, n/a, n/a&&Aperture Wheel 2
ADD_REC trecs:cc:aprtrWhl:debug.DOL&& in, STRING, n/a, n/a&&Aperture Wheel 2
ADD_REC trecs:cc:aprtrWhl:debug.OUT&& out, STRING, n/a, n/a&&Aperture Wheel 2
ADD_REC trecs:cc:aprtrWhl:motorC.IVAL&& in, STRING, n/a, n/a&&Aperture Wheel 2
ADD_REC trecs:cc:aprtrWhl:motorC.IMSS&& in, STRING, n/a, n/a&&Aperture Wheel 2
ADD_REC trecs:cc:aprtrWhl:motorC.VAL&& out, STRING, n/a, n/a&&Aperture Wheel 2
ADD_REC trecs:cc:aprtrWhl:motorC.OMSS&& out, STRING, n/a, n/a&&Aperture Wheel 2
~add motor Aperture Wheel 3
ADD_REC trecs:cc:aprtrWhl:motorG.A&& in, LONG, n/a, perform_test&&Aperture Wheel 3
ADD_REC trecs:cc:aprtrWhl:motorG.C&& in, LONG, n/a, command mode&&Aperture Wheel 3
ADD_REC trecs:cc:aprtrWhl:motorG.E&& in, LONG, n/a, datum_motor&&Aperture Wheel 3
ADD_REC trecs:cc:aprtrWhl:motorG.G&& in, DOUBLE, n/a, num_steps&&Aperture Wheel 3
ADD_REC trecs:cc:aprtrWhl:motorG.I&& in, STRING, n/a, stop_mess&&Aperture Wheel 3
ADD_REC trecs:cc:aprtrWhl:motorG.K&& in, STRING, n/a, abort_mess&&Aperture Wheel 3
ADD_REC trecs:cc:aprtrWhl:motorG.M&& in, STRING, n/a, debug mode&&Aperture Wheel 3
ADD_REC trecs:cc:aprtrWhl:motorG.N&& in, DOUBLE, n/a, init_velocity&&Aperture Wheel 3
ADD_REC trecs:cc:aprtrWhl:motorG.O&& in, DOUBLE, n/a, slew_velocity&&Aperture Wheel 3
ADD_REC trecs:cc:aprtrWhl:motorG.P&& in, DOUBLE, n/a, acceleration&&Aperture Wheel 3
ADD_REC trecs:cc:aprtrWhl:motorG.Q&& in, DOUBLE, n/a, deceleration&&Aperture Wheel 3
ADD_REC trecs:cc:aprtrWhl:motorG.R&& in, DOUBLE, n/a, drive_current&&Aperture Wheel 3
ADD_REC trecs:cc:aprtrWhl:motorG.S&& in, DOUBLE, n/a, datum_speed&&Aperture Wheel 3
ADD_REC trecs:cc:aprtrWhl:motorG.T&& in, LONG, n/a, datum_direction&&Aperture Wheel 3
ADD_REC trecs:cc:aprtrWhl:motorG.VALA&& out, LONG, n/a, command mode&&Aperture Wheel 3
ADD_REC trecs:cc:aprtrWhl:motorG.VALB&& out, LONG, n/a, socket number&&Aperture Wheel 3
ADD_REC trecs:cc:aprtrWhl:motorG.VALC&& out, STRING, n/a, current position&&Aperture Wheel 3
ADD_REC trecs:cc:aprtrWhl:motorG.VALD&& out, DOUBLE, n/a, init_velocity&&Aperture Wheel 3
ADD_REC trecs:cc:aprtrWhl:motorG.VALE&& out, DOUBLE, n/a, slew_velocity&&Aperture Wheel 3
ADD_REC trecs:cc:aprtrWhl:motorG.VALF&& out, DOUBLE, n/a, acceleration&&Aperture Wheel 3
ADD_REC trecs:cc:aprtrWhl:motorG.VALG&& out, DOUBLE, n/a, deceleration&&Aperture Wheel 3
ADD_REC trecs:cc:aprtrWhl:motorG.VALH&& out, DOUBLE, n/a, drive_current&&Aperture Wheel 3
ADD_REC trecs:cc:aprtrWhl:motorG.VALI&& out, DOUBLE, n/a, datum_speed&&Aperture Wheel 3
ADD_REC trecs:cc:aprtrWhl:motorG.VALJ&& out, LONG, n/a, datum_direction&&Aperture Wheel 3
~add motor Filter Wheel 1 1
ADD_REC trecs:cc:fltrWhl1:motorApply.A&& in, STRING, n/a, n/a(from cc:fltrWhl1:test.VAL)&&Filter Wheel 1 1
ADD_REC trecs:cc:fltrWhl1:motorApply.B&& in, STRING, n/a, n/a(from cc:fltrWhl1:init.VAL)&&Filter Wheel 1 1
ADD_REC trecs:cc:fltrWhl1:motorApply.C&& in, STRING, n/a, n/a(from cc:fltrWhl1:datum.VAL)&&Filter Wheel 1 1
ADD_REC trecs:cc:fltrWhl1:motorApply.D&& in, STRING, n/a, n/a(from cc:fltrWhl1:namedPos.VAL)&&Filter Wheel 1 1
ADD_REC trecs:cc:fltrWhl1:motorApply.E&& in, STRING, n/a, n/a(from cc:fltrWhl1:steps.VAL)&&Filter Wheel 1 1
ADD_REC trecs:cc:fltrWhl1:motorApply.F&& in, STRING, n/a, n/a(from cc:fltrWhl1:abort.VAL)&&Filter Wheel 1 1
ADD_REC trecs:cc:fltrWhl1:motorApply.G&& in, STRING, n/a, n/a(from cc:fltrWhl1:stop.VAL)&&Filter Wheel 1 1
ADD_REC trecs:cc:fltrWhl1:motorApply.VALA&& out, STRING, n/a, n/a(to cc:fltrWhl1:test.DIR)&&Filter Wheel 1 1
ADD_REC trecs:cc:fltrWhl1:motorApply.VALB&& out, STRING, n/a, n/a(to cc:fltrWhl1:init.DIR)&&Filter Wheel 1 1
ADD_REC trecs:cc:fltrWhl1:motorApply.VALC&& out, STRING, n/a, n/a(to cc:fltrWhl1:datum.DIR)&&Filter Wheel 1 1
ADD_REC trecs:cc:fltrWhl1:motorApply.VALD&& out, STRING, n/a, n/a(to cc:fltrWhl1:namedPos.DIR)&&Filter Wheel 1 1
ADD_REC trecs:cc:fltrWhl1:motorApply.VALE&& out, STRING, n/a, n/a(to cc:fltrWhl1:steps.DIR)&&Filter Wheel 1 1
ADD_REC trecs:cc:fltrWhl1:motorApply.VALF&& out, STRING, n/a, n/a(to cc:fltrWhl1:abort.DIR)&&Filter Wheel 1 1
ADD_REC trecs:cc:fltrWhl1:motorApply.VALG&& out, STRING, n/a, n/a(to cc:fltrWhl1:stop.DIR)&&Filter Wheel 1 1
ADD_REC trecs:cc:fltrWhl1:test.A&& in, STRING, n/a, test directive&&Filter Wheel 1 1
ADD_REC trecs:cc:fltrWhl1:test.VALA&& out, LONG, n/a, n/a(internal)&&Filter Wheel 1 1
ADD_REC trecs:cc:fltrWhl1:init.A&& in, STRING, n/a, init directive&&Filter Wheel 1 1
ADD_REC trecs:cc:fltrWhl1:init.VALA&& out, LONG, n/a, n/a&&Filter Wheel 1 1
ADD_REC trecs:cc:fltrWhl1:datum.A&& in, STRING, n/a, datum directive&&Filter Wheel 1 1
ADD_REC trecs:cc:fltrWhl1:datum.VALA&& out, LONG, n/a, n/a&&Filter Wheel 1 1
~add motor Filter Wheel 1 2
ADD_REC trecs:cc:fltrWhl1:namedPos.A&& in, STRING, n/a, Position number&&Filter Wheel 1 2
ADD_REC trecs:cc:fltrWhl1:namedPos.VALA&& out, LONG, n/a, n/a&&Filter Wheel 1 2
ADD_REC trecs:cc:fltrWhl1:namedPos.VALB&& out, DOUBLE, n/a, n/a&&Filter Wheel 1 2
ADD_REC trecs:cc:fltrWhl1:namedPos.VALC&& out, TBD, n/a, Position number(to sad:fltrWhl1:Pos.VAL)&&Filter Wheel 1 2
ADD_REC trecs:cc:fltrWhl1:stop.A&& in, STRING, n/a, stop message&&Filter Wheel 1 2
ADD_REC trecs:cc:fltrWhl1:stop.VALA&& out, STRING, n/a, n/a&&Filter Wheel 1 2
ADD_REC trecs:cc:fltrWhl1:abort.A&& in, STRING, n/a, abort message&&Filter Wheel 1 2
ADD_REC trecs:cc:fltrWhl1:abort.VALA&& out, STRING, n/a, n/a&&Filter Wheel 1 2
ADD_REC trecs:cc:fltrWhl1:steps.A&& in, STRING, n/a, steps&&Filter Wheel 1 2
ADD_REC trecs:cc:fltrWhl1:steps.VALA&& out, DOUBLE, n/a, n/a&&Filter Wheel 1 2
ADD_REC trecs:cc:fltrWhl1:debug.DOL&& in, STRING, n/a, n/a&&Filter Wheel 1 2
ADD_REC trecs:cc:fltrWhl1:debug.OUT&& out, STRING, n/a, n/a&&Filter Wheel 1 2
ADD_REC trecs:cc:fltrWhl1:motorC.IVAL&& in, STRING, n/a, n/a&&Filter Wheel 1 2
ADD_REC trecs:cc:fltrWhl1:motorC.IMSS&& in, STRING, n/a, n/a&&Filter Wheel 1 2
ADD_REC trecs:cc:fltrWhl1:motorC.VAL&& out, STRING, n/a, n/a&&Filter Wheel 1 2
ADD_REC trecs:cc:fltrWhl1:motorC.OMSS&& out, STRING, n/a, n/a&&Filter Wheel 1 2
~add motor Filter Wheel 1 3
ADD_REC trecs:cc:fltrWhl1:motorG.A&& in, LONG, n/a, perform_test&&Filter Wheel 1 3
ADD_REC trecs:cc:fltrWhl1:motorG.C&& in, LONG, n/a, command mode&&Filter Wheel 1 3
ADD_REC trecs:cc:fltrWhl1:motorG.E&& in, LONG, n/a, datum_motor&&Filter Wheel 1 3
ADD_REC trecs:cc:fltrWhl1:motorG.G&& in, DOUBLE, n/a, num_steps&&Filter Wheel 1 3
ADD_REC trecs:cc:fltrWhl1:motorG.I&& in, STRING, n/a, stop_mess&&Filter Wheel 1 3
ADD_REC trecs:cc:fltrWhl1:motorG.K&& in, STRING, n/a, abort_mess&&Filter Wheel 1 3
ADD_REC trecs:cc:fltrWhl1:motorG.M&& in, STRING, n/a, debug mode&&Filter Wheel 1 3
ADD_REC trecs:cc:fltrWhl1:motorG.N&& in, DOUBLE, n/a, init_velocity&&Filter Wheel 1 3
ADD_REC trecs:cc:fltrWhl1:motorG.O&& in, DOUBLE, n/a, slew_velocity&&Filter Wheel 1 3
ADD_REC trecs:cc:fltrWhl1:motorG.P&& in, DOUBLE, n/a, acceleration&&Filter Wheel 1 3
ADD_REC trecs:cc:fltrWhl1:motorG.Q&& in, DOUBLE, n/a, deceleration&&Filter Wheel 1 3
ADD_REC trecs:cc:fltrWhl1:motorG.R&& in, DOUBLE, n/a, drive_current&&Filter Wheel 1 3
ADD_REC trecs:cc:fltrWhl1:motorG.S&& in, DOUBLE, n/a, datum_speed&&Filter Wheel 1 3
ADD_REC trecs:cc:fltrWhl1:motorG.T&& in, LONG, n/a, datum_direction&&Filter Wheel 1 3
ADD_REC trecs:cc:fltrWhl1:motorG.VALA&& out, LONG, n/a, command mode&&Filter Wheel 1 3
ADD_REC trecs:cc:fltrWhl1:motorG.VALB&& out, LONG, n/a, socket number&&Filter Wheel 1 3
ADD_REC trecs:cc:fltrWhl1:motorG.VALC&& out, STRING, n/a, current position&&Filter Wheel 1 3
ADD_REC trecs:cc:fltrWhl1:motorG.VALD&& out, DOUBLE, n/a, init_velocity&&Filter Wheel 1 3
ADD_REC trecs:cc:fltrWhl1:motorG.VALE&& out, DOUBLE, n/a, slew_velocity&&Filter Wheel 1 3
ADD_REC trecs:cc:fltrWhl1:motorG.VALF&& out, DOUBLE, n/a, acceleration&&Filter Wheel 1 3
ADD_REC trecs:cc:fltrWhl1:motorG.VALG&& out, DOUBLE, n/a, deceleration&&Filter Wheel 1 3
ADD_REC trecs:cc:fltrWhl1:motorG.VALH&& out, DOUBLE, n/a, drive_current&&Filter Wheel 1 3
ADD_REC trecs:cc:fltrWhl1:motorG.VALI&& out, DOUBLE, n/a, datum_speed&&Filter Wheel 1 3
ADD_REC trecs:cc:fltrWhl1:motorG.VALJ&& out, LONG, n/a, datum_direction&&Filter Wheel 1 3
~add motor Lyot Wheel 1
ADD_REC trecs:cc:lyotWhl:motorApply.A&& in, STRING, n/a, n/a(from cc:lyotWhl:test.VAL)&&Lyot Wheel 1
ADD_REC trecs:cc:lyotWhl:motorApply.B&& in, STRING, n/a, n/a(from cc:lyotWhl:init.VAL)&&Lyot Wheel 1
ADD_REC trecs:cc:lyotWhl:motorApply.C&& in, STRING, n/a, n/a(from cc:lyotWhl:datum.VAL)&&Lyot Wheel 1
ADD_REC trecs:cc:lyotWhl:motorApply.D&& in, STRING, n/a, n/a(from cc:lyotWhl:namedPos.VAL)&&Lyot Wheel 1
ADD_REC trecs:cc:lyotWhl:motorApply.E&& in, STRING, n/a, n/a(from cc:lyotWhl:steps.VAL)&&Lyot Wheel 1
ADD_REC trecs:cc:lyotWhl:motorApply.F&& in, STRING, n/a, n/a(from cc:lyotWhl:abort.VAL)&&Lyot Wheel 1
ADD_REC trecs:cc:lyotWhl:motorApply.G&& in, STRING, n/a, n/a(from cc:lyotWhl:stop.VAL)&&Lyot Wheel 1
ADD_REC trecs:cc:lyotWhl:motorApply.VALA&& out, STRING, n/a, n/a(to cc:lyotWhl:test.DIR)&&Lyot Wheel 1
ADD_REC trecs:cc:lyotWhl:motorApply.VALB&& out, STRING, n/a, n/a(to cc:lyotWhl:init.DIR)&&Lyot Wheel 1
ADD_REC trecs:cc:lyotWhl:motorApply.VALC&& out, STRING, n/a, n/a(to cc:lyotWhl:datum.DIR)&&Lyot Wheel 1
ADD_REC trecs:cc:lyotWhl:motorApply.VALD&& out, STRING, n/a, n/a(to cc:lyotWhl:namedPos.DIR)&&Lyot Wheel 1
ADD_REC trecs:cc:lyotWhl:motorApply.VALE&& out, STRING, n/a, n/a(to cc:lyotWhl:steps.DIR)&&Lyot Wheel 1
ADD_REC trecs:cc:lyotWhl:motorApply.VALF&& out, STRING, n/a, n/a(to cc:lyotWhl:abort.DIR)&&Lyot Wheel 1
ADD_REC trecs:cc:lyotWhl:motorApply.VALG&& out, STRING, n/a, n/a(to cc:lyotWhl:stop.DIR)&&Lyot Wheel 1
ADD_REC trecs:cc:lyotWhl:test.A&& in, STRING, n/a, test directive&&Lyot Wheel 1
ADD_REC trecs:cc:lyotWhl:test.VALA&& out, LONG, n/a, n/a(internal)&&Lyot Wheel 1
ADD_REC trecs:cc:lyotWhl:init.A&& in, STRING, n/a, init directive&&Lyot Wheel 1
ADD_REC trecs:cc:lyotWhl:init.VALA&& out, LONG, n/a, n/a&&Lyot Wheel 1
ADD_REC trecs:cc:lyotWhl:datum.A&& in, STRING, n/a, datum directive&&Lyot Wheel 1
ADD_REC trecs:cc:lyotWhl:datum.VALA&& out, LONG, n/a, n/a&&Lyot Wheel 1
~add motor Lyot Wheel 2
ADD_REC trecs:cc:lyotWhl:namedPos.A&& in, STRING, n/a, Position number&&Lyot Wheel 2
ADD_REC trecs:cc:lyotWhl:namedPos.VALA&& out, LONG, n/a, n/a&&Lyot Wheel 2
ADD_REC trecs:cc:lyotWhl:namedPos.VALB&& out, DOUBLE, n/a, n/a&&Lyot Wheel 2
ADD_REC trecs:cc:lyotWhl:namedPos.VALC&& out, TBD, n/a, Position number(to sad:lyotWhl:Pos.VAL)&&Lyot Wheel 2
ADD_REC trecs:cc:lyotWhl:stop.A&& in, STRING, n/a, stop message&&Lyot Wheel 2
ADD_REC trecs:cc:lyotWhl:stop.VALA&& out, STRING, n/a, n/a&&Lyot Wheel 2
ADD_REC trecs:cc:lyotWhl:abort.A&& in, STRING, n/a, abort message&&Lyot Wheel 2
ADD_REC trecs:cc:lyotWhl:abort.VALA&& out, STRING, n/a, n/a&&Lyot Wheel 2
ADD_REC trecs:cc:lyotWhl:steps.A&& in, STRING, n/a, steps&&Lyot Wheel 2
ADD_REC trecs:cc:lyotWhl:steps.VALA&& out, DOUBLE, n/a, n/a&&Lyot Wheel 2
ADD_REC trecs:cc:lyotWhl:debug.DOL&& in, STRING, n/a, n/a&&Lyot Wheel 2
ADD_REC trecs:cc:lyotWhl:debug.OUT&& out, STRING, n/a, n/a&&Lyot Wheel 2
ADD_REC trecs:cc:lyotWhl:motorC.IVAL&& in, STRING, n/a, n/a&&Lyot Wheel 2
ADD_REC trecs:cc:lyotWhl:motorC.IMSS&& in, STRING, n/a, n/a&&Lyot Wheel 2
ADD_REC trecs:cc:lyotWhl:motorC.VAL&& out, STRING, n/a, n/a&&Lyot Wheel 2
ADD_REC trecs:cc:lyotWhl:motorC.OMSS&& out, STRING, n/a, n/a&&Lyot Wheel 2
~add motor Lyot Wheel 3
ADD_REC trecs:cc:lyotWhl:motorG.A&& in, LONG, n/a, perform_test&&Lyot Wheel 3
ADD_REC trecs:cc:lyotWhl:motorG.C&& in, LONG, n/a, command mode&&Lyot Wheel 3
ADD_REC trecs:cc:lyotWhl:motorG.E&& in, LONG, n/a, datum_motor&&Lyot Wheel 3
ADD_REC trecs:cc:lyotWhl:motorG.G&& in, DOUBLE, n/a, num_steps&&Lyot Wheel 3
ADD_REC trecs:cc:lyotWhl:motorG.I&& in, STRING, n/a, stop_mess&&Lyot Wheel 3
ADD_REC trecs:cc:lyotWhl:motorG.K&& in, STRING, n/a, abort_mess&&Lyot Wheel 3
ADD_REC trecs:cc:lyotWhl:motorG.M&& in, STRING, n/a, debug mode&&Lyot Wheel 3
ADD_REC trecs:cc:lyotWhl:motorG.N&& in, DOUBLE, n/a, init_velocity&&Lyot Wheel 3
ADD_REC trecs:cc:lyotWhl:motorG.O&& in, DOUBLE, n/a, slew_velocity&&Lyot Wheel 3
ADD_REC trecs:cc:lyotWhl:motorG.P&& in, DOUBLE, n/a, acceleration&&Lyot Wheel 3
ADD_REC trecs:cc:lyotWhl:motorG.Q&& in, DOUBLE, n/a, deceleration&&Lyot Wheel 3
ADD_REC trecs:cc:lyotWhl:motorG.R&& in, DOUBLE, n/a, drive_current&&Lyot Wheel 3
ADD_REC trecs:cc:lyotWhl:motorG.S&& in, DOUBLE, n/a, datum_speed&&Lyot Wheel 3
ADD_REC trecs:cc:lyotWhl:motorG.T&& in, LONG, n/a, datum_direction&&Lyot Wheel 3
ADD_REC trecs:cc:lyotWhl:motorG.VALA&& out, LONG, n/a, command mode&&Lyot Wheel 3
ADD_REC trecs:cc:lyotWhl:motorG.VALB&& out, LONG, n/a, socket number&&Lyot Wheel 3
ADD_REC trecs:cc:lyotWhl:motorG.VALC&& out, STRING, n/a, current position&&Lyot Wheel 3
ADD_REC trecs:cc:lyotWhl:motorG.VALD&& out, DOUBLE, n/a, init_velocity&&Lyot Wheel 3
ADD_REC trecs:cc:lyotWhl:motorG.VALE&& out, DOUBLE, n/a, slew_velocity&&Lyot Wheel 3
ADD_REC trecs:cc:lyotWhl:motorG.VALF&& out, DOUBLE, n/a, acceleration&&Lyot Wheel 3
ADD_REC trecs:cc:lyotWhl:motorG.VALG&& out, DOUBLE, n/a, deceleration&&Lyot Wheel 3
ADD_REC trecs:cc:lyotWhl:motorG.VALH&& out, DOUBLE, n/a, drive_current&&Lyot Wheel 3
ADD_REC trecs:cc:lyotWhl:motorG.VALI&& out, DOUBLE, n/a, datum_speed&&Lyot Wheel 3
ADD_REC trecs:cc:lyotWhl:motorG.VALJ&& out, LONG, n/a, datum_direction&&Lyot Wheel 3
~add motor Filter Wheel 2 1
ADD_REC trecs:cc:fltrWhl2:motorApply.A&& in, STRING, n/a, n/a(from cc:fltrWhl2:test.VAL)&&Filter Wheel 2 1
ADD_REC trecs:cc:fltrWhl2:motorApply.B&& in, STRING, n/a, n/a(from cc:fltrWhl2:init.VAL)&&Filter Wheel 2 1
ADD_REC trecs:cc:fltrWhl2:motorApply.C&& in, STRING, n/a, n/a(from cc:fltrWhl2:datum.VAL)&&Filter Wheel 2 1
ADD_REC trecs:cc:fltrWhl2:motorApply.D&& in, STRING, n/a, n/a(from cc:fltrWhl2:namedPos.VAL)&&Filter Wheel 2 1
ADD_REC trecs:cc:fltrWhl2:motorApply.E&& in, STRING, n/a, n/a(from cc:fltrWhl2:steps.VAL)&&Filter Wheel 2 1
ADD_REC trecs:cc:fltrWhl2:motorApply.F&& in, STRING, n/a, n/a(from cc:fltrWhl2:abort.VAL)&&Filter Wheel 2 1
ADD_REC trecs:cc:fltrWhl2:motorApply.G&& in, STRING, n/a, n/a(from cc:fltrWhl2:stop.VAL)&&Filter Wheel 2 1
ADD_REC trecs:cc:fltrWhl2:motorApply.VALA&& out, STRING, n/a, n/a(to cc:fltrWhl2:test.DIR)&&Filter Wheel 2 1
ADD_REC trecs:cc:fltrWhl2:motorApply.VALB&& out, STRING, n/a, n/a(to cc:fltrWhl2:init.DIR)&&Filter Wheel 2 1
ADD_REC trecs:cc:fltrWhl2:motorApply.VALC&& out, STRING, n/a, n/a(to cc:fltrWhl2:datum.DIR)&&Filter Wheel 2 1
ADD_REC trecs:cc:fltrWhl2:motorApply.VALD&& out, STRING, n/a, n/a(to cc:fltrWhl2:namedPos.DIR)&&Filter Wheel 2 1
ADD_REC trecs:cc:fltrWhl2:motorApply.VALE&& out, STRING, n/a, n/a(to cc:fltrWhl2:steps.DIR)&&Filter Wheel 2 1
ADD_REC trecs:cc:fltrWhl2:motorApply.VALF&& out, STRING, n/a, n/a(to cc:fltrWhl2:abort.DIR)&&Filter Wheel 2 1
ADD_REC trecs:cc:fltrWhl2:motorApply.VALG&& out, STRING, n/a, n/a(to cc:fltrWhl2:stop.DIR)&&Filter Wheel 2 1
ADD_REC trecs:cc:fltrWhl2:test.A&& in, STRING, n/a, test directive&&Filter Wheel 2 1
ADD_REC trecs:cc:fltrWhl2:test.VALA&& out, LONG, n/a, n/a(internal)&&Filter Wheel 2 1
ADD_REC trecs:cc:fltrWhl2:init.A&& in, STRING, n/a, init directive&&Filter Wheel 2 1
ADD_REC trecs:cc:fltrWhl2:init.VALA&& out, LONG, n/a, n/a&&Filter Wheel 2 1
ADD_REC trecs:cc:fltrWhl2:datum.A&& in, STRING, n/a, datum directive&&Filter Wheel 2 1
ADD_REC trecs:cc:fltrWhl2:datum.VALA&& out, LONG, n/a, n/a&&Filter Wheel 2 1
~add motor Filter Wheel 2 2
ADD_REC trecs:cc:fltrWhl2:namedPos.A&& in, STRING, n/a, Position number&&Filter Wheel 2 2
ADD_REC trecs:cc:fltrWhl2:namedPos.VALA&& out, LONG, n/a, n/a&&Filter Wheel 2 2
ADD_REC trecs:cc:fltrWhl2:namedPos.VALB&& out, DOUBLE, n/a, n/a&&Filter Wheel 2 2
ADD_REC trecs:cc:fltrWhl2:namedPos.VALC&& out, TBD, n/a, Position number(to sad:fltrWhl2:Pos.VAL)&&Filter Wheel 2 2
ADD_REC trecs:cc:fltrWhl2:stop.A&& in, STRING, n/a, stop message&&Filter Wheel 2 2
ADD_REC trecs:cc:fltrWhl2:stop.VALA&& out, STRING, n/a, n/a&&Filter Wheel 2 2
ADD_REC trecs:cc:fltrWhl2:abort.A&& in, STRING, n/a, abort message&&Filter Wheel 2 2
ADD_REC trecs:cc:fltrWhl2:abort.VALA&& out, STRING, n/a, n/a&&Filter Wheel 2 2
ADD_REC trecs:cc:fltrWhl2:steps.A&& in, STRING, n/a, steps&&Filter Wheel 2 2
ADD_REC trecs:cc:fltrWhl2:steps.VALA&& out, DOUBLE, n/a, n/a&&Filter Wheel 2 2
ADD_REC trecs:cc:fltrWhl2:debug.DOL&& in, STRING, n/a, n/a&&Filter Wheel 2 2
ADD_REC trecs:cc:fltrWhl2:debug.OUT&& out, STRING, n/a, n/a&&Filter Wheel 2 2
ADD_REC trecs:cc:fltrWhl2:motorC.IVAL&& in, STRING, n/a, n/a&&Filter Wheel 2 2
ADD_REC trecs:cc:fltrWhl2:motorC.IMSS&& in, STRING, n/a, n/a&&Filter Wheel 2 2
ADD_REC trecs:cc:fltrWhl2:motorC.VAL&& out, STRING, n/a, n/a&&Filter Wheel 2 2
ADD_REC trecs:cc:fltrWhl2:motorC.OMSS&& out, STRING, n/a, n/a&&Filter Wheel 2 2
~add motor Filter Wheel 2 3
ADD_REC trecs:cc:fltrWhl2:motorG.A&& in, LONG, n/a, perform_test&&Filter Wheel 2 3
ADD_REC trecs:cc:fltrWhl2:motorG.C&& in, LONG, n/a, command mode&&Filter Wheel 2 3
ADD_REC trecs:cc:fltrWhl2:motorG.E&& in, LONG, n/a, datum_motor&&Filter Wheel 2 3
ADD_REC trecs:cc:fltrWhl2:motorG.G&& in, DOUBLE, n/a, num_steps&&Filter Wheel 2 3
ADD_REC trecs:cc:fltrWhl2:motorG.I&& in, STRING, n/a, stop_mess&&Filter Wheel 2 3
ADD_REC trecs:cc:fltrWhl2:motorG.K&& in, STRING, n/a, abort_mess&&Filter Wheel 2 3
ADD_REC trecs:cc:fltrWhl2:motorG.M&& in, STRING, n/a, debug mode&&Filter Wheel 2 3
ADD_REC trecs:cc:fltrWhl2:motorG.N&& in, DOUBLE, n/a, init_velocity&&Filter Wheel 2 3
ADD_REC trecs:cc:fltrWhl2:motorG.O&& in, DOUBLE, n/a, slew_velocity&&Filter Wheel 2 3
ADD_REC trecs:cc:fltrWhl2:motorG.P&& in, DOUBLE, n/a, acceleration&&Filter Wheel 2 3
ADD_REC trecs:cc:fltrWhl2:motorG.Q&& in, DOUBLE, n/a, deceleration&&Filter Wheel 2 3
ADD_REC trecs:cc:fltrWhl2:motorG.R&& in, DOUBLE, n/a, drive_current&&Filter Wheel 2 3
ADD_REC trecs:cc:fltrWhl2:motorG.S&& in, DOUBLE, n/a, datum_speed&&Filter Wheel 2 3
ADD_REC trecs:cc:fltrWhl2:motorG.T&& in, LONG, n/a, datum_direction&&Filter Wheel 2 3
ADD_REC trecs:cc:fltrWhl2:motorG.VALA&& out, LONG, n/a, command mode&&Filter Wheel 2 3
ADD_REC trecs:cc:fltrWhl2:motorG.VALB&& out, LONG, n/a, socket number&&Filter Wheel 2 3
ADD_REC trecs:cc:fltrWhl2:motorG.VALC&& out, STRING, n/a, current position&&Filter Wheel 2 3
ADD_REC trecs:cc:fltrWhl2:motorG.VALD&& out, DOUBLE, n/a, init_velocity&&Filter Wheel 2 3
ADD_REC trecs:cc:fltrWhl2:motorG.VALE&& out, DOUBLE, n/a, slew_velocity&&Filter Wheel 2 3
ADD_REC trecs:cc:fltrWhl2:motorG.VALF&& out, DOUBLE, n/a, acceleration&&Filter Wheel 2 3
ADD_REC trecs:cc:fltrWhl2:motorG.VALG&& out, DOUBLE, n/a, deceleration&&Filter Wheel 2 3
ADD_REC trecs:cc:fltrWhl2:motorG.VALH&& out, DOUBLE, n/a, drive_current&&Filter Wheel 2 3
ADD_REC trecs:cc:fltrWhl2:motorG.VALI&& out, DOUBLE, n/a, datum_speed&&Filter Wheel 2 3
ADD_REC trecs:cc:fltrWhl2:motorG.VALJ&& out, LONG, n/a, datum_direction&&Filter Wheel 2 3
~add motor Pupil Wheel 1
ADD_REC trecs:cc:pplImg:motorApply.A&& in, STRING, n/a, n/a(from cc:pplImg:test.VAL)&&Pupil Wheel 1
ADD_REC trecs:cc:pplImg:motorApply.B&& in, STRING, n/a, n/a(from cc:pplImg:init.VAL)&&Pupil Wheel 1
ADD_REC trecs:cc:pplImg:motorApply.C&& in, STRING, n/a, n/a(from cc:pplImg:datum.VAL)&&Pupil Wheel 1
ADD_REC trecs:cc:pplImg:motorApply.D&& in, STRING, n/a, n/a(from cc:pplImg:namedPos.VAL)&&Pupil Wheel 1
ADD_REC trecs:cc:pplImg:motorApply.E&& in, STRING, n/a, n/a(from cc:pplImg:steps.VAL)&&Pupil Wheel 1
ADD_REC trecs:cc:pplImg:motorApply.F&& in, STRING, n/a, n/a(from cc:pplImg:abort.VAL)&&Pupil Wheel 1
ADD_REC trecs:cc:pplImg:motorApply.G&& in, STRING, n/a, n/a(from cc:pplImg:stop.VAL)&&Pupil Wheel 1
ADD_REC trecs:cc:pplImg:motorApply.VALA&& out, STRING, n/a, n/a(to cc:pplImg:test.DIR)&&Pupil Wheel 1
ADD_REC trecs:cc:pplImg:motorApply.VALB&& out, STRING, n/a, n/a(to cc:pplImg:init.DIR)&&Pupil Wheel 1
ADD_REC trecs:cc:pplImg:motorApply.VALC&& out, STRING, n/a, n/a(to cc:pplImg:datum.DIR)&&Pupil Wheel 1
ADD_REC trecs:cc:pplImg:motorApply.VALD&& out, STRING, n/a, n/a(to cc:pplImg:namedPos.DIR)&&Pupil Wheel 1
ADD_REC trecs:cc:pplImg:motorApply.VALE&& out, STRING, n/a, n/a(to cc:pplImg:steps.DIR)&&Pupil Wheel 1
ADD_REC trecs:cc:pplImg:motorApply.VALF&& out, STRING, n/a, n/a(to cc:pplImg:abort.DIR)&&Pupil Wheel 1
ADD_REC trecs:cc:pplImg:motorApply.VALG&& out, STRING, n/a, n/a(to cc:pplImg:stop.DIR)&&Pupil Wheel 1
ADD_REC trecs:cc:pplImg:test.A&& in, STRING, n/a, test directive&&Pupil Wheel 1
ADD_REC trecs:cc:pplImg:test.VALA&& out, LONG, n/a, n/a(internal)&&Pupil Wheel 1
ADD_REC trecs:cc:pplImg:init.A&& in, STRING, n/a, init directive&&Pupil Wheel 1
ADD_REC trecs:cc:pplImg:init.VALA&& out, LONG, n/a, n/a&&Pupil Wheel 1
ADD_REC trecs:cc:pplImg:datum.A&& in, STRING, n/a, datum directive&&Pupil Wheel 1
ADD_REC trecs:cc:pplImg:datum.VALA&& out, LONG, n/a, n/a&&Pupil Wheel 1
~add motor Pupil Wheel 2
ADD_REC trecs:cc:pplImg:namedPos.A&& in, STRING, n/a, Position number&&Pupil Wheel 2
ADD_REC trecs:cc:pplImg:namedPos.VALA&& out, LONG, n/a, n/a&&Pupil Wheel 2
ADD_REC trecs:cc:pplImg:namedPos.VALB&& out, DOUBLE, n/a, n/a&&Pupil Wheel 2
ADD_REC trecs:cc:pplImg:namedPos.VALC&& out, TBD, n/a, Position number(to sad:pplImg:Pos.VAL)&&Pupil Wheel 2
ADD_REC trecs:cc:pplImg:stop.A&& in, STRING, n/a, stop message&&Pupil Wheel 2
ADD_REC trecs:cc:pplImg:stop.VALA&& out, STRING, n/a, n/a&&Pupil Wheel 2
ADD_REC trecs:cc:pplImg:abort.A&& in, STRING, n/a, abort message&&Pupil Wheel 2
ADD_REC trecs:cc:pplImg:abort.VALA&& out, STRING, n/a, n/a&&Pupil Wheel 2
ADD_REC trecs:cc:pplImg:steps.A&& in, STRING, n/a, steps&&Pupil Wheel 2
ADD_REC trecs:cc:pplImg:steps.VALA&& out, DOUBLE, n/a, n/a&&Pupil Wheel 2
ADD_REC trecs:cc:pplImg:debug.DOL&& in, STRING, n/a, n/a&&Pupil Wheel 2
ADD_REC trecs:cc:pplImg:debug.OUT&& out, STRING, n/a, n/a&&Pupil Wheel 2
ADD_REC trecs:cc:pplImg:motorC.IVAL&& in, STRING, n/a, n/a&&Pupil Wheel 2
ADD_REC trecs:cc:pplImg:motorC.IMSS&& in, STRING, n/a, n/a&&Pupil Wheel 2
ADD_REC trecs:cc:pplImg:motorC.VAL&& out, STRING, n/a, n/a&&Pupil Wheel 2
ADD_REC trecs:cc:pplImg:motorC.OMSS&& out, STRING, n/a, n/a&&Pupil Wheel 2
~add motor Pupil Wheel 3
ADD_REC trecs:cc:pplImg:motorG.A&& in, LONG, n/a, perform_test&&Pupil Wheel 3
ADD_REC trecs:cc:pplImg:motorG.C&& in, LONG, n/a, command mode&&Pupil Wheel 3
ADD_REC trecs:cc:pplImg:motorG.E&& in, LONG, n/a, datum_motor&&Pupil Wheel 3
ADD_REC trecs:cc:pplImg:motorG.G&& in, DOUBLE, n/a, num_steps&&Pupil Wheel 3
ADD_REC trecs:cc:pplImg:motorG.I&& in, STRING, n/a, stop_mess&&Pupil Wheel 3
ADD_REC trecs:cc:pplImg:motorG.K&& in, STRING, n/a, abort_mess&&Pupil Wheel 3
ADD_REC trecs:cc:pplImg:motorG.M&& in, STRING, n/a, debug mode&&Pupil Wheel 3
ADD_REC trecs:cc:pplImg:motorG.N&& in, DOUBLE, n/a, init_velocity&&Pupil Wheel 3
ADD_REC trecs:cc:pplImg:motorG.O&& in, DOUBLE, n/a, slew_velocity&&Pupil Wheel 3
ADD_REC trecs:cc:pplImg:motorG.P&& in, DOUBLE, n/a, acceleration&&Pupil Wheel 3
ADD_REC trecs:cc:pplImg:motorG.Q&& in, DOUBLE, n/a, deceleration&&Pupil Wheel 3
ADD_REC trecs:cc:pplImg:motorG.R&& in, DOUBLE, n/a, drive_current&&Pupil Wheel 3
ADD_REC trecs:cc:pplImg:motorG.S&& in, DOUBLE, n/a, datum_speed&&Pupil Wheel 3
ADD_REC trecs:cc:pplImg:motorG.T&& in, LONG, n/a, datum_direction&&Pupil Wheel 3
ADD_REC trecs:cc:pplImg:motorG.VALA&& out, LONG, n/a, command mode&&Pupil Wheel 3
ADD_REC trecs:cc:pplImg:motorG.VALB&& out, LONG, n/a, socket number&&Pupil Wheel 3
ADD_REC trecs:cc:pplImg:motorG.VALC&& out, STRING, n/a, current position&&Pupil Wheel 3
ADD_REC trecs:cc:pplImg:motorG.VALD&& out, DOUBLE, n/a, init_velocity&&Pupil Wheel 3
ADD_REC trecs:cc:pplImg:motorG.VALE&& out, DOUBLE, n/a, slew_velocity&&Pupil Wheel 3
ADD_REC trecs:cc:pplImg:motorG.VALF&& out, DOUBLE, n/a, acceleration&&Pupil Wheel 3
ADD_REC trecs:cc:pplImg:motorG.VALG&& out, DOUBLE, n/a, deceleration&&Pupil Wheel 3
ADD_REC trecs:cc:pplImg:motorG.VALH&& out, DOUBLE, n/a, drive_current&&Pupil Wheel 3
ADD_REC trecs:cc:pplImg:motorG.VALI&& out, DOUBLE, n/a, datum_speed&&Pupil Wheel 3
ADD_REC trecs:cc:pplImg:motorG.VALJ&& out, LONG, n/a, datum_direction&&Pupil Wheel 3
~add motor Slit Wheel 1
ADD_REC trecs:cc:slitWhl:motorApply.A&& in, STRING, n/a, n/a(from cc:slitWhl:test.VAL)&&Slit Wheel 1
ADD_REC trecs:cc:slitWhl:motorApply.B&& in, STRING, n/a, n/a(from cc:slitWhl:init.VAL)&&Slit Wheel 1
ADD_REC trecs:cc:slitWhl:motorApply.C&& in, STRING, n/a, n/a(from cc:slitWhl:datum.VAL)&&Slit Wheel 1
ADD_REC trecs:cc:slitWhl:motorApply.D&& in, STRING, n/a, n/a(from cc:slitWhl:namedPos.VAL)&&Slit Wheel 1
ADD_REC trecs:cc:slitWhl:motorApply.E&& in, STRING, n/a, n/a(from cc:slitWhl:steps.VAL)&&Slit Wheel 1
ADD_REC trecs:cc:slitWhl:motorApply.F&& in, STRING, n/a, n/a(from cc:slitWhl:abort.VAL)&&Slit Wheel 1
ADD_REC trecs:cc:slitWhl:motorApply.G&& in, STRING, n/a, n/a(from cc:slitWhl:stop.VAL)&&Slit Wheel 1
ADD_REC trecs:cc:slitWhl:motorApply.VALA&& out, STRING, n/a, n/a(to cc:slitWhl:test.DIR)&&Slit Wheel 1
ADD_REC trecs:cc:slitWhl:motorApply.VALB&& out, STRING, n/a, n/a(to cc:slitWhl:init.DIR)&&Slit Wheel 1
ADD_REC trecs:cc:slitWhl:motorApply.VALC&& out, STRING, n/a, n/a(to cc:slitWhl:datum.DIR)&&Slit Wheel 1
ADD_REC trecs:cc:slitWhl:motorApply.VALD&& out, STRING, n/a, n/a(to cc:slitWhl:namedPos.DIR)&&Slit Wheel 1
ADD_REC trecs:cc:slitWhl:motorApply.VALE&& out, STRING, n/a, n/a(to cc:slitWhl:steps.DIR)&&Slit Wheel 1
ADD_REC trecs:cc:slitWhl:motorApply.VALF&& out, STRING, n/a, n/a(to cc:slitWhl:abort.DIR)&&Slit Wheel 1
ADD_REC trecs:cc:slitWhl:motorApply.VALG&& out, STRING, n/a, n/a(to cc:slitWhl:stop.DIR)&&Slit Wheel 1
ADD_REC trecs:cc:slitWhl:test.A&& in, STRING, n/a, test directive&&Slit Wheel 1
ADD_REC trecs:cc:slitWhl:test.VALA&& out, LONG, n/a, n/a(internal)&&Slit Wheel 1
ADD_REC trecs:cc:slitWhl:init.A&& in, STRING, n/a, init directive&&Slit Wheel 1
ADD_REC trecs:cc:slitWhl:init.VALA&& out, LONG, n/a, n/a&&Slit Wheel 1
ADD_REC trecs:cc:slitWhl:datum.A&& in, STRING, n/a, datum directive&&Slit Wheel 1
ADD_REC trecs:cc:slitWhl:datum.VALA&& out, LONG, n/a, n/a&&Slit Wheel 1
~add motor Slit Wheel 2
ADD_REC trecs:cc:slitWhl:namedPos.A&& in, STRING, n/a, Position number&&Slit Wheel 2
ADD_REC trecs:cc:slitWhl:namedPos.VALA&& out, LONG, n/a, n/a&&Slit Wheel 2
ADD_REC trecs:cc:slitWhl:namedPos.VALB&& out, DOUBLE, n/a, n/a&&Slit Wheel 2
ADD_REC trecs:cc:slitWhl:namedPos.VALC&& out, TBD, n/a, Position number(to sad:slitWhl:Pos.VAL)&&Slit Wheel 2
ADD_REC trecs:cc:slitWhl:stop.A&& in, STRING, n/a, stop message&&Slit Wheel 2
ADD_REC trecs:cc:slitWhl:stop.VALA&& out, STRING, n/a, n/a&&Slit Wheel 2
ADD_REC trecs:cc:slitWhl:abort.A&& in, STRING, n/a, abort message&&Slit Wheel 2
ADD_REC trecs:cc:slitWhl:abort.VALA&& out, STRING, n/a, n/a&&Slit Wheel 2
ADD_REC trecs:cc:slitWhl:steps.A&& in, STRING, n/a, steps&&Slit Wheel 2
ADD_REC trecs:cc:slitWhl:steps.VALA&& out, DOUBLE, n/a, n/a&&Slit Wheel 2
ADD_REC trecs:cc:slitWhl:debug.DOL&& in, STRING, n/a, n/a&&Slit Wheel 2
ADD_REC trecs:cc:slitWhl:debug.OUT&& out, STRING, n/a, n/a&&Slit Wheel 2
ADD_REC trecs:cc:slitWhl:motorC.IVAL&& in, STRING, n/a, n/a&&Slit Wheel 2
ADD_REC trecs:cc:slitWhl:motorC.IMSS&& in, STRING, n/a, n/a&&Slit Wheel 2
ADD_REC trecs:cc:slitWhl:motorC.VAL&& out, STRING, n/a, n/a&&Slit Wheel 2
ADD_REC trecs:cc:slitWhl:motorC.OMSS&& out, STRING, n/a, n/a&&Slit Wheel 2
~add motor Slit Wheel 3
ADD_REC trecs:cc:slitWhl:motorG.A&& in, LONG, n/a, perform_test&&Slit Wheel 3
ADD_REC trecs:cc:slitWhl:motorG.C&& in, LONG, n/a, command mode&&Slit Wheel 3
ADD_REC trecs:cc:slitWhl:motorG.E&& in, LONG, n/a, datum_motor&&Slit Wheel 3
ADD_REC trecs:cc:slitWhl:motorG.G&& in, DOUBLE, n/a, num_steps&&Slit Wheel 3
ADD_REC trecs:cc:slitWhl:motorG.I&& in, STRING, n/a, stop_mess&&Slit Wheel 3
ADD_REC trecs:cc:slitWhl:motorG.K&& in, STRING, n/a, abort_mess&&Slit Wheel 3
ADD_REC trecs:cc:slitWhl:motorG.M&& in, STRING, n/a, debug mode&&Slit Wheel 3
ADD_REC trecs:cc:slitWhl:motorG.N&& in, DOUBLE, n/a, init_velocity&&Slit Wheel 3
ADD_REC trecs:cc:slitWhl:motorG.O&& in, DOUBLE, n/a, slew_velocity&&Slit Wheel 3
ADD_REC trecs:cc:slitWhl:motorG.P&& in, DOUBLE, n/a, acceleration&&Slit Wheel 3
ADD_REC trecs:cc:slitWhl:motorG.Q&& in, DOUBLE, n/a, deceleration&&Slit Wheel 3
ADD_REC trecs:cc:slitWhl:motorG.R&& in, DOUBLE, n/a, drive_current&&Slit Wheel 3
ADD_REC trecs:cc:slitWhl:motorG.S&& in, DOUBLE, n/a, datum_speed&&Slit Wheel 3
ADD_REC trecs:cc:slitWhl:motorG.T&& in, LONG, n/a, datum_direction&&Slit Wheel 3
ADD_REC trecs:cc:slitWhl:motorG.VALA&& out, LONG, n/a, command mode&&Slit Wheel 3
ADD_REC trecs:cc:slitWhl:motorG.VALB&& out, LONG, n/a, socket number&&Slit Wheel 3
ADD_REC trecs:cc:slitWhl:motorG.VALC&& out, STRING, n/a, current position&&Slit Wheel 3
ADD_REC trecs:cc:slitWhl:motorG.VALD&& out, DOUBLE, n/a, init_velocity&&Slit Wheel 3
ADD_REC trecs:cc:slitWhl:motorG.VALE&& out, DOUBLE, n/a, slew_velocity&&Slit Wheel 3
ADD_REC trecs:cc:slitWhl:motorG.VALF&& out, DOUBLE, n/a, acceleration&&Slit Wheel 3
ADD_REC trecs:cc:slitWhl:motorG.VALG&& out, DOUBLE, n/a, deceleration&&Slit Wheel 3
ADD_REC trecs:cc:slitWhl:motorG.VALH&& out, DOUBLE, n/a, drive_current&&Slit Wheel 3
ADD_REC trecs:cc:slitWhl:motorG.VALI&& out, DOUBLE, n/a, datum_speed&&Slit Wheel 3
ADD_REC trecs:cc:slitWhl:motorG.VALJ&& out, LONG, n/a, datum_direction&&Slit Wheel 3
~add motor Grating Turret 1
ADD_REC trecs:cc:grating:motorApply.A&& in, STRING, n/a, n/a(from cc:grating:test.VAL)&&Grating Turret 1
ADD_REC trecs:cc:grating:motorApply.B&& in, STRING, n/a, n/a(from cc:grating:init.VAL)&&Grating Turret 1
ADD_REC trecs:cc:grating:motorApply.C&& in, STRING, n/a, n/a(from cc:grating:datum.VAL)&&Grating Turret 1
ADD_REC trecs:cc:grating:motorApply.D&& in, STRING, n/a, n/a(from cc:grating:namedPos.VAL)&&Grating Turret 1
ADD_REC trecs:cc:grating:motorApply.E&& in, STRING, n/a, n/a(from cc:grating:steps.VAL)&&Grating Turret 1
ADD_REC trecs:cc:grating:motorApply.F&& in, STRING, n/a, n/a(from cc:grating:abort.VAL)&&Grating Turret 1
ADD_REC trecs:cc:grating:motorApply.G&& in, STRING, n/a, n/a(from cc:grating:stop.VAL)&&Grating Turret 1
ADD_REC trecs:cc:grating:motorApply.VALA&& out, STRING, n/a, n/a(to cc:grating:test.DIR)&&Grating Turret 1
ADD_REC trecs:cc:grating:motorApply.VALB&& out, STRING, n/a, n/a(to cc:grating:init.DIR)&&Grating Turret 1
ADD_REC trecs:cc:grating:motorApply.VALC&& out, STRING, n/a, n/a(to cc:grating:datum.DIR)&&Grating Turret 1
ADD_REC trecs:cc:grating:motorApply.VALD&& out, STRING, n/a, n/a(to cc:grating:namedPos.DIR)&&Grating Turret 1
ADD_REC trecs:cc:grating:motorApply.VALE&& out, STRING, n/a, n/a(to cc:grating:steps.DIR)&&Grating Turret 1
ADD_REC trecs:cc:grating:motorApply.VALF&& out, STRING, n/a, n/a(to cc:grating:abort.DIR)&&Grating Turret 1
ADD_REC trecs:cc:grating:motorApply.VALG&& out, STRING, n/a, n/a(to cc:grating:stop.DIR)&&Grating Turret 1
ADD_REC trecs:cc:grating:test.A&& in, STRING, n/a, test directive&&Grating Turret 1
ADD_REC trecs:cc:grating:test.VALA&& out, LONG, n/a, n/a(internal)&&Grating Turret 1
ADD_REC trecs:cc:grating:init.A&& in, STRING, n/a, init directive&&Grating Turret 1
ADD_REC trecs:cc:grating:init.VALA&& out, LONG, n/a, n/a&&Grating Turret 1
ADD_REC trecs:cc:grating:datum.A&& in, STRING, n/a, datum directive&&Grating Turret 1
ADD_REC trecs:cc:grating:datum.VALA&& out, LONG, n/a, n/a&&Grating Turret 1
~add motor Grating Turret 2
ADD_REC trecs:cc:grating:namedPos.A&& in, STRING, n/a, Position number&&Grating Turret 2
ADD_REC trecs:cc:grating:namedPos.VALA&& out, LONG, n/a, n/a&&Grating Turret 2
ADD_REC trecs:cc:grating:namedPos.VALB&& out, DOUBLE, n/a, n/a&&Grating Turret 2
ADD_REC trecs:cc:grating:namedPos.VALC&& out, TBD, n/a, Position number(to sad:grating:Pos.VAL)&&Grating Turret 2
ADD_REC trecs:cc:grating:stop.A&& in, STRING, n/a, stop message&&Grating Turret 2
ADD_REC trecs:cc:grating:stop.VALA&& out, STRING, n/a, n/a&&Grating Turret 2
ADD_REC trecs:cc:grating:abort.A&& in, STRING, n/a, abort message&&Grating Turret 2
ADD_REC trecs:cc:grating:abort.VALA&& out, STRING, n/a, n/a&&Grating Turret 2
ADD_REC trecs:cc:grating:steps.A&& in, STRING, n/a, steps&&Grating Turret 2
ADD_REC trecs:cc:grating:steps.VALA&& out, DOUBLE, n/a, n/a&&Grating Turret 2
ADD_REC trecs:cc:grating:debug.DOL&& in, STRING, n/a, n/a&&Grating Turret 2
ADD_REC trecs:cc:grating:debug.OUT&& out, STRING, n/a, n/a&&Grating Turret 2
ADD_REC trecs:cc:grating:motorC.IVAL&& in, STRING, n/a, n/a&&Grating Turret 2
ADD_REC trecs:cc:grating:motorC.IMSS&& in, STRING, n/a, n/a&&Grating Turret 2
ADD_REC trecs:cc:grating:motorC.VAL&& out, STRING, n/a, n/a&&Grating Turret 2
ADD_REC trecs:cc:grating:motorC.OMSS&& out, STRING, n/a, n/a&&Grating Turret 2
~add motor Grating Turret 3
ADD_REC trecs:cc:grating:motorG.A&& in, LONG, n/a, perform_test&&Grating Turret 3
ADD_REC trecs:cc:grating:motorG.C&& in, LONG, n/a, command mode&&Grating Turret 3
ADD_REC trecs:cc:grating:motorG.E&& in, LONG, n/a, datum_motor&&Grating Turret 3
ADD_REC trecs:cc:grating:motorG.G&& in, DOUBLE, n/a, num_steps&&Grating Turret 3
ADD_REC trecs:cc:grating:motorG.I&& in, STRING, n/a, stop_mess&&Grating Turret 3
ADD_REC trecs:cc:grating:motorG.K&& in, STRING, n/a, abort_mess&&Grating Turret 3
ADD_REC trecs:cc:grating:motorG.M&& in, STRING, n/a, debug mode&&Grating Turret 3
ADD_REC trecs:cc:grating:motorG.N&& in, DOUBLE, n/a, init_velocity&&Grating Turret 3
ADD_REC trecs:cc:grating:motorG.O&& in, DOUBLE, n/a, slew_velocity&&Grating Turret 3
ADD_REC trecs:cc:grating:motorG.P&& in, DOUBLE, n/a, acceleration&&Grating Turret 3
ADD_REC trecs:cc:grating:motorG.Q&& in, DOUBLE, n/a, deceleration&&Grating Turret 3
ADD_REC trecs:cc:grating:motorG.R&& in, DOUBLE, n/a, drive_current&&Grating Turret 3
ADD_REC trecs:cc:grating:motorG.S&& in, DOUBLE, n/a, datum_speed&&Grating Turret 3
ADD_REC trecs:cc:grating:motorG.T&& in, LONG, n/a, datum_direction&&Grating Turret 3
ADD_REC trecs:cc:grating:motorG.VALA&& out, LONG, n/a, command mode&&Grating Turret 3
ADD_REC trecs:cc:grating:motorG.VALB&& out, LONG, n/a, socket number&&Grating Turret 3
ADD_REC trecs:cc:grating:motorG.VALC&& out, STRING, n/a, current position&&Grating Turret 3
ADD_REC trecs:cc:grating:motorG.VALD&& out, DOUBLE, n/a, init_velocity&&Grating Turret 3
ADD_REC trecs:cc:grating:motorG.VALE&& out, DOUBLE, n/a, slew_velocity&&Grating Turret 3
ADD_REC trecs:cc:grating:motorG.VALF&& out, DOUBLE, n/a, acceleration&&Grating Turret 3
ADD_REC trecs:cc:grating:motorG.VALG&& out, DOUBLE, n/a, deceleration&&Grating Turret 3
ADD_REC trecs:cc:grating:motorG.VALH&& out, DOUBLE, n/a, drive_current&&Grating Turret 3
ADD_REC trecs:cc:grating:motorG.VALI&& out, DOUBLE, n/a, datum_speed&&Grating Turret 3
ADD_REC trecs:cc:grating:motorG.VALJ&& out, LONG, n/a, datum_direction&&Grating Turret 3
~add motor Cold Clamp 1
ADD_REC trecs:cc:coldClmp:motorApply.A&& in, STRING, n/a, n/a(from cc:coldClmp:test.VAL)&&Cold Clamp 1
ADD_REC trecs:cc:coldClmp:motorApply.B&& in, STRING, n/a, n/a(from cc:coldClmp:init.VAL)&&Cold Clamp 1
ADD_REC trecs:cc:coldClmp:motorApply.C&& in, STRING, n/a, n/a(from cc:coldClmp:datum.VAL)&&Cold Clamp 1
ADD_REC trecs:cc:coldClmp:motorApply.D&& in, STRING, n/a, n/a(from cc:coldClmp:namedPos.VAL)&&Cold Clamp 1
ADD_REC trecs:cc:coldClmp:motorApply.E&& in, STRING, n/a, n/a(from cc:coldClmp:steps.VAL)&&Cold Clamp 1
ADD_REC trecs:cc:coldClmp:motorApply.F&& in, STRING, n/a, n/a(from cc:coldClmp:abort.VAL)&&Cold Clamp 1
ADD_REC trecs:cc:coldClmp:motorApply.G&& in, STRING, n/a, n/a(from cc:coldClmp:stop.VAL)&&Cold Clamp 1
ADD_REC trecs:cc:coldClmp:motorApply.VALA&& out, STRING, n/a, n/a(to cc:coldClmp:test.DIR)&&Cold Clamp 1
ADD_REC trecs:cc:coldClmp:motorApply.VALB&& out, STRING, n/a, n/a(to cc:coldClmp:init.DIR)&&Cold Clamp 1
ADD_REC trecs:cc:coldClmp:motorApply.VALC&& out, STRING, n/a, n/a(to cc:coldClmp:datum.DIR)&&Cold Clamp 1
ADD_REC trecs:cc:coldClmp:motorApply.VALD&& out, STRING, n/a, n/a(to cc:coldClmp:namedPos.DIR)&&Cold Clamp 1
ADD_REC trecs:cc:coldClmp:motorApply.VALE&& out, STRING, n/a, n/a(to cc:coldClmp:steps.DIR)&&Cold Clamp 1
ADD_REC trecs:cc:coldClmp:motorApply.VALF&& out, STRING, n/a, n/a(to cc:coldClmp:abort.DIR)&&Cold Clamp 1
ADD_REC trecs:cc:coldClmp:motorApply.VALG&& out, STRING, n/a, n/a(to cc:coldClmp:stop.DIR)&&Cold Clamp 1
ADD_REC trecs:cc:coldClmp:test.A&& in, STRING, n/a, test directive&&Cold Clamp 1
ADD_REC trecs:cc:coldClmp:test.VALA&& out, LONG, n/a, n/a(internal)&&Cold Clamp 1
ADD_REC trecs:cc:coldClmp:init.A&& in, STRING, n/a, init directive&&Cold Clamp 1
ADD_REC trecs:cc:coldClmp:init.VALA&& out, LONG, n/a, n/a&&Cold Clamp 1
ADD_REC trecs:cc:coldClmp:datum.A&& in, STRING, n/a, datum directive&&Cold Clamp 1
ADD_REC trecs:cc:coldClmp:datum.VALA&& out, LONG, n/a, n/a&&Cold Clamp 1
~add motor Cold Clamp 2
ADD_REC trecs:cc:coldClmp:namedPos.A&& in, STRING, n/a, Position number&&Cold Clamp 2
ADD_REC trecs:cc:coldClmp:namedPos.VALA&& out, LONG, n/a, n/a&&Cold Clamp 2
ADD_REC trecs:cc:coldClmp:namedPos.VALB&& out, DOUBLE, n/a, n/a&&Cold Clamp 2
ADD_REC trecs:cc:coldClmp:namedPos.VALC&& out, TBD, n/a, Position number(to sad:coldClmp:Pos.VAL)&&Cold Clamp 2
ADD_REC trecs:cc:coldClmp:stop.A&& in, STRING, n/a, stop message&&Cold Clamp 2
ADD_REC trecs:cc:coldClmp:stop.VALA&& out, STRING, n/a, n/a&&Cold Clamp 2
ADD_REC trecs:cc:coldClmp:abort.A&& in, STRING, n/a, abort message&&Cold Clamp 2
ADD_REC trecs:cc:coldClmp:abort.VALA&& out, STRING, n/a, n/a&&Cold Clamp 2
ADD_REC trecs:cc:coldClmp:steps.A&& in, STRING, n/a, steps&&Cold Clamp 2
ADD_REC trecs:cc:coldClmp:steps.VALA&& out, DOUBLE, n/a, n/a&&Cold Clamp 2
ADD_REC trecs:cc:coldClmp:debug.DOL&& in, STRING, n/a, n/a&&Cold Clamp 2
ADD_REC trecs:cc:coldClmp:debug.OUT&& out, STRING, n/a, n/a&&Cold Clamp 2
ADD_REC trecs:cc:coldClmp:motorC.IVAL&& in, STRING, n/a, n/a&&Cold Clamp 2
ADD_REC trecs:cc:coldClmp:motorC.IMSS&& in, STRING, n/a, n/a&&Cold Clamp 2
ADD_REC trecs:cc:coldClmp:motorC.VAL&& out, STRING, n/a, n/a&&Cold Clamp 2
ADD_REC trecs:cc:coldClmp:motorC.OMSS&& out, STRING, n/a, n/a&&Cold Clamp 2
~add motor Cold Clamp 3
ADD_REC trecs:cc:coldClmp:motorG.A&& in, LONG, n/a, perform_test&&Cold Clamp 3
ADD_REC trecs:cc:coldClmp:motorG.C&& in, LONG, n/a, command mode&&Cold Clamp 3
ADD_REC trecs:cc:coldClmp:motorG.E&& in, LONG, n/a, datum_motor&&Cold Clamp 3
ADD_REC trecs:cc:coldClmp:motorG.G&& in, DOUBLE, n/a, num_steps&&Cold Clamp 3
ADD_REC trecs:cc:coldClmp:motorG.I&& in, STRING, n/a, stop_mess&&Cold Clamp 3
ADD_REC trecs:cc:coldClmp:motorG.K&& in, STRING, n/a, abort_mess&&Cold Clamp 3
ADD_REC trecs:cc:coldClmp:motorG.M&& in, STRING, n/a, debug mode&&Cold Clamp 3
ADD_REC trecs:cc:coldClmp:motorG.N&& in, DOUBLE, n/a, init_velocity&&Cold Clamp 3
ADD_REC trecs:cc:coldClmp:motorG.O&& in, DOUBLE, n/a, slew_velocity&&Cold Clamp 3
ADD_REC trecs:cc:coldClmp:motorG.P&& in, DOUBLE, n/a, acceleration&&Cold Clamp 3
ADD_REC trecs:cc:coldClmp:motorG.Q&& in, DOUBLE, n/a, deceleration&&Cold Clamp 3
ADD_REC trecs:cc:coldClmp:motorG.R&& in, DOUBLE, n/a, drive_current&&Cold Clamp 3
ADD_REC trecs:cc:coldClmp:motorG.S&& in, DOUBLE, n/a, datum_speed&&Cold Clamp 3
ADD_REC trecs:cc:coldClmp:motorG.T&& in, LONG, n/a, datum_direction&&Cold Clamp 3
ADD_REC trecs:cc:coldClmp:motorG.VALA&& out, LONG, n/a, command mode&&Cold Clamp 3
ADD_REC trecs:cc:coldClmp:motorG.VALB&& out, LONG, n/a, socket number&&Cold Clamp 3
ADD_REC trecs:cc:coldClmp:motorG.VALC&& out, STRING, n/a, current position&&Cold Clamp 3
ADD_REC trecs:cc:coldClmp:motorG.VALD&& out, DOUBLE, n/a, init_velocity&&Cold Clamp 3
ADD_REC trecs:cc:coldClmp:motorG.VALE&& out, DOUBLE, n/a, slew_velocity&&Cold Clamp 3
ADD_REC trecs:cc:coldClmp:motorG.VALF&& out, DOUBLE, n/a, acceleration&&Cold Clamp 3
ADD_REC trecs:cc:coldClmp:motorG.VALG&& out, DOUBLE, n/a, deceleration&&Cold Clamp 3
ADD_REC trecs:cc:coldClmp:motorG.VALH&& out, DOUBLE, n/a, drive_current&&Cold Clamp 3
ADD_REC trecs:cc:coldClmp:motorG.VALI&& out, DOUBLE, n/a, datum_speed&&Cold Clamp 3
ADD_REC trecs:cc:coldClmp:motorG.VALJ&& out, LONG, n/a, datum_direction&&Cold Clamp 3


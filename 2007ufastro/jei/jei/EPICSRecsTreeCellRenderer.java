package ufjei;

import java.awt.*;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.JTree.*;

/**
 * Title:        JAVA Engineering Console
 * Description:
 * Copyright:    Copyright (c) Latif Albusairi and Tom Kisko
 * Company:      University of Florida
 * @author David Rashkin, Latif Albusairi and Tom Kisko
 * @version 1.0
 */


//===============================================================================
/**
 * Handles the Records Tree
 */
public class EPICSRecsTreeCellRenderer extends DefaultTreeCellRenderer {


//-------------------------------------------------------------------------------
  /**
   * Constructor
   * Override the standard renderer so it uses the EPICS info to determine the icon.
   */
  public EPICSRecsTreeCellRenderer() {
  } //end of EPICSRecsTreeCellRenderer


//-------------------------------------------------------------------------------
  /**
   * Returns a TreeCell Component
   *@param tree JTree: TBD
   *@param value Object: TBD
   *@param sel boolean: TBD
   *@param expanded boolean: TBD
   *@param leaf boolean: TBD
   *@param row int: TBD
   *@param hasFocus boolean: TBD
   */
  public Component getTreeCellRendererComponent(JTree tree, Object value,
         boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus)
  {
    String stringValue = tree.convertValueToText(value, sel, expanded,
                                                 leaf, row, hasFocus);
    String type = ((EPICSRecsNode)(((DefaultMutableTreeNode)value).getUserObject())).type;
    setText(stringValue);
    Icon statusIcon = null;
    if(sel) setForeground(getTextSelectionColor());
    else    setForeground(getTextNonSelectionColor());
    // This is the only part that changes
    if (type.equals("sect")) {
      //statusIcon = new ImageIcon(jei.data_path + "icon_sect.gif");
      if (expanded) setIcon(getOpenIcon());
      else setIcon(getClosedIcon());
    }
    else if (type.equals("rec")) {
      statusIcon = new ImageIcon(jei.data_path + "icon_rec.gif");
      //setIcon(statusIcon);
      setIcon(null);
    }
    else {
      setIcon(getLeafIcon());
    }
    //setOpenIcon(statusIcon);
    //setClosedIcon(statusIcon);
    //setLeafIcon(statusIcon);
    //if (leaf) {
    //  setIcon(getLeafIcon());
    //} else if (expanded) {
    //  setIcon(getOpenIcon());
    //} else {
    //  setIcon(getClosedIcon());
    //}
    selected = sel;
    return this;
  } //end of getTreeCellRendererComponent

} //end of Class EPICSRecsTreeCellRenderer

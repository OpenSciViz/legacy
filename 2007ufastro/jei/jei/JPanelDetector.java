package ufjei;
import java.awt.*;
import java.awt.event.*;
import jca.event.MonitorEvent;
import jca.dbr.DBR_DBString;
import jca.Ca;
import javax.swing.*;
import javax.swing.SwingConstants;
import javax.swing.border.*;
import java.io.*;

/**
 * Custom panel for detector control
 * @see java.swingx.JPanel
 * @authors David Rashkin, Thomas Kisko, and Frank Varosi
 */

//===============================================================================
/**
 * Detector Control Tabbed pane
 */
public class JPanelDetector extends JPanel {
  public static final String rcsID = "$Name:  $ $Id: JPanelDetector.java,v 0.29 2003/10/16 21:40:18 varosi beta $";

    // Text field indices for input and adjusted parameters:
    public static final int FRMTIME = 0;
    public static final int OBJTIME = 1;
    public static final int SAVEFREQ = 2;
    public static final int CHOPDUTY = 3;
    public static final int CHOPFREQ = 4;
    public static final int NODTIME = 5;
    public static final int NODDELAY = 6;
    public static final int PREVALID = 7;
    public static final int POSTVALID = 8;
    public static final int NODDUTY = 9;   //this and following are in adjusted parms. only
    public static final int SCSFREQ = 10;
    public static final int CHOPDELAY = 11;
    boolean needConfigure = true;
    boolean newAgentConnection = true;
    public static final int NinParms = 9;   // # of input parms
    public static final int NadjParms = 12; // # of adjusted parms
    public static final int NCTcnts = 10;   // # of CT counters
    public static final int ctiPixClock = 0;
    public static final int ctiFrameCoadds = 1;
    public static final int ctiChopSettleReads = 2;
    public static final int ctiChopCoadds = 3;
    public static final int ctiNodSettleChops = 4;
    public static final int ctiNodSettleReads = 5;
    public static final int ctiNodSets = 6;
    public static final int ctiPreValidChops = 7;
    public static final int ctiPostValidChops = 8;
    public static final int ctiSaveSets = 9;

    jeiCmd jeiCommand = new jeiCmd();

  //Construct the frame
  JLabel jLabelObsMode = new JLabel("      Observing Mode ");
  EPICSComboBox jComboBoxObservingMode;
  JLabel jLabelReadMode = new JLabel("      Readout Mode ");
  EPICSComboBox jComboBoxReadoutMode;
  JLabel jLabelNodMode = new JLabel("      Nod-Handshake ");
  EPICSComboBox jComboBoxNodHandshake;
  JLabel jLabelAcqMode = new JLabel("      Acq Mode ");
  JComboBox jComboBoxAcqMode;
  //to go in jPanelInputParameters
  JLabel[] labelGroup = new JLabel[NinParms];                     // Input Parameters Labels
  EPICSTextField[] textGroup = new EPICSTextField[NinParms];      // Input Parameters ETFs
  //to go in jPanelAdjustedParameters
  JLabel[] labelGroupAdj = new JLabel[NadjParms];                  // Adjusted Parameters Labels
  EPICSTextField[] textGroupAdj = new EPICSTextField[NadjParms];   // Adjusted Parameters ETFs
  //to go in jPanelCTcounters
  JButton applyCTcntsButton = new JButton("Apply CT counters");
  JLabel[] CTcLabelGroup = new JLabel[NCTcnts];                  // CT counters Labels
  EPICSTextField[] CTcTextGroup = new EPICSTextField[NCTcnts];   // CT counters ETFs
  JCheckBox jCheckBoxCTLock = new JCheckBox("Locked",true);
  JTextField jTextFieldTotalFrames = new JTextField("?");;
  //to go in jPanelButtons
  JButton connectButton = new JButton("Connect");
  JButton computeButton = new JButton("COMPUTE  >  CT counters");
  JButton configureButton = new JButton("CONFIGURE  >  MCE4 and AcqServer");
  JButton efficButton = new JButton("Efficiency  ?");
  JButton startButton = new JButton("START");
  JButton abortButton = new JButton("ABORT");
  JButton reInitButton = new JButton("Re-INIT Agent-Acq.Server");
  JPanel jPanelInputParameters = new JPanel();
  JPanel jPanelAdjustedParameters = new JPanel();
  JPanel jPanelTop = new JPanel();
  JPanel jPanelButtons = new JPanel();
  JPanel jPanelCTcounters = new JPanel();
  GridLayout gridLayoutInput = new GridLayout();
  GridLayout gridLayoutAdjusted = new GridLayout();
  GridLayout gridLayoutCTcounters = new GridLayout();
  RatioLayout mainLayout = new RatioLayout();
  JDialogDetectorEfficiency efficientWindow = new JDialogDetectorEfficiency();
  FlowLayout flowLayoutTop = new FlowLayout();
  FlowLayout flowLayoutButtons = new FlowLayout();

  //read-only EPICS info Labels (added at bottom of frame):
  EPICSLabel HeartBeatLabel = new EPICSLabel(EPICS.prefix + "dc:heartbeat",
					     EPICS.prefix + "dc:HeartBeat:");
  EPICSLabel BeamLabel = new EPICSLabel(EPICS.prefix + "sad:currentBeam","Beam:");
  EPICSLabel NodCycleLabel = new EPICSLabel(EPICS.prefix + "sad:currentNodCycle","Nod Cycle:");
  EPICSLabel fcountLabel = new EPICSLabel(EPICS.prefix + "dc:acqControlG.VALI","Frame # ");
  JLabel totFramesLabel = new JLabel("out of  ?");
  EPICSLabel CAR_CompLabel = new EPICSLabel(EPICS.prefix + "dc:physicalC.VAL","COMPUTE: CAR =",configureButton);
  EPICSLabel CAR_HardLabel = new EPICSLabel(EPICS.prefix + "dc:hardwareC.VAL","CT: CAR =");
  EPICSLabel CAR_AcqLabel = new EPICSLabel(EPICS.prefix + "dc:acqControlC.VAL","CONTROL: CAR =",startButton);
  EPICSLabel CAR_ObsLabel = new EPICSLabel(EPICS.prefix + "observeC.VAL","OBSERVE: CAR =");
  EPICSLabel errMessCompCAD = new EPICSLabel(EPICS.prefix + "dc:physical.MESS","Message:");
  EPICSLabel errMessCompLabel = new EPICSLabel(EPICS.prefix + "dc:physicalC.OMSS","Message:",connectButton);
    //EPICSLabel errMessHardCAD = new EPICSLabel(EPICS.prefix + "dc:hardware.MESS","Msg:");
  EPICSLabel errMessHardLabel = new EPICSLabel(EPICS.prefix + "dc:hardwareC.OMSS","Msg:");
  EPICSLabel errMessAcqCAD = new EPICSLabel(EPICS.prefix+"dc:acqControl.MESS","Message:");
  EPICSLabel errMessAcqLabel = new EPICSLabel(EPICS.prefix+"dc:acqControlC.OMSS","Message:",connectButton);
    //EPICSLabel errMessObsCAD = new EPICSLabel(EPICS.prefix + "observe.MESS","Message:");
  EPICSLabel errMessObsLabel = new EPICSLabel(EPICS.prefix + "observeC.OMSS","Message:");

    String[] DataModes = {"","save","discard","discard-all","no-dhs"};
    EPICSComboBox jComboBoxArchive = new EPICSComboBox(EPICS.prefix + "dc:acqControl.C",
							    EPICS.prefix + "dc:acqControlG.VALE",
							    DataModes, EPICSComboBox.ITEM);
    //read-only info about Local archiving:
    EPICSLabel jLabelArchiveHost = new EPICSLabel(EPICS.prefix + "sad:localFileDir","Host=");
    EPICSTextField jTextFieldArchiveFile = new EPICSTextField( EPICS.prefix+"sad:localFileName","Local" );
    EPICSTextField jTextFieldObsNote = new EPICSTextField(EPICS.prefix + "dc:acqControl.J",
							  EPICS.prefix + "dc:acqControlG.VALQ",
							  "ObsNote for FITS header", true);
//-------------------------------------------------------------------------------
  /**
   * JPanelDetector default constructor.  Calls private method
   * jbInit(), which does all compoment initialization, and reads
   * in parameters from the input file
   */
  public JPanelDetector() {
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    try {
      jbInitDetPanel();
    }
    catch (Exception e) { e.printStackTrace(); }
  } //end of JPanelDetector

//-------------------------------------------------------------------------------
  /**
   *Component initialization
   */
  private void jbInitDetPanel() throws Exception
  {
    connectButton.addActionListener(new java.awt.event.ActionListener()
	{ public void actionPerformed(ActionEvent e) { connectButton_action(e); } });

    String [] combobox_items_1 = {"", "chop-nod", "chop", "nod", "stare"};
    jComboBoxObservingMode = new EPICSComboBox( EPICS.prefix + "dc:physical.I",
						EPICS.prefix + "dc:physicalG.VALI",
						combobox_items_1, EPICSComboBox.ITEM, false );

    String [] combobox_items_2 = {"", "S1R3", "S1", "S1R1", "S1R1_CR",
				  "S1R3_raw", "S1R1_raw","S1R1_CR_raw" };
    jComboBoxReadoutMode = new EPICSComboBox( EPICS.prefix + "dc:physical.J",
					      EPICS.prefix + "dc:physicalG.VALJ",
					      combobox_items_2, EPICSComboBox.ITEM, false );

    String [] combobox_items_3 = {"", "TRUE", "FALSE"};
    jComboBoxNodHandshake = new EPICSComboBox( EPICS.prefix + "dc:acqControl.G",
					       EPICS.prefix + "dc:acqControlG.VALH",
					       combobox_items_3, EPICSComboBox.ITEM ); //auto put epics
    jComboBoxAcqMode = new JComboBox();
    jComboBoxAcqMode.addItem("normal");
    jComboBoxAcqMode.addItem("Fast");

    jComboBoxAcqMode.addActionListener( new java.awt.event.ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		if( jComboBoxAcqMode.getSelectedItem().equals("Fast") ) {
		    jComboBoxAcqMode.setBackground(Color.red);
		    jComboBoxAcqMode.setForeground(Color.white);
		} else {
		    jComboBoxAcqMode.setBackground(Color.white);
		    jComboBoxAcqMode.setForeground(Color.black);
		}
	    }
	});

    labelGroup[FRMTIME] = new JLabel("Frame Time  (ms):");
    labelGroup[SAVEFREQ] = new JLabel("Save Frequency (Hz):");
    labelGroup[OBJTIME] = new JLabel("On Source Time (min):");
    labelGroup[CHOPFREQ] = new JLabel("Chop Frequency  (Hz):");
    labelGroup[CHOPDUTY] = new JLabel("max SCS Duty Cycle (%):");
    labelGroup[NODTIME] = new JLabel("Nod Time (sec):");
    labelGroup[NODDELAY] = new JLabel("Nod Settle (sec):");
    labelGroup[PREVALID] = new JLabel("Pre-valid Chop Time (sec):");
    labelGroup[POSTVALID] = new JLabel("Post-valid Chop Time (sec):");

    textGroup[FRMTIME]  = new EPICSTextField(EPICS.prefix + "dc:physical.B",
					     EPICS.prefix + "dc:physicalG.VALC",
					     labelGroup[FRMTIME].getText());
    textGroup[SAVEFREQ] = new EPICSTextField(EPICS.prefix + "dc:physical.C",
					     EPICS.prefix + "dc:physicalG.VALD",
					     labelGroup[SAVEFREQ].getText());
    textGroup[OBJTIME]  = new EPICSTextField(EPICS.prefix + "dc:physical.D",
					     EPICS.prefix + "dc:physicalG.VALE",
					     labelGroup[OBJTIME].getText());
    textGroup[CHOPFREQ] = new EPICSTextField(EPICS.prefix + "dc:physical.E",
					     EPICS.prefix + "dc:physicalG.VALF",
					     labelGroup[CHOPFREQ].getText());
    textGroup[CHOPDUTY] = new EPICSTextField(EPICS.prefix + "dc:physical.F",
					     EPICS.prefix + "dc:SCSDtyCcl.VAL",
					     labelGroup[CHOPDUTY].getText());
    textGroup[NODTIME]  = new EPICSTextField(EPICS.prefix + "dc:physical.G",
					     EPICS.prefix + "dc:physicalG.VALG",
					     labelGroup[NODTIME].getText());
    textGroup[NODDELAY] = new EPICSTextField(EPICS.prefix + "dc:physical.H",
					     EPICS.prefix + "dc:physicalG.VALH",
					     labelGroup[NODDELAY].getText());
    textGroup[PREVALID] = new EPICSTextField(EPICS.prefix + "dc:physical.L",
					     EPICS.prefix + "dc:physicalG.VALK",
					     labelGroup[PREVALID].getText());
    textGroup[POSTVALID] = new EPICSTextField(EPICS.prefix + "dc:physical.M",
					      EPICS.prefix + "dc:physicalG.VALL",
					      labelGroup[POSTVALID].getText());

    labelGroupAdj[FRMTIME] = new JLabel("Frame Time  (ms):");
    labelGroupAdj[SAVEFREQ] = new JLabel("Save Frequency (Hz):");
    labelGroupAdj[OBJTIME] = new JLabel("On Source Time (min):");
    labelGroupAdj[CHOPFREQ] = new JLabel("Chop Frequency  (Hz):");
    labelGroupAdj[CHOPDUTY] = new JLabel("Chop Duty Cycle (%):");
    labelGroupAdj[NODTIME] = new JLabel("Nod Time (sec):");
    labelGroupAdj[NODDELAY] = new JLabel("Nod Settle (sec):");
    labelGroupAdj[NODDUTY] = new JLabel("Nod Duty Cycle (%):");
    labelGroupAdj[CHOPDELAY] = new JLabel("Chop Settle Time (ms)");
    labelGroupAdj[SCSFREQ] = new JLabel("SCS Frequency (Hz):");
    labelGroupAdj[PREVALID] = new JLabel("Pre-valid Chop Time (sec):");
    labelGroupAdj[POSTVALID] = new JLabel("Post-valid Chop Time (sec):");

    textGroupAdj[FRMTIME]   = new EPICSTextField(EPICS.prefix + "dc:physOutG.VALA",
						 labelGroupAdj[FRMTIME].getText());
    textGroupAdj[SAVEFREQ]  = new EPICSTextField(EPICS.prefix + "dc:physOutG.VALB",
						 labelGroupAdj[SAVEFREQ].getText());
    textGroupAdj[OBJTIME]   = new EPICSTextField(EPICS.prefix + "dc:physOutG.VALC",
						 labelGroupAdj[OBJTIME].getText());
    textGroupAdj[CHOPFREQ]  = new EPICSTextField(EPICS.prefix + "dc:physOutG.VALD",
						 labelGroupAdj[CHOPFREQ].getText());
    textGroupAdj[CHOPDUTY]  = new EPICSTextField(EPICS.prefix + "dc:physOutG.VALH",
						 labelGroupAdj[CHOPDUTY].getText());
    textGroupAdj[NODTIME]   = new EPICSTextField(EPICS.prefix + "dc:physOutG.VALF",
						 labelGroupAdj[NODTIME].getText());
    textGroupAdj[NODDELAY]  = new EPICSTextField(EPICS.prefix + "dc:physOutG.VALG",
						 labelGroupAdj[NODDELAY].getText());
    textGroupAdj[NODDUTY]   = new EPICSTextField(EPICS.prefix + "dc:physOutG.VALI",
						 labelGroupAdj[NODDUTY].getText());
    textGroupAdj[CHOPDELAY] = new EPICSTextField(EPICS.prefix + "dc:physOutG.VALE",
						 labelGroupAdj[CHOPDELAY].getText());
    textGroupAdj[SCSFREQ]   = new EPICSTextField(EPICS.prefix + "dc:physOutG.VALJ",
						 labelGroupAdj[SCSFREQ].getText());
    textGroupAdj[PREVALID]  = new EPICSTextField(EPICS.prefix + "dc:physOutG.VALK",
						 labelGroupAdj[PREVALID].getText());
    textGroupAdj[POSTVALID] = new EPICSTextField(EPICS.prefix + "dc:physOutG.VALL",
						 labelGroupAdj[POSTVALID].getText());

    this.setLayout(mainLayout);
    this.setSize(new Dimension(838, 579));

    computeButton.addActionListener(new java.awt.event.ActionListener() {
	    public void actionPerformed(ActionEvent e) { computeButton_action(e); } });

    configureButton.addActionListener(new java.awt.event.ActionListener() {
	    public void actionPerformed(ActionEvent e) { configureButton_action(e); } });

    efficButton.addActionListener(new java.awt.event.ActionListener() {
	    public void actionPerformed(ActionEvent e) { efficButton_action(e); } });

    startButton.addActionListener(new java.awt.event.ActionListener() {
	    public void actionPerformed(ActionEvent e) { startButton_action(e); } });

    abortButton.addActionListener(new java.awt.event.ActionListener() {
	    public void actionPerformed(ActionEvent e) { abortButton_action(e); } });

    reInitButton.addActionListener(new java.awt.event.ActionListener() {
	    public void actionPerformed(ActionEvent e) { reInitButton_action(e); } });

    flowLayoutTop.setHgap(0);
    jPanelTop.setLayout(flowLayoutTop);
    jPanelTop.add(connectButton, null);
    jPanelTop.add(jLabelObsMode, null);
    jPanelTop.add(jComboBoxObservingMode, null);
    jPanelTop.add(jLabelReadMode, null);
    jPanelTop.add(jComboBoxReadoutMode, null);
    jPanelTop.add(jLabelNodMode, null);
    jPanelTop.add(jComboBoxNodHandshake, null);
    jPanelTop.add(jLabelAcqMode, null);
    jPanelTop.add(jComboBoxAcqMode, null);

    jPanelButtons.setLayout(flowLayoutButtons);
    jPanelButtons.add(computeButton);
    jPanelButtons.add(configureButton);
    jPanelButtons.add(efficButton);
    jPanelButtons.add(startButton);
    jPanelButtons.add(abortButton);
    jPanelButtons.add(reInitButton);

    jPanelInputParameters.setLayout(gridLayoutInput);
    jPanelInputParameters.setBorder(new EtchedBorder(0));
    jPanelAdjustedParameters.setLayout(gridLayoutAdjusted);
    jPanelAdjustedParameters.setBorder(new EtchedBorder(0));
    jPanelCTcounters.setLayout(gridLayoutCTcounters);
    jPanelCTcounters.setBorder(new EtchedBorder(0));
    gridLayoutInput.setColumns(2);
    gridLayoutInput.setRows(0);
    gridLayoutAdjusted.setColumns(2);
    gridLayoutAdjusted.setRows(0);
    gridLayoutCTcounters.setColumns(2);
    gridLayoutCTcounters.setRows(0);

    JLabel dummyRightLabel = new JLabel("Input ",JLabel.RIGHT);
    JLabel dummyLeftLabel = new JLabel("Parameters",JLabel.LEFT);
    Font dummyFont = this.getFont();
    dummyFont = new Font (this.getFont().getName(),this.getFont().getStyle(),this.getFont().getSize()+2);
    dummyRightLabel.setFont(dummyFont);
    dummyLeftLabel.setFont(dummyFont);
    jPanelInputParameters.add(dummyRightLabel);
    jPanelInputParameters.add(dummyLeftLabel);

    for (int i=0; i<NinParms; i++) {
      jPanelInputParameters.add(labelGroup[i]);
      jPanelInputParameters.add(textGroup[i]);
    }

    dummyRightLabel = new JLabel("Adjusted ",JLabel.RIGHT);
    dummyLeftLabel = new JLabel("Parameters",JLabel.LEFT);
    dummyRightLabel.setFont(dummyFont);
    dummyLeftLabel.setFont(dummyFont);
    jPanelAdjustedParameters.add(dummyRightLabel);
    jPanelAdjustedParameters.add(dummyLeftLabel);

    for (int i=0; i<NadjParms; i++) {
      jPanelAdjustedParameters.add(labelGroupAdj[i]);
      textGroupAdj[i].setEditable(false);
      textGroupAdj[i].setBackground(new Color(175,175,175));
      jPanelAdjustedParameters.add(textGroupAdj[i]);
    }

    CTcLabelGroup[ctiFrameCoadds] = new JLabel ("FrameCoadds:");
    CTcLabelGroup[ctiChopSettleReads] = new JLabel ("ChopSettleReads:");
    CTcLabelGroup[ctiChopCoadds] = new JLabel ("ChopCoadds:");
    CTcLabelGroup[ctiNodSettleChops] = new JLabel ("NodSettleChops:");
    CTcLabelGroup[ctiNodSettleReads] = new JLabel ("NodSettleReads:");
    CTcLabelGroup[ctiSaveSets] = new JLabel ("SaveSets:");
    CTcLabelGroup[ctiNodSets] = new JLabel ("NodSets:");
    CTcLabelGroup[ctiPixClock] = new JLabel ("PixClock:");
    CTcLabelGroup[ctiPreValidChops] = new JLabel ("PreValidChops:");
    CTcLabelGroup[ctiPostValidChops] = new JLabel ("PostValidChops:");
    
    CTcTextGroup[ctiFrameCoadds] = new EPICSTextField(EPICS.prefix + "dc:hardware.B",
						     EPICS.prefix + "dc:hardwareG.VALC",
						     CTcLabelGroup[ctiFrameCoadds].getText() );
    CTcTextGroup[ctiChopSettleReads] = new EPICSTextField(EPICS.prefix + "dc:hardware.C",
							 EPICS.prefix + "dc:hardwareG.VALD",
							 CTcLabelGroup[ctiChopSettleReads].getText() );
    CTcTextGroup[ctiChopCoadds] = new EPICSTextField(EPICS.prefix + "dc:hardware.D",
						    EPICS.prefix + "dc:hardwareG.VALE",
						    CTcLabelGroup[ctiChopCoadds].getText() );
    CTcTextGroup[ctiNodSettleChops] = new EPICSTextField(EPICS.prefix + "dc:hardware.E",
							EPICS.prefix + "dc:hardwareG.VALF",
							CTcLabelGroup[ctiNodSettleChops].getText() );
    CTcTextGroup[ctiNodSettleReads] = new EPICSTextField(EPICS.prefix + "dc:hardware.F",
							EPICS.prefix + "dc:hardwareG.VALG",
							CTcLabelGroup[ctiNodSettleReads].getText() );
    CTcTextGroup[ctiSaveSets] = new EPICSTextField(EPICS.prefix + "dc:hardware.G",
						  EPICS.prefix + "dc:hardwareG.VALH",
						  CTcLabelGroup[ctiSaveSets].getText() );
    CTcTextGroup[ctiNodSets] = new EPICSTextField(EPICS.prefix + "dc:hardware.H",
						 EPICS.prefix + "dc:hardwareG.VALI",
						 CTcLabelGroup[ctiNodSets].getText() );
    CTcTextGroup[ctiPixClock] = new EPICSTextField(EPICS.prefix + "dc:hardware.I",
						  EPICS.prefix + "dc:hardwareG.VALJ",
						  CTcLabelGroup[ctiPixClock].getText() );
    CTcTextGroup[ctiPreValidChops] = new EPICSTextField(EPICS.prefix + "dc:hardware.J",
						       EPICS.prefix + "dc:hardwareG.VALK",
						       CTcLabelGroup[ctiPreValidChops].getText() );
    CTcTextGroup[ctiPostValidChops] = new EPICSTextField(EPICS.prefix + "dc:hardware.K",
							EPICS.prefix + "dc:hardwareG.VALL",
							CTcLabelGroup[ctiPostValidChops].getText() );

    applyCTcntsButton.setDefaultCapable(false);
    applyCTcntsButton.setEnabled(false);
    jPanelCTcounters.add(applyCTcntsButton);
    jPanelCTcounters.add(jCheckBoxCTLock);

    applyCTcntsButton.addActionListener(new java.awt.event.ActionListener() {
	    public void actionPerformed(ActionEvent e) { applyCTcntsButton_action(e); }	});

    jCheckBoxCTLock.addActionListener(new java.awt.event.ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		Color color;
		boolean editable;
		if (jCheckBoxCTLock.isSelected()) {
		    color = new Color (175,175,175);
		    editable = false;
		    applyCTcntsButton.setEnabled(false);
		}
		else {
		    color = new Color (255,255,255);
		    editable = true;
		    applyCTcntsButton.setEnabled(true);
		}
		for (int i=0; i<NCTcnts; i++) {
		    CTcTextGroup[i].setBackground(color);
		    CTcTextGroup[i].setEditable(editable);
		}
	    }
	});

    for (int i=0; i<NCTcnts; i++) {
      CTcTextGroup[i].setBackground(new Color(175,175,175));
      CTcTextGroup[i].setEditable(false);
      jPanelCTcounters.add(CTcLabelGroup[i]);
      jPanelCTcounters.add(CTcTextGroup[i]);
    }

    jPanelCTcounters.add(new JLabel("Total Save Frames:"));
    jTextFieldTotalFrames.setBackground(Color.white);
    jTextFieldTotalFrames.setEditable(false);
    jPanelCTcounters.add(jTextFieldTotalFrames);
    jPanelCTcounters.add(CAR_HardLabel);
    jPanelCTcounters.add(errMessHardLabel);

    JPanel jPanelArchComBox = new JPanel();
    jPanelArchComBox.setLayout( new FlowLayout() );
    jPanelArchComBox.add(new JLabel("Data Mode =",JLabel.LEFT));
    jPanelArchComBox.add(jComboBoxArchive);
    jPanelArchComBox.add(jLabelArchiveHost);

    JPanel jPanelArchObsNote = new JPanel();
    jPanelArchObsNote.setLayout( new RatioLayout() );
    jPanelArchObsNote.add("0.0,0.0;0.2,1.0", new JLabel("User Note:"));
    jPanelArchObsNote.add("0.2,0.0;0.8,1.0", jTextFieldObsNote);

    JPanel jPanelArchive = new JPanel();
    jPanelArchive.setLayout( new RatioLayout() );
    jPanelArchive.add("0.0,0.00;1.0,0.45", jPanelArchComBox );
    jPanelArchive.add("0.0,0.45;1.0,0.25", jTextFieldArchiveFile);
    jPanelArchive.add("0.0,0.70;1.0,0.30", jPanelArchObsNote );

    jTextFieldArchiveFile.setEditable(false);
    jTextFieldArchiveFile.setBackground(Color.white);

    this.add("0.00,0.02;1.00,0.07", jPanelTop);
    this.add("0.01,0.10;0.33,0.51", jPanelInputParameters);
    this.add("0.01,0.61;0.33,0.15", jPanelArchive);
    this.add("0.35,0.10;0.33,0.66", jPanelAdjustedParameters);
    this.add("0.69,0.10;0.30,0.66", jPanelCTcounters);
    this.add("0.00,0.77;1.00,0.08", jPanelButtons);

    HeartBeatLabel.setHorizontalAlignment(JLabel.LEFT);
    BeamLabel.setHorizontalAlignment(JLabel.LEFT);
    NodCycleLabel.setHorizontalAlignment(JLabel.LEFT);
    fcountLabel.setHorizontalAlignment(JLabel.LEFT);
    totFramesLabel.setHorizontalAlignment(JLabel.LEFT);
    CAR_CompLabel.setHorizontalAlignment(JLabel.LEFT);
    CAR_AcqLabel.setHorizontalAlignment(JLabel.LEFT);
    CAR_ObsLabel.setHorizontalAlignment(JLabel.LEFT);
    errMessCompLabel.setHorizontalAlignment(JLabel.LEFT);
    errMessAcqLabel.setHorizontalAlignment(JLabel.LEFT);
    errMessObsLabel.setHorizontalAlignment(JLabel.LEFT);

    // read-only EPICS information Labels:
    this.add("0.02,0.85;0.30,0.05", HeartBeatLabel);
    this.add("0.02,0.90;0.16,0.05", BeamLabel);
    this.add("0.18,0.90;0.17,0.05", NodCycleLabel);
    this.add("0.02,0.95;0.18,0.05", fcountLabel);
    this.add("0.20,0.95;0.15,0.05", totFramesLabel);
    this.add("0.35,0.85;0.20,0.05", CAR_CompLabel);
    this.add("0.35,0.90;0.20,0.05", CAR_AcqLabel);
    this.add("0.35,0.95;0.20,0.05", CAR_ObsLabel);
    this.add("0.55,0.85;0.45,0.05", errMessCompCAD);
    this.add("0.55,0.85;0.45,0.05", errMessCompLabel);
    this.add("0.55,0.90;0.45,0.05", errMessAcqCAD);
    this.add("0.55,0.90;0.45,0.05", errMessAcqLabel);
    this.add("0.55,0.95;0.45,0.05", errMessObsLabel);

    for (int i=0; i<NinParms; i++) {
	String parm = textGroup[i].getText();
	if( parm.equals("-1.00") || parm.equals("string") || parm.equals("") ) {
	    if( i == FRMTIME ) textGroup[i].setText("100");
	    else if( i == SAVEFREQ ) textGroup[i].setText("1");
	    else if( i == OBJTIME ) textGroup[i].setText("600");
	    else if( i == CHOPFREQ ) textGroup[i].setText("1");
	    else if( i == CHOPDUTY ) textGroup[i].setText("90");
	    else if( i == NODTIME ) textGroup[i].setText("33");
	    else if( i == NODDELAY ) textGroup[i].setText("4");
	    else textGroup[i].setText("1");
	}
    }

    checkConnection(true);

  } //end of jbInit().

//-------------------------------------------------------------------------------
  /**
   * JPanelDetector#Connect button action performed
   * Sends commands to EPICS dc:physicalG & dc:hardwareG records
   *  directing EPICS gensubs to connect via sockets to the Detector Control Agent,
   *  allowing direct engineering access to detector control parameters.
   *@param e not used
   */
  void connectButton_action(ActionEvent e)
    {
	connectButton.setText("Connecting");
	jeiCommand.execute("PUT " + EPICS.prefix + "dc:acqControl.E 0");
	jeiCommand.execute("PUT " + EPICS.prefix + "dc:acqControl.DIR START");
	try { Thread.sleep(990); } catch (Exception _e) {}

	jeiCommand.execute("PUT " + EPICS.prefix + "dc:physical.A 0");
	jeiCommand.execute("PUT " + EPICS.prefix + "dc:physical.DIR START");
	try { Thread.sleep(990); } catch (Exception _e) {}
	
	checkConnection(true);
	newAgentConnection = true;
    }

//-------------------------------------------------------------------------------

    void checkConnection( boolean indicateOK )
    {
	String socp = EPICS.get(EPICS.prefix + "dc:physicalG.VALB");
	String soca = EPICS.get(EPICS.prefix + "dc:acqControlG.VALB");

	if( socp.trim().equals("-1") || soca.trim().equals("-1") ||
	    checkforBadSocket(errMessCompLabel.getText()) ||
	    checkforBadSocket(errMessAcqLabel.getText()) )
	    { 
		connectButton.setBackground(Color.red);
		connectButton.setForeground(Color.white);
		connectButton.setText("Connect");
	    }
	else if( indicateOK ) {
	    connectButton.setBackground(Color.green);
	    connectButton.setForeground(Color.black);
	    connectButton.setText("Connected");
	}
    }

//-------------------------------------------------------------------------------

    boolean checkforBadSocket( String EPICSmessage )
    {
	if( EPICSmessage.toUpperCase().indexOf("BAD SOCKET") >= 0 ||
	    EPICSmessage.toUpperCase().indexOf("TIMED OUT") > 0   ||
	    EPICSmessage.toUpperCase().indexOf("ERROR CONNECTING") >= 0 )
	    return true;
	else
	    return false;
    }

//-------------------------------------------------------------------------------
    /**
     * JPanelDetector# apply CT counters button action performed
     *@param e not used
     */
    void applyCTcntsButton_action(ActionEvent e)
    {
	String soch = EPICS.get(EPICS.prefix + "dc:hardwareG.VALB"); 

	if( soch.equals("-1") || CAR_HardLabel.getText().equals("ERR")
	    || checkforBadSocket(errMessHardLabel.getText()) ) {
	    applyCTcntsButton.setText("Connecting");
	    String cmd = "PUT " + EPICS.prefix + "dc:hardware.A 0";
	    jeiCommand.execute(cmd);
	    try { Thread.sleep(700); } catch (Exception _e) {}
	    cmd = "PUT " + EPICS.prefix + "dc:hardware.DIR START ;connecting";
	    jeiCommand.execute(cmd);
	    try { Thread.sleep(700); } catch (Exception _e) {}
	    applyCTcntsButton.setText("Apply CT counters");
	}

	for (int i=0; i<NCTcnts; i++) CTcTextGroup[i].forcePut();
	putcmd( EPICS.prefix + "dc:hardware.M", jComboBoxReadoutMode._getSelectedItem() );

	String cmd = "PUT " + EPICS.prefix + "dc:hardware.DIR START ;" + applyCTcntsButton.getText();
	jeiCommand.execute(cmd);
	needConfigure = true;

	jTextFieldTotalFrames.setText( TotalFrames() + " " );
    }

//-------------------------------------------------------------------------------
    /**
     * JPanelDetector#compute button action performed
     * Directs the Detector Control Agent to compute adjusted dc:physicalG & dc:hardwareG record values
     * based on user input parameters and electronic/mechanical limitations of instrument.
     *@param e not used
     */
    void computeButton_action(ActionEvent e)
    {
	//get rid of annoying junk left in records when EPICS is rebooted:
	boolean defaultSet = false;
	for (int i=0; i<NinParms; i++) {
	    String parm = textGroup[i].getText().trim();
	    if( parm.equals("-1.00") || parm.equals("string") || parm.equals("") ) {
		if( i == FRMTIME ) textGroup[i].setText("100");
		else if( i == SAVEFREQ ) textGroup[i].setText("1");
		else if( i == OBJTIME ) textGroup[i].setText("700");
		else if( i == CHOPFREQ ) textGroup[i].setText("1");
		else if( i == CHOPDUTY ) textGroup[i].setText("90");
		else if( i == NODTIME ) textGroup[i].setText("33");
		else if( i == NODDELAY ) textGroup[i].setText("4");
		else if( i == PREVALID ) textGroup[i].setText("7");
		else textGroup[i].setText("1");
		defaultSet = true;
	    }
	}
	if( defaultSet ) return;

	for (int i=0; i<NinParms; i++) {
	    if( textGroup[i].getText().trim().equals("") ) {
		JOptionPane.showMessageDialog(this, "Need to specify all Input Parameters!",
					      "ERROR", JOptionPane.ERROR_MESSAGE);
		return;
	    }
	}

	if( needModeSelect() ) return;

	for (int i=0; i<NinParms; i++) textGroup[i].forcePut();
	jComboBoxObservingMode.forcePut();
	jComboBoxReadoutMode.forcePut();

	//jeiCommand.execute("PUT " + EPICS.prefix + "dc:physical.K  0 ;workaround bug in CAD/CAR/gensub");
	jeiCommand.execute("PUT " + EPICS.prefix + "dc:physical.DIR START ;" + computeButton.getText() );
	needConfigure = true;
	newAgentConnection = false;
	jTextFieldTotalFrames.setText( TotalFrames() + " " );
    }//end of computeButton_action

//-------------------------------------------------------------------------------
  /**
   * JPanelDetector#needCompute
   */
    boolean needCompute()
    {
	if( newAgentConnection ) return true;

	for (int i=0; i<NinParms; i++) {
	    if( textGroup[i].prev_text.trim().equals("") || textGroup[i].getText().trim().equals("") )
		return true;
	    if( Float.parseFloat( textGroup[i].prev_text ) != Float.parseFloat( textGroup[i].getText() ) )
		return true;
	}

	String newObsMode = jComboBoxObservingMode._getSelectedItem();
	String curObsMode = EPICS.get( EPICS.prefix + "dc:physicalG.VALI" );
	if( ! curObsMode.trim().equals( newObsMode.trim() ) ) return true;

	String newReadMode = jComboBoxReadoutMode._getSelectedItem();
	String curReadMode = EPICS.get( EPICS.prefix + "dc:physicalG.VALJ" );
	if( ! curReadMode.trim().equals( newReadMode.trim() ) ) return true;

	return false;
    }

//-------------------------------------------------------------------------------
  /**
   * JPanelDetector#needModeSelect
   */
    boolean needModeSelect()
    {
	boolean needSelect = false;

	String newObsMode = jComboBoxObservingMode._getSelectedItem();
	if( newObsMode.trim().equals("") ) needSelect = true;

	String newReadMode = jComboBoxReadoutMode._getSelectedItem();
	if( newReadMode.trim().equals("") ) needSelect = true;

	if( needSelect ) {
	    JOptionPane.showMessageDialog(this, "Need to select Obs mode and Readout mode !",
					  "ERROR", JOptionPane.ERROR_MESSAGE);
	    return true;
	} else
	    return false;
    }

//-------------------------------------------------------------------------------
  /**
   * JPanelDetector#needNodhandshakeSelect
   */
    boolean needNodhandshakeSelect()
    {
	String newNodMode = jComboBoxNodHandshake._getSelectedItem();

	if( newNodMode.trim().equals("") ) {
	    JOptionPane.showMessageDialog(this, "Need to select Nod-handshake mode !",
					  "ERROR", JOptionPane.ERROR_MESSAGE);
	    return true;
	} else
	    return false;
    }

//-------------------------------------------------------------------------------
    /**
     *JPanelDetector#configure button action performed
     * Causes EPICS to tell the det. control. agent to send configuration to MCE4 and FrameAcqServer.
     *@param e not used
     */
    void configureButton_action(ActionEvent e)
    {
	if( needCompute() ) {
	    JOptionPane.showMessageDialog(this, "Input parameters have changed:  press COMPUTE button !",
					  "ERROR", JOptionPane.ERROR_MESSAGE);
	    return;
	}

	if( needNodhandshakeSelect() ) return;

	jTextFieldTotalFrames.setText( TotalFrames() + " " );
	totFramesLabel.setText("out of  " + jTextFieldTotalFrames.getText() );

	String cmd = "PUT " + EPICS.prefix + "dc:acqControl.A CONFIGURE";
	jeiCommand.execute(cmd);
	cmd = "PUT " + EPICS.prefix + "dc:acqControl.DIR START  ;" + configureButton.getText();
	jeiCommand.execute(cmd);
	needConfigure = false;
    } //end of configureButton_action

//-------------------------------------------------------------------------------
  /**
   * JPanelDetector#TotalFrames
   *  Compute total # of frames in observation from MCE hardware parameters.
   */
    int TotalFrames()
    {
	int ChopCoadds = Integer.parseInt( CTcTextGroup[ctiChopCoadds].getText() );
	int NodReads =   Integer.parseInt( CTcTextGroup[ctiNodSettleReads].getText() );
	int SaveSets =   Integer.parseInt( CTcTextGroup[ctiSaveSets].getText() );
	int NodSets =    Integer.parseInt( CTcTextGroup[ctiNodSets].getText() );
	int frames = SaveSets * NodSets;
	if( ChopCoadds > 0 ) frames *= 2;
	if( NodReads > 0 ) frames *= 2;
	return frames;
    }

//-------------------------------------------------------------------------------
  /**
   * Puts the val to the specified EPICS record field
   *@param putRec String: the EPICS record
   *@param value String: Value to be put
   */
    void putcmd( String putRec, String value ) {
	if( value.trim().equals("") || putRec.trim().equals("") ) return;
	String cmd = new String("PUT " + putRec + " " + "\""+value+ "\"") ;
	jeiCommand.execute(cmd);
    } //end of putcmd

//-------------------------------------------------------------------------------
    /**
     * JPanelDetector#start button action performed
     * Checks if COMPUTE or CONFIGURE needs to be pressed,
     *  if all ok, tells EPICS to send observation START directive to detector controller agent.
     *@param e not used
     */
    void startButton_action(ActionEvent e)
    {
	if( needModeSelect() ) return;
	if( needNodhandshakeSelect() ) return;

	if( needCompute() ) {
	    JOptionPane.showMessageDialog(this, "Input parameters have changed: press COMPUTE button!",
					  "ERROR", JOptionPane.ERROR_MESSAGE);
	    return;
	}

	if( needConfigure ) {
	    JOptionPane.showMessageDialog(this, "First press CONFIGURE button to send new configuration!",
					  "ERROR", JOptionPane.ERROR_MESSAGE);
	    return;
	}

	String comment = EPICS.get( EPICS.prefix + "dc:acqControlG.VALQ" );
	int fmindex = comment.indexOf("FASTMODE");
	int fmindex2 = comment.indexOf("FASTERMODE"); //faster is not used but code is retained.

	if( jComboBoxAcqMode.getSelectedItem().equals("Fast") ) {
	    if( fmindex2 >= 0 )
		comment = comment.substring(0,fmindex2) + comment.substring(fmindex+11);
	    if( fmindex < 0 )
		putcmd( EPICS.prefix + "dc:acqControl.J", "FASTMODE:" + comment );
	}
	else if( jComboBoxAcqMode.getSelectedItem().equals("Faster") ) {
	    if( fmindex >= 0 )
		comment = comment.substring(0,fmindex) + comment.substring(fmindex+9);
	    if( fmindex2 < 0 )
		putcmd( EPICS.prefix + "dc:acqControl.J", "FASTERMODE:" + comment );
	}
	else if( fmindex >= 0 || fmindex2 >= 0 ) {  //remove fastmode directives from comment record:
	    if( fmindex >= 0 )
		comment = comment.substring(0,fmindex) + comment.substring(fmindex+9);
	    else if( fmindex2 >= 0 )
		comment = comment.substring(0,fmindex2) + comment.substring(fmindex+11);
	    if( comment.trim().equals("") ) comment = ".";
	    putcmd( EPICS.prefix + "dc:acqControl.J", comment );
	}

	if( checkArchvFileStatus() ) {
	    String cmd = "PUT " + EPICS.prefix + "dc:acqControl.A START";
	    jeiCommand.execute(cmd);
	    cmd = "PUT " + EPICS.prefix + "dc:acqControl.DIR START  ;" + startButton.getText();
	    jeiCommand.execute(cmd);
	}
    } //end of startButton_action

//-------------------------------------------------------------------------------
    /**
     *JPanelDetector#checkArchvFileStatus
     * Gets the current Archive File status from EPICS, and if TRUE returns true.
     */
    boolean checkArchvFileStatus()
    {
	String DataMode = EPICS.get(EPICS.prefix + "dc:acqControl.C");
	if( DataMode.trim().equals("") )
	    DataMode = EPICS.get(EPICS.prefix + "dc:acqControlG.VALE");

	if( DataMode.trim().toUpperCase().equals("SAVE") )
	    return true;
	else {
	    if( JOptionPane.NO_OPTION ==
		JOptionPane.showConfirmDialog(this,"Data will NOT be saved, continue anyway?","Warning",
					      JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE) )
		return false;
	    else
		return true;
	}
    }

//-------------------------------------------------------------------------------
  /**
   * JPanelDetector#abort button action performed
   * Tells EPICS to send ABORT directive to detector control agent.
   *@param e not used
   */
  void abortButton_action(ActionEvent e) {
      if (JOptionPane.showConfirmDialog(this,"ABORT the observation ?") == JOptionPane.YES_OPTION)
	  {
	      String cmd = "PUT " + EPICS.prefix + "dc:acqControl.A ABORT";
	      jeiCommand.execute(cmd);
	      cmd = "PUT " + EPICS.prefix + "dc:acqControl.DIR START ;" + abortButton.getText();
	      jeiCommand.execute(cmd);
	  }
  } //end of abortButton_action

//-------------------------------------------------------------------------------
  /**
   * JPanelDetector#reInit button action performed
   * Tells EPICS to send DATUM directive to Detector Control agent,
   * which causes it to re-connect to Data Acquisition Server and re-send the Pixel Maps,
   * and also causes the DC agent to re-connect to other agents.
   *@param e not used
   */
  void reInitButton_action(ActionEvent e) {
      if (JOptionPane.showConfirmDialog(this,
	  "Command DC Agent to re-read Meta-Config file and re-send Pixel Maps to AcqServer ?")
	  == JOptionPane.YES_OPTION)
	  {
	      String cmd = "PUT " + EPICS.prefix + "dc:acqControl.A DATUM";
	      jeiCommand.execute(cmd);
	      cmd = "PUT " + EPICS.prefix + "dc:acqControl.DIR START ;" + abortButton.getText();
	      jeiCommand.execute(cmd);
	  }
  } //end of reInitButton_action

//-------------------------------------------------------------------------------
  /**
   *Pops up the efficiency window
   *@param e not used
   */
  void efficButton_action(ActionEvent e) {
     Point loc = getLocation();
     Dimension frmSize = new Dimension( 250, 250 );
     efficientWindow.setSize( frmSize );
     Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
     efficientWindow.setLocation( (int)(screenSize.getWidth() - frmSize.getWidth()*2),
				  (int)(screenSize.getHeight() - frmSize.getHeight()) - 100 );
     efficientWindow.show();
     efficientWindow.setState( Frame.NORMAL );
     jTextFieldTotalFrames.setText( TotalFrames() + " " );
     efficientWindow.buttonEffic_action();
  } //end of efficButton_action

//===============================================================================
  /**
   * Handles the detector efficiency dialog box
   */
  public class JDialogDetectorEfficiency extends JFrame {
      EPICSTextField framedc = new EPICSTextField(EPICS.prefix + "dc:acqControlG.VALR","framedc");
      //EPICSTextField chopdc = new EPICSTextField(EPICS.prefix + "dc:acqControlG.VALM","chopdc");
      //EPICSTextField noddc = new EPICSTextField(EPICS.prefix + "dc:acqControlG.VALN","noddc");
      EPICSTextField chopdc = new EPICSTextField(EPICS.prefix + "dc:physOutG.VALH","chopdc");
      EPICSTextField noddc = new EPICSTextField(EPICS.prefix + "dc:physOutG.VALI","noddc");
      JTextField photdc = new JTextField("");
      JTextField srcdc = new JTextField("");
      JPanel labelpanel = new JPanel();
      JPanel textpanel = new JPanel();
      JPanel buttonpanel = new JPanel();
      JButton buttonEffic = new JButton ("Compute Efficiency");
      JButton buttonDONE = new JButton ("DONE");

//-------------------------------------------------------------------------------
    /**
     *Constructor
     */
    JDialogDetectorEfficiency () {
      super();
      enableEvents(AWTEvent.WINDOW_EVENT_MASK);
      try  {
        jbInitEffBox();
      }
      catch(Exception e) {
        e.printStackTrace();
      }
    } //end of JDialogDetectorEfficiency

//-------------------------------------------------------------------------------
    /**
     *Component initialization
     */
    private void jbInitEffBox()
    {
      buttonDONE.addActionListener(new java.awt.event.ActionListener()
	  { public void actionPerformed(ActionEvent e) { buttonDONE_action(); } });

      buttonEffic.addActionListener(new java.awt.event.ActionListener()
	  { public void actionPerformed(ActionEvent e) { buttonEffic_action(); } });

      labelpanel.setLayout(new GridLayout(0,1,0,10));
      textpanel.setLayout(new GridLayout(0,1,0,10));
      buttonpanel.setLayout(new FlowLayout());
      this.getContentPane().setLayout(new BorderLayout());
      framedc.setEditable(false);
      framedc.setColumns(7); 
      noddc.setEditable(false);
      noddc.setColumns(7);
      chopdc.setEditable(false);
      chopdc.setColumns(7);
      photdc.setBackground(Color.white);
      photdc.setEditable(false);
      photdc.setColumns(7);
      srcdc.setBackground(Color.white);
      srcdc.setEditable(false);
      srcdc.setColumns(7);
      labelpanel.add(new JLabel(""));
      labelpanel.add(new JLabel("Frame Duty Cycle (%):"));
      labelpanel.add(new JLabel("Chop Duty Cycle (%):"));
      labelpanel.add(new JLabel("Nod Duty Cycle (%):"));
      labelpanel.add(new JLabel("Photon Duty Cycle (%):"));
      labelpanel.add(new JLabel("Source Duty Cycle (%):"));
      labelpanel.add(new JLabel(""));
      textpanel.add(new JLabel(""));
      textpanel.add(framedc);
      textpanel.add(chopdc);
      textpanel.add(noddc);
      textpanel.add(photdc);
      textpanel.add(srcdc);
      textpanel.add(new JLabel(""));
      buttonpanel.add(buttonEffic);
      buttonpanel.add(buttonDONE);
      this.setTitle("Detector Efficiency");
      this.getContentPane().add(labelpanel,BorderLayout.WEST);
      this.getContentPane().add(textpanel,BorderLayout.EAST);
      this.getContentPane().add(buttonpanel,BorderLayout.SOUTH);
      buttonDONE.grabFocus();
      this.setSize(new Dimension(250,250));
    } //end of jbInit

//--------------------------------------------------------
    void buttonDONE_action() { this.dispose(); }

//--------------------------------------------------------
    void buttonEffic_action()
    {
	try {
	    String frmdc = framedc.getText().trim();
	    float fdc = 1;
	    if( ! frmdc.equals("") )
		fdc = Float.parseFloat( frmdc )/100;
	    float pdc = 100 * fdc
		* Float.parseFloat( chopdc.getText() )/100
		* Float.parseFloat( noddc.getText() )/100;
	    String duty = Float.toString( pdc );
	    photdc.setText( duty.substring(0,4) );
	    duty = Float.toString( pdc/2 );
	    srcdc.setText( duty.substring(0,4) );
	}
	catch(Exception e) { System.out.println( e.toString() ); }
    }

//-------------------------------------------------------------------------------
    /**
     * Window event handler
     * closes window
     *@param e TBD??
     */
    protected void processWindowEvent(WindowEvent e) {
      if(e.getID() == WindowEvent.WINDOW_CLOSING) {
        this.dispose();
      }
      super.processWindowEvent(e);
    } //end of processWindowEvent

  } //end of class JDialogDetectorEfficiency

} //end of class JPanelDetector



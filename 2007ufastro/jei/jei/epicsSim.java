package ufjei;

import java.util.*;
import java.io.*;
import jca.*;
import jca.dbr.*;
import jca.event.*;

public class epicsSim {
  public static boolean in_simulation_mode;
  private static Properties recs = new Properties();


  synchronized static String get(String rec) {
    String val = recs.getProperty(rec);
    if (val == null) {
      val = "";
      put(rec, val);
    }
    return val;
  }

  synchronized static void put(String rec, String val) {
    recs.put(rec, val);
    save();
  }

  synchronized public static void load() {
    try {
      FileInputStream input = new FileInputStream( "sim_recs.txt" );
      recs.load( input );
      input.close();
    }
    catch( IOException ex ) {
       System.out.println( ex.toString() );
    }
  }

  synchronized static void save() {
    try {
      FileOutputStream output = new FileOutputStream( "sim_recs.txt" );
      recs.store( output, "simulated EPICS records" );
      output.close();
    }
    catch( IOException ex ) {
       System.out.println( ex.toString() );
    }
  }

  static void monitor(PV pv, String rec, jca.event.MonitorListener l, int mask) {
    // Start a thread that checks to see if rec changes value.
    // In the thread, call arg.monitorChanged if it does.
    get(rec); // create the rec if is not there
    mon_thread aThread = new mon_thread(pv, rec, l, mask);
    aThread.start();
  }

}

class mon_thread extends Thread {
  PV pv;
  String rec;
  jca.event.MonitorListener l;
  int mask;
  String val;

  public mon_thread(PV _pv, String _rec, jca.event.MonitorListener _l, int _mask) {
    super(_rec);
    pv = _pv;
    rec = _rec;
    l = _l;
    mask = _mask;
    val = epicsSim.get(rec);
  }

  public void run() {
    while (true) {
      // tbd - load if newer
      //System.out.println( "checking " + val );
      if (val.compareTo(epicsSim.get(rec)) != 0) {
        val = epicsSim.get(rec);
        MonitorEvent event = new MonitorEvent(pv, new jca.dbr.DBR_DBString(), null, l);
        event.fire();
      }
      try {
        Thread.currentThread().sleep(1000);
      } catch (InterruptedException e) { }
    }
  }
}

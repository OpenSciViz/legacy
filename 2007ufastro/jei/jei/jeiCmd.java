package ufjei;

import java.awt.*;
import javax.swing.JOptionPane;
import java.util.*;
import jca.*;
import jca.dbr.*;
import jca.event.*;

/**
 *Title:      T-ReCS Java Engineering Interface
 *Version:
 *Copyright:  Copyright (c) 1999
 *Author:     David Rashkin, Thomas Kisko
 *Company:    University of Florida
 *Description:T-ReCS is the Mid IR instrument for the Gemini South telescope.
 *This is the T-ReCS Java Engineering Interface.
 */


//===============================================================================
/**
 * Handles jei commands
 */
public class jeiCmd {
  public static final String rcsID = "$Name:  $ $Id: jeiCmd.java,v 0.2 2003/10/23 22:53:10 varosi beta $";
  public boolean accepted;
  public String response;
  public String error;
  public static final String str_term = "\n"; // null terminated??
  public static CmdListFrame cmdListFrame = new CmdListFrame();


//-------------------------------------------------------------------------------
  /**
   * Default constructor
   */
  jeiCmd () {
    accepted = false;
    response = "";
    error = "";
  } //end of jeiCmd


//-------------------------------------------------------------------------------
  /**
   * Execute method accepts internal commands from jei and executes them
   * TBD: format of command string
   *@param cmdline String: Command string to be processed
   */
  void execute (String cmdline) {
    StringTokenizer st;
    accepted = false;
    response = "";
    error = "";
    // add cmd to cmdListFrame
    if (cmdListFrame.jTextAreaCmds.getLineCount() > 100) {
      // delete the first line
      try {
        int i = cmdListFrame.jTextAreaCmds.getLineStartOffset(1);
        cmdListFrame.jTextAreaCmds.replaceRange("", 0, i);
      } catch (javax.swing.text.BadLocationException te) {}
    }
    cmdListFrame.jTextAreaCmds.append("\n" + cmdline);
    int j=0;
    try {
      j = cmdListFrame.jTextAreaCmds.getLineStartOffset(cmdListFrame.jTextAreaCmds.getLineCount()-1);
    }
    catch (javax.swing.text.BadLocationException ble) {
      j = 0;
    }
    cmdListFrame.jTextAreaCmds.setCaretPosition(j);
    if (JPanelScripts.recording) {
      jeiFrame.statusBar.setText(">>> RECORDING <<< Added: " + cmdline);
      jeiFrame.jPanelScripts.add_new_cmd(cmdline);
      accepted = true;
    }
    if (!JPanelScripts.recording || JPanelScripts.execWhileRecording.isSelected()) {
      try {
        st = new StringTokenizer(cmdline);
        String cmd = st.nextToken().toUpperCase();
        if (cmdline.charAt(0) == ';') { // a comment, ignore the line
          accepted = true;
        }
        else if (cmd.equals("PUT")) { //do a CA put
          String rec = st.nextToken();
          // if the first character of val is a double quote, then
          // assume that the value has multiple words separated by spaces.
          // We should assume that everything between the double quotes
          // is part of the value
          String val = "";
          if (cmdline.indexOf("\"") != -1) {
            st.nextToken("\"");
            val = st.nextToken("\"");
          } else val = st.nextToken();
	  //if the first character of val is a semicolon, then
          //we assume that the string token received is actually
          //a description, and that the actual value of val is
          //an empty string (or a space)
	  if (val.indexOf(";") == 0) val = "";
          EPICS.put(rec,val);
          //if this is a put to an apply record, then several things
          //need to happen...  
          if (rec.endsWith("pply.DIR")) { //leave off the leading 'a' or 'A'
	      String tempRec = rec.substring(0,rec.length()-3);
              tempRec += "VAL";
	      String res = EPICS.get(tempRec); 
              if (res.equals("-1")) {
                  tempRec = tempRec.substring(0,rec.length()-3);
                  tempRec += "MESS";
		  jeiError.show(tempRec + ": " + EPICS.get(tempRec));
              }
	  }
          accepted = true;
        }
        else if (cmd.equals("GET")) { //do a CA get
          String rec = st.nextToken();
          response = EPICS.get(rec);
          accepted = true;
        }
        else if (cmd.equals("ADD_REC")) { // add a record to a EPICSRecListFrame
          // syntax: ADD_REC rec.field && description && window_name
          String rec = st.nextToken("&&").trim();
          String desc = st.nextToken("&&").trim();
          String win_name = st.nextToken("&&").trim();
          EPICSRecListFrame frame = null;
          // is win already open?
          boolean found = false;
          int i = 0;
          int n = JPanelEPICSRecs.EPICSRecListFrame_names.size();
          while (!found && i < n) {
            if (win_name.equals((String)JPanelEPICSRecs.EPICSRecListFrame_names.elementAt(i))) {
              found = true;
              frame = (EPICSRecListFrame)JPanelEPICSRecs.EPICSRecListFrames.elementAt(i);
            }
            else i++;
          }
          // if not found, open it and add to list of open wins
          if (!found) {
            frame = new EPICSRecListFrame(win_name);
            frame.validate();
            frame.setVisible(true);
            JPanelEPICSRecs.EPICSRecListFrame_names.add(win_name);
            JPanelEPICSRecs.EPICSRecListFrames.add(frame);
          }
          frame.add_rec(rec, desc);
        }
        else if (cmdline.charAt(0) == '.') { // do part of a Master screen configuration
          st = new StringTokenizer(cmdline,":");
          String name = st.nextToken().trim();
          String val = st.nextToken().trim();
          if (name.equals(".Camera mode")) {
            jeiFrame.jPanelMaster.jComboBoxCameraMode.set_and_putcmd(val);
          }
          else if (name.equals(".Observing mode")) {
            jeiFrame.jPanelMaster.jComboBoxObservingMode.set_and_putcmd(val);
          }
          else if (name.equals(".Imaging mode")) {
            jeiFrame.jPanelMaster.jComboBoxImagingMode.set_and_putcmd(val);
          }
          else if (name.equals(".Data mode")) {
            jeiFrame.jPanelMaster.jComboBoxDataMode.set_and_putcmd(val);
          }
          else if (name.equals(".Sky Background")) {
            jeiFrame.jPanelMaster.jComboBoxSkyBackgrnd.set_and_putcmd(val);
          }
          else if (name.equals(".Sky Noise")) {
            jeiFrame.jPanelMaster.jComboBoxSkyNoise.set_and_putcmd(val);
          }
          else if (name.equals(".Sector")) {
            jeiFrame.jPanelMaster.jComboBoxSector.set_and_putcmd(val);
          }
          else if (name.equals(".Filter")) {
            jeiFrame.jPanelMaster.jComboBoxFilter.set_and_putcmd(val);
          }
          else if (name.equals(".Lyot stop")) {
            jeiFrame.jPanelMaster.jComboBoxLyotStop.set_and_putcmd(val);
          }
          else if (name.equals(".Slit width")) {
            jeiFrame.jPanelMaster.jComboBoxSlitWidth.set_and_putcmd(val);
          }
          else if (name.equals(".Grating")) {
            jeiFrame.jPanelMaster.jComboBoxGrating.set_and_putcmd(val);
          }
          else if (name.equals(".Central wavelength")) {
            jeiFrame.jPanelMaster.jTextFieldCentralWavelength.set_and_putcmd(val);
          }
          else if (name.equals(".Aperture")) {
            jeiFrame.jPanelMaster.jCheckBoxAperture.setSelected(true);
            jeiFrame.jPanelMaster.jComboBoxAperture.setEnabled(true);
            jeiFrame.jPanelMaster.jComboBoxAperture.set_and_putcmd(val);
          }
          else if (name.equals(".Pupil") || name.equals(".Lens")) {
            jeiFrame.jPanelMaster.jCheckBoxPupil.setSelected(true);
            jeiFrame.jPanelMaster.jComboBoxPupil.setEnabled(true);
            jeiFrame.jPanelMaster.jComboBoxPupil.set_and_putcmd(val);
          }
          else if (name.equals(".Window")) {
            jeiFrame.jPanelMaster.jCheckBoxWindow.setSelected(true);
            jeiFrame.jPanelMaster.jComboBoxWindow.setEnabled(true);
            jeiFrame.jPanelMaster.jComboBoxWindow.set_and_putcmd(val);
          }
          else if (name.equals(".On source time")) {
            jeiFrame.jPanelMaster.jTextFieldOnSrcTime.set_and_putcmd(val);
          }
          else if (name.equals(".Observation data label")) {
            jeiFrame.jPanelMaster.jTextFieldObsDataLabel.set_and_putcmd(val);
          }

          else {
            jeiError.show("Error - unsupported dot command: " + cmdline);
          }
          //System.out.println(name);
          //System.out.println(val);
        }
        else if (cmd.equals("PUSH")) {
          accepted = true;
          if (cmdline.equals("PUSH View Recent Commands")) {
            cmdListFrame.setVisible(true);
            cmdListFrame.setState( Frame.NORMAL );
          }
          else if (cmdline.equals("PUSH Optical Path")) {
            if (jeiFrame.Opathframe == null) {
              jeiFrame.Opathframe = new jeiOpath();
              jeiFrame.Opathframe.setVisible(true);
            }
            else {
              jeiFrame.Opathframe.requestFocus();
            }
          }
          else accepted = false;
        }
        else {  //unsupported command
          System.out.println("Error - unsupported command: " + cmdline);
        }
      }
      catch (Exception e) {
        jeiError.show("Error trying to parse command: " + cmdline);
        jeiError.show(e.toString());
        accepted = false;
        error = e.toString();
        response = "error";
      }
    }

  } //end of execute

} //end of class jeiCmd

package ufjei;

import javax.swing.*;
import java.awt.*;


//===============================================================================
/**
 * Array of status JLabels
 */
public class JPanelMotorStatus extends JPanel {
  public static final String rcsID = "$Name:  $ $Id: JPanelMotorStatus.java,v 0.1 2002/07/25 22:06:01 varosi beta $";
  public JLabel Status[] ;


//-------------------------------------------------------------------------------
  /**
   * Constructs given the motor parameters
   * @param mot_param jeiMotorParameters: motor parameters
   */
  public JPanelMotorStatus(jeiMotorParameters mot_param) {
    Status = new JLabel[mot_param.getNumberMotors()];
    GridLayout gridLayout = new GridLayout();
    setLayout(gridLayout);
    gridLayout.setRows(mot_param.getNumberMotors()+1);
    gridLayout.setVgap(5);
    add(new JLabel("Status"), null);
    for (int i=0; i < mot_param.getNumberMotors(); i++) {
	Status[i] = new JLabel("Unknown");
	add( Status[i] );
    }
  } //end of JPanelMotorStatus


//-------------------------------------------------------------------------------
  /**
   * Returns the status string given the motor number and current position
   * @param motorNum int: motor number
   * @param currPos double: current position
   */
  public String getStatus(int motorNum, double currPos) {
    try {
	int PosIndex=0, i=0;
	double delta, PosDistance=1.0e9;

	for (i = 1; i <= jeiMotorParameters.getNumLocations(motorNum); i++)
	    {
		delta = currPos - jeiMotorParameters.getLocationOffset( motorNum, i );
		if( Math.abs( delta ) < Math.abs( PosDistance ) ) {
		    PosDistance = delta;
		    PosIndex = i;
		}
	    }

	String PosName ="\""+ jeiMotorParameters.getNamedLocations( motorNum, PosIndex )+"\"";

	if( PosDistance > 0 )
	    return "At " + currPos + ", " + PosName + " + " + PosDistance;
	else if( PosDistance < 0 )
	    return "At " + currPos + ", " + PosName + " - " + Math.abs( PosDistance );
	else
	    return "At " + currPos + ", " + PosName;
    }
    catch (Exception e) {
      jeiError.show("Problem getting motor position status");
      return "Error getting motor location";
    }
  } //end of getStatus

//-------------------------------------------------------------------------------
    /**
     * Commands the EPICS database to query the Motor Control Agent for status of indexors.
     */
    public void queryMotorPositions() {
        String str = "PUT " + EPICS.prefix + "cc:test.A 1 ; Send query to motor agent";
        jeiCmd cmd = new jeiCmd();
        cmd.execute(str);
        str = "PUT " + EPICS.prefix + "cc:apply.DIR START ; Apply the request";
	cmd.execute(str);
    }

//-------------------------------------------------------------------------------
    /**
     * Sends commands to EPICS cc: records
     *  to direct EPICS gensubs to connect via sockets to the Motor Control Agent.
     */
    void connectEPICStoMotorAgent()
    {
	String cmd;
	jeiCmd command = new jeiCmd();

	cmd = "PUT " + EPICS.prefix + "cc:apply.DIR CLEAR";
	command.execute(cmd);
        try { Thread.sleep(2000); } catch (Exception _e) {}

	cmd = "PUT " + EPICS.prefix + "cc:init.A 0";
	command.execute(cmd);
	cmd = "PUT " + EPICS.prefix + "cc:apply.DIR START";
	command.execute(cmd);
    }

} //end of class JPanelMotorStatus

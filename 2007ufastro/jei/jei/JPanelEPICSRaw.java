package ufjei;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;
import java.util.*;
import jca.*;
import jca.event.*;
import jca.dbr.*;

public class JPanelEPICSRaw extends JPanel implements PutListener, GetListener, MonitorListener{
  public static final String rcsID = "$Name:  $ $Id: JPanelEPICSRaw.java,v 0.0 2002/06/03 17:42:30 hon beta $";
  Vector sections;

  JTextField putVal = new JTextField("",5);
  JComboBox pvPutName = new JComboBox();
  JLabel pvPutNameLabel = new JLabel("pvName : ");
  JLabel putLabel = new JLabel("Value to Put : ");
  JButton putButton = new JButton("PUT");
  JCheckBox putCallback = new JCheckBox("CallBack",false);

  JTextField getVal = new JTextField("",5);
  JComboBox pvGetName = new JComboBox();
  JLabel pvGetNameLabel = new JLabel("pvName : ");
  JLabel getLabel = new JLabel("Value : ");
  JButton getButton = new JButton("GET");
  JCheckBox getCallback = new JCheckBox("CallBack",false);

  JComboBox pvMonName = new JComboBox();
  JLabel pvMonNameLabel = new JLabel("pvName : ");
  JButton monButton = new JButton("MONITOR");
  JButton clearButton = new JButton("CLEAR");
  JCheckBox monVal = new JCheckBox("Value",true);
  JCheckBox monLog = new JCheckBox("Log",true);
  JCheckBox monAlrm = new JCheckBox("Alarm",true);

  JPanel putPanel = new JPanel(new FlowLayout());
  JPanel getPanel = new JPanel(new FlowLayout());
  JPanel monPanel = new JPanel(new FlowLayout());

  //PV [] pv = new PV[256];
  //private static int pvCount = 0;
  //PV pv;
  Monitor mon;
  EPICSHeartBeat poller = new EPICSHeartBeat();

  public JPanelEPICSRaw() {
    this.setLayout(new GridLayout(3,1));
    pvPutName.addItem("trecs:cc:tempCont:tempMonG");
    pvPutName.setEditable(true);
    pvPutName.setSelectedIndex(0);
    pvPutName.setAutoscrolls(true);
    pvGetName.addItem("trecs:cc:tempCont:tempMonG");
    pvGetName.setEditable(true);
    pvGetName.setSelectedIndex(0);
    pvGetName.setAutoscrolls(true);
    pvMonName.addItem("trecs:cc:tempCont:tempMonG");
    pvMonName.setEditable(true);
    pvMonName.setSelectedIndex(0);
    pvMonName.setAutoscrolls(true);
    putButton.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
              putButtonActionPerformed(e);
          }
      }
    );


    getButton.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
              getButtonActionPerformed(e);
          }
      }
    );

    monButton.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
              monButtonActionPerformed(e);
          }
      }
    );

    clearButton.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
              clearButtonActionPerformed(e);
          }
      }
    );

    getVal.setEditable(false);
    getVal.setBackground(new Color(175,175,175));

    putPanel.add(pvPutNameLabel);
    putPanel.add(pvPutName);
    putPanel.add(putLabel);
    putPanel.add(putVal);
    putPanel.add(putButton);
    putPanel.add(putCallback);

    getPanel.add(pvGetNameLabel);
    getPanel.add(pvGetName);
    getPanel.add(getLabel);
    getPanel.add(getVal);
    getPanel.add(getButton);
    getPanel.add(getCallback);

    monPanel.add(pvMonNameLabel);
    monPanel.add(pvMonName);
    monPanel.add(monButton);
    monPanel.add(clearButton);
    monPanel.add(monVal);
    monPanel.add(monLog);
    monPanel.add(monAlrm);

    this.add(putPanel);
    this.add(getPanel);
    this.add(monPanel);

    //pv = new PV("LakeshoreStatus");
    //Ca.pendEvent(1.0);
    //initialize monitor array in C dll

  }

  public void putCompleted(PutEvent e) {
    JFrame dd = new JFrame();
    dd.getContentPane().setLayout(new FlowLayout());
    dd.setTitle("Put Callback");
    dd.getContentPane().add(new JLabel("Put completed : "+ e.pv().name()));
    dd.setSize(200,100);
    dd.setLocation(this.getLocation());
    dd.show();
  }

  public void getCompleted(GetEvent e) {
    JFrame dd = new JFrame();
    dd.getContentPane().setLayout(new FlowLayout());
    dd.setTitle("Get Callback");
    dd.getContentPane().add(new JLabel("Get completed : "+ e.pv().name()));
    dd.getContentPane().add(new JLabel("Data Retrieved : "+ ((DBR_DBString)e.dbr()).valueAt(0)));
    dd.setSize(200,100);
    dd.setLocation(this.getLocation());
    dd.show();
  }

  public void monitorChanged(MonitorEvent event) {
    try {
      JFrame dd = new JFrame();
      dd.getContentPane().setLayout(new FlowLayout());
      dd.setTitle("PV Changed");
      dd.getContentPane().add(new JLabel("PV Name : "+ event.pv().name()));
      dd.getContentPane().add(new JLabel("PV Id : "+ event.pv().id()));
      DBR_DBString dbr = new DBR_DBString("");
      try {
        event.pv().get(dbr);
        Ca.pendIO(0.5);
      }
      catch (Exception ee) {}
      dd.getContentPane().add(new JLabel("PV Value : "+ dbr.valueAt(0)));
      //dd.getContentPane().add(new JLabel("I don't know what to do "));
      //dd.getContentPane().add(new JLabel("with a 'monitorChanged' event!!"));
      dd.setSize(200,100);
      dd.setLocation(this.getLocation());
      dd.show();
    }
    catch (Exception ee) {
      System.out.println(ee.toString());
    }
  }

  private void setHistory (JComboBox j, String s) {
    int count = j.getItemCount();
    for (int i=0; i<count; i++)
      if (((String)j.getItemAt(i)).compareTo(s) == 0) return;
    j.addItem(s);
  }

  public void monButtonActionPerformed (ActionEvent e) {
    try {
       //Ca.flushIO();
       setHistory(pvMonName,(String)pvMonName.getSelectedItem());
       PV pv = new PV((String)pvMonName.getSelectedItem());
       Ca.pendEvent(0.5);
       int mask = 0;
       if (monVal.isSelected()) mask |= Monitor.VALUE;
       if (monLog.isSelected()) mask |= Monitor.LOG;
       if (monAlrm.isSelected()) mask |= Monitor.ALARM;
       mon = pv.addMonitor(new DBR_DBString(), this, null, mask);

       Ca.pendIO(0.5);
       if (!poller.keepRunning && !jeiFrame.hb.keepRunning)
         poller.start();
    }
    catch (Exception exp) {
       System.out.println(exp.toString());
    }
  }

  public void putButtonActionPerformed (ActionEvent e) {
     try {
       //Ca.flushIO();
       setHistory(pvPutName,(String)pvPutName.getSelectedItem());
       PV pv = new PV((String)pvPutName.getSelectedItem());
       Ca.pendEvent(0.5);
       String val = new String(putVal.getText());
       if (putCallback.isSelected()) {
         pv.put(val,this,"put_callback");
         Ca.pendEvent(0.5);
       }
       else {
         jeiCmd cmd = new jeiCmd();
         cmd.execute("PUT " + (String)pvPutName.getSelectedItem() + " " + val);
       }
     }
     catch (Exception exp) {
        System.out.println(exp.toString());
        //JOptionPane.showMessageDialog(this,exp.toString());
     }

  }

  public void getButtonActionPerformed (ActionEvent e) {
     try {
       //Ca.flushIO();
       setHistory(pvGetName,(String)pvGetName.getSelectedItem());
       PV pv = new PV((String)pvGetName.getSelectedItem());
       Ca.pendEvent(0.5);
       jeiCmd cmd = new jeiCmd();

       DBR_DBString dbr = new DBR_DBString("");
       //DBR_DBString dbr;
       if (getCallback.isSelected()) {
         pv.get(dbr,this,"get_callback");
         Ca.pendEvent(0.5);
       }
       else {
         cmd.execute("GET " + (String)pvGetName.getSelectedItem());
       }

       getVal.setText(cmd.response);
     }
     catch (Exception exp) {
       System.out.println(exp.toString());
     }
  }

  public void clearButtonActionPerformed (ActionEvent e) {
    try {
       mon.clear();
       Ca.pendIO(0.5);
       poller.STOPITNOW();
    }
    catch (Exception exp) {
      System.out.println(exp.toString());
    }
  }

  void load_pdf_recs_txt_file() {
    // to be implemented - error checking
    pdf_recs section;
    pdf_rec rec;
    pdf_field fld;
                                             
    try {
      sections = new Vector();
//      System.out.println("Reading pdf_recs.txt");
      String file_name = "pdf_recs.txt";
      String line;
      int i = 0;
      BufferedReader in_file ;
      in_file = new  BufferedReader (new FileReader(file_name));
/* sample pdf_recs.txt
envCont

$(ec)tempMonG,genSub,TBA
  A,in,STRING,TBA,cold1 upper
  B,in,STRING,TBA,cold2 upper
  VALA,out,STRING,TBA,cold1
  VALB,out,STRING,TBA,cold2

$(ec)pressMonG,genSub,TBA
  A,in,STRING,TBA,pressure upper
  VALA,out,STRING,TBA,cryostat pressure
  VALB,out,STRING,TBA,cryostat pressure OK to cool instrument

$(ec)heartbeat
  SLNK,in,STRING,n/a,n/a(recieves from the 3 longin's: $(ec)ls***Heartbeat above)
  OUT,out,STRING,n/a,n/a(goes to $(sad)ecHeartbeat


eod
*/
      while (!(line = in_file.readLine()).equals("eod")) {
//        System.out.println("section = " + line);  // should be a section title line
        section = new pdf_recs();
        section.title_line = line;
        section.pdf_recs = new Vector();
        sections.add(section);
        line = in_file.readLine(); // should be a blank line
        while ((line = in_file.readLine()).trim().length() > 0) {   // read recs for this section
//          System.out.println("rec = " + line);
          rec = new pdf_rec();
          section.pdf_recs.add(rec);
          // line contains rec_name, type, info
          rec.name = line;
          while ((line = in_file.readLine()).trim().length() > 0) {   // read fields for this section
//            System.out.println("fld = " + line);
            fld = new pdf_field();
            rec.fields.add(fld);
            // line contains fiels_name, in_or_out, type, range, info
            fld.name = line;
            line = in_file.readLine(); // should be a blank line
          }
        }
      }
      in_file.close();
    }
    catch(Exception e) {
      e.printStackTrace();
    }
  }

}

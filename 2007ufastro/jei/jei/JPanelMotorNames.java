package ufjei;

import javax.swing.*;
import java.awt.*;


//===============================================================================
/**
 *Panel of motor names
 */
public class JPanelMotorNames extends JPanel {
  public static final String rcsID = "$Name:  $ $Id: JPanelMotorNames.java,v 0.0 2002/06/03 17:42:30 hon beta $";


//-------------------------------------------------------------------------------
  /**
   *Default constructor
   */
  public JPanelMotorNames(jeiMotorParameters mot_param) {
    GridLayout gridLayout = new GridLayout();
    setLayout(gridLayout);
    gridLayout.setRows(mot_param.getNumberMotors()+1);
    gridLayout.setVgap(5);
    add(new JLabel("     Motor Name"), null);

    for (int i=0; i < mot_param.getNumberMotors(); i++) {
       add (new JLabel(String.valueOf(i+1) + " - " + mot_param.getMotorName(i+1)));
    }
  } //end of JPanelMotorNames

} //end of class JPanelMotorNames

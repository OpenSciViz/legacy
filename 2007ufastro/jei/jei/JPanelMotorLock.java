package ufjei;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;


//===============================================================================
/**
 * Motor CheckBox Locks for Motor High-Level Panel
 */
public class JPanelMotorLock extends JPanel implements ActionListener {
  public static final String rcsID = "$Name:  $Id:";
  public JCheckBox b[] ;

//-------------------------------------------------------------------------------
  /**
   *Constructor
   *@param mot_param jeiMotorParameters: motor parameters object
   */
  public JPanelMotorLock(jeiMotorParameters mot_param) {
    b = new JCheckBox [mot_param.getNumberMotors()];
    GridLayout gridLayout = new GridLayout();
    setLayout(gridLayout);
    gridLayout.setRows(mot_param.getNumberMotors()+1);
    gridLayout.setVgap(5);
    add(new JLabel(" "), null);
    for (int i=0; i < mot_param.getNumberMotors(); i++) {
       b[i] = new JCheckBox ();
       b[i].addActionListener(this);
       b[i].setActionCommand(String.valueOf(i));
       b[i].setEnabled(true) ;
       b[i].setSelected(true);
       add (b[i]);
    }
  } //end of JPanelMotorLock

//-------------------------------------------------------------------------------
  /**
   * Action Performed??
   *@param e TBD
   */
  public void actionPerformed(ActionEvent e) {
    boolean bool;
    String mot_num_str = e.getActionCommand();
    int mot_num = Integer.parseInt(mot_num_str)+1;
    bool = b[mot_num-1].isSelected();
    JPanelMotorHighLevel.jPanelMotorLock.b[mot_num-1].setSelected(bool);
    JPanelMotorLowLevel.jPanelMotorLock.b[mot_num-1].setSelected(bool);
    JPanelMotorHighLevel.jPanelNamedLocation.NamedLocation[mot_num-1].setEnabled(bool);
    JPanelMotorHighLevel.jPanelMove.b[mot_num-1].setEnabled(bool);
    JPanelMotorHighLevel.jPanelHome.b[mot_num-1].setEnabled(bool);
    JPanelMotorHighLevel.jPanelAbort.b[mot_num-1].setEnabled(bool);
    JPanelMotorLowLevel.jPanelMoveLow.b[mot_num-1].setEnabled(bool);
    JPanelMotorLowLevel.jPanelHomeLow.b[mot_num-1].setEnabled(bool);
    JPanelMotorLowLevel.jPanelOrigin.b[mot_num-1].setEnabled(bool);
    JPanelMotorLowLevel.jPanelAbort.b[mot_num-1].setEnabled(bool);
  } //end of actionPerformed

} //end of class JPanelMotorLock

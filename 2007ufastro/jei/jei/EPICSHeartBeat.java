package ufjei;

import jca.*;
import jca.dbr.*;
import jca.event.*;
import javax.swing.*;


/**
 * Title:        JAVA Engineering Console<p>
 * Description:  <p>
 * Copyright:    Copyright (c) David Rashkin, Latif Albusairi and Tom Kisko<p>
 * Company:      University of Florida<p>
 * @author David Rashkin, Latif Albusairi and Tom Kisko
 * @version 1.0
 */


//===============================================================================
/**
 * Handles the heartbeat that comes from EPICS??
 */
public class EPICSHeartBeat extends Thread{
  public static final String rcsID = "$Name:  $ $Id: EPICSHeartBeat.java,v 0.0 2002/06/03 17:42:30 hon beta $";

  public boolean keepRunning = false;


//-------------------------------------------------------------------------------
  /**
   * Default Constructor
   */
  public EPICSHeartBeat() {
    keepRunning = false;
  } //end of EPICSHeartBeat


//-------------------------------------------------------------------------------
  /**
   *Polls the EPICS heartbeat record.
   *Note -- you should use the Thread's <code> start </code>
   *Method to invoke the run method
   */
  public synchronized void run () {
    keepRunning = true;
    //PV temp = new PV (EPICS.prefix+"heartbeat");
    //catch (Exception e) { System.out.println("heart error"+e.toString()); }
    //DBR_DBString tempIt = new DBR_DBString("");
    while (keepRunning) {
      try {
        //temp.get(tempIt);
        Ca.pendIO(jeiFrame.TIMEOUT);
        //System.out.println(EPICS.prefix + "heartbeat = " + tempIt.valueAt(0));
      }
      catch (Exception ee){
        jeiError.show("Error: heartbeat -- " + ee.toString());
       //keepRunning = false;
      }
      try {
        this.currentThread().sleep(100);
      }
      catch (Exception ee) {
        jeiError.show("Error: trecs:heartbeat -- " + ee.toString());
       //keepRunning = false;
      }
    }
  } //end of run


//-------------------------------------------------------------------------------
  /**
   *Stops polling the EPICS heartbeat record -- returns no arguments
   */
  public void STOPITNOW () {
    keepRunning = false;
  } //end of STOPITNOW

} //end of class EPICSHeartBeat




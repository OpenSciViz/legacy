#if !defined(__UFDBL_H__)
#define __UFDBL_H__ "$Name:  $ $Id: ufdbl.h,v 1.1 2002/07/01 19:34:14 hon Developmental $"
/* uf modification of "dbl" func. in epics 3.12Gem */
extern long ufdbl(char *precdesname);
/* latif stuff: */
extern long ufHeartbeat();
extern long ufuptime();
extern long ufmemShow();
extern long ufAlive();
/* more (hon) stuff: */
extern long ufdbStrGet(char* name, char str[40]);
extern long ufdbStrTransfer(char* input, char* output);
#endif /* __UFDBL_H__ */

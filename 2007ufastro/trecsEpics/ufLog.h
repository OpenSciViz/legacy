#if !defined(__UFLog_h__)
#define __UFLog_h__ "$Name:  $ $Id: ufLog.h,v 0.3 2002/07/01 19:34:14 hon Developmental $"
#define __UFLog_H__(arg) const char arg##Log_h__rcsId[] = __UFLog_h__;
#include "sys/types.h"

/* log interface */
#if !defined(_IDL_)
extern void _uflog(const char* msg);
#endif /* _IDL_ */
#endif /* __UFLog_h__ */

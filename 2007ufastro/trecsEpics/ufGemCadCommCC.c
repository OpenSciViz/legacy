#if !defined(__UFGEMCADCOMMCC_C__)
#define __UFGEMCADCOMMCC_C__ "RCS: $Name:  $ $Id: ufGemCadCommCC.c,v 0.3 2002/07/01 19:34:14 hon Developmental $"
static const char rcsIdufGEMCADCOMMCCC[] = __UFGEMCADCOMMCC_C__;

#include "stdio.h"
#include "ufClient.h"
#include "ufLog.h"

#include <stdioLib.h>
#include <string.h>

#include <cad.h>
#include <cadRecord.h>

#include "trecs.h"
#include "ufGemComm.h"


/**********************************************************/
/**********************************************************/
/**********************************************************/
/**********************************************************/
/**********************************************************/
/**********************************************************/
/**************   Motor  Controller CAD's   **************/

/**********************************************************/
/**********************************************************/
/**********************************************************/
long
motorInitCommand (cadRecord * pcr)
{

  long out_vala;
  char *endptr;
  long temp_long;
  switch (pcr->dir)
    {
      /*
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case CAD_MARK:
      break;

    case CAD_CLEAR:
      break;

    case CAD_STOP:
      break;

      /*
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case CAD_PRESET:
    case CAD_START:
      /* printf("!!!!!!!@ %s @%s@\n",pcr->name,pcr->a) ; */
      if (strcmp (pcr->a, "") != 0)
	{
	  /* printf("!!!!!!!@ %s @%s@\n",pcr->name,pcr->a) ; */
	  out_vala = (long) strtod (pcr->a, &endptr);
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Can't init command", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }

	  temp_long = (long) out_vala;
	  if (UFcheck_long_r (temp_long, 0, 3) == -1)
	    {
	      strncpy (pcr->mess, "invalid init command", MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  *(long *) pcr->vala = out_vala;
	}
      else
	{
	  *(long *) pcr->vala = -1;
	  /* strncpy (pcr->mess, "Bad init command", MAX_STRING_SIZE);
	     return CAD_REJECT; */
	}
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}


/**********************************************************/
/**********************************************************/
/**********************************************************/
long
motorTestCommand (cadRecord * pcr)
{
  /*
     double out_val ;
     char *endptr ;
     long temp_long ; */
  switch (pcr->dir)
    {
      /*
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case CAD_MARK:
      break;

    case CAD_CLEAR:
      break;

    case CAD_STOP:
      break;

      /*
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case CAD_PRESET:
    case CAD_START:
      /*
         out_val = strtod(pcr->a,&endptr) ; 

         if (*endptr != '\0'){ 
         strncpy (pcr->mess, "Can't convert test directive", MAX_STRING_SIZE); 
         return CAD_REJECT; 
         } 
         temp_long = (long)out_val ; 
         if (UFcheck_long(temp_long,0,1) == -1) { 
         strncpy(pcr->mess, "test directive is  not valid", MAX_STRING_SIZE) ; 
         return CAD_REJECT ; 
         } 
         *(long *)pcr->vala = out_val ;  
       */
      *(long *) pcr->vala = 1;
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}


/**********************************************************/
/**********************************************************/
/**********************************************************/
/**********************************************************/
long
motorStepsCommand (cadRecord * pcr)
{

  double out_val;
  char *endptr;
  switch (pcr->dir)
    {
      /*
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case CAD_MARK:
      break;

    case CAD_CLEAR:
      break;

    case CAD_STOP:
      break;

      /*
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case CAD_PRESET:
    case CAD_START:
      out_val = strtod (pcr->a, &endptr);
      if (*endptr != '\0')
	{
	  strncpy (pcr->mess, "Can't convert steps value", MAX_STRING_SIZE);
	  return CAD_REJECT;
	}
      if (UFcheck_double (fabs (out_val), 0.0, num_steps_hi) == -1)
	{
	  strncpy (pcr->mess, " Number of Steps is not valid",
		   MAX_STRING_SIZE);
	  return CAD_REJECT;
	}
      *(double *) pcr->vala = out_val;
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}


/**********************************************************/
/**********************************************************/
/**********************************************************/
long
motorDatumCommand (cadRecord * pcr)
{
  /*
     double out_val ;
     char *endptr ;
     long temp_long ; */
  switch (pcr->dir)
    {
      /*
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case CAD_MARK:
      break;

    case CAD_CLEAR:
      break;

    case CAD_STOP:
      break;

      /*
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case CAD_PRESET:
    case CAD_START:
      /*
         out_val = strtod(pcr->a,&endptr) ; 

         if (*endptr != '\0'){ 
         strncpy (pcr->mess, "Can't convert datum directive", MAX_STRING_SIZE); 
         return CAD_REJECT; 
         } 
         temp_long = (long)out_val ; 
         if (UFcheck_long(temp_long,0,1) == -1) { 
         strncpy(pcr->mess, "datum directive is  not valid", MAX_STRING_SIZE) ; 
         return CAD_REJECT ; 
         } 
         *(long *)pcr->vala = temp_long ; 
       */
      *(long *) pcr->vala = 1;
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}


/**********************************************************/
/**********************************************************/
/**********************************************************/
long
motorStopCommand (cadRecord * pcr)
{

  switch (pcr->dir)
    {
      /*
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case CAD_MARK:
      break;

    case CAD_CLEAR:
      break;

    case CAD_STOP:
      break;

      /*
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case CAD_PRESET:
    case CAD_START:
      strcpy (pcr->vala, pcr->a);
      break;
    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }
  return CAD_ACCEPT;
}


/**********************************************************/
/**********************************************************/
/**********************************************************/
long
motorAbortCommand (cadRecord * pcr)
{

  switch (pcr->dir)
    {
      /*
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case CAD_MARK:
      break;

    case CAD_CLEAR:
      break;

    case CAD_STOP:
      break;

      /*
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case CAD_PRESET:
    case CAD_START:
      strcpy (pcr->vala, pcr->a);
      break;
    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}


/**********************************************************/
/**********************************************************/
/**********************************************************/
long
motorInitNamedPosCommand (cadRecord * pcr)
{
  if (strstr (pcr->name, "trecs") != NULL)
    return UFReadPositionsFile_old ("trecs");
  else
    return UFReadPositionsFile_old ("miri");
  Hi_Level_Move[0] = 0;
  Hi_Level_Move[1] = 0;
  Hi_Level_Move[2] = 0;
  Hi_Level_Move[3] = 0;
  Hi_Level_Move[4] = 0;
  Hi_Level_Move[5] = 0;
  Hi_Level_Move[6] = 0;
  Hi_Level_Move[7] = 0;
  Hi_Level_Move[8] = 0;
  Hi_Level_Move[9] = 0;
}


/**********************************************************/
/**********************************************************/
/**********************************************************/
long
motorNamedPosCommand (cadRecord * pcr)
{
  char rec_name[30];
  double out_val;
  int pos_num;
  char *endptr;
  int HL_index;
  /* char pos_name[30] ; */

  long status;
  switch (pcr->dir)
    {
      /*
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case CAD_MARK:
      break;

    case CAD_CLEAR:
      break;

    case CAD_STOP:
      break;

      /*
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case CAD_PRESET:
    case CAD_START:
      if (strstr (pcr->name, "sectWhl") != NULL)
	{
	  strcpy (rec_name, "sectWhl");
	  HL_index = 0;
	}
      if (strstr (pcr->name, "winChngr") != NULL)
	{
	  strcpy (rec_name, "winChngr");
	  HL_index = 1;
	}
      if (strstr (pcr->name, "aprtrWhl") != NULL)
	{
	  strcpy (rec_name, "aprtrWhl");
	  HL_index = 2;
	}
      if (strstr (pcr->name, "fltrWhl1") != NULL)
	{
	  strcpy (rec_name, "fltrWhl1");
	  HL_index = 3;
	}
      if (strstr (pcr->name, "lyotWhl") != NULL)
	{
	  strcpy (rec_name, "lyotWhl");
	  HL_index = 4;
	}
      if (strstr (pcr->name, "fltrWhl2") != NULL)
	{
	  strcpy (rec_name, "fltrWhl2");
	  HL_index = 5;
	}
      if (strstr (pcr->name, "pplImg") != NULL)
	{
	  strcpy (rec_name, "pplImg");
	  HL_index = 6;
	}
      if (strstr (pcr->name, "slitWhl") != NULL)
	{
	  strcpy (rec_name, "slitWhl");
	  HL_index = 7;
	}
      if (strstr (pcr->name, "grating") != NULL)
	{
	  strcpy (rec_name, "grating");
	  HL_index = 8;
	}
      if (strstr (pcr->name, "coldClmp") != NULL)
	{
	  strcpy (rec_name, "coldClmp");
	  HL_index = 9;
	}
      /* printf("pcr->a is: %s \n",pcr->a) ; */
      /* printf("HL Index is: %d\n",HL_index) ; */
      /* printf("Number of postions is %d\n",namedPositionsTable[HL_index].num_pos) ; */
      /* strcpy(pos_name,pcr->a) ; */
      if (strcmp (pcr->a, "PARK") == 0)
	pos_num = namedPositionsTable[HL_index].num_pos;
      else
	{
	  pos_num = strtod (pcr->a, &endptr);
	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Can't convert position num",
		       MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	}
      status = UFGetSteps (rec_name, pos_num, &out_val);
      /* printf ("????????????????????? %s %d %f\n",rec_name, pos_num, out_val) ; */
      if (status < 0)
	{
	  strncpy (pcr->mess, "Invalid position number", MAX_STRING_SIZE);
	  return CAD_REJECT;
	}
      Hi_Level_Move[HL_index] = 1;
      *(long *) pcr->vala = 1;
      *(double *) pcr->valb = out_val;
      strcpy (pcr->valc, pcr->a);

      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }
  return CAD_ACCEPT;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
long
motorOriginCommand (cadRecord * pcr)
{
  /*
     double out_val ;
     char *endptr ;
     long temp_long ; */
  switch (pcr->dir)
    {
      /*
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case CAD_MARK:
      break;

    case CAD_CLEAR:
      break;

    case CAD_STOP:
      break;

      /*
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case CAD_PRESET:
    case CAD_START:
      /*
         out_val = strtod(pcr->a,&endptr) ; 

         if (*endptr != '\0'){ 
         strncpy (pcr->mess, "Can't convert test directive", MAX_STRING_SIZE); 
         return CAD_REJECT; 
         } 
         temp_long = (long)out_val ; 
         if (UFcheck_long(temp_long,0,1) == -1) { 
         strncpy(pcr->mess, "test directive is  not valid", MAX_STRING_SIZE) ; 
         return CAD_REJECT ; 
         } 
         *(long *)pcr->vala = out_val ;  
       */
      *(long *) pcr->vala = 1;
      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}


/**********************************************************/
/**********************************************************/
/**********************************************************/
long
ccAbortCommand (cadRecord * pcr)
{

  switch (pcr->dir)
    {
      /*
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case CAD_MARK:
      break;

    case CAD_CLEAR:
      break;

    case CAD_STOP:
      break;

      /*
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case CAD_PRESET:
    case CAD_START:
      /* printf("!!!!!!!@ %s @%s@\n",pcr->name,pcr->a) ; */
      strcpy (pcr->vala, pcr->a);
      break;
    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
long
ccTestCommand (cadRecord * pcr)
{

  switch (pcr->dir)
    {
      /*
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case CAD_MARK:
      break;

    case CAD_CLEAR:
      break;

    case CAD_STOP:
      break;

      /*
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case CAD_PRESET:
    case CAD_START:
      /* printf("!!!!!!!@ %s @%s@\n",pcr->name,pcr->a) ; */
      strcpy (pcr->vala, pcr->a);
      break;
    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}


/**********************************************************/
/**********************************************************/
/**********************************************************/
long
ccDatumCommand (cadRecord * pcr)
{

  switch (pcr->dir)
    {
      /*
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case CAD_MARK:
      break;

    case CAD_CLEAR:
      break;

    case CAD_STOP:
      break;

      /*
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case CAD_PRESET:
    case CAD_START:
      /* printf("!!!!!!!@ %s @%s@\n",pcr->name,pcr->a) ; */
      strcpy (pcr->vala, "1");
      break;
    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
long
ccStopCommand (cadRecord * pcr)
{

  switch (pcr->dir)
    {
      /*
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case CAD_MARK:
      break;

    case CAD_CLEAR:
      break;

    case CAD_STOP:
      break;

      /*
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case CAD_PRESET:
    case CAD_START:
      /* printf("!!!!!!!@ %s @%s@\n",pcr->name,pcr->a) ; */
      strcpy (pcr->vala, pcr->a);
      break;
    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}

/**********************************************************/
/**********************************************************/
/**********************************************************/
long
ccParkCommand (cadRecord * pcr)
{

  switch (pcr->dir)
    {
      /*
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case CAD_MARK:
      break;

    case CAD_CLEAR:
      break;

    case CAD_STOP:
      break;

      /*
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case CAD_PRESET:
    case CAD_START:
      /* printf("!!!!!!!@ %s @%s@\n",pcr->name,pcr->a) ; */
      /*
         if (strcmp(pcr->a,"PARK") != 0) { 
         strncpy(pcr->mess, "PARK directive  not valid", MAX_STRING_SIZE) ; 
         return CAD_REJECT ; 
         }  */
      strcpy (pcr->vala, "PARK");
      break;
    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}


/**********************************************************/
/**********************************************************/
/**********************************************************/
long
ccInitCommand (cadRecord * pcr)
{
  double out_val;
  char *endptr;
  long temp_long;
  char empty_str[40];
  char in_str[40];

  strcpy (empty_str, "");
  strcpy (in_str, pcr->a);
  switch (pcr->dir)
    {
      /*
       *  Mark, Clear and Stop do nothing so can be accepted immediately     
       */

    case CAD_MARK:
      break;

    case CAD_CLEAR:
      break;

    case CAD_STOP:
      break;

      /*
       *  Preset and Start check attribute A for the command type
       *  and then the other attributes required by the command.
       */

    case CAD_PRESET:
    case CAD_START:
      /* printf("!!!!!!!@ %s @%s@\n",pcr->name,pcr->a) ; */
      if (strcmp (in_str, empty_str) != 0)
	{
	  /* printf("!!!!!!!@ %s @%s@ %d %d\n",pcr->name,pcr->a, strlen(in_str), strlen(empty_str)) ; */
	  out_val = strtod (pcr->a, &endptr);

	  if (*endptr != '\0')
	    {
	      strncpy (pcr->mess, "Can't convert init directive",
		       MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  temp_long = (long) out_val;
	  /*printf("******** The input command is: %ld %ld",(long)out_val,
	     UFcheck_long_r(temp_long,0,3)); */
	  if (UFcheck_long_r (temp_long, 0, 3) == -1)
	    {
	      strncpy (pcr->mess, "Init directive is  not valid",
		       MAX_STRING_SIZE);
	      return CAD_REJECT;
	    }
	  if (temp_long == SIMM_FAST)
	    *(long *) pcr->valb = 1;
	  else
	    *(long *) pcr->valb = 0;
	  strcpy (pcr->vala, pcr->a);
	}
      else
	{
	  /*   strncpy (pcr->mess, "Can't convert init directive", MAX_STRING_SIZE);
	     return CAD_REJECT; */
	}

      break;
    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}

#endif /* __UFGEMCADCOMMCC_C__ */

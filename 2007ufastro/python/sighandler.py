#!/usr/bin/env python
rcsId = '$Name:  $ $Id: sighandler.py 14 2008-06-11 01:49:45Z hon $'
import os, signal, sys, time

def handler(signum, frame):
  """
  example interactive signal/interrupt handler function object
  """
  print 'Signal handler called with signal', signum
  sys.stdout.write("exit? [n]: ")
  rep = "y"; rep = sys.stdin.readline()
  if rep[0] == 'y' :
    sys.exit()
  #endif
#end handler def


def sethandler(callback=handler, sigs=[signal.SIGINT, signal.SIGTSTP]):
  """
  establish a signal handler (i.e. the same function) for each signal in the list 
  """
  for sig in sigs:  
    print "set handler for sig:", sig
    signal.signal(sig, handler)
#end sethandler def

if __name__ == "__main__":
  """
  sighandler test main with silly command-line 
  """
  arg = app = sys.argv.pop(0)
  if len(sys.argv) > 0 :
    arg = sys.argv.pop(0)
    if arg == "-h" or arg == "-help" :
      print "no help in sight"
      exit
    else:
      instrum = arg
      print "interrupt instrument conrol sequence for: " + instrum
    #endif
  #endif

  if len(sys.argv) > 0 :
    arg = sys.argv.pop(0)
    print "arg == " + arg
  #endif

  sethandler()
  while( True ):
    print "waiting for signal...\n"
    time.sleep(0.5)
  #endwhile
#endmain

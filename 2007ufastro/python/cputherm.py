#!/usr/bin/env python
svnID = '$Id: cputherm.py 635 2009-06-15 16:44:30Z hon $'
# http://www.lm-sensors.org/
# yum install 'lm-sensors*' into /usr/bin/sensor ...
import errno, os, signal, socket, sys, time
from subprocess import *

ldpath = os.getenv("LD_LIBRARY_PATH")
if (ldpath == None) :
  print "LD_LIBRARY_PATH is not defined..."
#  sys.exit()
#end if
#print "ldpath= "+ldpath

pypath = os.getenv("PYTHONPATH")
if (pypath == None) :
  print "PYTHONPATH is not defined..."
#  sys.exit()
#end if
#print "pypath= "+pypath

ftemp = Popen(["/usr/bin/sensors", "-f"], stdout=PIPE).communicate()[0]
print ftemp.split()[6][0:3],'and',ftemp.split()[9][0:3]

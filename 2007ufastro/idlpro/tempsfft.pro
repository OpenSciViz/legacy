function tempsfft, tfil, TIME=time, TDET=tdet, TCF=tcf, NDATA=ndata, NTEMPS=ntemps, $
			TUNIF=tu, TSPLINE=tsplin, TAVG=tempav, FREQ=freq, $
			FITCF=fitcf, FITDD=fitdd, YRAN=yran, INDEX=tindex, PRAN=pran

	if N_elements( ntemps ) ne 1 then ntemps = 2
	if N_elements( ndata ) ne 1 then ndata = 200
	line=''
	on_ioerror,EOF
	help,tfil

	openr,Lun,tfil,/GET_LUN
	readf,Lun,line

	if strpos( strlowcase( line ), 'time' ) ge 0 then begin
		tdat=fltarr( 5, ndata )
		readf,Lun,tdat
	 endif else begin
		close,Lun
		openr,Lun,tfil
		tdat=fltarr( ntemps+1, ndata )
		readf,Lun,tdat
	  endelse
EOF:
	free_Lun,Lun

	th=transpose(tdat)

	w=where(th[*,0],nt)
	help,nt
	th=th[0:nt-1,*]

	time=th[*,0]
	tdet=th[*,1]
	tcf=th[*,2]

;spline interpolate onto uniform time (tu):

	tu = min(time) + (max(time)-min(time))*findgen(1000)/999

	deltat=tu[1]-tu[0]
	freq = findgen(500)/499/deltat/2

	if keyword_set( fitcf ) then begin
		temps=tcf
		yran=[-.006,.006]
		ptit='average( T_cold-finger ) = '
	 endif else if keyword_set( fitdd ) then begin
		temps=tdet
		yran=[-.004,.004]
		ptit='average( T_det-diode ) = '
	  endif else begin
		if N_elements(tindex) ne 1 then tindex=2
		temps = th[*,tindex]
		ptit='average( T_' + strtrim(tindex,2) + ') = '
		if N_elements(yran) ne 2 then yran=[-.01,.01]
	   endelse

	tsplin=spline(time,temps,tu)
	tempav=total(tsplin)/N_elements(tsplin)
	help,tempav
	ptit = ptit + strtrim(tempav,2) + ' K'

	!p.multi=[0,0,2]

	plot, tu, tsplin - tempav, XSTY=1, xtit='seconds', TIT=tfil, $
			ytit='delta( T ) K', YRAN=yran, ysty=1
	oplot, time, temps - tempav, ps=4

	tempfft = abs( fft( tsplin-tempav, -1 ) )
	print,max(tempfft)

	if N_elements( pran ) eq 2 then begin
		plot, freq, tempfft, YRAN=pran, $
			XRAN=[0,2], XTIT='Freq. Hz',YTIT='abs(FFT)',TIT=ptit
	 endif else plot, freq, tempfft, XRAN=[0,2], XTIT='Freq. Hz',YTIT='abs(FFT)',TIT=ptit
	!p.multi=0

return, tempfft
end

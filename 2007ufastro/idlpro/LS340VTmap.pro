;; interpolate and fit the Cold finger Temp + 0.5 K versus Det.Diode voltage
;; to obtain a curve for Det.Diode Temp as function of voltage.
;; Result is written to file in CRVPT command format for loading LS340.

save,hours,LS340,LS340V,datend,datstart,/verb,fil="DetectorDiode_T-V.idlsave"
!X.title = "Voltage"
!Y.title = "T (K)"

vd=ls340v[*,0]
vk=ls340v[*,1]
tk=ls340[*,1]
w=where(vd gt 0,nw)

hw=hours[w]
vd=vd[w]
vk=vk[w]
tk=tk[w]
td = tk + ((200-tk)>0)/400

plot,vd,tk,ps=3
oplot,vd,td,ps=3
help,vd,td
print,max(vd),min(td),min(tk)

junk=''
read,junk

vmin=.3
vbrk=.9
if N_elements( vfit ) ne 1 then vfit = 2.0
if N_elements( npi ) ne 1 then npi=70
if N_elements( vmax ) ne 1 then vmax=2.5

vx = [ (vbrk-vmin)*findgen(10)/10+vmin, (vfit-vbrk)*findgen(npi)/npi+vbrk ]
tx = interpol( td, vd, vx )

plot,vx,tx,ps=4,xr=[vmin,vfit],/xsty
oplot,vd,td,ps=3

help,vx,tx
print,max(vx),min(tx)

read,junk

plot,vx,tx,ps=4,xr=[vbrk,vfit],/xsty,/ylog,/ysty
oplot,vd,td
read,junk

if N_elements( npf ) ne 1 then npf=70
vxf = (vmax-vfit)*findgen(npf)/(npf-1) + vfit

wf = where( (vd GT vfit) AND (hw LT 95) )
vdf = vd[wf]
vkf = vk[wf]
tkf = tk[wf]
tdf = td[wf]
hwf = hw[wf]
if N_elements( pdeg ) ne 1 then pdeg = 2

if keyword_set( gfit ) then begin
 endif else if keyword_set( LogLogfit ) then begin
	cf = poly_fit( alog(vdf), alog(tdf), pdeg )
	txf = exp( poly( alog(vxf), cf ) )
  endif else if keyword_set( Logfit ) then begin
	cf = poly_fit( vdf, alog(tdf), pdeg )
	txf = exp( poly( vxf, cf ) )
   endif else begin
	cf = poly_fit( vdf, tdf, pdeg )
	txf = poly( vxf, cf )
    endelse

print,cf
plot,vxf,txf,/xsty,/ysty,ps=4
oplot,vdf,tdf,ps=1
read,junk

vx = [vx,vxf]
tx = [tx,txf]

help,vx,tx
print,max(vx),min(tx)

plot,vx,tx,ps=4,xr=[vmin,vmax],/xsty,/ylog,/ysty
oplot,vd,td

crv = 'crv.push_back("CRVPT 21,'
cend = '");'

openw,1,"crvpts.txt"
for i=0,N_elements(vx)-1 do printf,1,crv,i+1,vx[i],tx[i],cend,$
				FORM="(2X,A,I4,',',F8.5,',',F10.5,A)"
close,1
end


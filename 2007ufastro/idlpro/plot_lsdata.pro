pro plot_LSdata, hours, LSdata, CHANNEL=channel, TRAN=Tran, TITLE=title, $
				HARDCOPY=hardcopy, _EXTRA=extra, NSKIP=nskip

	sz = size( LSdata )

	if sz(0) ne 2 then begin
		message,"LS_CHANS must = 2D array",/INFO
		return
	   endif

	if sz(2) LE 2 then LSname="LS340" else LSname="LS218"
	if N_elements( Tran ) ne 2 then Tran=[0,300]
	if N_elements( channel ) ne 1 then channel = 1
	chan = (channel-1) > 0
	ptit = LSname + " ch " + strtrim( channel, 2 )
	if N_elements( title ) eq 1 then ptit = ptit + ": " + title

	if keyword_set( hardcopy ) then psport,/SQ,FIL=LSname+"-ch"+strtrim(channel,2)+".ps"

	if keyword_set( nskip ) then begin
		ndat = N_elements( hours )
		w = nskip * Lindgen( ndat/nskip )
		w = [w,ndat-1]
		x = hours[w]
		y = LSdata[w,chan]
		w = where( y gt 0, nw )
		if( nw GT 0 ) then plot, x[w], y[w], TIT=ptit, yran=Tran, ps=3, $
					XTIT="Hours", YTIT="Temperature ( K )", _EXTRA=extra
	 endif else begin
		plot, hours, LSdata[*,chan], TIT=ptit, yran=Tran, ps=3, $
			XTIT="Hours", YTIT="Temperature ( K )", _EXTRA=extra
	  endelse

	if keyword_set( hardcopy ) then psclose
end

pro Det_TempLog, FILE=file, NDATA=ndata, AGENT=tagent, NTEMPS=ntemps

	if N_elements( ntemps ) ne 1 then ntemps = 2
	if N_elements( ndata ) ne 1 then ndata = 200
	if N_elements( tagent ) ne 1 then tagent = "LakeShore340"
	if N_elements( file ) ne 1 then file="TempLog-" + tagent + ".txt"

	openw,Lun,file,/GET_LUN
	tstart=systime(1) 

	for i=1L,ndata do begin

		nb = UFagentSend(["raw","krdg?a;krdg?b"],"v",tagent)

		Treply = UFagentRecv(phdr,tagent)
		tsec = systime(1)-tstart

		Tarray = get_words( Treply, DELIM=["+",";",string(13b),"E",","] )
		Tarray = Tarray[where(strlen(Tarray) gt 1)]
		Tarray = Tarray[0:ntemps-1]
		print,tsec,Tarray
		printf,Lun,tsec,Tarray
	  endfor

	free_Lun,Lun
end

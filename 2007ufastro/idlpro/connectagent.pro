function ConnectAgent, AGENT=agent, PORT=port, HOST=host

   common AgentConnections, connections
 
	if N_elements( agent ) ne 1 then agent="MCE4"
	if N_elements( host ) ne 1 then host="newton"
	if N_elements( port ) ne 1 then port=52008

	socfd = UFagentConnect( agent, host, port )

	if socfd LE 0 then return, socfd
		
	szc = size( connections )

	if szc[szc[0]+1] eq 8 then begin
		wa = where( agent eq connections.agent, nw )
	 endif else nw = 0

	if nw LE 0 then begin
		message,"sending client name...",/INFO
		cname = "IDLclient"
		nbsent = uf_request( cname, agent )
		nothing = UFagentRecv( phdr, agent )
		print, string(phdr.name)
		newConnection = { CONNECTAGENT,	agent:agent,	$
						port:port,	$
						socket:socfd,	$
						host:host,	$
						client:cname,	$
						handshake:string(phdr.name) }
		if szc[szc[0]+1] eq 8 then $
			connections = [ connections, newConnection ] $
		  else	connections = newConnection
	   endif

return, socfd
end

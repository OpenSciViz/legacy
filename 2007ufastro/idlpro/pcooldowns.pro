oktdiff=ls340[*,0]-ls340[*,1]
okh=hours
if N_elements(nskip) ne 1 then nskip=3
if N_elements(xhran) ne 2 then xhran=[0,90]
w = nskip * indgen( N_elements(badh)/nskip )
plot,badh(w),badtdiff(w),ps=psbad,yra=[-.2,1.5],/ysty,xr=xhran,xtit='Hours',ytit='Tdet - Tcf',/xsty
w = nskip * indgen( N_elements(okh)/nskip )
oplot,okh(w),oktdiff(w),ps=psok,symsize=psymsize
w = nskip * indgen( N_elements(oldh)/nskip )
oplot,oldh(w),oldtdiff(w),ps=psold


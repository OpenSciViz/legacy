#if !defined(__ufgpf26xvacd_cc__)
#define __ufgpf26xvacd_cc__ "$Name:  $ $Id: ufgpf26xvacd.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __ufgpf26xvacd_cc__;

#include "UFGemPfeifferAgent.h"

int main(int argc, char** argv, char** env) {
  try {
    UFGemPfeifferAgent::main(argc, argv, env);
  }
  catch( std::exception& e ) {
    clog<<"ufgpf26xvacd> exception in stdlib: "<<endl;
    return -1;
  }
  catch( ... ) {
    clog<<"ufgpf26xvacd> unknown exception..."<<endl;
    return -2;
  }
  return 0;
}
      
#endif // __ufgpf26xvacd_cc__

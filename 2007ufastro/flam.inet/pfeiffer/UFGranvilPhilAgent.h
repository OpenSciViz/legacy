#if !defined(__UFGranvilPhilAgent_h__)
#define __UFGranvilPhilAgent_h__ "$Name:  $ $Id: UFGranvilPhilAgent.h,v 0.0 2003/11/04 19:17:22 hon beta $"
#define __UFGranvilPhilAgent_H__(arg) const char arg##UFGranvilPhilAgent_h__rcsId[] = __UFGranvilPhilAgent_h__;

#include "string"
#include "vector"
#include "iostream.h"

#include "UFDeviceAgent.h"
#include "UFAnnexIO.h"

class UFGranvilPhilAgent: public UFDeviceAgent {
public:
  static int main(int argc, char** argv);
  UFGranvilPhilAgent(int argc, char** argv);
  UFGranvilPhilAgent(const string& name, int argc, char** argv);
  inline virtual ~UFGranvilPhilAgent() {}

  // override these UFDeviceAgent virtuals:
  // supplemental options (should call UFDeviceAgent::options() last)
  virtual int options(string& servlist);

  virtual UFTermServ* init(const string& host= "192.168.111.101", int port= 0);

  //virtual int initTables(UFDeviceAgent::DevCmdTable& dtbl, UFDeviceAgent::QuerryCmds& qlist);

  // some (raw) GranvilPhil actions return replies, some do not:
  //virtual UFProtocol* action(UFDeviceAgent::CmdInfo* act);
  // new signature for action:
  virtual int action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& rep);

  // all queries should reply
  virtual UFStrings* query(UFDeviceAgent::CmdInfo* q);

  inline string getCurrent() { return _currentval; }

protected:
  string _currentval;
  UFStrings* _allChan(UFDeviceAgent::CmdInfo* req);
  vector < string > _history;
};

#endif // __UFGranvilPhilAgent_h__
      

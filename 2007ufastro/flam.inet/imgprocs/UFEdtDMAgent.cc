#if !defined(__UFEdtDMAgent_cc__)
#define __UFEdtDMAgent_cc__ "$Name:  $ $Id: UFEdtDMAgent.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFEdtDMAgent_cc__;

#include "sys/types.h"
#include "sys/stat.h"
#include "fcntl.h"
#include "strings.h"

#include "UFEdtDMAgent.h"
#include "UFEdtDMAConfig.h"
#include "UFRuntime.h"
#include "UFgd.h"
#include "UFImgHandler.h"
#include "uffits.h"

// global statics
// for use with external test card (since it transmits continously)
// flamingos exposures times will always be greater than this?
double UFEdtDMAgent::_exptime= 0.0; // test/sim exposure time
bool UFEdtDMAgent::_SimAcq= false;
bool UFEdtDMAgent::_obsdone= false;

// make these available to static funcs:
//string UFEdtDMAgent::_instrum;
string UFEdtDMAgent::_edtacq;
string UFEdtDMAgent::_edtfrm;
string UFEdtDMAgent::_edttot;
string UFEdtDMAgent::_car;
string UFEdtDMAgent::_simFITS; // simulation FITS input file
map< UFStrings*, UFInts* > UFEdtDMAgent::_simFrms;
pthread_mutex_t UFEdtDMAgent::_theFrmMutex;
pthread_mutex_t UFEdtDMAgent::_theEdtMutex;
pthread_t UFEdtDMAgent::_dmaThrdId= 0;

// buffer info. (one-time init., no mutex required)
int UFEdtDMAgent::_maxBuff= 3;
map < string, int > *UFEdtDMAgent::_nameIdxFrmBuff= 0; // index # of named frame buff.
// use theFrmMutex when accessing this:
deque < UFInts* > *UFEdtDMAgent::_dataFrmBuff= 0; // each named buff is a UFInt object

UFFrameConfig* UFEdtDMAgent::_theFrmConf= 0;
UFObsConfig* UFEdtDMAgent::_theObsConf= 0;
UFDs9 UFEdtDMAgent::_ds9;
int UFEdtDMAgent::_totalcnt= 0; // for observation
int UFEdtDMAgent::_edttotal= 0; // for all observations thus far

// globals below need to be cleared for each new dma thread?
// abort discards, stop saves data.?
// take options stored as Argv:
UFRuntime::Argv UFEdtDMAgent::_acqArgs;

// post acq. activities
int UFEdtDMAgent::_postcnt= 0;
int UFEdtDMAgent::_ds9Disp= 0; // # image frames to expect in the ds9 display (0 - 4)
bool UFEdtDMAgent::_jpeg= false;
bool UFEdtDMAgent:: _png= false;
int UFEdtDMAgent::_scale= 1;

void UFEdtDMAgent::_clearAll() {
  // globals below need to be cleared for each new dma thread?
  //_replicate= 0; _replicate is inherited from RndRobinServ

  // abort discards, stop saves data.?
  // take options stored as Argv:
  UFEdtDMAgent::_acqArgs.clear();

  // post acq. activities
  UFEdtDMAgent::_postcnt= 0;
  UFEdtDMAgent::_ds9Disp= 0; // # image frames to expect in the ds9 display (0 - 4)
  UFEdtDMAgent::_jpeg= false;
  UFEdtDMAgent:: _png= false;
  UFEdtDMAgent::_scale= 1;
}

// helper for ctors:
void UFEdtDMAgent::_setDefaults(const string& instrum) {
  _StatRecs.clear();
  UFDeviceAgent::_setDefaults(instrum);
  if( instrum != "" && instrum != "false" ) {
    _epics = instrum;
    _confGenSub = "";
    _heart = ""; //_epics + ":ec:heartbeat.vacuum";
    //_StatRecs.push_back(_epics + ":sad:EDTFRAME.INP");
    _StatRecs.push_back(_epics + ":sad:EDTFRAME.VAL");
    //_StatRecs.push_back(_epics + ":sad:EDTTOTAL.INP");
    _StatRecs.push_back(_epics + ":sad:EDTTOTAL.VAL");
    //_StatRecs.push_back(_epics + ":sad:EDTACTN.INP");
    _StatRecs.push_back(_epics + ":sad:EDTACTN.VAL");
    _edtfrm = _statRec = _StatRecs[0];
    _edttot = _StatRecs[1];
    _edtacq = _StatRecs[2];
  }
  else {
    _confGenSub = "";
    _heart = "";
    _edtacq = _edttot = _edtfrm = _statRec = "";
  }
}

// ctors:
UFEdtDMAgent::UFEdtDMAgent(const string& name, int argc,
			   char** argv, char** envp) : UFGemDeviceAgent(name, argc, argv, envp) {
  _instrum = name;
  UFRndRobinServ::threadedServ();
  ::pthread_mutex_init(&_theFrmMutex, 0);
  ::pthread_mutex_init(&_theEdtMutex, 0);

  _allocNamedBuffs(name);
}

// static funcs:
int UFEdtDMAgent::main(const string& name, int argc, char** argv, char** envp) {
  UFEdtDMAgent ufdma(name, argc, argv, envp); // ctor binds to listen socket
  // enter event loop:
  ufdma.exec((void*)&ufdma);
  return 0;
}

void UFEdtDMAgent::_sighandler(int sig) {
  switch(sig) {
  case SIGTERM:
    if( UFDeviceAgent::_verbose )
      clog<<"UFEdtDMAgent::_sighandler> (SIGTERM) sig: "<<sig<<endl;
  case SIGINT: {
    if( UFDeviceAgent::_verbose )
       clog<<"UFEdtDMAgent::_sighandler> (SIGINT) sig: "<<sig<<endl;
    if( _dmaThrdId ) {
      ::pthread_cancel(_dmaThrdId); // cancel handler unlocks mutex if edtdma thread owns it
      _dmaThrdId = 0;
    }
    UFRndRobinServ::_shutdown = true;
    UFRndRobinServ::shutdown();
  }
  default:
    if( UFDeviceAgent::_verbose )
      clog<<"UFEdtDMAgent::_sighandler> (default) sig: "<<sig<<endl;
    UFRndRobinServ::sighandlerDefault(sig); // handle SIGPIPE, SIGCHILD
    break;
  }
}

void UFEdtDMAgent::_cancelhandler(void* p) {
  UFEdtDMAgent* dma = static_cast< UFEdtDMAgent* > (p);
  if( !_epics.empty() && _epics != "false" )
    if(!_edtacq.empty() && !_obsdone ) sendEpics(_edtacq, "Aborted/Stopped");
  // always unlock mutexes on cancellation of thread
  ::pthread_mutex_unlock(&dma->_theFrmMutex);
  ::pthread_mutex_unlock(&dma->_theEdtMutex);
}

// acqbuf is raw pixel data, fitbuf is (lut sorted) image with simple fits header:
void UFEdtDMAgent::_postacq(unsigned char* acqbuf, char* fitsbuf, int sz, const char* filenm) {
  ++_postcnt; ++_edttotal;
  if( UFDeviceAgent::_verbose ) 
    clog<<"UFEdtDMAgent::_postacq> acqbuf, fitsbuf: "<< (int)acqbuf <<", "<< (int)fitsbuf<<", sz: "<<sz<<endl;

  if( !_epics.empty() && _epics != "false" ) {
    if(!_edtacq.empty() ) sendEpics(_edtacq, "PostProcessing");
    if(!_edtfrm.empty() ) sendEpics(_edtfrm, UFRuntime::concat("", _postcnt));
    if(!_edttot.empty() ) sendEpics(_edttot, UFRuntime::concat("", _edttotal));
  }

  if( _ds9Disp && (filenm != 0 || fitsbuf != 0) ) {
    if( UFDeviceAgent::_verbose )
      if( filenm != 0 )
        clog<<"UFEdtDMAgent::_postacq> DS9 fits:"<<filenm<<endl;
      else 
        clog<<"UFEdtDMAgent::_postacq> DS9 fits, no filename specified"<<endl;
    int stat = -1;
    if( filenm != 0 )
      stat  = _ds9.display(filenm, _ds9Disp);
    else if( fitsbuf != 0 && sz > 0 )
      stat  = _ds9.display(fitsbuf, sz, _ds9Disp);
    else
      clog<<"UFEdtDMAgent::_postacq> DS9 no fits data?"<<endl;

    if( stat < 0 )
      clog<<"_postacq> Ds9 display failed?"<<endl;
  }

  // also insert acquired frame into frame buffer replcation queue
  int* imgbuf = (int*) (&fitsbuf[2880]);
  if( _replicate )
    _replFrm((int*) imgbuf);

  //if( _dataFrmBuff->size() > 0 ) // set history data full & qlook frames
    _histFrm((int*) imgbuf);

  // and while the above is being handled,
  // write jpeg and/or png to disk:
  if( _jpeg || _png ) {
    if( UFDeviceAgent::_verbose )
      clog<<"UFEdtDMAgent::_postacq> _jpeg, _png: "<<_jpeg<<", "<<_png<<endl;
    _writeImg(fitsbuf, sz, filenm, _png, _jpeg);
  }

  // presumably this is followed by another frame acq, or completion to idle..
  if( !_epics.empty() && _epics != "false" )
    if( !_edtacq.empty() ) sendEpics(_edtacq, "Acquiring"); 

  if( threaded() && _exptime >= 0.001) {
    // this helps when using the extiernal simulator (since it sends continuously)
    clog<<"UFEdtDMAgent::_postacq> dma thread _exptime: "<<_exptime<<" (sec.)"<<endl;
    UFPosixRuntime::sleep(_exptime); // allow other threads some cpu
  }
} // _postacq

void UFEdtDMAgent::startup() {
  // start signal handler thread
  sigWaitThread(UFEdtDMAgent::_sighandler); // create a thread devoted to all signals of interrest
  //clog << "UFEdtDMAgent::startup> established signal handler (sigwait) thread."<<endl;
  // should parse cmd-line options and start listening
  string servlist;
  int listenport = options(servlist);
  init();
  UFSocketInfo socinfo = _theServer.listen(listenport);

  clog << "UFEdtDMAgent::startup> listening on port= " <<listenport<<endl;
  //clog<<", with server soc: "<<_theServer.description()<<endl;
  return;  
}

UFTermServ* UFEdtDMAgent::init() {
  // save edtconfig to a file
  pid_t p = getpid();
  strstream s;
  s<<"/usr/tmp/.edtconf."<<p<<".txt"<<ends;
  int fd = ::open(s.str(), O_RDWR | O_CREAT, 0777);
  if( fd <= 0 ) {
    clog<<"UFEdtDMAgent> unable to open "<<s.str()<<endl;
    delete s.str();
    return 0;
  }
  delete s.str();

  int w= 0, h= 0;
  UFEdtDMA::dimensions(w, h);
  if( UFEdtDMAgent::_theFrmConf == 0 )
    UFEdtDMAgent::_theFrmConf = new UFFrameConfig(w, h, 32, false, 1, 1); 

  strstream ss;
  ss<<name()<<"> Initial/current Edt dev. conf is: "<<w<<"x"<<h<<endl;
  char *edtstr = ss.str();
  int nc = ::write(fd, edtstr, strlen(edtstr));
  if( nc <= 0 ) {
    clog<<"UFEdtDMAgent> edt config log failed..."<<endl;
  }
  ::close(fd);

  if( _sim ) {
    _theObsConf = _readMEF(_simFITS, _simFrms);
  }

  if( _epics != "false" ) {
    _capipe = pipeToEpicsCAchild(_epics);
    if( _capipe )
      clog<<"UFEdtDMAgent::init> Epics CA child proc. started, fd: "<<fileno(_capipe)<<endl;
    else
      clog<<"UFEdtDMAgent::init> Ecpics CA child proc. failed to start"<<endl;
  }

  return 0;
} // init

UFObsConfig* UFEdtDMAgent::_readMEF(const string& filenm, map< UFStrings*, UFInts* >& data) {
  int fd = ::open(filenm.c_str(), O_RDONLY);
  if( fd < 0 ) 
    return 0;

  UFObsConfig* oc = new UFObsConfig(filenm);
  // parse the MEF to create an obsconfig & load all headers & frames into data:
  // ...
  // ...
  ::close(fd);
  return oc;
}

// this should always return the service/agent listen port #
int UFEdtDMAgent::options(string& servlist) {
  int port = UFDeviceAgent::options(servlist);
  port = 52001; // force it to this
  _config = new UFEdtDMAConfig(); // needs to be proper device subclass
  // portescap defaults for trecs:
  _config->_tsport = -1;
  _config->_tshost = "";

  _setDefaults("flam");

  // _epics may have been reset by cmd line:
  string arg = findArg("-noepics"); // epics host/db name
  if( arg == "true"  ) {
    _setDefaults("false");
  }

  arg = findArg("-epics"); // epics db name
  if( arg != "false" &&  arg != "true" ) {
    _setDefaults(arg);
  }

  arg = findArg("-v");
  if( arg == "true" ) {
    UFPosixRuntime::_verbose = UFDeviceAgent::_verbose = true;
  }
  arg = findArg("-vv");
  if( arg == "true" ) {
    UFEdtDMA::_verbose = UFDeviceAgent::_verbose = UFPosixRuntime::_verbose = UFPSem::_verbose = true;
  }

  arg = findArg("-mmap");
  if( arg == "true" )
    UFEdtDMA::_usemmap = true;

  arg = findArg("-sim");
  if( arg == "true" ) {
    _sim = true;
    _SimAcq = _sim;
  }

  arg = findArg("-exp");
  if( arg != "false" && arg != "true" ) {
    _exptime = ::atof(arg.c_str());
  }
  if( UFDeviceAgent::_verbose || UFEdtDMA::_verbose )
    clog<<"UFEdtDMAgent::dmaThread> exposure time (sec.): "<<_exptime<<endl;
  /*
  if( _config->_tsport >= 0 ) {
    port = 52000 + _config->_tsport - 7000;
  }
  else {
    port = 52001;
  }
  */

  if( UFDeviceAgent::_verbose ) {
    if( threaded() )
      clog<<"UFEdtDMAgent::options> this service is threaded..."<<endl;
    clog<<"UFEdtDMAgent::options> set port to EdtDMAgent port "<<port<<endl;
  }
  return port;
} // options

int UFEdtDMAgent::_setAcqArgs(UFDeviceAgent::CmdInfo* act) {
  _acqArgs.clear();
  for( int i = 0; i < (int)act->cmd_name.size(); ++i ) {
    _acqArgs.push_back(act->cmd_name[i]); _acqArgs.push_back(act->cmd_impl[i]);
  }

  return (int)_acqArgs.size();
}

// the key virtual function(s) to override,
// presumably any allocated (UFStrings reply) memory is freed by the calling layer....
int UFEdtDMAgent::action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& rep) {
  UFObsConfig* ufobs = UFEdtDMAConfig::obsconf(act);
  if( ufobs ) {
    clog<<"UFEdtDMAgent::action> got new observation start, with new obsconf."<<endl;
    delete _theObsConf; // free old obs. conf.
    _theObsConf = ufobs; // new obs conf.
    _totalcnt = ufobs->chopBeams() * ufobs->saveSets() * ufobs->nodBeams() * ufobs->nodSets();
  }
  bool reply_expected= true;
  int stat = -1;

  // allocate empty reply vectot:
  rep = new vector< UFProtocol* >;
  vector< UFProtocol* >& replies = *rep;
  string reply, simInstrum = name();
  for( int i = 0; i < (int)act->cmd_name.size(); ++i ) {
    string cmdname = act->cmd_name[i];
    string cmdimpl = act->cmd_impl[i];
    if( UFDeviceAgent::_verbose )
      clog<<"UFEdtDMAgent::action> "<<cmdname<<" :: "<<cmdimpl<<endl;

    int nr = _config->validCmd(cmdname, cmdimpl);
    if( nr < 0 ) {
      clog<<"UFEdtDMAgent::action> ?Bad cmd: "<<cmdname<<" :: "<<cmdimpl<<endl;
      continue;
    }
    act->time_submitted = currentTime();
    _active = act;

    // check if this bundle is from the epics db (or the dc agent, or some other client)
    if( cmdname.find("car") == 0 || cmdname.find("Car") == 0 || cmdname.find("CAR") == 0 ) {
      reply_expected= false;
      _car = _carRec = cmdimpl;
      if( UFDeviceAgent::_verbose ) clog<<"UFEdtDMAgent::action> CAR: "<<_car<<endl;
      continue;
    }

    // check if this bundle is from the epics db (or the dc agent, or some other client)
    if( cmdname.find("sad") == 0 || cmdname.find("Sad") == 0 || cmdname.find("SAD") == 0 ) {
      reply_expected= false;
      _edtfrm = _statRec = cmdimpl;
      if( UFDeviceAgent::_verbose ) clog<<"UFEdtDMAgent::action> SAD: "<<_edtfrm<<endl;
      continue;
    }

    // check if sim, don't create dma thread
    if( cmdname.find("sim") != string::npos ||
	cmdname.find("Sim") != string::npos ||
	cmdname.find("SIM") != string::npos) {
	_sim = true;
        if( cmdimpl.find("flam") != string::npos || cmdimpl.find("Flam") != string::npos || cmdimpl.find("FLAM") != string::npos ) 
	  simInstrum = "flam";
    }

    if( cmdname.find("shutdown") != string::npos || cmdname.find("Shutdown") != string::npos || 
	cmdname.find("SHUTDOWN") != string::npos ) {
      if( _dmaThrdId ) {
        ::pthread_cancel(_dmaThrdId); // cancel handler unlocks mutex if edtdma thread owns it
        _dmaThrdId = 0;
      }
      UFRndRobinServ::_shutdown = true; //UFRndRobinServ::shutdown();
      if( reply_expected ) {
        reply = "Ok, shutting down: " + cmdname + "::" + cmdimpl;
        act->cmd_reply.push_back(reply);
        replies.push_back(new UFStrings("UFEdtDMAgent::action> shutdown", &reply));
      }
      if( !_epics.empty() && _epics != "false" && !_car.empty() )
        setCARIdle(_car);

      break;
    }

    if( cmdname.find("acq") != string::npos || cmdname.find("Acq") != string::npos || 
	cmdname.find("ACQ") != string::npos ) {
      if( cmdimpl.find("abort") != string::npos || cmdimpl.find("Abort") != string::npos ||
	  cmdimpl.find("ABORT") != string::npos ) {
	// abort acquisition
        ::pthread_mutex_unlock(&_theEdtMutex); // fails if not owned by this thread
        if( _dmaThrdId )
	  ::pthread_cancel(_dmaThrdId); // cancel handler unlocks mutex if edtdma thread owns it
        stat = (int) _dmaThrdId = 0;
        if( reply_expected ) {
  	  reply = "Ok, aborted DMA: " + cmdname + "::" + cmdimpl;
          act->cmd_reply.push_back(reply);
          replies.push_back(new UFStrings("UFEdtDMAgent::action> acq::abort", &reply));
	}
	break;
      } // acq::abort
      if( cmdimpl.find("stop") != string::npos || cmdimpl.find("Stop") != string::npos || 
	  cmdimpl.find("STOP") != string::npos ) {
	// get dma status cmdimpl == fits/other
        ::pthread_mutex_unlock(&_theEdtMutex); // fails if not owned by this thread
        if( _dmaThrdId ) 
	  ::pthread_cancel(_dmaThrdId); // cancel handler unlocks mutex if edtdma thread owns it
        stat = (int) _dmaThrdId = 0;
	reply = "Ok, stopped DMA: " + cmdname + "::" + cmdimpl;
        act->cmd_reply.push_back(reply);
        replies.push_back(new UFStrings("UFEdtDMAgent::action> acq::stop", &reply));
	break;
      } // acq::stop
      if( cmdimpl.find("start") != string::npos || cmdimpl.find("Start") != string::npos ||
	  cmdimpl.find("START") != string::npos ) {
        // start acquisition
        stat = _setAcqArgs(act);
        int w, h;
        bool idle = UFEdtDMA::takeIdle(w, h);
        if( UFDeviceAgent::_verbose )
	  clog<<"UFEdtDMAgent::action> takeIdle: "<<idle<<"; w, h = "<<w<<", "<<h<<endl;
        if( !idle ) { // 
          clog<<"UFEdtDMAgent::action> "<<cmdimpl<<" :: "<<cmdimpl<<" rejected, Waiting on Image"<<endl;
          strstream s;
          s<<cmdimpl<<" :: "<<cmdimpl<<" rejected, dma still in progress"<<ends;
          reply = s.str(); delete s.str();
          act->cmd_reply.push_back(reply);
          replies.push_back(new UFStrings("UFEdtDMAgent::action> acq::start", &reply));
	  break;
        }
        else if( _dmaThrdId > 0 ) { 
	  int m = ::pthread_mutex_trylock(&_theEdtMutex);
	  if( m != 0 && errno == EBUSY ) // edt dma in process, reject request
          clog<<"UFEdtDMAgent::action> "<<cmdname<<" :: "<<cmdimpl<<" rejected, dma (thread) still in progress"<<endl;
          strstream s;
          s<<cmdname<<" :: "<<cmdimpl<<" rejected, dma still in progress"<<ends;
          reply = s.str(); delete s.str();
          act->cmd_reply.push_back(reply);
          replies.push_back(new UFStrings("UFEdtDMAgent::action> acq::start", &reply));
	  break;
        }

	// send (new?) obsconf to replication client:
        if( _replicate &&  _theObsConf )  {
          clog<<"UFEdtDMAgent::action> sending new obsconf to replication client..."<<endl;
	  _replicate->send(_theObsConf);
        }
        // create & run dma thread
	// dmaThread should lock and unlock the EdtMutex before exiting..
        stat = (int)_dmaThrdId = newThread(_dmaThread, this);
	reply = "Ok, started DMA thread for: " + cmdname + "::" + cmdimpl;
        act->cmd_reply.push_back(reply);
        replies.push_back(new UFStrings("UFEdtDMAgent::action> acq::start", &reply));
	break;
      } // acq start
    } // end acq
    else if( cmdname.find("fram") != string::npos || cmdname.find("Fram") != string::npos ||
	     cmdname.find("FRAM") != string::npos ) { // fetch frame from cmdimpl == buffer name or index
      const char* c = cmdimpl.c_str();
      int frmidx = 0;
      if( isdigit(c[0]) ) {// specify frame by index (index 0 should be most recent full image) or by name 
	frmidx = atoi(c);
      }
      else if( cmdimpl == "flam:FullImage" || cmdimpl == "flam:1024Image" || cmdimpl == "flam:512Image") {
	frmidx = (*_nameIdxFrmBuff)[cmdimpl];
      }
      else if( cmdimpl == "trecs:src1" || cmdimpl == "trecs:ref1" || cmdimpl == "trecs:src2" || cmdimpl == "trecs:ref2" ||
	       cmdimpl == "trecs:dif1" || cmdimpl == "trecs:dif2" ||
	       cmdimpl == "trecs:accum(src1)" || cmdimpl == "trecs:accum(ref1)" || cmdimpl == "trecs:accum(dif1)" ||
	       cmdimpl == "trecs:accum(src2)" || cmdimpl == "trecs:accum(ref2)" || cmdimpl == "trecs:accum(dif2)" ||
	       cmdimpl == "trecs:sig"  || cmdimpl == "trecs:accum(sig)" )
	frmidx = (*_nameIdxFrmBuff)[cmdimpl];
      else {
	frmidx = _maxBuff-1;
      }
      if( frmidx >= _maxBuff ) frmidx = _maxBuff-1; 
      if( frmidx < 0 ) frmidx = 0;
      UFInts* frm = (*_dataFrmBuff)[frmidx];
      //int m = ::pthread_mutex_trylock(&_theFrmMutex); // fails if not owned by this thread
      int m = ::pthread_mutex_lock(&_theFrmMutex); // blocks...
      if( m != 0 ) { // failed to get the lock
        reply = "UFEdtDMAgent::action> unable to access frame buffer, mutex still locked...";
        act->cmd_reply.push_back(reply);
        replies.push_back(new UFStrings("UFEdtDMAgent::action> frame", &reply));
        break;
      }
      strstream s;
      s<<"UFEdtDMAgent::action> sending "<<frm->name()<<", pixcnt: "<<frm->numVals()<<ends;
      reply = s.str(); delete s.str();
      if( UFDeviceAgent::_verbose )
        clog<<reply<<endl;
      replies.push_back(new UFStrings("UFEdtDMAgent::action> frame", &reply));
      // make a deep copy to be sent back to client (and deleted by UFDeviceAgent::exec)
      UFInts* img = new UFInts(frm->name(), (int*)frm->valData(), frm->numVals());
      ::pthread_mutex_unlock(&_theFrmMutex);
      int cnt = takeCnt(), tot = takeTotCnt();
      // send frame config, followed by image?
      /*
      UFFrameConfig* fc = new UFFrameConfig(width(), height());
      int edtdone = doneCnt();
      fc->setDMACnt(edtdone);
      fc->setFrameObsSeqTot(tot);
      fc->setImageCnt(cnt);
      fc->setFrameWriteCnt(cnt);
      replies.push_back(fc);
      */
      img->setSeq(cnt, tot); // (ab)use the seq. cnt in header for frame info.
      replies.push_back(img); // replies are sent back via child thread...
      return (int) replies.size(); // reply w/o conf & image objects
    }
    else if( cmdname.find("conf") != string::npos ||
	     cmdname.find("Conf") != string::npos ||
	     cmdname.find("CONF") != string::npos ) { // fetch obs or frame config, cmdimpl == frm/obs
      bool all = (cmdimpl.find("all") != string::npos ||
	          cmdimpl.find("All") != string::npos || 
	          cmdimpl.find("ALL") != string::npos);
      if( all ) {
        int cnt = takeCnt(), tot = takeTotCnt(), edtdone = doneCnt();
        UFFrameConfig* fc = new UFFrameConfig(width(), height());
        fc->setDMACnt(edtdone);
        fc->setFrameObsSeqTot(tot);
        fc->setImageCnt(cnt);
        fc->setFrameWriteCnt(cnt);
        UFObsConfig* oc = new UFObsConfig();
	reply = "UFEdtDMAgent::action> sending " + fc->name() + ", and " + oc->name();
	replies.push_back(new UFStrings("UFEdtDMAgent::action> frame", &reply));
        replies.push_back(fc);
        replies.push_back(oc);
      } // all configs
      else if( cmdimpl.find("frame") != string::npos ||
	       cmdimpl.find("Frame") != string::npos || 
	       cmdimpl.find("FRAME") != string::npos ) {
        int cnt = takeCnt(), tot = takeTotCnt(), edtdone = doneCnt();
        UFFrameConfig* fc = new UFFrameConfig(width(), height());
        fc->setDMACnt(edtdone);
        fc->setFrameObsSeqTot(tot);
        fc->setImageCnt(cnt);
        fc->setFrameWriteCnt(cnt);
	reply = "UFEdtDMAgent::action> sending " + fc->name();
	replies.push_back(new UFStrings("UFEdtDMAgent::action> frame", &reply));
        replies.push_back(fc);
      } // frame config
      else if( cmdimpl.find("obs") != string::npos ||
	       cmdimpl.find("Obs") != string::npos || 
	       cmdimpl.find("OBS") != string::npos ) {
	reply = "UFEdtDMAgent::action> sending " + _theObsConf->name();
	replies.push_back(new UFStrings("UFEdtDMAgent::action> frame", &reply));
        replies.push_back(_theObsConf);
      } // obs config
      return (int) replies.size(); // reply with image object
    }
    else if( cmdname.find("stat") != string::npos || cmdname.find("Stat") != string::npos ||
	     cmdname.find("STAT") != string::npos ) { // get dma status cmdimpl == edt, fits/other
      if( cmdimpl.find("edt") != string::npos || cmdimpl.find("Edt") != string::npos ||
	  cmdimpl.find("EDT") != string::npos ||
	  cmdimpl.find("frm") != string::npos || cmdimpl.find("Frm") != string::npos ||
	  cmdimpl.find("FRM") != string::npos ) { // edt config (initcam) & take sem. counter vals
        reply = UFEdtDMA::status();
	replies.push_back(new UFStrings("UFEdtDMAgent::action> stat::edt", &reply));
        stat = reply.length();
      }
      else if( cmdimpl.find("fits") != string::npos || cmdimpl.find("Fits") != string::npos ||
	       cmdimpl.find("FITS") != string::npos ) {
        act->cmd_reply.push_back(reply);
	replies.push_back(_config->statusFITS(this));
      }
      else {
        act->cmd_reply.push_back(reply);
        replies.push_back(_config->status(this));
      }
    }
    else if( cmdname.find("archv") != string::npos ||
	     cmdname.find("Archv") != string::npos ||
	     cmdname.find("ARCHV") != string::npos ) { // query (cmdimpl == '?') or set fits file pathname
      clog<<"UFEdtDMAgent::action> "<<cmdname<<" :: "<<cmdimpl<<" not supported."<<endl;
      strstream s;
      s<<cmdname<<" :: "<<cmdimpl<<" not supported."<<ends;
      reply = s.str(); delete s.str();
      act->cmd_reply.push_back(reply);
      replies.push_back(new UFStrings("UFEdtDMAgent::action> archv", &reply));
    }
    else if( cmdname.find("obs") != string::npos ||
	     cmdname.find("Obs") != string::npos ||
	     cmdname.find("OBS") != string::npos ) { // reply with obsconf & frameconf?
      int cnt = takeCnt(), tot = takeTotCnt(), edtdone = doneCnt();
      UFFrameConfig* fc = new UFFrameConfig(width(), height());
      fc->setDMACnt(edtdone);
      fc->setFrameObsSeqTot(tot);
      fc->setImageCnt(cnt);
      fc->setFrameWriteCnt(cnt);
      reply = "UFEdtDMAgent::action> sending " + fc->name();
      replies.push_back(new UFStrings("UFEdtDMAgent::action> frame", &reply));
      replies.push_back(fc);
      reply = "UFEdtDMAgent::action> sending " + _theObsConf->name();
      replies.push_back(new UFStrings("UFEdtDMAgent::action> frame", &reply));
      replies.push_back(_theObsConf);
    }
    else if( ufobs == 0 ) {
      clog<<"UFEdtDMAgent::action> "<<cmdname<<" :: "<<cmdimpl<<" not supported."<<endl;
      strstream s;
      s<<cmdname<<" :: "<<cmdimpl<<" not supported."<<ends;
      reply = s.str(); delete s.str();
      act->cmd_reply.push_back(reply);
      replies.push_back(new UFStrings("UFEdtDMAgent::action> unknown", &reply));
    }
  } // end for loop of cmd bundle

  if( stat < 0 )
    act->status_cmd = "failed";
  else if( act->cmd_reply.empty() )
    act->status_cmd = "failed/rejected";
  else
    act->status_cmd = "succeeded";
  
  act->time_completed = currentTime();
  _completed.insert(UFDeviceAgent::CmdList::value_type(act->cmd_time, act));

  replies.push_back(new UFStrings(act->clientinfo, act->cmd_reply));
  return (int) replies.size();
} 


int UFEdtDMAgent::query(const string& q, vector<string>& qreply) {
  return UFDeviceAgent::query(q, qreply);
}

void UFEdtDMAgent::dmaThread(UFEdtDMAgent& dma) {
  string usage = "[-v] [-nosem] [-stdout] [-index start index] [-cnt frmcnt] [-timeout sec.] [-lut [lutfile]] [-fits fitsfile] [-file hint] [-rename] [-lock lockfile] [-ds9] [-jpeg [scale]] [-png [scale]] [-frmconf]";

  // is agent in very verbose mode?
  string arg = dma.findArg("-vv");
  if( arg == "true" ) {
    UFEdtDMA::_verbose = UFDeviceAgent::_verbose = UFPosixRuntime::_verbose = UFPSem::_verbose = true;
  }

  if( UFDeviceAgent::_verbose ) {
    clog<<"UFEdtDMAgent::dmaThread> _exptime: "<<_exptime<<endl;
    for( int i = 0; i < (int)dma._acqArgs.size(); ++i )
      clog<<"UFEdtDMAgent::dmaThread> arg# "<<i<<" : "<<dma._acqArgs[i]<<endl;
  }
 
  arg = UFRuntime::argVal("-exp", dma._acqArgs);
  if( arg != "false" && arg != "true" ) {
    _exptime = ::atof(arg.c_str());
  }
  if( UFDeviceAgent::_verbose || UFEdtDMA::_verbose )
    clog<<"UFEdtDMAgent::dmaThread> exposure time (sec): "<<_exptime<<endl;

  _ds9Disp = false;
  arg = UFRuntime::argVal("-ds9", dma._acqArgs);
  if( arg != "false" ) {
    if( UFDeviceAgent::_verbose ) clog<<"UFEdtDMAgent::dmaThread> display to ds9..."<<endl;
    _ds9Disp = 1;
    const char* cs = arg.c_str();
    if( isdigit(cs[0]) )
      _ds9Disp = atoi(cs);
  }

  _png = false;
  arg = UFRuntime::argVal("-png", dma._acqArgs);
  if( arg != "false" ) {
    _png = true;
    if( arg != "true" ) // scale png
      _scale = atoi(arg.c_str());
    if( UFDeviceAgent::_verbose ) clog<<"UFEdtDMAgent::dmaThread> write PNG (portable network graphics) file(s), _scale: "<<_scale<<endl;
  }

  _jpeg = false;
  arg = UFRuntime::argVal("-jpg", dma._acqArgs);
  if( arg != "false" ) {
    _jpeg = true;
    if( arg != "true" ) // scale jpeg
      _scale = atoi(arg.c_str());
    if( UFDeviceAgent::_verbose ) clog<<"UFEdtDMAgent::dmaThread> write JPEG file(s), _scale: "<<_scale<<endl;
  }
  arg = dma.UFRuntime::argVal("-jpeg", dma._acqArgs);
  if( arg != "false" ) {
    _jpeg = true;
    if( arg != "true" ) // scale jpeg requested
      _scale = atoi(arg.c_str());
    if( UFDeviceAgent::_verbose ) clog<<"UFEdtDMAgent::dmaThread> write JPEG file(s), _scale: "<<_scale<<endl;
  }

  int ecnt= 1, timeOut= 0, index= 0, fits_sz= 0;
  char *filehint= 0, *fits= 0;
  int* lut= 0; 
  // this also examines acq args:
  UFEdtDMA::init(dma._acqArgs, ecnt, timeOut, lut, index, filehint, fits, fits_sz);
  if( fits == 0 ) {
    if( width() == 2048 ) {
      if( UFDeviceAgent::_verbose ) clog<<"UFEdtDMAgent::dmaThread> set fits header to default for current EDT config.(FLAMINGOS)"<<endl;
      fits = (char*)fits2048;
      fits_sz = strlen(fits);
    }
    else {
      if( UFDeviceAgent::_verbose ) clog<<"UFEdtDMAgent::dmaThread> set fits header to default for current EDT config.(TReCS)"<<endl;
      fits = (char*)fits320x240;
      fits_sz = strlen(fits);
    }
  }
  UFEdtDMA::setDpySeq(1); // call postacquisition function modula 1
  // enter frame acquisition/take loop:
  // postacq func. should lock the FrmMutex, copy the frame into the _dmeFrameBuff UFInt*
  // and setSeq(takeCnt(), takeTotal())... 
  dma._postcnt = 0; // clear the global counte

  // clear the ThrdId
  dma._dmaThrdId = 0;
 
  //if( UFDeviceAgent::_verbose )
    clog<<"UFEdtDMAgent::dmaThread> Start DMA acq. of frames: "<<ecnt<<", time: "<<UFRuntime::currentTime()<<endl;

  _obsdone = false;
  int cnt= 0;
  if( !_SimAcq )
    cnt = UFEdtDMA::takeAndStore(ecnt, index, filehint, lut, fits, fits_sz, _postacq);
  else
    cnt = UFEdtDMA::takeAndStore(ecnt, index, filehint, lut, fits, fits_sz, _postacq, _simacq);

  //if( UFDeviceAgent::_verbose )
    clog<<"UFEdtDMAgent::dmaThread> Completed DMA acq. of frames: "<<cnt<<", time: "<<UFRuntime::currentTime()<<endl;

  _obsdone = true;

  return;
}

// allow dma event loop to be run in its own (cancellable) thread:
void* UFEdtDMAgent::_dmaThread(void* p) {
  if( p == 0 )
    return p;

  UFEdtDMAgent* dma = static_cast< UFEdtDMAgent* > (p);
  // lock edt dma loop
  int m = ::pthread_mutex_trylock(&dma->_theEdtMutex);
  if( m != 0 && errno == EBUSY ) { // edt dma in process?
    clog<<"UFEdtDMAgent::_dmaThread> edt mutex lock failed ? dma in progress"<<endl;
    return p;
  }
   
  int oldstate, oldtype;
  ::pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, &oldstate);
  ::pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, &oldtype);
// since linux & solaris "pthread.h" declare pthread_cleanup_push as a macro
// that g++ seems to have problems compiling on each, use the actual
// extern function
#if defined(LINUX)
  struct _pthread_cleanup_buffer _buffer;
  ::_pthread_cleanup_push(&_buffer, UFEdtDMAgent::_cancelhandler, p);
#else
  _cleanup_t _cleanup_info;
  ::__pthread_cleanup_push(UFEdtDMAgent::_cancelhandler, p,
			   (caddr_t)_getfp(), &_cleanup_info);
#endif  
  // clear acq. dma related globals (of any lingering values from prior thread)
  //_clearAll();

  // if commanded from epics clear the current obs. edt frame count sad
  if( !_epics.empty() && _epics != "false" ) {
    if( !_edtacq.empty() ) sendEpics(_edtacq, "Acquiring"); 
    if( !_edtfrm.empty() ) sendEpics(_edtfrm, "0");
  } 

  // enter dma event loop:
  dma->dmaThread(*dma);

  // if commanded from epics, and _car/_sad set, indicate command completion:
  if( !_epics.empty() && _epics != "false" ) {
    if( !_edtacq.empty() && _obsdone) sendEpics(_edtacq, "Idle"); 
    if( !_edtacq.empty() && !_obsdone) sendEpics(_edtacq, "Aborted/Stopped"); 
    if( !_edtfrm.empty() ) sendEpics(_edtfrm, UFRuntime::concat("", dma->_postcnt));
    if( !_edttot.empty() ) sendEpics(_edttot, UFRuntime::concat("", dma->_edttotal));
    if( !_car.empty() ) setCARIdle(_car);
  }
  
  // clear the ThrdId
  dma->_dmaThrdId = 0;

  // upon return from edt dma loop, unlock
  ::pthread_mutex_unlock(&dma->_theEdtMutex);

  int exitstat;
  ::pthread_exit(&exitstat);
  return p;
}

// helpers
int UFEdtDMAgent::_allocNamedBuffs(const string& instrum) {
  //clog<<"UFEdtDMAgent::_allocNamedBuffs> instrum: "<< instrum<<endl;
  if( _nameIdxFrmBuff == 0 )
    _nameIdxFrmBuff = new map < string, int >;
  if( _dataFrmBuff == 0 )
    _dataFrmBuff = new deque < UFInts* >;

  vector< string > names;
  _maxBuff = _buffNames(instrum, names);

  string bufname;
  int elem = width() * height(); // if failed, use defaults
  vector< int > elems;
  for( int i = 0; i<_maxBuff; ++i )
    elems.push_back(elem);
  if( elem == 2048*2048 ) { // flamingos1or2
    elems[1] = 1024*1024; elems[2] = 512*512;
  }
  for( int i = 0; i<_maxBuff; ++i ) {
    const int* imgdata = (const int*) new int[elems[i]];
    memset((int*)imgdata, 0, sizeof(int)*elems[i]); // clear buff
    bufname = names[i];
    (*_nameIdxFrmBuff)[bufname] = i;
    (*_dataFrmBuff)[i] = new UFInts(bufname, imgdata, elems[i]); // shallow ctor
    clog<<"UFEdtDMAgent::_allocNamedBuffs> i, elem, name: "<<(*_nameIdxFrmBuff)[bufname]<<", "<<elems[i]<<", "<<bufname<<endl;
  }
  return _maxBuff;  
}

int UFEdtDMAgent::_buffNames(const string& instrum, vector< string >& names) {
  names.clear();
  if( instrum.find("flam") != string::npos || 
      instrum.find("Flam") != string::npos || 
      instrum.find("FLAM")!= string::npos  ) {
    names.push_back(instrum+":FullImage");
    names.push_back(instrum+":1024Image");
    names.push_back(instrum+":512Image");
    return (int) names.size();
  }

  // if not flamingos, must be trecs or canaricam
  // according to varosi's FrameProcessThread.cc:
  names.push_back(instrum+":src1");        //0 == UFObsConfig::buffId(FrameCount) - 1
  names.push_back(instrum+":ref1");        //1 == UFObsConfig::buffId(FrameCount) - 1
  names.push_back(instrum+":ref2");        //2 == UFObsConfig::buffId(FrameCount) - 1
  names.push_back(instrum+":src2");        //3 == UFObsConfig::buffId(FrameCount) - 1
  names.push_back(instrum+":accum(src1)"); //4
  names.push_back(instrum+":accum(ref1)"); //5
  names.push_back(instrum+":accum(src2)"); //6
  names.push_back(instrum+":accum(ref2)"); //7
  names.push_back(instrum+":dif1");        //8
  names.push_back(instrum+":dif2");        //9
  names.push_back(instrum+":accum(dif1)"); //10
  names.push_back(instrum+":accum(dif2)"); //11
  names.push_back(instrum+":sig");         //12
  names.push_back(instrum+":accum(sig)");  //13

  return (int) names.size();
}

int UFEdtDMAgent::_replFrm(int* acqbuf) {
  // replication service, always send frame config followed by frame
  int w, h, elem = UFEdtDMA::dimensions(w, h); 
  int cnt = UFEdtDMA::takeCnt(), dcnt = UFEdtDMA::doneCnt(), tcnt = UFEdtDMA::takeTotCnt();
  if( UFDeviceAgent::_verbose )
    clog<<"UFEdtDMAgent::_replFrm> w= "<<w<<", cnt= "<<cnt<<", h= "<<h<<", dcnt= "<<dcnt<<", tcnt= "<<tcnt
	<<", _replicate: "<<(int)_replicate<<endl;

  string t = currentTime();
  if( !_replicate->validConnection() ) {
    clog<<"UFEdtDMAgent::_replFrm> replication socket is stale..."<<endl;
    //_replicate->close(); // assume signal handler already closed it.
    //delete _replicate;
    _replicate = 0;
    return 0;
  }
  // this will presumably be freed by the sendThread
  std::vector < UFProtocol* >* ufv = new (nothrow) std::vector < UFProtocol* >;
  if( ufv == 0 ) {
    clog<<"UFEdtDMAgent::_replFrm> replication sendThread protocol vec. cannot be allocated."<<endl;
    return 0;
  }
  UFInts* acqfrm = new UFInts("Raw Edt Acq Buf", (int*) acqbuf, elem); // deep ctor
  if( acqfrm == 0 ) {
    if( UFDeviceAgent::_verbose )
      clog<<"UFEdtDMAgent::_replFrm> replication sendThread protocol Image. cannot be allocated."<<endl;
    return 0;
  }
  acqfrm->setSeq(_postcnt, _totalcnt);

  /*
  UFFrameConfig* frmconf = new UFFrameConfig(w, h);
  frmconf->setDMACnt(dcnt);
  frmconf->setImageCnt(cnt);
  frmconf->setFrameObsSeqNo(cnt);
  frmconf->setFrameObsSeqTot(tcnt);
  if( frmconf == 0 ) {
    clog<<"UFEdtDMAgent::_replFrm> replication sendThread protocol FrameConf cannot be allocated."<<endl;
    return 0;
  }
  ufv->push_back(frmconf);
  clog<<"UFEdtDMAgent::_replFrm> "<<t<<" send FrameConfig and ImgFrame to replication client"<<endl;
  */

  ufv->push_back(acqfrm);
  if( UFDeviceAgent::_verbose )
    clog<<"UFEdtDMAgent::_replFrm> "<<t<<" send ImgFrame to replication client"<<endl;
  pthread_t sthrd = _replicate->sendThread(ufv); // will free ufv and its contents
  if( UFDeviceAgent::_verbose )
    clog<<"UFEdtDMAgent::_replFrm> "<<t<<" spawned thread to send FrameImage: "<<sthrd<<endl;
  return 1;
}

int UFEdtDMAgent::_histFrm(int* acqbuf) {
  int w, h, elem = UFEdtDMA::dimensions(w, h); 
  int cnt = UFEdtDMA::takeCnt(), dcnt = UFEdtDMA::doneCnt(), tcnt = UFEdtDMA::takeTotCnt();
  if( UFDeviceAgent::_verbose )
    clog<<"UFEdtDMAgent::_histFrm> w= "<<w<<", h= "<<h
	<<", cnt= "<<cnt<<", dcnt= "<<dcnt<<", tcnt= "<<tcnt<<endl;

  // Flamingos frames go into 3 buffers:
  if( (elem >= 2048*2048 ) ) {
    if( UFDeviceAgent::_verbose )
      clog<<"UFEdtDMAgent::_histFrm> Flamingos..."<<endl;
    // lock the mutex and copy image frame:
    int m = ::pthread_mutex_trylock(&_theFrmMutex); // should fail if not owned by this thread
    if( m != 0 ) { // failed to get the lock
      clog<<"UFEdtDMAgent::_histFrm> unable to access frame buffer, mutex still locked..."<<endl;
      return 0;
    }
    if( UFDeviceAgent::_verbose )
      clog<<"UFEdtDMAgent::_histFrm> mutex locked ..."<<endl;

    UFInts* ufi = (*_dataFrmBuff)[0]; // full image
    int elem = ufi->numVals();
    int* img0 = (int*) ufi->valData();

    ufi = (*_dataFrmBuff)[1]; // 1/4 image
    int elem4 =  ufi->numVals();
    int* img1024 = (int*) ufi->valData();

    ufi = (*_dataFrmBuff)[2]; // 1/16 image
    int elem16 =  ufi->numVals();
    int* img512 = (int*) ufi->valData();
    if( UFDeviceAgent::_verbose )
      clog<<"UFEdtDMAgent::_histFrm> all pixcnt= "<<elem<<", 1/4= "<<elem4<<", 1/16= "<<elem16<<endl;

    // replace old (full) image with new:
    //for( int i = 0; i < elem; ++i )
    //  img0[i] = acqbuf[i];
    memcpy((char*)img0, (char*)acqbuf, elem*sizeof(int));

    if( UFDeviceAgent::_verbose )
      clog<<"UFEdtDMAgent::_histFrm> eval 2x2 avg. (1024x1024 pixcnt)..."<<endl;
    // 2x2 avg:
    int w2 = w/2, h2 = h/2;
    //double sum1024= 0.0;
    int i, j, k, kk0= 0, kk1= 0;
    for( i= 0, k= 0; i < h2-1; ++i ) {
      for( j = 0; j < w2-1; ++j, ++k ) {
        kk0 = i*w + 2*j; kk1 = (i+1)*w + 2*j;
	img1024[k] = (img0[kk0] + img0[kk0 + 1] + img0[kk1] + img0[kk1 + 1]) / 4;
      }
      kk0 = i*w + 2*j; kk1 = (i+1)*w + 2*j;
      img1024[k++] = (img0[kk0] + img0[kk1]) / 2;
    }
    if( UFDeviceAgent::_verbose )
      clog<<"UFEdtDMAgent::_histFrm> eval 2x2 avg."<<endl;

    // 4x4 avg:        
    if( UFDeviceAgent::_verbose )
      clog<<"UFEdtDMAgent::_histFrm> eval 4x4 avg. (512x512 pixcnt)..."<<endl;
    int w4 = w2/2, h4 = h2/2;
    //double sum512= 0.0;
    for( i= 0, k= 0; i < h4-1; ++i ) {
      for( j = 0; j < w4-1; ++j, ++k ) {
        int kk0 = i*w2 + 2*j, kk1 = (i+1)*w2 + 2*j;
	img512[k] = (img1024[kk0] + img1024[kk0 + 1] + img1024[kk1] + img1024[kk1 + 1]) / 4;
      }
      kk0 = i*w + 2*j; kk1 = (i+1)*w + 2*j;
      img512[k++] = (img1024[kk0] + img1024[kk1]) / 2;
    }

    if( UFDeviceAgent::_verbose )
      clog<<"UFEdtDMAgent::_histFrm> eval 4x4 avg."<<endl;

    m = ::pthread_mutex_unlock(&_theFrmMutex);
    if( UFDeviceAgent::_verbose )
      clog<<"UFEdtDMAgent::_histFrm> mutex unlocked, copied new frame elem: "<<elem<<" ..."<<endl;

    return 3;
  }

  // trecs-canaricam uses 14 buffers
  if( UFDeviceAgent::_verbose )
    clog<<"UFEdtDMAgent::_histFrm> T-ReCS or CanariCam."<<endl;
  // trecs frames go into 14 buffers:
  int buffIdx = _postcnt % 4; // default
  if( _theObsConf != 0 )
    buffIdx = _theObsConf->buffId(_postcnt) - 1;
  UFInts* ufi = (*_dataFrmBuff)[buffIdx]; // src1 or ref1 or src2 or ref2
  elem = ufi->elements();
  int* img = (int*) ufi->valData();

  // lock the mutex and copy image frame:
  int m = ::pthread_mutex_trylock(&_theFrmMutex); // fails if not owned by this thread
  if( m != 0 ) { // failed to get the lock
    clog<<"UFEdtDMAgent::_histFrm> unable to access frame buffer, mutex still locked..."<<endl;
    return 0;
  }
  if( UFDeviceAgent::_verbose )
    clog<<"UFEdtDMAgent::_histFrm> write frame buffer, mutex locked, copy new frame elem: "<<elem<<" ..."<<endl;
  // replace old (full) image with new:
  for( int i = 0; i < elem; ++i )
    img[i] = acqbuf[i];

  // evaluate all appropriate differences and accumilations
  int nbuf = _evalAccumDiffs(_postcnt-1, buffIdx); 

  m = ::pthread_mutex_unlock(&_theFrmMutex);
  if( UFDeviceAgent::_verbose ) 
    clog<<"UFEdtDMAgent::_histFrm> mutex unlocked."<<endl;

  return nbuf; // number of buffers modified
}

int UFEdtDMAgent::_evalAccumDiffs(int frmIdx, int buffIdx) { 
  if( _theObsConf == 0 ) // no observation configuration
    return 0;

  UFInts *src1= (*_dataFrmBuff)[0], *src1accum= (*_dataFrmBuff)[4],
         *ref1= (*_dataFrmBuff)[1], *ref1accum= (*_dataFrmBuff)[5],
         *dif1= (*_dataFrmBuff)[8], *dif1accum=(*_dataFrmBuff)[10],
         *src2= (*_dataFrmBuff)[2], *src2accum= (*_dataFrmBuff)[6],
         *ref2= (*_dataFrmBuff)[3], *ref2accum= (*_dataFrmBuff)[7],
         *dif2= (*_dataFrmBuff)[9], *dif2accum=(*_dataFrmBuff)[11],
         *sig= (*_dataFrmBuff)[12], *sigaccum= (*_dataFrmBuff)[13];

  int elem = sig->elements();
  int nb= 1;

  switch( buffIdx ) {
  case 0: { // src1
    clog<<"UFEdtDMAgent::_evalAccumDiffs> src1: "<<endl;
    int* s1 = (int*) src1->valData();
    int* s1acc = (int*) src1accum->valData();
    for( int i = 0; i < elem; ++i )
      s1acc[i] = s1acc[i] + s1[i];
    break;
  }
  case 1: { // ref1
    clog<<"UFEdtDMAgent::_evalAccumDiffs> ref1: "<<endl;
    int* r1 = (int*) ref1->valData();
    int* r1acc = (int*) ref1accum->valData();
    for( int i = 0; i < elem; ++i )
      r1acc[i] = r1acc[i] + r1[i];

    if( _theObsConf->doDiff1(frmIdx) ) {
      clog<<"UFEdtDMAgent::_evalAccumDiffs> dif1: "<<endl;
      ++nb;
      int* d1 = (int*) dif1->valData();
      int* s1 = (int*) src1->valData();
      for( int i = 0; i < elem; ++i )
        d1[i] = s1[i] - r1[i];
    }
    if( _theObsConf->doAccumDiff1(frmIdx) ) {
      clog<<"UFEdtDMAgent::_evalAccumDiffs> dif1accum: "<<endl;
      ++nb;
      int* d1 = (int*) dif1->valData();
      int* d1acc = (int*) dif1accum->valData();
      for( int i = 0; i < elem; ++i )
        d1acc[i] = d1acc[i] + d1[i];
    }
    break;
  }
  case 2: {// src2
    clog<<"UFEdtDMAgent::_evalAccumDiffs> src2: "<<endl;
    int* s2 = (int*) src2->valData();
    int* s2acc = (int*) src2accum->valData();
    for( int i = 0; i < elem; ++i )
      s2acc[i] = s2acc[i] + s2[i];
    break;
  }
  case 3: { // ref2
    clog<<"UFEdtDMAgent::_evalAccumDiffs> ref2: "<<endl;
    int* r2 = (int*) ref2->valData();
    int* r2acc = (int*) ref2accum->valData();
    for( int i = 0; i < elem; ++i )
      r2acc[i] = r2acc[i] + r2[i];

    if( _theObsConf->doDiff2(frmIdx) ) {
      clog<<"UFEdtDMAgent::_evalAccumDiffs> dif2: "<<endl;
      ++nb;
      int* d2 = (int*) dif2->valData();
      int* s2 = (int*) src2->valData();
      for( int i = 0; i < elem; ++i )
        d2[i] = s2[i] - r2[i];
    }
    if( _theObsConf->doAccumDiff2(frmIdx) ) {
      clog<<"UFEdtDMAgent::_evalAccumDiffs> dif2accum: "<<endl;
      ++nb;
      int* d2 = (int*) dif2->valData();
      int* d2acc = (int*) dif2accum->valData();
      for( int i = 0; i < elem; ++i )
        d2acc[i] = d2acc[i] + d2[i];
    }
  }
  default:
    if( _theObsConf->doSig(frmIdx) ) { // just what is the propper evaluation of sig?
      clog<<"UFEdtDMAgent::_evalAccumDiffs> sig & sigaccum: "<<endl;
      nb += 2;
      int* d1 = (int*) dif1->valData();
      int* d2 = (int*) dif2->valData();
      int* s = (int*) sig->valData();
      int* sacc = (int*) sigaccum->valData();
      for( int i = 0; i < elem; ++i ) {
        s[i] = d1[i] + d2[i];
	sacc[i] = sacc[i] + s[i];
      }
    }
    break;
  }

  return nb;
}

// simulation mode
void UFEdtDMAgent::_simacq(unsigned char* acqbuf, char* fitsbuf, int sz, const char* filenm) {
  //++_postcnt;
  int elem = 320*240; // TReCS/CanariCam
  if( sz >= 2048*2048*(int)sizeof(int) )
    elem = 2048*2048; // Flamingos1/2
  else if( sz >= 1024*1024*(int)sizeof(int) )
    elem = 1024*1024; // test?
  else if( sz >= 512*512*(int)sizeof(int) )
    elem = 512*512; // test?

  int* simbuf = (int*) acqbuf;
  for( int i = 0; i < elem; ++i ) {
    simbuf[i] = i + _postcnt;
  }
  return;
}

void UFEdtDMAgent::_writeImg(char* fits, int sz, const char* filenm, bool png, bool jpeg) {
  if( _scale == 0 ) _scale = 1; // compiler/thread bug?
  UFImgHandler::_scale = _scale;
  UFImgHandler::writePngJpeg(fits, sz, filenm, png, jpeg);
}

#endif // UFEdtDMAgent

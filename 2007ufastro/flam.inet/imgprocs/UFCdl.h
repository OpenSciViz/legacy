#if !defined(__UFCdl_h__)
#define __UFCdl_h__ "$Name:  $ $Id: UFCdl.h,v 0.0 2002/06/03 17:42:21 hon beta $"
#define __UFCdl_H__(arg) const char arg##Cdl_h__rcsId[] = __UFCdl_h__;

#include "stdio.h"
#include "unistd.h"
#include "stdlib.h"
#include "sys/types.h"
#include "iostream"

// IRAF Client Display Library (Cdl & ImTool)
#include "cdl.h"

class UFCdl {
public:
  inline UFCdl() {}
  inline ~UFCdl() { close(); }
  //static int main(int argc, char** argv);
  static int open();
  static int close();
  // Flamingos default 
  static int display(unsigned int* img, int w= 2048, int h= 2048); 

protected:
  static CDLPtr _cdl;
};

#endif // __UFCdl_h__

#if !defined(__UFGD_h__)
#define __UFGD_h__ "$Name:  $ $Id: UFgd.h,v 0.1 2003/03/26 18:11:02 hon beta $"
#define __UFGD_H__(arg) const char arg##GD_h__rcsId[] = __UFGD_h__;

#include "gd.h" // the GD library header

// STL template classes:
#include "vector" 
#include "string" 

using namespace std;

class UFgd {
public:
  typedef struct { int r; int g; int b; } Lut8bit; 
  typedef vector< Lut8bit > Lut; 
  UFgd(int w= 320, int h= 240);
  virtual ~UFgd();

  // simple unit test main
  static int main(int argc, char** argv);

  int setDim(int w, int h);
  int getDim(int& w, int& h);

  // simple grayscale lut is default:
  int grayLut();

  // for use by "endian-conversions"
  int reorder(int val);
  // Little-endian data:
  int apply8bitLutL(int *data, int w, int h);
  // Big-endian data:
  int apply8bitLutB(int *data, int w, int h);

  int writePng(const string& filenm);
  int writeJpeg(const string& filenm);

  // the following are not yet implemented:
  int readLut(const string& lutfilenm);
  int writeLut(const string& lutfilenm);
  int getLut(Lut& lut);
  int resetLut(const Lut& lut);

protected:
  int _w, _h;
  gdImagePtr _im;
  vector< Lut8bit > _lut;
};

#endif // __UFGD_h__

#if !defined(__uffrmgrab_cc__)
#define __uffrmgrab_cc__ "$Name:  $ $Id: uffrmgrab.cc,v 0.1 2000/02/18 20:50:29 hon Rel
ease $"
const char rcsId[] = __uffrmgrab_cc__;

#include "UFFlm1FrameGrab.h"
__UFFlm1FrameGrab_H__(uffrmgrab_cc);

int main(int argc, char** argv) {
  // just call UFFlm1FrameGrab main:
  return UFFlm1FrameGrab::main(argc, argv);
}

#endif // __uffrmgrab_cc__

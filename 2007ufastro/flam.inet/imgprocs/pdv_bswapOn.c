void bswapOn(EdtDev *edt_p) {
  if( edt_p == 0 ) {
    clog<<"bswapOn> unitialized device pointer..."<<endl;
    return;
  }
  Dependent *dd_p = edt_p->dd_p;
  dd_p->byteswap = 1;
  // set Byteswap/ Hwpad 
  int padword = dd_p->hwpad << 1;
  padword |= PDV_BSWAP;
  edt_msg(DEBUG2, "PAD_SWAP %x\n", padword);
  dep_wait(edt_p);
  edt_reg_write(edt_p, PDV_BYTESWAP, padword);
}

#if !defined(__ufpngtake_cc__)
#define __ufpngtake_cc__ "$Name:  $ $Id: ufpngtake.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __ufpngtake_cc__;

#include "UFRuntime.h"
#include "UFEdtDMA.h"
#include "UFgd.h"
#include "sys/stat.h"

static int _cnt= 0;

int fitsInt(const string& key, char* fitsHdr) {
  char entry[81]; memset(entry, 0, sizeof(entry));
  strncpy(entry, fitsHdr, 80); fitsHdr += 80;
  string s = entry;
  int pos = s.find(key);
  if( pos == (int)string::npos )
    return -1;
  pos = 1+s.find("=", pos);
  if( pos == (int)string::npos )
    return -1;
  s = s.substr(1+pos);  
  char* c = (char*)s.c_str();
  while( *c == ' ' ) ++c;  // eliminate leading white sp
  s = c; 
  pos = s.find(" ");
  if( pos == (int)string::npos )
    pos = s.find("/");
  if( pos !=  (int)string::npos ) {
    char cv[1+pos]; memset(cv, 0, 1+pos); strncpy(cv, s.c_str(), pos);
    return atoi(cv);
  }
  return -1;
}

void writePng(char* fits, int sz) {
  int* data= 0;
  char* ep = fits;
  char end[4]; memset(end, 0, sizeof(end));
  char naxis[7]; memset(naxis, 0, sizeof(naxis));
  int w= 0, h= 0;
  strcpy(end, "END");
  int ne = strlen(end);
  int na = strlen("NAXIS0");
  string s, key;
  do {
    memcpy(end, ep, ne);
    s = end;
    memcpy(naxis, ep, na);
    key = naxis;
    if( key == "NAXIS1" )
      w = fitsInt(key, ep);
    if( key == "NAXIS2" )
      h = fitsInt(key, ep);
    ep += 80;
  } while( s != "END" );
  data = (int*)(ep + 80); // fits data is Big-Endian:
  if( w <= 0 || h <= 0 )
    return;
  clog<<"writing output files for img w= "<<w<<", h= "<<h<<endl;
  UFgd gd(w, h);
  gd.applyLutB(data, w, h);
  strstream sp;
  sp << "quicklook_"<<_cnt<<".png"<<ends;
  gd.writePng(sp.str()); delete sp.str();
  //strstream sj;
  //sj << "quicklook_"<<_cnt<<".jpg"<<ends;
  //gd.writeJpeg(sj.str()); delete sj.str();
  ++_cnt;
}

int main(int argc, char** argv) {
  int cnt= 1, index= 0, fits_sz= 0;
  char *filehint= 0, *fits= 0;
  int* lut= 0;
  UFEdtDMA::init(argc, argv, cnt, lut, index, filehint, fits, fits_sz);
  UFEdtDMA::setDpySeq(1); // call display function modula 1
  // open connetion to Png and enter frame take loop:
  cnt = UFEdtDMA::takeAndStore(cnt, index, filehint, lut, fits, fits_sz, writePng);
  clog<<"ufpngtake> took/grabbed frames: "<<cnt<<endl;
  return 0;
}

#endif // __ufpngtake_cc__

#if !defined(__UFDaemon_cc__)
#define __UFGD_cc__ "$Name:  $ $Id: UFgd.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFGD_cc__;

#include "UFgd.h"
__UFGD_H__(__UFGD_cc);

#include "stdio.h"
#include "iostream.h"

#include "UFRuntime.h"

UFgd::UFgd(int w, int h) :  _w(w), _h(h), _im(0), _lut() {
  // allocate the image & set lut
  setDim(w, h);
  // default to gray lut
  grayLut();
}

UFgd::~UFgd() {
  _lut.clear();
  for( int i = 0; i <= 255; ++i )
    ::gdImageColorDeallocate(_im, i);
  
  // destroy the image in memory.
  ::gdImageDestroy(_im); _im = 0;
}

int UFgd::setDim(int w, int h) {
  _w = w;
  _h = h;
  // destroy & re=allocate image in memory.
  if( _im ) 
    ::gdImageDestroy(_im);
  _im = ::gdImageCreate(w, h);
  return _w*_h;
}

int UFgd::getDim(int& w, int& h) {
  w = _w;
  h = _h;
  return _w*_h;
}

int UFgd::grayLut() {
  Lut8bit gray; 
  for( int i = 0; i <= 255; ++i ) {
    // assuming that gdImageColorAllocate returns indices 0-255 sequentially:
    gray.r = gray.g = gray.b = i;
    int color = ::gdImageColorAllocate(_im, gray.r, gray.g, gray.b);
    if( color < 0 )
      clog<<"UFgd::grayLut> colorAllocation failed: "<<color<<endl;
    _lut.push_back(gray);
    //clog<<"UFgd::grayLut> color: "<< color << ", lutrgb: "
    //<< _lut[color].r << " " << _lut[color].g << " " << _lut[color].b << endl;
  }
  return (int)_lut.size();
}

int UFgd::reorder(int val) {
  char* tmp = (char*) &val;
  char b = tmp[0];
  tmp[0] = tmp[3]; tmp[3] = b;  b = tmp[1]; tmp[1] = tmp[2]; tmp[2] = b;
  int* r = (int*)tmp;

  return *r;
}
  
// little-endian data
int UFgd::apply8bitLutL(int *data, int w, int h) {
  // truncate to fit, if necessary:
  if( w > _w ) w = _w;
  if( h > _h ) h = _h;
  int elem = w*h;
  double min, max;

  if( !UFRuntime::littleEndian() ) { // big-endian machine  
    min = max = reorder(data[0]);
    for(int i= 1; i < elem; ++i ) {
      int tmp = reorder(data[i]);
      if( tmp > max ) max = tmp;
      if( tmp < min ) min = tmp;
    }
    double diff, spread = max - min;
    int i= 0;
    for( int y= 0; y < h; ++y ) {
      for( int x= 0; x < w; ++x ) {
        diff = reorder(data[i++]) - min;
        int color = (int)floor(255 * diff / spread); 
        _im->pixels[y][x] = color;
      }
    }
    return elem;
  }

  // little-endian host
  min = max = data[0];
  for( int i= 1; i < elem; ++i ) {
    if( data[i] > max ) max = data[i];
    if( data[i] < min ) min = data[i];
  }
  double diff, spread = max - min;
  int i= 0;
  for( int y= 0; y < h; ++y ) {
    for( int x= 0; x < w; ++x ) {
      diff = data[i++] - min;
      int color = (int)floor(255 * diff / spread); 
      _im->pixels[y][x] = color;
    }
  }
  return elem;
}

// data is big-endian
int UFgd::apply8bitLutB(int *data, int w, int h) {
  // big-endian data
  // truncate to fit, if necessary:
  if( w > _w ) w = _w;
  if( h > _h ) h = _h;
  int elem = w*h;
  double min, max;
  if( UFRuntime::littleEndian() ) { // little-endian machine  
    min = max = reorder(data[0]);
    for(int i= 0; i < elem; ++i ) {
      int tmp = reorder(data[i]);
      if( tmp > max ) max = tmp;
      if( tmp < min ) min = tmp;
    }
    double diff= 0.0, spread = max - min;
    //clog<<"UFgd::apply8bitB> min, max, spread: "<< min <<", "<< max<<", "<<spread<<endl;
    int i= 0;
    for(int y= 0; y < h; ++y ) {
      for(int x= 0; x < w; ++x, ++i ) {
	diff = reorder(data[i]) - min;
        int color = (int)floor(255 * diff / spread); 
        _im->pixels[y][x] = color;
      }
    }
    return elem;
  }

  // big-endian host  
  min = max = data[0];
  for(int i= 1; i < elem; ++i ) {
    if( data[i] > max ) max = data[i];
    if( data[i] < min ) min = data[i];
  }
  double diff= 0.0, spread = max - min;
  //clog<<"UFgd::apply8bitB> min, max, spread: "<< min <<", "<< max<<", "<<spread<<endl;
  int i= 0;
  for(int y= 0; y < h; ++y ) {
    for(int x= 0; x < w; ++x, ++i ) {
      diff = data[i] - min;
      int color = (int)floor(255 * diff / spread); 
      _im->pixels[y][x] = color;
    }
  }
  return elem;
}

int UFgd::writePng(const string& filenm) {  
  FILE *pngout = 0;
  if( filenm == "stdout" ) {
    pngout = stdout;
    ::gdImagePng(_im, pngout);
    return 0;
  }
  pngout = ::fopen(filenm.c_str(), "w");
  ::gdImagePng(_im, pngout);
  return ::fclose(pngout);
}

int UFgd::writeJpeg(const string& filenm) {
  FILE *jpegout = 0;
  if( filenm == "stdout" ) {
    jpegout = stdout;
    ::gdImageJpeg(_im, jpegout, -1);
    return 0;
  }
  jpegout = ::fopen(filenm.c_str(), "w");
  ::gdImageJpeg(_im, jpegout, -1);
  return ::fclose(jpegout);
}

int UFgd::main(int argc, char** argv) {
  // test  trecs
  int w= 320, h= 240;
  const int ttot= w*h;
  int trecs[ttot];
  for( int i = 0; i < ttot; ++i )
    trecs[i] = i;
  
  UFgd tgd(w, h);
  if( UFRuntime::littleEndian() )
    tgd.apply8bitLutL(trecs, w, h);
  else
    tgd.apply8bitLutB(trecs, w, h);

  tgd.writeJpeg("trecs.jpg");
  tgd.writePng("trecs.png");

  // test flamingos
  w = h = 2048;
  const int ftot= w*h;
  int flmn[ftot];
  for( int i = 0; i < ftot; ++i )
    flmn[i] = i;
  
  UFgd fgd(w, h);
  if( UFRuntime::littleEndian() )
    fgd.apply8bitLutL(flmn, w, h);
  else
    fgd.apply8bitLutB(flmn, w, h);

  fgd.writeJpeg("flmn.jpg");
  fgd.writePng("flmn.png");

  return 0;
}

#endif // __UFGD_cc__

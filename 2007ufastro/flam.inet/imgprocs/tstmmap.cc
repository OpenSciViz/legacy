#include "sys/mman.h"
#include "sys/types.h"
#include "sys/stat.h"
#include "time.h"
#include "fcntl.h"
#include "errno.h"
#include "unistd.h"

#include "cstdio"
#include "iostream"
#include "fstream"
#include "strstream"
#include "string"

using namespace std ;

struct MMapFile { int fd; int size; string name; unsigned char* buf; };

int main(int argc, char** argv) {
  string filenm = "./tstmmap.dat";
  MMapFile mf;
  mf.size = 2048*2048*sizeof(int);
  mode_t m = S_IRUSR | S_IRGRP | S_IROTH | S_IWUSR | S_IWGRP | S_IWOTH;

  mf.fd = ::open(filenm.c_str(), O_RDWR|O_CREAT|O_TRUNC, m);
  if( mf.fd < 0 ) {
    clog<<"unable to open "<<filenm<<" "<<strerror(errno)<<endl;
    return mf.fd;
  }

  int stat = fchmod(mf.fd, m);
  if( stat < 0 ) {
    clog<<"unable to chmod"<<filenm<<" "<<strerror(errno)<<endl;
    ::close(mf.fd);
    return mf.fd;
  }
  
  stat = ::ftruncate(mf.fd, mf.size);
  if( stat < 0 ) {
    clog<<"unable to ftruncate"<<filenm<<" "<<strerror(errno)<<endl;
    ::close(mf.fd);
    return mf.fd;
  }

  mf.buf = (unsigned char*) ::mmap(0, mf.size, PROT_READ|PROT_WRITE, MAP_SHARED, mf.fd, 0);
  if( mf.buf == 0 || mf.buf == MAP_FAILED || errno != 0 ) {
    clog<<"UFPosixRuntime::mmapFileOpen> unable to mmap to "<<filenm<<", buf= "<<(int)mf.buf<<endl;
    if( errno != 0 )
      clog<<strerror(errno)<<endl;
    ::close(mf.fd);
    mf.fd = -1;
    return mf.fd;
  }
  else {
    clog<<"UFPosixRuntime::mmapFileOpen>  mmaped to "<<filenm<<" "<<endl;
    if( errno != 0 )
      clog<<strerror(errno)<<endl;
    mf.name = filenm;
  }
  int* frm= new int[2048*2048];
  for( int i = 0; i < 2048*2048; ++i )
    frm[i] = i;
  ::memcpy(mf.buf, frm, mf.size);
  ::close(mf.fd);
  return 0;
}

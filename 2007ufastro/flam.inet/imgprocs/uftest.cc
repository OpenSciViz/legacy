// gcc -o uftest uftest.cc -I/usr/local/iraf/external/include -L/usr/local/iraf/external/lib -lstdc++ -lcdl -lsocket -lnsl -lm
#include "stdio.h"
#include "unistd.h"
#include "stdlib.h"
#include "sys/types.h"
#include "iostream"

// IRAF Client Display Library (Ds9 & ImTool)
#include "cdl.h"

class Ds9 {
public:
  inline Ds9() : _cdl(0) {}
  inline ~Ds9() { close(); }
  static int main(int argc, char** argv);
  int open();
  int close();
  int display(int* img, int nx= 2048, int ny= 2048);

protected:
  CDLPtr _cdl;
};

int Ds9::open() {
  _cdl = ::cdl_open( (char *)getenv("IMTDEV") );
  if( _cdl == 0 ) {
    clog<<"Ds9::open> cdl open failed"<<endl;
    return -1;
  }
  return (int)_cdl;
}

int Ds9::close() {
  if( _cdl != 0 )
    ::cdl_close( _cdl ); 

  _cdl = 0;
  return (int)_cdl;
}

int Ds9::display(int* img, int nx, int ny) {
  int fb_w, fb_h, nf, lx, ly;
  int stat= 0, frame= 1, fbconfig= 0;
  float max = img[0];

  for(int i = 0; i < nx*ny; ++i )
    if( max < img[i] ) max = img[i];

  ::cdl_selectFB( _cdl, nx, ny, &fbconfig, &fb_w, &fb_h, &nf, 1 );
  stat = ::cdl_displayPix( _cdl, img, nx, ny, 8*sizeof(int), 1, fbconfig, 1 ); 
  return stat;
}

int Ds9::main(int argc, char** argv) {
  int stat= 0, nx= 2048, ny= 2048;
  int *img = new int[nx*ny];
  for( int i = 0; i < nx*ny; ++i )
    img[i] = i;

  Ds9 d;
  d.open();
  d.display(img, nx, ny);
  d.close();
}

int main(int argc, char** argv) {
  return Ds9::main(argc, argv);
}

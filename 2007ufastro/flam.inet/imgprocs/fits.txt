Subject: FITS example
Date: Mon, 30 Oct 2000 11:07:14 -0500
From: <elston@astro.ufl.edu>
To: hon@astro.ufl.edu

SIMPLE  =                    T / Fits standard
BITPIX  =                  -32 / Bits per pixel
NAXIS   =                    2 / Number of axes
NAXIS1  =                 2048 / Axis length
NAXIS2  =                 2048 / Axis length
EXTEND  =                    F / File may contain extensions
ORIGIN  = 'UF FLAMINGOS'       / FITS file originator
DATE    = '05/11/00  '         / Date FITS file was generated
UTTIME  = '23:00:00.0'         / UT Time at start of observation
OBJECT  = 'The Object Name'    / Name of the object observed
INSTRUME= 'FLAMINGOS         ' /
COADDS  =                    1 / Number of COADDs in image
LNRS    =                    8 / Number of nondestructive reads
INT_S   =                  10. / Single frame integration time
MODE    = 'stare             ' /
UCODE   = 'Version number    ' / Version of controller code
DECKER  = 'Imaging Hole      ' / Decker wheel position
SLIT    = 'Slit mask position' / Slit wheel position
SLITID  = 'Slit mask ID      ' / Name of Slit mask
FILTER  = 'K                 ' / Filter wheel postion
LYOT    = 'MMT               ' / Lyot wheel position
GRISM   = 'HK                ' / GRISM wheel postion
TELESCOP= 'MMT               ' / Name of Telescope
RA      = '23:17:42.33       ' / Telescope RA at start of exposure
DEC     = '-30:08:48.8       ' / Telescope Dec at start of exposure
EPOCH   =                1950. / Coordinate Epoc
AIRMASS =                   1. / Object Airmass at start of exposure
HA      = '01:00:20.0E       ' / Hour angle at start of exposure
COMMENT = 'none              ' / User comment
OBSERVAT= 'KPNO              ' /
IMAGETYP= 'object            ' / Type of image (object,dark,flat,lamp)

#if !defined(__UFDs9_h__)
#define __UFDs9_h__ "$Name:  $ $Id: UFDs9.h,v 0.3 2003/03/26 18:11:02 hon beta $"
#define __UFDs9_H__(arg) const char arg##Ds9_h__rcsId[] = __UFDs9_h__;

#include "stdio.h"
#include "unistd.h"
#include "stdlib.h"
#include "sys/types.h"

#include "iostream"
#include "string"

using namespace std;

class UFDs9 {
public:
  inline UFDs9() {}
  inline ~UFDs9() { close(); }
  //static int main(int argc, char** argv);
  static int open(int dpy= 2, const char* fitsfilenm= 0);
  static int close();
  static int display(const char* fitsfilenm, int dpy= 2); 
  static int display(char* fitsbuff, int sz, int dpy= 2); 

protected:
  static int _tile;
  static FILE* _xpa;
  static string* _filenm;
};

#endif // __UFDs9_h__

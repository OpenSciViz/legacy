#if !defined(__uffrmstatus_cc__)
#define __uffrmstatus_cc__ "$Name:  $ $Id: uffrmstatus.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __uffrmstatus_cc__;

#include "UFEdtDMA.h"
__UFEdtDMA_H__(uffrmstatus_cc);

#include "UFPosixRuntime.h"
__UFPosixRuntime_H__(uffrmstatus_cc);

#include "math.h"

int main(int argc, char** argv) {
  // call some statics in UFEdtDMA::
  int cnt = 1;
  if( argc > 1 ) {
    if( strcmp(argv[1], "-del" ) == 0 || strcmp(argv[1], "-rm" ) == 0 ) {
#if defined(SOLARIS)
      ::system("/bin/rm -rf /tmp/.SEMD /tmp/.SEML");
#endif
      UFEdtDMA::_semRmAll();	
      return 0;
    }
    if( isdigit(argv[1][0]) || isdigit(argv[1][1]) )
      cnt = abs(atoi(argv[1]));
  }
  bool forever = false;
  if( cnt == 0 )
    forever = true;

  while( true ) { 
    cout<<UFEdtDMA::status()<<endl;
    if( --cnt <= 0 && !forever )
      return 0;
    UFPosixRuntime::sleep(0.5);
  }
  return 0;
}

#endif // __uffrmstatus_cc__

#if !defined(__uftake_cc__)
#define __uftake_cc__ "$Name:  $ $Id: uftake.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __uftake_cc__;

#include "UFEdtDMA.h"

int main(int argc, char** argv) {
  return UFEdtDMA::main(argc, argv);
}

#endif // __uftake_cc__

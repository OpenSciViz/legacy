From - Thu Oct 26 12:12:43 2000
Return-Path: <tom@kofa.as.arizona.edu>
Delivered-To: hon@astro.ufl.edu
Received: from kofa.as.arizona.edu (kofa.as.arizona.edu [128.196.208.20])
	by polaris.astro.ufl.edu (Postfix) with ESMTP id E73B91BE45
	for <hon@astro.ufl.edu>; Wed, 25 Oct 2000 17:49:22 -0400 (EDT)
Received: (from tom@localhost)
	by kofa.as.arizona.edu (8.9.3/8.9.3) id OAA06455
	for hon@astro.ufl.edu; Wed, 25 Oct 2000 14:49:21 -0700 (MST)
Date: Wed, 25 Oct 2000 14:49:21 -0700
From: Tom Trebisky <tom@kofa.as.arizona.edu>
To: david b hon <hon@astro.ufl.edu>
Subject: Re: mmt command strings
Message-ID: <20001025144921.A6087@kofa.as.arizona.edu>
References: <39F76F0E.B1E90EF3@astro.ufl.edu>
Mime-Version: 1.0
Content-Type: text/plain; charset=us-ascii
X-Mailer: Mutt 0.95.3us
In-Reply-To: <39F76F0E.B1E90EF3@astro.ufl.edu>; from david b hon on Wed, Oct 25, 2000 at 04:38:54PM -0700

On Wed, Oct 25, 2000 at 04:38:54PM -0700, david b hon wrote:
> Tom,
>     Any progress on a final or semi-final set of telescope command strings?
> Any new Perl (client) code fragments you might care to share?

The commands are ever changing - but the set is growing, and
new responses appear within existing commands.

This may sound scary, but it isn't bad if you follow the general
approach I will outline quickly here.

Commands which elicit informational responses (such as telpos)
will get a set of pair which consist of a tag and a value
and finally end in an EOF sentinel.  Any code which issues
a telpos (or any other command) should loop thru the lines
it receives, looping thru all tag/value pairs grabbing the
things it wants, ignoring unexpected or undesired responses.

Hopefully all this makes sense.

I have a script "showme" that sends some command and then
shows the response strings.  Right now, for example,
telpos does this:

(/u1/tom) cholla $ showme telpos
az 179.983735
alt 14.999986
rot 0.000000
pa -360.000000
ha -00:00:05.19
lst 15:50:26.65
ra 15:50:30.08
dec -43:20:58.33
(/u1/tom) cholla $ 

Notice the new tags pa and rot.

I do need to generate an updated list of all commands and
examples of the current responses (I could and will just
use showme in a script).

Also, here is the showme script in its current form.

--------------------------------------------------------------------

#!/usr/bin/perl -w

use Tk;
use IO::Socket;
use strict;

if ( @ARGV < 1 ) {
	print "Buzz off!\n";
	exit ( 1 );
}

$_ = shift;

my $port = 5231;
my $host = "mmtvme1";

new_data ( $_ );

# -----------------------------------------------------

sub new_data {
    my $sock = IO::Socket::INET->new (
			PeerAddr => $host,
			PeerPort => $port,
			Proto => 'tcp'
		    );
    my ( $cmd ) = @_;

    die "No socket!! (Reason: $!)\n" unless $sock;

    print $sock "$cmd\n";

    while ( defined($_ = <$sock>) ) { 
	last if /^.EOF/;
	print;
    }

    close ($sock);
}

# THE END

-- 
	Tom Trebisky			MMT Observatory
	ttrebisky@as.arizona.edu	University of Arizona
	http://kofa.as.arizona.edu/	Tucson, Arizona 85721
					(520) 621-5135

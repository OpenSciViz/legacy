#if !defined(__ufqlook_cc__)
#define __ufqlook_cc__ "$Name:  $ $Id: ufqlook.cc 14 2008-06-11 01:49:45Z hon $"

#include "string"
#include "vector"
#include "stdio.h"

#include "UFClientApp.h"
#include "UFProtocol.h"
#include "UFInts.h"
#include "UFFrameConfig.h" // trecs frameconfig is still of general use
#include "UFFlamObsConf.h" // flam obsconfig includes its frameconfig
#include "UFStrings.h"
#include "UFImgHandler.h"

class ufqlook : public UFClientApp {
public:
  inline ufqlook(string& name, int argc,
	         char** argv, char** envp, int port= -1) : UFClientApp(name, argc, argv, envp, port) {}
  inline ~ufqlook() {}
  static int main(string& name, int argc, char** argv, char** envp);
  inline virtual string description() const { return __ufqlook_cc__; }

  // options: -stat -frame [bufname] -conf -obseq -file [fitsfilename] -archvname -ds9 -jpeg -png
  // not -obseq shouild send the entire observation sequence of frames, each frame preceded by a
  // frame conf.; the first preceeded by the obs. conf.
  int parse(vector< string >& req);

  // interactive mode: (no '-')
  int parse(const string& line, vector< string >& req);

  // submit req. bundle, flush output and sleep a bit.
  // recv reply and return reply as string.
  int submit(const vector< string >& req, UFProtocol*& reply, float flush= -1);

  int handleReq(const vector< string >& req);
  int handleFrame(UFProtocol* ufp= 0);

  static bool _stat, _conf, _frmdata, _png, _jpeg, _verbose;
  static int _ds9, _frmcnt, _cnt;
  static float _flush, _sleep;
  static string _imgfile, _fitsfile, _replicant;

};

bool ufqlook::_stat= false, ufqlook::_conf= false,  ufqlook::_frmdata= false;
bool ufqlook::_png= false, ufqlook::_jpeg= false, ufqlook::_verbose= false;
int ufqlook::_ds9= 0, ufqlook::_frmcnt= 0, ufqlook::_cnt= 0;
float ufqlook::_flush= -1, ufqlook::_sleep= 0;
string ufqlook::_imgfile, ufqlook::_fitsfile, ufqlook::_replicant;

int ufqlook::parse(const string& line, vector< string >& req) {
  req.clear();
  size_t offset=0, pos = line.find("stat");
  if( pos != string::npos ) {
    _stat= true;
    req.push_back("-stat"); offset = strlen("-stat"); req.push_back(line.substr(pos+offset));
  }

  pos = line.find("file");
  if( pos != string::npos ) {
    offset += strlen("-file"); _imgfile = _fitsfile = line.substr(pos+offset);
    _fitsfile += ".fits";
  }

  pos = line.find("archvname");
  if( pos != string::npos ) {
    req.push_back("-archvname"); req.push_back("true"); 
  }

  pos = line.find("obseq");
  if( pos != string::npos ) {
    req.push_back("-obseq"); req.push_back("true"); 
  }

  pos = line.find("conf");
  if( pos != string::npos ) {
    _conf = true;
    req.push_back("-conf"); req.push_back("obs"); 
  }

  pos = line.find("frame");
  if( pos != string::npos ) {
    req.push_back("-frame"); offset += strlen("-frame");
    if( line.length() > offset ) {
      string opt = line.substr(pos+offset);
      const char* cs = opt.c_str(); 
      if( cs[0] != '-' && opt.size() >= 1 ) // assume this is a frame buffer name
        req.push_back(line.substr(pos+offset));
      else // most recent frame:
        req.push_back("flam:FullImage");
    }
    else {
      req.push_back("flam:FullImage");
    }
    _frmdata = true;
  }

  pos = line.find("nods9");
  if( pos != string::npos ) {
    _ds9 = 0;
  }

  pos = line.find("ds9");
  if( pos != string::npos ) {
    _ds9 = 1; offset += strlen("-ds9");
    if( line.length() > offset ) {
      string opt = line.substr(pos+offset);
      const char* cs = opt.c_str(); 
      if( cs[0] != '-' && opt.size() >= 1 ) // ds9 frame tile cnt
        _ds9 = atoi((line.substr(pos+offset)).c_str());
    }
  }

  pos = line.find("png");
  if( pos != string::npos ) {
    _png = true; offset += strlen("-png");
    if( line.length() > offset ) {
      string opt = line.substr(pos+offset);
      const char* cs = opt.c_str(); 
      if( cs[0] != '-' && opt.size() >= 1 ) // png file name hint
        _imgfile = line.substr(pos+offset);
    }
  }

  pos = line.find("nopng");
  if( pos != string::npos ) {
    _png = false;
  }

  pos = line.find("jpeg");
  if( pos != string::npos ) {
    _jpeg = true; offset += strlen("-jpeg");
    if( line.length() > offset ) {
      string opt = line.substr(pos+offset);
      const char* cs = opt.c_str(); 
      if( cs[0] != '-' && opt.size() >= 1 ) // jpeg file name hint
        _imgfile = line.substr(pos+offset);
    }
  }

  pos = line.find("nojpeg");
  if( pos != string::npos ) {
    _jpeg = false;
  }

  return req.size()/2;
}

int ufqlook::parse(vector< string >& req) {
  req.clear();
  string name = UFDaemon::name();

  string arg = findArg("-repl");
  if( arg != "false" ) {
    name += "::replication"; // this is used in greeting init/connect to frame server
    rename(name);
    if( arg == "true")
      _replicant = "flam";
    else
      _replicant = arg;
  }
  
  arg = findArg("-replicate");
  if( arg != "false" ) {
    name += "::replication";
    rename(name);
    if( arg == "true")
      _replicant = "flam";
    else
      _replicant = arg;
  }
  
  arg = findArg("-replicant");
  if( arg != "false" ) {
    name += "::replication";
    rename(name);
    if( arg == "true")
      _replicant = "flam";
    else
      _replicant = arg;
  }
  
  arg = findArg("-replication");
  if( arg != "false" ) {
    name += "::replication";
    rename(name);
    if( arg == "true")
      _replicant = "flam";
    else
      _replicant = arg;
  }

  arg = findArg("-q");
  if( arg != "false" )
    _quiet = true;

  arg = findArg("-stat");
  if( arg != "false") {
    _stat = true;
    if(  arg != "true") {
      req.push_back("-stat"); req.push_back(arg);
    }
    else {
      req.push_back("-stat"); req.push_back("edt");
    }
  }

  arg = findArg("-conf");
  if( arg == "true" ) {
    req.push_back("-conf"); req.push_back("obs");
    _conf = true; _cnt = 1;
  }

  arg = findArg("-frame");
  if( arg != "false" ) {
    char* ac = (char*) arg.c_str();
    _frmdata = true; req.push_back("-frame");
    if( arg == "true" ) {
      req.push_back("flam:FullImage");
    }
    else if( isdigit(ac[0]) ) {
      _sleep = atoi(ac);
      req.push_back("flam:FullImage");
    }
    else { // hopefully a proper buffer name
      req.push_back(arg);
    }
  }

  arg = findArg("-archvname");
  if( arg != "false" ) {
    req.push_back("-archvname"); req.push_back("true");
  }

  clock_t t = clock();
  strstream s;
  s<<"ufqlook."<<t<<ends;

  arg = findArg("-file"); // file name hint
  if( arg != "false" && arg != "true" ) {
    _imgfile = _fitsfile = arg;
    _fitsfile += ".fits";
  }
  else if( arg == "true" ) {
    _imgfile = _fitsfile = s.str();
    _fitsfile += ".fits";
  }

  arg = findArg("-ds9");
  if( arg != "false" ) {
    _ds9 = 1;
    if( arg != "true" ) {
      _ds9 = atoi(arg.c_str());
    }
  }

  arg = findArg("-nods9");
  if( arg != "false" ) {
    _ds9 = 0;
  }

  arg = findArg("-png");
  if( arg != "false" ) {
    _png = true;
    if( arg != "true" )
     _imgfile = arg;
    else 
    _imgfile = s.str();
  }

  arg = findArg("-nopng");
  if( arg != "false" ) {
    _png = false;
  }

  arg = findArg("-jpeg");
  if( arg != "false" ) {
    _jpeg = true;
    if( arg != "true" )
     _imgfile = arg;
    else 
    _imgfile = s.str();
  }

  arg = findArg("-nojpeg");
  if( arg != "false" ) {
    _jpeg = false;
  }

  return req.size()/2;
}

int ufqlook::submit(const vector< string >& req, UFProtocol*& r, float flush) {
  for( int i = 0; i < (int)req.size(); ++i )
    clog<<"ufqlook::submit> "<<i<<" "<<req[i]<<endl;
  UFStrings ufs(name(), req);
  int ns = send(ufs);
  if( ns <= 0 )
    return ns;
  // try flushing the socket output stream
  // this is the only way i can think to do it:
  int stat = fflush(_fs);
  if( stat != 0 ) clog<<"ufqlook::submitPar> socket flush failed? ns= "<<ns<<endl;

  if( flush > 0.000001 ) // optionally sleep after the flush
    UFPosixRuntime::sleep(flush);

  r = UFProtocol::createFrom(*this);
  if( r != 0 ) {
    clog<<"ufqlook::submit> reply: "<<r->name()<<endl;
    return r->elements();
  }

  return 0;
}

int ufqlook::main(string& name, int argc, char** argv, char** envp) {
  ufqlook qlook(name, argc, argv, envp);
  string host = hostname();
  int port= 52001;
  string usage = "usage: 'ufqlook [-q] [-flush sec.] [-host host] [-port port] [-stat] [-conf] [-repl[icant]] [-frame [bufname] [-obseq] [-file hint] [-[no]ds9 [1-N]] [-[no]jpeg [imgfilename/stdout]] [-[no]png [imgfilename/stdout]]'";

  string arg = qlook.findArg("-h");
  if( arg != "false" ) {
    clog<<"ufqlook> "<<usage<<endl;
    return 0;
  }

  arg = qlook.findArg("-help");
  if( arg != "false" ) {
    clog<<"ufqlook> "<<usage<<endl;
    return 0;
  }
  
  arg = qlook.findArg("-v");
  if( arg != "false" )
    _verbose = true;
  
  arg = qlook.findArg("-host");
  if( arg != "false" )
    host = arg;

  arg = qlook.findArg("-port");
  if( arg != "false" && arg != "true" )
    port = atoi(arg.c_str());

  if( port <= 0 || host.empty() ) {
    clog<<"ufqlook> "<<usage<<endl;
    return 0;
  }

  arg = qlook.findArg("-flush");
  if( arg != "false" )
    _flush = atof(arg.c_str());

  arg = qlook.findArg("-cnt");
  if( arg != "false" && arg != "true" )
    _cnt = atoi(arg.c_str()); // if qlook subset of obs, or 0 for forever loop?

   // parse all cmd line opts
  vector< string > req;
  int narg = qlook.parse(req); // parse all other options
  string t0 = UFRuntime::currentTime("local");
  clog<<"ufqlook> connect to ufgedtd@"<<host<<" at t0: "<<t0<<endl;
  // if replication service is desired, handle it here with initial greeting
  FILE* f  = qlook.init(host, port);
  if( f == 0 ) {
    clog<<"ufqlook> unable to connect to agent..."<<endl;
    return -1;
  }
  string tc = UFRuntime::currentTime("local");
  clog<<"ufqlook> connected to ufgedtd@"<<host<<" at tc: "<<tc<<endl;
  // if this is a replication client, no requests are needed
  // enter infinite replication event loop:
  if( _replicant != "" ) {
    clog<<"ufqlook> connected to agent as replicant, cnt: "<<_cnt<<endl;
    int handle= 0;
    while( handle >= 0 ) { // handleFrame should return <= 0 on error or completion of cnt > 0
      handle= qlook.handleFrame();
    }
    return qlook.close();
  }

  if( narg > 0 ) {
    while( _cnt == 0 ) { // forever
      _cnt = qlook.handleReq(req);
      if( _cnt != 0 ) {
        clog<<"ufqlook> cnt: "<<_cnt<<endl;
	_cnt = 0;
	if( !qlook.validConnection() ) {
          clog<<"ufqlook> lost connection? reconnect to agent."<<endl;
	  qlook.init(host, port);
	}
      }
      if( _sleep > 0.01 ) UFPosixRuntime::sleep(_sleep);
    }
    for( int i = 0; i < _cnt; ++i ) { // finite
      qlook.handleReq(req);
      if( _sleep > 0.01 ) UFPosixRuntime::sleep(_sleep);
    }
    return qlook.close();
  }

  // getting here indicates command prompt mode:
  string line;
  while( true ) {
    clog<<"ufqlook> "<<ends;
    _frmdata= false;
    getline(cin, line);
    if( line == "exit" || line == "quit" || line == "q" ) return 0;
    if( line.size() <= 1 )
      continue; // ignore empty line

    narg = qlook.parse(line, req);
    if( narg <= 0 )
      continue;

    qlook.handleReq(req);
  }

  return qlook.close(); 
} // qlook::main

int ufqlook::handleReq(const vector< string >& req) {
  // req. status reply:
  UFProtocol* reply= 0;
  if( _verbose ) {
    size_t nr = req.size();
    for( size_t i = 0; i < nr; ++i )
      clog << "ufqlook> submit reqvec: "<<req[i]<<endl;
  }
  int ns = submit(req, reply, _flush);
  if( ns <= 0 ) {
    clog << "ufqlook> failed to submit(req, reply, flush: "<<req[0]<<"::"<<req[1]<<endl;
    close(); return -1;
  }
  if( reply == 0 ) {
    clog << "ufqlook> no reply"<<endl;
    close(); return -1;
  }
  if( _verbose ) {
    clog<<"ufqlook> typId: "<<reply->typeId()<<" time: "<<reply->timeStamp()<<", name: "<<reply->name()<<endl;
    //UFProtocol::printHeader(reply->attributesPtr());
  }

  if( reply->isNotice() ) {
    clog<<"ufqlook> notice: "<<reply->name()<<endl;
    delete reply; reply= 0;
  }
  else if( reply->isStrings() ) {
    clog<<"ufqlook> Text: "<<reply->name()<<endl;
    UFStrings* s = dynamic_cast< UFStrings* > ( reply );
    for( int i=0; i < reply->elements(); ++i )
      clog<<(*s)[i]<<endl;
    delete s; reply = s = 0;
  }
  else if( reply->isObsConf() ) {
    clog<<"ufqlook> ObsConf: "<<reply->name()<<endl;
    UFFlamObsConf* oc = dynamic_cast< UFFlamObsConf* > (reply);
    if( oc == 0 ) {
      clog<<"ufqlook> cast to FlamObsConf failed..."<<endl;
      close(); return -1;
    }
    clog<<"ufqlook> datalabel: "<<oc->datalabel()<<endl;
    clog<<"ufqlook> exptime: "<<oc->expTime()<<endl;
    clog<<"ufqlook> w: "<<oc->width()<<", h: "<<oc->height()<<endl;
    clog<<"ufqlook> framesPerNod: "<<oc->framesPerNod()<<endl;
    clog<<"ufqlook> nodsPerObs: "<<oc->nodsPerObs()<<endl;
    clog<<"ufqlook> dmacnt: "<<oc->dmaCnt()<<endl;
    clog<<"ufqlook> pixel sort: "<<oc->pixelSort()<<endl;
    delete oc; reply = oc = 0;
  }

  if( _stat ) { // presumably not a data request
    _stat = false;
    return _cnt;
  }
  if( _conf ) { // presumably not a data request
    _conf = false;
    return _cnt;
  }

  // reply is data or precedes data?
  return handleFrame(reply);
} // handleReq

int ufqlook::handleFrame(UFProtocol* ufp) {
  UFFlamObsConf* oc= 0;
  if( ufp == 0 ) {
    //clog<<"ufqlook::handleFrame> waiting for next transaction from edtd t: "<<UFRuntime::currentTime("local")<<", frmcnt: "<<_frmcnt<<", _replicant: "<<_replicant<<endl;
    clog<<"ufqlook::handleFrame> waiting for next transaction from edtd..."<<endl;
    ufp = UFProtocol::createFrom(*this);
    if( ufp == 0 ) {
      clog<<"ufqlook::handleFrame> failed UFProtocol recv ..."<<endl;
      close(); return 0;
    }
    //clog<<"ufqlook::handleFrame> recv'd type: "<<ufp->typeId()<<", "<<ufp->name()<<endl;
  }

  // check for notices or fits headers or text:
  while( !ufp->isData() ) {
    if( ufp->isNotice() || ufp->isStrings() ) {
      clog<<"ufqlook::handleFrame> notice or text/fits: "<< ufp->name()<<endl;
      delete ufp; ufp = 0;
    }
    else if( ufp->isObsConf() ) {
      delete oc; oc = 0;
      clog<<"ufqlook::handleFrame> recv'd f2obsconf type: "<<ufp->typeId()<<", name: "<<ufp->name()<<endl;
      // replication server is expected to precede each observation with obsconf
      if( ufp->typeId() == UFProtocol::_FlamObsConf_) {
        oc = dynamic_cast<UFFlamObsConf*> (ufp);
        if( oc == 0 ) {
          clog<<"ufqlook::handleFrame> failed cast to UFFlamObsConf..."<<endl;
        }
        else {
          string t = currentTime();
          clog<<"ufqlook::handleFrame> UFFlamObsConf: "<<oc->name()<<", expect frame cnt: "<<oc->totImgCnt()<<endl;
          clog<<"ufqlook::handleFrame> t: "<<t<<", frame: "<<oc->timeStamp()<<", "<<oc->name()<<endl;
        }
      }
    }
    clog<<"ufqlook::handleFrame> recv next protocol obj..."<<endl;
    ufp = UFProtocol::createFrom(*this);
    if( ufp == 0 ) {
      clog<<"ufqlook::handleFrame> failed UFProtocol recv ..."<<endl;
      close(); return 0;
    }
  }
  // should be data!
  if( !ufp->isData() ) {
    clog<<"ufqlook::handleFrame> expected data? "<<ufp->name()<<endl;
    delete ufp;
    return 0;
  }
  // if we get here expect one or more images
  int imgcnt= 1;
  if( oc ) {
    imgcnt = oc->totImgCnt();
    clog<<"ufqlook::handleFrame> obsconfig indicates imgcnt: "<<imgcnt<<endl;
  }
  else {
    clog<<"ufqlook::handleFrame> No obsconfig? imgcnt: "<<imgcnt<<endl;
  }
  vector< UFInts* > imgptr;
  string t = currentTime();
  while( --imgcnt >= 0 ) {
    UFInts* frm = dynamic_cast<UFInts*> (ufp);
    if( frm == 0 ) {
      clog<<"ufqlook::handleFrame> failed to cast (Ints) frame, name: "<<ufp->name()<<endl;
      delete ufp; ufp = 0;
      break;
    }
    else {
      imgptr.push_back(frm);
      int npix = frm->elements();
      clog<<"ufqlook::handleFrame> t: "<<t<<", frame: "<<frm->timeStamp()<<", "<<frm->name()<<", npix: "<<npix<<endl;
      clog<<"ufqlook::handleFrame> pixmin: "<<frm->minVal()<<", pixmax: "<< frm->maxVal()<<endl;
      clog<<"ufqlook::handleFrame> first, last pixels: "<<(*frm)[0]<<", "<<(*frm)[npix - 1]<<endl;
      clog<<"ufqlook::handleFrame> central pixels: "<<(*frm)[npix/2-1]<<", "<<(*frm)[npix/2]<<", "<<(*frm)[npix/2+1]<<endl;
      _frmcnt++;
    }
    if( imgcnt > 0 ) {
      ufp = UFProtocol::createFrom(*this);
      if( ufp == 0 ) {
        clog<<"ufqlook::handleFrame> failed UFProtocol recv ..."<<endl;
        close(); exit(0);
      }
    }
  }
  if( imgptr.size() <= 0 ) {
    clog<<"ufqlook::handleFrame> no UFInts data received?"<<endl;
    delete ufp;
    return 0;
  }

  // use UFImgHandler class to create fits buf from protocol object, optionally save to fitsfile,
  // display on ds9, save as png and/or jpeg:
  UFImgHandler imgh(oc); // oc = 0 is Ok (default ctor)
  int total_sz= 0, imgIdx= (int)imgptr.size()-1;
  // need new signature: UFImgHandler::fitsBuf(const vector< UFInts* >& frmvec, total_sz);
  UFInts* frm = imgptr[imgIdx]; // just look at the final frame for now
  char* fitsbuf = imgh.fitsBuf(frm, total_sz); // allocates with default fits header 

  if( _fitsfile != "" ) {
    string file = _fitsfile + t + ".fits";
    clog<<"ufqlook::handleFrame> writing FITS: "<<file<<endl;
    imgh.writeFits(file, fitsbuf, total_sz);
    if( _ds9 )
      imgh.display(_fitsfile, _ds9);
  }
  else if( _ds9 ) {
    clog<<"ufqlook::handleFrame> pipe to ds9..."<<endl;
    imgh.display(fitsbuf, total_sz, _ds9);
  }
  if( _png || _jpeg ) {
    strstream s;
    s<<_imgfile<<"_"<<_frmcnt<<ends;
    string filehint = s.str(); delete s.str();
    if( _imgfile == "stdout" ) filehint = _imgfile;
    clog<<"ufqlook::handleFrame> writing JPEG/PNG: "<<_imgfile<<endl;
    imgh.writePngJpeg(fitsbuf, total_sz, filehint, _png, _jpeg);
    t = currentTime();
    clog<<"ufqlook::handleFrame> t: "<<t<<", imgfile: "<<frm->timeStamp()<<", "<<filehint<<endl;
  }
  for( size_t i= 0; i < imgptr.size(); ++i )
    delete imgptr[i];
  delete oc; delete fitsbuf;

  if( _cnt > 0 && _frmcnt >= _cnt )
   return -4;

  return _frmcnt;
}   

static void _sigHandler(int signum) {
  switch(signum) {
    case SIGABRT: clog<<"ufqlook> caught signal "<<signum<<" SIGABRT"<<endl; break;
    case SIGALRM: clog<<"ufqlook> caught signal "<<signum<<" SIGALRM"<<endl; break;
    case SIGCHLD: clog<<"ufqlook> caught signal "<<signum<<" SIGCHLD"<<endl; break;
    case SIGCONT: clog<<"ufqlook> caught signal "<<signum<<" SIGCONT"<<endl; break;
    case SIGFPE: clog<<"ufqlook> caught signal "<<signum<<" SIGFPE"<<endl; break;
    case SIGINT: clog<<"ufqlook> caught signal "<<signum<<" SIGINT"<<endl; exit(0); break;
    case SIGPIPE: clog<<"ufqlook> caught signal "<<signum<<" SIGPIPE"<<endl; break;
    case SIGPWR: clog<<"ufqlook> caught signal "<<signum<<" SIGPWR"<<endl; break;
    case SIGTERM: clog<<"ufqlook> caught signal "<<signum<<" SIGTERM"<<endl; exit(0); break;
    case SIGURG: clog<<"ufqlook> caught signal "<<signum<<" SIGURG"<<endl; break;
    case SIGUSR1: clog<<"ufqlook> caught signal "<<signum<<" SIGG"<<endl; break;
    case SIGUSR2: clog<<"ufqlook> caught signal "<<signum<<" SIGUSR2"<<endl; break;
    case SIGVTALRM: clog<<"ufqlook> caught signal "<<signum<<" SIGVTALRM"<<endl; break;
    case SIGXFSZ: clog<<"ufqlook> caught signal "<<signum<<" SIGXFSZ"<<endl; break;
    default: clog<<"ufqlook> caught signal "<<signum<<" SIG???"<<endl; break;
  }
}

int main(int argc, char** argv, char** envp) {
  //UFPosixRuntime::setSignalHandler(_sigHandler); // single threaded
  UFPosixRuntime::sigWaitThread(_sigHandler); // separate signal thread
  string qlname = argv[0];
  return ufqlook::main(qlname, argc, argv, envp);
}

#endif // __ufqlook_cc__

#include "iostream.h"
#include "string"
#include "UFF2EngFITS.h"
#include "UFF2SciFITS.h"
#include "strings.h"

int main(int argc, char** argv) {
  //clog<<"sizeof UFF2EngFITSPrm: "<<sizeof(UFF2EngFITSPrm)<<endl;
  clog<<"strlen UFF2EngFITSPrm: "<<strlen(UFF2EngFITSPrm)<<endl;
  //clog<<"sizeof UFF2EngFITSExt: "<<sizeof(UFF2EngFITSExt)<<endl;
  clog<<"strlen UFF2EngFITSExt: "<<strlen(UFF2EngFITSExt)<<endl;
  //clog<<"sizeof UFF2SciFITSPrm: "<<sizeof(UFF2SciFITSPrm)<<endl;
  clog<<"strlen UFF2SciFITSPrm: "<<strlen(UFF2SciFITSPrm)<<endl;
  //clog<<"sizeof UFF2SciFITSExt: "<<sizeof(UFF2SciFITSExt)<<endl;
  clog<<"strlen UFF2SciFITSExt: "<<strlen(UFF2SciFITSExt)<<endl;

  char card[81]; memset(card, 0, sizeof(card));
  char key[9]= "FITSKEY8";
  char *eq= 0, *comment= 0, *f = (char*) UFF2SciFITSPrm;
  char instrum[]= "Flam";

  bool _writeDD= false;
  if( argc > 1 ) {
    if( argv[1][1] == 'd' &&  argv[1][2] == 'd' )
      _writeDD = true;
    if( argv[1][1] == 's' &&  argv[1][2] == 'p' )
      f = (char*) UFF2SciFITSPrm;
    if( argv[1][1] == 's' &&  argv[1][2] == 'e' )
      f = (char*) UFF2SciFITSExt;
    if( argv[1][1] == 'e' &&  argv[1][2] == 'p' )
      f = (char*) UFF2EngFITSPrm;
    if( argv[1][1] == 'e' &&  argv[1][2] == 'e' )
      f = (char*) UFF2EngFITSExt;
  }

  char *blank = &card[80];
  int n= 1, nc= strlen(f) / 80;
  for( int i= 0; n <= nc ; i += 80, ++n ) {
    for( int c= 0; c < 80; ++c )
      if( *(f+i+c) <= 32 && *(f+i+c) >= 127 ) *(f+i+c) = ' ';
    strncpy(card, f+i, 80); blank = &card[80];
    while( *blank == ' ' ) *blank-- = '\0'; // eliminate trailing blanks
    eq = index(card, '='); comment = index(card, '/');
    if( eq != 0 )
      strncpy(key, card, (size_t)(eq-card));
    if( comment++ != 0 )
      while( *comment == ' ' ) ++comment; // eliminate leading blanks
    else
      comment = "No Comment";
    //string s = card;
    if( _writeDD && eq != 0 )
      cout<<"OK "<<instrum<<"\t"<<key<<"\tINT\t"<<key<<"\tF\tNULL\tNONE\tNULL\t\""<<comment<<"\""<<endl;
    else if( _writeDD ) {
      if( card[0] != ' ' && card[1] != ' ' && card[2] != ' ' && card[3] != ' ' )
        cout<<"Ok "<<card<<endl;
    }
    else if( !_writeDD )
    cout<<n<<" / "<<nc<<": "<<card<<endl;
  }

  return 0;
}

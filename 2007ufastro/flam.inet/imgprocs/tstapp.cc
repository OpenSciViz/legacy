#include "UFRuntime.h"

int main(int argc, char** argv) {
  pid_t tktimer = UFRuntime::execApp("env LD_LIBRARY_PATH=/usr/openwin/lib /usr/openwin/bin/xclock -d -update 1");

  UFPosixRuntime::sleep(2.5);

  return kill(tktimer, SIGTERM);
}

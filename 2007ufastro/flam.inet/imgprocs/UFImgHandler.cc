#if !defined(__UFImgHandler_cc__)
#define __UFImgHandler_cc__ "$Name:  $ $Id: UFImgHandler.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFImgHandler_cc__;

#include "UFImgHandler.h"
__UFImgHandler_H__(UFImgHandler_cc);

#include "stdio.h"
#include "unistd.h"
#include "stdlib.h"
#include "errno.h"
#include "string.h"
#include "sys/types.h"
#include "sys/stat.h"

#include "iostream"
#include "UFRuntime.h"
#include "uffits.h"

#include "UFDs9.h"

int UFImgHandler::_scale = 1;

void UFImgHandler::display(const string& filenm, int dpy) {
  int stat = UFDs9::open(dpy, filenm.c_str()); // does file exist?
  
  if( stat <= 0 ) {
    clog<<"FImgHandler::display> file does not exist: "<<filenm<<endl;
    return;
  }

  UFDs9::display(filenm.c_str(), dpy);
  UFDs9::close();
}

void UFImgHandler::display(char* fits, int sz, int dpy) {
  int stat = UFDs9::open(dpy); // open pipe to xpaset/ds9
  if( stat <= 0 ) {
    clog<<"FImgHandler::display> failed to open pipe to xpaset/ds9"<<endl;
    return;
  }
  UFDs9::display(fits, sz, dpy);
  UFDs9::close();
  return;
}

// this assumes raw image (of 32 bit pixels) has been represented/transacted as unsigned char*
// i.e. NO htonl and ntohl has been performed
char* UFImgHandler::fitsBuf(UFBytes* ufb, int& total_sz, int fits_sz, char* fitsHdr) {
  int sz = sizeof(int) * ufb->numVals();
  int mid_ir = sizeof(int) * 320*240;
  //int near_ir = sizeof(int) * 2048*2048;
  int q1024 = sizeof(int) * 1024*1024;
  int q512 = sizeof(int) * 512*512;

  if( sz <= mid_ir && fitsHdr == 0 ) { // assume mid-infrared imager
    fitsHdr = (char*)fits320x240; // default from uffits.h
    fits_sz = sizeof(fits320x240);
  }
  else if( sz <= q512 && fitsHdr == 0 ) { // assume qlook 512x512
    fitsHdr = (char*)fits512; // default from uffits.h
    fits_sz = sizeof(fits512);
  }
  else if( sz <= q1024 && fitsHdr == 0 ) { // assume qlook 1024x1024
    fitsHdr = (char*)fits1024; // default from uffits.h
    fits_sz = sizeof(fits1024);
  }
  else if( fitsHdr == 0 ) { // assume full size near ir
    fitsHdr = (char*)fits2048; // default from uffits.h
    fits_sz = sizeof(fits2048);
  }

  if( sz <= mid_ir && fits_sz == 0 )  // assume mid-infrared
    fits_sz = sizeof(fits320x240);
  else if( fits_sz == 0 )  // assume near-infrared
    fits_sz = sizeof(fits2048);

  total_sz = sz + fits_sz;

  char* imgfits = new (nothrow) char[total_sz];
  if( imgfits == 0 ) {
    clog<<"UFImgHandler::fitsBuf> unable to allocate image (fits) buffer. "<<endl;
    total_sz = 0;
    return 0;
  }

  // clear the imgbuffer so that timed-out frames don't contain old data?
  ::memset(imgfits, 0, total_sz);

  char* pixfits = imgfits + fits_sz;
  ::memcpy(imgfits, fitsHdr, fits_sz); // prepend fits
  ::memcpy(pixfits, ufb->valData(), sz);

  return imgfits;
}

// this assumes raw image (of 32 bit pixels) has been represented/transacted as int*
// i.e. htonl and ntohl has been performed
char* UFImgHandler::fitsBuf(UFInts* ufi, int& total_sz, int fits_sz, char* fitsHdr) {
  int sz = sizeof(int) * ufi->numVals();
  int mid_ir = sizeof(int) * 320*240;
  //int near_ir = sizeof(int) * 2048*2048;
  int q1024 = sizeof(int) * 1024*1024;
  int q512 = sizeof(int) * 512*512;

  if( sz <= mid_ir && fitsHdr == 0 ) { // assume mid-infrared imager
    fitsHdr = (char*)fits320x240; // default from uffits.h
    fits_sz = sizeof(fits320x240);
  }
  else if( sz <= q512 && fitsHdr == 0 ) { // assume qlook 512x512
    fitsHdr = (char*)fits512; // default from uffits.h
    fits_sz = sizeof(fits512);
  }
  else if( sz <= q1024 && fitsHdr == 0 ) { // assume qlook 1024x1024
    fitsHdr = (char*)fits1024; // default from uffits.h
    fits_sz = sizeof(fits1024);
  }
  else if( fitsHdr == 0 ) { // assume full size near ir
    fitsHdr = (char*)fits2048; // default from uffits.h
    fits_sz = sizeof(fits2048);
  }

  // if fits_sz not set or supplied, someting amiss?
  if( sz <= mid_ir && fits_sz == 0 )  // assum mid-infrared
    fits_sz = sizeof(fits320x240);
  else if( fits_sz == 0 )  // assum near-infrared
    fits_sz = sizeof(fits2048);

  total_sz = sz + fits_sz;
  char* imgfits = new (nothrow) char[total_sz];
  if( imgfits == 0 ) {
    clog<<"UFImgHandler::fitsBuf> unable to allocate image (fits) buffer. "<<endl;
    total_sz = 0;
    return 0;
  }

  // clear the imgbuffer so that timed-out frames don't contain old data?
  ::memset(imgfits, 0, total_sz);

   // test ramp:
  /*
  unsigned int *tst = (unsigned int *) ufi->valData();
  for( int i = 0; i < sz; ++i )
    tst[i] = i;
  */
  char* pixfits = imgfits + fits_sz;
  ::memcpy(imgfits, fitsHdr, fits_sz); // prepend fits
  ::memcpy(pixfits, ufi->valData(), sz);

  return imgfits;
}

int* UFImgHandler::scale(int* data, int ws, int hs, int s) {
  int* sdata = new int[ws * hs]; 
  for( int y = 0; y < hs; ++y ) {
    for( int x = 0; x < ws; ++x ) {
      float pix = 0; 
      for( int i = 0; i < s; ++i ) {
        for( int j = 0; j < s; ++j ) {
	  pix += data[s*y + i + s*x + j];
	}
      }
      sdata[y*hs + x] = (int) pix/s/s;
    }
  }
  return sdata; 
}

int UFImgHandler::fitsInt(const string& key, char* fitsHdr) {
  char entry[81]; memset(entry, 0, sizeof(entry));
  strncpy(entry, fitsHdr, 80); fitsHdr += 80;
  string s = entry;
  int pos = s.find(key);
  if( pos == (int)string::npos )
    return -1;
  pos = 1+s.find("=", pos);
  if( pos == (int)string::npos )
    return -1;
  s = s.substr(1+pos);  
  char* c = (char*)s.c_str();
  while( *c == ' ' ) ++c;  // eliminate leading white sp
  s = c; 
  pos = s.find(" ");
  if( pos == (int)string::npos )
    pos = s.find("/");
  if( pos !=  (int)string::npos ) {
    char cv[1+pos]; memset(cv, 0, 1+pos); strncpy(cv, s.c_str(), pos);
    return atoi(cv);
  }
  return -1;
}

void UFImgHandler::writePngJpeg(char* fits, int sz, const string& filenm, bool png, bool jpeg) {
  int* data= 0;
  int* sdata = data;
  char* ep = fits;
  char end[4]; memset(end, 0, sizeof(end));
  char naxis[7]; memset(naxis, 0, sizeof(naxis));
  int w= 0, h= 0;
  int ws= w/_scale, hs= h/_scale; // reduce _scale 
  strcpy(end, "END");
  int ne = strlen(end);
  int na = strlen("NAXIS0");
  string s, key;
  int fits_sz= 0; // allow 10x2880 (10 headers)?
  do {
    memcpy(end, ep, ne);
    s = end;
    memcpy(naxis, ep, na);
    key = naxis;
    if( key == "NAXIS1" )
      w = fitsInt(key, ep);
    if( key == "NAXIS2" )
      h = fitsInt(key, ep);
    ep += 80;
    fits_sz += 80;
  } while( s != "END" && fits_sz < 28800 );
  if( fits_sz >= 28800 && s != "END" ) {
    clog<<"UFImgHandler::writePngJpeg> sorry, improper or no FITS header?"<<endl;
    return;
  }
  data = (int*)(ep + 80); // fits data is Big-Endian:
  if( w <= 0 || h <= 0 ) {
    clog<<"UFImgHandler::writePngJpeg> sorry FITS header lacks NAXIS1/2"<<endl;
    return;
  }
  ws = w/_scale; hs = h/_scale;
  clog<<"UFImgHandler::writePngJpeg> output img w= "<<ws<<", h= "<<hs<<" ... "<<ends;
  UFgd gd(ws, hs);
  if( _scale == 1 ) {
    gd.apply8bitLutB(data, w, h);
  }
  else {
    sdata = scale(data, ws, hs, _scale);
    gd.apply8bitLutB(data, w, h);
    delete [] sdata;
  }

  string jimfile = filenm;
  string pimfile = filenm;

  if( filenm != "stdout" ) {
    char* cf = (char*)filenm.c_str();
    while( *cf == '.' || *cf == ' ' || *cf == '\t' ) ++cf; // elim. leading whites or dots
    string ss = cf;
    strstream sj, sp;
    size_t fpos = ss.find(".fits");
    if( fpos == string::npos ) {
      if( _scale != 1 ) {
	sp << ss <<"."<< ws <<"x"<<hs<<".png"<<ends;
        sj << ss <<"."<< ws <<"x"<<hs<<".jpg"<<ends;
      }
      else {
	sp << ss <<".png"<<ends;
        sj << ss <<".jpg"<<ends;
      }
    }
    else {
      if( _scale != 1 ) {
        sp << ss.substr(0, fpos) <<"."<< ws <<"x"<<hs<<".png"<<ends;
        sj << ss.substr(0, fpos) <<"."<< ws <<"x"<<hs<<".jpg"<<ends;
      }
      else {
        sp << ss.substr(0, fpos) <<".png"<<ends;
        sj << ss.substr(0, fpos) <<".jpg"<<ends;
      }
    }
    pimfile = sp.str();
    jimfile = sj.str();
  }
  if(jpeg )
    gd.writeJpeg(jimfile);

  if( png )
    gd.writePng(pimfile);

  clog<<" ... all done."<<endl;
  return;
}

#endif // __UFImgHandler_cc__

#if !defined(__UFDs9_cc__)
#define __UFDs9_cc__ "$Name:  $ $Id: UFDs9.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFDs9_cc__;

#include "UFDs9.h"
__UFDs9_H__(UFDs9_cc);

#include "stdio.h"
#include "unistd.h"
#include "stdlib.h"
#include "errno.h"
#include "string.h"
#include "sys/types.h"
#include "sys/stat.h"

#include "iostream"
#include "UFRuntime.h"
#include "UFEdtDMA.h"

int UFDs9::_tile= -1;
FILE* UFDs9::_xpa = 0;
string* UFDs9::_filenm = 0;

// if a filename is provided just xpaset the name, otherwise assume a pipe to xpaset is desired:
int UFDs9::open(int dpy, const char* fitsfilenm) {
  if( dpy > 1 && _tile < dpy ) {
    _tile = dpy;
    ///clog<<":system(\"/usr/local/bin/xpaset -p ds9 tile\")"<<endl;
    ::system("/usr/local/bin/xpaset -p ds9 tile");
  }
  else if( _tile < 0 ) {
    _tile = dpy;
    //clog<<":system(\"/usr/local/bin/xpaset -p ds9 single\")"<<endl;
    ::system("/usr/local/bin/xpaset -p ds9 single");
  }
  int acqcnt = UFEdtDMA::acqCnt();
  strstream sys;
  sys<<"/usr/local/bin/xpaset -p ds9 frame "<<(1 + acqcnt%dpy)<<ends;
  //clog<<sys.str()<<endl;
  ::system(sys.str());
  delete sys.str();                   
  if( fitsfilenm ) {
    delete _filenm; _filenm = 0;
    struct stat statbuf; memset(&statbuf, 0, sizeof(statbuf));
    int cnt= 3;
    while( --cnt >= 0 ) {
      int file_exists = stat(fitsfilenm, &statbuf);
      if( file_exists != 0 ) {
        clog<<"UFDs9::open> stat of "<<fitsfilenm<<" failed: "<<strerror(errno)<<endl;
        if( cnt == 0 ) return -1;
        UFPosixRuntime::sleep(0.1);
      }
      else {
	cnt = 0; // done
      }
    }
    _filenm = new string(fitsfilenm);
    //if( statbuf.st_size < 80 ) // at least one line fits header present?
    clog<<"UFDs9::open> st_size: "<<statbuf.st_size<<endl;
    return statbuf.st_size;
  }
  // assuming pipe
  _xpa = ::popen("/usr/local/bin/xpaset ds9 fits", "w");
  if( _xpa == 0 ) {
    clog<<"UFDs9::open> xpaset open failed"<<strerror(errno)<<endl;
    return 0;
  }
  return fileno(_xpa);
}

int UFDs9::close() {
  if( _xpa != 0 ) {
    ::pclose( _xpa ); 
    _xpa = 0;
  }
  else {
    delete _filenm; _filenm = 0;
  }
  return 0;
}

int UFDs9::display(char* fits, int sz, int dpy) {
  if( open(dpy) <= 0 ) {
    clog<<"UFDs9::display> unable to open pipe to xpaset..."<<endl;
    return -1;
  }
  clog<<"UFDs9::display> fits sz: "<<sz<<endl;
  UFRuntime::bell();
  if( _xpa == 0 )
    open();
  int nb = ::write(fileno(_xpa), fits, sz);
  fflush(_xpa);
  // evidently we can only write one fits-file buffer at a time
  // close & re-open the pipe to xpaset:
  close();
  return nb;
}

int UFDs9::display(const char* fitsfilenm, int dpy) {
  if( fitsfilenm == 0 ) {
    clog<<"UFDs9::display> filename not set? "<<endl;
    return -1;
  } 
  
  string sys = "/usr/local/bin/xpaset -p ds9 file ";
  if( open(dpy, fitsfilenm) <= 0 ) {
    clog<<"UFDs9::display> unable to stat: "<<fitsfilenm<<endl;
    //return -1;
  } // _filenm should get set by open
  if( _filenm == 0 ) {
    clog<<"UFDs9::display> filename not set? "<<fitsfilenm<<endl;
    return -1;
  }

  sys += *_filenm;
  return ::system(sys.c_str());                   
}

#endif // __UFDs9_cc__

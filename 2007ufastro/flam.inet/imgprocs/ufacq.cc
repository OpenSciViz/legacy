#if !defined(__ufacq_cc__)
#define __ufacq_cc__ "$Name:  $ $Id: ufacq.cc 14 2008-06-11 01:49:45Z hon $"

#include "string"
#include "deque"
#include "vector"
#include "stdio.h"

#include "UFClientApp.h"
#include "UFAcqCtrl.h"
#include "UFInts.h"
#include "UFStrings.h"
#include "UFFlamObsConf.h"

static bool _flamconf= false, _frmdata= false, _acquire= false;

class ufacq : public UFClientApp {
public:
  inline ufacq(const string& name, int argc,
	       char** argv, char** envp, int port= -1) : UFClientApp(name, argc, argv, envp, port) {}
  inline ~ufacq() {}
  static int main(const string& name, int argc, char** argv, char** envp);
  inline virtual string description() const { return __ufacq_cc__; }

  // options: -shutdown -stat -start frmcnt -stop -abort -index start index -timeout sec. 
  //          -lut [lutfile] -fits fitsheader -file hint -ds9 [1,2,3,4] -jpeg -png
  size_t addOption(const string& cmdline, const string& opt, const string& dflt, vector< string >& req);
  int parse(vector< string >& req);
  // interactive mode: (no '-')?
  int parse(const string& line, vector< string >& req);

  // use UFAcqCtrl::Observe::asStrings()
  int parse(deque< string >& req);
  int parse(string& line, deque< string >& req);

  // submit req. bundle, flush output and sleep a bit.
  // recv reply and return reply as string.
  int submit(vector< string >& req, UFStrings*& reply, float flush= -1.0);
  int submit(deque< string >& req, UFStrings*& reply, float flush= -1.0);

  void handleFrame();
};

// for non-boolean options
size_t ufacq::addOption(const string& cmdline, const string& opt, const string& dflt, vector< string >& req) {
  size_t pos = cmdline.find(opt);
  char* nodashopt = (char*) opt.c_str(); ++nodashopt;
  if( pos != string::npos ) { // option is present
    req.push_back(nodashopt); 
    size_t offset = pos + opt.length();
    if( offset >= cmdline.length() ) { // eol, use default
      req.push_back(dflt);
      return pos;
    }      
    string optval = cmdline.substr(offset);
    const char* cs = optval.c_str(); 
    if( cs[0] == '-' ) { // but next option follows, assume this option has default
      req.push_back(dflt);
    }
    else {
      size_t next = cmdline.find("-", offset);
      if( next != string::npos ) {
        size_t len = next - offset;
	string val = cmdline.substr(offset, len);
        req.push_back(val);
      }
      else { // eol
	if( optval == "" || optval == " " || optval == "  " ) // just white spaces
	  optval = dflt;
        req.push_back(optval);
      }
    }
  }
  return pos;
}

int ufacq::parse(const string& line, vector< string >& req) {
  _acquire = false;
  UFPosixRuntime::_verbose = false;
  req.clear();
  size_t pos = line.find("-v");
  if( pos != string::npos ) {
    UFPosixRuntime::_verbose = true;
  }

  pos = line.find("-shutdown");
  if( pos != string::npos ) {
    req.push_back("shutdown"); req.push_back("true"); return req.size()/2;
  }

  pos = line.find("-sim");
  if( pos != string::npos ) { // boolean
    req.push_back("sim"); req.push_back("true"); return req.size()/2;
  }

  // convenience booleans for -acq option
  pos = line.find("-start");
  if( pos != string::npos ) {
    int cnt = 1;
    req.push_back("acq"); req.push_back("start"); 
    _acquire = true;
    // if -start is followed by numeric, assume shorthand for -cnt
    size_t offset = pos + strlen("-start");
    string optval = line.substr(offset);
    const char* cs = optval.c_str(); 
    if( cs[0] != '-' ) { // cnt value follows
      req.push_back("cnt"); 
      size_t next = line.find("-", offset);
      if( next != string::npos ) {
        size_t len = next - offset;
        req.push_back(line.substr(offset, len));
      }
      else { // must be final arg
	req.push_back(optval);
      }
    }
    else {
      strstream s;
      s<<cnt<<ends;
      req.push_back("cnt"); req.push_back(s.str()); delete s.str();
    }
  }

  pos = line.find("-stop");
  if( pos != string::npos ) {
    req.push_back("acq"); req.push_back("stop"); 
  }
  pos = line.find("-abort");
  if( pos != string::npos ) {
    req.push_back("acq"); req.push_back("abort"); 
  }

  pos = addOption(line, "-stat", "fits", req);
  pos = addOption(line, "-ds9", "1", req); // -ds9 [#frame/image in display]
  pos = addOption(line, "-index", "0", req);
  pos = addOption(line, "-timeout", "0", req);

  string datatoday("/data/");
  datatoday += UFRuntime::currentDate();
  pos = addOption(line, "-file", datatoday, req);

  pos = addOption(line, "-lut", "true", req);
  pos = addOption(line, "-frame", "flam:512Image", req);
  pos = addOption(line, "-conf", "all", req);
  pos = addOption(line, "-fits", "false", req); // fits header template to use
  pos = addOption(line, "-png", "true", req);
  pos = addOption(line, "-jpeg", "true", req);

  return req.size()/2;
}

int ufacq::parse(vector< string >& req) {
  string line= ""; // put entire command line into this and call parse(line, etc.)
  UFRuntime::Argv& argv = *_args;
  for( int i= 0; i < (int)argv.size(); ++i )
    line += argv[i];

  return parse(line, req);
}

int ufacq::parse(string& line, deque< string >& req) {
  _acquire = false;
  UFPosixRuntime::_verbose = false;
  req.clear();
  UFRuntime::Argv args;
  int argc = UFRuntime::argVec(line, args);
  string arg = UFRuntime::argVal("-v", args);
  if( arg != "false" ) {
    UFPosixRuntime::_verbose = true;
  }
  if( argc <= 1 ) return 0;

  arg = UFRuntime::argVal("-shutdown", args);
  if( arg != "false" ) {
    req.push_back("shutdown"); req.push_back("true"); return req.size()/2;
  }
  arg = UFRuntime::argVal("-stop", args);
  if( arg != "false" ) {
    req.push_back("acq"); req.push_back("stop"); return req.size()/2;
  }
  arg = UFRuntime::argVal("-abort", args);
  if( arg != "false" ) {
    req.push_back("acq"); req.push_back("abort"); return req.size()/2;
  }
  arg = UFRuntime::argVal("-sim", args);
  if( arg != "false" ) {
    req.push_back("sim"); req.push_back("true"); return req.size()/2;
  }
  arg = UFRuntime::argVal("-stat", args);
  if( arg != "false" ) {
    req.push_back("stat"); req.push_back("true"); return req.size()/2;
  }
  arg = UFRuntime::argVal("-frame", args);
  if( arg != "false" ) {
    req.push_back("frame"); req.push_back("flam:512Image"); return req.size()/2;
  }
  arg = UFRuntime::argVal("-conf", args);
  if( arg != "false" ) {
    req.push_back("conf"); req.push_back("all"); return req.size()/2;
  }

  // start obs
  string _exptime, _expcnt, _dhslabel, _datamode, _dhsqlook, _nfshost, _datadir, _fitsfile, _lutfile, _index,
         _usrinfo, _comment, _ds9, _pngjpegfile;
  UFAcqCtrl::Observe obsreq;
  string timeout= "10";

  arg = UFRuntime::argVal("-timeout", args);
  if( arg != "false" ) {
    if( arg != "true" )
      timeout = arg;
  }

  arg = UFRuntime::argVal("-start", args);
  if( arg != "false" ) {
    if( arg != "true" )
      obsreq._expcnt = arg;
    else
      obsreq._expcnt = "1";
  }
  arg = UFRuntime::argVal("-ds9", args);
  if( arg != "false" ) {
    if( arg != "true" )
      obsreq._ds9 = arg;
    else
      obsreq._ds9 = "1";
  }
  arg = UFRuntime::argVal("-index", args);
  if( arg != "false" ) {
    if( arg != "true" )
      obsreq._index = arg;
    else
      obsreq._index = "0";
  }
  arg = UFRuntime::argVal("-label", args);
  if( arg != "false" ) {
    if( arg != "true" )
      obsreq._dhslabel = arg;
    else
      obsreq._dhslabel = "F2Test";
  }
  arg = UFRuntime::argVal("-file", args);
  if( arg != "false" ) {
    if( arg != "true" )
      obsreq._fitsfile = arg;
    else
      obsreq._fitsfile = "/data/" + obsreq._dhslabel;
  }
  arg = UFRuntime::argVal("-fits", args);
  if( arg != "false" ) {
    if( arg != "true" )
      obsreq._fitsfile = arg;
    else
      obsreq._fitsfile = "/data/" + obsreq._dhslabel;
  }
  arg = UFRuntime::argVal("-lut", args);
  if( arg != "false" ) {
    if( arg != "true" )
      obsreq._lutfile = arg;
    else
#if defined(sparc)
      obsreq._lutfile = "UFFlamingos.lutBE";
#else
      obsreq._lutfile = "UFFlamingos.lutLE";
#endif
  }
  arg = UFRuntime::argVal("-png", args);
  if( arg != "false" ) {
    if( arg != "true" )
      obsreq._pngjpegfile = arg;
  }
  arg = UFRuntime::argVal("-jpeg", args);
  if( arg != "false" ) {
    if( arg != "true" )
      obsreq._pngjpegfile = arg;
  }
  int nr = obsreq.asStrings(req);
  return nr/2;
}

int ufacq::parse(deque< string >& req) {
  string line= ""; // put entire command line into this and call parse(line, etc.)
  UFRuntime::Argv& argv = *_args;
  int argc = (int)argv.size();
  for( int i= 0; i < argc; ++i ) {
    line += argv[i];
    if( i < (argc-1) ) line += " ";
  }

  return parse(line, req);
}

int ufacq::submit(vector< string >& req, UFStrings*& r, float flush) {
  //if( UFPosixRuntime::_verbose ) {
    for( int i = 0; i < (int) (req.size() - 1); i += 2 )
      clog<<"ufacq::submit> req: "<<req[i]<<" : "<<req[i+1]<<endl;
  //}
  UFStrings ufs(name(), req);
  int ns = send(ufs);
  if( ns <= 0 )
    return ns;
  // try flushing the socket output stream
  // this is the only way i can think to do it:
  int stat = fflush(_fs);
  if( stat != 0 ) clog<<"ufacq::submitPar> socket flush failed? ns= "<<ns<<endl;

  if( flush > 0.0 ) // optionally sleep after the flush
    UFPosixRuntime::sleep(flush);

  r = dynamic_cast<UFStrings*> (UFProtocol::createFrom(*this));
  if( r != 0 )
    return r->elements();
  else
    return 0;
}

int ufacq::submit(deque< string >& req, UFStrings*& r, float flush) {
  //if( UFPosixRuntime::_verbose ) {
    for( int i = 0; i < (int) (req.size() - 1); i += 2 )
      clog<<"ufacq::submit> req: "<<req[i]<<" : "<<req[i+1]<<endl;
  //}
  UFStrings ufs(name(), req);
  int ns = send(ufs);
  if( ns <= 0 )
    return ns;
  // try flushing the socket output stream
  // this is the only way i can think to do it:
  int stat = fflush(_fs);
  if( stat != 0 ) clog<<"ufacq::submitPar> socket flush failed? ns= "<<ns<<endl;

  if( flush > 0.0 ) // optionally sleep after the flush
    UFPosixRuntime::sleep(flush);

  r = dynamic_cast<UFStrings*> (UFProtocol::createFrom(*this));
  if( r != 0 )
    return r->elements();
  else
    return 0;
}

int ufacq::main(const string& name, int argc, char** argv, char** envp) {
  ufacq acq(name, argc, argv, envp);
  string arg, host(hostname());
  int port= 52001;
  float flush= -1.0;

  string usage = "usage: 'ufacq [-q] [-flush sec.] [-host host] [-port port] [-stat] [-conf [obs/frame/all] [-start [cnt]] [-stop -abort] [-index start index] [-timeout sec.] [-lut lutfile] [-fits fitsfile] [-file hint] [-ds9 [1,N]] [-jpeg] [-png]'";
  
  arg = acq.findArg("-h");
  if( arg != "false" ) {
    clog<<"ufacq> "<<usage<<endl;
    return 0;
  }

  arg = acq.findArg("-help");
  if( arg != "false" ) {
    clog<<"ufacq> "<<usage<<endl;
    return 0;
  }

  arg = acq.findArg("-q");
  if( arg != "false" )
    _quiet = true;

  arg = acq.findArg("-host");
  if( arg != "false" )
    host = arg;

  arg = acq.findArg("-port");
  if( arg != "false" && arg != "true" )
    port = atoi(arg.c_str());

  if( port <= 0 || host.empty() ) {
    clog<<"ufacq> "<<usage<<endl;
    return 0;
  }

  arg = acq.findArg("-flush");
  if( arg != "false" )
    flush = atof(arg.c_str());

  // req. bundle:
  deque< string > req;
  // req. status reply:
  UFStrings* preply= 0;

  // allow parse logic to be tested before connect:
  int narg = acq.parse(req); // parse command line

  FILE* f = acq.init(host, port);
  if( f == 0 ) {
    clog<<"ufacq> unable to connect to edtd agent..."<<endl;
    return -2;
  }

  if( narg >= 1 ) {
    int ns = acq.submit(req, preply, flush);
    if( ns <= 0 ) {
      clog << "ufacq> failed to submit: "<<req[0]<<"::"<<req[1]<<endl;
      return acq.close();
    }
    UFStrings& reply = *preply;
    if( !_quiet )
      cout<<reply.timeStamp()<<reply.name()<<endl;
    for( int i=0; i < reply.elements(); ++i )
      cout<<reply[i]<<endl;

    delete preply; preply = 0;

    if( _flamconf || _frmdata )
      acq.handleFrame();

    return acq.close();
  }

  // getting here indicates command prompt mode:
  string line;
  while( true ) {
    clog<<"ufacq> "<<ends;
   _flamconf= false; _frmdata= false;
    getline(cin, line);
    if( line == "exit" || line == "quit" || line == "q" ) return 0;
    if( line.size() <= 1 )
      continue; // ignore empty line

    narg = acq.parse(line, req); // parse interactive prompt line
    if( narg <= 0 )
      continue;

    int ns = acq.submit(req, preply, flush);
    if( ns <= 0 ) {
      clog << "ufacq> failed to submit: "<<req[0]<<"::"<<req[1]<<endl;
      return acq.close();
    }
    UFStrings& reply = *preply;
    if( !_quiet )
      cout<<reply.timeStamp()<<reply.name()<<endl;
    for( int i=0; i < reply.elements(); ++i )
      cout<<reply[i]<<endl;

    delete preply; preply = 0;

    if( _flamconf || _frmdata )
      acq.handleFrame();
   }
  return acq.close(); 
}


void ufacq::handleFrame() {
  UFFlamObsConf* fc = dynamic_cast<UFFlamObsConf*> (UFProtocol::createFrom(*this));
  if( fc == 0 ) {
    clog<<"ufacq::handleFrame> failed to recv. UFFlamObsConf..."<<endl;
    return;
  }
  clog<<"ufacq::handleFrame> got UFFlamObsConf: "<< fc->name()<<endl;

  UFInts* frm = dynamic_cast<UFInts*> (UFProtocol::createFrom(*this));
  if( fc == 0 ) {
    clog<<"ufacq::handleFrame> failed to recv (Ints) frame..."<<endl;
    return;
  }
  clog<<"ufacq::handleFrame> got (Ints) frame: "<< frm->name()<<endl;

  delete fc; delete frm;
  return;
}   

int main(int argc, char** argv, char** envp) {
  return ufacq::main("ufacq", argc, argv, envp);
}

#endif // __ufacq_cc__

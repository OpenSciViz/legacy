#if !defined(__UFGemSymbolAgent_cc__)
#define __UFGemSymbolAgent_cc__ "$Name:  $ $Id: UFGemSymbolAgent.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFGemSymbolAgent_cc__;

#include "sys/types.h"
#include "sys/stat.h"
#include "fcntl.h"

#include "string"
#include "vector"

#include "UFGemSymbolAgent.h"
#include "UFSBConfig.h"
#include "UFSADFITS.h"

string UFGemSymbolAgent::_current;
string UFGemSymbolAgent::_postsad;
string UFGemSymbolAgent::_postcar;
string UFGemSymbolAgent::_simtrigger;

// helper for ctors:
void UFGemSymbolAgent::setDefaults(const string& instrum, bool initsad) {
  _current = _postsad = _postcar = "";
  _StatRecs.clear();
  if( !instrum.empty() && instrum != "false" )
    _epics = instrum;
  
  if( initsad && _epics != "" && _epics != "false" ) {
    UFSADFITS::initSADHash(_epics);
    //_confGenSub = _epics + ":ec:configG";
    _confGenSub = "";
    _heart = _epics + ":ec:Barheartbeat.VAL";
      for( int i = 0; i < (int) UFSADFITS::_ccsymbarcdsad->size(); ++i )
      //_StatRecs.push_back((*UFSADFITS::_ccsymbarcdsad)[i] + ".INP");
      _StatRecs.push_back((*UFSADFITS::_ccsymbarcdsad)[i] + ".VAL");
    /*
    _StatRecs.push_back(_epics + ":sad:MOSPlateBarCode.inp"); // _current
    _StatRecs.push_back(_epics + ":sad:circMOSPos1BarCode.inp");
    _StatRecs.push_back(_epics + ":sad:circMOSPos2BarCode.inp");
    _StatRecs.push_back(_epics + ":sad:MOSPos1BarCode.inp");
    _StatRecs.push_back(_epics + ":sad:MOSPos2BarCode.inp");
    _StatRecs.push_back(_epics + ":sad:MOSPos3BarCode.inp");
    _StatRecs.push_back(_epics + ":sad:MOSPos4BarCode.inp");
    _StatRecs.push_back(_epics + ":sad:MOSPos5BarCode.inp");
    _StatRecs.push_back(_epics + ":sad:MOSPos6BarCode.inp");
    _StatRecs.push_back(_epics + ":sad:MOSPos7BarCode.inp");
    _StatRecs.push_back(_epics + ":sad:MOSPos8BarCode.inp");
    _StatRecs.push_back(_epics + ":sad:MOSPos9BarCode.inp");
    */
    _statRec = _StatRecs[0];
  }
  else {
    _confGenSub = "";
    _heart = "";
    _statRec = "";
  }
  _simtrigger = "15";
}

// ctors:
UFGemSymbolAgent::UFGemSymbolAgent(int argc, char** argv, char** env) : UFGemDeviceAgent(argc, argv, env) {
  // use ancillary function to retrieve barcode readings
  _flush = 0.2; // inherited
  bool *bp = new bool;
  *bp = false; // arg to ancillary func. indicates whatever
  UFRndRobinServ::_ancil = (void*) bp; // non-null indicate acillary func. should be invoked
}

UFGemSymbolAgent::UFGemSymbolAgent(const string& name,
					     int argc, char** argv, char** env) : UFGemDeviceAgent(name, argc, argv, env) {
  // use ancillary function to retrieve barcode readings
  _flush =  0.2; // inherited
  bool *bp = new bool;
  *bp = false; // arg to ancillary func. indicates whatever
  UFRndRobinServ::_ancil = (void*) bp; // non-null indicate acillary func. should be invoked
}

// static funcs:
int UFGemSymbolAgent::main(int argc, char** argv, char** env) {
  UFGemSymbolAgent ufs("UFGemSymbolAgent", argc, argv, env); // ctor binds to listen socket
  ufs.exec((void*)&ufs);
  return argc;
}

// public virtual funcs:
// this should always return the service/agent listen port #
int UFGemSymbolAgent::options(string& servlist) {
  UFDeviceAgent::_config = _config = new UFSBConfig(); // needs to be proper device subclass (GP345)
  int port = UFGemDeviceAgent::options(servlist);
  // _epics may be reset by above:
  if( _epics.empty() ) // use "flam" default
    setDefaults();
  else
    setDefaults(_epics);

  // g-p defaults for ufinstrum
  _config->_tsport = _config->_tsport = 7005;
  _config->_tshost = _config->_tshost = "flamperle"; // "192.168.111.125";

  string arg = findArg("-sim"); // sim mode
  if( arg != "false" ) {
    _sim = true;
    if( arg != "true" )
      _simtrigger = arg;
  }

  arg = findArg("-tsport"); 
  if( arg != "false" && arg != "true" )
    _config->_tsport = atoi(arg.c_str());

  arg = findArg("-tshost");
  if( arg != "false" && arg != "true" )
    _config->_tshost = arg;
  
  arg = findArg("-flush"); 
  if( arg != "false" && arg != "true" )
    _flush = atof(arg.c_str());

  arg = findArg("-v");
  if( arg != "false" ) {
    _verbose = true;
    UFTermServ::_verbose = _verbose;
    //UFSocket::_verbose = _verbose;
  }

  arg = findArg("-c");
  if( arg != "false" )
    _confirm = true;
  arg = findArg("-g");
  if( arg != "false" )
    _confirm = true;

  if( _config->_tsport ) {
    port = 52000 + _config->_tsport - 7000;
  }
  else {
    port = 52005;
  }
  if( _verbose ) {
    clog<<"UFGemSymbolAgent::options> _epics: "<< _epics<<endl;
    clog<<"UFGemSymbolAgent::options> set port to GemSymbol port "<<port<<endl;
  }
  return port;
}

// override base class startup here to avoid connect to term. serv
// then do server listen stuff
void UFGemSymbolAgent::startup() {
  setSignalHandler(UFGemDeviceAgent::sighandler);
  clog << "UFGemSymbolAgent::startup> established signal handler."<<endl;

  // should parse cmd-line options and start listening
  string servlist;
  int listenport = options(servlist);
  // agent that actually uses a serial device
  // attached to the perle should initialize a connection to it here:
  if( _config == 0 ) {
    clog<<"UFGemSymbolAgent::startup> no DeviceConfig, assume simulation? "<<endl;
    init();
  }
  else {
    init(_config->_tshost, _config->_tsport);
  }
  UFSocketInfo socinfo = _theServer.listen(listenport);

  clog << "UFGemSymbolAgent::startup> Ok, startup completed, listening on port= " <<listenport<<endl;
  return;  
}

UFTermServ* UFGemSymbolAgent::init(const string& tshost, int tsport) {
  UFTermServ* ts= 0;
  if( _epics != "false" ) {
    _capipe = pipeToEpicsCAchild(_epics);
    if( _capipe )
      clog<<"UFGemSymbolAgent> Epics CA child proc. started, fd: "<<fileno(_capipe)<<endl;
    else
      clog<<"UFGemSymbolAgent> Ecpics CA child proc. failed to start"<<endl;
  }
  if( !_sim ) {
    if( _verbose )
	clog << "UFGemSymbolAgent::init> connect to PerleConsole: "<<tshost<<" on port= " <<tsport<<endl;
    // connect to terminal server:
    ts = _config->connect(tshost, tsport);
    if( ts == 0 || _config->_devIO == 0 ) {
      clog<<"UFGemSymbolAgent> failed connection to annex/iocom."<<endl;
      exit(-1);
    }
  }
  // test connectivity to barcode (beep the bell):
  string barc; // cmd & reply strings 
  time_t clck = time(0);
  if( clck % 2 == 0 )
    barc = "F11223344";
  else
    barc = "F55667788";

  if( _sim ) {
    clog<<"UFGemSymbolAgent> simulating connection to Symbol BarCode reader."<<endl;
  }
  else {
    clog<<"UFGemSymbolAgent> checking availiblity of Symbol BarCode reader."<<endl;
    UFClientSocket* soc = static_cast< UFClientSocket* > (_config->_devIO);
    int na = soc->available();
    unsigned char con[2];
    if( na < 0 ) {
      clog<<"UFGemSymbolAgent> Symbol BarCode connect failed..."<<endl;
      _config->_devIO->close();
      delete _config->_devIO;
      exit(-1);
    }
    /*
    else if( na == 0 ) {
      soc->recv(con, 1); // block on 1 byte
    }
    */
    if( na > 0 ) {
      soc->recv(con, na); // empty out any 'greeting' from device
    }
    /*
    UFSBConfig* barcd = static_cast< UFSBConfig* > (_config);
    int nc = barcd->beep();
    if( nc <= 0 ) {
      clog<<"UFGemSymbolAgent> Symbol BarCode beep failed..."<<endl;
      _config->_devIO->close();
      delete _config->_devIO;
      exit(-1);
    }
    */
  }

  return _config->_devIO;
}

void* UFGemSymbolAgent::ancillary(void* p) {
  bool block = false;
  if( p )
    block = *((bool*)p);

  time_t clck = time(0);
  time_t trig = atoi(_simtrigger.c_str());
  // assume blocking i/o for now...
  if( !_sim && _config && _config->_devIO ) {
    UFSBConfig* barcd = static_cast< UFSBConfig* > (_config);
    UFClientSocket* soc = static_cast< UFClientSocket* > (_config->_devIO);
    int na = soc->available();
    if( na > 0 ) {
      //if( _verbose )
        clog<<"UFGemSymbolAgent::ancillary> na: "<<na<<endl;
      _current = barcd->recvPacket(0.5); // recv any buffered scan
      //if( _verbose )
	clog<<"UFGemSymbolAgent::ancillary> new barcode reading length: "<<_current.length()
	    <<", reading: "<<_current<<endl;
    }
    else if( _verbose ) {
      clog<<"UFGemSymbolAgent::ancillary> no new barcode reading yet..."<<endl;
      //_current = ""; // clear current reading only after postsad prerformed...
    }
    /*
    int nc = barcd->beep();
    if( nc <= 0 ) {
      clog<<"UFGemSymbolAgent> Symbol BarCode beep failed..."<<endl;
      _config->_devIO->close();
      delete _config->_devIO; _config->_devIO = 0;
      return 0;
    }
    */
  }
  else if( _sim && !_postsad.empty() && (clck % trig == 0) && (clck % 2*trig == 0) ) { 
    if( clck % 2 == 0 )
      _current = "F11223344";
    else
      _current = "F55667788";
  }

  // update the status value:
  if( _verbose ) clog<<"UFGemSymbolAgent::ancillary> _current: "<<_current
		     <<", _postsad: "<<_postsad
		     <<", _statRec: "<<_statRec<<endl;
  if( !_current.empty() ) {
    if( _capipe && _epics != "false" && !_statRec.empty() ) {
      clog<<"UFGemSymbolAgent::ancillary> sendEpics: "<<_statRec<<", current: "<<_current<<endl;
      sendEpics(_statRec, _current);
    }
    if( !_postsad.empty() ) {
      UFSBConfig::setBarc(_postsad, _current);
      if( _capipe && _epics != "false") {
        clog<<"UFGemSymbolAgent::ancillary> sendEpics: "<<_postsad<<", current: "<<_current<<endl;
        sendEpics(_postsad, _current); // post to mos position sad
	if( !_postcar.empty() ) {
          clog<<"UFGemSymbolAgent::ancillary> set Idle: "<<_postcar<<endl;
	  setCARIdle(_postcar); // set CAR to Idle
	}
      }
      _postsad = ""; _postcar = ""; _current = ""; // clear current reading & postings
    }
  }
   
  if( !_current.empty() )
    _DevHistory.push_back(_current);
  if( _DevHistory.size() > _DevHistSz ) // should be configurable size
    _DevHistory.pop_front();

  //if( _verbose )
   // clog<<"UFGemSymbolAgent::ancillary> current BarCode reading: "<<_current<<endl;

  // update the heartbeat:
  if( _capipe && _epics != "false" && _heart != "" ) {
    strstream s;
    s<<clck<<ends;
    updateEpics(_heart, s.str());
    delete s.str();
  }
 
  return p;
} //ancillary

// these are the key virtual functions to override,
// send command string to TermServ, and return response
// presumably any allocated memory is freed by the calling layer....
// for the moment assume "raw" commands and simply pass along
// the Gemini/Epics version must always return null to client, and
// instead send the command completion status to the designated CAR,
// if no CAR is desginated, an error/alarm condition should be indicated
int UFGemSymbolAgent::action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& rep) {
  //int stat= 0;
  bool reply_expected= true;
  rep = new vector< UFProtocol* >;
  vector< UFProtocol* >& replies = *rep;

  if( act->clientinfo.find(":") != string::npos ) { // must be epics client
    reply_expected = false;
    clog<<"UFGemSymbolAgent::action> No replies will be sent to Gemini/Epics client: "
        <<act->clientinfo<<endl;
  }
  if( _verbose ) {
    clog<<"UFGemSymbolAgent::action> client: "<<act->clientinfo<<endl;
    for( int i = 0; i<(int)act->cmd_name.size(); ++i ) 
      clog<<"UFGemSymbolAgent::action> elem= "<<i<<" "<<act->cmd_name[i]<<" "<<act->cmd_impl[i]<<endl;
  }
   
  string car, errmsg, hist;
  for( int i = 0; i < (int)act->cmd_name.size(); ++i ) {
    bool sim= false;
    string cmdname= act->cmd_name[i];
    string cmdimpl= act->cmd_impl[i];
    //if( _verbose ) {
      clog<<"UFGemSymbolAgent::action> cmdname: "<<cmdname<<endl;
      clog<<"UFGemSymbolAgent::action> cmdimpl: "<<cmdimpl<<endl;
    //}
    if( cmdname.find("sta") == 0 || cmdname.find("Sta") == 0 || cmdname.find("STA") == 0 ) {
      // presumably a non-epics client has requested status info (generic of FITS)
      // assume for now that it is OK to return upon processing the status request
      if( cmdimpl.find("fit") != string::npos ||
	  cmdimpl.find("Fit") != string::npos || cmdimpl.find("FIT") != string::npos  ) {
        replies.push_back(_config->statusFITS(this));
	return (int)replies.size();
      }
      else {
        replies.push_back(_config->status(this)); 
	return (int)replies.size();
      }
    }
    else if( cmdname.find("car") == 0 ||
	cmdname.find("Car") == 0 ||
        cmdname.find("CAR") == 0 ) {
      reply_expected= false;
      _postcar = car = cmdimpl;
      //if( _verbose ) 
        clog<<"UFGemSymbolAgent::action> CAR: "<<car<<endl;
      continue;
    }
    else if( cmdname.find("post") == 0 ||
	cmdname.find("Post") == 0 ||
        cmdname.find("POST") == 0 ) {
      _postsad = cmdimpl;
      //if( _verbose )
        clog<<"UFGemSymbolAgent::action> (expect trigger) Post SAD: "<<_postsad<<endl;
    }
    else if( cmdname.find("abort") == 0 ||
	cmdname.find("Abort") == 0 ||
        cmdname.find("ABORT") == 0 ) {
      _postsad = _statRec;
      //if( _verbose ) 
        clog<<"UFGemSymbolAgent::action> (abort trigger) Post SAD: "<<_postsad<<endl;
    }
    else if( cmdname.find("his") == 0 ||
	     cmdname.find("His") == 0 || 
	     cmdname.find("HIS") == 0 ) {
      hist = cmdimpl;
    }
    else if( cmdname.find("raw") == 0 ||
	     cmdname.find("Raw") == 0 || 
	     cmdname.find("RAW") == 0 ) {
      int errIdx = cmdname.find("!!");
      if( errIdx > 0 ) 
        errmsg = cmdname.substr(2+errIdx);
      //if( _verbose ) 
        clog<<"UFGemSymbolAgent::action> Raw: "<<cmdimpl<<"; errmsg= "<<errmsg<<endl;
    }
    else if( cmdname.find("sim") != 0 ||
	     cmdname.find("Sim") != 0 || 
	     cmdname.find("SIM") != 0 ) {
      sim = true;
      size_t errIdx = cmdname.find("!!");
      if( errIdx != string::npos && errIdx > 0 ) 
        errmsg = cmdname.substr(2+errIdx);
      if( _verbose ) clog<<"UFGemSymbolAgent::action> Sim: "<<cmdimpl<<"; errmsg= "<<errmsg<<endl;
    }
    else {
      act->status_cmd = "rejected";
      clog<<"UFGemSymbolAgent::action> ?improperly formatted request?"<<endl;
      act->cmd_reply.push_back("?improperly formatted request? Rejected.");
      if( reply_expected ) {
        replies.push_back(new UFStrings(act->clientinfo+">>"+cmdname+" "+cmdimpl,act->cmd_reply));
        return (int)replies.size();
      }
      else {
	delete rep; rep = 0;
        return 0;      
      }
    }
    // use the annex/iocomm/perle port to submit the action command and get reply:
    act->time_submitted = currentTime();
    _active = act;
    if( !_sim && _config != 0 && _config->_devIO ) {
      int nr = _config->validCmd(cmdname);
      if( nr > 0 ) { // valid  cmd
        if( _verbose ) clog<<"UFGemSymbolAgent::action>: "<<cmdname<<" : "<<cmdimpl<<endl;
      }
      else { // default to history
	hist = "1";
      }
    }
    if( hist != "" ) {
      int last = (int)_DevHistory.size() - 1;
      int cnt = atoi(cmdimpl.c_str());
      if( cnt <= 0 ) cnt = 1;
      if( cnt > last+1 ) cnt = last+1;
      for( int i = 0; i < cnt; ++i )
        act->cmd_reply.push_back(_DevHistory[last - i]);
    }
    // simulate an error condition?
    if( sim && (cmdimpl.find("err") != string::npos || cmdimpl.find("ERR") != string::npos) ) {
      errmsg += cmdimpl;
      act->cmd_reply.push_back("Sim OK, sleeping 1/2 sec...");
      //if( _verbose ) 
	clog<<"UFGemSymbolAgent::action> simulating error condition..."<<endl;
      if( car != "" && _epics != "false" && _capipe ) setCARError(car, &errmsg);
    }
    else if( sim ) {
      time_t clck = time(0);
      if( clck % 2 == 0 )
        _current = "F11112345";
      else
        _current = "F99998765";
      act->cmd_reply.push_back(_current);
    }
    if( _postsad != "" )
      act->cmd_reply.push_back(_postsad);
  } // end for loop of cmd bundle

  if(  act->cmd_reply.empty() ) {
    // send error (val=3) & error message to _capipe
    act->status_cmd = "failed";
    //if( _verbose ) 
      clog<<"UFGemSymbolAgent::action> error condition?"<<endl;
    if( car != "" && _epics != "false" && _capipe )
      setCARError(car, &errmsg);
      //sendEpics(car, errmsg);
  }
  else {
    // success (CAR idle val = 0) to _capipe performed by acnillary
    act->status_cmd = "succeeded";
  }  
  act->time_completed = currentTime();
  _completed.insert(UFDeviceAgent::CmdList::value_type(act->cmd_time, act));

  if( reply_expected ) {
    replies.push_back(new UFStrings(act->clientinfo, act->cmd_reply));
    return (int) replies.size(); // rep gets deleted by UFRndRobin::exec
  }

  delete rep; rep = 0; // rep not used, delete it here
  return 0;
} 

int UFGemSymbolAgent::query(const string& q, vector<string>& qreply) {
  return UFDeviceAgent::query(q, qreply);
}

void UFGemSymbolAgent::hibernate() {
  if( _queued.size() == 0 && UFRndRobinServ::_theConnections->size() == 0) {
    // no connections, no queued reqs., go to sleep
    mlsleep(_Update);
  }
  else {
    //UFSocket::waitOnAll(_Update);
    UFRndRobinServ::hibernate();
  }
}

#endif // UFGemSymbolAgent

#if !defined(__ufsymbc__)
#define __ufsymbc__ "$Name:  $ $Id: ufsymbc.cc 14 2008-06-11 01:49:45Z hon $";

#include "string"
#include "vector"
#include "stdio.h"

#include "UFDaemon.h"
#include "UFClientSocket.h"
#include "UFTimeStamp.h"
#include "UFStrings.h"

static bool _quiet = false;

class ufsymbc : public UFDaemon, public UFClientSocket {
public:
  ~ufsymbc();
  inline ufsymbc(int argc, char** argv, int port= -1) : UFDaemon(argc, argv), UFClientSocket(port), _fs(0) {
    rename("ufsymbc@" + hostname());
  }

  static int main(int argc, char** argv);

  // return 0 on connection failure:
  FILE* init(const string& host, int port);

  // submit cmd & par (only), return immediately without fetching reply
  int submit(const string& c, const string& p, float flush= -1.0); // flush output and sleep flush sec.

  // submit string, recv reply and return reply as string
  //int submit(const string& raw, UFStrings& reply, float flush= -1.0); // flush output and sleep flush sec.
  //int submit(const string& raw, UFStrings*& reply, float flush= -1.0); // flush output and sleep flush sec.
  int submit(const string& c, const string& p, UFStrings*& reply, float flush= -1.0); // flush output and sleep flush sec.

  virtual string description() const { return __ufsymbc__; }

protected:
  FILE* _fs; // for flushing ... this should really go into UFSocket someday
};

int ufsymbc::main(int argc, char** argv) {
  ufsymbc symbc(argc, argv);
  bool prompt = false;
  string arg, host(hostname()), hist("false"), stat("false"),
         post("false"), beep("false"), scan("false"), abort("false"); // default is usage
  int port= 52005;
  float flush= -1.0;

  arg = symbc.findArg("-q");
  if( arg != "false" )
    _quiet = true;

  arg = symbc.findArg("-beep");
  if( arg != "false" ) {
    _quiet = true;
    beep = "beep";
  }

  arg = symbc.findArg("-scan");
  if( arg != "false" ) {
    _quiet = true;
    scan = "scan";
  }

  arg = symbc.findArg("-abort");
  if( arg != "false" ) {
    _quiet = true;
    abort = "abort";
  }

  arg = symbc.findArg("-post");
  if( arg != "false" ) {
    _quiet = true;
    if( arg != "true" ) // explicit cmd string 
      post = arg;
    else
      post = "flam:sad:MOSPlateBarCode"; // "flam:sad:MOSPos9BarCode"
  }

  arg = symbc.findArg("-hist");
  if( arg != "false" ) {
    _quiet = true;
    if( arg != "true" ) // explicit cmd string 
      hist = arg;
    else
      hist = "1";
  }

  arg = symbc.findArg("-stat");
  if( arg != "false" ) {
    _quiet = true;
    if( arg != "true" ) // explicit cmd string 
      stat = arg;
    else
      stat = "fits";
  }

  arg = symbc.findArg("-flush");
  if( arg != "false" )
    flush = atof(arg.c_str());

  arg = symbc.findArg("-host");
  if( arg != "false" )
    host = arg;

  arg = symbc.findArg("-port");
  if( arg != "false" && arg != "true" )
    port = atoi(arg.c_str());

  if( port <= 0 || host.empty() ) {
    clog<<"ufsymbc> usage: 'ufsymbc -host host -port port -post \"instrum:sad:whatever\" -beep -abort -scan -hist'"<<endl;
    return 0;
  }

  FILE* f = symbc.init(host, port);
  if( f == 0 ) {
    clog<<"ufsymbc> unable to connect to Granville-Phillips symbcuum agent..."<<endl;
    return 1;
  }

  UFStrings* reply_p= 0;
  if( post != "false" ) {
    // raw (explicit) command should be executed once
    int ns = symbc.submit("POST", post, reply_p, flush);
    if( ns <= 0 )
      clog << "ufsymbc> failed to submit post= "<<post<<endl;
  }
  else if( beep != "false" ) {
    // command should be executed once
    int ns = symbc.submit("BEEP", beep, reply_p, flush);
    if( ns <= 0 )
      clog << "ufsymbc> failed to submit beep= "<<beep<<endl;
  }
  else if( scan != "false" ) {
    // command should be executed once
    int ns = symbc.submit("SCAN", scan, reply_p, flush);
    if( ns <= 0 )
      clog << "ufsymbc> failed to submit scan= "<<scan<<endl;
  }
  else if( hist != "false" ) {
    // command should be executed once
    int ns = symbc.submit("his", hist, reply_p, flush);
    if( ns <= 0 )
      clog << "ufsymbc> failed to submit hist= "<<hist<<endl;
  }
  else if( stat != "false" ) {
    // command should be executed once
    int ns = symbc.submit("stat", stat, reply_p, flush);
    if( ns <= 0 )
      clog << "ufsymbc> failed to submit stat= "<<stat<<endl;
  }
  else {
    prompt = true;
  }

  if( !prompt && reply_p == 0 ) {
    clog<<"ufsymbc> no reply..."<<endl;
    return -1;
  }
  else if( !prompt ) {
    UFStrings& reply = *reply_p;
    //cout<<reply.timeStamp()<<reply.name()<<endl;
    for( int i=0; i < reply.elements(); ++i )
      cout<<reply[i]<<endl;

    delete reply_p; reply_p = 0;
    return 0;
  }
  // enter command loop
  /*
  string line;
  while( true ) {
    clog<<"ufsymbc> "<<ends;
    getline(cin, line);
    if( line == "exit" || line == "quit" || line == "q" )
      return 0;

    raw = line;
    int ns = symbc.submit(raw, reply_p, flush);
    if( ns <= 0 )
      clog << "ufsymbc> failed to submit raw= "<<raw<<endl;

    if( reply_p == 0 ) {
      clog<<"ufsymbc> no reply..."<<endl;
      return -1;
    }
    UFStrings& reply = *reply_p;
    //cout<<reply.timeStamp()<<reply.name()<<endl;
    for( int i=0; i < reply.elements(); ++i )
      cout<<reply[i]<<endl;
    delete reply_p; reply_p = 0;
  }
  */
  clog<<"ufsymbc> interactive mode not supported, only command-line..."<<endl;
  return 0;
} // ufsymbc::main

// public func:
// return < 0 on connection failure:
FILE* ufsymbc::init(const string& host, int port) {
  int fd = connect(host, port);
  if( fd <= 0 ) {
    return 0;
  }
  _fs = fdopen(fd, "w");

  // after accetping connection, server/agent will expect client to send
  // a UFProtocol object identifying itself, and echos it back (slightly
  // modified) 
  UFTimeStamp greet(name());
  int ns = send(greet);
  ns = recv(greet);
  if( !_quiet )
    clog<<"ufsymbc> greeting: "<< greet.name() << " " << greet.timeStamp() <<endl;

  return _fs;
}

// public
ufsymbc::~ufsymbc() {
  if( _fs ) {
    close(); 
    fclose(_fs);
    _fs = 0;
  }
}

// submit string (only), return immediately without fetching reply
int ufsymbc::submit(const string& c, const string& p, float flush) {
  vector<string> cmd;
  cmd.push_back(c);
  cmd.push_back(p);
  UFStrings ufs(name(), cmd);
  int ns = send(ufs);
  // try flushing the socket output stream
  // this is the only way i can think to do it:
  int stat = fflush(_fs);
  if( stat != 0 ) clog<<"ufsymbc::submit> socket flush failed? ns= "<<ns<<endl;

  if( flush > 0.0 ) // optionally sleep after the flush
    UFPosixRuntime::sleep(flush);

  return ns;
}

// submit string, recv reply
/*
int ufsymbc::submit(const string& raw, UFStrings*& r, float flush) {
  int ns = submit("raw", raw, flush);
  if( ns <= 0 )
    return ns;

  r = dynamic_cast<UFStrings*> (UFProtocol::createFrom(*this));

  return r->elements();
}
*/

// submit string, recv reply
int ufsymbc::submit(const string& c, const string& p, UFStrings*& r, float flush) {
  int ns = submit(c, p, flush);
  if( ns <= 0 )
    return ns;

  r = dynamic_cast<UFStrings*> (UFProtocol::createFrom(*this));

  return r->elements();
}

int main(int argc, char** argv) {
  return ufsymbc::main(argc, argv);
}

#endif // __ufsymbc__

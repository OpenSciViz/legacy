#if !defined(__UFSBConfig_h__)
#define __UFSBConfig_h__ "$Name:  $ $Id: UFSBConfig.h,v 0.2 2004/11/23 21:48:07 hon Exp $"
#define __UFSBConfig_H__(arg) const char arg##UFSBConfig_h__rcsId[] = __UFSBConfig_h__;

#include "UFDeviceConfig.h"
#include "vector"

class UFSBConfig : public UFDeviceConfig {
public:
  UFSBConfig(const string& name= "UnknownSB@DefaultConfig");
  UFSBConfig(const string& name, const string& tshost, int tsport);
  inline virtual ~UFSBConfig() {}

  // override these virtuals:
  virtual vector< string >& UFSBConfig::queryCmds();
  virtual vector< string >& UFSBConfig::actionCmds();
  // return -1 if invalid, 0 if valid but no reply expected,
  // n > 0 if valid and n reply strings expected:
  virtual int validCmd(const string& c);

  // override these virtuals:
  // device i/o behavior
  //virtual string prefix();
  virtual string terminator();
  virtual UFTermServ* connect(const string& host= "", int port= 0);
  virtual UFStrings* status(UFDeviceAgent* da);
  virtual UFStrings* statusFITS(UFDeviceAgent* da);

  // Symbol Barcode command packet funcs:
  // init a host packet buff:
  int hostPacket(unsigned char opcode, unsigned char* p, const string& cmddata= "");
  int sendPacket(unsigned char opcode, const string& cmddata= "");
  string recvPacket(float timeOut= 0.5, int tryCnt= 3);

  inline int startScan() { return sendPacket(0xe4); }
  inline int abortScan() { return sendPacket(0xe5); }
  inline int beep() { return sendPacket(0xe6); }

  static vector< string > _circMOSBarCode; // 2
  static vector< string > _plateMOSBarCode; // 9

  static int setBarc(const string& postsad, const string& current);

};

#endif // __UFSBConfig_h__

#if !defined(__ufdhsqlook_cc__)
#define __ufdhsqlook_cc__ "$Name:  $ $Id: ufdhsget.cc 14 2008-06-11 01:49:45Z hon $";

#include "UFRuntime.h"
#include "UFSocket.h"
#include "UFDHSwrap.h"

#define __UFMAIN__
#include "UFSAODs9.h"

// DHS data server name at gemini south summit:
static string _dataserver = "dataServerSS";

int main(int argc, char** argv, char** envp) {
  UFRuntime::Argv args(argc, argv);
  bool _verbose= false;
  string arg = UFRuntime::argVal("-v", args);
  if( arg == "true" ) {
    _verbose = true;
    UFDHSwrap::_verbose = true;
  }

  bool _ds9= false;
  arg = UFRuntime::argVal("-ds9", args);
  if( arg == "true" ) 
    _ds9 = true;

  bool _save= false;
  arg = UFRuntime::argVal("-save", args);
  if( arg == "true" ) 
    _save = true;

  string _name = "dhstrecs1";
  arg = UFRuntime::argVal("-name", args);
  if( arg != "false" && arg != "true" )
    _name = arg;

  bool _qlook= false;
  arg = UFRuntime::argVal("-quick", args);
  if( arg == "true" ) {
    _qlook = true;
  }
  else if( arg != "false" ) {
    _qlook = true;
    _name = arg;
  }

  arg = UFRuntime::argVal("-q", args);
  if( arg == "true" ) {
    _qlook = true;
  }
  else if( arg != "false" ) {
    _qlook = true;
    _name = arg;
  }

  int _w= 320;
  arg = UFRuntime::argVal("-w", args);
  if( arg != "false" && arg != "true" )
    _w = atoi(arg.c_str());

  int _h= 240;
  arg = UFRuntime::argVal("-h", args);
  if( arg != "false" && arg != "true" )
    _h = atoi(arg.c_str());

  int _trycnt= 1;
  arg = UFRuntime::argVal("-try", args);
  if( arg != "false" && arg != "true" )
    _trycnt = atoi(arg.c_str());

  // connect to the dhs:
  arg = UFRuntime::argVal("-ns", args);
  if( arg != "true" && arg != "false" )
    _dataserver = "dataServerNS";

  arg = UFRuntime::argVal("-nb", args);
  if( arg != "true" && arg != "false" )
    _dataserver = "dataServerNB";

  string clhost = UFRuntime::hostname();
  string clhostIP = UFSocket::ipAddrOf(clhost);
  string _dhshost = clhostIP;
  arg = UFRuntime::argVal("-dhs", args);
  if( arg != "false" && arg != "true" ) {
    //_dhshost = arg;
    _dhshost = UFSocket::ipAddrOf(arg);
    if( arg.find("kepler") != string::npos ) // force dataserver name:
      _dataserver = "dataServerNB";
    if( arg.find("trifid") != string::npos ) // force dataserver name:
      _dataserver = "dataServerNS";
  }
  if( _dhshost == clhostIP ) {
    if( clhost.find("kepler") != string::npos ) // force dataserver name:
      _dataserver = "dataServerNB";
    if( clhost.find("trifid") != string::npos ) // force dataserver name:
      _dataserver = "dataServerNS";
  }

  UFDHSwrap dhs(_name, _w, _h);
  DHS_STATUS dstat = dhs.open(_dhshost, _dataserver);
  if( dstat != DHS_S_SUCCESS )
    return -1;

  int size= 0;
  unsigned char* fits = 0;

  if( _qlook ) {
    int cnt= 0;
    while( true ) {
      ++cnt;
      fits = dhs.getData(_name, size, _trycnt);
      if( fits == 0 || size == 0 ) {
        clog<<"ufdhsget::qlook> get failed, name: "<<_name<<endl;
	return -1;
      }
      if( _ds9 ) {
        int stat = UFSAODs9::display(fits, size, 1 + (cnt % 4));
        if( stat < size )
          clog<<"ufdhsget::qlook> Ds9 not up?"<<endl;
      }
      if( _save ) {
	strstream s;
	s<<_name<<"-"<<cnt<<".fits"<<ends;
	string filenm = s.str(); delete s.str();
        UFPosixRuntime::MMapFile mf = UFPosixRuntime::mmapFileCreate(_name, size);
        if( mf.fd < 0 ) {
          clog<<"unable to create mmap file: "<<_name<<endl;
          return -1;
        }
        ::memcpy(mf.buf, fits, size);
        int stat = UFPosixRuntime::mmapFileClose(mf);  // why does this take so long?
        if( stat < 0 )
          clog<<"unable to close mmap file:  "<<_name<<endl;
      }
      else {
	::sleep(2);
      }
    }
    dhs.close();
    return 0;
  } 

  fits = dhs.getData(_name, size, _trycnt);
  dhs.close();

  if( fits == 0 || size == 0 ) {
    clog<<"ufdhsget::get> failed, name: "<<_name<<endl;
    return -1;
  }

  _name += ".fits";
  UFPosixRuntime::MMapFile mf = UFPosixRuntime::mmapFileCreate(_name, size);
  if( mf.fd < 0 ) {
    clog<<"unable to create mmap file: "<<_name<<endl;
    return -1;
  }
  ::memcpy(mf.buf, fits, size);
  int stat = UFPosixRuntime::mmapFileClose(mf);  // why does this take so long?
  if( stat < 0 )
    clog<<"unable to close mmap file:  "<<_name<<endl;

  clog<<"ufdhsget> all done, fits file size: "<<size<<", name= "<<_name<<endl;
  return stat;
}

#endif // __ufdhsqlook_cc__

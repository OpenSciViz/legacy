#if !defined(__UFDHSFlam_cc__)
#define __UFDHSFlam_cc__ "$Name:  $ $Id: UFDHSFlam.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFDHSFlam_cc__;

#include "UFDHSFlam.h"
__UFDHSFlam_H__(__UFDHSFlam_cc);

#include "UFRuntime.h"
#include "UFGemDeviceAgent.h"

// statics/globals:
string UFDHSFlam::_agenthost;
string UFDHSFlam::_frmhost;
UFFITSClient* UFDHSFlam::_ufits = 0;
UFSocket::ConnectTable UFDHSFlam::_connections;
bool UFDHSFlam::_observatory= false;
float UFDHSFlam::_timeout = 100; // max nod settle time 100 sec.
float UFDHSFlam::_fitstimeout = 1.0; // FITS fetch timeout
bool UFDHSFlam::_qlook = false;
int UFDHSFlam::_archvThrdCnt= 0;

// mutex to deal with thread-safety here and perhaps in stdlib
static pthread_mutex_t _mutex;

// ctor should alloc. instrument spec. qlook streams
UFDHSFlam::UFDHSFlam(int w, int h) : UFDHSwrap("Flam", w, h) {
  ::pthread_mutex_init(&_mutex, 0);
  UFDHSwrap::_archvDataLabel = "";
  UFDHSwrap::_finalKeyExt = "2";
  // q-look dhs streams & buffers need only be created once:
  UFDHSwrap::_QlookNames.clear(); UFDHSwrap::_QlookStrms.clear();
  //clog<<"UFDHSFlam> set qlook stream names"<<endl;
  vector < string > qlnames;
  int qcnt = UFFlamObsConf::getBufNames(qlnames);
  if( _verbose )
    clog<<"UFDHSFlam> setting qlook stream names, cnt= "<<qcnt<<endl;
  UFDHSwrap::setQlNames(qlnames);
}

DHS_STATUS UFDHSFlam::open(const string& serverHost, const string& service) {
  if( UFDHSwrap::open(serverHost, service) != DHS_S_SUCCESS )
    return _status;

  //clog<<"UFDHSFlam::open> alloc qlook streams: "<<UFDHSwrap::_QlookNames.size()<<endl;
  // allocate one time -- reuse data buffer
  
  for( size_t i = 0; i < _QlookNames.size(); ++i ) {
    //clog<<"UFDHSwrap> alloc _QlookStrms: "<<_QlookNames[i]<<endl;
    UFDHSwrap::Stream* qls = new UFDHSwrap::Stream(_QlookNames[i]);
    UFDHSwrap::_QlookStrms[_QlookNames[i]] = qls;
    qls->_dataLabel = ""; // force generation of new dataleabel for each qlstream
    UFDHSwrap::newDataset(qls->_dataLabel, qls->_dataSet, (const string*) &(qls->_name)); 
    qls->_pDhsData = UFDHSwrap::newFrame(qls->_dataLabel, qls->_dataSet, qls->_dataFrame, 0, 1, &_QlookNames[i]);
    if( _verbose )
      clog<<"UFDHSwrap> allocated qlstrm: "<<qls->_name<<", with dataLabel: "<<qls->_dataLabel<<endl;
  }
 
  return _status;
}

UFDHSFlam::~UFDHSFlam() {} // don't really care about de-alloc of streams here

UFDHSFlam::ObsDataQue::ObsDataQue(UFDHSFlam* dhs, UFFlamObsConf* obscfg) :_dhs(dhs), _obscfg(obscfg) {
  ::pthread_mutex_init(&_quemutex, 0);
}

UFDHSFlam::ObsDataQue::~ObsDataQue() { 
  delete _obscfg; _obscfg = 0;
  for( size_t i= 0; i < size(); ++i ) {
    delete (*this)[i];
  }
  clear();
}

//int UFDHSFlam::ObsDataQue::push(UFInts* data) {
int UFDHSFlam::ObsDataQue::push(UFProtocol* data) {
  //clog<<"UFDHSFlam::ObsDataQue::push> waiting on mutex lock to queue new image... "<<endl;
  //int stat = ::pthread_mutex_trylock( &_quemutex );
  int stat = ::pthread_mutex_lock( &_quemutex );
  if( stat == 0 ) { // succeeded
    push_back(data);
    stat = (int) size();
    if( _verbose )
      clog<<"UFDHSFlam::ObsDataQue::push> queued new data, qlength: "<<stat<<endl;
    ::pthread_mutex_unlock( &_quemutex );
  }
  else{
    clog<<"UFDHSFlam::ObsDataQue::push> (trylock) failed to queue new data."<<endl;
    return -1;
  }

  return stat;
}

//UFInts* UFDHSFlam::ObsDataQue::pop(int& length) {
UFProtocol* UFDHSFlam::ObsDataQue::pop(int& length) {
  length= -1;
  UFProtocol* data= 0;
  //if( _verbose )
  //  clog<<"UFDHSFlam::ObsDataQue::pop> waiting on mutex lock to de-queue image... "<<endl;
  bool done= false, dots= true;
  float slp= 0.5;
  int cnt = (int)::ceil(_timeout / slp), cnttry = cnt;
  do {
    int stat = ::pthread_mutex_lock( &_quemutex );
    if( stat == 0 ) { // succeeded
      if( size() <= 0 ) {
        //if( _verbose )
          //clog<<"UFDHSFlam::ObsDataQue::pop> dataque empty, telescope nodding and/or MCE4 paused..."<<endl;
        if( dots ) clog<<" . ";
        ::pthread_mutex_unlock( &_quemutex );
        UFPosixRuntime::sleep(0.5); // want to emulate blocking when dataque is empty
        continue;
      }
      else if( dots ) 
	clog<<endl;
    }
    else {
      clog<<"UFDHSFlam::ObsDataQue::pop> (mutex unlock) failed to de-queue frame."<<endl;
      return 0;
    }
    data = (*this)[0]; // if final frame has been processed this will be null
    pop_front();
    length = (int) size();
    if( data != 0 && _verbose )
      clog<<"UFDHSFlam::ObsDataQue::pop> de-queued frame: "<<data->name()
          <<"seq cnt: "<<data->seqCnt()<<", seqTot: "<<data->seqTot()<<", qlength: "<<length<<endl;
    else if( _verbose )
      clog<<"UFDHSFlam::ObsDataQue::pop> de-queued null frame, qlength (should be 0): "<<length<<endl;

    ::pthread_mutex_unlock( &_quemutex );
    done = true;
  } while( --cnttry >= 0 && !done );

  if( cnttry < 0 && !done )
    clog<<"UFDHSFlam::ObsDataQue::pop> no data after "<<slp*cnt<<" sec..."<<endl;

  return data;
}

UFDHSFlam::ArchvPutData::ArchvPutData(UFDHSFlam* dhs, const vector< UFInts* >& datavec,
				       const string& label, DHS_BD_DATASET dataset, int* pDhsData, int imgidx, bool final) :
  _dhs(dhs), _label(label), _dataset(dataset), _pDhsData(pDhsData), _imgidx(imgidx), _final(final) {
  for( size_t i = 0; i < datavec.size(); ++i )
    push_back(datavec[i]);
}

UFDHSFlam::ArchvPutData::~ArchvPutData() {
  // and free all the data
  size_t nim = size();
  //clog<<"~ArchvPutData()> free datablock & dataset for datalabel; "<<_label<<", imgagecnt: "<<nim<<endl;

  _dhs->freeDataset(_dataset); // dataset gets freed after put of extension frame block
  UFInts *img= 0, *previmg= 0;
  for( size_t i = 0; i < nim; ++i ) {
    previmg = img;
    img = (*this)[i];
    if( img != previmg ) // ensure that duplicated pointers get deleted only once
      delete img;
  }
};


DHS_STATUS UFDHSFlam::setAttributes(const UFStrings& fitsHdr, DHS_AV_LIST& av, int hdrtyp) {
  vector< const string* > fitsvec;
  int cnt = fitsHdr.elements();
  //clog<<"UFDHSFlam::setAttributes> cnt: "<<cnt<<endl;
  for( int i = 0; i < cnt; ++i ) {
    const string* card =  fitsHdr.stringAt(i);
    fitsvec.push_back( card );
    //clog<<"UFDHSFlam::setAttributes> card: "<<*card<<endl;
  }
  return UFDHSwrap::setAttributes( fitsvec, av, hdrtyp );
} // setAttributes


DHS_STATUS UFDHSFlam::finalDataset(const UFStrings& fitsHdr, string& dataLabel,
				   DHS_BD_DATASET& dataSet, const string* quicklook) {
  _status = UFDHSwrap::newDataset(dataLabel, dataSet, quicklook);
  if( !quicklook ) {
    setAttributes(fitsHdr, dataSet, UFDHSwrap::All ); // set all attrib.
    //setAttributes(fitsHdr, dataSet, UFDHSwrap::Final); // set final attrib.
  }
  return _status;
} // finalDataset with final attributes

DHS_STATUS UFDHSFlam::newDataset(const UFStrings& fitsHdr, string& dataLabel,
				  DHS_BD_DATASET& dataSet, const string* quicklook) {
  _status = DHS_S_SUCCESS;

  UFDHSwrap::newDataset(dataLabel, dataSet, quicklook);
  if( !quicklook ) {
    setAttributes(fitsHdr, dataSet, UFDHSwrap::Initial);
  }
  return _status;
} // newDataset with initial attributes

// create new frame for new dataset, setting all initial and/or final value attributes,
// index & total are compared to decide which attributes to set; this seems
// to be the only way it works (the server seems to like setting the instrument
// attributes only after at least one frame is allocated into the dataset?). 
int* UFDHSFlam::newFrame(const UFStrings& fitsPrmHdr, const UFStrings& fitsExtHdr, string& dataLabel, 
	                  DHS_BD_DATASET& dataSet, DHS_BD_FRAME& dataFrame,
			  int index, int total, const string* quicklook) {
  vector< const string* > prmfitsvec;
  int cnt = fitsPrmHdr.elements();
  for( int i = 0; i < cnt; ++i )
    prmfitsvec.push_back( fitsPrmHdr.stringAt(i) );

  vector< const string* > extfitsvec;
  cnt = fitsExtHdr.elements();
  for( int i = 0; i < cnt; ++i )
    extfitsvec.push_back( fitsExtHdr.stringAt(i) );

  map< string, int > extradims; // 1
  string frmmode = "F2SciMode";
  if( total > 1 ) frmmode = "F2EngMode";
  extradims.insert(pair<string, int>(frmmode, total)); 
  return UFDHSwrap::newFrame(prmfitsvec, extfitsvec, dataLabel, dataSet, dataFrame,
                             extradims, index, total, quicklook);
}

// for higher dimensional frames: (SciMode only?)
int* UFDHSFlam::newFrame(const UFStrings& fitsPrmHdr, const UFStrings& fitsExtHdr, string& dataLabel, 
			 DHS_BD_DATASET& dataSet, DHS_BD_FRAME& dataFrame,
			 int extcnt, // naxis3
		         int index, int total, const string* quicklook) {
  vector< const string* > prmfitsvec;
  int cnt = fitsPrmHdr.elements();
  for( int i = 0; i < cnt; ++i )
    prmfitsvec.push_back( fitsPrmHdr.stringAt(i) );

  vector< const string* > extfitsvec;
  cnt = fitsExtHdr.elements();
  for( int i = 0; i < cnt; ++i )
    extfitsvec.push_back( fitsExtHdr.stringAt(i) );

  map< string, int > extradims; extradims.insert(pair<string, int>("Extensions", extcnt)); 

  return UFDHSwrap::newFrame(prmfitsvec, extfitsvec, dataLabel, dataSet, dataFrame,
                             extradims, index, total, quicklook);
}


// create new frame for new dataset, setting all initial and/or final value attributes,
// index & total are compared to decide which attributes to set; this seems
// to be the only way it works (the server seems to like setting the instrument
// attributes only after at least one frame is allocated into the dataset?). 
int* UFDHSFlam::newFrame(const UFStrings& fitsExtHdr, string& dataLabel, 
	                  DHS_BD_DATASET& dataSet, DHS_BD_FRAME& dataFrame,
			  int index, int total, const string* quicklook) {
  vector< const string* > fitsvec;
  int cnt = fitsExtHdr.elements();
  for( int i = 0; i < cnt; ++i )
    fitsvec.push_back( fitsExtHdr.stringAt(i) );

  map< string, int > extradims; // none
  return UFDHSwrap::newFrame(fitsvec, dataLabel, dataSet, dataFrame, extradims, index, total, quicklook);
}

// for higher dimensional frames:
int* UFDHSFlam::newFrame(const UFStrings& fitsExtHdr, string& dataLabel, 
			  DHS_BD_DATASET& dataSet, DHS_BD_FRAME& dataFrame,
			  int extcnt, // naxis3
			  int index, int total,
			  const string* quicklook) {
  vector< const string* > fitsvec;
  int cnt = fitsExtHdr.elements();
  for( int i = 0; i < cnt; ++i )
    fitsvec.push_back( fitsExtHdr.stringAt(i) );

  map< string, int > extradims; extradims.insert(pair<string, int>("Extensions", extcnt));

  return UFDHSwrap::newFrame(fitsvec, dataLabel, dataSet, dataFrame, extradims, index, total, quicklook);
}

// for higher dimensional frames, no extension header:
int* UFDHSFlam::newFrame(string& dataLabel, 
			  DHS_BD_DATASET& dataSet, DHS_BD_FRAME& dataFrame,
			  int extcnt, // naxis3
			  int index, int total,
			  const string* quicklook) {

  map< string, int > extradims; extradims.insert(pair<string, int>("Extenstions", extcnt));
  return UFDHSwrap::newFrame(dataLabel, dataSet, dataFrame, extradims, index, total, quicklook);
}

// decode header provided as UFStrings
int UFDHSFlam::decodeFITS(const UFStrings& hdr,
			  map< string, string >& svals,
			  map< string, int >& ivals,
			  map< string, float >& fvals,
			  map< string, string >& comments) {
  int elem = hdr.elements();
  for( int i= 0; i < elem; ++i ) 
    UFDHSwrap::decodeFITS(hdr[i], svals, ivals, fvals, comments); // default here should be to append

  return (int)(svals.size()+ivals.size()+fvals.size());
} // decode FITS from UFStrings

// decode (OSCIR type) initial header provided as a UFStrings
int UFDHSFlam::decodeInitialFITS(const UFStrings& hdr,
				 map< string, string >& svals,
				 map< string, int >& ivals,
				 map< string, float >& fvals,
				 map< string, string>& comments)  {
  vector< const string* > fitsvec;
  int cnt = hdr.elements();
  for( int i = 0; i < cnt; ++i )
    fitsvec.push_back( hdr.stringAt(i) );

  return UFDHSwrap::decodeInitialFITS(fitsvec, svals, ivals, fvals, comments);
} // decode (OSCIR type) primary header initial attributes from UFStrings

// decode (OSCIR type) final header provided as a UFStrings
int UFDHSFlam::decodeFinalFITS(const UFStrings& hdr,
			       map< string, string >& svals,
			       map< string, int >& ivals,
			       map< string, float >& fvals,
			       map< string, string>& comments)  {
  vector< const string* > fitsvec;
  int cnt = hdr.elements();
  for( int i = 0; i < cnt; ++i )
    fitsvec.push_back( hdr.stringAt(i) );

  return UFDHSwrap::decodeFinalFITS(fitsvec, svals, ivals, fvals, comments);
} // decode (OSCIR type) primary header finaal attributes from UFStrings

// set pDhsData with data values and send to dhs
DHS_STATUS UFDHSFlam::putArchiveFrame(const UFInts& data, const string& dataLabel,
                	               DHS_BD_DATASET& dataSet, int* pDhsData,
			               int idx, bool final) {
  clog<<"UFDHSFlam::putArchiveFrame> UFInts numVals: "<<data.numVals()<<", labal: "<<dataLabel<<endl;
  return UFDHSwrap::putArchiveFrame((int*)data.valData(), data.numVals(), dataLabel, dataSet, pDhsData, idx, final);
} // putArchiveFrame

// set pDhsData with data vec. values and send to dhs
DHS_STATUS UFDHSFlam::putArchiveFrame(const vector< UFInts* >& data, const string& dataLabel,
                	               DHS_BD_DATASET& dataSet, int* pDhsData,
			               int idx, bool final) {
  if( data.size() == 0 ) {
    clog<<"UFDHSFlam::putArchiveFrame> no data? "<<endl;
    return DHS_E_NULLVALUE;
  }
  if( data.size() == 1 ) {
    clog<<"UFDHSFlam::putArchiveFrame> single frame... "<<endl;
    UFInts& ufi = *(data[0]);
    return putArchiveFrame(ufi, dataLabel, dataSet, pDhsData, idx, final);
  }
  vector< int* > datavec; int elem=0;
  for( size_t i = 0; i < data.size(); ++i ) {
    UFInts* ufi = data[i]; elem = ufi->elements();
    if( ufi && ufi->valData() )
      datavec.push_back((int*) ufi->valData() );
  }
  return UFDHSwrap::putArchiveFrame(datavec, elem, dataLabel, dataSet, pDhsData, idx, final);
} // putArchiveFrame

// unit test of qlook(s) (only) puts:
// set pDhsData with data values and send to dhs
/*
DHS_STATUS UFDHSFlam::putQlookFrame(const UFInts& data, const string& dataLabel,
				     DHS_BD_DATASET& dataSet, int* pDhsData,
            		             int idx, bool final) {
  int npix = width() * height();
  int* idata = (int*) data.valData(); int elem = data.numVals();
  if( idata == 0 || elem <= 0) {
    clog<<"UFDHSFlam::putQlookFrame> null data pointer?, elem: "<<elem<<"buffnam: "<<data.name()<<endl;
    return DHS_E_NULLVALUE;
  }
  if( elem > npix ) {
    clog<<"UFDHSFlam::putQlookFrame> dubious elem: "<<elem<<"buffnam: "<<data.name()<<endl;
    return DHS_E_NULLVALUE;
  }
  return UFDHSwrap::putQlookFrame(idata, elem, dataLabel, dataSet, pDhsData, idx, final);
} // putQlookFrame

DHS_STATUS UFDHSFlam::putQlookFrame(const UFInts& data, const vector< int >& offsets,
			             const string& dataLabel,
				     DHS_BD_DATASET& dataset, int* pDhsData,
				     int idx, bool final) {
  // if there are multiple offsets assume they indicate a set of images 
  // (chopset of nodset) that are all intended for the same q-look stream
  return UFDHSwrap::putQlookFrame((const int*) data.valData(), data.numVals(), offsets,
				   dataLabel, dataset, pDhsData, idx, final); 
}

void UFDHSFlam::putQlookStreams(UFDHSFlam& dhs) {
  // connect to the frame server:
  UFFrameClient ufrm(UFDHSFlam::_frmhost, 52001, dhs.width(), dhs.height());
  int fc = ufrm.connect(UFDHSFlam::_frmhost, 52001);
  if( fc <= 0 )
    clog<<"UFDHSFlam::putQlookStreams> unable to connect to Frame Server"<<endl;
  else 
    clog<<"UFDHSFlam::putQlookStreams> connected to Frame Server"<<endl;

  DHS_BD_DATASET dataSet;
  DHS_BD_FRAME dataFrame;
  map< string, string > dataLabels; // separate dataset labels for each qlstream
  map< string, int > idx; // count the frames sent to each stream
  int nstrm = _QlookNames.size();
  string key;
  for( int i = 0; i < nstrm; ++i ) {
    key = _QlookNames[i];
    dataLabels[key] = ""; // force datalabel initialization
    idx[key] = 0; // keep track of frmcnt for each qlstream
  }
  // each quicklook stream should reuse the same frame-data-buffer pDhsData
  boolean done= false;
  int frmcnt= 0, frmtotal= 0;
  vector< string > qlbufnames;
  while( !done ) {
    int bufcnt = ufrm.getUpdatedQlNames("trecs", qlbufnames, frmcnt, frmtotal);
    for( int i = 0; i < bufcnt; ++i ) {
      key = qlbufnames[i];
      //string datalabel = dataLabels[key];
      string datalabel; // force generation of new datalabel for each qlstream
      UFDHSwrap::newDataset(datalabel, dataSet, &key); 
      dataLabels[key] = datalabel;
      int* pDhsData = UFDHSwrap::newFrame(datalabel, dataSet, dataFrame, frmcnt, frmtotal, &key); 
      if( pDhsData == 0 ) {
        clog<<"UFDHSFlam::putQlookStreams> failed to get frame allocation from dhs runtime."<<endl;
      }
      else {
        UFInts* data= 0;
        // return the the requested buffer frame and (?) current frame count:
        data = ufrm.fetchFrame(key, frmcnt, frmtotal);
        if( data == 0 ) {
          clog<<"ufdhsput> failed to get image data from frame service."<<endl;
          //break;
        }
        else {
	  int qlidx = idx[key];
	  dhs.putQlookFrame(*data, datalabel, dataSet, pDhsData, qlidx);
          idx[key] = ++qlidx;
          delete data;
        }
      }
    } // for bufcnt
    //run forever...
    if( frmcnt >= frmtotal ) {
      done = true;
    }
    else {
      UFPosixRuntime::sleep(0.1);
      //UFPosixRuntime::yield();
    }
  } // while !done
  return;
} // putQlookStreams

// allow this to be run in its own thread:
void* UFDHSFlam::_putQlookStreams(void* p) {
  if( p == 0 )
    return p;

  UFDHSFlam* dhs = static_cast< UFDHSFlam* > (p);
  dhs->putQlookStreams(*dhs);

  return p;
}

void UFDHSFlam::putArchiveStream(UFDHSFlam& dhs) {
  // connect to the frame server:
  int w =  dhs.width(), h = dhs.height(); 
  UFFrameClient ufrm(UFDHSFlam::_frmhost, 52001, w, h);
  int fc = ufrm.connect(UFDHSFlam::_frmhost, 52001);
  if( fc <= 0 )
    clog<<"UFDHSFlam::putArchiveStream> unable to connect to Frame Server"<<endl;
  else 
    clog<<"UFDHSFlam::putArchiveStream> connected to Frame Server"<<endl;

  int frmcnt= 0, frmtotal= 0;
  bool done= false;
  while( !done ) {
    string readyfile, filename, prevfile;
    bool obsdone= false;
    while( !obsdone ) {
      obsdone = ufrm.getFileStoreName(readyfile, filename, prevfile, frmcnt, frmtotal);
      if( !obsdone )
        ::sleep(10);
      //clog<<"UFDHSFlam::putArchiveStream> observation frame("<<frmcnt<<" of" <<frmtotal<<endl;
    }
    //clog<<"UFDHSFlam::putArchiveStream> observation frame buffered to: "<<readyfile<<endl;
    
    // archive should use N. Hill's algorithm, re-use the dataLabel,
    // alloc. and free dataSets:
    DHS_BD_DATASET dataSet;
    DHS_BD_FRAME dataFrame;
    string dataLabel = UFDHSwrap::_archvDataLabel;
    int* pDhsData = 0; 
    UFStrings* fitsHdr= 0;
    int fd = ufrm.openFITS(readyfile, fitsHdr, w, h, frmtotal);
    if( w != dhs.width() || h != dhs.height() ) 
      clog<<"UFDHSFlam::putArchiveStream> frame dimensions mismatch: w= "<<w<<"< h= "<<h<<endl;
      
    for( int idx= 0; idx < frmtotal-1; ++idx ) {
      // get next frame
      UFInts* data = ufrm.seekFITSData(fd);
      if( data == 0 ) {
        clog<<"UFDHSFlam::putArchiveStream> failed to get image data from frame service."<<endl;
	// done = true; // run forever...
        break;
      }
      // dhs accepts attributes only after first frame is put?
      //if( idx == 0 ) { // first frame, set initial fits hdr attributes, dataLabel gets set != ""
      //  dhs.newDataset(*fitsHdr, dataLabel, dataSet);
      //  pDhsData = dhs.newFrame(*fitsHdr, dataLabel, dataSet, dataFrame, idx, frmcnt); 
      //  dhs.putArchiveFrame(*data, dataLabel, dataSet, pDhsData, idx);
      //}
      //
      if( idx == frmcnt - 1 ) { // final frame, get final fits values
        pDhsData = dhs.newFrame(*fitsHdr, dataLabel, dataSet, dataFrame, idx, frmcnt); 
        dhs.putArchiveFrame(*data, dataLabel, dataSet, pDhsData, idx, true); // finalframe == true
      }
      else { // intermediate frame, only extension fitsHdr info required.
        pDhsData = UFDHSwrap::newFrame(dataLabel, dataSet, dataFrame, idx, frmcnt); 
        dhs.putArchiveFrame(*data, dataLabel, dataSet, pDhsData, idx);
      }
      delete data;
      dhs.freeDataset(dataSet); // frees frame; but re-use dataLabel
    } // end frame for loop
    ::close(fd); // close the local store file
  } // end while 
  return;
} // putArchiveStream

// allow this to be run in its own thread:
void* UFDHSFlam::_putArchiveStream(void* p) {
  if( p == 0 )
    return p;

  UFDHSFlam* dhs = static_cast< UFDHSFlam* > (p);
  dhs->putArchiveStream(*dhs);

  return p;
}
*/

// allow putAllStreams to be run in its own thread:
void* UFDHSFlam::_putAllStreams(void* p) {
  if( p == 0 )
    return p;

  UFDHSFlam::ObsDataQue* dataque = static_cast< UFDHSFlam::ObsDataQue* > (p);
  if( _verbose )
    clog<<"UFDHSFlam::_putAllStreams> started new thread: "<<pthread_self()
        <<", datalabel: "<<dataque->datalabel()<<endl;
  UFDHSFlam& dhs = dataque->getDHS();
  dhs.putAllStreams(*dataque);

  if( _verbose )
    clog<<"UFDHSFlam::_putAllStreams> completed: "<<pthread_self()
        <<", for datalabel: "<<dataque->datalabel()<<endl;

  //delete dataque;
  return 0;
}

void UFDHSFlam::putAllStreams(UFDHSFlam::ObsDataQue& dataque) {
  /*
  if( ::pthread_mutex_lock(&_mutex) < 0 ) {
    if( errno == EINVAL ) {
      ::pthread_mutex_init(&_mutex, 0);
      if( ::pthread_mutex_lock(&_mutex) < 0 ) {
        clog<<"UFDHSFlam::putAllStreams> failed to lock mutex..."<<endl;
        return;
      }
    }
  } 
  */
  // presumably a new thread has been created for each observations (new archive dhs datalabel)
  UFFlamObsConf& obscfg = dataque.getObs();
  int nods = obscfg.nodsPerObs();
  int imgcnt = obscfg.framesPerNod(); // per dhs frame 'extension'
  int extcnt = imgcnt * (1 + nods);
  int finalcnt = extcnt;
  string datalabel = obscfg.datalabel();
  UFDHSwrap::_archvDataLabel = datalabel;
  clog<<"UFDHSFlam::putAllStreams> datalabel: \""<<datalabel<<", nods= "<<nods
      <<", imgcnt/ext= "<<imgcnt<<", extcnt= "<<extcnt<<", finalcnt= "<<finalcnt<<endl;
  if( finalcnt != obscfg.totImgCnt() ) { 
    clog<<"UFDHSFlam::putAllStreams> mismatch in total/final image cnts: "<<obscfg.totImgCnt()
        <<"/"<<finalcnt<<endl;
  }
  DHS_BD_DATASET archvdataset;
  // UFStrings* fits0Hdr =_ufits->fetchAllFITS(_connections, _observatory);
  UFStrings* fits0Hdr = 0; // don's set dataset header here, wait 'til after first frame allocation...
  if( fits0Hdr != 0 ) {
    clog<<"UFDHSFlam::putAllStreams> allocating new DataSet with initial Flam FITS elem: "<<fits0Hdr->elements()<<" DataLabel: "<< datalabel<<endl;
    // set primary header observation here (ala OCS):
    newDataset(*fits0Hdr, datalabel, archvdataset); // set primary header observation here
    delete fits0Hdr;
  }
  else {
    clog<<"UFDHSFlam::putAllStreams> allocating new DataSet without Flam FITS."<<endl;
    UFDHSwrap::newDataset(datalabel, archvdataset); // do not set primary header observation here
  }
  obscfg.relabel(datalabel); // if datalabel was not set before newDataset call, it is now, keep it in sync.
  UFDHSwrap::_archvDataLabel = datalabel;
  // always create a new archv stream:
  UFDHSwrap::Stream* archv = new UFDHSwrap::Stream(datalabel, archvdataset); // archvdataset used for first extension
  clog<<"UFDHSFlam::putAllStreams> initialized new Archive DataSet with datalabel: \""<<datalabel<<"\""<<endl;

  //::pthread_mutex_unlock(&_mutex);

  int datacnt = putObservation( *archv, dataque );

  UFDHSwrap::freeStream(archv);
  clog<<"UFDHSFlam::putAllStreams> Observation completed, total image cnt: "<<datacnt<<", datalabel: "<<datalabel<<endl;
} // putAllStreams

int UFDHSFlam::putObservation( UFDHSwrap::Stream& archv, ObsDataQue& dataque ) {
  /*
  if( ::pthread_mutex_lock(&_mutex) < 0 ) {
    clog<<"UFDHSFlam::putObservation> failed to lock mutex..."<<endl;
    return 0;
  }
  */
  int frmcnt= 0, imgIdx= 0;
  UFDHSwrap::_archvFrmCnt = 0;
  UFFlamObsConf& obscfg = dataque.getObs();
  //UFFrameConfig* frmcfg = dataque.getFrmConf();
  int nods = obscfg.nodsPerObs();
  int imgcnt = obscfg.framesPerNod();
  // F2 uses 1 image frame per dhs frame 'extension'
  int extcnt = imgcnt * (1 + nods);
  int finalcnt = extcnt;
  string datalabel = obscfg.datalabel();
  UFDHSwrap::_archvDataLabel = datalabel;
  clog<<"UFDHSFlam::putObservation> datalabel: \""<<datalabel<<", nods= "<<nods
      <<", imgcnt/ext= "<<imgcnt<<", extcnt= "<<extcnt<<", finalcnt= "<<finalcnt<<endl;

  string qnam;
  bool abort= false, stop= false;
  int qlength= 0;
  string utstart, utend;
  double airmasstart = 1.0, airmassend = 1.0;
  UFStrings *fitsHdr= 0, *fits0Hdr=0;
  vector< UFInts* > datavec; // archv nod/frame extenison buf
  map < string, UFInts* > bufs; // q-look bufs;
  UFInts* nullImg = new UFInts(archv._dataLabel, _width*_height, false);
  bool obsdiscard = false;
  bool frmdiscard = false;
  bool freeFrm= false;  // if archiving, do not free image data here, freed by archiving thread
  UFProtocol* data = 0;

  //::pthread_mutex_unlock(&_mutex);
  string obsname = obscfg.name();
  if( obsname.find("discard") != string::npos || obsname.find("Discard") != string::npos || 
      obsname.find("DISCARD") != string::npos )
    obsdiscard = true; // do not archive this obs, but perhaps use qlook...

  clog<<"UFDHSFlam::putObservation> expecting total image count: "<<finalcnt<<endl;

  while( imgIdx < finalcnt && !abort && !stop ) {
    frmdiscard = false;
    /*
    if( ::pthread_mutex_lock(&_mutex) < 0 ) {
      clog<<"UFDHSFlam::putObservation> failed to lock mutex..."<<endl;
      return 0;
    }
    */
    // deal with the image at the front of the que:
    //clog<<"UFDHSFlam::putObservation> fetch next frame from dataque..."<<endl;
    data = dataque.pop(qlength); // this should block until new data is available
    // before breaking out of the put loop, send final frame?
    if( data != 0 ) {
      string datname = data->name();
      if( datname.find("no-dhs") != string::npos || datname.find("No-DHS") != string::npos || 
          datname.find("NO-DHS") != string::npos )
        frmdiscard = true; // don't qlook this frame (actually frm.acq.server will not send in this case)

      if( data->elements() <= 0 || !data->isData() ) { // explicit stop or abort occured
        string notice = data->name();
        if( notice.find("stop") != string::npos ) {
          stop = true; data = nullImg; // send a null image to DHS?
          clog<<"UFDHSFlam::putObservation> Unexpected termination of observation: "<<notice<<endl;
        }
        if( notice.find("abort") != string::npos ) {
          abort = true; data = nullImg; // send a null image to DHS?
          clog<<"UFDHSFlam::putObservation> Unexpected termination of observation: "<<notice<<endl;
        }
      }
      if( data->isFITS() ) { // FITS Header
        clog<<"UFDHSFlam::putObservation> FITS header: "<<data->name()<<endl;
        delete fits0Hdr;
        fits0Hdr = dynamic_cast< UFStrings* > ( data );
        if( fits0Hdr == 0 )
	  clog<<"UFDHSFlam::putObservation> bad FITS header popped off dataqueue..."<<endl;
	continue; // proceed with the next 'pop' off the dataqueue
      }
      if( !data->isData() || data->elements() <= 0 ) {
        clog<<"UFDHSFlam::putObservation> unexpected: "<<data->name()<<endl;
      }
      else {
        frmcnt++;
        clog<<"UFDHSFlam::putObservation> fetched data frame: "<<frmcnt<<", qlength: "<<qlength<<endl;
      }
    }
    else if( imgIdx < finalcnt ) { // null data in queue means...
      stop = true; data = nullImg;
      clog<<"UFDHSFlam::putObservation> Unexpected termination of observation, assume STOP ..."<<endl;
    }
    else if( imgIdx >= finalcnt ) {
      // deal with final values for primary header?
      //if( _verbose )
        clog<<"UFDHSFlam::putObservation> Completion of observation..."<<endl;
      //::pthread_mutex_unlock(&_mutex);
      break;
    }

    // if observation was aborted don't bother with q-look or archive:
    if( abort ) {
      UFDHSwrap::abort(archv._dataLabel);
      continue;
    }

    // send to all relevant q-look streams & (optionally) archv...
    // use obsconfig convenience func. to evaluate all relevant q-look buffs
    // and send them off to qlook streams:
    // note that above logic may result in nullImg being sent to q-look:
    /*
    if( _qlook && !frmdiscard && data->isData() && data->elements() > 0 ) { 
      UFInts* imgdata= 0;
      UFInts* idata = dynamic_cast< UFInts* > ( data );
      int nq = obscfg.evalFlam(imgIdx, idata, bufs, frmcfg, freeFrm);
      if( _verbose ) 
        clog<<"UFDHSFlam::putObservation> imgIdx: "<<imgIdx<<", new qlook bufs: "<<nq<<endl;
      map < string, UFInts* >::iterator it = bufs.begin();
      do {
        qnam = it->first; // note this lacks "trecs:" prefix...
        imgdata = it->second;
        if( imgdata == 0 ) {
          clog<<"UFDHSFlam::putObservation> Null image data buff for dhs stream: "<<qnam<<endl;
          continue;
        }
        else if( imgdata->valData() == 0 || imgdata->numVals() == 0 ) {
          clog<<"UFDHSFlam::putObservation> No pixels in image data for dhs stream: "<<qnam<<endl;
          continue;
        }
        UFDHSwrap::Stream* qls = _QlookStrms[qnam];
        if( qls == 0 ) {
          clog<<"UFDHSFlam::putObservation> No dhs stream for: "<<qnam<<endl;
          continue;
        }
        if( _verbose )
          clog<<"UFDHSFlam::putObservation> putting qlook buf name: "<<qnam<<" "<<imgdata->name()<<" "<<imgdata->timeStamp()<<endl;
        // if( _fitsHdr) dhs.setAttributes(*fitsHdr, qls->_dataSet, UFDHSwrap::Extension);
        //putQlookFrame(*imgdata, qls->_dataLabel, qls->_dataSet, qls->_pDhsData, idx);
        putQlookFrame(*imgdata, qls->_dataLabel, qls->_dataSet, qls->_pDhsData, 0); // always idx=0 ?
        //putQlookFrame(*imgdata, qls->_dataLabel, qls->_dataSet, qls->_pDhsData, 0, true); // always final?
      } while( ++it != bufs.end() ); // qlook streams
    } // if isData()
    */
    if( frmdiscard ) {
      clog<<"UFDHSFlam::putObservation> data frame marked DISCARD, bypassed qlook of this frame: "
          <<data->name()<<" seq. cnt: "<<data->seqCnt()<<" of: "<<data->seqTot()<<endl;
    }
    if( obsdiscard ) {
	clog<<"UFDHSFlam::putObservation> observation marked DISCARD, will not archive this frame: "
            <<data->name()<<" seq. cnt: "<<data->seqCnt()<<" of: "<<data->seqTot()<<endl;
      delete data; data = 0; // memory leak?
      continue; // not archiving this observation or frame 
    }

    // if we get here, then neither the image frame nor the observation is marked for discard
    // now archive logic: insert image into nod/(fits-ext) datavec, if it's the last image
    // for this nod, send resulting 'extension frame' to dhs archive via new thread...
    if( datavec.empty() ) { // this is the first image for this nod
      utstart = data->timeStamp();
      airmasstart = _getAirmass(); // should be caget from TCS valid values: 1.0 and up
      clog<<"UFDHSFlam::putObservation> utstart: "<<utstart<<", airmass: "<<airmasstart<<endl;
    }
    // insert data image into buffer for saveset:
    if( _verbose )
      clog<<"UFDHSFlam::putObservation> inserted idx: "<<imgIdx<<" into saveset, need "<<imgcnt<<" to complete it"<<endl;
    UFInts* idata = dynamic_cast< UFInts* > ( data );
    if( idata != 0 ) {
      datavec.push_back(idata);
      clog<<"UFDHSFlam::putObservation> UFInts elem: "<<idata->elements()<<endl;
    }
    else {
      clog<<"UFDHSFlam::putObservation> null image! "<<endl;
    }

    if( stop || datavec.size() == (size_t) imgcnt && idata != 0 ) { // final image of extension
      while( stop && datavec.size() < (size_t) imgcnt ) { // is obs. stopped, this should be a null image
      // replicate this last (null) image to complete the extension
        datavec.push_back(idata);
        //if( _verbose )
          clog<<"UFDHSFlam::putObservation> stopped, inserted filler: "<<datavec.size()
	      <<" into saveset, need "<<imgcnt<<" to complete extension..."<<endl;
      }
      int* pDhsData = 0; // saveset dhs 'frame' buffer
      utend = data->timeStamp();
      airmassend = _getAirmass(); // should be caget from TCS valid values: 1.0 and up
      clog<<"UFDHSFlam::putObservation> saveset complete, utend: "<<utend<<", airmass: "<<airmassend<<endl;
      // presumably this is the last image of the savesetas, and telescope is nodding
      bool final = (imgIdx == finalcnt - 1) || stop;
      // get frame attributes (ext. header):
      fitsHdr = _extFITS(obscfg, imgIdx, utstart, utend, airmasstart, airmassend);
      if( fitsHdr != 0 ) { // extension header available
        // set ext header start values here with newFrame...
        pDhsData = newFrame(*fitsHdr, archv._dataLabel, archv._dataSet, archv._dataFrame, extcnt, imgIdx, finalcnt); 
	delete fitsHdr; fitsHdr = 0; //free it
      }
      else { // no extension header?
        pDhsData = newFrame(archv._dataLabel, archv._dataSet, archv._dataFrame,
			    extcnt, imgIdx, finalcnt); 
      }
      // set dataset attributes after frame is allocated?
      // is this the first nod (dhs frame) of the dataset?
      if( imgIdx == 0 ) { // first frame -- set fits prim. hdr initial attributes, dataLabel gets set != ""
        //if( fits0Hdr == 0 ) // never popped off dataque?
	// fits0Hdr = _ufits->fetchAllFITS(_connections, _fitstimeout, _observatory);
        if( fits0Hdr != 0 ) {
          clog<<"UFDHSFlam::putObservation> Set Initial FITS, elem: "<<fits0Hdr->elements()<<" DataLabel: "<<archv._dataLabel<<endl;
          setAttributes(*fits0Hdr, archv._dataSet, UFDHSwrap::Initial); // set for dataSet not dataFrame!
          delete fits0Hdr; fits0Hdr = 0; // free it
        }
        else {
          clog<<"UFDHSFlam::putObservation> failed to apply initial FITS info. (never popped off queue & fetchAll failed)?"<<endl;
        }
      } // first frame
      if( final ) { // final frame, set final fits values for primary header
        //if( fits0Hdr == 0 ) // never popped off dataque?
	//fits0Hdr = _ufits->fetchAllFITS(_connections, _fitstimeout, _observatory);
        if( fits0Hdr ) {
          clog<<"UFDHSFlam::putObservation> Set Final FITS, elem: "<<fits0Hdr->elements()<<" DataLabel: "<<archv._dataLabel<<endl;
          //setAttributes(*fits0Hdr, archv._dataSet, UFDHSwrap::Final); // set only final attrib. for dataSet 
          setAttributes(*fits0Hdr, archv._dataSet, UFDHSwrap::All); // or set all attrib. for dataSet
          delete fits0Hdr; fits0Hdr = 0; // free it
	}
	else {
          clog<<"UFDHSFlam::putObservation> failed to apply final FITS info. (never popped off queue & fetchAll failed)?"<<endl;
	}
      } // final frame
      // this can take a while, and takes a fairly long while for the final frame
      // so this really should have it's own thread:
      // putArchiveFrame(datavec, archv._dataLabel, archv._dataSet, pDhsData, imgIdx, final); // finalframe == true/false
      // this makes a shallow copy of datavec, and deletes the contents after the put
      // thread should also free archv._dataSet
      // this copies content of datavec into DHS allocated pDhsData
      UFDHSFlam::ArchvPutData* dataframe = new UFDHSFlam::ArchvPutData(this, datavec, archv._dataLabel, archv._dataSet, pDhsData, imgIdx, final);
      // new thread will delete dataset & dataframe
      pthread_t archvthrd = UFPosixRuntime::newThread(UFDHSFlam::_putArchvThread, (void*) dataframe);
      if( archvthrd ==  0 ) {
        clog<<"UFDHSFlam::putObservation> unable to start new thread for archv frame put ..."<<endl;
	delete dataframe; // frees all data in datavec copy, frees dataset
	break;
      }
      //clog<<"UFDHSFlam::putObservation> started new thread for dhs archive of saveset, thrdId: "<<archvthrd<<endl;
      UFPosixRuntime::sleep(0.1);
      datavec.clear();
      // allocate new dataset for existing label
      UFDHSwrap::newDataset(archv._dataLabel, archv._dataSet);
      UFDHSwrap::_archvFrmCnt++;
      //clog<<"UFDHSFlam::putObservation> archv._dataLabel: "<<archv._dataLabel<<", archvFrmCnt: "<<UFDHSwrap::_archvFrmCnt<<endl;
    } // archive frame via datavec
    //::pthread_mutex_unlock(&_mutex);
    if( !stop )  // if stopped, the imgIdx has been incremented with fill frames...
      ++imgIdx;
  } // observation completed or aborted or stopped, end of for loop

  if( abort )
    clog<<"UFDHSFlam::putObservation> Aborted datalabel: "<<archv._dataLabel<<", frmcnt: "<<frmcnt
        <<", qlength: "<<qlength<<endl;
  else if( stop )
    clog<<"UFDHSFlam::putObservation> Stopped datalabel: "<<archv._dataLabel<<", frmcnt: "<<frmcnt
        <<", qlength: "<<qlength<<endl;
  else
    clog<<"UFDHSFlam::putObservation> Completed datalabel: "<<archv._dataLabel<<", frmcnt: "<<frmcnt
        <<", qlength: "<<qlength<<endl;

  if( !abort && !stop ) delete nullImg; // did not use this

  if( freeFrm )
    delete data;
  else  // insure that freeStreams does not delete this, because putArchvThread() should...
    archv._dataSet = DHS_BD_DATASET_NULL;

  return frmcnt;
} // putObservation

// allow putArchiveFrame to be run in its own thread:
void* UFDHSFlam::_putArchvThread(void* p) {
  if( p == 0 )
    return p;

  UFDHSFlam::ArchvPutData* dataframe = static_cast< UFDHSFlam::ArchvPutData* > (p);
  string t0 = UFRuntime::currentTime(); t0 = t0.substr(0, 19);
  if( _verbose )
    clog<<"UFDHSFlam::_putArchvThread> "<<t0<<" -- started new thread: "<<pthread_self()
        <<", datalabel: "<<dataframe->_label<<endl;

  UFDHSFlam& dhs = dataframe->getDHS();

  bool mtx= true;
  int stat = ::pthread_mutex_lock( &_mutex );
  if( stat != 0 ) { // failed 
    mtx = false;
    clog<<"UFDHSFlam::_putArchvThread> (lock) failed to increment thread cnt."<<endl;
  }
  else {
    dhs._archvThrdCnt++; // keep track of total number of (active) archive put threads
    ::pthread_mutex_unlock( &_mutex );
  }

  vector< UFInts* >* datavec = static_cast< vector< UFInts* >* > (dataframe);
  dhs.putArchiveFrame(*datavec, dataframe->_label, dataframe->_dataset,
		      dataframe->_pDhsData, dataframe->_imgidx, dataframe->_final);

  string t = UFRuntime::currentTime(); t = t.substr(2, 16);
  //clog<<"UFDHSFlam::_putArchvThread> thread exiting: "<<pthread_self()<<endl;
  //if( _verbose )
    clog<<t0<<" -- "<<t<<" -- completed DHS archive put for datalabel: "<<dataframe->_label<<endl;

  // UFDHSFlam::ArchvPutData dtor should free all image buffs in extension block, and associated dataset
  delete dataframe; p = 0;

  if( mtx ) {
    stat = ::pthread_mutex_lock( &_mutex );
    if( stat == 0 ) { // succeeded
      dhs._archvThrdCnt--; // keep track of total number of archive put threads
      ::pthread_mutex_unlock( &_mutex );
    }
  }

  return p;
} // _putArchvThread

// convenience func. should be given an open dhs connection for datalabel
// and connects to agents for latest FITS attribute info.
int UFDHSFlam::initAttributes(string& label, UFDHSFlam& dhs, const string& agenthost) {
  UFFITSClient ufits(label);
  // connect to agents for tits transaction
  UFFITSClient::AgentLoc loc;
  int aidx = ufits.locateAgents(agenthost, loc);
  if( aidx <= 0 ) {
    clog<<"UFDHSConfig::initAttributes> no agents located @ "<<agenthost<<endl;
    return -1;
  }
  int ncon= 0;
  while( ncon <= 0 ) {
    ncon = ufits.connectAgents(loc, dhs._connections, -1, true);
    if( ncon <= 0 ) {
      clog<<"UFDHSConfig::initAttributes> no connections yet, sleep & retry..."<<endl;
      ufits.sleep(5.0);
    }
  }
  UFStrings* fits0Hdr = ufits.fetchAllFITS(dhs._connections, _fitstimeout, dhs._observatory);
  if( fits0Hdr == 0 ) {
    clog<<"UFDHSConfig::initAttributes> failed to get any FITS info from agents @ "<<agenthost<<endl;
    return -1;
  }
  // init the dataset's primary header
  DHS_BD_DATASET dataSet;
  DHS_STATUS stat = dhs.newDataset(*fits0Hdr, label, dataSet);
  dhs.freeDataset(dataSet);
  delete fits0Hdr;
  return (int)stat;
}

// this must use epics caget for airmass value... 
// format shoud be fits card, to be used by setAttributes...
UFStrings* UFDHSFlam::_extFITS(UFFlamObsConf& obscfg, int imgIdx, string& utstart, string& utend, double airmasstart, double airmassend) {
  UFFITSheader ufits;
  int nod = obscfg.nodIdx();
  utstart = utstart.substr(9);
  utend = utend.substr(9);
  clog<<"UFDHSFlam::_extFITS> nod: "<<nod<<", utstart: "<<utstart<<", utend: "<<utend<<", amstart: "<<airmasstart<<endl;

  ufits.add("NOD", nod, "Nod/Offset/Dither Index");
  ufits.add("UTSTART", utstart, "UT at extension start");
  ufits.add("UTEND", utend, "UT at extension end");
  ufits.add("AMSTART", airmasstart, "Airmass at start");

  UFStrings* ret = ufits.Strings();
  //if( _verbose )
    for( int i = 0; i < ret->elements(); ++i )
      clog<<"UFDHSFlam::_extFITS> "<<(*ret)[i]<<endl;

  return ret;
}

double UFDHSFlam::_getAirmass() {
  static int havecapipe = -1;
  const string epics = "tcs";
  const string airmassdb = "tcs:sad:airMassNow";
  double val = 0.0;
  string airmass = "0.0";
  if( havecapipe <= 0 )
    havecapipe = -1; //UFGemDeviceAgent::pipeToFromEpicsCAchild(epics);

  if( havecapipe > 0  )
    UFGemDeviceAgent::getEpics(epics, airmassdb, airmass);
  
  if( airmass.length() > 0 ) {
    const char* amc = airmass.c_str();
    if( isdigit(amc[0]) )
      val = atof(amc);
  }

  return val;
}

#endif // __UFDHSFlam_cc__

#if !defined(__UFFlamDHSAgent_h__)
#define __UFFlamDHSAgent_h__ "$Name:  $ $Id: UFFlamDHSAgent.h,v 0.1 2004/06/15 19:01:08 hon beta $"
#define __UFFlamDHSAgent_H__(arg) const char arg##UFFlamDHSAgent_h__rcsId[] = __UFFlamDHSAgent_h__;

#include "iostream.h"
#include "deque"
#include "map"
#include "string"
#include "vector"

#include "UFGemDeviceAgent.h"
#include "UFTermServ.h"
#include "UFDHSFlam.h"

class UFFlamDHSAgent: public UFGemDeviceAgent, public UFDHSFlam {
public:
  // instrument name should indicate nature of frame buffs.
  // trecs has 14, flamingos just 2.
  UFFlamDHSAgent(const string& name, int argc, char** argv, char** envp);
  inline virtual ~UFFlamDHSAgent() {}

  // override these UFDeviceAgent virtuals:
  // supplemental options (should call UFDeviceAgent::options() last)
  virtual void* exec(void* p);
  virtual void startup();
  virtual UFTermServ* init(); // this is dubious since there is no serial device here...
  virtual int options(string& servlist);
  virtual int query(const string& q, vector<string>& reply);

  // dhs put event loop(s):
  static void dhsThreads(UFFlamDHSAgent& dha);
  // start dhs event loop(s) in new pthread
  static void* _dhsThreads(void* p= 0);

  virtual int action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& replies);

  // main should start signal handler thread then dhs and agent::exec threads
  static int main(const string& name, int argc, char** argv, char** envp);

protected:
  // handlers
  static void _sighandler(int sig);
  //  static void _cancelhandler(void* p= 0);

  static pthread_t _dhsThrdId;
  // dhs and agent(main) threads may share addresses, if so, protect
  // them with this mutex:
  static pthread_mutex_t _theDhsMutex;
  static int _frmport, _nods;
  static string _Instrum; 
  static UFDHSFlam* _dhs; // reference to self as DHSwrap for use in agent(main) and dhs threads?
  static bool _reconnect;
  static UFSocket* _replicate;

  // generate new data label and optionally init. the dataset's primary header
  // (behave like the ocs).
  string _newdatalabel(bool ocsheader= false);
};

#endif // __UFFlamDHSAgent_h__
      

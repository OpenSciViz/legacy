#if !defined(__UFFlamDHSAgent_cc__)
#define __UFFlamDHSAgent_cc__ "$Name:  $ $Id: UFFlamDHSAgent.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFFlamDHSAgent_cc__;

#include "sys/types.h"
#include "sys/stat.h"
#include "fcntl.h"
#include "strings.h"

#include "UFFlamDHSAgent.h"
#include "UFDHSConfig.h"

// global statics
// DHS data server name at gemini south summit:
static string _dataserver = "dataServerSS";

UFSocket* UFFlamDHSAgent::_replicate= 0;
bool UFFlamDHSAgent::_reconnect= false;
//int UFFlamDHSAgent::_frmport= 52009; // ufedtd 
int UFFlamDHSAgent::_frmport= 52000; // ufacqframed
int UFFlamDHSAgent::_nods = 0;

// make instrument name available to static funcs:
string UFFlamDHSAgent::_Instrum;
pthread_mutex_t UFFlamDHSAgent::_theDhsMutex;
pthread_t UFFlamDHSAgent::_dhsThrdId= 0;
// reference to self as DHSwrap for use in agent(main) and dhs threads
UFDHSFlam* UFFlamDHSAgent::_dhs;

// ctors:
UFFlamDHSAgent::UFFlamDHSAgent(const string& name,
				 int argc, char** argv,
				 char** envp) : UFGemDeviceAgent(name, argc, argv, envp), UFDHSFlam() {
  UFPosixRuntime::_threaded = true;
  _Instrum = name;
  ::pthread_mutex_init(&_theDhsMutex, 0);
  _dhs = reinterpret_cast< UFDHSFlam* > (this);
}

// static funcs:
int UFFlamDHSAgent::main(const string& name,
			  int argc, char** argv,
			  char** envp) {
  UFFlamDHSAgent ufdhsa(name, argc, argv, envp);
  // create & start dhs thread(s), then enter agent exec (which creates sigthread on startup) 
  // and enters listen/accept/recv action/query event loop
  _dhsThrdId = UFPosixRuntime::newThread(_dhsThreads, (void*)&ufdhsa);
  UFPosixRuntime::sleep(2.0); // allow dhs threads time to startup
  // check for dhs startup errors before proceeding?
  ufdhsa.exec( (void*)&ufdhsa );
  return 0;
}

void UFFlamDHSAgent::_sighandler(int sig) {
  switch(sig) {
  case SIGTERM:
    if( UFDeviceAgent::_verbose )
      clog<<"UFFlamDHSAgent::_sighandler> (SIGTERM) sig: "<<sig<<endl;
  case SIGINT: {
    if( UFDeviceAgent::_verbose )
       clog<<"UFFlamDHSAgent::_sighandler> (SIGINT) sig: "<<sig<<endl;
    /*
    if( _dhsThrdId ) {
      ::pthread_cancel(_dhsThrdId); // cancel handler unlocks mutex if edtdma thread owns it
      _dhsThrdId = 0;
    }
    */
    UFRndRobinServ::_shutdown = true;
    UFRndRobinServ::shutdown();
  }
  default:
    if( UFDeviceAgent::_verbose )
      clog<<"UFFlamDHSAgent::_sighandler> (default) sig: "<<sig<<endl;
    UFRndRobinServ::sighandlerDefault(sig); // handle SIGPIPE, SIGCHILD
    break;
  }
}

/*
void UFFlamDHSAgent::_cancelhandler(void* p) {
  UFFlamDHSAgent* dma = static_cast< UFFlamDHSAgent* > (p);
  // always unlock mutexes on cancellation of thread
  ::pthread_mutex_unlock(&dma->_theFrmMutex);
  ::pthread_mutex_unlock(&dma->_theEdtMutex);
}
*/

// public virtual funcs:
/*
string UFFlamDHSAgent::newClient(UFSocket* clsoc) {
  string clname= "", service = name();
  if( UFDeviceAgent::_verbose ) 
    clog<<"UFFlamDHSAgent::newClient> "<<service<<" new client connection, exchange greeting..."<<endl;
  try {
    clname = greetClient(clsoc, service);  
  }
  catch(std::exception& e) {
    clog<<"UFFlamDHSAgent::newClient> stdlib exception occured in newClient accept: "<<e.what()<<endl;
  }
  catch(...) {
    clog<<"UFFlamDHSAgent::newClient> unknown exception occured in newClient accept..."<<endl;
  }
  if( clname.find("replicat") != string::npos ||
      clname.find("Replicat") != string::npos ||
      clname.find("REPLICAT") != string::npos && _replicate == 0 )
      _replicate = clsoc;

  return clname;
}
*/

void UFFlamDHSAgent::startup() {
  // start signal handler thread
  sigWaitThread(UFFlamDHSAgent::_sighandler); // create a thread devoted to all signals of interrest
  clog << "UFFlamDHSAgent::startup> established signal handler (sigwait) thread."<<endl;
  // should parse cmd-line options and start listening
  string servlist;
  int listenport = options(servlist);
  init();
  UFSocketInfo socinfo = _theServer.listen(listenport);

  clog << "UFFlamDHSAgent::startup> listening on port= " <<listenport
       <<", with server soc: "<<_theServer.description()<<endl;
  return;  
}

UFTermServ* UFFlamDHSAgent::init() {
  clog<<"UFFlamDHSAgent::init> no terminal console connection/serial device needed..."<<endl;
  return 0;
}


// this should always return the service/agent listen port #
int UFFlamDHSAgent::options(string& servlist) {
  int port = UFGemDeviceAgent::options(servlist);
  _config = new UFDHSConfig(name()); // needs to be proper device subclass
  // portescap defaults for trecs:
  _config->_tsport = -1;
  _config->_tshost = "";

  string arg = findArg("-v");
  if( arg == "true" ) {
    UFDeviceAgent::_verbose = true;
  }
  arg = findArg("-vv");
  if( arg == "true" ) {
    UFDeviceAgent::_verbose = UFPosixRuntime::_verbose = true;
  }
  arg = findArg("-port");
  if( arg != "false" && arg != "true" ) {
    port = _config->_tsport = atoi(arg.c_str()); 
  }
  arg = findArg("-tsport");
  if( arg != "false" && arg != "true" ) {
    _config->_tsport = atoi(arg.c_str()); 
  }
  arg = findArg("-nodtimeout");
  if( arg != "false" && arg != "true" ) {
    UFDHSFlam::_timeout = atoi(arg.c_str()); 
  }
  arg = findArg("-fitstimeout");
  if( arg != "false" && arg != "true" ) {
    UFDHSFlam::_fitstimeout = atoi(arg.c_str()); 
  }
  /* this is done in dhsThreads
  arg = findArg("-agent");
  if( arg != "false" && arg != "true" ) {
    _agenthost = arg;
  }
  arg = findArg("-agents");
  if( arg != "false" && arg != "true" ) {
    _agenthost = arg;
  }
  */
  if( _config->_tsport >= 0 && _config->_tsport < 50000 ) {
    port = 52000 + _config->_tsport - 7000;
  }
  else if( port < 50000 ) { // all uf services us ports > 50000
    port = 52010;
  }

  //if( threaded() )
  //clog<<"UFFlamDHSAgent::options> this service is threaded..."<<endl;



  clog<<"UFFlamDHSAgent::options> set port to FlamDHSAgent port "<<port<<endl;
  return port;
}

// the key virtual function(s) to override,
// presumably any allocated (UFStrings reply) memory is freed by the calling layer....
int UFFlamDHSAgent::action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& rep) {
  int stat = -1;
  rep = new vector< UFProtocol* >;
  vector< UFProtocol* >& replies = *rep;;
  string reply, simInstrum = name();
  bool label= false, ocslike= false;

  for( int i = 0; i < (int)act->cmd_name.size(); ++i ) {
    string cmdname = act->cmd_name[i];
    string cmdimpl = act->cmd_impl[i];
    if( UFDeviceAgent::_verbose )
      clog<<"UFFlamDHSAgent::action> "<<cmdname<<" :: "<<cmdimpl<<endl;

    int nr = _config->validCmd(cmdname, cmdimpl);
    if( nr < 0 ) {
      clog<<"UFFlamDHSAgent::action> ?Bad cmd: "<<cmdname<<" :: "<<cmdimpl<<endl;
      continue;
    }
    act->time_submitted = currentTime();
    _active = act;

    if( cmdname.find("shutdown") != string::npos || cmdname.find("Shutdown") != string::npos || 
	cmdname.find("SHUTDOWN") != string::npos ) {
      /*
      if( _dhsThrdId ) {
        ::pthread_cancel(_dhsThrdId); // cancel handler unlocks mutex if edtdma thread owns it
        _dhsThrdId = 0;
      }
      */
      UFRndRobinServ::_shutdown = true; //UFRndRobinServ::shutdown();
      reply = "Ok, shutting down: " + cmdname + "::" + cmdimpl;
      act->cmd_reply.push_back(reply);
      replies.push_back(new UFStrings("UFFlamDHSAgent::action> shutdown", &reply));
      break;
    }
    if( cmdname.find("stat") != string::npos || cmdname.find("Stat") != string::npos ||
	     cmdname.find("STAT") != string::npos ) { // get dma status cmdimpl == edt, fits/other
      if( cmdimpl.find("fits") != string::npos || cmdimpl.find("Fits") != string::npos ||
	       cmdimpl.find("FITS") != string::npos ) {
        act->cmd_reply.push_back(reply);
	replies.push_back(_config->statusFITS(this));
      }
      else {
        act->cmd_reply.push_back(reply);
        replies.push_back(_config->status(this));
      }
      break;
    }
    else if( cmdname.find("dhs") != string::npos ||
	     cmdname.find("Dhs") != string::npos ||
	     cmdname.find("DHS") != string::npos ) { // query (cmdimpl == '?') or set fits file pathname
      clog<<"UFFlamDHSAgent::action> "<<cmdname<<" :: "<<cmdimpl<<endl;
      if( cmdimpl.find("label") != string::npos || cmdimpl.find("Label") != string::npos ||
	       cmdimpl.find("LABEL") != string::npos ) {
        label = true;
        strstream s;
        s<<cmdname<<" :: "<<cmdimpl<<" OK ..."<<ends;
        reply = s.str(); delete s.str();
        act->cmd_reply.push_back(reply);
      }
      else if( cmdimpl.find("ocs") != string::npos || cmdimpl.find("Ocs") != string::npos ||
	       cmdimpl.find("OCS") != string::npos ) {
        label = true;
        ocslike = true;
        strstream s;
        s<<cmdname<<" :: "<<cmdimpl<<" OK ..."<<ends;
        reply = s.str(); delete s.str();
        act->cmd_reply.push_back(reply);
      }
    }
  } // end for loop of cmd action bundle

  if( label ) { // new dhs label requested:
    string datalabel = _newdatalabel(ocslike);
    act->cmd_reply.push_back(datalabel);
    if( datalabel == "" )
      stat = -1;
  }

  if( stat < 0 )
    act->status_cmd = "failed";
  else if( act->cmd_reply.empty() )
    act->status_cmd = "failed/rejected";
  else
    act->status_cmd = "succeeded";
  
  act->time_completed = currentTime();
  _completed.insert(UFDeviceAgent::CmdList::value_type(act->cmd_time, act));

  replies.push_back(new UFStrings(act->clientinfo, act->cmd_reply));
  return (int) replies.size();
} 

int UFFlamDHSAgent::query(const string& q, vector<string>& qreply) {
  return UFDeviceAgent::query(q, qreply);
}

string UFFlamDHSAgent::_newdatalabel(bool ocsheader) {
  string label;
  UFStrings* fits0Hdr = 0;
  if( ocsheader ) {
    if( _dhs->_ufits != 0 ) {
      fits0Hdr = _dhs->_ufits->fetchAllFITS(_dhs->_connections, UFDHSFlam::_fitstimeout, true);
      DHS_BD_DATASET dataset;
      if( fits0Hdr != 0 )
        _dhs->newDataset(*fits0Hdr, label, dataset); // if succesfull, sets new data label and inits primary header
      _dhs->freeDataset(dataset);
    }
    if( fits0Hdr == 0 ) // try alternate connection to agents 
      initAttributes(label, (UFDHSFlam&)(*this), _agenthost);
  }
  if( label == "" ) // if no ocsheader option or if ocs option failed:
    label = _dhs->newlabel();

  return label;
}

// allow dhs event loop to be run in its own (cancellable) thread:
void* UFFlamDHSAgent::_dhsThreads(void* p) {
  if( p == 0 )
    return p;

  UFFlamDHSAgent* dhsa = static_cast< UFFlamDHSAgent* > (p);
/*   
  int oldstate, oldtype;
  ::pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, &oldstate);
  ::pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, &oldtype);
// since linux & solaris "pthread.h" declare pthread_cleanup_push as a macro
// that g++ seems to have problems compiling on each, use the actual
// extern function
#if defined(LINUX)
  struct _pthread_cleanup_buffer _buffer;
  ::_pthread_cleanup_push(&_buffer, UFFlamDHSAgent::_cancelhandler, p);
#else
  _cleanup_t _cleanup_info;
  ::__pthread_cleanup_push(UFFlamDHSAgent::_cancelhandler, p,
			   (caddr_t)_getfp(), &_cleanup_info);
#endif  
*/
  // enter dhs event loop:
  dhsa->dhsThreads(*dhsa);
  // clear the ThrdId
  dhsa->_dhsThrdId = 0;

  int exitstat;
  ::pthread_exit(&exitstat);
  return p;
}

// this is just cut-pasted more or less from ufdhsput,
// really should consolidate somewhere...
void UFFlamDHSAgent::dhsThreads(UFFlamDHSAgent& dhsa) {
  UFDHSFlam::_ufits= new UFFITSClient(dhsa.name());
  UFFITSClient& ufits= *_ufits;

  string arg = dhsa.findArg("-v");
  if( arg == "true" ) { 
    UFDHSwrap::_verbose = true;
  }

  arg = dhsa.findArg("-vv");
  if( arg == "true" ) { 
    UFDHSwrap::_verbose = true;
    UFFITSheader::_verbose = true;
  }

  arg = dhsa.findArg("-nowait");
  if( arg == "true" )
    UFDHSwrap::_wait = false;

  _agenthost = UFRuntime::hostname();
  arg = dhsa.findArg("-host");
  if( arg != "true" && arg != "false" )
    _agenthost = arg;

  arg = dhsa.findArg("-agents");
  if( arg != "true" && arg != "false" )
    _agenthost = arg;

  arg = dhsa.findArg("-agent");
  if( arg != "true" && arg != "false" )
    _agenthost = arg;

  arg = dhsa.findArg("-frmport");
  if( arg != "true" && arg != "false" )
    _frmport = atoi(arg.c_str());
  else
    _frmport = 52000;

  // connect to the dhs:
  arg = ufits.findArg("-ns");
  if( arg != "true" && arg != "false" )
    _dataserver = "dataServerNS";

  arg = ufits.findArg("-nb");
  if( arg != "true" && arg != "false" )
    _dataserver = "dataServerNB";

  string clhost = UFRuntime::hostname();
  string clhostIP = UFSocket::ipAddrOf(clhost);
  string _dhshost = clhostIP;
  arg = dhsa.findArg("-dhs");
  if( arg != "false" && arg != "true" ) {
    //_dhshost = arg;
    _dhshost = UFSocket::ipAddrOf(arg);
    if( arg.find("kepler") != string::npos ) // force dataserver name:
      _dataserver = "dataServerNB";
    if( arg.find("trifid") != string::npos ) // force dataserver name:
      _dataserver = "dataServerNS";
  }
  if( _dhshost == clhostIP ) {
    if( clhost.find("kepler") != string::npos ) // force dataserver name:
      _dataserver = "dataServerNB";
    if( clhost.find("trifid") != string::npos ) // force dataserver name:
      _dataserver = "dataServerNS";
  }

  clog<<"UFFlamDHSAgent::dhsThreads> connect to DHS: "<<_dhshost<<endl;
  DHS_STATUS dstat = dhsa.open(_dhshost, _dataserver);
  if( dstat != DHS_S_SUCCESS ) {
    clog<<"UFFlamDHSAgent::dhsThreads> failed to connect to DHS."<<endl;
    return;
  }
 
  clog<<"UFFlamDHSAgent::dhsThreads> connecting to Frame Server: "<<_agenthost<<", port: "<<_frmport<<endl;
  UFFrameClient ufrm(_agenthost, _frmport, dhsa.width(), dhsa.height());
  int fc = ufrm.replConnect(_agenthost, _frmport);
  if( fc <= 0 )
    clog<<"UFFlamDHSAgent::dhsThreads> unable to connect to Frame Server: "<<_agenthost<<", port: "<<_frmport<<endl;
  else if( UFDeviceAgent::_verbose )
    clog<<"UFFlamDHSAgent::dhsThreads> connected to Frame Server: "<<_agenthost<<", port: "<<_frmport<<endl;


  // connect to agents
  UFFITSClient::AgentLoc loc;
  int aidx = ufits.locateAgents(_agenthost, loc);
  if( aidx <= 0 ) {
    clog<<"UFFlamDHSAgent::dhsThreads> no agents located"<<endl;
    return;
  }
  int ncon= 0;
  while( ncon <= 0 ) {
    ncon = ufits.connectAgents(loc, dhsa._connections, -1, true);
    if( ncon <= 0 ) {
      if( UFDHSwrap::_verbose )
        clog<<"UFFlamDHSAgent::dhsThreads> no connections yet, sleep & retry..."<<endl;
      ufits.sleep(5.0);
    }
  }

  // get current header, for start of observation
  UFStrings* _fits0Hdr = ufits.fetchAllFITS(dhsa._connections, UFDHSFlam::_fitstimeout, dhsa._observatory);
  if( _fits0Hdr == 0 ) {
    clog<<"UFFlamDHSAgent::dhsThreads> failed to get any FITS info from agents."<<endl;
    //return;
  }
  // infinit loop should create obsdataque and obsthread for each new
  // UFObdConfig that is sent either from frame service or from dc agent...
  // test one observation:
  // if no observation datalabel is provided, set to ""
  // and generate label via dhs runtime...
  int idx= 0, total=0, qlength = 0;
  UFInts* data = 0;
  UFProtocol* ufp = 0;
  UFDHSFlam::ObsDataQue* dataque = 0;
  int obsimgcnt= 0, finalcnt= -1;
  while( true ) {
    // the frame service should send a new observation configuration upon the start of a new obs.
    if( _reconnect && !ufrm.validConnection() ) {
       clog<<"UFFlamDHSAgent::dhsThreads> no frame service connection (connection closed by server?), (re)connect to Frame Server: "<<_agenthost<<", port: "<<_frmport<<endl;
      int fc = ufrm.replConnect(_agenthost, _frmport);
      if( fc <= 0 ) {
        clog<<"UFFlamDHSAgent::dhsThreads> unable to (re)connect to Frame Server: "<<_agenthost<<", port: "<<_frmport<<endl;
        sleep(2);
        continue;
      }
      else {
        _reconnect = false; // reset from sigpipe (server side closed down, or invalid socket)
        clog<<"UFFlamDHSAgent::dhsThreads> (re)connected to Frame Server: "<<_agenthost<<", port: "<<_frmport<<endl;
      }
    }
    clog<<"UFFlamDHSAgent::dhsThreads> waiting on data..."<<endl;
    ufp = ufrm.replFrame(idx, total); // recv replicated frame, incr. idx
    if( ufp == 0 ) {
      clog<<"UFFlamDHSAgent::dhsThreads> got null data object, closing connection to frame server..."<<endl;
      ufrm.close();
      _reconnect = true;
      continue;
    }
    else if( ufp->isNotice() ) { // notification (only stop/abort is currently anticipated)
      string notice = ufp->name(); 
      clog<<"UFFlamDHSAgent::dhsThreads> notice: "<<notice<<", for datalabel: "<<ufp->datalabel()<<endl;
      UFStrings::lowerCase(notice);
      if( (notice.find("stop") != string::npos ||  notice.find("abort") != string::npos) && dataque ) {
        clog<<"UFFlamDHSAgent::dhsThreads> aborting datastreams for datalabel: "<<ufp->datalabel()<<endl;
        qlength = dataque->push(0); // thread should clean up after itselef (free dataque, etc.)
        dataque = 0; // dataque thread will free que and exit on null frame...
      }
    } // notice
    else if( ufp->isObsConf() ) { // new observation, should be first protocol object received...
      _dhs->_obscfg = dynamic_cast< UFFlamObsConf* > (ufp);
      finalcnt =_dhs->_obscfg->totImgCnt();
      clog<<"UFFlamDHSAgent::dhsThreads> total/final image cnts: "<<finalcnt<<endl;
      string datalabel =_dhs->_obscfg->datalabel();
      if( datalabel.length() != 13 || datalabel.find("N2") != 0 ) {
        clog<<"UFFlamDHSAgent::dhsThreads> bogus datalabel: "<<datalabel<<", force DHS to create new one?"<<endl;
        _dhs->_obscfg->relabel(""); // force creation of new datalabel in dhs 
      }
      _nods =_dhs->_obscfg->nodsPerObs();
      datalabel =_dhs->_obscfg->datalabel();
      clog<<"UFFlamDHSAgent::dhsThreads> start new obs. with datalabel: \""
	  <<datalabel<<", nods= "<<_nods<<", expect finalcnt= "<<finalcnt<<" images for this obs."<<endl;
      if( dataque ) {
        clog<<"UFFlamDHSAgent::dhsThreads> aborted previous observation?"<<endl;
        // null data indicates termination of (prior) observation..
        qlength = dataque->push(0); // thread should clean up after itselef (free dataque, etc.)
      }
      obsimgcnt = 0;
      dataque = new UFDHSFlam::ObsDataQue(_dhs, _dhs->_obscfg); // create new dataque for obs...
      pthread_t obsthrd = UFPosixRuntime::newThread(UFDHSFlam::_putAllStreams, (void*) dataque);
      if( obsthrd ==  0 ) {
        clog<<"UFFlamDHSAgent::dhsThreads> unable to start new thread for putAllStreams ..."<<endl;
        delete dataque;
        return;
      }
      clog<<"UFFlamDHSAgent::dhsThreads> started new thread for putAllStreams ..."<<endl;
    } // obsconf
    else if( ufp->isData() ) {
      data = dynamic_cast< UFInts* > (ufp);
      ++obsimgcnt;
      if( dataque ) {
        qlength = dataque->push(data);
        clog<<"UFFlamDHSAgent::dhsThreads> inserted new image: "<<data->name()<<" into dataque idx: "<<idx<<", qlength: "<<qlength<<endl;
        if( obsimgcnt == finalcnt ) {
          dataque->push(0); // indicate final frame
          dataque = 0; // dataque thread will free que and exit on null frame...
        }
      }
      else {
        clog<<"UFFlamDHSAgent::dhsThreads> no dataque for new image (never got obsconf?): "<<data->name()<<endl;
      }
    }
    else {
      clog<<"UFFlamDHSAgent::dhsThreads> no interest in this uf protocol object type: "<<ufp->typeId()
          <<", qlength: "<<qlength<<endl;
    }
    //UFPosixRuntime::yield(); // allow dhs threads cpu time
    //clog<<"UFFlamDHSAgent::dhsThreads> sleeping "<<_sleep<<" sec. to allow dhs trehads some cpu..."<<endl;
    //UFPosixRuntime::sleep(_sleep); // allow dhs threads cpu time
  } // all observations

  clog<<"UFFlamDHSAgent::dhsThreads> all observations completed, final idx: "<<idx<<endl;
  //UFPosixRuntime::sleep(_sleep); // allow dhs threads cpu time

  // should there be any final dhswait here?
  dhsa.close();

  return;
}

// also override exec:
void* UFFlamDHSAgent::exec(void* p) {
  // an instance of this/myself (UFFlamDHSAgent*) should be provided:
  UFFlamDHSAgent* agent = static_cast<UFFlamDHSAgent*>(p); 
  clog << "UFFlamDHSAgent::exec> starting service: " << agent->name()<<endl;

  // check options, which at the minimum should indicate server port #
  startup(); // start listening

  // enter recv req.& send reply loop
  unsigned long reqtot=0;
  string newnam;
  vector <UFSocket*> clientreqs;
  UFFlamDHSAgent::MsgTable reqtbl;
  while( true ) {
    // check if the sighandler has cought sigint or sigpipe or sigchld:
    if( _shutdown ) {
      clog<<"UFFlamDHSAgent::exec> termination signal recv'd, or shutdown command..."<<endl;
      shutdown();
    }
    if( _lost_connection ) {
      clog<<"UFFlamDHSAgent::exec> lost connection signal recv'd, check which one(s) to close..."<<endl;
      int cnt = lostConnection(agent->_theServer);
      // reset the global boolean
      _lost_connection = false;
      clog<<"UFFlamDHSAgent::exec> closed "<<cnt<<" connections..."<<endl;
    }

    // any ancillary logic?
    if( _ancil ) {
      try {
        void *p = ancillary(_ancil);
        if( p == 0 ) clog<<"UFFlamDHSAgent> error occured with ancillary logic?"<<endl;
      }
      catch(std::exception& e) {
        clog<<"UFFlamDHSAgent::exec> stdlib exception occured in ancil: "<<e.what()<<endl;
      }
      catch(...) {
        clog<<"UFFlamDHSAgent::exec> unknown exception occured in ancil..."<<endl;
      }
    }
    // check for new client connection:
    string agentname = agent->name();
    UFSocket* clsoc= 0;
    //clsoc = agent->acceptOn(UFFlamDHSAgent::_theServer);
    //clsoc = agent->accept();
    try {
      //clsoc = agent->acceptOn(UFFlamDHSAgent::_theServer);
      clsoc = agent->accept();
      if( clsoc != 0 ) {
        clog<<"UFFlamDHSAgent::exec> "<< agentname<<" got new connection from: "<<clsoc->peerIP()<<endl;
        string clname = newClient(clsoc, agentname);
        if( clname != "" ) {
          (*theConnections)[clname] = clsoc; // insure the table entry exists
          //if( _verbose ) 
            clog<<"UFFlamDHSAgent::exec> new client name: "<<clname<<endl;
        }
        else {
          clog<<"UFFlamDHSAgent::exec> new client failed greeting..."<<endl;
          clsoc->close(); delete clsoc; clsoc = 0;
        }
      }
    }
    catch(std::exception& e) {
      clog<<"UFFlamDHSAgent::exec> clsoc: "<<clsoc<<", stdlib exception occured in newClient accept: "<<e.what()<<endl;
    }
    catch(...) {
      clog<<"UFFlamDHSAgent::exec>  clsoc: "<<clsoc<<", unknown exception occured in newClient accept..."<<endl;
    }

    //check which clients have submitted requests:
    int reqcnt= 0;
    clientreqs.clear();
    reqtbl.clear();
    if( agent->theConnections->size() > 0 ) { 
      reqcnt = agent->pendingReqs(clientreqs);
      if( reqcnt > 0 ) {
        clog<<"UFFlamDHSAgent::exec> process "<<reqcnt<<" new requests"
            <<", # of clients= "<<UFFlamDHSAgent::theConnections->size()
            <<", # of requests= "<<reqcnt<<endl;
        reqtot += reqcnt;
        agent->recvReqs(clientreqs); // recv & insert ufprotocols into _immed or _queued
        if( _immed.size() > 0 ) {
          try {
            agent->servImmed(); // process table, and clear it
          }
          catch(std::exception& e) {
            clog<<"UFFlamDHSAgent::exec> stdlib exception occured in servImmed: "<<e.what()<<endl;
          }
          catch(...) {
            clog<<"UFFlamDHSAgent::exec> unknown exception occured in servImmed..."<<endl;
          }
        }
      }
      else if( _queued.size() > 0 ) {
        try {
          agent->servQueued(); // process queued table, and clear it
        }
        catch(std::exception& e) {
          clog<<"UFFlamDHSAgent::exec> stdlib exception occured in servQueued: "<<e.what()<<endl;
        }
        catch(...) {
          clog<<"UFFlamDHSAgent::exec> unknown exception occured in servQueued..."<<endl;
        }
      }
      else { // no requests
        //clog<<"UFFlamDHSAgent::exec> hibernate due to lack of client requests"
        //  <<", # of clients= "<<UFFlamDHSAgent::theConnections->size()
        //  <<", # of requests= "<<reqcnt<<endl;
        agent->hibernate();
      }
    }
    else { // no clients?
      //clog<<"UFFlamDHSAgent::exec> hibernate due to lack of clients"
      //  <<", # of clients= "<<UFFlamDHSAgent::theConnections->size()
      //  <<", # of requests= "<<reqcnt<<endl;
      agent->hibernate();
    }
  } // while forever
  return p;
}

#endif // UFFlamDHSAgent

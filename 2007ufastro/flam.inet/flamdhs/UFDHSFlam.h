#if !defined(__UFDHSFlam_h__)
#define __UFDHSFlam_h__ "$Name:  $ $Id: UFDHSFlam.h,v 0.3 2005/04/27 19:33:34 hon Exp $"
#define __UFDHSFlam_H__(arg) const char arg##DHSFlam_h__rcsId[] = __UFDHSFlam_h__;

// UFlib:
#include "UFDHSwrap.h"
#include "UFRuntime.h"
#include "UFInts.h"
#include "UFStrings.h"
#include "UFFITSClient.h"
#include "UFFrameClient.h"
#include "UFFlamObsConf.h"

// stdc++
#include "deque"

class UFDHSFlam : public UFDHSwrap {
public:
  UFDHSFlam(int w= 2048, int h= 2048);
  ~UFDHSFlam();

  // override this to alloc. specific qlook streams
  virtual DHS_STATUS open(const string& serverHost, const string& service= "dataServerSS");
 
  // each observation thread should have its own dataque
  //struct ObsDataQue : public deque< UFInts* > {
  struct ObsDataQue : public deque< UFProtocol* > {
    pthread_mutex_t _quemutex;
    UFDHSFlam* _dhs; UFFlamObsConf* _obscfg;
    ObsDataQue(UFDHSFlam* dhs, UFFlamObsConf* obscfg); 
    ~ObsDataQue();
    inline UFDHSFlam& getDHS() const { return *_dhs; }
    inline UFFlamObsConf& getObs() const { return *_obscfg; }
    inline UFFlamObsConf& setObs(UFFlamObsConf* oc){ if( oc!=0 ) _obscfg= oc; return *_obscfg; }

    inline string datalabel() const {
      if( _dhs == 0 || _obscfg == 0 ) { clog<<"Bad ObsDataQue, abort!"<<endl; ::exit(-1); }
      return _obscfg->datalabel();
    }
    //int push(UFInts* ufi);
    int push(UFProtocol* ufp);
    //UFInts* pop(int& length);
    UFProtocol* pop(int& length);
    private: inline ObsDataQue() { clog<<"ObsDataQue() default ctor ??"<<endl; }
  };

  // optional object for individual extension frame put archv thread:
  struct ArchvPutData : public vector< UFInts* > {
    UFDHSFlam* _dhs; string _label; DHS_BD_DATASET _dataset; int* _pDhsData; int _imgidx; bool _final;
    ArchvPutData(UFDHSFlam* dhs, const vector< UFInts* >& datavec, const string& label, DHS_BD_DATASET dataset,
		 int* pDhsData, int imgidx, bool final);
    ~ArchvPutData();
    inline UFDHSFlam& getDHS() const { return *_dhs; }
    private: inline ArchvPutData() {};
  };

  // scan an 'internal' fitsHdr string array, assume that (most) keys that end in '2' are
  // final frame values, all others are initial values -- kinda kludgy OSCIR/Flam
  static DHS_STATUS setAttributes(const UFStrings& fitsHdr, DHS_AV_LIST& av,
			          int hdrtyp= UFDHSFlam::Initial);

  // create a (new) dataset, setting all initial value attributes,
  // this does not really seem to work?
  DHS_STATUS newDataset(const UFStrings& fitsHdr, string& dataLabel,
			DHS_BD_DATASET& dataSet, const string* quicklook= 0);

  // create final frame/dataset, setting all final value attributes, this also
  // seems dubious 
  DHS_STATUS finalDataset(const UFStrings& fitsHdr, string& dataLabel,
			  DHS_BD_DATASET& dataSet, const string* quicklook= 0);

  // create new frame setting all initial and/or final primary header attributes,
  // and current extension header attributes.
  // index & total are compared to decide which attributes to set; this seems
  // to be the only way it works (the server seems to like setting the instrument
  // attributes only after at least one frame is allocated into the dataset?). 
  int* newFrame(const UFStrings& fitsPrmHdr, const UFStrings& fitsExtHdr, string& dataLabel,
		DHS_BD_DATASET& dataSet, DHS_BD_FRAME& dataFrame,
		int index= 0, int total= 1, const string* quicklook= 0);
  int* newFrame(const UFStrings& fitsExtHdr, string& dataLabel,
		DHS_BD_DATASET& dataSet, DHS_BD_FRAME& dataFrame,
		int index= 0, int total= 1, const string* quicklook= 0);
  // for higher dimensional frames (although it it's unlikely quicklook will ever support this,
  // so this is primarily for archiving):
  // don't set mef header
  int* newFrame(string& dataLabel, 
		DHS_BD_DATASET& dataSet, DHS_BD_FRAME& dataFrame, 
                int nods, // naxis3 == 1+nods
		int index, int total, 
		const string* quicklook= 0);
  // set mef/extension header
  int* newFrame(const UFStrings& fitsExtHdr, string& dataLabel, 
		DHS_BD_DATASET& dataSet, DHS_BD_FRAME& dataFrame, 
		int nods, // naxis3 == 1+nods
		int index, int total, 
		const string* quicklook= 0);
  // set primary & mef extention header
  int* newFrame(const UFStrings& fitsPrmHdr, const UFStrings& fitsExtHdr, string& dataLabel, 
		DHS_BD_DATASET& dataSet, DHS_BD_FRAME& dataFrame, 
		int nods, // naxis3 == 1+nods
		int index, int total, 
		const string* quicklook= 0);

  // decode header provided as a UFStrings
  static int decodeFITS(const UFStrings& hdr,
		        map< string, string >& svals, 
		        map< string, int >& ivals,
		        map< string, float >& fvals,
		        map< string, string >& comments);

  // decode (OSCIR or Flam) header provided as a UFStrings
  // initial value key-names may be any form or end with 1
  static int decodeInitialFITS(const UFStrings& hdr,
			       map< string, string >& svals, 
			       map< string, int >& ivals,
			       map< string, float >& fvals,
			       map< string, string >& comments);

  // decode (OSCIR or Flam) header provided as a UFStrings
  // final value key-names are assumed to end in 2
  static int decodeFinalFITS(const UFStrings& hdr,
		             map< string, string >& svals, 
		             map< string, int >& ivals,
		             map< string, float >& fvals,
		             map< string, string >& comments);

  // put (archive & qlook) unit test funcs.:
  // if archive datalabel is empty, dhsBdName will be used by UFDHSwrap::newDataset():
  // do archive only
  // send dataset to dhs, using copy from UFInts into dhs library's pDHsData buffer...
  /*
  DHS_STATUS putQlookFrame(const UFInts& data, const string& dataLabel,
			   DHS_BD_DATASET& dataset, int* pDhsData,
		           int index= 0, bool finalframe= false);
  DHS_STATUS putQlookFrame(const UFInts& data, const vector< int >& offsets,
			   const string& dataLabel,
			   DHS_BD_DATASET& dataset, int* pDhsData,
		           int index= 0, bool finalframe= false);

  void putArchiveStream(UFDHSFlam& dhs);
  // allow it to be run in its own pthread:
  static void* _putArchiveStream(void* p= 0);

  // do qlook only
  void putQlookStreams(UFDHSFlam& dhs);
  // allow it to be run in its own pthread:
  static void* _putQlookStreams(void* p= 0);
  */
  // send dataset to dhs, using copy from UFInts into dhs library's pDHsData buffer...
  // used by _putArchvThread
  DHS_STATUS putArchiveFrame(const UFInts& data, const string& dataLabel,
			     DHS_BD_DATASET& dataSet, int* pDhsData,
		             int index= 0, bool finalframe= false);
  DHS_STATUS putArchiveFrame(const vector< UFInts* >& data, const string& dataLabel,
			     DHS_BD_DATASET& dataSet, int* pDhsData,
		             int index= 0, bool finalframe= false);

  // do archiving and qlook: -- main should connect to frame service, fetch data and feed to thread
  // via UFInts* deque. main should use trylock, thread should use blocking lock on mutex
  // thread can itself connect to agents for fits header(s) 
  // main creates new thread for each new observation. thread is responsible
  // for freeing frames and obsconfig on completion of observation. main indicates
  // to thread that observation is complete by inserting null UFInts* into deque:
  void putAllStreams(UFDHSFlam::ObsDataQue& obs);
  // allow it to be run in its own pthread:
  static void* _putAllStreams(void* p= 0);

  int putObservation( UFDHSwrap::Stream& archv, ObsDataQue& dataque );

  // allow each nod/ext/frame its own put thread:
  static void* UFDHSFlam::_putArchvThread(void* p);

  inline UFFlamObsConf* setObs( UFFlamObsConf* newobs )
   { UFFlamObsConf* old = _obscfg; _obscfg = newobs; return old; }
  inline UFFlamObsConf* getObs() { return _obscfg; }

  //protected:
  UFFlamObsConf* _obscfg; // new one for each observation seq.
  UFStrings* _extFITS(UFFlamObsConf& obscfg, int imgIdx, string& utstart, string& utendi,
		      double airmassart, double airmassend);
  double _getAirmass();

  // convenience func. should be given an open dhs connection for datalabel
  // and connects to agents for lates FITS attribute info.
  static int initAttributes(string& label, UFDHSFlam& dhs, const string& agenthost);

  // only one of these needed/process:
  static string _agenthost;
  static string _frmhost;
  static UFFITSClient* _ufits;
  static UFSocket::ConnectTable _connections;
  static bool _observatory;
  static float _timeout, _fitstimeout;
  static bool _qlook;
  static int _archvThrdCnt; // for use by terminate/shutdown func.
};

#endif // __UFDHSFlam_h__

#if !defined(__UFGemSchaevitzAgent_h__)
#define __UFGemSchaevitzAgent_h__ "$Name:  $ $Id: UFGemSchaevitzAgent.h,v 0.3 2005/06/07 15:52:03 hon Exp $"
#define __UFGemSchaevitzAgent_H__(arg) const char arg##UFGemSchaevitzAgent_h__rcsId[] = __UFGemSchaevitzAgent_h__;

#include "string"
#include "vector"
#include "iostream.h"

#include "UFGemDeviceAgent.h"
#include "UFLVDTConfig.h"

class UFGemSchaevitzAgent: public UFGemDeviceAgent {
public:
  static int main(int argc, char** argv, char** env);
  UFGemSchaevitzAgent(int argc, char** argv, char** envv);
  UFGemSchaevitzAgent(const string& name, int argc, char** argv, char** envv);
  inline virtual ~UFGemSchaevitzAgent() {}

  static void sighandler(int sig);

  // override these UFDeviceAgent virtuals:
  virtual void startup();

  // supplemental options (should call UFDeviceAgent::options() last)
  virtual int options(string& servlist);

  // override UFGemDevAgent virtual:
  virtual void setDefaults(const string& instrum= "flam", bool initsad= true);

  // in adition to establishing the iocomm/annex connection to
  // the motors, open the pipe to the epics channel access "helper"
  virtual UFTermServ* init(const string& host= "", int port= 0);

  //virtual int initTables(UFDeviceAgent::DevCmdTable& dtbl, UFDeviceAgent::QuerryCmds& qlist);

  //virtual UFProtocol* action(UFDeviceAgent::CmdInfo* act);
  // new signature for action:
  virtual int action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& rep);

  virtual void* ancillary(void* p);
  virtual void hibernate();

  // default base class behavior should suffice...
  virtual int query(const string& q, vector<string>& reply);

  static string getCurrent(string& v, string& d);

protected:
  static string _currentVolt, _currentDisp;
};

#endif // __UFGemSchaevitzAgent_h__
      

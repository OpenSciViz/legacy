#if !defined(__UFLVDTConfig_h__)
#define __UFLVDTConfig_h__ "$Name:  $ $Id: UFLVDTConfig.h,v 0.5 2006/10/16 17:32:58 hon Exp $"
#define __UFLVDTConfig_H__(arg) const char arg##UFLVDTConfig_h__rcsId[] = __UFLVDTConfig_h__;

#include "UFDeviceConfig.h"
#include "UFBaytech.h"

class UFLVDTConfig : public UFDeviceConfig {
public:
  UFLVDTConfig(const string& name= "UnknownSB@DefaultConfig");
  UFLVDTConfig(const string& name, const string& tshost, int tsport);
  inline virtual ~UFLVDTConfig() {}

  // override these virtuals:
  virtual vector< string >& UFLVDTConfig::queryCmds();
  virtual vector< string >& UFLVDTConfig::actionCmds();
  // return -1 if invalid, 0 if valid but no reply expected,
  // n > 0 if valid and n reply strings expected:
  virtual int validCmd(const string& c);

  // override these virtuals:
  // device i/o behavior
  //virtual string prefix();
  virtual string prefix();
  virtual string terminator();
  virtual UFTermServ* connect(const string& host= "", int port= 0);
  virtual UFStrings* status(UFDeviceAgent* da);
  virtual UFStrings* statusFITS(UFDeviceAgent* da);

  static string UFLVDTConfig::_baytech; // baytech rpc host name or ip
  static int _outlet;
  static bool _powered;

  inline static bool isOn() { return _powered; } 
  inline static bool isOff() { return !_powered; }

  // minimal set of query/action command strings:
  // analogue
  inline static string getMeasVal() { return "RA 1 1"; }
  inline static string getDispVal() { return "RA 2 1"; }

  inline static string setInputDefault() { return "SA 62 0"; }
  inline static string setInputRatio() { return "SA 62 1"; }
  inline static string setInputSumDifRatio() { return "SA 62 2"; }

  inline static string getInputSensitivity() { return "RA 63 1"; }
  inline static string setInputSensitivity(const string& val) { return "SA 63 " + val; }

  inline static string getStrokelength() { return "RA 64 1"; }
  inline static string setStrokelength(const string& val) { return "SA 64 " + val; }

  // logical 
  inline static string excHzOn() { return "SL 55 ON"; } // 5 KHz
  inline static string excHzOff() { return "SL 55 OFF"; } // 2.5 KHz
  inline static string excVoOn() { return "SL 56 ON"; } // 5 volts
  inline static string excVoOff() { return "SL 56 OFF"; } //3 volts

  static string refresh(); // refresh baytech menu (and keep connected)
  static string pwrOn(); // power LVDT on via baytech outlet (6)
  static string pwrOff(); // power LVDT off via baytech outlet (6)

private:
  static UFBaytech* _btechsoc; // soc to baytech power control for lvdt on/off
};

#endif // __UFLVDTConfig_h__

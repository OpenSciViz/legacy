#if !defined(__uflvdttalk__)
#define __uflvdttalk__ "$Name:  $ $Id: uflvdttalk.cc 14 2008-06-11 01:49:45Z hon $";

#include "string"
#include "vector"
#include "stdio.h"
#include "UFDaemon.h"
#include "UFLVDTConfig.h"
#include "UFTermServ.h"
#include "UFTimeStamp.h"
#include "UFStrings.h"

static bool _quiet = true;

class uflvdttalk : public UFDaemon, public UFLVDTConfig {
public:
  ~uflvdttalk();
  uflvdttalk(int argc, char** argv, int port= 7006);

  // return 0 on connection failure:
  UFClientSocket* init(const string& host, int port= 7006);
  int waitForReply(float timeOut);

  // submit req and wait for reply, return 
  string submit(string& cmd, float flush= -1.0);

  virtual string description() const { return __uflvdttalk__; }

  UFClientSocket* _soc; 
};

uflvdttalk::uflvdttalk(int argc, char** argv, int port) : UFDaemon(argc, argv), UFLVDTConfig(), _soc(0) {
  rename("uflvdttalk@" + hostname());
}


// public func:
// return < 0 on connection failure:
UFClientSocket* uflvdttalk::init(const string& host, int port) {
  UFTermServ* ts = connect(host, port);
  if( ts == 0 ) {
    return 0;
  }
  // use this for lvdt i/o
  _soc = static_cast< UFClientSocket* > (ts );

  return _soc;
}

// public
uflvdttalk::~uflvdttalk() {
  if( _soc ) {
    _soc->close();
  }
}

int uflvdttalk::waitForReply(float timeOut) {
  float to= 0;
  int nr= _soc->available();
  while ( nr <= 0 && ((timeOut < 0) || (timeOut > 0 && to < timeOut)) ) {
    UFPosixRuntime::sleep(0.5); to += 0.5;
    nr = _soc->available();
    if( nr < 0 )
      return nr;
  }
  if( nr <= 0 )
    return nr;

  return nr;
}


// submit data read req. & recv. reply
string uflvdttalk::submit(string& cmd, float flush) {
  // use funcs. inherited from UFLVDTConfig:
  string r;

  if( _soc == 0 )
    return r;
 
  _soc->send(cmd);

  int nr = waitForReply(3.0);
  if( nr > 0 ) {
    unsigned char lvdt[nr+1]; memset(lvdt, 0, nr+1);
    nr = _soc->recv(lvdt, nr);
    r = (char*)lvdt;
    rmJunk(r);
  }

  return r;
}

int main(int argc, char** argv, char** env) {
  uflvdttalk lvdt(argc, argv);
  bool prompt = false, powerOn= false, powerOff= false; 
  string arg, host = "192.168.111.125"; // UFRuntime::hostname();
  int port= 7006;
  float flush= -1.0;
  string raw;

  arg = lvdt.findArg("-q");
  if( arg != "false" ) {
    _quiet = true;
    if( arg != "true" )
      raw = arg;
  }

  arg = lvdt.findArg("-v");
  if( arg != "false" ) 
    UFDeviceConfig::_verbose = UFTermServ::_verbose = true;

  arg = lvdt.findArg("-flush");
  if( arg != "false" )
    flush = atof(arg.c_str());

  arg = lvdt.findArg("-host");
  if( arg != "false" )
    host = arg;

  arg = lvdt.findArg("-port");
  if( arg != "false" && arg != "true" )
    port = atoi(arg.c_str());

  arg = lvdt.findArg("-on");
  if( arg != "false" ) {
    powerOn = true;
    string btech = UFLVDTConfig::pwrOn();
    clog<<"uflvdttalk> bteck poweron reply: "<<btech<<endl;
  }

  arg = lvdt.findArg("-off");
  if( arg != "false" ) {
    powerOff = true;
    string btech = UFLVDTConfig::pwrOn();
    clog<<"uflvdttalk> bteck poweroff reply: "<<btech<<endl;
  }


  if( port <= 0 || host.empty() ) {
    clog<<"uflvdttalk> usage: 'uflvdttalk -host host -port port -on -off -q [-raw rawcmd]"<<endl;
    return 0;
  }
 
  clog<<"uflvdttalk> connect to LVDT via Perle Console Server: "<<host<<" : "<<port<<endl;
  UFClientSocket* soc = lvdt.init(host, port);
  if( soc == 0 ) {
    clog<<"uflvdttalk> unable to connect to LVDT via Perle: "<<host<<" : "<<port<<endl;
    return 1;
  }

  string r;
  if( !raw.empty() ) {
    // data read command/req should be executed once
    r = lvdt.submit(raw, flush);
    if( r.empty() )
      clog << "uflvdttalk> failed to submit raw req: "<<raw<<endl;
  }
  else {
    prompt = true;
  }

  if( !prompt && r.empty() ) {
    clog<<"uflvdttalk> no data returned cmd: "<<raw<<endl;
    return -1;
  }
  else if( !prompt ) {
    cout<<r<<endl;
    return 0;
  }
  // enter command loop
  string line;
  while( true ) {
    cout<<"uflvdttalk> enter cmd or quit> "<<ends;
    getline(cin, line);
    if( line == "exit" || line == "quit" || line == "q" )
      break;
    raw = line;
    string reply = lvdt.submit(raw, flush);
    if( reply.empty() ) {
      clog << "uflvdttalk> failed to read req for raw: "<<raw<<endl;
      continue;
    }
    cout<<r<<endl;
  }

  return 0;
} // uflvdttalk::main

#endif // __uflvdttalk__

#if !defined(__UFMCE4LUTs_h__)
#define __UFMCE4LUTs_h__ "$Name:  $ $Id: UFMCE4LUTs.h,v 0.4 2006/08/16 20:10:09 warner Exp $"
#define __UFMCE4LUTs_H__(arg) const char arg##UFMCE4LUTs_h__rcsId[] = __UFMCE4LUTs_h__;

#include "UFInts.h"
#include "UFRuntime.h"

using namespace std;

class UFMCE4LUTs {
public:
  //create and return Look-Up-Tables (pixel maps) for CRC-774 MUX on Raytheon det.array:
  static UFInts* allocADCChanLUT(const int width= 2048, const int height= 2048, const int chan= 16);
  static UFInts* allocMUXLUT(const int width= 2048, const int height= 2048, const int nMux= 32, const int nChan= 16);
  static UFInts* allocMUXLUTlab(const int width= 2048, const int height= 2048, const int nMux= 32, const int nChan= 16);
  static UFInts* allocMUXLUTside(const int width= 2048, const int height= 2048, const int nMux= 32, const int nChan= 16);
  static UFInts* allocMUXLUTup(const int width= 2048, const int height= 2048, const int nMux= 32, const int nChan= 16);
  static int writeBigEndianFile(const UFInts& lut); 
  static int writeLittleEndianFile(const UFInts& lut);
  static int writeFITS(const UFInts& lut, bool swap= true);
  static UFInts* readLUTandSwap4(const string& file);
  static int main(int argc, char** argv, char** envp);
};

#endif // __UFMCE4LUTs_h__

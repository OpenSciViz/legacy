#include "UFRuntime.h"
#include "sys/types.h"
#include "sys/stat.h"
#include "fcntl.h"

#include "string"
#include "vector"
#include "deque"

//static FILE *_capipeWr= 0, *_capipeRd= 0; // for full-duplex in/out pipe ((multiple/indefinite) to/from ufcaget)
static int _capipeWr= -1, _capipeRd= -1; // for full-duplex in/out pipe ((multiple/indefinite) to/from ufcaget)
static string _epics; 

int pipeToFromEpicsCAchild(const string& epicshost) {
  // spawn child with full duplex pipe to parent:
  string cmd = "ufcaget -q";
  return (int)UFRuntime::pipeReadWriteChild(cmd, _capipeWr, _capipeRd); // quiet outputs single string only or error(s)
}

int main(int argc, char** argv, char** envp) {
  string host = "192.168.111.20";
  string echan = "flam:heartbeat\n";
  if( argc > 1 ) {
    echan = argv[1]; echan += "\n";
  }

  clog<<"ufcapipe> open full duplex pipe to ufcaget, and fetch: "<<echan<<ends;
  int stat = pipeToFromEpicsCAchild(host); // flam epics running on flamd20
  if( stat < 0  || _capipeWr < 0 || _capipeRd < 0 ) {
    clog<<"ufcapipe> failed to create pipe to child, stat: "<<stat<<", pipeWr: "<<_capipeWr<<", pipeRd: "<<_capipeRd<<endl;
    exit(-1);
  }
  else {
    clog<<"ufcapipe> created pipe to child, _capipeWr= "<<_capipeWr<<", _capipeRd= "<<_capipeRd<<endl;
  }

  int cnt = 10;
  string val;
  while( --cnt >= 0 ) {
    clog<<"ufcapipe> write to child: "<<echan<<ends;
    UFRuntime::psend(_capipeWr, echan);
    UFRuntime::precv(_capipeRd, val);
    clog<<"ufcapipe> recv from child: "<<val<<endl;  
  }
  string quit = "quit\n";
  UFRuntime::psend(_capipeWr, quit);
}

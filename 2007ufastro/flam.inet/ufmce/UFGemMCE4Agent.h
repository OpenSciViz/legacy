#if !defined(__UFGemMCE4Agent_h__)
#define __UFGemMCE4Agent_h__ "$Name:  $ $Id: UFGemMCE4Agent.h,v 0.12 2006/02/22 23:43:05 hon Exp $"
#define __UFGemMCE4Agent_H__(arg) const char arg##UFGemMCE4Agent_h__rcsId[] = __UFGemMCE4Agent_h__;

#include "UFGemDeviceAgent.h"
#include "UFMCE4Config.h"
#include "UFClientSocket.h"
#include "UFStrings.h"

#include "UFFlamObsConf.h"
//__UFFlamObsConf_H__(UFGemMCE4Agent_h);

#include "UFAcqCtrl.h" // combines Edtd and FITS stuff, coordinates obssetup & observe cmds
//#include "UFFITSClient.h"

#include "string"
#include "list"
#include "vector"
#include "map"
#include "iostream.h"

class UFGemMCE4Agent: public UFGemDeviceAgent {
public:
  static int main(int argc, char** argv, char** env);
  UFGemMCE4Agent(int argc, char** argv, char** env);
  UFGemMCE4Agent(const string& name, int argc, char** argv, char** env);
  inline virtual ~UFGemMCE4Agent() {}

  // override these UFRndRobinServ/UFDeviceAgent virtuals:
  //virtual int pendingReqs(vector <UFSocket*>& clients);
  virtual void startup();
  virtual string newClient(UFSocket* client);
  // supplemental options (should call UFDeviceAgent::options)
  virtual int options(string& servlist);
  // override UFGemDevAgent virtual:
  virtual void setDefaults(const string& instrum= "flam", bool initsad= true);

  // in addition to establishing the iocomm/annex connection to mce4,
  // open the pipe to the epics channel access "helper"
  virtual UFTermServ* init(const string& host= "", int port= 0);

  virtual UFProtocol* action(UFDeviceAgent::CmdInfo* act);
  // new signature for action:
  virtual int action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& rep);

  //virtual int query(const string& q, vector<string>& reply);
  virtual void* ancillary(void* p);
  virtual void hibernate();

  // create the basic FITS header which contains NAXIS values,
  // defined using the current _FrmConf, _ObsConf, _dhsLabel, and _ObsCompStat, etc.:
  virtual UFFITSheader* basicFITSheader();

  // set epics acquisition & readout flags
  void setEpicsAcqFlags(bool acq= false, bool rdout= false);

  // query MCE for basic frame times (at PBC=1, initializes UFMCEConfig):
  //bool initMCEtiming( bool queryMCE4 = true );

  // check status of MCE, in particular the bias board voltage states,
  // setting the protected attribs: string _biasPower, _Vgate, _wellDepth, _biasLevel
  //  and then send status to Epics if connected to a db:
  void checkMCEstatus();

protected:
  // use this for obssetup and acq/observe start/stop/abort logic:
  static UFAcqCtrl* _AcqCtrl;
  
  static string _frmhost, _devhost, _FrmAcqVersion, _warmmux;
  static int _frmport;
  //The host on which other Device Agents are running (default=localhost),
  static bool _idle;

  // keep track of and disambiguate current obs., aborted/stopped and next obs setup(s)
  // current/active observation (if not idle, i.e. acquiring exposure) == *_ObsConf
  // obseteu(s) == *_nextObsConf -- can be reset many times before applying (copy to current/active)
  // stopped/aborted obs == *_abortedObsConf
  static UFFlamObsConf *_ObsConf, *_nextObsConf, *_abortedObsConf;
  static float _exptime;
  static int _arrayColumns, _arrayRows, _arrayChannels;
  static string _ObsCompStat;
  static int _bgADUStart, _rdADUStart;
  static float _bgWellStart, _bgWellMax, _bgWellMin;
 
  string _biasPower, _Vgate, _wellDepth, _biasLevel;
  float airMassBeg;

  bool _archive, _dhsWrite;
  string _dhsLabel, _comment;
  //root dir sent to FrmAcqServer/ufgedtd dictating where FITS files are created.
  static string _archvRootDir;

  // support simulation option:
  void* _simMCE4(void* p);
};

#endif // __UFGemMCE4Agent_h__
      

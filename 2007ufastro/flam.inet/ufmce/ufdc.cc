#if !defined(__ufdc_cc__)
#define __ufdc_cc__ "$Name:  $ $Id: ufdc.cc 14 2008-06-11 01:49:45Z hon $";

#include "string"
#include "vector"
#include "stdio.h"

#include "UFClientApp.h"
#include "UFMCE4Config.h"

class ufdc :  public UFClientApp {
public:
  inline ufdc(const string& name, int argc, char** argv, char** envp, int port= -1);
  inline ~ufdc() {}
  static int main(const string& name, int argc, char** argv, char** envp);
  inline virtual string description() const { return __ufdc_cc__; }

  // parse & insert into string vector assuming
  // format: -par "hard:0,1,2,3,4,5,6,7,8,9"
  // or:     -par "phys:0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0"
  // or:     -par "meta:a,b,c,d,e,f,g,h,i,j,k"
  // also truncate to directive name par ==> "hard/phys/meta"
  // note these are strictly trecs options, not flamingos
  int parsePar(string& par, vector < string >& pars);

  // submit parameter bundle, flush output and sleep a bit.
  // recv reply and return reply as string.
  int submitPar(vector < string >& par, const string& obsmode, const string& rdoutmode,
		UFStrings*& reply, float flush= -1.0);

  // parse & insert into string vector assuming
  // format: -obssetup "ObsIdi/Datalabel, Host, RootDir, File"
  int parseObsSetup(string& conf, vector < string >& acqconf);

  // test dac bias & preamp commands
  int testDACBias(UFStrings*& reply, int well= 0, int det= 0, float flush= -1.0);
  int testDACPreamp(UFStrings*& reply, float flush= -1.0);
};

ufdc::ufdc(const string& name, int argc,
	   char** argv, char** envp, int port) : UFClientApp(name, argc, argv, envp, port) {}

int ufdc::main(const string& name, int argc, char** argv, char** envp) {
  ufdc dc(name, argc, argv, envp);
  string arg, host(hostname());
  string acq("false"), obssetup("false"), par("false"), sim("false"), status("false");
  string cmdline("false"), raw("true"); // default is raw command mode
  int port= 52008;
  float flush= -1.0;
  int nods= 0, dac= -1;
  bool init= false, test= false;
  string usage = "ufdc> usage: 'ufdc [-flush sec.] [-host host] [-port port] [-q/-raw raw-command] [-acq [start(or exptime)/stop/abort]] [-obssetup \"ObsId/DataLabel,File,RootDir,NFSHost\"] [-nod[s] [0(stare)]] [-rdoutmode default] [-par [hard:1,2,...] [meta:a,b,...]] [-dac 0/1/2] -test/-status -init -exptime [sec.] -expcnt [frames/nod]'\n";
  usage += "ufdc> note that the gemini 'observe' results from -acq; -obssetup should precede -acq; and all other options support dc/instrument setup, should precede -obssetup.";
  
  string obsmode= "Stare", rdoutmode= "Default";

  arg = dc.findArg("-h");
  if( arg != "false" ) {
    clog<<"ufdc> "<<usage<<endl;
    return 0;
  }

  arg = dc.findArg("-help");
  if( arg != "false" ) {
    clog<<"ufdc> "<<usage<<endl;
    return 0;
  }

  arg = dc.findArg("-obssetup");
  if( arg != "false" ) {
    raw = "false";
    if( arg == "true" ) { // allow default value
      char testnm[] = "Test.XXXXXX";
      cmdline = obssetup = mkstemp(testnm);
    }
    else
      cmdline = obssetup = arg;
  }

  arg = dc.findArg("-nod");
  if( arg != "false" && arg != "true" )
    nods = atoi(arg.c_str());

  arg = dc.findArg("-rdoutmode");
  if( arg != "false" && arg != "true" )
    rdoutmode = arg;

  arg = dc.findArg("-q");
  if( arg != "false" ) {
    _quiet = true;
    if( arg != "true" )
      raw = cmdline = arg;
  }

  arg = dc.findArg("-flush");
  if( arg != "false" )
    flush = atof(arg.c_str());

  arg = dc.findArg("-init");
  if( arg != "false" )
    init = true;

  arg = dc.findArg("-test");
  if( arg != "false" )
    test = true;

  arg = dc.findArg("-raw");
  if( arg != "false" && arg != "true" )
    raw = cmdline = arg;

  arg = dc.findArg("-status");
  if( arg != "false" ) {
    raw = "false";
    _quiet = true;
    if( arg != "true" ) 
      status = cmdline = arg;
    else
      status = cmdline = "All"; // or 'Obs'
  }

  arg = dc.findArg("-stat");
  if( arg != "false" ) { 
    raw = "false";
    _quiet = true;
    if( arg != "true" ) 
      status = cmdline = arg;
    else
      status = cmdline = "All";
  }

  arg = dc.findArg("-sim"); // dcagent should relay -sim to ufedtd
  if( arg != "false" && arg != "true" ) {
    sim = cmdline = arg;
    raw = "false";
  }
  /*
  arg = dc.findArg("-car");
  if( arg != "false" && arg != "true" ) {
   car = cmdline = arg;
     raw = "false";
  }
  */
  // format: -par "hard:0,1,2,3,4,5,6,7,8,9"
  // or:     -par "phys:0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0"
  // or:     -par "meta:a,b,c,d,e,f,g,h,i,j,k"
  // or:     -par clear
  arg = dc.findArg("-parm");
  if( arg != "false" && arg != "true" ) { // explicit cmd string 
    par = cmdline = arg;
    raw = "false";
  }
  arg = dc.findArg("-par");
  if( arg != "false" && arg != "true" ) { // explicit cmd string 
    par = cmdline = arg;
    raw = "false";
  }

  arg = dc.findArg("-acq");
  if( arg != "false" && arg != "true" ) { // explicit cmd string 
    acq = cmdline = arg;
    raw = "false";
  }

  arg = dc.findArg("-host");
  if( arg != "false" )
    host = arg;

  arg = dc.findArg("-port");
  if( arg != "false" && arg != "true" )
    port = atoi(arg.c_str());

  arg = dc.findArg("-dac");
  if( arg != "false" && arg != "true" ) {
    dac = atoi(arg.c_str());
    cmdline = arg;
    raw = "false";
  }

  if( port <= 0 || host.empty() ) {
    clog<<"ufdc> "<<usage<<endl;
    return 0;
  }

  clog<<"ufdc> connect to MCE4 command server agent..."<<endl;
  FILE* f  = dc.init(host, port);
  if( f == 0 ) {
    clog<<"ufdc> unable to connect to MCE4 command server agent..."<<endl;
    return -2;
  }

  UFStrings* reply_p;
  if( cmdline != "false" ) {
    // command should be executed once
    vector< string > mult;
    if( par != "false" ) {
      int npar = dc.parsePar(par, mult);
      if( npar <= 0 ) {
        clog << "ufdc> failed to parse parameters: "<<cmdline<<endl;
        return -1;
      }
    }
    else if( obssetup != "false" ) {
      int n = dc.parseObsSetup(obssetup, mult);
      if( n <= 0 ) {
        clog << "ufdc> failed to parse obssetupu option: "<<cmdline<<endl;
        return -1;
      }
    }
    else if( dac < 0 ) {
      int nraw = dc.parse(cmdline, mult);
      if( nraw <= 0 ) {
        clog << "ufdc> failed to parse cmd(s): "<<cmdline<<endl;
        return -1;
      }
    }

    int ns= 0;
    if( raw != "false" ) {
      for( size_t i = 0; i < mult.size(); ++i )
	clog<<"ufdc> submit "<<mult[i]<<endl;
      ns = dc.submit("raw", mult, reply_p, flush);
      if( ns < 0 ) {
        clog << "ufdc> failed to submit: "<<raw<<endl;
        return -3;
      }
    }
    else if( status != "false" ) {
      ns = dc.submit("status", mult, reply_p, flush);
      if( ns < 0 ) {
        clog << "ufdc> failed to submit: "<<raw<<endl;
        return -3;
      }
    }
    else if( sim != "false" ) {
      ns = dc.submit("sim", mult, reply_p, flush);
      if( ns < 0 ) {
        clog << "ufdc> failed to submit: "<<sim<<endl;
        return -3;
      }
    }
    else if( acq != "false" ) {
      ns = dc.submit("acq", mult, reply_p, flush);
      if( ns < 0 ) {
        clog << "ufdc> failed to submit: "<<acq<<endl;
        return -3;
      }
    }
    else if( obssetup != "false" ) {
      ns = dc.submit("ObsSetup", mult, reply_p, flush);
      if( ns < 0 ) {
        clog << "ufdc> failed to submit: "<<acq<<endl;
        return -3;
      }
    }
    else if( par != "false" ) {
      ns = dc.submitPar(mult, obsmode, rdoutmode, reply_p, flush);
      if( ns < 0 ) {
        clog << "ufdc> failed to submit: "<<par<<endl;
        return -3;
      }
    }  
    else if( dac == 0 ) {
      ns = dc.testDACBias(reply_p, 0, 0, flush);
      if( ns < 0 ) {
        clog << "ufdc> failed to submit: "<<par<<endl;
        return -3;
      }
      ns = dc.testDACPreamp(reply_p, flush);
      if( ns < 0 ) {
        clog << "ufdc> failed to submit: "<<par<<endl;
        return -3;
      }
    }
    else if( dac == 1 ) {
      ns = dc.testDACBias(reply_p, 1, 1, flush);
      if( ns < 0 ) {
        clog << "ufdc> failed to submit: "<<par<<endl;
        return -3;
      }
      ns = dc.testDACPreamp(reply_p, flush);
      if( ns < 0 ) {
        clog << "ufdc> failed to submit: "<<par<<endl;
        return -3;
      }
    }
    else if( dac >= 2 ) {
      ns = dc.testDACBias(reply_p, 1, 2, flush);
      if( ns < 0 ) {
        clog << "ufdc> failed to submit: "<<par<<endl;
        return -3;
      }
      ns = dc.testDACPreamp(reply_p, flush);
      if( ns < 0 ) {
        clog << "ufdc> failed to submit: "<<par<<endl;
        return -3;
      }
    }

    if( reply_p == 0 ) {
      clog<<"ufdc> no reply..."<<endl;
      return dc.close();
    }
    else if( reply_p->elements() <= 0 ) {
     clog<<"ufdc> empty reply. "<<reply_p->name()<<endl;
      return dc.close();
    }

    UFStrings& reply = *reply_p;
    if( !_quiet )
      cout<<reply.timeStamp()<<reply.name()<<endl;
    for( int i=0; i < reply.elements(); ++i )
      cout<<reply[i]<<endl;

    delete reply_p; reply_p = 0;
    return dc.close();
  }

  string line;
  while( true ) {
    clog<<"ufdc> "<<ends;
    getline(cin, line);
    if( line == "exit" || line == "quit" || line == "q" ) return 0;
    if( line.size() <= 1 )
      continue; // ignore empty line

    if( line.find("par") != string::npos ||
	line.find("Par") != string::npos || 
	line.find("PAR") != string::npos )
      par = line;
    vector< string > mult;
    if( par != "false" ) {
      int npar = dc.parsePar(par, mult);
      if( npar <= 0 ) {
        clog << "ufdc> failed to parse parameters: "<<cmdline<<endl;
        return -2;
      }
    }
    else {
      int nraw = dc.parse(line, mult);
      if( nraw <= 0 ) {
        clog << "ufdc> failed to parse cmd(s): "<<cmdline<<endl;
        return -2;
      }
    }

    int ns= 0;
    if( raw != "false" ) {
      ns = dc.submit("raw", mult, reply_p, flush);
      if( ns <= 0 ) {
        clog << "ufdc> failed to submit: "<<raw<<endl;
        return -3;
      }
    }
    else if( sim != "false" ) {
      ns = dc.submit("sim", mult, reply_p, flush);
      if( ns <= 0 ) {
        clog << "ufdc> failed to submit: "<<sim<<endl;
        return -3;
      }
    }
    else if( acq != "false" ) {
      ns = dc.submit("acq", mult, reply_p, flush);
      if( ns <= 0 ) {
        clog << "ufdc> failed to submit: "<<acq<<endl;
        return -3;
      }
    }
    else if( par != "false" ) {
      ns = dc.submitPar(mult, obsmode, rdoutmode, reply_p, flush);
      if( ns <= 0 ) {
        clog << "ufdc> failed to submit: "<<par<<endl;
        return -3;
      }
    }

    UFStrings& reply = *reply_p;
    //cout<<reply.timeStamp()<<reply.name()<<endl;
    for( int i=0; i < reply.elements(); ++i )
      cout<<reply[i]<<endl;
    delete reply_p; reply_p = 0;
  }
  return dc.close(); 
}

// parse mutil-cmd line assuming
// format: -par "hard:0,1,2,3,4,5,6,7,8,9"
// or:     -par "phys:0.0,1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0"
// or:     -par "meta:a,b,c,d,e,f,g,h,i,j,k"
// insert into string vector
int ufdc::parsePar(string& par, vector <string>& pars) {
  pars.clear();
  pars.push_back("Par");
  if( par.find("cle") != string::npos ||
      par.find("Cle") != string::npos || 
      par.find("CLE") != string::npos ) {
    pars.push_back("Clear");
    return (int)pars.size();
  }

  size_t colp =  par.find(":");
  if( colp == string::npos ) {
    pars.clear();
    return 0;
  }
  string parlist = par.substr(1+colp);

  if( par.find("har") != string::npos ||
      par.find("Har") != string::npos || 
      par.find("HAR") != string::npos ) {
    pars.push_back("Har");
    UFMCE4Config::HdwrParms hp;
    UFMCE4Config::clear(hp);
    UFMCE4Config::HdwrParms::iterator itr = hp.begin();
    size_t com = 0, pos = 0;
    string sval;
    do {
      string key = itr->first;
      pars.push_back(key);
      com = parlist.find(",", pos);
      if( com == string::npos ) // assume it's final val
	sval = parlist.substr(pos);
      else
        sval = parlist.substr(pos, com-pos);
      pars.push_back(sval);
      //clog<<"ufdc::parsePar> key: "<<key<<", val: "<<sval<<endl;
      pos = 1+com;
    } while( ++itr != hp.end());
    return (int)pars.size();
  }

  if( par.find("met") != string::npos ||
      par.find("Met") != string::npos || 
      par.find("MET") != string::npos ) {
    pars.push_back("Met");
    UFMCE4Config::TextParms mp;
    UFMCE4Config::clear(mp);
    UFMCE4Config::TextParms::iterator itr = mp.begin();
    size_t com = 0, pos = 0;
    string sval;
    do {
      string key = itr->first;
      pars.push_back(key);
      com = parlist.find(",", pos);
      if( com == string::npos ) // assume it's final val
	sval = parlist.substr(pos);
      else
        sval = parlist.substr(pos, com-pos);
      pars.push_back(sval);
      if( !_quiet )
        clog<<"ufdc::parsePar> key: "<<key<<", val: "<<sval<<endl;
      pos = 1+com;
    } while( ++itr != mp.end());
    return (int)pars.size();
  }
  return -1;
}

int ufdc::parseObsSetup(string& obssetup, vector <string>& setup) {
  setup.clear();
  size_t comp =  obssetup.find(",");
  string datalabel= "DataLabel ";
  if( comp == string::npos ) { // this is datalabel only
    datalabel += obssetup;    
    setup.push_back(datalabel);
    return setup.size();
  } // nothing else provided
  else {
    datalabel += obssetup.substr(0, comp); // this is datalabel
    setup.push_back(datalabel);
  }

  string filenm = "File ";
  size_t prev = 1+comp;
  comp =  obssetup.find(",", prev);
  if( comp == string::npos ) {
    filenm += obssetup.substr(prev);
    setup.push_back(filenm);
    return setup.size();
  }
  else {
    filenm += obssetup.substr(prev, 1+comp-prev);
    setup.push_back(filenm);
  }
    
  string rootdir ="RootDir ";
  prev = 1+comp;
  comp =  obssetup.find(",", prev);
  if( comp == string::npos ) {
    rootdir += obssetup.substr(prev);
    setup.push_back(rootdir);
    return setup.size();
  }
  else {
    rootdir += obssetup.substr(prev, 1+comp-prev);
    setup.push_back(rootdir);
  }

  // last value, filename, should not be followed by a comma:
  prev = 1+comp;
  if( prev >= obssetup.length() ) {
    return setup.size();
  }
    
  string nfshost = "NFSHost ";
  comp =  obssetup.find(",", prev);
  if( comp == string::npos ) {
    nfshost += obssetup.substr(prev);
    setup.push_back(nfshost);
    return setup.size();
  }
  else {
    nfshost += obssetup.substr(prev, 1+comp-prev);
    setup.push_back(nfshost);
  }

  return setup.size();
}

int ufdc::submitPar(vector< string >& vs, const string& obsmode,
		    const string& rdoutmode, UFStrings*& r, float flush) {

  vs.push_back("obs_mode"); vs.push_back(obsmode);
  vs.push_back("readout_mode"); vs.push_back(rdoutmode);

  UFStrings ufs(name(), vs);
  int ns = send(ufs);
  if( ns <= 0 )
    return ns;
  // try flushing the socket output stream
  // this is the only way i can think to do it:
  int stat = fflush(_fs);
  if( stat != 0 ) clog<<"ufdc::submitPar> socket flush failed? ns= "<<ns<<endl;

  if( flush > 0.0 ) // optionally sleep after the flush
    UFPosixRuntime::sleep(flush);

  r = dynamic_cast<UFStrings*> (UFProtocol::createFrom(*this));
  if( r != 0 )
    return r->elements();
  else
    return 0;
}

int ufdc::testDACBias(UFStrings*& r, int well, int det, float flush) {
  vector< string > vs;

  vs.push_back("BiasPark"); vs.push_back("ARRAY_POWER_DOWN_BIAS");
  vs.push_back("BiasDatum"); vs.push_back("ARRAY_POWER_DAC_DEFAULTS_BIAS");
  vs.push_back("BiasPower"); vs.push_back("ARRAY_POWER_UP_BIAS");
  strstream sw;
  sw<<"ARRAY_WELL "<<well<<ends;
  vs.push_back("BiasvWell"); vs.push_back(sw.str()); delete sw.str();
  strstream sd;
  sd<<"ARRAY_SET_DET_BIAS "<<det<<ends;
  vs.push_back("BiasvBias"); vs.push_back(sd.str()); delete sd.str();

  UFStrings ufs(name(), vs);
  int ns = send(ufs);
  if( ns <= 0 )
    return ns;
  // try flushing the socket output stream
  // this is the only way i can think to do it:
  int stat = fflush(_fs);
  if( stat != 0 ) clog<<"ufdc::submitPar> socket flush failed? ns= "<<ns<<endl;

  if( flush > 0.0 ) // optionally sleep after the flush
    UFPosixRuntime::sleep(flush);

  r = dynamic_cast<UFStrings*> (UFProtocol::createFrom(*this));
  if( r != 0 )
    return r->elements();
  else
    return 0;
}

int ufdc::testDACPreamp(UFStrings*& r, float flush) {
  vector< string > vs;

  vs.push_back("BiasPark"); vs.push_back("ARRAY_POWER_DOWN_BIAS");
  vs.push_back("BiasDatum"); vs.push_back("ARRAY_POWER_DAC_DEFAULTS_BIAS");
  vs.push_back("BiasPower"); vs.push_back("ARRAY_POWER_UP_BIAS");

  UFStrings ufs(name(), vs);
  int ns = send(ufs);
  if( ns <= 0 )
    return ns;
  // try flushing the socket output stream
  // this is the only way i can think to do it:
  int stat = fflush(_fs);
  if( stat != 0 ) clog<<"ufdc::submitPar> socket flush failed? ns= "<<ns<<endl;

  if( flush > 0.0 ) // optionally sleep after the flush
    UFPosixRuntime::sleep(flush);

  r = dynamic_cast<UFStrings*> (UFProtocol::createFrom(*this));
  if( r != 0 )
    return r->elements();
  else
    return 0;
}

int main(int argc, char** argv, char** envp) {
  return ufdc::main("ufdc", argc, argv, envp);
}

#endif // __ufdc_cc__

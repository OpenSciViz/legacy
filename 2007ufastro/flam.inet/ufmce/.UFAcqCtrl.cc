#if !defined(__UFAcqCtrl_cc__)
#define __UFAcqCtrl_cc__ "$Name:  $ $Id: .UFAcqCtrl.cc 14 2008-06-11 01:49:45Z hon $";
const char rcsId[] = __UFAcqCtrl_cc__;

#include "UFAcqCtrl.h"
#include "UFSADFITS.h"
#include "UFSADFITS.h"

// globals:
UFAcqCtrl::Observe* UFAcqCtrl::_obssetup= 0;
float UFAcqCtrl::_timeout = -1.0;
bool UFAcqCtrl::_sim = false;
bool UFAcqCtrl::_verbose = false;
bool UFAcqCtrl::_observatory = true;
map< string, UFAcqCtrl::Observe* > UFAcqCtrl::_Observations; // datalabel, ObsSetups

UFAcqCtrl::Observe::Observe(double exptime,
			    const string& dhslabel, const string& dhsqlook, const string& comment,
			    const string& file, const string& fileIdx, const string& jpeg, const string& ds9,
			    const string& rootdir, const string& nfshost
			    ) : _exptime("1.0"), _dhswrite(dhslabel), _dhsqlook(dhsqlook),
				_comment(comment), _nfshost(nfshost), _rootdir(rootdir),
				_file(file), _fileIdx(fileIdx), _jpeg(jpeg), _ds9(ds9),
				_obscfg(0) {
  strstream s; s<<exptime<<ends; _exptime = s.str(); delete s.str();
  _obscfg = new UFFlamObsConf(exptime, dhslabel);;
}

// start/stop/abort Obs should prepend "ACQ", "START/STOP/ABORT" to this text list 
string UFAcqCtrl::Observe::asStrings(deque< string >& text) {
  text.clear();
  int expcnt= 1;
  text.push_back("EXPTIME"); text.push_back(_exptime);
  string name= _dhswrite; // should be congruent with datalabel
  if( _obscfg != 0 ) {
    name = _obscfg->datalabel();
    expcnt= _obscfg->totImgCnt();
  }
  text.push_back("DHSLABEL"); text.push_back(name);
  text.push_back("DHSQLOOK"); text.push_back(_dhsqlook); 
  text.push_back("COMMENT"); text.push_back(_comment); 
  text.push_back("NFSHOST"); text.push_back(_nfshost); 
  text.push_back("ROOTDIR"); text.push_back(_rootdir); 
  text.push_back("FILE"); text.push_back(_file); 
  text.push_back("FILEIDX"); text.push_back(_fileIdx); 
  text.push_back("JPEG"); text.push_back(_jpeg); 
  text.push_back("DS9"); text.push_back(_ds9);
  strstream s; s<<expcnt<<ends;
  text.push_back("CNT"); text.push_back(s.str()); delete s.str();
  return name;
} 

UFAcqCtrl::UFAcqCtrl(const string& agenthost, const string& framehost, map< string, int>& portmap) :
           _connected(false), _agthost(agenthost), _frmhost(framehost), _clientFITS(0) {
  // ctor should not connect to agents
  if( _agthost == "" ) _agthost = UFRuntime::hostname();
  if( _frmhost == "" ) _frmhost = UFRuntime::hostname();
  if( portmap.size() <= 0 ) defaultPorts(portmap);
  map< string, int>::iterator it = portmap.begin();
  for( size_t i = 0; i < portmap.size(); ++i ) {
    _portmap[it->first] = it->second;
  }
  if( _obssetup == 0 ) _obssetup = new UFAcqCtrl::Observe;
} 

UFAcqCtrl::UFAcqCtrl(const string& agenthost, const string& framehost) : 
    _connected(false), _agthost(agenthost), _frmhost(framehost), _clientFITS(0) {
  if( _agthost == "" ) _agthost = UFRuntime::hostname();
  if( _frmhost == "" ) _frmhost = UFRuntime::hostname();
  defaultPorts(_portmap);
  if( _obssetup == 0 ) _obssetup = new UFAcqCtrl::Observe;
}

int UFAcqCtrl::defaultPorts(map< string, int>& portmap) {
  portmap.clear();
  string agnt;

  agnt = "ufgls218d"; agnt += "(FITS)@"; // supports FITS status info.
  agnt += _agthost;
  portmap[agnt] = 52002; // lakeshore 218

  agnt = "ufgls33xd"; agnt += "(FITS)@"; // supports FITS status info.
  agnt += _agthost;
  portmap[agnt] = 52003; // lakeshore 332

  agnt = "ufgpf26xd"; agnt += "(FITS)@"; // supports FITS status info.
  agnt += _agthost;
  portmap[agnt] = 52004; // pfeiffer 262 vacuum

  agnt = "ufgmotord"; agnt += "(FITS)@"; // supports FITS status info.
  agnt += _agthost;
  portmap[agnt] = 52024; // portescap motor indexors

  agnt = "ufgmce4d" ; agnt += "(FITS)@"; // supports FITS status info.
  agnt += _agthost;
  portmap[agnt] = 52008; // mce4 detector

  agnt = "ufgedtd@" + _frmhost; // no FITS status (as yet)
  portmap[agnt] = 52001; // frame capture/acquisition agent

  agnt = "ufgflam2d@" + _agthost;
  portmap[agnt] = 52000; // executive

  agnt = "ufgsymbarcd@" + _agthost;
  portmap[agnt] = 52005; // symbol barcode

  agnt = "ufglvdtd@" + _agthost;
  portmap[agnt] = 52006; // linear variable differential transformer (focus position)

  agnt = "ufgisplcd@" + _agthost;
  portmap[agnt] = 52007; // linear variable differential transformer (focus position)

  return (int) portmap.size();
}

// minimal (optional?) attributes of the aquisition (ACQ) directive?
UFAcqCtrl::Observe* UFAcqCtrl::attributes(UFDeviceAgent::CmdInfo* act) { 
  // get acq. attributes
  if( act == 0 ) {
    clog<<"UFGemMCE4Agent::acqAttributes> null pointer arg?"<<endl;
    return 0;
  }
  // car, "carval" is optional, followed by
  // acq, "conf"; obsId, "DHS Label"; DHSwrite, "save|discard"; etc...
  int cnt= 0, elemcnt = (int)act->cmd_name.size();
  UFAcqCtrl::Observe* obs = new UFAcqCtrl::Observe;
  for( int i = 0; i < elemcnt; ++i ) {
    string val = act->cmd_impl[i];
    string key = act->cmd_name[i];
    UFStrings::upperCase( key );

    if( key.find("DHS") != string::npos ) { //the DHSwrite status.
      ++cnt;
      obs->_dhswrite = val;
      UFStrings::upperCase( val );
      if( val.find("DISCARD") != string::npos ) { //no saving data 
	obs->_dhswrite = ""; //do not send to dhs either.
	if( val.find("ALL") != string::npos )
	  obs->_nfshost = obs->_rootdir = obs->_file = "";
      }
    }
    else if( key.find("COMMENT") != string::npos ) {
      ++cnt;
      obs->_comment = val;
    }
    else if( key.find("ARCHIVE") != string::npos ) {
      if( key.find("HOST") != string::npos )
	obs->_nfshost = val;
      else if( key.find("DIR") != string::npos )
	obs->_rootdir = val;
      else if( key.find("FILE") != string::npos )
	obs->_file = val;
    }
  } //end of loop over act->cmd...

  clog<<"UFGemMCE4Agent::attributes> archive file => "<<obs->_file<<endl;
  clog<<"UFGemMCE4Agent::attributes> DHS write => "<<obs->_dhswrite<<endl;
  clog<<"UFGemMCE4Agent::attributes> archive directory => "<<obs->_rootdir<<endl;
  clog<<"UFGemMCE4Agent::attributes> frame server (nfs) host => "<<obs->_nfshost<<endl;
  return obs;
}


// create a FlamObsConf and additional observation attributes from 
// parsed UFStrings transation to/from an agent (obssetup prior to acq/observe directive)
UFAcqCtrl::Observe* UFAcqCtrl::obsSetup(UFDeviceAgent::CmdInfo* act) {
  static UFAcqCtrl::Observe* _obssetup= 0;;
  bool dhswrite= false, reply_client= true;
  if( act->clientinfo.find("f2:") != string::npos ||
      act->clientinfo.find("fu:") != string::npos ||
      act->clientinfo.find("foo:") != string::npos ||
      act->clientinfo.find("flam:") != string::npos ) {
    reply_client = false; // epics clients get replies via channel access, not ufprotocol
    //if( _verbose )
    //clog<<"UFGemMCE4Agent::> No UFProtocol replies will be sent to Gemini/Epics client: "
    //<<act->clientinfo<<endl;
  }
  int elemcnt = (int)act->cmd_name.size();
  //if( _verbose )
  //clog<<"UFGemMCE4Agent::obsSetup> client: "<<act->clientinfo<<", req. elem.: "<<elemcnt<<endl;
  string car= "", sad= "", errmsg= "";
  float exptime= 1.0;
  string dhslabel= "", dhsqlook= "", comment= "", nfshost= "", rootdir= "", file= ""; // archive

  for( int i = 0; i < elemcnt; ++i ) {
    string cmdname = act->cmd_name[i];
    UFStrings::upperCase(cmdname);
    string cmdimpl = act->cmd_impl[i];
    if( cmdname.find("CAR") != string::npos ) {
      reply_client= false;
      car = cmdimpl;
      clog<<"UFGemMCE4Agent::obsSetup> CAR: "<<car<<endl;
      continue;
    }
    if( cmdname.find("SAD") != string::npos ) {
      reply_client= false;
      sad = cmdimpl;
      clog<<"UFGemMCE4Agent::obsSetup> SAD: "<<sad<<endl;
      continue;
    }
    if( cmdname.find("OBSSETUP") == string::npos ) {
      clog<<"UFGemMCE4Agent::obsSetup> deal with this later? obssetup: "<<cmdimpl<<endl;
      continue;
    }

    if( cmdname.find("DATALABEL") != string::npos ||
	cmdname.find("DHSLABEL") != string::npos ||
	cmdname.find("OBSID") != string::npos) {
      clog<<"UFGemMCE4Agent::obsSetup> local archive and dhs: "<<cmdimpl<<endl;
      dhswrite = true; //assume data mode = save
      dhslabel = cmdimpl;
    }
    if( cmdname.find("DHSQLOOK") != string::npos ) {
      clog<<"UFGemMCE4Agent::obsSetup> local archive and dhs: "<<cmdimpl<<endl;
      dhsqlook = cmdimpl;
    }

    if( cmdname.find("EXPTIME") != string::npos ) {
      clog<<"UFGemMCE4Agent::obsSetup> exptime: "<<cmdimpl<<endl;
      exptime = (float)atof(cmdimpl.c_str());
    }

    if( cmdname.find("NFSHOST") != string::npos ) {
      clog<<"UFGemMCE4Agent::obsSetup> nfshost: "<<cmdimpl<<endl;
      nfshost = cmdimpl;
    }
 
    if( cmdname.find("ROOT") != string::npos ) {
      clog<<"UFGemMCE4Agent::obsSetup> archive root directory: "<<cmdimpl<<endl;
      rootdir = cmdimpl;
    }

    if( cmdname.find("FILE") != string::npos || cmdname.find("ARCHIVE") != string::npos ) {
      clog<<"UFGemMCE4Agent::obsSetup> archive file: "<<cmdimpl<<endl;
      file = cmdimpl; //assume data mode = save
    }

    if( cmdname.find("DISCARD") != string::npos ) { // all (archive and dhs) or just dhs?
      clog<<"UFGemMCE4Agent::obsSetup> discard: "<<cmdimpl<<endl;
      comment = cmdname + cmdimpl;
    }

    if( cmdname.find("COMMENT") != string::npos ) { // all (archive and dhs) or just dhs?
      clog<<"UFGemMCE4Agent::obsSetup> discard: "<<cmdimpl<<endl;
      comment = cmdimpl;
    }


  } // finished parsing action array

  if( _obssetup == 0 ) {
    _obssetup = new (nothrow) UFAcqCtrl::Observe(exptime, dhslabel, dhsqlook, comment, nfshost, rootdir, file);
  }
  else {
    _obssetup->_obscfg->setExpTime(exptime);
    _obssetup->_obscfg->relabel(dhslabel);
    _obssetup->_dhswrite = dhslabel;
    _obssetup->_dhsqlook = dhsqlook;
    _obssetup->_comment = comment;
    _obssetup->_nfshost = nfshost;
    _obssetup->_rootdir = rootdir;
    _obssetup->_file = file;
  }

  return _obssetup;
} // obsSetup -- full acq ctrl?

// send the obsconf to the dhsclient and get back a text string reply?
string UFAcqCtrl::submitDhsc(UFSocket* replicant) {
  // send flam2obsconf to dhsclient (replicant client)
  string dhsc;
  if( _obssetup == 0 )
    return dhsc;

  if( _obssetup->_obscfg == 0 )
    return dhsc;

  if( replicant == 0 )
    return dhsc;

  if( !replicant->validConnection() )
    return dhsc;

  int ns= replicant->send(_obssetup->_obscfg);
  if( ns > 0 ) dhsc = "DHSclient notified";
  // expect reply with local file name:
  /*
  UFStrings* reply = dynamic_cast<UFStrings*>(UFProtocol::createFrom(*this));
  if( reply ) {
    if( reply->numVals() > 0 ) {
      dhsc = (*reply)[0];
    }
  }
  */
  return dhsc;
}

string UFAcqCtrl::submitEdtd() {
  string edtd;
  if( _obssetup == 0 )
    return edtd;

  if( _obssetup->_obscfg == 0 )
    return edtd;

  if( !_connected ) {
    int stat = connectEdtd(_obssetup->_obscfg->name(), _frmhost); 
    _connected = (stat == 1);

    int fitscnt = (int)_portmap.size() - 1;
    stat = connectFITS(_agthost);
    _connected = (stat == fitscnt) && _connected;
  }
  /*
  if( validConnection() ) {
    if( pfits && efits ) {
      pfits->setSeq(1, 3); efits->setSeq(2, 3); obscfg->setSeq(3, 3);
      send(*pfits); send(*efits); send(*obscfg);
    }
    else if( pfits ) {
      pfits->setSeq(1, 2); obscfg->setSeq(2, 2);
      send(*pfits); send(*obscfg);
    }
    else {
      obscfg->setSeq(1, 1);
      send(*obscfg);
    }
  }
  else {
    _connected = false;
    close();
    return "";
  }
  */
  // expect reply with local file name:
  UFStrings* reply = dynamic_cast<UFStrings*>(UFProtocol::createFrom(*this));
  if( reply ) {
    if( reply->numVals() > 0 ) {
      edtd = (*reply)[0];
    }
  }
  return edtd;
} // submitEdt

// start observation (connect to ufedtd if necessary) return datalabel/local archive file name
//string UFAcqCtrl::startObs(UFMCE4Config* mce, UFFlamObsConf*& obscfg, 
//			   UFStrings*& prmFITS, UFStrings*& extFITS) {
string UFAcqCtrl::startObs(UFMCE4Config* mce) {
  string filenm;
  int width= 0, height= 0;
  bool idle = UFEdtDMA::takeIdle(width,  height);
  if( !idle ) {
    clog<<"UFAcqCtrl::startObs> unable to start new obs, still busy with prior obs?"<<endl;
    return filenm;
  }
  if( _obssetup == 0 ) {
    clog<<"UFAcqCtrl::startObs> unable to start new obs, no obssetup?"<<endl;
    return filenm;
  }
  if( _obssetup->_obscfg == 0 ) {
    clog<<"UFAcqCtrl::startObs> unable to start new obs, no obsconf?"<<endl;
    return filenm;
  }

  // insure all parameter are set in conf:
  _obssetup->_obscfg->startObs();
  // the primary header should be sent to the edtd before it enters into
  // the frame data wait blocking func:
  //prmFITS = fitsHdrPrm(obs.datalabel());
  //extrFITS = fitsHdrExt(obs.datalabel());
  filenm = submitEdtd(); //, prmFITS, extFITS);
  return filenm;
}

// start a (test) discard observation (connect to ufedtd if necessary)
//string UFAcqCtrl::discardObs(UFMCE4Config* mce4, UFFlamObsConf*& obscfg,
//			     UFStrings*& prmFITS, UFStrings*& extFITS) {
string UFAcqCtrl::discardObs(UFMCE4Config* mce4) {
  string filenm;
  if( _obssetup == 0 ) {
    clog<<"UFAcqCtrl::startObs> unable to start new obs, no obssetup?"<<endl;
    return filenm;
  }
  if( _obssetup->_obscfg == 0 ) {
    clog<<"UFAcqCtrl::startObs> unable to start new obs, no obsconf?"<<endl;
    return filenm;
  }
  _obssetup->_obscfg->discardObs();
  filenm = submitEdtd();
  return filenm;
}

string UFAcqCtrl::discardObs(UFSocket* replicant) {
  string filenm;
  if( replicant == 0 ) {
    clog<<"UFAcqCtrl::discardObs> unable to abort obs, no dhsclient socket?"<<endl;
    return filenm;
  }
  if( _obssetup == 0 ) {
    clog<<"UFAcqCtrl::discardObs> unable to abort obs, no obssetup?"<<endl;
    return filenm;
  }
  if( _obssetup->_obscfg == 0 ) {
    clog<<"UFAcqCtrl::discardObs> unable to abort obs, no obsconf?"<<endl;
    return filenm;
  }

  _obssetup->_obscfg->discardObs();
  filenm = submitDhsc(replicant);
  return filenm;
}

// stop observation (connect to ufedtd if necessary)
//string UFAcqCtrl::stopObs(UFMCE4Config* mce4, UFFlamObsConf*& obscfg,
//			  UFStrings*& prmFITS, UFStrings*& extFITS) {
string UFAcqCtrl::stopObs(UFMCE4Config* mce4) {
  string filenm;
  if( _obssetup == 0 ) {
    clog<<"UFAcqCtrl::stopObs> unable to stop obs, no obssetup?"<<endl;
    return filenm;
  }
  if( _obssetup->_obscfg == 0 ) {
    clog<<"UFAcqCtrl::stopObs> unable to stop obs, no obsconf?"<<endl;
    return filenm;
  }

  _obssetup->_obscfg->stopObs();
  filenm = submitEdtd();
  return filenm;
}

string UFAcqCtrl::stopObs(UFSocket* replicant) {
  string filenm;
  if( replicant == 0 ) {
    clog<<"UFAcqCtrl::stopObs> unable to abort obs, no dhsclient socket?"<<endl;
    return filenm;
  }
  if( _obssetup == 0 ) {
    clog<<"UFAcqCtrl::startObs> unable to abort obs, no obssetup?"<<endl;
    return filenm;
  }
  if( _obssetup->_obscfg == 0 ) {
    clog<<"UFAcqCtrl::stopObs> unable to abort obs, no obsconf?"<<endl;
    return filenm;
  }

  _obssetup->_obscfg->stopObs();
  filenm = submitDhsc(replicant);
  return filenm;
}

// stop observation (connect to ufedtd if necessary) { }
//string UFAcqCtrl::abortObs(UFMCE4Config* mce4, UFFlamObsConf*& obscfg) {
string UFAcqCtrl::abortObs(UFMCE4Config* mce4) {
  string filenm;
  if( _obssetup == 0 ) {
    clog<<"UFAcqCtrl::abortObs> unable to abort obs, no obssetup?"<<endl;
    return filenm;
  }
  if( _obssetup->_obscfg == 0 ) {
    clog<<"UFAcqCtrl::abortObs> unable to abort obs, no obsconf?"<<endl;
    return filenm;
  }

  _obssetup->_obscfg->abortObs();
  filenm = submitEdtd();
  return filenm;
}

string UFAcqCtrl::abortObs(UFSocket* replicant) {
  string filenm;
  if( replicant == 0 ) {
    clog<<"UFAcqCtrl::abortObs> unable to abort obs, no dhsclient socket?"<<endl;
    return filenm;
  }
  if( _obssetup == 0 ) {
    clog<<"UFAcqCtrl::abortObs> unable to abort obs, no obssetup?"<<endl;
    return filenm;
  }
  if( _obssetup->_obscfg == 0 ) {
    clog<<"UFAcqCtrl::abortObs> unable to abort obs, no obsconf?"<<endl;
    return filenm;
  }

  _obssetup->_obscfg->abortObs();
  filenm = submitDhsc(replicant);
  return filenm;
}

// allocate & fetch/set FITS header for obbs. datalabel:
UFStrings* UFAcqCtrl::fitsHdrPrm(const string& datalabel, UFFITSheader& fh, const string& epics) {
  if( _clientFITS == 0 ) {
    clog<<"UFAcqCtrl::fitsHdrPrm> no FITS client connection?"<<endl;
    return 0;
  }
  // primary header (initial or final vals)
  UFStrings* fitsHdr = _clientFITS->fetchAllFITS(_fitsoc, 0.5, _observatory);
  if( fitsHdr ) {
    fh.add(fitsHdr);
    clog<<"UFAcqCtrl::fitsHdrPrm> Primary: "<<fitsHdr->name()<<endl;
  }
  else {
    clog<<"UFAcqCtrl::fitsHdrPrm> failed FITS fetch..."<<endl;
  }
  delete fitsHdr;
  return fh.Strings();
}

UFStrings* UFAcqCtrl::fitsHdrExt(const string& datalabel, const string& epics) {
  // extension header
  return new UFStrings("F2FITSExt");
}

int UFAcqCtrl::connectFITS(string& agenthost, const string& exclude) { 
  if( agenthost.length() < 1 && _agthost == "" )
    _agthost = agenthost = UFRuntime::hostname();
  if( _agthost != agenthost )
    _agthost = agenthost;

  UFFITSClient::AgentLoc agentLocs;
  agentLocs.clear();
  map< string, int >::iterator it = _portmap.begin();
  for( ; it != _portmap.end(); ++it ) {
    string agt = it->first;
    if( agt.find("FITS") == string::npos )
      continue;
    int port = it->second;
    //clog << "UFAcqCtrl::connectFITS> "<<agt<<", port: "<<port<<endl;
    string agthost = agenthost;
    size_t atpos = agt.find("@");
    if( atpos != string::npos )
      agthost = agt.substr(1+atpos);
    if( agt.find(exclude) == string::npos ) {
      agentLocs[agt] = new UFFITSClient::AgentLocation(agthost, port);
      //clog << "UFAcqCtrl::connectFITS> "<<agt<<", host: "<<agthost<<", port: "<<port<<endl;
    }
    else {
      clog << "UFAcqCtrl::connectFITS> exclude: "<<agt<<endl;
    }
  }
  bool block = false;
  _clientFITS = new UFFITSClient(exclude);
  clog << "UFAcqCtrl::connectFITS> connecting to other Device Agents on host: "
       <<agenthost<<" (block="<<block<<")"<<endl;
  int nfits= _clientFITS->connectAgents(agentLocs, _fitsoc, block);
  if( nfits <= 0 ) {
    clog<<"UFGemMCE4Agent::AgentsConnected> ERROR: no connections to other device agents !"<<endl;
  }
  return nfits;
}

// AcqCtrl is a clientsocket subclass -- connected to the ufgedtd...
int UFAcqCtrl::connectEdtd(const string& clientname, string& frmhost, int frmport, bool block) {
  if( frmhost.length() < 1 && _frmhost == "" )
    _frmhost = frmhost = UFRuntime::hostname();
  if( _frmhost != frmhost )
    _frmhost = frmhost;
  string agt = "ufgedtd@" + _frmhost;
  //if( frmport > 0 ) _portmap[agt] = frmport;
  //if( frmport < 0 ) _portmap[agt] = 52001;
  clog<<"UFAcqCtrl::connectEdtd> frmhost: "<<frmhost<<", frmport: "<<frmport<<endl;
  UFTimeStamp greet(clientname);
  int socfd = connect(frmhost, frmport, block);
  if( socfd < 0 ) {
    clog<<"UFAcqCtrl::connectEdtd> connection failed..."<<endl;
    return socfd;
  }
  clog<<"UFAcqCtrl::connectEdtd> send (clientname) greeting: "<<clientname<<endl;
  int ns = greet.sendTo(*this);
  clog<<"UFAcqCtrl::connectEdtd> sent ns: "<<ns<<", (clientname) greeting: "<<greet.name()<<endl;
  UFProtocol* reply = UFProtocol::createFrom(*this);
  if( reply == 0 ) {
    clog<<"UFAcqCtrl::connectEdtd> greeting failed..."<<endl;
    return 0;
  }
  clog<<"UFAcqCtrl::connectEdtd> greeting reply: "<<reply->name()<<endl;
  delete reply;
  return socfd;
}

UFInts* UFAcqCtrl::simAcq(const string& name, int index) {
  return new UFInts("SimInternal", (int*)0, 2028*2048);
}

int UFAcqCtrl::fitsInt(const string& key, char* fitsHdr) {
  char entry[81]; memset(entry, 0, sizeof(entry));
  strncpy(entry, fitsHdr, 80); fitsHdr += 80;
  string s = entry;
  int pos = s.find(key);
  if( pos == (int)string::npos )
    return -1;
  pos = 1+s.find("=", pos);
  if( pos == (int)string::npos )
    return -1;
  s = s.substr(1+pos);  
  char* c = (char*)s.c_str();
  while( *c == ' ' ) ++c;  // eliminate leading white sp
  s = c; 
  pos = s.find(" ");
  if( pos == (int)string::npos )
    pos = s.find("/");
  if( pos !=  (int)string::npos ) {
    char cv[1+pos]; memset(cv, 0, 1+pos); strncpy(cv, s.c_str(), pos);
    return atoi(cv);
  }
  return -1;
}

// float/double key value
double UFAcqCtrl::fitsFlt(const string& key, char* fitsHdr) { 
  return -1.0;
}

// string key value
string UFAcqCtrl::fitsStr(const string& key, char* fitsHdr) { 
  return "null";
}

// return FITS header from opened file, seeking to start of first data frame
int UFAcqCtrl::openFITS(const string& filename, UFStrings*& fitsHdr, 
			    int& width, int& height, int& frmtotal) {
  fitsHdr = 0;
  frmtotal = 0;
  struct stat st;
  int rs = ::stat(filename.c_str(), &st);
  if( rs < 0 ) {
    clog<<"UFAcqCtrl::openFITS> unable to stat: "<<filename<<endl;
    return 0;
  }
  int totalbytes = st.st_size;

  int fd = ::open(filename.c_str(), O_RDONLY);
  if( fd <= 0 ) {
    clog<<"UFAcqCtrl::openFITS> unable to open: "<<filename<<endl;
    return 0;
  }

  char fitshd1[80];
  char end[4]; ::memset(end, 0, sizeof(end));
  char naxis[7]; ::memset(naxis, 0, sizeof(naxis));
  int w= 0, h= 0;

  strcpy(end, "END");
  int ne = strlen(end);
  int na = strlen("NAXIS0");
  string s, key;
  int fitsbytes= 0; // allow 10x2880 (10 headers)?
  UFFITSheader ufits;
  do {
    ::memset(fitshd1, 0, sizeof(fitshd1));
    fitsbytes += ::read(fd, fitshd1, 80);
    memcpy(end, fitshd1, ne);
    s = end;
    memcpy(naxis, fitshd1, na);
    key = naxis;
    if( key == "NAXIS1" )
      w = fitsInt(key, fitshd1);
    if( key == "NAXIS2" )
      h = fitsInt(key, fitshd1);
    // add entry to header
    string fitshdr = fitshd1;
    ufits.add(fitshdr);
  } while( s != "END" && fitsbytes < 10*2880 );
  if( fitsbytes >= 10*2880 && s != "END" ) {
    clog<<"UFAcqCtrl::openFITS> sorry, improper or no FITS header?"<<endl;
    ::close(fd);
    return 0;
  }
  if( w <= 0 || h <= 0 ) {
    clog<<"UFAcqCtrl::openFITS> sorry FITS header lacks NAXIS1,2"<<endl;
    ::close(fd);
    return 0;
  }
  // fits image data is Big-Endian:
  if( _verbose )
    clog<<"UFAcqCtrl::openFITS> header fitbytes= "<<fitsbytes<<", total= "<<totalbytes<<endl;
  int frmbytes = totalbytes - fitsbytes;
  frmtotal = frmbytes / (w*h*sizeof(int));
  width = w; height = h;
  if( _verbose )
    clog<<"UFAcqCtrl::openFITS> header indicates: w= "<<w<<", h= "<<h
        <<", total frames= "<<frmtotal<<endl;

  fitsHdr = ufits.Strings(filename);
  return fd;
}
  
// return FITS data from open file, seeking to next data frame
UFInts* UFAcqCtrl::seekFITSData(const int fd, const int width, const int height) {
  int* frame = new int[width*height];
  int nb = ::read(fd, frame, width*height*sizeof(int));
  if( nb < width*height*(int)sizeof(int) ) {
    clog<<"UFAcqCtrl::seekFITSData> unable to read full image data, nb= "<<nb
        <<", expected "<< width*height*sizeof(int)<<endl;
    delete frame;
    return 0;
  }    
  return new UFInts("ImageFrame", (const int*) frame);
}

/*
int UFAcqCtrl::fitsHdrs(const string& datalabel, deque< UFStrings* >& fits) {
  multimap< string, UFStrings* >::iterator it = _FITSheaders.lower_bound(datalabel);
  multimap< string, UFStrings* >::iterator stop = _FITSheaders.upper_bound(datalabel);
  fits.clear();
  while( it != stop )
    fits.push_back(it->second);

  return (int) fits.size();
}
*/
#endif // __UFAcqCtrl_cc__

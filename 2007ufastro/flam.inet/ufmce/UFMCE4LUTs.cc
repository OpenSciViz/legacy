#if !defined(__UFMCE4LUTs_cc__)
#define __UFMCE4LUTs_cc__ "$Name:  $ $Id: UFMCE4LUTs.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFMCE4LUTs_cc__;

#include "UFMCE4LUTs.h"
#include "UFRuntime.h"
#include "uffits.h"

#include "sys/types.h"
#include "sys/stat.h"
#include "sys/wait.h"
#include "netinet/in.h"
#include "fcntl.h"
#include "unistd.h"
#include "fstream.h"

#define _MAX2_(a,b) ((a)>(b)) ? (a) : (b)
#define _MAX3_(a,b,c) (_MAX2_(_MAX2_(a,b),_MAX2_(b,c)))
#define _MIN2_(a,b) ((a)<(b)) ? (a) : (b)
#define _MIN3_(a,b,c) (_MIN2_(_MIN2_(a,b),_MIN2_(b,c)))
 
//(from frank) David, this is basic LUT for Trecs & CC single sample mode:
// Sort pixels using LHS:
// for (iPix=0; iPix<nPixPerBuf; iPix++) 
//    OutBufP[LUT[iPix]] = InBufP[iPix]; 
UFInts* UFMCE4LUTs::allocADCChanLUT(int nCol, int nRow, int nChan) {
  const int nFast= nCol / nChan;
  const int ntPix= nCol * nRow;
  int* LUT= new int[ntPix]; memset(LUT, 0, ntPix*sizeof(int));

  clog<<"UFMCE4LUTs::allocADCChanLUT> ntPix: "<<ntPix<<endl;
  for(int iRow= 0, iPix= 0; iRow<nRow; iRow++) {
    for(int iFast= 0; iFast < nFast; iFast++) {
      for (int iChan= 0; iChan<nChan; iChan++, iPix++ ) {
        LUT[iPix] = iRow*nCol + iChan*nFast + iFast;
	if( LUT[iPix] < 0 ) clog<<"UFMCE4LUTs::allocADCChanLUT> iPix "<<iPix<<", LUT: "<<LUT[iPix]<<endl;
      }
    }
  }

  // use shallow UFInts ctor:
  UFInts* ufi = new UFInts("MCE4ADCChans.lut", (const int*) LUT, ntPix, true);
  return ufi;
}

// each of the 32 mux's should be organized into width= 64 by height= 2048 rectangle of pixels 
// with origin at lower-left corner. each mux rectangle placed side-to-side left-to-right (1-32)
// results in a 2048x2048 pixel image:
UFInts* UFMCE4LUTs::allocMUXLUT(const int nCol, const int nRow, const int nMux, const int nChan) {
  const int ntPix= nCol*nRow; // muxPix= 64*nRow;
  int* LUT = new int[ntPix]; memset(LUT, 0, ntPix*sizeof(int));
  //int* mux = new int[nMux*muxPix]; memset(mux, 0, nMux*muxPix*sizeof(int));
  int mux[32][16]; memset(mux,0,32*16);
  long i = 0;
  int row = 0, col = 0;
  while (i < ntPix) {
    //Read in 16 elements to each mux
    for (int j = 0; j < 16; j++) {
      for (int l = 0; l < 32; l++) {
	if (l % 2 == 0) mux[j*2][l/2] = i;
        if (l % 2 == 1) mux[j*2+1][l/2] = i;
        i++;
      }
    }
    //Add new pixels to image
    for (int j = 0; j < 32; j++) {
      for (int l = 0; l < 16; l++) {
        LUT[j*64+col+(l+row)*2048] = mux[j][l];
      }
    }
    row+=16;
    if (row == 2048) {
      row = 0;
      col++;
    }
  }
  /*
  for( int imux= 0; imux < nMux ; imux++ ) { // 32 iterations
    int offset = imux * muxPix; 
    for( int mc = 0; mc < muxPix/nChan; mc++ ) { // 64*nRow / 16 iterations
      for( int i= 0; i < nChan; ++i ) { // 16 iterations
        int idx = i + mc*nChan;
        int midx = imux*muxPix + idx;
        mux[midx] = offset + 2*idx + mc*(nMux-2)*nChan;
        if( imux == 31 && ((midx % 64) == 0) ) {
	  clog<<"UFMCE4LUTs::allocMUXLUT> Mux0 midx: "<<midx<<" == "<<mux[imux*muxPix + idx]<<endl;
	}
      }
      //clog<<"continue with enter key."<<endl; getchar();
    }
    memcpy(&LUT[imux*muxPix], &mux[imux*muxPix], muxPix*sizeof(int));
  }
  delete [] mux;
  */
  // use shallow UFInts ctor:
  UFInts* ufi = new UFInts("MCE4MUXs.lut", (const int*) LUT, ntPix, true);
  return ufi;
} 

UFInts* UFMCE4LUTs::allocMUXLUTlab(const int nCol, const int nRow, const int nMux, const int nChan) {
  const int ntPix= nCol*nRow; // muxPix= 64*nRow;
  int* LUT = new int[ntPix]; memset(LUT, 0, ntPix*sizeof(int));
  //int* mux = new int[nMux*muxPix]; memset(mux, 0, nMux*muxPix*sizeof(int));
  int mux[32][16]; memset(mux,0,32*16);
  long i = 0;
  int row = 0, col = 0;
  while (i < ntPix) {
    //Read in 16 elements to each mux
    for (int j = 0; j < 16; j++) {
      for (int l = 0; l < 32; l++) {
        if (l % 2 == 0) mux[j*2][l/2] = i;
        if (l % 2 == 1) mux[j*2+1][l/2] = i;
        i++;
      }
    }
    //Add new pixels to image
    for (int j = 0; j < 32; j++) {
      for (int l = 0; l < 16; l++) {
        LUT[l+row+(2047-j*64-col)*2048] = mux[j][l];
      }
    }
    row+=16;
    if (row == 2048) {
      row = 0;
      col++;
    }
  }
  UFInts* ufi = new UFInts("MCE4MUXsLab.lut", (const int*) LUT, ntPix, true);
  return ufi;
}

UFInts* UFMCE4LUTs::allocMUXLUTside(const int nCol, const int nRow, const int nMux, const int nChan) {
  const int ntPix= nCol*nRow; // muxPix= 64*nRow;
  int* LUT = new int[ntPix]; memset(LUT, 0, ntPix*sizeof(int));
  //int* mux = new int[nMux*muxPix]; memset(mux, 0, nMux*muxPix*sizeof(int));
  int mux[32][16]; memset(mux,0,32*16);
  long i = 0;
  int row = 0, col = 0;
  while (i < ntPix) {
    //Read in 16 elements to each mux
    for (int j = 0; j < 16; j++) {
      for (int l = 0; l < 32; l++) {
        if (l % 2 == 0) mux[j*2][l/2] = i;
        if (l % 2 == 1) mux[j*2+1][l/2] = i;
        i++;
      }
    }
    //Add new pixels to image
    for (int j = 0; j < 32; j++) {
      for (int l = 0; l < 16; l++) {
        LUT[2047-l-row+(j*64+col)*2048] = mux[j][l];
      }
    }
    row+=16;
    if (row == 2048) {
      row = 0;
      col++;
    }
  }
  UFInts* ufi = new UFInts("MCE4MUXsSide.lut", (const int*) LUT, ntPix, true);
  return ufi;
}

UFInts* UFMCE4LUTs::allocMUXLUTup(const int nCol, const int nRow, const int nMux, const int nChan) {
  const int ntPix= nCol*nRow; // muxPix= 64*nRow;
  int* LUT = new int[ntPix]; memset(LUT, 0, ntPix*sizeof(int));
  //int* mux = new int[nMux*muxPix]; memset(mux, 0, nMux*muxPix*sizeof(int));
  int mux[32][16]; memset(mux,0,32*16);
  long i = 0;
  int row = 0, col = 0;
  while (i < ntPix) {
    //Read in 16 elements to each mux
    for (int j = 0; j < 16; j++) {
      for (int l = 0; l < 32; l++) {
        if (l % 2 == 0) mux[j*2][l/2] = i;
        if (l % 2 == 1) mux[j*2+1][l/2] = i;
        i++;
      }
    }
    //Add new pixels to image
    for (int j = 0; j < 32; j++) {
      for (int l = 0; l < 16; l++) {
        LUT[2047-l-row+(2047-j*64-col)*2048] = mux[j][l];
        //LUT[l+row+(j*64+col)*2048] = mux[j][l];
      }
    }
    row+=16;
    if (row == 2048) {
      row = 0;
      col++;
    }
  }
  UFInts* ufi = new UFInts("MCE4MUXsUp.lut", (const int*) LUT, ntPix, true);
  return ufi;
}

int UFMCE4LUTs::writeFITS(const UFInts& lut, bool swap) {
  int nelem = lut.elements();
  const char* cpix = lut.valData();
  int* ipix = (int*) cpix;
  string filenm = lut.name();
  filenm += ".fits";
  ::umask(0);
  int fd = ::open(filenm.c_str(), O_SYNC | O_CREAT | O_RDWR, S_IRWXU | S_IRGRP | S_IROTH);
  if( fd < 0 ) return fd;
  if( UFRuntime::littleEndian() && swap ) {
    clog<<"UFMCE4LUTs::writeFITS> little-endian system, swapping to big-endian for fits file, nelem: "<<nelem<<endl;
    union { int i; char c[4]; } ictmp;
    for( int i= 0; i < nelem; ++i ) {
      //ipix[i] = ntohl(ipix[i]);
      ictmp.i = ipix[i];
      char c = ictmp.c[0]; ictmp.c[0] = ictmp.c[3]; ictmp.c[3] = c;
      c = ictmp.c[1]; ictmp.c[1] = ictmp.c[2]; ictmp.c[2] = c;
      ipix[i] = ictmp.i;
    }
  }
  else {
    clog<<"UFMCE4LUTs::writeFITS> big-endian system, nelem: "<<nelem<<endl;
  } 
  clog<<"UFMCE4LUTs::writeFITS> file: "<<filenm<<endl;
  size_t nb = ::write(fd, fits2048, sizeof(fits2048)-1); // ????
  nb += ::write(fd, ipix, nelem*sizeof(int));
#if defined(LINUX)
  const size_t npixpad = (size_t) ((1 - (((1.0*nb) / 2880.0) - (nb/2880))) * 2880);
#else
  const size_t npixpad = (size_t) ((1 - (((1.0*nb) / 2880.0) - (nb/2880))) * 2880) + 1;
#endif
  char* pad = new char[npixpad]; memset(pad, 0, npixpad);
  nb += ::write(fd, pad, npixpad); delete [] pad;
  return nb;
}

int UFMCE4LUTs::writeBigEndianFile(const UFInts& lut) {
  int nelem = lut.elements();
  const char* cpix = lut.valData();
  int* ipix = (int*) cpix;
  string filenm = lut.name();
  filenm += "BE";
  ::umask(0);
  int fd = ::open(filenm.c_str(), O_SYNC | O_CREAT | O_RDWR, S_IRWXU | S_IRGRP | S_IROTH);
  if( fd < 0 ) return fd;
  if( UFRuntime::littleEndian() ) {
    clog<<"UFMCE4LUTs::writeBigEndianFile> little-endian system, swapping to big-endian for file, nelem: "<<nelem<<endl;
    union { int i; char c[4]; } ictmp;
    for( int i= 0; i < nelem; ++i ) {
      ictmp.i = ipix[i];
      char c = ictmp.c[0]; ictmp.c[0] = ictmp.c[3]; ictmp.c[3] = c;
      c = ictmp.c[1]; ictmp.c[1] = ictmp.c[2]; ictmp.c[2] = c;
      ipix[i] = ictmp.i;
    }
  }
  clog<<"UFMCE4LUTs::writeBigEndianFile> write nelem: "<<nelem<<" to "<<filenm<<endl;
  return ::write(fd, ipix, nelem*sizeof(int));
}

int UFMCE4LUTs::writeLittleEndianFile(const UFInts& lut) {
  int nelem = lut.elements();
  const char* cpix = lut.valData();
  int* ipix = (int*) cpix;
  string filenm = lut.name();
  filenm += "LE";
  ::umask(0);
  int fd = ::open(filenm.c_str(), O_SYNC | O_CREAT | O_RDWR, S_IRWXU | S_IRGRP | S_IROTH);
  if( fd < 0 ) return fd;
  if( !UFRuntime::littleEndian() ) {
    clog<<"UFMCE4LUTs::writeLittleEndianFile> big-endian system, swapping to little-endian for file, nelem: "<<nelem<<endl;
    union { int i; char c[4]; } ictmp;
    for( int i= 0; i < nelem; ++i ) {
      ictmp.i = ipix[i];
      char c = ictmp.c[0]; ictmp.c[0] = ictmp.c[3]; ictmp.c[3] = c;
      c = ictmp.c[1]; ictmp.c[1] = ictmp.c[2]; ictmp.c[2] = c;
      ipix[i] = ictmp.i;
    }
  }
  clog<<"UFMCE4LUTs::writeLittleEndianFile> write nelem: "<<nelem<<" to "<<filenm<<endl;
  return ::write(fd, ipix, nelem*sizeof(int));
}

UFInts* UFMCE4LUTs::readLUTandSwap4(const string& file) {
  int fd = ::open(file.c_str(), O_RDONLY);
  if( fd < 0 ) return 0;
  const int nelem = 2048*2048;
  int* lut = new int[nelem];
  int nb = ::read(fd, lut, nelem*sizeof(int));
  clog<<"UFMCE4LUTs::readLUTandSwap4> read and swap bytes from file: "<<file<<endl;
  if( nb <= 0 ) { delete [] lut; return 0; }
  union { int i; char c[4]; } ictmp;
  for( int i= 0; i < nelem; ++i ) {
    ictmp.i = lut[i];
    char c = ictmp.c[0]; ictmp.c[0] = ictmp.c[3]; ictmp.c[3] = c;
    c = ictmp.c[1]; ictmp.c[1] = ictmp.c[2]; ictmp.c[2] = c;
    lut[i] = ictmp.i;
  }
  UFInts* ufi = new UFInts(file, (const int*) lut, nelem, true);
  return ufi;
}

int UFMCE4LUTs::main(int argc, char** argv, char** envp) {
  string file, opt = "-mux", usage = "usage: ufgmce4d [-help] [-mux] [-adc] [-swap filename]";
  if( argc > 1 ) opt = argv[1];
  if( opt.find("help") != string::npos ) {
    clog<<usage<<endl;
    return 0;
  }
  UFInts* lut= 0;
  bool swap= false;
  if( opt.find("swap") != string::npos ) {
    if( argc <= 2 ) {
      clog<<"UFMCE4LUTs::main> -swap[file] option should be followed by (full-path) filename"<<endl;
      return -1;
    }
    file = argv[2]; 
    lut = readLUTandSwap4(file);
    if( lut == 0 ) {
      clog<<"UFMCE4LUTs::main> failed to read file? "<<file<<endl;
      return -1;
    }
    if( UFRuntime::littleEndian() )
      file += "LE";
    else
      file += "BE";

    lut->rename(file);
    clog<<"UFMCE4LUTs::main> swap output file: "<<lut->name()<<endl;
    swap = true;
  }
  else if( opt.find("adc") != string::npos )
    lut = allocADCChanLUT();
  else if (opt.find("lab") != string::npos)
    lut = allocMUXLUTlab();
  else if (opt.find("side") != string::npos)
    lut = allocMUXLUTside();
  else if (opt.find("up") != string::npos)
    lut = allocMUXLUTup();
  else // assume mux
    lut = allocMUXLUT();
  
  if( lut == 0 )
    return -1;

  int nw = writeFITS(*lut, !swap); // if not swapped here, and littlendian fits should swap...
  if( !swap ) { // write both 
    nw += writeBigEndianFile(*lut);
    nw += writeLittleEndianFile(*lut);
    return 0;
  }
  else if( file.empty() ) { // no output file?
    clog<<usage<<endl;
    return -1;
  }
  // just write swapped result
  ::umask(0);
  int fd = ::open(file.c_str(), O_SYNC | O_CREAT | O_RDWR, S_IRWXU | S_IRGRP | S_IROTH);
  if( fd < 0 ) return fd;
  clog<<"UFMCE4LUTs::main> write swapped bytes to: "<<lut->name()<<endl;
  nw += ::write(fd, (int*) lut->valData(), lut->elements()*sizeof(int));
  return 0;
}

#endif // __UFMCE4LUTs_cc__

#if !defined(__UFLS33xConfig_h__)
#define __UFLS33xConfig_h__ "$Name:  $ $Id: UFLS33xConfig.h,v 0.2 2006/08/28 21:26:07 hon Exp $"
#define __UFLS33xConfig_H__(arg) const char arg##UFLS33xConfig_h__rcsId[] = __UFLS33xConfig_h__;

#include "UFDeviceConfig.h"
#include "map"

class UFLS33xConfig : public UFDeviceConfig {
public:
  static const float _MaxSetPnt, _NominalSetPnt;
  static const int _BadSetPnt, _BadPID;
  static const string _MaxAllowedCmd, _NominalSetPntCmd;
  UFLS33xConfig(const string& name= "UnknownLS33x@DefaultConfig");
  UFLS33xConfig(const string& name, const string& tshost, int tsport);
  inline virtual ~UFLS33xConfig() {}

  // override these virtuals:
  virtual vector< string >& UFLS33xConfig::queryCmds();
  virtual vector< string >& UFLS33xConfig::actionCmds();
  // return -1 if invalid, 0 if valid but no reply expected,
  // n > 0 if valid and n reply strings expected:
  virtual int validCmd(const string& c);

  // the default behavior is ok here:
  //virtual string terminator();
  //virtual string prefix();

  // overide these:
  virtual UFStrings* status(UFDeviceAgent* da);
  virtual UFStrings* statusFITS(UFDeviceAgent* da);

  // set F2 default PID:
  static int setPID(float P= 1000.0, float I= 500.0, float D= 1000.0);

  // update SAD: map key should be epics sadchan, pair value shoulde be (ls332cmd, reply) strings
  static string updateSAD(const string& instrum, std::map< string, std::pair<string, string> >& sadcmds);

  // set F2 standard calibration curve
  static int setCrvFlam2Detector();
  static int setInTypFlam2Detector();
};

#endif // __UFLS33xConfig_h__

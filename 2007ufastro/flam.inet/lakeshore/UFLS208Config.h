#if !defined(__UFLS208Config_h__)
#define __UFLS208Config_h__ "$Name:  $ $Id: UFLS208Config.h,v 0.1 2002/06/26 20:34:05 trecs beta $"
#define __UFLS208Config_H__(arg) const char arg##UFLS208Config_h__rcsId[] = __UFLS208Config_h__;

#include "UFDeviceConfig.h"

class UFLS208Config : public UFDeviceConfig {
public:
  UFLS208Config(const string& name= "UnknownLS208@DefaultConfig");
  UFLS208Config(const string& name, const string& tshost, int tsport);
  inline virtual ~UFLS208Config() {}

  // override these virtuals:
  virtual vector< string >& UFLS208Config::queryCmds();
  virtual vector< string >& UFLS208Config::actionCmds();
  // return -1 if invalid, 0 if valid but no reply expected,
  // n > 0 if valid and n reply strings expected:
  virtual int validCmd(const string& c);

  virtual string terminator();

  // the default behavior is ok here:
  //virtual string prefix();

  virtual UFStrings* status(UFDeviceAgent* da);
  virtual UFStrings* statusFITS(UFDeviceAgent* da);
};

#endif // __UFLS208Config_h__

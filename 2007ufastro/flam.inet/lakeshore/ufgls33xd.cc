#if !defined(__ufgls33xd_cc__)
#define __ufgls33xd_cc__ "$Name:  $ $Id: ufgls33xd.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __ufgls33xd_cc__;

#include "UFGemLakeShore33xAgent.h"

int main(int argc, char** argv) {
  try {
    UFGemLakeShore33xAgent::main(argc, argv);
  }
  catch( std::exception& e ) {
    clog<<"ufgls33xd> exception in stdlib: "<<e.what()<<endl;
    return -1;
  }
  catch( ... ) {
    clog<<"ufgls33xd> unknown exception..."<<endl;
    return -2;
  }
  return 0;
}
      
#endif // __ufgls33xd_cc__

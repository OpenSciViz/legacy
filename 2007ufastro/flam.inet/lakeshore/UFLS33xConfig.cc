#if !defined(__UFLS33xConfig_cc__)
#define __UFLS33xConfig_cc__ "$Name:  $ $Id: UFLS33xConfig.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFLS33xConfig_cc__;

#include "UFLS33xConfig.h"
#include "UFGemLakeShore33xAgent.h"
#include "UFFITSheader.h"

// global/statics:
const float UFLS33xConfig::_NominalSetPnt= 71.0;
const string UFLS33xConfig::_NominalSetPntCmd= "SETP 1, 71.0";
const float UFLS33xConfig::_MaxSetPnt= 85.1;
const string UFLS33xConfig::_MaxAllowedCmd= "SETP 1, 85.1"; // control loop 1
const int UFLS33xConfig::_BadSetPnt= 85;
const int UFLS33xConfig::_BadPID= -10;

UFLS33xConfig::UFLS33xConfig(const string& name) : UFDeviceConfig(name) {}

UFLS33xConfig::UFLS33xConfig(const string& name,
			     const string& tshost,
			     int tsport) : UFDeviceConfig(name, tshost, tsport) {}

// LS33x terminator is new-line:
//string UFLS33xConfig::terminator() { return "\n"; }

// LS33x prefix is none:
//string UFLS33xConfig::prefix() { return ""; }

// update SAD: map key should be epics sadchan, pair value shoulde be (ls332cmd, reply) strings
string UFLS33xConfig::updateSAD(const string& instrum, std::map< string, std::pair<string, string> >& sadmap) {
  sadmap.clear();
  string key; pair< string, string > val;

  // fectch the current temperatures first:  
  key = instrum + ":sad:CAMBDETC"; val = pair< string, string >("KRDG?B", "null"); sadmap[key] = val;
  key = instrum + ":sad:CAMABENC"; val = pair< string, string >("KRDG?A", "null"); sadmap[key] = val;

  key = instrum + ":sad:CAMBSETP"; val = pair< string, string >("SETP?2", "null"); sadmap[key] = val;
  key = instrum + ":sad:CAMBHTPW"; val = pair< string, string >("MOUT?2", "null"); sadmap[key] = val;
  key = instrum + ":sad:CAMBPID"; val = pair< string, string >("PID?2", "null"); sadmap[key] = val;

  key = instrum + ":sad:CAMASETP"; val = pair< string, string >("SETP?1", "null"); sadmap[key] = val;
  key = instrum + ":sad:CAMAHTPW"; val = pair< string, string >("MOUT?1", "null"); sadmap[key] = val;
  key = instrum + ":sad:CAMAPID"; val = pair< string, string >("PID?1", "null"); sadmap[key] = val;
  // note only control loop 1 supports power ranges (and assume control 1 is assigned to channel A)
  key = instrum + ":sad:CAMARANG"; val = pair< string, string >("RANGE?", "null"); sadmap[key] = val;

  key = instrum + ":sad:CAMBAPWR"; val = pair< string, string >("AOUT?", "null"); sadmap[key] = val;
  key = instrum + ":sad:CAMAAPWR"; val = pair< string, string >("HTR?", "null"); sadmap[key] = val;
  key = instrum + ":sad:LOCK"; val = pair< string, string >("LOCK?", "null"); sadmap[key] = val;
  key = instrum + ":sad:ANALOG"; val = pair< string, string >("ANALOG?", "null"); sadmap[key] = val;

  std::map< string, std::pair<string, string> >::iterator it = sadmap.begin();
  string sadchan, query, ls332val;
  string replyAB= "+301.11;+299.99", replyA = "+301.11", replyB= "+299.99";
  for( ; it != sadmap.end(); ++it ) {
    sadchan = it->first;
    query = it->second.first;
    _devIO->submit(query, ls332val, 0.1);
    it->second.second = ls332val;
    clog<<"UFLS33xConfig::updateSAD> query: "<<query<<" == "<<ls332val<<endl;
    if( !instrum.empty() && instrum != "false" )
      UFGemDeviceAgent::sendEpics(sadchan, ls332val);
    clog<<"UFLS33xConfig::updateSAD> "<<sadchan<<" == "<<ls332val<<endl;
    if( sadchan.find("CAMA") != string::npos )
      replyA = ls332val;
    else 
      replyB = ls332val;
  } 
  replyAB = replyA + ";" + replyB;
  return replyAB;
}

vector< string >& UFLS33xConfig::queryCmds() {
  if( _queries.size() > 0 ) // already set
    return _queries;

  _queries.push_back("ANALOG?"); _queries.push_back("analog?");
  _queries.push_back("AOUT?"); _queries.push_back("aout?");
  _queries.push_back("CRVHDR?"); _queries.push_back("crvhdr?");
  _queries.push_back("CRVPT?"); _queries.push_back("crvpt?");
  _queries.push_back("CMODE?"); _queries.push_back("cmode?");
  _queries.push_back("CSET?"); _queries.push_back("cset?");
  _queries.push_back("HTR?"); _queries.push_back("htr?");
  _queries.push_back("INCRV?"); _queries.push_back("incrv?");
  _queries.push_back("INSET?"); _queries.push_back("inset?");
  _queries.push_back("INTYPE?"); _queries.push_back("intype?");
  _queries.push_back("KRDG?A"); _queries.push_back("krdg?a"); // current sensor reading in kelvin
  _queries.push_back("KRDG?B"); _queries.push_back("krdg?b");// current sensor reading in kelvin
  _queries.push_back("LOCK?"); _queries.push_back("lock?");
  _queries.push_back("MOUT?"); _queries.push_back("mout?"); // manual heater power %
  _queries.push_back("PID?"); _queries.push_back("pid?"); // PID
  _queries.push_back("RANGE?"); _queries.push_back("range?");
  _queries.push_back("SETP?"); _queries.push_back("setp?a"); // Set Point in kelvin
  _queries.push_back("SRDG?A"); _queries.push_back("srdg?a");
  _queries.push_back("SRDG?B"); _queries.push_back("srdg?b");
  _queries.push_back("ZONE?"); _queries.push_back("zone?");
	
  return _queries;
}

vector< string >& UFLS33xConfig::actionCmds() {
  if( _actions.size() > 0 ) // already set
    return _actions;

  _actions.push_back("ANALOG"); _actions.push_back("analog");
  _actions.push_back("CRVHDR"); _actions.push_back("crvhdr");
  _actions.push_back("CRVPT"); _actions.push_back("crvpt");
  _actions.push_back("CRVSAV"); _actions.push_back("crvsav");
  _actions.push_back("CMODE"); _actions.push_back("cmode");
  _actions.push_back("CSET"); _actions.push_back("cset");
  _actions.push_back("INCRV"); _actions.push_back("incrv");
  _actions.push_back("INSET"); _actions.push_back("inset");
  _actions.push_back("INTYPE"); _actions.push_back("intype");
  _actions.push_back("LOCK"); _actions.push_back("lock");
  _actions.push_back("MOUT"); _actions.push_back("mout");
  _actions.push_back("PID"); _actions.push_back("pid");
  _actions.push_back("SETP"); _actions.push_back("setp");
  _actions.push_back("SRDG"); _actions.push_back("srdg");
  _actions.push_back("RANGE"); _actions.push_back("range");
  _actions.push_back("ZONE"); _actions.push_back("zone");

  return _actions;
}

int UFLS33xConfig::validCmd(const string& c) {
  //clog<<"UFLS33xConfig::validCmd> check cmd: "<<c<<endl;
  vector< string >& av = actionCmds();
  int i= 0, cnt = (int)av.size();
  while( i < cnt ) {
    if( c.find(av[i++]) != string::npos ) { 
      clog<<"UFLS33xConfig::validCmd> found cmd: "<<c<<endl;
      if( c.find("?") != string::npos ) // query ok, expect lakeshore reply
        return 1;
      int ppos= -1, pos = -1; 
      string val;
      if( c.find("SETP") != string::npos || c.find("Setp") != string::npos || c.find("setp") != string::npos )
	pos = 1+c.find(",");
      if( pos > 0 )  { // check that value is acceptable
	val = c.substr(pos);
	double kelvin = atof(val.c_str());
        if( kelvin > _MaxSetPnt )
	  return _BadSetPnt;
	else
          return 0;
	continue;
      }
      if( c.find("PID") != string::npos || c.find("Pid") != string::npos || c.find("pid") != string::npos )
        pos = 1+c.find(","); // parse 'PID 1, P, I, D' to insure I<P,D
      if( pos > 0 ) { // check that value is acceptable
        ppos = pos;
        pos  = 1+c.find(",", pos);
        val = c.substr(ppos, pos-ppos-1);
        double P = atof(val.c_str());
        ppos = pos;
        pos  = 1+c.find(",", pos);
        val = c.substr(ppos, pos-ppos-1);
        double I = atof(val.c_str());
        val = c.substr(pos);
        double D = atof(val.c_str());
        clog<<"UFLS33xConfig::validCmd> PID cmd: "<<P<<", "<<I<<", "<<D<<endl;
        if( I > P || I > D ) {
          clog<<"UFLS33xConfig::validCmd> suspicious PID (P>I>D) cmd: "<<P<<" > "<<I<<" > "<<D<<endl;
          //return _BadPID; // reject command?
	}
 	return 0;
      }
      return 0; // accept command, expect no reply from lakeshore
    }
  }
  
  vector< string >& qv = queryCmds();
  i= 0; cnt = (int)qv.size();
  while( i < cnt ) {
    if( c.find(qv[i++]) != string::npos )
      return 1;
  }
  clog<<"UFLS33xConfig::validCmd> !?Bad cmd: "<<c<<endl; 
  return -1;
}

UFStrings* UFLS33xConfig::status(UFDeviceAgent* da) {
  UFGemLakeShore33xAgent* da33x = dynamic_cast< UFGemLakeShore33xAgent* > (da);
  if( da33x->_verbose )
    clog<<"UFLS33xConfig::status> "<<da33x->name()<<endl;

  string a, b, spa, spb;
  da33x->getCurrent(a, b);
  da33x->getSetPnts(spa, spb);

  vector< string > vals;
  string s = "LS332Kelvin1"; s += " == " + a + " Kelvin";
  UFFITSheader::rmJunk(s); // use this conv. func. to eliminate '\n' & '\r' etc.
  vals.push_back(s);

  s = "LS332Kelvin2"; s += " == " + b + " Kelvin";
  UFFITSheader::rmJunk(s); // use this conv. func. to eliminate '\n' & '\r' etc.
  vals.push_back(s);

  s = "SetPoint A"; s += " == " + spa + " Kelvin";
  UFFITSheader::rmJunk(s); // use this conv. func. to eliminate '\n' & '\r' etc.
  vals.push_back(s);

  s = "SetPoint B"; s += " == " + spb + " Kelvin";
  UFFITSheader::rmJunk(s); // use this conv. func. to eliminate '\n' & '\r' etc.
  vals.push_back(s);

  return new UFStrings(da33x->name(), vals);
}

UFStrings* UFLS33xConfig::statusFITS(UFDeviceAgent* da) {
  UFGemLakeShore33xAgent* da33x = dynamic_cast< UFGemLakeShore33xAgent* > (da);
  if( da33x->_verbose )
    clog<<"UFLS33xConfig::status> "<<da33x->name()<<endl;

  string a, b, spa, spb;
  da33x->getCurrent(a, b);
  UFFITSheader::rmJunk(a); // use this conv. func. to eliminate '\n' & '\r' etc.
  UFFITSheader::rmJunk(b); // use this conv. func. to eliminate '\n' & '\r' etc.
  da33x->getSetPnts(spa, spb);
 
  map< string, string > valhash, comments;
  valhash["LS332K1"] = a; 
  comments["LS332K1"]  = "Kelvin";
  valhash["LS332K2"] = b; 
  comments["LS332K2"]  = "Kelvin";
  valhash["SetPntA"] = spa;
  comments["SetPntA"]  = "Kelvin";
  valhash["SetPntB"] = spb;
  comments["SetPntB"]  = "Kelvin";

  UFStrings* ufs = UFFITSheader::asStrings(valhash, comments);
  ufs->rename(da33x->name());

  return ufs;
}

int UFLS33xConfig::setPID(float P, float I, float D) {
  if( I > P || I > D )
    return _BadPID;

  strstream s;
  s << "PID 1, "<<P<<", "<<I<<", "<<D<<ends;
  string pidcmd = s.str(); delete s.str();
  _devIO->submit(pidcmd, 0.5);

  return 0;
}

int UFLS33xConfig::setInTypFlam2Detector() {
  // INTYPE <input>, <sensor type>, <compensation>[term]
  // Detector is on input B, type is Silicon Diode(= 0), compensation=0 for diodes
  string intyp = "INTYPE B,0,0,";
  clog<<"UFLS33xConfig::setInTypFlam2Detector> submit (and wait 10 sec.) "<<intyp<<endl;
  _devIO->submit(intyp, 10.0);
  return 0;
}

int UFLS33xConfig::setCrvFlam2Detector() {
  if( _devIO == 0 ) {
    clog<<"UFLS33xConfig::setCrvFlam2Detector> no device connection..."<<endl;
    return -1;
  }
  clog<<"UFLS33xConfig::setCrvFlam2Detector> installing Flam2 array temperature calibration curve into channel B as User Curve 21: "<<endl;
  vector< string > crv;
  crv.push_back("CRVHDR 21, FLAM2_DET, D70807, 2, 325.0, 1");
  crv.push_back("CRVPT 21,   1, 0.464943, 325.000");
  crv.push_back("CRVPT 21,   2, 0.476926, 320.000");
  crv.push_back("CRVPT 21,   3, 0.488897, 315.000");
  crv.push_back("CRVPT 21,   4, 0.500852, 310.000");
  crv.push_back("CRVPT 21,   5, 0.512791, 305.000");
  crv.push_back("CRVPT 21,   6, 0.524714, 300.000");
  crv.push_back("CRVPT 21,   7, 0.536622, 295.000");
  crv.push_back("CRVPT 21,   8, 0.548517, 290.000");
  crv.push_back("CRVPT 21,   9, 0.560398, 285.000");
  crv.push_back("CRVPT 21,  10, 0.572266, 280.000");
  crv.push_back("CRVPT 21,  11, 0.584120, 275.000");
  crv.push_back("CRVPT 21,  12, 0.588502, 273.150");
  crv.push_back("CRVPT 21,  13, 0.595959, 270.000");
  crv.push_back("CRVPT 21,  14, 0.607781, 265.000");
  crv.push_back("CRVPT 21,  15, 0.619583, 260.000");
  crv.push_back("CRVPT 21,  16, 0.631364, 255.000");
  crv.push_back("CRVPT 21,  17, 0.643122, 250.000");
  crv.push_back("CRVPT 21,  18, 0.654856, 245.000");
  crv.push_back("CRVPT 21,  19, 0.666564, 240.000");
  crv.push_back("CRVPT 21,  20, 0.678244, 235.000");
  crv.push_back("CRVPT 21,  21, 0.689895, 230.000");
  crv.push_back("CRVPT 21,  22, 0.701515, 225.000");
  crv.push_back("CRVPT 21,  23, 0.713102, 220.000");
  crv.push_back("CRVPT 21,  24, 0.724656, 215.000");
  crv.push_back("CRVPT 21,  25, 0.736175, 210.000");
  crv.push_back("CRVPT 21,  26, 0.747659, 205.000");
  crv.push_back("CRVPT 21,  27, 0.759105, 200.000");
  crv.push_back("CRVPT 21,  28, 0.770512, 195.000");
  crv.push_back("CRVPT 21,  29, 0.781880, 190.000");
  crv.push_back("CRVPT 21,  30, 0.793205, 185.000");
  crv.push_back("CRVPT 21,  31, 0.804487, 180.000");
  crv.push_back("CRVPT 21,  32, 0.815724, 175.000");
  crv.push_back("CRVPT 21,  33, 0.826913, 170.000");
  crv.push_back("CRVPT 21,  34, 0.838052, 165.000");
  crv.push_back("CRVPT 21,  35, 0.849140, 160.000");
  crv.push_back("CRVPT 21,  36, 0.860172, 155.000");
  crv.push_back("CRVPT 21,  37, 0.871146, 150.000");
  crv.push_back("CRVPT 21,  38, 0.882059, 145.000");
  crv.push_back("CRVPT 21,  39, 0.892907, 140.000");
  crv.push_back("CRVPT 21,  40, 0.903688, 135.000");
  crv.push_back("CRVPT 21,  41, 0.914396, 130.000");
  crv.push_back("CRVPT 21,  42, 0.925027, 125.000");
  crv.push_back("CRVPT 21,  43, 0.935575, 120.000");
  crv.push_back("CRVPT 21,  44, 0.946033, 115.000");
  crv.push_back("CRVPT 21,  45, 0.956395, 110.000");
  crv.push_back("CRVPT 21,  46, 0.966653, 105.000");
  crv.push_back("CRVPT 21,  47, 0.976802, 100.000");
  crv.push_back("CRVPT 21,  48, 0.986837,  95.000");
  crv.push_back("CRVPT 21,  49, 0.996744,  90.000");
  crv.push_back("CRVPT 21,  50, 1.006516,  85.000");
  crv.push_back("CRVPT 21,  51, 1.016147,  80.000");
  crv.push_back("CRVPT 21,  52, 1.021192,  77.350");
  crv.push_back("CRVPT 21,  53, 1.025631,  75.000");
  crv.push_back("CRVPT 21,  54, 1.034966,  70.000");

  for( int i= 0; i < (int)crv.size(); ++i ) {
    clog<<"UFLS33xConfig::setCrvFlam2Detector> submit: "<<crv[i]<<endl;
    _devIO->submit(crv[i], 0.5);
  }
  /*
  for( int i= 1; i <= 200; ++i ) {
    strstream s;
    s<<"CRVPT? 21, "<<i<<ends;
    string q = s.str(); delete s.str();
    string reply;
    _devIO->submit(q, reply, 0.1);
    clog<<"UFLS33xConfig::setCrvFlam2Detector> reply: "<<i<<": "<<reply<<endl;
  }
  */
  string use21 = "INCRV B, 21";
  clog<<"UFLS33xConfig::setCrvFlam2Detector> submit "<<use21<<endl;
  _devIO->submit(use21, 0.5);
  string sav21 = "CRVSAV";
  clog<<"UFLS33xConfig::setCrvFlam2Detector> submit (and wait 10 sec.) "<<sav21<<endl;
  _devIO->submit(sav21, 10.0);
  setInTypFlam2Detector();
  clog<<"UFLS33xConfig::setCrvFlam2Detector> all done. "<<endl;
  return (int)crv.size();

}
#endif // __UFLS33xConfig_cc__

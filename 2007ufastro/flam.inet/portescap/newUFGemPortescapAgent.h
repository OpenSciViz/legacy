#if !defined(__UFGemPortescapAgent_h__)
#define __UFGemPortescapAgent_h__ "$Name:  $ $Id: newUFGemPortescapAgent.h,v 0.2 2006/12/20 21:22:06 hon Exp $"
#define __UFGemPortescapAgent_H__(arg) const char arg##UFGemPortescapAgent_h__rcsId[] = __UFGemPortescapAgent_h__;

#include "UFGemDeviceAgent.h"
#include "newUFPortescapConfig.h"

#include "string"
#include "list"
#include "map"
#include "iostream.h"

class UFGemPortescapAgent: public UFGemDeviceAgent {
public:
  static int main(int argc, char** argv);
  UFGemPortescapAgent(int argc, char** argv);
  UFGemPortescapAgent(const string& name, int argc, char** argv);
  inline virtual ~UFGemPortescapAgent() {}

  // override these UFRndRobinServ/UFDeviceAgent virtuals:
  virtual void startup();

  virtual string newClient(UFSocket* client);

  virtual void setDefaults(const string& instrum= "flam", bool initsad= true);

  // supplemental options (should call UFDeviceAgent::options() last)
  virtual int options(string& servlist);

  // in addition to establishing the iocomm/annex connection tp lakeshore,
  // open the pipe to the epics channel access "helper"
  virtual UFTermServ* init(const string& host= "", int port= 0);

  //virtual int initTables(UFDeviceAgent::DevCmdTable& dtbl, UFDeviceAgent::QuerryCmds& qlist);

  virtual UFProtocol* action(UFDeviceAgent::CmdInfo* act);
  // new signature for action:
  virtual int action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& rep);

  virtual void* ancillary(void* p);
  virtual void hibernate();

  // default base class behavior should suffice...
  virtual int query(const string& q, vector<string>& reply);

  inline virtual int getValue(const string& indexor, double& steps) { 
    steps = UFGemPortescapAgent::_CurPosition[indexor]; return 0;
  }

  inline virtual int instrumentId() { return _instrumId; }

  // reset hold&run current after each home?
  static bool _resetHRC;
  static string _SADDatumCnt;
  void datumCompleted(int errcnt, string& errmsg);

protected:
  // these are used to coordinate the action() and ancillary() functions:
  static int _instrumId;
  static string _currentval;
  static string _reqval;
  static time_t _timeOut; // in seconds 
  static time_t _clock;
  static double _NearHome, _simSpeed;
  static map<string, string> _Status; // use this instead of _StatList for EDB/CA names
  static map<string, string> _ErrPosition; // error message

  // old compund motion hash maps and related funcs.
  static map<string, string> UFGemPortescapAgent::_HomeThenStep; // compound motion seq.: first home then step
  static map<string, string> UFGemPortescapAgent::_HomeStepHome; // compound motion: home then step then home step parms
  static map<string, string> UFGemPortescapAgent::_HomeHome; // compound motion: home then step then home 2nd home parms
  static map<string, string> UFGemPortescapAgent::_InitVelCmd; // last/latest initial velocity command setting
  static map<string, double> UFGemPortescapAgent::_Motion; // in-motion desired position (steps from home)
  static map<string, double> UFGemPortescapAgent::_Homing; // homing desired position should always be 0
  static map<string, double> UFGemPortescapAgent::_HomingFinal; // home-step-home
  static map<string, double> UFGemPortescapAgent::_NearHoming; // homing desired position should always be 0
  static map<string, double> UFGemPortescapAgent::_LimitSeeking; // seeking limit desired position should always be +-Limit
  static int _reqHomeThenMore(UFDeviceAgent::CmdInfo* act);

  // new F2 compound motions

  // all arbitary idexor motion sequences -- if vector is 0, no motion sequence is active or pending
  // vector size == 1 for atomic motion, > 1for compound motion
  static map<string, vector<string>*> _SeqMotion;
  // current and desired position (steps from home) hash tables for each type of motion
  static map<string, double> _CurPosition; // current position (steps from home) via incexor status
  static map<string, double> _DesPosition; // desired/requested
  static map<string, double> _CurMotion; // active/in-motion desired position (steps from home)
  //static map<string, double> _NextMotion; // next-motion desired position (steps from home)
  // to support all the compound cmd seq.s above, as well as atomics, copy the approp. commands
  // at the approp. time to these for subsequent examination:
  static map<string, string> _CurCmd; // current motion command executing (set after sucessful submit)
  static map<string, vector <string> > _ParamSads;

  // return 0 if no compound motions found (although atomic motions may be present)
  // return >= 1 if one or more allowed motions found and also parce and place into supplied hash table
  // return < 0 if one or more unallowed motions found.
  // motions are unallowed if there is no hash table defined for them of if
  // the defined hash table is already populated (i.e. compound motion in progress) 
  int _allowedMotions(UFDeviceAgent::CmdInfo* act, map< string, vector< string > >& reqseq);

  // more convenience funcs.
  bool _nearHome(const string& motor, string& cmdimpl);
  void _execMovement(map<string, double>& moving);
  void _simMovement(map<string, double>& moving);
  string _guessSADchan(const string& indexor);
  int _parseXCmdReply(const string& indexor, const string& xReply);

  // if motion in progress, indicate which :
  inline string _hasCurMotion(const string& indexor) {
    if( _SeqMotion[indexor] == 0 || _CurCmd[indexor].empty() || _CurCmd[indexor] == "null" )
      return "null";
    vector< string >& vseq = *_SeqMotion[indexor];
    for( size_t i = 0; i < vseq.size(); ++i ) {
      if( vseq[i] == _CurCmd[indexor] )
	return _CurCmd[indexor];
    }
    return "null";
  }

  // if current motion command is empty no motion has ever been requested,
  // if == "null" at least one motion request has been processed;
  // and if != "null" it is the currently active motion command
  inline bool isCurMotion(const string& indexor) {
    string cmd  = _hasCurMotion(indexor);
    if( cmd.empty() || cmd == "null" )
      return false;
    return true;
  }

  // is the current motion the final motion of a sequence?
  inline bool _isCurMotionFinal(const string& indexor) {
    if( _SeqMotion[indexor] == 0 || _CurCmd[indexor].empty() || _CurCmd[indexor] == "null" )
      return true;
    vector< string >& vseq = *_SeqMotion[indexor];
    for( size_t i = 0; i < vseq.size(); ++i ) {
      if( vseq[i] == _CurCmd[indexor] )
	if( i < (vseq.size() - 1) )
	  return false;
    }
    return true;
  }
};

#endif // __UFGemPortescapAgent_h__
      


#include	"iostream"

#include	"UFPortescapConfig.h"
#include	"uftrecsmech.h"

using std::clog ;
using std::endl ;
using std::cin ;

int main( int argc, const char** argv )
{
if( argc != 2 )
	{
	cout	<< "Usage: "
		<< argv[ 0 ]
		<< " <motor param file>"
		<< endl ;
	return 0 ;
	}

// This code is from testmech.c
UFTReCSMech *TheTReCSMech = getTheUFTReCSMech();
for( int m = 0; m < TheTReCSMech->mechCnt; ++m )
	{
	clog	<< TheTReCSMech->mech[m].name
		<< endl ;
	}

clog	<< "Shall I rehash and display output again? (y/n) " ;
clog.flush() ;
char c ;
cin >> c ;

UFPortescapConfig config ;
config.rehashParamFile( argv[ 1 ] ) ;

TheTReCSMech = getTheUFTReCSMech();
for( int m = 0; m < TheTReCSMech->mechCnt; ++m )
	{
	clog	<< TheTReCSMech->mech[m].name
		<< endl ;
	}

return 0 ;
}

/*
 * Utility driver to create a motor parameter
 * file (V2 format) for use with fjec
 */

#include "ufflam2mech.h"

int main (void) {
  int i,j;
  UFFLAM2Mech * mech = getTheUFFLAM2Mech();
  printf(";Motor Parameter File\n");
  printf("; $Name:  $ $Id: motor_param_V2.txt,v\n");
  printf(";Number of Motors\n");
  printf("%d\n;\n",mech->mechCnt);
  for (i=0; i<mech->mechCnt; i++) {
    printf("BEGIN_REC\n");
    printf(";HWName\tEpicsName\n");
    printf("%s\t%s\n",mech->indexor[i],mech->mech[i].name);
    printf("POSITIONS\n;#Positions for %s\n",mech->mech[i].name);
    printf("%d\n",mech->mech[i].posCnt);
    printf(";#\tOffset\t\tName\n");
    for (j=0; j<mech->mech[i].posCnt; j++) {
      printf("%d\t%f\t%s\n",(j+1),mech->mech[i].steps[j],mech->mech[i].positions[j]);
    }
    printf("END_REC\n;\n");
  }
}

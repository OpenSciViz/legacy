#if !defined(__ufF2mechMotion_h__)
#define __ufF2mechMotion_h__
#ifdef SWIG
/*
  Instructions to build:
  gcc -ansi -c ufF2mechMotion.c
  swig -perl ufF2mechMotion.h
  perl Makefile.PL PREFIX=$UFINSTALL/perl
  make
  cp -R blib/arch/auto UFF2.pm $UFINSTALL/perl/
*/
%module UFF2 
%{
#include "ufF2mechMotion.h"
%}
#endif

/*
  Note that all char* and char** pointers should be initialixed to point to pre-allocated buffers
  of sufficient size!
*/
/* export some of the convenience functions in the ufflam2mech.h */
extern char* ufF2indexorOfMechName(const char* name);
extern char* ufF2mechNameOfIndexor(const char* idxor);
extern int ufF2setIndexorMechNameList(char* list);
extern double ufF2stepsFromHome(const char* mechName, char* posName);
extern char* ufF2posNameNear(const char* mechName, double stepsFromHome);
extern int ufF2setNamedPositionList(const char* mechName, char* posnames);
extern int ufF2printNamedPositions(const char* mechName);
extern int ufF2printAllNamedPositions();
extern double ufF2park(const char* mechName);

/* F2 compound motion commands */
extern int ufF2mechMotion(char* mechname, double currentPosition, double desiredPosition, double backlash,
			  int fastVel, int slowVel, char** indexorCmdBuf);

extern int ufF2mechMotion4(char* mechname, double currentPosition, double desiredPosition, double backlash,
			   int fastVel, int slowVel, char* indexorCmd0, char* indexorCmd1, char* indexorCmd2, char* indexorCmd3); 


extern int ufF2mechMotionNamed(char* mechname, double currentPosition, char* desiredPositionName, double backlash,
			       int fastVel, int slowVel, char** indexorCmdBuf);

extern int ufF2mechMotion4Named(char* mechname, double currentPosition, char* desiredPositionName, double backlash, int fastVel,
				int slowVel, char* indexorCmd0, char* indexorCmd1, char* indexorCmd2, char* indexorCmd3);

/* these two functions provide indexor the command sequence in one comma separated string */
extern int ufF2mechMotionP(char* mechname, double currentPosition, double desiredPosition, double backlash,
			   int fastVel, int slowVel, char* indexorCmds); /* comma separated */

extern int ufF2mechMotionPNamed(char* mechname, double currentPosition, char* desiredPositionName, double backlash,
				int fastVel, int slowVel, char* indexorCmds); /* comma separated */
/*
 provide convenienc function that sets new 'IF2-N::FinalStepCnt 'cmdname' for  'cmdimpl' from 'indexorCmds'
 provided above: resulting string pair used in client/server transactions (epics/other <--> agent).
 e.g. for indexor B -- cmdname: "BF2-3::1000 paired with cmdimpl: "B+1020::1020,B-40::980,B+20::1000"
such a command name can be assumed to derive from this trusted function and now validity check required by
the agent.
*/

extern int cmdTransaction(char* indexorCmds, char* cmdName);

extern char* getPositionName(char* mechname, double currentPosition);

extern void getNamedPositions(char* mechname);

extern int parseMotorString(char* motorString, int n, char* command);

#endif

#if !defined(__UFPortescapConfig_h__)
#define __UFPortescapConfig_h__ "$Name:  $ $Id: newUFPortescapConfig.h,v 0.3 2006/12/20 21:22:06 hon Exp $"
#define __UFPortescapConfig_H__(arg) const char arg##UFPortescapConfig_h__rcsId[] = __UFPortescapConfig_h__;

#include "UFDeviceConfig.h"
#include "UFDeviceAgent.h"
#include "UFPortescapTermServ.h"
#include "ufF2mechMotion.h"

using namespace std;
#include "string"
#include "deque"
#include "map"

// forward declarations for friendships
class UFPortescapAgent;
class UFGemPortescapAgent;

class UFPortescapConfig : public UFDeviceConfig {
public:
  UFPortescapConfig(const string& name= "UnknownPortescap@DefaultConfig");
  UFPortescapConfig(const string& name, const string& tshost, int tsport);
  inline virtual ~UFPortescapConfig() {}

  // additiona helpers
  bool isMotionCmd(const string& cmd, string& indexor);
  int cntDelim(const string& cmds, string delim= ",");
  int splitCmdlist(const string& cmds, map< string, string >& motion, map< string, string >& nonmotion);
  // new helper functions for agent action virtual
  // sequenced cmds must be performed via ancillay func via exec or sim Motion funcs.
  // immmediate cmds can be sent to device directly from agent action func.
  // in this case the string values is the deliminated set of motion parameters,
  // and/or status reqs, optionally followed by a single/atomic motion request
  // set immed. cmds. from cmdinfo*
  int queryStepCnts(map< string, double >& curstepcnts);
  int parseCmds(UFDeviceAgent::CmdInfo* cmds, map< string, string >& cars, map< string, string >& sads, 
		map< string, string >& immedcmds, map< string, string >& seqcmds);

  // submit immediates
  int submitImmedCmds(map< string, string >& immedcmds, map< string, double >& curstepcnts);

  // once the action func has processed any/all immmediate cmds, it should use this
  // to start any/all sequenced motions (aided by helpers below):
  // this should submit the first command in the comma-separated list of commands
  // for each indexor, and remove it fromthe list...
  // i.e.: suppose seqcmds['B'] = "BU+1000::2000,B-50::1950" the first +1000 should be
  // submitted and the map value reset to seqcmds['B'] = "B-50::1950"
  int startSeqCmds(map< string, string >& seqcmds, map< string, double >& curstepcnts);

  // start indexor motion return starting position step count
  double startIndexorMotion(const string& indexor, const string& cmd); 

  // query indexor for motion status -- return motion? true or false and latest/current stetp count
  bool statMotion(const string& indexor, double& stepcnt);

  static void setIndexorNames(int instrumId, const string& motors= "");
  static bool newSettingCmd(const string& indexor, const string& cmdimpl);
  static int numReplyLinesExpected(const string& cmdimpl);

  // to support party-line or direct-line indexors:
  // wrappers to support party-line or direct-line indexors:
  // if _Indexors look like: "A:portA B:portB ... N:portN", or not, use tsport 
  static int parsePorts(int& tsport, map< string, int >& portmap);
  // if parsePorts return 1, assume party-line:
  static int parsePorts(const string& indexorports, map< string, int >& portmap, const string& start= "A");
  static int partyLine(int port);
  // if multiple ports are indicated, assume direct lines:
  static int directLines(map< string, int >& portmap);

  // override these virtuals:
  // device i/o behavior
  virtual UFTermServ* connect(const string& host= "", int port= 0);

  virtual UFStrings* status(UFDeviceAgent*);
  virtual UFStrings* statusFITS(UFDeviceAgent*);

  // device cmds
  virtual vector< string >& UFPortescapConfig::queryCmds();
  virtual vector< string >& UFPortescapConfig::actionCmds();

  // return -1 if invalid, 0 if valid but no reply expected,
  // n > 0 if valid and n reply strings expected:
  virtual int validCmd(const string& c);

  // the default behavior is ok here:
  //virtual string terminator();
  //virtual string prefix();

  static bool jogMotor(const string& motor, const string& cmd, double curpos, string& jogcmd);

  static bool _verbose;
  static int _instrumId;

  // keep last hold-run current settings
  static map< string, string > _HoldRunC;
  static map< string, int> _DatumCnt;
  static string setMotionParms(const string& indexor, string& ivel, string& vel, string& acc,
			     string& holdrunc, string& res);

  // support 2 limit switches in simulation mode (use the same two vals for all indexors)
  // on cmd 'M -/+vel' step to current-_leftLim or current+_rightLim
  static double _leftLim, _MinLim, _rightLim, _MaxLim;

protected:
  static vector <string> _Indexors; // names of All indexors 
  static string _Motors; // names as single cmd-line string
  // keep track of prior settings cmd:
  static map< string, string> _HoldRunCcmd;
  static map< string, string> _SlewVcmd;
  static map< string, string> _AccelDecelcmd;
  static map< string, string> _InitVcmd;
  static map< string, string> _Settlecmd;
  static map< string, string> _Jogcmd;

  // allow Agents access to static _Indexors
  friend class UFPortescapAgent;
  friend class UFGemPortescapAgent;
};

#endif // __UFPortescapConfig_h__

#include "ufF2mechMotion.h"
#include "stdio.h"
#include "string.h"
#include "stdlib.h"
#include "ctype.h"
#include "ufflam2mech.h"

#include "iostream.h"

int main(int argc, char** argv, char** env) {
  int s= 0, nc= 0;
  double curpos= -13910, newpos= 3000, blash= 10;
  char cmds[BUFSIZ], cmd[BUFSIZ], cmdname[BUFSIZ];
  memset(cmds, 0, BUFSIZ); memset(cmd, 0, BUFSIZ); memset(cmdname, 0, BUFSIZ);


  if (sizeof(argv) > 1 && argv[1] == (char*)NULL) {
    printf("ERROR: Motori/Indexor not specified!\n");
    return 0;
  }
  argv[1][0] = toupper(argv[1][0]); // force capitalization
  printf("Motion(s) for Motor/Indexor: %s\n", argv[1]);
  //printNamedPositions("Filter1");
  int np = printNamedPositions(argv[1]);
  if( np < 0 ) { clog<<"uff2mechtest> non-existent mechanism/indexor? "<<argv[1]<<endl; exit(np); }

  if( argc > 1 ) {
    if( argv[2] != (char*)NULL && (isdigit(argv[2][0]) || argv[2][0] == '-')) {
      curpos = atof(argv[2]);
      printf(" %s ", argv[2]);
    }
    else {
      printf("Using current position: %f\n", curpos);
    }
  }
  if( argc > 2 ) {
    if(argv[3] != (char*)NULL && (isdigit(argv[3][0]) || argv[3][0] == '-')) {
      newpos = atof(argv[3]);
      printf(" %s", argv[3]);
    }
    else {
      printf("Using new position: %f\n", newpos);
    }
  }

  if( argc > 2 ) {
    if(argv[4] != (char*)NULL && isdigit(argv[4][0])) {
      blash = atof(argv[4]);
      printf(" %s", argv[4]);
    }
    else {
      printf("Using backlash: %f\n",blash);
    }
  }

  nc = ufF2mechMotionP(argv[1], curpos, newpos, blash, 600, 200, cmds);
  cout<<"uff2mechtest> nc == "<<nc<<", cmdstring: "<<cmds<<endl;
  for( int i= 0; i < nc; ++i ) {
    s = parseMotorString(cmds, i,  cmd);                      
    printf("%s parse 0: %s steps= %d\n", cmds, cmd, s);
  }

  s = cmdTransaction(cmds, cmdname);
  cout<<"uff2mechtest> cmdname == "<<cmdname<<", cmdimpl == "<<cmds<<endl;

  return 0;
}

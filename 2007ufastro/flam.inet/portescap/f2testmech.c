#include "ufF2mechMotion.h"
#include "stdio.h"
#include "string.h"
#include "stdlib.h"
#include "ctype.h"
#include "ufflam2mech.h"
int main(int argc, char** argv, char** env) {
  int x= 0, nr= 0;
  double curpos= -13910, newpos= 3000, blash= 10;
  const size_t nc= 4, sz= 32;
  /* allocate 3 string bufs of 31 characters, recall that the final char[31] must  == 0 (NULL) */
  char c0[sz];
  char c1[sz];
  char c2[sz];
  char c3[sz];
  char* cmds[nc];
  cmds[0] = c0;
  cmds[1] = c1;
  cmds[2] = c2;
  cmds[3] = c3;
  char a[BUFSIZ], b[BUFSIZ], c[BUFSIZ];
  memset(a, 0, BUFSIZ); memset(b, 0, BUFSIZ);memset(c, 0, BUFSIZ);
  memset(cmds[0], 0, sz); memset(cmds[1], 0, sz); memset(cmds[2], 0, sz); memset(cmds[3], 0, sz);

  printNamedPositions("Filter1");

  if (sizeof(argv) > 1 && argv[1] == (char*)NULL) {
    printf("ERROR: Motor not specified!\n");
    return 0;
  }

  if (argv[2] != (char*)NULL && (isdigit(argv[2][0]) || argv[2][0] == '-')) {
    curpos = atof(argv[2]);
  } else {
    printf("WARNING: current position not a number!  Using %f\n", curpos);
  }
  if (argv[3] != (char*)NULL && (isdigit(argv[3][0]) || argv[3][0] == '-')) {
    newpos = atof(argv[3]);
  } else {
    printf("WARNING: new position not a number!  Using %f\n", newpos);
  }
  if (argv[4] != (char*)NULL && isdigit(argv[4][0])) {
    blash = atof(argv[4]);
  } else {
    printf("WARNING: backlash not a number!  Using %f\n",blash);
  }
  printf("Motions for MOTOR %s\n", argv[1]);
/*
  nr = ufF2mechMotionP(argv[1], curpos, newpos, blash, 600, 200, c);
  x = parseMotorString(c, 0, a);
  printf("%s parse 0: %s steps= %d\n", c, a, x);
  x = parseMotorString(c, 1, b);
  printf("%s parse 1: %s steps= %d\n", c, b, x);
*/
/*
  printf(c);
  printf("\n");
  char* a1;
  int n = strstr(c,",")-c;
  strncpy(a1,c,n);
  printf(a1);
  printf("\n");
  a1 = strstr(c,",")+1;
  printf(a1);
  printf("\n");
  a1 = strstr(a1,",");
  printf(a1);
*/
/*
  nr = ufF2mechMotion(argv[1], curpos, newpos, blash, 600, 200, (char**) cmds);
*/
  nr = ufF2mechMotion4(argv[1], curpos, newpos, blash, 600, 200, c0, c1, c2, c3);
/*
  nr = ufF2mechMotion("A", curpos, newpos, blash, (char**) cmds);
  nr = ufF2mechMotion("Window", curpos, newpos, blash, (char**) cmds);
  nr = ufF2mechMotion("Filter1", 55090, 13690, 10, (char**) cmds);
  nr = ufF2mechMotion("Lyot", -2864, -2289, 10, (char**) cmds);
printf("\n");
  nr = ufF2mechMotion("Grism", 55110, 13710, 10, (char**) cmds);
*/

  return 0;
}

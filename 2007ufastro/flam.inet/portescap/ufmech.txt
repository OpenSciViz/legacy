;Motor Parameter File
; $Name:  $ $Id: ufmech.txt 14 2008-06-11 01:49:45Z hon $
;cc agent host IP number
;128.227.184.237
192.168.111.40
;cc agent port number
;52005 -- changed to 52009 for testing
52011
;Number of Motors
9
;
;#  : Motor Number
;IS : Initial Speed
;TS : Terminal Speed
;A  : Acceleration
;D  : Deceleration
;HC : Hold Current
;DC : Drive Current
;AN : Axis Name (one character)
;DS : Datum Speed
;FDS: Final Datum Speed
;DD : Datum Direction 
;BL : Back-Lash correction param.
;Name : Motor name
;
BEGIN_REC
;IS  TS  A   D   HC DC AN DS  FDS DD  BL  Name
 40  80  10  10  0 15  A  80  40  1   10  Sector Wheel
POSITIONS
;#_Positions for Sector Wheel
 5 
;# Offset  Throughput Name (Comment)
 1 268     1.00      Open
 2 556     1.00      BB-Low
 3 841     1.00      BB-High
 4 1126    0.60      Polystyrene
 5 0       1.00      Datum
END_REC
;
BEGIN_REC
;IS  TS  A   D   HC DC AN DS  FDS DD  BL  Name 
 40  40  255 255 0 100 B  80  20  1   40  Window Changer
POSITIONS
;#_Positions for Window Changer
 6 
;# Offset  Throughput  Name (Comment)
 1 0       0.50        Block
 2 1289    0.97        KRS-5
 3 2578    0.95        ZnSe
 4 3862    0.70        KBr (uncoated)
 5 5150    0.95        KBrC (coated)
 6 0       1.00        Datum
END_REC
;
BEGIN_REC
;IS  TS  A   D   HC DC AN DS  FDS DD  BL  Name 
 40  80  10  10  0 15  C  80  40  0  -30  Aperture Wheel
POSITIONS
;#_Positions for Aperture Wheel
 6 
;# Offset  Throughput  Name (Comment)
 1 21      1.00        Grid_Mask
 2 167     1.00        Matched
 3 313     1.00        Oversized
 4 460     0.97        Window_Imager
 5 607     1.00        Spot_Mask
 6 0       1.00        Datum
END_REC
;
BEGIN_REC
;IS  TS  A   D   HC DC AN DS  FDS DD  BL  Name 
 40  80  10  10  0 15  D  80  40  0  -10  Filter Wheel 1
POSITIONS
;#_Positions for Filter Wheel 1 
 14 
;# Offset  Throughput  Name (Comment)
 1 -72     1.00        Open
 2 15      0.71        Qw-20.8um
 3 102     0.95        Si-7.9um
 4 189     0.77        PAH-8.6um
 5 276     0.95        Si-8.8um
 6 363     1.00        Block
 7 451     0.92        Si-9.7um
 8 538     1.00        PAH2
 9 625     0.97        Si-10.4um
10 712     0.70        Ar-III
11 799     0.88        Si-11.7um
12 886     0.86        Si-12.3um
13 973     1.00        Align-Spot
14 0       1.00        Datum
END_REC
;
BEGIN_REC
;IS  TS  A   D   HC DC AN DS  FDS DD  BL  Name 
 40  80  10  10  0 15  E  80  40  1   20  Lyot Wheel
POSITIONS
;#_Positions for Lyot Wheel
 14 
;# Offset  Throughput  Name (Comment)
 1 809     1.00        Grid_Mask
 2 896     1.00        Spot_Mask
 3 982     1.00        Ciardi
 4 1069    0.97        Open
 5 1156    1.00        Quakham_Mask
 6 1243    0.60        Polystyrene
 7 1329    1.00        Circ-2
 8 1416    1.00        Circ-4
 9 1503    1.00        Circ+2
10 1589    1.03        Circ+4
11 1677    1.00        Circ+6
12 1762    0.97        Circ+8
13 1849    1.00        Block
14 0       1.00        Datum
END_REC
;
BEGIN_REC
;IS  TS  A   D   HC DC AN DS  FDS DD  BL  Name 
 40  80  10  10  0 15  F  80  40  1   20  Filter Wheel 2
POSITIONS
;#_Positions for Filter Wheel 2 
 14
;# Offset  Throughput  Name (Comment)
 1 530     1.00        Open
 2 1574    0.85        K
 3 1487    0.97        L
 4 1400    0.94        M
 5 1313    1.00        NeII
 6 1226    0.72        NeII_ref2
 7 1139    1.00        SIV
 8 1052    0.64        Qs-18.3um
 9 965     1.00        Qone
10 878     0.64        Ql-24.5um
11 791     0.80        N
12 704     1.00        Block
13 617     1.00        Align-Spot
14  0      1.00        Datum
END_REC
;
BEGIN_REC
;IS  TS  A   D   HC DC AN DS  FDS DD  BL  Name 
 40  80  10  10  0 15  G  80  40  1   40  Pupil Imaging Wheel
POSITIONS
;#_Positions Pupil Imaging Wheel
 5 
;# Offset  Throughput  Name (Comment)
 1 203     1.00        Open-1
 2 417     0.97        Pupil_Imager
 3 630     1.00        Open-2
 4 843     1.00        Open-3
 5 0       1.00        Datum
END_REC
;
BEGIN_REC
;IS  TS  A   D   HC DC AN DS  FDS DD  BL  Name 
 40  200 100 100 0 15  H  200 40  1  100  Slit Wheel
POSITIONS
;#_Positions Pupil Imaging Wheel
 9 
;# Offset  Throughput  Name (Comment)
 1 -2316   1.00        Open
 2 195     1.00        1.32 (")
 3 2661    1.00        0.72 (")
 4 5189    1.00        0.66 (")
 5 7662    1.00        0.36 (")
 6 10190   1.00        0.31 (")
 7 12634   1.00        0.26 (")
 8 15104   1.00        0.21 (")
 9 0       1.00        Datum
END_REC
;
BEGIN_REC
;IS  TS  A   D   HC DC AN DS  FDS DD  BL  Name 
 20  40  255 255 0 30  I  40  40  1  500  Grating Wheel
POSITIONS
;#_Positions Grating Wheel
 7 
;# Offset  Throughput  Name (Comment)
 1 -1006   0.99        Mirror
 2 1136    0.90        LowRes-10
 3 2392    0.99        LR_Ref_Mirror
 4 3401    0.90        LowRes-20
 5 5028    0.90        HighRes-10
 6 6972    0.99        HR_Ref_Mirror
 7 0       1.00        Datum (@ 0)
GRATING_PARAMS
;fiducial_steps   fiducial_angle   default_central_wavelen
 6605             31.395           8.588
END_REC
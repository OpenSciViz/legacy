/* for use by C & C++ to describe mechanism names & positions */

#include <string.h>

#define _ArrayLen__(array) (sizeof((array))/sizeof((array)[0]))

static char globline[256]; //global char array used to read file

typedef struct {
  int posCnt; /* number of (<= max) positions (including home) in use */
  char name[256]; /* name[__UFEPICSSLEN_]; */ /* mech. named pos */
  char* *positions; /* positions[__UFMAXPOSTRECS_][__UFEPICSSLEN_]; */
  float *steps; /* steps[__UFMAXPOSTRECS_]; + steps from home */
} UFMechPos;

typedef struct __UFMech_ {
  int mechCnt;
  char **indexor; /* indexor[__UFMAXINDXTRECS_][__UFEPICSSLEN_]; */
  UFMechPos *mech; /* mech[__UFMAXINDXTRECS_]; */
} UFMech;

static char *  nextUncommentedLine(FILE * fp) {
  memset(globline,0,256);
  globline[0] = ';';
  int i=1;
  while (globline[i-1] != EOF && globline[0] == ';') {
    memset(globline,0,256);
    i=0;
    while ( (globline[i++] = fgetc(fp)) != '\n' && globline[i-1] != EOF);
  }
  globline[i-1] = 0;
  return globline;
}

static void getNameFromLine(char * nameBuff, const char * line) {
  int n = strlen(line);
  int i=0,tokenCnt = 0;
  //strip off initial blank spaces
  while (line[i++] == ' ');
  while(tokenCnt < 11) {
    while (line[i++]!=' ');
    while (line[i++]==' ');
    tokenCnt++;
  }
  const char * cs = line+i-1;
  memcpy(nameBuff,cs,n-i+1);
}

static void getPositionAndStepsFromLine(char* position, float* steps, const char * line) {
  int i =0,j=0;
  char buff[256];
  memset(buff,0,256);
  while (line[i++] == ' ');i--;//strip off initial blank spaces
  while (line[i++] != ' ');i--;//get first token
  while (line[i++] == ' ');i--;//get ready for next token
  while ( (buff[j++] = line[i++]) != ' ');i--;buff[j-1]=0;
  *steps = atoi(buff);
  memset(buff,0,256);
  j=0;
  while (line[i++] == ' ');i--;
  while (line[i++] != ' ');i--;
  while (line[i++] == ' ');i--;
  while ( (buff[j++] = line[i++]) != 0); buff[j-1]=0;
  memcpy(position,buff,j);
}

static UFMech*  getTheUFMechV2(const char* filename ) {
  FILE * fp = fopen(filename,"rt"); //open file in read mode / text mode
  nextUncommentedLine(fp); // ipaddress of agent
  nextUncommentedLine(fp); // port number of agent to connect to
  static UFMech _theMech;
  int i=0,j=0;
  _theMech.mechCnt = atoi(nextUncommentedLine(fp));
  if (_theMech.mech != 0) { // free up any previously used memory
    for ( i=0; i<_theMech.mechCnt; i++) {
      if (_theMech.mech[i].positions != NULL) free(_theMech.mech[i].positions);
      if (_theMech.mech[i].steps != NULL) free(_theMech.mech[i].steps);
    }
    free(_theMech.mech);  // free up any previously used memory
  }
  _theMech.mech = (UFMechPos*)calloc(_theMech.mechCnt, sizeof(UFMechPos));
  for ( i=0; i<_theMech.mechCnt; i++) {
    char* line;
    if ( strcmp(line=nextUncommentedLine(fp),"BEGIN_REC") != 0) { 
      fprintf(stderr,"Expected: BEGIN_REC. Read: %s.  Possible corrupt file, aborting\n",line);
      _theMech.mechCnt = i;
      fclose(fp);
      return &_theMech;  
    }
    getNameFromLine(_theMech.mech[i].name,nextUncommentedLine(fp));
    if ( strcmp(line=nextUncommentedLine(fp),"POSITIONS") != 0) { 
      fprintf(stderr,"Expected: POSITIONS. Read: %s.  Possible corrupt file, aborting\n",line); 
      _theMech.mechCnt = i;
      fclose(fp);
      return &_theMech;  
    }
    _theMech.mech[i].posCnt = atoi(nextUncommentedLine(fp));
    _theMech.mech[i].positions = (char**)calloc(_theMech.mech[i].posCnt,sizeof(char*));
    _theMech.mech[i].steps = (float*)calloc(_theMech.mech[i].posCnt,sizeof(float));
    for (j=0; j<_theMech.mech[i].posCnt; j++) {
      if (_theMech.mech[i].positions[j] != NULL)
	free(_theMech.mech[i].positions[j]);
      _theMech.mech[i].positions[j] = (char*)calloc(256,sizeof(char));
      getPositionAndStepsFromLine(_theMech.mech[i].positions[j],&_theMech.mech[i].steps[j],nextUncommentedLine(fp));
    }
    if ( strcmp(line=nextUncommentedLine(fp),"END_REC") != 0 && strcmp(line,"GRATING_PARAMS") != 0) { 
      fprintf(stderr,"Expected: END_REC or GRATING_PARAMS. Read: %s.  Possible corrupt file, aborting\n",line); 
      _theMech.mechCnt = i;
      fclose(fp);
      return &_theMech;  
    } else if ( strcmp(line,"GRATING_PARAMS")==0) {
      line = nextUncommentedLine(fp);
      if ( strcmp(line=nextUncommentedLine(fp),"END_REC")!=0) {
	fprintf(stderr,"Expected: END_REC. Read: %s.  Possible corrupt file, aborting\n",line);
	_theMech.mechCnt = i;
	fclose(fp);
	return &_theMech;
      }
    }
  }
  fclose(fp);
  return &_theMech;
}
static UFMech*  getTheUFMechV1(const char* filename) {
  FILE * fp = fopen(filename,"rt"); //open file in read mode / text mode
  nextUncommentedLine(fp); // ipaddress of agent
  nextUncommentedLine(fp); // port number of agent to connect to
  static UFMech _theMech;
  int i=0,j=0; unsigned int k=0;
  _theMech.mechCnt = atoi(nextUncommentedLine(fp));
  if (_theMech.mech != 0) { // free up any previously used memory
    for ( i=0; i<_theMech.mechCnt; i++) {
      if (_theMech.mech[i].positions != NULL) free(_theMech.mech[i].positions);
      if (_theMech.mech[i].steps != NULL) free(_theMech.mech[i].steps);
    }
    free(_theMech.mech);  // free up any previously used memory
  }
  _theMech.mech = (UFMechPos*)calloc(_theMech.mechCnt, sizeof(UFMechPos));
  for ( i=0; i<_theMech.mechCnt; i++) {
    getNameFromLine(_theMech.mech[i].name,nextUncommentedLine(fp));
  }
  for ( i=0; i<_theMech.mechCnt; i++) {
    //    UFStringTokenizer ufst(nextUncommentedLine(fp));
    char * line = nextUncommentedLine(fp);
    int k=0,l=0;
    char buff[256];
    memset(buff,0,256);
    while(line[k++] == ' '); k--;
    while(line[k++] != ' '); k--;
    while(line[k++] == ' '); k--;
    while( (buff[l++] = line[k++]) != ' ');
    _theMech.mech[i].posCnt = atoi(buff);
    _theMech.mech[i].positions = (char**)calloc(_theMech.mech[i].posCnt,sizeof(char*));
    _theMech.mech[i].steps = (float*)calloc(_theMech.mech[i].posCnt,sizeof(float));
    for ( j=0; j<_theMech.mech[i].posCnt; j++) {
      if (_theMech.mech[i].positions[j] != NULL)
	free(_theMech.mech[i].positions[j]);
      _theMech.mech[i].positions[j] = (char*)calloc(256,sizeof(char));
      getPositionAndStepsFromLine(_theMech.mech[i].positions[j],&_theMech.mech[i].steps[j],nextUncommentedLine(fp));
    }
  }
  fclose(fp);
  return &_theMech;
}

static UFMech* getTheUFMech(const char * filename) {
  FILE * fp = fopen(filename,"rt");
  char *  line;
  int i=0;
  for (i=0; i<4; i++) line = nextUncommentedLine(fp);
  fclose(fp);
  if (strcmp(line, "BEGIN_REC") == 0) return getTheUFMechV2(filename);
  else return getTheUFMechV1(filename);
}

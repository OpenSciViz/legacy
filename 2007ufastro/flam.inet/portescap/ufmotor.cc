#if !defined(__ufmotor_cc__)
#define __ufmotor_cc__ "$Name:  $ $Id: ufmotor.cc 14 2008-06-11 01:49:45Z hon $";

#include "string"
#include "vector"
#include "stdio.h"

#include "UFClientApp.h"

bool expect_reply = true;

class ufmotor : public UFClientApp {
public:
  inline ufmotor(const string& name, int argc, char** argv, char** envp, int port= -1);
  inline ~ufmotor() {}
  static int main(const string& name, int argc, char** argv, char** envp);
  inline virtual string description() const { return __ufmotor_cc__; }
};

ufmotor::ufmotor(const string& name,
		 int argc, char** argv, char** envp,
		 int port) : UFClientApp(name, argc, argv, envp, port) {}

int ufmotor::main(const string& name, int argc, char** argv, char** envp) {
  ufmotor motor(name, argc, argv, envp);
  string arg, host(hostname()),
         sim("false"), caput("false");
  string raw = "true"; // default is raw command mode (with replies)
  string status = ""; 
  int port= 52024;
  float flush= -1.0;

  arg = motor.findArg("-flush");
  if( arg != "false" )
    flush = atof(arg.c_str());

  arg = motor.findArg("-raw");
  if( arg != "false" &&  arg != "true" ) // explicit cmd string 
    raw = arg;

  arg = motor.findArg("-q");
  if( arg != "false" ) { 
    _quiet = true;
    raw = arg;
  }
  else
    UFProtocol::_verbose = true;

  arg = motor.findArg("-sim");
  if( arg != "false" &&  arg != "true" ) // explicit cmd string 
    sim = arg;

  arg = motor.findArg("-caput");
  if( arg != "false" &&  arg != "true" ) // explicit cmd string 
    caput = arg;
  /*
  arg = motor.findArg("-noreply");
  if( arg != "false" ) // explicit cmd string 
    expect_reply = false;
  */
  arg = motor.findArg("-host");
  if( arg != "false" )
    host = arg;

  arg = motor.findArg("-port");
  if( arg != "false" && arg != "true" )
    port = atoi(arg.c_str());


  arg = motor.findArg("-stat");
  if( arg != "false" ) { // full or fits
    _quiet = true;
    raw = "false";
    if( arg != "true" ) 
      status = arg;
    else
      status = "All";
      //status = "FITS"; // only two allowed values 
  }

  arg = motor.findArg("-status");
  if( arg != "false" ) { // full or fits
    _quiet = true;
    raw = "false";
    if( arg != "true" ) 
      status = arg;
    else
      status = "All";
      //status = "FITS"; // only two allowed values 
  }

  if( port <= 0 || host.empty() ) {
    clog<<"ufmotor> usage: 'ufmotor -flush sec. -host host -port port -raw raw-command'"<<endl;
    return 0;
  }

  FILE* f= motor.init(host, port);
  if( f == 0 ) {
    clog<<"ufmotor> unable to connect to Portescap command server agent..."<<endl;
    return 1;
  }

  bool iterate = true;
  string line, prompt = "ufmotor> ";

  if( raw != "true" && raw != "false" ) {
    // explicit command(s) should be executed once
    iterate = false;
    line = "raw " + raw;
  }
  else if( status != "true" && status != "false" ) {
    // explicit command(s) should be executed once
    iterate = false;
    line = "status " + status;
  }
  else if( sim != "true" && sim != "false" ) {
    // explicit command(s) should be executed once
    iterate = false;
    line = "sim " + sim;
  }
  else if( caput != "true" && caput != "false" ) {
    // explicit command(s) should be executed once
    iterate = false;
    line = "caput " + caput;
  }

  vector< string > history;
  UFStrings* reply= 0;

  do {
    if( iterate ) {
      //clog<<prompt<<ends;
      line = motor.getLine(prompt, history);
    }
    UFRuntime::rmJunk(line);
    if( line == "exit" || line == "quit" || line == "q" )
      return 0;
    if( line.size() <= 1 )
      continue; // ignore empty line

    //clog<<"ufmotor> you entered: "<<line<<endl;
    string cmdtyp = "raw";
    vector< string > multiline;
    int statpos = line.find("stat");
    int simpos = line.find("sim");
    int rawpos = line.find("raw");
    int caputpos = line.find("caput");
    if( statpos != (int)string::npos ) {
      cmdtyp = "status";
      line = line.substr(statpos+line.find(" ",statpos)); 
    }
    if( simpos != (int)string::npos ) {
      cmdtyp = "sim";
      line = line.substr(simpos+line.find(" ",simpos));
    }
    else if( rawpos != (int)string::npos ) {
      cmdtyp = "raw";
      line = line.substr(rawpos+line.find(" ",rawpos));
    }
     else if( caputpos != (int)string::npos ) {
      cmdtyp = "caput";
      line = line.substr(caputpos+line.find(" ",caputpos));
    }
    int np = motor.parse(line, multiline);
    if( np <= 0 ) {
      clog << "ufmotor> ? invalid request ?"<<endl;
      continue;
    }
    int ns = motor.submit(cmdtyp, multiline, reply, flush);
    if( ns <= 0 ) {
      clog << "ufmotor> failed to submit: "<<line<<endl;
      return -1;
    }
    if( reply != 0 && expect_reply ) {
      //cout<<reply->timeStamp()<<reply->name()<<endl;
      for( int i=0; i < reply->elements(); ++i )
        cout<<(*reply)[i]<<endl;

      delete reply; reply = 0;
    }
  } while( iterate );

  motor.close(); // force socket closure?
  return 0; 
}

int main(int argc, char** argv, char** envp) {
  return ufmotor::main("ufmotor", argc, argv, envp);
}

#endif // __ufmotor_cc__

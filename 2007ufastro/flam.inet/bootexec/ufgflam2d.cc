#if !defined(__ufgflam2d_cc__)
#define __ufgflam2d_cc__ "$Name:  $ $Id: ufgflam2d.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __ufgflam2d_cc__;

#include "UFFLAMExecutiveAgent.h"

int main(int argc, char** argv, char** env) {
  try {
    UFFLAMExecutiveAgent::main(argc, argv, env);
  }
  catch( std::exception& e ) {
    clog<<"ufgflam2d> exception in stdlib: "<<e.what()<<endl;
    return -1;
  }
  catch( ... ) {
    clog<<"ufgflam2d> unknown exception..."<<endl;
    return -2;
  }
  return 0;
}
#endif // __ufgflam2d_cc__

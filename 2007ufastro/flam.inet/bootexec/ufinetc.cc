#if !defined(__ufinetc_cc__)
#define __ufinetc_cc__ "$Name:  $ $Id: ufinetc.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __ufinetc_cc__;

#include "UFClientSocket.h"
__UFSocket_H__(ufinetc_cc);

#include "UFTimeStamp.h"
__UFTimeStamp_H__(ufinetc_cc);

#include "UFRuntime.h"
__UFRuntime_H__(ufinetc_cc);

#include "cstdio"
#include "cstring"
const bool _verbose = true;

int main(int argc, char** argv, char** envp) {
  // /usr/local/sbin/{ufinetboot,ufstartup,ufboot,ufshutdown}
  // ufinetbootc will be exec'd by the inetd listening on port 3720
  // and so it is expected to associate file desc. 0, 1, and 2 with
  // client connection:
  string usage = "ufinetc -shutdown OR -boot [sim] [-script \"uf start agent script name\"] [-host hostname]";
  if( argc <= 1 ) {
    clog<<"ufinetc> usage: "<<usage<<endl;
    return 0;
  }
  // /etc/services port no registered with iana:
  int ufastro_instr = 3720;
  string host = UFRuntime::hostname();
  UFRuntime::Argv args(argc, argv);
  string arg = UFRuntime::argVal("-host", args);
  if( arg != "true" && arg != "false" )
    host = arg;
  
  arg = UFRuntime::argVal("-h", args);
  if( arg == "true" ) {
    clog<<"ufinetc> usage: "<<usage<<endl;
    return 0;
  }
    
  arg = UFRuntime::argVal("-help", args);
  if( arg == "true" ) {
    clog<<"ufinetc> usage: "<<usage<<endl;
    return 0;
  }

  // default (no cmdline options) is shutdown request
  string inetreq = "UFFLAMShutDown@" + host;
  string boot = UFRuntime::argVal("-boot", args);
  if( boot != "false" ) {
    if( boot == "true" )
      inetreq = "UFFLAMBoot@" + host;
    else // assume sim
      inetreq = "UFFLAMBootSim@" + host;
  }

  string reboot = UFRuntime::argVal("-reboot", args);
  if( reboot != "false" ) {
    if( reboot == "true" )
      inetreq = "UFFLAMReBoot@" + host;
    else // assume sim
      inetreq = "UFFLAMReBootSim@" + host;
  }
  string script = UFRuntime::argVal("-script", args);
  if( script != "false" ) {
    if( reboot == "true" ) {
      if( inetreq.find("Sim") == string::npos )
        inetreq += " script: ufstartagents";
      else
        inetreq += " script: ufsimagents";
    }	
    else
      inetreq += " script: " + script;
  }
  
  string shutdown = UFRuntime::argVal("-shutdown", args);
  if( boot == "false" && reboot == "false" && shutdown == "false" ) {
    clog<<"ufinetc> usage: "<<usage<<endl;
    return 0;
  }

  clog<<"ufinetc> connect to: "<<host<<", port: "<<ufastro_instr<<", send: "<<inetreq<<endl;
  UFClientSocket clsoc;
  if( clsoc.connect(host, ufastro_instr, false) < 0 ) {
    clog<<"ufinetc> failed to connect to ufinetboot"<<endl;
    return 1;
  }
  clog<<"ufinetc> connected to ufinetboot: "<<clsoc.description()<<endl;
  // client connected:
  int ns = clsoc.send(inetreq); // send boot request
  if( ns <= 0 ) {
    clog<<"ufinetc> failed to send request. "<<inetreq<<endl;
    clsoc.close();
    return 2;
  }
  else {
    clog<<"ufinetc> sent request, ns= "<<ns<<", "<<inetreq<<endl;
  }
  // expect single string reply:
  string reply;
  int nr = clsoc.recv(reply);
  if( nr <= 0 ) {
    clog<<"ufinetc> failed to recv boot request reply"<<endl;
    clsoc.close();
    return 3;
  }
  clog<<"ufinetc> reply, nr: "<<nr<<" -- "<<reply<<endl;
  clsoc.close();
  return 0;
}

#endif // __ufinetc_cc__

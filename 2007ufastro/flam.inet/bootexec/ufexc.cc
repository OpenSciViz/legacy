#if !defined(__ufexc_cc__)
#define __ufexc_cc__ "$Name:  $ $Id: ufexc.cc 14 2008-06-11 01:49:45Z hon $"

#include "string"
#include "vector"
#include "stdio.h"

#include "UFClientApp.h"
//#include "UFExecutiveConfig.h"

class ufexc : public UFClientApp {
public:

  inline ufexc(const string& name, int argc,
	       char** argv, char** envp, int port= -1) : UFClientApp(name, argc, argv, envp, port) {}
  inline ~ufexc() {}
  static int main(const string& name, int argc, char** argv, char** envp);
  inline virtual string description() const { return __ufexc_cc__; }

  // options: -pwron/off "all;1-8;name1-name8"
  // or:      -pwrstat "all;rmsamps;maxamps;celsius;1-8;name1-name8"
  // or:      -sim "all;agentname"
  // or:      -star "all;agentname"
  // or:      -stop  "all;agentname"
  int pwrOnOff(string& pwrOnOff, string& chan);
  int pwrStat(string& stat);
  int agent(string& simstartstop, string& agent);

  // interactive mode: (no '-')
  int parse(const string& line, string& shutdown,
	    string& pwron, string& pwroff, string& pwrstat,
	    string& sim, string& start, string& stop);

  // submit req. bundle, flush output and sleep a bit.
  // recv reply and return reply as string.
  int submit(vector< string >& req, UFStrings*& reply, float flush= -1.0);
};

int ufexc::parse(const string& line, string& shutdown,
		 string& pwron, string& pwroff, string& pwrstat,
		 string& sim, string& start, string& stop) {
  //
  shutdown = pwron = pwroff = pwrstat = sim = start = stop = "false";
  size_t pos = line.find("shutdown"); size_t offset = strlen("shutdown");
  if( pos != string::npos ) {
    shutdown = line.substr(pos+offset);
    return 1;
  }
  pos = line.find("on"); offset = strlen("on");
  if( pos != string::npos ) {
    pwron = line.substr(pos+offset);
    return 1;
  }
  pos = line.find("off"); offset = strlen("off");
  if( pos != string::npos ) {
    pwroff = line.substr(pos+offset);
    return 1;
  }
  pos = line.find("stat"); offset = strlen("stat");
  if( pos != string::npos ) {
    pwrstat = line.substr(pos+offset);
    return 1;
  }

  pos = line.find("sim"); offset = strlen("sim");
  if( pos != string::npos ) {
    sim = line.substr(pos+offset);
    return 1;
  }
  pos = line.find("start"); offset = strlen("start");
  if( pos != string::npos ) {
    start = line.substr(pos+offset);
    return 1;
  }
  pos = line.find("stop"); offset = strlen("stop");
  if( pos != string::npos ) {
    stop = line.substr(pos+offset);
    return 1;
  }
  return 0;
}

int ufexc::submit(vector< string >& req, UFStrings*& r, float flush) {
  UFStrings ufs(name(), req);
  int ns = send(ufs);
  if( ns <= 0 )
    return ns;
  // try flushing the socket output stream
  // this is the only way i can think to do it:
  int stat = fflush(_fs);
  if( stat != 0 ) clog<<"ufexc::submitPar> socket flush failed? ns= "<<ns<<endl;

  if( flush > 0.0 ) // optionally sleep after the flush
    UFPosixRuntime::sleep(flush);

  r = dynamic_cast<UFStrings*> (UFProtocol::createFrom(*this));
  if( r != 0 )
    return r->elements();
  else
    return 0;
}


int ufexc::main(const string& name, int argc, char** argv, char** envp) {
  ufexc ex(name, argc, argv, envp);
  string arg, host(hostname());
  string pwrstat= "false", pwron= "false", pwroff= "false",
         sim= "false", start= "false", stop= "false",
         cmdline= "false", shutdown= "false";
  int port= 52000;
  float flush= -1.0;

  string usage = "usage: 'ufexc [-q] [-flush sec.] [-host host] [-port port] [-pwrn/off all;1-8;name1-name9] [-pwrstat all;rmsamps;maxamps;celsius;1-8;name1-name8] [-sim all;agentname] [-start all;agentname] [-stop all;agentname]'";
  
  arg = ex.findArg("-h");
  if( arg != "false" ) {
    clog<<"ufexc> "<<usage<<endl;
    return 0;
  }

  arg = ex.findArg("-help");
  if( arg != "false" ) {
    clog<<"ufexc> "<<usage<<endl;
    return 0;
  }

  arg = ex.findArg("-q");
  if( arg != "false" )
    _quiet = true;

  arg = ex.findArg("-host");
  if( arg != "false" )
    host = arg;

  arg = ex.findArg("-port");
  if( arg != "false" && arg != "true" )
    port = atoi(arg.c_str());

  if( port <= 0 || host.empty() ) {
    clog<<"ufexc> "<<usage<<endl;
    return 0;
  }

  arg = ex.findArg("-flush");
  if( arg != "false" )
    flush = atof(arg.c_str());

  arg = ex.findArg("-shutdown");
  if( arg != "false" ) { 
    if( arg != "true" ) 
      shutdown = cmdline = "Full"; // stop all agents, powerOff all baytech channels, terminate ufgflam2d
    else
      shutdown = cmdline = "Executive"; // terminate ufgflam2d only
  }

  arg = ex.findArg("-pwrstat");
  if( arg != "false" ) { 
    if( arg != "true" ) 
      pwrstat = cmdline = arg;
    else
      pwrstat = cmdline = "All";
  }

  arg = ex.findArg("-pwron");
  if( arg != "false" ) { 
    if( arg != "true" ) 
      pwron = cmdline = arg;
    else
      pwron = cmdline = "All";
  }

  arg = ex.findArg("-pwroff");
  if( arg != "false" ) { 
    if( arg != "true" ) 
      pwroff = cmdline = arg;
    else
      pwroff = cmdline = "All";
  }

  arg = ex.findArg("-sim");
  if( arg != "false" ) {
    if( arg != "true" ) 
      sim = cmdline = arg;
    else
      sim = cmdline = "All";
  }

  arg = ex.findArg("-start");
  if( arg != "false" ) {
    if( arg != "true" ) 
      start = cmdline = arg;
    else
      start = cmdline = "All";
  }

  arg = ex.findArg("-stop");
  if( arg != "false" ) {
    if( arg != "true" ) 
      stop = cmdline = arg;
    else
      stop = cmdline = "All";
  }

  FILE* f  = ex.init(host, port);
  if( f == 0 ) {
    clog<<"ufexc> unable to connect to Executive command server agent..."<<endl;
    return -2;
  }

  int ns= 0;
  UFStrings* preply= 0;
  vector< string > req;

  if( cmdline != "false" ) {
    // command should be executed once
    if( shutdown != "false" ) {
      req.push_back("Shutdown");
      req.push_back(shutdown);
      ns = ex.submit(req, preply, flush);
      if( ns <= 0 ) {
        clog << "ufexc> failed to submit: "<<req[0]<<"::"<<req[1]<<endl;
        return -3;
      }
    }
    else if( pwroff != "false" ) {
      req.push_back("PowerOff");
      req.push_back(pwroff);
      ns = ex.submit(req, preply, flush);
      if( ns <= 0 ) {
        clog << "ufexc> failed to submit: "<<req[0]<<"::"<<req[1]<<endl;
        return -3;
      }
    }
    else if( pwron != "false" ) {
      req.push_back("PowerOn");
      req.push_back(pwron);
      ns = ex.submit(req, preply, flush);
      if( ns <= 0 ) {
        clog << "ufexc> failed to submit: "<<req[0]<<"::"<<req[1]<<endl;
        return -3;
      }
    }
    else if( pwrstat != "false" ) {
      req.push_back("PowerStat");
      req.push_back(pwrstat);
      ns = ex.submit(req, preply, flush);
      if( ns <= 0 ) {
        clog << "ufexc> failed to submit: "<<req[0]<<"::"<<req[1]<<endl;
        return -3;
      }
    }
    else if( sim != "false" ) {
      req.push_back("SimAgent");
      req.push_back(sim);
      ns = ex.submit(req, preply, flush);
      if( ns <= 0 ) {
        clog << "ufexc> failed to submit: "<<req[0]<<"::"<<req[1]<<endl;
        return -3;
      }
    }
    else if( start != "false" ) {
      req.push_back("StartAgent");
      req.push_back(start);
      ns = ex.submit(req, preply, flush);
      if( ns <= 0 ) {
        clog << "ufexc> failed to submit: "<<req[0]<<"::"<<req[1]<<endl;
        return -3;
      }
    }
    else if( stop != "false" ) {
      req.push_back("StopAgent");
      req.push_back(stop);
      ns = ex.submit(req, preply, flush);
      if( ns <= 0 ) {
        clog << "ufexc> failed to submit: "<<req[0]<<"::"<<req[1]<<endl;
        return -3;
      }
    }

    UFStrings& reply = *preply;
    if( !_quiet )
      cout<<reply.timeStamp()<<reply.name()<<endl;
    for( int i=0; i < reply.elements(); ++i )
      cout<<reply[i]<<endl;

    delete preply; preply = 0;
    return ex.close();
  }

  string line;
  while( true ) {
    clog<<"ufexc> "<<ends;
    getline(cin, line);
    if( line == "exit" || line == "quit" || line == "q" ) return 0;
    if( line.size() <= 1 )
      continue; // ignore empty line

    int n = ex.parse(line, shutdown, pwron, pwroff, pwrstat, sim, start, stop);
    if( n <= 0 )
      continue;

    if( shutdown != "false" ) {
      req.push_back("Shutdown");
      req.push_back(shutdown);
      ns = ex.submit(req, preply, flush);
      if( ns <= 0 ) {
        clog << "ufexc> failed to submit: "<<req[0]<<"::"<<req[1]<<endl;
        return -3;
      }
    }
    else if( pwroff != "false" ) {
      req.push_back("PowerOff");
      req.push_back(pwroff);
      ns = ex.submit(req, preply, flush);
      if( ns <= 0 ) {
        clog << "ufexc> failed to submit: "<<req[0]<<"::"<<req[1]<<endl;
        return -3;
      }
    }
    else if( pwron != "false" ) {
      req.push_back("PowerOn");
      req.push_back(pwron);
      ns = ex.submit(req, preply, flush);
      if( ns <= 0 ) {
        clog << "ufexc> failed to submit: "<<req[0]<<"::"<<req[1]<<endl;
        return -3;
      }
    }
    else if( pwrstat != "false" ) {
      req.push_back("PowerStat");
      req.push_back(pwrstat);
      ns = ex.submit(req, preply, flush);
      if( ns <= 0 ) {
        clog << "ufexc> failed to submit: "<<req[0]<<"::"<<req[1]<<endl;
        return -3;
      }
    }
    else if( sim != "false" ) {
      req.push_back("SimAgent");
      req.push_back(sim);
      ns = ex.submit(req, preply, flush);
      if( ns <= 0 ) {
        clog << "ufexc> failed to submit: "<<req[0]<<"::"<<req[1]<<endl;
        return -3;
      }
    }
    else if( start != "false" ) {
      req.push_back("StartAgent");
      req.push_back(start);
      ns = ex.submit(req, preply, flush);
      if( ns <= 0 ) {
        clog << "ufexc> failed to submit: "<<req[0]<<"::"<<req[1]<<endl;
        return -3;
      }
    }
    else if( stop != "false" ) {
      req.push_back("StopAgent");
      req.push_back(stop);
      ns = ex.submit(req, preply, flush);
      if( ns <= 0 ) {
        clog << "ufexc> failed to submit: "<<req[0]<<"::"<<req[1]<<endl;
        return -3;
      }
    }

    UFStrings& reply = *preply;
    if( !_quiet )
      cout<<reply.timeStamp()<<reply.name()<<endl;
    for( int i=0; i < reply.elements(); ++i )
      cout<<reply[i]<<endl;

    delete preply; preply = 0;
    req.clear();
  }

  return ex.close(); 
}

int main(int argc, char** argv, char** envp) {
  return ufexc::main("ufexc", argc, argv, envp);
}

#endif // __ufexc_cc__

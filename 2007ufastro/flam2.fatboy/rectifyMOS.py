#!/usr/bin/python -u
from numarray import *
import numarray.strings
import scipy
from scipy.optimize import leastsq
import pyfits
from UFPipelineOps import *
import curses.ascii
from pyraf import iraf
from datetime import datetime
import Numeric
import numarray.convolve as conv

class rectifyMOS:

   def __init__(self, infile, logfile, warnfile, guiMessages):
      #Read input parameter file
      self.guiMessages = guiMessages
      if (self.guiMessages):
        print "STATUS: Initialization..."
        print "PROGRESS: 0"
      f = open(infile, "rb")
      self.params = f.read()
      f.close()
      #Setup log, warning files
      self.logfile = logfile
      self.warnfile = warnfile
      f = open(self.logfile,'wb')
      f.write('LOG of '+infile+' at ' +  str(datetime.today()) + '\n')
      f.write('Run at path: '+os.getcwd()+'\n')
      f.write('--------Parameter List--------\n')
      f.write(self.params)
      f.write('------------------------------\n')
      f.close()
      self.params = self.params.split("\n")
      try: self.params.remove('')
      except ValueError:
	dummy = '' 
      for j in range(len(self.params)-1, -1, -1):
        if (self.params[j].strip() == ''): self.params.pop(j)
        elif (not curses.ascii.isalpha(self.params[j][0])): self.params.pop(j)
      self.params = dict([(j[0:j.find("=")].strip().upper(), j[j.find("=")+1:].strip()) for j in self.params])
      self.setDefaultParams()
      f = open(self.warnfile,'wb')
      f.write('Warnings and errors for '+infile+' at '+ str(datetime.today())+'\n')
      f.close()
      self.files = []
      self.ycen = []
      self.xsize = 2048
      f = open(self.params['REG_FILE_LIST'],'rb')
      self.rfiles = f.read().split('\n')
      f.close()
      try: self.rfiles.remove('')
      except ValueError:
        dummy = ''
      if (guiMessages): print "PROGRESS: 5"
      if (self.params['DO_REGMASSAGE'].lower() == "yes"):
	if (guiMessages): print "STATUS: Running regmassage..."
	for j in range(len(self.rfiles)):
	   os.system(self.params['REGMASSAGE_PATH']+' '+self.rfiles[j]+' '+self.params['REGMASSAGE_OFFSET'])
	   self.rfiles[j] += '_2.reg'
      if (guiMessages): print "PROGRESS: 10"
      if (self.params['SETUP_LIST'].lower() == "yes"):
	if (guiMessages): print "STATUS: Aligning region files..."
	self.alignRegion()
	if (guiMessages):
	   print "PROGRESS: 15"
	   print "STATUS: Finding continua..."
	self.readFiles(self.params['FILE_LIST'])
      if (guiMessages): print "PROGRESS: 20"
      if (self.params['DO_RECTIFY'].lower() == "yes"):
        if (guiMessages): print "STATUS: Rectifying continua..."
	speclsq = self.doRectify()
      else:
	speclsq = []

   def setDefaultParams(self):
      #Set Default Parameters
      self.params.setdefault('FILE_LIST','list.dat')
      self.params.setdefault('DARK_FRAME','')
      self.params.setdefault('REG_FILE_LIST','')
      self.params.setdefault('FLAT_FIELD_LIST','')
      self.params.setdefault('DARK_FRAMES_FOR_FLATS','')
      self.params.setdefault('DO_REGMASSAGE','no')
      self.params.setdefault('REGMASSAGE_PATH','regmassage')
      self.params.setdefault('REGMASSAGE_OFFSET','0')
      self.params.setdefault('SETUP_LIST','yes')
      self.params.setdefault('MAX_SLIT_WIDTH','10')
      self.params['MAX_SLIT_WIDTH'] = float(self.params['MAX_SLIT_WIDTH'])
      self.params.setdefault('MAX_SPECTRAL_WIDTH','20')
      self.params['MAX_SPECTRAL_WIDTH'] = float(self.params['MAX_SPECTRAL_WIDTH'])
      self.params.setdefault('SPECTRAL_THRESHOLD','2')
      self.params['SPECTRAL_THRESHOLD'] = float(self.params['SPECTRAL_THRESHOLD'])
      self.params.setdefault('BOX_CENTER','1024')
      self.params['BOX_CENTER'] = int(self.params['BOX_CENTER'])
      self.params.setdefault('BOX_WIDTH','1000')
      self.params['BOX_WIDTH'] = int(self.params['BOX_WIDTH'])
      self.params.setdefault('SPECTRAL_ORIENTATION','horizontal')
      self.params.setdefault('LIST_OUTPUT','list.out')
      self.params.setdefault('DO_RECTIFY','yes')
      self.params.setdefault('LIST_INPUT','')
      self.params.setdefault('MIN_COVERAGE_FRACTION','70')
      self.params['MIN_COVERAGE_FRACTION'] = float(self.params['MIN_COVERAGE_FRACTION'])
      self.params.setdefault('DATABASE_FILE','mosrectdb.dat')
      self.params.setdefault('APPEND_DATABASE','yes')
      self.params.setdefault('FIT_ORDER','3')
      self.params['FIT_ORDER'] = int(self.params['FIT_ORDER'])

   def alignRegion(self):
      print "Reading Region File..."
      f = open(self.logfile,'ab')
      f.write("Reading Region File...\n")
      f.close()
      f = open(self.params['FLAT_FIELD_LIST'],'rb')
      flats = f.read().split('\n')
      f.close()
      try: flats.remove('')
      except ValueError:
        dummy = ''
      f = open(self.params['DARK_FRAMES_FOR_FLATS'],'rb')
      darks = f.read().split('\n')
      f.close()
      try: darks.remove('')
      except ValueError:
        dummy = ''
      self.slitx = []
      self.slity = []
      self.slitw = []
      self.slith = []
      for l in range(len(self.rfiles)):
	sx = []
	sy = []
	sw = []
	sh = []
	if (not (os.access(self.rfiles[l], os.F_OK) and os.access(flats[l], os.F_OK) and os.access(darks[l],os.F_OK))):
	   print 'Region file ' + self.rfiles[l] + ' not valid!'
           f = open(self.logfile,'ab')
           f.write('Region file ' + self.rfiles[l] + ' not valid!\n')
           f.close()
	   f = open(self.warnfile,'ab')
	   f.write('WARNING: Region file ' + self.rfiles[l] + ' not valid!\n')
           f.write('Time: '+str(datetime.today())+'\n\n')
	   f.close()
	   for k in range(2):
	      self.slitx.append(array(sx))
	      self.slity.append(array(sy))
	      self.slitw.append(array(sw))
	      self.slith.append(array(sh))
	   continue
        f = open(self.rfiles[l],'rb')
        s = f.read().split('\n')
        f.close()
	try: s.remove('')
        except ValueError:
	   dummy = ''
        for j in range(len(s)):
	   if (s[j].startswith("image;box")):
	      temp = s[j][s[j].find('(')+1:]
	      temp = temp.split(',')
              if (temp[-1].find(')') != -1):
                temp[-1] = temp[-1][:temp[-1].find(')')]
	      sx.append(float(temp[0]))
	      sy.append(float(temp[1]))
	      sw.append(float(temp[2]))
	      sh.append(float(temp[3]))
        sx = array(sx)
        sy = array(sy)
        sw = array(sw)
        sh = array(sh)
        temp = pyfits.open(flats[l])
        d = pyfits.open(darks[l])
        self.xsize = d[0].data.shape[1]
        self.ysize = d[0].data.shape[0]
        temp[0].data -= d[0].data
        flat = arraymedian(temp[0].data,axis="X")
        d.close()
        temp.close()
        islit = zeros(self.ysize)+0.
        for j in range(len(sy)):
           ylo = max(1,int(sy[j]-sh[j]/2))-1
           yhi = int(sy[j]+sh[j]/2)-1
           islit[ylo:yhi]=5000
        #ccor = scipy.signal.correlate(flat,islit,mode='same')
        ccor = conv.correlate(flat,islit,mode='same')
        ccor = array(ccor)
        mcor = where(ccor == max(ccor))[0][0]
        yshift = len(ccor)/2-mcor
        sy-=yshift
        xshift = (self.xsize/2.)-(min(sx)+max(sx))/2.
        sx+=xshift
	#Write new region file
        f = open(self.rfiles[l],'wb')
        f.write('# Region file format: DS9 version 3.0\n')
        f.write('global color=green select=1 edit=1 move=1 delete=1 include=1 fixed=0\n')
	for j in range(len(sy)):
	   f.write('image;box('+str(sx[j])+','+str(sy[j])+','+str(sw[j])+','+str(sh[j])+')\n')
	f.close()
	b = where(sw <= self.params['MAX_SLIT_WIDTH'])
	for k in range(2):
	   self.slitx.append(sx[b])
           self.slity.append(sy[b])
	   self.slitw.append(sw[b])
	   self.slith.append(sh[b])

   def readFiles(self, infile):
      print "Reading Files..."
      f = open(self.logfile,'ab')
      f.write("Reading Files...\n")
      f.close()
      f = open(infile)
      flist = f.read().split('\n')
      f.close()
      try: flist.remove('')
      except ValueError:
        dummy = ''
      self.ycen = []
      self.ybox = []
      self.xslit = []
      for j in range(len(flist)):
        ycenters = []
        yboxsize = []
	tempxslit = []
	temp = pyfits.open(flist[j])
	if (j % 2 == 0):
	   sky = pyfits.open(flist[j+1])
	else:
	   sky = pyfits.open(flist[j-1])
	temp[0].data -= sky[0].data
	box1 = self.params['BOX_CENTER']-self.params['BOX_WIDTH']/2
	box2 = self.params['BOX_CENTER']+self.params['BOX_WIDTH']/2
        #x = sum(temp[0].data[:,box1:box2+1], 1)
	x = arraymedian(temp[0].data[:,box1:box2+1], axis="X")
	y = extractSpectra(x,self.params['SPECTRAL_THRESHOLD'],5)
	for l in range(len(y)):
	   yc = (y[l][0]+y[l][1])/2
	   if (yc < 50 or yc > (self.ysize-50)):
	      continue
	   b = compress(where(self.slity[j]-self.slith[j]/2 < yc,1,0)*where(self.slity[j]+self.slith[j]/2 > yc,1,0), self.slitx[j])
	   if (len(b) == 1 and (y[l][1]-y[l][0])/2 <= self.params['MAX_SPECTRAL_WIDTH']):
	      ycenters.append(yc)
              yboxsize.append((y[l][1]-y[l][0])/2)
	      tempxslit.append(b[0])
	sky.close()
	temp.close()
        ycenters = array(ycenters)
	yboxsize = array(yboxsize)
	tempxslit = array(tempxslit)
	self.ycen.append(ycenters)
	self.ybox.append(yboxsize)
	self.xslit.append(tempxslit)
      self.files = numarray.strings.array(flist)
      f = open(self.params['LIST_OUTPUT'],'wb')
      for j in range(len(self.files)):
	f.write(self.files[j]+'\t'+str(len(self.ycen[j]))+'\n')
	for l in range(len(self.ycen[j])):
	   f.write(str(self.ycen[j][l])+'\t'+str(self.ybox[j][l])+'\t'+str(self.xslit[j][l])+'\n')
      f.close()

   def gaussResiduals(self, p, x, out):
      f = zeros(len(x))+0.
      z = (x-p[1])/p[2]
      f = p[3]+p[0]*math.e**(-z**2/2)
      err = out-f
      err = Numeric.array(err.tolist())
      return err

   def surfaceResiduals(self, p, x, y, z, out, order):
      f = zeros(len(x))+0.
      n = 1
      for j in range(1,order+1):
        for l in range(1,j+2):
	   for k in range(1,l+1):
	      f+=p[n]*x**(j-l+1)*y**(l-k)*z**(k-1)
              n+=1
      err = out - f
      err = Numeric.array(err.tolist())
      return err

   def polyResiduals(self, p, x, y, out, order):
      f = zeros(len(x))+0.
      f+=p[0]*y
      for j in range(1,order+1):
	f+=p[j]*x**j
      err = out-f
      err = Numeric.array(err.tolist())
      return err

   def doRectify(self):
      print "Performing rectification..."
      f = open(self.logfile,'ab')
      f.write("Performing rectification...\n")
      f.close()
      if (os.access(self.params['LIST_INPUT'], os.F_OK)):
	f = open(self.params['LIST_INPUT'],'rb')
	temp = f.read().split('\n')
	f.close()
	try: temp.remove('')
        except ValueError:
           dummy = ''
	self.files = []
	self.ycen = []
	self.ybox = []
	self.xslit = []
	while (len(temp) > 0):
	   t = temp.pop(0).split()
	   self.files.append(t[0])
	   n = int(t[1])
	   ycenters = []
	   yboxsize = []
	   tempxslit = []
	   for j in range(n):
	      t = temp.pop(0).split()
	      ycenters.append(float(t[0]))
	      yboxsize.append(float(t[1]))
	      tempxslit.append(float(t[2]))
	   self.ycen.append(array(ycenters))
	   self.ybox.append(array(yboxsize).astype("Int32"))
	   self.xslit.append(array(tempxslit))
	temp = pyfits.open(self.files[0])
        self.xsize = temp[0].data.shape[1]
        self.ysize = temp[0].data.shape[0]
	temp.close()
      print "Using "+str(len(self.files))+" files..."
      f = open(self.logfile,'ab')
      f.write("Using "+str(len(self.files))+" files...\n")
      f.close()
      count = 0
      step = 5
      xin = []
      yin = []
      yout = []
      xslitin = []
      #xin = zeros((len(xs), len(self.files)),"Float64")
      #yin = zeros((len(xs), len(self.files)),"Float64")
      #yout = zeros((len(xs), len(self.files)),"Float64")
      #box1 = self.params['BOX_CENTER']-self.params['BOX_WIDTH']/2
      #box2 = self.params['BOX_CENTER']+self.params['BOX_WIDTH']/2
      for j in range(len(self.files)):
        temp = pyfits.open(self.files[j])
        if (j % 2 == 0):
           sky = pyfits.open(self.files[j+1])
        else:
           sky = pyfits.open(self.files[j-1])
        temp[0].data -= sky[0].data
	for k in range(len(self.ycen[j])):
	   if (guiMessages):
	      print "PROGRESS: "+str(int(20+((j+0.)/len(self.files)+(k+0.)/(len(self.ycen[j])*len(self.files)))*70))
	   currY = self.ycen[j][k]
           #if (self.ybox[j][k] < 3): self.ybox[j][k] = 3
           ylo = int(currY - self.ybox[j][k])
           yhi = int(currY + self.ybox[j][k])+1
	   #cut1d = arraymedian(temp[0].data[ylo:yhi,:],axis="Y")
           cut1d = sum(temp[0].data[ylo:yhi,:],0)
	   cut1d = smooth1d(cut1d,50,5)
	   xs = [where(cut1d[100:-100] == cut1d[100:-100].max())[0][0]+100]
	   #if (xs[0] < box1 or xs[0] > box2):
	      #xs = [self.params['BOX_CENTER']] 
	   #print cut1d[1024], arraymedian(cut1d), cut1d.stddev()
	   #xs = [1024]
           for l in range(xs[0]-step,20,-1*step):
	      xs.append(l)
           for l in range(xs[0]+step,self.xsize-9-self.xsize%10,step):
              xs.append(l)
	   txin = zeros(len(xs),"Float64")
	   tyin = zeros(len(xs),"Float64")
	   tyout = zeros(len(xs),"Float64")
	   txslitin = zeros(len(xs),"Float64")
	   currX = xs[0]
           if (self.ybox[j][k] < 3): self.ybox[j][k] = 3
	   lastXs = []
	   lastYs = []
	   for l in range(len(xs)):
	      if (xs[l] == xs[0]+step):
		currY = self.ycen[j][k]
	        lastYs = [tyin[0]]
		lastXs = [xs[0]]
	      ylo = int(currY - self.ybox[j][k])
	      yhi = int(currY + self.ybox[j][k])+1
	      #y = sum(temp[0].data[:,xs[l]-2:xs[l]+3], 1)+0.
	      #y = arraymedian(temp[0].data[:,xs[l]-2:xs[l]+3], axis="X")+0.
	      #x = arange(len(y))+0.
	      if (abs(xs[l]-currX) > 50):
		ylo = int(currY - self.ybox[j][k]-int(abs(xs[l]-currX)*.02))
	        yhi = int(currY + self.ybox[j][k]+int(abs(xs[l]-currX)*.02))+1
	      outerbox = arraymedian(temp[0].data[max(0, ylo-self.ybox[j][k]):yhi+self.ybox[j][k],xs[l]-2:xs[l]+3], axis="X")+0.
	      y = arraymedian(temp[0].data[ylo:yhi,xs[l]-2:xs[l]+3], axis="X")+0.
	      #outerbox = y[int(ylo-self.ybox[j][k]):int(yhi+self.ybox[j][k])]
	      x = arange(len(y))+ylo+0.
	      noData = False
	      #if (y[ylo:yhi].mean() <= outerbox.mean()):
	      if (y.mean() <= outerbox.mean()):
		noData = True 
	      #if (arraymedian(y[ylo:yhi]) <= arraymedian(outerbox)):
	      if (arraymedian(y) <= arraymedian(outerbox)):
		noData = True 
	      #if (max(smooth1d(y[ylo:yhi],3,1)) < arraymedian(outerbox) + outerbox.stddev()):
	      if (max(smooth1d(y,3,1)) < arraymedian(outerbox)+outerbox.stddev()):
		noData = True
	      if (noData == True and l == 0): break
	      #if (abs(xs[l]-currX) > 100): noData = False
	      if (not noData):
		p = zeros(4)+0.
	        #p[0] = max(y[ylo:yhi])
		p[0] = max(y)
	        #p[1] = (where(y[ylo:yhi] == p[0]))[0][0]+ylo
		p[1] = where(y == p[0])[0][0]+ylo
	        p[2] = self.ybox[j][k]/2
	        p[3] = arraymedian(y)
		p = Numeric.array(p.tolist())
	        #lsq = leastsq(self.gaussResiduals, p, args=(x[ylo:yhi], y[ylo:yhi]))
                lsq = leastsq(self.gaussResiduals, p, args=(x, y))
		#print xs[l],lsq[0][0:],y.max()
		if (lsq[0][0]+lsq[0][3] < 0): continue
		if (lsq[0][2] < 0 and l != 0): continue
		if (l == 0):
		   currY = lsq[0][1]
		   currX = xs[0]
		   txin[0] = xs[0]
		   tyin[0] = lsq[0][1]
		   txslitin[0] = self.xslit[j][k]
		   tyout[0] = tyin[0]
		   lastXs.append(xs[0])
		   lastYs.append(lsq[0][1])
		else:
		   wavg = 0.
		   wavgx = 0.
		   wavgDivisor = 0.
		   for i in range(len(lastYs)):
		      wavg += lastYs[i]/sqrt(abs(lastXs[i]-xs[l]))
		      wavgx += lastXs[i]/sqrt(abs(lastXs[i]-xs[l]))
		      wavgDivisor += 1./sqrt(abs(lastXs[i]-xs[l]))
                      #wavg += lastYs[i]/abs(lastXs[i]-xs[l])
                      #wavgx += lastXs[i]/abs(lastXs[i]-xs[l])
                      #wavgDivisor += 1./abs(lastXs[i]-xs[l])
		   if (wavgDivisor != 0):
		      wavg = wavg/wavgDivisor
		      wavgx = wavgx/wavgDivisor
		   else:
		      wavg = currY
		      wavgx = currX
                   #med = arraymedian(array(lastYs))
		   #b = where(abs(array(lastYs)-med) == min(abs(array(lastYs)-med)))[0][0]
		   #if (abs(xs[l]-xs[l-1]) == step and abs(lastXs[b]-xs[l]) > 50):
		   if (abs(xs[l]-xs[l-1]) == step and abs(wavgx-xs[l]) > 50):
		      if (len(lastYs) > 1):
			lin = leastsq(linResiduals, [0.,0.], args=(array(lastXs),array(lastYs)))
			slope = lin[0][1]
		      else:
			slope = 0.
		      #refY = slope*(xs[l]-lastFiveXs[b])+med
		      refY = wavg+slope*(xs[l]-wavgx)
		      maxerr = 2+int(abs(xs[l]-wavgx)*.02)
		      #maxerr = 2
		   else:
		      maxerr = 2
		      refY = wavg 
		      slope = 0
                   #print j,xs[l],lsq[0][1],refY, wavgx, lsq[0][0]+lsq[0][3], abs(lsq[0][1] - refY) < maxerr
		   #if (abs(lsq[0][1] - currY) < maxerr):
		   if (xs[l] == xs[0]+step and abs(lsq[0][1]-tyin[0]) < 3):
                      currY = lsq[0][1]
                      currX = xs[l]
                      txin[l] = xs[l]
                      tyin[l] = currY
                      txslitin[l] = self.xslit[j][k]
                      tyout[l] = tyin[0]
		      lastXs.append(xs[l])
                      lastYs.append(lsq[0][1])
                   #if (abs(lsq[0][1] - currY) < maxerr):
		   elif (abs(lsq[0][1] - refY) < maxerr):
		      currY = lsq[0][1]
		      currX = xs[l]
		      txin[l] = xs[l]
		      tyin[l] = currY
		      txslitin[l] = self.xslit[j][k]
		      tyout[l] = tyin[0]
		      lastXs.append(xs[l])
		      lastYs.append(lsq[0][1])
		      if (len(lastYs) > 10):
			lastXs.pop(0)
			lastYs.pop(0)
	   b = where(abs(txin-self.params['BOX_CENTER']) == min(abs(txin-self.params['BOX_CENTER'])))[0][0]
	   tyout[:] = tyin[b]
	   sky.close()
	   temp.close()
	   covfrac = len(where(tyin > 0)[0])/(len(tyin)+0.)*100
	   print currY, covfrac, self.ycen[j][k], xs[0]
           f = open(self.logfile,'ab')
           f.write("\tY: "+str(self.ycen[j][k])+"\t Xref: "+str(xs[0])+"\t Cov. Frac: "+str(covfrac)+"\n")
           f.close()
	   if (covfrac >= self.params['MIN_COVERAGE_FRACTION']):
	      xin.append(txin)
	      yin.append(tyin)
	      xslitin.append(txslitin)
	      yout.append(tyout)
      nlines = len(xin)
      order = self.params['FIT_ORDER']
      terms = 0
      nterms = 0
      for j in range(order+2):
	nterms+=j
	terms+=nterms
      p = zeros(terms)
      p[2] = 1
      xin = array(xin)
      yin = array(yin)
      yout = array(yout)
      xslitin = array(xslitin)
      xin = reshape(xin, xin.nelements())+0.
      yin = reshape(yin, yin.nelements())+0.
      yout = reshape(yout, yout.nelements())+0.
      xslitin = reshape(xslitin, xslitin.nelements())+0.
      if (guiMessages):
	print "STATUS: Fitting..."
	print "PROGRESS: 90"
      if (self.params['APPEND_DATABASE'].lower() == 'no'):
	f = open(self.params['DATABASE_FILE'],'wb')
      else:
	f = open(self.params['DATABASE_FILE'],'ab')
      for j in range(len(yin)):
	f.write(str(xin[j])+'\t'+str(yin[j])+'\t'+str(yout[j])+'\t'+str(xslitin[j])+'\n')
      f.close()
      if (self.params['APPEND_DATABASE'].lower() == 'yes'):
	f = open(self.params['DATABASE_FILE'],'rb')
	s = f.read().split('\n')
	f.close()
	try: s.remove('')
        except ValueError:
           dummy = ''
	xin = []
	yin = []
	yout = []
	xslitin = []
	for j in range(len(s)):
	   temp = s[j].split()
	   xin.append(float(temp[0]))
	   yin.append(float(temp[1]))
	   yout.append(float(temp[2]))
	   xslitin.append(float(temp[3]))
	xin = array(xin)
	yin = array(yin)
	yout = array(yout)
	xslitin = array(xslitin)
      b = where(xin != 0)
      yout = array(yout)
      xin = array(xin)
      yin = array(yin)
      xslitin = array(xslitin)
      p = Numeric.array(p.tolist())
      lsq = leastsq(self.surfaceResiduals, p, args=(xin[b], yin[b], xslitin[b], yout[b], order))
      print lsq
      f = open(self.logfile,'ab')
      f.write("Fit: "+str(lsq[0])+"\n")
      f.close()
      yfit = yin*0.
      n = 1
      for j in range(1,order+1):
        for l in range(1,j+2):
           for k in range(1,l+1):
              yfit[b]+=lsq[0][n]*xin[b]**(j-l+1)*yin[b]**(l-k)*xslitin[b]**(k-1)
              n+=1
      med = arraymedian(yout[b]-yfit[b])
      sig = (yout[b]-yfit[b]).stddev()
      print 'Traced out '+str(nlines)+' continua.'
      print "Data - fit    median: "+str(med)+ "   sigma: "+str(sig)
      f = open(self.logfile,'ab')
      f.write('Traced out '+str(nlines)+' continua.\n')
      f.write("Data - fit    median: "+str(med)+ "   sigma: "+str(sig)+"\n")
      f.close()
      if (guiMessages):
        print "PROGRESS: 93"
      good = where(abs(yout[b]-yfit[b]) <= med + 2*sig)
      p = Numeric.array(p.tolist())
      lsq = leastsq(self.surfaceResiduals, p, args=(xin[b][good], yin[b][good], xslitin[b][good],yout[b][good], order))
      print "REDO: kept "+str((len(good[0])+0.)/len(b[0])*100)+"%"
      print lsq
      f = open(self.logfile,'ab')
      f.write('Throwing away outliers and refitting: kept '+str((len(good[0])+0.)/len(b[0])*100)+"%\n")
      f.write('New fit: '+str(lsq[0])+'\n')
      f.close()
      yfit = yin*0.
      n = 1
      for j in range(1,order+1):
        for l in range(1,j+2):
           for k in range(1,l+1):
              yfit[b]+=lsq[0][n]*xin[b]**(j-l+1)*yin[b]**(l-k)*xslitin[b]**(k-1)
              n+=1
      med = arraymedian(yout[b][good]-yfit[b][good])
      sig = (yout[b][good]-yfit[b][good]).stddev()
      print "Data - fit    median: "+str(med)+ "   sigma: "+str(sig)
      f = open(self.logfile,'ab')
      f.write("Data - fit    median: "+str(med)+ "   sigma: "+str(sig)+"\n")
      f.close()
      if (guiMessages):
        print "STATUS: Writing Database..."
        print "PROGRESS: 96"
      #f = open('rectdata.dat','wb')
      #for j in range(len(yout)):
	#f.write(str(yout[j])+'\t'+str(yin[j])+'\t'+str(yfit[j])+'\n')
      #f.close()
      f = open(self.params['DATABASE_FILE'],'wb')
      for j in range(len(yin[b][good])):
        f.write(str(xin[b][good][j])+'\t'+str(yin[b][good][j])+'\t'+str(yout[b][good][j])+'\t'+str(xslitin[b][good][j])+'\n')
      f.close()
      return lsq[0]

#Main
if (len(sys.argv) > 1): infile = sys.argv[1]
else: infile = 'rectmosparams.dat'
if (len(sys.argv) > 2): logfile = sys.argv[2]
else: logfile = 'rectmospipeline.log'
if (len(sys.argv) > 3): warnfile = sys.argv[3]
else: warnfile = 'rectmospipeline.warn'
if (len(sys.argv) > 4):
   if (sys.argv[4] == "-gui"): guiMessages = True
   else: guiMessages = False
else: guiMessages = False
a = rectifyMOS(infile, logfile, warnfile, guiMessages)
if (guiMessages):
   print "PROGRESS: 100"
   print "STATUS: Done!"
f = open(a.logfile,'ab')
f.write('End time: '+str(datetime.today())+'\n')
f.close()

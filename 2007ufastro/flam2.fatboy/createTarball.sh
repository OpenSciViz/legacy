#!/bin/sh
cp -r FATBOY_GUI.jar *.py fatboy README FATBOY
cp -r distortionMaps/*_driz.dat FATBOY/distortionMaps/
tar cvf fatboy.tar FATBOY/FATBOY_GUI.jar FATBOY/*.py FATBOY/fatboy FATBOY/distortionMaps/*_driz.dat FATBOY/README
gzip fatboy.tar
cp fatboy.tar.gz /astro/data/instlab/INST_GROUP/PROJECTS/FATBOY/

#!/usr/bin/python -u
import math, scipy, pyfits, sys, os
from scipy.optimize import leastsq
from numarray import *
import numarray.strings
#from numarray.image.combine import * 
from UFPipelineOps import *
from pyraf import iraf
from iraf import stsdas
from iraf import dither 
import curses.ascii
import glob
from datetime import datetime
import Numeric
import numarray.convolve as conv

class f2spec:

   #Members:
   #list filenames, symLinks, exptimes, objnames, darkfiles, darktimes
   #list flatfiles, flattimes, flatobjnames
   #badPixelMask bpMask
   #dict params
   #String prefix, delim, bpmFile
   #bool useBPMask

   def __init__(self, infile, logfile, warnfile, guiMessages):
      self.guiMessages = guiMessages
      if (not os.access('.', os.W_OK)):
	print "ERROR: No write access to current working directory!"
	if (self.guiMessages):
	   print "GUI_ERROR: No write access to current working directory!"
	sys.exit()
      if (self.guiMessages):
        print "STATUS: Initialization..."
        print "PROGRESS: 0"
      self.prefix = ""
      self.oldpfix = ''
      self.useBPMask = False 
      #Read input parameter file
      f = open(infile, "rb")
      self.params = f.read()
      f.close()
      #Setup log, warning files
      self.logfile = logfile
      self.warnfile = warnfile
      f = open(self.logfile,'wb')
      f.write('LOG of '+infile+' at ' +  str(datetime.today()) + '\n')
      f.write('Run at path: '+os.getcwd()+'\n')
      f.write('--------Parameter List--------\n')
      f.write(self.params)
      f.write('------------------------------\n')
      f.close()
      self.params = self.params.split("\n")
      try: self.params.remove('') 
      except ValueError:
	junk = ''
      self.params = dict([(j[0:j.find("=")].strip().upper(), j[j.find("=")+1:].strip()) for j in self.params]) 
      self.setDefaultParams()
      self.delim = self.params['PREFIX_DELIMITER']
      f = open(self.warnfile,'wb')
      f.write('Warnings and errors for '+infile+' at '+ str(datetime.today())+'\n')
      f.close()
      #Create Filelist
      filelist = self.params['FILES']
      if (filelist.endswith(',')):
        filelist = filelist[:-1]
      if (os.path.isdir(filelist)):
	self.filenames = os.listdir(filelist)
      elif (os.path.isfile(filelist) and not filelist.lower().endswith('.fits') and not filelist.lower().endswith('.fit')):
        f = open(filelist, "rb")
        self.filenames = f.read()
        f.close()
        self.filenames = self.filenames.split()
      elif (filelist.count(',') != 0):
        self.filenames = filelist.split(',')
        try: self.filenames.remove('')
        except ValueError:
           dummy = ''
        for j in range(len(self.filenames)):
           self.filenames[j] = self.filenames[j].strip()
      else:
	#os.system('ls -1 ' + filelist + ' > tempFlamPipelineFileList.dat')
	#f = open('tempFlamPipelineFileList.dat', "rb")
	#self.filenames = f.read()
        #f.close()
        #self.filenames = self.filenames.split()
        #os.unlink('tempFlamPipelineFileList.dat')
	self.filenames = glob.glob(filelist)
      print "Found ", len(self.filenames), " files..."
      f = open(self.logfile,'ab')
      f.write('Found '+str(len(self.filenames))+' files...\n')
      f.close()
      f = open(self.warnfile,'ab')
      f.write('Found '+str(len(self.filenames))+' files...\n')
      f.close()
      self.guiCRdone = 0
      if (self.params['REMOVE_COSMIC_RAYS'].lower() == 'no'):
        self.guiCRf = 1.0
      else:
        self.guiCRf = 0.5
      self.readAll()
      if (self.guiMessages): print "PROGRESS: "+str(int(self.guiCRf*4+0.1))

   def setDefaultParams(self):
      #Set Default Parameters
      self.params.setdefault('FILES','files.dat')
      self.params.setdefault('GROUP_OUTPUT_BY','filename')
      self.params.setdefault('SPECIFY_GROUPING','')
      self.params.setdefault('MEF_EXTENSION','')
      if (self.params['MEF_EXTENSION'] == ''):
        self.params['MEF_EXTENSION']='-1'
      if (not curses.ascii.isdigit(self.params['MEF_EXTENSION'][0])):
        self.params['MEF_EXTENSION']='-1'
      try:
        self.params['MEF_EXTENSION'] = int(self.params['MEF_EXTENSION'])
      except ValueError:
        self.params['MEF_EXTENSION'] = -1
      self.params.setdefault('DARK_FILE_LIST','')
      self.params.setdefault('FLAT_FILE_LIST','')
      self.params.setdefault('ARCLAMP_FILE_LIST','')
      self.params.setdefault('OVERWRITE_FILES','no')
      self.params.setdefault('QUICK_START_FILE','')
      self.params.setdefault('CLEAN_UP','no')
      self.params.setdefault('DO_NOISEMAPS','yes')
      self.params.setdefault('PREFIX_DELIMITER','.')
      self.params.setdefault('EXPTIME_KEYWORD','EXP_TIME')
      self.params.setdefault('OBSTYPE_KEYWORD','OBS_TYPE')
      self.params.setdefault('FILTER_KEYWORD','FILTER')
      self.params['FILTER_KEYWORD'] = self.params['FILTER_KEYWORD'].replace(' ','').split(',')
      self.params.setdefault('RA_KEYWORD','RA')
      self.params.setdefault('DEC_KEYWORD','DEC')
      self.params.setdefault('RELATIVE_OFFSET_ARCSEC','no')
      self.params.setdefault('OBJECT_KEYWORD','OBJECT')
      self.params.setdefault('GAIN_KEYWORD','GAIN_2')
      self.params.setdefault('READNOISE_KEYWORD','RDNOIS_2')
      self.params.setdefault('GRISM_KEYWORD','GRISM')
      self.params.setdefault('MIN_FRAME_VALUE','0')
      self.params.setdefault('MAX_FRAME_VALUE','0')
      if (self.params['MIN_FRAME_VALUE'] == ''):
	self.params['MIN_FRAME_VALUE'] = '0'
      if (self.params['MAX_FRAME_VALUE'] == ''):
        self.params['MAX_FRAME_VALUE'] = '0'
      self.params['MIN_FRAME_VALUE'] = float(self.params['MIN_FRAME_VALUE'])
      self.params['MAX_FRAME_VALUE'] = float(self.params['MAX_FRAME_VALUE'])
      self.params.setdefault('IGNORE_FIRST_FRAMES','no')
      self.params.setdefault('IGNORE_AFTER_BAD_READ','no')
      self.params.setdefault('ROTATION_ANGLE','')
      if (self.params['ROTATION_ANGLE'] == ''):
	self.params['ROTATION_ANGLE'] = '0'
      self.params['ROTATION_ANGLE'] = float(self.params['ROTATION_ANGLE'])
      self.params.setdefault('DO_LINEARIZE','yes')
      self.params.setdefault('LINEARITY_COEFFS','1')
      self.params['LINEARITY_COEFFS'] = self.params['LINEARITY_COEFFS'].split()
      for j in range(len(self.params['LINEARITY_COEFFS'])):
	self.params['LINEARITY_COEFFS'][j] = float(self.params['LINEARITY_COEFFS'][j])
      self.params.setdefault('DO_DARK_COMBINE','yes')
      self.params.setdefault('DO_DARK_SUBTRACT','yes')
      self.params.setdefault('DEFAULT_MASTER_DARK','')
      if (self.params['DEFAULT_MASTER_DARK'].count('.dat') != 0):
        f = open(self.params['DEFAULT_MASTER_DARK'],'rb')
        s = f.read().split('\n')
        f.close()
        try: s.remove('')
        except ValueError:
           junk = ''
        self.params['DEFAULT_MASTER_DARK'] = s
      elif (self.params['DEFAULT_MASTER_DARK'].count(',') != 0):
        self.params['DEFAULT_MASTER_DARK'] = self.params['DEFAULT_MASTER_DARK'].split(',')
        for j in range(len(self.params['DEFAULT_MASTER_DARK'])):
           self.params['DEFAULT_MASTER_DARK'][j] = self.params['DEFAULT_MASTER_DARK'][j].strip()
      else:
        self.params['DEFAULT_MASTER_DARK'] = self.params['DEFAULT_MASTER_DARK'].split()
      self.params.setdefault('PROMPT_FOR_MISSING_DARK','yes')
      self.params.setdefault('DO_ARCLAMP_COMBINE','yes')
      self.params.setdefault('ARCLAMP_SELECTION','auto')
      self.params.setdefault('DO_FLAT_COMBINE','yes')
      self.params.setdefault('FLAT_SELECTION','auto')
      self.params.setdefault('FLAT_METHOD','on-off')
      self.params.setdefault('FLAT_LAMP_OFF_FILES','off')
      self.params.setdefault('FLAT_LOW_THRESH','0')
      self.params.setdefault('FLAT_LOW_REPLACE','1')
      self.params.setdefault('FLAT_HI_THRESH','0')
      self.params.setdefault('FLAT_HI_REPLACE','1')
      if (self.params['FLAT_LOW_THRESH'] == ''):
        self.params['FLAT_LOW_THRESH'] = '0'
      if (self.params['FLAT_LOW_REPLACE'] == ''):
        self.params['FLAT_LOW_REPLACE'] = '1'
      if (self.params['FLAT_HI_THRESH'] == ''):
        self.params['FLAT_HI_THRESH'] = '0'
      if (self.params['FLAT_HI_REPLACE'] == ''):
        self.params['FLAT_HI_REPLACE'] = '1'
      self.params['FLAT_LOW_THRESH'] = float(self.params['FLAT_LOW_THRESH'])
      self.params['FLAT_LOW_REPLACE'] = float(self.params['FLAT_LOW_REPLACE'])
      self.params['FLAT_HI_THRESH'] = float(self.params['FLAT_HI_THRESH'])
      self.params['FLAT_HI_REPLACE'] = float(self.params['FLAT_HI_REPLACE'])
      self.params.setdefault('DO_BAD_PIXEL_MASK','yes')
      self.params.setdefault('DEFAULT_BAD_PIXEL_MASK','')
      self.params.setdefault('IMAGING_FRAMES_FOR_BPM','')
      self.params.setdefault('BAD_PIXEL_MASK_CLIPPING','values')
      self.params.setdefault('BAD_PIXEL_HIGH','2.0')
      self.params.setdefault('BAD_PIXEL_LOW','0.5')
      self.params.setdefault('BAD_PIXEL_SIGMA','5')
      self.params.setdefault('EDGE_REJECT','5')
      self.params.setdefault('RADIUS_REJECT','0')
      self.params['BAD_PIXEL_HIGH'] = float(self.params['BAD_PIXEL_HIGH'])
      self.params['BAD_PIXEL_LOW'] = float(self.params['BAD_PIXEL_LOW'])
      self.params['BAD_PIXEL_SIGMA'] = float(self.params['BAD_PIXEL_SIGMA'])
      self.params['EDGE_REJECT'] = int(self.params['EDGE_REJECT'])
      self.params['RADIUS_REJECT'] = float(self.params['RADIUS_REJECT'])
      self.params.setdefault('DO_FLAT_DIVIDE','yes')
      self.params.setdefault('DEFAULT_MASTER_FLAT','')
      self.params.setdefault('REMOVE_COSMIC_RAYS','no')
      self.params.setdefault('COSMIC_RAY_SIGMA','10')
      self.params['COSMIC_RAY_SIGMA'] = float(self.params['COSMIC_RAY_SIGMA'])
      self.params.setdefault('COSMIC_RAY_PASSES','1')
      self.params['COSMIC_RAY_PASSES'] = int(self.params['COSMIC_RAY_PASSES'])
      self.params.setdefault('DO_SKY_SUBTRACT','yes')
      self.params.setdefault('SKY_SUBTRACT_METHOD','dither')
      self.params.setdefault('SKY_REJECT_TYPE','sigclip')
      self.params.setdefault('SKY_NLOW','1')
      self.params.setdefault('SKY_NHIGH','1')
      self.params.setdefault('SKY_LSIGMA','5')
      self.params.setdefault('SKY_HSIGMA','5')
      self.params.setdefault('USE_SKY_FILES','all')
      self.params.setdefault('SKY_FILES_RANGE','3')
      self.params.setdefault('SKY_DITHERING_RANGE','2')
      self.params['SKY_FILES_RANGE'] = int(self.params['SKY_FILES_RANGE'])
      self.params['SKY_DITHERING_RANGE'] = float(self.params['SKY_DITHERING_RANGE'])
      self.params.setdefault('IGNORE_ODD_FRAMES','yes')
      self.params.setdefault('SKY_OFFSOURCE_RANGE','240')
      self.params['SKY_OFFSOURCE_RANGE'] = int(self.params['SKY_OFFSOURCE_RANGE'])
      self.params.setdefault('SKY_OFFSOURCE_METHOD','auto')
      self.params.setdefault('SKY_OFFSOURCE_MANUAL','')
      self.params.setdefault('DO_RECTIFY','yes')
      self.params.setdefault('DATA_TYPE','longslit')
      self.params.setdefault('LONGSLIT_RECT_COEFFS','rect.dat')
      self.params.setdefault('MOS_RECT_DB','')
      self.params.setdefault('MOS_RECT_ORDER','3')
      self.params['MOS_RECT_ORDER'] = int(self.params['MOS_RECT_ORDER'])
      self.params.setdefault('MOS_REGION_FILE','')
      self.params.setdefault('MOS_YSLIT_BUFFER_LOW','')
      self.params.setdefault('MOS_YSLIT_BUFFER_HIGH','')
      if (self.params['MOS_YSLIT_BUFFER_LOW'] == ''):
        self.params['MOS_YSLIT_BUFFER_LOW'] = '0'
      if (self.params['MOS_YSLIT_BUFFER_HIGH'] == ''):
        self.params['MOS_YSLIT_BUFFER_HIGH'] = '0'
      self.params.setdefault('MOS_SKYLINE_SIGMA','3')
      if (self.params['MOS_SKYLINE_SIGMA'] == ''):
        self.params['MOS_SKYLINE_SIGMA'] = '0'
      self.params.setdefault('MOS_MAX_SLIT_WIDTH','10')
      self.params['MOS_MAX_SLIT_WIDTH'] = float(self.params['MOS_MAX_SLIT_WIDTH'])
      self.params['MOS_YSLIT_BUFFER_LOW'] = int(self.params['MOS_YSLIT_BUFFER_LOW'])
      self.params['MOS_YSLIT_BUFFER_HIGH'] = int(self.params['MOS_YSLIT_BUFFER_HIGH'])
      self.params['MOS_SKYLINE_SIGMA']=float(self.params['MOS_SKYLINE_SIGMA'])
      self.params.setdefault('MOS_RECT_USE_ARCLAMPS', 'no')
      self.params.setdefault('MOS_SKY_TWO_PASS','yes')
      self.params.setdefault('MOS_SKY_BOX_SIZE','6')
      self.params['MOS_SKY_BOX_SIZE'] = int(self.params['MOS_SKY_BOX_SIZE'])
      self.params.setdefault('MOS_SKY_FIT_ORDER','2')
      self.params['MOS_SKY_FIT_ORDER'] = int(self.params['MOS_SKY_FIT_ORDER'])
      self.params.setdefault('DO_DOUBLE_SUBTRACT','yes')
      self.params.setdefault('SUM_BOX_WIDTH','25')
      self.params.setdefault('SUM_BOX_CENTER','1024')
      self.params['SUM_BOX_WIDTH'] = int(self.params['SUM_BOX_WIDTH'])
      self.params['SUM_BOX_CENTER'] = int(self.params['SUM_BOX_CENTER'])
      self.params.setdefault('SPECTRAL_ORIENTATION','horizontal')
      self.params.setdefault('MAX_DITHER','2000')
      self.params['MAX_DITHER'] = int(self.params['MAX_DITHER'])
      self.params.setdefault('DO_SHIFT_ADD','yes')
      self.params.setdefault('MASK_OUT_TROUGHS','yes')
      self.params.setdefault('DO_EXTRACT_SPECTRA','yes')
      self.params.setdefault('EXTRACT_METHOD','auto')
      self.params.setdefault('EXTRACT_FILENAME','extract.dat')
      self.params.setdefault('EXTRACT_BOX_GUESS_FILE','')
      self.params.setdefault('EXTRACT_N_SPECTRA','')
      if (self.params['EXTRACT_N_SPECTRA'] == ''):
        self.params['EXTRACT_N_SPECTRA'] = '0'
      self.params.setdefault('EXTRACT_BOX_WIDTH','0')
      self.params.setdefault('EXTRACT_BOX_CENTER','0')
      self.params.setdefault('EXTRACT_SIGMA','2')
      self.params.setdefault('EXTRACT_MIN_WIDTH','5')
      self.params.setdefault('EXTRACT_WEIGHTING','gaussian')
      self.params['EXTRACT_BOX_WIDTH']=self.params['EXTRACT_BOX_WIDTH'].split()
      for j in range(len(self.params['EXTRACT_BOX_WIDTH'])):
        self.params['EXTRACT_BOX_WIDTH'][j] = int(self.params['EXTRACT_BOX_WIDTH'][j])
      self.params['EXTRACT_BOX_CENTER']=self.params['EXTRACT_BOX_CENTER'].split()
      for j in range(len(self.params['EXTRACT_BOX_CENTER'])):
        self.params['EXTRACT_BOX_CENTER'][j] = int(self.params['EXTRACT_BOX_CENTER'][j])
      self.params['EXTRACT_SIGMA']=self.params['EXTRACT_SIGMA'].split()
      for j in range(len(self.params['EXTRACT_SIGMA'])):
        self.params['EXTRACT_SIGMA'][j] = float(self.params['EXTRACT_SIGMA'][j])
      self.params['EXTRACT_MIN_WIDTH']=self.params['EXTRACT_MIN_WIDTH'].split()
      for j in range(len(self.params['EXTRACT_MIN_WIDTH'])):
        self.params['EXTRACT_MIN_WIDTH'][j] = int(self.params['EXTRACT_MIN_WIDTH'][j])
      self.params['EXTRACT_N_SPECTRA']=self.params['EXTRACT_N_SPECTRA'].split()
      for j in range(len(self.params['EXTRACT_N_SPECTRA'])):
        self.params['EXTRACT_N_SPECTRA'][j] = int(self.params['EXTRACT_N_SPECTRA'][j])
      self.params.setdefault('DO_MOS_ALIGN_SKYLINES','yes')
      self.params.setdefault('MOS_ALIGN_USE_ARCLAMPS','no')
      self.params.setdefault('REFERENCE_SLIT','')
      if (self.params['REFERENCE_SLIT'] == ''):
	self.params['REFERENCE_SLIT'] = '-1'
      if (self.params['REFERENCE_SLIT'].lower() == 'prompt'):
	self.params['REFERENCE_SLIT'] = '-2'
      self.params['REFERENCE_SLIT'] = int(self.params['REFERENCE_SLIT'])
      self.params.setdefault('NEBULAR_EMISSION_CHECK','no')
      self.params.setdefault('REVERSE_ORDER_OF_SEGMENTS','no')
      self.params.setdefault('MEDIAN_FILTER_1D','yes')
      self.params.setdefault('MOS_ALIGN_NLINES','25')
      self.params['MOS_ALIGN_NLINES'] = self.params['MOS_ALIGN_NLINES'].split()
      for j in range(len(self.params['MOS_ALIGN_NLINES'])):
        self.params['MOS_ALIGN_NLINES'][j] = float(self.params['MOS_ALIGN_NLINES'][j])
      self.params.setdefault('MOS_ALIGN_SIGMA','2')
      self.params['MOS_ALIGN_SIGMA'] = self.params['MOS_ALIGN_SIGMA'].split()
      for j in range(len(self.params['MOS_ALIGN_SIGMA'])):
        self.params['MOS_ALIGN_SIGMA'][j] = float(self.params['MOS_ALIGN_SIGMA'][j])
      self.params.setdefault('MOS_ALIGN_FIT_ORDER','3')
      self.params['MOS_ALIGN_FIT_ORDER'] = int(self.params['MOS_ALIGN_FIT_ORDER'])
      self.params.setdefault('DO_WAVELENGTH_CALIBRATE','yes')
      self.params.setdefault('WAVELENGTH_CALIBRATE_USE_ARCLAMPS','no')
      self.params.setdefault('SKY_LINES_FILE','')
      self.params.setdefault('CALIBRATION_INFO_FILE','')
      self.params.setdefault('ANGSTROMS_PER_UNIT','1')
      self.params['ANGSTROMS_PER_UNIT'] = float(self.params['ANGSTROMS_PER_UNIT'])
      self.params.setdefault('CALIBRATION_MIN_SIGMA','3')
      self.params.setdefault('CALIBRATION_FIT_ORDER','3')
      self.params['CALIBRATION_MIN_SIGMA'] = float(self.params['CALIBRATION_MIN_SIGMA'])
      self.params['CALIBRATION_FIT_ORDER'] = int(self.params['CALIBRATION_FIT_ORDER'])
      self.params.setdefault('DO_CALIBRATION_STAR_DIVIDE','yes')
      self.params.setdefault('CALIBRATION_STAR_FILE','')

   def readAll(self):
      #Read Files, setup arrays
      self.darkfiles = []
      self.darktimes = []
      self.flatfiles = []
      self.flattimes = []
      self.flatobjnames = []
      self.flatnames = []
      self.exptimes = []
      self.objnames = []
      self.symLinks = []
      self.expFrames = []
      self.cleanFrames = []
      self.lampfiles = []
      self.lampobjnames = []
      self.lampnames = []
      delFiles = []
      lastPrefix = ""
      currExp = 0
      manDarks = False
      manFlats = False
      manLamps = False
      qsfilenames = []
      qsmedians = []
      if (os.access(self.params['QUICK_START_FILE'], os.F_OK)):
        f = open(self.params['QUICK_START_FILE'], 'rb')
        s = f.read().split('\n')
        f.close()
        try: s.remove('')
        except ValueError:
           junk = ''
        for j in range(len(s)):
           qsfilenames.append(s[j].split()[0])
           qsmedians.append(float(s[j].split()[1]))
        qsfilenames = numarray.strings.array(qsfilenames)
      tmpdarks = []
      tmpflats = []
      tmplamps = []
      if (os.access(self.params['DARK_FILE_LIST'], os.F_OK) and self.params['DARK_FILE_LIST'].lower().count('.fit') > 0):
        tmpdarks = [self.params['DARK_FILE_LIST']]
        manDarks = True
        if (tmpdarks[0].find('/') != -1):
           tmpdarks[0] = tmpdarks[0].replace('/','_')
      elif (os.access(self.params['DARK_FILE_LIST'], os.F_OK)):
        f = open(self.params['DARK_FILE_LIST'])
        tmpdarks = f.read().split('\n')
        f.close()
        manDarks = True
        try: tmpdarks.remove('')
        except ValueError:
           junk = ''
	for j in range(len(tmpdarks)):
	   if (tmpdarks[j].find('/') != -1):
              tmpdarks[j] = tmpdarks[j].replace('/','_')
      elif (self.params['DARK_FILE_LIST'].count(',') != 0):
        tmpdarks = self.params['DARK_FILE_LIST'].split(',')
	manDarks = True
        try: tmpdarks.remove('')
        except ValueError:
           dummy = ''
        for j in range(len(tmpdarks)):
           tmpdarks[j] = tmpdarks[j].strip()
           if (tmpdarks[j].find('/') != -1):
              tmpdarks[j] = tmpdarks[j].replace('/','_')
      if (os.access(self.params['FLAT_FILE_LIST'], os.F_OK) and self.params['FLAT_FILE_LIST'].lower().count('.fit') > 0):
        tmpflats = [self.params['FLAT_FILE_LIST']]
        manFlats = True
        if (tmpflats[0].find('/') != -1):
           tmpflats[0] = tmpflats[0].replace('/','_')
      elif (os.access(self.params['FLAT_FILE_LIST'], os.F_OK)):
        f = open(self.params['FLAT_FILE_LIST'])
        tmpflats = f.read().split('\n')
        f.close()
        manFlats = True
        try: tmpflats.remove('')
        except ValueError:
           junk = ''
        for j in range(len(tmpflats)):
           if (tmpflats[j].find('/') != -1):
              tmpflats[j] = tmpflats[j].replace('/','_')
      elif (self.params['FLAT_FILE_LIST'].count(',') != 0):
	tmpflats = self.params['FLAT_FILE_LIST'].split(',')
	manFlats = True
        try: tmpflats.remove('')
        except ValueError:
           dummy = ''
        for j in range(len(tmpflats)):
           tmpflats[j] = tmpflats[j].strip()
           if (tmpflats[j].find('/') != -1):
              tmpflats[j] = tmpflats[j].replace('/','_')
      if (os.access(self.params['ARCLAMP_FILE_LIST'], os.F_OK) and self.params['ARCLAMP_FILE_LIST'].lower().count('.fit') > 0):
	tmplamps = [self.params['ARCLAMP_FILE_LIST']]
	manLamps = True
	if (tmplamps[0].find('/') != -1):
	   tmplamps[0] = tmplamps[0].replace('/','_')
      elif (os.access(self.params['ARCLAMP_FILE_LIST'], os.F_OK)):
        f = open(self.params['ARCLAMP_FILE_LIST'])
        tmplamps = f.read().split('\n')
        f.close()
        manLamps = True
        try: tmplamps.remove('')
        except ValueError:
           junk = ''
        for j in range(len(tmplamps)):
           if (tmplamps[j].find('/') != -1):
              tmplamps[j] = tmplamps[j].replace('/','_')
      elif (self.params['ARCLAMP_FILE_LIST'].count(',') != 0):
        tmplamps = self.params['ARCLAMP_FILE_LIST'].split(',')
	manLamps = True
        try: tmplamps.remove('')
        except ValueError:
           dummy = ''
        for j in range(len(tmplamps)):
           tmplamps[j] = tmplamps[j].strip()
           if (tmplamps[j].find('/') != -1):
              tmplamps[j] = tmplamps[j].replace('/','_')

      #Grouping
      groupType = 'filename'
      if (self.params['GROUP_OUTPUT_BY'].lower() == 'from manual file' and os.access(self.params['SPECIFY_GROUPING'], os.F_OK)):
        groupType = 'manual'
        f = open(self.params['SPECIFY_GROUPING'], 'rb')
        groupManual = f.read().split('\n')
        f.close()
        for l in range(groupManual.count('')):
           try: groupManual.remove('')
           except ValueError:
              dummy = ''
      elif (self.params['GROUP_OUTPUT_BY'].lower() == 'fits keyword'):
        try:
           temp = pyfits.open(self.filenames[0])
           if (temp[0].header.has_key(self.params['SPECIFY_GROUPING'])):
              groupType = 'keyword'
           temp.close()
        except Exception:
           groupType = 'filename'

      for j in range(len(self.filenames)):
        if (self.filenames[j].endswith('.gz')):
           if (os.access(self.filenames[j], os.W_OK)):
              os.system('gunzip '+self.filenames[j])
              self.filenames[j] = self.filenames[j][:-3]
           else:
              if (not os.access("unzipped", os.F_OK)):
                os.mkdir("unzipped",0755)
              newfilename = 'unzipped/'+self.filenames[j][self.filenames[j].rfind('/')+1:-3]
              os.system('gunzip -c '+self.filenames[j]+' > '+newfilename)
              self.filenames[j] = newfilename
        if (self.filenames[j].find('/') != -1 or groupType != 'filename'):
           newname = self.filenames[j].replace('/','_')
           if (tmpdarks.count(newname) > 0):
              itmpdark = tmpdarks.index(newname)
           else:
              itmpdark = -1
	   if (tmpflats.count(newname) > 0):
	      itmpflat = tmpflats.index(newname)
	   else:
	      itmpflat = -1
           if (tmplamps.count(newname) > 0):
              itmplamp = tmplamps.index(newname)
           else:
              itmplamp = -1
           if (groupType == 'manual'):
              for l in range(len(groupManual)):
                mangrp = groupManual[l].split()
                sindex = mangrp[1]
                mangrp[1] = int(mangrp[1])
                mangrp[2] = int(mangrp[2])
                if (len(mangrp) > 4):
                   mangrp[4] = int(mangrp[4])-mangrp[1]
                fileprefix = self.filenames[j][:self.filenames[j].rfind(self.delim,0,self.filenames[j].rfind('.'))]
                fileindex = int(self.filenames[j][self.filenames[j].rfind(self.delim,0,self.filenames[j].rfind('.'))+len(self.delim):self.filenames[j].rfind('.')])
                if (fileprefix.find(mangrp[0]) != -1 and fileindex >= mangrp[1] and fileindex <= mangrp[2]):
                   if (len(mangrp) > 4):
                      newfi = str(mangrp[4]+fileindex)
                      zeros = '0000'
                      newfi = zeros[len(newfi):]+newfi
                      newname = mangrp[3]+'.'+newfi+'.fits'
                   elif (len(mangrp) > 3):
                      newname = mangrp[3]+self.filenames[j][self.filenames[j].find(fileprefix)+len(fileprefix):]
                   else:
                      newname = fileprefix.replace('/','_')+'.'+sindex+self.filenames[j][self.filenames[j].find(fileprefix)+len(fileprefix):]
           elif (groupType == 'keyword'):
              temp = pyfits.open(self.filenames[j])
              if (temp[0].header.has_key(self.params['SPECIFY_GROUPING'])):
                fileprefix = self.filenames[j][:self.filenames[j].rfind(self.delim,0,self.filenames[j].rfind('.'))]
                newname = fileprefix.replace('/','_')+'_'+str(temp[0].header[self.params['SPECIFY_GROUPING']]).replace(' ','_')+self.filenames[j][self.filenames[j].find(fileprefix)+len(fileprefix):]
              temp.close()
           if (len(newname) > 40):
	      newname = newname[-40:]
              newname = newname[newname.index('_')+1:]
              while (newname[0] == '.' or newname[0] == '_'):
                newname = newname[1:]
           if (not os.access(newname, os.F_OK)):
              os.symlink(self.filenames[j], newname)
           self.filenames[j] = newname
           self.symLinks.append(newname)
	   if (itmpdark != -1):
	      tmpdarks[itmpdark] = newname
           if (itmpflat != -1):
              tmpflats[itmpflat] = newname
           if (itmplamp != -1):
              tmplamps[itmplamp] = newname
      keepNext = True
      makeqs = False
      #if (self.params['QUICK_START_FILE'] != '' and not os.access(self.params['QUICK_START_FILE'], os.F_OK)):
      if (self.params['QUICK_START_FILE'] != ''):
	if (not os.access(self.params['QUICK_START_FILE'], os.F_OK)):
	   mode = 'wb'
	else:
	   mode = 'ab'
        if (self.params['MIN_FRAME_VALUE'] != 0 or self.params['MAX_FRAME_VALUE'] != 0):
           qsfile = open(self.params['QUICK_START_FILE'], mode)
           makeqs = True
      startPrefix = ''
      if (not os.access(self.filenames[0], os.F_OK)):
	if (self.params['ROTATION_ANGLE'] > 0 and os.access('rotated/rot_'+self.filenames[0], os.F_OK)):
	   startPrefix = 'rotated/rot_'
        if (self.params['DO_DARK_COMBINE'].lower() == "yes" and os.access('linearized/lin_'+self.filenames[0], os.F_OK)):
           startPrefix = 'linearized/lin_'
        elif (self.params['DO_DARK_SUBTRACT'].lower() == "yes" and os.access('linearized/lin_'+self.filenames[0], os.F_OK)):
           startPrefix = 'linearized/lin_'
        elif (self.params['DO_FLAT_COMBINE'].lower() == "yes" and os.access('darkSubtracted/ds_'+self.filenames[0], os.F_OK)):
           startPrefix = 'darkSubtracted/ds_'
	elif (self.params['REMOVE_COSMIC_RAYS'].lower() == "yes" and os.access('darkSubtracted/ds_'+self.filenames[0], os.F_OK)):
	   startPrefix = 'darkSubtracted/ds_'
        elif (self.params['DO_BAD_PIXEL_MASK'].lower() == "yes" and os.access('removedCosmicRays/rcr_'+self.filenames[0], os.F_OK)):
           startPrefix = 'removedCosmicRays/rcr_'
        elif (self.params['DO_SKY_SUBTRACT'].lower() == "yes" and os.access('removedCosmicRays/rcr_'+self.filenames[0], os.F_OK)):
           startPrefix = 'removedCosmicRays/rcr_'
        elif (self.params['DO_SKY_SUBTRACT'].lower() == "yes" and os.access('darkSubtracted/ds_'+self.filenames[0], os.F_OK)):
           startPrefix = 'darkSubtracted/ds_'
        elif (self.params['DO_RECTIFY'].lower() == "yes" and os.access('skySubtracted/ss_'+self.filenames[0], os.F_OK)):
           startPrefix = 'skySubtracted/ss_'
	elif (self.params['DO_DOUBLE_SUBTRACT'].lower() == "yes" and os.access('rectified/rct_'+self.filenames[0], os.F_OK)):
	   startPrefix = 'rectified/rct_'
	elif (self.params['DO_SHIFT_ADD'].lower() == "yes" and os.access('doubleSubtracted/ds_'+self.filenames[0], os.F_OK)):
	   startPrefix = 'doubleSubtracted/ds_'
	elif (self.params['DO_MOS_ALIGN_SKYLINES'] == "yes" and os.access('shiftedAdded/sa_'+self.filenames[0], os.F_OK)):
	   startPrefix = 'shiftedAdded/sa_'
	elif (self.params['DO_WAVELENGTH_CALIBRATE'] == "yes" and os.access('mosAlignedSkylines/mas_'+self.filenames[0], os.F_OK)):
	   startPrefix = 'mosAlignedSkylines/mas_'
	elif (self.params['DO_WAVELENGTH_CALIBRATE'] == "yes" and os.access('shiftedAdded/sa_'+self.filenames[0], os.F_OK)):
	   startPrefix = 'shiftedAdded/sa_'
	elif (self.params['DO_EXTRACT_SPECTRA'] == "yes" and os.access('wavelengthCalibrated/wc_'+self.filenames[0], os.F_OK)):
	   startPrefix = 'wavelengthCalibrated/wc_'
	elif (self.params['DO_CALIBRATION_STAR_DIVIDE'] == "yes" and os.access('extractedSpectra/es_'+self.filenames[0], os.F_OK)):
	   startPrefix = 'extractedSpectra/es_'
      guiCount = 0.0
      defExp = 0
      self.mef = self.params['MEF_EXTENSION']
      for j in self.filenames:
	if (keepNext == True):
	   keep = True
	else:
	   keep = False
	   keepNext = True
	if (os.access(startPrefix+j, os.F_OK) and not os.access(startPrefix+j, os.W_OK)):
	   if (not os.access("copies", os.F_OK)): os.mkdir("copies",0755)
	   temp = pyfits.open(startPrefix+j)
	   temp.verify('silentfix')
	   temp.writeto('copies/'+j)
	   temp.close()
	   os.unlink(startPrefix+j)
           os.symlink('copies/'+j, startPrefix+j)
	   print 'File '+startPrefix+j+' is not writeable.  Making local copy.'
           f = open(self.logfile,'ab')
           f.write('File '+startPrefix+j+' is not writeable.  Making local copy.\n')
           f.close()
           f = open(self.warnfile,'ab')
           f.write('File '+startPrefix+j+' is not writeable.  Making local copy.\n')
           f.write('Time: '+str(datetime.today())+'\n\n')
           f.close()
        try:
           temp = pyfits.open(startPrefix+j,"update")
        except Exception:
           print 'Error: file '+startPrefix+j+' not found!  Ignoring file.'
           f = open(self.logfile,'ab')
           f.write('Error: file '+startPrefix+j+' not found!  Ignoring file.\n')
           f.close()
           f = open(self.warnfile,'ab')
           f.write('ERROR: file '+startPrefix+j+' not found!  Ignoring file.\n')
           f.write('Time: '+str(datetime.today())+'\n\n')
           f.close()
           keep = False
           delFiles.append(j)
           continue
        #Check for MEF extensions if set to auto
        if (self.mef < 0):
           for l in range(len(temp)):
              if (temp[l].data != None):
                self.mef = l
                break
	#If file is not FITS
	#if (len(a) == 0):
	   #print 'Error: file '+startPrefix+j+' is not a FITS file!  Ignorning file.'
           #f = open(self.logfile,'ab')
           #f.write('Error: file '+startPrefix+j+' is not a FITS file!  Ignoring file.\n')
           #f.close()
           #f = open(self.warnfile,'ab')
           #f.write('ERROR: file '+startPrefix+j+' is not a FITS file!  Ignoring file.\n')
           #f.write('Time: '+str(datetime.today())+'\n\n')
           #f.close()
           #keep = False
           #delFiles.append(j)
           #continue
        #Check flags for min, max frame values, ignore first frames
        if (self.params['MIN_FRAME_VALUE'] != 0 or self.params['MAX_FRAME_VALUE'] != 0):
           if (len(qsfilenames) != 0):
              b = where(qsfilenames == j)
              if (len(b[0]) != 0):
                med = qsmedians[b[0][0]]
              else:
                med = arraymedian(temp[self.mef].data)
                if (makeqs):
                   qsfile.write(j+'\t'+str(med)+'\n')
           else:
              med = arraymedian(temp[self.mef].data)
              if (makeqs):
                qsfile.write(j+'\t'+str(med)+'\n')
	   if (med < self.params['MIN_FRAME_VALUE'] and temp[0].header[self.params['OBSTYPE_KEYWORD']].lower() != 'dark'):
	      keep = False
	   if (self.params['MAX_FRAME_VALUE'] != 0 and med > self.params['MAX_FRAME_VALUE']):
	      keep = False
	      if (self.params['IGNORE_AFTER_BAD_READ'].lower() == 'yes'):
		keepNext = False
	prefix = j[:j.rfind(self.delim,0,j.rfind('.'))]
        if (not temp[0].header.has_key(self.params['EXPTIME_KEYWORD'])):
           if (curses.ascii.isdigit(self.params['EXPTIME_KEYWORD'][0])):
              currExp = int(self.params['EXPTIME_KEYWORD'])
              defExp = int(self.params['EXPTIME_KEYWORD'])
              self.params['EXPTIME_KEYWORD'] = 'EXP_TIME'
           else:
              currExp = defExp
        else:
           currExp = temp[0].header[self.params['EXPTIME_KEYWORD']]
	if (self.params['IGNORE_FIRST_FRAMES'].lower() == 'yes' and (prefix != lastPrefix or currExp != lastExp)):
	   keep = False
	lastPrefix = prefix
	lastExp = currExp
	if (keep):
           if (manDarks):
              if (tmpdarks.count(j) > 0):
                if (temp[0].header[self.params['OBSTYPE_KEYWORD']].lower() != 'dark'):
                   temp[0].header.update('OBS_ORIG',temp[0].header[self.params['OBSTYPE_KEYWORD']])
                temp[0].header.update(self.params['OBSTYPE_KEYWORD'],'dark')
                tmpdarks.remove(j)
           if (manFlats):
              if (tmpflats.count(j) > 0):
                if (temp[0].header[self.params['OBSTYPE_KEYWORD']].lower() != 'flat'):
                   temp[0].header.update('OBS_ORIG',temp[0].header[self.params['OBSTYPE_KEYWORD']])
                temp[0].header.update(self.params['OBSTYPE_KEYWORD'],'flat')
                tmpflats.remove(j)
           if (manLamps):
              if (tmplamps.count(j) > 0):
                if (temp[0].header[self.params['OBSTYPE_KEYWORD']].lower() != 'lamp'):
                   temp[0].header.update('OBS_ORIG',temp[0].header[self.params['OBSTYPE_KEYWORD']])
                temp[0].header.update(self.params['OBSTYPE_KEYWORD'],'lamp')
                tmplamps.remove(j)
	   if (curses.ascii.isdigit(self.params['GAIN_KEYWORD'][0]) and not temp[0].header.has_key('GAIN')):
	      temp[0].header.update('GAIN', self.params['GAIN_KEYWORD'])
           if (curses.ascii.isdigit(self.params['READNOISE_KEYWORD'][0]) and not temp[0].header.has_key('RDNOIS')):
	      temp[0].header.update('RDNOIS',self.params['READNOISE_KEYWORD'])
           if (not temp[0].header.has_key(self.params['EXPTIME_KEYWORD'])):
              temp[0].header.update(self.params['EXPTIME_KEYWORD'], defExp)
              print "Using exposure time "+str(defExp)+" for file "+j
              f = open(self.logfile,'ab')
              f.write("Warning: Using exposure time "+str(defExp)+" for file "+j+'\n')
              f.close()
              f = open(self.warnfile,'ab')
              f.write("WARNING: Using exposure time "+str(defExp)+" for file "+j+'\n')
              f.write('Time: '+str(datetime.today())+'\n\n')
              f.close()
	   self.exptimes.append(temp[0].header[self.params['EXPTIME_KEYWORD']])
           self.objnames.append(temp[0].header[self.params['OBJECT_KEYWORD']].replace(' ',''))
	   if (temp[0].header[self.params['OBSTYPE_KEYWORD']].lower() == 'dark'):
	      self.darkfiles.append(j)
              self.darktimes.append(temp[0].header[self.params['EXPTIME_KEYWORD']])
           if (temp[0].header[self.params['OBSTYPE_KEYWORD']].lower().find('flat') != -1):
	      self.flatfiles.append(j)
	      self.flattimes.append(temp[0].header[self.params['EXPTIME_KEYWORD']])
	      self.flatobjnames.append(temp[0].header[self.params['OBJECT_KEYWORD']].replace(' ',''))
           if (temp[0].header[self.params['OBSTYPE_KEYWORD']].lower().count('lamp') > 0 or temp[0].header[self.params['OBSTYPE_KEYWORD']].lower().count('arc') > 0):
              self.lampfiles.append(j)
              self.lampobjnames.append(temp[0].header[self.params['OBJECT_KEYWORD']].replace(' ',''))
	else:
           print 'Warning: Ignoring file '+startPrefix+j
           f = open(self.logfile,'ab')
           f.write('Warning: Ignoring file '+startPrefix+j+'.\n')
           f.close()
           f = open(self.warnfile,'ab')
           f.write('WARNING: Ignoring file '+startPrefix+j+'.\n')
           f.write('Time: '+str(datetime.today())+'\n\n')
           f.close()
	   delFiles.append(j)
	temp.close()
	guiCount+=1.
	if (self.guiMessages): print "PROGRESS: "+str(int(guiCount/len(self.filenames)*4*self.guiCRf))
      if (curses.ascii.isdigit(self.params['GAIN_KEYWORD'][0])):
	self.params['GAIN_KEYWORD'] = 'GAIN'
      if (curses.ascii.isdigit(self.params['READNOISE_KEYWORD'][0])):
	self.params['READNOISE_KEYWORD'] = 'RDNOIS'
      for j in delFiles:
	if self.filenames.count(j) != 0:
	   self.filenames.remove(j)
      #Check for empty file list
      if (len(self.filenames) == 0):
        print "ERROR: File list has become empty!"
        f = open(self.logfile,'ab')
        f.write('ERROR: File list has become empty!\n')
        f.close()
        f = open(self.warnfile,'ab')
        f.write('ERROR: File list has become empty!\n')
        f.write('Time: '+str(datetime.today())+'\n\n')
        f.close()
        if (self.guiMessages): print "GUI_ERROR: File list has become empty!  Aborting!"
        sys.exit()

   def rotateAll(self, angle):
      #Use IRAF's rotate to rotate all files by the specified angle
      print "Performing rotation..."
      if (self.guiMessages): print "STATUS: Rotating Images..."
      f = open(self.logfile,'ab')
      f.write('Rotating all files by '+str(angle)+' degrees.\n')
      f.close()
      if (not os.access("rotated", os.F_OK)): os.mkdir("rotated",0755)
      for j in range(len(self.filenames)):
	rotfile = "rotated/rot_"+self.filenames[j]
	if (os.access(rotfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(rotfile)
	if (not os.access(rotfile, os.F_OK)):
	   #Use IRAF's rotate
	   iraf.unlearn('rotate')
	   iraf.imgeom.rotate(self.prefix+self.filenames[j], rotfile, angle)
	   temp = pyfits.open(rotfile, "update")
	   temp[0].header.update('FILENAME',rotfile)
	   temp[0].header.update('HISTORY','Rotated by '+str(angle)+'degrees.')
	   temp.verify('silentfix')
	   temp.flush()
	   temp.close()
	if (guiMessages):
	   print "PROGRESS: "+str(int(self.guiCRf*(16+(j+1.)/len(self.filenames)*4)))
           print "FILE: "+rotfile
      self.prefix = 'rotated/rot_'

   def convertMEFs(self):
      print "Converting MEFs to single extension FITS files..."
      if (self.guiMessages): print "STATUS: Converting MEFs..."
      f = open(self.logfile,'ab')
      f.write("Converting MEFs to single extension FITS files...\n")
      f.close()
      if (not os.access("singleExtFits", os.F_OK)): os.mkdir("singleExtFits",0755)
      for j in self.filenames:
        seffile = "singleExtFits/sef_"+j
        if (os.access(seffile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(seffile)
        if (not os.access(seffile, os.F_OK)):
           temp = pyfits.open(j)
           head = temp[self.mef].header.ascardlist()
           for l in range(len(head)):
              temp[0].header.update(head.keys()[l], temp[self.mef].header[l])
           temp[0].data = temp[self.mef].data
           while (len(temp) > 1):
              temp.remove(temp[1])
           temp[0].header.update('HISTORY','Converted to single extension FITS')
           temp[0].header.update('FILENAME',seffile)
           temp.verify('silentfix')
           temp.writeto(seffile)
           temp.close()
      self.prefix = "singleExtFits/sef_"

   def linearizeAll(self, coeffs):
      #Perform Linearity correction on all files
      print "Performing Linearity Correction..."
      if (self.guiMessages): print "STATUS: Linearity Correction..."
      f = open(self.logfile,'ab')
      f.write("Performing Linearity Correction...\n")
      f.close()
      n = 0
      if (not os.access("linearized", os.F_OK)): os.mkdir("linearized",0755) 
      guiCount = 0.
      for j in self.filenames:
        linfile = "linearized/lin_"+j
        if (os.access(linfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(linfile) 
        if (not os.access(linfile, os.F_OK)):
	   temp = pyfits.open(self.prefix+j)
           #Convert MEFs if necessary
           if (self.mef != 0):
              head = temp[self.mef].header.ascardlist()
              for l in range(len(head)):
                temp[0].header.update(head.keys()[l], temp[self.mef].header[l])
              temp[0].data = temp[self.mef].data
              temp[0].header.update('HISTORY','Converted to single extension FITS')
           while (len(temp) > 1):
              temp.remove(temp[1])
	   temp[0].data = linearity(temp[0].data, coeffs)
	   temp[0].header.update('HISTORY','linearized')
	   temp[0].header.update('FILENAME', linfile)
	   temp.verify('silentfix')
	   temp.writeto(linfile)
	   temp.close()
	n+=1
        guiCount+=1.
        if (self.guiMessages):
	   print "PROGRESS: "+str(int(self.guiCRf*(4+guiCount/len(self.filenames)*12)))
           print "FILE: "+linfile
      self.prefix = "linearized/lin_"

   def doNoisemaps(self):
      #Generate noisemasks
      print "Generating Noisemaps..."
      if (self.guiMessages): print "STATUS: Generating Noisemaps..."
      f = open(self.logfile,'ab')
      f.write("Generating Noisemaps...\n")
      f.close()
      guiCount = 0.0
      for j in range(len(self.filenames)):
	nmfile = self.prefix+'NM_'+self.filenames[j]
	if (os.access(nmfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(nmfile)
	if (not os.access(nmfile, os.F_OK)):
	   temp = pyfits.open(self.prefix+self.filenames[j])
	   gain = float(temp[0].header[self.params['GAIN_KEYWORD']])
	   temp[0].data = sqrt(abs(temp[0].data/gain))
	   temp[0].header.update('HISTORY','noisemap')
	   temp[0].header.update('FILENAME',nmfile)
	   temp.verify('silentfix')
	   temp.writeto(nmfile)
	   temp.close()
        guiCount+=1.
        if (self.guiMessages):
	   print "PROGRESS: "+str(int(self.guiCRf*(20+guiCount/len(self.filenames)*4)))
           print "FILE: "+nmfile

   def darkCombine(self):
      if (self.guiMessages): print "STATUS: Combining Darks..."
      mdarks = []
      mdarktimes = []
      tmpdarktimes = array(self.darktimes)
      n = len(self.darkfiles)
      if (not os.access("masterDarks", os.F_OK)): os.mkdir("masterDarks",0755)
      while (n > 0):
	exptime = tmpdarktimes[0] 
	tmpdarktimes = compress(where(tmpdarktimes != exptime, 1, 0), tmpdarktimes)
        print "Creating Master Dark for exposure time: ",exptime,"..."
        f = open(self.logfile,'ab')
        f.write('Creating Master Dark for exposure time: '+str(exptime)+'...\n')
        f.close()
        darks = ""
	for j in range(len(self.darkfiles)):
	   if (self.darktimes[j] == exptime):
	      darks+=","+self.prefix+self.darkfiles[j]
	      n-=1
        darks = darks.strip(",")
	mdfilename="masterDarks/mdark-exp"+str(exptime)+".fits"
        if (os.access(mdfilename, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(mdfilename)
	if (not os.access(mdfilename, os.F_OK)):
	   #Use IRAF's imcombine to combine darks
           iraf.unlearn('imcombine')
	   #imcombine can't handle strings longer than 1024 bytes
	   if (len(darks) > 1023):
	      tmpFile = open("tmpIRAFFile.dat", "wb")
	      tmpFile.write(darks.replace(",","\n"))
	      tmpFile.close()
	      iraf.images.immatch.imcombine("@tmpIRAFFile.dat", mdfilename, combine="median")
	      os.unlink("tmpIRAFFile.dat")
	   else:
	      iraf.images.immatch.imcombine(darks, mdfilename, combine="median")
           masterDark = pyfits.open(mdfilename, "update")
	   masterDark[0].header.update(self.params['OBSTYPE_KEYWORD'], "Master Dark")
           masterDark[0].header.update('FILENAME', mdfilename)
	   masterDark.verify('silentfix')
	   masterDark.flush()
	   masterDark.close()
	mdarks.append(mdfilename)
	mdarktimes.append(exptime)
        if (self.guiMessages):
	    print "PROGRESS: "+str(int(self.guiCRf*(28-(len(tmpdarktimes)+0.)/len(self.darktimes)*4)))
	    print "FILE: "+mdfilename
      #Update filename and exptime lists
      for j in self.darkfiles:
	if (self.params['CLEAN_UP'].lower() == 'yes'):
	   if (os.access('linearized/lin_'+j, os.F_OK)):
	      os.unlink('linearized/lin_'+j)
           if (os.access('linearized/lin_NM_'+j, os.F_OK)):
              os.unlink('linearized/lin_NM_'+j)
	i = self.filenames.index(j)
	self.filenames.pop(i)
	self.exptimes.pop(i)
	self.objnames.pop(i)
      self.darkfiles = mdarks
      self.darktimes = mdarktimes

   def darkSubtract(self):
      n = len(self.filenames)
      if (not os.access("darkSubtracted", os.F_OK)): os.mkdir("darkSubtracted",0755)
      tmpexptimes = array(self.exptimes)
      print "Subtracting Master Darks..."
      if (self.guiMessages): print "STATUS: Subtracting Darks..."
      f = open(self.logfile,'ab')
      f.write("Subtracting Master Darks...\n")
      f.close()
      defexptimes = []
      for j in range(len(self.params['DEFAULT_MASTER_DARK'])):
        if (os.access(self.params['DEFAULT_MASTER_DARK'][j], os.F_OK)):
           temp = pyfits.open(self.params['DEFAULT_MASTER_DARK'][j])
           defexptimes.append(float(temp[0].header[self.params['EXPTIME_KEYWORD']]))
           temp.close()
        else:
           defexptimes.append(-1000)
      defexptimes = array(defexptimes)
      guiCount = 0.
      while (n > 0):
        exptime = tmpexptimes[0]
        tmpexptimes = compress(where(tmpexptimes != exptime, 1, 0), tmpexptimes)
        mdfilename="masterDarks/mdark-exp"+str(exptime)+".fits"
	#Search for master dark
        if (not os.access(mdfilename, os.R_OK)):
	   print "Master Dark for exposure "+str(exptime)+" not found!"
           f = open(self.logfile,'ab')
           f.write("Master Dark for exposure "+str(exptime)+" not found!\n")
           f.close()
	   #Try default master dark
           if (len(self.params['DEFAULT_MASTER_DARK']) > 0):
              b = where(defexptimes == exptime)[0]
              if (len(b) != 0):
                if (os.access(self.params['DEFAULT_MASTER_DARK'][b[0]], os.R_OK)):
                   mdfilename = self.params['DEFAULT_MASTER_DARK'][b[0]]
                   print "Using " + mdfilename + " instead."
                   f = open(self.logfile,'ab')
                   f.write("Using " + mdfilename + " instead.\n")
                   f.close()
           elif (self.params['PROMPT_FOR_MISSING_DARK'] == 'no'):
              if (len(self.darktimes) != 0):
                b = where(abs(array(self.darktimes)-exptime) == min(abs(array(self.darktimes)-exptime)))[0][0]
                mdfilename = self.darkfiles[b]
              elif (len(defexptimes) != 0):
                b = where(abs(defexptimes-exptime) == min(abs(defexptimes-exptime)))[0][0]
                if (os.access(self.params['DEFAULT_MASTER_DARK'][b], os.F_OK)):
                   mdfilename = self.params['DEFAULT_MASTER_DARK'][b]
              print "Using " + mdfilename + " instead."
              f = open(self.logfile,'ab')
              f.write("Using " + mdfilename + " instead.\n")
              f.close()
              f = open(self.warnfile,'ab')
              f.write("WARNING: Master dark for exposure "+str(exptime)+" not found!\n")
              f.write("Using " + mdfilename + " instead.\n")
              nwarn = 0
              for i in range(len(self.filenames)):
                if (self.exptimes[i] == exptime):
                   nwarn+=1
              f.write(str(nwarn)+" files affected.\n")
              f.write('Time: '+str(datetime.today())+'\n\n')
              f.close()
	   #Prompt User for dark file
	   else:
	      print "List of darks and exposure times:"
	      for j in range(len(self.darkfiles)):
		print self.darkfiles[j],self.darktimes[j]
              if (guiMessages):
                print "INPUT: File: Select a filename to use as a dark for exposure time "+str(exptime)
	      tmp = raw_input("Select a filename to use as a dark: ")
              if (guiMessages): print tmp
	      if (os.access(tmp, os.R_OK)):
		mdfilename = tmp
	      else:
		print "Dark not subtracted for exposure time "+str(exptime)
                f = open(self.logfile,'ab')
                f.write("Dark not subtracted for exposure time "+str(exptime)+'\n')
                f.close()
                f = open(self.warnfile,'ab')
                f.write("WARNING: Dark not subtracted for exposure time "+str(exptime)+'\n')
                nwarn = 0
		for j in range(len(self.filenames)):
		   if (self.exptimes[j] == exptime): n-=1
		   nwarn+=1
		continue
                f.write(str(nwarn)+" files affected.\n")
                f.write('Time: '+str(datetime.today())+'\n\n')
                f.close()
	masterDark = pyfits.open(mdfilename)
	if (self.params['DO_NOISEMAPS'].lower() == "yes"):
	   if (masterDark[0].header.has_key('NCOMBINE')):
	      ncomb = float(masterDark[0].header['NCOMBINE'])
	   else: ncomb = 1.0
	   nmdark = masterDark[0].data/ncomb
	#Subtract master dark from remaining files
	filelist = []
        for j in range(len(self.filenames)):
           if (self.exptimes[j] == exptime):
	      dsfile = "darkSubtracted/ds_"+self.filenames[j]
	      filelist.append(dsfile)
	      if (os.access(dsfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(dsfile)
              if (not os.access(dsfile, os.F_OK)):
		image = pyfits.open(self.prefix+self.filenames[j])
		image[0].data -= masterDark[0].data
		image[0].header.update('HISTORY','Dark subtracted')
		image[0].header.update('FILENAME', dsfile)
		image.writeto(dsfile)
		image.close()
	      if (self.params['DO_NOISEMAPS'].lower() == "yes"):
		nmfile = "darkSubtracted/ds_NM_"+self.filenames[j]
		if (os.access(nmfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(nmfile)
		if (not os.access(nmfile, os.F_OK)):
		   nm = pyfits.open(self.prefix+'NM_'+self.filenames[j])
		   nm[0].data = nm[0].data.astype("Float64")
		   nm[0].data = sqrt(nm[0].data**2+abs(nmdark))
		   nm[0].header.update('FILENAME', nmfile)
		   nm.writeto(nmfile)
		   nm.close()
              n-=1
              guiCount+=1.
              if (self.guiMessages):
		print "PROGRESS: "+str(int(self.guiCRf*(28+guiCount/len(self.filenames)*4)))
		print "FILE: "+dsfile
		if (self.params['DO_NOISEMAPS'].lower() == "yes"):
		   print "FILE: "+nmfile
	masterDark.close()
      if (self.params['CLEAN_UP'].lower() == 'yes'):
        for j in self.filenames:
	   if (os.access('linearized/lin_'+j, os.F_OK)):
	      os.unlink('linearized/lin_'+j)
           if (os.access('linearized/lin_NM_'+j, os.F_OK)):
              os.unlink('linearized/lin_NM_'+j)

      self.oldpfix = self.prefix
      self.prefix = "darkSubtracted/ds_"

   def lampCombine(self):
      if (self.guiMessages): print "STATUS: Combining Arclamps..."
      mlamps = []
      mlampobjects = []
      self.lampnames = ['']*len(self.filenames)
      n = len(self.lampfiles)
      tmplampobjnames = numarray.strings.array(self.lampobjnames)
      if (not os.access('masterLamps', os.F_OK)): os.mkdir('masterLamps',0755)
      method = self.params['ARCLAMP_SELECTION']
      lamplist = []
      mlfiles = []
      while (n > 0):
	if (method == 'auto'):
	   objname = tmplampobjnames[0]
	   b = where(tmplampobjnames != objname, 1, 0)
	   tmplampobjnames = compress(b, tmplampobjnames)
	   print "Creating Master Lamp for object: "+objname+"..."
	   f = open(self.logfile,'ab')
	   f.write("Creating Master Lamp for object: "+objname+"...\n")
	   f.close()
	   lamps = ""
	   for j in range(len(self.lampfiles)):
	      if (self.lampobjnames[j] == objname):
		lamps+=self.prefix+self.lampfiles[j]+","
		n-=1
	   lamps = lamps.strip(",")
	   mlfilename = "masterLamps/mlamp-"+objname+".fits"
	   for j in range(len(self.filenames)):
	      if (self.objnames[j] == objname):
		self.lampnames[j] = mlfilename
	   mlfiles.append(mlfilename)
	   lamplist.append(lamps)
	elif (os.access(method, os.F_OK)):
	   f = open(method, 'rb')
	   s = f.read().split('\n')
	   f.close()
	   try: s.remove('')
           except ValueError:
	      junk = ''
	   print "Creating Master Lamps from: "+method+"..."
	   f = open(self.logfile,'ab')
	   f.write("Creating Master Lamps from: "+method+"...\n")
	   f.close()
	   for j in range(len(s)):
	      lamps = ""
	      manlmp = s[j].split()
	      for l in range(2):
		manlmp[l+1] = int(manlmp[l+1])
		manlmp[l+2] = int(manlmp[l+2])
	      for l in range(len(self.lampfiles)):
		prefix = self.lampfiles[l][:self.lampfiles[l].rfind(self.delim,0,self.lampfiles[l].rfind('.'))]
		index = int(self.lampfiles[l][self.lampfiles[l].rfind(self.delim,0,self.lampfiles[l].rfind('.'))+len(self.delim):self.lampfiles[l].rfind('.')])
		if (prefix.find(manlmp[3]) != -1 and index >= manlmp[4] and index <= manlmp[5]):
		   lamps+=self.prefix+self.lampfiles[l]+","
	      lamps = lamps.strip(",")
	      mlfilename = "masterLamps/mlamp-"+manlmp[0]+".fits"
	      lamplist.append(lamps)
	      mlfiles.append(mlfilename)
	      for l in range(len(self.filenames)):
		prefix = self.filenames[l][:self.filenames[l].rfind(self.delim,0,self.filenames[l].rfind('.'))]
		index = int(self.filenames[l][self.filenames[l].rfind(self.delim,0,self.filenames[l].rfind('.'))+len(self.delim):self.filenames[l].rfind('.')])
		if (prefix.find(manlmp[0]) != -1 and index >= manlmp[1] and index <= manlmp[2]):
		   self.lampnames[l] = mlfilename
	   n = 0
	else:
	   print "Error: File "+method+" not found!"
	   print 'Master arclamps can not be created!'
	   f = open(self.logfile,'ab')
	   f.write('Error: File '+method+' not found!\n')
           f.write('Master arclamps can not be created!\n')
	   f.close()
	   f = open(self.warnfile,'ab')
           f.write('Error: File '+method+' not found!\n')
	   f.write('Master arclamps can not be created!\n')
	   f.write('Time: '+str(datetime.today())+'\n\n')
	   f.close()
      for j in range(len(mlfiles)):
	if (os.access(mlfiles[j], os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(mlfiles[j])
	if (not os.access(mlfiles[j], os.F_OK)):
	   iraf.unlearn('imcombine')
	   #Combine Lamps with IRAF's imcombine
	   #imcombine can't handle strings longer than 1024 bytes
           if (len(lamplist[j]) > 1023):
	      tmpFile = open("tmpIRAFFile.dat", "wb")
              tmpFile.write(lamplist[j].replace(",","\n"))
              tmpFile.close()
              iraf.images.immatch.imcombine("@tmpIRAFFile.dat", mfliles[j], combine="median")
	      os.unlink("tmpIRAFFile.dat")
           else:
	      iraf.images.immatch.imcombine(lamplist[j], mlfiles[j], combine="median")
           masterLamp = pyfits.open(mlfiles[j], "update")
	   masterLamp[0].header.update(self.params['OBSTYPE_KEYWORD'], "Master Lamp")
           masterLamp[0].header.update('FILENAME', mlfiles[j])
	   masterLamp.verify('silentfix')
	   masterLamp.flush()
	   masterLamp.close()
	mlamps.append(mlfilename)
	if (self.guiMessages):
	   print "FILE: "+mlfilename

      #Replace individual lamps with master lamps in file list
      for j in self.lampfiles:
        if (self.params['CLEAN_UP'].lower() == 'yes'):
           if (os.access('darkSubtracted/ds_'+j, os.F_OK)):
              os.unlink('darkSubtracted/ds_'+j)
           if (os.access('darkSubtracted/ds_NM_'+j, os.F_OK)):
              os.unlink('darkSubtracted/ds_NM_'+j)
        i = self.filenames.index(j)
        self.filenames.pop(i)
        self.exptimes.pop(i)
        self.objnames.pop(i)
	self.lampnames.pop(i)
      self.lampfiles = mlamps
 
   def removeCosmicRays(self):
      self.guiCRdone = 50
      if (not os.access("removedCosmicRays", os.F_OK)):
	os.mkdir("removedCosmicRays",0755)
      print "Removing Cosmic Rays..."
      if (self.guiMessages): print "STATUS: Removing Cosmic Rays..."
      f = open(self.logfile,'ab')
      f.write("Removing Cosmic Rays...\n")
      f.close()
      n = len(self.filenames)
      types = self.findTypes(self.filenames, self.params['DATA_TYPE'])
      #Read in MOS rect database to find indiviudal slits in MOS
      dbfile = self.params['MOS_RECT_DB']
      order = self.params['MOS_RECT_ORDER']
      if (os.access(dbfile, os.F_OK)):
	f = open(dbfile,'rb')
        s = f.read().split('\n')
        f.close()
        try: s.remove('')
        except ValueError:
           junk = ''
        xin = []
        yin = []
        yout = []
        xslitin = []
        for j in range(len(s)):
	   temp = s[j].split()
           xin.append(float(temp[0]))
           yin.append(float(temp[1]))
           yout.append(float(temp[2]))
           xslitin.append(float(temp[3]))
        terms = 0
        nterms = 0
        for j in range(order+2):
	   nterms+=j
           terms+=nterms
        p = zeros(terms)
        p[2] = 1
        xin = array(xin)
        yin = array(yin)
        yout = array(yout)
        xslitin = array(xslitin)
        b = where(xin != 0)
        #Fit inverse function, get coeffs, for determining slit locations
        p = Numeric.array(p.tolist());
        lsq = leastsq(surface3dResiduals, p, args=(xin[b], yout[b], xslitin[b], yin[b], order))
        invcoeffs = array(lsq[0])
	mosDB = True
      else:
	mosDB = False
      #Read region file
      if (os.access(self.params['MOS_REGION_FILE'], os.F_OK)):
	rfile = self.params['MOS_REGION_FILE']
	sylo = []
	syhi = []
        slitx = []
        f = open(rfile,'rb')
        s = f.read().split('\n')
        f.close()
        try: s.remove('')
        except ValueError:
           junk = ''
        for l in range(len(s)):
           if (s[l].startswith("image;box") or s[l].startswith("box(")):
              temp=s[l][s[l].find('(')+1:]
              temp = temp.split(',')
	      if (temp[-1].find(')') != -1):
		temp[-1] = temp[-1][:temp[-1].find(')')]
	      slitx.append(float(temp[0])-1)
              sylo.append(int(float(temp[1])-float(temp[3])/2.-1))
              syhi.append(int(float(temp[1])+float(temp[3])/2.-1))
      #Loop over all files
      for j in range(n):
	print '   File: '+self.prefix+self.filenames[j]
	if (guiMessages): print "PROGRESS: "+str(int(self.guiCRf*36+(j+0.)/n*50))
        f = open(self.logfile,'ab')
        f.write("\tFile: "+self.prefix+self.filenames[j]+'\n')
        f.close()
        crfile = 'removedCosmicRays/rcr_'+self.filenames[j]
        if (os.access(crfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(crfile)
	if (os.access(crfile, os.F_OK)):
	   continue
	if (types[j] == "longslit"):
	   temp = pyfits.open(self.prefix+self.filenames[j])
	   gain = float(temp[0].header[self.params['GAIN_KEYWORD']])
	   rdns = float(temp[0].header[self.params['READNOISE_KEYWORD']])
           temp.close()
	   lacos_spec(self.prefix+self.filenames[j], crfile, 'TMPoutMask.fits', gain=gain, readn=rdns, sigclip=self.params['COSMIC_RAY_SIGMA'], niter=self.params['COSMIC_RAY_PASSES'])
           if (self.params['DO_NOISEMAPS'].lower() == "yes"):
	      nmfile = 'removedCosmicRays/rcr_NM_'+self.filenames[j]
	      if (os.access(nmfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"):
		os.unlink(nmfile)
	      if (not os.access(nmfile, os.F_OK)):
		nm = pyfits.open(self.prefix+'NM_'+self.filenames[j])
	        nm.writeto(nmfile)
		nm.close()
	   os.unlink('TMPoutMask.fits')
	else:
       	   if (not os.access(self.params['MOS_REGION_FILE'], os.F_OK)):
	      print 'No region file found!  Unable to process object '+self.filenames[j]+'!' 
	      f = open(self.logfile,'ab')
	      f.write('No region file found!  Unable to process object '+self.filenames[j]+'!\n')
	      f.close()
	      f = open(self.warnfile,'ab')
	      f.write('WARNING: No region file found!  Unable to process object '+self.filenames[j]+'!\n')
              f.write('Time: '+str(datetime.today())+'\n\n')
              f.close()
	      continue
	   flat = pyfits.open(self.flatnames[j])
           xsize = flat[0].data.shape[1]
           ysize = flat[0].data.shape[0]
	   #Create dummy flat field
           islit = zeros(ysize)+0.
           for l in range(len(sylo)):
              sylo[l] = max(sylo[l],1)
              syhi[l] = min(syhi[l], flat[0].data.shape[0])
              islit[sylo[l]-1:syhi[l]] = 5000
	   if (mosDB):
	      #Cross-correlate region file with flat to determine offset
              cut1d = arraymedian(flat[0].data, axis="X") 
	      #ccor = scipy.signal.correlate(cut1d, islit, mode='same')
              ccor = conv.correlate(cut1d, islit, mode='same')
	      ccor = array(ccor)
              mcor = where(ccor == max(ccor))[0][0]
	      yshift = len(ccor)/2-mcor
	      #Setup array for finding slits
	      yind = arange(xsize*ysize, shape=(ysize,xsize))/xsize+0.
	      xind = arange(xsize)+0.
              #Open image, loop over slits, remove cosmic rays in each
              image = pyfits.open(self.prefix+self.filenames[j])
              image[0].data = image[0].data.astype("Float64")
              gain = float(image[0].header[self.params['GAIN_KEYWORD']])
              rdns = float(image[0].header[self.params['READNOISE_KEYWORD']])
              for l in range(len(sylo)):
		print "   Slit "+str((l+1))
      		f = open(self.logfile,'ab')
      		f.write('\tSlit '+str((l+1))+'\n')
      		f.close()
                ylo = sylo[l]-yshift
                yhi = syhi[l]-yshift
	   	yinlo = surface3d(invcoeffs, xind, ylo, slitx[l], order)
	   	yinhi = surface3d(invcoeffs, xind, yhi, slitx[l], order)
		yinlo += ylo-yinlo[int(slitx[l])]
	        yinhi += yhi-yinhi[int(slitx[l])]
                mask = where(yind >= yinlo-2,1,0)*where(yind <= yinhi+2,1,0)
                b = where(where(yind >= yinlo+2,1,0)*where(yind <= yinhi-2,1,0) == 1)
                ylo = max(int(yinlo.min()-2),0)
                yhi = min(int(yinhi.max()+2),image[0].data.shape[0])
                temp = pyfits.open(self.prefix+self.filenames[j])
                temp[0].data = image[0].data[ylo:yhi,:]*mask[ylo:yhi,:]
                if (os.access('TMPcrin.fits',os.F_OK)): os.unlink('TMPcrin.fits')
                temp.writeto('TMPcrin.fits')
                lacos_spec('TMPcrin.fits', 'TMPcrout.fits', 'TMPoutMask.fits', gain=gain, readn=rdns, sigclip=self.params['COSMIC_RAY_SIGMA'], niter=self.params['COSMIC_RAY_PASSES'])
                temp = pyfits.open('TMPcrout.fits')
                b2 = (b[0]-ylo,b[1])
                image[0].data[b] = temp[0].data[b2]
                temp.close()
                os.unlink('TMPcrin.fits')
                os.unlink('TMPcrout.fits')
              image.writeto(crfile)
              mask = 0
              yind = 0
              xind = 0
              b = 0
              b2 = 0
	   else:
              #Cross-correlate region file with flat to determine slope
              xcoords = [75.0, xsize/2.0, xsize-75.0]
              ycoords = []
              for l in range(len(xcoords)):
                cut1d = sum(flat[0].data[:,int(xcoords[l]-25):int(xcoords[l]+26)],1)
                #ccor = scipy.signal.correlate(cut1d, islit, mode='same')
                ccor = conv.correlate(cut1d, islit, mode='same')
                ccor = array(ccor)
                mcor = where(ccor == max(ccor))[0][0]
                ycoords.append(mcor)
              lsq = leastsq(linResiduals, [0.,0.], args=(array(xcoords), array(ycoords)))
              yshift = len(ccor)/2-ycoords[1]
              slitSlope = lsq[0][1]
              flat.close()
              yind = arange(xsize*ysize, shape=(ysize,xsize))/xsize+0.
              xind = arange(xsize*ysize, shape=(ysize,xsize))%xsize+0.
              #Open image, loop over slits, remove cosmic rays in each
	      image = pyfits.open(self.prefix+self.filenames[j])
	      image[0].data = image[0].data.astype("Float64")
              gain = float(image[0].header[self.params['GAIN_KEYWORD']])
              rdns = float(image[0].header[self.params['READNOISE_KEYWORD']])
	      for l in range(len(sylo)):
	        print "   Slit "+str((l+1))
                f = open(self.logfile,'ab')
                f.write('\tSlit '+str((l+1))+'\n')
                f.close()
                ylo = sylo[l]-slitx[l]*slitSlope-yshift
                yhi = syhi[l]-slitx[l]*slitSlope-yshift
                mask = where(yind >= ylo+slitSlope*xind-2,1,0)*where(yind <= yhi+ slitSlope*xind+2,1,0)
	        b = where(where(yind >= ylo+slitSlope*xind+2,1,0)*where(yind <= yhi+slitSlope*xind-2,1,0) == 1)
	        if (slitSlope < 0):
		   ylo = max(int(ylo+slitSlope*xsize)+1,1)
	           yhi = min(int(yhi)+1,image[0].data.shape[0])
	        else:
		   ylo = max(int(ylo)+1,1)
	    	   yhi = min(int(yhi+slitSlope*xsize)+1,1)
	        temp = pyfits.open(self.prefix+self.filenames[j])
	        temp[0].data = image[0].data[ylo-1:yhi,:]*mask[ylo-1:yhi,:]
	        if (os.access('TMPcrin.fits',os.F_OK)): os.unlink('TMPcrin.fits')
                temp.writeto('TMPcrin.fits')
	        lacos_spec('TMPcrin.fits', 'TMPcrout.fits', 'TMPoutMask.fits', gain=gain, readn=rdns, sigclip=self.params['COSMIC_RAY_SIGMA'], niter=self.params['COSMIC_RAY_PASSES'])
	        temp = pyfits.open('TMPcrout.fits')
	        b2 = (b[0]-(ylo-1),b[1])
	        image[0].data[b] = temp[0].data[b2]
	        temp.close()
	        os.unlink('TMPcrin.fits')
	        os.unlink('TMPcrout.fits')
	      image.writeto(crfile)
	      mask = 0
              yind = 0
	      xind = 0
	      b = 0
              b2 = 0
           if (self.params['DO_NOISEMAPS'].lower() == "yes"):
              nmfile = 'removedCosmicRays/rcr_NM_'+self.filenames[j]
              if (os.access(nmfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"):
                os.unlink(nmfile)
              if (not os.access(nmfile, os.F_OK)):
                nm = pyfits.open(self.prefix+'NM_'+self.filenames[j])
                nm.writeto(nmfile)
                nm.close()
           os.unlink('TMPoutMask.fits')
	if (self.guiMessages):
	   print "FILE: "+crfile
           if (self.params['DO_NOISEMAPS'].lower() == "yes"):
	      print "FILE: "+nmfile
      self.prefix="removedCosmicRays/rcr_"

   def flatCombine(self):
      if (self.guiMessages): print "STATUS: Combining Flats..."
      mflats = []
      self.flatnames = ['']*len(self.filenames)
      n = len(self.flatfiles)
      tmpflatobjnames = numarray.strings.array(self.flatobjnames)
      if (not os.access("masterFlats", os.F_OK)): os.mkdir("masterFlats",0755)
      #Process Dome Flats
      method = self.params['FLAT_METHOD'].lower()
      offselect = self.params['FLAT_LAMP_OFF_FILES']
      while (n > 0):
	if (method == "on-off"):
	   onflatlist = []
	   offflatlist = []
	   mffiles = []
           fltselect = self.params['FLAT_SELECTION']
           if (fltselect == 'auto'):
	      objname = tmpflatobjnames[0].replace(' ','')
              b = where(tmpflatobjnames != objname, 1, 0)
              tmpflatobjnames = compress(b, tmpflatobjnames)
              print "Creating Master Flat for object: "+objname+"..."
              print "Using method "+method
              f = open(self.logfile,'ab')
              f.write("Creating Master Flat for object: "+objname+"...\n")
              f.write("Using method "+method+"\n")
              f.close()
	      onflats = ""
	      offflats = ""
	      for j in range(0, len(self.flatfiles),1):
		if (self.flatobjnames[j] == objname):
		   if (self.flatfiles[j].find(offselect) != -1):
		      offflats+=self.prefix+self.flatfiles[j]+","
		   else: onflats+=self.prefix+self.flatfiles[j]+","
		   n-=1
	      onflats = onflats.strip(",")
	      offflats = offflats.strip(",")
	      mffilename="masterFlats/mdomeflat-on-off-"+objname+".fits"
	      mffilename = mffilename.replace(' ','')
	      onflatlist.append(onflats)
	      offflatlist.append(offflats)
	      mffiles.append(mffilename)
	      for j in range(len(self.filenames)):
		if (self.objnames[j] == objname):
		   self.flatnames[j]=mffilename
           elif (fltselect == 'all'):
              print "Creating Master Flat from all flats..."
              print "Using method "+method
              f = open(self.logfile,'ab')
              f.write("Creating Master Flat from all flats...\n") 
              f.write("Using method "+method+"\n")
              f.close()
              onflats = ""
	      offflats = ""
              for j in range(0, len(self.flatfiles),1):
                if (self.flatfiles[j].find(offselect) != -1):
		   offflats+=self.prefix+self.flatfiles[j]+","
                else: onflats+=self.prefix+self.flatfiles[j]+","
                n-=1
              onflats = onflats.strip(",")
	      offflats = offflats.strip(",")
              mffilename="masterFlats/mdomeflat-on-off-all-"+tmpflatobjnames[0]+".fits"
              onflatlist.append(onflats)
	      offflatlist.append(offflats)
              mffiles.append(mffilename)
              for j in range(len(self.filenames)):
                self.flatnames[j]=mffilename
	   else:
	      f = open(offselect, "rb")
	      s = f.read().split("\n")
	      f.close()
	      try: s.remove('')
              except ValueError:
		junk = ''
              print "Creating Master Flats from: "+offselect+"..."
              print "Using method "+method
              f = open(self.logfile,'ab')
              f.write("Creating Master Flat from: "+offselect+"...\n")
              f.write("Using method "+method+"\n")
              f.close()
	      for j in range(len(s)):
		onflats = ""
		offflats = ""
		manflt = s[j].split()
		for l in range(3): 
		   manflt[l*3+1] = int(manflt[l*3+1])
		   manflt[l*3+2] = int(manflt[l*3+2])
		for l in range(len(self.flatfiles)):
		   prefix = self.flatfiles[l][:self.flatfiles[l].rfind(self.delim,0,self.flatfiles[l].rfind('.'))]
		   index = int(self.flatfiles[l][self.flatfiles[l].rfind(self.delim,0,self.flatfiles[l].rfind('.'))+len(self.delim):self.flatfiles[l].rfind('.')])
		   if (prefix.find(manflt[3]) != -1 and index >= manflt[4] and index <= manflt[5]):
		      onflats+=self.prefix+self.flatfiles[l]+","
		   elif (prefix.find(manflt[6]) != -1 and index >= manflt[7] and index <= manflt[8]):
		      offflats+=self.prefix+self.flatfiles[l]+","
		onflats = onflats.strip(",")
                offflats = offflats.strip(",")
                mffilename="masterFlats/mdomeflat-on-off-"+manflt[0]+".fits"
                onflatlist.append(onflats)
                offflatlist.append(offflats)
                mffiles.append(mffilename)
                for l in range(len(self.filenames)):
                   prefix = self.filenames[l][:self.filenames[l].rfind(self.delim,0,self.filenames[l].rfind('.'))]
                   index = int(self.filenames[l][self.filenames[l].rfind(self.delim,0,self.filenames[l].rfind('.'))+len(self.delim):self.filenames[l].rfind('.')])
		   if (prefix.find(manflt[0]) != -1 and index >= manflt[1] and index <= manflt[2]):
		      self.flatnames[l]=mffilename
	      n = 0
	   for j in range(len(mffiles)):
              mffilename = mffiles[j]
	      if (os.access(mffiles[j], os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(mffiles[j])
              if (not os.access(mffiles[j], os.F_OK)):
		iraf.unlearn('imcombine')
                #Combine Flats with IRAF's imcombine
                #imcombine can't handle strings longer than 1024 bytes
                if (len(onflatlist[j]) > 1023):
                   tmpFile = open("tmpIRAFFile.dat", "wb")
                   tmpFile.write(onflatlist[j].replace(",","\n"))
                   tmpFile.close()
                   iraf.images.immatch.imcombine("@tmpIRAFFile.dat", mffiles[j], combine="median")
                   os.unlink("tmpIRAFFile.dat")
                elif (onflatlist[j] == ""):
                   print "No On flats found for file "+mffiles[j]
                   f = open(self.logfile,'ab')
                   f.write("No On flats found for file "+mffiles[j]+"\n") 
                   f.close()
                   f = open(self.warnfile,'ab')
                   f.write("WARNING: No On flats found for file "+mffiles[j]+"\n")
                   f.write('Time: '+str(datetime.today())+'\n\n')
                   f.close()
                   continue
                else:
                   iraf.images.immatch.imcombine(onflatlist[j], mffiles[j], combine="median")
                if (len(offflatlist[j]) > 1023):
                   tmpFile = open("tmpIRAFFile.dat", "wb")
                   tmpFile.write(offflatlist[j].replace(",","\n"))
                   tmpFile.close()
                   iraf.images.immatch.imcombine("@tmpIRAFFile.dat", mffiles[j]+".off.fits", combine="median")
                   os.unlink("tmpIRAFFile.dat")
                elif (offflatlist[j] == ""):
                   print "No Off flats found for file "+mffiles[j]
                   f = open(self.logfile,'ab')
                   f.write("No Off flats found for file "+mffiles[j]+"\n")
                   f.close()
                   f = open(self.warnfile,'ab')
                   f.write("WARNING: No Off flats found for file "+mffiles[j]+"\n")
                   f.write('Time: '+str(datetime.today())+'\n\n')
                   f.close()
                   os.unlink(mffilename)
                   continue
                else:
                   iraf.images.immatch.imcombine(offflatlist[j], mffiles[j]+".off.fits", combine="median")
                #Subtract lamp on - lamp off
                masterFlat = pyfits.open(mffiles[j], "update")
                masterOff = pyfits.open(mffiles[j]+".off.fits")
		masterFlat[0].data = masterFlat[0].data.astype("Float64")
		masterOff[0].data = masterOff[0].data.astype("Float64")
        	if (self.params['DO_NOISEMAPS'].lower() == "yes"):
           	   if (masterFlat[0].header.has_key('NCOMBINE')):
		      ncomb1 = float(masterFlat[0].header['NCOMBINE'])
           	   else: ncomb1 = 1.0
                   if (masterOff[0].header.has_key('NCOMBINE')):
                      ncomb2 = float(masterOff[0].header['NCOMBINE'])
                   else: ncomb2 = 1.0
		   nmflat = sqrt(abs(masterFlat[0].data/ncomb1+masterOff[0].data/ncomb2))
                masterFlat[0].data -= masterOff[0].data
                masterOff.close()
                os.unlink(mffilename+".off.fits")
                #Normalize Master Flat and update header
                med = arraymedian(masterFlat[0].data)
                masterFlat[0].data = masterFlat[0].data/med
		if (self.params['FLAT_LOW_THRESH'] != 0):
		   b=where(masterFlat[0].data < self.params['FLAT_LOW_THRESH'])
		   masterFlat[0].data[b] = self.params['FLAT_LOW_REPLACE']
		if (self.params['FLAT_HI_THRESH'] != 0):
		   b=where(masterFlat[0].data > self.params['FLAT_HI_THRESH'])
		   masterFlat[0].data[b] = self.params['FLAT_HI_REPLACE']
                masterFlat[0].header.update(self.params['OBSTYPE_KEYWORD'], "Master Flat")
                masterFlat[0].header.update('FILENAME', mffilename)
                masterFlat[0].header.update('HISTORY',"Normalized by: "+str(med))
                masterFlat.verify('silentfix')
                masterFlat.flush()
                masterFlat.close()
		if (self.params['DO_NOISEMAPS'].lower() == "yes"):
		   nmfile = mffiles[j].replace('masterFlats/', 'masterFlats/nm_')
		   if (os.access(nmfile, os.F_OK)): os.unlink(nmfile)
		   nmFlat = pyfits.open(mffilename)
		   nmFlat[0].data = nmflat/med
		   nmFlat[0].header.update('FILENAME',nmfile)
		   nmFlat.writeto(nmfile)
		   nmFlat.close()
	      mflats.append(mffiles[j])
	      if (self.guiMessages):
		print "FILE: "+mffiles[j]
		if (self.params['DO_NOISEMAPS'].lower() == "yes"):
                   nmfile = mffiles[j].replace('masterFlats/', 'masterFlats/nm_')
		   print "FILE: "+nmfile

	elif (method == "on"):
           flatlist = []
           mffiles = []
	   fltselect = self.params['FLAT_SELECTION']
           if (fltselect == 'auto'):
              objname = tmpflatobjnames[0]
              b = where(tmpflatobjnames != objname, 1, 0)
              tmpflatobjnames = compress(b, tmpflatobjnames)
              print "Creating Master Flat for object: "+objname+"..."
              print "Using method "+method
              f = open(self.logfile,'ab')
              f.write("Creating Master Flat for object: "+objname+"...\n")
              f.write("Using method "+method+"\n")
              f.close()
              flats = ""
              for j in range(0, len(self.flatfiles),1):
                if (self.flatobjnames[j] == objname):
                   flats+=self.prefix+self.flatfiles[j]+","
                   n-=1
              flats = flats.strip(",")
              mffilename="masterFlats/mdomeflat-on-"+objname+".fits"
              flatlist.append(flats)
              mffiles.append(mffilename)
              for j in range(len(self.filenames)):
                if (self.objnames[j] == objname):
                   self.flatnames[j]=mffilename
	   elif (fltselect == 'all'):
              print "Creating Master Flat from all flats..." 
              print "Using method "+method
              f = open(self.logfile,'ab')
              f.write("Creating Master Flat from all flats...\n")
              f.write("Using method "+method+"\n")
              f.close()
	      flats = ""
	      for j in range(0, len(self.flatfiles),1):
		flats+=self.prefix+self.flatfiles[j]+","
		n-=1
	      flats = flats.strip(",")
              mffilename="masterFlats/mdomeflat-on-all-"+tmpflatobjnames[0]+".fits"
              flatlist.append(flats)
              mffiles.append(mffilename)
              for j in range(len(self.filenames)):
		self.flatnames[j]=mffilename
           else:
              f = open(offselect, "rb")
              s = f.read().split("\n")
              f.close()
              try: s.remove('')
      	      except ValueError:
        	junk = ''
              print "Creating Master Flats from: "+offselect+"..."
              print "Using method "+method
              f = open(self.logfile,'ab')
              f.write("Creating Master Flat from: "+offselect+"...\n")
              f.write("Using method "+method+"\n")
              f.close()
              for j in range(len(s)):
                flats = ""
                manflt = s[j].split()
                for l in range(2):
                   manflt[l+1] = int(manflt[l+1])
                   manflt[l+2] = int(manflt[l+2])
                for l in range(len(self.flatfiles)):
                   prefix = self.flatfiles[l][:self.flatfiles[l].rfind(self.delim,0,self.flatfiles[l].rfind('.'))]
                   index = int(self.flatfiles[l][self.flatfiles[l].rfind(self.delim,0,self.flatfiles[l].rfind('.'))+len(self.delim):self.flatfiles[l].rfind('.')])
                   if (prefix.find(manflt[3]) != -1 and index >= manflt[4] and index <= manflt[5]):
                      flats+=self.prefix+self.flatfiles[l]+","
                flats = flats.strip(",")
                mffilename="masterFlats/mdomeflat-on-"+manflt[0]+".fits"
                flatlist.append(flats)
                mffiles.append(mffilename)
                for l in range(len(self.filenames)):
                   prefix = self.filenames[l][:self.filenames[l].rfind(self.delim,0,self.filenames[l].rfind('.'))]
                   index = int(self.filenames[l][self.filenames[l].rfind(self.delim,0,self.filenames[l].rfind('.'))+len(self.delim):self.filenames[l].rfind('.')])
                   if (prefix.find(manflt[0]) != -1 and index >= manflt[1] and index <= manflt[2]):
                      self.flatnames[l]=mffilename
              n = 0
           for j in range(len(mffiles)):
	      if (os.access(mffiles[j], os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(mffiles[j])
              if (not os.access(mffiles[j], os.F_OK)):
		iraf.unlearn('imcombine')
                #Combine Flats with IRAF's imcombine
	        #imcombine can't handle strings longer than 1024 bytes
                if (len(flatlist[j]) > 1023):
		   tmpFile = open("tmpIRAFFile.dat", "wb")
                   tmpFile.write(flatlist[j].replace(",","\n"))
                   tmpFile.close()
                   iraf.images.immatch.imcombine("@tmpIRAFFile.dat", mffiles[j], combine="median")
                   os.unlink("tmpIRAFFile.dat")
                else:
                   iraf.images.immatch.imcombine(flatlist[j], mffiles[j], combine="median")
                masterFlat = pyfits.open(mffiles[j], "update")
		masterFlat[0].data = masterFlat[0].data.astype("Float64")
                if (self.params['DO_NOISEMAPS'].lower() == "yes"):
		   if (masterFlat[0].header.has_key('NCOMBINE')):
		      ncomb = float(masterFlat[0].header['NCOMBINE'])
		   else: ncomb = 1.0
		   nmflat = sqrt(abs(masterFlat[0].data/ncomb))
                #Normalize Master Flat and update header
                med = arraymedian(masterFlat[0].data)
                masterFlat[0].data = masterFlat[0].data/med
                masterFlat[0].header.update(self.params['OBSTYPE_KEYWORD'], "Master Flat")
                masterFlat[0].header.update('FILENAME', mffiles[j])
                masterFlat[0].header.update('HISTORY',"Normalized by: "+str(med))
                masterFlat.verify('silentfix')
                masterFlat.flush()
                masterFlat.close()
                if (self.params['DO_NOISEMAPS'].lower() == "yes"):
                   nmfile = mffiles[j].replace('masterFlats/', 'masterFlats/nm_')
		   if (os.access(nmfile, os.F_OK)): os.unlink(nmfile)
                   nmFlat = pyfits.open(mffilename)
                   nmFlat[0].data = nmflat/med
                   nmFlat[0].header.update('FILENAME',nmfile)
                   nmFlat.writeto(nmfile)
                   nmFlat.close()
	      mflats.append(mffiles[j])
              if (self.guiMessages):
                print "FILE: "+mffiles[j]
                if (self.params['DO_NOISEMAPS'].lower() == "yes"):
                   nmfile = mffiles[j].replace('masterFlats/', 'masterFlats/nm_')
                   print "FILE: "+nmfile

      #Replace individual flats with master flats in file list
      for j in self.flatfiles:
        if (self.params['CLEAN_UP'].lower() == 'yes'):
           if (os.access('darkSubtracted/ds_'+j, os.F_OK)):
              os.unlink('darkSubtracted/ds_'+j)
           if (os.access('darkSubtracted/ds_NM_'+j, os.F_OK)):
              os.unlink('darkSubtracted/ds_NM_'+j)
        i = self.filenames.index(j)
        self.filenames.pop(i)
        self.exptimes.pop(i)
        self.objnames.pop(i)
	self.flatnames.pop(i)
	self.lampnames.pop(i)
      self.flatfiles = mflats
      if (self.flatnames.count('') != 0):
	print 'WARNING: There are no flat fields associated with ' + str(self.flatnames.count('')) + ' files.'
	print 'Check your OBJECT keyword or manual flat field list!'
        f = open(self.logfile,'ab')
        f.write('WARNING: There are no flat fields associated with ' + str(self.flatnames.count('')) + ' files.\n')
        f.write('Check your OBJECT keyword or manual flat field list!\n')
        f.close()
        f = open(self.warnfile,'ab')
        f.write('WARNING: There are no flat fields associated with ' + str(self.flatnames.count('')) + ' files.\n')
        f.write('Check your OBJECT keyword or manual flat field list!\n')
        f.write('Time: '+str(datetime.today())+'\n\n')
        f.close()

   def badPixelMask(self):
      #Generate bad pixel masks
      if (self.guiMessages): print "STATUS: Bad Pixel Masks..."
      self.useBPMask = True
      if (not os.access("badPixelMasks", os.F_OK)):
        os.mkdir("badPixelMasks", 0755)
      defBPMask = self.params['DEFAULT_BAD_PIXEL_MASK']
      edge = self.params['EDGE_REJECT']
      radius = self.params['RADIUS_REJECT']
      #Load from file if specified
      if (os.access(defBPMask, os.R_OK)):
        print "Using "+defBPMask+" as bad pixel mask..."
        f = open(self.logfile,'ab')
        f.write("Using "+defBPMask+" as bad pixel mask...\n")
        f.close()
        self.bpMask = badPixelMask(filename=defBPMask)
        self.bpmFile = defBPMask
      #Otherwise create from imaging flats if given 
      elif (os.access(self.params['IMAGING_FRAMES_FOR_BPM'], os.F_OK)):
	print "Using Imaging Frames to generate bad pixel mask..."
        f = open(self.logfile,'ab')
        f.write("Using Imaging Frames to generate bad pixel mask...\n")
        f.close()
	f = open(self.params['IMAGING_FRAMES_FOR_BPM'])
	s = f.read().split('\n')
	f.close()
	try: s.remove('')
        except ValueError:
           junk = ''
	tmpflats = ''
	for j in s:
	   tmpflats += j + ','
	tmpflats.strip(',')
        if (os.access('TEMP_BPM_source.fits', os.F_OK)):
	   os.unlink('TEMP_BPM_source.fits')
        iraf.unlearn('imcombine')
        #Combine Flats with IRAF's imcombine
        #imcombine can't handle strings longer than 1024 bytes
        if (len(tmpflats) > 1023):
           tmpFile = open("tmpIRAFFile.dat", "wb")
           tmpFile.write(tmpflats.replace(",","\n"))
           tmpFile.close()
           iraf.images.immatch.imcombine("@tmpIRAFFile.dat", 'TEMP_BPM_source.fits', combine="median")
           os.unlink("tmpIRAFFile.dat")
        else:
           iraf.images.immatch.imcombine(tmpflats, 'TEMP_BPM_source.fits', combine="median")
        masterBPM = pyfits.open('TEMP_BPM_source.fits')
	bpmsource = self.params['IMAGING_FRAMES_FOR_BPM']
        bpmFile="badPixelMasks/BPM_"+bpmsource[j][bpmsource[j].rfind("/")+1:]+'.fits'
        #Set rejection threshold based on clipping type
        if (self.params['BAD_PIXEL_MASK_CLIPPING'] == "values"):
	   lo = self.params['BAD_PIXEL_LOW']
           hi = self.params['BAD_PIXEL_HIGH']
        elif (self.params['BAD_PIXEL_MASK_CLIPPING'] == "sigma"):
           sig = self.params['BAD_PIXEL_SIGMA']
           sigclip = sigmaFromClipping(masterBPM[0].data, sig, 5)
           med = sigclip[1]
           stddev = sigclip[2]
           lo = med-sig*stddev
           hi = med+sig*stddev
        #Create bad pixel mask
        b = (where(masterBPM[0].data< lo, 1, 0)+where(masterBPM[0].data > hi, 1, 0)+1)/2
        if (edge > 0):
           b[0:edge,:] = 1
           b[:,0:edge] = 1
           b[-1*edge:,:] = 1
           b[:,-1*edge:] = 1
        if (radius > 0):
           xs = b.shape[0]
           ys = b.shape[1]
           for r in range(xs):
	      ry = radius**2-(xs/2.-r)**2
              if (ry < 0):
                b[r,:] = 1
              else:
                b[r,0:int(ys/2.-sqrt(ry)+1)] = 1
                b[r,int(ys/2.+sqrt(ry)+1):ys] = 1
        self.bpMask = badPixelMask(inputMask = b, filter=bpmFilter)
        if (os.access(bpmFile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(bpmFile)
        if (not os.access(bpmFile, os.F_OK)):
           self.bpMask.saveBadPixelMask(bpmFile)
           print "Bad Pixel Mask written to "+bpmFile
           f = open(self.logfile,'ab')
           f.write("Bad Pixel Mask written to "+bpmFile+'\n')
           f.close()
        self.bpmFile = bpmFile
	if (self.guiMessages):
	   print "FILE: "+bpmFile
      else:
	print "No bad pixel mask applied!"
        f = open(self.logfile,'ab')
        f.write("No bad pixel mask applied!\n")
        f.close()
        f = open(self.warnfile,'ab')
        f.write("WARNING: No bad pixel mask applied!\n")
        f.write('Time: '+str(datetime.today())+'\n\n')
        f.close()
	self.useBPMask = False

   def flatDivide(self):
      if (self.guiMessages): print "STATUS: Dividing Flats..."
      n = len(self.filenames)
      if (not os.access("flatDivided", os.F_OK)): os.mkdir("flatDivided",0755)
      print "Applying Bad Pixel Mask and Dividing By Master Flats..."
      f = open(self.logfile,'ab')
      f.write("Applying Bad Pixel Mask and Dividing By Master Flats...\n")
      f.close()
      guiCount = 0.
      #Check for master flat(s)
      if (os.access(self.params['DEFAULT_MASTER_FLAT'], os.R_OK)):
        if (self.params['DEFAULT_MASTER_FLAT'].count('.dat') != 0):
           f = open(self.params['DEFAULT_MASTER_FLAT'])
           s = f.read().split('\n')
           f.close()
           try: s.remove('')
           except ValueError:
              junk = ''
	   for j in range(len(s)):
	      temp = s[0].split()
	      pfix = temp[0]
	      defflat = temp[1]
	      for l in range(len(self.filenames)):
		if (self.filenames[l].find(pfix) != -1):
		   self.flatnames[l] = defflat
        else:
           self.flatnames=[self.params['DEFAULT_MASTER_FLAT']]*n
      for j in range(n):
        fdfile = "flatDivided/fd_"+self.filenames[j] 
        if (os.access(fdfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(fdfile)
        if (not os.access(fdfile, os.F_OK)):
	   #Handle missing flat field
	   if (not os.access(self.flatnames[j], os.F_OK)):
	      print "WARNING: No flat found for file "+self.filenames[j]
	      print "Proceeding without dividing by any flat field!"
	      f = open(self.logfile,'ab')
	      f.write("WARNING: No flat found for file "+self.filenames[j]+"\n")
	      f.write("Proceeding without dividing by any flat field!\n")
	      f.close()
	      f = open(self.warnfile,'ab')
              f.write("WARNING: No flat found for file "+self.filenames[j]+"\n")
              f.write("Proceeding without dividing by any flat field!\n")
	      f.write('Time: '+str(datetime.today())+'\n\n')
	      f.close()
	      image = pyfits.open(self.prefix+self.filenames[j])
              image[0].data = image[0].data.astype("Float64")
	      image[0].header.update('HISTORY','NOT Flat Divided')
              image[0].header.update('FILENAME', fdfile)
              image.verify('silentfix')
              image.writeto(fdfile)
              if (self.params['DO_NOISEMAPS'].lower() == "yes"):
		nmfile = "flatDivided/fd_NM_"+self.filenames[j]
		if (os.access(nmfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(nmfile)
		if (not os.access(nmfile, os.F_OK)):
		   nm = pyfits.open(self.prefix+'NM_'+self.filenames[j])
                   nm[0].header.update('FILENAME', nmfile)
                   nm.writeto(nmfile)
                   nm.close()
	      continue
	   masterFlat = pyfits.open(self.flatnames[j])
	   masterFlat[0].data = masterFlat[0].data.astype("Float64")
           #Apply Bad Pixel Mask to Master Flat and renormalize Master Flat
           if (not self.useBPMask):
              self.bpMask = badPixelMask(zeros(shape(masterFlat[0].data)))
           mfmed = arraymedian(compress(self.bpMask.getGoodPixelMask(), masterFlat[0].data))
           masterFlat[0].data = masterFlat[0].data / mfmed
           #Set bad pixels to 0
           masterFlat[0].data = masterFlat[0].data*(self.bpMask.getGoodPixelMask()+0.)
           #Divide files by master flats
	   image = pyfits.open(self.prefix+self.filenames[j])
	   image[0].data = image[0].data.astype("Float64")
           if (self.params['DO_NOISEMAPS'].lower() == "yes"):
	      oldimage = image[0].data+0.
           image[0].data = image[0].data/masterFlat[0].data
           #Set bad pixels to 0
           image[0].data = reshape(image[0].data, self.bpMask.arraySize)
           image[0].data[self.bpMask.badPixels] = 0
           bp2 = reshape(where(masterFlat[0].data == 0, 1, 0), masterFlat[0].data.nelements())
           image[0].data[compress(bp2, arange(masterFlat[0].data.nelements()))] = 0
           image[0].data = reshape(image[0].data, self.bpMask.arrayShape)
           image[0].header.update('HISTORY','Flat Divided')
           image[0].header.update('FILENAME', fdfile)
           image.verify('silentfix')
           image.writeto(fdfile)
           if (self.params['DO_NOISEMAPS'].lower() == "yes"):
	      nmfile = "flatDivided/fd_NM_"+self.filenames[j]
              if (os.access(nmfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(nmfile)
              if (not os.access(nmfile, os.F_OK)):
                nmFlat = pyfits.open(self.flatnames[j].replace('masterFlats/','masterFlats/nm_'))
		nmFlat[0].data = nmFlat[0].data.astype("Float64")
                nmFlat[0].data = nmFlat[0].data / mfmed
		nm = pyfits.open(self.prefix+'NM_'+self.filenames[j])
		b1 = where(oldimage == 0)
		b2 = where(masterFlat[0].data == 0)
		nm[0].data = abs(image[0].data)*sqrt(abs(nm[0].data**2/oldimage**2+nmFlat[0].data**2/masterFlat[0].data**2+0.))
		nm[0].data[b1] = 0
		nm[0].data[b2] = 0
                nm[0].header.update('FILENAME', nmfile)
                nm.writeto(nmfile)
		nmFlat.close()
                nm.close()
	   image.close() 
	   masterFlat.close()
	guiCount+=1.
	if (guiMessages):
	   print "PROGRESS: "+str(int(self.guiCRf*(68+guiCount/n*4)))
	   print "FILE: "+fdfile
           if (self.params['DO_NOISEMAPS'].lower() == "yes"):
              nmfile = "flatDivided/fd_NM_"+self.filenames[j]
	      print "FILE: "+nmfile
      if (self.params['CLEAN_UP'].lower() == 'yes'):
        for j in self.filenames:
           if (os.access('darkSubtracted/ds_'+j, os.F_OK)):
              os.unlink('darkSubtracted/ds_'+j)
           if (os.access('darkSubtracted/ds_NM_'+j, os.F_OK)):
              os.unlink('darkSubtracted/ds_NM_'+j)
      self.oldpfix = self.prefix
      self.prefix = "flatDivided/fd_"

   def selectSkyFiles(self, prefix, files, j, rng, diff):
      #Select out from a filelist those files with matching prefixes,
      #RA or DEC at least diff arcsec away, and
      #optionally within a certain range (e.g. +/-3).  Setting
      #rng to 0 selects all files that match the first two criteria
      #Use these as the sky.
      n = len(files)
      befFiles = []
      aftFiles = []
      temp = pyfits.open(files[j])
      objDec = getRADec(temp[0].header[self.params['DEC_KEYWORD']],warnfile=self.warnfile,rel=self.params['RELATIVE_OFFSET_ARCSEC'].lower(),dec=True)
      objRA = getRADec(temp[0].header[self.params['RA_KEYWORD']],warnfile=self.warnfile,rel=self.params['RELATIVE_OFFSET_ARCSEC'].lower())*15
      temp.close()
      lastRA = 0
      lastDec = 0
      #Find matching files before the current object
      if (j > 0):
        for l in range(j-1,-1,-1):
           if (j != l and files[j][:files[j].rfind(self.delim,0,files[j].rfind('.'))] == files[l][:files[l].rfind(self.delim,0,files[l].rfind('.'))]):
              temp = pyfits.open(prefix+files[l])
              currDec = getRADec(temp[0].header[self.params['DEC_KEYWORD']],warnfile=self.warnfile,rel=self.params['RELATIVE_OFFSET_ARCSEC'].lower(),dec=True)
              currRA = getRADec(temp[0].header[self.params['RA_KEYWORD']],warnfile=self.warnfile,rel=self.params['RELATIVE_OFFSET_ARCSEC'].lower())*15
              temp.close()
              diffRA = (currRA-objRA)*math.cos(objDec*math.pi/180)
              #if ((abs(diffRA) > diff or abs(currDec-objDec) > diff) and (abs(diffRA) > diff or abs(currDec-lastDec) > diff)):
	      if ((abs(diffRA) > diff or abs(currDec-objDec) > diff)):
                 #lastRA = currRA
                 #lastDec = currDec
                 befFiles.append(files[l])
      #Find matching files after the current object
      if (j < n-1):
        for l in range(j+1,n,1):
           if (j != l and files[j][:files[j].rfind(self.delim,0,files[j].rfind('.'))] == files[l][:files[l].rfind(self.delim,0,files[j].rfind('.'))]):
              temp = pyfits.open(prefix+files[l])
              currDec = getRADec(temp[0].header[self.params['DEC_KEYWORD']],warnfile=self.warnfile,rel=self.params['RELATIVE_OFFSET_ARCSEC'].lower(),dec=True)
              currRA = getRADec(temp[0].header[self.params['RA_KEYWORD']],warnfile=self.warnfile,rel=self.params['RELATIVE_OFFSET_ARCSEC'].lower())*15
              temp.close()
              diffRA = (currRA-objRA)*math.cos(objDec*math.pi/180)
              #if ((abs(diffRA) > diff or abs(currDec-objDec) > diff) and (abs(diffRA) > diff or abs(currDec-lastDec) > diff)):
              if ((abs(diffRA) > diff or abs(currDec-objDec) > diff)):
                 #lastRA = currRA
                 #lastDec = currDec
                 aftFiles.append(files[l])
      bef = len(befFiles)
      aft = len(aftFiles)
      if (rng == 0):
        nbef = bef
        naft = aft
      else:
        nbef = min(rng, bef)
        naft = min(rng, aft)
        if (bef < rng):
           naft = 2*rng-bef
        elif (aft < rng):
           nbef = 2*rng-aft
        if (nbef+naft < 2*rng):
           print "Warning: Only able to use ",nbef+naft," files for sky."
           f = open(self.logfile,'ab')
           f.write("Warning: Only able to use "+str(nbef+naft)+" files for sky.\n")
           f.close()
           f = open(self.warnfile,'ab')
           f.write("WARNING: Only able to use "+str(nbef+naft)+" files for sky.\n")
           f.write("Image: "+self.filenames[j]+"\n")
           f.write('Time: '+str(datetime.today())+'\n\n')
           f.close()
      skyfiles = ""
      for l in range(nbef):
        skyfiles+=prefix+befFiles[l]+","
      for l in range(naft):
        skyfiles+=prefix+aftFiles[l]+","
      skyfiles=skyfiles.strip(",")
      return skyfiles

   def skySubtract(self):
      if (not os.access("skySubtracted", os.F_OK) and self.params['DO_SKY_SUBTRACT'].lower() == 'yes'): os.mkdir("skySubtracted",0755)
      print "Performing Sky Subtraction..."
      if (self.guiMessages): print "STATUS: Sky Subtraction..."
      f = open(self.logfile,'ab')
      f.write("Performing Sky Subtraction...\n")
      f.close()
      doss = self.params['DO_SKY_SUBTRACT'].lower()
      if (self.params['USE_SKY_FILES'].lower() == "all"):
        rng = 0
      elif (self.params['USE_SKY_FILES'].lower() == "range"):
        rng = self.params['SKY_FILES_RANGE']
      diff = self.params['SKY_DITHERING_RANGE']/3600.
      method = self.params['SKY_SUBTRACT_METHOD'].lower()
      n = len(self.filenames)
      useOnlyPos = []
      newfilenames = []
      newflatnames = []
      newlampnames = []
      print "Creating master skies from other images and subtracting..."
      f = open(self.logfile,'ab')
      f.write("Creating master skies from other images and subtracting...\n")
      f.close()
      methods = self.findSkyMethods(self.filenames, self.params['SKY_SUBTRACT_METHOD'])
      guiCount = 0.
      if (doss == "no"):
	newfilenames = self.filenames
	newflatnames = self.flatnames
	newlampnames = self.lampnames
      if (doss == "yes" and methods.count('median') > 0):
	for j in range(n):
           if (methods[j] != 'median'):
              continue
	   ssfile = "skySubtracted/ss_"+self.filenames[j]
	   if (os.access(ssfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(ssfile)
	   if (not os.access(ssfile, os.F_OK)):
              #Find all filenames with prefix matching the object
              #and other matching criteria
              #Use these as skies
	      skyfiles = self.selectSkyFiles(self.prefix, self.filenames, j, rng, diff)
	      if (skyfiles == ''):
		print "Only one image found for object ",self.filenames[j]
                print "Sky NOT subtracted!"
                f = open(self.logfile,'ab')
                f.write("Only one image found for object "+self.filenames[j]+'\n')
                f.write('Sky NOT subtracted!\n')
                f.close()
                f = open(self.warnfile,'ab')
                f.write("WARNING: Only one image found for object "+self.filenames[j]+'\n')
                f.write("Sky NOT subtracted!\n")
                f.write('Time: '+str(datetime.today())+'\n\n')
                f.close()
                self.filenames[j] = 'REMOVE'
                continue
	      useOnlyPos.append(True)
	      skyfilename='tempSky.fits'
	      if (os.access(skyfilename, os.F_OK)): os.unlink(skyfilename)
	      iraf.unlearn('imcombine')
              #Combine Skys with imcombine.  Scale each sky by the
              #reciprocal of its median and then median combine them.
              #imcombine can't handle strings longer than 1024 bytes
              if (len(skyfiles) > 1023):
                tmpFile = open("tmpIRAFFile.dat", "wb")
                tmpFile.write(skyfiles.replace(",","\n"))
                tmpFile.close()
                iraf.images.immatch.imcombine("@tmpIRAFFile.dat", skyfilename, combine="median", scale="median", reject=self.params['SKY_REJECT_TYPE'], nlow=self.params['SKY_NLOW'], nhigh=self.params['SKY_NHIGH'], lsigma=self.params['SKY_LSIGMA'], hsigma=self.params['SKY_HSIGMA'])
                os.unlink("tmpIRAFFile.dat")
              else:
                iraf.images.immatch.imcombine(skyfiles, skyfilename, combine="median", scale="median", reject=self.params['SKY_REJECT_TYPE'], nlow=self.params['SKY_NLOW'], nhigh=self.params['SKY_NHIGH'], lsigma=self.params['SKY_LSIGMA'], hsigma=self.params['SKY_HSIGMA'])
              masterSky = pyfits.open(skyfilename)
              image = pyfits.open(self.prefix+self.filenames[j])
              #subtract
              image[0].data = image[0].data-masterSky[0].data
              image[0].header.update('HISTORY', "Sky subtracted: "+methods[j])
              image[0].header.update('FILENAME', ssfile)
              if (self.params['DO_NOISEMAPS'].lower() == "yes"):
                if (masterSky[0].header.has_key('NCOMBINE')):
		   ncomb = float(masterSky[0].header['NCOMBINE'])
                else: ncomb = 1.0
                nmsky = masterSky[0].data/ncomb
                nmfile = "skySubtracted/ss_NM_"+self.filenames[j]
                if (os.access(nmfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(nmfile)
                if (not os.access(nmfile, os.F_OK)):
                   nm = pyfits.open(self.prefix+'NM_'+self.filenames[j])
                   nm[0].data = sqrt(nm[0].data**2+nmsky)
                   nm[0].header.update('FILENAME', nmfile)
                   nm.writeto(nmfile)
                   nm.close()
              image.verify('silentfix')
              image.writeto(ssfile)
              image.close()
              masterSky.close()
              if (os.access('tempSky.fits',os.F_OK)): os.unlink('tempSky.fits')
	      newfilenames.append(self.filenames[j])
	      newflatnames.append(self.flatnames[j])
	      newlampnames.append(self.lampnames[j])
	   guiCount+=1.
	   if (guiMessages):
	      print "PROGRESS: "+str(int(self.guiCRdone+self.guiCRf*(38+guiCount/n*4)))
	      print "FILE: "+ssfile
              if (self.params['DO_NOISEMAPS'].lower() == "yes"):
                nmfile = "skySubtracted/ss_NM_"+self.filenames[j]
		print "FILE: "+nmfile
      if (doss == "yes" and methods.count('dither') > 0):
	objDec=[]
	objRA=[]
	isUsed = zeros(n)
        for j in range(n):
	   temp = pyfits.open(self.prefix+self.filenames[j])
           objDec.append(getRADec(temp[0].header[self.params['DEC_KEYWORD']],warnfile=self.warnfile,rel=self.params['RELATIVE_OFFSET_ARCSEC'].lower(),dec=True))
           objRA.append(getRADec(temp[0].header[self.params['RA_KEYWORD']],warnfile=self.warnfile,rel=self.params['RELATIVE_OFFSET_ARCSEC'].lower())*15)
           temp.close()
        for j in range(n):
	   if (methods[j] != 'dither'):
	      continue
           ssfile = "skySubtracted/ss_"+self.filenames[j]
           if (os.access(ssfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(ssfile)
           #Find all filenames with prefix matching the object
           #Use these as skies
	   if (isUsed[j] == 1): continue
           skyfiles = []
	   skyDec = []
	   skyRA = []
	   ind = []
	   nmsky = []
           for l in range(n):
	      if (methods[l] != 'dither'): continue
	      if (self.filenames[j][:self.filenames[j].rfind(self.delim,0,self.filenames[j].rfind('.'))] == self.filenames[l][:self.filenames[l].rfind(self.delim,0,self.filenames[l].rfind('.'))]):
		if (j == l): currFile = len(skyfiles)
                skyfiles.append(self.prefix+self.filenames[l])
		skyDec.append(objDec[l])
		skyRA.append(objRA[l])
		ind.append(l)
		nmsky.append(self.prefix+'NM_'+self.filenames[l])
	   if (len(skyfiles) == 1):
	      print "Only one image found for object ",self.filenames[j]
              print "Sky NOT subtracted!"
              f = open(self.logfile,'ab')
              f.write("Only one image found for object "+self.filenames[j]+'\n')
              f.write('Sky NOT subtracted!\n')
              f.close()
              f = open(self.warnfile,'ab')
              f.write("WARNING: Only one image found for object "+self.filenames[j]+'\n')
              f.write("Sky NOT subtracted!\n")
              f.write('Time: '+str(datetime.today())+'\n\n')
              f.close()
              self.filenames[j] = 'REMOVE'
              continue
	   isUsed[j] = 1
	   match = -1
	   for l in range(currFile+1, len(skyfiles)):
	      diffRA = (skyRA[l]-skyRA[currFile])*math.cos(skyDec[currFile]*math.pi/180)
              if ((abs(diffRA) > diff or abs(skyDec[l]-skyDec[currFile]) > diff) and self.flatnames[j] == self.flatnames[ind[l]] and isUsed[ind[l]] != 1):
		match = l
		isUsed[ind[l]] = 1
		useOnlyPos.append(False)
		newflatnames.append(self.flatnames[j])
		newlampnames.append(self.lampnames[j])
		break
	   if (match == -1):
	      if (self.params['IGNORE_ODD_FRAMES'].lower() == 'yes'):
		print self.filenames[j]+' ignored.'
                f = open(self.logfile,'ab')
                f.write("Odd frame "+self.filenames[j]+' ignored.\n')
                f.close()
                f = open(self.warnfile,'ab')
                f.write("WARNING: Odd frame "+self.filenames[j]+' ignored.\n')
                f.write('Time: '+str(datetime.today())+'\n\n')
                f.close()
		continue
	      else:
		for l in range(currFile-1, -1, -1):
		   diffRA = (skyRA[l]-skyRA[currFile])*math.cos(skyDec[currFile]*math.pi/180)
		   if ((abs(diffRA) > diff or abs(skyDec[l]-skyDec[currFile]) > diff) and self.flatnames[j] == self.flatnames[ind[l]]):
		      match = l
		      useOnlyPos.append(True)
		      newflatnames.append(self.flatnames[j])
		      newlampnames.append(self.lampnames[j])
		      break
	   skyfilename = skyfiles[match]
	   nmskyname = nmsky[match]
           newfilenames.append(self.filenames[j])
	   if (not os.access(ssfile, os.F_OK)):
	      masterSky = pyfits.open(skyfilename)
	      image = pyfits.open(self.prefix+self.filenames[j])
              #subtract
              image[0].data = image[0].data-masterSky[0].data
              image[0].header.update('HISTORY', "Sky subtracted: "+methods[j])
              image[0].header.update('FILENAME', ssfile)
              if (self.params['DO_NOISEMAPS'].lower() == "yes"):
                nmfile = "skySubtracted/ss_NM_"+self.filenames[j]
                if (os.access(nmfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(nmfile)
                if (not os.access(nmfile, os.F_OK)):
                   nm1 = pyfits.open(self.prefix+'NM_'+self.filenames[j])
		   nm2 = pyfits.open(nmskyname)
                   nm1[0].data = sqrt(nm1[0].data**2+nm2[0].data**2)
                   nm1[0].header.update('FILENAME', nmfile)
                   nm1.writeto(nmfile)
                   nm1.close()
		   nm2.close()
              image.verify('silentfix')
              image.writeto(ssfile)
              image.close()
              masterSky.close()
           guiCount+=1.
           if (guiMessages):
	      print "PROGRESS: "+str(int(self.guiCRdone+self.guiCRf*(38+guiCount/n*4)))
              print "FILE: "+ssfile
              if (self.params['DO_NOISEMAPS'].lower() == "yes"):
                nmfile = "skySubtracted/ss_NM_"+self.filenames[j]
                print "FILE: "+nmfile

      if (doss == "yes" and methods.count('ifu_onsource_dither') > 0):
	objDec=[]
	objRA=[]
	isUsed = zeros(n)
        for j in range(n):
	   temp = pyfits.open(self.prefix+self.filenames[j])
           objDec.append(getRADec(temp[0].header[self.params['DEC_KEYWORD']],warnfile=self.warnfile,rel=self.params['RELATIVE_OFFSET_ARCSEC'].lower(),dec=True))
           objRA.append(getRADec(temp[0].header[self.params['RA_KEYWORD']],warnfile=self.warnfile,rel=self.params['RELATIVE_OFFSET_ARCSEC'].lower())*15)
           temp.close()
        for j in range(n):
	   if (methods[j] != 'ifu_onsource_dither'):
	      continue
           ssfile = "skySubtracted/ss_"+self.filenames[j]
           if (os.access(ssfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(ssfile)
           #Find all filenames with prefix matching the object
           #Use these as skies
	   if (isUsed[j] == 1): continue
           skyfiles = []
	   skyDec = []
	   skyRA = []
	   ind = []
	   nmsky = []
           for l in range(n):
	      if (methods[l] != 'ifu_onsource_dither'): continue
	      if (self.filenames[j][:self.filenames[j].rfind(self.delim,0,self.filenames[j].rfind('.'))] == self.filenames[l][:self.filenames[l].rfind(self.delim,0,self.filenames[l].rfind('.'))]):
		if (j == l): currFile = len(skyfiles)
                skyfiles.append(self.prefix+self.filenames[l])
		skyDec.append(objDec[l])
		skyRA.append(objRA[l])
		ind.append(l)
		nmsky.append(self.prefix+'NM_'+self.filenames[l])
	   if (len(skyfiles) == 1):
	      print "Only one image found for object ",self.filenames[j]
              print "Sky NOT subtracted!"
              f = open(self.logfile,'ab')
              f.write("Only one image found for object "+self.filenames[j]+'\n')
              f.write('Sky NOT subtracted!\n')
              f.close()
              f = open(self.warnfile,'ab')
              f.write("WARNING: Only one image found for object "+self.filenames[j]+'\n')
              f.write("Sky NOT subtracted!\n")
              f.write('Time: '+str(datetime.today())+'\n\n')
              f.close()
              self.filenames[j] = 'REMOVE'
              continue
	   isUsed[j] = 1
	   match = -1
	   #Match up each on-source frame with the next sky frame -- at least diff away in RA or Dec from the on-source frame
	   for l in range(currFile+1, len(skyfiles)):
	      diffRA = (skyRA[l]-skyRA[currFile])*math.cos(skyDec[currFile]*math.pi/180)
              if ((abs(diffRA) > diff or abs(skyDec[l]-skyDec[currFile]) > diff) and self.flatnames[j] == self.flatnames[ind[l]] and isUsed[ind[l]] != 1):
		match = l
		isUsed[ind[l]] = 1
		useOnlyPos.append(True)
		newflatnames.append(self.flatnames[j])
		newlampnames.append(self.lampnames[j])
	        useOnlyPos.append(True)
                newflatnames.append(self.flatnames[ind[l]])
                newlampnames.append(self.lampnames[ind[l]])
                newfilenames.append(self.filenames[j])
                newfilenames.append(self.filenames[ind[l]])
		break
	   if (match == -1):
	      if (self.params['IGNORE_ODD_FRAMES'].lower() == 'yes'):
		print self.filenames[j]+' ignored.'
                f = open(self.logfile,'ab')
                f.write("Odd frame "+self.filenames[j]+' ignored.\n')
                f.close()
                f = open(self.warnfile,'ab')
                f.write("WARNING: Odd frame "+self.filenames[j]+' ignored.\n')
                f.write('Time: '+str(datetime.today())+'\n\n')
                f.close()
		continue
	      else:
		for l in range(currFile-1, -1, -1):
		   diffRA = (skyRA[l]-skyRA[currFile])*math.cos(skyDec[currFile]*math.pi/180)
		   if ((abs(diffRA) > diff or abs(skyDec[l]-skyDec[currFile]) > diff) and self.flatnames[j] == self.flatnames[ind[l]]):
		      match = l
		      useOnlyPos.append(True)
		      newflatnames.append(self.flatnames[j])
		      newlampnames.append(self.lampnames[j])
                      newfilenames.append(self.filenames[j])
		      break
	   skyfilename = skyfiles[match]
	   nmskyname = nmsky[match]
           #newfilenames.append(self.filenames[j])
	   #newfilenames.append(self.filenames[ind[match]])
           ssfile2 = "skySubtracted/ss_"+self.filenames[ind[match]]
           if (os.access(ssfile2, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(ssfile2)
	   if (not os.access(ssfile, os.F_OK) or not os.access(ssfile2, os.F_OK)):
	      masterSky = pyfits.open(skyfilename)
	      image = pyfits.open(self.prefix+self.filenames[j])
              #subtract
              image[0].data = image[0].data-masterSky[0].data
              image[0].header.update('HISTORY', "Sky subtracted: "+methods[j])
              image[0].header.update('FILENAME', ssfile)
              if (self.params['DO_NOISEMAPS'].lower() == "yes"):
                nmfile = "skySubtracted/ss_NM_"+self.filenames[j]
                if (os.access(nmfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(nmfile)
		nmfile2 = "skySubtracted/ss_NM_"+self.filenames[ind[match]]
		if (os.access(nmfile2, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(nmfile2)
                nm1 = pyfits.open(self.prefix+'NM_'+self.filenames[j])
		nm2 = pyfits.open(nmskyname)
                nm1[0].data = sqrt(nm1[0].data**2+nm2[0].data**2)
                nm1[0].header.update('FILENAME', nmfile)
                if (not os.access(nmfile, os.F_OK)):
		   nm1.writeto(nmfile)
		if (not os.access(nmfile2, os.F_OK)):
		   nm1[0].header.update('FILENAME', nmfile2)
		   nm1.writeto(nmfile2)
                nm1.close()
		nm2.close()
              image.verify('silentfix')
              if (not os.access(ssfile, os.F_OK)):
		image.writeto(ssfile)
	      if (not os.access(ssfile2, os.F_OK)):
		image[0].data = -1*image[0].data
                image[0].header.update('FILENAME', ssfile2)
		image.writeto(ssfile2)
              image.close()
              masterSky.close()	      
           guiCount+=1.
           if (guiMessages):
	      print "PROGRESS: "+str(int(self.guiCRdone+self.guiCRf*(38+guiCount/n*4)))
              print "FILE: "+ssfile
              if (self.params['DO_NOISEMAPS'].lower() == "yes"):
                nmfile = "skySubtracted/ss_NM_"+self.filenames[j]
                print "FILE: "+nmfile
	      
      if (doss == "yes" and methods.count('step') > 0):
        isUsed = zeros(n)
        for j in range(n):
	   if (methods[j] != 'step'):
	      continue
           ssfile = "skySubtracted/ss_"+self.filenames[j]
           if (os.access(ssfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(ssfile)
           #Find all filenames with prefix matching the object
           #Use these as skies
           if (isUsed[j] == 1): continue
	   skyfiles = []
	   ind = []
	   nmsky = []
           for l in range(n):
	      if (methods[l] != 'step'): continue
	      if (self.filenames[j][:self.filenames[j].rfind(self.delim,0,self.filenames[j].rfind('.'))] == self.filenames[l][:self.filenames[l].rfind(self.delim,0,self.filenames[l].rfind('.'))]):
		if (j == l): currFile = len(skyfiles)
                skyfiles.append(self.prefix+self.filenames[l])
		ind.append(l)
		nmsky.append(self.prefix+'NM_'+self.filenames[l])
           if (len(skyfiles) == 1):
	      print "Only one image found for object ",self.filenames[j]
              print "Sky NOT subtracted!"
              f = open(self.logfile,'ab')
              f.write("Only one image found for object "+self.filenames[j]+'\n')
              f.write('Sky NOT subtracted!\n')
              f.close()
              f = open(self.warnfile,'ab')
              f.write("WARNING: Only one image found for object "+self.filenames[j]+'\n')
              f.write("Sky NOT subtracted!\n")
              f.write('Time: '+str(datetime.today())+'\n\n')
              f.close()
              self.filenames[j] = 'REMOVE'
              continue
           isUsed[j] = 1
	   if (len(skyfiles) % 2 == 0):
	      skyfilename = skyfiles[currFile+1]
	      isUsed[ind[currFile+1]] = 1
	      useOnlyPos.append(False)
	      newflatnames.append(self.flatnames[j])
	      newlampnames.append(self.lampnames[j])
	      nmskyname = nmsky[currFile+1]
	   else:
	      skyfilename = skyfiles[(currFile+1)%len(skyfiles)]
	      useOnlyPos.append(True)
	      newflatnames.append(self.flatnames[j])
	      newlampnames.append(self.lampnames[j])
	      nmskyname = nmsky[(currFile+1)%len(skyfiles)]
           newfilenames.append(self.filenames[j])
           if (not os.access(ssfile, os.F_OK)):
              masterSky = pyfits.open(skyfilename)
              image = pyfits.open(self.prefix+self.filenames[j])
              #subtract
              image[0].data = image[0].data-masterSky[0].data
              image[0].header.update('HISTORY', "Sky subtracted: "+methods[j])
              image[0].header.update('FILENAME', ssfile)
              if (self.params['DO_NOISEMAPS'].lower() == "yes"):
                nmfile = "skySubtracted/ss_NM_"+self.filenames[j]
                if (os.access(nmfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(nmfile)
                if (not os.access(nmfile, os.F_OK)):
                   nm1 = pyfits.open(self.prefix+'NM_'+self.filenames[j])
                   nm2 = pyfits.open(nmskyname)
                   nm1[0].data = sqrt(nm1[0].data**2+nm2[0].data**2)
                   nm1[0].header.update('FILENAME', nmfile)
                   nm1.writeto(nmfile)
                   nm1.close()
                   nm2.close()
              image.verify('silentfix')
              image.writeto(ssfile)
              image.close()
              masterSky.close()
           guiCount+=1.
           if (guiMessages):
	      print "PROGRESS: "+str(int(self.guiCRdone+self.guiCRf*(38+guiCount/n*4)))
              print "FILE: "+ssfile
              if (self.params['DO_NOISEMAPS'].lower() == "yes"):
                nmfile = "skySubtracted/ss_NM_"+self.filenames[j]
                print "FILE: "+nmfile

      if (doss == "yes" and methods.count('offsource_dither') > 0):
	shift = self.params['SKY_OFFSOURCE_RANGE']/3600.
	objfilenames = []
	skyfilenames = []
	isSky = []
        if (self.params['SKY_OFFSOURCE_METHOD'].lower() == "auto"):
           objDec=[]
           objRA=[]
           tmpprefixes = []
	   refRA = []
	   refDec = []
	   group = []
	   ind = []
	   nsky = 0
           #Create array with prefixes, RA and Dec for first object
           #with each prefix (assumed to be an on-source).
	   for j in range(n):
	      if (methods[j] != 'offsource_dither'):
		continue
              skyprefix = self.filenames[j][:self.filenames[j].rfind(self.delim,0,self.filenames[j].rfind('.'))]
              temp = pyfits.open(self.prefix+self.filenames[j])
              tempDec = getRADec(temp[0].header[self.params['DEC_KEYWORD']],warnfile=self.warnfile,rel=self.params['RELATIVE_OFFSET_ARCSEC'].lower(),dec=True)
              tempRA = getRADec(temp[0].header[self.params['RA_KEYWORD']],warnfile=self.warnfile,rel=self.params['RELATIVE_OFFSET_ARCSEC'].lower())*15
              temp.close()
              if (tmpprefixes.count(skyprefix) == 0):
                tmpprefixes.append(skyprefix)
		group.append(nsky)
		refRA.append(tempRA)
		refDec.append(tempDec)
		nsky+=1
	      else:
		group.append(tmpprefixes.index(skyprefix))
	      ind.append(j)
              objDec.append(tempDec)
              objRA.append(tempRA)
	      diffRA = (tempRA-refRA[group[j]])*math.cos(refDec[group[j]]*math.pi/180)
	      if (abs(diffRA) > shift or abs(tempDec-refDec[group[j]]) > shift):
		isSky.append(1)
	      else:
		isSky.append(0)
	   #Use the isUsed and isSky arrays to pair up objects and skies
	   #with the same prefix.
	   isUsed = zeros(n)
	   lastObj = 0
	   for j in range(len(group)):
	      if (isSky[j] == 0):
		match = -1
		for l in range(lastObj,len(group)):
		   if (group[j] == group[l] and isSky[l] == 1 and isUsed[l] == 0):
		      objfilenames.append(self.filenames[ind[j]])
		      skyfilenames.append(self.filenames[ind[l]])
		      newfilenames.append(self.filenames[ind[j]])
		      newflatnames.append(self.flatnames[ind[j]])
		      newlampnames.append(self.lampnames[ind[j]])
		      isUsed[j] = 1
		      isUsed[l] = 1
		      match = 1
		      lastObj = j
		      break
		if (match == -1):
		   if (self.params['IGNORE_ODD_FRAMES'].lower() == 'yes'):
		      print self.filenames[ind[j]]+' ignored.'
                      f = open(self.logfile,'ab')
                      f.write("Odd frame "+self.filenames[ind[j]]+' ignored.\n')
                      f.close()
                      f = open(self.warnfile,'ab')
                      f.write("WARNING: Odd frame "+self.filenames[ind[j]]+' ignored.\n')
                      f.write('Time: '+str(datetime.today())+'\n\n')
                      f.close()
		      continue
		   else:
		      currMatch = 1000
		      for l in range(len(group)):
			if (group[j] == group[l] and isSky[l] == 1 and abs(l-j) < currMatch):
			   match = l
			   currMatch = abs(l-j)
		      objfilenames.append(self.filenames[ind[j]])
                      skyfilenames.append(self.filenames[ind[match]])
                      newfilenames.append(self.filenames[ind[j]])
                      newflatnames.append(self.flatnames[ind[j]])
                      newlampnames.append(self.lampnames[ind[j]])
		      if (currMatch != 1000):
			isUsed[j] = 1
                        isUsed[match] = 1
			lastObj = j
		        print self.filenames[ind[match]]+' used as sky for odd frame '+self.filenames[ind[j]]
                        f = open(self.logfile,'ab')
                        f.write(self.filenames[ind[match]]+' used as sky for odd frame '+self.filenames[ind[j]]+'.\n')
                        f.close()
                        f = open(self.warnfile,'ab')
                        f.write("WARNING: "+self.filenames[ind[match]]+' used as sky for odd frame '+self.filenames[ind[j]]+'.\n')
                        f.write('Time: '+str(datetime.today())+'\n\n')
                        f.close()
		      else:
                        print self.filenames[ind[j]]+' ignored.'
                        f = open(self.logfile,'ab')
                        f.write("Odd frame "+self.filenames[ind[j]]+' ignored.\n')
                        f.close()
                        f = open(self.warnfile,'ab')
                        f.write("WARNING: Odd frame "+self.filenames[ind[j]]+' ignored.\n')
                        f.write('Time: '+str(datetime.today())+'\n\n')
                        f.close()
	elif (self.params['SKY_OFFSOURCE_METHOD'].lower() == 'manual'):
	   #Read in ASCII file manually listing 2 columns: object sky
	   mfile = self.params['SKY_OFFSOURCE_MANUAL']
           if (not os.access(mfile, os.R_OK)):
              print "File "+mfile+" not found!"
              f = open(self.logfile,'ab')
              f.write("File "+mfile+" not found!\n")
              f.close()
              if (guiMessages):
                print "INPUT: File: Enter a file with manual offsource sky info:"
              tmp = raw_input("Enter a file with manual offsource sky info: ")
              if (guiMessages): print tmp
              if (os.access(tmp, os.R_OK)):
                mfile = tmp
              else:
                print "File " + tmp + " not found!"
                print "Sky not subtracted!"
                f = open(self.logfile,'ab')
                f.write("File " + tmp + " not found!\n")
                f.write("Sky not subtracted!\n")
                f.close()
                f = open(self.warnfile,'ab')
                f.write("WARNING: File "+mfile+" and file " + tmp + " not found!\n")
                f.write("Sky NOT subtracted!\n")
                f.write('Time: '+str(datetime.today())+'\n\n')
                f.close()
	   else:
              f = open(mfile, "rb")
	      s = f.read().split("\n")
              f.close()
              try: s.remove('')
      	      except ValueError:
        	junk = ''
	      for j in range(len(s)):
		temp = s[j].split()
		tempind0 = -1
		for l in range(len(self.filenames)):
		   if (self.filenames[l].find(temp[0][-31:]) != -1):
		      tempind0 = l
		tempind1 = -1
                for l in range(len(self.filenames)):
                   if (self.filenames[l].find(temp[1][-31:]) != -1):
                      tempind1 = l
	        #if (self.filenames.count(temp[0]) == 0):
		if (tempind0 == -1):
		   print "File "+temp[0]+" not valid!"
                   f = open(self.logfile,'ab')
                   f.write("File " + temp[0] + " not valid!\n")
                   f.close()
                   f = open(self.warnfile,'ab')
                   f.write("WARNING: File "+temp[0]+ " not valid!\n")
                   f.write("Sky NOT subtracted!\n")
                   f.write('Time: '+str(datetime.today())+'\n\n')
                   f.close()
		#elif (self.filenames.count(temp[1]) == 0):
		elif (tempind1 == -1):
                   print "File "+temp[1]+" not valid!"
                   f = open(self.logfile,'ab')
                   f.write("File " + temp[1] + " not valid!\n")
                   f.close()
                   f = open(self.warnfile,'ab')
                   f.write("WARNING: File "+temp[1]+ " not valid!\n")
                   f.write("Sky NOT subtracted for "+temp[0]+"!\n")
                   f.write('Time: '+str(datetime.today())+'\n\n')
                   f.close()
		else:
		   #tempind = self.filenames.index(temp[0])
		   tempind = tempind0
		   #objfilenames.append(temp[0])
		   #skyfilenames.append(temp[1])
		   objfilenames.append(self.filenames[tempind])
		   skyfilenames.append(self.filenames[tempind1])
		   newfilenames.append(self.filenames[tempind])
		   newflatnames.append(self.flatnames[tempind])
		   newlampnames.append(self.lampnames[tempind])
	for j in range(len(objfilenames)):
	   ssfile = "skySubtracted/ss_"+objfilenames[j]
	   useOnlyPos.append(True)
           if (os.access(ssfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(ssfile)
	   if (not os.access(ssfile, os.F_OK)):
	      masterSky = pyfits.open(self.prefix+skyfilenames[j])
	      image = pyfits.open(self.prefix+objfilenames[j])
              #subtract
              image[0].data = image[0].data-masterSky[0].data
              image[0].header.update('HISTORY', "Sky subtracted: "+methods[j])
              image[0].header.update('FILENAME', ssfile)
              if (self.params['DO_NOISEMAPS'].lower() == "yes"):
                nmfile = "skySubtracted/ss_NM_"+objfilenames[j]
                if (os.access(nmfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(nmfile)
                if (not os.access(nmfile, os.F_OK)):
                   nm1 = pyfits.open(self.prefix+'NM_'+objfilenames[j])
		   nm2 = pyfits.open(self.prefix+'NM_'+skyfilenames[j])
                   nm1[0].data = sqrt(nm1[0].data**2+nm2[0].data**2)
                   nm1[0].header.update('FILENAME', nmfile)
                   nm1.writeto(nmfile)
                   nm1.close()
		   nm2.close()
              image.verify('silentfix')
              image.writeto(ssfile)
              image.close()
              masterSky.close()
           guiCount+=1.
           if (guiMessages):
	      print "PROGRESS: "+str(int(self.guiCRdone+self.guiCRf*(38+guiCount/n*4)))
              print "FILE: "+ssfile
              if (self.params['DO_NOISEMAPS'].lower() == "yes"):
                nmfile = "skySubtracted/ss_NM_"+self.filenames[j]
                print "FILE: "+nmfile

      if (doss == "yes" and methods.count('offsource_multi_dither') > 0):
	shift = self.params['SKY_OFFSOURCE_RANGE']/3600.
	objfilenames = []
	skyfilenames = []
	isSky = []
        if (self.params['SKY_OFFSOURCE_METHOD'].lower() == "auto"):
           objDec=[]
           objRA=[]
           tmpprefixes = []
	   refRA = []
	   refDec = []
	   group = []
	   ind = []
	   nsky = 0
           #Create array with prefixes, RA and Dec for first object
           #with each prefix (assumed to be an on-source).
           #Create useSky array which contains the index of the correct
           #sky to use for each image.
	   for j in range(n):
	      if (methods[j] != 'offsource_multi_dither'):
		continue
              skyprefix = self.filenames[j][:self.filenames[j].rfind(self.delim,0,self.filenames[j].rfind('.'))]
              temp = pyfits.open(self.prefix+self.filenames[j])
              tempDec = getRADec(temp[0].header[self.params['DEC_KEYWORD']],warnfile=self.warnfile,rel=self.params['RELATIVE_OFFSET_ARCSEC'].lower(),dec=True)
              tempRA = getRADec(temp[0].header[self.params['RA_KEYWORD']],warnfile=self.warnfile,rel=self.params['RELATIVE_OFFSET_ARCSEC'].lower())*15
              temp.close()
              if (tmpprefixes.count(skyprefix) == 0):
                tmpprefixes.append(skyprefix)
		group.append(nsky)
		refRA.append(tempRA)
		refDec.append(tempDec)
		nsky+=1
	      else:
		group.append(tmpprefixes.index(skyprefix))
	      ind.append(j)
              objDec.append(tempDec)
              objRA.append(tempRA)
	      diffRA = (tempRA-refRA[group[j]])*math.cos(refDec[group[j]]*math.pi/180)
	      if (abs(diffRA) > shift or abs(tempDec-refDec[group[j]]) > shift):
		isSky.append(1)
	      else:
		isSky.append(0)
	   isUsed = zeros(n)
	   useSky = zeros(n)
	   tempskyfiles = []
	   lastObj = 0
	   nsky = 0
	   for j in range(len(group)):
	      if (isSky[j] == 0 and isUsed[j] == 0):
		match = -1
		tempobj = []
		for l in range(j,len(group)):
		   if (group[l] == group[j] and isUsed[l] == 0 and objRA[l] == objRA[j] and objDec[l] == objDec[j] and isSky[l] == 0):
		      isUsed[l] = 1
		      useSky[l] = nsky
		      tempobj.append(l)
		for l in range(lastObj,len(group)):
		   if (group[j] == group[l] and isSky[l] == 1 and isUsed[l] == 0):
		      match = 1
		      lastObj = j
		      for k in range(l,min(len(group),l+len(tempobj))):
			if (group[k] == group[l] and isUsed[k] == 0 and objRA[k] == objRA[l] and objDec[k] == objDec[l] and isSky[k] == 1):
			   isUsed[k] = 1
			   useSky[k] = nsky
		      nsky+=1
		      tempskyfiles.append(self.filenames[ind[j]])
                      newfilenames.append(self.filenames[ind[j]])
                      newflatnames.append(self.flatnames[ind[j]])
                      newlampnames.append(self.lampnames[ind[j]])
		      break
		if (match == -1):
		   if (self.params['IGNORE_ODD_FRAMES'].lower() == 'yes'):
		      for l in range(len(tempobj)):
			useSky[tempobj[l]] = -1
		        print self.filenames[ind[tempobj[l]]]+' ignored.'
                        f = open(self.logfile,'ab')
                        f.write("Odd frame "+self.filenames[ind[tempobj[l]]]+' ignored.\n')
                        f.close()
                        f = open(self.warnfile,'ab')
                        f.write("WARNING: Odd frame "+self.filenames[ind[tempobj[l]]]+' ignored.\n')
                        f.write('Time: '+str(datetime.today())+'\n\n')
                        f.close()
		      continue
		   else:
		      currMatch = 1000
		      for l in range(len(group)):
                        if (group[j] == group[l] and isSky[l] == 1 and abs(l-j) < currMatch):
                           match = l
                           currMatch = abs(l-j)
                      if (currMatch != 1000):
			lastObj = j
			f = open(self.logfile,'ab')
			fwarn = open(self.warnfile,'ab')
			f.write('Files ')
			fwarn.write('WARNING: Files ')
			print 'Files: '
			for k in range(max(0,l-len(tempobj)+1),min(len(group),l+len(tempobj))):
			   if (group[k] == group[match] and isUsed[k] == 0 and objRA[k] == objRA[match] and objDec[k] == objDec[match] and isSky[k] == 1):
			      isUsed[k] = 1
                              useSky[k] = nsky
			      f.write(self.filenames[ind[k]]+' ')
			      fwarn.write(self.filenames[ind[k]]+' ')
			      print self.filenames[ind[k]]
                        nsky+=1
			tempskyfiles.append(self.filenames[ind[j]])
                        newfilenames.append(self.filenames[ind[j]])
                        newflatnames.append(self.flatnames[ind[j]])
                        newlampnames.append(self.lampnames[ind[j]])
                        print '   used as sky for odd frames'
                        f.write(' used as sky for odd frames ')
                        fwarn.write(' used as sky for odd frames ')
                        for l in range(len(tempobj)):
                           print self.filenames[ind[tempobj[l]]]
			   f.write(self.filenames[ind[tempobj[l]]]+' ')
			   fwarn.write(self.filenames[ind[tempobj[l]]]+' ')
			f.write('\n')
                        fwarn.write('\nTime: '+str(datetime.today())+'\n\n')
                        f.close()
			fwarn.close()
                      else:
			for l in range(len(tempobj)):
			   useSky[tempobj[l]] = -1
                           print self.filenames[ind[tempobj[l]]]+' ignored.'
                           f = open(self.logfile,'ab')
                           f.write("Odd frame "+self.filenames[ind[tempobj[l]]]+' ignored.\n')
                           f.close()
                           f = open(self.warnfile,'ab')
                           f.write("WARNING: Odd frame "+self.filenames[ind[tempobj[l]]]+' ignored.\n')
                           f.write('Time: '+str(datetime.today())+'\n\n')
                           f.close()
	elif (self.params['SKY_OFFSOURCE_METHOD'].lower() == 'manual'):
	   mfile = self.params['SKY_OFFSOURCE_MANUAL']
           if (not os.access(mfile, os.R_OK)):
              print "File "+mfile+" not found!"
              f = open(self.logfile,'ab')
              f.write("File "+mfile+" not found!\n")
              f.close()
              if (guiMessages):
                print "INPUT: File: Enter a file with manual offsource sky info:"
              tmp = raw_input("Enter a file with manual offsource sky info: ")
              if (guiMessages): print tmp
              if (os.access(tmp, os.R_OK)):
                mfile = tmp
              else:
                print "File " + tmp + " not found!"
                print "Sky not subtracted!"
                f = open(self.logfile,'ab')
                f.write("File " + tmp + " not found!\n")
                f.write("Sky not subtracted!\n")
                f.close()
                f = open(self.warnfile,'ab')
                f.write("WARNING: File "+mfile+" and file " + tmp + " not found!\n")
                f.write("Sky NOT subtracted!\n")
                f.write('Time: '+str(datetime.today())+'\n\n')
                f.close()
	   else:
              f = open(mfile, "rb")
	      s = f.read().split("\n")
              f.close()
              try: s.remove('')
              except ValueError:
        	junk = ''
	      tempskyfiles = []
              useSky = zeros(n).tolist()
              isSky = zeros(n)
	      nsky = len(s)
              for j in range(len(s)):
		mansky = s[j].split()
                mansky[1] = int(mansky[1])
                mansky[2] = int(mansky[2])
                mansky[4] = int(mansky[4])
                mansky[5] = int(mansky[5])
                tempskyfiles.append(mansky[3]+str(j))
		for l in range(n):
		   skyprefix = self.filenames[l][:self.filenames[l].rfind(self.delim,0,self.filenames[l].rfind('.'))]
                   skyindex = int(self.filenames[l][self.filenames[l].rfind(self.delim,0,self.filenames[l].rfind('.'))+len(self.delim):self.filenames[l].rfind('.')])
                   if (skyprefix.find(mansky[0]) != -1 and skyindex >= mansky[1] and skyindex <= mansky[2]):
                      useSky[l] = j
                   if (skyprefix.find(mansky[3]) != -1 and skyindex >= mansky[4] and skyindex <= mansky[5]):
                      useSky[l] = j
                      isSky[l] = 1
	for j in range(nsky):
	   useOnlyPos.append(True)
	   ssfile = "skySubtracted/ss_"+tempskyfiles[j]
           if (os.access(ssfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(ssfile)
	   if (not os.access(ssfile, os.F_OK)):
	      objfilelist = ""
	      skyfilelist = ""
	      objNMlist = []
	      skyNMlist = []
	      for l in range(n):
		if (useSky[l] == j and isSky[l] == 0):
		   objfilelist+=self.prefix+self.filenames[ind[l]]+','
		   objNMlist.append(self.prefix+'NM_'+self.filenames[ind[l]])
		if (useSky[l] == j and isSky[l] == 1):
		   skyfilelist+=self.prefix+self.filenames[ind[l]]+','
		   skyNMlist.append(self.prefix+'NM_'+self.filenames[ind[l]])
	      objfilelist = objfilelist.strip(",")
	      skyfilelist = skyfilelist.strip(",")
	      tmpskyname = 'tempSky.fits'
	      tmpobjname = 'tempObj.fits'
	      if (os.access(tmpskyname, os.F_OK)): os.unlink(tmpskyname)
	      if (os.access(tmpobjname, os.F_OK)): os.unlink(tmpobjname)
	      iraf.unlearn('imcombine')
              #Combine Skys with imcombine.  Scale each sky by the
              #reciprocal of its median and then median combine them.
              #imcombine can't handle strings longer than 1024 bytes
              if (len(skyfilelist) > 1023 or len(objfilelist)):
                tmpFile = open("tmpIRAFFile.dat", "wb")
                tmpFile.write(skyfilelist.replace(",","\n"))
                tmpFile.close()
                iraf.images.immatch.imcombine("@tmpIRAFFile.dat", tmpskyname, combine="median", scale="median", reject=self.params['SKY_REJECT_TYPE'], nlow=self.params['SKY_NLOW'], nhigh=self.params['SKY_NHIGH'], lsigma=self.params['SKY_LSIGMA'], hsigma=self.params['SKY_HSIGMA'])
                tmpFile = open("tmpIRAFFile.dat", "wb")
                tmpFile.write(objfilelist.replace(",","\n"))
                tmpFile.close()
                iraf.images.immatch.imcombine("@tmpIRAFFile.dat", tmpobjname, combine="median", scale="median", reject=self.params['SKY_REJECT_TYPE'], nlow=self.params['SKY_NLOW'], nhigh=self.params['SKY_NHIGH'], lsigma=self.params['SKY_LSIGMA'], hsigma=self.params['SKY_HSIGMA'])
                os.unlink("tmpIRAFFile.dat")
              else:
                iraf.images.immatch.imcombine(skyfilelist, tmpskyname, combine="median", scale="median", reject=self.params['SKY_REJECT_TYPE'], nlow=self.params['SKY_NLOW'], nhigh=self.params['SKY_NHIGH'], lsigma=self.params['SKY_LSIGMA'], hsigma=self.params['SKY_HSIGMA'])
                iraf.images.immatch.imcombine(objfilelist, tmpobjname, combine="median", scale="median", reject=self.params['SKY_REJECT_TYPE'], nlow=self.params['SKY_NLOW'], nhigh=self.params['SKY_NHIGH'], lsigma=self.params['SKY_LSIGMA'],hsigma=self.params['SKY_HSIGMA'])
	      masterSky = pyfits.open(tmpskyname)
	      image = pyfits.open(tmpobjname)
              #scale and subtract
              msmed = arraymedian(compress(where(masterSky[0].data != 0, 1, 0), masterSky[0].data))
              immed = arraymedian(compress(where(image[0].data != 0, 1, 0), image[0].data))
              masterSky[0].data = masterSky[0].data * immed/msmed
              image[0].data = image[0].data-masterSky[0].data
              image[0].header.update('HISTORY', "Sky subtracted: "+methods[j])
              image[0].header.update('FILENAME', ssfile)
              if (self.params['DO_NOISEMAPS'].lower() == "yes"):
                nmfile = "skySubtracted/ss_NM_"+tempskyfiles[j]
                if (os.access(nmfile, os.F_OK)): os.unlink(nmfile)
		if (len(objNMlist) != 0 and len(skyNMlist) != 0 and os.access(objNMlist[0], os.F_OK) and os.access(skyNMlist[0], os.F_OK)):
		   nm1 = pyfits.open(objNMlist[0])
		   nm1[0].data = nm1[0].data**2
		   for i in range(1, len(objNMlist)):
		      temp = pyfits.open(objNMlist[i])
		      nm1[0].data+=temp[0].data**2
		      temp.close()
		   nm1[0].data = sqrt(nm1[0].data)/sqrt(len(objNMlist))
		   nm2 = pyfits.open(skyNMlist[0])
		   nm2[0].data = nm2[0].data**2
		   for i in range(1, len(skyNMlist)):
		      temp = pyfits.open(skyNMlist[i])
		      nm2[0].data+=temp[0].data**2
		      temp.close()
		   nm2[0].data = sqrt(nm2[0].data)/sqrt(len(skyNMlist))
                   nm1[0].data = sqrt(nm1[0].data**2+nm2[0].data**2)
                   nm1[0].header.update('FILENAME', nmfile)
                   nm1.writeto(nmfile)
                   nm1.close()
		   nm2.close()
              image.verify('silentfix')
              image.writeto(ssfile)
              image.close()
              masterSky.close()
           guiCount+=1.
           if (guiMessages):
	      print"PROGRESS: "+str(int(self.guiCRdone+self.guiCRf*(38+guiCount/n*4)))
              print "FILE: "+ssfile
              if (self.params['DO_NOISEMAPS'].lower() == "yes"):
                nmfile = "skySubtracted/ss_NM_"+self.filenames[j]
                print "FILE: "+nmfile
	if (os.access('tempSky.fits',os.F_OK)): os.unlink('tempSky.fits')
	if (os.access('tempObj.fits',os.F_OK)): os.unlink('tempObj.fits')

      self.filenames = newfilenames
      self.flatnames = newflatnames
      self.lampnames = newlampnames
      while self.filenames.count('REMOVE'):
	self.filenames.remove('REMOVE')
      self.oldpfix = self.prefix
      if (os.access('skySubtracted',os.F_OK) and self.params['DO_SKY_SUBTRACT'].lower() == 'yes'):
	self.prefix = "skySubtracted/ss_"
      ssprefix = self.prefix
      ssfiles = self.filenames
      if (self.params['CLEAN_UP'].lower() == 'yes'):
	for j in self.filenames:
	   if (os.access('removedCosmicRays/rcr_'+j, os.F_OK)):
	      os.unlink('removedCosmicRays/rcr_'+j)
           if (os.access('removedCosmicRays/rcr_NM_'+j, os.F_OK)):
              os.unlink('removedCosmicRays/rcr_NM_'+j)
      if (self.guiMessages): print "PROGRESS: " + str(int(a.guiCRdone+a.guiCRf*42+0.1))
      self.guiCount = 0.0
      #Check for empty file list
      if (len(self.filenames) == 0):
	print "ERROR: File list has become empty!"
	f = open(self.logfile,'ab')
	f.write('ERROR: File list has become empty!\n')
	f.close()
	f = open(self.warnfile,'ab')
        f.write('ERROR: File list has become empty!\n')
	f.write('Time: '+str(datetime.today())+'\n\n')
	f.close()
	if (self.guiMessages): print "GUI_ERROR: File list has become empty!  Aborting!"	
	sys.exit()
      if (self.params['DO_RECTIFY'].lower() == "yes"):
        print "Performing rectification..."
        f = open(self.logfile,'ab')
        f.write("Performing rectification...\n")
        f.close()
	self.rectifyLongslit(1)
	self.doRectifyMOS()
        self.oldpfix = self.prefix
	self.prefix = 'rectified/rct_'
      elif (os.access('rectified/rct_'+self.filenames[0],os.F_OK)):
	self.oldprefix = self.prefix
	self.prefix = 'rectified/rct_'
	self.slitMask = ['junk']*len(self.filenames)
	self.skyregfile = [self.params['MOS_REGION_FILE']]*len(self.filenames)
	self.cleanNMs = ['']*len(self.filenames)
      if (self.guiMessages): print "PROGRESS: " + str(int(a.guiCRdone+a.guiCRf*64+0.1))
      if (self.params['DO_DOUBLE_SUBTRACT'].lower() == "yes"):
	shifts = self.findShiftsForDS(useOnlyPos)
	dsfiles = self.doubleSubtract(useOnlyPos, shifts, 1)
	self.prefix = 'doubleSubtracted/ds_'
      else: dsfiles = self.filenames
      if (self.guiMessages): print "PROGRESS: " + str(int(a.guiCRdone+a.guiCRf*66+0.1))
      if (self.params['DO_SHIFT_ADD'].lower() == "yes"):
	offsets = self.findOffsetsForSA(dsfiles)
	safiles = self.shiftAdd(dsfiles, offsets, 1)
      else:
	safiles = dsfiles
	for l in range(len(safiles)):
	   tempfile = safiles[l].replace(self.prefix,'')
	   if (self.prefix == ''):
	      cleanfile = 'cleanFiles/clean_'+tempfile
	      if (not os.access('cleanFiles', os.F_OK)):
		os.mkdir('cleanFiles',0755)
	   else:
	      cleanfile = self.prefix[:self.prefix.find('/')+1]+'clean_'+tempfile
	   self.cleanFrames.append(cleanfile)
	   if (os.access(cleanfile, os.F_OK)): os.unlink(cleanfile)
	   if (not os.access(self.prefix+tempfile, os.F_OK) and os.access(tempfile, os.F_OK)):
	      self.prefix = ''
	   temp = pyfits.open(self.prefix+tempfile)
	   temp.writeto(cleanfile)
	   temp.close()
      #if (self.params['DO_EXTRACT_SPECTRA'].lower() == "yes"):
	#spectra = self.findSpectra(safiles)
      if (self.guiMessages): print "PROGRESS: " + str(int(a.guiCRdone+a.guiCRf*68+0.1))
      self.filenames = ssfiles
      self.oldpfix = self.prefix
      self.prefix = ssprefix
      #Check for empty file list
      if (len(self.filenames) == 0):
        print "ERROR: File list has become empty!"
        f = open(self.logfile,'ab')
        f.write('ERROR: File list has become empty!\n')
        f.close()
        f = open(self.warnfile,'ab')
        f.write('ERROR: File list has become empty!\n')
        f.write('Time: '+str(datetime.today())+'\n\n')
        f.close()
        if (self.guiMessages): print "GUI_ERROR: File list has become empty!  Aborting!"
        sys.exit()
      if (self.params['DO_FLAT_DIVIDE'].lower() == "yes"):
        self.flatDivide()
      elif (os.access("flatDivided/fd_"+self.filenames[0], os.F_OK)):
	filesExist = True
	for j in self.filenames:
	   if (not os.access("flatDivided/fd_"+j, os.F_OK)):
	      filesExist = False
        if (filesExist): self.prefix = 'flatDivided/fd_'
      if (self.guiMessages): print "PROGRESS: " + str(int(a.guiCRdone+a.guiCRf*72+0.1))
      self.guiCount = 0.0
      if (self.params['DO_RECTIFY'].lower() == "yes"):
        print "Performing rectification..."
        f = open(self.logfile,'ab')
        f.write("Performing rectification...\n")
        f.close()
	self.rectifyLongslit(2)
	self.rectifyMOS()
        self.oldpfix = self.prefix
	self.prefix = 'rectified/rct_'
	if (self.params['CLEAN_UP'].lower() == 'yes'):
	   for j in self.filenames:
	      if (os.access('skySubtracted/ss_'+j, os.F_OK)):
		os.unlink('skySubtracted/ss_'+j)
              if (os.access('flatDivided/fd_'+j, os.F_OK)):
		os.unlink('flatDivided/fd_'+j)
              if (os.access('skySubtracted/ss_NM_'+j, os.F_OK)):
                os.unlink('skySubtracted/ss_NM_'+j)
              if (os.access('flatDivided/fd_NM_'+j, os.F_OK)):
                os.unlink('flatDivided/fd_NM_'+j)
      elif (os.access('rectified/rct_'+self.filenames[0],os.F_OK)):
        self.oldprefix = self.prefix
        self.prefix = 'rectified/rct_'
      if (self.guiMessages): print "PROGRESS: " + str(int(a.guiCRdone+a.guiCRf*82+0.1))
      if (self.params['DO_DOUBLE_SUBTRACT'].lower() == "yes"):
        dsfiles = self.doubleSubtract(useOnlyPos, shifts, 2)
        if (self.params['CLEAN_UP'].lower() == 'yes'):
           for j in self.filenames:
              if (os.access('rectified/rct_'+j, os.F_OK)):
                os.unlink('rectified/rct_'+j)
              if (os.access('rectified/rct_NM_'+j, os.F_OK)):
                os.unlink('rectified/rct_NM_'+j)
        self.oldpfix = self.prefix
	self.prefix = 'doubleSubtracted/ds_'
        self.filenames = dsfiles
      elif (os.access('doubleSubtracted/ds_'+self.filenames[0],os.F_OK)):
	self.oldpfix = self.prefix
        self.prefix = 'doubleSubtracted/ds_'
      if (self.guiMessages): print "PROGRESS: " + str(int(a.guiCRdone+a.guiCRf*84+0.1))
      #Check for empty file list
      if (len(self.filenames) == 0):
        print "ERROR: File list has become empty!"
        f = open(self.logfile,'ab')
        f.write('ERROR: File list has become empty!\n')
        f.close()
        f = open(self.warnfile,'ab')
        f.write('ERROR: File list has become empty!\n')
        f.write('Time: '+str(datetime.today())+'\n\n')
        f.close()
        if (self.guiMessages): print "GUI_ERROR: File list has become empty!  Aborting!"
        sys.exit()
      if (self.params['DO_SHIFT_ADD'].lower() == "yes"):
        safiles = self.shiftAdd(dsfiles, offsets, 2)
        if (self.params['CLEAN_UP'].lower() == 'yes'):
           for j in self.filenames:
              if (os.access('doubleSubtracted/ds_'+j, os.F_OK)):
                os.unlink('doubleSubtracted/ds_'+j)
              if (os.access('doubleSubtracted/ds_NM_'+j, os.F_OK)):
                os.unlink('doubleSubtracted/ds_NM_'+j)
        self.oldpfix = self.prefix
	self.prefix = 'shiftedAdded/sa_'
	self.filenames = safiles
      else:
	temp = self.filenames[0].replace(self.prefix,'')
	if (os.access('shiftedAdded/sa_'+temp, os.F_OK)):
	   self.oldpfix = self.prefix
	   self.prefix = 'shiftedAdded/sa_'
      #Check for empty file list
      if (len(self.filenames) == 0):
        print "ERROR: File list has become empty!"
        f = open(self.logfile,'ab')
        f.write('ERROR: File list has become empty!\n')
        f.close()
        f = open(self.warnfile,'ab')
        f.write('ERROR: File list has become empty!\n')
        f.write('Time: '+str(datetime.today())+'\n\n')
        f.close()
        if (self.guiMessages): print "GUI_ERROR: File list has become empty!  Aborting!"
        sys.exit()
      for j in range(len(self.filenames)):
	self.filenames[j] = self.filenames[j].replace(self.prefix,'')
      #if (self.params['DO_EXTRACT_SPECTRA'].lower() == "yes"):
	#print self.prefix
	#print self.filenames
        #print spectra

   def rectifyLongslit(self, npass):
      if (self.guiMessages): print "STATUS: Rectifying Longslit Data: Pass "+str(npass)
      if (not os.access("rectified",os.F_OK)):
        os.mkdir("rectified",0755)
      coeffs = self.findRectCoeffs(self.filenames, self.params['LONGSLIT_RECT_COEFFS'])
      if (npass == 1):
	self.overwriteRectLS = []
	self.cleanNMs = ['']*len(self.filenames)
        cflats = []
	cNMs = []
	clamps = []
      types = self.findTypes(self.filenames, self.params['DATA_TYPE'])
      for j in range(len(self.filenames)):
	if (types[j] != 'longslit'):
	   if (npass == 1):
	      self.overwriteRectLS.append(False)
              b = where(numarray.strings.array(cflats) == self.flatnames[j])
              if (len(b[0]) == 0):
                cflats.append(self.flatnames[j])
		cNMs.append('rectified/clean_sky_'+self.filenames[j])
                #self.cleanNMs.append('rectified/clean_sky_'+self.filenames[j])
	   continue
	rctfile = 'rectified/rct_'+self.filenames[j]
	temp = pyfits.open(self.prefix+self.filenames[j])
	xsize = temp[0].data.shape[1]
	ysize = temp[0].data.shape[0]
	temp.close()
        if (os.access(rctfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(rctfile)
        if (npass == 2 and self.overwriteRectLS[j] == True and os.access(rctfile, os.F_OK)): os.unlink(rctfile)
        if (not os.access(rctfile, os.F_OK)):
           if (npass == 2 and self.overwriteRectLS[j] == False): continue
           if (npass == 1): self.overwriteRectLS.append(True)
	   #temp = pyfits.open(self.prefix+self.filenames[j])
	   #temp[0].data = drihizzle(temp[0].data, coeffs)
	   iraf.unlearn('drizzle')
	   iraf.stsdas.analysis.dither.drizzle(self.prefix+self.filenames[j], rctfile, wt_scl='exptime', outnx = xsize+10, outny = ysize+10, expkey=self.params['EXPTIME_KEYWORD'], pixfrac=0.01, kernel="point", coeffs=coeffs[j])
	   if (npass == 1):
	      #Create clean-sky image
	      b = where(numarray.strings.array(cflats) == self.flatnames[j])
	      if (len(b[0]) == 0):
		cflats.append(self.flatnames[j])
	        cNMs.append('rectified/clean_sky_'+self.filenames[j])
		self.cleanNMs[j] = 'rectified/clean_sky_'+self.filenames[j]
                if (os.access('rectified/clean_sky_'+self.filenames[j], os.F_OK)):
		   os.unlink('rectified/clean_sky_'+self.filenames[j])
		#pfix = self.filenames[j][:self.filenames[j].find('.')]
        	pfix = self.filenames[j][:self.filenames[j].rfind(self.delim,0,self.filenames[j].rfind('.'))]
                medfile = 'darkSubtracted/ds_'+pfix+'_med.fits'
        	if (not os.access(medfile, os.F_OK)):
		   #Use IRAF's imcombine to combine darks
           	   iraf.unlearn('imcombine')
           	   #imcombine can't handle strings longer than 1024 bytes
		   filelist = glob.glob('darkSubtracted/ds_'+pfix+'.*')
		   tmpFile = open("tmpIRAFFile.dat", "wb")
                   for l in range(len(filelist)):
		      tmpFile.write(filelist[l]+'\n')
                   tmpFile.close()
                   iraf.images.immatch.imcombine("@tmpIRAFFile.dat", medfile, combine="median")
                   os.unlink("tmpIRAFFile.dat")
		dsfile = medfile
		if (os.access(dsfile, os.F_OK)):
		   iraf.stsdas.analysis.dither.drizzle(dsfile, 'rectified/clean_sky_'+self.filenames[j], wt_scl='exptime', outnx = xsize+10, outny = ysize+10, expkey=self.params['EXPTIME_KEYWORD'], pixfrac=1, kernel="square", coeffs=coeffs[j])
                #self.cleanNMs.append('rectified/clean_sky_'+self.filenames[j])
	      else:
		self.cleanNMs[j] = cNMs[b[0][0]]
	      #Rectify master arc lamp
	      if (self.lampnames[j] != ''): newlfile = 'rectified/'+self.lampnames[j][self.lampnames[j].rfind('/')+1:]
	      else: newlfile = ''
	      b = where(numarray.strings.array(clamps) == newlfile)
	      if (len(b[0]) == 0 and os.access(self.lampnames[j], os.F_OK)):
		clamps.append(newlfile)
		oldlfile = self.lampnames[j]
		if (os.access(newlfile, os.F_OK)):
		   os.unlink(newlfile)
		iraf.stsdas.analysis.dither.drizzle(oldlfile, newlfile, wt_scl='exptime', outnx = xsize+10, outny = ysize+10, expkey=self.params['EXPTIME_KEYWORD'], pixfrac=1, kernel="square", coeffs=coeffs[j])
		for l in range(len(self.lampnames)):
		   if (self.lampnames[l] == oldlfile):
		      self.lampnames[l] = newlfile
           if (self.params['DO_NOISEMAPS'].lower() == "yes"):
              nmfile = "rectified/rct_NM_"+self.filenames[j]
              if (npass == 2 and self.overwriteRectLS[j] == True and os.access(nmfile,os.F_OK)): os.unlink(nmfile)
              if (os.access(nmfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(nmfile)
              if (not os.access(nmfile, os.F_OK)):
		iraf.stsdas.analysis.dither.drizzle(self.prefix+'NM_'+self.filenames[j], nmfile, wt_scl='exptime', outnx = xsize+10, outny = ysize+10, expkey=self.params['EXPTIME_KEYWORD'], pixfrac=0.01, kernel="point", coeffs=coeffs[j])
	   #temp.writeto(rctfile)
	   #temp.close()
        else:
	   self.overwriteRectLS.append(False)
	   if (npass == 1 and os.access('rectified/clean_sky_'+self.filenames[j], os.F_OK)):
	      self.cleanNMs[j] = 'rectified/clean_sky_'+self.filenames[j]
	self.guiCount+=1. 
	if (self.guiMessages):
	   if (npass == 1): print "PROGRESS: "+str(int(self.guiCRdone+self.guiCRf*(42+self.guiCount/(len(self.filenames)+12)*22)))
	   else: print "PROGRESS: "+str(int(self.guiCRdone+self.guiCRf*(72++self.guiCount/len(self.filenames)*10)))
	   print "FILE: "+rctfile
	   if (self.params['DO_NOISEMAPS'].lower() == "yes"):
              nmfile = "rectified/rct_NM_"+self.filenames[j]
	      print "FILE: "+nmfile
	   if (os.access('rectified/clean_sky_'+self.filenames[j], os.F_OK)):
	      print "FILE: "+'rectified/clean_sky_'+self.filenames[j]

   def doRectifyMOS(self):
      if (self.guiMessages): print "STATUS: Rectifying MOS Data: Pass 1"
      if (not os.access("rectified",os.F_OK)):
        os.mkdir("rectified",0755)
      dbfile = self.params['MOS_RECT_DB']
      ybuflo = self.params['MOS_YSLIT_BUFFER_LOW']
      ybufhi = self.params['MOS_YSLIT_BUFFER_HIGH']
      #Read in MOS rect database, fit with 3d surface to get coeffs
      order = self.params['MOS_RECT_ORDER']
      if (os.access(dbfile, os.F_OK)):
	f = open(dbfile,'rb')
        s = f.read().split('\n')
        f.close()
        try: s.remove('')
        except ValueError:
           junk = ''
        xin = []
        yin = []
        yout = []
        xslitin = []
        for j in range(len(s)):
	   temp = s[j].split()
           xin.append(float(temp[0]))
           yin.append(float(temp[1]))
           yout.append(float(temp[2]))
           xslitin.append(float(temp[3]))
        terms = 0
        nterms = 0
        for j in range(order+2):
           nterms+=j
           terms+=nterms
        p = Numeric.zeros(terms)
        p[2] = 1
        xin = array(xin)
        yin = array(yin)
        yout = array(yout)
        xslitin = array(xslitin)
        b = where(xin != 0)
        p = Numeric.array(p.tolist());
        lsq = leastsq(surface3dResiduals, p, args=(xin[b], yin[b], xslitin[b],yout[b], order))
        coeffs = array(lsq[0])
        #Fit inverse function, get coeffs, for determining slit locations
        p = Numeric.array(p.tolist());
        lsq = leastsq(surface3dResiduals, p, args=(xin[b], yout[b], xslitin[b], yin[b], order))
        invcoeffs = array(lsq[0])
      else:
	order = 1
	coeffs = array([0,0,1,0])
	invcoeffs = array([0,0,1,0])
      #Setup arrays
      rfile = self.params['MOS_REGION_FILE']
      tmpflatnames = numarray.strings.array(self.flatnames)
      tmplampnames = numarray.strings.array(self.lampnames)
      n = len(self.filenames)
      ndata = 0
      self.overwriteRectMOS = [False]*n
      #self.cleanNMs = []
      self.regfile = ['junk']*n
      self.skyregfile = ['junk']*n
      self.slitMask = []
      #Group all frames with same flat together
      while (n > 0):
        try:
	   currLamp = tmplampnames[0]
	except Exception:
	   currLamp = ''
	   if (tmpflatnames[0] == '' or tmpflatnames[0] == ' '): 
	      print 'WARNING: There are no flat fields associated with some files.'
              print 'Check your OBJECT keyword or manual flat field list!'
              f = open(self.logfile,'ab')
              f.write('WARNING: There are no flat fields associated with some files.\n')
              f.write('Check your OBJECT keyword or manual flat field list!\n')
              f.close()
              f = open(self.warnfile,'ab')
              f.write('WARNING: There are no flat fields associated with some files.\n')
              f.write('Check your OBJECT keyword or manual flat field list!\n')
              f.write('Time: '+str(datetime.today())+'\n\n')
              f.close()
	   else:
              print 'WARNING: There are no lamps associated with some files.'
              f = open(self.logfile,'ab')
              f.write('WARNING: There are no lamps associated with some files.\n')
              f.close()
              f = open(self.warnfile,'ab')
              f.write('WARNING: There are no lamps associated with some files.\n')
              f.write('Time: '+str(datetime.today())+'\n\n')
              f.close()
        currFlat = tmpflatnames[0]
        tmplampnames = tmplampnames[where(tmpflatnames != currFlat)]
	tmpflatnames = tmpflatnames[where(tmpflatnames != currFlat)]
	frames = []
	keepFrame = []
	frameNum = []
	ndata+=1
	for j in range(len(self.filenames)):
	   if (self.flatnames[j] == currFlat):
	      frames.append(self.filenames[j])
	      keepFrame.append(True)
	      frameNum.append(j)
	      n-=1
	types = self.findTypes(frames, self.params['DATA_TYPE'])
	for j in range(len(frames)-1, -1, -1):
	   if (types[j] != 'mos' and types[j] != 'ifu'):
	      frames.pop(j)
	      types.pop(j)
	      keepFrame.pop(j)
	      frameNum.pop(j) 
	      self.slitMask.append('junk')
	if (len(frames) == 0):
	   #self.regfile.append('junk')
	   #self.skyregfile.append('junk')
	   #self.slitMask.append('junk')
	   continue
        frames = numarray.strings.array(frames)
	for j in range(len(frames)):
	   newrfile = rfile[:rfile.rfind('.reg')]+'_rect.reg'
	   self.regfile[frameNum[j]] = newrfile
	   self.skyregfile[frameNum[j]] = newrfile
	   rctfile = 'rectified/rct_'+frames[j]
	   if (os.access(rctfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"):
	      os.unlink(rctfile)
	   if (os.access(rctfile, os.F_OK)):
	      keepFrame[j] = False
              newlfile = 'rectified/'+currLamp[currLamp.rfind('/')+1:]
	      if (newlfile == 'rectified/'):
		newlfile = ''
	      if (os.access(newlfile, os.F_OK) and self.lampnames[frameNum[j]] == currLamp):
		self.lampnames[frameNum[j]] = newlfile
              if (os.access('rectified/clean_sky_'+frames[0], os.F_OK)):
		self.cleanNMs[frameNum[j]] = 'rectified/clean_sky_'+frames[0]
	      if (self.guiMessages):
		print "FILE: "+rctfile
	        if (os.access(newlfile, os.F_OK)):
		   print "FILE: "+newlfile
		if (os.access("rectified/clean_sky_"+frames[0], os.F_OK)):
		   print "FILE: rectified/clean_sky_"+frames[0]
		if (os.access("rectified/slitmask_"+frames[0], os.F_OK)):
		   print "FILE: rectified/slitmask_"+frames[0]
		if (os.access("rectified/NM_"+frames[j], os.F_OK)):
		   print "FILE: rectified/NM_"+frames[j]
	   else:
	      self.overwriteRectMOS[frameNum[j]] = True
	   self.slitMask.append('rectified/slitmask_'+frames[0])
	b = where(keepFrame)
	if (len(b[0]) == 0):
	   #newrfile = rfile[:rfile.rfind('.reg')]+'_rect.reg'
	   #self.regfile.append(newrfile)
	   #self.skyregfile.append(newrfile)
	   #self.cleanNMs.append('rectified/clean_sky_'+frames[0])
	   continue
	frames = frames[b]
	#Read flat field and region file
	try:
	   flat = pyfits.open(currFlat)
	except Exception:
           print 'No flat field found!  Unable to process object '+self.filenames[j]+'!'
           f = open(self.logfile,'ab')
           f.write('No flat field found!  Unable to process object '+self.filenames[j]+'!\n')
           f.close()
           f = open(self.warnfile,'ab')
           f.write('WARNING: No flat field found!  Unable to process object '+self.filenames[j]+'!\n')
           f.write('Time: '+str(datetime.today())+'\n\n')
           f.close()
           continue
	xsize = flat[0].data.shape[1]
	ysize = flat[0].data.shape[0]
	flat.close()
	slitx = []
	slity = []
	slitw = []
	slith = []
	if (not os.access(rfile, os.F_OK)):
	   print 'No region file found!  Unable to process object '+self.filenames[j]+'!'
           f = open(self.logfile,'ab')
           f.write('No region file found!  Unable to process object '+self.filenames[j]+'!\n')
           f.close()
           f = open(self.warnfile,'ab')
           f.write('WARNING: No region file found!  Unable to process object '+self.filenames[j]+'!\n')
           f.write('Time: '+str(datetime.today())+'\n\n')
           f.close()
           continue
	f = open(rfile,'rb')
	s = f.read().split('\n')
	f.close()
	try: s.remove('')
        except ValueError:
           junk = ''
	for l in range(len(s)):
	   if (s[l].startswith("image;box") or s[l].startswith("box(")):
	      temp=s[l][s[l].find('(')+1:]
	      temp = temp.split(',')
              if (temp[-1].find(')') != -1):
                temp[-1] = temp[-1][:temp[-1].find(')')]
	      slitx.append(float(temp[0])-1)
	      slity.append(float(temp[1])-1)
	      slitw.append(float(temp[2]))
	      slith.append(float(temp[3]))
	#Deal with non-sequential region file entries by sorting
	sorted = array(slity).argsort()
	slitx = array(slitx)[sorted].tolist()
	slity = array(slity)[sorted].tolist()
	slitw = array(slitw)[sorted].tolist()
	slith = array(slith)[sorted].tolist()
	#Cross-correlate region file with flat to determine offset 
	islit = zeros(ysize)+0.
	for l in range(len(slity)):
	   ylo = int(slity[l]-slith[l]/2)
	   yhi = int(slity[l]+slith[l]/2)
	   islit[ylo:yhi] = 5000
	   #if (slitw[l] > self.params['MOS_MAX_SLIT_WIDTH']):
	      #islit[ylo:yhi] += 5000
        cut1d = arraymedian(flat[0].data, axis="X") 
	#ccor = scipy.signal.correlate(cut1d, islit, mode='same')
        ccor = conv.correlate(cut1d, islit, mode='same')
	ccor = array(ccor)
        mcor = where(ccor == max(ccor))[0][0]
	yshift = len(ccor)/2-mcor
	#Setup array for finding slits
	yind = arange(xsize*ysize, shape=(ysize,xsize))/xsize+0.
	#xind = arange(xsize*ysize, shape=(ysize,xsize))%xsize+0.
	xind = arange(xsize)+0.
	z = zeros((ysize,xsize))+0.
	if (ybuflo+ybufhi != 0):
	   z2 = zeros((ysize,xsize))+0.
	   yout2 = zeros((ysize,xsize))+0.
	print "   Finding Slits..."
        f = open(self.logfile,'ab')
        f.write("\tFinding Slits...\n")
        f.close()
	ylos = []
	yhis = []
        yout = zeros((ysize,xsize))+0.
        rylo = zeros(len(slitx))+1.
        ryhi = zeros(len(slitx))+1.
	for l in range(len(slitx)):
	   #Create a mask for each slit
	   ylo = slity[l]-slith[l]/2-yshift-ybuflo
	   yhi = slity[l]+slith[l]/2-yshift+ybufhi
	   yinlo = surface3d(invcoeffs, arange(xsize)+0., ylo, slitx[l], order)
	   yinhi = surface3d(invcoeffs, arange(xsize)+0., yhi, slitx[l], order)
	   if (l < len(slitx)-1):
	      ylonext = slity[l+1]-slith[l+1]/2-yshift-ybuflo
	      yinlonext = surface3d(invcoeffs, arange(xsize)+0., ylonext, slitx[l+1], order)
	      yinlonext += ylonext-yinlonext[int(slitx[l+1])]
	   yinlo += ylo-yinlo[int(slitx[l])]
	   yinhi += yhi-yinhi[int(slitx[l])]
	   print ylo,yhi
           f = open(self.logfile,'ab')
           f.write("\tSlit found: "+str(ylo)+"\t"+str(yhi)+"\n")
           f.close()
	   ylos.append(ylo)
	   yhis.append(yhi)
	   if (ybuflo+ybufhi == 0):
	      b = where(yind >= yinlo, 1, 0)*where(yind <= yinhi, 1, 0)
	   else:
	      if (l < len(slitx)-1):
		yhitemp = ((yinhi-ybufhi) <= yinlonext)*(yinhi-ybufhi) + (yinlonext < (yinhi-ybufhi))*yinlonext
	      else:
		yhitemp = yinhi-ybufhi
	      b = where(yind >= yinlo, 1, 0)*where(yind <= yinhi-ybufhi, 1, 0)
	      b2 = where(yind > yhitemp, 1, 0)*where(yind <= yinhi, 1, 0)
	      putmask(z2, b2, slitx[l])
	      putmask(yout, b, l*(ybuflo+ybufhi))
	      putmask(yout2, b2, l*(ybuflo+ybufhi))
	      rylo[l] += l*(ybuflo+ybufhi)
	      ryhi[l] += l*(ybuflo+ybufhi)
	      b2 = 0.
	   putmask(z, b, slitx[l])
	   b = 0.
	if (ndata == 1): self.guiCount+=3
	self.guiCount = min(self.guiCount, len(self.filenames)+12)
        if (self.guiMessages): print "PROGRESS: "+str(int(self.guiCRdone+self.guiCRf*(42+self.guiCount/(len(self.filenames)+12)*22)))
	#Calculate transform
	print "   Calculating transformation..."
        f = open(self.logfile,'ab')
        f.write("\tCalculating transformation...\n")
        f.close()
	ylos = array(ylos)
	yhis = array(yhis)
	i = 0
	if (ybuflo+ybufhi == 0):
	   for j in range(order+1):
	      for l in range(1,j+2):
		for k in range(1,l+1):
		   yout+=coeffs[i]*xind**(j-l+1)*yind**(l-k)*z**(k-1)
		   rylo+=coeffs[i]*array(slitx)**(j-l+1)*ylos**(l-k)*array(slitx)**(k-1)
		   ryhi+=coeffs[i]*array(slitx)**(j-l+1)*yhis**(l-k)*array(slitx)**(k-1)
		   i+=1
	else:
	   for j in range(order+1):
	      for l in range(1,j+2):
		for k in range(1,l+1):
                   yout+=coeffs[i]*xind**(j-l+1)*yind**(l-k)*z**(k-1)
		   yout2+=coeffs[i]*xind**(j-l+1)*yind**(l-k)*z2**(k-1)
		   rylo+=coeffs[i]*array(slitx)**(j-l+1)*ylos**(l-k)*array(slitx)**(k-1)
		   ryhi+=coeffs[i]*array(slitx)**(j-l+1)*yhis**(l-k)*array(slitx)**(k-1)
		   i+=1
	xind = 0
	yind = 0
        mask = where(z != 0, 1, 0)
	yout *= mask
	z = 0.
	if (ybuflo+ybufhi != 0):
	   mask2 = where(z2 != 0, 1, 0)
	   yout2 *= mask2
	   z2 = 0.
        if (ndata == 1): self.guiCount+=3
        self.guiCount = min(self.guiCount, len(self.filenames)+12)
        if (self.guiMessages): print "PROGRESS: "+str(int(self.guiCRdone+self.guiCRf*(42+self.guiCount/(len(self.filenames)+12)*22)))
	#Save yout to temp fits file
        if (os.access('TEMPyoutMOS'+frames[0], os.F_OK)):
           os.unlink('TEMPyoutMOS'+frames[0])
        fitsobj = pyfits.HDUList()
        hdu = pyfits.PrimaryHDU()
        hdu.data = yout
        fitsobj.append(hdu)
        fitsobj.verify('silentfix')
        fitsobj.writeto('TEMPyoutMOS'+frames[0])
	if (ybuflo+ybufhi != 0):
	   if (os.access('TEMPyout2MOS'+frames[0], os.F_OK)):
	      os.unlink('TEMPyout2MOS'+frames[0])
	   fitsobj[0].data = yout2
	   fitsobj.writeto('TEMPyout2MOS'+frames[0])
	#fitsobj.close()
	#fitsobj = 0.
	#Setup parameters for drihizzle
        ymin = int(yout.min())
        ymax = int(yout.max())+2
	rylo -= ymin
	ryhi -= ymin
	rylo = rylo*(rylo > 0)
	b = where(array(slitw) < self.params['MOS_MAX_SLIT_WIDTH'])
        #Output new region file and slit mask
        newrfile = rfile[:rfile.rfind('.reg')]+'_rect.reg'
        f = open(newrfile,'wb')
        f.write('# Region file format: DS9 version 3.0\n')
        f.write('global color=green select=1 edit=1 move=1 delete=1 include=1 fixed=0\n')
        for j in range(len(rylo[b[0]])):
	   if (ryhi[b[0][j]] > 0):
	      f.write('image;box('+str(slitx[b[0][j]])+','+str((rylo[b[0][j]]+ryhi[b[0][j]])/2)+','+str(slitw[b[0][j]])+','+str(ryhi[b[0][j]]-rylo[b[0][j]])+')\n')
	f.close()
	#self.regfile.append(newrfile)
        #self.skyregfile.append(newrfile)
	rylo = rylo[b].astype("Int32")
	ryhi = ryhi[b].astype("Int32")
	bpos = where(ryhi > 0)
	rylo = rylo[bpos]
	ryhi = ryhi[bpos]
	intpart = floor(yout-ymin).astype("Int32")
	frac = yout-ymin-intpart
	yout = 0.
	if (ybuflo+ybufhi != 0):
	   ymin = int(min(ymin, yout2.min()))
	   ymax = int(max(ymax-2, yout2.max()))+2
	   intpart2 = floor(yout2-ymin).astype("Int32")
	   frac2 = yout2-ymin-intpart2
	#Drihizzle3d
	for j in range(len(frames)):
	   print "   Rectifying... "+frames[j]
           f = open(self.logfile,'ab')
           f.write("\tRectifying... "+frames[j]+"\n")
           f.close()
           rctfile = 'rectified/rct_'+frames[j]
	   image = pyfits.open(self.prefix+frames[j])
	   if (ybuflo+ybufhi == 0):
	      image[0].data = drihizzle3d(image[0].data, ymax-ymin, intpart, frac, mask = mask)
	   else:
	      temp = drihizzle3d(image[0].data, ymax-ymin, intpart, frac, mask = mask)
	      image[0].data = temp+drihizzle3d(image[0].data, ymax-ymin, intpart2, frac2, mask=mask2)
	      temp = 0.
	   image.writeto(rctfile)
	   image.close()
           if (self.params['DO_NOISEMAPS'].lower() == "yes"):
              nmfile = "rectified/rct_NM_"+frames[j]
              if (os.access(nmfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(nmfile)
              if (not os.access(nmfile, os.F_OK)):
		nm = pyfits.open(self.prefix+'NM_'+frames[j])
		if (ybuflo+ybufhi == 0):
		   nm[0].data = drihizzle3d(nm[0].data, ymax-ymin, intpart, frac, mask = mask)
		else:
		   temp = drihizzle3d(nm[0].data, ymax-ymin, intpart, frac, mask = mask)
		   nm[0].data = temp+drihizzle3d(nm[0].data, ymax-ymin, intpart2, frac2, mask = mask2)
		   temp = 0.
		nm.writeto(nmfile)
		nm.close()
	   self.guiCount+=0.5
           self.guiCount = min(self.guiCount, len(self.filenames)+12)
           if (self.guiMessages): print "PROGRESS: "+str(int(self.guiCRdone+self.guiCRf*(42+self.guiCount/(len(self.filenames)+12)*22)))
	f.close()
	#Select image to use for sky lines
        print "   Creating and rectifying clean sky image..."
        f = open(self.logfile,'ab')
        f.write("\tCreating and rectifying clean sky image...\n")
        f.close()
        #pfix = frames[0][:frames[0].find('.')]
        pfix = frames[0][:frames[0].rfind(self.delim,0,frames[0].rfind('.'))]
        medfile = 'darkSubtracted/ds_'+pfix+'_med.fits'
        if (not os.access(medfile, os.F_OK)):
           #Use IRAF's imcombine to combine darks
           iraf.unlearn('imcombine')
           #imcombine can't handle strings longer than 1024 bytes
           filelist = glob.glob('darkSubtracted/ds_'+pfix+'.*')
	   if (len(filelist) == 0):
	      print "No dark subtracted frames found for rectifying skylines!"
	      f = open(self.logfile,'ab')
	      f.write('No dark subtracted frames found for rectifying skylines!\n')
	      f.close()
	      f = open(self.warnfile,'ab')
              f.write('No dark subtracted frames found for rectifying skylines!\n')
	      f.write('Object: '+pfix+'\n')
	      f.write('Time: '+str(datetime.today())+'\n\n')
              f.close()
	      continue 
           tmpFile = open("tmpIRAFFile.dat", "wb")
           for l in range(len(filelist)):
              tmpFile.write(filelist[l]+'\n')
           tmpFile.close()
           iraf.images.immatch.imcombine("@tmpIRAFFile.dat", medfile, combine="median")
           os.unlink("tmpIRAFFile.dat")
	dsfile = medfile
        sky = pyfits.open(dsfile)
	if (ybuflo+ybufhi == 0):
           sky[0].data = drihizzle3d(sky[0].data, ymax-ymin, intpart, frac, mask = mask)
	else:
	   temp = drihizzle3d(sky[0].data, ymax-ymin, intpart, frac, mask = mask)
	   sky[0].data = temp+drihizzle3d(sky[0].data, ymax-ymin, intpart2, frac2, mask = mask2)
        if (os.access('rectified/clean_sky_'+frames[0], os.F_OK)):
           os.unlink('rectified/clean_sky_'+frames[0])
        sky.writeto('rectified/clean_sky_'+frames[0])
	sky.close()
	medfile = 'rectified/clean_sky_'+frames[0]
	#Rectify master arc lamp
	newlfile = 'rectified/'+currLamp[currLamp.rfind('/')+1:]
	if (newlfile == 'rectified/'):
	   newlfile = ''
        if (os.access(currLamp, os.F_OK)):
           print "   Rectifying master arc lamp..."
           f = open(self.logfile,'ab')
           f.write("\tRectifying master arc lamp...\n")
           f.close()
	   if (os.access(newlfile, os.F_OK)):
	      os.unlink(newlfile)
	   lamp = pyfits.open(currLamp)
	   if (ybuflo+ybufhi == 0):
	      lamp[0].data = drihizzle3d(lamp[0].data, ymax-ymin, intpart, frac, mask = mask)
           else:
              temp = drihizzle3d(lamp[0].data, ymax-ymin, intpart, frac, mask = mask)
              lamp[0].data = temp+drihizzle3d(lamp[0].data, ymax-ymin, intpart2, frac2, mask = mask2)
	   lamp.writeto(newlfile)
	   lamp.close()
	   for l in range(len(self.lampnames)):
	      if (self.lampnames[l] == currLamp):
		self.lampnames[l] = newlfile                              

        #Free memory
        intpart = 0
        frac = 0
        mask = 0
        intpart2 = 0
        frac2 = 0
        mask2 = 0
	fits = []
        if (ndata == 1): self.guiCount+=3
        self.guiCount = min(self.guiCount, len(self.filenames)+12)
        if (self.guiMessages): print "PROGRESS: "+str(int(self.guiCRdone+self.guiCRf*(42+self.guiCount/(len(self.filenames)+12)*22)))

	#Choose sky or lamp
        if (self.params['MOS_RECT_USE_ARCLAMPS'].lower() == "yes" and os.access(newlfile, os.F_OK)):
           dsfile = newlfile
           usingArcLamps = True
           print "Using ARCLAMP " + newlfile 
           f = open(self.logfile,'ab')
           f.write('Using ARCLAMP ' +newlfile+'\n')
           f.close()  
        else:
           dsfile = medfile
           usingArcLamps = False
           print "Using SKY " + medfile
           f = open(self.logfile,'ab')
           f.write('Using SKY ' +medfile+'\n')
           f.close()
	sky = pyfits.open(dsfile, "update")
	xout = sky[0].data*0.
	ysize = xout.shape[0]
	xsize = xout.shape[1]
        yind = arange(xsize*ysize, shape=(ysize,xsize))/xsize+0.
        xind = arange(xsize*ysize, shape=(ysize,xsize))%xsize+0.
	#Find sky lines
	print "   Finding sky lines..."
        f = open(self.logfile,'ab')
        f.write("\tFinding sky lines...\n")
        f.close()
	for j in range(len(rylo)):
	   #x = sum(sky[0].data[rylo[j]:ryhi[j],:], 0)
	   x = arraymedian(sky[0].data[rylo[j]:ryhi[j]+1,:],axis="Y")
	   z = smooth1d(x,7,1)
	   med = arraymedian(z[where(z != 0)])
	   sig = z[where(z != 0)].stddev()
	   z[0:15] = 0
	   z[-15:] = 0
	   xcenters = []
	   while (max(z) > med+self.params['MOS_SKYLINE_SIGMA']*sig):
	      b = (where(z == max(z)))[0][0]
	      if (z[b] > med+self.params['MOS_SKYLINE_SIGMA']*sig):
		valid = True
		for l in range(b-5,b+6):
		   if (z[l] == 0): valid = False
		sigpts = concatenate((arange(max(0,b-50),b-5),arange(b+6,min(len(z),b+50))))
		if (len(where(z[sigpts] != 0)[0]) > 1):
                   lsigma = (z[b]-arraymedian(z[sigpts]))/z[sigpts].stddev()
		else:
		   lsigma = 0
		if (lsigma < self.params['MOS_SKYLINE_SIGMA']):
		   valid = False
		if valid: xcenters.append(b)
		z[b-15:b+15] = 0
		if (len(where(z != 0)[0]) <= 1):
		   break
		med = arraymedian(z[where(z != 0)])
		sig = z[where(z != 0)].stddev()
	   xs = []
	   if (self.params['MOS_SKY_TWO_PASS'].lower() == 'yes'):
	      for k in range(100, xsize-99, (xsize-200)/4):
		xs.append(k)
	   for k in range(len(xs)-1):
	      az = z[xs[k]:xs[k+1]]
	      az[0:15] = 0
	      az[-15:] = 0
	      if (len(where(az != 0)[0]) <= 1): continue
	      med = arraymedian(az[where(az != 0)])
	      sig = az[where(az != 0)].stddev()
	      firstPass = True
	      while max(az) > med+self.params['MOS_SKYLINE_SIGMA']*sig or firstPass:
		b = (where(az == max(az)))[0][0]
		if (az[b] > med+self.params['MOS_SKYLINE_SIGMA']*sig or firstPass):
		   valid = True
		   for l in range(b-5,b+6):
		      if (az[l] == 0): valid = False
		   firstPass = False
		   if valid:
		      xcenters.append(b+xs[k])
		   az[b-15:b+15] = 0
		   if (len(where(az != 0)[0]) <= 1): break
		   med = arraymedian(az[where(az != 0)])
		   sig = az[where(az != 0)].stddev()
	   xboxsize = []
	   for l in range(len(xcenters)):
	      xboxsize.append(self.params['MOS_SKY_BOX_SIZE'])
	   xcenters = array(xcenters)
	   xboxsize = array(xboxsize)
	   ys = [int((rylo[j]+ryhi[j])/2)]
	   for l in range(ys[0]-5, rylo[j]+ybuflo, -5):
	      ys.append(l)
	   for l in range(ys[0]+5, ryhi[j]-ybufhi, 5):
	      ys.append(l)
	   rxin = zeros((len(ys), len(xcenters)),"Float64")
	   ryin = zeros((len(ys), len(xcenters)),"Float64")
           rxout = zeros((len(ys), len(xcenters)),"Float64")
	   for l in range(len(xcenters)):
	      currX = xcenters[l]
	      for k in range(len(ys)):
		if (ys[k] == ys[0]+5): currX = xcenters[l]
		xlo = int(currX - xboxsize[l])
		xhi = int(currX + xboxsize[l])
		x = sum(sky[0].data[ys[k]-2:ys[k]+3,:], 0)+0.
		y = arange(len(x))+0.
		outerbox = x[int(xlo-xboxsize[l]):int(xhi+xboxsize[l])]
		noData = False
		if (x[xlo:xhi].mean() <= outerbox.mean()): noData = True
		if (arraymedian(x[xlo:xhi]) <= arraymedian(outerbox)):
		   noData = True
		if (max(smooth1d(x[xlo:xhi],3,1)) < arraymedian(outerbox) + outerbox.stddev()): noData = True
		if (noData == True and k == 0): break
		if (not noData):
		   p = zeros(4)+0.
		   p[0] = max(x[xlo:xhi])
		   p[1] = (where(x[xlo:xhi] == p[0]))[0][0]+xlo
		   p[2] = xboxsize[l]/2
		   p[3] = arraymedian(x)
                   p = Numeric.array(p.tolist());
		   lsq = leastsq(gaussResiduals, p, args=(y[xlo:xhi], x[xlo:xhi]))
		   if (k == 0):
		      ryin[0][l] = ys[0]
		      rxin[0][l] = lsq[0][1]
		      rxout[0][l] = rxin[0][l]
		   else:
		      if (abs(lsq[0][1] - currX) < 2):
			currX = lsq[0][1]
		        ryin[k][l] = ys[k]
		        rxin[k][l] = currX
		        rxout[k][l] = rxin[0][l]
		      elif (ys[k] == ys[0]+5 and abs(lsq[0][1] - rxin[0][l]) < 2):
		        currX = lsq[0][1]
                        ryin[k][l] = ys[k]
                        rxin[k][l] = currX
                        rxout[k][l] = rxin[0][l]
	   skyorder = self.params['MOS_SKY_FIT_ORDER']
	   terms = 0
	   for l in range(skyorder+2):
	      terms+=l
	   p = zeros(terms)
           p[1] = 1
	   rxin = array(rxin)
	   ryin = array(ryin)
	   rxout = array(rxout)
	   rxin = reshape(rxin, rxin.nelements())+0.
	   ryin = reshape(ryin, ryin.nelements())+0.
	   rxout = reshape(rxout, rxout.nelements())+0.
	   b = where(rxin != 0)
	   unique = []
	   for l in range(len(rxout[b])):
	      if (len(where(array(unique) == rxout[b][l])[0]) == 0):
	        unique.append(rxout[b][l])
	   if (len(unique) > 2):
              p = Numeric.array(p.tolist());
	      lsq = leastsq(surfaceResiduals, p, args=(rxin[b], ryin[b], rxout[b], skyorder))
	   else:
	      lsq = [p]
	   i = 0
	   xout[rylo[j]:ryhi[j]+1,:] = 0
	   for l in range(skyorder+1):
	      for k in range(l+1):
		xout[rylo[j]:ryhi[j]+1,:]+=lsq[0][i]*xind[rylo[j]:ryhi[j]+1,:]**(l-k)*yind[rylo[j]:ryhi[j]+1,:]**k
		i+=1
	#Save xout to temp fits file
	if (os.access('TEMPxoutMOS'+frames[0], os.F_OK)):
	   os.unlink('TEMPxoutMOS'+frames[0])
        fitsobj = pyfits.HDUList()
        hdu = pyfits.PrimaryHDU()
        hdu.data = xout
        fitsobj.append(hdu)
        fitsobj.verify('silentfix')
        fitsobj.writeto('TEMPxoutMOS'+frames[0])
        fitsobj.close()
	#Free Memory
	fitsobj = 0
	xind = 0
	yind = 0
        if (ndata == 1): self.guiCount+=3
        self.guiCount = min(self.guiCount, len(self.filenames)+12)
        if (self.guiMessages): print "PROGRESS: "+str(int(self.guiCRdone+self.guiCRf*(42+self.guiCount/(len(self.filenames)+12)*22)))
	#Setup paramaters for drihizzle
        xmin = int(xout.min())
        xmax = int(xout.max())+2
        intpart = floor(xout-xmin).astype("Int32")
        frac = xout-xmin-intpart
        mask = where(xout != 0, 1, 0)
	#Drihizzle3d
	sky[0].data = drihizzle3d(sky[0].data, xmax-xmin, intpart, frac, mask=mask, axis="X")
	sky.flush()
	sky.close()
	sky = pyfits.open(dsfile)
	#if (os.access('rectified/clean_sky_'+frames[0], os.F_OK)):
	   #os.unlink('rectified/clean_sky_'+frames[0])
	#sky.writeto('rectified/clean_sky_'+frames[0])
        sky[0].data[:,:]=0
        for j in range(len(rylo)):
           sky[0].data[rylo[j]:ryhi[j]+1,:] = j+1
	if (os.access('rectified/slitmask_'+frames[0], os.F_OK)):
	  os.unlink('rectified/slitmask_'+frames[0])
        sky.writeto('rectified/slitmask_'+frames[0])
        sky.close()
	if (self.guiMessages):
	   print "FILE: "+dsfile
	   print "FILE: rectified/slitmask_"+frames[0]
        for j in range(len(frames)):
	   self.cleanNMs[frameNum[j]] = 'rectified/clean_sky_'+frames[0]
           print "   Rectifying sky lines for... "+frames[j]
           f = open(self.logfile,'ab')
           f.write("\tRectifying sky lines for... "+frames[j]+"\n")
           f.close()
           rctfile = 'rectified/rct_'+frames[j]
           image = pyfits.open(rctfile,"update")
           image[0].data = drihizzle3d(image[0].data, xmax-xmin, intpart, frac,mask = mask, axis="X")
           image.flush()
           image.close()
           if (self.params['DO_NOISEMAPS'].lower() == "yes"):
              nmfile = "rectified/rct_NM_"+frames[j]
              nm = pyfits.open(nmfile,"update")
              nm[0].data = drihizzle3d(nm[0].data, xmax-xmin, intpart, frac, mask = mask, axis="X")
              nm.flush()
              nm.close()
           self.guiCount+=0.5
           self.guiCount = min(self.guiCount, len(self.filenames)+12)
           if (self.guiMessages):
	      print "PROGRESS: "+str(int(self.guiCRdone+self.guiCRf*(42+self.guiCount/(len(self.filenames)+12)*22)))
	      print "FILE: "+rctfile
	      if (self.params['DO_NOISEMAPS'].lower() == "yes"):
		print "FILE: "+nmfile
        #Rectify master arc lamp
	if (os.access(newlfile, os.F_OK) and not usingArcLamps):
           print "   Rectifying master arc lamp..."
           f = open(self.logfile,'ab')
           f.write("\tRectifying master arc lamp...\n")
           f.close()
	   lamp = pyfits.open(newlfile,"update")
           lamp[0].data = drihizzle3d(lamp[0].data, xmax-xmin, intpart, frac, mask = mask, axis="X")
	   lamp.flush()
           lamp.close()
	   if (self.guiMessages):
	      print "FILE: "+newlfile
	if (os.access("rectified/clean_sky_"+frames[0], os.F_OK) and usingArcLamps):
	   print "   Rectifying clean sky image..."
           f = open(self.logfile,'ab')
           f.write("\tRectifying clean sky image...\n")
           f.close()
	   sky = pyfits.open("rectified/clean_sky_"+frames[0], "update")
	   sky[0].data = drihizzle3d(sky[0].data, xmax-xmin, intpart, frac, mask = mask, axis="X")
	   sky.flush()
	   sky.close()
	   if (self.guiMessages):
	      print "FILE: "+newlfile
	#Free memory
	intpart = 0
	frac = 0
	mask = 0

   def rectifyMOS(self):
      if (self.guiMessages): print "STATUS: Rectifying MOS Data: Pass 2"
      #Group all frames with same flat together
      tmpflatnames = numarray.strings.array(self.flatnames)
      ybuflo = self.params['MOS_YSLIT_BUFFER_LOW']
      ybufhi = self.params['MOS_YSLIT_BUFFER_HIGH']
      n = len(self.filenames)
      while (n > 0):
	currFlat = tmpflatnames[0]
	tmpflatnames = tmpflatnames[where(tmpflatnames != currFlat)]
	frames = []
	keepFrame = []
	frameNum = []
	for j in range(len(self.filenames)):
	   if (self.flatnames[j] == currFlat):
	      frames.append(self.filenames[j])
	      keepFrame.append(True)
	      frameNum.append(j)
	      n-=1
        types = self.findTypes(frames, self.params['DATA_TYPE'])
        for j in range(len(frames)-1, -1, -1):
           if (types[j] != 'mos' and types[j] != 'ifu'):
              frames.pop(j)
              types.pop(j)
        if (len(frames) == 0): continue
        frames = numarray.strings.array(frames)
	for j in range(len(frames)):
	   rctfile = 'rectified/rct_'+frames[j]
	   if (os.access(rctfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"):
	      os.unlink(rctfile)
	   if (self.overwriteRectMOS[frameNum[j]] == True and os.access(rctfile, os.F_OK)):
	      os.unlink(rctfile)
	   if (os.access(rctfile, os.F_OK)):
	      keepFrame[j] = False
	   else:
	      if (self.overwriteRectMOS[frameNum[j]] == False): 
		keepFrame[j] = False
	b = where(keepFrame)
	if (len(b[0]) == 0): continue
	frames = frames[b]
	if (not os.access('TEMPyoutMOS'+frames[0], os.F_OK)):
	   print "Rectification transformation not found!"
           f = open(self.logfile,'ab')
           f.write("Rectification transformation not found!\n")
           f.close()
           f = open(self.warnfile,'ab')
           f.write("Rectification transformation not found!\n")
           f.write('Object: '+frames[0]+'\n')
           f.write('Time: '+str(datetime.today())+'\n\n')
           f.close()
           continue
	temp = pyfits.open('TEMPyoutMOS'+frames[0])
	yout = temp[0].data
	temp.close()
	os.unlink('TEMPyoutMOS'+frames[0])
	ymin = int(yout.min())
	ymax = int(yout.max())+2
	intpart = floor(yout-ymin).astype("Int32")
	frac = yout-ymin-intpart
        mask = where(yout != 0, 1, 0)
        if (ybuflo+ybufhi != 0):
	   temp = pyfits.open('TEMPyout2MOS'+frames[0])
	   yout2 = temp[0].data
	   temp.close()
	   os.unlink('TEMPyout2MOS'+frames[0])
           ymin = int(min(ymin, yout2.min()))
           ymax = int(max(ymax-2, yout2.max()))+2
           intpart2 = floor(yout2-ymin).astype("Int32")
           frac2 = yout2-ymin-intpart2
	   mask2 = where(yout2 != 0, 1, 0)
	   yout2 = 0.
	yout = 0.
	temp = 0.
	#Drihizzle3d
	for j in range(len(frames)):
	   print "   Rectifying... "+frames[j]
           f = open(self.logfile,'ab')
           f.write("\tRectifying... "+frames[j]+"\n")
           f.close()
           rctfile = 'rectified/rct_'+frames[j]
	   image = pyfits.open(self.prefix+frames[j])
           if (ybuflo+ybufhi == 0):
              image[0].data = drihizzle3d(image[0].data, ymax-ymin, intpart, frac, mask = mask)
           else:
              temp = drihizzle3d(image[0].data, ymax-ymin, intpart, frac, mask= mask)
              image[0].data = temp+drihizzle3d(image[0].data, ymax-ymin, intpart2, frac2, mask=mask2)
              temp = 0.
	   image.writeto(rctfile)
	   image.close()
           if (self.params['DO_NOISEMAPS'].lower() == "yes"):
              nmfile = "rectified/rct_NM_"+frames[j]
              if (os.access(nmfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(nmfile)
              if (self.overwriteRectMOS[frameNum[j]] == True and os.access(nmfile, os.F_OK)): os.unlink(nmfile)
              if (not os.access(nmfile, os.F_OK)):
		nm = pyfits.open(self.prefix+'NM_'+frames[j])
                if (ybuflo+ybufhi == 0):
                   nm[0].data = drihizzle3d(nm[0].data, ymax-ymin, intpart, frac, mask = mask)
                else:
                   temp = drihizzle3d(nm[0].data, ymax-ymin, intpart, frac, mask = mask)
                   nm[0].data = temp+drihizzle3d(nm[0].data, ymax-ymin, intpart2, frac2, mask = mask2)
                   temp = 0.
		nm.writeto(nmfile)
		nm.close()
	   self.guiCount+=0.5
           if (self.guiMessages): print "PROGRESS: "+str(int(self.guiCRdone+self.guiCRf*(72+self.guiCount/len(self.filenames)*10)))
        if (not os.access('TEMPxoutMOS'+frames[0], os.F_OK)):
           print "Rectification transformation not found!"
           f = open(self.logfile,'ab')
           f.write("Rectification transformation not found!\n")
           f.close()
           f = open(self.warnfile,'ab')
           f.write("Rectification transformation not found!\n")
           f.write('Object: '+frames[0]+'\n')
           f.write('Time: '+str(datetime.today())+'\n\n')
           f.close()
           continue
        temp = pyfits.open('TEMPxoutMOS'+frames[0])
        xout = temp[0].data
        temp.close()
        os.unlink('TEMPxoutMOS'+frames[0])
        xmin = int(xout.min())
        xmax = int(xout.max())+2
        intpart = floor(xout-xmin).astype("Int32")
        frac = xout-xmin-intpart
        mask = where(xout != 0, 1, 0)
        for j in range(len(frames)):
           print "   Rectifying sky lines for... "+frames[j]
           f = open(self.logfile,'ab')
           f.write("\tRectifying sky lines for... "+frames[j]+"\n")
           f.close()
           rctfile = 'rectified/rct_'+frames[j]
           image = pyfits.open(rctfile,"update")
           image[0].data = drihizzle3d(image[0].data, xmax-xmin, intpart, frac,mask = mask, axis="X")
           image.flush()
           image.close()
           if (self.params['DO_NOISEMAPS'].lower() == "yes"):
              nmfile = "rectified/rct_NM_"+frames[j]
              nm = pyfits.open(nmfile,"update")
              nm[0].data = drihizzle3d(nm[0].data, xmax-xmin, intpart, frac, mask = mask, axis="X")
              nm.flush()
              nm.close()
           self.guiCount+=0.5
           if (self.guiMessages): print "PROGRESS: "+str(int(self.guiCRdone+self.guiCRf*(72+self.guiCount/len(self.filenames)*10)))

   def findShiftsForDS(self, useOnlyPos):
      print "Finding shifts for double subtraction..."
      if (self.guiMessages): print "STATUS: Double subtraction..."
      f = open(self.logfile,'ab')
      f.write("Finding shifts for double subtraction...\n")
      f.close()
      box1 = self.params['SUM_BOX_CENTER']-self.params['SUM_BOX_WIDTH']/2
      box2 = box1 + self.params['SUM_BOX_WIDTH']
      orient = self.params['SPECTRAL_ORIENTATION']
      n = len(self.filenames)
      shifts = zeros(n)
      for j in range(n):
        dsfile = "doubleSubtracted/ds_"+self.filenames[j]
        if (os.access(dsfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(dsfile)
        if (not os.access(dsfile, os.F_OK)):
           image = pyfits.open(self.prefix+self.filenames[j])
           if (useOnlyPos[j]):
              image.close()
              continue
           pos = 1.0*image[0].data
           neg = -1.0 * pos
           pos*= where(pos > 0, 1, 0)
           neg*= where(neg > 0, 1, 0)
           #Cross correlate positives only vs inverse of negatives only
           #Find integer pixel offsets
           if (orient == 'vertical'):
	      posCut = arraymedian(pos[box1:box2,:],axis="Y")
              negCut = arraymedian(neg[box1:box2,:],axis="Y") 
           else:
	      posCut = arraymedian(pos[:,box1:box2],axis="X")
	      negCut = arraymedian(neg[:,box1:box2],axis="X")
           #ccor = scipy.signal.correlate(posCut,negCut,mode='same')
           ccor = conv.correlate(posCut,negCut,mode='same')
           ccor = array(ccor)
           mcor = where(ccor == max(ccor))[0]
           shifts[j] = len(ccor)/2-mcor[0]
	   niter = 0
	   while (abs(shifts[j]) > self.params['MAX_DITHER'] and niter < 100):
	      if (max(negCut) > max(posCut)):
		mNegCut = where(negCut == max(negCut))[0][0]
	        negCut[mNegCut-5:mNegCut+6] = 0
	      else:
		mPosCut = where(posCut == max(posCut))[0][0]
		posCut[mPosCut-5:mPosCut+6] = 0
              #ccor = scipy.signal.correlate(posCut,negCut,mode='same')
              ccor = conv.correlate(posCut,negCut,mode='same')
              ccor = array(ccor)
              mcor = compress(where(ccor == max(ccor), 1, 0), arange(len(ccor)))
	      shifts[j] = len(ccor)/2-mcor[0]
	      niter+=1
	   image.close()
	   if (niter >= 100):
	      print "Warning: Unable to find proper shift for double subtraction."
	      print "File: "+self.filenames[j]+" - using shift of "+str(shifts[j])
	      f = open(self.logfile,'ab')
	      f.write('Warning: Unable to find proper shift for double subtraction.\n')
	      f.write("File: "+self.filenames[j]+" - using shift of "+str(shifts[j])+"\n")
	      f.close()
	      f = open(self.warnfile,'ab')
	      f.write('WARNING: Unable to find proper shift for double subtraction.\n')
              f.write("File: "+self.filenames[j]+" - using shift of "+str(shifts[j])+"\n")
              f.write('Time: '+str(datetime.today())+'\n\n')
	      f.close()
      return shifts

   def doubleSubtract(self, useOnlyPos, shifts, npass):
      if (not os.access("doubleSubtracted",os.F_OK)):
	os.mkdir("doubleSubtracted",0755)
      print "Performing Double Subtraction..."
      if (self.guiMessages): print "STATUS: Double subtraction..."
      f = open(self.logfile,'ab')
      f.write("Performing double subtraction...\n")
      f.close()
      orient = self.params['SPECTRAL_ORIENTATION']
      n = len(self.filenames)
      dsfilenames = []
      if (npass == 1): self.overwriteDS = []
      types = self.findTypes(self.filenames, self.params['DATA_TYPE'])
      for j in range(n):
	dsfile = "doubleSubtracted/ds_"+self.filenames[j]
	dsfilenames.append(dsfile)
        if (os.access(dsfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(dsfile)
	if (npass == 2 and self.overwriteDS[j] == True and os.access(dsfile, os.F_OK)): os.unlink(dsfile)
        if (not os.access(dsfile, os.F_OK)):
	   if (npass == 2 and self.overwriteDS[j] == False): continue
	   if (npass == 1): self.overwriteDS.append(True)
	   image = pyfits.open(self.prefix+self.filenames[j])
	   if (self.params['DO_NOISEMAPS'].lower() == "yes"):
	      nmfile = "doubleSubtracted/ds_NM_"+self.filenames[j]
	      if (os.access(nmfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(nmfile)
              if (npass == 2 and self.overwriteDS[j] == True and os.access(nmfile, os.F_OK)): os.unlink(nmfile)
              #if (not os.access(nmfile, os.F_OK)):
	      if (os.access(nmfile, os.F_OK)): os.unlink(nmfile)
	      try: nm = pyfits.open(self.prefix+'NM_'+self.filenames[j])
	      except Exception:
		print "Noisemap not found for file "+self.filenames[j]
		f = open(self.logfile,'ab')
		f.write("Noisemap not found for file "+self.filenames[j]+".  Skipping this frame.\n")
		f.close()
		f = open(self.warnfile,'ab')
		f.write("WARNING: Noisemap not found for file "+self.filenames[j]+".  Skipping this frame.\n")
		f.write('Time: '+str(datetime.today())+'\n\n')
		f.close()
		continue
	      nmpos = nm[0].data
	   if (useOnlyPos[j]):
	      image.writeto(dsfile)
              if (npass == 1 and (types[j] == "mos" or types[j] == "ifu")):
	        try: f = open(self.regfile[j],'rb')
		except Exception:
		   print "Region file "+self.regfile[j]+" not found!  Skipping image "+self.filenames[j]
		   f = open(self.logfile,'ab')
		   f.write("Region file "+self.regfile[j]+" not found!  Skipping image "+self.filenames[j]+"\n")
		   f.close()
                   f = open(self.warnfile,'ab')
                   f.write("WARNING: Region file "+self.regfile[j]+" not found!  Skipping image "+self.filenames[j]+"\n")
                   f.write('Time: '+str(datetime.today())+'\n\n')
                   f.close()
		   continue
        	s = f.read()
        	f.close()
		f = open(dsfile+'.reg','wb')
		f.write(s)
		f.close()
	      image.close()
	      if (self.params['DO_NOISEMAPS'].lower() == "yes"):
		if (not os.access(nmfile, os.F_OK)):
		   nm.writeto(nmfile)
		   nm.close()
	      if (self.guiMessages):
		print "FILE: "+dsfile
		if (self.params['DO_NOISEMAPS'].lower() == "yes"):
		   nmfile = "doubleSubtracted/ds_NM_"+self.filenames[j]
		   print "FILE: "+nmfile
	      continue
	   if (os.access(self.slitMask[j], os.F_OK)):
	      slitmask = pyfits.open(self.slitMask[j])
	   else:
	      slitmask = pyfits.open(self.prefix+self.filenames[j]) 
	      slitmask[0].data[:] = 1.
           xsize = image[0].data.shape[0]
           ysize = image[0].data.shape[1]
           pos = image[0].data
	   slitpos = slitmask[0].data
           if (orient == 'vertical'):
	      image[0].data = zeros((xsize, ysize+abs(shifts[j])))+0.
              if (self.params['DO_NOISEMAPS'].lower() == "yes"):
		nm[0].data = zeros((xsize, ysize+abs(shifts[j])))+0.
	      slitmask[0].data = zeros((xsize, ysize+abs(shifts[j])))
           else:
              image[0].data = zeros((xsize+abs(shifts[j]), ysize))+0.
              if (self.params['DO_NOISEMAPS'].lower() == "yes"):
 		nm[0].data = zeros((xsize+abs(shifts[j]), ysize))+0.
	      slitmask[0].data = zeros((xsize+abs(shifts[j]), ysize))
           negOffset = 0-min([0,shifts[j]])
	   posOffset = shifts[j]-min([0,shifts[j]])
           if (orient == 'vertical'):
              image[0].data[:,posOffset:posOffset+ysize] = pos
	      image[0].data[:,negOffset:negOffset+ysize] -= pos
              if (self.params['DO_NOISEMAPS'].lower() == "yes"):
		nm[0].data[:,posOffset:posOffset+ysize] = nmpos**2
		nm[0].data[:,negOffset:negOffset+ysize] += nmpos**2
	      slitmask[0].data[:,posOffset:posOffset+ysize] = slitpos
	      slitmask[0].data[:,negOffset:negOffset+ysize] -= slitpos
           else:
              image[0].data[posOffset:posOffset+xsize,:] = pos
	      image[0].data[negOffset:negOffset+xsize,:] -= pos
              if (self.params['DO_NOISEMAPS'].lower() == "yes"):
		nm[0].data[posOffset:posOffset+xsize,:] = nmpos**2
		nm[0].data[negOffset:negOffset+xsize,:] += nmpos**2
	      slitmask[0].data[posOffset:posOffset+xsize,:] = slitpos
	      slitmask[0].data[negOffset:negOffset+xsize,:] -= slitpos
	   pos = 0
	   nmpos = 0
	   slitpos = 0
	   image[0].data*=where(slitmask[0].data == 0, 1, 0)
	   b = where(image[0].data != 0, 1, 0)
	   image[0].header.update(self.params['EXPTIME_KEYWORD'], 2*image[0].header[self.params['EXPTIME_KEYWORD']])
	   image.verify('silentfix')
	   image.writeto(dsfile)
	   image.close()
	   image = 0
	   if (npass == 1 and (types[j] == "mos" or types[j] == "ifu")):
	      slits = where(arraymedian(b,axis="X") != 0, 1, 0)
	      sylo = []
	      syhi = []
	      for l in range(1,len(slits)-1):
		if (slits[l] > slits[l-1]):
		   sylo.append(l+0.)
	        elif (slits[l] > slits[l+1]):
		   syhi.append(l+0.)
	      xcen = slitmask[0].data.shape[1]/2
              #Output new region file and slit mask
              newrfile = 'doubleSubtracted/ds_'+self.filenames[j]+'.reg'
              f = open(newrfile,'wb')
              f.write('# Region file format: DS9 version 3.0\n')
              f.write('global color=green select=1 edit=1 move=1 delete=1 include=1 fixed=0\n')
              for l in range(len(sylo)):
	        f.write('image;box('+str(xcen)+','+str((sylo[l]+syhi[l])/2+1)+',3,'+str(syhi[l]-sylo[l])+')\n')
              f.close()
	   if (self.params['DO_NOISEMAPS'].lower() == "yes"):
	      nm[0].data = sqrt(nm[0].data)
	      nm[0].data*=where(slitmask[0].data == 0, 1, 0)
	      nm.writeto(nmfile)
	      nm.close()
	      nm = 0
	   slitmask.close()
	   slitmask = 0
	else: self.overwriteDS.append(False)
	if (self.guiMessages):
	   print "FILE: "+dsfile
           if (self.params['DO_NOISEMAPS'].lower() == "yes"):
	      nmfile = "doubleSubtracted/ds_NM_"+self.filenames[j]
	      print "FILE: "+nmfile
      return dsfilenames

   def findOffsetsForSA(self, dsfiles):
      print "Finding offsets for shifting and adding..."
      if (self.guiMessages): print "STATUS: Shifting and Adding..." 
      f = open(self.logfile,'ab')
      f.write("Finding offsets for shifting and adding...\n")
      f.close()
      box1 = self.params['SUM_BOX_CENTER']-self.params['SUM_BOX_WIDTH']/2
      box2 = box1 + self.params['SUM_BOX_WIDTH']
      orient = self.params['SPECTRAL_ORIENTATION']
      prefixes = numarray.strings.array(dsfiles)
      offsets = []
      for j in range(len(prefixes)):
        prefixes[j] = prefixes[j][:prefixes[j].rfind(self.delim,0,prefixes[j].rfind('.'))]
      n = len(dsfiles)
      while (n > 0):
        #Select all objects with the same prefix for processing
        pfix = prefixes[0]
        prefixes = compress(where(prefixes != pfix, 1, 0), prefixes)
        print "Processing object: ",pfix,"..."
        f = open(self.logfile,'ab')
        f.write("\tProcessing object: "+pfix+"...\n")
        f.close()
        frames = []
        for j in range(len(dsfiles)):
           if (dsfiles[j][:dsfiles[j].rfind(self.delim,0,dsfiles[j].rfind('.'))] == pfix):
              frames.append(dsfiles[j])
              n-=1
        safile = 'shiftedAdded/sa_'+pfix+'.fits'
	safile = safile.replace('doubleSubtracted/ds_','')
        if (os.access(safile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(safile)
        if (os.access(safile, os.F_OK)):
	   for j in range(len(frames)): offsets.append([0,0,0])
	   continue
        #Read info from the first frame of the object
        saImage = pyfits.open(frames[0])
	saImage[0].data*=1.0*where(saImage[0].data > 0, 1, 0)
        if (orient == 'vertical'):
           #firstFrame = sum(saImage[0].data[box1:box2,:],0)+0.
	   firstFrame = arraymedian(saImage[0].data[box1:box2,:],axis="Y")+0.
        else:
           #firstFrame = sum(saImage[0].data[:,box1:box2],1)+0.
	   firstFrame = arraymedian(saImage[0].data[:,box1:box2],axis="X")+0.
        if (len(frames) == 1):
	   saImage.close()
           continue
        xsize = saImage[0].data.shape[1]
        ysize = saImage[0].data.shape[0]
        offsets.append([0,xsize,ysize])
	saImage.close()
        #Cross correlate all other frames with first frame.
        #Find integer pixel offsets
        for j in range(1, len(frames), 1):
           temp = pyfits.open(frames[j])
           xsize = temp[0].data.shape[1]
           ysize = temp[0].data.shape[0]
	   temp[0].data*=1.0*where(temp[0].data > 0, 1, 0)
           if (orient == 'vertical'):
              #currFrame = sum(temp[0].data[box1:box2,:],0)+0.
	      currFrame = arraymedian(temp[0].data[box1:box2,:],axis="Y")+0.
           else:
              #currFrame = sum(temp[0].data[:,box1:box2],1)+0.
	      currFrame = arraymedian(temp[0].data[:,box1:box2],axis="X")+0.
	   if (len(firstFrame) < len(currFrame)):
	      corrFrame = currFrame*0.
	      corrFrame[:len(firstFrame)] = firstFrame
	   else: corrFrame = firstFrame
           temp.close()
           #ccor = scipy.signal.correlate(corrFrame,currFrame,mode='same')
           ccor = conv.correlate(corrFrame,currFrame,mode='same')
           mcor = compress(where(ccor == max(ccor), 1, 0), arange(len(ccor)))
           offsets.append([-1*(len(ccor)/2-mcor[0]), xsize, ysize])
      return offsets

   def shiftAdd(self, dsfiles, offsets, npass):
      orient = self.params['SPECTRAL_ORIENTATION']
      if (not os.access("shiftedAdded",os.F_OK)): os.mkdir("shiftedAdded",0755)
      print "Performing Shifting and Adding..."
      if (self.guiMessages): print "STATUS: Shifting and Adding..."
      f = open(self.logfile,'ab')
      f.write("Performing Shifting and Adding...\n")
      f.close()
      prefixes = numarray.strings.array(dsfiles)
      for j in range(len(prefixes)):
        prefixes[j] = prefixes[j][:prefixes[j].rfind(self.delim,0,prefixes[j].rfind('.'))]
      n = len(dsfiles)
      safilenames = []
      if (npass == 1):
	self.overwriteSA = []
	self.troughs = []
	self.cleanFrames = []
	self.expFrames = []
	newlfiles = []
        tmplampnames = numarray.strings.array(self.lampnames)
      if (npass == 2):
	self.regfile = []
	newskyregfiles = []
	newcleanNMs = []
      count = 0
      types = self.findTypes(dsfiles, self.params['DATA_TYPE'])
      while (n > 0):
	#Select all objects with the same prefix for processing
        ptype = self.findTypes(prefixes, self.params['DATA_TYPE'])[0]
	pfix = prefixes[0]
	if (npass == 1):
	   newlfiles.append(tmplampnames[0])
	   #tmplampnames = compress(where(prefixes != pfix, 1, 0), tmplampnames)
	   tmplampnames = tmplampnames[where(prefixes != pfix)]
        prefixes = compress(where(prefixes != pfix, 1, 0), prefixes)
        print "Processing object: ",pfix,"..."
        f = open(self.logfile,'ab')
        f.write("\tProcessing object: "+pfix+"...\n")
        f.close()
        frames = []
	ind = []
	xsz = []
	ysz = []
	off = []
        for j in range(len(dsfiles)):
           if (dsfiles[j][:dsfiles[j].rfind(self.delim,0,dsfiles[j].rfind('.'))] == pfix and types[j] == ptype):
              frames.append(dsfiles[j])
	      off.append(offsets[j][0])
	      xsz.append(offsets[j][1])
	      ysz.append(offsets[j][2])
	      ind.append(j)
              n-=1
	safile = 'shiftedAdded/sa_'+pfix+'.fits'
	expfile = 'shiftedAdded/exp_'+pfix+'.fits'
        safile = safile.replace('doubleSubtracted/ds_','')
	expfile = expfile.replace('doubleSubtracted/ds_','')
	cleanfile = 'shiftedAdded/clean_'+pfix+'.fits'
	cleanfile = cleanfile.replace('doubleSubtracted/ds_','')
	if (npass == 1):
	   self.expFrames.append(expfile)
	   self.cleanFrames.append(cleanfile)
	newrfile = safile + '.reg'
	if (npass == 2):
	   if (ptype == 'mos' or ptype == 'ifu'):
	      self.regfile.append(newrfile)
	   else:
	      self.regfile.append('junk')
	   newskyregfiles.append(self.skyregfile[ind[0]])
	   newcleanNMs.append(self.cleanNMs[ind[0]])
	safilenames.append(safile)
	if (os.access(safile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"):
	   os.unlink(safile)
	   if (os.access(expfile, os.F_OK)): os.unlink(expfile)
        if (npass == 2 and self.overwriteSA[len(safilenames)-1] == True and os.access(safile, os.F_OK)):
	   os.unlink(safile)
	   if (os.access(expfile, os.F_OK)): os.unlink(expfile)
	if (os.access(safile, os.F_OK)):
	   self.overwriteSA.append(False)
	   if (self.guiMessages):
	      print "FILE: "+safile
	      if (os.access(expfile, os.F_OK)): print "FILE: "+expfile
              if (os.access(cleanfile, os.F_OK)): print "FILE: "+cleanfile
	      nmfile = "shiftedAdded/sa_NM_"+pfix+".fits"
              nmfile = nmfile.replace('doubleSubtracted/ds_','')
	      if (os.access(nmfile, os.F_OK)): print "FILE: "+nmfile
	   continue
        if (npass == 2 and self.overwriteSA[len(safilenames)-1] == False): continue
        if (npass == 1): self.overwriteSA.append(True)
	#Read info from the first frame of the object
	if (os.access(expfile, os.F_OK)): os.unlink(expfile)
        saImage = pyfits.open(frames[0])
	if (self.params['DO_NOISEMAPS'].lower() == "yes"):
	   nmfile = "shiftedAdded/sa_NM_"+pfix+".fits"
	   nmfile = nmfile.replace('doubleSubtracted/ds_','')
	   if (os.access(nmfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(nmfile)
           if (npass == 2 and self.overwriteSA[len(safilenames)-1] == True and os.access(nmfile, os.F_OK)): os.unlink(nmfile)
           if (not os.access(nmfile, os.F_OK)):
	      nmspos = frames[0].rfind('/')
	      nmprefix = frames[0][nmspos:frames[0].find('_',nmspos)+1]
	      nmname = frames[0].replace(nmprefix,nmprefix+'NM_')
	      try: nm = pyfits.open(nmname)
              except Exception:
                print "Noisemap "+nmname+" not found.  Skipping all noisemaps!" 
                f = open(self.logfile,'ab')
                f.write("Noisemap "+nmname+" not found.  Skipping all noisemaps!\n")
                f.close()
                f = open(self.warnfile,'ab')
                f.write("WARNING: Noisemap "+nmname+" not found.  Skipping all noisemaps!\n")
                f.write('Time: '+str(datetime.today())+'\n\n')
                f.close()
                self.params['DO_NOISEMAPS'] = 'no'
              nmpos = nm[0].data
	if (len(frames) == 1):
           print "Object ",frames[0]," has only one frame!"
           f = open(self.logfile,'ab')
           f.write('Object '+frames[0]+' has only one frame!\n')
           f.write('Shifting and Adding NOT done!\n')
           f.close()
           f = open(self.warnfile,'ab')
           f.write("WARNING: Object "+frames[0]+' has only one frame!\n')
           f.write("Shifting and Adding NOT done!\n")
           f.write('Time: '+str(datetime.today())+'\n\n')
           f.close()
	   saImage.writeto(safile)
	   if (npass == 1):
	      saImage.writeto(cleanfile)
	   saImage[0].data = saImage[0].data*0.+saImage[0].header[self.params['EXPTIME_KEYWORD']]
	   saImage.writeto(expfile)
	   saImage.close()
	   if (self.params['DO_NOISEMAPS'].lower() == "yes"):
	      nm.writeto(nmfile)
	      nm.close()
           if (self.guiMessages):
              print "FILE: "+safile
              if (os.access(expfile, os.F_OK)): print "FILE: "+expfile
              if (os.access(cleanfile, os.F_OK)): print "FILE: "+cleanfile
              if (os.access(nmfile, os.F_OK)): print "FILE: "+nmfile
	   if (ptype == "mos" or ptype == "ifu"):
	      try: f = open(frames[0]+'.reg','rb')
              except Exception:
		print "Region file "+frames[0]+".reg not found!  Skipping image "+frames[0]
                f = open(self.logfile,'ab')
                f.write("Region file "+frames[0]+".reg not found!  Skipping image "+frames[0]+"\n")
                f.close()
                f = open(self.warnfile,'ab')
                f.write("WARNING: Region file "+frames[0]+".reg not found!  Skipping image "+frames[0]+"\n")
                f.write('Time: '+str(datetime.today())+'\n\n')
                f.close()
		continue
	      s = f.read()
	      f.close()
	      f = open(newrfile,'wb')
	      f.write(s)
	      f.close()
	   continue
	#Use region files, pad or compress buffer between slits
	if (ptype == "mos" or ptype == "ifu"):
	   insylo = []
	   insyhi = []
	   adjsylo = []
	   adjsyhi = []
	   minOffset = min(off)
	   for j in range(len(frames)):
	      try: f = open(frames[j]+'.reg','rb')
              except Exception:
		print "Region file "+frames[j]+".reg not found!  Skipping image "+frames[j]
                f = open(self.logfile,'ab')
                f.write("Region file "+frames[j]+".reg not found!  Skipping image "+frames[j]+"\n")
                f.close()
                f = open(self.warnfile,'ab')
                f.write("WARNING: Region file "+frames[j]+" not found!  Skipping image "+frames[j]+"\n")
                f.write('Time: '+str(datetime.today())+'\n\n')
                f.close()
		continue
              s = f.read().split('\n')
              f.close()
              try: s.remove('')
              except ValueError:
        	junk = ''
	      tempylo = []
	      tempyhi = []
              for l in range(len(s)):
           	if (s[l].startswith("image;box") or s[l].startswith("box(")):
              	   temp=s[l][s[l].find('(')+1:-1]
                   temp = temp.split(',')
                   if (temp[-1].find(')') != -1):
                      temp[-1] = temp[-1][:temp[-1].find(')')]
		   tempylo.append(float(temp[1])-1-float(temp[3])/2.)
		   tempyhi.append(float(temp[1])-1+float(temp[3])/2.)
	      adjsylo.append(array(tempylo)+off[j])
	      adjsyhi.append(array(tempyhi)+off[j])
              insylo.append(array(tempylo))
              insyhi.append(array(tempyhi))
	   xcen = float(temp[0])
	   insylo = array(insylo)
	   insyhi = array(insyhi)
	   adjsylo = array(adjsylo)
	   adjsyhi = array(adjsyhi)
	   msylo = []
	   msyhi = []
	   for j in range(len(insylo[0])):
	     msylo.append(adjsylo[:,j].min())
	     msyhi.append(adjsyhi[:,j].max())
	   diffs = [msylo[0]]
	   moutlo = [0]
	   mouthi = [msyhi[0]-msylo[0]]
	   for j in range(1,len(msylo)):
	      moutlo.append(mouthi[j-1]+4)
	      mouthi.append(moutlo[j]+msyhi[j]-msylo[j])
	      diffs.append(msylo[j]-moutlo[j])
	   outsylo = (adjsylo-array(diffs)).astype("Int32")
	   outsyhi = (adjsyhi-array(diffs)).astype("Int32")
	   insylo = insylo.astype("Int32")
	   insyhi = insyhi.astype("Int32")
	   #Write out region file
           f = open(newrfile,'wb')
           f.write('# Region file format: DS9 version 3.0\n')
           f.write('global color=green select=1 edit=1 move=1 delete=1 include=1 fixed=0\n')
           for l in range(len(moutlo)):
	      f.write('image;box('+str(xcen)+','+str((moutlo[l]+mouthi[l])/2+1)+',3,'+str(mouthi[l]-moutlo[l])+')\n')
           f.close()
	   #Create new image sizes
	   xsize = max(xsz)
	   ysize = int(max(mouthi)+1)
	   saImage[0].data = zeros((ysize, xsize))+0.
	   if (self.params['DO_NOISEMAPS'].lower() == "yes"):
	      nm[0].data = zeros((ysize, xsize))+0.
	   expmap = saImage[0].data*0.
	   #Loop over images then over slits, shift and add
           for j in range(len(frames)):
	      temp = pyfits.open(frames[j])
              currExpMap = temp[0].data*0.+temp[0].header[self.params['EXPTIME_KEYWORD']]
              if (self.params['DO_NOISEMAPS'].lower() == "yes"):
                 tempnm = pyfits.open(frames[j].replace(nmprefix,nmprefix+'NM_'))
	      #Mask out darkest troughs
              if (self.params['MASK_OUT_TROUGHS'].lower() == 'yes'):
		if (npass == 1):
                   if (orient == 'vertical'):
                      cut1d = sum(temp[0].data,0)+0.
                   else:
                      cut1d = sum(temp[0].data,1)+0.
                   troughs = extractTroughs(cut1d,self.params['EXTRACT_SIGMA'][0],self.params['EXTRACT_MIN_WIDTH'][0])
                   self.troughs.append(troughs)
                elif (npass == 2):
                   troughs = self.troughs[count]
                   count+=1
                for l in range(len(troughs)):
                   if (orient == 'vertical'):
                      temp[0].data[:,troughs[l][0]:troughs[l][1]+1] = 0
                      currExpMap[:,troughs[l][0]:troughs[l][1]+1] = 0
                      if (self.params['DO_NOISEMAPS'].lower() == "yes"):
			tempnm[0].data[:,troughs[l][0]:troughs[l][1]+1] = 0
                   else:
                      temp[0].data[troughs[l][0]:troughs[l][1]+1,:] = 0
                      currExpMap[troughs[l][0]:troughs[l][1]+1,:] = 0
                      if (self.params['DO_NOISEMAPS'].lower() == "yes"):
                        tempnm[0].data[troughs[l][0]:troughs[l][1]+1,:] = 0
	      #Loop over slits, shift and add
	      for l in range(len(insylo[j])):
		saImage[0].data[outsylo[j][l]:outsyhi[j][l]+1,:] += temp[0].data[insylo[j][l]:insyhi[j][l]+1,:]
                expmap[outsylo[j][l]:outsyhi[j][l]+1,:] += currExpMap[insylo[j][l]:insyhi[j][l]+1,:]*where(temp[0].data[insylo[j][l]:insyhi[j][l]+1,:] != 0, 1, 0)
                if (self.params['DO_NOISEMAPS'].lower() == "yes"):
                   nm[0].data[outsylo[j][l]:outsyhi[j][l]+1,:] += tempnm[0].data[insylo[j][l]:insyhi[j][l]+1,:]**2
              temp.close()
	else:
           #Create new image sizes
	   xsize = max(xsz)
	   ysize = max(ysz) 
	   minOffset = min(off)
	   maxOffset = max(off)
	   if (orient == 'vertical'):
	      saImage[0].data = zeros((ysize, xsize+maxOffset-minOffset))+0.
	      if (self.params['DO_NOISEMAPS'].lower() == "yes"):
	        nm[0].data = zeros((ysize, xsize+maxOffset-minOffset))+0.
	   else:
	      saImage[0].data = zeros((ysize+maxOffset-minOffset, xsize))+0.
	      if (self.params['DO_NOISEMAPS'].lower() == "yes"):
	        nm[0].data = zeros((ysize+maxOffset-minOffset, xsize))+0.
	   expmap = saImage[0].data*0.
	   #Loop over image, shift and add all at once
	   for j in range(len(frames)):
	      #temp = pyfits.open(frames[ind[j]])
	      temp = pyfits.open(frames[j])
	      currExpMap = temp[0].data*0.+temp[0].header[self.params['EXPTIME_KEYWORD']]
	      if (self.params['DO_NOISEMAPS'].lower() == "yes"):
	        tempnm = pyfits.open(frames[j].replace(nmprefix,nmprefix+'NM_'))
              #Mask out darkest troughs
	      if (self.params['MASK_OUT_TROUGHS'].lower() == 'yes'):
	        if (npass == 1):
		   if (orient == 'vertical'):
		      cut1d = sum(temp[0].data,0)+0.
	           else:
		      cut1d = sum(temp[0].data,1)+0.
	           troughs = extractTroughs(cut1d,self.params['EXTRACT_SIGMA'][0],self.params['EXTRACT_MIN_WIDTH'][0])
		   self.troughs.append(troughs)
	        elif (npass == 2):
		   troughs = self.troughs[count]
		   count+=1
	        for l in range(len(troughs)):
		   if (orient == 'vertical'):
		      temp[0].data[:,troughs[l][0]:troughs[l][1]+1] = 0
		      currExpMap[:,troughs[l][0]:troughs[l][1]+1] = 0
		      if (self.params['DO_NOISEMAPS'].lower() == "yes"):
			tempnm[0].data[:,troughs[l][0]:troughs[l][1]+1] = 0
		   else:
		      temp[0].data[troughs[l][0]:troughs[l][1]+1,:] = 0
		      currExpMap[troughs[l][0]:troughs[l][1]+1,:] = 0
                      if (self.params['DO_NOISEMAPS'].lower() == "yes"):
		        tempnm[0].data[troughs[l][0]:troughs[l][1]+1,:] = 0
	      #temp[0].data *= where(temp[0].data > 0, 1, 0)
	      currOffset = offsets[ind[j]][0]-minOffset
	      xsize = temp[0].data.shape[1]
	      ysize = temp[0].data.shape[0]
	      #Shift and add
	      if (orient == 'vertical'):
		saImage[0].data[:,currOffset:currOffset+xsize] += temp[0].data
	        expmap[:,currOffset:currOffset+xsize] += currExpMap*where(temp[0].data != 0, 1, 0) 
	        if (self.params['DO_NOISEMAPS'].lower() == "yes"):
		   nm[0].data[:,currOffset:currOffset+xsize] += tempnm[0].data**2
	      else:
	        saImage[0].data[currOffset:currOffset+ysize,:] += temp[0].data
	        expmap[currOffset:currOffset+ysize,:] += currExpMap*where(temp[0].data != 0, 1, 0)
	        if (self.params['DO_NOISEMAPS'].lower() == "yes"):
		   nm[0].data[currOffset:currOffset+ysize,:] += tempnm[0].data**2
	      temp.close()
	#Divide by expmap and save
	b = where(expmap != 0)
	saImage[0].data[b] = saImage[0].data[b]/expmap[b]
	saImage.writeto(safile)
	if (npass == 1):
	   if (os.access(cleanfile, os.F_OK)):
	      os.unlink(cleanfile)
	   saImage.writeto(cleanfile)
	saImage[0].data = expmap
	saImage.writeto(expfile)
	saImage.close()
	if (self.params['DO_NOISEMAPS'].lower() == "yes"):
	   nm[0].data = sqrt(nm[0].data)
	   nm[0].data[b] = nm[0].data[b]/expmap[b]
	   nm.writeto(nmfile)
	   nm.close()
	if (self.guiMessages):
	   print "FILE: "+safile
           if (os.access(expfile, os.F_OK)): print "FILE: "+expfile
           if (os.access(cleanfile, os.F_OK)): print "FILE: "+cleanfile
           nmfile = "shiftedAdded/sa_NM_"+pfix+".fits"
           nmfile = nmfile.replace('doubleSubtracted/ds_','')
           if (os.access(nmfile, os.F_OK)): print "FILE: "+nmfile
      if (npass == 1):
	self.lampnames = newlfiles
      if (npass == 2):
	self.skyregfile = newskyregfiles
	self.cleanNMs = newcleanNMs
      return safilenames

   def findSpectra(self,safiles):
      print "Finding spectra..."
      if (self.guiMessages): print "STATUS: Finding spectra..."
      f = open(self.logfile,'ab')
      f.write("Finding spectra...\n")
      f.close()
      method = self.params['EXTRACT_METHOD'].lower()
      box1 = array(self.params['EXTRACT_BOX_CENTER'])-array(self.params['EXTRACT_BOX_WIDTH'])/2
      box2 = box1 + array(self.params['EXTRACT_BOX_WIDTH'])
      orient = self.params['SPECTRAL_ORIENTATION']
      sigma = array(self.params['EXTRACT_SIGMA'])
      width = array(self.params['EXTRACT_MIN_WIDTH'])
      nspec = self.params['EXTRACT_N_SPECTRA']
      n = len(safiles)
      types = self.findTypes(safiles, self.params['DATA_TYPE'])
      multi = False
      if (os.access(method, os.F_OK)):
	f = open(method,'rb')
	s = f.read().split('\n')
	f.close()
	try: s.remove('')
        except ValueError:
           junk = ''
	tmppfx = []
	tmpboxw = []
	tmpboxc = []
	tmpn = []
	tmpsig = []
	tmpwid = []
	for j in range(len(s)):
	   temp = s[j].split()
	   tmppfx.append(temp[0])
	   tmpboxw.append(int(temp[1]))
	   tmpboxc.append(int(temp[2]))
	   tmpn.append(int(temp[3]))
	   tmpsig.append(float(temp[4]))
	   tmpwid.append(int(temp[5]))
	multibox1 = []
	multibox2 = []
	multinspec = []
	multisigma = []
	multiwidth = []
	for j in range(n):
	   multibox1.append([])
           multibox2.append([])
           multinspec.append([])
           multisigma.append([])
           multiwidth.append([])
	   for l in range(len(tmppfx)):
	      if (safiles[j].find(tmppfx[l]) != -1):
		multibox1[j].append(tmpboxc[l]-tmpboxw[l]/2)
		multibox2[j].append(tmpboxc[l]+tmpboxw[l]/2)
		multinspec[j].append(tmpn[l])
		multisigma[j].append(tmpsig[l])
		multiwidth[j].append(tmpwid[l])
	method = 'auto'
	multi = True
      spectra = []
      if (method == 'auto' and os.access(self.params['EXTRACT_BOX_GUESS_FILE'], os.F_OK)):
	f = open(self.params['EXTRACT_BOX_GUESS_FILE'])
	s = f.read().split('\n')
        f.close()
        b = 0
        start = 0
	y1 = []
	y2 = []
	x1 = []
	x2 = []
	nspec = []
	sig = []
	minwid = []
        while (b < len(s)):
	   if (s[b] == '' or curses.ascii.isalpha(s[b][0])):
	      tempy1 = []
              tempy2 = []
              tempx1 = []
              tempx2 = []
              tempnspec = []
              tempsig = []
              tempminwid = []
              for j in range(start, b):
		tempy1.append(int(s[j].split()[0]))
		tempy2.append(int(s[j].split()[1]))
		tempx1.append(int(s[j].split()[2]))
	        tempx2.append(int(s[j].split()[3]))
		tempnspec.append(int(s[j].split()[4]))
		tempsig.append(int(s[j].split()[5]))
		tempminwid.append(int(s[j].split()[6]))
	      y1.append(tempy1)
	      y2.append(tempy2)
	      x1.append(tempx1)
	      x2.append(tempx2)
	      nspec.append(tempnspec)
	      sig.append(tempsig)
	      minwid.append(tempminwid)
              start = b+1
           b+=1
	for j in range(n):
           image = pyfits.open(safiles[j])
	   y = []
	   for l in range(len(y1[j])):
	      if (orient == 'vertical'):
		#cut = sum(image[0].data[x1[j][l]:x2[j][l]+1,:],0)+0.
                cut = arraymedian(image[0].data[x1[j][l]:x2[j][l]+1,:],axis="Y")+0.
              else:
                #cut = sum(image[0].data[:,x1[j][l]:x2[j][l]+1],1)+0.
		cut = arraymedian(image[0].data[:,x1[j][l]:x2[j][l]+1],axis="X")+0.
	      cut[where(cut == 0)] = 1.e-6
	      cut[where(cut < 0)] = 0.
	      temp = extractSpectra(cut[y1[j][l]:y2[j][l]+1], sig[j][l], minwid[j][l], nspec=nspec[j][l])
	      if (temp[0][0] != -1):
		temp+=y1[j][l]
		temp = temp.tolist()
	      else:
		temp = []
	      for i in range(len(temp)):
		temp[i].append(x1[j][l])
		temp[i].append(x2[j][l])
		y.append(temp[i])
	   spectra.append(array(y))
	f = open(self.params['EXTRACT_FILENAME'],'wb')
	for j in range(len(spectra)):
	   for l in range(len(spectra[j])):
	      f.write(str(spectra[j][l][0])+'\t'+str(spectra[j][l][1])+'\t'+str(spectra[j][l][2])+'\t'+str(spectra[j][l][3])+'\n')
	   f.write('\n')
	f.close()
      elif (method == 'auto'):
	for j in range(n):
	   if (types[j] != 'longslit'):
	      spectra.append(array(0))
	      continue
	   if (multi):
	      box1 = multibox1[j]
	      box2 = multibox2[j]
	      nspec = multinspec[j]
	      sigma = multisigma[j]
	      width = multiwidth[j]
           image = pyfits.open(safiles[j])
           for l in range(len(box1)):
	      fullBox = False
	      if (box1[l] == 0 and box2[l] == 0):
		fullBox = True
		box2[l] = image[0].data.shape[1]
	      print box1[l],box2[l]
	      if (orient == 'vertical'):
		#cut = sum(image[0].data[box1[l]:box2[l],:],0)+0.
		cut = arraymedian(image[0].data[box1[l]:box2[l],:],axis="Y")+0.
              else:
                #cut = sum(image[0].data[:,box1[l]:box2[l]],1)+0.
		cut = arraymedian(image[0].data[box1[l]:box2[l],:],axis="X")+0.
	      cut[where(cut == 0)] = 1.e-6
	      cut[where(cut < 0)] = 0.
	      if (l == 0):
		y = extractSpectra(cut, sigma[l], width[l], nspec=nspec[l])
		if (y[0][0] != -1):
		   y = y.tolist()
		   for i in range(len(y)):
		      y[i].append(box1[l])
		      y[i].append(box2[l])
		else: y = []
	      else:
		temp = extractSpectra(cut, sigma[l], width[l], sort='yes')
		if (temp[0][0] != -1):
		   temp = temp.tolist()
		else: temp = []
		ispec = 0
		for i in range(len(temp)):
		   if (ispec >= nspec): continue
		   print l,ispec,temp[i][0]
		   mid = (temp[i][0]+temp[i][1])/2
		   add = True
		   for k in range(len(y)):
		      if (temp[i][0] >= y[k][0] and temp[i][0] <= y[k][1]):
			add = False
		      if (temp[i][1] >= y[k][0] and temp[i][1] <= y[k][1]):
			add = False
		      #if (mid >= y[k][0] and mid <= y[k][1]): add = False
		   if (add):
		      temp[i].append(box1[l])
		      temp[i].append(box2[l])
		      y.append(temp[i])
		      ispec+=1
	      if (fullBox and box1[l] == 0):
		box2[l] = 0
	   spectra.append(array(y))
	#f = open(self.params['EXTRACT_FILENAME'],'wb')
	#for j in range(len(spectra)):
	   #for l in range(len(spectra[j])):
	      #f.write(str(spectra[j][l][0])+'\t'+str(spectra[j][l][1])+'\t'+str(spectra[j][l][2])+'\t'+str(spectra[j][l][3])+'\n')
	   #f.write('\n')
	#f.close()
	for j in range(n):
	   if (types[j] != 'mos' and types[j] != 'ifu'):
	      continue
           if (multi):
              box1 = multibox1[j]
              box2 = multibox2[j]
              nspec = multinspec[j]
              sigma = multisigma[j]
              width = multiwidth[j]
           image = pyfits.open(safiles[j])
           #Read region file
           sylo = []
           syhi = []
           f = open(self.regfile[j],'rb')
           s = f.read().split('\n')
           f.close()
           try: s.remove('')
           except ValueError:
              junk = ''
           for l in range(len(s)):
	      if (s[l].startswith("image;box") or s[l].startswith("box(")):
		temp=s[l][s[l].find('(')+1:-1]
                temp = temp.split(',')
                if (temp[-1].find(')') != -1):
                   temp[-1] = temp[-1][:temp[-1].find(')')]
		sylo.append(int(float(temp[1])-float(temp[3])/2.))
		syhi.append(int(float(temp[1])+float(temp[3])/2.))
           for l in range(len(box1)):
	      fullBox = False
	      if (box1[l] == 0 and box2[l] == 0):
		fullBox = True
		box2[l] = image[0].data.shape[1]
	      print box1[l],box2[l]
	      if (orient == 'vertical'):
		#cut = sum(image[0].data[box1[l]:box2[l],:],0)+0.
		cut = arraymedian(image[0].data[box1[l]:box2[l],:],axis="Y")+0.
              else:
                #cut = sum(image[0].data[:,box1[l]:box2[l]],1)+0.
		cut = arraymedian(image[0].data[:,box1[l]:box2[l]],axis="X")+0.
	      cut[where(cut == 0)] = 1.e-6
	      cut[where(cut < 0)] = 0.
	      if (l == 0):
		y = []
		for r in range(len(sylo)):
		   temp = extractSpectra(cut[sylo[r]:syhi[r]+1], sigma[l], width[l], nspec=nspec[l])
		   if (temp[0][0] != -1):
		      temp+=sylo[r]
		      temp = temp.tolist()
		   else:
		      temp = []
		   for i in range(len(temp)):
		      temp[i].append(box1[l])
		      temp[i].append(box2[l])
		      y.append(temp[i])
	      else:
		for r in range(len(sylo)):
		   temp = extractSpectra(cut[sylo[r]:syhi[r]+1], sigma[l], width[l], sort='yes')
		   if (temp[0][0] != -1):
		      temp+=sylo[r]
		      temp = temp.tolist()
		   else: temp = []
		   ispec = 0
		   for i in range(len(temp)):
		      if (ispec >= nspec): continue
		      mid = (temp[i][0]+temp[i][1])/2
		      add = True
		      for k in range(len(y)):
			if (temp[i][0] >= y[k][0] and temp[i][0] <= y[k][1]):
			   add = False
		        if (temp[i][1] >= y[k][0] and temp[i][1] <= y[k][1]):
			   add = False
		        #if (mid >= y[k][0] and mid <= y[k][1]): add = False
		      if (add):
		        temp[i].append(box1[l])
			temp[i].append(box2[l])
		        y.append(temp[i])
		        ispec+=1
	      if (fullBox and box1[l] == 0):
		box2[l] = 0
	   #spectra.append(array(y))
	   spectra[j] = array(y)
	f = open(self.params['EXTRACT_FILENAME'],'wb')
	for j in range(len(spectra)):
	   for l in range(len(spectra[j])):
	      f.write(str(spectra[j][l][0])+'\t'+str(spectra[j][l][1])+'\t'+str(spectra[j][l][2])+'\t'+str(spectra[j][l][3])+'\n')
	   f.write('\n')
	f.close()
      elif (method == 'manual'):
	if (os.access(self.params['EXTRACT_FILENAME'], os.F_OK)):
	   f = open(self.params['EXTRACT_FILENAME'])
	   s = f.read().split('\n')
	   f.close()
	   b = 0
	   start = 0
	   while (b < len(s)):
	      if (s[b] == '' or curses.ascii.isalpha(s[b][0])):
		ys = []
		for j in range(start, b):
		   temp = [int(s[j].split()[0]), int(s[j].split()[1]), int(s[j].split()[2]), int(s[j].split()[3])]
		   ys.append(temp)
		ys = array(ys)
		if (len(ys) > 0):
		   spectra.append(ys)
		start = b+1
	      b+=1
      elif (method == 'full'):
	for j in range(n):
	   image = pyfits.open(safiles[j])
	   spectra.append(array([0,image[0].data.shape[0],0,image[0].data.shape[1]]))
      return spectra

   def wavelengthCorrectMOS(self):
      if (not os.access("mosAlignedSkylines",os.F_OK)):
        os.mkdir("mosAlignedSkylines",0755)
      print "Aligning Slits..."
      if (self.guiMessages): print "STATUS: Aligning MOS Slits..."
      f = open(self.logfile,'ab')
      f.write("Aligning Slits...\n")
      f.close()
      types = self.findTypes(self.filenames, self.params['DATA_TYPE'])
      for j in range(len(self.filenames)):
	if (self.guiMessages): print "PROGRESS: "+str(int(self.guiCRdone+self.guiCRf*(86+(j+0.)/len(self.filenames)*4)))
        if (types[j] != 'mos' and types[j] != 'ifu'):
	   continue
        wcfile = 'mosAlignedSkylines/mas_'+self.filenames[j]
        if (os.access(wcfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(wcfile)
        if (os.access(wcfile, os.F_OK)):
	   if (os.access('mosAlignedSkylines/clean_sky_'+self.filenames[j], os.F_OK)):
	      self.cleanNMs[j] = 'mosAlignedSkylines/clean_sky_'+self.filenames[j]
	   if (os.access('mosAlignedSkylines/clean_'+self.filenames[j], os.F_OK)):
	      self.cleanFrames[j] = 'mosAlignedSkylines/clean_'+self.filenames[j]
	   newlfile = 'mosAlignedSkylines/'+self.lampnames[j][self.lampnames[j].rfind('/')+1:]
	   if (os.access(newlfile, os.F_OK)):
	      self.lampnames[j] = newlfile
	   if (self.guiMessages):
	      print "FILE: "+wcfile
	      if (os.access(self.cleanNMs[j], os.F_OK)): print "FILE: "+self.cleanNMs[j]
              if (os.access(self.cleanFrames[j], os.F_OK)): print "FILE: "+self.cleanFrames[j]
	      if (os.access('mosAlignedSkylines/exp_'+self.filenames[j], os.F_OK)): print "FILE: mosAlignedSkylines/exp_"+self.filenames[j]
              nmfile = "mosAlignedSkylines/mas_NM_"+self.filenames[j]
	      if (os.access(nmfile, os.F_OK)): print "FILE: "+nmfile
           continue
	if (os.access('mosAlignedSkylines/exp_'+self.filenames[j], os.F_OK)):
	   os.unlink('mosAlignedSkylines/exp_'+self.filenames[j])
	#Read region file
	slitx = []
        slity = []
        slitw = []
        slith = []
	if (not os.access(self.skyregfile[j], os.F_OK)):
	   print "Region file "+self.skyregfile[j]+" not found."
	   print "Slits not aligned for file "+self.cleanNMs[j]
	   f = open(self.logfile,'ab')
	   f.write("Warning: Region file "+self.skyregfile[j]+"not found.\n")
	   f.write("\tSlits not aligned for file "+self.cleanNMs[j]+"\n")
	   f.close()
	   f = open(self.warnfile,'ab')
           f.write("WARNING: Region file "+self.skyregfile[j]+"not found.\n")
           f.write("\tSlits not aligned for file "+self.cleanNMs[j]+"\n")
	   f.write('Time: '+str(datetime.today())+'\n\n')
           f.close()
	   continue
        f = open(self.skyregfile[j],'rb')
        s = f.read().split('\n')
        f.close()
        try: s.remove('')
        except ValueError:
           junk = ''
        for l in range(len(s)):
	   if (s[l].startswith("image;box") or s[l].startswith("box(")):
	      temp=s[l][s[l].find('(')+1:-1]
              temp = temp.split(',')
              if (temp[-1].find(')') != -1):
                temp[-1] = temp[-1][:temp[-1].find(')')]
              slitx.append(float(temp[0]))
              slity.append(float(temp[1]))
              slitw.append(float(temp[2]))
              slith.append(float(temp[3]))
        sxmin = int(min(slitx))
        sxmax = int(max(slitx))+1
        sylo = array(slity)-array(slith)/2
        syhi = array(slity)+array(slith)/2
	#Read in image
	if (self.params['MOS_ALIGN_USE_ARCLAMPS'].lower() == "yes" and os.access(self.lampnames[j], os.F_OK)):
	   image = pyfits.open(self.lampnames[j])
	   usingArcLamps = True
	   imfile = self.lampnames[j]
           print "Using ARCLAMP " + self.lampnames[j] 
           f = open(self.logfile,'ab')
           f.write('Using ARCLAMP ' +self.lampnames[j]+'\n')
           f.close()
	else:
	   if (os.access(self.cleanNMs[j], os.F_OK)):
	      image = pyfits.open(self.cleanNMs[j])
	      imfile = self.cleanNMs[j]
	      print "Using clean sky image " + self.cleanNMs[j]
	      f = open(self.logfile,'ab')
              f.write('Using clean sky image ' +self.cleanNMs[j]+'\n')
	      f.close()
	   else:
	      print "Can not find arc lamp or clean sky image to use for alignment of "+self.filenames[j]+"!"
	      f = open(self.logfile,'ab')
	      f.write('Warning: Can not find arc lamp or clean sky image to use\n')
	      f.write("for alignment of "+self.filenames[j]+"!\n")
	      f.close()
	      f = open(self.warnfile,'ab')
	      f.write('WARNING: Can not find arc lamp or clean sky image to use\n')
	      f.write("for alignment of "+self.filenames[j]+"!  Step will be skipped for this file!\n")
              f.write('Time: '+str(datetime.today())+'\n\n')
	      f.close()
	      continue
	   usingArcLamps = False
        onedcuts = []
	xoffinit = []
	#Take 1-d cuts (spectra), find initial guesses for offsets
        for l in range(len(sylo)):
	   print l, sylo[l], syhi[l]
	   if (l != 0 and sylo[l] > sylo[0]):
	      tempylo = max(int(sylo[l]), int(syhi[l-1]))
	   elif (l != len(sylo)-1 and sylo[l] < sylo[0]):
		tempylo = max(int(sylo[l]), int(syhi[l+1]))
	   else:
	      tempylo = int(sylo[l])
	   if (l != len(sylo)-1 and syhi[l] > syhi[0]):
	      tempyhi = min(int(syhi[l]), int(sylo[l+1]))
	   elif (l != 0 and syhi[l] < syhi[0]):
	      tempyhi = min(int(syhi[l]), int(sylo[l-1]))
	   else:
	      tempyhi = int(syhi[l])
	   onedcuts.append(arraymedian(image[0].data[tempylo:tempyhi+1,:],axis="Y"))
	   xoffinit.append(sxmax-int(slitx[l]))
	xoffinit = array(xoffinit)
	#Select reference frame
	if (self.params['REFERENCE_SLIT'] == -1):
	   ref = where(abs(xoffinit-arraymedian(xoffinit[1:-1])) == min(abs(xoffinit-arraymedian(xoffinit[1:-1]))))[0][0]+1
	elif (self.params['REFERENCE_SLIT'] == -2):
	   print 'Select a reference slit to use for file '+imfile
	   print 'The bottom slit is slit 0.'
	   if (guiMessages):
	      print "INPUT: Text: Select a reference slit to use for file "+imfile+" (bottom slit = 0)"
	   tmp = raw_input("Select a slit: ")
           if (guiMessages): print tmp
	   try: ref = int(tmp)
	   except Exception:
	      ref =  where(abs(xoffinit-arraymedian(xoffinit[1:-1])) == min(abs(xoffinit-arraymedian(xoffinit[1:-1]))))[0][0]+1
	else:
	   ref = self.params['REFERENCE_SLIT']
        if (self.params['MEDIAN_FILTER_1D'].lower() == "yes"):
	   for l in range(len(onedcuts)):
	      tempcut = zeros(len(onedcuts[l]))
	      for k in range(25):
		tempcut[k] = onedcuts[l][k] - arraymedian(onedcuts[l][:50])
	      for k in range(25,len(onedcuts[l])-25):
	        tempcut[k] = onedcuts[l][k] - arraymedian(onedcuts[l][k-25:k+26])
	      for k in range(len(onedcuts[l])-25, len(onedcuts[l])):
		tempcut[k] = onedcuts[l][k] - arraymedian(onedcuts[l][len(onedcuts[l])-50:])
	      onedcuts[l] = tempcut
	pxmin = max(xoffinit)-xoffinit[ref]+25
	pxmax = min(xoffinit)-xoffinit[ref]+len(onedcuts[ref])-25
	nseg = len(self.params['MOS_ALIGN_NLINES'])
	xoffsets = []
	xlines = []
	validlines = []
	#Loop over segments
	nmiss = 0
	print "   Matching Up Lines..."
        f = open(self.logfile,'ab')
        f.write("\tMatching Up Lines...\n")
        f.close()
	if (self.params['REVERSE_ORDER_OF_SEGMENTS'].lower() == 'no'):
	   seg1 = 0
	   seg2 = nseg
	   seginc = 1
	else:
	   seg1 = nseg-1
	   seg2 = -1
	   seginc = -1
	for k in range(seg1,seg2,seginc):
           pxmin = max(xoffinit)-xoffinit[ref]+25
	   pxmax = min(xoffinit)-xoffinit[ref]+len(onedcuts[ref])-25
	   xoff1 = xoffinit+0
	   xlo = int(pxmin+(pxmax-pxmin)*k/nseg)
	   xhi = int(pxmin+(pxmax-pxmin)*(k+1)/nseg)
	   validxs = arange(xhi-xlo+1)+xlo
	   #Find brightest line in segment
	   blref = validxs[where(onedcuts[ref][validxs] == max(onedcuts[ref][validxs]))[0][0]]+xoff1[ref]
	   nlines = 1
	   print blref
	   bline = []
	   tmpblines = zeros((3,len(sylo)),"Float64")
	   tmpsigmas = zeros((3,len(sylo)),"Float64")
	   for l in range(len(sylo)):
	      bxlo = max(blref-xoff1[l]-50,0)
	      bxhi = min(bxlo+101,xhi+xoff1[ref]-xoff1[l])
	      bline.append(where(onedcuts[l][bxlo:bxhi] == max(onedcuts[l][bxlo:bxhi]))[0][0]+bxlo+xoff1[l])
	      #Continue to next iteration if first segment
	      #OR if not doing double checking
	      if (len(xlines) == 0 or self.params['NEBULAR_EMISSION_CHECK'].lower() == 'no'): continue
	      tmpcut = (onedcuts[l][bxlo:bxhi]).copy()
	      #Find reference line index
              isValidLine = False
              templines = array(xlines)
              while (not isValidLine):
                refline = where(abs(templines-blref) == min(abs(templines-blref)))[0][0]
                if (validlines[refline]):
                   isValidLine = True
                else:
                   templines[refline] = -10000
	      #Use only valid lines for each slit, do linear fit
	      b = where(array(xoffsets)[:,l] > -9000)
              lsq = leastsq(linResiduals, [0.,0.], args=(array(xlines)[b],(array(xoffsets)[:,l])[b]))
	      tempxlines = (array(xlines)[b]).tolist()+[blref]
	      #Iterate over three brightest lines in the 101 pixel box
	      for z in range(3):
		tmppeak = where(tmpcut == max(tmpcut))[0][0]
		tmpbline = tmppeak+bxlo+xoff1[l]
		diff = abs(xoffsets[refline][l]-(xoff1[l]+blref-tmpbline))
		tempxoff = ((array(xoffsets)[:,l])[b]).tolist()+[xoff1[l]+blref-tmpbline]
		tmpsigma = (array(tempxoff)-(lsq[0][0]+lsq[0][1]*array(tempxlines))).stddev()
		if (z == 0):
		   minsigma = tmpsigma
		   mindiff = diff
		elif ((tmpsigma < minsigma and diff < mindiff) or tmpsigma < minsigma/3):
		   minsigma = tmpsigma
		   mindiff = diff
		   print 'Slit '+str(l)+':  Using skyline at '+str(tmppeak+bxlo)+' instead of '+str(bline[l]-xoff1[l])
		   f = open(self.logfile,'ab')
		   f.write('File: '+self.filenames[j]+', slit '+str(l)+': Using skyline at '+str(tmppeak+bxlo)+' instead of '+str(bline[l]-xoff1[l])+'.\n')
		   f.close()
	 	   f = open(self.warnfile,'ab')
		   f.write('WARNING:  Using skyline at '+str(tmppeak+bxlo)+' instead of '+str(bline[l]-xoff1[l])+'.\n')
		   f.write('File: '+self.filenames[j]+', slit '+str(l)+'\n')
	           f.write('Time: '+str(datetime.today())+'\n\n')
		   f.close()
		   bline[l] = tmpbline
		tmpcut[max(0,tmppeak-10):min(tmppeak+11,101)] = 0
	   #Use 21-px box around line in ref frame for x-corr
           cref = onedcuts[ref][blref-10-xoff1[ref]:blref+11-xoff1[ref]]
	   xoff2 = []
	   #print (bline)-array(xoff1)
	   #Cross-correlate ref with 21-px search box in each slit
	   for l in range(len(bline)):
	      xlo = bline[l]-xoff1[l]-10
	      #ccor = scipy.signal.correlate(cref-arraymedian(cref),onedcuts[l][xlo:xlo+21]-arraymedian(onedcuts[l][xlo:xlo+21]),mode='same')
              ccor = conv.correlate(cref-arraymedian(cref),onedcuts[l][xlo:xlo+21]-arraymedian(onedcuts[l][xlo:xlo+21]),mode='same')
	      ccor = array(ccor)
              p = zeros(4)+0.
              p[0] = max(ccor)
              p[1] = 10
              p[2] = 5
              p[3] = 0
              p = Numeric.array(p.tolist());
              lsq = leastsq(gaussResiduals, p, args=(arange(len(ccor)), ccor))
              mcor = lsq[0][1]
	      #mcor = where(ccor == max(ccor))[0][0]
	      xoff2.append(xoff1[l]-bline[l]+bline[ref]+10-mcor)
	      #print 0,l,mcor,xoff2[l]
	   #Store offsets, line, update validxs, blank out line
	   xoffsets.append(array(xoff2))
	   xoff2 = (array(xoff2)+0.5).astype("Int32")
	   xlines.append(blref)
	   validlines.append(True)
           pxmin = 25
           pxmax = len(onedcuts[ref])-25
	   xlo = int(pxmin+(pxmax-pxmin)*k/nseg)
	   xhi = int(pxmin+(pxmax-pxmin)*(k+1)/nseg)
	   validxs = arange(xhi-xlo+1)+xlo
	   validxs = concatenate((validxs[where(validxs < blref-5-xoff1[ref])], validxs[where(validxs > blref+5-xoff1[ref])]))
	   for l in range(len(onedcuts)):
	      onedcuts[l][blref-5-xoff2[l]:blref+6-xoff2[l]] = 0
	   #Loop over other lines until NLINES and < SIGMA are reached
	   inloop = 0
	   while (nlines < self.params['MOS_ALIGN_NLINES'][k] and inloop<10):
	      #print "Pass "+str(k)+", Line "+str(nlines)+" Inloop: "+str(inloop)
	      #Find next brightest line
	      blref = validxs[where(onedcuts[ref][validxs] == max(onedcuts[ref][validxs]))[0][0]]+xoffinit[ref]
	      #Setup box sizes
	      refbox = 25
	      searchbox = 25
	      xoff2 = []
	      isValidLine = False
	      templines = array(xlines)
	      while (not isValidLine):
		refline = where(abs(templines-blref) == min(abs(templines-blref)))[0][0]
		if (validlines[refline]):
		   isValidLine = True
		else:
		   templines[refline] = -10000
	      if (abs(xlines[refline]-blref) < 13):
		onedcuts[ref][blref-1-xoff1[ref]:blref+2-xoff1[ref]] = 0
		inloop+=1
		continue
	      if (abs(xlines[refline]-blref) < 50):
		searchbox = 10
		refbox = 10
	      #Choose nearest aligned line for initial guess
	      xoff1 = (xoffsets[refline]+0.5).astype("Int32")
	      cref = onedcuts[ref][blref-refbox-xoff1[ref]:blref+refbox+1-xoff1[ref]]
	      crefmed = arraymedian(onedcuts[ref][blref-25-xoff1[ref]:blref+26-xoff1[ref]])
	      #Skip line if next to zeros
	      if (len(where(cref[refbox-7:refbox+8] == 0)[0]) > 1):
		onedcuts[ref][blref-1-xoff1[ref]:blref+2-xoff1[ref]] = 0
		if (nlines > 5): inloop+=1
		else: inloop+=0.1
		continue
	      sigpts = concatenate((where(onedcuts[ref][blref-xoff1[ref]-2*searchbox:blref-xoff1[ref]-5] != 0)[0]+blref-xoff1[ref]-2*searchbox, where(onedcuts[ref][blref-xoff1[ref]+6:blref-xoff1[ref]+2*searchbox+1] != 0)[0]+blref-xoff1[ref]+6))
	      if (len(where(sigpts+xoff1[ref] < blref)[0]) <= 3 or len(where(sigpts+xoff1[ref] > blref)[0]) <= 3):
		onedcuts[ref][blref-1-xoff1[ref]:blref+2-xoff1[ref]] = 0
		if (nlines > 5): inloop+=1
                else: inloop+=0.1
		continue	
              lsigma = (onedcuts[ref][blref-xoff1[ref]]-arraymedian(onedcuts[ref][sigpts]))/onedcuts[ref][sigpts].stddev()
	      if (lsigma < self.params['MOS_ALIGN_SIGMA'][k]):
		onedcuts[ref][blref-1-xoff1[ref]:blref+2-xoff1[ref]] = 0
		if (nlines > 5): inloop+=1
                else: inloop+=0.1
		continue

	      if (lsigma > 4 and nlines <= 5 and abs(xlines[refline]-blref) > 200):
		#Find brightest line in 51 px box around initial guess
                for l in range(len(sylo)):
                   xlo = blref-xoff1[l]-25
		   if (xlo < 25 or xlo > len(onedcuts[l])-75):
		      continue
                   bline = where(onedcuts[l][xlo:xlo+51] == max(onedcuts[l][xlo:xlo+51]))[0][0]
		   xoff1[l]=xoff1[l]+25-bline

	      #Cross correlate refbox with searchbox in ref slit for check
	      xlo = blref-searchbox-xoff1[ref]
	      #ccor = scipy.signal.correlate(cref,onedcuts[ref][xlo:xlo+searchbox*2+1], mode='same')
              ccor = conv.correlate(cref,onedcuts[ref][xlo:xlo+searchbox*2+1], mode='same')
	      ccor = array(ccor)
	      mcor = where(ccor == max(ccor))[0][0]
	      #If ccor breaks, skip this line
	      if (abs(searchbox-mcor) > 1):
	         onedcuts[ref][blref-5-xoff1[ref]:blref+6-xoff1[ref]] = 0
		 inloop+=1
	         continue
              print nlines, blref, lsigma
      	      f = open(self.logfile,'ab')
      	      f.write("\tLine found: position "+str(blref-xoff1[ref])+" in slit "+str(ref)+", "+str(lsigma)+"sigma.\n") 
      	      f.close()
	      #Cross-correlate
	      tempmcors = []
              for l in range(len(xoff1)):
		xlo = blref-searchbox-xoff1[l]
		if (xlo < 25 or xlo > len(onedcuts[l])-2*searchbox-25):
		   xoff2.append(-10000)
		   tempmcors.append(0)
		   continue
		temp = onedcuts[l][xlo:xlo+searchbox*2+1]
		#ccor = scipy.signal.correlate(temp-arraymedian(temp),cref-crefmed,mode='same')
                ccor = conv.correlate(temp-arraymedian(temp),cref-crefmed,mode='same')
                ccor = array(ccor)
                p = zeros(4)+0.
                p[0] = max(ccor)
                p[1] = where(ccor == max(ccor))[0][0]
                p[2] = 2
		p[3] = arraymedian(ccor)
		llo = max(0,int(p[1]-5))
		lhi = min(len(ccor),int(p[1]+6))
                p = Numeric.array(p.tolist());
                lsq = leastsq(gaussResiduals, p, args=(arange(lhi-llo)+llo, ccor[llo:lhi]))
		if (abs(where(ccor == max(ccor))[0][0] - lsq[0][1]) > 1):
		   nmiss+=1
		mcor = lsq[0][1]
		tempmcors.append(mcor)
	        xoff2.append(xoff1[l]+searchbox-mcor)
	        #if (mcor != searchbox): print nlines,l,mcor,xoff2[l]
		#print nlines,l,mcor,xoff2[l]
	      #If ccor unsuccessful for any one slit, skip line
	      b = where(array(xoff2) != -10000)
	      if (len(b[0]) == 1):
                onedcuts[ref][blref-5-xoff1[ref]:blref+6-xoff1[ref]] = 0
		inloop+=1
                f = open(self.logfile,'ab')
                f.write('\t\tLine thrown out because only found in one slit!\n')
                f.close()
                f = open(self.warnfile,'ab')
                f.write("WARNING: Line found at position "+str(blref-xoff1[ref])+" in slit "+str(ref)+"\n")
                f.write("\thas been thrown out because it was not found in other slits\n")
                f.write('Time: '+str(datetime.today())+'\n\n')
		continue
	      if (max(abs(array(xoff2)[b]-array(xoff1)[b])) > abs(xlines[refline]-blref)/10.):
		onedcuts[ref][blref-5-xoff1[ref]:blref+6-xoff1[ref]] = 0
                inloop+=1
		f = open(self.logfile,'ab')
		f.write('\t\tLine thrown out because one or more slits do not match!\n')
		f.close()
        	f = open(self.warnfile,'ab')
        	f.write("WARNING: Line found at position "+str(blref-xoff1[ref])+" in slit "+str(ref)+"\n")
		f.write("\thas been thrown out due to a failure to match up with\n")
		f.write("\tline in slit "+str(where(abs(array(xoff2)[b]-array(xoff1)[b]) == max(abs(array(xoff2)[b]-array(xoff1)[b])))[0][0])+'\n')
        	f.write('Time: '+str(datetime.today())+'\n\n')
        	f.close()
		continue
	      #Fit expected vs actual offsets with line, find outliers
	      lsq = leastsq(linResiduals, [0.,0.], args=(array(xoff2)[b], array(tempmcors)[b]))
	      offresid = abs(array(tempmcors)[b]-(lsq[0][0]+lsq[0][1]*array(xoff2)[b]))
	      for l in range(len(offresid)):
		if (offresid[l] > 5):
		   xoff2[b[0][l]] = -20000
	      nlines+=1
	      inloop=0
	      #print xoff2
	      #Blank out line, update xoffsets, xlines, validxs
              xoffsets.append(array(xoff2))
              xlines.append(blref)
	      xoff2 = (array(xoff2)+0.5).astype("Int32")
	      for l in range(len(onedcuts)):
	        if (xoff2[l] != -10000):
		   onedcuts[l][blref-5-xoff2[l]:blref+6-xoff2[l]] = 0
	      if (len(b[0]) == len(xoff2)):
		validlines.append(True)
	      else:
		validlines.append(False)
	      validxs = concatenate((validxs[where(validxs < blref-5-xoff1[ref])], validxs[where(validxs > blref+5-xoff1[ref])]))
	#Setup xin, xout (refxs)
	print array(xlines)[where(validlines)]
	print "   Calculating Transformation..."
        f = open(self.logfile,'ab')
        f.write("\tFound "+str(len(xlines))+" lines.\n")
	f.write("\tCalculating Transformation...\n")
        f.close()
	refxs = []
	xin = []
	for l in range(len(xlines)):
	   refxs.append(xlines[l]-xoffsets[l][ref]+xoffsets[0][ref]-xlines[0])
	refxs = array(refxs)
	for l in range(len(sylo)):
	   temp = []
	   for k in range(len(xlines)):
	      temp.append(xlines[k]-xoffsets[k][l]+xoffsets[0][l]-xlines[0])
	   xin.append(array(temp))
        order = self.params['MOS_ALIGN_FIT_ORDER']
        xout = zeros(image[0].data.shape)+0.
	fits = []
	for l in range(len(sylo)):
	   xs = arange(image[0].data.shape[1])+xoffsets[0][l]-xlines[0]+0.
	   v = where(xin[l] < 8000)
	   p = zeros(order+1)
           p[1] = 1
	   if (len(refxs[v]) > 3):
              p = Numeric.array(p.tolist());
	      lsq = leastsq(polyResiduals, p, args=(xin[l][v], refxs[v], order))
	   else:
	      lsq = [p]
	   xout[int(sylo[l]):int(syhi[l]+1),:] = 0
	   temp1 = zeros(len(refxs[v]))+0.-refxs[v]
	   for k in range(len(lsq[0])):
              temp1+=lsq[0][k]*(xin[l][v]+0.)**k
	      xout[int(sylo[l]):int(syhi[l]+1),:]+=lsq[0][k]*xs**k
	   print temp1.mean(), temp1.stddev()
           f = open(self.logfile,'ab')
           f.write("\tSlit "+str(l)+" fit: "+str(lsq[0])+"\n")
	   f.write("\t\tData - fit    mean: "+str(temp1.mean())+"    sigma: "+str(temp1.stddev())+"\n")
           f.close()
	   if (temp1.stddev() != 0):
	      bad = where(abs(temp1-temp1.mean())/temp1.stddev() > 2)
	      if (len(bad[0]) > 0):
		good = where(abs(temp1-temp1.mean())/temp1.stddev() <= 2)
		p = zeros(order+1)
		p[1] = 1
                p = Numeric.array(p.tolist());
		lsq = leastsq(polyResiduals, p, args=(xin[l][v][good], refxs[v][good], order))
		print "REDO: ",lsq[0]
		xout[int(sylo[l]):int(syhi[l]+1),:] = 0
		temp1 = zeros(len(refxs[v][good]))+0.-refxs[v][good]
		for k in range(len(lsq[0])):
		   temp1+=lsq[0][k]*(xin[l][v][good]+0.)**k
		   xout[int(sylo[l]):int(syhi[l]+1),:]+=lsq[0][k]*xs**k
		print temp1.mean(), temp1.stddev()
                f = open(self.logfile,'ab')
                f.write("\t\tThrowing away outliers and refitting.\n")
		f.write("\t\tNew fit: "+str(lsq[0])+"\n")
           	f.write("\t\tData - fit    mean: "+str(temp1.mean())+"    sigma: "+str(temp1.stddev())+"\n")
           	f.close()
	   fits.append(lsq[0])
        #Setup paramaters for drihizzle
 	print "   Applying Transformation..."
        f = open(self.logfile,'ab')
        f.write("\tApplying Transformation...\n")
        f.close()
        xmin = int(xout.min())
        xmax = int(xout.max())+2
	if (xmax-xmin > image[0].data.shape[0]*3):
           print "Unable to properly fit lines for "+self.filenames[j]
           f = open(self.logfile,'ab')
           f.write('Warning: '+"Unable to properly fit lines for "+self.filenames[j]+'.\n')
           f.close()
           f = open(self.warnfile,'ab')
           f.write('WARNING: '+"Unable to properly fit lines for "+self.filenames[j]+'.\n')
           f.write('Time: '+str(datetime.today())+'\n\n')
           f.close()
           continue
        intpart = floor(xout-xmin).astype("Int32")
        frac = xout-xmin-intpart
        mask = where(xout != 0, 1, 0)
        #Drihizzle3d
        image[0].data = drihizzle3d(image[0].data, xmax-xmin, intpart, frac, mask=mask, axis="X")
	expmap =  drihizzle3d(mask+0.,xmax-xmin,intpart,frac,mask=mask,axis="X")
	expmap += where(expmap == 0, 1, 0)
        image[0].data/=expmap
	#xoff2 = ((xoffsets[24]+0.5)*where(xoffsets[24] != -10000, 1, 0)).astype("Int32")
	#print xlines[24]
	#print xoff2
	#xoff2 -= min(xoff2)
        #xsize = image[0].data.shape[1]
	#newdata = zeros((image[0].data.shape[0],max(xoff2)-min(xoff2)+xsize+1))+0.
	#for l in range(len(xoff2)):
	   #newdata[int(sylo[l]):int(syhi[l]),xoff2[l]:xoff2[l]+xsize] = image[0].data[int(sylo[l]):int(syhi[l]),:]
	#image[0].data = newdata
	if (usingArcLamps):
           newlfile = 'mosAlignedSkylines/'+self.lampnames[j][self.lampnames[j].rfind('/')+1:]
	   if (os.access(newlfile, os.F_OK)):
	      os.unlink(newlfile)
	   image.writeto(newlfile)
	   if (self.guiMessages):
	      print "FILE: "+newlfile
	   self.lampnames[j] = newlfile
	   image.close()
           image = pyfits.open(self.cleanNMs[j])
           image[0].data = drihizzle3d(image[0].data, xmax-xmin, intpart, frac, mask=mask, axis="X")
           image[0].data/=expmap
	if (os.access('mosAlignedSkylines/clean_sky_'+self.filenames[j], os.F_OK)):
	   os.unlink('mosAlignedSkylines/clean_sky_'+self.filenames[j])
	image.writeto('mosAlignedSkylines/clean_sky_'+self.filenames[j])
	self.cleanNMs[j] = 'mosAlignedSkylines/clean_sky_'+self.filenames[j]
	image.close()
	if (self.guiMessages):
	   print "FILE: "+self.cleanNMs[j]
        #Read region file for shifted added image
        slitx = []
        slity = []
        slitw = []
        slith = []
	if (os.access(self.prefix+self.filenames[j]+'.reg', os.F_OK)):
	   f = open(self.prefix+self.filenames[j]+'.reg','rb')
	elif (os.access(self.regfile[j], os.F_OK)):
	   print "Using "+self.regfile[j]+" as region file for "+self.filenames[j]
	   f = open(self.logfile,'ab')
	   f.write("Using "+self.regfile[j]+" as region file for "+self.filenames[j]+'.\n')
	   f.close()
	   f = open(self.regfile[j], 'rb')
	elif (os.access(self.params['MOS_REGION_FILE'], os.F_OK)):
	   print "Using "+self.params['MOS_REGION_FILE']+" as region file for "+self.filenames[j]
	   f = open(self.logfile,'ab')
	   f.write("Using "+self.params['MOS_REGION_FILE']+" as region file for "+self.filenames[j]+".\n")
	   f.close()
	   f = open(self.params['MOS_REGION_FILE'])
	else:
	   print "No region file found for "+self.filenames[j]
	   f = open(self.logfile,'ab')
	   f.write('Warning: '+"No region file found for "+self.filenames[j]+'.\n')
	   f.close()
	   f = open(self.warnfile,'ab')
	   f.write('WARNING: '+"No region file found for "+self.filenames[j]+'.\n')
	   f.write('Time: '+str(datetime.today())+'\n\n')
	   f.close()
	   continue
        s = f.read().split('\n')
        f.close()
        try: s.remove('')
        except ValueError:
           junk = ''
        for l in range(len(s)):
           if (s[l].startswith("image;box") or s[l].startswith("box(")):
              temp=s[l][s[l].find('(')+1:-1]
              temp = temp.split(',')
              if (temp[-1].find(')') != -1):
                temp[-1] = temp[-1][:temp[-1].find(')')]
              slitx.append(float(temp[0]))
              slity.append(float(temp[1])-1)
              slitw.append(float(temp[2]))
              slith.append(float(temp[3]))
        sxmin = int(min(slitx))
        sxmax = int(max(slitx))+1
        sylo = array(slity)-array(slith)/2
        syhi = array(slity)+array(slith)/2
	#Read in image, re-setup drihizzle params
        image = pyfits.open(self.prefix+self.filenames[j])
        xout = zeros(image[0].data.shape)+0.
        for l in range(len(sylo)):
           xs = arange(image[0].data.shape[1])+xoffsets[0][l]-xlines[0]+0.
           xout[int(sylo[l]):int(syhi[l]+1),:] = 0.
           for k in range(len(fits[l])):
              xout[int(sylo[l]):int(syhi[l]+1),:]+=fits[l][k]*xs**k
        xmin = int(xout.min())
        xmax = int(xout.max())+2
        intpart = floor(xout-xmin).astype("Int32")
        frac = xout-xmin-intpart
        mask = where(xout != 0, 1, 0)
	image[0].data = drihizzle3d(image[0].data, xmax-xmin, intpart, frac, mask=mask, axis="X")
        expmap =  drihizzle3d(mask+0.,xmax-xmin,intpart,frac,mask=mask,axis="X")
        expmap += where(expmap == 0, 1, 0)
        image[0].data/=expmap
	image.writeto(wcfile)
	if (os.access('mosAlignedSkylines/clean_'+self.filenames[j], os.F_OK)):
	   os.unlink('mosAlignedSkylines/clean_'+self.filenames[j])
	if (os.access(self.cleanFrames[j], os.F_OK)):
	   image.close()
	   image = pyfits.open(self.cleanFrames[j])
	   image[0].data = drihizzle3d(image[0].data, xmax-xmin, intpart, frac, mask=mask, axis="X")
	   image[0].data/=expmap
	else:
	   print "Warning: Clean frame not found for image "+self.filenames[j]+".  Using flat-fielded frame instead."
	   f = open(self.logfile,'ab')
	   f.write("Warning: Clean frame not found for image "+self.filenames[j]+".  Using flat-fielded frame instead.\n")
	   f.close()
	   f = open(self.warnfile,'ab')
	   f.write("WARNING: Clean frame not found for image "+self.filenames[j]+".  Using flat-fielded frame instead.\n")
	   f.write('Time: '+str(datetime.today())+'\n\n')
	   f.close()
        image.writeto('mosAlignedSkylines/clean_'+self.filenames[j])
        self.cleanFrames[j] = 'mosAlignedSkylines/clean_'+self.filenames[j]
	image.close()
        if (self.params['DO_NOISEMAPS'].lower() == "yes"):
           nmfile = "mosAlignedSkylines/mas_NM_"+self.filenames[j]
           if (os.access(nmfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(nmfile)
           if (not os.access(nmfile, os.F_OK)):
	      nm = pyfits.open(self.prefix+'NM_'+self.filenames[j])
	      nm[0].data = drihizzle3d(nm[0].data, xmax-xmin, intpart, frac, mask=mask, axis="X")
	      nm[0].data/=expmap
	      nm.writeto(nmfile)
	      nm.close
	expmapname = self.prefix[:self.prefix.rfind('/')+1]+'exp_'+self.filenames[j]
	if (os.access(expmapname, os.F_OK)):
	   image = pyfits.open(expmapname)
	   image[0].data = drihizzle3d(image[0].data, xmax-xmin, intpart, frac, mask=mask, axis="X")
           image[0].data/=expmap
           image.writeto('mosAlignedSkylines/exp_'+self.filenames[j])
	   self.expFrames[j] = 'mosAlignedSkylines/exp_'+self.filenames[j]
	   image.close()
	else:
	   print "Warning: Exposure map not found for "+self.filenames[j]
	   f = open(self.logfile,'ab')
	   f.write("Warning: Exposure map not found for "+self.filenames[j]+".\n")
	   f.close()
	   f = open(self.warnfile,'ab')
	   f.write("WARNING: Exposure map not found for "+self.filenames[j]+".\n")
           f.write('Time: '+str(datetime.today())+'\n\n')
           f.close()
	if (self.guiMessages):
	   print "FILE: "+wcfile
	   print "FILE: "+self.cleanFrames[j]
	   if (self.params['DO_NOISEMAPS'].lower() == "yes"):
	      print "FILE: "+nmfile
	   if (os.access(expmapname, os.F_OK)):
	      print "FILE: "+expFrames[j]
      self.oldpfix = self.prefix
      self.prefix = 'mosAlignedSkylines/mas_'

   def wavelengthCalibrate(self):
      print self.prefix
      print self.filenames
      print "CLEAN:"
      try:
	self.cleanNMs
      except Exception:
	try:
	   self.cleanNMs = self.lampfiles
	except Exception:
	   self.cleanNMs = []
      print self.cleanNMs
      print self.cleanFrames
      if (not os.access("wavelengthCalibrated",os.F_OK)):
        os.mkdir("wavelengthCalibrated",0755)
      print "Calibrating Wavelength Solution..."
      if (self.guiMessages): print "STATUS: Calibrating Wavelength Solution..."
      f = open(self.logfile,'ab')
      f.write("Calibrating Wavelength Solution...\n")
      f.close()
      #Check calib file
      if (not os.access(self.params['CALIBRATION_INFO_FILE'], os.F_OK)):
	print 'ERROR: Calibration file is missing.  See documentation.'
        print 'Wavelength calibration can not be performed!'
	f = open(self.logfile,'ab')
	f.write('ERROR: Calibration file is missing.  See documentation.\n')
	f.write('\tWavelength calibration can not be performed!\n')
	f.close()
        f = open(self.warnfile,'ab')
        f.write('ERROR: Calibration file is missing.  See documentation.\n')
        f.write('\tWavelength calibration can not be performed!\n')
        f.write('Time: '+str(datetime.today())+'\n\n')
        f.close()
	return
      #Read in calibration info file
      f = open(self.params['CALIBRATION_INFO_FILE'], 'rb')
      calib = f.read().split('\n')
      f.close()
      try: calib.remove('')
      except ValueError:
        junk = ''
      types = self.findTypes(self.filenames, self.params['DATA_TYPE'])
      #Loop over files
      for j in range(len(self.filenames)):
        if (self.guiMessages): print "PROGRESS: "+str(int(self.guiCRdone+self.guiCRf*(90+(j+0.)/len(self.filenames)*4)))
	print "    Processing object "+self.filenames[j]
        f = open(self.logfile,'ab')
        f.write("\tProcessing object "+self.filenames[j]+"\n")
        f.close()
	if (types[j] == 'longslit' and self.oldpfix != ''):
	   pfix = self.oldpfix
	else:
	   pfix = self.prefix
        wcfile = 'wavelengthCalibrated/wc_'+self.filenames[j]
        if (os.access(wcfile, os.F_OK) and self.params['OVERWRITE_FILES'].lower() == "yes"): os.unlink(wcfile)
        if (os.access(wcfile, os.F_OK)):
	   if (self.guiMessages):
	      print "FILE: "+wcfile
	      if (os.access("wavelengthCalibrated/clean_sky_"+self.filenames[j], os.F_OK)):
              	print "FILE: wavelengthCalibrated/clean_sky_"+self.filenames[j]
	      if (os.access("wavelengthCalibrated/wc_NM_"+self.filenames[j], os.F_OK)):
		print "FILE: wavelengthCalibrated/wc_NM_"+self.filenames[j]
	      if (os.access("wavelengthCalibrated/exp_"+self.filenames[j], os.F_OK)):
		print "FILE: wavelengthCalibrated/exp_"+self.filenames[j]
	      if (os.access("wavelengthCalibrated/clean_"+self.filenames[j], os.F_OK)):
		print "FILE: wavelengthCalibrated/clean_"+self.filenames[j]
           continue
	if (os.access('wavelengthCalibrated/wc_NM_'+self.filenames[j], os.F_OK)):
	   os.unlink('wavelengthCalibrated/wc_NM_'+self.filenames[j])
        if (os.access('wavelengthCalibrated/exp_'+self.filenames[j], os.F_OK)):
           os.unlink('wavelengthCalibrated/exp_'+self.filenames[j])
	#Find correct grism and filter
        if (self.params['WAVELENGTH_CALIBRATE_USE_ARCLAMPS'].lower() == "yes" and os.access(self.lampnames[j], os.F_OK)):
           image = pyfits.open(self.lampnames[j])
           usingArcLamps = True
	   print "Using ARCLAMP " + self.lampnames[j]
	   f = open(self.logfile,'ab')
	   f.write('Using ARCLAMP ' +self.lampnames[j]+'\n')
	   f.close()
        else:
           if (os.access(self.cleanNMs[j], os.F_OK)):
              image = pyfits.open(self.cleanNMs[j])
	      print "Using clean sky image " + self.cleanNMs[j]
              f = open(self.logfile,'ab')
              f.write('Using clean sky image ' +self.cleanNMs[j]+'\n')
              f.close()
           else:
              print "Can not find arc lamp or clean sky image to use for wavelength calibration."
	      print "File: "+self.filenames[j]
              f = open(self.logfile,'ab')
              f.write('Warning: Can not find arc lamp or clean sky image to use\n')
              f.write("for wavelength calibration of "+self.filenames[j]+"!\n")
              f.close()
              f = open(self.warnfile,'ab')
              f.write('WARNING: Can not find arc lamp or clean sky image to use\n')
              f.write("for wavelength calibration of "+self.filenames[j]+"!\n")
              f.write('Time: '+str(datetime.today())+'\n\n')
              f.close()
              continue
           usingArcLamps = False
	isMatch = False
	for l in range(len(calib)):
	   temp = calib[l].split()
	   isMatch = False
	   for i in range(len(self.params['FILTER_KEYWORD'])):
	      if (image[0].header[self.params['GRISM_KEYWORD']].lower() == temp[0].lower() and image[0].header[self.params['FILTER_KEYWORD']][i].lower() == temp[1].lower()):
		isMatch = True
		break
	   if (isMatch):
	      if (len(temp) < 8):
		print "Improper entry in calibration file at line "+str(l) 
		f = open(self.logfile,'ab')
		f.write("Warning: Improper entry in calibration file at line "+str(l)+"\n")
		f.close()
		f = open(self.warnfile,'ab')
		f.write("WARNING: Improper entry in calibration file at line "+str(l)+"\n")
		f.write('Time: '+str(datetime.today())+'\n\n')
                f.close()
		sfile = ''
		continue
	      minwave = float(temp[5])
	      maxwave = float(temp[6])
	      scale = abs(float(temp[2])-float(temp[3]))/float(temp[4])
	      sfile = temp[7]
	      isMatch = True
	   #if (self.filenames[j].startswith(temp[0])):
	   if (self.filenames[j].find(temp[0]) != -1):
	      if (len(temp) < 7):
                print "Improper entry in calibration file at line "+str(l)
                f = open(self.logfile,'ab')
                f.write("Warning: Improper entry in calibration file at line "+str(l)+"\n")
                f.close()
                f = open(self.warnfile,'ab')
                f.write("WARNING: Improper entry in calibration file at line "+str(l)+"\n")
                f.write('Time: '+str(datetime.today())+'\n\n')
                f.close()
		sfile = ''
                continue
	      minwave = float(temp[4])
	      maxwave = float(temp[5])
	      scale = abs(float(temp[1])-float(temp[2]))/float(temp[3])
	      sfile = temp[6]
	      isMatch = True
     	#Read in sky lines file
	if (not isMatch or not os.access(sfile, os.F_OK)):
	   print "No entry found in calibration file for "+self.filenames[j]
	   f = open(self.logfile,'ab')
	   f.write("Warning: No entry found in calibration file for "+self.filenames[j]+"\n")
	   f.close()
	   f = open(self.warnfile,'ab')
	   f.write("WARNING: No entry found in calibration file for "+self.filenames[j]+"\n")
	   f.write('Time: '+str(datetime.today())+'\n\n')
	   f.close()
      	f = open(sfile, 'rb')
      	s = f.read().split('\n')
        f.close()
      	try: s.remove('')
        except ValueError:
           junk = ''
      	masterWave = []
      	masterFlux = []
      	for l in range(len(s)):
	   if (s[l][0] != '#'):
	      temp = s[l].split()
              masterWave.append(float(temp[0]))
              masterFlux.append(float(temp[1]))
	#Adjust if units not in angstroms
      	masterWave = array(masterWave)*self.params['ANGSTROMS_PER_UNIT']
        masterFlux = array(masterFlux)
	minwave*=self.params['ANGSTROMS_PER_UNIT']
	maxwave*=self.params['ANGSTROMS_PER_UNIT']
	scale*=self.params['ANGSTROMS_PER_UNIT']
	#Setup arrays wc (data), dummy (fluxes), dwave (wavelengths)
	b = where(masterWave >= minwave, 1, 0)*where(masterWave <= maxwave, 1, 0)*where(masterFlux > 0, 1, 0)
	wave = compress(b, masterWave)
        flux = compress(b, masterFlux)
	dummy = zeros(int(maxwave)-int(minwave)+1)+0.
	dwave = zeros(int(maxwave)-int(minwave)+1)+0.
	for l in range(len(wave)):
	   index = int(wave[l]-minwave)
	   dummy[index-7:index+8] = flux[l]
	   dwave[index-7:index+8] = wave[l]
	wc = arraymedian(image[0].data, axis="Y")+0.
        #Correct for big negative values
        wc[where(wc < -100)] = 1.e-6
	if (len(dummy) < len(wc)):
	   print "Warning: Units seem wrong in calibration file!  Skipping!"
	   f = open(self.logfile,'ab')
	   f.write("Warning: Units seem wrong in calibration file!  Skipping!\n")
	   f.close()
	   f = open(self.warnfile,'ab')
	   f.write("WARNING: Units seem wrong in calibration file!  Skipping!\n")
	   f.write("\tFile: "+self.filenames[j]+"\n")
	   f.write('Time: '+str(datetime.today())+'\n\n')
	   f.close()
	   continue
	if (self.params['MEDIAN_FILTER_1D'].lower() == "yes"):
	   tempcut = zeros(len(wc))+0.
	   for k in range(25):
	      tempcut[k] = wc[k] - arraymedian(wc[:51])
	   for k in range(25,len(wc)-25):
	      tempcut[k] = wc[k] - arraymedian(wc[k-25:k+26])
	   for k in range(len(wc)-25,len(wc)):
	      tempcut[k] = wc[k] - arraymedian(wc[len(wc)-50:])
           wc = tempcut
	else:
	   wc[where(wc != 0)] -= arraymedian(wc)
        wc[where(wc == 0)] = 1.e-6
	#Apply scale to setup arrays d2, w2 = flux, lambda
	d2 = zeros(int(len(dummy)/scale)+1)+0.
	w2 = zeros(int(len(dwave)/scale)+1)+0.
	for l in range(len(d2)):
	   d2[l] = arraymedian(dummy[int(l*scale):int((l+1)*scale)+1])
	   w2[l] = arraymedian(dwave[int(l*scale):int((l+1)*scale)+1])
	#Find 2 brightest lines in image, 10 brightest in template
	wclines = []
	temp = wc+0.
	for l in range(2):
	   blref = where(temp == max(temp))[0][0]
	   wclines.append(blref)
	   temp[blref-10:blref+11] = 0
	dlines = []
	temp = d2+0.
	for l in range(10):
	   blref = where(temp == max(temp))[0][0]
	   dlines.append(blref)
	   temp[max(0,blref-10):min(len(temp),blref+11)] = 0
	#Match up 2 lines in image with corresponding lines in template
	xlines = []
	wlines = []
	for i in range(len(wclines)):
	   icor = 0
	   for l in range(len(dlines)):
	      if (dlines[l] < 100): continue
	      wcref = wc[wclines[i]-100:wclines[i]+101]
	      d2ref = d2[dlines[l]-100:dlines[l]+101]
	      wcref -= arraymedian(wcref)
	      d2ref -= arraymedian(d2ref)
	      n = min(len(wcref), len(d2ref))
	      wcref = wcref[:(n+1)]
	      d2ref = d2ref[:(n+1)]
	      #ccor = scipy.signal.correlate(wcref, d2ref, mode='same')
              ccor = conv.correlate(wcref, d2ref, mode='same')
	      replace = False 
	      if (max(ccor) > icor and i == 0): replace = True
	      if (i == 1):
		tempScale = (wlines[0]-w2[dlines[l]]+0.)/(xlines[0]-wclines[i])
		#print i,l,wclines[i],dlines[l],max(ccor),tempScale
	        if (max(ccor) > icor and tempScale/scale > 0.67 and tempScale/scale < 1.33): replace = True 
	      if (replace):
		icor = max(ccor)
		ccor = array(ccor)
		p = zeros(4)+0.
		p[0] = max(ccor)
		p[1] = where(ccor == max(ccor))[0][0]
		p[2] = 2
		p[3] = arraymedian(ccor)
		llo = max(0, int(p[1]-5))
		lhi = min(len(ccor), int(p[1]+6))
                p = Numeric.array(p.tolist());
		lsq = leastsq(gaussResiduals, p, args=(arange(lhi-llo)+llo, ccor[llo:lhi]))
		mcor = lsq[0][1]
		#Centroid line
		p = zeros(4)+0.
        	p[0] = max(wc[wclines[i]-5:wclines[i]+6])
        	p[1] = wclines[i]
        	p[2] = 2
        	p[3] = arraymedian(wc[wclines[i]-5:wclines[i]+6])
                p = Numeric.array(p.tolist());
        	lsq = leastsq(gaussResiduals, p, args=(arange(11)+wclines[i]-5, wc[wclines[i]-5:wclines[i]+6]))
		tempxline = lsq[0][1]
		tempwline = dlines[l]
	   d2[tempwline-15:tempwline+16] = 0.
	   wc[int(tempxline-15):int(tempxline+16)] = 0.
	   wlines.append(w2[tempwline])
	   xlines.append(tempxline)
	#Refine scale, d2, w2 scaled dummy arrays
	print wlines,xlines
	if (xlines[0] == xlines[1]):
	   print "Could not match two brightest lines!"
	   f = open(self.logfile,'ab')
	   f.write('WARNING: Was not able to correctly match lines!  File: '+self.filenames[j]+'\n')
           f.close()
           f = open(self.warnfile,'ab')
           f.write('WARNING: Was not able to correctly match lines!  File: '+self.filenames[j]+'\n')
           f.write('Time: '+str(datetime.today())+'\n\n')
           f.close()
           continue
	scale = (wlines[0]-wlines[1]+0.)/(xlines[0]-xlines[1])
	print scale
        f = open(self.logfile,'ab')
        f.write("\tFirst two lines matched.\n")
	f.write("\tWavelengths: "+str(wlines)+"\n")
	f.write("\tPixel Values: "+str(xlines)+"\n")
	f.write("\tGuess at scale (A/pixel): "+str(scale)+"\n")
        f.close()
	if (scale < 0):
	   print "Scale is < 0!  Was not able to correctly match lines!"
	   f = open(self.logfile,'ab')
	   f.write('Warning: Scale is < 0!  Was not able to correctly match lines!\n')
	   f.close()
	   f = open(self.warnfile,'ab')
	   f.write('WARNING: Scale is < 0!  Was not able to correctly match lines!\n')
	   f.write('Time: '+str(datetime.today())+'\n\n')
	   f.close()
	   continue
	d2 = zeros(int(len(dummy)/scale)+1)+0.
	w2 = zeros(int(len(dwave)/scale)+1)+0.
	for l in range(len(d2)):
	   d2[l] = arraymedian(dummy[int(l*scale):int((l+1)*scale)+1])
	   w2[l] = arraymedian(dwave[int(l*scale):int((l+1)*scale)+1])
	findLines = True
	inloop = 0
	xlo = int(max(min(xlines)-200, 50))
	xhi = int(min(max(xlines)+200, len(wc)-51))
	#Loop over other lines in +/- 200 px area
	nlines = 2
	endIter = False
	while (findLines and inloop < 20):
	   blref = where(wc[xlo:xhi+1] == max(wc[xlo:xhi+1]))[0][0]+xlo
	   refbox = 25
	   searchbox = 25
	   templines = array(xlines)
	   refline = where(abs(templines-blref) == min(abs(templines-blref)))[0][0]
	   if (len(where(w2 == wlines[refline])[0]) == 0):
	      print "Unable to perform wavelength calibration for dataset "+self.filenames[j]
	      f = open(self.logfile,'ab')
	      f.write("Unable to perform wavelength calibration for dataset "+self.filenames[j]+'\n')
	      f.close()
	      f = open(self.warnfile,'ab')
	      f.write("Unable to perform wavelength calibration for dataset "+self.filenames[j]+'\n')
	      f.write("Data may be too noisy.\n")
	      f.write('Time: '+str(datetime.today())+'\n\n')
              f.close()
	      endIter = True
	      break
	   xoff1 = int(where(w2 == wlines[refline])[0][0] - xlines[refline])
	   wcref = wc[blref-refbox:blref+refbox+1]
	   #Skip line if next to zeros or low sigma
	   if (len(where(wcref[refbox-7:refbox+8] == 0)[0]) > 1):
	      wc[blref-1:blref+2] = 0.
	      if (nlines > 3): inloop+=1
	      else: inloop+=0.1
	      continue
	   if (len(where(d2[blref+xoff1-7:blref+xoff1+8] != 0)[0]) == 0):
	      wc[blref-7:blref+8] = 0.
	      continue
	   sigpts = concatenate((where(wc[blref-2*searchbox:blref-5] != 0)[0]+blref-2*searchbox, where(wc[blref+6:blref+2*searchbox+1] != 0)[0]+blref+6))
	   if (len(where(sigpts < blref)[0]) <= 3 or len(where(sigpts > blref)[0]) <= 3 ):
	      wc[blref-1:blref+2] = 0.
	      if (nlines > 3): inloop+=1
	      else: inloop+=0.1
	      continue
	   lsigma = (wc[blref]-arraymedian(wc[sigpts]))/wc[sigpts].stddev()
	   if (lsigma < self.params['CALIBRATION_MIN_SIGMA']):
	      wc[blref-1:blref+2] = 0.
	      if (nlines > 3): inloop+=1
	      else: inloop+=0.1
	      continue
	   #Cross-correlate
	   d2ref = d2[blref-searchbox+xoff1:blref+searchbox+1+xoff1]
	   n = min(len(wcref),len(d2ref))
	   wcref = wcref[:(n+1)]
	   d2ref = d2ref[:(n+1)]
	   #ccor = scipy.signal.correlate(wcref, d2ref, mode='same')
           ccor = conv.correlate(wcref, d2ref, mode='same')
	   ccor = array(ccor)
	   p = zeros(4)+0.
	   p[0] = max(ccor)
	   p[1] = where(ccor == max(ccor))[0][0]
	   p[2] = 2
	   p[3] = arraymedian(ccor)
	   llo = max(0, int(p[1]-5))
	   lhi = min(len(ccor), int(p[1]+6))
           p = Numeric.array(p.tolist());
	   lsq = leastsq(gaussResiduals, p, args=(arange(lhi-llo)+llo, ccor[llo:lhi]))
	   mcor = lsq[0][1]
	   if (mcor > 50 or mcor < 0):
	      wc[blref-1:blref+2] = 0.
	      if (nlines > 3): inloop+=1
	      else: inloop+=0.1
	      continue
	   #Centroid line
	   p = zeros(4)+0.
	   p[0] = max(wc[blref-5:blref+6])
	   p[1] = blref
	   p[2] = 2
	   p[3] = arraymedian(wc[blref-5:blref+6])
           p = Numeric.array(p.tolist());
	   lsq = leastsq(gaussResiduals, p, args=(arange(11)+blref-5, wc[blref-5:blref+6]))
	   tempxline = lsq[0][1]
	   d2[int(blref+xoff1+searchbox-mcor-10):int(blref+xoff1+searchbox-mcor+11)] = 0.
	   wc[int(tempxline-10):int(tempxline+11)] = 0.
	   wtemp = max(w2[int(blref+xoff1+searchbox-mcor-1):int(blref+xoff1+searchbox-mcor+2)])
	   diff = abs(xlines[refline] - tempxline)
	   if (abs(mcor-refbox) > 0.05*diff or wtemp == 0):
	      continue
	   nlines+=1
	   inloop = 0
	   xlines.append(tempxline)
	   wlines.append(wtemp)
	   print nlines, max(ccor), where(ccor == max(ccor)), mcor, lsq[0][1], wtemp
           f = open(self.logfile,'ab')
           f.write("\tLine found: pixel "+str(lsq[0][1])+", wavelength "+str(wtemp)+"\n")
           f.close()
	   if (max(wc[xlo:xhi+1]) == 0): findLines = False
	if (endIter):
	   continue
	#Expand search range out incrementally until entire image reached
	npass = 0
	while (xlo > 50 or xhi < len(wc)-51):
	   #Fit order 2 polynomial to current data
	   p =  zeros(3)+0.
	   p[1] = scale
           p = Numeric.array(p.tolist());
	   try:
	      lsq = leastsq(polyResiduals, p, args=(array(xlines), array(wlines), len(p)-1))
	   except Exception:
	      print "Unable to fit wavelength solution for "+self.filenames[j]
              f = open(self.logfile,'ab')
              f.write("Unable to fit wavelength solution for "+self.filenames[j]+"\n")
              f.close()
              f = open(self.warnfile,'ab')
              f.write("WARNING: Unable to fit wavelength solution for "+self.filenames[j]+"\n")
              f.write('Time: '+str(datetime.today())+'\n\n')
              f.close()
	   #Refine d2, w2 scaled arrays
	   temp = zeros(len(wc))+0.
	   for l in range(len(lsq[0])):
	      temp+=lsq[0][l]*arange(len(wc))**l
	   d2 = zeros(len(wc))+0.
	   w2 = zeros(len(wc))+0.
	   for l in range(len(temp)-1):
	      if (min(temp[l], temp[l+1]) > minwave and max(temp[l], temp[l+1] < maxwave)):
		d2[l] = arraymedian(dummy[int(temp[l]-minwave):int(temp[l+1]-minwave)])
		w2[l] = arraymedian(dwave[int(temp[l]-minwave):int(temp[l+1]-minwave)])
	   findLines = True
	   inloop = 0
	   xlo = int(max(min(xlines)-(250+npass*50), 50))
	   xhi = int(min(max(xlines)+(250+npass*50), len(wc)-51))
	   npass+=1
	   #Loop over other lines in area
	   while (findLines and inloop < 25):
	      blref = where(wc[xlo:xhi+1] == max(wc[xlo:xhi+1]))[0][0]+xlo
	      refbox = 25
	      searchbox = 25
	      templines = array(xlines)
	      refline = where(abs(templines-blref) == min(abs(templines-blref)))[0][0]
	      #Choose another reference line
	      if (len(where(w2 == wlines[refline])[0]) == 0):
		isValidLine = False
	        templines[refline] = -10000
		while (not isValidLine):
		   refline = where(abs(templines-blref) == min(abs(templines-blref)))[0][0]
		   if (len(where(w2 == wlines[refline])[0]) != 0):
		      isValidLine = True
		   else:
		      templines[refline] = -10000
	      xoff1 = int(where(w2 == wlines[refline])[0][0] - xlines[refline])
	      wcref = wc[blref-refbox:blref+refbox+1]
	      #Skip line if next to zeros or low sigma
	      if (len(where(wcref[refbox-7:refbox+8] == 0)[0]) > 1):
	        wc[blref-1:blref+2] = 0.
        	if (nlines > 5): inloop+=1
		else: inloop+=0.1
        	continue
	      if (len(where(d2[blref+xoff1-7:blref+xoff1+8] != 0)[0]) == 0):
	        wc[blref-7:blref+8] = 0.
                if (nlines > 5): inloop+=1
                else: inloop+=0.1
        	continue
	      sigpts = concatenate((where(wc[blref-2*searchbox:blref-5] != 0)[0]+blref-2*searchbox, where(wc[blref+6:blref+2*searchbox+1] != 0)[0]+blref+6))
	      if (len(where(sigpts < blref)[0]) <=3 or len(where(sigpts > blref)[0]) <=3):
        	wc[blref-1:blref+2] = 0
        	if (nlines > 5): inloop+=1
		else: inloop+=0.1
        	continue
	      lsigma = (wc[blref]-arraymedian(wc[sigpts]))/wc[sigpts].stddev()
	      if (lsigma < self.params['CALIBRATION_MIN_SIGMA']):
	        wc[blref-1:blref+2] = 0
        	if (nlines > 5): inloop+=1
                else: inloop+=0.1
        	continue
	      #Cross-correlate
	      d2ref = d2[blref-searchbox+xoff1:blref+searchbox+1+xoff1]
	      n = min(len(wcref),len(d2ref))
	      wcref = wcref[:(n+1)]
	      d2ref = d2ref[:(n+1)]
	      #ccor = scipy.signal.correlate(wcref, d2ref, mode='same')
              ccor = conv.correlate(wcref, d2ref, mode='same')
	      ccor = array(ccor)
	      p = zeros(4)+0.
	      p[0] = max(ccor)
	      p[1] = where(ccor == max(ccor))[0][0]
	      p[2] = 2
	      p[3] = arraymedian(ccor)
	      llo = max(0, int(p[1]-5))
	      lhi = min(len(ccor), int(p[1]+6))
              p = Numeric.array(p.tolist());
	      lsq = leastsq(gaussResiduals, p, args=(arange(lhi-llo)+llo, ccor[llo:lhi]))
	      mcor = lsq[0][1]
	      #Centroid line
	      p = zeros(4)+0.
	      p[0] = max(wc[blref-5:blref+6])
	      p[1] = blref
	      p[2] = 2
	      p[3] = arraymedian(wc[blref-5:blref+6])
              p = Numeric.array(p.tolist());
	      lsq = leastsq(gaussResiduals, p, args=(arange(11)+blref-5, wc[blref-5:blref+6]))
	      tempxline = lsq[0][1]
	      d2[int(blref+xoff1+searchbox-mcor-10):int(blref+xoff1+searchbox-mcor+11)]=0.
	      wc[int(tempxline-10):int(tempxline+11)] = 0.
	      if (int(blref+xoff1+searchbox-mcor-1) < 0):
		continue
	      wtemp = max(w2[int(blref+xoff1+searchbox-mcor-1):int(blref+xoff1+searchbox-mcor+2)])
	      diff = abs(xlines[refline]-tempxline)
	      if (abs(mcor-refbox) > 0.05*diff or wtemp == 0):
        	continue
	      nlines+=1
	      inloop = 0
	      xlines.append(tempxline)
	      wlines.append(wtemp)
	      print nlines, max(ccor), where(ccor == max(ccor)), mcor, lsq[0][1], wtemp
              f = open(self.logfile,'ab')
              f.write("\tLine found: pixel "+str(lsq[0][1])+", wavelength "+str(wtemp)+"\n")
              f.close()
	#Fit polynomial to find wavelength solution
	xlines = array(xlines)
	wlines = array(wlines)
	order = self.params['CALIBRATION_FIT_ORDER']
	p = zeros(order+1)+0.
	p[1] = scale
        p = Numeric.array(p.tolist());
	if (len(xlines) == 1):
	   print "Error: Could not find enough lines to fit."
	   f = open(self.logfile,'ab')
	   f.write("Error: Could not find enough lines to fit.")
	   f.close()
	   return
	lsq = leastsq(polyResiduals, p, args=(xlines, wlines, order))
	fitlines = zeros(len(xlines))+0. - wlines
	for l in range(len(lsq[0])):
	   fitlines+=lsq[0][l]*xlines**l
	print lsq[0]
        print fitlines.mean(), fitlines.stddev()
        f = open(self.logfile,'ab')
        f.write("\tFound "+str(len(xlines))+" lines.\n")
        f.write("\tFit: "+str(lsq[0])+"\n")
        f.write("\tData - fit    mean: "+str(fitlines.mean())+"    sigma: "+str(fitlines.stddev())+"\n")
        f.close()
	bad = where(abs(fitlines-fitlines.mean())/fitlines.stddev() > 2)
	if (len(bad[0]) > 0):
	   good = where(abs(fitlines-fitlines.mean())/fitlines.stddev() <=2)
	   p = zeros(order+1)+0.
	   p[1] = scale
           p = Numeric.array(p.tolist());
	   lsq = leastsq(polyResiduals, p, args=(xlines[good], wlines[good], order))
	   fitlines = zeros(len(xlines[good]))+0. - wlines[good]
	   for l in range(len(lsq[0])):
	      fitlines+=lsq[0][l]*xlines[good]**l
	   print "REDO: ", lsq[0]
	   print fitlines.mean(), fitlines.stddev()
           f = open(self.logfile,'ab')
           f.write("\tThrowing away outliers and refitting.\n")
           f.write("\tNew fit: "+str(lsq[0])+"\n")
           f.write("\tData - fit    mean: "+str(fitlines.mean())+"    sigma: "+str(fitlines.stddev())+"\n")
           f.close()
	#Mark where skylines are at top and bottom of "clean sky" image
	image[0].data[0:20,:] = 0.
	image[0].data[-20:,:] = 0.
	for l in range(len(xlines)):
	   image[0].data[0:20,int(xlines[l]-0.5):int(xlines[l]+2.5)] = 10000.
	   image[0].data[-20:,int(xlines[l]-0.5):int(xlines[l]+2.5)] = 10000.
	#Update Fits Headers
        if (usingArcLamps):
           newlfile = 'wavelengthCalibrated/'+self.lampnames[j][self.lampnames[j].rfind('/')+1:]
           if (os.access(newlfile, os.F_OK)):
              os.unlink(newlfile)
           image[0].header.update('FILENAME',newlfile)
           image[0].header.update('HISTORY','Wavelength Calibrated')
           image[0].header.update('CTYPE1','WAVE-PLY')
           image[0].header.update('PORDER',order)
           for l in range(order+1):
	      image[0].header.update('PCOEFF_'+str(l),lsq[0][l])
           image.writeto(newlfile)
           self.lampnames[j] = newlfile
           image.close()
	   if (self.guiMessages):
	      print "FILE: "+newlfile
           image = pyfits.open(self.cleanNMs[j])
	image[0].header.update('FILENAME','clean_sky_'+self.filenames[j])
	image[0].header.update('HISTORY','Wavelength Calibrated')
	image[0].header.update('CTYPE1','WAVE-PLY')
	image[0].header.update('PORDER',order)
	for l in range(order+1):
	   image[0].header.update('PCOEFF_'+str(l),lsq[0][l])
	if (os.access('wavelengthCalibrated/clean_sky_'+self.filenames[j], os.F_OK)):
	   os.unlink('wavelengthCalibrated/clean_sky_'+self.filenames[j])
	image.writeto('wavelengthCalibrated/clean_sky_'+self.filenames[j])
	image.close()
	image = pyfits.open(pfix+self.filenames[j])
	image[0].header.update('FILENAME',wcfile)
	image[0].header.update('HISTORY','Wavelength Calibrated')
        image[0].header.update('CTYPE1','WAVE-PLY')
        image[0].header.update('PORDER',order)
        for l in range(order+1):
           image[0].header.update('PCOEFF_'+str(l),lsq[0][l])
	image.writeto(wcfile)
	image.close()
	if (self.params['DO_NOISEMAPS'].lower() == "yes" and os.access(pfix+'NM_'+self.filenames[j], os.F_OK)):
	   nm = pyfits.open(pfix+'NM_'+self.filenames[j])
           nm[0].header.update('FILENAME','wavelengthCalibrated/wc_NM_'+self.filenames[j])
           nm[0].header.update('HISTORY','Wavelength Calibrated')
           nm[0].header.update('CTYPE1','WAVE-PLY')
           nm[0].header.update('PORDER',order)
           for l in range(order+1):
              nm[0].header.update('PCOEFF_'+str(l),lsq[0][l])
           nm.writeto('wavelengthCalibrated/wc_NM_'+self.filenames[j])
           nm.close()
	if (len(self.expFrames) > j):
	   if (os.access(self.expFrames[j], os.F_OK)):
	      exp = pyfits.open(self.expFrames[j])
              exp[0].header.update('FILENAME','wavelengthCalibrated/exp_'+self.filenames[j])
              exp[0].header.update('HISTORY','Wavelength Calibrated')
              exp[0].header.update('CTYPE1','WAVE-PLY')
              exp[0].header.update('PORDER',order)
              for l in range(order+1):
		exp[0].header.update('PCOEFF_'+str(l),lsq[0][l])
              exp.writeto('wavelengthCalibrated/exp_'+self.filenames[j])
	      self.expFrames[j] = 'wavelengthCalibrated/exp_'+self.filenames[j]
              exp.close()
	if (len(self.cleanFrames) > j):
           if (os.access(self.cleanFrames[j], os.F_OK)):
              clean = pyfits.open(self.cleanFrames[j])
              clean[0].header.update('FILENAME','wavelengthCalibrated/clean_'+self.filenames[j])
              clean[0].header.update('HISTORY','Wavelength Calibrated')
              clean[0].header.update('CTYPE1','WAVE-PLY')
              clean[0].header.update('PORDER',order)
              for l in range(order+1):
                clean[0].header.update('PCOEFF_'+str(l),lsq[0][l])
	      if (os.access('wavelengthCalibrated/clean_'+self.filenames[j], os.F_OK)):
		os.unlink('wavelengthCalibrated/clean_'+self.filenames[j])
              clean.writeto('wavelengthCalibrated/clean_'+self.filenames[j])
              self.cleanFrames[j] = 'wavelengthCalibrated/clean_'+self.filenames[j]
              clean.close()
	if (self.guiMessages):
	   print "FILE: "+wcfile
	   print "FILE: wavelengthCalibrated/clean_sky_"+self.filenames[j]
	   if (os.access("wavelengthCalibrated/wc_NM_"+self.filenames[j], os.F_OK)):
	      print "FILE: wavelengthCalibrated/wc_NM_"+self.filenames[j]
	   if (os.access("wavelengthCalibrated/exp_"+self.filenames[j], os.F_OK)):
	      print "FILE: wavelengthCalibrated/exp_"+self.filenames[j]
	   if (os.access("wavelengthCalibrated/clean_"+self.filenames[j], os.F_OK)):
	      print "FILE: wavelengthCalibrated/clean_"+self.filenames[j]
      self.oldpfixls = self.oldpfix
      self.oldpfix = self.prefix
      self.prefix='wavelengthCalibrated/wc_'

   def extractSpectra(self, spectra):
      print "Extracting Spectra..."
      if (self.guiMessages): print "STATUS: Extracting Spectra..."
      f = open(self.logfile,'ab')
      f.write("Extracting Spectra...\n")
      f.close()
      if (not os.access("extractedSpectra",os.F_OK)):
        os.mkdir("extractedSpectra",0755)
      for j in range(len(spectra)):
	print "    Processing object "+self.filenames[j]+" with "+self.params['EXTRACT_WEIGHTING']+" weighting."
        f = open(self.logfile,'ab')
        f.write("\tProcessing object "+self.filenames[j]+" with "+self.params['EXTRACT_WEIGHTING']+" weighting.\n")
        f.close()
	esfile = 'extractedSpectra/es_'+self.filenames[j]
	tabfile = 'extractedSpectra/es_table_'+self.filenames[j]
	if (os.access(esfile, os.F_OK)):
	   continue
	if (not os.access(self.prefix+self.filenames[j], os.F_OK)):
	   print "Error: Can not find file "+self.prefix+self.filenames[j]
	   f = open(self.logfile,'ab')
	   f.write("Error: Can not find file "+self.prefix+self.filenames[j]+'\n')
	   f.close()
	   f = open(self.warnfile,'ab')
	   f.write("ERROR: Can not find file "+self.prefix+self.filenames[j]+'\n')
           f.close()
	   if (os.access(self.oldpfix+self.filenames[j], os.F_OK)):
	      print "Using "+self.oldpfix+self.filenames[j]+" instead"
	      f = open(self.logfile,'ab')
	      f.write("Using "+self.oldpfix+self.filenames[j]+" instead\n")
	      f.close()
	      f = open(self.warnfile,'ab')
	      f.write("Using "+self.oldpfix+self.filenames[j]+" instead\n")
              f.write('Time: '+str(datetime.today())+'\n\n')
              f.close()
	      image = pyfits.open(self.oldpfix+self.filenames[j])
	   elif (os.access(self.oldpfixls+self.filenames[j], os.F_OK)):
              print "Using "+self.oldpfixls+self.filenames[j]+" instead"
              f = open(self.logfile,'ab')
              f.write("Using "+self.oldpfixls+self.filenames[j]+" instead\n")
              f.close()
              f = open(self.warnfile,'ab')
              f.write("Using "+self.oldpfixls+self.filenames[j]+" instead\n")
              f.write('Time: '+str(datetime.today())+'\n\n')
              f.close()
              image = pyfits.open(self.oldpfixls+self.filenames[j])
	   else:
              f = open(self.warnfile,'ab')
              f.write('Time: '+str(datetime.today())+'\n\n')
              f.close()
	      continue
	else:
	   image = pyfits.open(self.prefix+self.filenames[j])
	if (len(spectra[j]) == 0):
           print 'WARNING: Unable to find any spectra in file '+self.prefix+self.filenames[j]
           f = open(self.logfile,'ab')
           f.write("ERROR: Unable to find any spectra in file "+self.prefix+self.filenames[j]+"\n")
	   f.write("\tUsing entire image to extract a spectrum.\n")
           f.close()
           f = open(self.warnfile,'ab')
           f.write("ERROR: Unable to find any spectra in file "+self.prefix+self.filenames[j]+"\n")
           f.write("\tUsing entire image to extract a spectrum.\n")
           f.write('Time: '+str(datetime.today())+'\n\n')
           f.close()
	   ys = []
	   temp = [0,image[0].data.shape[0]-1,0,image[0].data.shape[1]]
           ys.append(temp)
           ys = array(ys)
	   spectra[j] = ys 
	out = zeros((len(spectra[j]), image[0].data.shape[1]))+0.
        wave = zeros(image[0].data.shape[0])+0.
        if (not image[0].header.has_key('PORDER')):
           print 'Can not find header keyword PORDER in '+self.prefix+self.filenames[j]
	   print 'Wavelength scale will not be written to FITS table.'
           f = open(self.logfile,'ab')
           f.write("Can not find header keyword PORDER in "+self.prefix+self.filenames[j]+"\n")
	   f.write("Wavelength scale will not be written to FITS table.\n")
           f.close()
           f = open(self.warnfile,'ab')
           f.write("WARNING: Can not find header keyword PORDER in "+self.prefix+self.filenames[j]+"\n")
           f.write("Wavelength scale will not be written to FITS table.\n")
           f.write('Time: '+str(datetime.today())+'\n\n')
           f.close()
           continue
        for l in range(image[0].header['PORDER']+1):
           wave+=image[0].header['PCOEFF_'+str(l)]*arange(image[0].data.shape[0],type="Float64")**l
	columns = [pyfits.Column(name='Wavelength', format='D', array=wave)]
	for l in range(len(spectra[j])):
	   y1 = spectra[j][l][0]
	   y2 = spectra[j][l][1]
	   x1 = 0
	   x2 = image[0].data.shape[1]-1
	   if (self.params['EXTRACT_WEIGHTING'].lower() == 'linear'):
	      out[l,:] = sum(image[0].data[y1:y2+1,:],0)
	   elif (self.params['EXTRACT_WEIGHTING'].lower() == 'median'):
	      out[l,:] = arraymedian(image[0].data[y1:y2+1,:],axis="Y")
	   elif (self.params['EXTRACT_WEIGHTING'].lower() == 'gaussian'):
	      #temp = arraymedian(image[0].data[:,x1:x2+1], axis="X")
	      temp = zeros(len(image[0].data))+0.
	      for i in range(len(temp)):
		temp[i] = arraymedian(image[0].data[i,x1:x2+1][where(image[0].data[i,x1:x2+1] != 0)])
	      p = zeros(4)+0.
	      p[0] = max(temp[y1:y2+1])
	      p[1] = where(temp[y1:y2+1] == p[0])[0][0]+y1
	      hm = where(temp[y1:y2+1] > p[0]/2)[0]
	      try: p[2] = hm[-1]-hm[0]+1
	      except Exception: p[2] = 3
	      if (p[2] < 3):
		p[2] = max(abs(y2-y1)/4.,3)
	      p[2] = min(p[2], abs(y2-y1)/2.)
	      p[3] = min(temp[y1:y2+1])
	      print "P: ",p
              p = Numeric.array(p.tolist());
	      try:
		lsq = leastsq(gaussResiduals, p, args=(arange(y2-y1+21)+y1-10., temp[y1-10:y2+11]))
	      except ValueError:
		lsq = leastsq(gaussResiduals, p, args=(arange(y2-y1)+y1, temp[y1:y2])) 
	      print lsq[0]
	      xs = arange(y2-y1+1)+y1+0.
	      f = zeros(len(xs))+0.
	      z = (xs-lsq[0][1])/lsq[0][2]
	      f = p[3]+p[0]*math.e**(-z**2/2)
	      for i in range(image[0].data.shape[1]):
		image[0].data[y1:y2+1,i]*=f
	      out[l,:] = sum(image[0].data[y1:y2+1,:],0)
	   columns.append(pyfits.Column(name='Spectrum_'+str(l+1), format='D', array=out[l,:]))
	image[0].data = out
	image[0].header.update('FILENAME',esfile)
        image[0].header.update('HISTORY','Extracted with '+self.params['EXTRACT_WEIGHTING']+' weighting')
	image.writeto(esfile)
	image.close()
	tbhdu = pyfits.new_table(pyfits.ColDefs(columns))
	hdulist = pyfits.HDUList([tbhdu])
	hdulist.verify('silentfix')
	hdulist.writeto(tabfile)
      self.oldpfix = self.prefix
      self.prefix = 'extractedSpectra/es_'

   def calibStar(self):
      print "Dividing by calibration star..."
      if (self.guiMessages): print "STATUS: Dividing by calibration star..."
      f = open(self.logfile,'ab')
      f.write("Dividing by calibration star...\n")
      f.close()
      if (not os.access("calibStarDivided",os.F_OK)):
        os.mkdir("calibStarDivided",0755)
      csfile = self.params['CALIBRATION_STAR_FILE']
      if (not os.access(csfile, os.F_OK)):
	print 'Calibration star file missing!'
        f = open(self.logfile,'ab')
        f.write("Calibration star file "+csfile+" missing!\n")
        f.close()
        f = open(self.warnfile,'ab')
        f.write('WARNING: Calibration star file '+csfile+' missing!\n')
        f.write('Time: '+str(datetime.today())+'\n\n')
        f.close()
	return
      frames = []
      starpfix = []
      if (csfile.lower().endswith('.fits')):
	for j in range(len(self.filenames)):
	   frames.append(self.filenames[j])
	   starpfix.append(csfile)
      else:
	f = open(csfile, 'rb')
	s = f.read().split('\n')
	f.close()
	try: s.remove('')
        except ValueError:
           junk = ''
	for j in range(len(s)):
	   temp = s[j].split()
	   for l in range(len(self.filenames)):
	      if (self.filenames[j].find(temp[0]) != -1):
		frames.append(self.filenames[j])
	        starpfix.append(temp[1])
      for j in range(len(frames)):
        csdfile = 'calibStarDivided/csd_'+frames[j]
        if (os.access(csdfile, os.F_OK)):
           continue
	if (os.access(starpfix[j], os.F_OK)):
	   cstarname = starpfix[j]
	else:
	   i = -1
	   for l in range(len(self.filenames)):
	      if (self.filenames[l].find(starpfix[j]) != -1):
		i = l
		break
	   if (i == -1):
	      print 'Calibration star not found!'
              f = open(self.logfile,'ab')
              f.write("Calibration star "+starpfix[j]+" not found!\n")
              f.close()
              f = open(self.warnfile,'ab')
              f.write("WARNING: Calibration star "+starpfix[j]+" not found!\n")
              f.write('Time: '+str(datetime.today())+'\n\n')
              f.close()
	      continue
	   else:
	      cstarname = self.prefix+self.filenames[i]
	cstar = pyfits.open(cstarname)
	if (len(cstar[0].data.shape) > 1):
	   cstar[0].data = cstar[0].data[0]
	xcstar = zeros(cstar[0].data.shape[0])+0.
        if (not cstar[0].header.has_key('PORDER')):
	   print 'Can not find header keyword PORDER in calibration star '+cstarname
           f = open(self.logfile,'ab')
           f.write("Can not find header keyword PORDER in calibration star "+cstarname+"\n")
           f.close()
           f = open(self.warnfile,'ab')
           f.write("WARNING: Can not find header keyword PORDER in calibration star "+cstarname+"\n")
           f.write('Time: '+str(datetime.today())+'\n\n')
           f.close()
           continue
	for l in range(cstar[0].header['PORDER']+1):
           xcstar+=cstar[0].header['PCOEFF_'+str(l)]*arange(cstar[0].data.shape[0],type="Float64")**l
	spec = pyfits.open(self.prefix+frames[j])
        if (len(spec[0].data.shape) == 1):
           spec[0].data = array([spec[0].data])
	xspec = zeros(spec[0].data.shape[1])+0.
        if (not spec[0].header.has_key('PORDER')):
           print 'Can not find header keyword PORDER in ' + self.prefix + frames[j] 
           f = open(self.logfile,'ab')
           f.write("Can not find header keyword PORDER in "+self.prefix+fnames[j]+"\n")
           f.close()
           f = open(self.warnfile,'ab')
           f.write("WARNING: Can not find header keyword PORDER in "+self.prefix+fnames[j]+"\n")
           f.write('Time: '+str(datetime.today())+'\n\n')
           f.close()
           continue
	for l in range(spec[0].header['PORDER']+1):
	   xspec+=spec[0].header['PCOEFF_'+str(l)]*arange(spec[0].data.shape[1],type="Float64")**l
	xlo = max(xcstar.min(), xspec.min())
	xhi = min(xcstar.max(), xspec.max())
        b = where(where(xspec >= xlo, 1, 0)*where(xspec <= xhi, 1, 0) ==1)[0]
	ystar = []
	for l in range(len(b)):
	   ref = where(abs(xcstar-xspec[b][l]) == min(abs(xcstar-xspec[b][l])))[0][0]
	   if (xcstar[ref] == xspec[b][l]):
	      ystar.append(cstar[0].data[ref])
	      continue
	   elif (xcstar[ref] > xspec[b][l]):
	      ref2 = ref-1
	   else:
	      ref2 = ref+1
	   w1 = abs(xcstar[ref]-xspec[b][l])
	   w2 = abs(xcstar[ref2]-xspec[b][l])
	   ystar.append((w2*cstar[0].data[ref]+w1*cstar[0].data[ref2])/(w1+w2))
	ystar = array(ystar,type="Float64")
	cstar[0].data = ystar
	cstar.close()
	for l in range(len(spec[0].data)):
	   temp = zeros(spec[0].data.shape[1])+0.
	   temp[b] = spec[0].data[l][b]/ystar
	   spec[0].data[l] = temp
        spec[0].header.update('FILENAME',csdfile)
        spec[0].header.update('HISTORY','Calibration Star: '+cstarname)
	spec.writeto(csdfile)
	spec.close()

   def findTypes(self, files, lom):
      types = []
      if (os.access(lom, os.F_OK)):
	f = open(lom, 'rb')
	s = f.read().split('\n')
	f.close()
	try: s.remove('')
        except ValueError:
           junk = ''
	tmpprefix = []
	tmptype = []
	for j in range(len(s)):
	   temp = s[j].split()
	   tmpprefix.append(temp[0])
	   tmptype.append(temp[1])
	for j in range(len(files)):
	   currType = 'longslit'
	   for l in range(len(tmpprefix)):
	      if (files[j].find(tmpprefix[l]) != -1):
		currType = tmptype[l]
		continue
	   types.append(currType)
      elif (lom.lower() == 'mos'):
	for j in range(len(files)): types.append('mos')
      elif (lom.lower() == 'ifu'):
	for j in range(len(files)): types.append('ifu')
      else:
	for j in range(len(files)): types.append('longslit')
      return types

   def findSkyMethods(self, files, ssm):
      methods = []
      if (os.access(ssm, os.F_OK)):
        f = open(ssm, 'rb')
        s = f.read().split('\n')
        f.close()
        try: s.remove('')
        except ValueError:
           junk = ''
        tmpprefix = []
        tmpmeth = []
        for j in range(len(s)):
           temp = s[j].split()
           tmpprefix.append(temp[0])
           tmpmeth.append(temp[1])
        for j in range(len(files)):
           currMeth = 'dither'
           for l in range(len(tmpprefix)):
              if (files[j].find(tmpprefix[l]) != -1):
                currMeth = tmpmeth[l]
                continue
           methods.append(currMeth)
      elif (ssm.lower() == 'median'):
        for j in range(len(files)): methods.append('median')
      elif (ssm.lower() == 'step'):
	for j in range(len(files)): methods.append('step')
      elif (ssm.lower() == 'offsource_dither'):
	for j in range(len(files)): methods.append('offsource_dither')
      elif (ssm.lower() == 'offsource_multi_dither'):
        for j in range(len(files)): methods.append('offsource_multi_dither')
      elif (ssm.lower() == 'ifu_onsource_dither'):
	for j in range(len(files)): methods.append('ifu_onsource_dither')
      else:
        for j in range(len(files)): methods.append('dither')
      return methods

   def findRectCoeffs(self, files, clist):
      coeffs = ['']*len(files)
      if (os.access(clist, os.F_OK)):
        f = open(clist, 'rb')
        s = f.read().split('\n')
        f.close()
        try: s.remove('')
        except ValueError:
           junk = ''
	temp = s[0].split()
	if (temp[0].lower() == 'poly'):
	   coeffs = [clist]*len(files)
	else:
	   coeffs = []
	   tmpprefix = []
           tmpcfile = []
           for j in range(len(s)):
	      temp = s[j].split()
              tmpprefix.append(temp[0])
              tmpcfile.append(temp[1])
	   for j in range(len(files)):
	      currFile = ''
	      for l in range(len(tmpprefix)):
		if (files[j].find(tmpprefix[l]) != -1):
		   currFile = tmpcfile[l]
                   continue
	      coeffs.append(currFile)
      return coeffs

   def cleanUp(self):
      for j in self.symLinks:
	if (os.access(j, os.F_OK)): os.unlink(j)

#Main
if (len(sys.argv) > 1): infile = sys.argv[1]
else: infile = 'specparams.dat'
if (len(sys.argv) > 2): logfile = sys.argv[2]
else: logfile = 'specpipeline.log'
if (len(sys.argv) > 3): warnfile = sys.argv[3]
else: warnfile = 'specpipeline.warn'
if (len(sys.argv) > 4):
   if (sys.argv[4] == "-gui"): guiMessages = True
   else: guiMessages = False
else: guiMessages = False
a = f2spec(infile, logfile, warnfile, guiMessages)
if (a.params['DO_LINEARIZE'].lower() == "yes"):
   a.linearizeAll(a.params['LINEARITY_COEFFS'])
elif (os.access("linearized/lin_"+a.filenames[0], os.F_OK)):
   filesExist = True
   for j in a.filenames:
      if (not os.access("linearized/lin"+j, os.F_OK)):
        filesExist = False
   if (filesExist): a.prefix = 'linearized/lin_'
elif (a.mef != 0):
   a.convertMEFs()
if (guiMessages): print "PROGRESS: " + str(int(a.guiCRf*16+0.1))
if (a.params['ROTATION_ANGLE'] > 0):
   a.rotateAll(a.params['ROTATION_ANGLE'])
elif (os.access('rotated/rot_'+a.filenames[0], os.F_OK)):
   filesExist = True
   for j in a.filenames:
      if (not os.access("rotated/rot"+j, os.F_OK)):
        filesExist = False
   if (filesExist): a.prefix = 'rotated/rot_'
if (guiMessages): print "PROGRESS: " + str(int(a.guiCRf*20+0.1))
if (a.params['DO_NOISEMAPS'].lower() == "yes"):
   a.doNoisemaps()
if (guiMessages): print "PROGRESS: " + str(int(a.guiCRf*24+0.1))
if (a.params['DO_DARK_COMBINE'].lower() == "yes"):
   a.darkCombine()
else:
   for j in a.darkfiles:
      i = a.filenames.index(j)
      a.filenames.pop(i)
      a.exptimes.pop(i)
      a.objnames.pop(i)
if (guiMessages): print "PROGRESS: " + str(int(a.guiCRf*28+0.1))
if (a.params['DO_DARK_SUBTRACT'].lower() == "yes"):
   a.darkSubtract()
elif (os.access("darkSubtracted/ds_"+a.filenames[0], os.F_OK)):
   filesExist = True
   for j in a.filenames:
      if (not os.access("darkSubtracted/ds_"+j, os.F_OK)):
        filesExist = False
   if (filesExist): a.prefix = 'darkSubtracted/ds_'
if (guiMessages): print "PROGRESS: " + str(int(a.guiCRf*32+0.1))
if (a.params['DO_ARCLAMP_COMBINE'].lower() == "yes"):
   a.lampCombine()
else:
   for j in a.lampfiles:
      i = a.filenames.index(j)
      a.filenames.pop(i)
      a.exptimes.pop(i)
      a.objnames.pop(i)
   a.lampnames = ['']*len(a.filenames)
if (guiMessages): print "PROGRESS: " + str(int(a.guiCRf*34+0.1))
if (a.params['DO_FLAT_COMBINE'].lower() == "yes"):
   a.flatCombine()
else:
   a.flatnames = ['']*len(a.filenames)
   for j in a.flatfiles:
      i = a.filenames.index(j)
      a.filenames.pop(i)
      a.exptimes.pop(i)
      a.objnames.pop(i)
      a.flatnames.pop(i)
if (guiMessages): print "PROGRESS: " + str(int(a.guiCRf*36+0.1))
if (a.params['REMOVE_COSMIC_RAYS'].lower() == "yes"):
   a.removeCosmicRays()
elif (os.access("removedCosmicRays/rcr_"+a.filenames[0], os.F_OK)):
   filesExist = True
   for j in a.filenames:
      if (not os.access("removedCosmicRays/rcr_"+j, os.F_OK)):
	filesExist = False
   if (filesExist): a.prefix = 'removedCosmicRays/rcr_'
if (guiMessages): print "PROGRESS: " + str(int(a.guiCRdone+a.guiCRf*36+0.1))
if (a.params['DO_BAD_PIXEL_MASK'].lower() == "yes"):
   a.badPixelMask()
if (guiMessages): print "PROGRESS: " + str(int(a.guiCRdone+a.guiCRf*38+0.1))
a.skySubtract()
if (guiMessages): print "PROGRESS: " + str(int(a.guiCRdone+a.guiCRf*86+0.1))
if (a.params['DO_MOS_ALIGN_SKYLINES'].lower() == "yes"):
   a.wavelengthCorrectMOS()
elif (os.access('mosAlignedSkylines', os.F_OK)):
   for j in range(len(a.filenames)):
      if (os.access('mosAlignedSkylines/mas_'+a.filenames[j], os.F_OK)):
	a.oldpfix = a.prefix
        a.prefix = 'mosAlignedSkylines/mas_'
if (guiMessages): print "PROGRESS: " + str(int(a.guiCRdone+a.guiCRf*90+0.1))
if (a.params['DO_WAVELENGTH_CALIBRATE'].lower() == "yes"):
   a.wavelengthCalibrate()
elif (os.access('wavelengthCalibrated/wc_'+a.filenames[0],os.F_OK)):
   filesExist = True
   for j in a.filenames:
      if (not os.access("wavelengthCalibrated/wc_"+j, os.F_OK)):
	filesExist = false
   if (filesExist): a.prefix = 'wavelengthCalibrated/wc_'
if (guiMessages): print "PROGRESS: " + str(int(a.guiCRdone+a.guiCRf*94+0.1))
if (a.params['DO_EXTRACT_SPECTRA'].lower() == "yes"):
   spectra = a.findSpectra(a.cleanFrames)
   if (guiMessages): print "PROGRESS: "+str(int(a.guiCRdone+a.guiCRf*96+0.1))
   print spectra
   a.extractSpectra(spectra)
elif (os.access('extractedSpectra/es_'+a.filenames[0],os.F_OK)):
   filesExist = True
   for j in a.filenames:
      if (not os.access("extractedSpectra/es_"+j, os.F_OK)):
	filesExist = false
   if (filesExist): a.prefix = 'extractedSpectra/es_'
if (guiMessages): print "PROGRESS: " + str(int(a.guiCRdone+a.guiCRf*98+0.1))
if (a.params['DO_CALIBRATION_STAR_DIVIDE'].lower() == "yes"):
   a.calibStar()
if (guiMessages):
   print "PROGRESS: 100"
   print "STATUS: Done!"
f = open(a.logfile,'ab')
f.write('End time: '+str(datetime.today())+'\n')
f.close()
a.cleanUp()

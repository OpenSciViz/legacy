import java.io.*;

public abstract class FATBOYdata 
{
    protected int _elem = 0;
    protected String _name;
    protected int _type = 0;
    protected int width = 0, height = 0;

    public static class MsgTyp {
        public static final int _MsgError_=-1;
        public static final int _TimeStamp_=0;
        public static final int _Strings_=1;
        public static final int _Bytes_=2;
        public static final int _Shorts_=3;
        public static final int _Ints_=4;
        public static final int _Floats_=5;
        public static final int _FlamObsConf_=7;
        public static final int _ObsConfig_=8;
        public static final int _FrameConfig_=9;
        public static final int _ByteFrames_=11;
        public static final int _IntFrames_=13;
        public static final int _FloatFrames_=14;
    }

    private void _init() {
	_type = MsgTyp._Ints_;
    }

    public FATBOYdata() {
	_init();
    }

    public abstract double[] values();

    public abstract int recvData(DataInputStream inps);

    public abstract int sendData(DataOutputStream outps);

    // fetch name
    public String name() { return _name; };

    public int elements() { return _elem; }

    public int getWidth() { return width; }

    public int getHeight() { return height; }

    public abstract void flipFrame();

}

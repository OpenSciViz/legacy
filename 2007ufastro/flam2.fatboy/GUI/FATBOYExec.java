import java.awt.*;
import javax.swing.*;
import java.awt.image.*;
import java.awt.geom.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;
import java.util.*;

public class FATBOYExec extends javax.swing.JFrame implements Runnable {

    String command, params, defaultPath;
    JLabel label;
    JTextArea messages;
    JScrollPane scrollPane;
    JButton dismiss, hide;
    JCheckBox autoScroll;
    ProcessBuilder pb;
    Process p;
    Map m;
    BufferedReader reader;
    PrintWriter writer;
    boolean isRunning, isValid, selected, isUnique;
    Thread thread;
    String os = "", path = "";
    JProgressBar progressBar;
    JTextField tStatus;
    JPanel panel;
    FATBOYDisplayFrame display;

    public static String getEnvVar(String envVar)  {
        try {
            Properties env = new Properties();
            env.load(Runtime.getRuntime().exec("env").getInputStream());
            return (String)env.getProperty(envVar);
        } catch (Exception e){
            System.out.println("UFExecCommand.getEnvVar> "+e.toString());
            return null;
        }
    }

    public String getEnv(String key) {
        if (m == null) return null;
        String value = (String)m.get(key);
        return value;
    }

    public void setEnv(String key, String value) {
        if (m == null) {
            System.err.println("UFExecCommand error > environment not defined.");
            return;
        }
        m.put(key, value);
    }

    public FATBOYExec(String command, String params) {
	this(command, params, "", new JProgressBar(), new JTextField());
    }

    public FATBOYExec(String command, String params, String defaultPath) {
	this(command, params, defaultPath, new JProgressBar(), new JTextField());
    }

    public FATBOYExec(String command, String params, JProgressBar progressBar, JTextField tStatus) {
	this(command, params, "", progressBar, tStatus);
    }

    public FATBOYExec(String command, String params, String defaultPath, JProgressBar progressBar, JTextField tStatus) {
	this(command, params, "", progressBar, tStatus, null);
    }

    public FATBOYExec(String command, String params, String defaultPath, JProgressBar progressBar, JTextField tStatus, FATBOYDisplayFrame display) {
	super("FATBOY Messages");
	command = command.trim();
        params = params.trim();
	this.progressBar = progressBar;
	this.tStatus = tStatus;
	this.display = display;

        int pos = command.indexOf(" ");
	if (pos != -1) command = command.substring(0,pos);
	if (command.endsWith("&")) command = command.substring(0, command.length()-1);
	if (params.endsWith("&")) params = params.substring(0, params.length()-1);
	this.command = command;
	this.params = params;
	this.defaultPath = defaultPath.trim();
	Vector v = new Vector();
	v.add(command);
	String[] tempArgs = params.split(" ");
	for (int j = 0; j < tempArgs.length; j++) {
	   v.add(tempArgs[j]);
	}
	pb = new ProcessBuilder(v);
	pb.redirectErrorStream(true);
        try {
           m = pb.environment();
        } catch(Exception e) {
           System.err.println("ExecCommand error > unable to get environment");
        }
        panel = new JPanel();
        label = new JLabel("About to run "+command+" with options "+params);
        messages = new JTextArea(30,80);
	messages.setEditable(false);
	messages.setBackground(Color.WHITE);
        scrollPane = new JScrollPane(messages);
        dismiss = new JButton("End Process");
	hide = new JButton("Hide Messages");
        SpringLayout layout = new SpringLayout();
        panel.setLayout(layout);
        panel.add(label);
        layout.putConstraint(SpringLayout.WEST, label, 10, SpringLayout.WEST, panel);
        layout.putConstraint(SpringLayout.NORTH, label, 10, SpringLayout.NORTH, panel);
        panel.add(scrollPane);
        layout.putConstraint(SpringLayout.WEST, scrollPane, 10, SpringLayout.WEST, panel);
        layout.putConstraint(SpringLayout.NORTH, scrollPane, 15, SpringLayout.SOUTH, label);
	panel.add(hide);
	hide.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent ev) {
	        setVisible(false);
	    }
	});
        layout.putConstraint(SpringLayout.WEST, hide, 10, SpringLayout.WEST, panel);
        layout.putConstraint(SpringLayout.NORTH, hide, 15, SpringLayout.SOUTH, scrollPane);
        panel.add(dismiss);
        dismiss.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                isRunning = false;
                if (p != null) p.destroy();
            }
        });
        layout.putConstraint(SpringLayout.WEST, dismiss, 20, SpringLayout.EAST, hide);
        layout.putConstraint(SpringLayout.NORTH, dismiss, 15, SpringLayout.SOUTH, scrollPane);
	autoScroll = new JCheckBox("Autoscroll",true);
	panel.add(autoScroll);
        layout.putConstraint(SpringLayout.WEST, autoScroll, 20, SpringLayout.EAST, dismiss);
        layout.putConstraint(SpringLayout.NORTH, autoScroll, 15, SpringLayout.SOUTH, scrollPane);
        layout.putConstraint(SpringLayout.EAST, panel, 30, SpringLayout.EAST, scrollPane);
        layout.putConstraint(SpringLayout.SOUTH, panel, 10, SpringLayout.SOUTH, dismiss);
	getContentPane().add(panel);
	pack();
    }

    public void start() {
        isValid = testCommand();
        if (isValid) {
	    boolean isUnique = checkProcesses();
	    if (isUnique) {
		label.setText("Running "+command+" with options "+params);
                isRunning = true;
                thread = new Thread(this);
                thread.start();
	    }
	    else label.setText("Failed to run "+command+" with options "+params);
        } else {
	   label.setText("Failed to run "+command+" with options "+params);
           if (! isValid) System.err.println("FATBOYExec error > command "+command+" not found.");
        }
    }

    public boolean testCommand() {
	try {
	    String s = "";
	    Process unamep = new ProcessBuilder("/bin/uname").start();
	    BufferedReader unamebr = new BufferedReader(new InputStreamReader(unamep.getInputStream()));
	    s = unamebr.readLine();
	    unamebr.close();
	    if (s.toLowerCase().indexOf("linux") != -1) {
		os = "linux";
		path = "/usr/bin/";
	    } else if (s.toLowerCase().indexOf("sun") != -1) {
		os = "sun";
		path = "/usr/ucb/";
	    } else path = "";
	    Process testp;
	    if (defaultPath.equals("")) {
		testp = new ProcessBuilder(path+"whereis",command).start();
	    } else {
		Vector vComm = new Vector();
		vComm.add(path+"whereis");
	        vComm.add("-B");
		String[] temp = defaultPath.split(" ");
		for (int l = 0; l < temp.length; l++) vComm.add(temp[l]);
		vComm.add("-f");
		vComm.add(command);
		//testp = new ProcessBuilder(path+"whereis","-B",defaultPath,"-f",command).start();
		testp = new ProcessBuilder(vComm).start();
	    }
	    BufferedReader testbr = new BufferedReader(new InputStreamReader(testp.getInputStream()));
            s = testbr.readLine(); 
	    unamebr.close();
            if (s.endsWith(":")) {
	      File f = new File(defaultPath+command);
	      if (f.isFile()) {
		command = defaultPath+command;
		return true;
	      } else return false;
	    } else return true; 
	} catch (Exception e) {
	    return false;
	}
    }

    public boolean checkProcesses() {
	isUnique = true;
	selected = true;
        String pid = "";
        String who = "";
	int pscount = 0;
	try {
	    Process checkps = new ProcessBuilder("/bin/ps","-eo","pid,user,comm").start();
	    BufferedReader brps = new BufferedReader(new InputStreamReader(checkps.getInputStream()));
	    String s = " ";
	    String commandBase = command.substring(command.lastIndexOf("/")+1,command.lastIndexOf("."));
	    while (s != null) {
		s = brps.readLine();
		if (s == null) break;
		if (s.indexOf(commandBase) != -1) {
		   isUnique = false; 
		   pid = s;
		   pscount++;
		}
	    }
	    brps.close();
	    Process checkwho = new ProcessBuilder(path+"whoami").start();
	    BufferedReader brwho = new BufferedReader(new InputStreamReader(checkwho.getInputStream()));
	    s = " ";
	    while (s != null) {
	        s = brwho.readLine();
		if (s == null) break;
		else who = s;
	    }
	} catch (Exception e) {
	    return false;
	}
	if (!isUnique) {
	    selected = false;
	    Object[] options = null;
	    String question = "";
	    if (pscount > 1) {
                options = "Abort new process,Ignore and run anyway,Kill your old processes and run".split(",");
                question = "Multiple instances of "+command+" are already running.  Do you want to:";
	    } else if (pid.indexOf(who) != -1) {
		options = "Abort new process,Ignore and run anyway,Kill old process and run".split(",");
		question = "The process "+command+" is already running.  Do you want to:";
	    } else {
		options = "Abort new process,Ignore and run anyway".split(",");
		question = "The process "+command+" is being run by another user on this system.  Do you want to:";
	    }
	    int n = JOptionPane.showOptionDialog(FATBOYExec.this, question, "Process running...", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
	    if (n == 0) {
		selected = true;
	    } else if (n == 1) {
		isUnique = true;
		selected = true;
	    } else if (n == 2) {
		try {
		    Process checkps = new ProcessBuilder("/usr/bin/pkill","-f",command).start();
		    isUnique = true;
		    int x = checkps.waitFor();
		} catch (Exception e) {
		    System.err.println("FATBOYExec error > Unable to kill "+command);
		}
		selected = true;
	    }
	}
	return isUnique;
    }

    public void run() {
	try {
	    p = pb.start();
	    reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
	    writer = new PrintWriter(p.getOutputStream(), true);
	    String s = " ";
	    while (isRunning) {
		s = reader.readLine();
		if (isInput(s)) {
		   requestInput(s);
		} else if (isValid(s)) {
		   messages.append(s+"\n");
		   if (autoScroll.isSelected()) messages.setCaretPosition(messages.getDocument().getLength());
		}
	        if (s == null) isRunning=false;
	    }
	} catch(Exception e) {
	    isRunning = false;
	    System.err.println("FATBOYExec.run> "+e.toString());
	}
	if (p != null) p.destroy();
	label.setText("Done!");
    }

    public boolean isValid(String s) {
	if (s == null) return false;
	boolean valid = true;
	String[] invalid = {"card is too long", "Warning: Encountered", "Warning: Number of calls"};
	for (int j = 0; j < invalid.length; j++) {
	    if (s.startsWith(invalid[j])) valid = false;
	}
	if (s.startsWith("PROGRESS: ")) {
	   progressBar.setValue(Integer.parseInt(s.substring(10)));
	   progressBar.setString(s.substring(10)+"%");
	   valid = false;
	} else if (s.startsWith("STATUS: ")) {
	   tStatus.setText(s.substring(8));
	   valid = false;
	} else if (s.startsWith("FILE: ")) {
	   valid = false;
	   if (display != null) {
	      display.addFile(s.substring(6));
	   }
	}
	return valid;
    }

    public boolean isInput(String s) {
	if (s.startsWith("INPUT: ")) return true; else return false;
    }

    public void requestInput(String s) {
	s = s.substring(7);
	if (s.startsWith("File: ")) {
	    s = s.substring(6);
            Object[] fileOptions = {"OK","Cancel"};
	    int n = JOptionPane.showOptionDialog(this, s, "Select a File!", JOptionPane.YES_NO_OPTION, JOptionPane.INFORMATION_MESSAGE, null, fileOptions, fileOptions[0]);
	    if (n == 1) {
		writer.println("");
	    } else {
		String dir = ".";
		if (s.indexOf("dark") != -1) {
		   dir = "masterDarks/";
		} else if (s.indexOf("flat") != -1) {
		   dir = "masterFlats/";
		}
		JFileChooser jfc = new JFileChooser(dir);
		int returnVal = jfc.showOpenDialog(this);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
		    String filename = jfc.getSelectedFile().getAbsolutePath();
		    writer.println(filename);
		} else {
		    writer.println("");
		}
	    }
	} else if (s.startsWith("Text: ")) {
	    s = s.substring(6);
	    String temp = JOptionPane.showInputDialog(this, s, "FATBOY Prompt", JOptionPane.QUESTION_MESSAGE);
	    writer.println(temp);
	}
    }

    public static void main(String[] args) {
 	//FATBOYExec ufec = new FATBOYExec("flam2pipeline.py","pipelineparams.dat pipeline.log pipeline.warn");
	FATBOYExec ufec = new FATBOYExec("testinput.py","");
 	ufec.start();
	ufec.setVisible(true);
    }
}

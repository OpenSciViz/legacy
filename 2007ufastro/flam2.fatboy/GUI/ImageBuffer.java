public class ImageBuffer {
    
    protected FATBOYdata data; 
    protected String name;
    public int width;
    public int height;
    public double min =  2147483647;
    public double max = -2147483648;
    public double s_min=0;
    public double s_max=0;
    protected double[] ZscaleRange = null;
    public double pixels[];
    public float zoomFactor = 1;
    public int xStart, yStart;
    public double image[][];

    public ImageBuffer(FATBOYdata data) {
	data.flipFrame();  //because Java coordinate sys of (Left,Top) = (0,0)
	this.data = data;
	this.width = data.getWidth();
	this.height = data.getHeight();
	this.pixels = data.values();
	this.min = UFArrayOps.minValue(pixels);
	this.max = UFArrayOps.maxValue(pixels);
	s_min = min;
	s_max = max;
	this.name = data.name();
    }

    public ImageBuffer() {
	width = 2048;
	height = 2048;
	data = null; //(FATBOYdata)(new FATBOYints("empty",2048*2048,2048,2048));
	pixels = null; // data.values();
	//min = UFArrayOps.minValue(pixels);
	//max = UFArrayOps.maxValue(pixels);
	//s_min = min;
	//s_max = max;
	//name = data.name();
    }

//----------------------------------------------------------------------------------------------

    public synchronized void Zscale()
    {
        if( image != null )
            this.ZscaleRange = UFImageOps.Zscale( image );
        else
            this.ZscaleRange = UFImageOps.Zscale( pixels );
    }
//----------------------------------------------------------------------------------------------

    public ImageBuffer zoom( int xZoomCen, int yZoomCen, int zoomSize, float zoomFactor )
    {
        int xStart = xZoomCen - zoomSize/2 ;
        int yStart = yZoomCen - zoomSize/2 ;

	if( xStart < 0 ) xStart = 0;
	if( yStart < 0 ) yStart = 0;

        if( (xStart + zoomSize) > this.width  && xStart > 0 ) xStart--;
        if( (yStart + zoomSize) > this.height && yStart > 0 ) yStart--;

	double[][] zoomPixels = new double[zoomSize][zoomSize];
	double zmin =  2147483647;
	double zmax = -2147483648;

	for( int i = 0; i < zoomSize; i++ ) {
	    int isw = xStart + (i + yStart) * this.width;
	    for( int j = 0; j < zoomSize; j++ ) {
		int k = isw + j;
		if( k < this.pixels.length ) {
		    double data = this.pixels[k];
		    zoomPixels[i][j] = data;
		    if( data < zmin ) zmin = data;
		    if( data > zmax ) zmax = data;
		}
	    }
	}

	ImageBuffer zoomBuffer = new ImageBuffer( this.data );

        zoomBuffer.image = zoomPixels;
	zoomBuffer.zoomFactor = zoomFactor;
	zoomBuffer.xStart = xStart;
	zoomBuffer.yStart = yStart;
        zoomBuffer.name = new String( this.name );
        zoomBuffer.width = zoomSize;
        zoomBuffer.height = zoomSize;
        zoomBuffer.min = zmin;
        zoomBuffer.max = zmax;
        zoomBuffer.s_min = zmin;
        zoomBuffer.s_max = zmax;

	return zoomBuffer;
    }
//----------------------------------------------------------------------------------------------
    public ImageBuffer rescale(int size) 
    {
        int hsize = size;
	double scale = (double)this.width/size;
	if (width != height) hsize = (int)(height/scale)+1; 
        double[][] rescaleData = new double[size][hsize];
        double zmin =  2147483647;
        double zmax = -2147483648;
        double[] rescalePixels = new double[size*hsize];

        for( int i = 0; i < hsize; i++ ) {
            int isw = (int)Math.floor(scale*i) * this.width;
            for( int j = 0; j < size; j++ ) {
                int k = isw + (int)Math.floor(scale*j);
                int l = i*size+j;
                if( k < this.pixels.length ) {
                    double data = this.pixels[k];
                    rescalePixels[l] = this.pixels[k];
                    rescaleData[j][i] = data;
                    if( data < zmin ) zmin = data;
                    if( data > zmax ) zmax = data;
                }
            }
        }

        ImageBuffer rescaleBuffer = new ImageBuffer( this.data );

        rescaleBuffer.image = rescaleData;
        rescaleBuffer.name = new String( this.name );
        rescaleBuffer.width = size;
        rescaleBuffer.height = hsize;
        rescaleBuffer.pixels = rescalePixels;
        rescaleBuffer.min = zmin;
        rescaleBuffer.max = zmax;
        rescaleBuffer.s_min = zmin;
        rescaleBuffer.s_max = zmax;

        return rescaleBuffer;
    }

}

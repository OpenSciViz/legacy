import java.io.*;
import javax.imageio.stream.*;

public class FATBOYdoubles extends FATBOYdata
{
    protected double[] _values=null;

    private void _init() {
	_type = MsgTyp._Floats_;
    }

    public FATBOYdoubles() {
	_init();
    }

    public FATBOYdoubles(String name, int nelem) {
	_init();
	_name = new String(name);
	_elem = nelem;
    }

    public FATBOYdoubles(String name, int nelem, int width, int height) {
        this(name, nelem);
        this.width = width;
        this.height = height;
    }

    public FATBOYdoubles(String name, double[] vals) {
	_init();	
	_name = new String(name);
	_elem = vals.length;
	_values = vals;
    }

    public FATBOYdoubles(String name, double[] vals, int width, int height) {
        this(name, vals);
        this.width = width;
        this.height = height;
    }

    // all methods declared abstract by UFProtocol can be defined here:

    public String description() { return new String("FATBOYdoubles"); }
 
    // return size of an element's value:
    public int valSize(int elemIdx) { 
	if( elemIdx >= _values.length )
	    return 0;
	else
	    return 4;
    }

    public double[] values() { return _values; }

    public double valData(int index) { return _values[index]; }

    public double maxVal() {
	double max=-1.0e38f;
	for( int i=0; i<_values.length; i++ )
	    if( _values[i] > max ) max = _values[i];
	return max;
    }

    public double minVal() {
	double min=1.0e38f;
	for( int i=0; i<_values.length; i++ )
	    if( _values[i] < min ) min = _values[i];
	return min;
    }

    public int numVals() {
	if (_values != null)
	    return _values.length;
	else
	    return 0;
    }

    // recv data values (length, type, and header have already been read)

    public int recvData(DataInputStream inp) {
	int retval=0;
	try {
	    _values = new double[_elem];
/*
	    for( int elem=0; elem<_elem; elem++ ) {
		_values[elem] = inp.readFloat();
		retval += 4;
	    }
*/
	    MemoryCacheImageInputStream mciis = new MemoryCacheImageInputStream(inp);
	    mciis.readFully( _values, 0, _elem );
	    mciis.close();
            return 8*_elem;
	}
	catch(EOFException eof) {
	    System.err.println("FATBOYdoubles::recvData> "+eof.toString());
	}
	catch(IOException ioe) {
	    System.err.println("FATBOYdoubles::recvData> "+ioe.toString());
	}
	catch( Exception e ) {
	    System.err.println("FATBOYdoubles::recvData> "+e.toString());
	}
	return retval;
    }

    // send data values (header already sent):

    public int sendData(DataOutputStream out) {
	int retval=0;
	try {
	    if (_values != null && _values.length > 0) {
		ByteArrayOutputStream outArray = new ByteArrayOutputStream(_elem*4);
		DataOutputStream outToArray = new DataOutputStream(outArray);
		for( int elem=0; elem<_elem; elem++ ) {
		    outToArray.writeDouble(_values[elem]);
		    retval += 4;
		}
		outArray.writeTo(out); // write all doubles as bytes in one chunk
	    }
	}
	catch(EOFException eof) {
	    System.err.println("FATBOYdoubles::sendData> "+eof.toString());
	} 
	catch(IOException ioe) {
	    System.err.println("FATBOYdoubles::sendData> "+ioe.toString());
	}
	catch( Exception e ) {
	    System.err.println("FATBOYdoubles::sendData> "+e.toString());
	}
	return retval;
    }

    public void flipFrame()
    {
        int nrow = height;
        int ncol = width;

        for( int irow=0; irow < (nrow/2); irow++ )
            {
                int iflip = nrow - irow - 1;
                int kp = irow * ncol;
                int kf = iflip * ncol;

                for( int k=0; k<ncol; k++ ) {
                    double pixval = _values[kp];
                    _values[kp++] = _values[kf];
                    _values[kf++] = pixval;
                }
            }
    }

}

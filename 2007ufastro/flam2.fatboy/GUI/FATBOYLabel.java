import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;

public class FATBOYLabel extends javax.swing.JLabel {

   public Border theBorder = null;
   public Font theFont = new Font("Dialog",Font.BOLD,16);
   public static int NONE = 0;
   public static int RYBEVEL = 1;
   public static int YGBEVEL = 2;
   public static int RYETCH = 3;

   public FATBOYLabel() {
      super();
   }

   public FATBOYLabel(Icon image) {
      super(image);
   }

   public FATBOYLabel(Icon image, int ha) {
      super(image, ha);
   }

   public FATBOYLabel(String text) {
      super(text);
   }

   public FATBOYLabel(String text, Icon icon, int ha) {
      super(text, icon, ha);
   }

   public FATBOYLabel(String text, int ha) {
      super(text, ha);
   }

   public FATBOYLabel(String text, int border, boolean bigText) {
      super(text);
      switch (border) {
	case 1:
	   theBorder = new SoftBevelBorder(0, Color.RED, Color.YELLOW);
	   break;
	case 2:
	   theBorder = new SoftBevelBorder(0, Color.YELLOW,Color.GREEN);
	   break;
	case 3:
	   theBorder = new EtchedBorder(Color.RED, Color.YELLOW);
	   break;
	default:
	   theBorder = null;
	   break;
      }
      this.setBorder(theBorder);
      if (bigText) {
	this.setFont(theFont);
      }
   }

   public void showBorder() {
      this.setBorder(theBorder);
   }

   public void hideBorder() {
      super.setBorder(null);
   }

   public void setBorder(Border border) {
      super.setBorder(border);
      theBorder = border;
   }

   public void setFont(Font font) {
      super.setFont(font);
      theFont = font;
   }

}

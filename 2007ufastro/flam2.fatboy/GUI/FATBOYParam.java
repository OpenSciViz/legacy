/*
 * Author:      Craig Warner
 * Company:     University of Florida
 */

import java.awt.*;
import javax.swing.*;
import javax.swing.text.*;
import java.util.*;

public abstract class FATBOYParam {

   public FATBOYLabel lParam;
   public JTextArea taParam;
   public JTextField tParam;
   public JScrollPane spParam;
   public JButton bParam, cParam;
   public JRadioButton yParam, nParam;
   public ButtonGroup bgParam;
   public JComboBox cbParam;
   public Vector grayOptions = new Vector(), grayParams = new Vector();
   public FATBOYParam mirrorParam;

   public FATBOYParam() {}

   public abstract void setParamValue(String value);

   public abstract String getParamValue();

   public void addBrowse(boolean multiSelect) {
   }

   public void addBrowse() {
   }

   public void addBrowse(String s) {
   }

   public void addDirBrowse() {
   }

   public void addClear() {
   }

   public void addFocusBorder() {
   }

   public void setGray(boolean isGray) {
   }

   public void addGrayComponents(String option, FATBOYParam... paramList) {
   }

   public void checkGrayState() {
   }

   public void addMirror(FATBOYParam p) {
   }

   public void updateMirror() {
   }

   public abstract void addTo(Container panel);

   public void setConstraints(SpringLayout layout, int y, int x1, int x2, JComponent leftComponent, JComponent topComponent, boolean isTop) {
   }

   public void setConstraints(SpringLayout layout, int y, int x1, int x2, JComponent leftComponent, JComponent topComponent) {
      this.setConstraints(layout, y, x1, x2, leftComponent, topComponent, false);
   }

   public void setConstraints(SpringLayout layout, int y, int x1, int x2, int x3, JComponent leftComponent, JComponent topComponent, boolean useLeft3, boolean isTop) {
   }

   public void setConstraints(SpringLayout layout, int y, int x1, int x2, int x3, JComponent leftComponent, JComponent topComponent) {
      this.setConstraints(layout, y, x1, x2, x3, leftComponent, topComponent, false, false);
   }

   public void setConstraints(SpringLayout layout, int y1, int x1, int x2, int x3, int y2, JComponent leftComponent, JComponent topComponent, boolean useLeft3, boolean useTop4, boolean isTop) {
   }

   public void setConstraints(SpringLayout layout, int y1, int x1, int x2, int x3, int y2, JComponent leftComponent, JComponent topComponent) {
      this.setConstraints(layout, y1, x1, x2, x3, y2, leftComponent, topComponent, false, false, false);
   }

   public void setConstraints(SpringLayout layout, int y1, int x1, int x2, int y2, int x3, int x4, JComponent leftComponent, JComponent topComponent, boolean useTop3, boolean useLeft4, boolean isTop) {
   }

   public void setConstraints(SpringLayout layout, int y1, int x1, int x2, int y2, int x3, int x4, JComponent leftComponent, JComponent topComponent) {
      this.setConstraints(layout, y1, x1, x2, y2, x3, x4, leftComponent, topComponent, false, false, false);
   }

   public void setToolTipText(String s) {
      lParam.setToolTipText(s);
   }
}

/*
 * Author:      Craig Warner
 * Company:     University of Florida
 */

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class FATBOYTextParam extends FATBOYParam {

   public FATBOYTextParam(String labelText, int size) {
      this(labelText, FATBOYLabel.NONE, size);
   }

   public FATBOYTextParam(String labelText, int border, int size) {
      lParam = new FATBOYLabel(labelText, border, true);
      tParam = new JTextField(size);
   }

   public FATBOYTextParam(String labelText, int size, String text) {
      this(labelText, FATBOYLabel.NONE, size, text);
   }

   public FATBOYTextParam(String labelText, int border, int size,String text) {
      lParam = new FATBOYLabel(labelText, border, true);
      tParam = new JTextField(size);
      tParam.setText(text);
   }

   public void setParamValue(String value) {
      tParam.setText(value);
   }

   public String getParamValue() {
      return tParam.getText();
   }

   public void addFocusBorder() {
      tParam.addFocusListener(new FocusListener() {
	public void focusGained(FocusEvent fe) {
	   lParam.hideBorder();
	}
	public void focusLost(FocusEvent fe) {
	   if (tParam.getText().trim().equals("")) lParam.showBorder();
	}
      });
   }

   public void setGray(boolean isGray) {
      if (isGray) {
        tParam.setEnabled(false);
      } else {
        tParam.setEnabled(true);
      }
   }

   public void addMirror(FATBOYParam p) {
      mirrorParam = p;
      tParam.addFocusListener(new FocusListener() {
        public void focusGained(FocusEvent ev) {
        }
        public void focusLost(FocusEvent ev) {
           String temp = tParam.getText();
           if (!mirrorParam.getParamValue().equalsIgnoreCase(temp)) {
              mirrorParam.setParamValue(temp);
           }
        }
      });
      String tt = p.lParam.getToolTipText();
      if (tt != null) setToolTipText(tt);
   }

   public void updateMirror() {
      if (mirrorParam == null) return;
      String temp = tParam.getText();
      if (!mirrorParam.getParamValue().equalsIgnoreCase(temp)) {
        mirrorParam.setParamValue(temp);
      }
   }

   public void addTo(Container panel) {
      panel.add(lParam);
      panel.add(tParam);
   }

   public void setConstraints(SpringLayout layout, int y, int x1, int x2, JComponent leftComponent, JComponent topComponent) {
      this.setConstraints(layout, y, x1, x2, leftComponent, topComponent, false);
   }

   public void setConstraints(SpringLayout layout, int y, int x1, int x2, JComponent leftComponent, JComponent topComponent, boolean isTop) {
      String topPos;
      if (isTop) topPos = SpringLayout.NORTH; else topPos = SpringLayout.SOUTH;
      layout.putConstraint(SpringLayout.WEST, lParam, x1, SpringLayout.WEST, leftComponent);
      layout.putConstraint(SpringLayout.NORTH, lParam, y, topPos,topComponent);
      layout.putConstraint(SpringLayout.WEST, tParam, x2, SpringLayout.WEST, leftComponent);
      layout.putConstraint(SpringLayout.NORTH, tParam,y,topPos,topComponent);
   }

}

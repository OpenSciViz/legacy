/*
 * Author:      Craig Warner
 * Company:     University of Florida
 */

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class FATBOYComboBrowseParam extends FATBOYParam {

   String browseOption;

   public FATBOYComboBrowseParam(String labelText, String comboOptions, int size, String bText) {
      this(labelText, FATBOYLabel.NONE, comboOptions, size, bText);
   }

   public FATBOYComboBrowseParam(String labelText, int border, String comboOptions, int size, String bText) {
      lParam = new FATBOYLabel(labelText, border, true);
      cbParam = new JComboBox(comboOptions.split(","));
      tParam = new JTextField(size);
      bParam = new JButton(bText);
      this.browseOption = (String)cbParam.getItemAt(cbParam.getItemCount()-1);
   }

   public FATBOYComboBrowseParam(String labelText, int border, String comboOptions, int size, String bText, String i) {
      lParam = new FATBOYLabel(labelText, border, true);
      cbParam = new JComboBox(comboOptions.split(","));
      cbParam.setSelectedItem(i);
      tParam = new JTextField(size);
      bParam = new JButton(bText);
      this.browseOption = (String)cbParam.getItemAt(cbParam.getItemCount()-1);
   }

   public FATBOYComboBrowseParam(String labelText, int border, String comboOptions, String browseOption, int size, String bText) {
      lParam = new FATBOYLabel(labelText, border, true);
      cbParam = new JComboBox(comboOptions.split(","));
      tParam = new JTextField(size);
      bParam = new JButton(bText);
      this.browseOption = browseOption;
   }

   public FATBOYComboBrowseParam(String labelText, int border, String comboOptions, String browseOption, int size, String bText, String i) {
      lParam = new FATBOYLabel(labelText, border, true);
      cbParam = new JComboBox(comboOptions.split(","));
      cbParam.setSelectedItem(i);
      tParam = new JTextField(size);
      bParam = new JButton(bText);
      this.browseOption = browseOption;
   }

   public FATBOYComboBrowseParam(String labelText, String[] comboOptions, int size, String bText) {
      this(labelText, FATBOYLabel.NONE, comboOptions, size, bText);
   }

   public FATBOYComboBrowseParam(String labelText, int border, String[] comboOptions, int size, String bText) {
      lParam = new FATBOYLabel(labelText, border, true);
      cbParam = new JComboBox(comboOptions);
      tParam = new JTextField(size);
      bParam = new JButton(bText);
      this.browseOption = (String)cbParam.getItemAt(cbParam.getItemCount()-1);
   }

   public FATBOYComboBrowseParam(String labelText, int border, String[] comboOptions, String browseOption, int size, String bText) {
      lParam = new FATBOYLabel(labelText, border, true);
      cbParam = new JComboBox(comboOptions);
      tParam = new JTextField(size);
      bParam = new JButton(bText);
      this.browseOption = browseOption;
   }

   public FATBOYComboBrowseParam(String labelText, int border, String[] comboOptions, String browseOption, int size, String bText, String i) {
      lParam = new FATBOYLabel(labelText, border, true);
      cbParam = new JComboBox(comboOptions);
      cbParam.setSelectedItem(i);
      tParam = new JTextField(size);
      bParam = new JButton(bText);
      this.browseOption = browseOption;
   }

   public void setParamValue(String value) {
      cbParam.setEditable(false);
      cbParam.setSelectedItem(value);
      if (cbParam.getSelectedItem().equals(value)) {
	tParam.setEnabled(false);
	bParam.setEnabled(false);
      } else {
	cbParam.setSelectedItem(browseOption);
	tParam.setEnabled(true);
	tParam.setText(value);
	bParam.setEnabled(true);
      }
      //cbParam.setEditable(true);
   }

   public void setParamValue(int index) {
      cbParam.setSelectedIndex(index);
      if (cbParam.getSelectedItem().equals(browseOption)) {
	tParam.setEnabled(true);
	bParam.setEnabled(true);
      }
   }

   public String getParamValue() {
      String temp = (String)cbParam.getSelectedItem();
      if (temp.equals(browseOption)) {
	return tParam.getText();
      }
      else return (String)cbParam.getSelectedItem();
   }

   public void addBrowse() {
      if (!cbParam.getSelectedItem().equals(browseOption)) {
        tParam.setEnabled(false);
	bParam.setEnabled(false);
      }
      cbParam.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent ev) {
	   if (cbParam.getSelectedItem().equals(browseOption)) {
	      tParam.setEnabled(true);
	      bParam.setEnabled(true);
	   } else {
              tParam.setEnabled(false);
	      bParam.setEnabled(false);
	   }
	}
      });
      bParam.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent ev) {
	   if (!tParam.isEditable()) return;
	   JFileChooser jfc = new JFileChooser(".");
           int returnVal = jfc.showOpenDialog((Component)ev.getSource());
           if (returnVal == JFileChooser.APPROVE_OPTION) {
	      File f = jfc.getSelectedFile();
	      tParam.setText(f.getAbsolutePath());
	   }
	   lParam.hideBorder();
           tParam.requestFocusInWindow();
	}
      });
   }

   public void addFocusBorder() {
      cbParam.addFocusListener(new FocusListener() {
	public void focusGained(FocusEvent fe) {
	   lParam.hideBorder();
	}
	public void focusLost(FocusEvent fe) {
	   if (cbParam.getSelectedItem().equals(browseOption) && tParam.getText().trim().equals("")) lParam.showBorder();
	}
      });
   }

   public void setGray(boolean isGray) {
      if (isGray) {
        cbParam.setEnabled(false);
        tParam.setEnabled(false);
        bParam.setEnabled(false);
      } else {
        cbParam.setEnabled(true);
        tParam.setEnabled(true);
        bParam.setEnabled(false);
      }
   }

   public void addGrayComponents(final String option, final FATBOYParam... paramList) {
      grayOptions.add(option);
      grayParams.add(paramList);
      this.checkGrayState();
      cbParam.addItemListener(new ItemListener() {
        public void itemStateChanged(ItemEvent ev) {
           if (!ev.getItem().equals(option)) return;
           boolean doGray;
           if (ev.getStateChange() == ItemEvent.SELECTED) {
              doGray = true;
           } else {
              doGray = false;
           }
           for (int j = 0; j < paramList.length; j++) {
              paramList[j].setGray(doGray);
           }
           for (int j = 0; j < paramList.length; j++) {
              paramList[j].checkGrayState();
           }
        }
      });
   }

   public void checkGrayState() {
      for (int i = 0; i < grayOptions.size(); i++) {
        if (cbParam.getSelectedItem().equals(grayOptions.get(i))) {
           FATBOYParam[] paramList = (FATBOYParam[])grayParams.get(i);
           for (int j = 0; j < paramList.length; j++) {
              paramList[j].setGray(true);
           }
        }
      }
   }

   public void addMirror(FATBOYParam p) {
      mirrorParam = p;
      cbParam.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent ev) {
	   String temp = (String)cbParam.getSelectedItem();
	   if (!mirrorParam.getParamValue().equalsIgnoreCase(temp)) {
              mirrorParam.setParamValue(temp);
              if (temp.equals(browseOption)) {
		mirrorParam.setParamValue(tParam.getText());
	      }
	   }
	}
      });
      tParam.addFocusListener(new FocusListener() {
        public void focusGained(FocusEvent ev) {
        }
        public void focusLost(FocusEvent ev) {
           String temp = tParam.getText();
           if (!mirrorParam.getParamValue().equalsIgnoreCase(temp)) {
              mirrorParam.setParamValue(temp);
           }
        }
      });
      String tt = p.lParam.getToolTipText();
      if (tt != null) setToolTipText(tt);
   }

   public void updateMirror() {
      if (mirrorParam == null) return;
      String temp = (String)cbParam.getSelectedItem();
      if (!mirrorParam.getParamValue().equalsIgnoreCase(temp)) {
	mirrorParam.setParamValue(temp);
	if (temp.equals(browseOption)) {
	   mirrorParam.setParamValue(tParam.getText());
	}
      }
   }

   public void addTo(Container panel) {
      panel.add(lParam);
      panel.add(cbParam);
      panel.add(tParam);
      panel.add(bParam);
   }

   public void setConstraints(SpringLayout layout, int y1, int x1, int x2, int y2, int x3, int x4, JComponent leftComponent, JComponent topComponent) {
      this.setConstraints(layout, y1, x1, x2, y2, x3, x4, leftComponent, topComponent, false, false, false);
   }

   public void setConstraints(SpringLayout layout, int y1, int x1, int x2, int y2, int x3, int x4, JComponent leftComponent, JComponent topComponent, boolean useTop3, boolean useLeft4, boolean isTop) {
      String topPos, leftPos4, topPos3;
      JComponent left4, top3;
      if (isTop) topPos = SpringLayout.NORTH; else topPos = SpringLayout.SOUTH;
      if (useLeft4) {
	left4 = leftComponent;
	leftPos4 = SpringLayout.WEST;
      } else {
	left4 = tParam;
	leftPos4 = SpringLayout.EAST;
      }
      if (useTop3) {
	top3 = topComponent;
	topPos3 = topPos;
      } else {
	top3 = cbParam;
	topPos3 = SpringLayout.SOUTH;
      }
      layout.putConstraint(SpringLayout.WEST, lParam, x1, SpringLayout.WEST, leftComponent);
      layout.putConstraint(SpringLayout.NORTH, lParam, y1, topPos,topComponent);
      layout.putConstraint(SpringLayout.WEST, cbParam, x2, SpringLayout.WEST, leftComponent);
      layout.putConstraint(SpringLayout.NORTH, cbParam,y1,topPos,topComponent);  
      layout.putConstraint(SpringLayout.WEST, tParam, x3, SpringLayout.WEST, leftComponent);
      layout.putConstraint(SpringLayout.NORTH, tParam,y2,topPos,top3);
      layout.putConstraint(SpringLayout.WEST, bParam, x4, leftPos4, left4);
      layout.putConstraint(SpringLayout.NORTH, bParam, y2, topPos,top3);
   }

}

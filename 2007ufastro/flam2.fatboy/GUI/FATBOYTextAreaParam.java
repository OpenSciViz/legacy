/*
 * Author:      Craig Warner
 * Company:     University of Florida
 */

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

public class FATBOYTextAreaParam extends FATBOYParam {

   public FATBOYTextAreaParam(String labelText, int rows, int cols, String b1Text, String b2Text) {
      this(labelText, FATBOYLabel.NONE, rows, cols, b1Text, b2Text);
   }

   public FATBOYTextAreaParam(String labelText, int border, int rows, int cols, String b1Text, String b2Text) {
      lParam = new FATBOYLabel(labelText, border, true);
      taParam = new JTextArea(rows, cols);
      spParam = new JScrollPane(taParam);
      bParam = new JButton(b1Text);
      cParam = new JButton(b2Text);
   }

   public void setParamValue(String value) {
      taParam.setText(value);
   }

   public String getParamValue() {
      return taParam.getText();
   }

   public void addBrowse(boolean multiSelect) {
      if (multiSelect) {
	bParam.addActionListener(new ActionListener() {
	   public void actionPerformed(ActionEvent ev) {
	      JFileChooser jfc = new JFileChooser(".");
	      jfc.setMultiSelectionEnabled(true);
	      int returnVal = jfc.showOpenDialog((Component)ev.getSource());
	      if (returnVal == JFileChooser.APPROVE_OPTION) {
		File[] f = jfc.getSelectedFiles();
		for (int j = 0; j < f.length; j++)
		   taParam.append(f[j].getAbsolutePath() + "\n");
	      }
	      lParam.hideBorder();
	      taParam.requestFocusInWindow();
	   }
	});
      } else {
	bParam.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent ev) {
              JFileChooser jfc = new JFileChooser(".");
              int returnVal = jfc.showOpenDialog((Component)ev.getSource());
              if (returnVal == JFileChooser.APPROVE_OPTION) {
		File f = jfc.getSelectedFile();
		taParam.setText(f.getAbsolutePath());
	      }
	      lParam.hideBorder();
              taParam.requestFocusInWindow();
	   }
	});
      }
   }

   public void addClear() {
      cParam.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent ev) {
	   taParam.setText("");
	   lParam.showBorder();
           taParam.requestFocusInWindow();
	}
      });
   }

   public void addFocusBorder() {
      taParam.addFocusListener(new FocusListener() {
	public void focusGained(FocusEvent fe) {
	   lParam.hideBorder();
	}
	public void focusLost(FocusEvent fe) {
	   if (taParam.getText().trim().equals("")) lParam.showBorder();
	}
      });
   }

   public void setGray(boolean isGray) {
      if (isGray) {
        taParam.setEnabled(false);
        bParam.setEnabled(false);
        cParam.setEnabled(false);
      } else {
        taParam.setEnabled(true);
        bParam.setEnabled(true);
        cParam.setEnabled(true);
      }
   }

   public void addMirror(FATBOYParam p) {
      mirrorParam = p;
      taParam.addFocusListener(new FocusListener() {
	public void focusGained(FocusEvent ev) {
        }
        public void focusLost(FocusEvent ev) {
           String temp = taParam.getText();
           if (!mirrorParam.getParamValue().equalsIgnoreCase(temp)) {
              mirrorParam.setParamValue(temp);
           }
        }
      });
      String tt = p.lParam.getToolTipText();
      if (tt != null) setToolTipText(tt);
   }

   public void updateMirror() {
      if (mirrorParam == null) return;
      String temp = taParam.getText();
      if (!mirrorParam.getParamValue().equalsIgnoreCase(temp)) {
	mirrorParam.setParamValue(temp);
      }
   }

   public void addTo(Container panel) {
      panel.add(lParam);
      panel.add(spParam);
      panel.add(bParam);
      panel.add(cParam);
   }

   public void setConstraints(SpringLayout layout, int y1, int x1, int x2, int x3, int y2, JComponent leftComponent, JComponent topComponent, boolean useLeft3, boolean useTop4, boolean isTop) {
      String topPos, leftPos3, topPos4;
      JComponent left3, top4;
      if (isTop) topPos = SpringLayout.NORTH; else topPos = SpringLayout.SOUTH;
      if (useLeft3) {
	left3 = leftComponent;
	leftPos3 = SpringLayout.WEST;
      } else {
	left3 = spParam;
	leftPos3 = SpringLayout.EAST;
      }
      if (useTop4) {
	top4 = topComponent;
	topPos4 = topPos;
      } else {
	top4 = bParam;
        topPos4 = SpringLayout.SOUTH;
      }
      layout.putConstraint(SpringLayout.WEST, lParam, x1, SpringLayout.WEST, leftComponent);
      layout.putConstraint(SpringLayout.NORTH, lParam, y1, topPos,topComponent);
      layout.putConstraint(SpringLayout.WEST, spParam, x2, SpringLayout.WEST, leftComponent);
      layout.putConstraint(SpringLayout.NORTH, spParam,y1,topPos,topComponent);
      layout.putConstraint(SpringLayout.WEST, bParam, x3, leftPos3, left3);
      layout.putConstraint(SpringLayout.NORTH, bParam, y1, topPos,topComponent);
      layout.putConstraint(SpringLayout.WEST, cParam, x3, leftPos3, left3);
      layout.putConstraint(SpringLayout.NORTH, cParam, y2, topPos4, top4);
   }

}

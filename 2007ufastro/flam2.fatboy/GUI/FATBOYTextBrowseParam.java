/*
 * Author:      Craig Warner
 * Company:     University of Florida
 */

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class FATBOYTextBrowseParam extends FATBOYParam {

   public FATBOYTextBrowseParam(String labelText, int size, String bText) {
      this(labelText, FATBOYLabel.NONE, size, bText);
   }

   public FATBOYTextBrowseParam(String labelText, int border, int size, String bText) {
      lParam = new FATBOYLabel(labelText, border, true);
      tParam = new JTextField(size);
      bParam = new JButton(bText);
   }

   public FATBOYTextBrowseParam(String labelText, int size, String bText, String text) {
      this(labelText, FATBOYLabel.NONE, size, bText, text);
   }

   public FATBOYTextBrowseParam(String labelText, int border, int size, String bText, String text) {
      lParam = new FATBOYLabel(labelText, border, true);
      tParam = new JTextField(size);
      bParam = new JButton(bText);
      tParam.setText(text);
   }

   public void setParamValue(String value) {
      tParam.setText(value);
   }

   public String getParamValue() {
      return tParam.getText();
   }

   public void addBrowse() {
      bParam.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent ev) {
	   JFileChooser jfc = new JFileChooser(".");
           int returnVal = jfc.showOpenDialog((Component)ev.getSource());
           if (returnVal == JFileChooser.APPROVE_OPTION) {
	      File f = jfc.getSelectedFile();
	      tParam.setText(f.getAbsolutePath());
	   }
	   lParam.hideBorder();
	   tParam.requestFocusInWindow();
	}
      });
   }

   public void addBrowse(String s) {
      final String dir;
      if (new File(s).exists()) dir = s; else dir = "."; 
      bParam.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent ev) {
           JFileChooser jfc = new JFileChooser(dir);
           int returnVal = jfc.showOpenDialog((Component)ev.getSource());
           if (returnVal == JFileChooser.APPROVE_OPTION) {
              File f = jfc.getSelectedFile();
              tParam.setText(f.getAbsolutePath());
           }
           lParam.hideBorder();
           tParam.requestFocusInWindow();
        }
      });
   }

   public void addDirBrowse() {
      bParam.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent ev) {
           JFileChooser jfc = new JFileChooser(".");
	   jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
           int returnVal = jfc.showOpenDialog((Component)ev.getSource());
           if (returnVal == JFileChooser.APPROVE_OPTION) {
              File f = jfc.getSelectedFile();
              tParam.setText(f.getAbsolutePath());
           }
           lParam.hideBorder();
           tParam.requestFocusInWindow();
        }
      });
   }

   public void addFocusBorder() {
      tParam.addFocusListener(new FocusListener() {
	public void focusGained(FocusEvent fe) {
	   lParam.hideBorder();
	}
	public void focusLost(FocusEvent fe) {
	   if (tParam.getText().trim().equals("")) lParam.showBorder();
	}
      });
   }

   public void setGray(boolean isGray) {
      if (isGray) {
        tParam.setEnabled(false);
        bParam.setEnabled(false);
      } else {
        tParam.setEnabled(true);
        bParam.setEnabled(true);
      }
   }

   public void addMirror(FATBOYParam p) {
      mirrorParam = p;
      tParam.addFocusListener(new FocusListener() {
        public void focusGained(FocusEvent ev) {
        }
        public void focusLost(FocusEvent ev) {
           String temp = tParam.getText();
           if (!mirrorParam.getParamValue().equalsIgnoreCase(temp)) {
              mirrorParam.setParamValue(temp);
           }
        }
      });
      String tt = p.lParam.getToolTipText();
      if (tt != null) setToolTipText(tt);
   }

   public void updateMirror() {
      if (mirrorParam == null) return;
      String temp = tParam.getText();
      if (!mirrorParam.getParamValue().equalsIgnoreCase(temp)) {
        mirrorParam.setParamValue(temp);
      }
   }

   public void addTo(Container panel) {
      panel.add(lParam);
      panel.add(tParam);
      panel.add(bParam);
   }

   public void setConstraints(SpringLayout layout, int y, int x1, int x2, int x3, JComponent leftComponent, JComponent topComponent) {
      this.setConstraints(layout, y, x1, x2, x3, leftComponent, topComponent, false, false);
   }

   public void setConstraints(SpringLayout layout, int y, int x1, int x2, int x3, JComponent leftComponent, JComponent topComponent, boolean useLeft3, boolean isTop) {
      String topPos, leftPos3;
      JComponent left3;
      if (isTop) topPos = SpringLayout.NORTH; else topPos = SpringLayout.SOUTH;
      if (useLeft3) {
	left3 = leftComponent;
	leftPos3 = SpringLayout.WEST;
      } else {
	left3 = tParam;
	leftPos3 = SpringLayout.EAST;
      }
      layout.putConstraint(SpringLayout.WEST, lParam, x1, SpringLayout.WEST, leftComponent);
      layout.putConstraint(SpringLayout.NORTH, lParam, y, topPos,topComponent);
      layout.putConstraint(SpringLayout.WEST, tParam, x2, SpringLayout.WEST, leftComponent);
      layout.putConstraint(SpringLayout.NORTH, tParam,y,topPos,topComponent);
      layout.putConstraint(SpringLayout.WEST, bParam, x3, leftPos3, left3);
      layout.putConstraint(SpringLayout.NORTH, bParam, y, topPos,topComponent);
   }

}


/* INDENT OFF */

/*+
 *
 * FILENAME
 * -------- 
 * flamIsCad.h
 *
 * PURPOSE
 * -------
 * Declare public CAD record support functions
 * 
 * FUNCTION NAME(S)
 * ----------------
 * 
 * DEPENDENCIES
 * ------------
 *
 * LIMITATIONS
 * -----------
 * 
 * AUTHOR
 * ------
 * William Rambold (wrambold@gemini.edu)
 * 
 * HISTORY
 * -------
 * 2000/12/11  WNR  Initial coding
 */

/* INDENT ON */

/* ===================================================================== */


/*
 *  Module level definitions
 */

#define TRX_MAX_QL_STREAMS       14


/*
 *
 *  Public function prototypes
 *
 */

long flamIsAbortProcess (cadRecord * pcr);
long flamIsDataModeProcess (cadRecord * pcr);
long flamIsDatumProcess (cadRecord * pcr);
long flamIsDebugProcess (cadRecord * pcr);
/* static long flamIsInitProcess (cadRecord * pcr); */
long flamIsInsSetupProcess (cadRecord * pcr);
static long flamIsNullInit (cadRecord * pcr);
long flamIsObsSetupProcess (cadRecord * pcr);
long flamIsObserveProcess (cadRecord * pcr);
long flamIsParkProcess (cadRecord * pcr);
long flamIsRebootInit (cadRecord * pcr);
long flamIsRebootProcess (cadRecord * pcr);
long flamIsSetDhsInfoProcess (cadRecord * pcr);
long flamIsSetWcsProcess (cadRecord * pcr);
long flamIsStopProcess (cadRecord * pcr);
long flamTestProcess (cadRecord * pcr);

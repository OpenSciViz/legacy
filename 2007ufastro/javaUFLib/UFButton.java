package javaUFLib;

//Title:        UFButton.java
//Version:      2.0
//Copyright:    Copyright (c) Frank Varosi
//Author:       Frank Varosi, 2003
//Company:      University of Florida
//Description:  Extension of JButton class.

import java.awt.*;
import javax.swing.JButton;

/**
 * Creates a Button with default very light gray background = Color(222,222,222).
 */

public class UFButton extends JButton
{
    public static final
	String rcsID = "$Name:  $ $Id: UFButton.java,v 1.1 2005/08/11 18:21:06 drashkin Exp $";

    Color _colorBackg = new Color(222,222,222);
    Color _colorForeg;
    String _name;
//-------------------------------------------------------------------------------
    /**
     * Default Constructor
     */
    public UFButton() {
	try  {
	    setBackground(_colorBackg);
	    setDefaultCapable(false);
	}
	catch(Exception ex) {
	    System.out.println("Error creating UFButton : " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Basic Constructor
     *@param name String: Text to call button
     */
    public UFButton(String name) {
	super(name);
	try  {
	    _name = name;
	    setBackground(_colorBackg);
	    setDefaultCapable(false);
	}
	catch(Exception ex) {
	    System.out.println("Error creating UFButton with name " + name + ": " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param  name        String: Text to call button
     *@param  background  Color:  color object to use for button background
     */
    public UFButton(String name, Color background) {
	super(name);
	try  {
	    _name = name;
	    _colorBackg = background;
	    setBackground(_colorBackg);
	    setDefaultCapable(false);
	}
	catch(Exception ex) {
	    System.out.println("Error creating UFButton with name " + name + ": " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param  name        String: Text to call button
     *@param  background  Color:  color object to use for button background
     *@param  foreground  Color:  color object to use for button foreground
     */
    public UFButton(String name, Color background, Color foreground) {
	super(name);
	try  {
	    _name = name;
	    _colorBackg = background;
	    _colorForeg = foreground;
	    setBackground(_colorBackg);
	    setForeground(_colorForeg);
	    setDefaultCapable(false);
	}
	catch(Exception ex) {
	    System.out.println("Error creating UFButton with name " + name + ": " + ex.toString());
	}
    }
}

package javaUFLib;

import java.io.*;
import java.net.*;
import java.util.*;
import java.text.*;
import java.awt.*;
import javax.swing.*;
import javaUFProtocol.*;

public class UFFITSheader extends LinkedList {

    public static final
	String rcsID = "$Name:  $ $Id: UFFITSheader.java,v 1.4 2006/03/03 20:21:34 drashkin Exp $";

    protected static boolean _verbose = false;
    protected static File _path = null;

    protected String  _FITSrecord = "";
    protected String  _FITSkey = "";
    protected UFFrameConfig _frameConfig;

    protected byte[] _blanks = new byte[80];
    protected String _blankLine;

    public UFFITSheader() {
	_init();
	_FITSrecord = "";
    }
    public UFFITSheader(String comment, boolean simple) {
	_init();
	_start(comment,simple);
    }
    public UFFITSheader(UFFrameConfig fc, String comment, boolean simple) {
	_init();
	_start( comment, simple );
	addrec("BITPIX",   fc.depth,  "Bits per pixel");
	addrec("NAXIS" ,   2,         " ");
	addrec("NAXIS1",   fc.width,  "X dimension of array");
	addrec("NAXIS2",   fc.height, "Y dimension of array");
	addrec("BUFCOADD", fc.coAdds, "# of saveset coadds in frame buffer");
	date();
	_frameConfig = new UFFrameConfig( fc.width, fc.height, fc.depth );
    }
    public UFFITSheader(UFFrameConfig fc, UFObsConfig oc, String comment, boolean simple) {
	_init();
	_start(comment, simple);
	addrec("BITPIX", fc.depth,     "Bits per pixel");
	addrec("NAXIS" , 6,            " ");
	addrec("NAXIS1", fc.width,     "X dimension of array");
	addrec("NAXIS2", fc.height,    "Y dimension of array");
	addrec("NAXIS3", oc.chopBeams(), "Number of chop positions");
	addrec("NAXIS4", oc.saveSets(),  "Number of savesets per nod phase");
	addrec("NAXIS5", oc.nodBeams(),  "Number of nod positions");
	addrec("NAXIS6", oc.nodSets(),   "Number of nod cycles");
	addrec("BUFCOADD", fc.coAdds,    "# of savesets coadds in frame buffer");
	date();
	_frameConfig = new UFFrameConfig( fc.width, fc.height, fc.depth );
    }

    public String description() { return new String("UFFITSheader"); }

    private void _init() {
	for( int i=0; i < _blanks.length; i++ ) _blanks[i] = 32;
	_blankLine = new String( _blanks );
    }

    public UFFrameConfig getFrameCon()
    {
	int bitpix = getInt("BITPIX");
	int nax = getInt("NAXIS");
	int npx = getInt("NAXIS1");
	int npy = 1;
	if( nax > 1 ) npy = getInt("NAXIS2");

	_frameConfig = new UFFrameConfig( npx, npy, bitpix );
	return _frameConfig;
    }

    public UFFrameConfig fullFrameCon()
    {
	if( _frameConfig == null ) getFrameCon();

	_frameConfig.frameCoadds = getInt("FRMCOADD");
	_frameConfig.chopCoadds = getInt("CHPCOADD");
	_frameConfig.frameTime = getFloat("FRMTIME");
	_frameConfig.savePeriod = 1/getFloat("SAVEFREQ");
	_frameConfig.coAdds = getInt("BUFCOADD");
	if( _frameConfig.coAdds < 1 ) _frameConfig.coAdds = 1;

	return _frameConfig;
    }

//----------------------------------------------------------------------------------------------------
    // write FITS header and data to a file:

    public int writeFITSfile( UFInts data, String filename, String[] moreHeaderInfo )
    {
	if( data == null ) {
	    System.err.println("UFFITSheader::writeFITSfile> data object is empty.");
	    return 0;
	}

	if( moreHeaderInfo != null ) addrecs( moreHeaderInfo );
	this.end();

	if( filename == null )
	    if( (filename = chooseFITSfileName(data.name())) == null ) return 0;

	FileOutputStream foutps = writeFITSheader( filename );

	if( foutps != null ) {
	    if(_verbose) System.out.println("UFFITSheader::writeFITSfile> writing data...");
	    try {
		int nbwritten = data.sendData( new DataOutputStream( foutps ) );
		foutps.close();
		System.out.println("UFFITSheader::writeFITSfile> " + nbwritten +
				   " bytes of data written to: " + filename);
		return nbwritten;
	    }
	    catch(IOException ioe) {
		System.err.println("UFFITSheader::writeFITSfile> " + ioe.toString());
		ioe.printStackTrace();
		try{ foutps.close(); } catch(Exception ex) {}
		return 0;
	    }
	}
	else {
	    System.err.println("UFFITSheader::writeFITSfile> File Output Stream is NULL.");
	    return 0;
	}
    }

    public int writeFITSfile( int[] data, String dataname, String filename, String[] moreHeaderInfo )
    {
	return writeFITSfile( new UFInts(dataname, data), filename, moreHeaderInfo );
    }

    public int writeFITSfile( int[] data, String filename, String[] moreHeaderInfo )
    {
	return writeFITSfile( data, "data", filename, moreHeaderInfo );
    }
//----------------------------------------------------------------------------------------------------
    // read FITS header and data from a file:

    public UFProtocol readFITSfile( String filename, String name )
    {
	if( filename == null )
	    if( (filename = chooseFITSfileName( name, true )) == null ) return null;

	FileInputStream finps = readFITSheader( filename );
	UFFrameConfig fc = getFrameCon();

	if( finps != null ) {

	    int npix = fc.width * fc.height;
	    String dname = filename.substring(filename.lastIndexOf("/")+1);
	    UFProtocol data = null;

	    switch( fc.depth )
		{
		case 16:
		    data = new UFShorts( dname, npix ); break;
		case 32:
		    data = new UFInts( dname, npix ); break;
		case -32:
		    data = new UFFloats( dname, npix ); break;
		default:
		    data = new UFInts( dname, npix );
		}

	    System.out.println("UFFITSheader::readFITSfile> reading " + data.elements() + " of "
			       + fc.depth + " bit pixels of data from file: " + filename + "...");
	    try {
		//int nbread = ((UFInts)data).recvData( new DataInputStream( finps ) );
		int nbread = data.recvData( new DataInputStream( finps ) );
		finps.close();
		System.out.println("UFFITSheader::readFITSfile> " + nbread +
				   " bytes of data read from: " + filename);
		return (UFProtocol)data;
	    }
	    catch(IOException ioe) {
		System.err.println("UFFITSheader::readFITSfile> " + ioe.toString());
		ioe.printStackTrace();
		try{ finps.close(); } catch(Exception ex) {}
		return null;
	    }
	}
	else {
	    System.err.println("UFFITSheader::readFITSfile> File Input Stream is NULL.");
	    return null;
	}
    }

    public UFProtocol readFITSfile( String filename ) { return readFITSfile( filename, "data" ); }

    public UFProtocol readFITSfile() { return readFITSfile( null, "data" ); }

//----------------------------------------------------------------------------------------------------

    public String chooseFITSfileName() { return chooseFITSfileName("data", false); }
    public String chooseFITSfileName(String name) { return chooseFITSfileName( name, false ); }
    public String chooseFITSfileName(boolean toRead) { return chooseFITSfileName("data", toRead); }

    public String chooseFITSfileName(String name, boolean toRead)
    {
	String filename = null;
	JFileChooser jfc = new JFileChooser(_path);
	ExtensionFilter ftype = new ExtensionFilter("FITS files:", new String[]{".fit",".fits"});
	jfc.setFileFilter(ftype);
	int jfcRetVal;

	if( toRead ) {
	    jfc.setDialogTitle("Read > " + name + " < from FITS file:");
	    jfcRetVal = jfc.showOpenDialog(null);
	}
	else {
	    jfc.setDialogTitle("Write > " + name + " < to FITS file:");
	    jfcRetVal = jfc.showSaveDialog(null);
	}

	if( jfcRetVal == JFileChooser.APPROVE_OPTION ) {
	    _path = jfc.getCurrentDirectory();
	    filename = jfc.getSelectedFile().getAbsolutePath();
	    if( filename.length() < 1 ) filename = "tmp.fits";
	    if( !filename.endsWith(".fit") && !filename.endsWith(".fits") ) filename += ".fits";
	}
	else System.out.println("UFFITSheader::chooseFITSfileName> canceled...");

	return filename;
    }
//----------------------------------------------------------------------------------------------------

    public FileOutputStream writeFITSheader(String filename)
    {
	try {
	    File theFile = new File( filename );

	    if( !theFile.createNewFile() ) {
		System.err.println("UFFITSheader::writeFITSheader> failed creating file: " + filename
				   + ", does file already exist ?");
		Toolkit.getDefaultToolkit().beep();
		JOptionPane.showMessageDialog(null,"File [" + filename + "] already exists.",
					      "ERROR", JOptionPane.ERROR_MESSAGE);		
		return null;
	    }

	    FileOutputStream foutps = new FileOutputStream( theFile );
	    ListIterator Lit = this.listIterator();

	    while( Lit.hasNext() ) {
		String frec = (String)Lit.next();
		if( frec.length() > 80 )
		    foutps.write( frec.getBytes(), 0, 80 );
		else {
		    foutps.write( frec.getBytes() );
		    if( frec.length() < 80 ) foutps.write( _blanks, 0, 80 - frec.length() );
		}
	    }

	    if(_verbose) System.out.println("UFFITSheader::writeFITSheader> wrote " + size() + " FITS recs.");

	    if( this.size() % 36 > 0 ) {
		int nextra = 36 - this.size() % 36;
		for( int i=0; i < nextra; i++ ) foutps.write( _blanks );
		if(_verbose) System.out.println("UFFITSheader::writeFITSheader> wrote "+nextra+" blank recs.");
	    }

	    return foutps;
	}
	catch( Exception e ) {
	    System.err.println("UFFITSheader::writeFITSheader> "+e.toString());
	    return null;
	}
    }
//----------------------------------------------------------------------------------------------------

    public FileInputStream readFITSheader(String filename)
    {
	try {
	    File theFile = new File( filename );

	    if( !theFile.canRead() ) {
		System.err.println("UFFITSheader::readFITSheader> cannot read file: " + filename);
		Toolkit.getDefaultToolkit().beep();
		JOptionPane.showMessageDialog(null,"File [" + filename + "] is not readable.",
					      "ERROR", JOptionPane.ERROR_MESSAGE);		
		return null;
	    }

	    FileInputStream finps = new FileInputStream( theFile );
	    this.clear();

	    try {
		byte[] frec = new byte[80];
		boolean end = false;
		int nrec=0;

		while( !end ) {
		    finps.read( frec );
		    ++nrec;
		    String Line = new String( frec );
		    if( Line.indexOf("END") == 0 ) end = true;

		    if( end )
			this.end();
		    else if( this.addrec( Line ) < 0 ) {
			System.err.println("UFFITSheader::readFITSheader> rec # " + nrec + " is bad ? ");
			System.err.println("UFFITSheader::readFITSheader> rec = " + Line);
		    }
		}

		if( nrec % 36 > 0 ) {
		    int nextra = 36 - nrec % 36;
		    for( int i=0; i < nextra; i++ ) finps.read( frec );
		    if(_verbose)
			System.out.println("UFFITSheader::readFITSheader> read "+nextra+" blank recs.");
		}
	    }
	    catch(EOFException eof) {
		System.err.println("UFFITSheader::readFITSheader> " + eof.toString());
	    }
	    catch(IOException ioe) {
		System.err.println("UFFITSheader::readFITSheader> "+ ioe.toString());
	    }
	    catch( Exception e ) {
		System.err.println("UFFITSheader::readFITSheader> " + e.toString());
	    }

	    if(_verbose) System.out.println("UFFITSheader::readFITSheader> read " + size() + " FITS recs.");

	    return finps;
	}
	catch( Exception e ) {
	    System.err.println("UFFITSheader::readFITSheader> "+e.toString());
	    return null;
	}
    }
//----------------------------------------------------------------------------------------------------
    // methods to add FITS records (keyword = value / comment) to the FITS header:

    public int addrec(String keyword, String value, String comment) {
	return _addrec( keyword, value, comment, true );
    }

    public int addrec(String keyword, int value, String comment) {
	return _addrec( keyword, value + "", comment, false );
    }

    public int addrec(String keyword, float value, String comment) {
	return _addrec( keyword, value + "", comment, false );
    }

    public int addrec(String keyword, double value, String comment) {
	return _addrec( keyword, value + "", comment, false );
    }

    public int replace(String keyword, String value, String comment) {
	_setrec( keyword, value, comment, true );
	return _replace();
    }
    public int replace(String keyword, int value, String comment) {
	_setrec( keyword, value + "", comment, false );
	return _replace();
    }
    public int replace(String keyword, float value, String comment) {
	_setrec( keyword, value + "", comment, false );
	return _replace();
    }
    public int replace(String keyword, double value, String comment) {
	_setrec( keyword, value + "", comment, false );
	return _replace();
    }
//----------------------------------------------------------------------------------------------------

    ///helper for ctors: sets the value of _FITSrecord and _FITSkey.

    protected int _setrec(String keyword, String value, String comment, boolean quote)
    {
	_FITSkey = rmJunk(keyword).toUpperCase();
	String valq = rmJunk(value);
	String cmt = rmJunk(comment);
	int vlen = valq.length();
	int klen = _FITSkey.length();
	int clen = cmt.length();

	if( quote ) {
	    if( vlen > 18)
		valq = valq.substring(0,18);
	    else if( vlen < 18)
		valq += _blankLine.substring( 0, 18 - vlen );
	    valq = "'" + valq + "'";
	}
	else {
	    if( vlen > 20)
		valq = valq.substring(0,20);
	    else if( vlen < 20)
		valq = _blankLine.substring( 0, 20 - vlen ) + valq;
	}

	if( klen > 8 )
	    _FITSkey = _FITSkey.substring(0,8);
	else if( klen < 8 )
	    _FITSkey += _blankLine.substring( 0, 8 - klen );

	if( clen > 48)
	    cmt = cmt.substring(0,48);
	else if( clen < 48)
	    cmt += _blankLine.substring( 0, 48 - clen );

	_FITSrecord = _FITSkey + "= " + valq + " /" + cmt;

	if(_verbose) System.out.println("UFFITSheader::_setrec> _FITSrecord=["
					+ _FITSrecord + "] " + _FITSrecord.length());

	return _FITSrecord.length();
    }

    protected int _addrec(String keyword, String value, String comment, boolean quote)
    {
	if( _setrec( keyword, value, comment, quote ) == 80 ) addLast( _FITSrecord );
	return this.size();
    }

    ///helper for ctors: adds one standard rec containing SIMPLE keyword
    protected int _start(String comment, boolean simple)
    {
	this.clear();

	if( simple )
	    return _addrec("SIMPLE","T", comment, false );
	else
	    return _addrec("SIMPLE","F", comment, false );
    }

    ///helper for replace funcs: uses current value of attrib _FITSkey.
    protected int _replace() {
	int cnt = this.size();
	boolean replaced = false;

	for( int i=0; i<cnt && !replaced; ++i ) {
	    String entry = (String)this.get(i);
	    if( entry.indexOf(_FITSkey) == 0 ) {
		remove(i);
		add( i, _FITSrecord );
		replaced = true;
	    }
	}

	if( !replaced ) addLast(_FITSrecord); // then just add it?

	return this.size();
    }
//----------------------------------------------------------------------------------------------------

    public int addrec(Map valhash, Map comments) {
	if (valhash.isEmpty())
	    return this.size();
	Iterator i  = valhash.keySet().iterator();
	String key, val, comment;
	while (i.hasNext()) {
	    key = (String) i.next();
	    val = (String) valhash.get(key);
	    if (comments.containsKey(key))
		comment = (String) comments.get(key);
	    else
		comment = "no comment";
	    addrec(key, val, comment);
	}
	return this.size();
    }

    // assuming args are in FITS header entry format
    // trucation or extension to 80 chars may occur:
    public int addrec(String s)
    {
	if (s.indexOf("=") != 8) return(-1); // clearly not a FITS header entry?
	int slen = s.length();

	if( slen > 80)
	    s = s.substring(0,80);
	else if( slen < 80)
	    s += _blankLine.substring( 0, 80 - slen );

	addLast(s);
	return this.size();
    }

    public int addrecs(String[] sar) {
	if( sar == null )
	    return this.size();
	
	for( int i=0; i < sar.length ; ++i ) addrec( sar[i] );
	return this.size();
    }

    public int addrecs(UFStrings ufs) {
	if (ufs == null)
	    return this.size();
	
	for( int i=0; i<ufs.numVals(); ++i ) addrec( (String)ufs.valData(i) );
	return this.size();
    }

    public int end() { // this adds the "END" record to header
	_FITSrecord = "END";
	_FITSrecord += _blankLine.substring( 0, 80 - _FITSrecord.length() );
	addLast(_FITSrecord);
	return this.size();
    }

    public int date(String comment) {
	SimpleDateFormat df = new SimpleDateFormat("yyyy:DDD:HH:mm:ss");
	String time = df.format(new Date());
	return addrec("DATE_FH", time, comment);
    }

    public int date() { return this.date("UT of header creation (YYYY:DAY:HH:MM:SS)"); }

    // convert UFFITSheader object into UFStrings object for client/server transactions:
    public UFStrings Strings(String name)
    {
	String[] FITSrecs = new String[this.size()];
	ListIterator Lit = this.listIterator();
	int i=0;
	while( Lit.hasNext() ) FITSrecs[i++] = (String)Lit.next();

	return new UFStrings( name, FITSrecs );
    }

    public UFStrings Strings() { return this.Strings("FITSheader"); }

//----------------------------------------------------------------------------------------------------

    public String getRecord( String key ) {

	ListIterator Lit = this.listIterator();
	String Key = key.toUpperCase();

	while( Lit.hasNext() ) {
	    String rec = (String)Lit.next();
	    if( rec.indexOf( Key ) == 0 ) return rec;
	}

	System.err.println("UFFITSheader::getRecord> key [" + Key + "] not found.");
	return null;
    }

    public String getParam( String key ) {

	String rec = getRecord( key );

	if( rec != null ) {
	    int ieq = rec.indexOf("=");
	    if( ++ieq > 1 )
		return rec.substring( ieq, 21+ieq ).trim();
	    else {
		System.err.println("UFFITSheader::getParam> equal sign for key [" + key + "] not found.");
		return null;
	    }
	}
	else return null;
    }

    public int getInt( String key ) {

	String param = getParam( key );
	if( param == null ) return(-1);

	try {
	    return Integer.parseInt( param );
	}
	catch( NumberFormatException nfe ) {
	    System.err.println("UFFITSheader::getInt> key=" + key +
			       ": param=" + param + ": " + nfe.toString() );
	    return(-1);
	}
    }

    public float getFloat( String key ) {

	String param = getParam( key );
	if( param == null ) return(-1);

	try {
	    return Float.parseFloat( param );
	}
	catch( NumberFormatException nfe ) {
	    System.err.println("UFFITSheader::getFloat> key=" + key +
			       ": param=" + param + ": " + nfe.toString() );
	    return(-1);
	}
    }
//----------------------------------------------------------------------------------------------------
    // Static methods:
    /**
     * This next public class used for functions that require more than 1 return value
     * @see readPrmHdr(...), readExtHdr(...)
     */
    public class ReturnVals {
	int w,h,total,nnods,nnodsets,chops,savesets;
	ReturnVals() { w = h = total = nnods = nnodsets = chops = savesets = 0; }
	void clearVals() { w = h = total = nnods = nnodsets = 0; chops = savesets = 1; }
    }

    // convenience function for use by UFDeviceConfig::statusFITS()
    public static UFStrings asStrings(Map valhash, Map comments) {
	UFFITSheader _fitshdr = new UFFITSheader();
	int sz = _fitshdr.addrec(valhash, comments);
	if (sz <= 0)
	    return null;
	return _fitshdr.Strings();
    }

    // convenience functions for client connection to an Agent to fetch FITS fragment
    public static UFStrings fetchFITS(String clientname, Socket soc) {
	if (soc == null)
	    return null;
	//formulate standard agent client key, request:
	String [] vs = { "STATUS", "FITS" };
	UFStrings req = new UFStrings(clientname, vs);
	req.sendTo(soc);
	UFStrings reply = (UFStrings) (UFProtocol.createFrom(soc));
	return reply;
    }

    public static String rmJunk(String st)
    {
	StringBuffer s = new StringBuffer(st);
	if( s.length() <= 0 ) {
	    System.err.println("UFFITSheader::rmJunk> empty string ?");
	    return "";
	}
	
	int pos = s.toString().indexOf("\n");
	while( pos != -1 ) {
	    s.replace(pos, pos+1, " "); // replace with space
	    pos = s.toString().indexOf("\n", 1+pos);
	}
	pos = s.toString().indexOf("\r");
	while( pos != -1 ) {
	    s.replace(pos, pos+1, " "); // replace with space
	    pos = s.toString().indexOf("\r", 1+pos);
	}
	
	// eliminate white (adjacent) multiples
	for( pos = 0; pos < s.length(); ++pos ) {
	    while ( (s.charAt(pos) == ' ' || s.charAt(pos) == '\t') && pos+1 < s.length()
		    && (s.charAt(pos+1) == ' ' || s.charAt(pos+1) == '\t'))
		s.deleteCharAt(pos);
	}

	return s.toString().trim(); // remove leading and trailing whitespace and return
    }
    
    // convenience functions for reading and parsing an MEF (muli-extension FITS file):
    // this parses an integer valued by key:
    public static int fitsInt(String key, String fitsHdrEntry) {
	String s = new String(fitsHdrEntry);
	int pos = s.indexOf(key);
	if (pos == -1)
	    return -1;
	pos = 1+s.indexOf("=",pos);
	if (pos == -1)
	    return -1;
	s = s.substring(1+pos);
	while (s.charAt(0) == ' ') s = s.substring(1); // eliminate leading white sp
	pos = s.indexOf(" ");
	if (pos == -1)
	    pos = s.indexOf("/");
	if (pos != -1) {
	    try {
		return Integer.parseInt(s.substring(0,pos));
	    } catch (NumberFormatException nfe) {
		System.err.println("UFFITSheader::fitsInt> Poorly formatted fits header???");
		return -1;
	    }
	}
	return -1;
    }

    //reads the primary header, skips to the next 2880 block,
    //and returns the entire header as a ufstrings obj:

    public static UFStrings readPrmHdr(FileReader fr, int total, ReturnVals rv){
	//create new rv for internal method storage if null
        if (rv == null) rv = (new UFFITSheader()).new ReturnVals();
	else rv.clearVals();
	char [] entry = new char[81]; Arrays.fill(entry,'\0');
	char []  end = new char[4]; Arrays.fill(end,'\0');
	end[0] = 'E'; end[1] = 'N'; end[2] = 'D';
	int ne = (new String("END")).length();
	char [] naxis = new char[7]; Arrays.fill(naxis,'\0');
	int na = (new String("NAXIS0")).length();
	char [] nods = new char[6]; Arrays.fill(nods,'\0');
	int nn = (new String("NNODS")).length();
	char [] nodsets = new char[9]; Arrays.fill(nodsets,'\0');
	int ns = (new String("NNODSETS")).length();
	String s, key, endkey;
	int fits_sz = 0, fitsunit = 80*36; // check for integral multiple of 2880
	int maxheadsz = 10*fitsunit; // if no END in sight...
	Vector vs = new Vector();
	do {
	    try {
		if ( fr.read(entry,0,80) != 80 ) {
		    System.err.println("UFFTISheader::readPrmFitsHdr> read of primary header failed.");
		    //if (x == -1)
		    //    System.err.println("UFFITSheader::readPrmFitsHdr> end of file.");
		    return null;
		}
	    } catch (IOException ioe) {
		System.err.println("UFFITSheader::readPrmFitsHdr> " + ioe.toString());
		return null;
	    }
	    fits_sz += 80;
	    vs.add(new String(entry));
	    if (_verbose)
		System.out.println("UFFITSheader::readPrmFitsHdr> cnt: "+vs.size()+
				   ", entry: " + entry);
	    for (int i=0; i<na; i++) naxis[i] = entry[i];
	    key = new String(naxis);
	    if (key.equals("NAXIS1"))
		rv.w = fitsInt(key, new String(entry));
	    if (key.equals("NAXIS2"))
		rv.h = fitsInt(key, new String(entry));

	    // nod info:
	    for (int i=0; i<ns; i++) nodsets[i] = entry[i];
	    key = new String(nodsets);
	    if (key.equals("NNODSETS")) 
		rv.nnodsets = fitsInt(key,new String(entry));
	    else {
		for (int i=0; i<nn; i++) nods[i] = entry[i];
		key = new String(nods);
		if (key.equals("NNODS"))
		    rv.nnods = fitsInt(key,new String(entry));
	    }
	    
	    // end:
	    for (int i=0; i<ne; i++) end[i] = entry[i];
	    endkey = new String(end);
	}
	while (!endkey.equals("END") && fits_sz < maxheadsz);

	total = rv.nnods + rv.nnodsets;
	if (_verbose)
	    System.out.println("UFFITSheader::readPrmFitsHdr> char cnt: " + fits_sz+ 
			       ", total extensions/frames expected: " + total);
	if (fits_sz >= maxheadsz && !endkey.equals("END")) {
	    System.err.println("UFFITSheader::readPrmFitsHdr> No 'END' entry, improper or no FITS header? fits_sz= " + fits_sz + ", endKey= " + endkey);
	    return null;
	}
	// before returning header, may need to seek to end of current 2880 
	double padfrac = 1.0 - ( (1.0*fits_sz/fitsunit) - Math.floor(1.0*fits_sz/fitsunit) );
	int pad = (int) Math.ceil(padfrac * fitsunit);
	if (_verbose)
	    System.out.println("UFFITSheader::readPrmFitsHdr> pad cnt: "+pad+
			       ", total header sz (+pad): " + (pad+fits_sz));
	try {
	    if (fr.skip(pad) != pad) 
		System.err.println("UFFITSheader::readPrmFitsHdr> Could not seek to next 2880 block");
	} catch (IOException ioe) {
	    System.err.println("UFFITSheader::readPrmFitsHdr> Could not seek to next 2880 block: "+
			       ioe.toString());
	}
	return new UFStrings("PrimaryHeader",(String [])(vs.toArray()));
    }
    ///this reads an extension header and the data block that follows it and skips to the next 2880 block:
    public static UFInts readExtData(DataInputStream dis, UFStrings exthdr, ReturnVals rv) {
	if (rv == null) rv = (new UFFITSheader()).new ReturnVals();
	else rv.clearVals();
	char [] entry = new char[81]; Arrays.fill(entry,'\0');
	char [] end   = new char[4];  Arrays.fill(end, '\0');
	end[0] = 'E'; end[1] = 'N'; end[2] = 'D';
	int ne = (new String("END")).length();
	char [] naxis = new char[7];  Arrays.fill(naxis,'\0');
	int na = (new String("NAXIS0")).length();
	String s = new String(); String key = new String(); String endkey = new String();
	int fits_sz = 0, fitsunit = 80*36; // check for integral multiple of 2880
	int maxexthdsz = 4 * fitsunit; // allow 4x2880 (4 block extension headers)?
	Vector vs = new Vector();
	do {
	    try {
		byte [] entryB = new byte[81]; for (int i=0; i<81; i++) entryB[i] = 0;
		if ( dis.read(entryB,0,80) != 80 ) {
		    System.err.println("UFFTISheader::readExtData> read of primary header failed.");
		    return null;
		}
		for (int i=0; i<81; i++) entry[i] = (char)entryB[i];
	    } catch (IOException ioe) {
		System.err.println("UFFITSheader::readExtData> " + ioe.toString());
		return null;
	    }
	    fits_sz += 80;
	    if (entry[0] == ' ' && entry[8] == ' ') 
		System.out.println("UFFITSheader::readExtData> blank line, char cnt: " + fits_sz);
	    else if (_verbose)
		System.out.println("UFFITSheader::readExtData> char cnt: " + fits_sz + " -- " + entry);
	    vs.add(new String(entry));
	    for (int i=0; i<na; i++) naxis[i] = entry[i];
	    key = new String(naxis);
	    if (key.equals("NAXIS1")) rv.w = fitsInt(key, new String(entry));
	    if (key.equals("NAXIS2")) rv.h = fitsInt(key, new String(entry));
	    if (key.equals("NAXIS3")) rv.chops = fitsInt(key, new String(entry));
	    if (key.equals("NAXIS4")) rv.savesets = fitsInt(key, new String(entry));
	    for (int i=0; i<ne; i++) end[i] = entry[i];
	    endkey = new String(end);
	}
	while ( !endkey.equals("END") && fits_sz < maxexthdsz);

	if (_verbose)
	    System.out.println("UFFITSheader::readExtData> char cnt: " + fits_sz);
	if (fits_sz >= maxexthdsz && !endkey.equals("END")) {
	    System.out.println("UFFITSheader::readExtData> No 'END' entry, improper or "+
			       "no FITS header? fits_sz= " + fits_sz + ", endkey = " + endkey);
	    return null;
	}
	//before returning ext. header & data, may need to skip to end of current 2880 block:
	double foo = 1.0*fits_sz/fitsunit;
	double floo = Math.floor(foo);
	double padfrac = 1.0 - (foo - floo);
	int pad = (int) Math.ceil(padfrac * fitsunit);
	try {
	    if (dis.skip(pad) != pad) 
		System.err.println("UFFITSheader::readExtData> Could not seek to next 2880 block");
	} catch (IOException ioe) {
	    System.err.println("UFFITSheader::readExtData> Could not seek to next 2880 block: "+
			       ioe.toString());
	}
	int i=0,nvals = rv.w * rv.h * rv.chops * rv.savesets;
	int [] vals = new int[nvals];
	try { for (i=0; i<nvals; i++) vals[i] = dis.readInt(); }
	catch (IOException ioe) {
	    System.err.println("UFFITSheader::readExtData> read of image frame failed, nvalrd: " +i+
			       ", expected: " + nvals);
	    return null;
	}
	fits_sz = i * 4; // 4 bytes per integer
	foo = 1.0*fits_sz/fitsunit;
	floo = Math.floor(foo);
	padfrac = 1.0 - (foo - floo);
	if (padfrac < 0.999999) { // no need to pad if this is 1.0
	    pad = (int) Math.ceil(padfrac * fitsunit);
	    try { dis.skip(pad);}
	    catch (IOException ioe) { System.err.println("UFFITSheader::readExtData> "+ioe.toString());}
	}
	exthdr.setNameAndVals("ExtHeader",(String []) (vs.toArray()));
	UFInts ufi = new UFInts();
	ufi.setNameAndVals("Frame",vals);
	return ufi;
    }
}

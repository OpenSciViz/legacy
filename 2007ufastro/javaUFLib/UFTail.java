package javaUFLib;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.io.*;

public class UFTail extends JTabbedPane{

    public UFTail (String[] fileNames) {
	int count = 0;
	for (int i=0; i<fileNames.length; i++)
	    addNewTail(fileNames[i]);
	addChangeListener(new ChangeListener(){
		public void stateChanged(ChangeEvent e) {
		    setBackgroundAt(getSelectedIndex(),null);
		}
	    });
    }

    public void addNewTail(String fileName) {
	try{
	    File f = new File(fileName.trim());
	    final BufferedInputStream br = 
		new BufferedInputStream(new FileInputStream(f));
	    final JTextArea jta = new JTextArea();
	    jta.setEditable(false);
	    final int myIdx = getTabCount();
	    final JScrollPane jsp = new JScrollPane(jta);
	    add(jsp,f.getName());
	    jta.addKeyListener(new KeyListener(){
		    public void keyPressed(KeyEvent ke) {
			if (
			    (ke.isControlDown() && ke.getKeyCode() == KeyEvent.VK_F) ||
			    (ke.isControlDown() && ke.getKeyCode() == KeyEvent.VK_S) ||
			    (ke.getKeyCode() == KeyEvent.VK_SLASH) ||
			    (ke.getKeyCode() == KeyEvent.VK_FIND)
			    ) {
			    final JFrame j = new JFrame("Find Text");
			    JPanel searchPanel = new JPanel();
			    final JLabel errLabel = new JLabel("");
			    final JTextField searchField = new JTextField("",10);
			    final JCheckBox caseBox = new JCheckBox("Case Sensitive",true);
			    final JCheckBox wrapBox = new JCheckBox("Wrap search",true);
			    searchPanel.add(new JLabel("Search String: "));
			    searchPanel.add(searchField);
			    searchPanel.add(caseBox);
			    searchPanel.add(wrapBox);
			    searchPanel.add(errLabel);
			    JButton searchButton = new JButton("Search");
			    searchButton.addActionListener(new ActionListener(){
				    public void actionPerformed(ActionEvent ae) {
					String ahhhh = jta.getText();
					String searchy = searchField.getText();
					errLabel.setText("");
					if (!caseBox.isSelected()) {
					    ahhhh = ahhhh.toLowerCase();
					    searchy = searchy.toLowerCase();
					}
					int pos = jta.getCaretPosition();
					int foundPos = ahhhh.indexOf(searchy,pos+1);
					if (foundPos != -1) {
					    jta.setCaretPosition(foundPos);
					    jta.setSelectionStart(foundPos);
					    jta.setSelectionEnd(foundPos+searchField.getText().length());
					} else if (wrapBox.isSelected()) {
					    pos = 0;
					    foundPos = ahhhh.indexOf(searchy,pos);
					    if (foundPos != -1) {
						jta.setCaretPosition(foundPos);
						jta.setSelectionStart(foundPos);
						jta.setSelectionEnd(foundPos+searchField.getText().length());
					    } else {
						errLabel.setText("Search string not found");
					    }
					} else errLabel.setText("Search string not found");
				    }
				});
			    JButton cancelButton = new JButton("Cancel");
			    cancelButton.addActionListener(new ActionListener(){
				    public void actionPerformed(ActionEvent ae) {
					j.setVisible(false);
				    }
				});
			    JPanel buttonPanel = new JPanel();
			    buttonPanel.add(searchButton);
			    buttonPanel.add(cancelButton);
			    j.getContentPane().add(searchPanel,BorderLayout.CENTER);
			    j.getContentPane().add(buttonPanel,BorderLayout.SOUTH);
			    j.setSize(200,200);
			    j.setLocation(getLocation());
			    j.setVisible(true);
			    j.setAlwaysOnTop(true);
			}
		    }
		    public void keyReleased(KeyEvent ke) {}
		    public void keyTyped(KeyEvent ke) {
			
		    }
		});
	    new Thread(){
		public boolean autoScroll;
		public void run() {
		    autoScroll = true; 		    
		    
		    jta.addMouseListener(new MouseListener(){
			    public void mousePressed(MouseEvent me) {
				if (me.getButton() == me.BUTTON1) autoScroll = true;
				else if (me.getButton() == me.BUTTON3) autoScroll = false;
			    }
			    public void mouseReleased(MouseEvent me) {}
			    public void mouseClicked(MouseEvent me){}
			    public void mouseExited(MouseEvent me){}
			    public void mouseEntered(MouseEvent me){}
			    });
		    while (true) {
			try {
			    byte []b = new byte[br.available()];
			    if (b.length > 0) {
				br.read(b,0,b.length);
				//if (jsb.getValue() >= jsb.getMaximum() - jsb.getVisibleAmount() -  (jsb.getUnitIncrement(-1)))
				//  autoScroll = true;
				//else
				//  autoScroll = false;
				jta.append(new String(b));
				if (myIdx != getSelectedIndex())
				    setBackgroundAt(myIdx,Color.yellow);
				if (autoScroll) {
				    JScrollBar jsb = jsp.getVerticalScrollBar();
				    jsb.setValue(jsb.getMaximum());
				}
			    }
			    Thread.sleep(500);
			} catch (Exception e) {
			    System.err.println("UFTail.run> "+e.toString());
			    break;
			}
		    }
		}
	    }.start();
	} catch (FileNotFoundException fnfe) {
	    System.err.println("UFTail> "+fnfe.toString());
	} catch (IOException ioe) {
	    System.err.println("UFTail> "+ioe.toString());
	}	
    }

    public static void main(String [] args) {
	JFrame x = new JFrame();
	x.setContentPane(new UFTail(args));
	x.setSize(400,400);
	x.setDefaultCloseOperation(3);
	x.setVisible(true);
	x.setTitle("UFTail");
    }

}

package javaUFLib;

/**
 * Title:        UFMessageLog.java: extends UFTextArea (which extends JTextArea)
 * Version:      (see rcsID)
 * Copyright:    Copyright (c) 2005
 * Authors:      Craig Warner and Frank Varosi
 * Company:      University of Florida
 * Description:  Holds a log of past messages in Vector and displays in text area.
 */

import java.util.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.sql.Time;

public class UFMessageLog extends UFTextArea {
    public static final
	String rcsID = "$Name:  $ $Id: UFMessageLog.java,v 1.18 2006/05/15 14:57:37 warner Exp $";

    protected String Name;
    protected boolean timestamp = true, showDups = false, addDups = false;
    protected String lastMessage = "";
    protected String prevMessage = "";

    private Vector _V;
    private int _maxAllowSize = 20000; //max allowed size of Vector containing Log of UFMessage objects.
    private int _maxLogSize = 100;     //current max Log size.
    private int _NmsgRecvd = 0;
    private JFrame _LogFrame;
    private int _Hsize, _Vsize;
//--------------------------------------------------------------------------------------------------
    /**
     * Constructors
     *@param  nMegaBytes  int: max number of Mega-Bytes allowed for document length.
     *@param  maxLogSize  int: max number of UFMessage objects that will be saved in Vector.
     */
    public UFMessageLog(String name, int nMegaBytes, int maxLogSize, int rows, int cols)
    {
	super( nMegaBytes, rows, cols );
	this.Name = name;
	this.setEditable(false);
	if( maxLogSize > _maxAllowSize ) maxLogSize = _maxAllowSize;
	this._maxLogSize = maxLogSize;
	this._V = new Vector(maxLogSize,10); //use non-zero capacity increment in case more is needed.
    }

    public UFMessageLog(String name, int maxLogSize, int rows, int cols) {
	this(name, 1, maxLogSize, rows, cols);
    }
    public UFMessageLog(int maxLogSize, int rows, int cols) {
	this("?", 1, maxLogSize, rows, cols);
    }

    public UFMessageLog(String name, int nMegaBytes, int maxLogSize)
    {
	super( nMegaBytes );
	this.Name = name;
	this.setEditable(false);
	if( maxLogSize > _maxAllowSize ) maxLogSize = _maxAllowSize;
	this._maxLogSize = maxLogSize;
	this._V = new Vector(maxLogSize,10); //use non-zero capacity increment in case more is needed.
    }

    public UFMessageLog(String name, int maxLogSize) { this(name, 1, maxLogSize); }

    public UFMessageLog(int maxLogSize) { this("?", 1, maxLogSize); }

    public UFMessageLog(String name) { this(name, 1, 100); }

    public UFMessageLog() { this("?", 1, 100); }
//-------------------------------------------------------------------------------

    public void viewFrame(int hsize, int vsize) { viewFrame(this.Name, hsize, vsize); }

    public void viewFrame(String Title, int hsize, int vsize)
    {
	if (_LogFrame != null) {
	    if( _Hsize == hsize && _Vsize == vsize ) {
		_LogFrame.setVisible(true);
		_LogFrame.setState( Frame.NORMAL );
		return;
	    }
	    else _LogFrame.dispose();
	}

	_Hsize = hsize;
	_Vsize = vsize;
	JPanel LogPanel = new JPanel();
	JScrollPane LogScroll = new JScrollPane(this);
	LogScroll.setPreferredSize(new Dimension(hsize,vsize));
	LogPanel.add( LogScroll );

	JPanel controlPanel = new JPanel();
	JButton closeButton = new JButton("Close");
	controlPanel.add(closeButton);
	closeButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ev) {
		    _LogFrame.dispose();
		}
	    });
	final JButton refresh = new JButton("Clear Dups");
	controlPanel.add(refresh);
	refresh.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ev) {
		    showAllMessages();
		}
	    });
	final JCheckBox AutoScroll = new JCheckBox("AutoScroll ",true); 
	controlPanel.add(AutoScroll);
	AutoScroll.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ev) {
		    autoScrollToBottom(AutoScroll.isSelected());
		}
	    });
	final JCheckBox timestamps = new JCheckBox("Timestamps ",true); 
	controlPanel.add(timestamps);
	timestamps.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ev) {
		    setTimestamps(timestamps.isSelected());
		    showAllMessages();
		}
	    });
	final JCheckBox duplicates = new JCheckBox("Duplicates ",false); 
	controlPanel.add(duplicates);
	duplicates.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ev) {
		    addDuplicates(duplicates.isSelected());
		    showDuplicates(duplicates.isSelected());
		}
	    });
	final String[] nSave = {"1000","2000","4000","8000"};
	final JComboBox maxSave = new JComboBox(nSave);
	controlPanel.add(new JLabel("Max Save="));
	controlPanel.add(maxSave);
	maxSave.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ev) {
		    setMaxLogSize( Integer.parseInt( (String)maxSave.getSelectedItem() ) );
		}
	    });

	_LogFrame = new JFrame("Log: " + Title);
	_LogFrame.getContentPane().setLayout(new BorderLayout()); 
	_LogFrame.getContentPane().add( LogPanel, BorderLayout.NORTH );
	_LogFrame.getContentPane().add( controlPanel, BorderLayout.SOUTH );
	_LogFrame.pack();
	_LogFrame.setVisible(true);
	this.showAllMessages();
    }
//--------------------------------------------------------------------------------------------------

    public void setName(String name) { this.Name = name; }

    public void setTimestamps(boolean b) { this.timestamp = b; }

    public void showDuplicates(boolean b) { this.showDups = b; }

    public void addDuplicates(boolean b) { this.addDups = b; }

    public int getLogSize() { return _V.size(); }

    public void setMaxLogSize(int n) {
	if( n > _maxAllowSize ) n = _maxAllowSize;
	_maxLogSize = n;
    }

    public void setMaxAllowSize(int n) { _maxAllowSize = n; }

    public int maxAllowSize() { return _maxAllowSize; }
    public int maxLogSize() { return _maxLogSize; }

    private void setToolTipText() {
	this.setToolTipText("# msgs displayed="+_NmsgDisp+", # recvd="+_NmsgRecvd+", saved="+_V.size());
    }
//--------------------------------------------------------------------------------------------------
    /*
     * UFMessageLog#appendMessage
     * Adds message to vector Log, first checking for duplicates, and appends it to TextArea.
     * Default is to keep just the oldest and newest (current) of duplicate messages,
     * removing all the middle duplicates.
     *@param  message  String: text to add into vector storage of message Log and append to TextArea.
     */
    public void appendMessage(String message)
    {
	addMessage( message );
	appendLastMessage();
    }
//--------------------------------------------------------------------------------------------------
    /*
     * UFMessageLog#addMessage
     * Adds message to vector Log, first checking for duplicates.
     * Default is to keep just the oldest and newest (current) of duplicate messages,
     * removing all the middle duplicates.
     *@param  message  String: text to add into vector storage of message Log.
     */
    public void addMessage(String message) {
      ++_NmsgRecvd;
      while( _V.size() >= _maxLogSize ) _V.removeElementAt(0);
      boolean duplicate = false;

      if( message.equals(lastMessage) && message.equals(prevMessage) ) duplicate = true;
      else {
	  prevMessage = lastMessage;
	  lastMessage = message;
      }

      if( !addDups && duplicate ) { //remove the older middle duplicate (keeping newest and oldest).
	  int size = _V.size();
	  if (size > 0) _V.removeElementAt(size-1);
      }

      _V.add(new UFMessage(message, duplicate));
    }

    public void addMessageAt(int x, String message) {
	++_NmsgRecvd;
	if (x >= 0 && x < _V.size()) _V.add(x, new UFMessage(message));
    }

    // Display all messages in text area.
    // If showDups==false then display just the first and last in each sequence of duplicates.
    // This is done by checking if next message is also a duplicate.

    public void showAllMessages() {
	int size = _V.size();
	if( size < 1 ) return;
	String messages = "";
	UFMessage M = (UFMessage)_V.firstElement();
	_NmsgDisp = 0;
	for( int i=1; i < size; i++ ) {
	    UFMessage nextM = (UFMessage)_V.elementAt(i);
	    if( showDups || !M.duplicate || !nextM.duplicate  ) {
		messages += M.getMessage(timestamp) ; 
		++_NmsgDisp;
	    }
	    M = nextM;
	}
	//always include the last message:
	messages += M.getMessage(timestamp); 
	++_NmsgDisp;
	this.setText(messages);
	this.setToolTipText();
    }

    public String[] getAllMessages() {
	int size = _V.size();
	if( size < 1 ) return null;
	UFMessage[] Mall = new UFMessage[size];
	UFMessage M = (UFMessage)_V.firstElement();
	int n = 0;
	for( int i=1; i < size; i++ ) {
	    UFMessage nextM = (UFMessage)_V.elementAt(i);
	    if( showDups || !M.duplicate || !nextM.duplicate  ) Mall[n++] = M;
	    M = nextM;
	}
	//always include the last message:
	Mall[n++] = M;
	String[] messages = new String[n];
	for (int j = 0; j < n; j++) messages[j] = Mall[j].getMessage(timestamp); 
	return messages;
    }

    public void appendLastMessage() {
	if( _V.size() < 1 ) return;
	UFMessage M = (UFMessage)_V.lastElement();
	this.append( M.getMessage(timestamp) );
	this.setToolTipText();
    }

    public String getLastMessage() {
	if( _V.size() < 1 ) return null;
	UFMessage M = (UFMessage)_V.lastElement();
	return M.getMessage(timestamp);
    }

    public void showLastMessage() {
	if( _V.size() < 1 ) return;
	UFMessage M = (UFMessage)_V.lastElement();
	_NmsgDisp = 1;
	this.setText( M.getMessage(timestamp) );
	this.setToolTipText();
    }

    public String[] getLastMessages(int x) {
	int size = _V.size();
	if( size < 1 ) return null;
	if( x > size ) x = size;
	UFMessage[] M = new UFMessage[x];
	int n = 0;
	for (int j = size-x; j < size; j++) {
	    UFMessage temp = (UFMessage)_V.elementAt(j);
	    if( showDups || !temp.duplicate || j==size-1 ) M[n++] = temp;
	}
	String[] messages = new String[n];
	for (int j = 0; j < n; j++) messages[j] = M[j].getMessage(timestamp); 
	return messages;
    }

    public void showLastMessages(int x) {
	int size = _V.size();
	if( size < 1 ) return;
	if( x > size ) x = size;
	String messages = "";
	_NmsgDisp = 0;
	for (int j = size-x; j < size; j++) {
	    UFMessage M = (UFMessage)_V.elementAt(j);
	    if( showDups || !M.duplicate || j==size-1 ) {
		messages += M.getMessage(timestamp);
		++_NmsgDisp;
	    }
	}
	this.setText(messages);
	this.setToolTipText();
    }

    public void appendLastMessages(int x) {
	int size = _V.size();
	if( size < 1 ) return;
	if( x > size ) x = size;
	for (int j = size-x; j < size; j++) {
	    UFMessage M = (UFMessage)_V.elementAt(j);
	    if( showDups || !M.duplicate || j==size-1 ) {
		this.append( M.getMessage(timestamp) );
	    }
	}
    }

    public String getFirstMessage() {
	if( _V.size() < 1 ) return null;
	UFMessage M = (UFMessage)_V.firstElement();
	return M.getMessage(timestamp);
    }

    public void showFirstMessage() {
	if( _V.size() < 1 ) return;
	UFMessage M = (UFMessage)_V.firstElement();
	_NmsgDisp = 1;
	this.setText( M.getMessage(timestamp) );
	this.setToolTipText();
    }

    public void appendFirstMessage() {
	if( _V.size() < 1 ) return;
	UFMessage M = (UFMessage)_V.firstElement();
	this.append( M.getMessage(timestamp) );
	this.setToolTipText();
    }

    public String[] getFirstMessages(int x) {
	int size = _V.size();
	if( size < 1 ) return null;
	if( x > size ) x = size;
	UFMessage[] M = new UFMessage[x];
	UFMessage temp;
	int n = 0;
	for (int j = 0; j < x; j++) {
	    temp = (UFMessage)_V.elementAt(j);
	    if (showDups || !temp.duplicate) {
		M[n] = temp; 
		n++;
	    }
	}
	String[] messages = new String[n];
	for (int j = 0; j < n; j++) messages[j] = M[j].getMessage(timestamp); 
	return messages;
    }

    public void showFirstMessages(int x) {
	int size = _V.size();
	if( size < 1 ) return;
	if( x > size ) x = size;
	String messages = "";
	_NmsgDisp = 0;
	for (int j = 0; j < x; j++) {
	    UFMessage M = (UFMessage)_V.elementAt(j);
	    if (showDups || !M.duplicate) {
		messages += M.getMessage(timestamp) ;
		++_NmsgDisp;
	    }
	}
	this.setText(messages);
	this.setToolTipText();
    }

    public void appendFirstMessages(int x) {
	int size = _V.size();
	if( size < 1 ) return;
	if( x > size ) x = size;

	for (int j = 0; j < x; j++) {
	    UFMessage M = (UFMessage)_V.elementAt(j);
	    if( showDups || !M.duplicate )
		this.append( M.getMessage(timestamp) );
	}

	this.setToolTipText();
    }

   public String getMessageAt(int x) {
      UFMessage M = (UFMessage)_V.elementAt(x);
      return M.getMessage(timestamp);
   }

   public void showMessageAt(int x) {
      UFMessage M = (UFMessage)_V.elementAt(x);
      _NmsgDisp = 1;
      this.setText( M.getMessage(timestamp) );
      this.setToolTipText();
   }

   public void appendMessageAt(int x) {
      UFMessage M = (UFMessage)_V.elementAt(x);
      this.append( M.getMessage(timestamp) );
      this.setToolTipText();
   }

   public String[] getMessagesBetween(int x, int y) {
      if (x > y) {
	int temp = x;
	x = y;
	y = temp;
      }
      UFMessage[] M = new UFMessage[y-x];
      UFMessage temp;
      int n = 0;
      for (int j = x; j <= y; j++) {
	  if (j >= 0 && j < _V.size()) {
	      temp=(UFMessage)_V.elementAt(j);
	      if (showDups || !temp.duplicate) M[n++] = temp;
	  }
      }
      String[] messages = new String[n];
      for (int j = 0; j < n; j++) messages[j] = M[j].getMessage(timestamp); 
      return messages;
   }

   public void showMessagesBetween(int x, int y) {
      if (x > y) {
        int temp = x;
        x = y;
        y = temp;
      }
      String messages = "";
      _NmsgDisp = 0;
      for (int j = x; j <= y; j++) {
        if (j >= 0 && j < _V.size()) {
	   UFMessage M = (UFMessage)_V.elementAt(j);
           if (showDups || !M.duplicate) {
              messages += M.getMessage(timestamp) ;
	      ++_NmsgDisp;
           }
        }
      }
      this.setText(messages);
      this.setToolTipText();
   }

   public void appendMessagesBetween(int x, int y) {
      if (x > y) {
        int temp = x;
        x = y;
        y = temp;
      }

      for (int j = x; j <= y; j++) {
        if (j >= 0 && j < _V.size()) {
	   UFMessage M = (UFMessage)_V.elementAt(j);
	   if( showDups || !M.duplicate )
	       this.append( M.getMessage(timestamp) );
        }
      }

      this.setToolTipText();
   }

   public void removeLastMessages(int x) {
      int size = _V.size();
      for (int j = size-x; j < size; j++) {
	if (j >=0 && j < _V.size()) _V.removeElementAt(j);
      }
   }

   public void removeFirstMessages(int x) {
      for (int j = 0; j < x; j++) {
	if (j >=0 && j < _V.size()) _V.removeElementAt(j);
      }
   }

   public void removeMessagesBetween(int x, int y) {
      if (x > y) {
        int temp = x;
        x = y;
        y = temp;
      }
      for (int j = x; j <= y; j++) {
        if (j >=0 && j < _V.size()) _V.removeElementAt(j);
      }
   }

   public static void main(String[] args) {
      UFMessageLog log = new UFMessageLog();
      JFrame f = new JFrame();
      JPanel p = new JPanel();
      p.setPreferredSize(new Dimension(450,300));
      p.add(log);
      f.getContentPane().add(p);
      f.pack();
      f.setVisible(true);
      log.addMessage("This is a test.");
      log.addMessage("Test 2");
      log.addMessage("Test 2");
      log.addMessage("Test 3");
      log.showMessageAt(1);
   }
}
//----------------------------------------------------------------------------------------

class UFMessage {
   public String message, timestamp;
   public boolean duplicate;

   public UFMessage(String message, String timestamp, boolean duplicate) {
      this.message = message;
      this.timestamp = timestamp;
      this.duplicate = duplicate;
   }

   public UFMessage(String message, String timestamp) {
      this(message, timestamp, false);
   }

   public UFMessage(String message) {
      this(message, new Time(System.currentTimeMillis()).toString(), false);
   }

   public UFMessage(String message, boolean duplicate) {
      this(message, new Time(System.currentTimeMillis()).toString(), duplicate);
   }

    public String getMessage(boolean withTimeStamp)
    {
	if( withTimeStamp )
	    return new String(timestamp + "\t" + message + "\n");
	else
	    return new String(message + "\n");
    }
}

package javaUFLib;

//Title:        UFTextField extension of JTextField with history log of messages (errors, status, etc.).
//Version:      (see rcsID)
//Copyright:    Copyright (c) 2003-5
//Authors:      Frank Varosi and Craig Warner
//Company:      University of Florida
//Description:  Keeps track of state with color, uses UFMessageLog class for history of messages.

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.util.Vector;

//===============================================================================
/**
 * Extends the JTextField class to add some simple useful options,
 * like changing color depending on previous/current state.
 */

public class UFTextField extends JTextField implements MouseListener, FocusListener, KeyListener
{
    public static final
	String rcsID = "$Name:  $ $Id: UFTextField.java,v 1.36 2006/01/13 23:31:12 drashkin Exp $";

    protected String newText = "", prevText = "";
    protected String Name, Label="", Units="";
    protected Vector _alarmItems = new Vector();

    protected Color _greenWhite = new Color(245,249,245);
    protected Color _nearWhite = new Color(245,245,245);
    protected Color _normalColor = _greenWhite;

    protected boolean commitOnEnter = false;
    protected boolean showInToolTip = false;

    protected JFrame _LogFrame;
    private int _Hsize, _Vsize;
    public JPopupMenu popupMenu;
    protected UFMessageLog _Log;

//-------------------------------------------------------------------------------
    /**
     *Default Constructor
     */
    public UFTextField() {
	try {
	    createComponent("TextField", true);
	}
	catch(Exception ex) {
	    System.out.println("Error creating a UFTextField: " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param description String: Information regarding the field
     */
    public UFTextField(String description) {
	try {
	    createComponent( description, true );
	}
	catch(Exception ex) {
	    System.out.println("Error creating UFTextField: " + description + " : " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param description String: Information regarding the field
     *@param text        String: text to set in field
     */
    public UFTextField( String description, String text ) {
	try {
	    newText = text;
	    prevText = text;
	    createComponent( description, true );
	    this.setText( text );
	    _Log.addMessage(text);
	    _Log.appendLastMessage();
	}
	catch(Exception ex) {
	    System.out.println("Error creating UFTextField: " + description + " : " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor: special case with specified # of columns and no focuslistener.
     *@param description String: Information regarding the field
     *@param text        String: text to set in field
     *@param Ncolumns    int: # of columns to set for field
     */
    public UFTextField( String description, String text, int Ncolumns ) {
	try {
	    if( Ncolumns > 0 ) this.setColumns( Ncolumns );
	    newText = text;
	    prevText = text;
	    createComponent( description, true );
	    this.setText( text );
	    _Log.addMessage(text);
	    _Log.appendLastMessage();
	    removeFocusListener(this);
	}
	catch(Exception ex) {
	    System.out.println("Error creating UFTextField: " + description + " : " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param description String: Information regarding the field
     *@param editable    boolean: state of editing
     */
    public UFTextField( String description, boolean editable ) {
	try {
	    createComponent( description, editable );
	}
	catch(Exception ex) {
	    System.out.println("Error creating UFTextField: " + description + " : " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param description String: Information regarding the field
     *@param editable    boolean: state of editing
     *@param Ncolumns    int: # of columns to set for field
     */
    public UFTextField( String description, boolean editable, int Ncolumns ) {
	try {
	    if( Ncolumns > 0 ) this.setColumns( Ncolumns );
	    createComponent( description, editable );
	}
	catch(Exception ex) {
	    System.out.println("Error creating UFTextField: " + description + " : " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     *Component initialization
     *@param description String: Information regarding the field
     */
    private void createComponent( String description, boolean editable ) throws Exception
    {
	if( description.indexOf("(") > 0 ) {
	    int pos = description.indexOf("(");
	    this.Name = description.substring( 0, pos ).trim();
	    this.Units = description.substring( pos+1, description.indexOf(")") ).trim();
	}
	else if( description.indexOf(":") > 0 )
	    this.Name = description.substring( 0, description.indexOf(":") ).trim();
	else
	    this.Name = description.trim();
	
	this.Label = description;
	this.addFocusListener(this);
	this.addMouseListener(this);
	this.setEditable( editable );

	if( editable ) {
	    this.setBorder(new BevelBorder( BevelBorder.LOWERED ));
	    this.addKeyListener(this);
	    _normalColor = _greenWhite;
	}
	else _normalColor = _nearWhite;

	this.setBackground( _normalColor );
	this.setToolTipText();

	this._Log = new UFMessageLog(1000);
        this.popupMenu = new JPopupMenu();
        JMenuItem viewLogSmall = new JMenuItem("View Log: small");
        JMenuItem viewLogLarge = new JMenuItem("View Log: bigger");
        JMenuItem viewLogHuge = new JMenuItem("View Log: full size");
        this.popupMenu.add(viewLogSmall);
        this.popupMenu.add(viewLogLarge);
        this.popupMenu.add(viewLogHuge);

        viewLogSmall.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ev) { _Log.viewFrame( Label, 600, 200 ); } });

        viewLogLarge.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ev) { _Log.viewFrame( Label, 650, 500 ); } });

        viewLogHuge.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ev) { _Log.viewFrame( Label, 700, 900 ); } });
    }
//-------------------------------------------------------------------------------

    public void mousePressed(MouseEvent mevt) {
	if(( mevt.getModifiers() & InputEvent.BUTTON3_MASK) != 0 ) {
	    if( mevt.isPopupTrigger() ) {
		popupMenu.show( mevt.getComponent(), mevt.getX(), mevt.getY() );
	    }
	}
    }

    public void mouseClicked(MouseEvent mevt) {}
    public void mouseReleased(MouseEvent mevt) {}
    public void mouseEntered(MouseEvent mevt) {}
    public void mouseExited(MouseEvent mevt) {}

//-------------------------------------------------------------------------------
    /**
     *UFTextField#setAlarmValue
     *@param alarmVal     String: text value for which orange background color should be indicated.
     */
    public void setAlarmValue( String alarmVal ) { if( alarmVal != null ) _alarmItems.add( alarmVal ); }

//-------------------------------------------------------------------------------
    /**
     *UFTextField#setCommitOnEnter
     */
    public void setCommitOnEnter(boolean commit) { commitOnEnter = commit; }
    public void setCommitOnEnter() { commitOnEnter = true; }

//-------------------------------------------------------------------------------
    /**
     * Sets the text for the tool tip -- returns no arguments
     */
    protected void setToolTipText() {
	if( showInToolTip ) 
	    this.setToolTipText( Name + " = " + newText + " " + Units );
	else
	    this.setToolTipText("");
    }

    public void showInToolTip(boolean show) {
	this.showInToolTip = show;
	this.setToolTipText();
    }

    public void setToolTipText(String text) {
	super.setToolTipText( text + " -- Click right button to view Log." );
    }

//-------------------------------------------------------------------------------

    public String name() { return this.Name; }
    public String nameUp() { return Name.toUpperCase(); }
    public String nameLow() { return Name.toLowerCase(); }
    public String Label() { return Label; }
    public String getNewText() { return newText; }
    public String getPrevText() { return prevText; }

//-------------------------------------------------------------------------------

    public void setNewState()
    {
	boolean alarmSet = false;

	for( int i=0; i < _alarmItems.size(); i++ ) {
	    if( newText.equalsIgnoreCase( (String)_alarmItems.elementAt(i) ) ) {
		alarmSet = true;
		setBackground( Color.orange );
	    }
	}

	prevText = newText;
	setToolTipText();

	if( newText.indexOf("ERR") >= 0 ||
	    newText.indexOf("FAIL") >= 0 || newText.indexOf("Fail") >= 0 )
	    {
		Toolkit.getDefaultToolkit().beep();
		if( !alarmSet ) setBackground( Color.yellow );
	    }
	else if( !alarmSet ) {
	    if( newText.indexOf("WARN") >= 0 )
		setBackground( Color.yellow );
	    else
		setBackground( _normalColor );
	}
    }
//-------------------------------------------------------------------------------

    public void setNewText( String newText )
    {
	this.newText = newText.trim();
	setText( this.newText );
	_Log.addMessage( this.newText );
	_Log.appendLastMessage();

	if( this.newText.equals( this.prevText ) )
	    setBackground( _normalColor );
	else
	    setBackground( Color.yellow );
    }
//-------------------------------------------------------------------------------

    public void setNewState( String newText )
    {
	this.newText = newText.trim();
	setText( this.newText );
	setNewState();
	_Log.addMessage( newText );
	_Log.appendLastMessage();
    }

    public void setNewState( int newVal ) { setNewState( Integer.toString( newVal ) ); }
    public void setNewState( float newVal ) { setNewState( UFLabel.truncFormat( newVal ) ); }
    public void setNewState( double newVal ) { setNewState( UFLabel.truncFormat( newVal ) ); }

//-------------------------------------------------------------------------------
    /**
     * Event Handling Methods
     */
    public void focusGained(FocusEvent e) 
    { 
	if( prevText.trim().equals("") ) prevText = getText().trim();
    }
//-------------------------------------------------------------------------------

    public void focusLost(FocusEvent e) 
    { 
	newText = getText().trim();

	if( commitOnEnter ) {
	    prevText = newText;
	    setToolTipText();
	}

	if( newText.equals( prevText ) )
	    setBackground( _normalColor );
	else
	    setBackground( Color.yellow );
    }

//-------------------------------------------------------------------------------

    public void keyTyped(KeyEvent ke) { }
    public void keyReleased(KeyEvent ke) { }

//-------------------------------------------------------------------------------

    public void keyPressed(KeyEvent ke)
    {
	setBackground( Color.yellow );

	if( ke.getKeyChar() == '\n' )    // we got the enter key
	    {
		newText = getText().trim();
		_Log.addMessage( newText );
		_Log.appendLastMessage();

		if( commitOnEnter ) {
		    prevText = newText;
		    setToolTipText();
		}

		if( newText.equals( prevText ) )
		    setBackground( _normalColor );
	    }
	else if( ke.getKeyChar() == 27 )  // we got the escape key
	    {
		setText( prevText );
		setBackground( _normalColor );
	    }
    }
} //end of class UFTextField

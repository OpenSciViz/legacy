package javaUFLib;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;

//===============================================================================
  /**
   *Handles the Graph  
   */
public  class UFGraphPanel extends JPanel {
    static final long serialVersionUID = 0;
    
    // array of linked lists to store data points for the sensors.
    LinkedList [] xValues;
    LinkedList [] yValues;
    boolean [] showme;
    Color [] colors;
    String [] sensorNames;
    //number of points currently stored.
    int size;
    long currTime;
    //maximum number of points
    int panelWidth, panelHeight, panelMinX, panelMinY;
    double timeMin, timeMax, tempMin, tempMax;
    double xTickDelta, xLabelDelta, yTickDelta, yLabelDelta;
    int numSensors;
    boolean drawGrid;
    boolean autoscale;
    int startIdx;
    GraphThread graphThread;
    String logFileName;
    Object mutex;
//-------------------------------------------------------------------------------
    /**
     *Constructor Function for JGraphPanel
     */
    
    public UFGraphPanel (int numberOfSensors, int startIndexOfSensorsInLogFile) {
	mutex = new Object();
	numSensors = numberOfSensors;
	startIdx = startIndexOfSensorsInLogFile;
	drawGrid = true;
	autoscale = true;
	logFileName = "/nfs/irflam2a/share/data/environment/current";
	xValues = new LinkedList[numSensors];
	yValues = new LinkedList[numSensors];
	colors = new Color[numSensors];
	showme = new boolean[numSensors];
	size = 0; 
	xTickDelta = 360; yTickDelta = 10;
	xLabelDelta = 720; yLabelDelta = 20;
	panelMinX = 10; panelMinY = 10;
	tempMin = 0; tempMax = 100;
	timeMin = 0; timeMax = 3600;
	for (int i=0; i<numSensors; i++) {
	    xValues[i] = new LinkedList();
	    yValues[i] = new LinkedList();
	    colors[i] = new Color(0,255*i/numSensors,255*(1 - i/numSensors)); 
	    showme[i] = true;
	}	
	sensorNames = new String[numSensors];
	for (int i=0; i<numberOfSensors; i++)
	    sensorNames[i] = "Sensor "+i;
	
    }

    public UFGraphPanel (String[] namesOfSensors, int startIndexOfSensorsInLogFile) {
	this(namesOfSensors.length, startIndexOfSensorsInLogFile);
	sensorNames = new String[namesOfSensors.length];
	for (int i=0; i<sensorNames.length; i++)
	    sensorNames[i] = new String(namesOfSensors[i]);
	// see if colors are stored in config file
	try {
	    String home = UFExecCommand.getEnvVar("HOME");
       	    File f = new File(home+"/.ufflam2helper");
	    if (f.exists()) {
		BufferedReader br = new BufferedReader(new FileReader(f));
		while (br.ready()) {
		    String s = br.readLine();
		    if (s != null && !s.trim().equals("")) {
			StringTokenizer st = new StringTokenizer(s);
			if (st.countTokens() == 4) {
			    String sname = st.nextToken();
			    int rVal = Integer.parseInt(st.nextToken().trim());
			    int gVal = Integer.parseInt(st.nextToken().trim());
			    int bVal = Integer.parseInt(st.nextToken().trim());
			    for (int i=0; i<sensorNames.length; i++)
				if (sname.trim().equals(sensorNames[i])) {
				    colors[i] = new Color(rVal,gVal,bVal);
				}
			}
		    }
		}
	    }
	} catch (Exception e) {
	    System.err.println("UFGraphPanel> "+e.toString());
	}
    }

    //mutator methods

    public void setLogFileName(String newLogFileName) {
	logFileName = new String(newLogFileName);	
    }

    public void setTimeMin(double newTimeMin) {
	timeMin = newTimeMin;
    }
    public void setTimeMax(double newTimeMax) {
	timeMax = newTimeMax;
    }
    public void setTempMin(double newTempMin) {
	tempMin = newTempMin;
    }
    public void setTempMax(double newTempMax) {
	tempMax = newTempMax;
    }
    public void setXTickDelta(double newXTickDelta) {
	xTickDelta = newXTickDelta;
    }
    public void setXLabelDelta(double newXLabelDelta) {
	xLabelDelta = newXLabelDelta;
    }
    public void setYTickDelta(double newYTickDelta) {
	yTickDelta = newYTickDelta;
    }
    public void setYLabelDelta(double newYLabelDelta) {
	yLabelDelta = newYLabelDelta;
    }

    //accessor methods
    public String [] getSensorNames() { return sensorNames;}
    public Color[] getColors() { return colors;}


//-------------------------------------------------------------------------------
/**
 *Sets whether to draw grid on graph or not
 */

    public void setDrawGrid(boolean drawGrid) {
	this.drawGrid = drawGrid;
    }
//-------------------------------------------------------------------------------
/**
 *Sets whether to autoscale an empty graph
 */

    public void setAutoscale(boolean autoscale) {
	this.autoscale = autoscale;
    }

//-------------------------------------------------------------------------------
      /**
       *Clears the xValues array and the yValues array for the graph data
       */
      public void eraseGraphData () {
         size = 0;
         for (int i=0; i<numSensors; i++) {
           xValues[i].clear();
           yValues[i].clear();
         }
      } //end of eraseGraphData

//-------------------------------------------------------------------------------
      /**
       *reverses ??
       *@param arrayit TBD
       */
      public int[] reverse (int [] arrayit) {
        int [] temper = new int[arrayit.length];
        int k = arrayit.length-1;
        for (int i=0; i<arrayit.length; i++) {
            temper[k--] = arrayit[i];
        }
        return temper;
      } //end of reverse

//-------------------------------------------------------------------------------
      /**
       *TBD
       *@param x double x coordinate
       *@param y double y coordinate
       */
      public Point xform(double x, double y) {
         final double scr_org_x = panelWidth;
         final double scr_org_y = panelHeight;
         final double scr_x_per_x = -( panelWidth-panelMinX) / (timeMax-timeMin);
         final double scr_y_per_y = -( panelHeight-panelMinY)/ (tempMax-tempMin);
         y -= tempMin; x -= timeMin;
         Point scr_loc = new Point();
         scr_loc.x = Math.max((int)(scr_org_x + x * scr_x_per_x), panelMinX);
         scr_loc.y = Math.max((int)(scr_org_y + y * scr_y_per_y), panelMinY);
         scr_loc.y = Math.min( scr_loc.y, panelHeight );
         return scr_loc;
      } //end of xform
//-------------------------------------------------------------------------------
      /**
       *TBD
       *@param x double x coordinate
       *@param y double y coordinate
       */
      public Point xform3(double x, double y) {
         final double scr_org_x = panelWidth;
         final double scr_org_y = panelHeight;
         final double scr_x_per_x = -( panelWidth-panelMinX) / (timeMax-timeMin);
         final double scr_y_per_y = -( panelHeight-panelMinY)/ (tempMax-tempMin);
         y -= tempMin; x -= timeMin;
         Point scr_loc = new Point();
         scr_loc.x = Math.max((int)(scr_org_x + x * scr_x_per_x), panelMinX);
         scr_loc.y = Math.max((int)(scr_org_y + y * scr_y_per_y), panelMinY);
         scr_loc.y = Math.min( scr_loc.y, panelHeight );
         return scr_loc;
      } //end of xform

//-------------------------------------------------------------------------------
      /**
       *TBD
       *@param x double x coordinate
       *@param y double y coordinate
       */
      public Point xform2(double x, double y)
      {
         final double scr_org_x = 0;
         final double scr_org_y = panelHeight;
         final double scr_x_per_x = (panelWidth) / (timeMax-timeMin);
         final double scr_y_per_y = -( panelHeight-panelMinY) / (tempMax-tempMin);
         y -= tempMin;// x += timeMin;
         Point scr_loc = new Point();
         scr_loc.x = (int)(scr_org_x + x * scr_x_per_x);
         scr_loc.y = Math.max((int)(scr_org_y + y * scr_y_per_y), panelMinY);
         scr_loc.y = Math.min( scr_loc.y, panelHeight );
         return scr_loc;
      } //end of xform2

//-------------------------------------------------------------------------------
      /**
       *TBD
       *@param g Graphics TBD
       */
      public synchronized void paint(Graphics g) {
        panelWidth = (int)this.getWidth() - 38;
        panelHeight = (int)this.getHeight() - 18;
        g.setFont(new Font(g.getFont().getName(),g.getFont().getStyle(),10));
        g.drawLine( panelMinX, panelHeight, panelWidth, panelHeight );
        g.drawLine( panelWidth, panelMinY, panelWidth, panelHeight );

        double d = timeMin;
        while (d <= timeMax) {
	    Point p1 = xform(d,1); Point p2 = xform(d,-1);
	    if( drawGrid ) {
		g.setColor( Color.gray );
		g.drawLine( p1.x, panelHeight, p2.x, 0 );
	    }
	    g.setColor( Color.black );
	    g.drawLine( p1.x, panelHeight+1, p2.x, panelHeight-1 );
	    d+=xTickDelta;
        }

        d = timeMin;
        while (d <=timeMax) {
          Point p1 = xform(d,-15);
	  g.drawString( String.valueOf(-d), p1.x-9, panelHeight+15 );
          d+=xLabelDelta;
        }

        double y = tempMin;
        while (y <= tempMax) {
	    if( drawGrid ) {
		g.setColor( Color.gray );
		g.drawLine( panelMinX, xform2(panelMinX+1,y).y, panelWidth, xform2(panelMinX-1,y).y );
	    }
	    g.setColor( Color.black );
	    g.drawLine( panelWidth-1, xform2(0,y).y, panelWidth+1, xform2(0,y).y );
	    y+=yTickDelta;
        }

        y = tempMin;
        while (y <= tempMax) {
          g.drawString( String.valueOf(y), panelWidth+6, xform2(0,y).y+3 );
          y+=yLabelDelta;
        }

	size = xValues[0].size();
        int []tempx = new int[size];
        int []tempy = new int[size];
	Point srcLoc;
	Point avgTrash = new Point();
	// Compute screen Locations and use polylines to draw the graphs.

	synchronized (mutex) {
	    for (int i=0; i<numSensors; i++) {
		if (showme[i]) {
		    for (int j=0; j<size; j++){
			srcLoc = xform3( ( (currTime - ((Long)xValues[i].get(j)).longValue()))/1000,
					 ((Integer)yValues[i].get(j)).intValue()/100.0 );
			tempx[j] = srcLoc.x;
			tempy[j] = srcLoc.y;
			if (autoscale && size == 1) {
			    avgTrash.y += ((Integer)yValues[i].get(j)).intValue()/100.0;
			    avgTrash.x++;
			}
		    }
		    g.setColor( colors[i] );
		    g.drawPolyline(tempx,tempy,size);
		}
	    }
	}
	if (autoscale && size == 1 && avgTrash.x!=0) {
	    tempMin = avgTrash.y/avgTrash.x - 50;
	    tempMax = tempMin + 100;
	}
	g.setColor( Color.black );
      } //end of paint

    public void startThread() {
	if (isThreadAlive())
	    stopThread();
	graphThread = new GraphThread();
	graphThread.start();
    }

    public boolean isThreadAlive() {
	if (graphThread == null) return false;
	return graphThread.isAlive();
    }

    public void stopThread() {
	
	graphThread.keepRunning = false;
	while (graphThread.stillRunning);//wait for graphThread to exit
    }

    public class GraphThread extends Thread {
	boolean keepRunning = true;
	boolean stillRunning = false;
	//-------------------------------------------------------------------------------
	/**
	 *
	 */
      
	public void run() {
	    RandomAccessFile raf =null;
	    long beginTimestamp = 0;
	    keepRunning = true; stillRunning = true;
	    while (keepRunning) {
		try {
		    if (raf == null) {
			raf = new RandomAccessFile(logFileName,"r");
			StringTokenizer st = new StringTokenizer(raf.readLine());
			st.nextToken();st.nextToken();
			beginTimestamp = Long.parseLong(st.nextToken());
			long timeToStart = (System.currentTimeMillis()/1000 - beginTimestamp);
			raf.readLine(); // skip over header line
			int secOffset = 0;
			do {
			    String s = raf.readLine();
			    if (s ==null || s.trim().equals("")) break;
			    st = new StringTokenizer(s,",");
			    st.nextToken(); // skip over date
			    secOffset = Integer.parseInt(st.nextToken().trim());
			    //System.out.println(secOffset+" "+timeToStart+" "+jGraphPanel.timeMax);
			} while (secOffset < (timeToStart - timeMax)); 
		    }
		    stillRunning = true;
		    String s = raf.readLine();
		    if (s == null || s.trim().equals("")) { Thread.sleep(1000); continue; }
		    StringTokenizer st = new StringTokenizer(s,",");
		    st.nextToken();
		    int secOffset = Integer.parseInt(st.nextToken().trim());
		    long _currTime = beginTimestamp + (secOffset*1000);
		    currTime = _currTime;
		    //skip to the start index specified in the constructor
		    for (int i=0; i<startIdx; i++) st.nextToken();
		    synchronized (mutex) {
			for (int i=0; i<numSensors; i++) {
			    while (xValues[i].size() > 0 &&
				   (currTime-((Long)xValues[i].getLast()).longValue())/1000 > timeMax) {
				xValues[i].removeLast();
				yValues[i].removeLast();
			    }
			    // Add Integers since linkedlists can only hold objects, not primitives.
			    // Scale by 100 in order to preserve precision
			    xValues[i].addFirst( new Long( currTime ) );
			    yValues[i].addFirst( new Integer((int)(Double.parseDouble(st.nextToken().trim())*100)) );
			}
		    }
		    JRootPane jrp = getRootPane();
		    if (jrp != null) jrp.repaint();
		    //System.out.println("Repainted");
		} catch (Exception e) {
		    System.err.println("UFGraphPanel.GraphThread.run> "+e.toString());
		    keepRunning = stillRunning = false;
		}
	    }
	    stillRunning = false;
	}
    } //end of class GraphThread


    public JDialog getLegendDialog() {
	final JDialog retVal = new JDialog();
	retVal.setModal(false);
	retVal.setAlwaysOnTop(true);
	retVal.setSize(200,40*numSensors);
	retVal.setLayout(new GridLayout(0,1));	
	for (int i=0; i<numSensors; i++) {
	    final int myI = i;
	    final JCheckBox showMeBox = new JCheckBox(sensorNames[i],showme[i]);
	    showMeBox.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent ae) {
			showme[myI] = showMeBox.isSelected();
			getParent().repaint();
		    }
		});
	    final JButton colorButton = new JButton();
	    colorButton.setBackground(colors[i]);	    
	    colorButton.addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent ae) {
			Color c = JColorChooser.showDialog(retVal,"Choose Color",colors[myI]);
			if (c != null) colors[myI] = c;
			colorButton.setBackground(colors[myI]);
			getParent().repaint();
		    }
		});
	    JPanel pan = new JPanel();
	    pan.setLayout(new RatioLayout());
	    pan.add("0.01,0.01;0.80,0.99",showMeBox);
	    pan.add("0.81,0.01;0.19,0.99",colorButton);
	    retVal.add(pan);
	}
	return retVal;
    }


    public JDialog getParameterDialog() {
	JPanel labelpanel = new JPanel();
	JPanel textpanel = new JPanel();
	JPanel buttonpanel = new JPanel();
	JButton buttonOK = new JButton ("OK");
	JButton buttonCancel = new JButton ("Cancel");
	final JTextField [] graphParams = new JTextField[8];
	final JDialog retVal = new JDialog();
	retVal.setModal(true);
	retVal.setAlwaysOnTop(true);
	buttonOK.addActionListener(new java.awt.event.ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    timeMin = Double.parseDouble(graphParams[0].getText());
		    timeMax = Double.parseDouble(graphParams[1].getText());
		    tempMin = Double.parseDouble(graphParams[2].getText());
		    tempMax = Double.parseDouble(graphParams[3].getText());
		    xTickDelta = Double.parseDouble(graphParams[4].getText());
		    xLabelDelta = Double.parseDouble(graphParams[5].getText());
		    yTickDelta = Double.parseDouble(graphParams[6].getText());
		    yLabelDelta = Double.parseDouble(graphParams[7].getText());
		    retVal.dispose();
		}
	    });
	buttonCancel.addActionListener(new java.awt.event.ActionListener() {
		public void actionPerformed(ActionEvent e) {
		    retVal.dispose();
		}
	    });
	labelpanel.setLayout(new GridLayout(0,1,0,10));
	textpanel.setLayout(new GridLayout(0,1,0,10));
	buttonpanel.setLayout(new FlowLayout());
	retVal.getContentPane().setLayout (new BorderLayout());
	labelpanel.add(new JLabel());
	labelpanel.add(new JLabel("Min Time"));
	labelpanel.add(new JLabel("Max Time"));
	labelpanel.add(new JLabel("Display Min Temp"));
	labelpanel.add(new JLabel("Display Max Temp"));
	labelpanel.add(new JLabel("x Tick Delta"));
	labelpanel.add(new JLabel("x Label Delta"));
	labelpanel.add(new JLabel("y Tick Delta"));
	labelpanel.add(new JLabel("y Label Delta"));
	textpanel.add(new JLabel());
	for (int i=0; i<8; i++) {
	    graphParams[i] = new JTextField("0.0",5);
	    textpanel.add(graphParams[i]);
	}
	graphParams[0].setText(timeMin + "");
	graphParams[1].setText(timeMax + "");
	graphParams[2].setText(tempMin + "");
	graphParams[3].setText(tempMax + "");
	graphParams[4].setText(xTickDelta + "");
	graphParams[5].setText(xLabelDelta + "");
	graphParams[6].setText(yTickDelta + "");
	graphParams[7].setText(yLabelDelta + "");
	buttonpanel.add(buttonOK);
	buttonpanel.add(buttonCancel);
	retVal.getContentPane().add(labelpanel,BorderLayout.WEST);
	retVal.getContentPane().add(textpanel,BorderLayout.EAST);
	retVal.getContentPane().add(buttonpanel,BorderLayout.SOUTH);
	retVal.setTitle("Graph Parameters");
	buttonOK.grabFocus();
	retVal.setSize(new Dimension(200,350));
	return retVal;
    }
    
    public static void main (String [] args) {
	JFrame jf = new JFrame();
	jf.setDefaultCloseOperation(3);
	jf.setSize(600,600);
	jf.setContentPane(new UFGraphPanel(12,0));
	jf.setVisible(true);
    }

} //end of class JGraphPanel


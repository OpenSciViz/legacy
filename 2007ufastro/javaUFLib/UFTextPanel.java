package javaUFLib;

//Title:        UFTextPanel for Java Control Interface (JCI)
//Version:      (see rcsID)
//Copyright:    Copyright (c) 2003
//Author:       Frank Varosi
//Company:      University of Florida
//Description:  Creates 2 text fields with Label on left (default) or right, all in one Panel.

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

//===============================================================================
/**
 * Creates 2 text fields with Label on left (default) or right, all in one Panel,
 * together handling desired and active values, and changing color depending on state (diff or eq).
 * Also has options for param name to be a JCheckBox or JButton.
 */

public class UFTextPanel extends JPanel implements KeyListener
{
    public static final
	String rcsID = "$Name:  $ $Id: UFTextPanel.java,v 1.21 2005/11/29 20:20:29 drashkin Exp $";

    protected String Name, Units="";
    protected boolean _edit = false;
    protected boolean nameOnRight = false;
    protected boolean nameIsButton = false;
    protected boolean NOactiveField = false;
    protected boolean _checkBox = false; //if true then Label will be a JCheckBox for enable/disable.
    protected boolean _checkBoxState = false; //initially the JCheckBox will NOT be checked.
    protected double _fracField = 0;
    protected Color _normalColor = Color.white;

    protected JCheckBox paramEnable;
    protected JLabel paramName;
    protected JButton paramButton;

    protected String _desiredVal = "";
    protected String _activeVal = "";

    protected JTextField _desiredField = new JTextField();
    protected JTextField _activeField = new JTextField();

//-------------------------------------------------------------------------------
    /**
     *Default Constructor
     */
    public UFTextPanel() {
	try  {
	    createPanel("parameter");
	}
	catch(Exception ex) {
	    System.out.println("Error in creating UFTextPanel: " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param description String: Information regarding the field
     */
    public UFTextPanel(String description) {
	try  {
	    createPanel( description );
	}
	catch(Exception ex) {
	    System.out.println("Error in creating UFTextPanel: " + description + " : " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param checkBoxState  boolean: if present a check box is created on left with starting state.
     *@param description    String: Information regarding the field
     */
    public UFTextPanel( boolean checkBoxState, String description ) {
	try  {
	    _checkBox = true;
	    _checkBoxState = checkBoxState;
	    createPanel( description );
	}
	catch(Exception ex) {
	    System.out.println("Error in creating UFTextPanel: " + description + " : " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param checkBoxState  boolean: if present a check box is created on left with starting state.
     *@param description    String: Information regarding the field
     *@param nameRight      boolean: true causes param name to appear on right instead of left.
     */
    public UFTextPanel( boolean checkBoxState, String description, boolean nameRight ) {
	try  {
	    this.nameOnRight = nameRight;
	    _checkBox = true;
	    _checkBoxState = checkBoxState;
	    createPanel( description );
	}
	catch(Exception ex) {
	    System.out.println("Error in creating UFTextPanel: " + description + " : " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param description    String: Information regarding the field
     *@param nameRight      boolean: true causes param name to appear on right instead of left.
     */
    public UFTextPanel( String description, boolean nameRight ) {
	try  {
	    this.nameOnRight = nameRight;
	    createPanel( description );
	}
	catch(Exception ex) {
	    System.out.println("Error in creating UFTextPanel: " + description + " : " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param description    String: Information regarding the field
     *@param nameRight      boolean: true causes param name to appear on right instead of left.
     *@param nameIsButton   boolean: true causes param name to appear as JButton paramButton.
     */
    public UFTextPanel( String description, boolean nameRight, boolean nameIsButton ) {
	try  {
	    this.nameOnRight = nameRight;
	    this.nameIsButton = nameIsButton;
	    createPanel( description );
	}
	catch(Exception ex) {
	    System.out.println("Error in creating UFTextPanel: " + description + " : " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param numFields    integer: if < 2 then NO active field is created.
     *@param description  String: Information regarding the field
     */
    public UFTextPanel( int numFields, String description ) {
	try  {
	    if( numFields < 2 ) this.NOactiveField = true;
	    createPanel( description );
	}
	catch(Exception ex) {
	    System.out.println("Error in creating UFTextPanel: " + description + " : " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param fracField    float:  NO active field, and value specifies fraction of panel desiredField uses.
     *@param description  String: Information regarding the field
     */
    public UFTextPanel( double fracField, String description ) {
	try  {
	    this.NOactiveField = true;
	    this._fracField = fracField;
	    createPanel( description );
	}
	catch(Exception ex) {
	    System.out.println("Error in creating UFTextPanel: " + description + " : " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param fracField    float:  NO active field, and value specifies fraction of panel desiredField uses.
     *@param description  String: Information regarding the field
     *@param editable     boolean.
     */
    public UFTextPanel( double fracField, String description, boolean editable ) {
	try  {
	    this.NOactiveField = true;
	    this._fracField = fracField;
	    createPanel( description );
	    this._desiredField.setEditable( editable );
	}
	catch(Exception ex) {
	    System.out.println("Error in creating UFTextPanel: " + description + " : " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     *Component initialization
     *@param description String: Information regarding the field
     */
    private void createPanel( String description ) throws Exception
    {
	if( description.indexOf("(") > 0 ) {
	    int pos = description.indexOf("(");
	    this.Name = description.substring( 0, pos ).trim();
	    this.Units = description.substring( pos+1, description.indexOf(")") ).trim();
	}
	else if( description.indexOf(":") > 0 )
	    this.Name = description.substring( 0, description.indexOf(":") ).trim();
	else
	    this.Name = description.trim();
	
	if( _checkBox )
	    {
		paramEnable = new JCheckBox( description );
		paramEnable.setSelected(_checkBoxState);
		_desiredField.setEnabled(_checkBoxState);

		paramEnable.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			    if( paramEnable.isSelected() )
				_desiredField.setEnabled(true);
			    else
				_desiredField.setEnabled(false);
			}
		    });
	    }
	else if( nameIsButton ) {
	    paramButton = new JButton( Name );
	    paramButton.setHorizontalTextPosition( SwingConstants.LEFT );
	    paramButton.setBackground( new Color(222,222,222) );
	    _edit = true;
	    _desiredField.setEditable(true);
	    _desiredField.addKeyListener( this );
	}
	else {
	    paramName = new JLabel( description );
	    _desiredField.setEditable(false);
	}

	_desiredField.setBackground( _normalColor );
	_activeField.setBackground( Color.white );
	_activeField.setEditable(false);
	this.setToolTipText();
	setLayout(new RatioLayout());

	if( nameOnRight )
	    {
		if( NOactiveField ) {
		    this.add("0.01,0.01;0.49,0.99", _desiredField );
		    if( _checkBox )
			this.add("0.51,0.01;0.49,0.99", paramEnable);
		    else if( nameIsButton )
			this.add("0.51,0.01;0.49,0.99", paramButton);
		    else
			this.add("0.51,0.01;0.49,0.99", paramName);
		}
		else {
		    this.add("0.01,0.01;0.28,0.99", _desiredField );
		    this.add("0.30,0.01;0.28,0.99", _activeField );
		    if( _checkBox )
			this.add("0.59,0.01;0.41,0.99", paramEnable);
		    else if( nameIsButton )
			this.add("0.59,0.01;0.41,0.99", paramButton);
		    else
			this.add("0.59,0.01;0.41,0.99", paramName);
		}
	    }
	else  //default is name (Label) on Left:
	    {
		if( NOactiveField ) {
		    String paramLoc = "0.01,0.01;0.59,0.99";
		    String fieldLoc = "0.60,0.01;0.39,0.99";
		    if( _fracField > 0 && _fracField < 1 ) {
			paramLoc = "0.01,0.01;" + ( 1-_fracField-0.02 ) +",0.99";
			fieldLoc = ( 1-_fracField-0.01 ) + ",0.01;" + _fracField + ",0.99";
		    }
		    if( _checkBox )
			this.add( paramLoc, paramEnable);
		    else if( nameIsButton )
			this.add( paramLoc, paramButton);
		    else
			this.add( paramLoc, paramName);
		    this.add( fieldLoc, _desiredField );
		}
		else {
		    if( _checkBox )
			this.add("0.01,0.01;0.40,0.99", paramEnable);
		    else if( nameIsButton )
			this.add("0.01,0.01;0.40,0.99", paramButton);
		    else
			this.add("0.01,0.01;0.40,0.99", paramName);
		    this.add("0.41,0.01;0.28,0.99", _desiredField );
		    this.add("0.70,0.01;0.29,0.99", _activeField );
		}
	    }
    }
//-------------------------------------------------------------------------------

    public boolean isSelected()
    {
	if( _checkBox )
	    return paramEnable.isSelected();
	else
	    return true;
    }
//-------------------------------------------------------------------------------

    void setToolTipText() {
	if( NOactiveField )
	    setToolTipText( Name + " = " + _desiredVal + " " + Units );
	else
	    setToolTipText( Name + ": Desired = " + _desiredVal + " " + Units +
			    ",  Active = " + _activeVal + " " + Units );
    }
//-------------------------------------------------------------------------------

    public String getActive() {	return _activeVal; }
    public String getDesired() { return _desiredVal; }

//-------------------------------------------------------------------------------

    public String getActiveField() { return _activeField.getText().trim(); }
    public String getDesiredField() { return _desiredField.getText().trim(); }

//-------------------------------------------------------------------------------

    public void setActive( String newVal )
    {
	_activeVal = newVal.trim();
	setToolTipText();

	if( !_activeField.getText().equals( _activeVal ) ) _activeField.setText( _activeVal );
	if( _desiredVal.equals( _activeVal ) ) _desiredField.setBackground( _normalColor );
    }
//-------------------------------------------------------------------------------

    public void setDesired( String newVal )
    {
	_desiredVal = newVal.trim();
	setToolTipText();

	if( !_desiredField.getText().equals( _desiredVal ) ) _desiredField.setText( _desiredVal );
	if( !_desiredVal.equals( _activeVal ) ) _desiredField.setBackground( Color.yellow );
    }
//-------------------------------------------------------------------------------

    //for cases of NOactiveField = true, just set the text in visible field:

    public void setValue( String newVal )
    {
	_desiredVal = newVal.trim();
	setToolTipText();
	_desiredField.setBackground( _normalColor );
	_desiredField.setText( _desiredVal );
    }

    public void setValue( int newVal ) { setValue( Integer.toString(newVal) ); }
    public void setValue( float newVal ) { setValue( Float.toString(newVal) ); }
    public void setValue( double newVal ) { setValue( Double.toString(newVal) ); }

//-------------------------------------------------------------------------------

    public String name() { return Name; }
    public String nameUp() { return Name.toUpperCase(); }
    public String nameLow() { return Name.toLowerCase(); }

//-------------------------------------------------------------------------------

    public void setDesiredBackground( Color color )
    {
	_normalColor = color;
	this._desiredField.setBackground( _normalColor );
    }

//-------------------------------------------------------------------------------

    public void setDesiredEditable( boolean edit )
    {
	this._desiredField.setEditable( edit );
	_edit = edit;

	if( _edit ) {
	    this._desiredField.addKeyListener( this );
	    _normalColor = Color.white;
	}
	else _normalColor = new Color(240,230,220);

	this._desiredField.setBackground( _normalColor );
    }

//-------------------------------------------------------------------------------
    /**
     * Key Listener Event Handling Methods
     */
    public void keyTyped(KeyEvent ke) { }
    public void keyReleased(KeyEvent ke) { }

    public void keyPressed(KeyEvent ke)
    {
	this._desiredField.setBackground( Color.yellow );

	if( ke.getKeyChar() == '\n' )    // we got the enter key
	    {
		String newText = this._desiredField.getText().trim();

		if( newText.equals( this._desiredVal ) )
		    this._desiredField.setBackground( _normalColor );
		else {
		    this._desiredVal = newText;
		    setToolTipText();
		}
	    }
	else if( ke.getKeyChar() == 27 )  // we got the escape key
	    {
		this._desiredField.setText( this._desiredVal );
		this._desiredField.setBackground( _normalColor );
	    }
    }
} //end of class UFTextPanel


package ufjdd;
/**
 * Title:        LineCutPanel.java
 * Version:      (see rcsID)
 * Authors:      Ziad Saleh, Frank Varosi, Craig Warner
 * Company:      University of Florida
 * Description:  For plotting image data pixel values along a user selected line cut thru image in ZoomPanel.
 */
import javaUFLib.*;
import java.awt.*;
import java.awt.image.*;
import java.awt.geom.*;
import javax.swing.*;

public class LineCutPanel {

    public static final
	String rcsID = "$Name:  $ $Id: LineCutPanel.java,v 1.1 2005/04/29 21:30:10 drashkin Exp $";

    GatorPlot gp;
    boolean first;
    
    public LineCutPanel() {
	gp = new GatorPlot("Line Cut");
	first = true;
    }
    
    public void update(double[][] data, int x1, int y1, int x2, int y2, double scalefactor) {

        int xArange = Math.abs( x2 - x1 );
        int yArange = Math.abs( y2 - y1 ); 
        int range = Math.max( xArange, yArange );

	if( x2 < x1 ) {
	   int temp = x1;
	   x1 = x2;
	   x2 = temp;
	   temp = y1;
	   y1 = y2;
	   y2 = temp;
	}

	int yinc = 1;
	if( y2 < y1 && yArange > xArange ) yinc = -1;

        float slope = (float)( y2 - y1 )/( x2 - x1 );
        float theta = (float)Math.atan( slope );
        
        //System.out.println("(x1,y1) = (" + x1 + "," + y1 + ") : (x2,y2) = ( " + x2 + "," + y2 + ")");
        //System.out.println("slope = " + slope + " theta = " + theta);

	float[] xPlot = new float[range];
	float[] yPlot = new float[range];
        
	if( xArange >= yArange ) {

	   for(int i=0; i < range; i++) {
	      int xin = x1 + i;
              float yd = slope*i;
              int yin = (int)Math.round( yd + y1 );
	      xPlot[i] = (float)Math.sqrt( i*i + yd*yd );
	      yPlot[i] = (float)( data[yin][xin] * scalefactor );
	   }
	}
	else {
	   int yin = y1;
	   int xin = x1;

	   for(int i=0; i < range; i++) {
              yin += yinc;
	      float yd = yin - y1;
	      float xd = 0;
              if( xArange > 0 ) {
		  xd = yd/slope;
		  xin = (int)Math.round( x1 + xd );
	      }
              xPlot[i] = (float)Math.sqrt( xd*xd + yd*yd );;
              yPlot[i] = (float)( data[yin][xin] * scalefactor );
           }
	}

	if (first) {
	   gp.showPlot(false);
	   first = false;
	}

	gp.show();
	gp.setState( Frame.NORMAL );

        gp.plot( xPlot, yPlot,
		 "*xtitle=Distance( pixels ), *ytitle=DATA, *title=Line Cut, *xminval=0, *psym=-4");
    }
}

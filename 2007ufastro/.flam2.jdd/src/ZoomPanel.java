package ufjdd;
/**
 * Title:        ZoomPanel.java
 * Version:      (see rcsID)
 * Copyright:    Copyright (c) 2004
 * Author:       Frank Varosi, Ziad Saleh
 * Company:      University of Florida
 * Description:  For quick-look image display and analysis of Infrared Camera data stream.
 */
import java.awt.*;
import java.awt.image.*;
import javax.swing.*;
import javaUFProtocol.*;

public class ZoomPanel extends JPanel {

    public static final
	String rcsID = "$Name:  $ $Id: ZoomPanel.java,v 1.1 2005/04/29 21:30:10 drashkin Exp $";
    
    public ZoomImage zoomImage;
    protected AdjustZoomPanel adjustZoom;
    protected JLabel pixelValue, aperFlux;

    public JComboBox zoomFacSel;
    public JComboBox smoothIterSel;
    public int xSize, ySize;

    public ZoomPanel( IndexColorModel colorModel, HistogramPanel histoPanel )
    {
	this( 420, colorModel, histoPanel );
    }

    public ZoomPanel( int zoomSize, IndexColorModel colorModel, HistogramPanel histoPanel )
    {
	String zooms[] = {"2","3","4","5","7","10","20","30"}; //all divisors of 420.
        zoomFacSel = new JComboBox(zooms);

	String smoothFacts[] = {"0","1","2","3","4","6","9","12","16","20","25"};
        smoothIterSel = new JComboBox(smoothFacts);
	smoothIterSel.setMaximumRowCount(14);

        pixelValue = new JLabel(" Pixel Data Values");
        pixelValue.setBorder(BorderFactory.createLoweredBevelBorder());
        aperFlux = new JLabel(" Flux in Aperture");
        aperFlux.setBorder(BorderFactory.createLoweredBevelBorder());

	adjustZoom = new AdjustZoomPanel();
	zoomImage = new ZoomImage( zoomSize, smoothIterSel, colorModel,
				   adjustZoom, histoPanel.histoBar, pixelValue, aperFlux );
	adjustZoom.setZoomImage( zoomImage );
        histoPanel.setZoomImage( zoomImage );

	JPanel controlPanel = new JPanel();
	controlPanel.add( new JLabel("ZOOM  Factor = ",JLabel.RIGHT) );
	controlPanel.add( zoomFacSel );
	controlPanel.add( new JLabel("      Smooth  iterations = ",JLabel.RIGHT) );
	controlPanel.add( smoothIterSel );

	ySize = 2;
	controlPanel.setBounds( 2, ySize, zoomSize, 30 );
	ySize += 34;
	zoomImage.setBounds( 2, ySize, zoomSize, zoomSize );
	ySize += (zoomSize+2);
	int zh = zoomSize/2;
	pixelValue.setBounds( 2, ySize, zoomSize, 30 );
	//aperFlux.setBounds( 2 + zh, ySize, zh-1, 30 );
	ySize += 32;
	adjustZoom.setBounds( 2, ySize, zoomSize, 80 );
	ySize += 81;
	xSize = zoomSize + 4;

	this.setLayout(null);
	this.add( controlPanel );
	this.add( zoomImage );
        this.add( pixelValue );
        //this.add( aperFlux );
        this.add( adjustZoom );
	this.setBorder(BorderFactory.createLineBorder(Color.green, 1));
    }
}

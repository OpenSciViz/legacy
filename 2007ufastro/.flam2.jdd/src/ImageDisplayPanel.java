package ufjdd;
/**
 * Title:        Java Data Display (JDD): ImageDisplayPanel
 * Version:      (see rcsID)
 * Copyright:    Copyright (c) 2004
 * Author:       Ziad Saleh & Frank Varosi
 * Company:      University of Florida
 * Description:  For single image display of contents of a frame buffer from Data Acq. Server
 */
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.awt.geom.*;
import javax.swing.*;
import javaUFProtocol.*;

class ImageDisplayPanel extends JPanel {
    public static final
	String rcsID = "$Name:  $ $Id: ImageDisplayPanel.java,v 1.1 2005/04/29 21:30:10 drashkin Exp $";

    public int width, height;
    int xZoomCen, yZoomCen;
    int zoomRect;
    int zoomFactor=0;
    int oMin, oMax;
    int newMin, newMax;
    public String frmBuffName = "";
    boolean doZoom = false;
    protected int zoomPixels[][];
    protected byte scaledPixels[];
    protected Image pixImage = null;
    ImageBuffer imageBuffer;
    IndexColorModel colorModel;
    JComboBox zoomFacSelect;
    ZoomPanel zoomPanel;
    ImagePanel[] imagePanels;
    
    public ImageDisplayPanel( ImagePanel[] imagePanels, IndexColorModel colorModel,
			      UFFrameConfig frameConfig, ZoomPanel zoomPanel )
    {
        this.colorModel = colorModel;
	this.imagePanels = imagePanels;
        this.zoomPanel = zoomPanel;
        this.zoomFacSelect = zoomPanel.zoomFacSel;
	this.width = frameConfig.width;
	this.height = frameConfig.height;
        this.setMinimumSize(new Dimension(width, height));
        
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if( zoomFactor > 1 ) 
                    ZoomEvent(evt);
            }
        });

	zoomFactor = 7;
        zoomFacSelect.setSelectedItem("7");
	zoomRect = zoomPanel.zoomImage.xySize/zoomFactor;

        ZoomListener zoomListener = new ZoomListener();
        zoomFacSelect.addActionListener(zoomListener);
        
        scaledPixels = new byte[ width * height ];
        repaint();
    }
    
    public void applyLinearScale(int minv, int maxv)
    {
        float range = maxv - minv;
        
        for(int i=0; i < imageBuffer.pixels.length; i++)
	    {
		float f = (float)( imageBuffer.pixels[i] - minv )/range;

		if( f <= 0 )
		    scaledPixels[i] = 0;
		else if( f >= 1 )
		    scaledPixels[i] = (byte)255;
		else
		    scaledPixels[i] = (byte)Math.round(f*255);
	    }
    }
    
    public synchronized void applyLogScale(double threshold) {

        if( imageBuffer == null ) {
	    pixImage = null;
	    return;
	}

	if( threshold <= 0 ) threshold = 1;
        double minv = Math.log( threshold );
	double maxv = Math.log( (double)oMax );
        double range = maxv - minv;

        for( int i=0; i < imageBuffer.pixels.length; i++ )
	    {
		double pval = (double)imageBuffer.pixels[i];
		double f = 0.0;

		if( pval > threshold ) f = ( Math.log( pval ) - minv )/range;

 		if( f <= 0 )
		    scaledPixels[i] = 0;
		else if( f >= 1 )
		    scaledPixels[i] = (byte)255;
		else
		    scaledPixels[i] = (byte)Math.round(f*255);
	    }

        pixImage = createImage(new MemoryImageSource(width, height, colorModel, scaledPixels, 0, width));
        repaint();
    }
    
    public synchronized void applyPowerScale(double threshold, double power) {

        if( imageBuffer == null ) {
	    pixImage = null;
	    return;
	}

	if( threshold <= 0 ) threshold = 1;
	if( power <= 0 ) power = 0.5;
	if( power > 1 ) power = 1;
        double minv = Math.pow( threshold, power );
	double maxv = Math.pow( (double)oMax, power );
        double range = maxv - minv;

        for( int i=0; i < imageBuffer.pixels.length; i++ )
	    {
		double pval = (double)imageBuffer.pixels[i];
		double f = 0.0;

		if( pval > threshold ) f = ( Math.pow( pval, power ) - minv )/range;

 		if( f <= 0 )
		    scaledPixels[i] = 0;
		else if( f >= 1 )
		    scaledPixels[i] = (byte)255;
		else
		    scaledPixels[i] = (byte)Math.round(f*255);
	    }

        pixImage = createImage(new MemoryImageSource(width, height, colorModel, scaledPixels, 0, width));
        repaint();
    }
    
    public synchronized void updateImage(ImageBuffer imgBuff)
    {
        imageBuffer = imgBuff;
        
        if( imageBuffer == null ) pixImage = null;
        else {
            oMax = imageBuffer.max;
            oMin = imageBuffer.min;
        }
    }
    
    public void updateImage( IndexColorModel icm )
    {
        this.colorModel = icm;
        
        if( imageBuffer == null ) pixImage = null;
        else
            pixImage = createImage(new MemoryImageSource(width, height, colorModel, scaledPixels, 0, width));

        repaint();
    }
    
    public void updateImage(int newmin, int newmax)
    {
        newMin = newmin;
        newMax = newmax;
        
        if( imageBuffer == null ) pixImage = null;
        else {
            applyLinearScale( newMin, newMax );
            pixImage = createImage(new MemoryImageSource(width, height, colorModel, scaledPixels, 0, width));
        }

        repaint();
    }
    
    public void drawOriginalImage()
    {
        if( imageBuffer == null ) pixImage = null;
        else {
            applyLinearScale( oMin, oMax );
            pixImage = createImage(new MemoryImageSource(width, height, colorModel, scaledPixels, 0, width));
        }

        repaint();
    }
    
    public void paintComponent( Graphics g ) {
        super.paintComponent(g);
        
        if( pixImage == null ) {
	    g.setColor( Color.GRAY );
	    g.fillRect( 0, 0, width, height );
	    g.setColor( Color.BLACK );
            g.drawString("Buffer is Empty", 20, 20);
        }
	else {
            g.drawImage( pixImage, 0, 0, null );

	    if( zoomFactor > 1 && doZoom ) {
		// Paint a rectangle with a translucent color
		g.setColor(new Color(128, 255, 128, 56));
		g.fillRect(xZoomCen -zoomRect/2, yZoomCen - zoomRect/2, zoomRect, zoomRect);
		// Paint a solid black rectangular outline
		g.setColor(Color.BLACK);
		g.drawRect(xZoomCen - zoomRect/2, yZoomCen - zoomRect/2, zoomRect, zoomRect);
		ZoomImage();
	    }
        }
    }
    
    private void ZoomEvent( java.awt.event.MouseEvent evt ) {
        
        if(pixImage == null) return;
        
        xZoomCen = evt.getX();
        yZoomCen = evt.getY();

        if( xZoomCen < zoomRect/2 ) xZoomCen = zoomRect/2;
        if( yZoomCen < zoomRect/2 ) yZoomCen = zoomRect/2;

        if( (width - xZoomCen) < zoomRect/2 )  xZoomCen = width - zoomRect/2 ;
        if( (height - yZoomCen) < zoomRect/2 ) yZoomCen = height - zoomRect/2 ;
        
	if( !doZoom )
	    {
		for( int i=0; i < imagePanels.length; i++ ) {
		    if( imagePanels[i].imageDisplay.doZoom ) {
			imagePanels[i].imageDisplay.doZoom = false;
			imagePanels[i].imageDisplay.repaint();
		    }
		}
		doZoom = true;
	    }

        repaint();
    }
    
    private void ZoomImage()
    {
	zoomPixels = new int[zoomRect][zoomRect];

        int startX = xZoomCen - zoomRect/2 ;
        int startY = yZoomCen - zoomRect/2 ;

	if( startX < 0 ) startX = 0;
	if( startY < 0 ) startY = 0;

        if( (startX + zoomRect) > width )  startX--;
        if( (startY + zoomRect) > height ) startY--;

	for( int i = 0; i < zoomRect; i++ ) {
	    int isw = startX + (i + startY) * width;
	    for( int j = 0; j < zoomRect; j++ ) {
		int k = isw + j;
		if( k < imageBuffer.pixels.length ) zoomPixels[i][j] = imageBuffer.pixels[k];
	    }
	}

	ImageBuffer zoomBuff = new ImageBuffer( imageBuffer, zoomPixels,  zoomFactor );

        zoomPanel.zoomImage.updateImage( zoomBuff, colorModel, startX, startY );
    }
//---------------------------------------------------------------------------    
    
    class ZoomListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            
            String comboitem = (String)zoomFacSelect.getSelectedItem();

            if( comboitem.length() == 0 ) {
                zoomFactor = 1;
            } else {
                zoomFactor = Integer.parseInt( comboitem );
                zoomRect = zoomPanel.zoomImage.xySize/zoomFactor;
            }

            repaint();
        }
    }
}

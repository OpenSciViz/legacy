package ufjdd;
/**
 * Title:        HistogramBar.java
 * Version:      (see rcsID)
 * Authors:      Ziad Saleh and Frank Varosi
 * Company:      University of Florida
 * Description:  Object for displaying vertical histogram of byte values in an image.
 */
import java.awt.*;
import javax.swing.*;
import javaUFLib.*;

class HistogramBar extends JPanel {

    public static final
	String rcsID = "$Name:  $ $Id: HistogramBar.java,v 1.1 2005/04/29 21:30:10 drashkin Exp $";

    protected int[] histogram;
    protected int[] histoDisp;
    protected int width = 100;
    protected int Ncolors = 256;
    protected int histoMax;
    protected int zero_index = -1 ;
    protected int underMinCnt = 0;
    protected int overMaxCnt  = 0;
    protected UFLabel overMaxLab, underMinLab, maxFreqLab;
    
    public HistogramBar( int width, int Ncolors, UFLabel underMinLab, UFLabel overMaxLab, UFLabel maxFreqLab )
    {
	if( width > 1 ) this.width = width;
	if( Ncolors > 1 ) this.Ncolors = Ncolors;
	this.underMinLab = underMinLab;
	this.overMaxLab = overMaxLab;
	this.maxFreqLab = maxFreqLab;
        this.setPreferredSize( new Dimension( width, Ncolors ) );
        this.setBackground(Color.WHITE);
	histogram = new int[Ncolors];
	histoDisp = new int[Ncolors];
    }

    public void paintComponent( Graphics g ) {

        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D)g;
        
        for( int i=0; i < histoDisp.length; i++ )
	    {
		if( zero_index == i )
		    g2d.setPaint(Color.red);
		else
		    g2d.setPaint(Color.black);

		g2d.drawRect(0, Ncolors-i, histoDisp[i], 1);
	    }
    }
    
    public void updateHistogram( byte dat[][], int zeroIndex ) {

        for( int i=0; i < histogram.length; i++ ) histogram[i] = 0;
        
        for( int i=0; i < dat.length; i++ ) {
            for( int j=0; j < dat.length; j++ ) {
		int bv = (int)dat[i][j];
		if( bv < 0 ) bv += 256;
		++histogram[ bv ];
	    }
	}
        
	scaleHistogram();
	zero_index = zeroIndex;
        repaint();
    }
    
    public void updateHistogram( byte dat[], int zeroIndex ) {

        for( int i=0; i < histogram.length; i++ ) histogram[i] = 0;
        
	for( int j=0; j < dat.length; j++ ) {
	    int bv = (int)dat[j];
	    if( bv < 0 ) bv += 256;
	    ++histogram[ bv ];
	}

	scaleHistogram();
	zero_index = zeroIndex;
        repaint();
    }
    
    private void scaleHistogram() {

	underMinCnt = histogram[0];
	histogram[0] = 0;
	overMaxCnt = histogram[histogram.length-1];
	histogram[histogram.length-1] = 0;

	underMinLab.setText( " " + underMinCnt );
	overMaxLab.setText( " " + overMaxCnt );

        histoMax = (int)UFArrayOps.maxValue( histogram );
        maxFreqLab.setText( " " + histoMax );

        for(int i=0; i<histogram.length; i++)
            histoDisp[i] = Math.round( width * (float)histogram[i]/(float)histoMax );
    }

    public int[] equalizeHisto() {
        int accum = 0;
        int[] accHist = new int[Ncolors];
        
        for(int i=0; i<histogram.length; i++) {
            accum += histogram[i];
            accHist[i] = accum;
        }

        int accMax = accHist[accHist.length-1];
	int[] eqHist = new int[Ncolors];

        for( int i=0; i < accHist.length; i++ )
	{
            float f = (float)accHist[i]/(float)accMax;

            if(f<0) {
                eqHist[i] = 0; 
            }
            else if(f>1) {
                eqHist[i] = 255;
            }
            else
                eqHist[i] = Math.round(f*255);
        }
        
        for( int i=0; i<eqHist.length; i++ ) histoDisp[i] = ( width * eqHist[i] )/255;
        
        repaint();
        return eqHist;
    }
    
    public void applyOrigHisto() {

        for(int i=0; i<histogram.length; i++)
            histoDisp[i] = Math.round( width * (float)histogram[i]/(float)histoMax );

        repaint();
    }
}


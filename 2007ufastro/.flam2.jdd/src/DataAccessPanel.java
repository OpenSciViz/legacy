package ufjdd;
/**
 * Title:        Java Data Display (JDD): DataAccessPanel.java (to access data in frame acq. server buffers)
 * Version:      (see rcsID)
 * Copyright:    Copyright (c) 2004-5
 * Author:       Frank Varosi, Ziad Saleh
 * Company:      University of Florida
 * Description:  Extends class UFLibPanel to use _socket attrib. and connectToServer() method.
 */
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javaUFLib.*;
import javaUFProtocol.*;

class DataAccessPanel extends UFLibPanel {
    
    public static final
	String rcsID = "$Name:  $ $Id: DataAccessPanel.java,v 1.1 2005/04/29 21:30:10 drashkin Exp $";

    ImagePanel[] imagePanels; //needed for updateFrames() method below.
    public UFFrameConfig frameConfig;
    public String[] bufferNames;
    public JButton pauseUpdateButton = new JButton("Pause  Updates");
    boolean pauseUpdates = false;

    public DataAccessPanel(String host, int port, ImagePanel[] imagePanels)
    {
	super( host, port, "CT" );
	this.imagePanels = imagePanels;
	this.serverName = "Data Acq. Server";
	this.connectToServer();

        pauseUpdateButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e)
 		{
		    if( pauseUpdateButton.getText().toUpperCase().indexOf("PAUSE") >= 0 ){
			pauseUpdateButton.setText("Resume  Updates");
			pauseUpdateButton.setBackground( Color.red );
			pauseUpdateButton.setForeground( Color.white );
			pauseUpdates = true;
		    }
		    else {
			pauseUpdateButton.setText("Pause  Updates");
			pauseUpdateButton.setBackground( Color.green );
			pauseUpdateButton.setForeground( Color.black );
			pauseUpdates = false;
		    }
		}
	    });
    }

    public boolean connectToServer()
    {
	if( super.connectToServer() ) {
	    bufferNames = getBufferNames();
	    frameConfig = getFrameConfig();
	    return true;
	}
	else return false;
    }

    public String[] getBufferNames() {
        // Send the agent a timestamp with the command BN to get "Buffer Names"
        UFTimeStamp uft = new UFTimeStamp("BN");
        UFStrings reply = null;
        
        if( uft.sendTo(_socket) <= 0 ) {
            System.err.println("DataAccess.getBufferNames> UFTimeStamp send ERROR");
	    return null;
        }

        if( (reply = (UFStrings)UFProtocol.createFrom(_socket)) == null ) {
            System.err.println("DataAccess.getBufferNames> UFStrings recv ERROR");
	    return null;
        }
        
        int numOfFrames = reply.numVals();
        bufferNames = new String[numOfFrames];
        
        for( int k=0; k < numOfFrames ; k++ ) bufferNames[k] = reply.valData(k);
        return bufferNames;
    }

    public UFFrameConfig getFrameConfig() {
        // Send the agent a timestamp with the command FC to get Frame Configuration
        UFTimeStamp uft = new UFTimeStamp("FC");
        UFFrameConfig fc = null;
        
        if( uft.sendTo(_socket) <= 0 ) {
            System.err.println("DataAccess.getFrameConfig> UFTimeStamp send ERROR");
	    return null;
        }

        if( (fc = (UFFrameConfig)UFProtocol.createFrom(_socket)) == null ) {
            System.err.println("DataAccess.getFrameConfig> UFFrameConfig recv ERROR");
	    return null;
        }

	System.out.println("DataAccess.getFrameConfig> (width=" + fc.width + ",  height=" + fc.height
			   + ") ---> " + fc.name());
	frameConfig = fc;
	return fc;
    }

    public synchronized boolean request( String buffName )
    {
	if( buffName == null ) return false;
        // Send the agent a timestamp with the desired Buffer Name:
        UFTimeStamp uft = new UFTimeStamp(buffName);

        if( uft.sendTo(_socket) <= 0 ) {
            System.err.println("DataAccess.request(" + buffName + ")> UFTimeStamp send ERROR");
            return false;
        }
	else return true;
    }

    public synchronized ImageBuffer recvBuff()
    {
        UFFrameConfig bfc = null;
        try {
            if( (bfc = (UFFrameConfig)UFProtocol.createFrom(_socket)) == null ) {
                System.err.println("DataAccess.recvBuff> UFFrameConfig recv ERROR");
                return null;
            }
        } catch( ClassCastException cce ) {
            System.err.println("DataAccess.recvBuff> Class Cast Exception " + cce);
            return null;
        }
        
        if( bfc.name().indexOf("ERROR") >= 0 ) {
            System.out.println("DataAccess.recvBuff> " + bfc.name());
            return null;
        }
	else {
	    frameConfig = bfc;
	    UFInts ufi = null;
            
            if( (ufi = (UFInts)UFProtocol.createFrom(_socket)) == null )
                System.err.println("DataAccess.recvBuff> UFInts Read ERROR for buffer: " + bfc.name());

            ImageBuffer imgBuffer = new ImageBuffer(bfc, ufi);
            return imgBuffer;
        }
    }

    public synchronized ImageBuffer fetch( String buffName )
    {
	if( request( buffName ) )
	    return recvBuff();
	else
	    return null;
    }
    
    public ImageBuffer[] fetchBuffers( String[] displayBuffers, String updatedBuffers )
    {
	int Nbuffers = 0;
	String bufferList = null;

        for( int i=0 ; i < displayBuffers.length ; i++ )
	{
	    String buf = displayBuffers[i].toLowerCase();

	    if( updatedBuffers.indexOf( buf ) >= 0 ) {
		++Nbuffers;
		if( Nbuffers == 1 )
		    bufferList = buf;
		else
		    bufferList += ("^"+buf);
	    }
	}

	if( request( bufferList ) ) {

	    ImageBuffer[] vimbufs = new ImageBuffer[Nbuffers];
        
	    for( int i=0 ; i < Nbuffers ; i++ ) vimbufs[i] = recvBuff();

	    return vimbufs;
        }
	else return null;
    }

    public void updateFrames(UFFrameConfig notifyFrameConfig) {

	if( pauseUpdates ) return;

	if( notifyFrameConfig.frameProcCnt <= 1 ) { //erase displays:
	    for( int i=0; i < imagePanels.length; i++ ) {
		imagePanels[i].imageDisplay.updateImage( (ImageBuffer)null );
		imagePanels[i].imageDisplay.drawOriginalImage();
	    }
	}

	//The List of updated buffers is contained in: notifyFrameConfig.name().
	//First get the unique names of currently requested buffers displayed:

	HashSet dispBuffs = new HashSet();
	for( int i=0; i < imagePanels.length; i++ ) dispBuffs.add( imagePanels[i].imageDisplay.frmBuffName );

	String[] uniqBuffs = new String[dispBuffs.size()];
	int iu=0;
	for( Iterator bi = dispBuffs.iterator(); bi.hasNext(); ) uniqBuffs[iu++] = (String)bi.next();

        ImageBuffer[] frameBuffs = fetchBuffers( uniqBuffs, notifyFrameConfig.name() );
	if( frameBuffs == null ) return;

	for( int j=0; j < frameBuffs.length; j++ ) {

	    ImageBuffer newImgBuff = frameBuffs[j];

	    if( newImgBuff != null ) {

		for( int i=0; i < imagePanels.length; i++ ) {

		    if( newImgBuff.name.equalsIgnoreCase( imagePanels[i].imageDisplay.frmBuffName ) ) {
			ImagePanel imgPan = imagePanels[i];
			imgPan.imageDisplay.updateImage( newImgBuff );
			imgPan.adjustPanel.updateMinMaxVal( newImgBuff );
		        imgPan.adjustPanel.applySetting(); //checks desired scaling and repaints image.
		    }
		}
	    }
	}
    }
}

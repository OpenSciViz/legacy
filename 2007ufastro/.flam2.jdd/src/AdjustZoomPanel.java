package ufjdd;
/**
 * Title:       AdjustZoomPanel.java
 * Version:     (see rcsID)
 * Authors:     Frank Varosi
 * Company:     University of Florida
 * Description: Object for adjusting the display scaling of image in the ZoomPanel.
 */
public class AdjustZoomPanel extends AdjustPanel {
    
    public static final
	String rcsID = "$Name:  $ $Id: AdjustZoomPanel.java,v 1.1 2005/04/29 21:30:10 drashkin Exp $";

    private ZoomImage zoomImage;
    
    public AdjustZoomPanel() {
        super(null);
    }

    public void setZoomImage( ZoomImage zoomImage ) { this.zoomImage = zoomImage; }

    //just need to override the basic image display methods:

    protected void reDrawImage() { zoomImage.applyLinearScale(); }

    protected void reDrawImage(float min, float max)
    {
	if( useScaleFactor )
	    zoomImage.applyLinearScale( min * CoAdds, max * CoAdds );
	else
	    zoomImage.applyLinearScale( min, max );
    }

    protected void reDrawImage(double threshHold)
    {
	if( useScaleFactor )
	    zoomImage.applyLogScale( threshHold * CoAdds );
	else
	    zoomImage.applyLogScale( threshHold );
    }

    protected void reDrawImage(double threshHold, double power)
    {
	if( useScaleFactor )
	    zoomImage.applyPowerScale( threshHold * CoAdds, power );
	else
	    zoomImage.applyPowerScale( threshHold, power );
    }
}

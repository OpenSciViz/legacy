package ufjdd;
/**
 * Title:        StatisticsPanel.java
 * Version:      (see rcsID)
 * Authors:      Craig Warner, Frank Varosi
 * Company:      University of Florida
 * Description:  For plotting histogram of image data pixel values in a box.
 */
import javaUFLib.*;
import java.awt.*;

public class StatisticsPanel {
    
    public static final
	String rcsID = "$Name:  $ $Id: StatisticsPanel.java,v 1.1 2005/04/29 21:30:10 drashkin Exp $";

    GatorPlot gp;
    boolean first;

    public StatisticsPanel() {
       gp = new GatorPlot("Statistics");
       first = true;
    }
    
    public void update(double[][] data, int x1, int y1, int x2, int y2, double scalefactor) {
        
        //System.out.println("x1=" + x1 + " y1= " + y1 + " x2 = " + x2 + " y2 = " + y2);
        double[][] eData = UFArrayOps.multArrays( UFArrayOps.extractValues(data, y1, y2, x1, x2), scalefactor );
        float mean = (float)UFArrayOps.avgValue( eData );
	float stddev = (float)UFArrayOps.stddev( eData );
	float stdevTrue = stddev/(float)Math.sqrt(scalefactor);
	float sqnpts = (float)Math.sqrt( (x2-x1) * (y2-y1) );
	int nbins = 10*(int)(Math.floor(sqnpts/42)+1);

        if (first) {
           gp.showPlot(false);
           first = false;
        }

	gp.show();
	gp.setState( Frame.NORMAL );
	
	int[] histY = gp.hist( eData,
			       "*nbins=" + nbins +
			       ", *xticks=5, *xtitle=Data, *ytitle=Frequency, *title=Mean = "
			       + UFLabel.truncFormat( mean ) +
			       "   :   Std. Dev. = " + UFLabel.truncFormat( stddev ) +
			       "   :   Orig. Std. Dev. = " + UFLabel.truncFormat( stdevTrue ) );
	float max = (float)UFArrayOps.maxValue(eData);
	float min = (float)UFArrayOps.minValue(eData);
	float rng = max - min;
	int npts = 200;
	float[] x = new float[npts];
	for (int j = 0; j < npts; j++) x[j] = min + j*rng/npts;
        float[] z = UFArrayOps.divArrays( UFArrayOps.subArrays( x, mean ), stddev );
	float[] y = new float[npts];
	int ymax = UFArrayOps.maxValue(histY); 
	for (int j = 0; j < npts; j++) y[j] = (float)( ymax * Math.exp( -z[j]*z[j]/2 ) );
	for (int j = 0; j < npts; j++) x[j] = j*nbins/(float)npts;
	gp.overplot(x, y, ""); 
	gp.setOPlots(1, x, y, "");
    }
    
}

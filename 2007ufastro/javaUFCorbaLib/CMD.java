public class CMD {

    public static String getArg(String[] args, String arg) {
	for (int i=0; i<args.length; i++) 
	    if (args[i].toLowerCase().equals(arg.toLowerCase())) {
		if (i+1 < args.length) return args[i+1];
	    }
	return "null";
    }
    public static boolean findArg(String [] args, String arg) {
	for (int i=0; i<args.length; i++) 
	    if (args[i].toLowerCase().equals(arg.toLowerCase()))
		return true;
	return false;
    }
    public static boolean findArg(String [] args, String arg, String nextPArg){
	for (int i=0; i<args.length; i++)
	    if (args[i].toLowerCase().equals(arg.toLowerCase()) 
		&& i+1 < args.length && args[i+1].toLowerCase().
		indexOf(nextPArg.toLowerCase()) != -1)
		return true;
	    
	return false;
    }
    public static String[] stripArg(String [] args,String arg,String nextPArg){
	String [] retVal = new String[args.length-2];
	int j = 0;
	for (int i=0; i<args.length; i++) {
	    if (args[i].toLowerCase().equals(arg.toLowerCase())
		&& i+1 < args.length && args[i+1].toLowerCase().
		indexOf(nextPArg.toLowerCase()) != -1) {
		i++; continue;
	    }
	    else retVal[j++] = args[i];
	}
	return retVal;
    }
    public static String[] stripArg(String [] args, String arg, boolean next) {
	String [] retVal = new String[args.length - (next?2:1)];
	int j = 0;
	for (int i=0; i<args.length; i++) {
	    if (args[i].toLowerCase().equals(arg.toLowerCase())){
		if (next) 
		    i++;
		continue;
	    }
	    else retVal[j++] = args[i];
	}
	return retVal;
    }
    public static String[] addArg(String [] args, String arg, String nextArg){
	String [] retVal = new String[args.length+2];
	for (int i=0; i<args.length; i++) retVal[i] = args[i];
	retVal[args.length] = arg;
	retVal[args.length+1] = nextArg;
	return retVal;
    }
}

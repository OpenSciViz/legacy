

/*
 *
 *  Header for ec agent simulation CAD record code.
 *
 */

#include <stdioLib.h>
#include <string.h>

#include <sysLib.h>
#include <msgQLib.h>

#include "ufdbl.h"
#include <recSup.h>

#include <cad.h>
#include <cadRecord.h>

#include <trecsEcAgentLayer.h>
#include <trecsEcAgentSim.h>


/*
 * Local defines
 */

#define MAX_MESSAGES                10	/* max messages per 
					   queue */
#define CAD_Q_WAIT_TIME             5	/* max time to wait 
					   for Q */


/*
 *  Private data
 */

static MSG_Q_ID ecCommandQ = NULL;	/* message queue
					   for agent */



/*
 *************************************
 *
 *  Header for ecCadInit
 *
 *
 *************************************
 */


long
ecCadInit (cadRecord * pcr	/* cad record structure */
  )
{
  long status;			/* function return status */



  /* 
   *  If a message queue has already been created then there is nothing
   *  to do since initialization is only done the first time this function
   *  is called
   */

  if (ecCommandQ)
    {
      return OK;
    }


  /* 
   *  Otherwise this is the first call, create a message queue 
   *  to communicate with the simulated CC agent then initialize
   *  the agent itself.
   */

  ecCommandQ = msgQCreate (MAX_MESSAGES, sizeof (ecAgentCommand), MSG_Q_FIFO);
  if (ecCommandQ == NULL)
    {
      status = -1;
      recGblRecordError (status, pcr, __FILE__ ":can't create message Queue");
      return status;
    }

  status = initEcAgentSim (ecCommandQ);

  return status;
}


 /* 
  *************************************
  *
  *  Header for ecCadProcess 
  *
  *
  *************************************
  */

long
ecCadProcess (cadRecord * pcr	/* cad record structure */
  )
{
  char *pCommand;		/* command name pointer */
  ecAgentCommand command;	/* agent command structure */
  long status;			/* function return status */


  /* 
   *  Process aecording to the directive given
   */

  switch (pcr->dir)
    {
      /* 
       *  Mark, Clear and Stop do nothing so can be aecepted immediately
       */

    case CAD_MARK:
      break;

    case CAD_CLEAR:
      break;

    case CAD_STOP:
      break;


      /* 
       *  Preset and Start both check the input attributes before doing
       *  anything.
       */

    case CAD_PRESET:
    case CAD_START:

      /* 
       *  Preset just checks the attributes, so bail out here
       */

      if (pcr->dir == CAD_PRESET)
	{
	  break;
	}


      /* 
       *  Start executes the command.
       */


      /* 
       *  Copy record names into the command structure then
       *  place it on the message queue.
       */

      strcpy (command.carName, "trecs:ec:applyC");
      strcpy (command.attributeA, pcr->a);
      strcpy (command.attributeB, pcr->b);


      /* 
       *  Set the execution time based on the command received
       */

      for (pCommand = pcr->name; *pCommand != ':'; pCommand++);
      for (pCommand++; *pCommand != ':'; pCommand++);
      pCommand++;

      strcpy (command.commandName, pCommand);

      if (strcmp (pCommand, "tempSet") == 0)
	{
	  command.executionTime = 100;
	}

      else
	{
	  command.executionTime = 20;
	}

      status = msgQSend (ecCommandQ,
			 (char *) &command,
			 sizeof (command), CAD_Q_WAIT_TIME, MSG_PRI_NORMAL);
      if (status == ERROR)
	{
	  strncpy (pcr->mess, "Can not send agent message", MAX_STRING_SIZE);
	  return CAD_REJECT;
	}

      strcpy (pcr->vala, pcr->a);
      strcpy (pcr->valb, pcr->b);

      break;

    default:
      strncpy (pcr->mess, "Invalid directive received", MAX_STRING_SIZE);
      return CAD_REJECT;
    }

  return CAD_ACCEPT;
}

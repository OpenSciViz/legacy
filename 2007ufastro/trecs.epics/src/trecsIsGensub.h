
/* INDENT OFF */

/*+
 *
 * FILENAME
 * -------- 
 * trecsIsGensub.h
 *
 * PURPOSE
 * -------
 * declare public functions for the T-Recs instrument sequencer gensub
 * record support code.
 *
 * FUNCTION NAME(S)
 * ----------------
 * 
 * DEPENDENCIES
 * ------------
 *
 * LIMITATIONS
 * -----------
 * 
 * AUTHOR
 * ------
 * William Rambold (wrambold@gemini.edu)
 * 
 * HISTORY
 * -------
 * 2000/12/12  WNR  Initial coding
 *
 */

/* INDENT ON */

/* ===================================================================== */


/*
 *
 *  Public function prototypes
 *
 */

long trecsCmdCombineGProcess (genSubRecord * pgs);
long trecsIsCopyGProcess (genSubRecord * pgs);
long trecsIsInitGInit (genSubRecord * pgs);
long trecsIsInitGProcess (genSubRecord * pgs);
long trecsIsInsSetupCopy (genSubRecord * pgs);
long trecsIsInsSetupTranslate (genSubRecord * pgs);
long trecsIsNullGInit (genSubRecord * pgs);
long trecsIsRebootGInit (genSubRecord * pgs);
long trecsIsRebootGProcess (genSubRecord * pgs);
long trecsIsSubSysCombineProcess (genSubRecord * pgs);
long trecsIsTranslateGProcess (genSubRecord * pgs);
long trecsStateGProcess (genSubRecord * pgs);

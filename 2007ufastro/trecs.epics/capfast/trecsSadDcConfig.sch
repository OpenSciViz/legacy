[schematic2]
uniq 68
[tools]
[detail]
w 644 1499 100 2 n#67 hwin.hwin#66.in 640 1504 640 1504 esirs.esirs#32.INP
w 644 667 100 2 n#65 hwin.hwin#64.in 640 672 640 672 esirs.esirs#34.INP
w 644 1083 100 2 n#63 hwin.hwin#62.in 640 1088 640 1088 esirs.esirs#33.INP
w 1348 1915 100 2 n#61 hwin.hwin#60.in 1344 1920 1344 1920 esirs.esirs#40.INP
w -92 1915 100 2 n#59 hwin.hwin#58.in -96 1920 -96 1920 esirs.esirs#7.INP
w -92 1499 100 2 n#57 hwin.hwin#56.in -96 1504 -96 1504 esirs.esirs#27.INP
w -92 251 100 2 n#55 hwin.hwin#54.in -96 256 -96 256 esirs.esirs#30.INP
w 644 1915 100 2 n#53 hwin.hwin#52.in 640 1920 640 1920 esirs.esirs#31.INP
w -92 1083 100 2 n#51 hwin.hwin#50.in -96 1088 -96 1088 esirs.esirs#28.INP
w 644 251 100 2 n#49 hwin.hwin#48.in 640 256 640 256 esirs.esirs#35.INP
w -92 667 100 2 n#47 hwin.hwin#46.in -96 672 -96 672 esirs.esirs#29.INP
s 2624 2032 100 1792 2000/11/12
s 2480 2032 100 1792 NWR
s 2336 2032 100 1792 Initial Layout
s 2112 2032 100 1792 A
s 2512 -240 100 1792 trecsSadDcConfig.sch
s 2096 -272 100 1792 Author: NWR
s 2096 -240 100 1792 2000/11/12
s 2320 -240 100 1792 Rev: A
s 2432 -192 100 256 T-Recs DC Configuration
s 2096 -176 200 1792 T-ReCS
s 2240 -128 100 0 Gemini Thermal Region Camera System
[cell use]
use hwin 448 1463 100 0 hwin#66
xform 0 544 1504
p 352 1536 100 0 -1 val(in):$(dc)hardwareG.VALJ
use hwin 448 631 100 0 hwin#64
xform 0 544 672
p 352 704 100 0 -1 val(in):$(dc)hardwareG.VALK
use hwin 448 1047 100 0 hwin#62
xform 0 544 1088
p 352 1120 100 0 -1 val(in):$(dc)hardwareG.VALL
use hwin 1152 1879 100 0 hwin#60
xform 0 1248 1920
p 1056 1952 100 0 -1 val(in):$(dc)hardwareG.VALN
use hwin -288 1879 100 0 hwin#58
xform 0 -192 1920
p -384 1952 100 0 -1 val(in):$(dc)hardwareG.VALE
use hwin -288 1463 100 0 hwin#56
xform 0 -192 1504
p -384 1536 100 0 -1 val(in):$(dc)hardwareG.VALD
use hwin -288 215 100 0 hwin#54
xform 0 -192 256
p -384 288 100 0 -1 val(in):$(dc)hardwareG.VALF
use hwin 448 1879 100 0 hwin#52
xform 0 544 1920
p 352 1952 100 0 -1 val(in):$(dc)physOutG.VALG
use hwin -288 1047 100 0 hwin#50
xform 0 -192 1088
p -384 1120 100 0 -1 val(in):$(dc)hardwareG.VALC
use hwin 448 215 100 0 hwin#48
xform 0 544 256
p 352 288 100 0 -1 val(in):$(dc)hardwareG.VALH
use hwin -288 631 100 0 hwin#46
xform 0 -192 672
p -384 704 100 0 -1 val(in):$(dc)hardwareG.VALI
use esirs -96 1671 100 0 esirs#7
xform 0 112 1824
p -32 1632 100 0 1 SCAN:1 second
p 64 1664 100 1024 -1 name:$(top)chopCoaddUc
use esirs -96 1255 100 0 esirs#27
xform 0 112 1408
p -32 1216 100 0 1 SCAN:1 second
p 64 1248 100 1024 -1 name:$(top)chopSettleUf
use esirs -96 839 100 0 esirs#28
xform 0 112 992
p -32 800 100 0 1 SCAN:1 second
p 64 832 100 1024 -1 name:$(top)frameCoaddUf
use esirs -96 423 100 0 esirs#29
xform 0 112 576
p -32 384 100 0 1 SCAN:1 second
p 64 416 100 1024 -1 name:$(top)nodSets
use esirs -96 7 100 0 esirs#30
xform 0 112 160
p -32 -32 100 0 1 SCAN:Passive
p 64 0 100 1024 -1 name:$(top)nodSettleUc
use esirs 640 1671 100 0 esirs#31
xform 0 848 1824
p 704 1632 100 0 1 SCAN:1 second
p 800 1664 100 1024 -1 name:$(top)nodSettleUf
use esirs 640 1255 100 0 esirs#32
xform 0 848 1408
p 704 1216 100 0 1 SCAN:1 second
p 800 1248 100 1024 -1 name:$(top)pixelClocks
use esirs 640 839 100 0 esirs#33
xform 0 848 992
p 704 800 100 0 1 SCAN:1 second
p 800 832 100 1024 -1 name:$(top)postChopsUc
use esirs 640 423 100 0 esirs#34
xform 0 848 576
p 704 384 100 0 1 SCAN:1 second
p 800 416 100 1024 -1 name:$(top)preChopsUc
use esirs 640 7 100 0 esirs#35
xform 0 848 160
p 704 -32 100 0 1 SCAN:1 second
p 800 0 100 1024 -1 name:$(top)saveSets
use esirs 1344 7 100 0 esirs#36
xform 0 1552 160
p 1408 -32 100 0 1 SCAN:Passive
p 1504 0 100 1024 -1 name:$(top)photonEfficiency
use esirs 1344 423 100 0 esirs#37
xform 0 1552 576
p 1408 384 100 0 1 SCAN:Passive
p 1520 416 100 1024 -1 name:$(top)onSrcEfficiency
use esirs 1344 839 100 0 esirs#38
xform 0 1552 992
p 1408 800 100 0 1 SCAN:Passive
p 1504 832 100 1024 -1 name:$(top)nodEfficiency
use esirs 1344 1255 100 0 esirs#39
xform 0 1552 1408
p 1408 1216 100 0 1 SCAN:Passive
p 1504 1248 100 1024 -1 name:$(top)chopEfficiency
use esirs 1344 1671 100 0 esirs#40
xform 0 1552 1824
p 1408 1632 100 0 1 SCAN:1 second
p 1504 1664 100 1024 -1 name:$(top)readoutMode
use esirs 2048 1655 100 0 esirs#41
xform 0 2256 1808
p 2112 1616 100 0 1 SCAN:Passive
p 2208 1648 100 1024 -1 name:$(top)frameEfficiency
use esirs 2048 1239 100 0 esirs#42
xform 0 2256 1392
p 2112 1200 100 0 1 SCAN:Passive
p 2208 1232 100 1024 -1 name:$(top)detTempErrorBand
use esirs 2048 823 100 0 esirs#43
xform 0 2256 976
p 2112 784 100 0 1 SCAN:Passive
p 2208 816 100 1024 -1 name:$(top)maxDetWell
use esirs 2048 407 100 0 esirs#44
xform 0 2256 560
p 2112 368 100 0 1 SCAN:Passive
p 2208 400 100 1024 -1 name:$(top)minDetWell
use tb200abc 1984 1991 100 0 tb200abc#1
xform 0 2336 2048
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: trecsSadDcConfig.sch,v 0.2 2003/02/17 17:40:37 trecs beta $

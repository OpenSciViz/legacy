[schematic2]
uniq 190
[tools]
[detail]
w 194 203 100 0 n#188 hwin.hwin#186.in 160 192 288 192 ewaits.ewaits#187.INAN
w 962 843 100 0 n#184 eaos.eaos#76.OUT 928 832 1056 832 1056 768 1152 768 hwout.hwout#77.outp
w 1362 907 100 0 n#183 eaos.eaos#76.FLNK 928 896 1856 896 1856 704 2112 704 ecalcs.ecalcs#129.SLNK
w 1954 1099 100 0 n#182 hwin.hwin#133.in 1856 1088 2112 1088 ecalcs.ecalcs#129.INPA
w 1394 875 100 0 n#181 eaos.eaos#76.VAL 928 864 1920 864 1920 1056 2112 1056 ecalcs.ecalcs#129.INPB
w 562 171 100 0 n#155 ewaits.ewaits#187.FLNK 512 160 672 160 trecsOneShot.trecsOneShot#162.RESET
w 1010 171 100 0 n#154 trecsOneShot.trecsOneShot#162.RUNNING 928 160 1152 160 ebis.ebis#159.INP
w 956 1419 100 0 n#128 hwout.hwout#125.outp 1120 1312 960 1312 960 1536 832 1536 eapply.eapply#8.OCLD
w 988 1483 100 0 n#127 hwout.hwout#126.outp 1120 1408 992 1408 992 1568 832 1568 eapply.eapply#8.OUTD
w 324 1419 100 0 n#124 hwin.hwin#121.in 160 1312 320 1312 320 1536 448 1536 eapply.eapply#8.INMD
w 292 1483 100 0 n#123 hwin.hwin#122.in 160 1408 288 1408 288 1568 448 1568 eapply.eapply#8.INPD
w 600 875 100 0 n#85 ewaits.ewaits#81.FLNK 512 928 576 928 576 864 672 864 eaos.eaos#76.SLNK
w 568 907 100 0 n#84 ewaits.ewaits#81.VAL 512 896 672 896 eaos.eaos#76.DOL
w 216 939 100 0 n#83 hwin.hwin#62.in 160 896 192 896 192 928 288 928 ewaits.ewaits#81.INBN
w 216 971 100 0 n#82 hwin.hwin#60.in 160 992 192 992 192 960 288 960 ewaits.ewaits#81.INAN
w 920 1603 100 0 n#33 eapply.eapply#8.OCLC 832 1600 1056 1600 1056 1504 1120 1504 hwout.hwout#28.outp
w 936 1635 100 0 n#32 eapply.eapply#8.OUTC 832 1632 1088 1632 1088 1600 1120 1600 hwout.hwout#27.outp
w 936 1667 100 0 n#31 eapply.eapply#8.OCLB 832 1664 1088 1664 1088 1696 1120 1696 hwout.hwout#26.outp
w 920 1699 100 0 n#30 eapply.eapply#8.OUTB 832 1696 1056 1696 1056 1792 1120 1792 hwout.hwout#25.outp
w 888 1731 100 0 n#29 eapply.eapply#8.OCLA 832 1728 992 1728 992 1888 1120 1888 hwout.hwout#24.outp
w 956 1867 100 0 n#23 eapply.eapply#8.OUTA 832 1760 960 1760 960 1984 1120 1984 hwout.hwout#22.outp
w 312 1603 100 0 n#21 eapply.eapply#8.INMC 448 1600 224 1600 224 1504 160 1504 hwin.hwin#15.in
w 296 1635 100 0 n#20 eapply.eapply#8.INPC 448 1632 192 1632 192 1600 160 1600 hwin.hwin#14.in
w 296 1667 100 0 n#19 eapply.eapply#8.INMB 448 1664 192 1664 192 1696 160 1696 hwin.hwin#13.in
w 312 1699 100 0 n#18 eapply.eapply#8.INPB 448 1696 224 1696 224 1792 160 1792 hwin.hwin#12.in
w 344 1731 100 0 n#17 eapply.eapply#8.INMA 448 1728 288 1728 288 1888 160 1888 hwin.hwin#11.in
w 316 1867 100 0 n#16 eapply.eapply#8.INPA 448 1760 320 1760 320 1984 160 1984 hwin.hwin#10.in
s 2624 1936 100 1792 2002/15/02
s 2480 1936 100 1792 WNR
s 2240 1936 100 1792 Updated Dc names
s 2016 1936 100 1792 E
s 2624 1968 100 1792 2001/12/20
s 2480 1968 100 1792 WNR
s 2240 1968 100 1792 Moved external status
s 2016 1968 100 1792 D
s 2624 2032 100 1792 2001/02/03
s 2480 2032 100 1792 WNR
s 2240 2032 100 1792 Added RH comparitor
s 2016 2032 100 1792 B
s 2512 -240 100 1792 trecsControl.sch
s 2096 -272 100 1792 Author: WNR
s 2096 -240 100 1792 2002/05/02
s 2320 -240 100 1792 Rev: E
s 2432 -192 100 256 T-ReCS System Control
s 2096 -176 200 1792 T-ReCS
s 2240 -128 100 0 Gemini Thermal Region Camera System
s 2624 2064 100 1792 2000/12/18
s 2480 2064 100 1792 WNR
s 2240 2064 100 1792 Initial Layout
s 2016 2064 100 1792 A
s 2624 2000 100 1792 2001/09/06
s 2480 2000 100 1792 LTF
s 2240 2000 100 1792 Corrected some wires
s 2016 2000 100 1792 C
[cell use]
use changeBar 1984 1895 100 0 changeBar#189
xform 0 2336 1936
use changeBar 1984 1927 100 0 changeBar#152
xform 0 2336 1968
use changeBar 1984 1991 100 0 changeBar#140
xform 0 2336 2032
use changeBar 1984 2023 100 0 changeBar#141
xform 0 2336 2064
use changeBar 1984 1959 100 0 changeBar#142
xform 0 2336 2000
use ewaits 288 775 100 0 ewaits#81
xform 0 400 896
p 288 672 100 0 1 CALC:A+B
p 272 928 100 1280 -1 INBP:Yes
p 272 896 100 1280 -1 INCP:No
p 288 736 100 0 1 OOPT:On Change
p 288 704 100 0 1 SCAN:I/O Intr
p 288 768 100 0 1 name:$(top)rhMonitor
use ewaits 288 7 100 0 ewaits#187
xform 0 400 128
p 288 -96 100 0 1 CALC:A
p 272 160 100 1280 -1 INBP:No
p 272 128 100 1280 -1 INCP:No
p 288 -32 100 0 1 OOPT:On Change
p 288 -64 100 0 1 SCAN:I/O Intr
p 288 0 100 0 1 name:$(top)tcsHbMonitor
use hwin 1664 1047 100 0 hwin#133
xform 0 1760 1088
p 1392 1088 100 0 -1 val(in):$(top)kBrRhLimit.VAL
use hwin -32 1367 100 0 hwin#122
xform 0 64 1408
p -288 1408 100 0 -1 val(in):$(top)ec:apply.VAL
use hwin -32 1271 100 0 hwin#121
xform 0 64 1312
p -304 1312 100 0 -1 val(in):$(top)ec:apply.MESS
use hwin -32 1943 100 0 hwin#10
xform 0 64 1984
p -256 1984 100 0 -1 val(in):$(top)is:apply.VAL
use hwin -32 1847 100 0 hwin#11
xform 0 64 1888
p -272 1888 100 0 -1 val(in):$(top)is:apply.MESS
use hwin -32 1751 100 0 hwin#12
xform 0 64 1792
p -256 1792 100 0 -1 val(in):$(top)cc:apply.VAL
use hwin -32 1655 100 0 hwin#13
xform 0 64 1696
p -272 1696 100 0 -1 val(in):$(top)cc:apply.MESS
use hwin -32 1559 100 0 hwin#14
xform 0 64 1600
p -256 1600 100 0 -1 val(in):$(top)dc:apply.VAL
use hwin -32 1463 100 0 hwin#15
xform 0 64 1504
p -288 1504 100 0 -1 val(in):$(top)dc:apply.MESS
use hwin -32 951 100 0 hwin#60
xform 0 64 992
p -368 992 100 0 -1 val(in):$(top)externalHumidity.VAL
use hwin -32 855 100 0 hwin#62
xform 0 64 896
p -320 896 100 0 -1 val(in):$(top)ec:tempMonG.VALM
use hwin -32 151 100 0 hwin#186
xform 0 64 192
p -320 192 100 0 -1 val(in):$(top)tcsHeartbeat.VAL
use hwout 1120 1367 100 0 hwout#126
xform 0 1216 1408
p 1344 1408 100 0 -1 val(outp):$(top)ec:apply.DIR
use hwout 1120 1271 100 0 hwout#125
xform 0 1216 1312
p 1344 1312 100 0 -1 val(outp):$(top)ec:apply.CLID
use hwout 1120 1943 100 0 hwout#22
xform 0 1216 1984
p 1328 1984 100 0 -1 val(outp):$(top)is:apply.DIR
use hwout 1120 1847 100 0 hwout#24
xform 0 1216 1888
p 1328 1888 100 0 -1 val(outp):$(top)is:apply.CLID
use hwout 1120 1751 100 0 hwout#25
xform 0 1216 1792
p 1328 1792 100 0 -1 val(outp):$(top)cc:apply.DIR
use hwout 1120 1655 100 0 hwout#26
xform 0 1216 1696
p 1328 1696 100 0 -1 val(outp):$(top)cc:apply.CLID
use hwout 1120 1559 100 0 hwout#27
xform 0 1216 1600
p 1328 1600 100 0 -1 val(outp):$(top)dc:apply.DIR
use hwout 1120 1463 100 0 hwout#28
xform 0 1216 1504
p 1328 1504 100 0 -1 val(outp):$(top)dc:apply.CLID
use hwout 1152 727 100 0 hwout#77
xform 0 1248 768
p 1376 768 100 0 -1 val(outp):$(sad)windowRh.VAL PP NMS
use trecsOneShot 672 39 100 0 trecsOneShot#162
xform 0 800 160
p 672 -16 100 0 1 setTimeout:timeout $(HB_TIMEOUT)
p 672 16 100 0 1 setTimer:timer $(top)tcsHbTimer
use ebis 1152 55 100 0 ebis#159
xform 0 1280 128
p 1184 16 100 0 1 SCAN:1 second
p 1184 48 100 0 1 name:$(top)tcsIsAlive
use trecsExternalStatus 1984 1319 100 0 trecsExternalStatus#153
xform 0 2208 1600
use ecalcs 2112 615 100 0 ecalcs#129
xform 0 2256 880
p 2176 576 100 0 1 CALC:B>A
p 2176 608 100 0 1 name:$(top)rhTooHigh
use eaos 672 775 100 0 eaos#76
xform 0 800 864
p 736 736 100 0 1 OMSL:closed_loop
p 848 768 100 1024 1 name:$(top)windowRh
p 928 832 75 768 -1 pproc(OUT):PP
use eapply 448 1223 100 0 eapply#8
xform 0 640 1584
p 608 1200 100 1024 1 name:$(top)apply
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: trecsControl.sch,v 0.1 2002/11/26 22:03:24 hon beta $

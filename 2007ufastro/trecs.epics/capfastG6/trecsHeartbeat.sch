[schematic2]
uniq 110
[tools]
[detail]
w 1650 971 100 0 n#104 elongouts.elongouts#86.DOL 1728 960 1632 960 junction
w 1378 1195 100 0 n#104 ecalcs.ecalcs#92.VAL 1568 896 1632 896 1632 1184 1184 1184 1184 1088 1280 1088 ecalcs.ecalcs#92.INPA
w 994 491 100 0 n#107 ecalcs.ecalcs#92.INPD 1280 992 1120 992 1120 480 928 480 trecsOneShot.trecsOneShot#109.RUNNING
w 1074 1035 100 0 n#106 ecalcs.ecalcs#92.INPC 1280 1024 928 1024 trecsOneShot.trecsOneShot#108.RUNNING
w 994 1547 100 0 n#105 ecalcs.ecalcs#92.INPB 1280 1056 1120 1056 1120 1536 928 1536 trecsOneShot.trecsOneShot#89.RUNNING
w 1618 939 100 0 n#103 ecalcs.ecalcs#92.FLNK 1568 928 1728 928 elongouts.elongouts#86.SLNK
w 232 523 100 0 n#100 hwin.hwin#102.in 224 512 288 512 ewaits.ewaits#101.INAN
w 568 491 100 0 n#99 ewaits.ewaits#101.FLNK 512 480 672 480 trecsOneShot.trecsOneShot#109.RESET
w 568 1035 100 0 n#98 ewaits.ewaits#96.FLNK 512 1024 672 1024 trecsOneShot.trecsOneShot#108.RESET
w 232 1067 100 0 n#97 hwin.hwin#95.in 224 1056 288 1056 ewaits.ewaits#96.INAN
w 568 1547 100 0 n#94 ewaits.ewaits#45.FLNK 512 1536 672 1536 trecsOneShot.trecsOneShot#89.RESET
w 232 1579 100 0 n#93 hwin.hwin#52.in 224 1568 288 1568 ewaits.ewaits#45.INAN
w 1992 907 100 0 n#87 elongouts.elongouts#86.OUT 1984 896 2048 896 hwout.hwout#88.outp
s 2240 -128 100 0 Gemini Thermal Region Camera System
s 2096 -176 200 1792 T-ReCS
s 2432 -192 100 256 T-ReCS Heartbeat Generator
s 2320 -240 100 1792 Rev: A
s 2096 -240 100 1792 2001/01/11
s 2096 -272 100 1792 Author: WNR
s 2512 -240 100 1792 trecsHeartbeat.sch
s 2016 2032 100 1792 A
s 2240 2032 100 1792 Initial Layout
s 2480 2032 100 1792 WNR
s 2624 2032 100 1792 2001/01/11
[cell use]
use trecsOneShot 688 279 100 0 trecsOneShot#109
xform 0 800 480
p 672 320 100 0 1 setTimeout:timeout $(HB_TIMEOUT)
p 672 352 100 0 1 setTimer:timer $(top)ecHbTimer
use trecsOneShot 688 823 100 0 trecsOneShot#108
xform 0 800 1024
p 672 864 100 0 1 setTimeout:timeout $(HB_TIMEOUT)
p 672 896 100 0 1 setTimer:timer $(top)dcHbTimer
use trecsOneShot 688 1335 100 0 trecsOneShot#89
xform 0 800 1536
p 672 1376 100 0 1 setTimeout:timeout $(HB_TIMEOUT)
p 672 1408 100 0 1 setTimer:timer $(top)ccHbTimer
use hwin 32 471 100 0 hwin#102
xform 0 128 512
p -272 512 100 0 -1 val(in):$(top)ec:heartbeat.VAL
use hwin 32 1015 100 0 hwin#95
xform 0 128 1056
p -256 1056 100 0 -1 val(in):$(top)dc:heartbeat.VAL
use hwin 32 1527 100 0 hwin#52
xform 0 128 1568
p -288 1568 100 0 -1 val(in):$(top)cc:heartbeat.VAL
use ewaits 288 327 100 0 ewaits#101
xform 0 400 448
p 320 208 100 256 1 CALC:A
p 272 480 100 1280 -1 INBP:No
p 272 448 100 1280 -1 INCP:No
p 288 272 100 768 1 OOPT:On Change
p 288 240 100 0 1 SCAN:I/O Intr
p 416 320 100 1024 1 name:$(top)ecHbMonitor
use ewaits 288 871 100 0 ewaits#96
xform 0 400 992
p 320 752 100 256 1 CALC:A
p 272 1024 100 1280 -1 INBP:No
p 272 992 100 1280 -1 INCP:No
p 288 816 100 768 1 OOPT:On Change
p 288 784 100 0 1 SCAN:I/O Intr
p 416 864 100 1024 1 name:$(top)dcHbMonitor
use ewaits 288 1383 100 0 ewaits#45
xform 0 400 1504
p 320 1264 100 256 1 CALC:A
p 272 1536 100 1280 -1 INBP:No
p 272 1504 100 1280 -1 INCP:No
p 288 1328 100 768 1 OOPT:On Change
p 288 1296 100 0 1 SCAN:I/O Intr
p 416 1376 100 1024 1 name:$(top)ccHbMonitor
use ecalcs 1280 615 100 0 ecalcs#92
xform 0 1424 880
p 1280 576 100 0 1 CALC:(B&&C&&D)?A+1:0
p 1280 544 100 0 1 SCAN:1 second
p 1424 608 100 1024 1 name:$(top)heartbeatCalc
use hwout 2048 855 100 0 hwout#88
xform 0 2144 896
p 2272 896 100 0 -1 val(outp):$(sad)heartbeat.VAL PP NMS
use elongouts 1728 839 100 0 elongouts#86
xform 0 1856 928
p 1792 800 100 0 1 OMSL:closed_loop
p 1920 832 100 1024 1 name:$(top)heartbeat
p 1984 896 75 768 -1 pproc(OUT):PP
use tb200abc 1984 1991 100 0 tb200abc#1
xform 0 2336 2048
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: trecsHeartbeat.sch,v 0.1 2003/02/15 03:02:14 trecs beta $

#if !defined(__UFPortescapConfig_h__)
#define __UFPortescapConfig_h__ "$Name:  $ $Id: UFPortescapConfig.h,v 0.6 2003/09/24 21:16:48 hon beta $"
#define __UFPortescapConfig_H__(arg) const char arg##UFPortescapConfig_h__rcsId[] = __UFPortescapConfig_h__;

#include "UFDeviceConfig.h"
#include "UFPortescapTermServ.h"

using namespace std;
#include "string"

// forward declarations for friendships
class UFPortescapAgent;
class UFGemPortescapAgent;

class UFPortescapConfig : public UFDeviceConfig {
public:
  UFPortescapConfig(const string& name= "UnknownPortescap@DefaultConfig");
  UFPortescapConfig(const string& name, const string& tshost, int tsport);
  inline virtual ~UFPortescapConfig() {}

  static void setIndexorNames(int instrumId, const string& motors= "");
  static bool newSettingCmd(const string& indexor, const string cmdimpl);

  // to support party-line or direct-line indexors:
  static int parsePorts(const string indexorports, map< string, int >& portmap, const string& start= "A");
  // wrappers to support party-line or direct-line indexors:
  // if _Indexors look like: "A:portA B:portB ... N:portN", or not, use tsport 
  static int parsePorts(int& tsport, map< string, int >& portmap);
  // if parsePorts return 1, assume party-line:
  static int partyLine(int port);
  // if multiple ports are indicated, assume direct lines:
  static int directLines(const map< string, int >& portmap);

  // override these virtuals:
  // device i/o behavior
  virtual UFTermServ* connect(const string& host= "", int port= 0);

  virtual UFStrings* status(UFDeviceAgent*);
  virtual UFStrings* statusFITS(UFDeviceAgent*);

  // device cmds
  virtual vector< string >& UFPortescapConfig::queryCmds();
  virtual vector< string >& UFPortescapConfig::actionCmds();

  // return -1 if invalid, 0 if valid but no reply expected,
  // n > 0 if valid and n reply strings expected:
  virtual int validCmd(const string& c);

  // the default behavior is ok here:
  //virtual string terminator();
  //virtual string prefix();

  static bool jogMotor(const string& motor, const string& cmd, double curpos, string& jogcmd);

  static bool _verbose;
  static int _instrumId;

  // keep last hold-run current settings
  static map< string, string > _HoldRunC;
  static map< string, int> _DatumCnt;

protected:
  static vector <string> _Indexors; // names of All indexors 
  static string _Motors; // names as single cmd-line string
  // keep track of prior settings cmd:
  static map< string, string> _HoldRunCcmd;
  static map< string, string> _SlewVcmd;
  static map< string, string> _AccelDecelcmd;
  static map< string, string> _InitVcmd;
  static map< string, string> _Settlecmd;
  static map< string, string> _Jogcmd;

  // allow Agents access to static _Indexors
  friend class UFPortescapAgent;
  friend class UFGemPortescapAgent;
};

#endif // __UFPortescapConfig_h__

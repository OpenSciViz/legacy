#if !defined(__UFPortescapConfig_cc__)
#define __UFPortescapConfig_cc__ "$Name:  $ $Id: UFPortescapConfig.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFPortescapConfig_cc__;

#include "uftrecsmech.h" // TReCS mechanism names & named positions
#include "ufflam1mech.h" // Flamingos mechanism names & named positions

#include "UFPortescapConfig.h"
#include "UFGemPortescapAgent.h"
#include "UFFITSheader.h"

// statics
int _instrumId= UFDeviceAgent::TReCS;
bool UFPortescapConfig::_verbose;
vector <string> UFPortescapConfig::_Indexors;
string UFPortescapConfig::_Motors;

// hold-run current settings  
map< string, string> UFPortescapConfig::_HoldRunC;

// datum history (count) -- has indexor been datumed? how many times?
// 0 means never, > 0 means home command datum count
// < 0 means origin command executed rather than home command
// if the datum count is 0 for an indexor and the current indexor step 
// count is also 0, do not assume mechanism is datumed -- accept
// a datum command but interpose an intial step of 10 counts
// in the opposite direction of the requested home direction;
// otherwise treat a datum request as a no-op?
map< string, int> UFPortescapConfig::_DatumCnt;

// prior cmds:
map< string, string> UFPortescapConfig::_HoldRunCcmd;
map< string, string> UFPortescapConfig::_SlewVcmd;
map< string, string> UFPortescapConfig::_AccelDecelcmd;
map< string, string> UFPortescapConfig::_InitVcmd;
map< string, string> UFPortescapConfig::_Settlecmd;
map< string, string> UFPortescapConfig::_Jogcmd;

// note that this is not really a "jog" function, which the
// portescaps only support via its jog inputs (wires?)
bool UFPortescapConfig::jogMotor(const string& motor, const string& cmd, double curpos, string& jogcmd) {
  bool homecmd = (cmd.rfind("F") != string::npos && cmd.rfind("F") != 0);
  double hdif = ::fabs(curpos) - 0.01;
  // if this motor has never been datumed or origined its current position
  // may be ambiguous, so we must perform an inital step in the oposite direction
  // of the first home command to deal with the pathelogical behavior of
  // of some indexors when asked to home when already at home
  // consequently this jog action will insure that we will be in
  // the datum state when we issue the home command
  if( !homecmd || hdif > 0.01 ) {
    // no need to jiggle it (i.e. this is not a home req. and/or we are not at home...)
    jogcmd = "";
    return false;
  }
  // to insure that homing subsequent to this jog takes the shorter route,
  // always jog-step in the opposite direction of the following home, syntax of
  // homing motor A at speed 100 in direction 0: "AF100 0" or "AF100"
  // for direction 1: "AF100 1"
  // according to manual if indexor is in non-datum state (home switch low)
  // home dir == 0 motor will move in negative direction so we must jog in
  // positive (+) direction. conversely if dir == 1, jog inneg. (-) dirction:
  const char* cc = cmd.c_str();
  const char* sp0 = ::index(cc, ' ');
  const char* spr = ::rindex(cc, ' ');
  char* dir = 0;
  if( spr > sp0 )
    dir = ::rindex(spr, '1');

  if( dir ) { // 1
    jogcmd = motor;
    jogcmd += "-5";
  }
  else { // 0
    jogcmd = motor;
    jogcmd += "+5";
  }
  return true;
}

void UFPortescapConfig::setIndexorNames(int instrumId, const string& motors) {
  switch( instrumId ) {
  case UFDeviceAgent::Flamingos1:
    if( motors != "" )
      _Motors = motors;
    else
      _Motors = "a b c d e";
    break;
  case UFDeviceAgent::Flamingos2:
    if( motors != "" )
      _Motors = motors;
    else
      _Motors = "a b c d e";
    break;
  case UFDeviceAgent::CanariCam:
    if( motors != "" )
      _Motors = motors;
    else
      _Motors = "A B C D E F G H I J K L";
    break;
  case UFDeviceAgent::TReCS: // default
  default:
    if( motors != "" )
      _Motors = motors;
    else
      _Motors = "A B C D E F G H I";
    break;
  }

  // assuming space separated list of 1 character motor names:
  _Indexors.clear();
  int indxcnt = (1 + _Motors.length()) / 2;
  char* cm = (char*) _Motors.c_str(); // null-terminated c-string
  // nominal hold-run current settings
  string hrcval =  "0 15";
  for( int i = 0; i < indxcnt; ++i, cm += 2 ) {
    *(1+cm) = '\0'; // replace space with null
    string ms = cm;
    *(1+cm) = ' '; // restore space
    _Indexors.push_back(ms);
    _HoldRunC[ms] = hrcval;
    // while we're at it, init/clear nominal motion setting command 'history'
    _HoldRunCcmd[ms] = "";
    _SlewVcmd[ms] = "";
    _AccelDecelcmd[ms] = "";
    _InitVcmd[ms] = "";
    _Settlecmd[ms] = "";
    _Jogcmd[ms] = "";
    _DatumCnt[ms] = 0;
  }
}

// ctors
UFPortescapConfig::UFPortescapConfig(const string& name) : UFDeviceConfig(name) {
  _tsport = -1; 
  _tshost = "";
}

UFPortescapConfig::UFPortescapConfig(const string& name,
				     const string& tshost,
				     int tsport) : UFDeviceConfig(name, tshost, tsport) {}

// Portescap terminator is new-line (default):
//string UFPortescapConfig::terminator() { return "\n"; }

// Portescap prefix is none (default):
//string UFPortescapConfig::prefix() { return ""; }

// support setting party-line or direct-line indexor conf, may rely on _tsport being set in agent options:
int UFPortescapConfig::parsePorts(int& tsport, map< string, int >& portmap) {
  if( _Indexors.empty() ) {
    clog<<"UFPortescapConfig::parsePorts> empty indexor name list..."<<endl;
    return 0;
  }
  for( size_t i = 0; i <_Indexors.size(); ++i ) {
    string idxr = _Indexors[i].substr(0, 1);
    if( _Indexors[i].find(":") != string::npos ) {
      string sprt = _Indexors[i].substr(2);
      const char* cs = sprt.c_str();
      if( isdigit(*cs) ) { // this must be the first or only port #
        portmap[idxr] = ::atoi(cs);
	if( i == 0 ) tsport = portmap[idxr]; // force the first on the list?
      }
      else {
         portmap[idxr] = tsport + i;
      }
      // and truncate indexor name to eliminate port $
      _Indexors[i] = _Indexors[i].substr(0, 1);
    }
    else { // assume first as been provided in options?
      portmap[idxr] = tsport + i;
    }
  }
  /*
  map< string, int >::iterator it = portmap.begin();
  while( it != portmap.end() ) {
    clog<<"UFPortescapConfig::parsePorts> "<<it->first<<" "<<it->second<<endl;
    ++it;
  }
  */
  return (int) portmap.size();
}

// these are wrappers for the UFPortescapTermServ funcs:
int UFPortescapConfig::parsePorts(const string indexorports, map< string, int >& portmap, const string& start) {
  int np = UFPortescapTermServ::parsePorts(indexorports, portmap, start);
  if( np <= 0 )
    return np;
  /*
  map< string, int >::iterator it = portmap.begin();
  while( it != portmap.end() ) {
    clog<<"UFPortescapConfig::parsePorts> "<<it->first<<" "<<it->second<<endl;
    ++it;
  }
  */
  return np;
}

// wrapper... if parsePorts return 1, assume party-line:
int UFPortescapConfig::partyLine(int port) {
  return UFPortescapTermServ::partyLine(port, _Indexors);
}

// wrapper...if multiple ports are indicated, assume direct lines:
int UFPortescapConfig::directLines(const map< string, int >& portmap) {
  return UFPortescapTermServ::directLines(portmap);
}

// connect calles create which then connects
UFTermServ* UFPortescapConfig::connect(const string& host, int port) {
  // if already connected, just return...
  if( _devIO == 0 ) {
    if( host != "" ) _tshost = host;
    if( port > 0 ) _tsport = port;
    // use portescap specilized termserv class
    _devIO = UFPortescapTermServ::create(_tshost, _tsport);
  }
  if( _devIO == 0 ) {
    clog<<"UFPortescapConfig::connect> no connection to terminal server..."<<endl;
    return 0;
  }
  string eot = terminator();
  _devIO->resetEOTR(eot);
  _devIO->resetEOTS(eot);
  string sot = prefix();
  _devIO->resetSOTS(sot);
  return _devIO;
}

// get the current position of each indexor
// and generate a status report
UFStrings* UFPortescapConfig::status(UFDeviceAgent* da) {
  char **names, **posnames;
  int cnt = mechNamesTReCS(&names, &posnames);
  cnt = (int) _Indexors.size(); // since we may not be using all motors
  vector < string > list, comments;
  string indexor, name;
  for( int i = 0; i < cnt; ++i ) {
    indexor = _Indexors[i];
    name = names[i];
    comments.push_back(name);
    double steps;
    da->getValue(indexor, steps);
    char* nearest = posNameNearTReCS(name.c_str(), steps);
    strstream s;
    s<<name<< " ("<<indexor<<")"<<" == "<<steps<<", "<<nearest; 
    s<<"@"<<stepsFromHomeTReCS(name.c_str(), nearest); 
    s<<ends;
    list.push_back(s.str()); delete s.str();
  }
  return new UFStrings(da->name(), list);
}

UFStrings* UFPortescapConfig::statusFITS(UFDeviceAgent* da) {
  char **names, **posnames;
  int cnt= 0;
  char* (*posName)(const char*, double);
  double (*stepsFromHome)(const char*, char*);
  switch( da->instrumentId() ) {
    case UFDeviceAgent::Flamingos1: cnt = mechNamesFlam1(&names);
                                    posName = posNameNearFlam1;
                                    stepsFromHome = stepsFromHomeFlam1;
      break;
    case UFDeviceAgent::TReCS: // default
    default: cnt = mechNamesTReCS(&names, &posnames);
             posName = posNameNearTReCS;
             stepsFromHome = stepsFromHomeTReCS;
      break;
  }  
  cnt = (int) _Indexors.size(); // since we may not be using all motors
  map < string, string > valhash, comments;
  string indexor;
  for( int i = 0; i < cnt; ++i ) {
    indexor = _Indexors[i];
    string name = names[i];
    string posname = posnames[i];
    if( _verbose )
      clog<<"UFPortescapConfig> name, posname: "<<name<<", "<<posname<<endl;
    comments[name] = name + " (" + indexor + ") ";
    comments[posname] = posname + " (" + indexor + ") ";
    double steps;
    da->getValue(indexor, steps); // current step count of this indexor
    char* nearest = posName(name.c_str(), steps); // named position nearest this
    double namesteps = stepsFromHome(name.c_str(), nearest);
    double diff = steps - namesteps;
    strstream s;
    s<<nearest; // nearest named position
    if( diff < -0.01 )
      s<<" - "<< fabs(diff); 
    else if( diff > 0.01 )
      s<<" + "<< diff;

    s<<ends;
    string val = s.str();  delete s.str();
    comments[name] += val;
    comments[posname] = "Steps from Datum";
    valhash[name] = val;
    strstream ss;
    ss<<steps<<ends; 
    valhash[posname] = ss.str(); delete ss.str();
  }
  UFStrings* ufs = UFFITSheader::asStrings(valhash, comments);
  ufs->rename(da->name());

  return ufs;
}

// if the command is not a motion settings cmd, always return true
// check existing setting(s) (i.e. previous accepted command setting(s))
// and if the new request differs, return true; otherwise return false
bool UFPortescapConfig::newSettingCmd(const string& indexor, const string cmdimpl) {
  string setting;
  if( cmdimpl.rfind("B") != string::npos && cmdimpl.rfind("B") != 0 ) { // jog
    setting = _Jogcmd[indexor];
    if( setting == cmdimpl )
      return false;
    else
      _Jogcmd[indexor] = setting;
  }
  if( cmdimpl.rfind("E") != string::npos && cmdimpl.rfind("E") != 0 ) { // settle
    setting = _Settlecmd[indexor];
    if( setting == cmdimpl )
      return false;
    else
      _Settlecmd[indexor] = setting;
  }
  if( cmdimpl.rfind("I") != string::npos && cmdimpl.rfind("I") != 0 ) { // initial vel.
    setting = _InitVcmd[indexor];
    if( setting == cmdimpl )
      return false;
    else
      _InitVcmd[indexor] = setting;
  }
  if( cmdimpl.rfind("K") != string::npos && cmdimpl.rfind("K") != 0 ) { // accel/decl. 
    setting = _AccelDecelcmd[indexor];
    if( setting == cmdimpl )
      return false;
    else
      _AccelDecelcmd[indexor] = setting;
  }  
  if(cmdimpl.rfind("V") != string::npos && cmdimpl.rfind("V") != 0 ) { // slew
    setting = _SlewVcmd[indexor];
    if( setting == cmdimpl )
      return false;
    else
      _SlewVcmd[indexor] = setting;
  }
  if( cmdimpl.rfind("Y") != string::npos && cmdimpl.rfind("Y") != 0 ) { // hold & run currents
    setting = _HoldRunCcmd[indexor];
    if( setting == cmdimpl )
      return false;
    else
      _HoldRunCcmd[indexor] = setting;
  }
  return true;
}

vector< string >& UFPortescapConfig::actionCmds() {
  if( _actions.size() > 0 ) // already set
    return _actions;

  _actions.push_back("@"); // abort/stop
  _actions.push_back("["); // status
  _actions.push_back("+"); // + step
  _actions.push_back("-"); // - step
  _actions.push_back("B"); // jog speed
  _actions.push_back("E"); // settle time
  _actions.push_back("F"); // home
  _actions.push_back("M"); // continous (infinite) step
  _actions.push_back("N"); // near-home (our pseudo/bogus virtual home kludge)
  _actions.push_back("o"); // origin
  _actions.push_back("O"); // origin
  _actions.push_back("I"); // set initial velocity
  _actions.push_back("K"); // set accel/decel.
  _actions.push_back("S"); // save indexor settings to non-volatile mem.
  _actions.push_back("V"); // set slew velocity
  _actions.push_back("Y"); // set hold & run current

  return _actions;
}

vector< string >& UFPortescapConfig::queryCmds() {
  if( _queries.size() > 0 ) // already set
    return _queries;

  _queries.push_back("Z"); // query position
  _queries.push_back("^"); // query motion

  return _queries;
}


int UFPortescapConfig::validCmd(const string& c) {
  if( c.length() <= 1 )
   return -1; // indexor ID must precede cmd

  if( c.length() > 12 ) { // cmd too long
    clog<<"UFPortescapConfig::validCmd> cmd too long: "<<c<<endl;
    return -1; 
  }

  const char* cc = c.c_str();
  while( *cc == ' ' || *cc == '\t' ) 
    ++cc; // ignore leading white spaces

  // first check if indexor/motor name is present
  int i;
  string motname;
  for( i = 0; i < (int)_Indexors.size(); ++i ) {
    motname = _Indexors[i];
    const char* mcc = motname.c_str();
    //clog<<"UFPortescapConfig::validCmd> motor: "<<motname<<", c: "<<c<<endl;
    if( *mcc == *cc )
      break; // and check value of i
  }

  if( i >= (int)_Indexors.size() )  { // name not found...
    clog<<"UFPortescapConfig::validCmd> bad indexor name: "<<cc<<endl;
    return -1;
  }

  vector< string >& av = actionCmds();
  // portescaps always echo cmd back along with optional reply info.
  i = 0;
  int cnt = (int)av.size();
  string mc = cc; // motor cmd stripped of any leading white-space
  while( i < cnt ) {
    //if( _verbose )
     // clog<<"UFPortescapConfig::validCmd> av: "<<av[i]<<", mc: "<<mc<<endl;
    if( mc.find(av[i++]) != string::npos ) {
      size_t hrc = mc.find("Y"); // hold-run-current cmd?
      if( hrc != string::npos && hrc > 0 ) { // save this
	if( _verbose )
	  clog<<"UFPortescapConfig::validCmd> old _HoldRunC: "<<_HoldRunC[motname]<<endl;
	_HoldRunC[motname] = mc.substr(hrc);
	if( _verbose )
	  clog<<"UFPortescapConfig::validCmd> reset _HoldRunC: "<<_HoldRunC[motname]<<endl;
      }
      return 1; 
    }
  }
  vector< string >& qv = queryCmds();
  i= 0; cnt = (int)qv.size();
  while( i < cnt ) {
    //if( _verbose )
     // clog<<"UFPortescapConfig::validCmd> qv: "<<qv[i]<<", mc: "<<mc<<endl;
    if( mc.find(qv[i++]) != string::npos )
      return 1;
  }
  
  clog<<"UFPortescapConfig::validCmd> cmd unknown: "<<c<<endl; 
  return -1;
}

#endif // __UFPortescapConfig_cc__

#include "stdio.h"
#include "ufmech.h"

int main() {
  int m,n;
  UFMech *TheMech = getTheUFMech("ufmech.txt");
  for( m = 0; m < TheMech->mechCnt; ++m ) {
    printf("%s\n", TheMech->mech[m].name);
    for ( n = 0; n < TheMech->mech[m].posCnt; n++)
    printf("%f %s\n", TheMech->mech[m].steps[n],TheMech->mech[m].positions[n]); 
  }
  TheMech = getTheUFMech("uftrecsmech.txt");
  for( m = 0; m < TheMech->mechCnt; ++m ) {
    printf("%s\n", TheMech->mech[m].name);
    for (n = 0; n < TheMech->mech[m].posCnt; n++)
      printf("%f %s\n", TheMech->mech[m].steps[n],TheMech->mech[m].positions[n]);
      }
  return 0;
}

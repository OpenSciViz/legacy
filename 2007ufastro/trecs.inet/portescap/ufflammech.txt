;$Name:  $ $Id: ufflammech.txt 14 2008-06-11 01:49:45Z hon $
;Motor Parameter File
;cc agent host IP number
128.227.184.138
;cc agent port number
52004
;Number of Motors
5
;# IS   TS   A  D HC DC AN HS HD EN       Name
 1 500  500  10 10  0 15  a 500   0 deckrWhl  Decker Wheel
 2 1000 1000 10 10  0 50  b 1000  0 moslitWhl Mos/Slit Wheel
 3 100  100  10 10  0 15  c 100   0 fltrWhl Filter Wheel
 4 100  100  10 10  0 15  d 100   0 lyotWhl Lyot Wheel
 5 100  100  10 10  0 15  e 100   0 grismWhl  Grism Wheel
;
;#  : Motor Number
;IS : Initial Speed
;TS : Terminal Speed
;A  : Acceleration
;D  : Deceleration
;HC : Hold Current
;DC : Drive Current
;AN : Axis Name (one character)
;HS : Homing Speed
;HD : Homing Direction
;EN : Epics Name
;Name : Motor name
;
;Mot_#   #_Positions for Decker Wheel
1  4 deckrWhl
;# Offset  Throughput Name
 1 0     0.00  HOME/DARK
 2 2250  1.00  OPEN/IMAGE
 3 4500  1.00  MOS
 4 6750  1.00  LONG SLIT
;Mot_#   #_Positions for Mos/Slit Wheel
2  18 moslitWhl
;#  Offset  Throughput Name
 1  0     1.00  OPEN/HOME
 2  87336 1.00  6PIX/A
 3  83600 1.00  MOS/B
 4  76480 1.00  2PIX/C
 5  69360 1.00  MOS/D
 6  62240 1.00  12PIX/E
 7  55120 1.00  MOS/F
 8  48000 1.00  20PIX/G
 9  40880 1.00  MOS/H
 10 37312 1.00  9PIX/J
 11 33752 1.00  MOS/K
 12 30192 1.00  MOS/L
 13 26632 1.00  MOS/M
 14 23064 1.00  MOS/N
 15 19504 1.00  MOS/O
 16 15944 1.00  MOS/P
 17 12384 1.00  MOS/Q
 18 8356  1.00  3PIX/R
;Mot_#   #_Positions for Filter Wheel
3  6 fltrWhl
;# Offset  Throughput Name
 1 0     0.95  J-BAND/HOME
 2 208   0.95  H-BAND
 3 417   0.95  K-BAND
 4 625   0.95  HK-BAND
 5 833   0.95  JH-BAND
 6 1042  0.00  Ks-BAND
;Mot_#   #_Positions for Lyot Wheel
4  7 lyotWhl
;# Offset  Throughput Name
 1 0     0.00  DARK1
 2 178   0.00  DARK2
 3 357   1.00  OPEN
 4 536   0.95  GEMINI
 5 714   0.95  MMT
 6 893   0.95  4m-KPNO
 7 1071  0.95  2.1m-KPNO
;Mot_#   #_Positions for Grism Wheel
5  5 grismWhl
;# Offset  Throughput Name
 1 0     1.00  Open1 (Home)
 2 250   0.00  DARK1
 3 500   0.00  DARK2
 4 750   1.00  OPEN2
 5 1000  0.95  HK


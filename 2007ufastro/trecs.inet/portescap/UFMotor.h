#ifndef __UFMOTOR_H
#define __UFMOTOR_H "$Id: UFMotor.h,v 1.1 2003/06/02 17:58:29 dan beta $"

#include	<string>
#include	<map>
#include	<iostream>

#include	"UFStringTokenizer.h"
#include	"UFNamedPosition.h"

using std::string ;
using std::map ;
using std::endl ;

class UFMotor
{
public:
	/**
	 * Construct a UFMotor object from the top level table
	 * in the motor parameters file.
	 * In calling this method, the caller guarantees that
	 * the format and number of arguments in the 
	 * UFStringTokenizer is correct.
	 */
	UFMotor( const UFStringTokenizer& st ) ;
	UFMotor( const UFMotor& ) ;
	virtual ~UFMotor() ;

	UFMotor& operator=( const UFMotor& ) ;

	typedef map< unsigned int, UFNamedPosition >
		namedPositionMapType ;
	namedPositionMapType		namedPositionMap ;

	virtual void	addNamedPosition( const UFNamedPosition& ) ;

	unsigned int	motorNumber ;
	int		initialSpeed ;
	int		terminalSpeed ;
	int		acceleration ;
	int		deceleration ;
	int		holdCurrent ;
	int		driveCurrent ;
	char		axisName ;
	int		homingSpeed ;
	int		finalHomingSpeed ;
	char		homingDirection ;
	int		backlash ;
	string		epicsName ; // sectWhl
	string		motorName ; // Sector Wheel

	friend std::ostream& operator<<( std::ostream& out,
		const UFMotor& rhs )
	{
	out	<< rhs.motorNumber << " "
		<< rhs.initialSpeed << " "
		<< rhs.terminalSpeed << " "
		<< rhs.acceleration << " "
		<< rhs.deceleration << " "
		<< rhs.holdCurrent << " "
		<< rhs.driveCurrent << " "
		<< rhs.axisName << " "
		<< rhs.homingSpeed << " "
		<< rhs.finalHomingSpeed << " "
		<< rhs.homingDirection << " "
		<< rhs.backlash << " "
		<< rhs.epicsName << " "
		<< rhs.motorName ;

	if( !rhs.namedPositionMap.empty() )
		{
		out	<< endl
			<< "Named positions: " ;
		}

	for( namedPositionMapType::const_iterator posItr =
		rhs.namedPositionMap.begin() ;
		posItr != rhs.namedPositionMap.end() ;
		++posItr )
		{
		out	<< posItr->second ;
		}

	return out ;
	}
} ;

#endif // __UFMOTOR_H

#if !defined(__FrameAcqServer_cc__)
#define __FrameAcqServer_cc__ "$Name:  $ $Id: FrameAcqServer.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __FrameAcqServer_cc__;

#include "FrameAcqServer.h"    //see comments in FrameAcqServer.h for basic description of program.
#include "FrameServerThread.h"
#include "FrameGrabThread.h"
#include "FrameProcessThread.h"
#include "FrameNotifyThread.h"
#include "FrameSimThread.h"
#include "FrameStreamThread.h"
#include "FrameWriteThread.h"

// The default port on which the server will listen for incoming client connections:
unsigned short int ServerPort = 52000;

// The default number of frames to allocate for EDT-PDV DMA ring buffer:
static int Nbuf_EDT = 7 ;

// Give up waiting for EDT DMA when EDT timeouts >= MaxTimeOuts:
static int MaxTimeOuts = 1;

// In case of write/send error, the max # of frames allowed to wait in write/send List until obs is aborted:
static int MaxFramesWaitToWrite = 600 ;

static bool simulation = false;

// options for catching rare SIGFPE exceptions:
static int sigFPEcnt=0, sigFPEmod=10;

//*****************************************************************************************

void shutdownServer()
{
  // Perform shutdown and exit to OS.
  float sleepTime = 0.2;

  if( FrameGrabAbort("STOP")->name() != "NOOP" ) {
    FrameGrabKill("KILL");
    UFPosixRuntime::sleep( 3.0 ); //give a little time for grabbing/processing to wrap up...
    sleepTime = 1.0;
  }

  reportIOstats(); //method sends iostats to clog.
  clog<<"FrameAcqServer> signaling Worker Threads to terminate..."<<endl;
  FrameGrabShutdown();   UFPosixRuntime::sleep( sleepTime );
  FP_Shutdown();         UFPosixRuntime::sleep( sleepTime );
  FrameNotifyShutdown(); UFPosixRuntime::sleep( sleepTime );
  FrameStreamShutdown(); UFPosixRuntime::sleep( sleepTime );
  FrameWriteShutdown();  UFPosixRuntime::sleep( sleepTime );

  clog << "FrameAcqServer> exiting..."<<endl;
  exit(0);
}
//-------------------------------------------------------------------------
// the FrameAcqServer signal handler used by sigWait() call at end of main():

static void FAS_signal_Handler( int signum )
{
  switch( signum )
    {
    case SIGTERM:
      clog << "FrameAcqServer> caught TERM signal:  shutting down..." <<endl;
      shutdownServer();
      break;
    case SIGKILL:
      clog << "FrameAcqServer> caught KILL signal:  shutting down..." <<endl;
      shutdownServer();
      break;
    case SIGINT:
      clog << "FrameAcqServer> caught INT signal:  ignoring it..." <<endl;
      break;
    case SIGHUP:
      clog << "FrameAcqServer> caught HUP signal:  stopping obs..." <<endl;
      if( FrameGrabAbort("STOP")->name() != "NOOP" )
	FrameGrabKill("KILL");
      break;
    case SIGABRT:
      clog << "FrameAcqServer> caught ABRT signal:  aborting obs..." <<endl;
      if( FrameGrabAbort("ABORT")->name() != "NOOP" )
	FrameGrabKill("KILL");
      break;
    case SIGALRM:
      clog << "FrameAcqServer> caught ALRM signal:  ignoring it..." <<endl;
      break;
    case SIGPIPE:
      clog << "FrameAcqServer> caught PIPE signal:  ignoring it..." <<endl;
      break;
    case SIGFPE:
      {
	if( (sigFPEcnt++ % sigFPEmod) == 0 )
	  clog<<"FrameAcqServer> ignoring signal FPE: "<<sigFPEcnt<<" so far..."<<endl;
	if( sigFPEcnt > 10*sigFPEmod ) sigFPEmod *= 10;
      }
      break;
    default: UFPosixRuntime::defaultSigHandler( signum );
    }
}
//-------------------------------------------------------------------------------
// Output information about how to use this program.

void usage( const string& progName )
{
  cout << endl ;
  cout << "Usage: " << progName << " [options]\n\n" ;
  cout << "-s        : Run in automatic internal frame grab simulation mode.\n" ;
  cout << "-n <secs> : time (seconds) to pause for nods when in simulation mode (default = 7 secs).\n" ;
  cout << "-h        : Print this help menu\n" ;
  cout << "-p <port> : Listen on <port> for connections from clients (default = " << ServerPort << ")\n";
  cout << "-l        : Turn on client request & server reply Logging to files.\n" ;
  cout << "-b <Nbufs>: max # frames in EDT DMA ring buffer (default = " << Nbuf_EDT << ")\n";
  cout << "-d <Touts>: max # timeouts for EDT waiting DMA until ABORT (default = " << MaxTimeOuts << ")\n";
  cout << "-e <Nfrms>: when error: max # frames allowed waiting to write before abort (default = "
       << MaxFramesWaitToWrite << ")\n";
  cout << "-a <host> : hostname of system running device agents (optional, default = localhost)\n";
#ifdef LittleEndian
  cout << "-t <msecs>: time to wait between tries for frame send Lock, in milliseconds (min=10ms)\n";
  cout << "-w <msecs>: max time a FrameServerThread can wait for frame send Lock (min=1000ms)\n";
#endif
  cout << endl ;
}

//******************************* The main method: *******************************
// Start up all the threads and wait for signals.

int main( int argc, char** argv )
{
  int c = EOF ;
  string HostOfAgents = "?";

  // Parse command line options:
  while( (c = ::getopt( argc, argv, "a:b:d:e:hln:p:st:w:" )) != EOF )
    {
      switch( c )
	{
	case 'a':
	  HostOfAgents = optarg ;
	  break ;
	case 'p':
	  ServerPort = static_cast< unsigned short int >( atoi( optarg ) ) ;
	  break ;
	case 'b':
	  Nbuf_EDT = atoi( optarg ) ;
	  break ;
	case 'd':
	  MaxTimeOuts = atoi( optarg ) ;
	  break ;
	case 'e':
	  MaxFramesWaitToWrite = atoi( optarg ) ;
	  break ;
	case 'l':
	  setServerLogs( true );
	  clog << "\n FrameAcqServer> client Log files are enabled.\n" ;
	  break ;
	case 's':
	  simulation = true;
	  clog << "\n FrameAcqServer> automatic internal frame grab simulation is enabled.\n" ;
	  break ;
	case 'n':
	  setNodPause( atof( optarg ) );
	  break ;
#ifdef LittleEndian
	case 't':
	  FP_setLockWaitTimes( atoi( optarg ) ) ;
	  break ;
	case 'w':
	  FP_setLockWaitTimes( 0, atoi( optarg ) ) ;
	  break ;
#endif
	case 'h':
	  usage( argv[0] ) ;
	  return 0 ;
	}
    }

  string Version = string(__FrameAcqServer_cc__);
  if( Version.find("Id:") != string::npos ) Version = Version.substr(Version.find("Id:")+4);
  UFTimeStamp* ServerVersion = new UFTimeStamp( Version );
  clog << "\n" << ServerVersion->name() << endl;
  clog << "\n FrameAcqServer> creating the FrameProcessThread..." <<endl;

  if( !createFrameProcessThread() ) {
    clog << "\n FrameAcqServer> Error creating the FrameProcessThread: "<<strerror(errno)<<endl;
    return 0 ;
  }

  UFPosixRuntime::sleep( 0.2 );
  clog << "\n FrameAcqServer> creating the FrameNotifyThread..." <<endl;

  if( !createFrameNotifyThread() ) {
    clog << "\n FrameAcqServer> Error creating the FrameNotifyThread: "<<strerror(errno)<<endl;
    return 0 ;
  }

  UFPosixRuntime::sleep( 0.2 );
  clog << "\n FrameAcqServer> creating the FrameWriteThread..." <<endl;

  if( !createFrameWriteThread( ServerVersion->name(), MaxFramesWaitToWrite, HostOfAgents ) ) {
    clog << "\n FrameAcqServer> Error creating the FrameWriteThread: "<<strerror(errno)<<endl;
    return 0 ;
  }

  UFPosixRuntime::sleep( 0.2 );
  clog << "\n FrameAcqServer> creating the FrameStreamThread..." <<endl;

  if( !createFrameStreamThread( MaxFramesWaitToWrite ) ) {
    clog << "\n FrameAcqServer> Error creating the FrameStreamThread: "<<strerror(errno)<<endl;
    return 0 ;
  }

  UFPosixRuntime::sleep( 0.2 );
  clog << "\n FrameAcqServer> creating the FrameGrabThread...\n" <<endl;

  if( !createFrameGrabThread( Nbuf_EDT, simulation, MaxTimeOuts ) ) {
    clog << "\n FrameAcqServer> Error creating the FrameGrabThread: "<<strerror(errno)<<endl;
    return 0 ;
  }

  UFPosixRuntime::sleep( 1.0 );
  clog << "\n FrameAcqServer> creating thread to Listen for connections from clients..." <<endl;

  if( !createListenThread( ServerPort, ServerVersion ) ) {
    clog << "FrameAcqServer> Error creating clientListenThread: "<<strerror(errno)<<endl;
    return 0 ;
  }

  UFPosixRuntime::setSignalHandler(FAS_signal_Handler);
  UFPosixRuntime::sigWait();
  return 0 ;
}

#endif /* __FrameAcqServer_cc__ */

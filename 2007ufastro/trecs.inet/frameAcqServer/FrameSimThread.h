#if !defined(__FrameSimThread_h__)
#define __FrameSimThread_h__ "$Name:  $ $Id: FrameSimThread.h,v 1.2 2003/07/11 22:45:55 varosi beta $"

/*
 * When EDT/PDV device cannot be opened the server reverts to simulation mode:
 *   a FrameServerThread recvs UFInts frames and puts pointers in the receivedFramesList,
 *   then accessed by the FrameGrabThread which copies them into the grabbedFramesList,
 *   then accessed by the FrameProcessThread, which moves them into frameBuffers.
 *
 * Frank Varosi, July 9, 2003.
 */

extern void* FrameSimThread( void* ) ;
extern bool createFrameSimThread() ;
extern void setNodPause( float pauseTime );

#endif /* __FrameSimThread_h__ */

#if !defined(__FrameGrabThread_cc__)
#define __FrameGrabThread_cc__ "$Name:  $ $Id: FrameGrabThread.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __FrameGrabThread_cc__;

#include "FrameAcqServer.h"
#include "FrameGrabThread.h"
#include "FrameProcessThread.h"
#include "FrameWriteThread.h"
#include "FrameSimThread.h"
#include "FrameStreamThread.h"

extern "C" {
#include "edtinc.h"  // includes libedt.h and libpdv.h
}

/****************************************************************************
 * The thread ID and blocking mutex for starting/pausing the FrameGrabThread:
 */
pthread_t FrameGrabThreadID = 0 ;
pthread_mutex_t _FrameGrabMutex = PTHREAD_MUTEX_INITIALIZER ;
static bool _keepRunning = true ;

//--------------------- pointer to EDT-PDV device info structure:
PdvDev* _EDTpdv_dev = 0 ;
pthread_mutex_t _EDTdevMutex = PTHREAD_MUTEX_INITIALIZER ;
int Nbuf_EDTpdv = 4 ;    // default # of DMA buffers in EDT-PDV ring buffer.
int maxDMALag=0;         // track max Lag between grabbing and DMA count.
int timeout_EDTpdv = 0 ;
int _maxTimeOuts = 1;    // max # of EDT timeouts during an observation until an ABORT is done.
int NobsTimeOuts = 0 ;   // counters to track history of all events related to observations.
int NobsError = 0 ;
int NobsAbort = 0 ;
int NobsComp = 0 ;
int NframesGrabbed = 0;
bool _EDTdevExists = false;
string stopDueToError = ""; //message if abort/stop is due to grabbing error
bool _abortDHS = false;  //normally just send STOP to DHS, unless ABORT is requested.

UFTimeStamp* _CameraType = 0;
pthread_mutex_t _CameraTypeMutex = PTHREAD_MUTEX_INITIALIZER ;

/*
 * List used to store pointers to frames that are copied from EDT/DMA buffers by FrameGrabThread,
 *  and then later accessed by the FrameProcessThread:
 */
frameListType _grabbedFramesList ;
pthread_mutex_t _grabbedFramesListMutex = PTHREAD_MUTEX_INITIALIZER ;

// This mutex is just for simulation mode of FrameGrabThread,
// unlocked when a UFInts frame is received from a client:
pthread_mutex_t _FrameRecvMutex = PTHREAD_MUTEX_INITIALIZER ;

// The List used to store frames that are created by method FrameGrabSimulate() or received by a client
// FrameServerThread, and then copied by FrameGrabThread when in simulation mode (if no EDT device).
frameListType _receivedFramesList ;
pthread_mutex_t _receivedFramesListMutex ;

/*
 * If set to true, by invoking delayProcessing(), the FrameGrabThread goes into "fastmode":
 *  the FrameProcessThread is not signaled to execute until all frames are grabbed.
 * Otherwise (if false, the default) the FrameProcessThread executes along with the FrameGrabThread.
 * After all frames are grabbed it is always set back to false for next observation.
 */
bool _FPwaitGrab = false;

/*
 * Internal simulation mode: if set to true then the EDT device is ignored
 * and FrameGrabStart creates FrameSimThread every time, which calls FrameGrabSimulate().
 */
bool internSimulation = false;

//---------------------------------------------------------------------------------
// pixel index order mapping and mutex (values are received from a client):

UFInts* _PixelMap = 0;
UFInts* _mapS1 = 0;      //pixel maps for 4-sample detector readout modes.
UFInts* _mapR1 = 0;
UFInts* _mapR2 = 0;
UFInts* _mapR3 = 0;
UFInts* _mapS1OC = 0;    //OC = Output Clamp Ref 2-sample readout mode pixel maps.
UFInts* _mapR1OC = 0;
UFInts* _mapS1CR = 0;    //CR = Column clamp Ref 2-sample readout mode pixel maps.
UFInts* _mapR1CR = 0;
pthread_mutex_t _PixelMapMutex = PTHREAD_MUTEX_INITIALIZER ;

UFFrameConfig* _EDTconfig = 0;  //to store current frame config of EDT device.
string _ReadOutMode;

//***********************************************************************************

bool createFrameGrabThread( int Nbuf, bool simulate, int MaxTimeOuts)
{
  if( Nbuf > 1 ) Nbuf_EDTpdv = Nbuf;
  clog <<"FrameGrabThread> will hold up to " << Nbuf_EDTpdv
       <<" frames in EDT-PDV DMA ring buffer pipeline\n";

  if( MaxTimeOuts > 0 ) _maxTimeOuts = MaxTimeOuts;
  clog <<"FrameGrabThread> will ABORT after " << _maxTimeOuts
       <<" timeouts of EDT-PDV wait for DMA\n";

  if( simulate ) internSimulation = simulate;

  if( ( FrameGrabThreadID = UFPosixRuntime::newThread( FrameGrabThread ) ) > 0 )
    return true;
  else
    return false;
}
//---------------------------------------------------------------------------------

void* FrameGrabThread( void* )
{
  // This method copies the frames from the EDT ring buffers
  // and stores them in the grabbedFramesList, for FrameProcessThread.
  // Methods for controlling FrameGrabThread are (see code below):
  //      FrameGrabStart(), FrameGrabAbort(), and FrameGrabKill().
  // If there is no EDT device, external simulation mode is assumed and,
  // the frames are grabbed from the receivedFramesList,
  // assuming a client is sending a sequence of simulated frames.

  _EDTconfig = new UFFrameConfig( 0, 0, 0 ) ;
  _CameraType = new UFTimeStamp("unknown");
  checkEDTdevFrameConf();
  // start loop over observations (an obs is defined as grabbing a configured # of frames):

  while( _keepRunning )
    {
      ::pthread_mutex_lock( &_FrameGrabMutex );   //This double mutex block is
      ::pthread_mutex_lock( &_FrameGrabMutex );   // unlocked by FrameGrabStart.
      ::pthread_mutex_unlock( &_FrameGrabMutex ); //Unlock 2nd mutex lock to reset.
      if( !_keepRunning ) break;

      // Pointers to FrameConfig and ObsConfig objects are obtained by invoking
      // methods accessFrameConfig() and accessObsConfig(), which also lock respective mutexes.
      // To avoid deadlock if all 4 are needed, order of access (locking) should always be:
      //  PixelMap, CameraType, ObsConfig, then FrameConfig, and unlocking in reverse order.
      UFFrameConfig* theFrameConfig = 0;
      ::pthread_mutex_lock( &_PixelMapMutex );  //unlocked after frame grab loop is done.
      ::pthread_mutex_lock( &_CameraTypeMutex ); //this is unlocked before loop over frame grabs.
      //Lock the ObsConfig so it cannot be accessed or changed while setting up to grab frames
      UFObsConfig* theObsConfig = accessObsConfig( true );

      _ReadOutMode = theObsConfig->readoutMode();
      UFStrings::upperCase( _ReadOutMode );
      int EDTwidthFactor = 1;
      if( _ReadOutMode.find("S1R3") != string::npos ||
	  _ReadOutMode.find("1S3R") != string::npos ) EDTwidthFactor = 4;
      if( _ReadOutMode.find("S1R1") != string::npos ||
	  _ReadOutMode.find("1S1R") != string::npos ) EDTwidthFactor = 2;

      clearGrabbedList();
      removeOldFrames( 0 );  //delete ALL frames from processed named frame buffers.
      ResetCounters();       //reset FrameConfig etc. counters before setting FrameObsSeqNo=0.
      stopDueToError = "";
      string error="FrameGrabThread> no errors", status="";

      if( internSimulation ) {
	clog << "\n FrameGrabThread> internal simulation mode, ignoring EDT device..." <<endl;
	_EDTpdv_dev = 0;
      }
      else {
	clog << "\n FrameGrabThread> opening EDT device interface..." <<endl;
	_EDTpdv_dev = ::pdv_open( EDT_INTERFACE, 0 );
      }

      //if there is an EDT device then configure it according to ReadOutMode (EDTwidthFactor),
      // set the timeout, and the number of frames in DMA ring buffer:

      if( _EDTpdv_dev > 0 )
	{
	  _EDTdevExists = true;
	  theFrameConfig = accessFrameConfig( true );
	  ::pdv_set_width( _EDTpdv_dev, theFrameConfig->width() * EDTwidthFactor );
	  ::pdv_set_height( _EDTpdv_dev, theFrameConfig->height() );
	  ::pdv_set_depth( _EDTpdv_dev, theFrameConfig->depth() ) ;
	  theFrameConfig = accessFrameConfig( false );
	  ::edt_flush_fifo(_EDTpdv_dev);

	  if( ::pdv_set_timeout( _EDTpdv_dev, timeout_EDTpdv ) == -1 )
	    {
	      error = "ERROR: FrameGrabThread> Failed to set timeout for EDT-PDV : ";
	      clog << error << timeout_EDTpdv << endl;
	      theFrameConfig = accessFrameConfig( true );
	      theFrameConfig->rename( error );
	      theFrameConfig = accessFrameConfig( false );
	      continue;
	    }
	  else clog << "FrameGrabThread> set EDT-PDV timeout = " << timeout_EDTpdv <<" msec"<<endl;

	  if( ::pdv_multibuf( _EDTpdv_dev, Nbuf_EDTpdv ) == -1 )
	    {
	      error = "ERROR: FrameGrabThread> Failed to allocate EDT-PDV ring buffers: ";
	      clog << error << Nbuf_EDTpdv << endl;
	      theFrameConfig = accessFrameConfig( true );
	      theFrameConfig->rename( error );
	      theFrameConfig = accessFrameConfig( false );
	      continue;
	    }
	  else  clog <<"FrameGrabThread> Allocated " << Nbuf_EDTpdv
		     <<" buffers for EDT-PDV DMA ring buffer pipeline\n";

	  _EDTconfig->setWidth( ::pdv_get_width(_EDTpdv_dev) ) ;
	  _EDTconfig->setHeight( ::pdv_get_height(_EDTpdv_dev) ) ;
	  _EDTconfig->setDepth( ::pdv_get_depth(_EDTpdv_dev) ) ;
	  _EDTconfig->rename( ::pdv_get_cameratype(_EDTpdv_dev) ) ;
	  _CameraType->rename( _EDTconfig->name() ) ;
	}
      else {
	_EDTdevExists = false;
	theFrameConfig = accessFrameConfig( true );
	_EDTconfig->setWidth( theFrameConfig->width() * EDTwidthFactor );
	_EDTconfig->setHeight( theFrameConfig->height() );
	_EDTconfig->setDepth( theFrameConfig->depth() );
	theFrameConfig = accessFrameConfig( false );
	_CameraType->rename("MCE_SIM") ;
	_EDTconfig->rename( _CameraType->name() );
	if( !internSimulation )
	  clog<<"FrameGrabThread> Failed to open EDT device! Assuming external simulation mode...\n";
      }

      clog << "FrameGrabThread> EDT frame Config:"
	   << "  Width=" << _EDTconfig->width()
	   << ", Height=" << _EDTconfig->height()
	   << ", Depth=" << _EDTconfig->depth()
	   << ", CameraType=" << _CameraType->name()
	   << ", ReadOutMode=" << _ReadOutMode << endl;

      if( _PixelMap )
	clog<<"FrameGrabThread> pixel mapping done by frame process thread using "<<_PixelMap->name()<<endl;

      theFrameConfig = accessFrameConfig( true );
      theFrameConfig->setFrameObsSeqNo( 0 );      //FrameObsSeqNo=0 indicates that obs. is starting.
      status = "FrameGrabThread> Observation in Progress: " + _CameraType->name();
      theFrameConfig->rename( status );
      theFrameConfig->stampTime( UFRuntime::currentTime() );
      clog << "FrameGrabThread> " << theFrameConfig->timeStamp() <<"\n";
      theFrameConfig = accessFrameConfig( false );
      int chopBeams = theObsConfig->chopBeams();
      theObsConfig = accessObsConfig( false );      //can now unlock this since obs is starting...
      string fsrc = _CameraType->name();
      _CameraType->stampTime( UFRuntime::currentTime() ); //this records approx. obs. start time.
      ::pthread_mutex_unlock( &_CameraTypeMutex );

      int totNumFrames = totalFramesToGrab(); //note this call also temporarily locks theFrameConfig.
      int NelemFrame = _EDTconfig->width() * _EDTconfig->height();
      clog << "FrameGrabThread> starting grab of " << totNumFrames
	   << " frames, each with " << NelemFrame << " integers, from Camera: " << fsrc <<endl;

      if( _EDTpdv_dev > 0 ) ::pdv_start_images( _EDTpdv_dev, 0 );
      int nframe=0;
      int DMAcnt=0;
      int DMALag=0;
      maxDMALag=0;
      // FrameGrabAbort will signal abort by setting totalFramesToGrab to zero,
      // therefore, must use method totalFramesToGrab() because value may change.
      // Also check nframe MOD chopBeams to make sure even # of frames are grabbed when chopping.

      while( nframe < totalFramesToGrab() || (nframe % chopBeams) != 0 ) //frame grab loop
	{
	  UFInts* grabbedFrame = 0;
	  UFInts* receivedFrame = 0;
	  int* edt_frame = 0;

	  if( _EDTpdv_dev > 0 )
	    {
	      edt_frame = (int*)::pdv_wait_image( _EDTpdv_dev );
	      DMAcnt = ::edt_done_count( _EDTpdv_dev );
	      theFrameConfig = accessFrameConfig( true );
	      theFrameConfig->stampTime( UFRuntime::currentTime() );
	      theFrameConfig->setDMACnt( DMAcnt );
	      if( edt_frame ) theFrameConfig->frameGrabCnt( ++nframe );
	      theFrameConfig = accessFrameConfig( false );
	      DMALag = DMAcnt - nframe;
	      maxDMALag = max( DMALag, maxDMALag );

	      if( DMALag >= Nbuf_EDTpdv )
		{
		  error = "ERROR: FrameGrabThread> EDT ring buffer overrun ***";
		  theFrameConfig = accessFrameConfig( true );
		  theFrameConfig->frameGrabCnt( --nframe );
		  theFrameConfig = accessFrameConfig( false );
		  break;
		}
	    }
	  else //operate in simulation mode grabbing sequence of frames recvd from a client:
	    {
	      ::pthread_mutex_lock( &_FrameRecvMutex );

	      if( recvdListEmpty() )
		{
		  ::pthread_mutex_lock( &_FrameRecvMutex );  //blocking mutex lock.
		  // This second attempt to lock will BLOCK and WAIT.
		  // It will be unlocked when the system is shutting down or when a
		  // new frame is recieved by handleRequest( UFInts* receivedFrame ).
		}

	      ::pthread_mutex_unlock( &_FrameRecvMutex ); //unlock 1st or 2nd lock.

	      if( ! recvdListEmpty() )
		{
		  ::pthread_mutex_lock( &_receivedFramesListMutex );
		  receivedFrame = _receivedFramesList.back();    //got new frame.
		  _receivedFramesList.pop_back();
		  ::pthread_mutex_unlock( &_receivedFramesListMutex );

		  NelemFrame = receivedFrame->elements();
		  edt_frame = receivedFrame->valInts();

		  theFrameConfig = accessFrameConfig( true );
		  theFrameConfig->stampTime( UFRuntime::currentTime() );
		  theFrameConfig->frameGrabCnt( ++nframe );
		  theFrameConfig = accessFrameConfig( false );
		}
	      else clog <<"NOOP: FrameGrabThread> receivedFrameList (for sim) is EMTPY."<<endl;
	    }

	  if( edt_frame )  //copy EDT DMA-buffer values into grabbed list:
	    {
	      grabbedFrame = new (nothrow) UFInts( fsrc, edt_frame, NelemFrame );

	      if( ! UFProtocol::validElemCnt( grabbedFrame ) ) {
		error = "ERROR: FrameGrabThread> EDT frame copy alloc. failed! ***";
		theFrameConfig = accessFrameConfig( true );
		theFrameConfig->frameGrabCnt( --nframe );
		theFrameConfig = accessFrameConfig( false );
		break;
	      }

	      grabbedFrame->setSeq( nframe, totNumFrames );
	      ++NframesGrabbed; //this is total counter thru all observations for process history.

	      if( _FPwaitGrab )
		_grabbedFramesList.push_front( grabbedFrame );
	      else {
		::pthread_mutex_lock( &_grabbedFramesListMutex );
		_grabbedFramesList.push_front( grabbedFrame );
		::pthread_mutex_unlock( &_grabbedFramesListMutex );
		FP_Start();     //signal FrameProcessThread.
	      }

	      if( _EDTpdv_dev > 0 ) {
		int Ntimeouts = ::pdv_timeouts( _EDTpdv_dev );
		if( Ntimeouts > 0 )
		  {
		    if( Ntimeouts >= _maxTimeOuts ) { //break and abort...
		      ++NobsTimeOuts;
		      if( Ntimeouts > 1 )
			error = "ERROR: FrameGrabThread> EDT timeouts waiting for DMA = "
			  + UFRuntime::numToStr( Ntimeouts );
		      else
			error = "ERROR: FrameGrabThread> EDT timeout waiting for DMA.";
		      break;
		    }
		    else {
		      error = "WARNING: FrameGrabThread> " + UFRuntime::numToStr( Ntimeouts )
			+ " EDT timeouts waiting for DMA..."
			+ UFRuntime::numToStr( pdv_timeout_cleanup(_EDTpdv_dev) ) + " pending frames?";
		      clog << error << endl;
		      theFrameConfig = accessFrameConfig( true );
		      theFrameConfig->rename( error );
		      theFrameConfig = accessFrameConfig( false );
		    }
		  }
	      }
	    }
	  else {
	    if( _EDTpdv_dev > 0 )
	      error = "ERROR: FrameGrabThread> edt_frame pointer is NULL!";
	    else
	      clog << "NOOP: FrameGrabThread> sim edt_frame pointer is NULL!" <<endl;
	    break;
	  }

	  clog << "---------------------------------------------"
	       << "FrameGrabThread> nframe=" << nframe
	       << ",  DMAcnt=" << DMAcnt
	       << ",  DMALag=" << DMALag <<". \r";

	  if( receivedFrame != 0 ) delete receivedFrame;

	} // end of grab loop.

      if( _EDTpdv_dev > 0 )
	{
	  DMAcnt = ::edt_done_count( _EDTpdv_dev );
	  DMALag = DMAcnt - nframe;
	  maxDMALag = max( DMALag, maxDMALag );
	  theFrameConfig = accessFrameConfig( true );
	  theFrameConfig->setDMACnt( DMAcnt );
	  theFrameConfig = accessFrameConfig( false );
	  clog << "\nFrameGrabThread> closing EDT device interface..." <<endl;
	  ::pthread_mutex_lock( &_EDTdevMutex ); //to make sure device is not accessed:
	  ::pdv_close( _EDTpdv_dev );
	  _EDTpdv_dev = 0;
	  ::pthread_mutex_unlock( &_EDTdevMutex );
	}

      clog<< "\nFrameGrabThread> grabbed " << nframe
	  << " frames,  DMAcnt=" << DMAcnt
	  << ",    max DMA Lag=" << maxDMALag <<endl;
      _FPwaitGrab = false;

      if( ! grabListEmpty() )
	{
	  clog << "FrameGrabThread> " << _grabbedFramesList.size() << " frames still in grabbed List\n";
	  clog << "FrameGrabThread> signaling the FrameProcessThread..." <<endl;
	  FP_Start();
	}

      theObsConfig = accessObsConfig( true ); //always lock theObsConfig before theFrameConfig
      // note that function totalFramesToGrab() locks and unlocks theFrameConfig!

      if( nframe < totalFramesToGrab() )  //grabbing error must have occcured.
	{
	  ++NobsError;
	  clog << error << endl;
	  theFrameConfig = accessFrameConfig( true );
	  theFrameConfig->rename( error );
	  theFrameConfig->setFrameObsSeqTot( -(theObsConfig->totFrameCnt()) ); //indicate abort/stop.
	  clog << "FrameGrabThread> " << theFrameConfig->timeStamp() <<endl;
	  theFrameConfig = accessFrameConfig( false );
	  _abortDHS = false;
	  FrameStreamStart(); //signal the stream thread to detect abort/stop.
	}
      else  //normal end of grabbing:
	{
	  theFrameConfig = accessFrameConfig( true );
	  if( theFrameConfig->frameObsSeqTot() <= 0 ) //indicates abort/stop in progress
	    {
	      if( stopDueToError.find("ERROR") != string::npos ) {
		status = stopDueToError + ": Observation Stopped ***";
		_abortDHS = false;
	      }
	      else status = "FrameGrabThread> Observation Aborted ***";
	      ++NobsAbort;
	      clog << status <<endl;
	      theFrameConfig->rename( status );
	      theFrameConfig->setFrameObsSeqTot(-(theObsConfig->totFrameCnt())); //indicate abort/stop.
	      FrameStreamStart(); //signal the stream thread to detect abort/stop.
	    }
	  else {
	    ++NobsComp;
	    status = "FrameGrabThread> Observation Completed: " + _CameraType->name();
	    clog << status <<endl;
	    theFrameConfig->rename( status );
	  }
	  clog << "FrameGrabThread> " << theFrameConfig->timeStamp() <<endl;
	  theFrameConfig = accessFrameConfig( false );
	}

      theObsConfig = accessObsConfig( false );
      ::pthread_mutex_unlock( &_PixelMapMutex );
    } //end of while(_keepRunning).

  clog << "FrameGrabThread> terminated."<<endl;
  return 0;
}
/**-----------------------------------------------------------------------------------
 * The following methods may be invoked by other threads:
 */

UFTimeStamp* getCameraType()
{
  ::pthread_mutex_lock( &_CameraTypeMutex );
  UFTimeStamp* CT = new (nothrow) UFTimeStamp( _CameraType->name() );
  if( CT != 0 ) CT->stampTime( _CameraType->timeStamp() );
  ::pthread_mutex_unlock( &_CameraTypeMutex );
  return CT;
}
//--------------------------------------------------------------------------

int totalFramesToGrab()
{
  //lock the object since the value may change (to negative) due to ABORT or ERROR.
  UFFrameConfig* theFrameConfig = accessFrameConfig( true );
  int frameObsSeqTot = theFrameConfig->frameObsSeqTot();
  theFrameConfig = accessFrameConfig( false );

  //for multi-sample readout mode each grabbed frame contains 2 or 4 readout frames,
  // so for raw readout display the # of grabbed frames is a fraction of displayed frames.

  if( _ReadOutMode.find("RAW") != string::npos ) {
    if( _ReadOutMode.find("S1R3") != string::npos || _ReadOutMode.find("1S3R") != string::npos )
      return frameObsSeqTot/4;
    if( _ReadOutMode.find("S1R1") != string::npos || _ReadOutMode.find("1S1R") != string::npos )
      return frameObsSeqTot/2;
  }

  return frameObsSeqTot;
}
//----------------------------------------------------------

vector< UFInts* > popGrabbedFrames()
{
  // Method returns vector of readout frames, to handle the multi-sample detector readout modes.
  // First frame in vector is always the detector ON readout (S1).
  // This method is normally called by the FrameProcessThread,
  //  but is part of FrameGrab class because grabbedFramesList and _PixelMap are in this class.
  vector< UFInts* > readoutFrames;

  ::pthread_mutex_lock( &_grabbedFramesListMutex );
  UFInts* grabbedFrame = _grabbedFramesList.back();
  _grabbedFramesList.pop_back();
  ::pthread_mutex_unlock( &_grabbedFramesListMutex );

#ifdef BigEndian  // for EDT PCI card in sparc must reverse byte order, Little to Big endian:
  grabbedFrame->reverseByteOrder();
#endif

  if( _ReadOutMode.find("S1R3") != string::npos || _ReadOutMode.find("1S3R") != string::npos )
    {
      int* S1R3rdout = grabbedFrame->valInts();
      int nPix = grabbedFrame->numVals()/4;
      int* pixMap=0; int* mapS1=0; int* mapR1=0; int* mapR2=0; int* mapR3=0;
      if( _PixelMap && _mapS1 && _mapR1 && _mapR2 && _mapR3 ) {
	pixMap = _PixelMap->valInts();
	nPix = _PixelMap->numVals();
	mapS1 = _mapS1->valInts();
	mapR1 = _mapR1->valInts();
	mapR2 = _mapR2->valInts();
	mapR3 = _mapR3->valInts();
      } else {
	string status = "ERROR: FrameProcessThread:popGrabbedFrame> mapping arrays are not defined!";
	clog << status <<endl;
	UFFrameConfig* theFrameConfig = accessFrameConfig( true );
	theFrameConfig->rename(status);
	theFrameConfig = accessFrameConfig( false );
      }
      //these ctors perform pixel mapping from the int* S1R3rdout to the new int arrays.
      UFInts* S1frame = new (nothrow) UFInts( "S1rdout", S1R3rdout, mapS1, pixMap, nPix );
      UFInts* R1frame = new (nothrow) UFInts( "R1rdout", S1R3rdout, mapR1, pixMap, nPix );
      UFInts* R2frame = new (nothrow) UFInts( "R2rdout", S1R3rdout, mapR2, pixMap, nPix );
      UFInts* R3frame = new (nothrow) UFInts( "R3rdout", S1R3rdout, mapR3, pixMap, nPix );

      if( S1frame && R1frame && R2frame && R3frame ) {
	S1frame->stampTime( grabbedFrame->timeStamp() );
	R1frame->stampTime( grabbedFrame->timeStamp() );
	R2frame->stampTime( grabbedFrame->timeStamp() );
	R3frame->stampTime( grabbedFrame->timeStamp() );
	delete grabbedFrame;
	readoutFrames.push_back( S1frame );
	readoutFrames.push_back( R1frame );
	readoutFrames.push_back( R2frame );
	readoutFrames.push_back( R3frame );
      } else {
	string status = "ERROR: FrameProcessThread:popGrabbedFrame> Failed mem. alloc. for ";
	if( S1frame ) {
	  S1frame->stampTime( grabbedFrame->timeStamp() );
	  status += "R1,R2,R3 frames!";
	  delete grabbedFrame;
	  readoutFrames.push_back( S1frame );
	} else {
	  status += "S1,R1,R2,R3 frames!";
	  readoutFrames.push_back( grabbedFrame );
	}
	clog << status <<endl;
	UFFrameConfig* theFrameConfig = accessFrameConfig( true );
	theFrameConfig->rename(status);
	theFrameConfig = accessFrameConfig( false );
      }
    }
  else if( _ReadOutMode.find("S1R1") != string::npos || _ReadOutMode.find("1S1R") != string::npos )
    {
      int* S1R1rdout = grabbedFrame->valInts();
      int nPix = grabbedFrame->numVals()/2;
      int* pixMap=0; int* mapS1=0; int* mapR1=0;
      if( _PixelMap && _mapS1OC && _mapR1OC && _mapS1CR && _mapR1CR ) {
	pixMap = _PixelMap->valInts();
	nPix = _PixelMap->numVals();
	if( _ReadOutMode.find("CR") != string::npos ) {
	  mapS1 = _mapS1CR->valInts();
	  mapR1 = _mapR1CR->valInts();
	} else {
	  mapS1 = _mapS1OC->valInts();
	  mapR1 = _mapR1OC->valInts();
	}
      } else {
	string status = "ERROR: FrameProcessThread:popGrabbedFrame> mapping arrays are not defined!";
	clog << status <<endl;
	UFFrameConfig* theFrameConfig = accessFrameConfig( true );
	theFrameConfig->rename(status);
	theFrameConfig = accessFrameConfig( false );
      }
      //these ctors perform pixel mapping from the int* S1R1rdout to the new int arrays.
      UFInts* S1frame = new (nothrow) UFInts( "S1rdout", S1R1rdout, mapS1, pixMap, nPix );
      UFInts* R1frame = new (nothrow) UFInts( "R1rdout", S1R1rdout, mapR1, pixMap, nPix );

      if( S1frame && R1frame ) {
	S1frame->stampTime( grabbedFrame->timeStamp() );
	R1frame->stampTime( grabbedFrame->timeStamp() );
	delete grabbedFrame;
	readoutFrames.push_back( S1frame );
	readoutFrames.push_back( R1frame );
      } else {
	string status = "ERROR: FrameProcessThread:popGrabbedFrame> Failed mem. alloc. for ";
	if( S1frame ) {
	  status += "R1 frame!";
	  S1frame->stampTime( grabbedFrame->timeStamp() );
	  delete grabbedFrame;
	  readoutFrames.push_back( S1frame );
	}
	else if( R1frame ) {
	  status += "S1 frame!";
	  R1frame->stampTime( grabbedFrame->timeStamp() );
	  delete grabbedFrame;
	  readoutFrames.push_back( R1frame );
	} else {
	  status += "S1 & R1 frames!";
	  readoutFrames.push_back( grabbedFrame );
	}
	clog << status <<endl;
	UFFrameConfig* theFrameConfig = accessFrameConfig( true );
	theFrameConfig->rename(status);
	theFrameConfig = accessFrameConfig( false );
      }
    }
  else if( _PixelMap ) 	// map pixels from readout to display order for single sampling:
    {
      if( _PixelMap->numVals() == grabbedFrame->numVals() )
	{
	  UFInts* mappedFrame = new (nothrow) UFInts( grabbedFrame->name(),
						      grabbedFrame->valInts(),
						      grabbedFrame->numVals(), false,
						      _PixelMap->valInts() );
	  if( UFProtocol::validElemCnt( mappedFrame ) ) {
	    mappedFrame->stampTime( grabbedFrame->timeStamp() );
	    mappedFrame->setSeq( grabbedFrame->seqCnt(), grabbedFrame->seqTot() );
	    delete grabbedFrame;
	    readoutFrames.push_back( mappedFrame );
	  } else {
	    string status="ERROR: FrameProcessThread:popGrabbedFrame> Failed alloc. mapped frame!";
	    clog << status <<endl;
	    UFFrameConfig* theFrameConfig = accessFrameConfig( true );
	    theFrameConfig->rename(status);
	    theFrameConfig = accessFrameConfig( false );
	    readoutFrames.push_back( grabbedFrame );
	  }
	}
    }
  else readoutFrames.push_back( grabbedFrame );

  return readoutFrames;
}
//----------------------------------------------------------

void clearGrabbedList()
{
  ::pthread_mutex_lock( &_grabbedFramesListMutex );
  if( !_grabbedFramesList.empty() ) {
    clog<<"FrameGrabThread> "<<_grabbedFramesList.size()
	<<" frames still in _grabbedFramesList, deleting them..."<<endl;
    for( frameListType::iterator pf = _grabbedFramesList.begin(); pf != _grabbedFramesList.end(); ++pf )
      delete *pf;
  }
  _grabbedFramesList.clear(); //make sure it is clear
  clog<<"FrameGrabThread> _grabbedFramesList is clear."<<endl;
  ::pthread_mutex_unlock( &_grabbedFramesListMutex );
  ::pthread_mutex_lock( &_receivedFramesListMutex );
  if( !_receivedFramesList.empty() ) {
    clog<<"FrameGrabThread> "<<_receivedFramesList.size()
	<<" frames still in _receivedFramesList, deleting them..."<<endl;
    for( frameListType::iterator pf = _receivedFramesList.begin(); pf != _receivedFramesList.end(); ++pf )
      delete *pf;
    _receivedFramesList.clear();
    clog<<"FrameGrabThread> _receivedFramesList is clear for simulation option."<<endl;
  }
  ::pthread_mutex_unlock( &_receivedFramesListMutex );
}
//---------------------------------------------------------------------------------

bool grabListEmpty()
{
  ::pthread_mutex_lock( &_grabbedFramesListMutex );
  bool empty_status = _grabbedFramesList.empty();
  ::pthread_mutex_unlock( &_grabbedFramesListMutex );
  return empty_status;
}
//----------------------------------------------------------

bool recvdListEmpty()
{
  ::pthread_mutex_lock( &_receivedFramesListMutex );
  bool empty_status = _receivedFramesList.empty();
  ::pthread_mutex_unlock( &_receivedFramesListMutex );
  return empty_status;
}
//----------------------------------------------------------

UFStrings* delayProcessing()
{
  string message, status;

  if( ObsInProgress() || !grabListEmpty() ) {
    status = "ERROR";
    clog << status << ": ";
    message = "FrameGrabThread> cannot change processing sequence during obs.";
  } else {
    _FPwaitGrab = true ;
    status = "FPWAIT";
    message = "FrameAcqServer> FrameProcessThread will wait for FrameGrabThread.";
  }

  clog << message << endl;
  return new (nothrow) UFStrings( status, &message ) ;
}
//----------------------------------------------------------

string definePixelMap( UFInts* pixelMap )
{
  UFFrameConfig* theFrameConfig = accessFrameConfig( true );
  int nPix = theFrameConfig->width() * theFrameConfig->height();
  theFrameConfig = accessFrameConfig( false );

  if( pixelMap->numVals() != nPix )
    return ("ERROR: FrameAcqServer> wrong # elements in Pixel Map array.");

  ::pthread_mutex_lock( &_PixelMapMutex );

  string message; 
  string name = pixelMap->name();
  UFStrings::upperCase( name );

  if( name.find("SRC1") != string::npos || name.find("S1") != string::npos )
    {
      if( name.find("CR") != string::npos ) {
	if( pixelMap->maxVal() < 2*nPix && pixelMap->minVal() >= 0 ) {
	  if( _mapS1CR ) delete _mapS1CR;
	  _mapS1CR = pixelMap;
	  message = "FrameAcqServer> accepted new S1CR Map array for 2-sample readouts.";
	}
	else message = "ERROR: FrameAcqServer> recvd S1CR Map array has values > 2*nPix or < 0 !";
      }
      else if( name.find("OC") != string::npos ) {
	if( pixelMap->maxVal() < 2*nPix && pixelMap->minVal() >= 0 ) {
	  if( _mapS1OC ) delete _mapS1OC;
	  _mapS1OC = pixelMap;
	  message = "FrameAcqServer> accepted new S1OC Map array for 2-sample readouts.";
	}
	else message = "ERROR: FrameAcqServer> recvd S1OC Map array has values > 2*nPix or < 0 !";
      }
      else {
	if( pixelMap->maxVal() < 4*nPix && pixelMap->minVal() >= 0 ) {
	  if( _mapS1 ) delete _mapS1;
	  _mapS1 = pixelMap;
	  message = "FrameAcqServer> accepted new S1 Map array for 4-sample readouts.";
	}
	else message = "ERROR: FrameAcqServer> recvd S1 Map array has values > 4*nPix or < 0 !";
      }
    }
  else if( name.find("REF1") != string::npos || name.find("R1") != string::npos )
    {
      if( name.find("CR") != string::npos ) {
	if( pixelMap->maxVal() < 2*nPix && pixelMap->minVal() >= 0 ) {
	  if( _mapR1CR ) delete _mapR1CR;
	  _mapR1CR = pixelMap;
	  message = "FrameAcqServer> accepted new R1CR Map array for 2-sample readouts.";
	}
	else message = "ERROR: FrameAcqServer> recvd R1CR Map array has values > 2*nPix or < 0 !";
      }
      else if( name.find("OC") != string::npos ) {
	if( pixelMap->maxVal() < 2*nPix && pixelMap->minVal() >= 0 ) {
	  if( _mapR1OC ) delete _mapR1OC;
	  _mapR1OC = pixelMap;
	  message = "FrameAcqServer> accepted new R1OC Map array for 2-sample readouts.";
	}
	else message = "ERROR: FrameAcqServer> recvd R1OC Map array has values > 2*nPix or < 0 !";
      }
      else {
	if( pixelMap->maxVal() < 4*nPix && pixelMap->minVal() >= 0 ) {
	  if( _mapR1 ) delete _mapR1;
	  _mapR1 = pixelMap;
	  message = "FrameAcqServer> accepted new R1 Map array for 4-sample readouts.";
	}
	else message = "ERROR: FrameAcqServer> recvd R1 Map array has values > 4*nPix or < 0 !";
      }
    }
  else if( name.find("REF2") != string::npos || name.find("R2") != string::npos )
    {
      if( pixelMap->maxVal() < 4*nPix && pixelMap->minVal() >= 0 ) {
	if( _mapR2 ) delete _mapR2;
	_mapR2 = pixelMap;
	message = "FrameAcqServer> accepted new R2 Map array for 4-sample readouts.";
      }
      else message = "ERROR: FrameAcqServer> recvd R2 Map array has values > 4*nPix or < 0 !";
    }
  else if( name.find("REF3") != string::npos || name.find("R3") != string::npos )
    {
      if( pixelMap->maxVal() < 4*nPix && pixelMap->minVal() >= 0 ) {
	if( _mapR3 ) delete _mapR3;
	_mapR3 = pixelMap;
	message = "FrameAcqServer> accepted new R3 Map array for 4-sample readouts.";
      }
      else message = "ERROR: FrameAcqServer> recvd R3 Map array has values > 4*nPix or < 0 !";
    }
  else {
    if( pixelMap->maxVal() < nPix && pixelMap->minVal() >= 0 ) {
      if( _PixelMap ) delete _PixelMap;
      _PixelMap = pixelMap;
      theFrameConfig = accessFrameConfig( true );
      theFrameConfig->setPixelSort(1);
      theFrameConfig = accessFrameConfig( false );
      message = "FrameAcqServer> accepted new Pixel Map array.";
    }
    else message = "ERROR: FrameAcqServer> Pixel Map array has values > nPix or < 0 !";
  }

  ::pthread_mutex_unlock( &_PixelMapMutex );
  return message;
}
//----------------------------------------------------------
// helper method for copyPixelMap():

UFInts* _copyPixelMap( UFInts* pMap, string& message )
{
  if( pMap != 0 )
    {                     
      UFInts* pmc = new (nothrow) UFInts( pMap->name(),
					  pMap->valInts(),
					  pMap->numVals() );
      if( pmc == 0 )
	message = "ERROR: FrameAcqServer> Memory allocation failure for " + pMap->name();
      else {
	message = "FrameAcqServer> sending copy of " + pMap->name();
	pmc->stampTime( pMap->timeStamp() );
      }
      return pmc;
    }
  else {
    message = "ERROR: FrameAcqServer> requested pixel map is not yet defined: ";
    return (UFInts*)0;
  }
}
//----------------------------------------------------------

UFInts* copyPixelMap( string& message, string name )
{
  //do not lock pixel maps, not critical to clients (locked during obs by FrameGrabThread)
  UFStrings::upperCase( name );

  if( name.find("SRC1") != string::npos || name.find("S1") != string::npos ) {
    if( name.find("CR") != string::npos )
      return _copyPixelMap( _mapS1CR, message );
    else if( name.find("OC") != string::npos )
      return _copyPixelMap( _mapS1OC, message );
    else
      return _copyPixelMap( _mapS1, message );
  }
  else if( name.find("REF1") != string::npos || name.find("R1") != string::npos ) {
    if( name.find("CR") != string::npos )
      return _copyPixelMap( _mapR1CR, message );
    else if( name.find("OC") != string::npos )
      return _copyPixelMap( _mapR1OC, message );
    else
      return _copyPixelMap( _mapR1, message );
  }
  else if( name.find("REF2") != string::npos || name.find("R2") != string::npos )
    return _copyPixelMap( _mapR2, message );
  else if( name.find("REF3") != string::npos || name.find("R3") != string::npos )
    return _copyPixelMap( _mapR3, message );
  else
    return _copyPixelMap( _PixelMap, message );
}
//----------------------------------------------------------

void FrameGrabSetNbuffers( int Nbuf )
{
  Nbuf_EDTpdv = Nbuf;
  if( Nbuf_EDTpdv < 2 ) Nbuf_EDTpdv = 2;
}
//----------------------------------------------------------

void FrameGrabSetMaxTimeOuts( int maxTouts )
{
  _maxTimeOuts = maxTouts;
  if( _maxTimeOuts < 1 ) _maxTimeOuts = 1;
}
//----------------------------------------------------------

int FrameGrabMaxDMALag() { return maxDMALag; }
int FrameGrabNbuffers() { return Nbuf_EDTpdv; }
int FrameGrabNframes() { return NframesGrabbed; }
int FrameGrabNobsError() { return NobsError; }
int FrameGrabNobsAbort() { return NobsAbort; }
int FrameGrabNobsComp() { return NobsComp; }
int FrameGrabTimeOuts() { return NobsTimeOuts; }
int FrameGrabMaxTimeOuts() { return _maxTimeOuts; }
bool abortDHS() { return _abortDHS; }
bool EDTdeviceExists() { return _EDTdevExists; }
//----------------------------------------------------------

void checkEDTdevFrameConf( string Caller )
{
  if( internSimulation ) {
    clog << Caller + "> internal simulation mode: ignoring EDT device."<<endl;
    return;
  }

  // Try to open the EDT-PDV device and then store EDT config parameters:
  ::pthread_mutex_lock( &_EDTdevMutex );

  if( (_EDTpdv_dev = ::pdv_open( EDT_INTERFACE, 0 )) == 0 )
    {
      _EDTdevExists = false;
      clog << Caller + "> pdv_open(" << EDT_INTERFACE << "0) failed!"<<endl;
      _CameraType->rename("MCE_SIM") ;
      clog << Caller + "> assuming external simulation mode.";
    }
  else { // get image size and depth and store.
    _EDTdevExists = true;
    _EDTconfig->setWidth( ::pdv_get_width(_EDTpdv_dev) ) ;
    _EDTconfig->setHeight( ::pdv_get_height(_EDTpdv_dev) ) ;
    _EDTconfig->setDepth( ::pdv_get_depth(_EDTpdv_dev) ) ;
    _EDTconfig->rename( ::pdv_get_cameratype(_EDTpdv_dev) ) ;
    ::pdv_close( _EDTpdv_dev );
    _EDTpdv_dev = 0;
    _CameraType->rename( _EDTconfig->name() ) ;
    clog << Caller + "> opened & closed EDT device interface..." <<endl;
    clog << Caller + "> configuration of EDT-pdv:";
    clog << "  Width=" << _EDTconfig->width()
	 << ", Height=" << _EDTconfig->height()
	 << ", Depth=" << _EDTconfig->depth()
	 << ", CameraType=" << _CameraType->name() << endl;
  }

  ::pthread_mutex_unlock( &_EDTdevMutex );
  UFFrameConfig* theFrameConfig = accessFrameConfig( true );
  theFrameConfig->rename( _CameraType->name() ) ;
  theFrameConfig = accessFrameConfig( false );
}
//-----------------------------------------------------------------------------------
void FrameGrabShutdown()
{
  _keepRunning = false ;
  ::pthread_mutex_unlock( &_FrameRecvMutex );
  ::pthread_mutex_unlock( &_FrameGrabMutex );
}
//-----------------------------------------------------------------------------------

UFStrings* FrameGrabStart( string command, int TimeOut )
{
  string message;
  bool obsConfigDefined = true;  // check for existence:
  UFObsConfig* theObsConfig = accessObsConfig( true );
  if( theObsConfig == 0 ) obsConfigDefined = false;
  theObsConfig = accessObsConfig( false );

  if( !obsConfigDefined )
    {
      command = "ERROR";
      message = "FrameGrabStart> Observation Configuration is NOT defined, cannot START";
    }
  else if( ObsInProgress() )
    {
      command = "ERROR";
      message = "FrameGrabStart> observation in progress! Cannot START new obs.";
    }
  else
    {
      if( ! grabListEmpty() )
	{
	  FP_Start();     //signal FrameProcessThread.
	  clog << "FrameGrabStart> waiting 1 sec for FrameProcessThread to finish...\n";
	  UFPosixRuntime::sleep( 1.0 );

	  if( ! grabListEmpty() )
	    {
	      message = "FrameGrabStart> FrameProcessing NOT done! Cannot START new obs.";
	      clog << "ERROR: " << message << endl;
	      return( new UFStrings( "ERROR", &message ) );
	    }
	  UFPosixRuntime::sleep( 0.2 );  //wait a some more for FrameProcessThread to clean up.
	}

      if( nowSavingData() )
	{
	  UFFrameConfig* theFrameConfig = accessFrameConfig( true );
	  int FrameWriteCnt = theFrameConfig->frameWriteCnt();
	  theFrameConfig = accessFrameConfig( false );

	  if( FrameWriteCnt > 0 )
	    {
	      message = "FrameGrabStart> still writing to FITS file! Cannot START new obs.";
	      clog << "ERROR: " << message << endl;
	      return( new UFStrings( "ERROR", &message ) );
	    }
	  else message = "FrameGrabStart> Starting Observation: frames will be saved to FITS file.";
	}
      else message = "FrameGrabStart> Starting Observation: frames will NOT be saved.";

      if( TimeOut > 0 )
	timeout_EDTpdv = 1000 * TimeOut;  //convert secs to millisecs.
      else
	timeout_EDTpdv = 0;               //zero means no EDT timeout (wait forever).

      ResetCounters();        //reset here first to make theFrameConfig->frameObsSeqNo() = -1.
      clog << message << endl;
      ::pthread_mutex_unlock( &_FrameGrabMutex );       //signal FrameGrabThread to run.
      float sleepSec=0.5, waitSec=0.0, waitMax=9.0;
      int FrameCount = -1;

      while( FrameCount < 0 && waitSec < waitMax )     //wait until FrameGrabThread actually starts...
	{
	  UFPosixRuntime::sleep( sleepSec );
	  waitSec += sleepSec;
	  UFFrameConfig* theFrameConfig = accessFrameConfig( true );
	  FrameCount = theFrameConfig->frameObsSeqNo();  //FrameGrabThread sets this to zero.
	  theFrameConfig = accessFrameConfig( false );
	}

      if( waitSec > 1.0 )
	clog << "FrameGrabStart> waited " << waitSec << " sec for FrameGrabThread to start..."<<endl;

      if( FrameCount < 0 )
	{
	  message = "FrameGrabStart> FrameGrabThread did not start during 9 sec wait!";
	  command = "ERROR";
	}
      else if( internSimulation ) {
	if( !createFrameSimThread() ) {
	  message = "FrameGrabStart> Error creating the FrameSimThread for internal simulation: "
	       + string( strerror(errno) );
	  command = "ERROR";
	}
      }
    }

  if( command == "ERROR" ) clog << "ERROR: " << message << endl;
  return( new UFStrings( command, &message ) );
}
//-----------------------------------------------------------------------------------------------

UFStrings* FrameGrabAbort( string command )
{
  // Signal the FrameGrabThread to abort by setting frameObsSeqTot = frameObsSeqNo
  //  (attributes of theFrameConfig object) thus ending the loop over pdv_wait_image.
  // It is a good idea to call FrameGrabKill afterwards in case FrameGrabThread is
  //  stuck in blocking pdv_wait_image() call, thus detecting and freeing up FrameGrabThread.
  // If input command string has "ERROR" in it the abort is forced by an error condition,
  //  most likely an error in writing to FITS file in FrameWriteThread().
  string message;

  if( ObsInProgress() )
    {
      if( command.find("ABORT") != string::npos )
	_abortDHS = true;
      else
	_abortDHS = false;

      if( command.find("ERROR") != string::npos ) {
	stopDueToError = command;
	_abortDHS = false;
      }
      else stopDueToError = "";

      if( _abortDHS )
	message = stopDueToError + "FrameGrabAbort> Aborting Observation ***";
      else
	message = stopDueToError + "FrameGrabAbort> Stopping Observation ***";
      
      UFFrameConfig* theFrameConfig = accessFrameConfig( true );
      theFrameConfig->setFrameObsSeqTot( 0 );     //set grab loop max count to zero, forcing break.
      theFrameConfig->rename( message );
      if( theFrameConfig->frameObsSeqNo() == 0 )
	  theFrameConfig->stampTime( UFRuntime::currentTime() );
      theFrameConfig = accessFrameConfig( false );
    }
  else
    {
      command = "NOOP";
      message = "FrameGrabAbort> Observation Not in Progress: nothing to abort";
    }

  clog << "\n" << message << endl;
  return( new UFStrings( command, &message ) );
}
//--------------------------------------------------------------------------------------------

UFStrings* FrameGrabKill( string command )
{
  // If the obs does not abort after a few seconds (stuck on blocking in pdv_wait_image() function)
  //  then try aborting EDT-DMA.  If that does not un-stick it after 2 secs, then
  //  send SIGCONT signal to FrameGrabThread which frees it from blocking on pdv_wait_image().
  // The abort-DMA or CONT-signal is only sent if FrameGrabAbort has not succeeded, that is,
  //   the abort failed if  theFrameConfig->frameObsSeqTot()  still equals zero,
  //   since in a successfull abort it would be equal to the negative of expected total # frames.
  string message;
  float maxWaitTime=2.0, waitTime=0.0, sleepTime=1.0;

  if( timeout_EDTpdv > 0 ) {
    maxWaitTime = timeout_EDTpdv/1000.0;
    if( maxWaitTime > 4.0 ) maxWaitTime = 4.0;
  }

  clog << "FrameGrabKill> waiting up to " << maxWaitTime
       << " secs to see if FrameGrabAbort succeeds..." <<endl;
  UFFrameConfig* theFrameConfig = accessFrameConfig( true );
  int FrameTotal = theFrameConfig->frameObsSeqTot();
  theFrameConfig = accessFrameConfig( false );

  while( FrameTotal == 0 && waitTime < maxWaitTime ) {
    UFPosixRuntime::sleep( sleepTime );
    waitTime += sleepTime;
    theFrameConfig = accessFrameConfig( true );
    FrameTotal = theFrameConfig->frameObsSeqTot();
    theFrameConfig = accessFrameConfig( false );
  }

  if( FrameTotal == 0 ) //abort is still pending, try aborting the DMA...
    {
      ::pthread_mutex_lock( &_EDTdevMutex ); //to make sure device is not closed just before:
      if( _EDTpdv_dev > 0 ) {
	  clog<<"FrameGrabKill> *** try aborting EDT-DMA ***"<<endl;
	  if( ::edt_abort_current_dma( _EDTpdv_dev ) < 0 ) {
	    string error = strerror( errno );
	    clog<<"FrameGrabKill> error aborting current EDT-DMA: "<<error<<endl;
	  }
      }
      else {
	clog<<"FrameGrabKill> unlocking simulation frame recv mutex..."<<endl;
	::pthread_mutex_unlock( &_FrameRecvMutex ); //needed to abort if in simulation mode.
      }
      ::pthread_mutex_unlock( &_EDTdevMutex );

      maxWaitTime=2.0;
      waitTime=0.0;
      clog << "FrameGrabKill> waiting up to " << maxWaitTime
	   << " secs to see if abort EDT-DMA succeeds..." <<endl;

      while( FrameTotal == 0 && waitTime < maxWaitTime ) {
	UFPosixRuntime::sleep( sleepTime );
	waitTime += sleepTime;
	theFrameConfig = accessFrameConfig( true );
	FrameTotal = theFrameConfig->frameObsSeqTot();
	theFrameConfig = accessFrameConfig( false );
      }

      if( FrameTotal == 0 ) //last resort try sending SIGCONT to thread...
	{
	  if( ::pthread_kill( FrameGrabThreadID, SIGCONT ) != 0 )
	    {
	      command = "ERROR";
	      message = "FrameGrabKill> failed to send CONT signal to FrameGrabThread. ";
	    }
	  else message = "FrameGrabKill> aborted EDT frame grabbing with CONT signal. ";

	  theFrameConfig = accessFrameConfig( true );
	  theFrameConfig->rename( message );
	  theFrameConfig = accessFrameConfig( false );
	}
      else message = "FrameGrabKill> abort EDT-DMA succeeded. ";
    }
  else {
    command = "NOOP";
    message = "FrameGrabKill> ABORT/STOP succeeded. ";
  }

  clog << message << endl;
  return( new UFStrings( command, &message ) );
}
//--------------------------------------------------------------------------------------------

void FrameGrabSimulate( UFInts* simFrame )
{
  // Feed the simulation mode of FrameGrabThread when
  //   receiving a sequence of test frames from a client (no status reply),
  //     or from the internal frame simulation thread.
  //   If there is no EDT device,
  //     the frames are moved to the receivedFramesList,
  //     from which FrameGrabThread pretends to be grabbing.
  //   If there is a real EDT device then just delete recvd frame.

  if( EDTdeviceExists() && !internSimulation )
    {
      delete simFrame;
      clog << "FrameGrabThread> NOT in simulation mode: deleted recvd frame."<<endl;
    }
  else
    {
      if( ObsInProgress() )
	{
	  ::pthread_mutex_lock( &_receivedFramesListMutex );
	  _receivedFramesList.push_front( simFrame );
	  ::pthread_mutex_unlock( &_receivedFramesListMutex );
	}
      else  // observation NOT in progress:
	{
	  delete simFrame;
	  clog << "FrameGrabThread> cannot recv any more simulation frames: obs is done."<<endl;
	}

      ::pthread_mutex_unlock( &_FrameRecvMutex );   //always unlock so FrameGrabThread proceeds.
    }
}
//--------------------------------------------------------utility functions:-------

bool ObsInProgress()
{
  UFFrameConfig* theFrameConfig = accessFrameConfig( true );
  int FrameCount = theFrameConfig->frameObsSeqNo();
  int FrameGrabs = theFrameConfig->frameGrabCnt();
  int FrameTotal = theFrameConfig->frameObsSeqTot();
  theFrameConfig = accessFrameConfig( false );

  if( FrameCount >= 0 && FrameCount < FrameTotal && FrameGrabs < totalFramesToGrab() )
    return true;
  if( FrameCount >= 0 && FrameTotal == 0 )   //this mean that an ABORT is pending.
    return true;
  else
    return false;
}

#endif /* __FrameGrabThread_cc__ */

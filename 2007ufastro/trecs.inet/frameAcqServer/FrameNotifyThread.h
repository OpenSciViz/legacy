#if !defined(__FrameNotifyThread_h__)
#define __FrameNotifyThread_h__ "$Name:  $ $Id: FrameNotifyThread.h,v 1.2 2003/07/10 23:54:11 varosi beta $"

/*
 * The FrameNotifyThread sends stream of FrameConfigs to any client that has requested "NOTIFY".
 *  The name field gives the names of which buffers have been updated (delimeted with "^").
 *
 * Frank Varosi, July 9, 2003
 */

extern void* FrameNotifyThread( void* ) ;
extern bool createFrameNotifyThread() ;
extern void FrameNotifyStart() ;
extern void FrameNotifyShutdown() ;
extern void clearNotifyStream() ;
extern bool notifyStreamEmpty() ;
extern bool addtoNotifyList( UFSocket* clientSocket ) ;
extern bool inNotifyList( UFSocket* clientSocket ) ;
extern void removeNotifyList( UFSocket* clientSocket ) ;
extern void notifyClientsNewFC( const vector< string >& updatedBufferList ) ;
extern void sendNotifications( UFFrameConfig* currFC ) ;
extern int totalNotifiesSent() ;
extern int totalNotifiesError() ;
extern int totalNotifiesDropped() ;
extern int totalNotifiesPending() ;

#endif /* __FrameNotifyThread_h__ */

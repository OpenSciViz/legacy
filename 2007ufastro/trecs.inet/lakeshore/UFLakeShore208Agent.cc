#if !defined(__UFLakeShore208Agent_cc__)
#define __UFLakeShore208Agent_cc__ "$Name:  $ $Id: UFLakeShore208Agent.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFLakeShore208Agent_cc__;

#include "sys/types.h"
#include "sys/stat.h"
#include "fcntl.h"

#include "string"
#include "vector"
#include "deque"

#include "UFLakeShore208Agent.h"
#include "UFTimeStamp.h"
#include "UFStrings.h"
#include "UFBytes.h"
#include "UFShorts.h"
#include "UFInts.h"
#include "UFFloats.h"
#include "UFObsConfig.h"
#include "UFFrameConfig.h"
#include "UFFrames.h"
#include "UFDeviceConfig.h"
#include "UFLS208poll.h"

string UFLakeShore208Agent::_currentval;

// ctors:
UFLakeShore208Agent::UFLakeShore208Agent(int argc, char** argv) : UFDeviceAgent(argc, argv),
                                                                  _fifoInput(-1) {
  _currentval = "1,KKK.K 2,KKK.K 3,KKK.K 4,KKK.K 5,KKK.K 6,KKK.K 7,KKK.K 8,KKK.K\n";
  // use ancillary function to retrieve LS208 readings
  bool *bp = new bool;
  *bp = false; // arg to ancillary func. indicates non-blocking reads
  UFRndRobinServ::_ancil = (void*) bp;
}

UFLakeShore208Agent::UFLakeShore208Agent(const string& name,
			                 int argc, char** argv) : UFDeviceAgent(name, argc, argv),
                                                                  _fifoInput(-1) {
  _currentval = "1,KKK.K 2,KKK.K 3,KKK.K 4,KKK.K 5,KKK.K 6,KKK.K 7,KKK.K 8,KKK.K\n";
  // use ancillary function to retrieve LS208 reading
  bool *bp = new bool;
  *bp = false; // arg to ancillary func. indicates non-blocking reads
  UFRndRobinServ::_ancil = (void*) bp;
}

// static funcs:
int UFLakeShore208Agent::main(int argc, char** argv) {
  UFLakeShore208Agent ufs("UFLakeShore208Agent", argc, argv); // ctor binds to listen socket
  ufs.exec((void*)&ufs);
  return argc;
}

UFTermServ* UFLakeShore208Agent::init(const string& host, int port) {
  // this should NOT connect to the Terminal Server LakeShore208 port
  // because the child process will do that!
  // this assumes the inherited startup function has initialized _config
  // by calling options() before it calls this:
  if( _config == 0 ) {
    clog<<"UFLakeShore208Agent> failed to init device config..."<<endl;
    return 0;
  }
  _config->connect();
  if( _config == 0 ) {
    clog<<"UFLakeShore208Agent> failed to init device connection..."<<endl;
    return 0;
  }
 
  // UFDaemon child process for polling LS208
  UFLS208poll* ls208p = new UFLS208poll("LS208Poll", _config, _args);
  // create fifo first
  string fifonm = ls208p->fifoName();
  clog<<"UFLakeShore208Agent>  create output fifo: "<<fifonm<<endl;
  int stat = UFPosixRuntime::fifoCreate(fifonm);
  if( stat < 0 ) {
    clog<<"UFLS200poll> unable to create output fifo. "<<fifonm<<endl;
    return 0;
  }
  // create child, which should open the fifo for writes
  pid_t child = createChild(ls208p, ls208p); // child will run ls208p->exec()
  if( child == (pid_t) -1 ) {
    clog<<"UFLakeShore208Agent> unable to create & start child proc. to poll LS208"<<endl;
    delete _config;
    return 0;
  }
  // open the fifo for reading
  //clog<<"UFLakeShore208Agent> block on open LS208 fifo for read access: "<<fifonm<<endl;
  //_fifoInput = ::open(fifonm.c_str(), O_RDONLY | O_NONBLOCK);
  _fifoInput = ::open(fifonm.c_str(), O_RDONLY);

  if( _fifoInput <= 0 ) {
    clog<<"UFLakeShore208Agent> unable to open LS208 fifo for read access:"<<fifonm<<endl;
    delete _config;
    return 0;
  }
  clog<<"UFLakeShore208Agent> opened LS208 fifo (readonly): "<<fifonm<<", fd= "<<_fifoInput<<endl;

  bool block = true; // force read of first values
  ancillary((void*) &block);
  
  //clog<<"UFLakeShore208Agent> continue?:"<<flush; getc(stdin);
  return _config->_devIO;
}

void* UFLakeShore208Agent::ancillary(void* p) {
  bool block = false;
  if( p )
    block = *((bool*)p);

  /*
  if( block )
    clog<<"UFLakeShore208Agent::ancillary> blocking to get new LS208 reading..."<<endl;
  else
    clog<<"UFLakeShore208Agent::ancillary> NOT blocking to get new LS208 reading..."<<endl;
  */
  string data208;

  const size_t _HistorySize__ = 10000; 
  int nb= 0, na= 0;
  while( nb >= 0 && na >= 0 && data208.rfind("\n") == string::npos ) {
    na = available(_fifoInput);
    if( na < 0 ) {
      clog<<"unable to read LS208 fifo"<<endl;
      delete _config;
      return 0;
    }
    else if( na == 0 && block ) {
      na = 1; // block on reading one byte
    }
    if( na == 0 ) { // don't block...
      //clog<<"UFLakeShore208Agent::ancillary> (non-blocking, return) reading: "<<data208<<flush;
      return p;
    }
    char input[(const int) (1+na)]; memset(input, 0, (const int) (1+na));
    nb = ::read(_fifoInput, input, na); 
    data208 += input;
    //clog<<"UFLakeShore208Agent::ancillary> (blocking) reading: "<<data208<<flush;
  }
  _currentval = data208;
  _DevHistory.push_back(data208);
  if( _DevHistory.size() > _HistorySize__ ) // should be configurable size
    _DevHistory.pop_front();

  clog<<"UFLakeShore208Agent::ancillary> new reading: "<<data208<<flush;

  return p;
}

int UFLakeShore208Agent::options(string& servlist) {
  int listenport = UFDeviceAgent::options(servlist);
  string arg; 

  if( _config == 0 )
    _config = new UFLS208Config();

  arg = findArg("-tsport");
  if( arg != "false" && arg != "true" )
     _config->_tsport = atoi(arg.c_str());

  arg = findArg("-tshost");
  if( arg != "false" && arg != "true" )
     _config->_tshost = arg;
  
  if( _config->_tsport ) { // 7001 <= _tsport <= 7008
    listenport = 52000 +  _config->_tsport - 7000;
  }
  else {
    listenport = 52003;
  }  
  clog<<"UFLakeShore208Agent::options> set listen port to LakeShore208 agent: "<<listenport<<endl;
  return listenport;
}

// these are the key virtual functions to override,
// presumably any allocated memory is freed by the calling layer....
// for the moment assume "raw" commands and simply pass along to lakeshore208
int UFLakeShore208Agent::action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& rep) {
  rep = new vector< UFProtocol* >;
  rep->push_back(_allKelvin(act));
  return (int)rep->size();
}

// return reply
int UFLakeShore208Agent::query(const string& q, vector<string>& qreply) {
  return UFDeviceAgent::query(q, qreply);
}

// return either the last/latest set of temperature values,
// or the full history
UFStrings* UFLakeShore208Agent::_allKelvin(UFDeviceAgent::CmdInfo* req) {
  req->cmd_reply.clear();
  req->time_submitted = currentTime();
  _active = req;
  int cnt= 1, cmdcnt = (int)req->cmd_name.size();
  string cmdname, cmdimpl;
  bool raw= true, all= false;
  // this is somewhat bogus, since i never really expect to get 
  // "bundled" lakeshore 208 cmds.
  // however, lakeshore 218 & 340 models will..
  for(int i=0; i<cmdcnt; ++i) {
    cmdname = req->cmd_name[i];
    cmdimpl = req->cmd_impl[i];
    if( cmdimpl.find("&") != string::npos ) { // stop processing list
      clog<<"UFLakeShoreAgent::action> rejected: "<<cmdimpl<<endl;
      return (UFStrings*) 0;
    }
    //raw = (cmdname.find("raw") == 0 || cmdname.find("Raw") == 0 || cmdname.find("RAW"));
    //all = (cmdimpl.find("all") == 0 || cmdimpl.find("All") == 0 || cmdimpl.find("ALL"));
    raw = (cmdname == "raw" || cmdname == "Raw" || cmdname == "RAW");
    all = (cmdimpl == "all" || cmdimpl == "All" || cmdimpl == "ALL");
    if( !all ) { 
      // reset cnt -- just return the last/latest readings, assume request is cnt val!
      cnt = atoi(cmdimpl.c_str()); 
      if( cnt > (int)_DevHistory.size()) 
        cnt = (int)_DevHistory.size();
    }
    else {
      cnt = (int)_DevHistory.size(); 
    }
    clog<<"UFLakeShore208Agent> cnt= "<<cnt<<", cmd= "<<cmdimpl<<endl;
    int last = (int)_DevHistory.size() - 1;
    for( int i = 0; i < cnt; ++i ) {
      req->cmd_reply.push_back(_DevHistory[last - i]);
    }
  }
  req->time_completed = currentTime();

  if( req->cmd_reply.empty() )
    req->status_cmd = "failed";
  else
    req->status_cmd = "succeeded";

  _completed.insert(UFDeviceAgent::CmdList::value_type(req->cmd_time, req));

  UFStrings* retval= 0;
  if( raw )
    // return just the device reply;
    retval = new UFStrings(req->clientinfo+">> "+cmdname+" "+cmdimpl, req->cmd_reply);
  else
    // return all available cmd info
    retval = req->ufStrings(req->clientinfo+">> "+cmdname+" "+cmdimpl);

  return retval; 
} 

#endif // UFLakeShore208Agent

#if !defined(__ufls208_cc__)
#define __ufls208_cc__ "$Name:  $ $Id: ufls208.cc 14 2008-06-11 01:49:45Z hon $";

#include "string"
#include "vector"
#include "stdio.h"

#include "UFDaemon.h"
#include "UFClientSocket.h"
#include "UFTimeStamp.h"
#include "UFStrings.h"

bool quiet = false;

class ufls208 : public UFDaemon, public UFClientSocket {
public:
  ~ufls208();
  inline ufls208(int argc, char** argv, int port= -1) : UFDaemon(argc, argv),
						        UFClientSocket(port), _fs(0) {
    rename("ufls208@" + hostname());
  }

  static int main(int argc, char** argv);
  // return 0 on connection failure:
  FILE* init(const string& host, int port);

  // submit string (only), return immediately without fetching reply
  int submit(const string& raw, float flush= -1.0); // flush output and sleep flush sec.

  // submit string, recv reply and return reply as string
  //int submit(const string& raw, UFStrings& reply, float flush= -1.0); // flush output and sleep flush sec.
  int submit(const string& raw, UFStrings*& reply, float flush= -1.0); // flush output and sleep flush sec.

  virtual string description() const { return __ufls208_cc__; }

protected:
  FILE* _fs; // for flushing ... this should really go into UFSocket someday
};

int ufls208::main(int argc, char** argv) {
  ufls208 ls208(argc, argv);
  string arg, host(hostname()), raw("true"); // default is raw command mode
  int port= 52003;
  float flush= -1.0;

  arg = ls208.findArg("-flush");
  if( arg != "false" )
    flush = atof(arg.c_str());

  arg = ls208.findArg("-raw");
  if( arg != "false" &&  arg != "true" ) // explicit cmd string 
    raw = arg;

  arg = ls208.findArg("-host");
  if( arg != "false" )
    host = arg;

  arg = ls208.findArg("-port");
  if( arg != "false" && arg != "false" )
    port = atoi(arg.c_str());

  arg = ls208.findArg("-q");
  if( arg != "false" )
    quiet = true;

  if( port <= 0 || host.empty() ) {
    clog<<"ufls208> usage: 'ufls208 -flush sec. -host host -port port -raw raw-command'"<<endl;
    return 0;
  }

  FILE* f  = ls208.init(host, port);
  if( f == 0 ) {
    clog<<"ufls208> unable to connect to LakeShore208 command server/agent..."<<endl;
    return 1;
  }

  UFStrings* reply_p;;
  if( raw != "true" && raw != "false" ) {
    // raw (explicit) command should be executed once
    int ns = ls208.submit(raw, reply_p, flush);
    if( ns <= 0 )
      clog << "ufls208> failed to submit raw= "<<raw<<endl;

    UFStrings& reply = *reply_p;
    //cout<<reply.timeStamp()<<reply.name()<<endl;
    for( int i=0; i < reply.elements(); ++i )
      cout<<reply[i]<<endl;

    delete reply_p; reply_p = 0;
    return 0;
  }
  // enter command loop
  string line;
  while( true ) {
    clog<<"ufls208(raw)> "<<ends;
    getline(cin, line);
    if( line == "exit" || line == "quit" || line == "q" )
      return 0;

    raw = line;
    int ns = ls208.submit(raw, reply_p, flush);
    if( ns <= 0 )
      clog << "ufls208> failed to submit raw= "<<raw<<endl;

    UFStrings& reply = *reply_p;
    cout<<reply.timeStamp()<<reply.name()<<endl;
    for( int i=0; i < reply.elements(); ++i )
      cout<<reply[i]<<endl;
    delete reply_p; reply_p = 0;
  }
  return 0; 
}

// public static func:
// return < 0 on connection failure:
FILE* ufls208::init(const string& host, int port) {
  int fd = connect(host, port);
  if( fd <= 0 ) {
    return 0;
  }
  _fs = fdopen(fd, "w");

  // after accetping connection, server/agent will expect client to send
  // a UFProtocol object identifying itself, and echos it back (slightly
  // modified) 
  UFTimeStamp greet(name());
  int ns = send(greet);
  ns = recv(greet);
  if( !quiet )
    clog<<"ufls208> greeting: "<< greet.name() << " " << greet.timeStamp() <<endl;

  return _fs;
}

// public
ufls208::~ufls208() {
  if( _fs ) {
    close(); 
    fclose(_fs);
    _fs = 0;
  }
}

// submit string (only), return immediately without fetching reply
int ufls208::submit(const string& s, float flush) {
  vector<string> cmd;
  cmd.push_back("raw");
  cmd.push_back(s);
  UFStrings ufs(name(), cmd);
  int ns = send(ufs);
  // try flushing the socket output stream
  // this is the only way i can think to do it:
  int stat = fflush(_fs);
  if( stat != 0 ) clog<<"ufls208::submit> socket flush failed? ns= "<<ns<<endl;

  if( flush > 0.0 ) // optionally sleep after the flush
    UFPosixRuntime::sleep(flush);

  return ns;
}

// submit string, recv reply
//int ufls208::submit(const string& s, UFStrings& r, float flush) {
int ufls208::submit(const string& s, UFStrings*& r, float flush) {
  int ns = submit(s, flush);
  if( ns <= 0 )
    return ns;

  /*
  ns = recv(r);
  if( ns <= 0 )
    return ns;

  return r.elements();
  */

  r = dynamic_cast<UFStrings*> (UFProtocol::createFrom(*this));

  return r->elements();
}

int main(int argc, char** argv) {
  return ufls208::main(argc, argv);
}

#endif // __ufls208_cc__

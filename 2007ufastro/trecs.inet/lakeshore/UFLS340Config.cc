#if !defined(__UFLS340Config_cc__)
#define __UFLS340Config_cc__ "$Name:  $ $Id: UFLS340Config.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFLS340Config_cc__;

#include "UFLS340Config.h"
#include "UFGemLakeShore340Agent.h"
#include "UFFITSheader.h"

// global/statics:
const float UFLS340Config::_NominalSetPnt= 7.0;
const string UFLS340Config::_NominalSetPntCmd= "SETP 1, 7.0";
const float UFLS340Config::_MaxSetPnt= 15.1;
const string UFLS340Config::_MaxAllowedCmd= "SETP 1, 15.1"; // control loop 1
const int UFLS340Config::_BadSetPnt= 15;
const int UFLS340Config::_BadPID= -10;

UFLS340Config::UFLS340Config(const string& name) : UFDeviceConfig(name) {}

UFLS340Config::UFLS340Config(const string& name,
			     const string& tshost,
			     int tsport) : UFDeviceConfig(name, tshost, tsport) {}

// LS340 terminator is new-line:
//string UFLS340Config::terminator() { return "\n"; }

// LS340 prefix is none:
//string UFLS340Config::prefix() { return ""; }

vector< string >& UFLS340Config::queryCmds() {
  if( _queries.size() > 0 ) // already set
    return _queries;

  _queries.push_back("CRVHDR?"); _queries.push_back("crvhdr?");
  _queries.push_back("CRVPT?"); _queries.push_back("crvpt?");
  _queries.push_back("INCRV?"); _queries.push_back("incrv?");
  _queries.push_back("INSET?"); _queries.push_back("inset?");
  _queries.push_back("INTYPE?"); _queries.push_back("intype?");
  _queries.push_back("KRDG?A"); _queries.push_back("krdg?a");
  _queries.push_back("KRDG?B"); _queries.push_back("krdg?b");
  _queries.push_back("PID?"); _queries.push_back("pid?");
  _queries.push_back("RANGE?"); _queries.push_back("range?");
  _queries.push_back("SRDG?A"); _queries.push_back("srdg?a");
  _queries.push_back("SRDG?B"); _queries.push_back("srdg?b");
  _queries.push_back("ZONE?"); _queries.push_back("zone?");
	
  return _queries;
}

vector< string >& UFLS340Config::actionCmds() {
  if( _actions.size() > 0 ) // already set
    return _actions;

  _actions.push_back("CRVHDR"); _actions.push_back("crvhdr");
  _actions.push_back("CRVPT"); _actions.push_back("crvpt");
  _actions.push_back("CRVSAV"); _actions.push_back("crvsav");
  _actions.push_back("CMODE"); _actions.push_back("cmode");
  _actions.push_back("CSET"); _actions.push_back("cset");
  _actions.push_back("INCRV"); _actions.push_back("incrv");
  _actions.push_back("INSET"); _actions.push_back("inset");
  _actions.push_back("INTYPE"); _actions.push_back("intype");
  _actions.push_back("PID"); _actions.push_back("pid");
  _actions.push_back("SETP"); _actions.push_back("setp");
  _actions.push_back("SRDG"); _actions.push_back("srdg");
  _actions.push_back("RANGE"); _actions.push_back("range");
  _actions.push_back("ZONE"); _actions.push_back("zone");

  return _actions;
}

int UFLS340Config::validCmd(const string& c) {
  //clog<<"UFLS340Config::validCmd> check cmd: "<<c<<endl;
  vector< string >& av = actionCmds();
  int i= 0, cnt = (int)av.size();
  while( i < cnt ) {
    if( c.find(av[i++]) != string::npos ) { 
      clog<<"UFLS340Config::validCmd> found cmd: "<<c<<endl;
      if( c.find("?") != string::npos ) // query ok, expect lakeshore reply
        return 1;
      int ppos= -1, pos = -1; 
      string val;
      if( c.find("SETP") != string::npos || c.find("Setp") != string::npos || c.find("setp") != string::npos )
	pos = 1+c.find(",");
      if( pos > 0 )  { // check that value is acceptable
	val = c.substr(pos);
	double kelvin = atof(val.c_str());
        if( kelvin > _MaxSetPnt )
	  return _BadSetPnt;
	else
          return 0;
	continue;
      }
      if( c.find("PID") != string::npos || c.find("Pid") != string::npos || c.find("pid") != string::npos )
        pos = 1+c.find(","); // parse 'PID 1, P, I, D' to insure I<P,D
      if( pos > 0 ) { // check that value is acceptable
        ppos = pos;
        pos  = 1+c.find(",", pos);
        val = c.substr(ppos, pos-ppos-1);
        double P = atof(val.c_str());
        ppos = pos;
        pos  = 1+c.find(",", pos);
        val = c.substr(ppos, pos-ppos-1);
        double I = atof(val.c_str());
        val = c.substr(pos);
        double D = atof(val.c_str());
        clog<<"UFLS340Config::validCmd> PID cmd: "<<P<<", "<<I<<", "<<D<<endl;
        if( I > P || I > D )
          return _BadPID; // reject command
        else
	  return 0;
        continue;
      }
      return 0; // accept command, expect no reply from lakeshore
    }
  }
  
 vector< string >& qv = queryCmds();
  i= 0; cnt = (int)qv.size();
  while( i < cnt ) {
    if( c.find(qv[i++]) != string::npos )
      return 1;
  }
  clog<<"UFLS340Config::validCmd> !?Bad cmd: "<<c<<endl; 
  return -1;
}

UFStrings* UFLS340Config::status(UFDeviceAgent* da) {
  UFGemLakeShore340Agent* da340 = dynamic_cast< UFGemLakeShore340Agent* > (da);
  if( da340->_verbose )
    clog<<"UFLS340Config::status> "<<da340->name()<<endl;

  string a, b, sp;
  da340->getCurrent(a, b);
  da340->getSetPnt(sp);

  vector< string > vals;
  string s = "Detector Array"; s += " == " + a + " Kelvin";
  UFFITSheader::rmJunk(s); // use this conv. func. to eliminate '\n' & '\r' etc.
  vals.push_back(s);

  s = "Cold Finger"; s += " == " + b + " Kelvin";
  UFFITSheader::rmJunk(s); // use this conv. func. to eliminate '\n' & '\r' etc.
  vals.push_back(s);

  s = "Det. Set Point"; s += " == " + sp + " Kelvin";
  UFFITSheader::rmJunk(s); // use this conv. func. to eliminate '\n' & '\r' etc.
  vals.push_back(s);

  return new UFStrings(da340->name(), vals);
}

UFStrings* UFLS340Config::statusFITS(UFDeviceAgent* da) {
  UFGemLakeShore340Agent* da340 = dynamic_cast< UFGemLakeShore340Agent* > (da);
  if( da340->_verbose )
    clog<<"UFLS340Config::status> "<<da340->name()<<endl;

  string a, b, sp;
  da340->getCurrent(a, b);
  UFFITSheader::rmJunk(a); // use this conv. func. to eliminate '\n' & '\r' etc.
  UFFITSheader::rmJunk(b); // use this conv. func. to eliminate '\n' & '\r' etc.
  da340->getSetPnt(sp);
 
  map< string, string > valhash, comments;
  valhash["DetArray"] = a; 
  comments["DetArray"]  = "Kelvin";
  valhash["ColdFing"] = b; 
  comments["ColdFing"]  = "Kelvin";
  valhash["DetSet"] = sp;
  comments["DetSet"]  = "Kelvin";

  UFStrings* ufs = UFFITSheader::asStrings(valhash, comments);
  ufs->rename(da340->name());

  return ufs;
}

int UFLS340Config::setPID(float P, float I, float D) {
  if( I > P || I > D )
    return _BadPID;

  strstream s;
  s << "PID 1, "<<P<<", "<<I<<", "<<D<<ends;
  string pidcmd = s.str(); delete s.str();
  _devIO->submit(pidcmd, 0.5);

  return 0;
}

int UFLS340Config::setInTypTReCS() {
  string intyp = "intype a,0,1,1,3,11";
  clog<<"UFLS340Config::setInTypTReCS> submit (and wait 10 sec.) "<<intyp<<endl;
  _devIO->submit(intyp, 10.0);
  return 0;
}

int UFLS340Config::setCrvTReCS() {
  if( _devIO == 0 ) {
    clog<<"UFLS340Config::setCrvTReCS> no device connection..."<<endl;
    return -1;
  }
  clog<<"UFLS340Config::setCrvTReCS> installing TReCS array temperature calibration curve into channel A: "<<endl;
  vector< string > crv;
  crv.push_back("CRVHDR 21, T-ReCS, 774-30271, 2, 345.0, 1");
  crv.push_back("CRVPT 21,   1, 0.30000, 344.18243");
  crv.push_back("CRVPT 21,   2, 0.36000, 322.05237");
  crv.push_back("CRVPT 21,   3, 0.42000, 299.92230");
  crv.push_back("CRVPT 21,   4, 0.48000, 277.79221");
  crv.push_back("CRVPT 21,   5, 0.54000, 255.66217");
  crv.push_back("CRVPT 21,   6, 0.60000, 233.66438");
  crv.push_back("CRVPT 21,   7, 0.66000, 211.57458");
  crv.push_back("CRVPT 21,   8, 0.72000, 189.20183");
  crv.push_back("CRVPT 21,   9, 0.78000, 166.37228");
  crv.push_back("CRVPT 21,  10, 0.84000, 142.88715");
  crv.push_back("CRVPT 21,  11, 0.90000, 118.40949");
  crv.push_back("CRVPT 21,  12, 0.91286, 112.98148");
  crv.push_back("CRVPT 21,  13, 0.92571, 107.47499");
  crv.push_back("CRVPT 21,  14, 0.93857, 101.86960");
  crv.push_back("CRVPT 21,  15, 0.95143,  96.16261");
  crv.push_back("CRVPT 21,  16, 0.96429,  90.33686");
  crv.push_back("CRVPT 21,  17, 0.97714,  84.35753");
  crv.push_back("CRVPT 21,  18, 0.99000,  78.21526");
  crv.push_back("CRVPT 21,  19, 1.00286,  71.85890");
  crv.push_back("CRVPT 21,  20, 1.01571,  65.21358");
  crv.push_back("CRVPT 21,  21, 1.02857,  58.06507");
  crv.push_back("CRVPT 21,  22, 1.04143,  49.84371");
  crv.push_back("CRVPT 21,  23, 1.05429,  39.57777");
  crv.push_back("CRVPT 21,  24, 1.06714,  28.83760");
  crv.push_back("CRVPT 21,  25, 1.08000,  21.51671");
  crv.push_back("CRVPT 21,  26, 1.09286,  17.85848");
  crv.push_back("CRVPT 21,  27, 1.10571,  15.88013");
  crv.push_back("CRVPT 21,  28, 1.11857,  14.59640");
  crv.push_back("CRVPT 21,  29, 1.13143,  13.67490");
  crv.push_back("CRVPT 21,  30, 1.14429,  12.99773");
  crv.push_back("CRVPT 21,  31, 1.15714,  12.46981");
  crv.push_back("CRVPT 21,  32, 1.17000,  12.03194");
  crv.push_back("CRVPT 21,  33, 1.18286,  11.68552");
  crv.push_back("CRVPT 21,  34, 1.19571,  11.38676");
  crv.push_back("CRVPT 21,  35, 1.20857,  11.12455");
  crv.push_back("CRVPT 21,  36, 1.22143,  10.89686");
  crv.push_back("CRVPT 21,  37, 1.23429,  10.70375");
  crv.push_back("CRVPT 21,  38, 1.24714,  10.52923");
  crv.push_back("CRVPT 21,  39, 1.26000,  10.36692");
  crv.push_back("CRVPT 21,  40, 1.27286,  10.21387");
  crv.push_back("CRVPT 21,  41, 1.28571,  10.07441");
  crv.push_back("CRVPT 21,  42, 1.29857,   9.94969");
  crv.push_back("CRVPT 21,  43, 1.31143,   9.83300");
  crv.push_back("CRVPT 21,  44, 1.32429,   9.72842");
  crv.push_back("CRVPT 21,  45, 1.33714,   9.62666");
  crv.push_back("CRVPT 21,  46, 1.35000,   9.52863");
  crv.push_back("CRVPT 21,  47, 1.36286,   9.43118");
  crv.push_back("CRVPT 21,  48, 1.37571,   9.33626");
  crv.push_back("CRVPT 21,  49, 1.38857,   9.25339");
  crv.push_back("CRVPT 21,  50, 1.40143,   9.17608");
  crv.push_back("CRVPT 21,  51, 1.41429,   9.10492");
  crv.push_back("CRVPT 21,  52, 1.42714,   9.03407");
  crv.push_back("CRVPT 21,  53, 1.44000,   8.96541");
  crv.push_back("CRVPT 21,  54, 1.45286,   8.90032");
  crv.push_back("CRVPT 21,  55, 1.46571,   8.84411");
  crv.push_back("CRVPT 21,  56, 1.47857,   8.78540");
  crv.push_back("CRVPT 21,  57, 1.49143,   8.72740");
  crv.push_back("CRVPT 21,  58, 1.50429,   8.66998");
  crv.push_back("CRVPT 21,  59, 1.51714,   8.61388");
  crv.push_back("CRVPT 21,  60, 1.53000,   8.57726");
  crv.push_back("CRVPT 21,  61, 1.54286,   8.52813");
  crv.push_back("CRVPT 21,  62, 1.55571,   8.47624");
  crv.push_back("CRVPT 21,  63, 1.56857,   8.42110");
  crv.push_back("CRVPT 21,  64, 1.58143,   8.36918");
  crv.push_back("CRVPT 21,  65, 1.59429,   8.31727");
  crv.push_back("CRVPT 21,  66, 1.60714,   8.26535");
  crv.push_back("CRVPT 21,  67, 1.62000,   8.21344");
  crv.push_back("CRVPT 21,  68, 1.63286,   8.17439");
  crv.push_back("CRVPT 21,  69, 1.64571,   8.12561");
  crv.push_back("CRVPT 21,  70, 1.65857,   8.07682");
  crv.push_back("CRVPT 21,  71, 1.67143,   8.02804");
  crv.push_back("CRVPT 21,  72, 1.68429,   7.98201");
  crv.push_back("CRVPT 21,  73, 1.69714,   7.93515");
  crv.push_back("CRVPT 21,  74, 1.71000,   7.89540");
  crv.push_back("CRVPT 21,  75, 1.72286,   7.85307");
  crv.push_back("CRVPT 21,  76, 1.73571,   7.81074");
  crv.push_back("CRVPT 21,  77, 1.74857,   7.78144");
  crv.push_back("CRVPT 21,  78, 1.76143,   7.74322");
  crv.push_back("CRVPT 21,  79, 1.77429,   7.71106");
  crv.push_back("CRVPT 21,  80, 1.78714,   7.67432");
  crv.push_back("CRVPT 21,  81, 1.80000,   7.60766");
  crv.push_back("CRVPT 21,  82, 1.81429,   7.57226");
  crv.push_back("CRVPT 21,  83, 1.82857,   7.53700");
  crv.push_back("CRVPT 21,  84, 1.84286,   7.50189");
  crv.push_back("CRVPT 21,  85, 1.85714,   7.46692");
  crv.push_back("CRVPT 21,  86, 1.87143,   7.43209");
  crv.push_back("CRVPT 21,  87, 1.88571,   7.39740");
  crv.push_back("CRVPT 21,  88, 1.90000,   7.36285");
  crv.push_back("CRVPT 21,  89, 1.91429,   7.32845");
  crv.push_back("CRVPT 21,  90, 1.92857,   7.29418");
  crv.push_back("CRVPT 21,  91, 1.94286,   7.26006");
  crv.push_back("CRVPT 21,  92, 1.95714,   7.22609");
  crv.push_back("CRVPT 21,  93, 1.97143,   7.19225");
  crv.push_back("CRVPT 21,  94, 1.98571,   7.15855");
  crv.push_back("CRVPT 21,  95, 2.00000,   7.12500");
  crv.push_back("CRVPT 21,  96, 2.01429,   7.09159");
  crv.push_back("CRVPT 21,  97, 2.02857,   7.05832");
  crv.push_back("CRVPT 21,  98, 2.04286,   7.02519");
  crv.push_back("CRVPT 21,  99, 2.05714,   6.99221");
  crv.push_back("CRVPT 21, 100, 2.07143,   6.95936");
  crv.push_back("CRVPT 21, 101, 2.08571,   6.92666");
  crv.push_back("CRVPT 21, 102, 2.10000,   6.89410");
  crv.push_back("CRVPT 21, 103, 2.11429,   6.86168");
  crv.push_back("CRVPT 21, 104, 2.12857,   6.82941");
  crv.push_back("CRVPT 21, 105, 2.14286,   6.79727");
  crv.push_back("CRVPT 21, 106, 2.15714,   6.76528");
  crv.push_back("CRVPT 21, 107, 2.17143,   6.73343");
  crv.push_back("CRVPT 21, 108, 2.18571,   6.70172");
  crv.push_back("CRVPT 21, 109, 2.20000,   6.67016");
  crv.push_back("CRVPT 21, 110, 2.21429,   6.63873");
  crv.push_back("CRVPT 21, 111, 2.22857,   6.60745");
  crv.push_back("CRVPT 21, 112, 2.24286,   6.57631");
  crv.push_back("CRVPT 21, 113, 2.25714,   6.54531");
  crv.push_back("CRVPT 21, 114, 2.27143,   6.51445");
  crv.push_back("CRVPT 21, 115, 2.28571,   6.48374");
  crv.push_back("CRVPT 21, 116, 2.30000,   6.45316");
  crv.push_back("CRVPT 21, 117, 2.31429,   6.42273");
  crv.push_back("CRVPT 21, 118, 2.32857,   6.39244");
  crv.push_back("CRVPT 21, 119, 2.34286,   6.36230");
  crv.push_back("CRVPT 21, 120, 2.35714,   6.33229");
  crv.push_back("CRVPT 21, 121, 2.37143,   6.30243");
  crv.push_back("CRVPT 21, 122, 2.38571,   6.27270");
  crv.push_back("CRVPT 21, 123, 2.40000,   6.24312");
  crv.push_back("CRVPT 21, 124, 2.41429,   6.21369");
  crv.push_back("CRVPT 21, 125, 2.42857,   6.18439");
  crv.push_back("CRVPT 21, 126, 2.44286,   6.15524");
  crv.push_back("CRVPT 21, 127, 2.45714,   6.12622");
  crv.push_back("CRVPT 21, 128, 2.47143,   6.09735");
  crv.push_back("CRVPT 21, 129, 2.48571,   6.06863");
  crv.push_back("CRVPT 21, 130, 2.50000,   6.04004");

  for( int i= 0; i < (int)crv.size(); ++i ) {
    clog<<"UFLS340Config::setCrvTReCS> submit: "<<crv[i]<<endl;
    _devIO->submit(crv[i], 0.5);
  }
  /*
  for( int i= 1; i <= 200; ++i ) {
    strstream s;
    s<<"CRVPT? 21, "<<i<<ends;
    string q = s.str(); delete s.str();
    string reply;
    _devIO->submit(q, reply, 0.1);
    clog<<"UFLS340Config::setCrvTReCS> reply: "<<i<<": "<<reply<<endl;
  }
  */
  string use21 = "INCRV A, 21";
  clog<<"UFLS340Config::setCrvTReCS> submit "<<use21<<endl;
  _devIO->submit(use21, 0.5);
  //string sav21 = "CRVSAV";
  //clog<<"UFLS340Config::setCrvTReCS> submit (and wait 10 sec.) "<<sav21<<endl;
  //_devIO->submit(sav21, 10.0);
  setInTypTReCS();
  clog<<"UFLS340Config::setCrvTReCS> all done. "<<endl;
  return (int)crv.size();

}
#endif // __UFLS340Config_cc__

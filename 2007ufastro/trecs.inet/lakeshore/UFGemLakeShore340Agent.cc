#if !defined(__UFGemLakeShore340Agent_cc__)
#define __UFGemLakeShore340Agent_cc__ "$Name:  $ $Id: UFGemLakeShore340Agent.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFGemLakeShore340Agent_cc__;

#include "UFGemLakeShore340Agent.h"
#include "UFLS340Config.h"

string UFGemLakeShore340Agent::_currentvalA;
string UFGemLakeShore340Agent::_currentvalB;
float UFGemLakeShore340Agent::_setpval = -1.0;
string UFGemLakeShore340Agent::_setpvalCmd = "SETP 1, 7.0";;
string UFGemLakeShore340Agent::_setPointRec;  
string UFGemLakeShore340Agent::_simkelvin1;  
string UFGemLakeShore340Agent::_simkelvin2;  
time_t UFGemLakeShore340Agent::_timeOut = 180; // seconds
time_t UFGemLakeShore340Agent::_clock = 0; // seconds
bool UFGemLakeShore340Agent::_setcrv = false; // set TReCS temp. calibration curve
bool UFGemLakeShore340Agent::_setintyp = false; // set TReCS temp. calibration amps/volts input

//public:
// ctors:
UFGemLakeShore340Agent::UFGemLakeShore340Agent(int argc,char** argv) : UFGemDeviceAgent(argc, argv) {
  // use ancillary function to retrieve LS340 readings
  _flush = 0.05; // 0.1; // 0.35; // inherited value is not quite rigth for the 340
  bool *bp = new bool;
  *bp = false; // arg to ancillary func. indicates whatever
  UFRndRobinServ::_ancil = (void*) bp; // non-null indicate acillary func. should be invoked
  _confGenSub = _epics + ":ec:configG";
  _heart = _epics + ":ec:ls340Heartbeat";
  _statRec = _epics + ":ec:arrayG.J"; //
  _simkelvin1 = "";
}

UFGemLakeShore340Agent::UFGemLakeShore340Agent(const string& name, int argc,
				               char** argv) : UFGemDeviceAgent(name, argc, argv) {
  _flush = 0.05; //  0.1; //0.35;
  // use ancillary function to retrieve LS340 readings
  bool *bp = new bool;
  *bp = false; // arg to ancillary func. indicates whatever
  UFRndRobinServ::_ancil = (void*) bp; // non-null indicate acillary func. should be invoked
  _confGenSub = _epics + ":ec:configG";
  _heart = _epics + ":ec:ls340Heartbeat";
  _statRec = _epics + ":ec:arrayG.J"; // 
  _simkelvin1 = "";
}

// static funcs:
int UFGemLakeShore340Agent::main(int argc, char** argv) {
  UFGemLakeShore340Agent ufs("UFGemLakeShore340Agent", argc, argv); // ctor binds to listen socket
  ufs.exec((void*)&ufs);
  return argc;
}

// public virtual funcs:
string UFGemLakeShore340Agent::newClient(UFSocket* clsoc) {
  string clname = greetClient(clsoc, name()); // concat. the client's name & timestamp
  if( clname == "" )
    return clname; // badly behaved client rejected...

  if( clname.find("trecs:") != string::npos )
    _epics = "trecs";
  else if( clname.find("miri:") != string::npos )  
    _epics = "miri";
  else if( clname.find("flam:") != string::npos )  
    _epics = "flam";

  if( !( _epics == "trecs" || _epics == "miri" || _epics == "flam") )
    return clname;

  int pos = _confGenSub.find(":");
  string genSub = _confGenSub.substr(pos);
  string conf =  _epics + genSub + ".VALA";
  string periods =  _epics + genSub  + ".VALB";
  string hearts =  _epics + genSub + ".VALC";
  // get hearbeat recs. first:
  if( _verbose )
    clog<<"UFGemLakeShore340Agent::newClient> Epics client (I think) fetching rec. list from:"
        <<conf<<endl;

  // this is a deferred (post boot, one time) init.
  /*
  if( _StatList.size() != 0 ) { // need only do this on first epics connect...
    int nr = getEpicsOnBoot(_epics, hearts, _StatList);
    if( nr > 0 && _StatList.size() > 0 ) {
      _heart = _StatList[0]; // assume 340 is first in list
      if( _verbose ) {
        clog<<"UFGemLakeShore340Agent::newClient> Epics heartbeat list ("<<nr<<"): "<<endl;
        for( int i = 0; i < nr; ++i )
          clog<<"UFGemLakeShore340Agent::newClient> "<<_StatList[i]<<endl;
      }
    }

    nr = getEpicsOnBoot(_epics, periods, _StatList);
    if( nr > 0 && _StatList.size() > 0 ) {
      _Update = atof(_StatList[0].c_str()); // assume minimum is first in list
      if( _verbose ) {
        clog<<"UFGemLakeShore340Agent::newClient> Epics update periods list ("<<nr<<"): "<<endl;
        for( int i = 0; i < nr; ++i )
          clog<<"UFGemLakeShore340Agent::newClient> "<<_StatList[i]<<endl;
      }
    }
    // and lastly, the config info should be preserved in StatList:
    nr = getEpicsOnBoot(_epics, conf, _StatList);
    if( nr <= 0 || _StatList.size() <= 0 )
      clog<<"UFGemLakeShore340Agent::newClient> Epics connection failed. "<<endl;
    else {
      _statRec = _StatList[0];
      if( _verbose ) {
        clog<<"UFGemLakeShore340Agent::newClient> Epics client recs. list ("<<nr<<"): "<<endl;
        for( int i = 0; i < nr; ++i )
          clog<<"UFGemLakeShore340Agent::newClient> "<<_StatList[i]<<endl;
      }
    }
  }
  */
  if( _epics != "false" && _capipe == 0 ) {
    _capipe = pipeToEpicsCAchild(_epics);
    if( _capipe && _verbose )
      clog<<"UFGemLakeShore340Agent> Epics CA child proc. started"<<endl;
    else if( _capipe == 0 )
      clog<<"UFGemLakeShore340Agent> Ecpics CA child proc. failed to start"<<endl;
  }

  return clname;  
}

// this should always return the service/agent listen port #
int UFGemLakeShore340Agent::options(string& servlist) {
  int port = UFDeviceAgent::options(servlist);
  // _epics may have been reset
  _confGenSub = _epics + ":ec:configG";
  _heart = _epics + ":ec:ls340Heartbeat";
  _statRec = _epics + ":ec:arrayG.J"; // 

  _config = new UFLS340Config(); // needs to be proper device subclass (LS340)
  // 340 defaults for trecs:
  _config->_tsport = 7003;
  _config->_tshost = "192.168.111.101";

  string arg;
  arg = findArg("-sim"); // epics host/db name
  if( arg == "true" ) {
    _sim = true;
    _simkelvin1 = "+301.111E+0;+302.222E+0";
    _simkelvin2 = "+299.111E+0;+298.222E+0";
  }
  else if( arg == "cold" ) {
    _sim = true;
    _simkelvin1 = "+5.111E+0;+5.222E+0";
    _simkelvin2 = "+5.044E+0;+5.155E+0";
  }
  else if( arg != "false" ) {
    _sim = true;
    _simkelvin1 = arg;
    _simkelvin2 = arg;
  }

  arg = findArg("-noepics"); // epics host/db name
  if( arg == "true" ) {
    _epics = "false";
  }

  arg = findArg("-epics"); // epics host/db name
  if( arg != "false" && arg != "true" ) {
    _epics = arg;
    _confGenSub = _epics + ":ec:configG";
    _heart = _epics + ":ec:ls340Heartbeat";
    _statRec = _epics + ":ec:arrayG.J"; // 
  }

  arg = findArg("-heart");
  if( arg != "false" && arg != "true" )
    _heart = arg;

  arg = findArg("-conf");
  if( arg != "false" && arg != "true" )
    _confGenSub = arg;

   arg = findArg("-tsport"); 
  if( arg != "false" && arg != "true" )
    _config->_tsport = atoi(arg.c_str());

  arg = findArg("-tshost");
  if( arg != "false" && arg != "true" )
    _config->_tshost = arg;
  
  arg = findArg("-flush"); 
  if( arg != "false" && arg != "true" )
    _flush = atof(arg.c_str());

  arg = findArg("-crv"); 
  if( arg != "false" )
    _setcrv = true;;

  arg = findArg("-setcrv"); 
  if( arg != "false" )
    _setcrv = true;

  arg = findArg("-intyp"); 
  if( arg != "false" )
    _setintyp = true;
 
  arg = findArg("-setintyp"); 
  if( arg != "false" )
    _setintyp = true;

  arg = findArg("-v");
  if( arg != "false" )
    _verbose = true;

  arg = findArg("-c");
  if( arg != "false" )
    _confirm = true;
  arg = findArg("-g");
  if( arg != "false" )
    _confirm = true;

  if( _config->_tsport ) {
    port = 52000 + _config->_tsport - 7000;
  }
  else {
    port = 52003;
  }
  if( _verbose ) 
    clog<<"UFGemLakeShore340Agent::options> set port to GemLakeShore340 port "<<port<<endl;

  return port;
}

void UFGemLakeShore340Agent::startup() {
  setSignalHandler(UFGemLakeShore340Agent::sighandler);
  clog << "UFGemLakeShore340Agent::startup> established signal handler."<<endl;
  // should parse cmd-line options and start listening
  string servlist;
  int listenport = options(servlist);
  // agent that actually uses a serial device
  // attached to the annex or iocomm should initialize
  // a connection to it here:
  if( _config == 0 ) {
    clog<<"UFGemLakeShore340Agent::startup> no DeviceConfig, assume simulation? "<<endl;
    init();
  }
  else
    init(_config->_tshost, _config->_tsport);

  UFSocketInfo socinfo = _theServer.listen(listenport);
  clog << "UFGemLakeShore340Agent::startup> Ok, startup completed, listening on port= " <<listenport<<endl;
  return;  
}

UFTermServ* UFGemLakeShore340Agent::init(const string& tshost, int tsport) {
  // connect to the device:
  UFTermServ* ts = 0;
  if( !_sim ) {
    ts =_config->connect(tshost, tsport);
    if( _config->_devIO == 0 ) {
      clog<<"UFGemLakeShore340Agent> failed to connect to device port..."<<endl;
      return 0;
    }
    if( _setintyp )
      UFLS340Config::setInTypTReCS();
    if( _setcrv )
      UFLS340Config::setCrvTReCS();
  }
  // test connectivity to lakeshore:
  string lakeshore; // lakeshore cmd & reply strings
  lakeshore = "+301.111E+0;+302.222E+0";
  if( _simkelvin1 != "" ) 
    lakeshore = _simkelvin1;
  
  string allkelvin = "KRDG?A;KRDG?B"; // should be function in UFDeviceConfig
  // save transaction to a file
  /*
  pid_t p = getpid();
  strstream s;
  s<<"/tmp/.lakeshore340."<<p<<ends;
  int fd = ::open(s.str(), O_RDWR | O_CREAT, 0777);
  if( fd <= 0 ) {
    clog<<"UFGemLakeShore340Agent> unable to open "<<s.str()<<endl;
    delete s.str();
    return ts;
  }
  delete s.str();
  */
  if( !_sim ) {
    clog<<"UFGemLakeShore340Agent> checking availiblity of lakeshore 340 channels A, B."<<endl;
    int nc = _config->_devIO->submit(allkelvin, lakeshore, _flush);
    if( nc <= 0 ) {
      clog<<"UFGemLakeShore340Agent> lakeshore340 querry all kelvin failed..."<<endl;
      delete _config->_devIO; _config->_devIO= 0;
      return 0;
    }
    clog<<"UFGemLakeShore340Agent> reply: "<<lakeshore<<endl;
    // also set the desired control loop ID for all subsequent control commands:
    string cmode = "cmode?0";
    nc = _config->_devIO->submit(cmode, lakeshore, _flush);
    //nc = ::write(fd, lakeshore.c_str(), lakeshore.length());
    cmode = "cmode 1,1"; // no reply expected
    nc = _config->_devIO->submit(cmode, _flush);
    //nc = ::write(fd, lakeshore.c_str(), lakeshore.length());
    clog<<"UFGemLakeShore340Agent> set control mode: "<<cmode<<endl;
    // also set the desired control loop parameters:
    string cset = "cset?1";
    nc = _config->_devIO->submit(cset, lakeshore, _flush);
    //nc = ::write(fd, lakeshore.c_str(), lakeshore.length());
    cset = "cset 1,A,1,1,1"; // no reply expected
    nc = _config->_devIO->submit(cset, _flush);
    //nc = ::write(fd, lakeshore.c_str(), lakeshore.length());
    clog<<"UFGemLakeShore340Agent> set control parameter: "<<cset<<endl;
    string pid = "pid?1";
    nc = _config->_devIO->submit(pid, lakeshore, _flush);
    //nc = ::write(fd, lakeshore.c_str(), lakeshore.length());
    pid = "pid 1, 1000.0, 500.0, 1000.0";
    nc = _config->_devIO->submit(pid, _flush);
    //nc = ::write(fd, lakeshore.c_str(), lakeshore.length());
  }
  else {
    clog<<"UFGemLakeShore340Agent> simulate 340 two channels: "<<lakeshore<<endl;
  }
  /* 
  int nc = ::write(fd, lakeshore.c_str(), lakeshore.length());
  if( nc <= 0 ) {
    clog<<"UFGemLakeShore340Agent> status log failed..."<<endl;
  }
  ::close(fd);
  */
  return ts;
}

string UFGemLakeShore340Agent::_simsetp() {
  double setpnt = _setpval;
  char v[8], v1[8], v2[8];
  ::memset(v, 0, sizeof(v)); ::memset(v1, 0, sizeof(v1)); ::memset(v2, 0, sizeof(v2));
  // presumably simulation mode allows any non-physical setp?
  if( setpnt < 10 ) {
   ::sprintf(v, "%4.2f", setpnt); 
    ::sprintf(v1, "%4.2f", (setpnt+0.04)); ::sprintf(v2, "%4.2f", (setpnt-0.04)); 
  }
  else if( setpnt < 100 ) {
    ::sprintf(v, "%5.2f", setpnt); 
    ::sprintf(v1, "%5.2f", (setpnt+0.04)); ::sprintf(v2, "%5.2f", (setpnt-0.04)); 
  }
  else if( setpnt < 1000 ) {
    ::sprintf(v, "%6.2f", setpnt); 
    ::sprintf(v1, "%6.2f", (setpnt+0.04)); ::sprintf(v2, "%6.2f", (setpnt-0.04)); 
  }
  strstream s1, s2;
  s1 << "+" << v << "E+0;+" << v << "E+0" << ends;
  s2 << "+" << v1 << "E+0;+" << v2 << "E+0" << ends;
  string reply340 = _simkelvin1 = s1.str(); delete s1.str();
  _simkelvin2 = s2.str(); delete s2.str();
  //if( _verbose ) 
     // clog<<"UFGemLakeShore340Agent::ancillary> (sim setpnt) "<< reply340 <<endl;
  return reply340;
}

void* UFGemLakeShore340Agent::ancillary(void* p) {
  bool block = false;
  if( p )
    block = *((bool*)p);

  string reply340 = "+301.111E+0;+302.222E+0";
  if( _sim && _setpval > 0 ) 
    reply340 = _simsetp();

  if( _simkelvin1 != "" ) 
    reply340 = _simkelvin1;
  if( _DevHistory.size() % 2 == 0 ) {
    if( _simkelvin2 != "" ) 
      reply340 = _simkelvin2;
    else
      reply340 = "+299.111E+0;+298.222E+0";
  }

  if( !_sim && _config && _config->_devIO ) // not simulation
    _config->_devIO->submit("KRDG?A;KRDG?B", reply340, _flush); // expect single line reply
  if( _verbose )
    clog<<"UFGemLakeShore340Agent::ancillary> new reading: "<<reply340<<endl;

  // check the max history count, and decrement by two if limit has been reached
  // this insures that the modula 2 logic above continues properly
  if( _DevHistory.size() > _DevHistSz ) { // should be configurable size
    _DevHistory.pop_front();  _DevHistory.pop_front();
  }

  // insert value into history
  _DevHistory.push_back(reply340);
    
  if( _epics != "false" && _capipe == 0 ) {
    _capipe = pipeToEpicsCAchild(_epics);
    if( _capipe && _verbose )
      clog<<"UFGemLakeShore340Agent> Epics CA child proc. started"<<endl;
    else if( _capipe == 0 )
      clog<<"UFGemLakeShore340Agent> Ecpics CA child proc. failed to start"<<endl;
  }

  // update the heartbeat:
  time_t clck = time(0);
  if( _capipe && _heart != "" ) {
    strstream s; s<<clck<<ends; string val = s.str(); delete s.str();
    updateEpics(_heart, val);
  }

  // reply must be parsed and formatted into vector
  // eliminate all "+" and ,E+N" parts: "+304.173E+0;+304.410E+0"
  // since 340 only has 2 channels, don't bother with loop:
  vector< string > v;
  int posEa= reply340.find("E+");
  int posEb= reply340.rfind("E+");
  int posa= 1, posb= 5 + posEa;
  string a= reply340.substr(posa, posEa-1);
  string b= reply340.substr(posb, posEb-posb);
  // presumably A is the array's temperature, and it gets special treatment
  _currentvalA = a;
  _currentvalB = b;
  v.push_back(a);
  v.push_back(b);

  if( _statRec== "" && _StatList.size() > 0 ) {
    // assume 340 values are in [0]
    _statRec = _StatList[0];
  }
  if( _capipe && _statRec != "" )
    updateEpics(_statRec, v);

  // for set point "command completion"
  if( _capipe && _setPointRec != "" && _setpval > 0 ) {
    sendEpics(_setPointRec, _currentvalA);
    double setpval = _setpval;
    double currentval = atof(_currentvalA.c_str());
    double diff = fabs(currentval - setpval);
    if( _verbose )
      clog<<"UFGemLakeShore340Agent::ancillary> setpval, currentval (A), diff: "<<setpval<<", "
          <<currentval<<", "<<diff<<endl;
    if( diff <= 0.05 ) {
      sendEpics(_setPointRec, "OK");
      _setPointRec = "";
      clog<<"UFGemLakeShore340Agent::ancillary> OK. setpval, currentval (A), diff: "<<setpval<<", "
          <<currentval<<", "<<diff<<endl;
    }
    else if( (clck - _clock) > _timeOut ) {
      _setPointRec = "";
      clog<<"UFGemLakeShore340Agent::ancillary> TimedOut! setpval, currentval, diff: "<<setpval<<", "
          <<currentval<<", "<<diff<<", has timed out!"<<endl;
    }
  } 
  
  return p;
}

void UFGemLakeShore340Agent::hibernate() {
  if( _queued.size() == 0 && UFRndRobinServ::theConnections->size() == 0) {
    // no connections, no queued reqs., go to sleep
    sleep(_Update); 
  }
  else
    UFSocket::waitOnAll(_Update);
    // sleep(_Update); 
}

// these are the key virtual functions to override,
// send command string to TermServ, and return response
// presumably any allocated memory is freed by the calling layer....
// for the moment assume "raw" commands and simply pass along
// this version must always return null to client, and
// instead send the command completion status to the designated CAR,
// if no CAR is desginated, an error /alarm condition should be indicated
int UFGemLakeShore340Agent::action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& rep) {
  int stat = 0;
  bool reply_client= true;
  rep = new vector< UFProtocol* >;
  vector< UFProtocol* >& replies = *rep;;

  if( act->clientinfo.find("trecs:") != string::npos ||
      act->clientinfo.find("miri:") != string::npos ) {
    reply_client = false;
    clog<<"UFGemLakeShore340Agent::action> No replies will be sent to Gemini/Epics client: "
        <<act->clientinfo<<endl;
  }
  if( _verbose ) {
    clog<<"UFGemLakeShore340Agent::action> client: "<<act->clientinfo<<endl;
    for( int i = 0; i<(int)act->cmd_name.size(); ++i ) 
      clog<<"UFGemLakeShore340Agent::action> elem= "<<i<<" "<<act->cmd_name[i]<<" "<<act->cmd_impl[i]<<endl;
  }
   
  string car= "", errmsg= "", hist= "";
  for( int i = 0; i < (int)act->cmd_name.size(); ++i ) {
    bool sim = _sim;
    bool reqsetp = false;
    string cmdname = act->cmd_name[i];
    string cmdimpl = act->cmd_impl[i];

    //if( _verbose ) {
      clog<<"UFGemLakeShore340Agent::action> cmdname: "<<cmdname<<endl;
      clog<<"UFGemLakeShore340Agent::action> cmdimpl: "<<cmdimpl<<endl;
      //}

    if( cmdname.find("sta") == 0 || cmdname.find("Sta") == 0 || cmdname.find("STA") == 0 ) {
      // presumably a non-epics client has requested status info (generic of FITS)
      // assume for now that it is OK to return upon processing the status request
      if( cmdimpl.find("fit") != string::npos ||
	  cmdimpl.find("Fit") != string::npos || cmdimpl.find("FIT") != string::npos  ) {
	replies.push_back(_config->statusFITS(this)); 
        return (int) replies.size(); 
      }
      else {
	replies.push_back(_config->status(this)); 
        return (int) replies.size(); 
      }
    }
    else if( cmdname.find("car") == 0 ||
	cmdname.find("Car") == 0 ||
        cmdname.find("CAR") == 0 ) {
      reply_client= false;
      car = cmdimpl;
      if( _verbose ) clog<<"UFGemLakeShore340Agent::action> CAR: "<<car<<endl;
      continue;
    }
    else if( cmdname.find("caput") == 0 ||
	     cmdname.find("Caput") == 0 ||
	     cmdname.find("CaPut") == 0 || 
             cmdname.find("CAPUT") == 0 ) {
      if( _verbose ) clog<<"UFGemLakeShore340Agent::action> "<<cmdname<<" "<<cmdimpl<<endl;
      if( _epics != "false" && _capipe ) sendEpics(cmdimpl);
      delete rep; rep = 0;
      return 0;      
    }
    else if( cmdname.find("his") == 0 ||
	     cmdname.find("His") == 0 || 
	     cmdname.find("HIS") == 0 ) {
      hist = cmdimpl;
      int errIdx = cmdname.find("!!");
      if( errIdx > 0 ) 
        errmsg = cmdname.substr(2+errIdx);
      if( _verbose ) 
	clog<<"UFGemLakeShore340Agent::action> History: "<<cmdimpl<<"; errmsg= "<<errmsg<<endl;
    }
    else if( cmdname.find("sim") == 0 ||
	     cmdname.find("Sim") == 0 || 
	     cmdname.find("SIM") == 0 ) {
      sim = true;
      int errIdx = cmdname.find("!!");
      if( errIdx > 0 ) 
        errmsg = cmdname.substr(2+errIdx);
      if( _verbose ) 
	clog<<"UFGemLakeShore340Agent::action> Sim: "<<cmdimpl<<"; errmsg= "<<errmsg<<endl;
    }
    // if a sim or non-sim setpoint req... make a note of it
    else if( cmdname.find("raw") == 0 ||
	     cmdname.find("Raw") == 0 || 
	     cmdname.find("RAW") == 0 ) {
      int errIdx = cmdname.find("!!");
      if( errIdx > 0 ) 
        errmsg = cmdname.substr(2+errIdx);
      if( _verbose ) 
	clog<<"UFGemLakeShore340Agent::action> Raw: "<<cmdimpl<<"; errmsg= "<<errmsg<<endl;
      if( cmdimpl.find("SETP") != string::npos || cmdimpl.find("SetP") != string::npos 
          || cmdimpl.find("setp") != string::npos && cmdimpl.find("?") == string::npos ) {
        // was not a query
	reqsetp = true;
	if( _verbose )
          clog<<"UFGemLakeShore340Agent::action> req. setp: "<<cmdimpl<<endl;
        _setPointRec = car;
        int vpos = (int) cmdimpl.find(","); // "SETP 1, 7.0" where 1 is "loop Id"
        if( vpos != (int)string::npos ) {
          _clock = time(0); // to be used for temp. settle timeout
          double oldsetp = _setpval;
          string setpstr = cmdimpl.substr(1+vpos);
          double setp = atof(setpstr.c_str());
          clog<<"UFGemLakeShore340Agent::action> req. setp: "<<setpstr<<" == "<<setp<<endl;
          if( setp > UFLS340Config::_MaxSetPnt ) {
            clog<<"UFGemLakeShore340Agent::action> req. setpval exceeds max allowed: "<<UFLS340Config::_MaxSetPnt<<endl;
            if( oldsetp < 0 ) {
              _setpval = UFLS340Config::_NominalSetPnt;
              clog<<"UFGemLakeShore340Agent::action> changing req.: "<<setp<<", to nominal: "<<_setpval;
            }
            else { // use old value
             _setpval = oldsetp;
             clog<<"UFGemLakeShore340Agent::action> changing req.: "<<setp<<", to prev./old value: "<<_setpval<<endl;
            }
          }
	  else {
            _setpval = setp;
	  }
          clog<<"UFGemLakeShore340Agent::action> req. setpval, oldval: "<<_setpval<<", "<<oldsetp<<endl;
        }
        if( _verbose )
          clog<<"UFGemLakeShore340Agent::action> setpval, currentvals: "<<_setpval
              <<", A: "<<_currentvalA<<", B: "<<_currentvalB<<endl;
      }
    }
    else {
      clog<<"UFGemLakeShore340Agent::action> ?improperly formatted request?"<<endl;
      delete rep; rep = 0;
      return 0;      
    }

    string reply340= "";
    int nr = _config->validCmd(cmdimpl);
    if( nr == UFLS340Config::_BadSetPnt ) { // if this is first time ever, set whatever was determined above
      strstream s;
      s << "SETP 1, "<<_setpval<<ends;
      cmdimpl = s.str(); delete s.str();
      reply340 = "Err: Bad SetP! Using: " + cmdimpl;
      act->cmd_reply.push_back(reply340);
      clog<<"UFGemLakeShore340Agent::action> "<<reply340<<endl;
      nr = 0; // proceed with default
    }
    else if( nr == UFLS340Config::_BadPID ) { // if this is first time ever, set whatever was determined above
      reply340 = "Err: Bad PID! Using: " + cmdimpl;
      clog<<"UFGemLakeShore340Agent::action> "<<reply340<<endl;
      act->cmd_reply.push_back(reply340);
    }
    // use the annex/iocomm port to submit the action command and get reply:
    act->time_submitted = currentTime();
    _active = act;
    if( hist != "" ) { // history
      int last = (int)_DevHistory.size() - 1;
      int cnt = atoi(hist.c_str());
      if( cnt <= 0 ) 
        cnt = 1;
      else if( cnt > last )
	cnt = 1+last;
      for( int i = 0; i < cnt; ++i )
        act->cmd_reply.push_back(_DevHistory[last - i]);
    }
    else if( !sim && _config != 0 && _config->_devIO ) {
      if( _verbose ) clog<<"UFGemLakeShore340Agent::action> submit raw: "<<cmdimpl<<endl;
      if( nr < 0 ) { // assume history requested
        int last = (int)_DevHistory.size() - 1;
        int cnt = atoi(cmdimpl.c_str());
        if( cnt <= 0 )
	  cnt = 1;
	else if( cnt > last )
	  cnt = 1+last;
        else if( cmdimpl.find("all") != string::npos || 
		 cmdimpl.find("All") != string::npos ||
		 cmdimpl.find("All") != string::npos )
	  cnt = last;
        for( int i = 0; i < cnt; ++i )
          act->cmd_reply.push_back(_DevHistory[last - i]);
      }
      else if( nr == 0 ) { // no reply expected
	if( reply340 == "" ) reply340 = "OK: " + cmdimpl;
        stat = _config->_devIO->submit(cmdimpl, _flush);
        if( _verbose ) clog<<"UFGemLakeShore340Agent::action> reply340: "<<reply340<<endl;
        if( _DevHistory.size() >= _DevHistSz ) _DevHistory.pop_front();
        _DevHistory.push_back(reply340);
        if( stat < 0 ) {
	  errmsg = "Failed: " + cmdimpl;
	  //if( car != "" ) setCARError(car, &errmsg); // set error
	  if( _epics != "false" && _capipe &&  car != "" ) sendEpics(car, errmsg);
          if( _verbose )
	  clog<<"UFGemLakeShore340Agent::action> cmd error condition..."<<endl;
          delete rep; rep = 0;
	  return 0;
	}
      }
      else if( nr != UFLS340Config::_BadSetPnt) {
        stat = _config->_devIO->submit(cmdimpl, reply340, _flush); // expect single line reply?
        if( _verbose ) clog<<"UFGemLakeShore340Agent::action> reply340: "<<reply340<<endl;
        if( stat < 0 ) {
	  //if( car != "" ) setCARError(car, &errmsg); // set error
	  if( _epics != "false" && _capipe && car != "" ) sendEpics(car, errmsg);
          if( _verbose )
	  clog<<"UFGemLakeShore340Agent::action> cmd error condition..."<<endl;
          delete rep; rep = 0;
	  return 0;
	}
      }
    } // non-sim
    else if( sim ) {
      errmsg += cmdimpl;
      // "+301.111E+0;+302.222E+0"  LS340 format
      //int asim = 300*rand() / RAND_MAX;
      //int bsim = 300*rand() / RAND_MAX;
      //strstream s;
      //s<<asim<<".111E+0;"<<bsim<<".222E+0"<<ends;
      //reply340 = s.str(); delete s.str();
      int chan= 0; // 0 for both; 1 for a; 2 for b
      const char* cs = cmdimpl.c_str();
      size_t qpos1 = cmdimpl.find("?");
      if(qpos1 != string::npos && cs[1+qpos1] == 'a' || cs[1+qpos1] == 'A')
        chan = 1;
      if(qpos1 != string::npos && cs[1+qpos1] == 'b' || cs[1+qpos1] == 'B')
        chan = 2;
      size_t qpos2 = cmdimpl.find("?", 1+qpos1);
      if(qpos2 != string::npos) // 2 ? indicates query for both chan
        chan = 0;

      reply340 = "+301.111E+0;+302.222E+0"; // chan a & chan b
      if( reqsetp ) 
        reply340 = _simsetp();

      if( _simkelvin1 != "" )
        reply340 = _simkelvin1;

      if( _DevHistory.size() % 2 == 0 ) {
        if( _simkelvin2 != "" ) 
          reply340 = _simkelvin2;
        else
          reply340 = "+299.111E+0;+298.222E+0";
      }

      size_t qposa = reply340.find(";");
      size_t qposb = 1+qposa;

      switch(chan) {
      case 2: // b
        reply340 = reply340.substr(qposb);
	break;
      case 1: // a
        reply340 = reply340.substr(0,qposa);
	break;
      case 0: // both
      default:
	break;
      }

      if( _verbose ) clog<<"UFGemLakeShore340Agent::action> sim. reply340: "<<reply340<<endl;
      // simulate/force an error state transition:
      if( cmdimpl.find("err") != string::npos || 
	  cmdimpl.find("Err") != string::npos || 
	  cmdimpl.find("ERR") != string::npos  ) {
        //if( car != "" ) setCARError(car, &errmsg); // set error
	if( _epics != "false" && _capipe && car != "" ) sendEpics(car, errmsg);
        if( _verbose )
	  clog<<"UFGemLakeShore340Agent::action> sim. error condition..."<<endl;
        delete rep; rep = 0;
	return 0;
      }
    } // sim

    if( _DevHistory.size() >= _DevHistSz ) { 
      _DevHistory.pop_front();
      _DevHistory.pop_front();
    }
    _DevHistory.push_back(reply340);
    if( hist == "" && reply340 != "" ) act->cmd_reply.push_back(reply340);
  } // end for loop of cmd bundle

  if( stat < 0 || act->cmd_reply.empty() ) {
    // send error (val=3) & error message to _capipe
    if( _verbose )
      clog<<"UFGemLakeShore340Agent::action> cmd. failed, set CAR to Err... "<<endl;
    act->status_cmd = "failed";
    //if( car != "" ) sendEpicsCAR(car, 3, &errmsg);
    if( _epics != "false" && _capipe && car != "" ) sendEpics(car, errmsg);
  }
  else {
    // send success (idle val = 0) to _capipe
    if( _verbose )
      clog<<"UFGemLakeShore340Agent::action> cmd. complete, set CAR to idle... "<<endl;
    act->status_cmd = "succeeded";
    //if( car != "" ) setCARIdle(car);
    if( car != "" && _currentvalA != "" ) sendEpics(car, _currentvalA);
  }  
  act->time_completed = currentTime();
  _completed.insert(UFDeviceAgent::CmdList::value_type(act->cmd_time, act));

  if( !reply_client ) {
    delete rep; rep = 0;
    return 0;
  }
  replies.push_back(new UFStrings(act->clientinfo, act->cmd_reply));
  return (int)replies.size();; // this should be freed by the calling func...
} 

int UFGemLakeShore340Agent::query(const string& q, vector<string>& qreply) {
  return UFDeviceAgent::query(q, qreply);
}

#endif // UFGemLakeShore340Agent

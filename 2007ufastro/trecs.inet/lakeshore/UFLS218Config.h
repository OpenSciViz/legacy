#if !defined(__UFLS218Config_h__)
#define __UFLS218Config_h__ "$Name:  $ $Id: UFLS218Config.h,v 0.1 2002/06/26 20:34:06 trecs beta $"
#define __UFLS218Config_H__(arg) const char arg##UFLS218Config_h__rcsId[] = __UFLS218Config_h__;

#include "UFDeviceConfig.h"

class UFDeviceAgent;

class UFLS218Config : public UFDeviceConfig {
public:
  UFLS218Config(const string& name= "UnknownLS218@DefaultConfig");
  UFLS218Config(const string& name, const string& tshost, int tsport);
  inline virtual ~UFLS218Config() {}

  // override these virtuals:
  virtual vector< string >& UFLS218Config::queryCmds();
  virtual vector< string >& UFLS218Config::actionCmds();
  // return -1 if invalid, 0 if valid but no reply expected,
  // n > 0 if valid and n reply strings expected:
  virtual int validCmd(const string& c);

  virtual string terminator();
  // the default behavior is ok here:
  //virtual string prefix();

  // overide these:
  virtual UFStrings* status(UFDeviceAgent* da);
  virtual UFStrings* statusFITS(UFDeviceAgent* da);
};

#endif // __UFLS218Config_h__

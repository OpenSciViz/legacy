#if !defined(__UFMCE4Config_cc__)
#define __UFMCE4Config_cc__ "$Name:  $ $Id: UFMCE4Config.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFMCE4Config_cc__;

#include "UFMCE4Config.h"
#include "UFGemMCE4Agent.h"
#include "UFDaemon.h"
#include "UFROpticsCoef.h"

#include "sys/types.h"
#include "sys/stat.h"
#include "unistd.h"
#include "sys/wait.h"
#include "fstream.h"

#define _MAX2_(a,b) ((a)>(b)) ? (a) : (b)
#define _MAX3_(a,b,c) (_MAX2_(_MAX2_(a,b),_MAX2_(b,c)))
#define _MIN2_(a,b) ((a)<(b)) ? (a) : (b)
#define _MIN3_(a,b,c) (_MIN2_(_MIN2_(a,b),_MIN2_(b,c)))

// statics
int UFMCE4Config::_minPBC=2;           //minimum allowed Pixel Base Clock
bool UFMCE4Config::_verbose= false;
//UFEdtDMA::Conf* UFMCE4Config::_edtcfg;
double UFMCE4Config::_SCSFreq= 0.0;  // _SCSFreq == 1.0 / ( 1/ChopFrequency - _SCSDelta)
double UFMCE4Config::_SCSDelta= 0.006;

double UFMCE4Config::_FrmDutyCycle = 1.0;
int UFMCE4Config::_wellDepth = 0;
int UFMCE4Config::_biasLevel = 0;
int UFMCE4Config::_VdetgrvDACval = 0; //must perform DAC bias read all voltages to get value.
bool UFMCE4Config::_idle = true;
// for unit tests:
// frame count anticipated from current observation (0 == infinite, ala edt/pdv start_dma):
int UFMCE4Config::_frmCnt= 0; 
// option to run ufgtake child with mce4 start/run directive
pid_t UFMCE4Config::_takepid = (pid_t)-1;

// (TReCS only):
UFMCE4Config::DACParms UFMCE4Config::_biasP; // DAC bias params (as provided by mce4 query)
UFMCE4Config::DACParms UFMCE4Config::_preamP; // DAC preamp params (as provided by mce4 query)

string UFMCE4Config::_des_obsmode, UFMCE4Config::_des_readoutmode;
string UFMCE4Config::_actv_obsmode, UFMCE4Config::_actv_readoutmode;

// hardware settings currently active
UFMCE4Config::HdwrParms UFMCE4Config::_activeH; 
// desired hadrware settings
UFMCE4Config::HdwrParms UFMCE4Config::_desH; 
// physical params associated with active hardware params
UFMCE4Config::PhysParms UFMCE4Config::_activeP; 
// physical params associated with desired hardware params
UFMCE4Config::PhysParms UFMCE4Config::_desP; 
// meta params associated with physical params
UFMCE4Config::TextParms UFMCE4Config::_metaP;
// basic (PBC=1) MCE frame times for each readout mode from method queryMCEtiming().
UFMCE4Config::PhysParms UFMCE4Config::_frameTimes;
// constant meta config params from method readMetaConfigFile().
UFMCE4Config::PhysParms UFMCE4Config::_metaConParms;

// 4 possible cycle types
UFMCE4Config::HdwrParms UFMCE4Config::_cycletypes;
UFMCE4Config::TextParms UFMCE4Config::_mceparms;

// FITS header keywords and comments corresponding to HdwrParms and PhysParams:
UFMCE4Config::TextParms UFMCE4Config::FITS_KeyWords;
UFMCE4Config::TextParms UFMCE4Config::FITS_Comments;

//polynomial coeffs to compute R_Sky of each filter,
// to estimate optimal detector array frame integration time.
// (see code in initRSky() and in eval(TextParms& mp, PhysParms& pp,...) ).
UFMCE4Config::RSkyCoefsMap UFMCE4Config::_RSkyCfsMap;

// Initialize a TReCS Raytheon Array pixel LookUpTable map.
// It is returned as UFInts object of correct size.
// Sort pixels using LHS:
// for (iPix=0; iPix<nPixPerBuf; iPix++) 
//    OutBufP[LUT[iPix]] = InBufP[iPix]; 
 
vector< UFInts* > UFMCE4Config::setRaytheonLUTs( int nCol, int nRow, int nChan ) {
  int nFast=nCol/nChan; 
  int ntPix = nCol*nRow;
  int* LUT = new int[ntPix];
  int* S1 = new int[ntPix];
  int* R1 = new int[ntPix];
  int* R2 = new int[ntPix];
  int* R3 = new int[ntPix];
  // Output Clamp and Column Clamp reference subtraction index maps for 4-point sampling:

  for (int iRow=0, iPix=0; iRow<nRow; iRow++)
    {
      int iS1 = 4*nCol*iRow;
      int iR1 = iS1 + nChan;
      int iR2 = iS1 + 2*nCol;
      int iR3 = iR2 + nChan;
      for (int iFast=0; iFast<nFast; iFast++)
	{
	  for (int iChan=0; iChan<nChan; iChan++, iPix++ )
	    { 
	      S1[iPix] = iS1++;
	      R1[iPix] = iR1++;
	      R2[iPix] = iR2++;
	      R3[iPix] = iR3++;
	      LUT[iPix] = iRow*nCol + iChan*nFast + iFast; 
	    } 
	  iS1 += nChan;
	  iR1 += nChan;
	  iR2 += nChan;
	  iR3 += nChan;
	}
    }

  // Output Clamp only reference subtraction index maps for 2-point sampling:
  int* S1OC = new int[ntPix];
  int* R1OC = new int[ntPix];

  for (int iRow=0, iPix=0; iRow<nRow; iRow++)
    {
      int iS = 2*nCol*iRow;
      int iR = iS + nChan;
      for (int iFast=0; iFast<nFast; iFast++)
	{
	  for (int iChan=0; iChan<nChan; iChan++, iPix++ )
	    { 
	      S1OC[iPix] = iS++;
	      R1OC[iPix] = iR++;
	    } 
	  iS += nChan;
	  iR += nChan;
	}
    }

  // Column Clamp only reference subtraction index maps for 2-point sampling:
  int* S1CR = new int[ntPix];
  int* R1CR = new int[ntPix];

  for( int iRow=0, iPix=0; iRow<nRow; iRow++ )
    {
      int iS = 2*nCol*iRow;
      int iR = iS + nCol;
      for( int iCol=0; iCol<nCol; iCol++, iPix++ )
	{
	  S1CR[iPix] = iS++;
	  R1CR[iPix] = iR++;
	}
    }

  //copy the arrays into UFInts objects and then delete arrays:
  vector< UFInts* > LUTs;
  LUTs.push_back( new UFInts("PIXELMAP for Raytheon 320x240 CRC-774 MUX", LUT, ntPix) ); delete LUT;
  LUTs.push_back( new UFInts("PIXELMAP S1", S1, ntPix) ); delete S1;
  LUTs.push_back( new UFInts("PIXELMAP R1", R1, ntPix) ); delete R1;
  LUTs.push_back( new UFInts("PIXELMAP R2", R2, ntPix) ); delete R2;
  LUTs.push_back( new UFInts("PIXELMAP R3", R3, ntPix) ); delete R3;
  LUTs.push_back( new UFInts("PIXELMAP S1OC", S1OC, ntPix) ); delete S1OC;
  LUTs.push_back( new UFInts("PIXELMAP R1OC", R1OC, ntPix) ); delete R1OC;
  LUTs.push_back( new UFInts("PIXELMAP S1CR", S1CR, ntPix) ); delete S1CR;
  LUTs.push_back( new UFInts("PIXELMAP R1CR", R1CR, ntPix) ); delete R1CR;
  return LUTs;
} 

//called first time by eval( TextParms& metaParms ):

bool UFMCE4Config::readMetaConfigFile()
{
  string metaConfigFile = string(::getenv("UFINSTALL")) + string("/etc/MetaConfigDet.txt");
  clog << "UFMCE4Config::readMetaConfigFile> opening file: " << metaConfigFile << endl;
  ifstream metaConFileStream( metaConfigFile.c_str() );
  _metaConParms.clear();
  bool fileOK = true;
  string key;
  while( getline( metaConFileStream, key ) > 0 )
    {
      if( key.length() > 1 ) {
	UFFITSheader::rmJunk( key ); //remove all blanks, \n, \r, \t.
	if( key.length() > 1 ) {
	  if( key.find("//") == string::npos ) {
	    string value;
	    getline( metaConFileStream, value );
	    UFFITSheader::rmJunk( value );
	    if( value.length() > 0 ) {
	      _metaConParms[key] = ::atof( value.c_str() );
	      clog<<"UFMCE4Config::readMetaConfigFile> "<<key<<" = "<<_metaConParms[key]<<endl;
	    }
	    else {
	      fileOK = false;
	      clog<<"UFMCE4Config::readMetaConfigFile> read key: "<<key
		  <<", with empty following value line."<<endl;
	    }
	  }
	}
      }
    }
  clog << "UFMCE4Config::readMetaConfigFile> closing file: " << metaConfigFile << endl;
  metaConFileStream.close();
  if( _metaConParms.size() <= 0 ) fileOK = false;
  return fileOK;
}

// initialize _RSkyCfsMap from _RSkyCoefs defined in UFRSkyCoefs.h

int UFMCE4Config::initRSky() {
  int Nfilter = sizeof(_RSkyCoefs)/sizeof(UFRSkyCoef);
  clog << "UFMCE4Config::initRSky> # of filters in struct = " << Nfilter << endl;
  for( int i=0; i<Nfilter; i++ ) _RSkyCfsMap[string(_RSkyCoefs[i].Filter)] = _RSkyCoefs[i];
  clog << "UFMCE4Config::initRSky> Loaded R-Sky model coefficients for Filters:" <<endl;;
  for( RSkyCoefsMap::iterator ptr = _RSkyCfsMap.begin(); ptr != _RSkyCfsMap.end(); ptr++ ) {
    UFRSkyCoef Rskycf = ptr->second;
    clog<<"  "<<ptr->first<<",  nV="<<Rskycf.nV<<",  nX="<<Rskycf.nX<<",  Altitude="<<Rskycf.AtranAlt<<endl;
  }
  return _RSkyCfsMap.size();
}
  
// helper for ctors:
int UFMCE4Config::clearAll() {
  clear(_activeH); clear(_activeP);
  clear(_desH); clear(_desP);
  _des_obsmode = "Stare"; _des_readoutmode = "S1";
  return 1;
}

// helper for ctors:
void UFMCE4Config::_create() {
  clearAll();
  // cycletypes from table 3-4 of the TReCS System Design doc.:
  _cycletypes["stare"] = 20; _cycletypes["Stare"] = 20; _cycletypes["STARE"] = 20;
  _cycletypes["chop"] = 14; _cycletypes["Chop"] = 14; _cycletypes["CHOP"] = 14;
  _cycletypes["nod"] = 17; _cycletypes["Nod"] = 17; _cycletypes["NOD"] = 17;
  _cycletypes["chop-nod"] = 19; _cycletypes["ChopNod"] = 19; _cycletypes["CHOP-NOD"] = 19;
  clog<<"UFMCE4Config::_create> _cycletypes: stare= "<<_cycletypes["stare"]
      <<", chop= "<<_cycletypes["chop"]<<", nod= "<<_cycletypes["nod"]
      <<", chop-nod= "<<_cycletypes["chop-nod"]<<endl;
      
  _mceparms["FrameCoadds"] = "LDVAR 0 "; 
  _mceparms["FrameCoadds-1"] = "LDVAR 1 ";
  _mceparms["FrameCoadds-2"] = "LDVAR 2 ";
  _mceparms["ChopSettleReads"] = "LDVAR 3 ";
  _mceparms["ChopCoadds"] = "LDVAR 4 ";
  _mceparms["SaveSets"] = "LDVAR 5 ";
  _mceparms["NodSettleReads"] = "LDVAR 6 ";
  _mceparms["NodSets"] = "LDVAR 7 ";
  _mceparms["NodSettleChops"] = "LDVAR 8 ";
  _mceparms["PreValidChops"] = "LDVAR 9 ";
  _mceparms["PostValidChops"] = "LDVAR 10 ";
  _mceparms["PixClock"] = "PBC ";
  _mceparms["ObsMode"] =  "CT ";
  _mceparms["ReadoutMode"] = "ARRAY_SAMPLING_TYPE "; 

  //initialize sky background ratio computation parameters:
  initRSky();

  // Setup FITS keywords and comments for method makeFITSheader():

  // These values are obtained from corresponding _activeH["*"] map arrays:
  FITS_KeyWords["FrameCoadds"]     = "FRMCOADD"; 
  FITS_KeyWords["ChopSettleReads"] = "CHPSETTL"; 
  FITS_KeyWords["ChopCoadds"]      = "CHPCOADD"; 
  FITS_KeyWords["NodSettleChops"]  = "NODSETTL"; 
  FITS_KeyWords["NodSettleReads"]  = "NODSETTR"; 
  FITS_KeyWords["SaveSets"]        = "SAVESETS"; 
  FITS_KeyWords["NsaveSets"]       = "NSAVSETS"; 
  FITS_KeyWords["ChopBeams"]       = "NCHOPS"; 
  FITS_KeyWords["NodBeams"]        = "NNODS"; 
  FITS_KeyWords["NodSets"]         = "NODSETS "; 
  FITS_KeyWords["NNodSets"]        = "NNODSETS "; 
  FITS_KeyWords["PreValidChops"]   = "PRECHPS "; 
  FITS_KeyWords["PostValidChops"]  = "PSTCHPS "; 
  FITS_KeyWords["PixClock"]        = "PIXCLOCK"; 
  FITS_KeyWords["TotalFrames"]     = "TOTFRMS "; 

  // These values are obtained from corresponding _activeP["*"] map arrays:
  FITS_KeyWords["FrameTime"]       = "FRMTIME "; 
  FITS_KeyWords["SaveFrequency"]   = "SAVEFREQ"; 
  FITS_KeyWords["OnSourceTime"]    = "OBJTIME "; 
  FITS_KeyWords["ElapsedTime"]     = "EXPTIME "; 
  FITS_KeyWords["ChopFrequency"]   = "CHPFREQ "; 
  FITS_KeyWords["ChopSettleTime"]  = "CHPDELAY"; 
  FITS_KeyWords["SCSDutyCycle"]    = "SCSDUTY "; 
  FITS_KeyWords["NodDwellTime"]    = "NODTIME "; 
  FITS_KeyWords["NodSettleTime"]   = "NODDELAY"; 
  FITS_KeyWords["ChopDutyCycle"]   = "EFF_CHP "; 
  FITS_KeyWords["NodDutyCycle"]    = "EFF_NOD "; 
  FITS_KeyWords["FrameDutyCycle"]  = "EFF_FRM "; 
  FITS_KeyWords["preValidChopTime"] = "PRECHPT";
  FITS_KeyWords["postValidChopTime"] = "PSTCHPT";

  FITS_Comments["FrameCoadds"]     = "# frames coadded per chop phase";
  FITS_Comments["ChopSettleReads"] = "# frames discarded during chop settle time"; 
  FITS_Comments["ChopCoadds"]      = "# of coadded chop cycles per saveset";
  FITS_Comments["NodSettleChops"]  = "# of chop cycles discarded during nod settle";
  FITS_Comments["NodSettleReads"]  = "# of frames discarded during nod settle";
  FITS_Comments["SaveSets"]        = "# of savesets from MCE during each nod beam"; 
  FITS_Comments["NsaveSets"]       = "# of savesets from MCE during each nod beam"; 
  FITS_Comments["ChopBeams"]       = "number of chop beams"; 
  FITS_Comments["NodBeams"]        = "number of nod beams"; 
  FITS_Comments["NodSets"]         = "# of nod cycles per total integration"; 
  FITS_Comments["NNodSets"]        = "# of nod cycles per total integration"; 
  FITS_Comments["PreValidChops"]   = "# of pre-valid discarded chop cycles"; 
  FITS_Comments["PostValidChops"]  = "# of post-valid discarded chop cycles"; 
  FITS_Comments["PixClock"]        = "MCE4 Pixel Base Clock (PBC)"; 
  FITS_Comments["TotalFrames"]     = "total # of frames from MCE saved"; 

  FITS_Comments["FrameTime"]       = "Frame time - ms"; 
  FITS_Comments["SaveFrequency"]   = "Save frequency - Hz"; 
  FITS_Comments["OnSourceTime"]    = "On-source time - min"; 
  FITS_Comments["ElapsedTime"]     = "Elapsed time - min"; 
  FITS_Comments["ChopFrequency"]   = "Chop frequency - Hz"; 
  FITS_Comments["ChopSettleTime"]  = "Chop settle time - ms"; 
  FITS_Comments["SCSDutyCycle"]    = "Max duty cycle of secondary mirror chopper (%)"; 
  FITS_Comments["NodDwellTime"]    = "Nod time - sec"; 
  FITS_Comments["NodSettleTime"]   = "Nod settle time - sec"; 
  FITS_Comments["ChopDutyCycle"]   = "Chop duty cycle (%)"; 
  FITS_Comments["NodDutyCycle"]    = "Nod duty cycle (%)"; 
  FITS_Comments["FrameDutyCycle"]  = "Frame duty cycle (%)"; 
  FITS_Comments["preValidChopTime"] = "Time (sec) before valid chopping";
  FITS_Comments["postValidChopTime"] = "Time (sec) after valid chopping";
}

// ctors
UFMCE4Config::UFMCE4Config(const string& name) : UFDeviceConfig(name) {
  _create();
}

UFMCE4Config::UFMCE4Config(const string& name,
			   const string& tshost,
			   int tsport) : UFDeviceConfig(name, tshost, tsport) {
  _create();
}

// mce4 agent entry points...

// acquisition directives: start, stop/abort, etc.
int UFMCE4Config::acquisition(const string& directive, string& reply, float flush) { 
  // any paramaters that follow a "start" or "sim" directive to mce4 which 
  // are meant to be passed down to the optional  ufgtake child should be formatted
  // like a command-line options (dash-etc.), i.e. -file filehint.
  char* cd = (char*)directive.c_str(); while( *cd == ' ' || *cd == '\t' ) ++cd;
  string mced = cd; // eliminated any leading white spaces in directive
  UFStrings::upperCase( mced );

  // commit a configureation:
  if( mced.find("CONF") != string::npos )
    return parmConfig(reply, flush); // issue a parmConfig using current _desH values.
   
  // clear/datum a configureation:
  if( mced.find("CLEAR") != string::npos )
    return clearConfig(reply, flush); // clear parms, and set mce4 to cycle type 0.
   
  // test MCE
  if( mced.find("TEST") != string::npos ) {
    if( _devIO ) {
      if( _devIO->submit("STATUS", reply, flush) > 0 ) {
	size_t ct0 = reply.find("Cycle Type-------------------> 0");
	if( ct0 == string::npos )
	  _idle = false;
	else
	  _idle = true; // cycle type 0 is always idle, even after start/run directive?
	return reply.size();
      }
      else return(-1);  // indicate that test communication with MCE failed.
    }
    else { // assume sim.
      _idle = true;
      return 0;
    }
  }

  // start frame acquisition(s)
  size_t opos = mced.find(" -");

  if( mced.find("START") != string::npos ||
      mced.find("RUN")   != string::npos ) {
    // check for ufgtake options, start grab/take child, issue mce start
    if( opos != string::npos && mced.length() > opos ) {
      //string takeopt = "env LD_LIBRARY_PATH=/usr/local/lib:/usr/local/uf/lib/:/opt/EDTpdv ";
      string takeopt = "/usr/local/uf/bin/ufgtake " + mced.substr(opos);
      if( _takepid > 0 ) {
        clog<<"UFMCE4Config::acquisition> start ? frame grad/take in progress, mce4 not idle"<<endl;
	return -1;
      }
      if( _verbose )
        clog<<"UFMCE4Config::acquisition> exec ufgtake child before starting mce4."<<endl;
      _takepid = UFRuntime::execApp(takeopt, UFDaemon::envp());
      if( _takepid < 0 ) {
        clog<<"UFMCE4Config::acquisition> ? exec ufgtake child failed, pid: "<<_takepid<<endl;
	return _takepid;
      }
      else {
        UFPosixRuntime::sleep(0.5); // wait for takeIdle semaphore to be created and set to false
        int w, h, cnt= 10;
        while( --cnt >= 0 && !UFEdtDMA::takeIdle(w, h) )
	  UFPosixRuntime::sleep(0.5); // wait for takeIdle semaphore to be created and set to false

        if( UFEdtDMA::takeIdle(w, h) ) { // after all this time, something is amiss?
          clog<<"UFMCE4Config::acquisition> exec ufgtake child failed? aborting start directive"<<endl;
	  UFEdtDMA::takeAbort(_takepid);
	  _takepid = (pid_t) -1;
	 return -1;
        }
	_frmCnt = UFEdtDMA::takeTotCnt(); // if zero, expected cnt is infinte
      }
    }

    if( _devIO == 0 )
      return 0;

    string mcecmd = mced.substr(0, opos);
    if( mcecmd.find("START") != string::npos ) // replace with RUN
      mcecmd = "RUN";
	
    if( _verbose )
      clog<<"UFMCE4Config::acquisition> resetting MCE4 and submitting directive: "<<mcecmd<<endl;

    _devIO->submit("CT 0", reply, flush);
    _devIO->submit("START", reply, flush);
    string ctcmd = _mceparms["ObsMode"] + UFRuntime::numToStr(_cycletypes[_des_obsmode]);
    clog<<"UFMCE4Config::acquisition> re-submitting cycle type: "<<ctcmd<<endl;
    _devIO->submit(ctcmd, reply, flush);
    string reply2;
    clog<<"UFMCE4Config::acquisition> requesting chop beam 0: ";
    _devIO->submit("CHOP_0", reply2, flush);
    clog<<reply2<<endl;
    reply += reply2;
    _devIO->submit("RUN", reply2, flush);
    reply += reply2;

    if( _verbose )
      clog<<"UFMCE4Config::acquisition> MCE cycle started, check status of MCE4..."<<endl;
    string status;
    _devIO->submit("status", status, flush);
    size_t ct0 = status.find("Cycle Type-------------------> ");
    if( _verbose ) {
      string ct = status.substr(ct0, strlen("Cycle Type-------------------> CCC"));
      clog<<"UFMCE4Config::acquisition> MCE cycle started, check status of MCE4..."<<endl;
    }
    ct0 = status.find("Cycle Type-------------------> 0");
    if( ct0 == string::npos )
      _idle = false;
    else
      _idle = true; // cycle type 0 is always idle, even after start/run directive?

    return reply.length();
  } //end of proceesing START/RUN

  // check for (mce4) sim directive
  if( mced.find("SIM") != string::npos ) {
    // check for ufgtake options, start grab/take child, issue mce start
    size_t opos = mced.find(" -");
    if( opos != string::npos && mced.length() > opos ) {
      string takeopt = "env LD_LIBRARY_PATH=/usr/local/lib:/usr/local/uf/lib/:/opt/EDTpdv ";
      takeopt += "/usr/local/uf/bin/ufgtake " + mced.substr(opos);
      if( _takepid > 0 ) {
        clog<<"UFMCE4Config::acquisition> ? frame grab/take in progress, mce4 not idle"<<endl;
	return -1;
      }
      if( _verbose )
        clog<<"UFMCE4Config::acquisition> exec ufgtake child before starting mce4."<<endl;
      _takepid = UFRuntime::execApp(takeopt, UFDaemon::envp());
      if( _takepid < 0 ) {
        clog<<"UFMCE4Config::acquisition> ? frame grab/take failed to initialize."<<endl;
	return -1;
      }
      UFPosixRuntime::sleep(0.5); // wait for takeIdle semaphore to be created and set to false
      int w, h, cnt= 10;
      while( --cnt >= 0 && !UFEdtDMA::takeIdle(w, h) )
	UFPosixRuntime::sleep(0.5); // wait for takeIdle semaphore to be created and set to false

      if( UFEdtDMA::takeIdle(w, h) ) { // after all this time, something is amiss?
        clog<<"UFMCE4Config::acquisition> exec ufgtake child failed? aborting start directive"<<endl;
	UFEdtDMA::takeAbort(_takepid);
	int status; ::waitpid(_takepid, &status, WNOHANG);
	_takepid = (pid_t) -1;
	return -1;
      }
    }
    if( _devIO == 0 )
      return 0;

    string mcecmd = mced.substr(0, opos);
    if( _verbose )
      clog<<"UFMCE4Config::acquisition> submitting directive to MCE4: "<<mcecmd<<endl;
    _devIO->submit(mcecmd, reply, flush);

    if( _verbose )
      clog<<"UFMCE4Config::acquisition> MCE cycle started, check status of MCE4..."<<endl;
    string status;
    _devIO->submit("status", status, flush);
    size_t ct0 = status.find("Cycle Type-------------------> ");
    if( _verbose ) {
      string ct = status.substr(ct0, strlen("Cycle Type-------------------> CCC"));
      clog<<"UFMCE4Config::acquisition> MCE cycle started, check status of MCE4..."<<endl;
    }
    ct0 = status.find("Cycle Type-------------------> 0");
    if( ct0 == string::npos )
      _idle = false;
    else
      _idle = true; // cycle type 0 is always idle, even after start/run directive?

    return reply.length();
  }

  // stop or abort directive should be implemented by submitting CT 0 & START (start idle cycle-type) 
  if( mced.find("STOP") != string::npos ||
      mced.find("DATUM") != string::npos ||
      mced.find("PARK")  != string::npos ||
      mced.find("ABORT") != string::npos ) {

    if( _takepid > 0 ) {
      if( _verbose )
        clog<<"UFMCE4Config::acquisition> aborting ufgtake"<<endl;
      UFEdtDMA::takeAbort(_takepid);
      _takepid = (pid_t) -1;
    }
    if( _verbose )
      clog<<"UFMCE4Config::acquisition> submitting CT 0 & START directive to mce4 ("<<mced<<")"<<endl;

    if( _devIO ) {
      string rep;
      _devIO->submit("CT 0", rep, flush);
      reply += rep;
      _devIO->submit("START", rep, flush);
      reply += rep;
      clog<<"UFMCE4Config::acquisition> requesting chop beam 0: ";
      _devIO->submit("CHOP_0", rep, flush);
      clog<<rep<<endl;
      reply += rep;
      _idle = true;
      return reply.size();
    }
    else { // assume sim.
      _idle = true;
      return 0;
    }
  }

  clog<<"UFMCE4Config::acquisition> ? not supported (start/run/sim/stop/abort) directive: "<<mced<<endl;
  return -1;
}

int UFMCE4Config::validAcq(const string& directive) {
  string Directive = directive;
  UFStrings::upperCase( Directive );
  if( directive.find("START") != string::npos ||
      directive.find("RUN") != string::npos)
    return 1; // mce4 will no longer be idle, it should be generating frames
  else if( directive.find("ABORT") != string::npos ||
	   directive.find("CLEAR") != string::npos ||
	   directive.find("CLOSE") != string::npos ||
	   directive.find("TEST") != string::npos ||
	   directive.find("DATUM") != string::npos ||
	   directive.find("OPEN") != string::npos ||
	   directive.find("PARK") != string::npos ||
	   directive.find("STOP") != string::npos )
    return 0;

  return -1;
}

// commit hardware params to mce4
// if successfull, this resets actives to 
// be congruent with desired:
// parameter config directive:
int UFMCE4Config::parmConfig(string& reply, float flush) {
  if( _devIO == 0 ) 
    reply = "sim: ";
  else
    reply = "";
  string rep;

  // Submit the det.array readout mode ONLY if it has changed.
  // First determine the basic readoutmode, without the "RAW" option:
  UFStrings::upperCase( _des_readoutmode );
  string basic_des_rdout = _des_readoutmode;
  if( _des_readoutmode.find("_RAW") != string::npos )
    basic_des_rdout = _des_readoutmode.substr( 0, _des_readoutmode.find("_RAW") );
  else if( _des_readoutmode.find("RAW") != string::npos )
    basic_des_rdout = _des_readoutmode.substr( 0, _des_readoutmode.find("RAW") );

  string basic_actv_rdout = _actv_readoutmode;
  if( _actv_readoutmode.find("_RAW") != string::npos )
    basic_actv_rdout = _actv_readoutmode.substr( 0, _actv_readoutmode.find("_RAW") );
  else if( _actv_readoutmode.find("RAW") != string::npos )
    basic_actv_rdout = _actv_readoutmode.substr( 0, _actv_readoutmode.find("RAW") );

  if( basic_actv_rdout != basic_des_rdout ) {
    string rdocmd;

    if( _des_readoutmode.find("S1R3") != string::npos ||
	_des_readoutmode.find("1S3R") != string::npos ) {
      rdocmd = " 4"; //quadruple sampling (with both output and column clamp refs)
    }
    else if( _des_readoutmode.find("S1R1") != string::npos ||
	     _des_readoutmode.find("1S1R") != string::npos )
      {
	if( _des_readoutmode.find("CR") != string::npos ) //two types of double sampling are possible
	  rdocmd = " 2 2"; // Column clamp Ref.
	else
	  rdocmd = " 2 1"; // Output clamp Ref.
      }
    else rdocmd = " 1"; //single sampling

    rdocmd = _mceparms["ReadoutMode"] + rdocmd;
    clog<<"UFMCE4Config::parmConfig> readoutmode="<<_des_readoutmode<<", mcecmd="<<rdocmd<<endl;

    if( _devIO && rdocmd.length() > 1 ) {
      _devIO->submit(rdocmd, rep, flush);
      reply += rep;
      clog<<"UFMCE4Config::parmConfig> "<<rdocmd<<", reply: "<<rep<<endl;
      _devIO->submit("STOP", rep, flush);
      reply += rep;
      clog<<"UFMCE4Config::parmConfig> STOP, reply: "<<rep<<endl;
      _devIO->submit("LOAD_PAT_RAM", rep, flush);
      reply += rep;
      clog<<"UFMCE4Config::parmConfig> LOAD_PAT_RAM, reply: "<<rep<<endl;
    }
    else {  // assume simulation
      reply += "\n\t" + rdocmd;
      reply += "\n\t STOP";
      reply += "\n\t LOAD_PAT_RAM";
    }
  }

  //restart the clocking of array in idle cycle type (no data transfers):
  if( _devIO ) {
    _devIO->submit("CT 0", rep, flush);
    reply += rep;
    _devIO->submit("START", rep, flush);
    reply += rep;
  }

  // now configure the other MCE registers: LDVARs and CycleType:
  HdwrParms::iterator itr = _desH.begin();
  string mcep;

  do {
    bool cycle = false;
    if( mcep.find("CT") != string::npos )
      cycle = true;
    bool ldvar = false;
    if( mcep.find("LDVAR") != string::npos )
      ldvar = true;
    bool chop = false;
    if( _des_obsmode.find("chop") != string::npos ) // submit LDVAR 0 & 1
      chop = true;
    string key = itr->first;
    int val = itr->second;
    clog<<"UFMCE4Config::parmConfig> key: "<<key<<", val: "<<val<<endl;
    // is this a PBC or CT or LDVAR or some other MCE cmd?
    strstream s;
    mcep = _mceparms[key];
    if( key.find("ChopCoad") != string::npos ) { // always subtract 2 from specified ChopCoadds
      val -= 2;
      if( val < 1 ) val = 1;
    }
    if( ldvar && chop && key.find("FrameCoadds-2") != string::npos ) // no-op.
      s<<"0"<<ends;
    else if( ldvar && !chop && (mcep.find("LDVAR") != string::npos) &&  // no-op.
	     (key.find("FrameCoadds") != string::npos) && !(key.find("FrameCoadds-2") != string::npos) )
      s<<"0"<<ends;
    else if( cycle ) // deal with cycle type last
      s<<"0"<<ends;
    else if( mcep.length() < 1 ) // no-op
      s<<"0"<<ends;
    else // ok
      s<<mcep<<val<<ends;
       
    string cmd= s.str(); delete s.str();

    if( _devIO && cmd.length() > 1 ) {
      _devIO->submit(cmd, rep, flush);
      reply += rep;
      clog<<"UFMCE4Config::parmConfig> cmd:"<<cmd<<", reply: "<<rep<<endl;
    }
    else {// assume simulation
      strstream sim;
      if( key.find("ChopCoad") != string::npos ) val += 2;  //for sim output show actual ChopCoadds
      sim<<"\n\t"<<key<< " == " << val<<ends;
      reply += sim.str(); delete sim.str();
      if( cmd.length() > 1 )
	clog<<"UFMCE4Config::parmConfig> cmd:"<<cmd<<endl;
    }
  } while( ++itr != _desH.end() );

  // now submit the cycle type, followed by "SIM 0":
  mcep = _mceparms["ObsMode"];
  int ct = _cycletypes[_des_obsmode];
  clog<<"UFMCE4Config::parmConfig> obsmode= "<<_des_obsmode<<", mcep= "<<mcep<<", ct= "<<ct<<endl;
  strstream sc;
  sc<<mcep<<ct<<ends;
  string ctcmd= sc.str(); delete sc.str();

  if( _devIO && ctcmd.length() > 1 ) {
    _devIO->submit(ctcmd, rep, flush);
    reply += rep;
    clog<<"UFMCE4Config::parmConfig> "<<ctcmd<<", reply: "<<rep<<endl;
    _devIO->submit("SIM 0", rep, flush);
    reply += rep;    
    clog<<"UFMCE4Config::parmConfig> SIM 0, reply: "<<rep<<endl;
  }
  else {// assume simulation
    strstream sim;
    sim<<"\n\t"<<mcep<< " == " <<_cycletypes[_des_obsmode]<<ends;
    reply += sim.str(); delete sim.str();
  }

  //do a quick RUN of new CycleType and then back to idle to flush possible 1st frame:
  if( _devIO ) {
    _devIO->submit("RUN", rep, flush);
    reply += rep;
    _devIO->submit("CT 0", rep, flush);
    reply += rep;
    _devIO->submit("START", rep, flush);
    reply += rep;
  }

  if( _devIO == 0 ) 
    clog<<"UFMCE4Config::parmConfig> reply: "<<reply<<endl;

  if( reply != "" ) {
    _actv_obsmode = _des_obsmode;
    _actv_readoutmode = _des_readoutmode;

    _activeH["FrameCoadds"] = _desH["FrameCoadds"];
    _activeH["FrameCoadds-1"] = _desH["FrameCoadds-1"];
    _activeH["FrameCoadds-2"] = _desH["FrameCoadds-2"];
    _activeH["ChopSettleReads"] = _desH["ChopSettleReads"];
    _activeH["ChopCoadds"] = _desH["ChopCoadds"];
    _activeH["NodSettleChops"] = _desH["NodSettleChops"];
    _activeH["NodSettleReads"] = _desH["NodSettleReads"];
    _activeH["SaveSets"] = _desH["SaveSets"];
    _activeH["NsaveSets"] = _desH["SaveSets"];
    _activeH["ChopBeams"] = _desH["ChopBeams"];
    _activeH["NodBeams"] = _desH["NodBeams"];
    _activeH["NodSets"] = _desH["NodSets"];
    _activeH["NNodSets"] = _desH["NodSets"];
    _activeH["PreValidChops"] = _desH["PreValidChops"];
    _activeH["PostValidChops"] = _desH["PostValidChops"];
    _activeH["TotalFrames"] = _desH["TotalFrames"];
    _activeH["PixClock"] = _desH["PixClock"];

    _activeP["FrameTime"] = _desP["FrameTime"];
    _activeP["SaveFrequency"] = _desP["SaveFrequency"];
    _activeP["OnSourceTime"] = _desP["OnSourceTime"];
    _activeP["ElapsedTime"] = _desP["ElapsedTime"];
    _activeP["ChopFrequency"] = _desP["ChopFrequency"];
    _activeP["ChopSettleTime"] = _desP["ChopSettleTime"];
    _activeP["SCSDutyCycle"] = _desP["SCSDutyCycle"];
    _activeP["NodDwellTime"] = _desP["NodDwellTime"];
    _activeP["NodSettleTime"] = _desP["NodSettleTime"];
    _activeP["ChopDutyCycle"] = _desP["ChopDutyCycle"];
    _activeP["NodDutyCycle"] = _desP["NodDutyCycle"];
    _activeP["FrameDutyCycle"] = _desP["FrameDutyCycle"];
    _activeP["preValidChopTime"] = _desP["preValidChopTime"];
    _activeP["postValidChopTime"] = _desP["postValidChopTime"];
  }

  isConfigured();
  return reply.length();
}

// clear & commit hardware params to mce4
// if successfull, this resets actives to 
// be congruent with desired:
// parameter config directive:
int UFMCE4Config::clearConfig(string& reply, float flush) {
  if( _devIO == 0 ) 
    reply = "sim: ";
  else
    reply = "";
       
  string cmd= "CT 0";

  if( _devIO ) {
    string rep;
    _devIO->submit(cmd, rep, flush);
    reply += rep;
  }
  else {// assume simulation
    reply += cmd;
  }
  if( _verbose )
    clog<<"UFMCE4Config::parmConfig> "<<cmd<<", reply: "<<reply<<endl;

  if( reply != "" ) {
    clearAll();
  }

  isConfigured();
  return reply.length();
}

bool UFMCE4Config::isConfigured() {
  bool r = !isClear(_activeH);
  if( _verbose ) {
    if( r )
      clog<<"UFMCE4Config::isConfigured::> true"<<endl;
    else
      clog<<"UFMCE4Config::isConfigured::> false"<<endl;
  }
  return r;
}

bool UFMCE4Config::isClear(HdwrParms& hp) {
  if( hp.size() <= 0 )
    return true;

  HdwrParms::iterator itr = hp.begin();
  do {
    //string key = itr->first;
    int val = itr->second;
    if( val != 0 )
      return false;
  } while( ++itr != hp.end() );

  return true;
}

// clear Hardware Parameters:
int UFMCE4Config::clear(HdwrParms& hp) {
  hp.clear();
  hp.insert(HdwrParms::value_type("FrameCoadds", 3));
  hp.insert(HdwrParms::value_type("FrameCoadds-1", 2));
  hp.insert(HdwrParms::value_type("FrameCoadds-2", 1));
  hp.insert(HdwrParms::value_type("ChopSettleReads", 0));
  hp.insert(HdwrParms::value_type("ChopCoadds", 0));
  hp.insert(HdwrParms::value_type("NodSettleChops", 0));
  hp.insert(HdwrParms::value_type("NodSettleReads", 0));
  hp.insert(HdwrParms::value_type("NodSets", 1));
  hp.insert(HdwrParms::value_type("SaveSets", 1));
  hp.insert(HdwrParms::value_type("PreValidChops", 1));
  hp.insert(HdwrParms::value_type("PostValidChops", 1));
  hp.insert(HdwrParms::value_type("PixClock", 2));
  /*
  HdwrParms::iterator itr = hp.begin();
  do {
    string key = itr->first;
    int val = itr->second;
    clog<<"UFMCE4Config::clearHdr> key: "<<key<<", val: "<<val<<endl;
  } while( ++itr != hp.end() );
  */
  return hp.size();
}

// clear Phys. Parameters:
int UFMCE4Config::clear(PhysParms& pp) {
  pp.clear();
  pp.insert(PhysParms::value_type("FrameTime", 0.0));
  pp.insert(PhysParms::value_type("SaveFrequency", 0.0));
  pp.insert(PhysParms::value_type("OnSourceTime", 0.0));
  pp.insert(PhysParms::value_type("ChopFrequency", 0.0));
  pp.insert(PhysParms::value_type("ChopSettleTime", 0.0));
  pp.insert(PhysParms::value_type("SCSDutyCycle", 0.0));
  pp.insert(PhysParms::value_type("NodDwellTime", 0.0));
  pp.insert(PhysParms::value_type("NodSettleTime", 0.0));
  pp.insert(PhysParms::value_type("ChopDutyCycle", 0.0));
  pp.insert(PhysParms::value_type("NodDutyCycle", 0.0));
  pp.insert(PhysParms::value_type("FrameDutyCycle", 0.0));
  pp.insert(PhysParms::value_type("preValidChopTime", 0.0));
  pp.insert(PhysParms::value_type("postValidChopTime", 0.0));
  /*
  PhysParms::iterator itr = pp.begin();
  do {
    string key = itr->first;
    double val = itr->second;
    clog<<"UFMCE4Config::clearPhys> key: "<<key<<", val: "<<val<<endl;
  } while( ++itr != pp.end() );
  */
  return pp.size();
}

// clear Meta Parameters:
int UFMCE4Config::clear(TextParms& mp) {
  mp.clear();
  mp.insert(TextParms::value_type("FluxBackground", "nominal"));
  mp.insert(TextParms::value_type("FluxNoise", "nominal"));
  mp.insert(TextParms::value_type("CameraMode", "imaging"));
  mp.insert(TextParms::value_type("ObsMode", "chop-nod"));
  mp.insert(TextParms::value_type("OnSourceTime", "seconds"));
  mp.insert(TextParms::value_type("ApertureName", "Mask"));
  mp.insert(TextParms::value_type("FilterName", "Si7.8um"));
  mp.insert(TextParms::value_type("GratingName", "LowRes10"));
  mp.insert(TextParms::value_type("CentralWavelength", "microns"));
  mp.insert(TextParms::value_type("ImagingMode", "Field"));
  mp.insert(TextParms::value_type("LensName", "Pupil-Imager"));
  mp.insert(TextParms::value_type("LyotName", "CrossHair-100"));
  mp.insert(TextParms::value_type("SectorName", "Polystyrene"));
  mp.insert(TextParms::value_type("SlitName", "0.21"));
  mp.insert(TextParms::value_type("WindowName", "ZnSe"));
  mp.insert(TextParms::value_type("Throughput", "0.8"));
  mp.insert(TextParms::value_type("SetPoint", "7.0"));
  mp.insert(TextParms::value_type("HeaterPower", "watts"));
  /*
  TextParms::iterator itr = mp.begin();
  do {
    string key = itr->first;
    string val = itr->second;
    clog<<"UFMCE4Config::clearMeta> key: "<<key<<", val: "<<val<<endl;
  } while( ++itr != mp.end() );
  */
  return mp.size();
}

// encode hardware parameters for transaction:
// this allocation must be freed by caller.
// this should emulate expected transaction from epics
UFStrings* UFMCE4Config::encode(const HdwrParms& hard, 
				const string& obsmode, const string& readoutmode,
				const string& car) {
  string k, v;
  vector< string > sv; 
  k = "Car"; v = car;
  sv.push_back(k); sv.push_back(v);
  k = "Parm"; v = "Hardware";
  sv.push_back(k); sv.push_back(v);

  sv.push_back("ObsMode"); sv.push_back(obsmode);
  sv.push_back("ReadoutMode"); sv.push_back(readoutmode);

  HdwrParms::const_iterator itr = hard.begin();
  do {
    k = itr->first;
    strstream s;
    s<<itr->second<<ends; // int to str
    v = s.str(); delete s.str();
    sv.push_back(k); sv.push_back(v);
    //clog<<"UFMCE4Config::encode> key: "<<k<<", val: "<<v<<endl;
  }   while( ++itr != hard.end() );


  return new UFStrings("UFMCE4Config::encodeHdwr", sv);
}

// encode phys parameters for transaction:
// this allocation must be freed by caller.
// this should emulate expected transaction from epics
UFStrings* UFMCE4Config::encode(const PhysParms& phys,
 				const string& obsmode, const string& readoutmode,
				const string& car) {
  string k, v;
  vector< string > sv;
  k = "Car"; v = car;
  sv.push_back(k); sv.push_back(v);
  k = "Parm"; v = "Physical";
  sv.push_back(k); sv.push_back(v);

  PhysParms::const_iterator itr = phys.begin();
  do {
    k = itr->first;
    strstream s;
    s<<itr->second<<ends; // double to str
    v = s.str(); delete s.str();
    sv.push_back(k); sv.push_back(v);
    //clog<<"UFMCE4Config::encode> key: "<<k<<", val: "<<v<<endl;
  } while( ++itr != phys.end() );

  sv.push_back("ObsMode"); sv.push_back(obsmode);
  sv.push_back("ReadoutMode"); sv.push_back(readoutmode);

  return new UFStrings("UFMCE4Config::encodePhys", sv);
}

// encode meta parameters for transaction:
// this allocation must be freed by caller.
UFStrings* UFMCE4Config::encode(const TextParms& meta,
				const string& car) {
  string k, v;
  vector< string > sv; 
  k = "Car"; v = car;
  sv.push_back(k); sv.push_back(v);
  k = "Parm"; v = "Meta";
  sv.push_back(k); sv.push_back(v);

  TextParms::const_iterator itr = meta.begin();
  do {
    k = itr->first;
    v = itr->second;
    sv.push_back(k); sv.push_back(v);
    //clog<<"UFMCE4Config::encode> key: "<<k<<", val: "<<v<<endl;
  } while( ++itr != meta.end() );

  return new UFStrings("UFMCE4Config::encodeMeta", sv);
}

string UFMCE4Config::stripErrmsg( string& cmdname ) {
  string errmsg;
  int errIdx = cmdname.find("!!");
  if( errIdx != (int)string::npos ) {
    errmsg = cmdname.substr(2+errIdx);
    // strip error any white spaces out of the cmdname...
    cmdname = cmdname.substr(0, errIdx);
    char* cs = (char*)cmdname.c_str();
    // leading whites:
    while( *cs == ' ' || *cs == '\t' || *cs == '\n'|| *cs == '\r' ) ++cs;
    cmdname = cs;
    // trailing whites:
    int trw = cmdname.find(" ");
    if( trw != (int) string::npos )
      cmdname = cmdname.substr(0, trw);
  }
  return errmsg;
}

// decode & insert hardware parameters
int UFMCE4Config::parse(const UFDeviceAgent::CmdInfo* info, HdwrParms& hp,
		        string& obsmode, string& readoutmode) {
  // assume that a car must be followed by a hard
  bool valid= false;
  string cmdname;
  string cmdimpl;
  // and the list of parameter key/values follows the hard... 
  hp.clear();

  for( int i = 0; i < (int)info->cmd_name.size(); ++i ) {
    // this has gotten pretty clumsy,
    //  should change the UFDeviceAgent::CmdInfo def. to use a map key=name val=impl...
    cmdname = info->cmd_name[i];
    cmdimpl = info->cmd_impl[i];
    string CMDname = cmdname;  UFStrings::upperCase( CMDname );
    string CMDimpl = cmdimpl;  UFStrings::upperCase( CMDimpl );
    if( CMDname.find("CAR") != string::npos ) // not interested, skip epics CAR
      continue;
    else if( CMDimpl.find("HARD") != string::npos )
      valid = true; // valid directive
    else if( CMDname.find("MODE") != string::npos ) { // obs. or readout mode
      if( CMDname.find("OBS") != string::npos ) {
	obsmode = cmdimpl;
	clog<<"UFMCE4Config::parse> inserted Hdwr: obsmode => "<<cmdimpl<<endl;
      }
      else { // assume it is the readout
	readoutmode = cmdimpl;
	clog<<"UFMCE4Config::parse> inserted Hdwr: readoutmode => "<<cmdimpl<<endl;
      }
    } else { // not a mode parameter, put into hash:
      stripErrmsg( cmdname );
      hp.insert(HdwrParms::value_type(cmdname, atoi(cmdimpl.c_str())));
      clog<<"UFMCE4Config::parse> inserted Hdwr: "<<cmdname<<" => "<<cmdimpl<<endl;
    }
  }

  if( !valid ) {
    hp.clear();
    clog<<"UFMCE4Config::parse> Par directive lacks Hard keyword..."<<endl;
    return 0;
  }

  hp["ChopBeams"] = 1;
  hp["NodBeams"] = 1;
  obsmode = "stare";

  if( hp["ChopCoadds"] > 0 )
    {
      hp["ChopBeams"] = 2;
      obsmode = "chop";

      if( hp["NodSettleChops"] > 0 ) {
	hp["NodBeams"] = 2;
	obsmode = "chop-nod";
      }
    }
  else if( hp["NodSettleReads"] > 0 )
    {
      hp["NodBeams"] = 2;
      obsmode = "nod";
    }

  clog<<"UFMCE4Config::parse> set Hdwr: obsmode => "<<obsmode<<endl;

  if( readoutmode.length() < 2 ) {
    readoutmode = "S1";
    clog<<"UFMCE4Config::parse> set Hdwr: readoutmode => "<<readoutmode<<endl;
  }

  if( _verbose ) {
    HdwrParms::iterator itr = hp.begin();
    do {
      string key = itr->first;
      int val = itr->second;
      clog<<"UFMCE4Config::parseHdwr> parsed key: "<<key<<", val: "<<val<<endl;
    } while( ++itr != hp.end() );
  }

  return hp.size();
}

// decode & insert physical parameters
int UFMCE4Config::parse(const UFDeviceAgent::CmdInfo* info, PhysParms& pp,
		        string& obsmode, string& readoutmode) {
  // assume that a car must be followed by a PHY
  bool valid= false;
  string cmdname;
  string cmdimpl;
  pp.clear();

  for( int i = 0; i < (int)info->cmd_name.size(); ++i ) {
    // this has gotten pretty clumsy,
    // should change the UFDeviceAgent::CmdInfo def. to use a map key=name val=impl...
    cmdname = info->cmd_name[i];
    cmdimpl = info->cmd_impl[i];
    string CMDname = cmdname;
    //    UFStrings::upperCase( CMDname ); //not working, it alters the original string!!!!
    string CMDimpl = cmdimpl;
    //    UFStrings::upperCase( CMDimpl );
    if( CMDname.find("CAR") != string::npos || cmdname.find("car") != string::npos ) // skip epics CAR name
      continue;
    else if( CMDimpl.find("PHY") != string::npos || cmdimpl.find("phy") != string::npos )
      valid = true; // valid directive
    else if( CMDname.find("MODE") != string::npos || cmdname.find("Mode") != string::npos ) {
      // it is obsmode or readoutmode
      if( CMDname.find("OBS") != string::npos || cmdname.find("Obs") != string::npos ) {
	obsmode = cmdimpl;
	clog<<"UFMCE4Config::parse> inserted Phys: obsmode => "<<cmdimpl<<endl;
      }
      else { // assume it is the readout
	readoutmode = cmdimpl;
	clog<<"UFMCE4Config::parse> inserted Phys: readoutmode => "<<cmdimpl<<endl;
      }
    }
    else {  // if not a mode, must be a parameter value
      stripErrmsg( cmdname );
      pp.insert( PhysParms::value_type( cmdname, ::atof(cmdimpl.c_str()) ) );
      clog<<"UFMCE4Config::parse> inserted Phys: "<<cmdname<<" => "<<cmdimpl<<endl;
    }
  }

  if( !valid ) {
    pp.clear();
    clog<<"UFMCE4Config::parse> PARM directive lacks Phys keyword..."<<endl;
    return 0;
  }

  if( _verbose ) {
    PhysParms::iterator itr = pp.begin();
    do {
      string key = itr->first;
      double val = itr->second;
      clog<<"UFMCE4Config::parsePhys> parsed key: "<<key<<", val: "<<val<<endl;
    } while( ++itr != pp.end() );
  }

  return pp.size();
}

// decode & insert meta parameters:
int UFMCE4Config::parse(const UFDeviceAgent::CmdInfo* info, TextParms& mp) {
  // assume that a CAR must be followed by a PARM & META.
  string cmdname;
  string cmdimpl;
  int istart = 0;
  
  cmdname = info->cmd_name[istart];
  UFStrings::upperCase( cmdname );

  if( cmdname.find("CAR") != string::npos ) // skip epics CAR here
    ++istart;

  cmdname = info->cmd_name[istart];
  cmdimpl = info->cmd_impl[istart];
  UFStrings::upperCase( cmdimpl );

  if( cmdname.find("PARM") != string::npos && cmdimpl.find("META") == string::npos ) {
    clog<<"UFMCE4Config::parse> PARM directive lacks META keyword..."<<endl;
    return 0;
  }

  ++istart;
  mp.clear();

  for( int i = istart; i < (int)info->cmd_name.size(); ++i ) {
    // this has gotten pretty clumsy,
    // should change the UFDeviceAgent::CmdInfo def. to use a map key=name val=impl...
    cmdname = info->cmd_name[i];
    cmdimpl = info->cmd_impl[i];
    stripErrmsg( cmdname );
    mp.insert(TextParms::value_type(cmdname, cmdimpl));
    clog<<"UFMCE4Config::parse> inserted Meta: "<<cmdname<<" => "<<cmdimpl<<endl;
  }

  if( _verbose ) {
    TextParms::iterator itr = mp.begin();
    do {
      string key = itr->first;
      string val = itr->second;
      clog<<"UFMCE4Config::parseMeta> parsed key: "<<key<<", val: "<<val<<endl;
    } while( ++itr != mp.end() );
  }

  return (int)mp.size();
}

// get phys & hdwr parms via ordered vector of strings:
// as cmd action reply of phys parm directive to trecs epics:
int UFMCE4Config::trecsPhysCAR(vector<string>& tc, bool append) {
  if( !append ) 
    tc.clear();

  tc.push_back( UFRuntime::numToStr(_desP["FrameTime"]) );
  tc.push_back( UFRuntime::numToStr(_desP["SaveFrequency"]) );
  tc.push_back( UFRuntime::numToStr(_desP["OnSourceTime"]) );
  tc.push_back( UFRuntime::numToStr(_desP["ChopFrequency"]) );
  tc.push_back( UFRuntime::numToStr(_desP["ChopSettleTime"]) );
  tc.push_back( UFRuntime::numToStr(_desP["NodDwellTime"]) );
  tc.push_back( UFRuntime::numToStr(_desP["NodSettleTime"]) );
  tc.push_back( UFRuntime::numToStr(_desP["ChopDutyCycle"]) );
  tc.push_back( UFRuntime::numToStr(_desP["NodDutyCycle"]) );
  tc.push_back( UFRuntime::numToStr(_SCSFreq) );
  tc.push_back( UFRuntime::numToStr(_desP["preValidChopTime"]) );
  tc.push_back( UFRuntime::numToStr(_desP["postValidChopTime"]) );
  tc.push_back( UFRuntime::numToStr(_desP["FrameDutyCycle"]) );
  tc.push_back( UFRuntime::numToStr(_desH["FrameCoadds"]) );
  tc.push_back( UFRuntime::numToStr(_desH["ChopSettleReads"]) );
  tc.push_back( UFRuntime::numToStr(_desH["ChopCoadds"]) );
  tc.push_back( UFRuntime::numToStr(_desH["NodSettleChops"]) );
  tc.push_back( UFRuntime::numToStr(_desH["NodSettleReads"]) );
  tc.push_back( UFRuntime::numToStr(_desH["SaveSets"]) );
  tc.push_back( UFRuntime::numToStr(_desH["NodSets"]) );
  tc.push_back( UFRuntime::numToStr(_desH["PixClock"]) );
  tc.push_back( UFRuntime::numToStr(_desH["PreValidChops"]) );
  tc.push_back( UFRuntime::numToStr(_desH["PostValidChops"]) );
  tc.push_back(_des_obsmode);
  tc.push_back(_des_readoutmode);

  return tc.size();
}

// get phys & hdwr parms via ordered vector of strings:
// as cmd action reply of meta parm directive to trecs epics:
int UFMCE4Config::trecsMetaCAR(vector<string>& mc) {
  mc.clear();
  mc.push_back( _metaP["SetPoint"] );
  mc.push_back( _metaP["HeaterPower"] );
  mc.push_back( _metaP["MetaFrameTime"] );
  return trecsPhysCAR(mc, true);   
}

int UFMCE4Config::trecsBiasCAR(vector<string>& tc) { 
  tc.clear();
  int cnt = (int)_biasP.size();
  for( int i = 0; i < cnt; ++i )
    tc.push_back(_biasP[i]);
  return (int)tc.size();
}

int UFMCE4Config::trecsPreampCAR(vector<string>& tc) { 
  tc.clear();
  int cnt = (int)_preamP.size();
  for( int i = 0; i < cnt; ++i )
    tc.push_back(_preamP[i]);
  return (int)tc.size();
}

int UFMCE4Config::trecsBiasCmds(map< string, string >& biascmds) {
  biascmds.clear();
  biascmds["BiasDatum"] = "ARRAY_INIT_STATE_BIAS";
  biascmds["BiasPower"] = "ARRAY_POWER_UP_BIAS";
  biascmds["BiasPark"] = "ARRAY_POWER_DOWN_BIAS";
  biascmds["ReadAllBias"] = "ARRAY_DAC_READBACK_BIAS";
  biascmds["BiasvWell"] = "ARRAY_WELL 0/1";
  biascmds["BiasvBias"] = "ARRAY_SET_DET_BIAS 0/1/2/3";
  return (int) biascmds.size();
}

int UFMCE4Config::trecsPreampCmds(map< string, string >& preampcmds) {
  preampcmds.clear();
  preampcmds["PreampDatum"] = "ARRAY_OFFSET_DEFAULTS_PREAMP";
  preampcmds["PreampPower"] = "ARRAY_POWER_UP_PREAMP";
  preampcmds["PreampPark"] = "ARRAY_POWER_DOWN_PREAMP";
  preampcmds["ReadAllPreamp"] = "ARRAY_OFFSET_READBACK_PREAMP";
  return (int)preampcmds.size();
}

// format active parms into string vec.
int UFMCE4Config::getActive(vector< string >& act) {
  act.clear();
  string md = "Active: Observation Mode == ";
  md += _actv_obsmode + ", Readout Mode == " + _actv_readoutmode;
 act.push_back(md);

  act.push_back("Active Physical Parameters:");
  int pcnt = _activeP.size();
  PhysParms::iterator itrp = _activeP.begin();
  while(itrp != _activeP.end() && --pcnt >= 0 ) {
    string key = itrp->first;
    double val = itrp->second;
    ++itrp;
    if( key.length() <= 0 || key == "" || key == " " ) {
      clog<<"UFMCE4Config::getActive> active Phys. hash has blank key= "<< key <<endl;
      continue;
    }
    strstream s;
    s<<"\t"<<key<<" == "<<val<<ends;
    act.push_back(s.str()); delete s.str();
  }

  act.push_back("Active Hardware Parameters:");
  int hcnt = _activeH.size();
  HdwrParms::iterator itrh = _activeH.begin();
  while( itrh != _activeH.end() && --hcnt >= 0 ) {
    string key = itrh->first;
    int val = itrh->second;
    ++itrh;
    if( key.length() <= 0 || key == "" || key == " " ) {
      clog<<"UFMCE4Config::getActive> active Hard. hash has blank key= "<< key <<endl;
      continue;
    }
    strstream s;
    s<<"\t"<<key<<" == "<<val<<ends;
    act.push_back(s.str()); delete s.str();
  }
  return act.size();   
}

// Add all active Hardware and Physical Parms into UFFITSheader object.
// (F.Varosi 7/27/2001)
// Note: do not clear() or end() the FITS header in this method,
//  since it will have existing records and more records may be added later.

int UFMCE4Config::makeFITSheader( UFFITSheader* FITSheader )
{
  FITSheader->add("OBSMODE", _actv_obsmode,    "Observing mode (chop, nod, stare, chop-nod)");
  FITSheader->add("READMODE",_actv_readoutmode,"Detector Readout mode");

  PhysParms::iterator itrp = _activeP.begin();

  while( itrp != _activeP.end() )
    {
      string key = itrp->first;
      double val = itrp->second;
      ++itrp;
      if( key.length() <= 0 || key == "" || key == " " ) {
	clog<<"UFMCE4Config::makeFITSheader> active Phys. hash has blank key= "<< key <<endl;
	continue;
      }
      string FITSkey = FITS_KeyWords[key];
      if( FITSkey.length() > 0 )
	FITSheader->add( FITSkey, val, FITS_Comments[key] );
    }

  HdwrParms::iterator itrh = _activeH.begin();

  while( itrh != _activeH.end() )
    {
      string key = itrh->first;
      int val = itrh->second;
      ++itrh;
      if( key.length() <= 0 || key == "" || key == " " ) {
	clog<<"UFMCE4Config::makeFITSheader> active Hard. hash has blank key= "<< key <<endl;
	continue;
      }
      string FITSkey = FITS_KeyWords[key];
      if( FITSkey.length() > 0 )
	FITSheader->add( FITSkey, val, FITS_Comments[key] );
    }

  return FITSheader->size();
}

// Usually this next method is invoked by the Detector Control DeviceAgent,
// to create the basic FITS header of the D.C. DeviceAgent (from FrameConfig and ObsConfig),
// and then adding the MCE4Config physical & hardware parms to FITS header.
// But note that the invocation: da->basicFITSheader() calls the wrong method (goes to base class),
//  so that the FrameConfig and ObsConfig does not get used (fh starts out empty).
// the above was actually due to a mismatch in the virtual func. args from
// base class to extension classes...
// it is now fixed!

UFStrings* UFMCE4Config::statusFITS(UFDeviceAgent* da) {
  // this fails at link time for the original (non Gemini) MCE4Agent, for obvious reasons!
  //UFGemMCE4Agent* gda = dynamic_cast<UFGemMCE4Agent*>(da);
  // this works for both agents (virtuously?)
  UFGemDeviceAgent* gda = dynamic_cast<UFGemDeviceAgent*>(da);
  UFFITSheader* fh = gda->basicFITSheader();
  int fsz = makeFITSheader(fh);
  if( fsz <= 0 ) { 
    clog<<"UFMCE4Config::statusFITS> error setting FITS header."<<endl;
    return 0;
  }

  return fh->Strings(da->name());
}

// get phys & hdwr parms via one vector of strings:
int UFMCE4Config::getDes(vector<string>& des) {
  des.clear();
  string md = "Desired: Observation Mode == ";
  md += _des_obsmode + ", Readout Mode == " + _des_readoutmode;
  des.push_back(md);

  des.push_back("Desired Physical Parameters:");
  int pcnt = _desP.size();
  PhysParms::iterator itrp = _desP.begin();
  while(itrp != _desP.end() && --pcnt >= 0 ) {
    string key = itrp->first;
    double val = itrp->second;
    ++itrp;
    if( key.length() <= 0 || key == "" || key == " " ) {
      clog<<"UFMCE4Config::getDes> desys. hash has blank key= "<< key <<endl;
      continue;
    }
    strstream s;
    s<<"\t"<<key<<" == "<<val<<ends;
    des.push_back(s.str()); delete s.str();
  }
  // add the calculated SCSFreq:
  strstream ss;
  ss<<"SCSFreq == "<<_SCSFreq<<ends;
  des.push_back(ss.str()); delete ss.str();

  // and finally, the hardware parms:
  des.push_back("Desired Hardware Parameters:");
  int hcnt = _desH.size();
  HdwrParms::iterator itrh = _desH.begin();
  while( itrh != _desH.end() && --hcnt >= 0 ) {
    string key = itrh->first;
    int val = itrh->second;
    ++itrh;
    if( key.length() <= 0 || key == "" || key == " " ) {
      clog<<"UFMCE4Config::getDes> hard. hash has blank key= "<< key <<endl;
      continue;
    }
    strstream s;
    s<<"\t"<<key<<" == "<<val<<ends;
    des.push_back(s.str()); delete s.str();
  }
  return des.size();   
}

// get phys & hdwr parms via one map of strings:strings
int UFMCE4Config::getDes(map<string, string>& ph) {
  ph.clear();
  ph["ObsMode"] = _des_obsmode;
  ph["ReadoutMode"] = _des_readoutmode;

  int pcnt = _desP.size();
  PhysParms::iterator itrp = _desP.begin();
  while( itrp != _desP.end() && --pcnt >= 0 ) {
    string key = itrp->first;
    double val = itrp->second;
    ++itrp;
    if( key.length() <= 0 || key == "" || key == " " ) {
      clog<<"UFMCE4Config::getDes> phys. hash has blank key= "<< key <<endl;
      continue;
    }
    strstream s;
    s<<val<<ends;
    ph[key] = s.str(); delete s.str();
  }

  int hcnt = _desH.size();
  HdwrParms::iterator itrh = _desH.begin();
  while( itrh != _desH.end() && --hcnt >= 0 ) {
    string key = itrh->first;
    int val = itrh->second;
    ++itrh;
    if( key.length() <= 0 || key == "" || key == " " ) {
      clog<<"UFMCE4Config::getDes> hard. hash has blank key= "<< key <<endl;
      continue;
    }
    strstream s;
    s<<val<<ends;
    ph[key] = s.str(); delete s.str();
  } 

  return ph.size();   
}

// set desired params
// sets desired hardware parms from hardware input
int UFMCE4Config::setDes(HdwrParms& hp, const string& obsmode, const string& readoutmode) {
  hp["FrameCoadds-1"] = hp["FrameCoadds"] - 1;
  hp["FrameCoadds-2"] = hp["FrameCoadds"] - 2;
  hp["TotalFrames"] = hp["ChopBeams"] * hp["NodBeams"] * hp["SaveSets"] * hp["NodSets"];
  _desH.clear();
  _desH["FrameCoadds"] = hp["FrameCoadds"];
  _desH["FrameCoadds-1"] = hp["FrameCoadds-1"];
  _desH["FrameCoadds-2"] = hp["FrameCoadds-2"];
  _desH["ChopSettleReads"] = hp["ChopSettleReads"];
  _desH["ChopCoadds"] = hp["ChopCoadds"];
  _desH["NodSettleChops"] = hp["NodSettleChops"];
  _desH["NodSettleReads"] = hp["NodSettleReads"];
  _desH["SaveSets"] = hp["SaveSets"];
  _desH["NodSets"] = hp["NodSets"];
  _desH["PreValidChops"] = hp["PreValidChops"];
  _desH["PostValidChops"] = hp["PostValidChops"];
  _desH["PixClock"] = hp["PixClock"];
  _desH["ChopBeams"] = hp["ChopBeams"];
  _desH["NodBeams"] = hp["NodBeams"];
  _desH["TotalFrames"] = hp["TotalFrames"];
  _des_obsmode = obsmode;
  _des_readoutmode = readoutmode;

  return _desH.size();
}

// sets desired hardware parms from physical input
int UFMCE4Config::setDes(PhysParms& pp, const string& obsmode, const string& readoutmode) {
  HdwrParms hp;
  PhysParms ppadj;

  eval(pp, ppadj, hp, obsmode, readoutmode);

  _desP.clear();
  _desP["FrameTime"] = ppadj["FrameTime"];
  _desP["MetaFrameTime"] = ppadj["MetaFrameTime"];  //not used, just for the record.
  _desP["SaveFrequency"] = ppadj["SaveFrequency"];
  _desP["OnSourceTime"] = ppadj["OnSourceTime"];
  _desP["ElapsedTime"] = ppadj["ElapsedTime"];
  _desP["ChopFrequency"] = ppadj["ChopFrequency"];
  _desP["ChopSettleTime"] = ppadj["ChopSettleTime"];
  _desP["NodDwellTime"] = ppadj["NodDwellTime"];
  _desP["NodSettleTime"] = ppadj["NodSettleTime"];
  _desP["ChopDutyCycle"] = ppadj["ChopDutyCycle"];
  _desP["SCSDutyCycle"] = ppadj["SCSDutyCycle"];
  _desP["NodDutyCycle"] = ppadj["NodDutyCycle"];
  _desP["FrameDutyCycle"] = ppadj["FrameDutyCycle"];
  _desP["preValidChopTime"] = ppadj["preValidChopTime"];
  _desP["postValidChopTime"] = ppadj["postValidChopTime"];
  
  return setDes(hp, obsmode, readoutmode);
}

// set desired physical & hardware parms from meta-config input:
//(negative return value means error occurred and should check string errmsg)

int UFMCE4Config::setDes(TextParms& mp, string& errmsg) {
  PhysParms pp;
  string obsmode, readoutmode;

  // evaluate physical parameters from meta-config params,
  // and get temperature control settings back in meta params.
  if( eval(mp, pp, obsmode, readoutmode, errmsg) < 0 ) return(-1);

  // set all desired parameters (and put T cntrl set into _metaP):
  _metaP = mp;
  return setDes(pp, obsmode, readoutmode);
}

// generate physical params from meta-config params:
//(negative return value means error occurred and caller should check string errmsg)

int UFMCE4Config::eval(TextParms& mp, PhysParms& pp, string& obsmode, string& readoutmode, string& errmsg)
{
  //read file of meta-config parameters if first time:
  if( _metaConParms.size() <= 0 ) {
    if( !readMetaConfigFile() ) {
      errmsg = "ERROR reading MetaConfig parameter file";
      return(-1);
    }
  }

  pp.clear();
  obsmode = mp["ObsMode"];
  if( obsmode.length() < 3 ) obsmode = "chop-nod"; // use default if no override.
  readoutmode = mp["ReadoutMode"];
  //if readoutmode mode input does not have S1 in string it means use automatic decision/defaults.
  if( readoutmode.find("S1") == string::npos ) readoutmode = "S1R3";

  mp["MetaFrameTime"] = "?"; //must always have some default non-blank value for reply to epics.

  pp["OnSourceTime"] = ::atof( mp["OnSourceTime"].c_str() );
  pp["ChopThrow"] = ::atof( mp["ChopThrow"].c_str() );
  pp["ChopFrequency"] = ::atof( mp["ChopFrequency"].c_str() );
  pp["preValidChopTime"] = ::atof( mp["preValidChopTime"].c_str() );
  pp["preValidChopTime"] = _metaConParms["preValidChopTime"];
  pp["postValidChopTime"] = _metaConParms["postValidChopTime"];

  //check if nod dwell/settle times are specified, otherwise use current or default values:

  string nodtime = mp["NodDwellTime"];
  if( nodtime.length() > 0 )
    pp["NodDwellTime"] = ::atof( nodtime.c_str() );
  else
    pp["NodDwellTime"] = _activeP["NodDwellTime"];

  if( pp["NodDwellTime"] < 7 ) pp["NodDwellTime"] = 30;

  nodtime = mp["NodSettleTime"];
  if( nodtime.length() > 0 )
    pp["NodSettleTime"] = ::atof( nodtime.c_str() );
  else
    pp["NodSettleTime"] = _activeP["NodSettleTime"];

  if( pp["NodSettleTime"] < 1 ) pp["NodSettleTime"] = 4;

  double saveFreq = _activeP["SaveFrequency"];
  string sf = mp["SaveFrequency"];
  if( sf.length() > 0 ) saveFreq = ::atof( sf.c_str() );  //save freq override

  // This is curvefit to old estimated duty cycle provided by Gemini:
  //  double SCSDutyCycle = 100*( 1 - (pp["ChopFrequency"] * sqrt( pp["ChopThrow"] ))/234 );

  double frameTime= 0;
  string camMode = mp["CameraMode"];
  UFStrings::lowerCase( camMode );

  pp["MinDutyCycle"] = _metaConParms["MinDutyCycle::"+camMode];
  if( pp["MinDutyCycle"] <= 0 ) pp["MinDutyCycle"] = _metaConParms["MinDutyCycle::default"];
  if( pp["MinDutyCycle"] <= 0 ) pp["MinDutyCycle"] = _metaConParms["MinDutyCycle"];
  pp["SCSDutyCycle"] = _metaConParms["MaxDutyCycle"];
  clog<<"UFMCE4Config::eval(meta)> input ChopThrow = " << pp["ChopThrow"] << " arcsec " <<endl;
  clog<<"UFMCE4Config::eval(meta)> input ChopFrequency = " << pp["ChopFrequency"] << " Hz " <<endl;
  clog<<"UFMCE4Config::eval(meta)> max SCSDutyCycle = " << pp["SCSDutyCycle"] << " % " <<endl;
  clog<<"UFMCE4Config::eval(meta)> min detDutyCycle = " << pp["MinDutyCycle"] << " % " <<endl;

  if( camMode == "spectroscopy" ) {
    if( saveFreq < 0.01 || saveFreq > 3 ) saveFreq = 0.1;
    frameTime = metaFrameTimeSpectro( mp, readoutmode, errmsg );
  }
  else {
    if( saveFreq < 0.01 ) saveFreq = 0.5;
    frameTime = metaFrameTimeImaging( mp, readoutmode, errmsg ); //method may change readoutmode.
  }

  pp["SaveFrequency"] = saveFreq;
  clog<<"UFMCE4Config::eval(MetaParmConfig)> meta-FrameTime = "<<frameTime<<" ms."<<endl;

  double overRideFT = ::atof( mp["OverrideFrameTime"].c_str() );
  if( overRideFT > 0 ) {
    frameTime = overRideFT;
    clog<<"UFMCE4Config::eval(MetaParmConfig)> override-FrameTime = "<<frameTime<<" ms."<<endl;
  }

  pp["MetaFrameTime"] = frameTime;
  pp["FrameTime"] = frameTime;     //this one will get adjusted by eval(pp).
  if( frameTime <= 0 ) return(-1);

  // set default temperature control params:
  mp["SetPoint"] = UFRuntime::numToStr(_metaConParms["SetPoint::"+camMode]);
  mp["HeaterPower"] = "Medium";

  return 0;
}

//compute the frame integration time for Mid-IR imaging using algorithms developed by Robert Pina,
// or use values defined in file MetaConfigDet.txt (stored in string->double map _metaConParms)
// (negative return value means error occurred and should check string errmsg).

double UFMCE4Config::metaFrameTimeImaging(TextParms& mp, string& readoutmode, string& errmsg) {
  //cmd MCE for needed Well depth and Bias, and power up detector:
  vector<string> biasCmds;
  biasCmds.push_back("BiasWell");
  biasCmds.push_back("ARRAY_WELL 0"); //shallow well default, may become deep well...
  biasCmds.push_back("BiasVbias");
  biasCmds.push_back("ARRAY_SET_DET_BIAS 2"); //medium bias.
  biasCmds.push_back("BiasPower");
  biasCmds.push_back("ARRAY_POWER_UP_BIAS"); //power on bias voltages to detector.
  biasCmds.push_back("BiasVgate");
  biasCmds.push_back("ARRAY_VGATE 1"); //turn Vgate on.
  UFStrings BiasCmds( "BiasCmds", biasCmds );
  UFDeviceAgent::CmdInfo* BiasCmdinfo = new UFDeviceAgent::CmdInfo( BiasCmds );
  bool readflag, initflag;
  int statDAC = cmdDAC( BiasCmdinfo, readflag, initflag );
  delete BiasCmdinfo;
  clog<<"UFMCE4Config::metaFrameTimeImaging> Bias cmd DAC status = "<<statDAC<<endl;

  string filter = mp["Filter"];

  if( filter == "K" || filter == "L" || filter == "M" ) { //for Near-IR filters use standard frame times.
    double sFT = _metaConParms[filter+"::"+readoutmode];
    if( sFT <= 0 ) sFT = _metaConParms[filter+"::default"];
    if( sFT <= 0 ) sFT = 40.0;
    mp["MetaFrameTime"] = UFRuntime::numToStr( sFT );
    return sFT;
  }

  bool canChangeRdout = false;
  string recvdReadOutMode = mp["ReadoutMode"];
  if( recvdReadOutMode.find("S1") == string::npos ) canChangeRdout = true;

  if( _metaConParms[filter+"::FT"] > 0 ) {
    clog<<"UFMCE4Config::metaFrameTimeImaging> using predefined meta-config for filter: "<<filter<<endl;
    double vapThresh = _metaConParms[filter+"::VT"];
    if( vapThresh <= 0 ) {
      vapThresh = 5.0;
      clog<<"UFMCE4Config::metaFrameTimeImaging> water vapour threshold not defined, using default:"<<endl;
    }
    clog<<"UFMCE4Config::metaFrameTimeImaging> high b.g. water vapour threshold: "<<vapThresh<<endl;
    double vapour = ::atof( mp["WaterVapour"].c_str() );
    if( vapour <= 0 ) {
      clog<<"UFMCE4Config::metaFrameTimeImaging> invalid water vapour: "<<vapour<<endl;
      vapour = 1;
    }
    double filterBandWidth = _metaConParms[filter+"::BW"];
    //set bias to deep well if water vapour is above threshold or wideband filter:
    if( vapour > vapThresh || filterBandWidth > 1 ) {
      biasCmds.clear();
      biasCmds.push_back("BiasDeepWell");
      biasCmds.push_back("ARRAY_WELL 1");
      UFStrings BiasCmds( "BiasCmds", biasCmds );
      BiasCmdinfo = new UFDeviceAgent::CmdInfo( BiasCmds );
      statDAC = cmdDAC( BiasCmdinfo, readflag, initflag );
      delete BiasCmdinfo;
      clog<<"UFMCE4Config::metaFrameTimeImaging> set deep well cmd DAC status = "<<statDAC<<endl;
      if( canChangeRdout && vapour > vapThresh && filterBandWidth > 1 ) {
	readoutmode = "S1";
	clog<<"UFMCE4Config::metaFrameTimeImaging> requesting new readout mode = "<<readoutmode<<endl;
      }
    }
    double mFT = _metaConParms[filter+"::FT"];
    if( readoutmode == "S1" ) {
      double ft = _metaConParms[filter+"::FT-S1"];
      if( ft > 0 ) mFT = ft;
    }
    clog<<"UFMCE4Config::metaFrameTimeImaging> meta-FrameTime = "<<mFT<<" ms"<<endl;
    mp["MetaFrameTime"] = UFRuntime::numToStr( mFT );
    return mFT;
  }

  //compute meta frame time for Mid-IR filters using algorithm by Robert Pina:
  UFRSkyCoef RskyCoefs = _RSkyCfsMap[filter];
  double Rsky = 1;

  if( RskyCoefs.nV > 0 && RskyCoefs.Filter ) {
    clog<<"UFMCE4Config::metaFrameTimeImaging> selected coeffs for Filter: "<<RskyCoefs.Filter<<endl;
    if( string( RskyCoefs.Filter ) != filter )
      clog<<"UFMCE4Config::metaFrameTimeImaging> ERROR: Rsky filter inconsistent: "
	  << RskyCoefs.Filter << " != " << filter << endl;
    double vapour = ::atof( mp["WaterVapour"].c_str() );
    if( vapour <= 0 ) {
      clog<<"UFMCE4Config::metaFrameTimeImaging> invalid water vapour: "<<vapour<<endl;
      vapour = 1;
    }
    double airmass = ::atof( mp["AirMass"].c_str() );
    if( airmass < 1 ) {
      clog<<"UFMCE4Config::metaFrameTimeImaging> invalid airmass: "<<airmass<<endl;
      airmass = 1;
    }
    Rsky = RskyCoefs.RskyApprox( vapour, airmass );
  }
  else {
    clog<<"UFMCE4Config::metaFrameTimeImaging> ERROR: did not find Rsky map to filter: "<<filter<<endl;
    mp["MetaFrameTime"] = UFRuntime::numToStr( _metaConParms["FiducialFrameTime"] );
    return(_metaConParms["FiducialFrameTime"]);
  }

  clog<<"UFMCE4Config::metaFrameTimeImaging> Rsky = "<<Rsky<<endl;
  double LambdaLow = RskyCoefs.Lambda[0];
  double LambdaHi = RskyCoefs.Lambda[1];
  clog<<"UFMCE4Config::metaFrameTimeImaging> filter: " << filter
      <<", bandpass: "<<LambdaLow<<"um - "<<LambdaHi<<"um."<<endl;

  if( LambdaLow <= 0 ) {
    clog<<"UFMCE4Config::metaFrameTimeImaging> invalid bandpass: "<<LambdaLow<<"-"<<LambdaHi<<"um."<<endl;
    LambdaLow = _FiducialConfig.LambdaLow;
    clog<<"UFMCE4Config::metaFrameTimeImaging> using bandpass: "<<LambdaLow<<"-"<<LambdaHi<<"um."<<endl;
  }

  if( LambdaHi <= 0 ) {
    clog<<"UFMCE4Config::metaFrameTimeImaging> invalid bandpass: "<<LambdaLow<<"-"<<LambdaHi<<"um."<<endl;
    LambdaHi = _FiducialConfig.LambdaHi;
    clog<<"UFMCE4Config::metaFrameTimeImaging> using bandpass: "<<LambdaLow<<"-"<<LambdaHi<<"um."<<endl;
  }

  double Tambient = ::atof( mp["AmbientTemperature"].c_str() );
  int ipb = 0;
  if( (LambdaLow + LambdaHi )/2 > 15 ) ipb = 1;
  double IntConfig = _ROpticsCoefs[ipb].TaylorSeries( LambdaLow, LambdaHi, Tambient );
  double IntFiduc = _ROpticsCoefs[ipb].TaylorSeries( _FiducialConfig.LambdaLow,
						     _FiducialConfig.LambdaHi,
						     _FiducialConfig.TCoptics );
  clog<<"UFMCE4Config::metaFrameTimeImaging> Optics BB integrals: current="<< IntConfig
      <<",  fiducial=" << IntFiduc <<endl;

  double Tratio = (Tambient+273)/(_FiducialConfig.TCoptics+273);
  double Tratp3 = 1;
  for( int i=0; i<3; i++ ) Tratp3 *= Tratio;
  double emissivity = ::atof( mp["TelescopeEmissivity"].c_str() );
  double Roptics = Tratp3 * (emissivity/_FiducialConfig.emiOptics) *(IntConfig/IntFiduc);
  clog<<"UFMCE4Config::metaFrameTimeImaging> Roptics = "<<Roptics<<endl;

  double Qtrans = ::atof( mp["Throughput"].c_str() );
  clog<<"UFMCE4Config::metaFrameTimeImaging> Qtrans = " << Qtrans << endl;

  if( Qtrans < 0.01 ) {
    errmsg = "Throughput < 1%, rejecting obs...";
    clog<<"UFMCE4Config::metaFrameTimeImaging> "<<errmsg<< endl;
    return(-1);
  }

  double Rcoup = _FiducialConfig.OptSkyCoupling;
  double Rbkgrnd = ( Rsky/(1+Rcoup) + Roptics/(1+1/Rcoup) ) * (Qtrans/_FiducialConfig.Qtrans);
  clog<<"UFMCE4Config::metaFrameTimeImaging> Rbkgrnd = "<<Rbkgrnd<<endl;

  double fiducialFrameTime = _metaConParms["FiducialFrameTime"];
  if( fiducialFrameTime <= 0 ) {
    clog<<"UFMCE4Config::metaFrameTimeImaging> FiducialFrameTime is not defined! using 40ms"<<endl;
    fiducialFrameTime = 40;
  }

  double metaFrameTime = fiducialFrameTime/Rbkgrnd;
  clog<<"UFMCE4Config::metaFrameTimeImaging> fiducial-FrameTime = "<<fiducialFrameTime<<" ms"<<endl;
  clog<<"UFMCE4Config::metaFrameTimeImaging> meta-FrameTime = "<<metaFrameTime<<" ms"<<endl;

  //special cases of mid-IR filters that need extra checking, adjustment, and/or limiting:
  double finMetaFT = metaFrameTimeAdjust( metaFrameTime, LambdaHi - LambdaLow,
					  readoutmode, canChangeRdout );

  //save the adjusted metaFT, because finMetaFT may be also limited by a specified min val.
  mp["MetaFrameTime"] = UFRuntime::numToStr( metaFrameTime );
  return finMetaFT;
}

// Apply further adjustments to computed meta-frametime as set in file MetaConfigDet.txt (_metaConParms):

double UFMCE4Config::metaFrameTimeAdjust(double& metaFrameTime, double bandWidth,
					 string& readoutmode, bool canChangeRdout )
{
  double minFrameTime = _metaConParms["MinFrameTime::"+readoutmode];
  if( minFrameTime <= 0 ) {
    clog<<"UFMCE4Config::metaFrameTimeAdjust> MinFrameTime is not defined! using 22ms"<<endl;
    minFrameTime = 22;
  }

  clog<<"UFMCE4Config::metaFrameTimeAdjust> bandwidth = "<<bandWidth<<" um"<<endl;
  clog<<"UFMCE4Config::metaFrameTimeAdjust> min-FrameTime = "<<minFrameTime<<" ms"<<endl;
  double wideBand = 1.4; //wide band filters are wider than 1.4um.
  double narrowBand = 0.5; //narrow band filters are less than 0.5um wide.

  //set bias to deep well if wide band or frame time is too low:
  if( bandWidth > wideBand || metaFrameTime < minFrameTime ) {
    vector<string> biasCmds;
    biasCmds.push_back("BiasDeepWell");
    biasCmds.push_back("ARRAY_WELL 1");
    UFStrings BiasCmds( "BiasCmds", biasCmds );
    UFDeviceAgent::CmdInfo* BiasCmdinfo = new UFDeviceAgent::CmdInfo( BiasCmds );
    bool readflag, initflag;
    int statDAC = cmdDAC( BiasCmdinfo, readflag, initflag );
    clog<<"UFMCE4Config::metaFrameTimeAdjust> set deep well cmd DAC status = "<<statDAC<<endl;
  }

  //adjust frametime for wide/narrow band filters based on Tom Hayward's experience ?

  if( bandWidth > wideBand ) //using deep wells (3 times shallow) so increase frame time.
    metaFrameTime *= 3.0;
  else if( bandWidth < narrowBand ) //decrease frame time because get better duty cylces.
    metaFrameTime *= 0.7;

  clog<<"UFMCE4Config::metaFrameTimeAdjust> meta-FrameTime = "<<metaFrameTime<<" ms"<<endl;

  if( metaFrameTime < minFrameTime ) {
    if( canChangeRdout ) {
      if( bandWidth > wideBand && metaFrameTime < _metaConParms["MinFrameTime::S1"] ) {
	readoutmode = "S1";
	clog<<"UFMCE4Config::metaFrameTimeAdjust> new readout mode = "<<readoutmode<<endl;
	minFrameTime = _metaConParms["MinFrameTime::S1"];
      }
    }
    clog<<"UFMCE4Config::metaFrameTimeAdjust> using min-FrameTime = "<<minFrameTime<<" ms"<<endl;
    return minFrameTime;
  }

  return metaFrameTime;
}

//select frame integration time from lookup table for Mid-IR spectroscopy:
//(negative return value means error occurred and should check string errmsg)

double UFMCE4Config::metaFrameTimeSpectro(TextParms& mp, string readoutmode, string& errmsg) {
  //cmd MCE for needed detector Well depth and Bias:
  vector<string> biasCmds;
  biasCmds.push_back("BiasWell");
  biasCmds.push_back("ARRAY_WELL 0"); //shallow well.
  biasCmds.push_back("BiasVbias");
  biasCmds.push_back("ARRAY_SET_DET_BIAS 2"); //medium bias.
  biasCmds.push_back("BiasPower");
  biasCmds.push_back("ARRAY_POWER_UP_BIAS"); //power on bias voltages to detector.
  biasCmds.push_back("BiasVgate");
  biasCmds.push_back("ARRAY_VGATE 1"); //turn Vgate on.
  UFStrings BiasCmds( "BiasCmds", biasCmds );
  UFDeviceAgent::CmdInfo* BiasCmdinfo = new UFDeviceAgent::CmdInfo( BiasCmds );
  bool readflag, initflag;
  int statDAC = cmdDAC( BiasCmdinfo, readflag, initflag );
  clog<<"UFMCE4Config::metaFrameTimeSpectro> cmd DAC status = "<<statDAC<<endl;

  string grating = mp["Grating"];
  string wavelength = mp["CentralWavelength"];
  clog<<"UFMCE4Config::metaFrameTimeSpectro> grating = "<<grating<<endl;

  //use standard frame times already given:
  double metaFrameTime = _metaConParms[grating+"::"+readoutmode];

  if( metaFrameTime <= 0 ) metaFrameTime = _metaConParms[grating+"::default"];
  if( metaFrameTime <= 0 ) metaFrameTime = 70.0;

  mp["MetaFrameTime"] = UFRuntime::numToStr( metaFrameTime );
  return metaFrameTime;
}

//query the MCE for actual frame time in each readout mode,
// by configuring it and asking for array pattern clock length:

bool UFMCE4Config::queryMCEtiming( bool queryMCE4 ) {
  //set queryMCE4 = true if actual query of MCE4 is desired (takes 2 mins.),
  // or queryMCE4 = false to use defaults (last known timing).
  //query for simple stare mode, and all readout modes:
  _desH.clear();
  _desH["FrameCoadds"] = 3;
  _desH["FrameCoadds-1"] = 2;
  _desH["FrameCoadds-2"] = 1;
  _desH["ChopSettleReads"] = 0;
  _desH["ChopCoadds"] = 0;
  _desH["NodSettleChops"] = 0;
  _desH["NodSettleReads"] = 0;
  _desH["SaveSets"] = 1;
  _desH["NodSets"] = 1;
  _desH["PreValidChops"] = 1;
  _desH["PostValidChops"] = 1;
  _desH["PixClock"] = 1;
  _desH["ChopBeams"] = 1;
  _desH["NodBeams"] = 1;
  _des_obsmode = "stare";
  vector< string >readoutModes;
  readoutModes.push_back("S1");
  readoutModes.push_back("S1R1");
  readoutModes.push_back("S1R1_CR");
  readoutModes.push_back("S1R3");
  float flush = 0.05;
  string mceReply;

  for( int i=0; i < (int)readoutModes.size(); i++ )
    {
      _des_readoutmode = readoutModes[i];
      int mcestat = 1;

      if( _devIO && queryMCE4 ) {
	parmConfig( mceReply, flush ); //send the desired hardware params to MCE
      	mcestat = _devIO->submit("ARRAY_PATTERN_CLOCK_LEN", mceReply, flush);
      }
      else { // use last known values:
	if( _des_readoutmode == "S1" )      mceReply = "Pattern 2 26429";
	if( _des_readoutmode == "S1R1" )    mceReply = "Pattern 2 45629";
	if( _des_readoutmode == "S1R1_CR" ) mceReply = "Pattern 2 47789";
	if( _des_readoutmode == "S1R3" )    mceReply = "Pattern 2 86189";
      }

      clog<<"UFMCE4Config::queryMCEtiming> ReadoutMode:"<<_des_readoutmode<<", reply: "<<mceReply<<endl;
      if( mcestat <= 0 ) return false;

      if( mceReply.find("Pattern 2") != string::npos ) { //parse reply for length of Pattern 2:
	string pattern2 = mceReply.substr( mceReply.find("Pattern 2")+9, 10 );
	int clockLen = ::atoi( pattern2.c_str() );
	_frameTimes[_des_readoutmode] = 100e-9 * (clockLen-3);
	clog<<"UFMCE4Config::queryMCEtiming> read-out-mode:"<<_des_readoutmode
	    <<", clockLength="<<clockLen<<", frameTime="<<_frameTimes[_des_readoutmode]<<endl;
      }
      else return false;
    }
  return true;
}

//compute MCE PBC by finding nearest valid frame time:

int UFMCE4Config::pixClockAdj( double& frmTimeSec, const string& rdOutMd ) {
  double frameTime1; //frame time in secs for PBC=1.

  if( rdOutMd.find("S1R3") != string::npos ||
      rdOutMd.find("1S3R") != string::npos ) {
    //quadruple sampling (with 2 output clamp refs and 1 column clamp ref):
    frameTime1 = _frameTimes["S1R3"];
  }
  else if( rdOutMd.find("S1R1") != string::npos ||
	   rdOutMd.find("1S1R") != string::npos )
    {
      //two types of double sampling are possible:
      if( rdOutMd.find("CR") != string::npos )
	frameTime1 = _frameTimes["S1R1_CR"]; // Column clamp Ref:
      else
	frameTime1 = _frameTimes["S1R1"]; // Output clamp Ref:
    }
  else frameTime1 = _frameTimes["S1"]; //single sampling (no ref):

  //compute PBC and round off:
  double PBC = _MAX2_( _minPBC, rint( frmTimeSec / frameTime1 ) );
  frmTimeSec = PBC * frameTime1;
  return _MAX2_( 1, (int)PBC );
}

// generate mce4 hardware params from physical, adjust physical params
// to reflect actual mapping to hardware params:

int UFMCE4Config::eval(PhysParms& pp, PhysParms& ppadj, HdwrParms& hp,
		       const string& obsmode, const string& readoutmode) {
  clear(ppadj); clear(hp);
  
  // physical parms
  double frmreads;
  double FrameTime = pp["FrameTime"];
  double FrmTimeSec = FrameTime / 1000;
  double SaveFrequency = pp["SaveFrequency"];
  double OnSourceTime = pp["OnSourceTime"];
  double ObjTimeSec = OnSourceTime * 60;
  double ExpTimeSec = ObjTimeSec;
  double ChopFrequency = pp["ChopFrequency"];
  double ChopSettleTime = pp["ChopSettleTime"]; 
  double ChopSettleTimeSec = ChopSettleTime / 1000; 
  double NodDwellTime = pp["NodDwellTime"]; // dwell time at each nod position
  double NodSettleTime = pp["NodSettleTime"]; // settle time between nods
  double ChopDutyCycle = 1.;
  double NodDutyCycle = 1.;
  double SCSDutyCycle =  pp["SCSDutyCycle"]/100;    //convert duty cycle % to fraction.
  double minDutyCycle =  pp["MinDutyCycle"]/100;    //optional minimum allowed duty cycle.
  double preValidChopTime = pp["preValidChopTime"];
  double postValidChopTime = pp["postValidChopTime"];
  int NodBeams = 0;
  int ChopBeams = 0;

  // hardware parms
  int PixClock = pixClockAdj( FrmTimeSec, readoutmode ); // adjusts FrmTimeSec
  int FrameCoadds = 3;
  int ChopSettleReads = 0;
  int ChopCoadds = 0;
  int SaveSets = 1;
  int NodSettleChops = 0;
  int NodSettleReads = 0;
  int NodSets = 1;
  int PreValidChops = 1;
  int PostValidChops = 1;
  int maxTotFrames = 65535;

  if( obsmode == "stare" || obsmode == "Stare" || obsmode == "STARE" ) {
    // from system design 3-34:
    NodBeams = ChopBeams = 1;
    if( SaveFrequency < 0.01 ) SaveFrequency = 0.01;
    frmreads = rint(1.0/(SaveFrequency*FrmTimeSec));
    frmreads = _MIN2_(frmreads, ceil(ObjTimeSec/(FrmTimeSec*_FrmDutyCycle)));
    frmreads = _MAX2_(4, frmreads);
    //MCE skips one frame per SaveSet even in stare mode, so min frmreads=4, but 1 less for coadds:
    FrameCoadds = (int)frmreads - 1;
    double saveDutyCycle = FrameCoadds/frmreads;
    SaveFrequency = 1.0/(frmreads*FrmTimeSec);
    SaveSets = (int)ceil(ObjTimeSec*SaveFrequency/_FrmDutyCycle/saveDutyCycle);
    SaveSets = _MAX2_(1, SaveSets);
    //impose total frame limit:
    SaveSets = _MIN2_(maxTotFrames, SaveSets);
    ObjTimeSec = saveDutyCycle*_FrmDutyCycle*SaveSets/SaveFrequency;
    ExpTimeSec = ObjTimeSec/_FrmDutyCycle/saveDutyCycle;
    NodDwellTime = 0.;
    NodSettleTime = 0.;
    ChopFrequency = 0.;
    ChopSettleTime = 0.;
  }
  else if( obsmode == "nod" || obsmode == "Nod" || obsmode == "NOD" ) {
    // from system design 3-37:
    NodBeams = 2; ChopBeams = 1;
    if( SaveFrequency < 0.01 ) SaveFrequency = 0.01;
    frmreads = rint(1.0/(SaveFrequency*FrmTimeSec));
    frmreads = _MIN2_(frmreads, ceil(NodDwellTime/(FrmTimeSec*_FrmDutyCycle)));
    frmreads = _MAX2_(4, frmreads);
    //MCE skips one frame per SaveSet even in stare mode, so min frmreads=4, but 1 less for coadds:
    FrameCoadds = (int)frmreads - 1;
    SaveFrequency = 1.0/(frmreads*FrmTimeSec);
    SaveSets = (int)ceil(NodDwellTime*SaveFrequency/_FrmDutyCycle);
    SaveSets = _MAX2_(1, SaveSets);
    NodSets = (int)ceil(ObjTimeSec/(SaveSets*FrameCoadds*FrmTimeSec*_FrmDutyCycle));
    //impose total frame limit:
    if( NodSets * NodBeams * SaveSets > maxTotFrames ) {
      NodSets = maxTotFrames/(NodBeams * SaveSets);
      if( NodSets < 1 ) {
	NodSets = 1;
	SaveSets = maxTotFrames/NodBeams;
      }
    }
    ObjTimeSec = NodSets*SaveSets*FrameCoadds*FrmTimeSec*_FrmDutyCycle;
    NodDwellTime = SaveSets/SaveFrequency;
    NodSettleReads = (int)ceil(NodSettleTime/FrmTimeSec);
    NodSettleReads = _MAX2_(1,NodSettleReads);
    NodSettleTime = 1.0*NodSettleReads*FrmTimeSec;
    NodDutyCycle = NodDwellTime/(NodDwellTime+NodSettleTime); 
    ChopFrequency = 0.;
    ChopSettleTime = 0.;
    double saveDutyCycle = FrameCoadds/frmreads;
    ExpTimeSec = ObjTimeSec/_FrmDutyCycle/NodDutyCycle/saveDutyCycle;
  }
  else if( obsmode == "chop" || obsmode == "Chop" || obsmode == "CHOP" ) {
    // from system design 3-38,39:
    NodBeams = 1; ChopBeams = 2;
    if( ChopFrequency < 0.01 ) ChopFrequency = 0.01;
    if( SaveFrequency < 0.01 ) SaveFrequency = 0.01;
    ChopSettleTimeSec = (1.0 - SCSDutyCycle)/(ChopBeams*ChopFrequency);
    ChopSettleReads = _MAX2_( 1, (int)ceil(ChopSettleTimeSec/FrmTimeSec) );
    ++ChopSettleReads; //add one more skip because of continuous readout (does not work with 1 + _MAX2_).
    ChopSettleTimeSec = ChopSettleReads*FrmTimeSec;
    frmreads = ceil( 1.0/(ChopFrequency*ChopBeams*FrmTimeSec) );
    frmreads = _MAX2_( frmreads, 3 + ChopSettleReads );
    FrameCoadds = (int)(frmreads - ChopSettleReads);
    ChopDutyCycle = FrameCoadds/frmreads;
    if( ChopDutyCycle < minDutyCycle ) {
      FrameCoadds = (int)ceil( ChopSettleReads*minDutyCycle/(1-minDutyCycle) );
      frmreads = FrameCoadds + ChopSettleReads;
      ChopDutyCycle = FrameCoadds/frmreads;
    }
    ChopFrequency = 1.0/(ChopBeams*frmreads*FrmTimeSec);
    SaveFrequency = _MIN2_( SaveFrequency, ChopFrequency );
    ChopCoadds = (int)rint(ChopFrequency/SaveFrequency);
    ChopCoadds = (int)_MIN2_( ChopCoadds, (int)ceil(ObjTimeSec/(FrameCoadds*FrmTimeSec*_FrmDutyCycle)) );
    ChopCoadds = (int)_MAX2_( 3, ChopCoadds );
    SaveSets = (int)ceil(ObjTimeSec/(ChopCoadds*FrameCoadds*FrmTimeSec*_FrmDutyCycle));
    SaveSets = _MAX2_(1, SaveSets);
    //impose total frame limit:
    SaveSets = _MIN2_( maxTotFrames/ChopBeams, SaveSets );
    SaveFrequency = ChopFrequency/ChopCoadds;
    ObjTimeSec = SaveSets*ChopCoadds*FrameCoadds*FrmTimeSec*_FrmDutyCycle;
    ExpTimeSec = 2*ObjTimeSec/_FrmDutyCycle/ChopDutyCycle;
    NodDwellTime = 0.;
    NodSettleTime = 0.;
    PreValidChops = _MAX2_( 1, (int)ceil( preValidChopTime*ChopFrequency ) );
    PostValidChops = _MAX2_( 1, (int)ceil( postValidChopTime*ChopFrequency ) );
    preValidChopTime = PreValidChops/ChopFrequency;
    postValidChopTime = PostValidChops/ChopFrequency;
  }
  else if( (obsmode.find("chop") != string::npos || obsmode.find("Chop") != string::npos ||
	    obsmode.find("CHOP") != string::npos) &&
	   (obsmode.find("nod") != string::npos || obsmode.find("Nod") != string::npos ||
	    obsmode.find("NOD") != string::npos) ) {
    // from system design 3-40,41:
    NodBeams = ChopBeams = 2;
    if( ChopFrequency < 0.01 ) ChopFrequency = 0.01;
    if( SaveFrequency < 0.01 ) SaveFrequency = 0.01;
    ChopSettleTimeSec = (1.0 - SCSDutyCycle)/(ChopBeams*ChopFrequency);
    ChopSettleReads = _MAX2_( 1, (int)ceil(ChopSettleTimeSec/FrmTimeSec) );
    ++ChopSettleReads; //add one more skip because of continuous readout (does not work with 1 + _MAX2_).
    ChopSettleTimeSec = ChopSettleReads*FrmTimeSec;
    frmreads = ceil( 1.0/(ChopFrequency*ChopBeams*FrmTimeSec) );
    frmreads = _MAX2_( frmreads, 3 + ChopSettleReads );
    FrameCoadds = (int)(frmreads - ChopSettleReads);
    ChopDutyCycle = FrameCoadds/frmreads;
    if( ChopDutyCycle < minDutyCycle ) {
      FrameCoadds = (int)ceil( ChopSettleReads*minDutyCycle/(1-minDutyCycle) );
      frmreads = FrameCoadds + ChopSettleReads;
      ChopDutyCycle = FrameCoadds/frmreads;
    }
    ChopFrequency = 1.0/(ChopBeams*frmreads*FrmTimeSec);
    SaveFrequency = _MIN2_(SaveFrequency, ChopFrequency);
    SaveFrequency = _MAX2_(SaveFrequency, 1.0/NodDwellTime);
    ChopCoadds = (int)rint(ChopFrequency/SaveFrequency);
    ChopCoadds = _MAX2_(3, ChopCoadds);
    SaveFrequency = ChopFrequency/ChopCoadds;
    SaveSets = (int)rint(NodDwellTime*SaveFrequency);
    SaveSets = _MAX2_(1, SaveSets);
    NodSets = (int)ceil(ObjTimeSec/(NodBeams*SaveSets*ChopCoadds*FrameCoadds*FrmTimeSec*_FrmDutyCycle));
    //impose total frame limit:
    if( NodSets * NodBeams * SaveSets * ChopBeams > maxTotFrames ) {
      NodSets = maxTotFrames/(NodBeams * SaveSets * ChopBeams);
      if( NodSets < 1 ) {
	NodSets = 1;
	SaveSets = maxTotFrames/(NodBeams * ChopBeams);
      }
    }
    ObjTimeSec = NodSets*NodBeams*SaveSets*ChopCoadds*FrameCoadds*FrmTimeSec*_FrmDutyCycle;
    NodSettleChops = (int)ceil(ChopFrequency*NodSettleTime);
    NodSettleChops = _MAX2_(1, NodSettleChops);
    NodSettleReads = (int)(NodSettleChops*ChopBeams*(FrameCoadds+ChopSettleReads));
    NodSettleTime = NodSettleChops/ChopFrequency;
    NodDwellTime = SaveSets/SaveFrequency;
    NodDutyCycle = NodDwellTime/(NodDwellTime + NodSettleTime);
    ExpTimeSec = 2*ObjTimeSec/_FrmDutyCycle/ChopDutyCycle/NodDutyCycle;
    PreValidChops = _MAX2_( 1, (int)ceil( preValidChopTime*ChopFrequency ) );
    PostValidChops = _MAX2_( 1, (int)ceil( postValidChopTime*ChopFrequency ) );
    preValidChopTime = PreValidChops/ChopFrequency;
    postValidChopTime = PostValidChops/ChopFrequency;
  }
  else {
    clog<<"UFMCE4Config::eval> ?unknown/unsupported obsmode= "<<obsmode <<endl;
    return -1;
  }

  ppadj["FrameTime"] = FrmTimeSec*1000.0;
  ppadj["MetaFrameTime"] = pp["MetaFrameTime"];
  ppadj["SaveFrequency"] = SaveFrequency;
  ppadj["OnSourceTime"] = ObjTimeSec/60.0;
  ppadj["ElapsedTime"] = ExpTimeSec/60.0;
  ppadj["ChopFrequency"] = ChopFrequency;
  ppadj["ChopSettleTime"] = ChopSettleTimeSec*1000.0;
  ppadj["NodDwellTime"] = NodDwellTime;
  ppadj["NodSettleTime"] = NodSettleTime;
  ppadj["FrameDutyCycle"] = 100*_FrmDutyCycle;
  ppadj["ChopDutyCycle"] = 100*ChopDutyCycle;
  ppadj["SCSDutyCycle"] = 100*SCSDutyCycle;
  ppadj["NodDutyCycle"] = 100*NodDutyCycle;   //convert duty cycles back to % units.
  ppadj["preValidChopTime"] = preValidChopTime;
  ppadj["postValidChopTime"] = postValidChopTime;

  _SCSFreq = 1.0 / ( 1.0/ChopFrequency - _SCSDelta);

  hp["FrameCoadds"] = FrameCoadds;
  hp["ChopSettleReads"] = ChopSettleReads;
  hp["ChopCoadds"] = ChopCoadds;
  hp["NodSettleChops"] = NodSettleChops;
  hp["NodSettleReads"] = NodSettleReads;
  hp["SaveSets"] = SaveSets;
  hp["NodSets"] = NodSets;
  hp["PreValidChops"] = PreValidChops;
  hp["PostValidChops"] = PostValidChops;
  hp["PixClock"] = PixClock;
  hp["ChopBeams"] = ChopBeams;
  hp["NodBeams"] = NodBeams;

  return 0;
}

// Commanding and reading of detector Bias or PreAmp DACs:

int UFMCE4Config::cmdDAC(UFDeviceAgent::CmdInfo* info, bool& readings, bool& didInitBias, float flush) {
  // assume that a car must be followed by a Bias or Preamp directive
  string cmdname;
  string cmdimpl;
  int offset= 0;
   
  cmdname = info->cmd_name[0];
  UFStrings::upperCase( cmdname );

  if( cmdname.find("CAR") != string::npos ) // not interested, skip it.
    offset = 1;
 
  readings = false;
  bool bias= false, preamp= false;
  string reply, errmsg;
  int nret= 0;
  clog<<"UFMCE4Config::cmdDAC> processing " << info->cmd_name.size() <<"-"<< offset
      <<" cmd -> directive pairs..."<<endl;

  for( int i = offset; i < (int)info->cmd_name.size(); ++i ) {
    // this has gotten pretty clumsy,
    //  should change the UFDeviceAgent::CmdInfo def. to use a map key=name val=impl...
    cmdname = info->cmd_name[i];
    cmdimpl = info->cmd_impl[i];
    UFStrings::upperCase( cmdimpl );
    if( cmdimpl.find("DC:BIPASSENBLIN") != string::npos )
      break; //this is not a DAC cmd (just epics stuff) so skip it.

    errmsg = stripErrmsg( cmdname );
    clog<<"UFMCE4Config::cmdDAC> "<<cmdname<<" : "<<cmdimpl<<endl;
    int nc;

    if( cmdimpl.find("READ") != string::npos ||
	cmdimpl.find("ADRB") != string::npos ||
	cmdimpl.find("AORP") != string::npos ) {
      readings = true;
      if( cmdimpl.find("BIAS") != string::npos ||
	  cmdimpl.find("ADRB") != string::npos ) bias = true;
      if( cmdimpl.find("PREAMP") != string::npos ||
	  cmdimpl.find("AORP") != string::npos ) preamp = true;
    }

    if( cmdimpl.find("INIT") != string::npos && cmdimpl.find("BIAS") != string::npos )
      didInitBias = true;

    if( _devIO ) {
      if( cmdimpl.find("POWER_UP") != string::npos ) { //start idle clocking before power up.
	string repli;
	_devIO->submit("CT 0", repli, flush);
	clog<<"UFMCE4Config::cmdDAC> MCE reply: "<<repli<<endl;
	_devIO->submit("START", repli, flush);
	clog<<"UFMCE4Config::cmdDAC> MCE reply: "<<repli<<endl;
      }
      nc = _devIO->submit( cmdimpl, reply, flush );
    }
    else {
      if( readings ) {
	if( bias )
	  _simDACBias(reply);
	else if( preamp )
	  _simDACPreamp(reply);
	else
	  reply = "simulation reading ?";
      }
      else reply = "simulation";
      nc = 1;
      clog<<"UFMCE4Config::cmdDAC> no device connection... assuming simulation..."<<endl;
      if( cmdimpl.find("POWER_UP") != string::npos )
	clog<<"UFMCE4Config::cmdDAC> normally would set MCE to idle CT..."<<endl;
      if( cmdimpl.find("WELL") != string::npos )
	_wellDepth = ::atoi( (cmdimpl.substr( cmdimpl.find("WELL")+4 )).c_str() );
      if( cmdimpl.find("DET_BIAS") != string::npos )
	_biasLevel = ::atoi( (cmdimpl.substr( cmdimpl.find("DET_BIAS")+8 )).c_str() );
    }

    if( nc <= 0 ) {
      reply = errmsg + reply;
      nret = nc;
    }
    else if( reply.find("Invalid") != string::npos ) {
      clog<<"UFMCE4Config::cmdDAC> MCE reply: "<<reply<<endl;
      reply = reply.substr( reply.find("Invalid") );
      nret = 0;
    }
    else if( reply.find("Error") != string::npos ) {
      clog<<"UFMCE4Config::cmdDAC> MCE reply: "<<reply<<endl;
      reply = reply.substr( reply.find("Error") );
      nret = 0;
    }
    else if( reply.find("WARNING") != string::npos ) {
      clog<<"UFMCE4Config::cmdDAC> MCE reply: "<<reply<<endl;
      reply = reply.substr( reply.find("WARNING") );
      nret = 0;
    }
    else {
      if( readings ) {
	if( bias )
	  nret = _DACBiasReadings(reply);
	else if( preamp )
	  nret = _DACPreampReadings(reply);
      }
      else {
	clog<<"UFMCE4Config::cmdDAC> MCE reply: "<<reply<<endl;
	nret = nc;
      }
    }

    info->cmd_reply.push_back(reply);
    if( nret <= 0 ) return nret;
  }

  return nret;
}

int UFMCE4Config::_DACBiasReadings(const string& mcebias) {
  // parse the mce bias readings (reply) into the map
  size_t eqpos = mcebias.find("=");
  size_t eolpos = mcebias.find("\r", eqpos);
  size_t len = eolpos - eqpos;
  string line= "";
  int i= 0;
  while( eqpos != string::npos && i < 24 ) {
    line = mcebias.substr(++eqpos, len);
    UFFITSheader::rmJunk(line); // eliminate \n \r \t and extraneous spaces
    _biasP[i++] = line;
    eqpos = mcebias.find("=", eolpos);
    eolpos = mcebias.find("\r", eqpos);
    len = eolpos - eqpos;
  }
  size_t Vpos = mcebias.find("V_DETGRV");
  if( Vpos != string::npos ) {
    eqpos = mcebias.find("=",Vpos);
    eolpos = mcebias.find("(", eqpos);
    line = mcebias.substr( ++eqpos, eolpos - eqpos );
    _VdetgrvDACval = ::atoi( line.c_str() );
  }
  return (int)_biasP.size(); 
}

int UFMCE4Config::_DACPreampReadings(const string& mcepreamp) {
  // parse the mce preamp readings (reply) into the map
  size_t eqpos = mcepreamp.find("=");
  size_t eolpos = mcepreamp.find("\r", eqpos);
  size_t len = eolpos - eqpos;
  string line= "";
  int i= 0;
  while( eqpos != string::npos && i < 16 ) { //T-ReCS uses first 16 preamp channels out of 32.
    line = mcepreamp.substr(++eqpos, len);
    UFFITSheader::rmJunk(line); // eliminated \n \r \t and extraneous spaces
    _preamP[i++] = line;
    eqpos = mcepreamp.find("=", eolpos);
    eolpos = mcepreamp.find("\r", eqpos);
    len = eolpos - eqpos;
  }
  return (int)_preamP.size(); 
}

void UFMCE4Config::_simDACBias( string& reply ) {
      reply  = "ARRAY_DAC_READBACK_BIAS\r";
      reply += " Bias Board DAC Table Listing....\r";
      reply += "Channel #  0 : V_CASUC      =    26 (0x1a)   ;  Def:026, Min:023, Max:029\r";
      reply += "Channel #  1 : V_RSTUC      =    27 (0x1b)   ;  Def:027, Min:024, Max:030\r";
      reply += "Channel #  2 : V_DETGRDRNG  =    24 (0x18)   ;  Def:024, Min:021, Max:027\r";
      reply += "Channel #  3 : V_DETGRV     =    42 (0x2a)   ;  Def:042, Min:019, Max:046\r";
      reply += "Channel #  4 : V_CLCOL      =    25 (0x19)   ;  Def:025, Min:022, Max:028\r";
      reply += "Channel #  5 : V_SHPCRG     =    24 (0x18)   ;  Def:024, Min:021, Max:027\r";
      reply += "Channel #  6 : V_SH         =   255 (0xff)   ;  Def:255, Min:229, Max:255\r";
      reply += "Channel #  7 : V_OFFSET     =    27 (0x1b)   ;  Def:027, Min:024, Max:030\r";     
      reply += "Channel #  8 : V_CLOUT      =    26 (0x1a)   ;  Def:026, Min:023, Max:029\r";    
      reply += "Channel #  9 : V_PW         =    25 (0x19)   ;  Def:025, Min:022, Max:028\r";     
      reply += "Channel # 10 : V_VSS_1      =    25 (0x19)   ;  Def:025, Min:022, Max:028\r";     
      reply += "Channel # 11 : V_VSS_2      =    25 (0x19)   ;  Def:025, Min:022, Max:028\r";     
      reply += "Channel # 12 : V_VSS_D      =    26 (0x1a)   ;  Def:026, Min:023, Max:029\r";     
      reply += "Channel # 13 : V_VGG_1      =    24 (0x18)   ;  Def:024, Min:021, Max:027\r";    
      reply += "Channel # 14 : V_VGG_2      =    26 (0x1a)   ;  Def:026, Min:023, Max:029\r";     
      reply += "Channel # 15 : V_VGG_3      =    27 (0x1b)   ;  Def:027, Min:024, Max:030\r";     
      reply += "Channel # 16 : V_VGG_4      =    24 (0x18)   ;  Def:024, Min:021, Max:027\r";     
      reply += "Channel # 17 : V_IMUX       =    28 (0x1c)   ;  Def:028, Min:025, Max:031\r";     
      reply += "Channel # 18 : V_CAP        =   255 (0xff)   ;  Def:255, Min:229, Max:255\r";     
      reply += "Channel # 19 : V_GATE_HI    =   102 (0x66)   ;  Def:102, Min:091, Max:112\r";     
      reply += "Channel # 20 : V_GATE_LO    =    25 (0x19)   ;  Def:025, Min:022, Max:028\r";     
      reply += "Channel # 21 : V_GATE       =   102 (0x66)   ;  Def:102, Min:000, Max:255\r";     
      reply += "Channel # 22 : V_NEG        =    25 (0x19)   ;  Def:025, Min:022, Max:028\r";     
      reply += "Channel # 23 : V_SUB        =   255 (0xff)   ;  Def:255, Min:229, Max:255\r";     
}

void UFMCE4Config::_simDACPreamp( string& reply ) {
      reply  = "ARRAY_OFFSET_READBACK_PREAMP\r";
      reply += " Preamp DAC Offset Table Listing....\r";
      reply += "Channel #  0 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel #  1 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel #  2 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel #  3 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel #  4 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel #  5 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel #  6 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel #  7 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel #  8 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel #  9 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 10 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 11 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 12 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 13 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 14 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 15 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 16 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 17 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 18 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 19 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 20 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 21 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 22 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 23 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 24 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 25 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 26 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 27 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 28 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 29 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 30 =      255 (0xff)   ;   DEF:255\r";
      reply += "Channel # 31 =      255 (0xff)   ;   DEF:255\r";
}

// MCE4 terminator is carriage-return:
string UFMCE4Config::terminator() { return "\r"; }

// MCE4 prefix is none:
//string UFMCE4Config::prefix() { return ""; }

vector< string >& UFMCE4Config::queryCmds() {
  if( _queries.size() > 0 ) // already set
    return _queries;

  _queries.push_back("STATUS");
  _queries.push_back("CONFIG");

  return _queries;
}

vector< string >& UFMCE4Config::actionCmds() {
  if( _actions.size() > 0 ) // already set
    return _actions;

  // agent Par directives:
  _actions.push_back("HAR");
  _actions.push_back("PHY");
  _actions.push_back("meta"); _actions.push_back("Meta"); _actions.push_back("META");

   // agent Acq directives:
  _actions.push_back("ABOR");
  _actions.push_back("CLOSE");
  _actions.push_back("CONF");
  _actions.push_back("OPEN");
  _actions.push_back("START");
  _actions.push_back("STOP");
  _actions.push_back("TEST");

 // these are known mce4 dierctives: 
  _actions.push_back("?");
  _actions.push_back("DAC");
  _actions.push_back("DLEN");
  _actions.push_back("HELP");
  _actions.push_back("CHOP");
  // setting cycle types and loading acq. variables does not affect
  // the current/active acquisition (non-idle mce4). 
  // the new parameters will affect the next acq. (run/start):
  _actions.push_back("CT");
  _actions.push_back("CYCLE");
  _actions.push_back("DLAB");
  _actions.push_back("DLAP");
  _actions.push_back("DLSB");
  _actions.push_back("DLSP");
  _actions.push_back("DSSB");
  _actions.push_back("DSSP");
  _actions.push_back("DVAR");
  _actions.push_back("DUMP_PERIOD");
  _actions.push_back("LDVAR");
  _actions.push_back("NOD");
  _actions.push_back("PBC");
  _actions.push_back("REP");
  _actions.push_back("RESET");
  // note that "run" waits for current processing (in mce4) to complete
  _actions.push_back("RUN");
  // to switch out of sim mode issue "sim 0"
  _actions.push_back("SIM");
  // "start" will abort whatever is currently running on mce4 and start new acq.
  _actions.push_back("TIME");

  // long versions of dac preamp * bias cmds:
  _actions.push_back("PREAMP_DAC");
  _actions.push_back("DAC_SET_SINGLE_PREAMP");
  _actions.push_back("DAC_LAT_SINGLE_PREAMP");
  _actions.push_back("DAC_LAT_ALL_PREAMP");
  _actions.push_back("BIAS_DAC");
  _actions.push_back("DAC_SET_SINGLE_BIAS");
  _actions.push_back("DAC_LAT_SINGLE_BIAS");
  _actions.push_back("DAC_LAT_ALL_BIAS");
  _actions.push_back("DAC_MODE_BIAS");
  _actions.push_back("DAC_REF_ENABLE_BIAS");
  _actions.push_back("DUMP_PERIOD");

  // new dac commands:
  _actions.push_back("ARRAY_WELL"); _actions.push_back("AWELL");
  _actions.push_back("ARRAY_VGATE"); _actions.push_back("AVGATE");
  _actions.push_back("ARRAY_SET_DET_BIAS"); _actions.push_back("ASDB");     
  _actions.push_back("ARRAY_PW_BIAS"); _actions.push_back("APWB");
  _actions.push_back("ARRAY_INIT_STATE_BIAS"); _actions.push_back("AISB");
  _actions.push_back("ARRAY_POWER_UP_BIAS"); _actions.push_back("APUB");
  _actions.push_back("ARRAY_POWER_DOWN_BIAS"); _actions.push_back("APDB");
  _actions.push_back("ARRAY_READBACK_BIAS");
  _actions.push_back("ARRAY_DAC_READBACK_BIAS"); _actions.push_back("ADRB");
  _actions.push_back("ARRAY_OFFSET_DEFAULTS_PREAMP"); _actions.push_back("AODP");
  _actions.push_back("ARRAY_OFFSET_GLOBAL_PREAMP"); _actions.push_back("AOGP");
  _actions.push_back("ARRAY_OFFSET_SINGLE_PREAMP"); _actions.push_back("AOSP");
  _actions.push_back("ARRAY_OFFSET_ADJUST_PREAMP"); _actions.push_back("AOAP");
  _actions.push_back("ARRAY_OFFSET_READBACK_PREAMP"); _actions.push_back("AORP");
  
  _actions.push_back("ARRAY_PATTERN_CLOCK_LEN"); _actions.push_back("APCL");
  _actions.push_back("ARRAY_SAMPLING_TYPE"); _actions.push_back("AST");
  _actions.push_back("LOAD_PAT_RAM"); _actions.push_back("LPR");
  return _actions;
}

int UFMCE4Config::validCmd(const string& c) {
  char* cs = (char*)c.c_str();
  while( *cs == ' ' || *cs == '\t' ) ++cs; //eliminate leading whites
  string cc = cs;
  UFStrings::upperCase(cc); // compare as uppercase
  
  // mce4 always echos cmd back along with optional reply info.
  vector< string >& av = actionCmds();
  int i= 0, cnt = (int)av.size();
  while( i < cnt ) {
    if( cc.find(av[i++]) != string::npos ) {
      return 1; 
    }
  }
  
  vector <string >& qv = queryCmds();
  i= 0; cnt = (int)qv.size();
  while( i < cnt ) {
    if( cc.find(qv[i++]) != string::npos )
      return 1;
  }

  int vc = validAcq(cc);
  if( vc >= 0 )
    return 1;
 
  clog<<"UFMCE4Config::validCmd> unknown cmd: "<<cc<<endl; 
  return vc;
}

#endif // __UFMCE4Config_cc__

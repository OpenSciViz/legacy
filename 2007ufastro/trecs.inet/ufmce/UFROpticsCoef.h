#if !defined(__UFROpticsCoef_h__)
#define __UFROpticsCoef_h__ "$Name:  $ $Id: UFROpticsCoef.h,v 0.0 2002/06/03 17:42:20 hon beta $"
#define __UFROpticsCoef_H__(arg) const char arg##UFROpticsCoef_h__rcsId[] = __UFROpticsCoef_h__;

struct UFROpticsCoef {
  double wavelength, Xc;
  int Ncf;
  double coeff[6];
  inline double TaylorSeries( double LambdaLow, double LambdaHi, double TC ) const {
    double sum=0.0;
    double TK = TC + 273;
    double xac=1, Xac = 1.43784e4/(LambdaHi*TK) - Xc;
    double xbc=1, Xbc = 1.43784e4/(LambdaLow*TK) - Xc;
    int ifact=1;
    for( int i=0; i<Ncf; i++ ) {
      ifact *= (i+1);
      xac *= Xac;
      xbc *= Xbc;
      sum += ( (xbc - xac)*(coeff[i]/ifact) );
    }
    return sum;
  }
};

const UFROpticsCoef _ROpticsCoefs[] = {
  { 9.0, 5.7096, 6,
    {0.1084, -0.0708, 0.0399, -0.0159, -0.0013, 0.0116}
  },
  { 17.8, 2.8869, 6,
    {0.4921, -0.1802, -0.0213, 0.1192, -0.1271, 0.0698}
  },
};

struct UFFiducialParams {
  double OptSkyCoupling;      //"Coupling" constant between Optics and Sky ratios.
  double LambdaLow, LambdaHi; //passband wavelenth in micrometers.
  double Qtrans;
  double TCoptics, emiOptics;  //temperature in Centigrade.
  double TCsky, emiSky;
  double mmH2O, AirMass;      //water vapour in mm.
};

const UFFiducialParams _FiducialConfig = { 4.27, 11.09, 12.22, 0.836, 7.0, 0.08, 0.0, 0.021, 1.0, 1.0 };

#endif // __UFROpticsCoef_h__

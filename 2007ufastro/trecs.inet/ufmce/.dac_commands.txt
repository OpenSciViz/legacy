cam: MCE version 3+
Using BIAS_DAC initialization file c:\workfiles\idl\oscir\clients\command\config\T-ReCS.bias_dacs
Using PREAMP_DAC initialization file c:\workfiles\idl\oscir\clients\command\config\T-ReCS.preamp_dacs
Using PREAMP_DAC initialization file c:\workfiles\idl\oscir\clients\command\config\T-ReCS.preamp_dacs
PREAMP_DAC Latch Group 1
DAC_SET_SINGLE_PREAMP 0 0
DAC  0 set to   0  ==>  Offset_01 set to -5.00 volts
DAC_SET_SINGLE_PREAMP 2 0
DAC  2 set to   0  ==>  Offset_02 set to -5.00 volts
DAC_SET_SINGLE_PREAMP 4 0
DAC  4 set to   0  ==>  Offset_03 set to -5.00 volts
DAC_SET_SINGLE_PREAMP 6 0
DAC  6 set to   0  ==>  Offset_04 set to -5.00 volts
DAC_SET_SINGLE_PREAMP 8 0
DAC  8 set to   0  ==>  Offset_05 set to -5.00 volts
DAC_SET_SINGLE_PREAMP 10 0
DAC 10 set to   0  ==>  Offset_06 set to -5.00 volts
DAC_SET_SINGLE_PREAMP 12 0
DAC 12 set to   0  ==>  Offset_07 set to -5.00 volts
DAC_SET_SINGLE_PREAMP 14 0
DAC 14 set to   0  ==>  Offset_08 set to -5.00 volts
DAC_SET_SINGLE_PREAMP 16 0
DAC 16 set to   0  ==>  Offset_09 set to -5.00 volts
DAC_SET_SINGLE_PREAMP 18 0
DAC 18 set to   0  ==>  Offset_10 set to -5.00 volts
DAC_SET_SINGLE_PREAMP 20 0
DAC 20 set to   0  ==>  Offset_11 set to -5.00 volts
DAC_SET_SINGLE_PREAMP 22 0
DAC 22 set to   0  ==>  Offset_12 set to -5.00 volts
DAC_SET_SINGLE_PREAMP 24 0
DAC 24 set to   0  ==>  Offset_13 set to -5.00 volts
DAC_SET_SINGLE_PREAMP 26 0
DAC 26 set to   0  ==>  Offset_14 set to -5.00 volts
DAC_SET_SINGLE_PREAMP 28 0
DAC 28 set to   0  ==>  Offset_15 set to -5.00 volts
DAC_SET_SINGLE_PREAMP 30 0
DAC 30 set to   0  ==>  Offset_16 set to -5.00 volts
DAC_LAT_ALL_PREAMP
Using BIAS_DAC initialization file c:\workfiles\idl\oscir\clients\command\config\T-ReCS.bias_dacs
BIAS_DAC Latch Group 1
DAC_SET_SINGLE_BIAS 0 26
DAC  0 set to  26  ==>  V_CasUC  set to -3.50 volts
DAC_SET_SINGLE_BIAS 1 27
DAC  1 set to  27  ==>  V_RstUC  set to -2.00 volts
DAC_SET_SINGLE_BIAS 2 24
DAC  2 set to  24  ==>  V_DetGrd set to -5.29 volts
DAC_SET_SINGLE_BIAS 3 76
DAC  3 set to  76  ==>  V_DetGrv set to -5.29 volts
DAC_SET_SINGLE_BIAS 4 25
DAC  4 set to  25  ==>  V_ClCol  set to -5.49 volts
DAC_SET_SINGLE_BIAS 5 24
DAC  5 set to  24  ==>  V_ShPchg set to -3.00 volts
DAC_SET_SINGLE_BIAS 6 255
DAC  6 set to 255  ==>  V_Sh     set to  0.00 volts
DAC_SET_SINGLE_BIAS 7 27
DAC  7 set to  27  ==>  V_Offset set to -1.80 volts
DAC_SET_SINGLE_BIAS 8 26
DAC  8 set to  26  ==>  V_ClOut  set to -4.30 volts
DAC_SET_SINGLE_BIAS 9 25
DAC  9 set to  25  ==>  V_Pw     set to -5.99 volts
DAC_SET_SINGLE_BIAS 10 25
DAC 10 set to  25  ==>  V_Vss1   set to -5.50 volts
DAC_SET_SINGLE_BIAS 11 25
DAC 11 set to  25  ==>  V_Vss2   set to -5.49 volts
DAC_SET_SINGLE_BIAS 13 255
DAC 13 set to 255  ==>  V_Vgg1   set to  0.00 volts
DAC_SET_SINGLE_BIAS 14 26
DAC 14 set to  26  ==>  V_Vgg2   set to -1.80 volts
DAC_SET_SINGLE_BIAS 15 27
DAC 15 set to  27  ==>  V_Vgg3   set to -1.70 volts
DAC_SET_SINGLE_BIAS 16 24
DAC 16 set to  24  ==>  V_Vgg4   set to -1.50 volts
DAC_SET_SINGLE_BIAS 17 28
DAC 17 set to  28  ==>  V_Imux   set to -1.60 volts
DAC_SET_SINGLE_BIAS 18 255
DAC 18 set to 255  ==>  V_Cap    set to  0.00 volts
DAC_SET_SINGLE_BIAS 19 102
DAC 19 set to 102  ==>  V_GateHi set to -3.99 volts
DAC_SET_SINGLE_BIAS 20 25
DAC 20 set to  25  ==>  V_GateLo set to -6.00 volts
DAC_SET_SINGLE_BIAS 21 25
DAC 21 set to  25  ==>  V_Gate   set to -6.00 volts
DAC_SET_SINGLE_BIAS 22 25
DAC 22 set to  25  ==>  V_Neg    set to -7.01 volts
DAC_SET_SINGLE_BIAS 23 255
DAC 23 set to 255  ==>  V_Sub    set to  0.00 volts
DAC_LAT_ALL_BIAS
BIAS_DAC Latch Group 2
DAC_SET_SINGLE_BIAS 12 26
DAC 12 set to  26  ==>  V_VssD   set to -5.99 volts
DAC_LAT_ALL_BIAS
DAC_MODE_BIAS 3
DAC_REF_ENABLE_BIAS 1
BIAS_DAC Latch Group 1
DAC_SET_SINGLE_BIAS 21 128
DAC 21 set to 128  ==>  V_Gate   set to -3.31 volts
DAC_LAT_ALL_BIAS
BIAS_DAC Latch Group 2
DUMP_PERIOD 25
CYCLE 0
START

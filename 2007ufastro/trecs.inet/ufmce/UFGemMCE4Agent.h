#if !defined(__UFGemMCE4Agent_h__)
#define __UFGemMCE4Agent_h__ "$Name:  $ $Id: UFGemMCE4Agent.h,v 0.16 2003/10/07 22:22:39 varosi beta $"
#define __UFGemMCE4Agent_H__(arg) const char arg##UFGemMCE4Agent_h__rcsId[] = __UFGemMCE4Agent_h__;

#include "UFGemDeviceAgent.h"
#include "UFMCE4Config.h"
#include "UFClientSocket.h"
#include "UFStrings.h"
#include "UFFrameConfig.h"
//__UFFrameConfig_H__(UFGemMCE4Agent_h);

#include "UFFITSClient.h"

#include "string"
#include "list"
#include "map"
#include "iostream.h"

class UFGemMCE4Agent: public UFGemDeviceAgent {
public:
  static int main(int argc, char** argv, char** env);
  UFGemMCE4Agent(int argc, char** argv, char** env);
  UFGemMCE4Agent(const string& name, int argc, char** argv, char** env);
  inline virtual ~UFGemMCE4Agent() {}

  // override these UFRndRobinServ/UFDeviceAgent virtuals:
  virtual void startup();
  virtual string newClient(UFSocket* client);
  // supplemental options (should call UFDeviceAgent::options)
  virtual int options(string& servlist);
  // in addition to establishing the iocomm/annex connection to mce4,
  // open the pipe to the epics channel access "helper"
  virtual UFTermServ* init(const string& host= "", int port= 0);

  virtual UFProtocol* action(UFDeviceAgent::CmdInfo* act);
  // new signature for action:
  virtual int action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& rep);

  virtual int query(const string& q, vector<string>& reply);
  virtual void* ancillary(void* p);
  virtual void hibernate();

  // create the basic FITS header which contains NAXIS values,
  // defined using the current _FrmConf, _ObsConf, _dhsLabel, and _ObsCompStat, etc.:
  virtual UFFITSheader* basicFITSheader();

  // set epics acquisition & readout flags
  void setEpicsAcqFlags(bool acq= false, bool rdout= false);

  //query MCE for basic frame times (at PBC=1, initializes UFMCEConfig):
  bool initMCEtiming( bool queryMCE4 = true );

  // check status of MCE, in particular the bias board voltage states,
  // setting the protected attribs: string _biasPower, _Vgate, _wellDepth, _biasLevel
  //  and then send status to Epics if connected to a db:
  void checkMCEstatus();

  // methods to connect to OTHER device agents
  // and fetch all other parts of FITS header:
  bool AgentsConnected();
  UFStrings* fetchFITSheaders();

  // interface with frame acq. service:
  bool connectFrmAcq();
  bool sendFrmAcqLUTs();
  bool checkFrmAcqReply( UFStrings* frmreply, const string& transact );
  UFFrameConfig* pollFrmAcq();
  UFStrings* notifyFrmAcq(const string& directive, UFDeviceAgent::CmdInfo* act= 0);

  // interface with dhs client
  UFStrings* notifyDHSclient(const string& directive, UFDeviceAgent::CmdInfo* act= 0);

protected:
  static UFClientSocket *_frmsoc, *_ls340soc;
  static string _frmhost, _ls340host, _ls340handshake, _warmmux;
  static int _frmport, _ls340port, _nDTqueryFail, _maxDTqueryFail, _queryDTperiod;
  static bool _frmAcqNow, _fastmode, _sentLUTs;

  //The host on which other Device Agents are running (default=localhost),
  // and if defined by arg list then _ls340host = _devAgsHost :
  static string _devAgsHost;

  // Note that "nod" of telescope is synonymous with "beam switch".
  // _nodHandShake = true means wait for EPICS to indicate beam switch is complete,
  //                                      or just count down NodSettleTime seconds.
  // _BeamSwitching = true means invoke _ancilBeamSwitch() to check for completion,
  //                       and when complete (or countdown done) tell MCE4 to RESUME coadding frames.

  static bool _nodHandShake, _BeamSwitching, _BeamSwitchError, _MCE4waitError;
  static int _prevNodBeam, _nodCycle, _prevFrmCnt;
  static float _maxBeamSwitchTime;
  static int _bgADUStart, _rdADUStart;
  static float _bgWellStart, _bgWellMax, _bgWellMin;
  static float _minDTemp, _maxDTemp;

  static UFFrameConfig* _FrmConf;
  static UFObsConfig* _ObsConf;
  static UFObsConfig* _abortedObsConf;
  static string _FrmAcqVersion;
  static string _ObsCompStat;
  static int _arrayColumns, _arrayRows, _arrayChannels;

  // Open a socket and query the Lakeshore 340 for detector array temperature,
  // return value > 0 indicates success, otherwise query failed.
  // If no reply is recvd the socket is closed (will reconnected next call).
  // (string& detKelvin is changed only if reply is recvd,
  //  but if reply is not a valid temp it is set to NULL and 0 is returned)
  int _queryLS340DT(string& detKelvin);

  // helper func. scans action bundle and decodes acquistion attribute values:
  int _acqAttributes(UFDeviceAgent::CmdInfo* act);  

  // helper func. checks frame acq status, notifies EPICS,
  //  and initiates Beam Switch when obsmode = "chop-nod":
  // if quickCheck = true then frm acq status is polled and err/warn are indicated, then returns.
  void _ancilFrmAcq( bool quickCheck = false );

  //helper functions used by _ancilFrmAqc():
  void _checkDHSfrmCnt( UFFrameConfig* frmCfg );

  // helper func. waits for Beam Switch completion from EPICS,
  //  or just counts NodSettleTime secs if _nodHandShake = false,
  //  and then tells MCE4 to resume coadding frames:
  void _ancilBeamSwitch();

  time_t _nodStartTime;  //set by _ancilFrmAcq() and compared against in _ancilBeamSwitch().
  time_t _queryLastTime; //used by ancillary to tell when 60 secs elapsed to query and send Det.Temp.

  string _biasPower, _Vgate, _wellDepth, _biasLevel;
  bool _powerUpDone;
  float airMassBeg;

  bool _archive, _dhsWrite;
  string _dhsLabel, _comment;
  static string _archvRootDir; //root dir sent to FrmAcqServer dictating where FITS files are created.

  //persistent objects used to gather FITS header from other device agents:
  UFFITSClient::AgentLoc _agentLocs;
  UFSocket::ConnectTable _agentConnections;
  static UFFITSClient* _agentFITSClient;

  // support simulation option:
  void* _simMCE4(void* p);
};

#endif // __UFGemMCE4Agent_h__
      

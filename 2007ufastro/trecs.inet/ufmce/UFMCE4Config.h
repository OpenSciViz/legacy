#if !defined(__UFMCE4Config_h__)
#define __UFMCE4Config_h__ "$Name:  $ $Id: UFMCE4Config.h,v 0.23 2003/12/04 20:44:56 varosi beta $"
#define __UFMCE4Config_H__(arg) const char arg##UFMCE4Config_h__rcsId[] = __UFMCE4Config_h__;

#include "UFDeviceConfig.h"
__UFDeviceConfig_H__(MCE4Config_h);

#include "UFDeviceAgent.h"
__UFDeviceAgent_H__(MCE4Config_h);

#include "UFTimeStamp.h"
__UFTimeStamp_H__(MCE4Config_h);

#include "UFObsConfig.h"
__UFObsConfig_H__(MCE4Config_h);

#include "UFClientSocket.h"
__UFClientSocket_H__(MCE4Config_h);

#include "UFStrings.h"
__UFStrings_H__(MCE4Config_h);

#include "UFEdtDMA.h"
__UFEdtDMA_H__(MCE4Config_h);

#include "UFFITSheader.h"
__UFFITSheader_H__(MCE4Config_h);

#include "UFRSkyCoef.h"
__UFRSkyCoef_H__(MCE4Config_h);

using namespace std;
#include "map"

// forward declarations for friendships
class UFMCE4Agent;
class UFGemMCE4Agent;

class UFMCE4Config : public UFDeviceConfig {
public:
  typedef map<string, int> HdwrParms;
  typedef map<string, double> PhysParms;
  typedef map<string, string> TextParms;
  typedef map<int, string> DACParms;
  typedef map< string, UFRSkyCoef > RSkyCoefsMap;

  enum { HdwrParm = 1, HdwrParmCnt = 11,
	 PhysParm = 2, PhysParmCnt = 11,
	 MetaParm = 3, MetaParmCnt = 24,
         DACBias = 24, DACPreamp = 16 };

  static bool _verbose;
//  static UFEdtDMA::Conf* _edtcfg;

  //create and return Look-Up-Tables (pixel maps) for CRC-774 MUX on Raytheon det.array:
  static vector< UFInts* > setRaytheonLUTs( int arrayColumns, int arrayRows, int nChannels );

  //query the MCE for actual frame time in each readout mode and store in _frameTimes:
  //set queryMCE4=false to skip actual query of MCE4 (takes 2 mins.) and use defaults.
  static bool queryMCEtiming( bool queryMCE4 = true );

  //returns PBC parameter for MCE as function of desired frame time and readout mode:
  static int pixClockAdj(double& frmTimeSec, const string& rdOutMd);

  UFMCE4Config(const string& name= "TReCSMCE4");
  UFMCE4Config(const string& name, const string& tshost, int tsport);
  inline virtual ~UFMCE4Config() {}

  // override these derived virtuals:
  virtual vector< string >& UFMCE4Config::queryCmds();
  virtual vector< string >& UFMCE4Config::actionCmds();

  // supplemental fuctions to interact with mce4:
  // accessors & mutators
  inline static int getMeta(TextParms& mp) { mp = _metaP; return mp.size(); }
  inline static int getDesired(HdwrParms& hdes, PhysParms& pdes) {
    hdes = _desH; pdes = _desP; return pdes.size();
  }
  inline static int getActive(string& obsmode, string& rdoutmode, HdwrParms& hact) {
    obsmode = _actv_obsmode; rdoutmode = _actv_readoutmode;
    hact = _activeH; return hact.size();
  }
  inline static int getActive(string& obsmode, string& rdoutmode, HdwrParms& hact, PhysParms& pact) {
    obsmode = _actv_obsmode; rdoutmode = _actv_readoutmode;
    hact = _activeH; pact = _activeP; return hact.size() + pact.size();
  }
  inline static int getActive(string& obsmode, string& rdoutmode, PhysParms& pact) {
    pact = _activeP; return pact.size();
  }
  inline static int getActive(HdwrParms& hact, PhysParms& pact) {
    hact = _activeH; pact = _activeP; return hact.size() + pact.size();
  }

  inline static double getFrameTime() { return _activeP["FrameTime"]; }
  inline static double getSaveFrequency() { return _activeP["SaveFrequency"]; }
  inline static double getNodSettleTime() { return _activeP["NodSettleTime"]; }
  inline static double getPreValidChopTime() { return _activeP["preValidChopTime"]; }
  inline static int VdetgrvDAC() { return _VdetgrvDACval; }
  inline static int getWellDepth() { return _wellDepth; }
  inline static int getBiasLevel() { return _biasLevel; }

  // format all into single string vec.
  static int getActive(vector< string >& act);

  // format all active Hardware and Physical Parms into UFFITSheader object:
  static int makeFITSheader( UFFITSheader* );

  // mce4 agent entry points...
  // some may only be of use to trecs:
  // acquisition directives: config, start, stop/abort, etc.
  // notify frame-server, get its ack. & submit directive to mce4:
  static int acquisition(const string& directive, string& reply, float flush);
  // return -1 if not an acq., 1 for start/run (idle==false), 1 for all other valid acq directives
  static int validAcq(const string& directive);
  // commit desired values; if successfull, this resets actives to 
  // be congruent with desired; this is called for acquisition directive "conf":
  static int parmConfig(string& reply, float flush);
  // clear parms and set mce4 to CT 0; this is called for acquisition directive "datum or park"
  static int clearConfig(string& reply, float flush);

  // parameter config directives:
  static bool isConfigured();
  static bool isClear(HdwrParms& hp);
  static int clear(HdwrParms& hp);
  static int clear(PhysParms& pp);
  static int clear(TextParms& mp);
  // encode parameters (for use by client)
  static UFStrings* encode(const HdwrParms& hdwr, const string& obsmode, const string& readoutmode,
			   const string& car);
  static UFStrings* encode(const PhysParms& phys, const string& obsmode, const string& readoutmode,
			   const string& car);
  static UFStrings* encode(const TextParms& meta, const string& car);

  // decode hardware, phys, meta parameters
  static int parse(const UFDeviceAgent::CmdInfo* info, HdwrParms& hp,
		   string& obsmode, string& readoutmode); 
  static int parse(const UFDeviceAgent::CmdInfo* info, PhysParms& pp,
		   string& obsmode, string& readoutmode);  
  static int parse(const UFDeviceAgent::CmdInfo* info, TextParms& mp);

  // utility method to strip off errmsg (and return it) from a command:
  static string stripErrmsg( string& cmdname );

  // set (trecs) desired params
  // sets desired hardware parms from hardware input
  static int setDes(HdwrParms& hp, const string& obsmode, const string& readoutmode);
  // sets desired hardware parms from physical input
  static int setDes(PhysParms& pp, const string& obsmode, const string& readoutmode);
  // sets desired physical & hardware parms from meta input
  static int setDes(TextParms& mp, string& errmsg);
  // get phys & hdwr parms via one vector of strings:
  static int getDes(vector<string>& phvec);
  // get phys & hdwr parms via one map of strings:strings:
  static int getDes(map<string, string>& ph);

  //compute meta frame time from meta-config params, for imaging/spectroscopy modes:
  static double metaFrameTimeImaging(TextParms& mp, string& readoutmode, string& errmsg);
  static double metaFrameTimeAdjust(double& frameTime, double bandWidth, string& readoutmode, bool change );
  static double metaFrameTimeSpectro(TextParms& mp, string readoutmode, string& errmsg);

  // generate (trecs) physical params from meta:
  static int eval(TextParms& mp, PhysParms& pp,
		  string& obsmode, string& readoutmode, string& errmsg);

  // generate (trecs) mce4 hardware params from physical:
  static int eval(PhysParms& pp, PhysParms& ppadj, HdwrParms& hp,
		  const string& obsmode, const string& readoutmode);

  // trecs epics message format
  static int trecsPhysCAR(vector<string>& tc, bool append= false);
  static int trecsMetaCAR(vector<string>& tc);
  static int trecsBiasCAR(vector<string>& tc);
  static int trecsPreampCAR(vector<string>& tc);

  // trecs DAC cmd syntax
  static int trecsBiasCmds(map< string, string >& biascmds);
  static int trecsPreampCmds(map< string, string >& preampcmds);

  // allow client to get status back in FITS format:
  virtual UFStrings* statusFITS(UFDeviceAgent* da);

  // return -1 if invalid, 0 if valid but no reply expected,
  // n > 0 if valid and n reply strings expected:
  virtual int validCmd(const string& c);

  virtual string terminator();

  // the default behavior is ok here:
  //virtual string prefix();

  static int clearAll();

  //for mid-IR observing meta-config:
  static int initRSky();
  static bool readMetaConfigFile();

  // TReCS DACs
  // new mce4 dac stuf, use info for replies, indicate if action included readings req.
  static int cmdDAC(UFDeviceAgent::CmdInfo* info, bool& readings, bool& initBias, float flush = 0.05);
  
protected:
  static void _create();

  //polynomial coeffs to compute R_Sky of each filter, for mid-IR observing,
  // to estimate optimal detector array frame integration time.
  // (see code in initRSky() and in eval(TextParms& mp, PhysParms& pp,...) ).
  static RSkyCoefsMap _RSkyCfsMap;

  static double _FrmDutyCycle;
  static double _SCSFreq, _SCSDelta; // _SCSFreq == 1.0 / ( 1/ChopFreq - _SCSDelta)

  static int _wellDepth, _biasLevel;
  static int _VdetgrvDACval; //MCE-DAC value setting detector bias level voltage.
  static int _minPBC;       //minimum allowed Pixel Base Clock
  static bool _idle;
  static int _frmCnt; // expected frame count for observation
  static pid_t _takepid;
 
  // these modes can be supplied (as strings) by either
  // physical or hardware parms:
  static string _des_obsmode, _des_readoutmode; 
  static string _actv_obsmode, _actv_readoutmode;

  // configuration parameters
  static HdwrParms _activeH; // hardware settings currently active
  static HdwrParms _desH; // desired hadrware settings
  static PhysParms _activeP; // physical params associated with active hardware params
  static PhysParms _desP; // physical params associated with desired hardware params
  static TextParms _metaP; // derived meta params associated with desired physical params.
  static PhysParms _frameTimes; //MCE PBC=1 frame times for each readout mode from queryMCEtiming().
  static PhysParms _metaConParms; //constant meta config params from method readMetaConfigFile().

  static DACParms _biasP; // DAC bias params (as provided by mce4 query)
  static int _DACBiasReadings(const string& mcereply); // store mce dac readings in hash table 
  static DACParms _preamP; // DAC preamp params (as provided by mce4 query)
  static int _DACPreampReadings(const string& mcereply); // store mce dac readings in hash table 

  static void _simDACBias(string& mcereply);
  static void _simDACPreamp(string& mcereply);

  // mce cmd/directives:
  static HdwrParms _cycletypes; // 4 possible cycle types
  static TextParms _mceparms; // CTs, LDVARs, etc.

  // FITS header keywords and comments corresponding to HdwrParms and PhysParams:
  static TextParms FITS_KeyWords;
  static TextParms FITS_Comments;

  // allow Agent access to protected elements
  friend class UFMCE4Agent;
  friend class UFGemMCE4Agent;
};

#endif // __UFMCE4Config_h__

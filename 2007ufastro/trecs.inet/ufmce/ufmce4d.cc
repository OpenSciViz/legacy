#if !defined(__ufmce4d_cc__)
#define __ufmce4d_cc__ "$Name:  $ $Id: ufmce4d.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __ufmce4d_cc__;

#include "UFMCE4Agent.h"

int main(int argc, char** argv, char** envp) {
  return UFMCE4Agent::main(argc, argv, envp);
}
      
#endif // __ufmce4d_cc__

;+
; NAME:
;   CAM_COMPUTE
;
; PURPOSE:
;   Utility procedure called by CAM procedure suite -
;     Computes CAM control parameters from input observing parameters.
;
; CATEGORY:
;   CAM widget procedure suite.
;
; CALLING SEQUENCE:
;   CAM_COMPUTE
;
; INPUTS:
;   None.
;
; KEYWORD PARAMETERS:
;   None.
;
; OUTPUTS:
;   No explicit output.
;
; EXTERNAL CALLS:
;   CAM_SYS_MCLR   - Clear system message.
;   CAM_SYS_MSSG   - Sends system message.
;
;
; MODIFICATION HISTORY:
;  Written by:
;     Robert K. Pina
;     Department of Astronomy
;     211 Bryant Space Science Center
;     University of Florida
;     Gainesville, FL 32611-2055
;     Phone: (352)392-2052 x243
;     Fax:   (352)392-5089
;     E-mail: rpina@astro.ufl.edu
;-
;
;*****************************************************************************
;*** CAM_COMPUTE (Compute CAM control parameters from user inputs)

PRO cam_compute

  FORWARD_FUNCTION gr_cmd

  common adc, adc_offset, max_coadd_adc, adc_dark_frm, adc_dark, adc_sat_frm, adc_sat
  common buffers, bufID, dsply_bufs, nbufs, p_buf_seq, buf_seq_mask, p_chart_buf, p_stat_buf, n_stat_coadd, n_frm_coadd
  common buflist, cn_list, c_list, n_list, s_list
  common cam, wid, bwid
  common control, acquire, to_file, pk_flag, fast_mode, chart_flag, chart_update, sim_mode
  common data_poll, duration, savecount, gr_new, gr_old, exptime
  common grabber, array_name, xsize, ysize, byte_depth, nbytes_per_buf, p_buf, arr_map
  common obs, obsmode, obscount, chpbeams, savesets, nodbeams, nodsets, nodcycle, nodbeam, frmcoadd, chpcoadd, noddelay, totfrms
  common peek, pk_list, npk_list, pk_zoom, pk_buf, pk_grid, grid_flag, pk_list_selected

; Don't allow computation if acquire active.
  if acquire then begin
;    cam_sys_mssg, 'Error - Can''t compute while acquiring data ...'
    return
    endif

; Clear error messages
  cam_sys_mclr

; Get default min and max values for some parameters
  min_frmtime  = float(getenv('MIN_FRMTIME'))
  max_frmtime  = float(getenv('MAX_FRMTIME'))
  min_savefreq = float(getenv('MIN_SAVEFREQ'))
  max_chpfreq  = float(getenv('MAX_CHPFREQ' ))
  max_adc_frm  = (2.0^16)-1.0
  max_frmcoadd = 65535

; Get input observing parameters

  frmtime = wstr_get(wid.IN_FRMTIME,/float,format='(f8.2)')
  if frmtime lt min_frmtime then begin
    cam_sys_mssg, 'Warning - frmtime set to minimum ...'
    frmtime = min_frmtime
    wstr_set,wid.IN_FRMTIME ,frmtime,format='(f8.2)'
    endif
  if frmtime gt max_frmtime then begin
    cam_sys_mssg, 'Warning - frmtime set to maximum ...'
    frmtime = max_frmtime
    wstr_set,wid.IN_FRMTIME ,frmtime,format='(f8.2)'
    endif

  chpfreq = wstr_get(wid.IN_CHPFREQ,/float,format='(f6.2)')
  if chpfreq le 0.0 then begin
    cam_sys_mssg, 'Error - chpfreq must be > 0 ...'
    return
    endif
  if chpfreq gt max_chpfreq then begin
    cam_sys_mssg, 'Warning - chpfreq set to max freq. ...'
    chpfreq = max_chpfreq
    wstr_set,wid.IN_CHPFREQ ,chpfreq,format='(f6.2)'
    endif

  savefreq = wstr_get(wid.IN_SAVEFREQ,/float,format='(f6.2)')
  if savefreq lt min_savefreq then begin
    cam_sys_mssg, 'Will compute minimum savefreq...'
    savefreq = min_savefreq
    wstr_set,wid.IN_SAVEFREQ ,savefreq,format='(f6.2)'
    endif
  if (obsmode eq 'chop-nod' or obsmode eq 'chop') and (savefreq gt chpfreq) then begin
    cam_sys_mssg, 'Warning - Savefreq set to chpfreq ...'
    savefreq = chpfreq
    wstr_set,wid.IN_SAVEFREQ ,savefreq,format='(f6.2)'
    endif

  objtime = wstr_get(wid.IN_OBJTIME,/float,format='(f8.3)')
  objtime = objtime*60.0
  if objtime le 0.0 then begin
    cam_sys_mssg, 'Error - Objtime must be > 0 ...'
    return
    endif

  chpdelay = wstr_get(wid.IN_CHPDELAY,/float,format='(f6.2)')
  chpdelay = chpdelay*1.0e-3
  if chpdelay lt 0.0 then begin
    cam_sys_mssg, 'Warning - Chpdelay set to 0 ...'
    chpdelay = 0.0
    wstr_set,wid.IN_CHPDELAY ,chpdelay,format='(f6.2)'
    endif

  nodtime = wstr_get(wid.IN_NODTIME,/float,format='(f5.2)')
  if nodtime le 0.0 then begin
    cam_sys_mssg, 'Error - Nodtime must be > 0 ...'
    return
    endif

  noddelay = wstr_get(wid.IN_NODDELAY,/float,format='(f5.2)')
  if strlowcase(getenv('LOCALE')) eq 'gem' then begin
    noddelay = noddelay > (float(getenv('GEM_WAIT_SCS')) + float(getenv('GEM_WAIT_DECS')))
    noddelay = noddelay + float(getenv('GEM_WAIT_GUIDE'))
    endif $
  else begin
    if noddelay lt 0.0 then begin
      cam_sys_mssg, 'Warning - Noddelay set to 0.0 ...'
      noddelay = 0.0
      wstr_set,wid.IN_NODDELAY ,noddelay,format='(f5.2)'
      endif
    endelse

;Compute register value for pixel clock and adjust frmtime.
  dCK_dT   = float(getenv('dCK_dT'))
  CK_min   = float(getenv('CK_min'))
  pixclock = round(frmtime*dCK_dT + CK_min)
  frmtime  = (pixclock-CK_min)/dCK_dT

;Convert frmtime to seconds for following calculations.
  frmtime = frmtime*1.0e-3

; Compute average frame duty cycle i.e. frmdc
;   Note that the frame duty cycle ranges between 1015/1024 and 1022/1024 (with average
;   value [1015+1022]/2.0=1018.5) which results from the simultaneous half column reset
;   but staggered readout and reset being held for 2 pixel times where pixel_time=frame_time/1024.
  frmdc = 1018.5/1024.0

;Branch according to obs mode
  CASE obsmode OF

  'chop-nod' : BEGIN
     chpbeams = 2L  &  nodbeams = 2L

     chpsettle = ceil(chpdelay/frmtime) > 1L
     chpdelay  = chpsettle*frmtime

     frmreads = round( 1.0/(chpfreq*chpbeams*frmtime) ) > 1L
     if ( (frmreads-chpsettle) gt max_frmcoadd ) then $
       frmreads = chpsettle + max_frmcoadd
     if ( (frmreads-chpsettle) lt 2L    ) then frmreads = chpsettle + 2L
     frmcoadd = frmreads - chpsettle
     chpfreq  = 1.0/(frmreads*chpbeams*frmtime)

     savefreq = savefreq > 1.0/nodtime
     savefreq = savefreq < chpfreq
     chpcoadd = round(chpfreq/savefreq) > 3L
     savefreq = chpfreq/chpcoadd

     savesets = round(nodtime*savefreq) > 1L
     nodtime  = savesets/savefreq

     nodsettle_c  = ceil(chpfreq*noddelay) > 1L
     noddelay     = nodsettle_c/chpfreq
     nodsettle_f  = nodsettle_c*chpbeams*(frmcoadd+chpsettle)

     nodsets  = ceil( objtime/(nodbeams*savesets*chpcoadd*frmcoadd*frmtime*frmdc) ) > 1L
     objtime  = nodsets*(nodbeams*savesets*chpcoadd*frmcoadd*frmtime*frmdc)

     chpdc    = float(frmcoadd)/frmreads
     noddc    = 1.0/(1.0 + noddelay/nodtime)

     prechps = long(round(float(getenv('PRE_VALID_TIME'))*chpfreq))  > 1L
     pstchps = long(round(float(getenv('POST_VALID_TIME'))*chpfreq)) > 1L


     END

  'chop' : BEGIN
     chpbeams = 2L  &  nodbeams = 1L

     chpsettle = ceil(chpdelay/frmtime) > 1L
     chpdelay  = chpsettle*frmtime

     frmreads = round( 1.0/(chpfreq*chpbeams*frmtime) ) > 1L
     if ( (frmreads-chpsettle) gt max_frmcoadd ) then $
       frmreads = chpsettle + max_frmcoadd
     if ( (frmreads-chpsettle) lt 2L    ) then frmreads = chpsettle + 2L
     frmcoadd = frmreads - chpsettle
     chpfreq  = 1.0/(frmreads*chpbeams*frmtime)

     savefreq = savefreq < chpfreq
     if savefreq le 0.0 then begin
       chpcoadd = ceil(objtime/(frmcoadd*frmtime*frmdc)) > 3L
       savesets = 1L
       endif $
     else begin
       chpcoadd = round(chpfreq/savefreq)
       chpcoadd = chpcoadd < ceil(objtime/(frmcoadd*frmtime*frmdc))
       chpcoadd = chpcoadd > 3L
       savesets = ceil(objtime/(chpcoadd*frmcoadd*frmtime*frmdc)) > 1L
       endelse

     savefreq = chpfreq/chpcoadd
     objtime  = savesets*chpcoadd*frmcoadd*frmtime*frmdc

     chpdc   = float(frmcoadd)/frmreads

     noddelay    = 0.0
     noddc   = 1.0
     nodsets     = 1L
     nodsettle_c = 0L
     nodsettle_f = 0L

     prechps = long(round(float(getenv('PRE_VALID_TIME'))*chpfreq))  > 1L
     pstchps = long(round(float(getenv('POST_VALID_TIME'))*chpfreq)) > 1L

     END

  'nod' :      BEGIN
     chpbeams = 1L  &  nodbeams = 2L

     if savefreq le 0.0 then savefreq = 1.0/nodtime
     frmreads = round( 1.0/(savefreq*frmtime) ) > 3L
     frmreads = frmreads < max_frmcoadd

     savefreq = 1.0/(frmreads*frmtime)
     savesets = round(nodtime*savefreq) > 1L
     nodtime  = savesets/savefreq
     frmcoadd = frmreads
     nodsets  = ceil( objtime/(savesets*frmcoadd*frmtime*frmdc) ) > 1L
     objtime  = nodsets*(savesets*frmcoadd*frmtime*frmdc)

     nodsettle_f = ceil(noddelay/frmtime) > 1L
     noddelay    = nodsettle_f*frmtime

     noddc    = 1.0/(1.0 + noddelay/nodtime)

     chpdelay    = 0.0
     chpdc   = 1.0
     chpfreq     = 0.0
     chpsettle   = 0L
     nodsettle_c = 0L
     chpcoadd    = 1L
     prechps     = 0L
     pstchps     = 0L

     END

  'stare' :    BEGIN
     chpbeams = 1L  &  nodbeams = 1L

     if savefreq le 0.0 then begin
       frmreads = ceil(objtime/(frmtime*frmdc)) > 3L
       endif $
     else begin
       frmreads = round( 1.0/(savefreq*frmtime) )
       frmreads = frmreads < ceil(objtime/(frmtime*frmdc))
       frmreads = frmreads > 3L
       endelse

     frmreads = frmreads < max_frmcoadd
     savefreq = 1.0/(frmreads*frmtime)
     savesets = ceil(objtime*savefreq/frmdc) > 1L
     objtime  = frmdc*savesets/savefreq

     frmcoadd = frmreads

     chpdc   = 1.0
     noddc   = 1.0
     chpdelay    = 0.0
     chpsettle   = 0L
     chpcoadd    = 1L
     nodsettle_f = 0L
     nodsettle_c = 0L
     nodsets     = 1L
     prechps     = 0L
     pstchps     = 0L

     END

  ENDCASE

; Total photon collection efficiency, on-source efficiency, elapsed exposure time, and "nod" time in stare or chop mode
  photondc = frmdc*chpdc*noddc
  if obsmode eq 'stare' then srcdc = photondc else srcdc = photondc/2.0
  exptime  = objtime/srcdc
  if nodbeams eq 1 then nodtime = exptime

; Compute number of frames per buffer coadded in hardware
   n_frm_coadd = float(frmcoadd*chpcoadd)

; Compute some totals for observation
  totfrms = chpbeams*nodbeams*savesets*nodsets
  totbyte = (xsize/1024.0)*(ysize/1024.0)*byte_depth*totfrms

; Update display
  wstr_set,wid.OUT_FRMTIME ,frmtime/1.0e-3,  format='(f8.2)'
  wstr_set,wid.OUT_SAVEFREQ,savefreq,        format='(f6.2)'
  wstr_set,wid.OUT_OBJTIME, objtime/60.0,    format='(f8.3)'
  wstr_set,wid.OUT_CHPFREQ, chpfreq,         format='(f6.2)'
  wstr_set,wid.OUT_CHPDELAY,chpdelay/1.0e-3, format='(f6.2)'
  wstr_set,wid.OUT_NODTIME, nodtime,         format='(f6.2)'
  wstr_set,wid.OUT_NODDELAY,noddelay,        format='(f6.2)'

  widget_control,wid.CNTL_CHPSETTLE_F,set_value=chpsettle
  widget_control,wid.CNTL_NODSETTLE_F,set_value=nodsettle_f
  widget_control,wid.CNTL_NODSETTLE_C,set_value=nodsettle_c
  widget_control,wid.CNTL_FRMCOADD_F, set_value=frmcoadd
  widget_control,wid.CNTL_CHPCOADD_C, set_value=chpcoadd
  widget_control,wid.CNTL_SAVESETS,   set_value=savesets
  widget_control,wid.CNTL_NODSETS,    set_value=nodsets
  widget_control,wid.CNTL_PIXCLOCK,   set_value=pixclock
  widget_control,wid.CNTL_PRECHPS_C , set_value=prechps
  widget_control,wid.CNTL_PSTCHPS_C,  set_value=pstchps

  wstr_set,wid.EFF_FRMDC,    100.0*frmdc,   format='(f6.2)'
  wstr_set,wid.EFF_CHPDC,    100.0*chpdc,   format='(f6.2)'
  wstr_set,wid.EFF_NODDC,    100.0*noddc,   format='(f6.2)'
  wstr_set,wid.EFF_PHOTONDC, 100.0*photondc,format='(f6.2)'
  wstr_set,wid.EFF_SRCDC,    100.0*srcdc,   format='(f6.2)'

  widget_control,wid.OBS_TOGO_M, set_value=floor(exptime/60.0) > 0.0
  widget_control,wid.OBS_TOGO_S, set_value=(exptime mod 60.0) > 0.0

  widget_control,wid.STAT_EXPTIME,  set_value=exptime/60.0
  widget_control,wid.STAT_TOTFRMS,  set_value=totfrms
  widget_control,wid.STAT_TOTBYTE,  set_value=totbyte
  widget_control,wid.OBS_SAVES,     set_value=long(savesets)*nodbeams*nodsets
  widget_control,wid.OBS_NODS,      set_value=nodsets

; Compute variables used for background calculation.
   max_coadd_adc = n_frm_coadd*max_adc_frm
   adc_dark  = n_frm_coadd*adc_dark_frm
   adc_sat   = n_frm_coadd*adc_sat_frm

; Initialize ADC offset adjust variable.
   adc_offset = max_coadd_adc

; Compute duration for data polling interval
   tau = 1.0/savefreq
   duration = tau - min([1.0,0.2*tau])

; Set timeout value for frame capture
  timeout=long(1.2*tau*1000.0)
  if gr_cmd({name:'timeout',p1:timeout}) ne 0 then begin
    print,'cam_compute: Frame Grabber failed to accept sync timeout ...'
;    ret=gr_cmd({name:'close'})
    return
    endif
;SIMMOD

; Set appropriate peek list selection.
  CASE obsmode OF
    'chop-nod': pk_list=cn_list
    'chop'    : pk_list=c_list
    'nod'     : pk_list=n_list
    'stare'   : pk_list=s_list
    ELSE      :
    ENDCASE
  npk_list=n_elements(pk_list)

; Set control panel to indicate obs_mode
  widget_control,wid.MODE,set_value=obsmode

  return
END


#if !defined(__UFDHSTReCS_cc__)
#define __UFDHSTReCS_cc__ "$Name:  $ $Id: UFDHSTReCS.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFDHSTReCS_cc__;

#include "UFDHSTReCS.h"
__UFDHSTReCS_H__(__UFDHSTReCS_cc);

#include "UFRuntime.h"
#include "UFGemDeviceAgent.h"

// statics/globals:
string UFDHSTReCS::_agenthost;
string UFDHSTReCS::_frmhost;
UFFITSClient* UFDHSTReCS::_ufits = 0;
UFSocket::ConnectTable UFDHSTReCS::_connections;
bool UFDHSTReCS::_observatory= false;
float UFDHSTReCS::_timeout = 100; // max nod settle time 100 sec.
float UFDHSTReCS::_fitstimeout = 1.0; // FITS fetch timeout
bool UFDHSTReCS::_qlook = false;
int UFDHSTReCS::_archvThrdCnt= 0;

// mutex to deal with thread-safety here and perhaps in stdlib
static pthread_mutex_t _mutex;

// ctor should alloc. instrument spec. qlook streams
UFDHSTReCS::UFDHSTReCS(int w, int h) : UFDHSwrap("TReCS", w, h) {
  ::pthread_mutex_init(&_mutex, 0);
  UFDHSwrap::_archvDataLabel = "";
  UFDHSwrap::_finalKeyExt = "2";
  // q-look dhs streams & buffers need only be created once:
  UFDHSwrap::_QlookNames.clear(); UFDHSwrap::_QlookStrms.clear();
  //clog<<"UFDHSTReCS> set qlook stream names"<<endl;
  vector < string > qlnames;
  int qcnt = UFObsConfig::getBufNames("trecs", qlnames);
  if( _verbose )
    clog<<"UFDHSTReCS> setting qlook stream names, cnt= "<<qcnt<<endl;
  UFDHSwrap::setQlNames(qlnames);
}

DHS_STATUS UFDHSTReCS::open(const string& serverHost, const string& service) {
  if( UFDHSwrap::open(serverHost, service) != DHS_S_SUCCESS )
    return _status;

  //clog<<"UFDHSTReCS::open> alloc qlook streams: "<<UFDHSwrap::_QlookNames.size()<<endl;
  // allocate one time -- reuse data buffer
  
  for( size_t i = 0; i < _QlookNames.size(); ++i ) {
    //clog<<"UFDHSwrap> alloc _QlookStrms: "<<_QlookNames[i]<<endl;
    UFDHSwrap::Stream* qls = new UFDHSwrap::Stream(_QlookNames[i]);
    UFDHSwrap::_QlookStrms[_QlookNames[i]] = qls;
    qls->_dataLabel = ""; // force generation of new dataleabel for each qlstream
    UFDHSwrap::newDataset(qls->_dataLabel, qls->_dataSet, (const string*) &(qls->_name)); 
    qls->_pDhsData = UFDHSwrap::newFrame(qls->_dataLabel, qls->_dataSet, qls->_dataFrame, 0, 1, &_QlookNames[i]);
    if( _verbose )
      clog<<"UFDHSwrap> allocated qlstrm: "<<qls->_name<<", with dataLabel: "<<qls->_dataLabel<<endl;
  }
 
  return _status;
}

UFDHSTReCS::~UFDHSTReCS() {} // don't really care about de-alloc of streams here

UFDHSTReCS::ObsDataQue::ObsDataQue(UFDHSTReCS* dhs,
				   UFObsConfig* obscfg,
				   UFFrameConfig* frmcfg) :_dhs(dhs), _obscfg(obscfg), _frmcfg(frmcfg) {
  ::pthread_mutex_init(&_quemutex, 0);
}

UFDHSTReCS::ObsDataQue::~ObsDataQue() { 
  delete _obscfg;
  for( size_t i= 0; i < size(); ++i ) {
    delete (*this)[i];
  }
  clear();
}

//int UFDHSTReCS::ObsDataQue::push(UFInts* data) {
int UFDHSTReCS::ObsDataQue::push(UFProtocol* data) {
  //clog<<"UFDHSTReCS::ObsDataQue::push> waiting on mutex lock to queue new image... "<<endl;
  //int stat = ::pthread_mutex_trylock( &_quemutex );
  int stat = ::pthread_mutex_lock( &_quemutex );
  if( stat == 0 ) { // succeeded
    push_back(data);
    stat = (int) size();
    if( _verbose )
      clog<<"UFDHSTReCS::ObsDataQue::push> queued new data, qlength: "<<stat<<endl;
    ::pthread_mutex_unlock( &_quemutex );
  }
  else{
    clog<<"UFDHSTReCS::ObsDataQue::push> (trylock) failed to queue new data."<<endl;
    return -1;
  }

  return stat;
}

//UFInts* UFDHSTReCS::ObsDataQue::pop(int& length) {
UFProtocol* UFDHSTReCS::ObsDataQue::pop(int& length) {
  length= -1;
  UFProtocol* data= 0;
  //if( _verbose )
  //  clog<<"UFDHSTReCS::ObsDataQue::pop> waiting on mutex lock to de-queue image... "<<endl;
  bool done= false, dots= true;
  float slp= 0.5;
  int cnt = (int)::ceil(_timeout / slp), cnttry = cnt;
  do {
    int stat = ::pthread_mutex_lock( &_quemutex );
    if( stat == 0 ) { // succeeded
      if( size() <= 0 ) {
        //if( _verbose )
          //clog<<"UFDHSTReCS::ObsDataQue::pop> dataque empty, telescope nodding and/or MCE4 paused..."<<endl;
        if( dots ) clog<<" . ";
        ::pthread_mutex_unlock( &_quemutex );
        UFPosixRuntime::sleep(0.5); // want to emulate blocking when dataque is empty
        continue;
      }
      else if( dots ) 
	clog<<endl;
    }
    else {
      clog<<"UFDHSTReCS::ObsDataQue::pop> (mutex unlock) failed to de-queue frame."<<endl;
      return 0;
    }
    data = (*this)[0]; // if final frame has been processed this will be null
    pop_front();
    length = (int) size();
    if( data != 0 && _verbose )
      clog<<"UFDHSTReCS::ObsDataQue::pop> de-queued frame: "<<data->name()
          <<"seq cnt: "<<data->seqCnt()<<", seqTot: "<<data->seqTot()<<", qlength: "<<length<<endl;
    else if( _verbose )
      clog<<"UFDHSTReCS::ObsDataQue::pop> de-queued null frame, qlength (should be 0): "<<length<<endl;

    ::pthread_mutex_unlock( &_quemutex );
    done = true;
  } while( --cnttry >= 0 && !done );

  if( cnttry < 0 && !done )
    clog<<"UFDHSTReCS::ObsDataQue::pop> no data after "<<slp*cnt<<" sec..."<<endl;

  return data;
}

UFDHSTReCS::ArchvPutData::ArchvPutData(UFDHSTReCS* dhs, const vector< UFInts* >& datavec,
				       const string& label, DHS_BD_DATASET dataset, int* pDhsData, int imgidx, bool final) :
  _dhs(dhs), _label(label), _dataset(dataset), _pDhsData(pDhsData), _imgidx(imgidx), _final(final) {
  for( size_t i = 0; i < datavec.size(); ++i )
    push_back(datavec[i]);
}

UFDHSTReCS::ArchvPutData::~ArchvPutData() {
  // and free all the data
  size_t nim = size();
  //clog<<"~ArchvPutData()> free datablock & dataset for datalabel; "<<_label<<", imgagecnt: "<<nim<<endl;

  _dhs->freeDataset(_dataset); // dataset gets freed after put of extension frame block
  UFInts *img= 0, *previmg= 0;
  for( size_t i = 0; i < nim; ++i ) {
    previmg = img;
    img = (*this)[i];
    if( img != previmg ) // ensure that duplicated pointers get deleted only once
      delete img;
  }
};


DHS_STATUS UFDHSTReCS::setAttributes(const UFStrings& fitsHdr, DHS_AV_LIST& av, int hdrtyp) {
  vector< const string* > fitsvec;
  int cnt = fitsHdr.elements();
  //clog<<"UFDHSTReCS::setAttributes> cnt: "<<cnt<<endl;
  for( int i = 0; i < cnt; ++i ) {
    const string* card =  fitsHdr.stringAt(i);
    fitsvec.push_back( card );
    //clog<<"UFDHSTReCS::setAttributes> card: "<<*card<<endl;
  }
  return UFDHSwrap::setAttributes( fitsvec, av, hdrtyp );
} // setAttributes


DHS_STATUS UFDHSTReCS::finalDataset(const UFStrings& fitsHdr, string& dataLabel,
				   DHS_BD_DATASET& dataSet, const string* quicklook) {
  _status = UFDHSwrap::newDataset(dataLabel, dataSet, quicklook);
  if( !quicklook ) {
    setAttributes(fitsHdr, dataSet, UFDHSwrap::All ); // set all attrib.
    //setAttributes(fitsHdr, dataSet, UFDHSwrap::Final); // set final attrib.
  }
  return _status;
} // finalDataset with final attributes

DHS_STATUS UFDHSTReCS::newDataset(const UFStrings& fitsHdr, string& dataLabel,
				  DHS_BD_DATASET& dataSet, const string* quicklook) {
  _status = DHS_S_SUCCESS;

  UFDHSwrap::newDataset(dataLabel, dataSet, quicklook);
  if( !quicklook ) {
    setAttributes(fitsHdr, dataSet, UFDHSwrap::Initial);
  }
  return _status;
} // newDataset with initial attributes

// create new frame for new dataset, setting all initial and/or final value attributes,
// index & total are compared to decide which attributes to set; this seems
// to be the only way it works (the server seems to like setting the instrument
// attributes only after at least one frame is allocated into the dataset?). 
int* UFDHSTReCS::newFrame(const UFStrings& fitsPrmHdr, const UFStrings& fitsExtHdr, string& dataLabel, 
	                  DHS_BD_DATASET& dataSet, DHS_BD_FRAME& dataFrame,
			  int index, int total, const string* quicklook) {
  vector< const string* > prmfitsvec;
  int cnt = fitsPrmHdr.elements();
  for( int i = 0; i < cnt; ++i )
    prmfitsvec.push_back( fitsPrmHdr.stringAt(i) );

  vector< const string* > extfitsvec;
  cnt = fitsExtHdr.elements();
  for( int i = 0; i < cnt; ++i )
    extfitsvec.push_back( fitsExtHdr.stringAt(i) );

  map< string, int > extradims; // none
  return UFDHSwrap::newFrame(prmfitsvec, extfitsvec, dataLabel, dataSet, dataFrame,
                             extradims, index, total, quicklook);
}

// for higher dimensional frames:
int* UFDHSTReCS::newFrame(const UFStrings& fitsPrmHdr, const UFStrings& fitsExtHdr, string& dataLabel, 
			  DHS_BD_DATASET& dataSet, DHS_BD_FRAME& dataFrame,
			  int chops, // naxis3 == chops
			  int savesets, // naxis4 == savesets
			  int index, int total,
			  const string* quicklook) {
  vector< const string* > prmfitsvec;
  int cnt = fitsPrmHdr.elements();
  for( int i = 0; i < cnt; ++i )
    prmfitsvec.push_back( fitsPrmHdr.stringAt(i) );

  vector< const string* > extfitsvec;
  cnt = fitsExtHdr.elements();
  for( int i = 0; i < cnt; ++i )
    extfitsvec.push_back( fitsExtHdr.stringAt(i) );

  //map< string, int > extradims; extradims.insert(pair<string, int>("chops*savesets", chops*savesets)); 
  map< string, int > extradims; extradims.insert(pair<string, int>("ChopPositions", chops)); extradims.insert(pair<string, int>("Savesets", savesets)); 

  return UFDHSwrap::newFrame(prmfitsvec, extfitsvec, dataLabel, dataSet, dataFrame,
                             extradims, index, total, quicklook);
}


// create new frame for new dataset, setting all initial and/or final value attributes,
// index & total are compared to decide which attributes to set; this seems
// to be the only way it works (the server seems to like setting the instrument
// attributes only after at least one frame is allocated into the dataset?). 
int* UFDHSTReCS::newFrame(const UFStrings& fitsExtHdr, string& dataLabel, 
	                  DHS_BD_DATASET& dataSet, DHS_BD_FRAME& dataFrame,
			  int index, int total, const string* quicklook) {
  vector< const string* > fitsvec;
  int cnt = fitsExtHdr.elements();
  for( int i = 0; i < cnt; ++i )
    fitsvec.push_back( fitsExtHdr.stringAt(i) );

  map< string, int > extradims; // none
  return UFDHSwrap::newFrame(fitsvec, dataLabel, dataSet, dataFrame, extradims, index, total, quicklook);
}

// for higher dimensional frames:
int* UFDHSTReCS::newFrame(const UFStrings& fitsExtHdr, string& dataLabel, 
			  DHS_BD_DATASET& dataSet, DHS_BD_FRAME& dataFrame,
			  int chops, // naxis3 == chops
			  int savesets, // naxis4 == savesets
			  int index, int total,
			  const string* quicklook) {
  vector< const string* > fitsvec;
  int cnt = fitsExtHdr.elements();
  for( int i = 0; i < cnt; ++i )
    fitsvec.push_back( fitsExtHdr.stringAt(i) );

  map< string, int > extradims; extradims.insert(pair<string, int>("ChopPositions", chops)); extradims.insert(pair<string, int>("Savesets", savesets)); 

  return UFDHSwrap::newFrame(fitsvec, dataLabel, dataSet, dataFrame, extradims, index, total, quicklook);
}

// for higher dimensional frames, no extension header:
int* UFDHSTReCS::newFrame(string& dataLabel, 
			  DHS_BD_DATASET& dataSet, DHS_BD_FRAME& dataFrame,
			  int chops, // naxis3 == chops
			  int savesets, // naxis4 == savesets
			  int index, int total,
			  const string* quicklook) {

  map< string, int > extradims; extradims.insert(pair<string, int>("ChopPositions", chops)); extradims.insert(pair<string, int>("Savesets", savesets)); 

  return UFDHSwrap::newFrame(dataLabel, dataSet, dataFrame, extradims, index, total, quicklook);
}

// set pDhsData with data values and send to dhs
DHS_STATUS UFDHSTReCS::putArchiveFrame(const UFInts& data, const string& dataLabel,
                	               DHS_BD_DATASET& dataSet, int* pDhsData,
			               int idx, bool final) {
  return UFDHSwrap::putArchiveFrame((int*)data.valData(), data.numVals(), dataLabel, dataSet, pDhsData, idx, final);
} // putArchiveFrame

// set pDhsData with data vec. values and send to dhs
DHS_STATUS UFDHSTReCS::putArchiveFrame(const vector< UFInts* >& data, const string& dataLabel,
                	               DHS_BD_DATASET& dataSet, int* pDhsData,
			               int idx, bool final) {
  vector< int* > datavec; int elem=0;
  for( size_t i = 0; i < data.size(); ++i ) {
    UFInts* ufi = data[i]; elem = ufi->elements();
    if( ufi && ufi->valData() )
      datavec.push_back((int*) ufi->valData() );
  }
  return UFDHSwrap::putArchiveFrame(datavec, elem, dataLabel, dataSet, pDhsData, idx, final);
} // putArchiveFrame

// set pDhsData with data values and send to dhs
DHS_STATUS UFDHSTReCS::putQlookFrame(const UFInts& data, const string& dataLabel,
				     DHS_BD_DATASET& dataSet, int* pDhsData,
            		             int idx, bool final) {
  int npix = width() * height();
  int* idata = (int*) data.valData(); int elem = data.numVals();
  if( idata == 0 || elem <= 0) {
    clog<<"UFDHSTReCS::putQlookFrame> null data pointer?, elem: "<<elem<<"buffnam: "<<data.name()<<endl;
    return DHS_E_NULLVALUE;
  }
  if( elem > npix ) {
    clog<<"UFDHSTReCS::putQlookFrame> dubious elem: "<<elem<<"buffnam: "<<data.name()<<endl;
    return DHS_E_NULLVALUE;
  }
  return UFDHSwrap::putQlookFrame(idata, elem, dataLabel, dataSet, pDhsData, idx, final);
} // putQlookFrame

// decode header provided as UFStrings
int UFDHSTReCS::decodeFITS(const UFStrings& hdr,
			  map< string, string >& svals,
			  map< string, int >& ivals,
			  map< string, float >& fvals,
			  map< string, string >& comments) {
  int elem = hdr.elements();
  for( int i= 0; i < elem; ++i ) 
    UFDHSwrap::decodeFITS(hdr[i], svals, ivals, fvals, comments); // default here should be to append

  return (int)(svals.size()+ivals.size()+fvals.size());
} // decode FITS from UFStrings

// decode (OSCIR type) initial header provided as a UFStrings
int UFDHSTReCS::decodeInitialFITS(const UFStrings& hdr,
				 map< string, string >& svals,
				 map< string, int >& ivals,
				 map< string, float >& fvals,
				 map< string, string>& comments)  {
  vector< const string* > fitsvec;
  int cnt = hdr.elements();
  for( int i = 0; i < cnt; ++i )
    fitsvec.push_back( hdr.stringAt(i) );

  return UFDHSwrap::decodeInitialFITS(fitsvec, svals, ivals, fvals, comments);
} // decode (OSCIR type) primary header initial attributes from UFStrings

// decode (OSCIR type) final header provided as a UFStrings
int UFDHSTReCS::decodeFinalFITS(const UFStrings& hdr,
			       map< string, string >& svals,
			       map< string, int >& ivals,
			       map< string, float >& fvals,
			       map< string, string>& comments)  {
  vector< const string* > fitsvec;
  int cnt = hdr.elements();
  for( int i = 0; i < cnt; ++i )
    fitsvec.push_back( hdr.stringAt(i) );

  return UFDHSwrap::decodeFinalFITS(fitsvec, svals, ivals, fvals, comments);
} // decode (OSCIR type) primary header finaal attributes from UFStrings

DHS_STATUS UFDHSTReCS::putQlookFrame(const UFInts& data, const vector< int >& offsets,
			             const string& dataLabel,
				     DHS_BD_DATASET& dataset, int* pDhsData,
				     int idx, bool final) {
  // if there are multiple offsets assume they indicate a set of images 
  // (chopset of nodset) that are all intended for the same q-look stream
  return UFDHSwrap::putQlookFrame((const int*) data.valData(), data.numVals(), offsets,
				   dataLabel, dataset, pDhsData, idx, final); 
}


void UFDHSTReCS::putQlookStreams(UFDHSTReCS& dhs) {
  // connect to the frame server:
  UFFrameClient ufrm(UFDHSTReCS::_frmhost, 52000, dhs.width(), dhs.height());
  int fc = ufrm.connect(UFDHSTReCS::_frmhost, 52000);
  if( fc <= 0 )
    clog<<"UFDHSTReCS::putQlookStreams> unable to connect to Frame Server"<<endl;
  else 
    clog<<"UFDHSTReCS::putQlookStreams> connected to Frame Server"<<endl;

  DHS_BD_DATASET dataSet;
  DHS_BD_FRAME dataFrame;
  map< string, string > dataLabels; // separate dataset labels for each qlstream
  map< string, int > idx; // count the frames sent to each stream
  int nstrm = _QlookNames.size();
  string key;
  for( int i = 0; i < nstrm; ++i ) {
    key = _QlookNames[i];
    dataLabels[key] = ""; // force datalabel initialization
    idx[key] = 0; // keep track of frmcnt for each qlstream
  }
  // each quicklook stream should reuse the same frame-data-buffer pDhsData
  boolean done= false;
  int frmcnt= 0, frmtotal= 0;
  vector< string > qlbufnames;
  while( !done ) {
    int bufcnt = ufrm.getUpdatedQlNames("trecs", qlbufnames, frmcnt, frmtotal);
    for( int i = 0; i < bufcnt; ++i ) {
      key = qlbufnames[i];
      //string datalabel = dataLabels[key];
      string datalabel; // force generation of new datalabel for each qlstream
      UFDHSwrap::newDataset(datalabel, dataSet, &key); 
      dataLabels[key] = datalabel;
      int* pDhsData = UFDHSwrap::newFrame(datalabel, dataSet, dataFrame, frmcnt, frmtotal, &key); 
      if( pDhsData == 0 ) {
        clog<<"UFDHSTReCS::putQlookStreams> failed to get frame allocation from dhs runtime."<<endl;
      }
      else {
        UFInts* data= 0;
        // return the the requested buffer frame and (?) current frame count:
        data = ufrm.fetchFrame(key, frmcnt, frmtotal);
        if( data == 0 ) {
          clog<<"ufdhsput> failed to get image data from frame service."<<endl;
          //break;
        }
        else {
	  int qlidx = idx[key];
	  dhs.putQlookFrame(*data, datalabel, dataSet, pDhsData, qlidx);
          idx[key] = ++qlidx;
          delete data;
        }
      }
    } // for bufcnt
    //run forever...
    if( frmcnt >= frmtotal ) {
      done = true;
    }
    else {
      UFPosixRuntime::sleep(0.1);
      //UFPosixRuntime::yield();
    }
  } // while !done
  return;
} // putQlookStreams

// allow this to be run in its own thread:
void* UFDHSTReCS::_putQlookStreams(void* p) {
  if( p == 0 )
    return p;

  UFDHSTReCS* dhs = static_cast< UFDHSTReCS* > (p);
  dhs->putQlookStreams(*dhs);

  return p;
}

void UFDHSTReCS::putArchiveStream(UFDHSTReCS& dhs) {
  // connect to the frame server:
  int w =  dhs.width(), h = dhs.height(); 
  UFFrameClient ufrm(UFDHSTReCS::_frmhost, 52000, w, h);
  int fc = ufrm.connect(UFDHSTReCS::_frmhost, 52000);
  if( fc <= 0 )
    clog<<"UFDHSTReCS::putArchiveStream> unable to connect to Frame Server"<<endl;
  else 
    clog<<"UFDHSTReCS::putArchiveStream> connected to Frame Server"<<endl;

  int frmcnt= 0, frmtotal= 0;
  bool done= false;
  while( !done ) {
    string readyfile, filename, prevfile;
    bool obsdone= false;
    while( !obsdone ) {
      obsdone = ufrm.getFileStoreName(readyfile, filename, prevfile, frmcnt, frmtotal);
      if( !obsdone )
        ::sleep(10);
      //clog<<"UFDHSTReCS::putArchiveStream> observation frame("<<frmcnt<<" of" <<frmtotal<<endl;
    }
    //clog<<"UFDHSTReCS::putArchiveStream> observation frame buffered to: "<<readyfile<<endl;
    
    // archive should use N. Hill's algorithm, re-use the dataLabel,
    // alloc. and free dataSets:
    DHS_BD_DATASET dataSet;
    DHS_BD_FRAME dataFrame;
    string dataLabel = UFDHSwrap::_archvDataLabel;
    int* pDhsData = 0; 
    UFStrings* fitsHdr= 0;
    int fd = ufrm.openFITS(readyfile, fitsHdr, w, h, frmtotal);
    if( w != dhs.width() || h != dhs.height() ) 
      clog<<"UFDHSTReCS::putArchiveStream> frame dimensions mismatch: w= "<<w<<"< h= "<<h<<endl;
      
    for( int idx= 0; idx < frmtotal-1; ++idx ) {
      // get next frame
      UFInts* data = ufrm.seekFITSData(fd);
      if( data == 0 ) {
        clog<<"UFDHSTReCS::putArchiveStream> failed to get image data from frame service."<<endl;
	// done = true; // run forever...
        break;
      }
      // dhs accepts attributes only after first frame is put?
      /*
      if( idx == 0 ) { // first frame, set initial fits hdr attributes, dataLabel gets set != ""
        dhs.newDataset(*fitsHdr, dataLabel, dataSet);
        pDhsData = dhs.newFrame(*fitsHdr, dataLabel, dataSet, dataFrame, idx, frmcnt); 
        dhs.putArchiveFrame(*data, dataLabel, dataSet, pDhsData, idx);
      }
      */
      if( idx == frmcnt - 1 ) { // final frame, get final fits values
        pDhsData = dhs.newFrame(*fitsHdr, dataLabel, dataSet, dataFrame, idx, frmcnt); 
        dhs.putArchiveFrame(*data, dataLabel, dataSet, pDhsData, idx, true); // finalframe == true
      }
      else { // intermediate frame, only extension fitsHdr info required.
        pDhsData = UFDHSwrap::newFrame(dataLabel, dataSet, dataFrame, idx, frmcnt); 
        dhs.putArchiveFrame(*data, dataLabel, dataSet, pDhsData, idx);
      }
      delete data;
      dhs.freeDataset(dataSet); // frees frame; but re-use dataLabel
    } // end frame for loop
    ::close(fd); // close the local store file
  } // end while 
  return;
} // putArchiveStream

// allow this to be run in its own thread:
void* UFDHSTReCS::_putArchiveStream(void* p) {
  if( p == 0 )
    return p;

  UFDHSTReCS* dhs = static_cast< UFDHSTReCS* > (p);
  dhs->putArchiveStream(*dhs);

  return p;
}

// allow putAllStreams to be run in its own thread:
void* UFDHSTReCS::_putAllStreams(void* p) {
  if( p == 0 )
    return p;

  UFDHSTReCS::ObsDataQue* dataque = static_cast< UFDHSTReCS::ObsDataQue* > (p);
  if( _verbose )
    clog<<"UFDHSTReCS::_putAllStreams> started new thread: "<<pthread_self()
        <<", datalabel: "<<dataque->datalabel()<<endl;
  UFDHSTReCS& dhs = dataque->getDHS();
  dhs.putAllStreams(*dataque);

  if( _verbose )
    clog<<"UFDHSTReCS::_putAllStreams> thread exiting: "<<pthread_self()
        <<", for datalabel: "<<dataque->datalabel()<<endl;

  delete dataque; p = 0;

  return p;
}

void UFDHSTReCS::putAllStreams(UFDHSTReCS::ObsDataQue& dataque) {
  /*
  if( ::pthread_mutex_lock(&_mutex) < 0 ) {
    if( errno == EINVAL ) {
      ::pthread_mutex_init(&_mutex, 0);
      if( ::pthread_mutex_lock(&_mutex) < 0 ) {
        clog<<"UFDHSTReCS::putAllStreams> failed to lock mutex..."<<endl;
        return;
      }
    }
  } 
  */
  // presumably a new thread has been created for each observations (new archive dhs datalabel)
  UFObsConfig& obscfg = dataque.getObs();
  int chops = obscfg.chopBeams();
  int nods = obscfg.nodBeams();
  int savesets = obscfg.saveSets();
  int nodsets = obscfg.nodSets();
  int imgcnt = chops * savesets; // per dhs frame 'extension'
  int extcnt = nods * nodsets;
  int finalcnt = imgcnt * extcnt;
  string datalabel = obscfg.datalabel();
  UFDHSwrap::_archvDataLabel = datalabel;
  clog<<"UFDHSTReCS::putAllStreams> datalabel: \""<<datalabel<<", chops= "<<chops<<", nods= "<<nods
      <<", savesets= "<<savesets<<", nodsets= "<<nodsets<<", imgcnt/ext= "<<imgcnt
      <<", extcnt= "<<extcnt<<", finalcnt= "<<finalcnt<<endl;
  if( finalcnt != obscfg.totFrameCnt() ) { 
    clog<<"UFDHSTReCS::putAllStreams> mismatch in total/final image cnts: "<<obscfg.totFrameCnt()
        <<"/"<<finalcnt<<endl;
  }
  DHS_BD_DATASET archvdataset;
  // UFStrings* fits0Hdr =_ufits->fetchAllFITS(_connections, _observatory);
  UFStrings* fits0Hdr = 0; // don's set dataset header here, wait 'til after first frame allocation...
  if( fits0Hdr != 0 ) {
    clog<<"UFDHSTReCS::putAllStreams> allocating new DataSet with initial TReCS FITS elem: "<<fits0Hdr->elements()<<" DataLabel: "<< datalabel<<endl;
    // set primary header observation here (ala OCS):
    newDataset(*fits0Hdr, datalabel, archvdataset); // set primary header observation here
    delete fits0Hdr;
  }
  else {
    clog<<"UFDHSTReCS::putAllStreams> allocating new DataSet without TReCS FITS."<<endl;
    UFDHSwrap::newDataset(datalabel, archvdataset); // do not set primary header observation here
  }
  obscfg.relabel(datalabel); // if datalabel was not set before newDataset call, it is now, keep it in sync.
  UFDHSwrap::_archvDataLabel = datalabel;
  // always create a new archv stream:
  UFDHSwrap::Stream* archv = new UFDHSwrap::Stream(datalabel, archvdataset); // archvdataset used for first extension
  clog<<"UFDHSTReCS::putAllStreams> initialized new Archive DataSet with datalabel: \""<<datalabel<<"\""<<endl;

  //::pthread_mutex_unlock(&_mutex);

  int datacnt = putObservation( *archv, dataque );

  UFDHSwrap::freeStream(archv);
  clog<<"UFDHSTReCS::putAllStreams> Observation completed, total image cnt: "<<datacnt<<", datalabel: "<<datalabel<<endl;
} // putAllStreams

int UFDHSTReCS::putObservation( UFDHSwrap::Stream& archv, ObsDataQue& dataque ) {
  /*
  if( ::pthread_mutex_lock(&_mutex) < 0 ) {
    clog<<"UFDHSTReCS::putObservation> failed to lock mutex..."<<endl;
    return 0;
  }
  */
  int frmcnt= 0, imgIdx= 0;
  UFDHSwrap::_archvFrmCnt = 0;
  UFObsConfig& obscfg = dataque.getObs();
  UFFrameConfig* frmcfg = dataque.getFrmConf();
  int chops = obscfg.chopBeams();
  int savesets = obscfg.saveSets();
  int imgcnt = chops * savesets;
  int finalcnt = obscfg.totFrameCnt();
  string qnam;
  bool abort= false, stop= false;
  int qlength= 0;
  string utstart, utend;
  double airmasstart = 1.0, airmassend = 1.0;
  UFStrings *fitsHdr= 0, *fits0Hdr=0;
  vector< UFInts* > datavec; // archv nod/frame extenison buf
  map < string, UFInts* > bufs; // q-look bufs;
  UFInts* nullImg = new UFInts(archv._dataLabel, _width*_height, false);
  bool obsdiscard = false;
  bool frmdiscard = false;
  bool freeFrm= false;  // if archiving, do not free image data here, freed by archiving thread
  UFProtocol* data = 0;

  //::pthread_mutex_unlock(&_mutex);
  string obsname = obscfg.name();
  if( obsname.find("discard") != string::npos || obsname.find("Discard") != string::npos || 
      obsname.find("DISCARD") != string::npos )
    obsdiscard = true; // do not archive this obs, but perhaps use qlook...

  clog<<"UFDHSTReCS::putObservation> expecting total image count: "<<finalcnt<<endl;

  while( imgIdx < finalcnt && !abort && !stop ) {
    frmdiscard = false;
    /*
    if( ::pthread_mutex_lock(&_mutex) < 0 ) {
      clog<<"UFDHSTReCS::putObservation> failed to lock mutex..."<<endl;
      return 0;
    }
    */
    // deal with the image at the front of the que:
    //clog<<"UFDHSTReCS::putObservation> fetch next frame from dataque..."<<endl;
    data = dataque.pop(qlength); // this should block until new data is available
    // before breaking out of the put loop, send final frame?
    if( data != 0 ) {
      string datname = data->name();
      if( datname.find("no-dhs") != string::npos || datname.find("No-DHS") != string::npos || 
          datname.find("NO-DHS") != string::npos )
        frmdiscard = true; // don't qlook this frame (actually frm.acq.server will not send in this case)

      if( data->elements() <= 0 || !data->isData() ) { // explicit stop or abort occured
        string notice = data->name();
        if( notice.find("stop") != string::npos ) {
          stop = true; data = nullImg; // send a null image to DHS?
          clog<<"UFDHSTReCS::putObservation> Unexpected termination of observation: "<<notice<<endl;
        }
        if( notice.find("abort") != string::npos ) {
          abort = true; data = nullImg; // send a null image to DHS?
          clog<<"UFDHSTReCS::putObservation> Unexpected termination of observation: "<<notice<<endl;
        }
      }

      if( data->isStrings() ) { // Assume FITS Header
        clog<<"UFDHSTReCS::putObservation> FITS header: "<<data->name()<<endl;
        delete fits0Hdr;
        fits0Hdr = dynamic_cast< UFStrings* > ( data );
        if( fits0Hdr == 0 )
	  clog<<"UFDHSTReCS::putObservation> bad FITS header popped off dataqueue..."<<endl;
	continue; // proceed with the next 'pop' off the dataqueue
      }
      if( !data->isData() || data->elements() <= 0 ) {
        clog<<"UFDHSTReCS::putObservation> unexpected: "<<data->name()<<endl;
      }
      else {
        frmcnt++;
        clog<<"UFDHSTReCS::putObservation> fetched data frame: "<<frmcnt<<", qlength: "<<qlength<<endl;
      }
    }
    else if( imgIdx < finalcnt ) { // null data in queue means...
      stop = true; data = nullImg;
      clog<<"UFDHSTReCS::putObservation> Unexpected termination of observation, assume STOP ..."<<endl;
    }
    else if( imgIdx >= finalcnt ) {
      // deal with final values for primary header?
      if( _verbose )  clog<<"UFDHSTReCS::putObservation> Completion of observation..."<<endl;
      //::pthread_mutex_unlock(&_mutex);
      break;
    }

    // if observation was aborted don't bother with q-look or archive:
    if( abort ) {
      UFDHSwrap::abort(archv._dataLabel);
      continue;
    }

    // send to all relevant q-look streams & (optionally) archv...
    // use obsconfig convenience func. to evaluate all relevant q-look buffs
    // and send them off to qlook streams:
    // note that above logic may result in nullImg being sent to q-look:
    //if( _qlook && data->isData() && data->elements() > 0 ) { 
    if( _qlook && !frmdiscard && data->isData() && data->elements() > 0 ) { 
      UFInts* imgdata= 0;
      UFInts* idata = dynamic_cast< UFInts* > ( data );
      int nq = obscfg.evalTReCS(imgIdx, idata, bufs, frmcfg, freeFrm);
      if( _verbose ) 
        clog<<"UFDHSTReCS::putObservation> imgIdx: "<<imgIdx<<", new qlook bufs: "<<nq<<endl;
      map < string, UFInts* >::iterator it = bufs.begin();
      do {
        qnam = it->first; // note this lacks "trecs:" prefix...
        imgdata = it->second;
        if( imgdata == 0 ) {
          clog<<"UFDHSTReCS::putObservation> Null image data buff for dhs stream: "<<qnam<<endl;
          continue;
        }
        else if( imgdata->valData() == 0 || imgdata->numVals() == 0 ) {
          clog<<"UFDHSTReCS::putObservation> No pixels in image data for dhs stream: "<<qnam<<endl;
          continue;
        }
        UFDHSwrap::Stream* qls = _QlookStrms[qnam];
        if( qls == 0 ) {
          clog<<"UFDHSTReCS::putObservation> No dhs stream for: "<<qnam<<endl;
          continue;
        }
        if( _verbose )
          clog<<"UFDHSTReCS::putObservation> putting qlook buf name: "<<qnam<<" "<<imgdata->name()<<" "<<imgdata->timeStamp()<<endl;
        // if( _fitsHdr) dhs.setAttributes(*fitsHdr, qls->_dataSet, UFDHSwrap::Extension);
        //putQlookFrame(*imgdata, qls->_dataLabel, qls->_dataSet, qls->_pDhsData, idx);
        putQlookFrame(*imgdata, qls->_dataLabel, qls->_dataSet, qls->_pDhsData, 0); // always idx=0 ?
        //putQlookFrame(*imgdata, qls->_dataLabel, qls->_dataSet, qls->_pDhsData, 0, true); // always final?
      } while( ++it != bufs.end() ); // qlook streams
    } // if isData()

    if( frmdiscard ) {
      clog<<"UFDHSTReCS::putObservation> data frame marked DISCARD, bypassed qlook of this frame: "
          <<data->name()<<" seq. cnt: "<<data->seqCnt()<<" of: "<<data->seqTot()<<endl;
    }
    if( obsdiscard ) {
	clog<<"UFDHSTReCS::putObservation> observation marked DISCARD, will not archive this frame: "
            <<data->name()<<" seq. cnt: "<<data->seqCnt()<<" of: "<<data->seqTot()<<endl;
      delete data; data = 0; // memory leak?
      continue; // not archiving this observation or frame 
    }
    // if we get here, then neither the image frame nor the observation is marked for discard
    // now archive logic: insert image into nod/(fits-ext) datavec, if it's the last image
    // of the savesets for this nod, send resulting 'extension frame' to dhs archive via new thread...
    if( datavec.empty() ) { // this is the first image of the savesets for this nod
      utstart = data->timeStamp();
      airmasstart = _getAirmass(); // should be caget from TCS valid values: 1.0 and up
      clog<<"UFDHSTReCS::putObservation> utstart: "<<utstart<<", airmass: "<<airmasstart<<endl;
    }
    // insert data image into buffer for saveset:
    //if( _verbose )
    //clog<<"UFDHSTReCS::putObservation> inserted idx: "<<imgIdx<<" into saveset, need "<<imgcnt<<" to complete it"<<endl;
    UFInts* idata = dynamic_cast< UFInts* > ( data );
    if( idata != 0 )
      datavec.push_back(idata);
    else
      clog<<"UFDHSTReCS::putObservation> data not an image (ufints)?"<<endl;
    if( stop || datavec.size() == (size_t) imgcnt && idata != 0 ) { // final image of extension (saveset for nod)
      while( stop && datavec.size() < (size_t) imgcnt ) { // is obs. stopped, this should be a null image
      // replicate this last (null) image to complete the extension
        datavec.push_back(idata);
        //if( _verbose )
          clog<<"UFDHSTReCS::putObservation> stopped, inserted filler: "<<datavec.size()
	      <<" into saveset, need "<<imgcnt<<" to complete extension..."<<endl;
      }
      int* pDhsData = 0; // saveset dhs 'frame' buffer
      utend = data->timeStamp();
      airmassend = _getAirmass(); // should be caget from TCS valid values: 1.0 and up
      clog<<"UFDHSTReCS::putObservation> saveset complete, utend: "<<utend<<", airmass: "<<airmassend<<endl;
      // presumably this is the last image of the savesetas, and telescope is nodding
      bool final = (imgIdx == finalcnt - 1) || stop;
      // get frame attributes (ext. header):
      fitsHdr = _extFITS(obscfg, imgIdx, utstart, utend, airmasstart, airmassend);
      if( fitsHdr != 0 ) { // extension header available
        // set ext header start values here with newFrame...
        pDhsData = newFrame(*fitsHdr, archv._dataLabel, archv._dataSet, archv._dataFrame,
			    chops, savesets, imgIdx, finalcnt); 
	delete fitsHdr; fitsHdr = 0; //free it
      }
      else { // no extension header?
        pDhsData = newFrame(archv._dataLabel, archv._dataSet, archv._dataFrame,
			    chops, savesets, imgIdx, finalcnt); 
      }
      // set dataset attributes after frame is allocated?
      // is this the first nod (dhs frame) of the dataset?
      if( imgIdx == 0 ) { // first frame -- set fits prim. hdr initial attributes, dataLabel gets set != ""
        if( fits0Hdr == 0 ) // never popped off dataque?
          fits0Hdr = _ufits->fetchAllFITS(_connections, _fitstimeout, _observatory);
        if( fits0Hdr != 0 ) {
          clog<<"UFDHSTReCS::putObservation> Set Initial FITS, elem: "<<fits0Hdr->elements()<<" DataLabel: "<<archv._dataLabel<<endl;
          setAttributes(*fits0Hdr, archv._dataSet, UFDHSwrap::Initial); // set for dataSet not dataFrame!
          delete fits0Hdr; fits0Hdr = 0; // free it
        }
        else {
          clog<<"UFDHSTReCS::putObservation> failed to apply initial FITS info. (never popped off queue & fetchAll failed)?"<<endl;
        }
      } // first frame
      if( final ) { // final frame, set final fits values for primary header
        if( fits0Hdr == 0 ) // never popped off dataque?
          fits0Hdr = _ufits->fetchAllFITS(_connections, _fitstimeout, _observatory);
        if( fits0Hdr ) {
          clog<<"UFDHSTReCS::putObservation> Set Final FITS, elem: "<<fits0Hdr->elements()<<" DataLabel: "<<archv._dataLabel<<endl;
          //setAttributes(*fits0Hdr, archv._dataSet, UFDHSwrap::Final); // set only final attrib. for dataSet 
          setAttributes(*fits0Hdr, archv._dataSet, UFDHSwrap::All); // or set all attrib. for dataSet
          delete fits0Hdr; fits0Hdr = 0; // free it
	}
	else {
          clog<<"UFDHSTReCS::putObservation> failed to apply final FITS info. (never popped off queue & fetchAll failed)?"<<endl;
	}
      } // final frame
      // this can take a while, and takes a fairly long while for the final frame
      // so this really should have it's own thread:
      // putArchiveFrame(datavec, archv._dataLabel, archv._dataSet, pDhsData, imgIdx, final); // finalframe == true/false
      // this makes a shallow copy of datavec, and deletes the contents after the put
      // thread should also free archv._dataSet
      // this copies content of datavec into DHS allocated pDhsData
      UFDHSTReCS::ArchvPutData* dataframe = new UFDHSTReCS::ArchvPutData(this, datavec, archv._dataLabel, archv._dataSet, pDhsData, imgIdx, final);
      // new thread will delete dataset & dataframe
      pthread_t archvthrd = UFPosixRuntime::newThread(UFDHSTReCS::_putArchvThread, (void*) dataframe);
      if( archvthrd ==  0 ) {
        clog<<"UFDHSTReCS::putObservation> unable to start new thread for archv frame put ..."<<endl;
	delete dataframe; // frees all data in datavec copy, frees dataset
	break;
      }
      //clog<<"UFDHSTReCS::putObservation> started new thread for dhs archive of saveset, thrdId: "<<archvthrd<<endl;
      UFPosixRuntime::sleep(0.1);
      datavec.clear();
      // allocate new dataset for existing label
      UFDHSwrap::newDataset(archv._dataLabel, archv._dataSet);
      UFDHSwrap::_archvFrmCnt++;
      //clog<<"UFDHSTReCS::putObservation> archv._dataLabel: "<<archv._dataLabel<<", archvFrmCnt: "<<UFDHSwrap::_archvFrmCnt<<endl;
    } // archive frame via datavec
    //::pthread_mutex_unlock(&_mutex);
    if( !stop )  // if stopped, the imgIdx has been incremented with fill frames...
      ++imgIdx;
  } // observation completed or aborted or stopped, end of for loop

  if( abort )
    clog<<"UFDHSTReCS::putObservation> Aborted datalabel: "<<archv._dataLabel<<", frmcnt: "<<frmcnt
        <<", qlength: "<<qlength<<endl;
  else if( stop )
    clog<<"UFDHSTReCS::putObservation> Stopped datalabel: "<<archv._dataLabel<<", frmcnt: "<<frmcnt
        <<", qlength: "<<qlength<<endl;
  else
    clog<<"UFDHSTReCS::putObservation> Completed datalabel: "<<archv._dataLabel<<", frmcnt: "<<frmcnt
        <<", qlength: "<<qlength<<endl;

  if( !abort && !stop ) delete nullImg; // did not use this

  if( freeFrm )
    delete data;
  else 
    archv._dataSet = DHS_BD_DATASET_NULL; // insure that freeStreams does not delete this, because putArchvThread() should...

  return frmcnt;
}

// allow putArchiveFrame to be run in its own thread:
void* UFDHSTReCS::_putArchvThread(void* p) {
  if( p == 0 )
    return p;

  UFDHSTReCS::ArchvPutData* dataframe = static_cast< UFDHSTReCS::ArchvPutData* > (p);
  string t0 = UFRuntime::currentTime(); t0 = t0.substr(0, 19);
  if( _verbose )
    clog<<"UFDHSTReCS::_putArchvThread> "<<t0<<" -- started new thread: "<<pthread_self()
        <<", datalabel: "<<dataframe->_label<<endl;

  UFDHSTReCS& dhs = dataframe->getDHS();

  bool mtx= true;
  int stat = ::pthread_mutex_lock( &_mutex );
  if( stat != 0 ) { // failed 
    mtx = false;
    clog<<"UFDHSTReCS::_putArchvThread> (lock) failed to increment thread cnt."<<endl;
  }
  else {
    dhs._archvThrdCnt++; // keep track of total number of (active) archive put threads
    ::pthread_mutex_unlock( &_mutex );
  }

  vector< UFInts* >* datavec = static_cast< vector< UFInts* >* > (dataframe);
  dhs.putArchiveFrame(*datavec, dataframe->_label, dataframe->_dataset, dataframe->_pDhsData, dataframe->_imgidx, dataframe->_final);

  string t = UFRuntime::currentTime(); t = t.substr(2, 16);
  //clog<<"UFDHSTReCS::_putArchvThread> thread exiting: "<<pthread_self()<<endl;
  //if( _verbose )
    clog<<t0<<" -- "<<t<<" -- completed DHS archive put for datalabel: "<<dataframe->_label<<endl;

  // UFDHSTReCS::ArchvPutData dtor should free all image buffs in extension block, and associated dataset
  delete dataframe; p = 0;

  if( mtx ) {
    stat = ::pthread_mutex_lock( &_mutex );
    if( stat == 0 ) { // succeeded
      dhs._archvThrdCnt--; // keep track of total number of archive put threads
      ::pthread_mutex_unlock( &_mutex );
    }
  }

  return p;
}

// convenience func. should be given an open dhs connection for datalabel
// and connects to agents for latest FITS attribute info.
int UFDHSTReCS::initAttributes(string& label, UFDHSTReCS& dhs, const string& agenthost) {
  UFFITSClient ufits(label);
  // connect to agents for tits transaction
  UFFITSClient::AgentLoc loc;
  int aidx = ufits.locateAgents(agenthost, loc);
  if( aidx <= 0 ) {
    clog<<"UFDHSConfig::initAttributes> no agents located @ "<<agenthost<<endl;
    return -1;
  }
  int ncon= 0;
  while( ncon <= 0 ) {
    ncon = ufits.connectAgents(loc, dhs._connections, -1, true);
    if( ncon <= 0 ) {
      clog<<"UFDHSConfig::initAttributes> no connections yet, sleep & retry..."<<endl;
      ufits.sleep(5.0);
    }
  }
  UFStrings* fits0Hdr = ufits.fetchAllFITS(dhs._connections, _fitstimeout, dhs._observatory);
  if( fits0Hdr == 0 ) {
    clog<<"UFDHSConfig::initAttributes> failed to get any FITS info from agents @ "<<agenthost<<endl;
    return -1;
  }
  // init the dataset's primary header
  DHS_BD_DATASET dataSet;
  DHS_STATUS stat = dhs.newDataset(*fits0Hdr, label, dataSet);
  dhs.freeDataset(dataSet);
  delete fits0Hdr;
  return (int)stat;
}

// this must use epics caget for airmass value... 
// format shoud be fits card, to be used by setAttributes...
UFStrings* UFDHSTReCS::_extFITS(UFObsConfig& obscfg, int imgIdx, string& utstart, string& utend, double airmasstart, double airmassend) {
  UFFITSheader ufits;
  int nod = obscfg.nodPosition(imgIdx);
  string nodphase = "A"; // nod pos. == 0
  if( nod == 1 )
    nodphase = "B";

  //int nodsets = obscfg.nodSets();
  int chops = obscfg.chopBeams();
  int savesets = obscfg.saveSets();
  int imgcnt = chops * savesets; // per nod 
  int nodset = 1 + imgIdx / imgcnt; // nod beams == 1
  if( obscfg.nodBeams() == 2 )
    nodset = 1 + imgIdx / (2*imgcnt);
  utstart = utstart.substr(9);
  utend = utend.substr(9);
  clog<<"UFDHSTReCS::_extFITS> nod: "<<nod<<", nodset: "<<nodset<<", utstart: "<<utstart<<", utend: "<<utend<<", amstart: "<<airmasstart<<endl;

  ufits.add("NOD", nodphase, "Nod Phase");
  ufits.add("NODSET", nodset, "Index of NodSet");
  ufits.add("UTSTART", utstart, "UT at extension start");
  ufits.add("UTEND", utend, "UT at extension end");
  ufits.add("AMSTART", airmasstart, "Airmass at start");

  UFStrings* ret = ufits.Strings();
  //if( _verbose )
    for( int i = 0; i < ret->elements(); ++i )
      clog<<"UFDHSTReCS::_extFITS> "<<(*ret)[i]<<endl;

  return ret;
}

double UFDHSTReCS::_getAirmass() {
  static int havecapipe = -1;
  const string epics = "tcs";
  const string airmassdb = "tcs:sad:airMassNow";

  if( havecapipe <= 0 )
    havecapipe = UFGemDeviceAgent::pipeToFromEpicsCAchild(epics);

  string airmass = "0.0";
  if( havecapipe > 0  )
    UFGemDeviceAgent::getEpics(epics, airmassdb, airmass);
  
  double val = 0.0;
  if( airmass.length() > 0 ) {
    const char* amc = airmass.c_str();
    if( isdigit(amc[0]) )
      val = atof(amc);
  }
  return val;
}

#endif // __UFDHSTReCS_cc__

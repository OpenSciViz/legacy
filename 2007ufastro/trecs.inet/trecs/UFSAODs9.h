#if !defined(__UFSAODs9_h__)
#define __UFSAODs9_h__ "$Name:  $ $Id: UFSAODs9.h,v 0.0 2002/06/03 17:42:29 hon Developmental $"
#define __UFSAODs9_H__(arg) const char arg##SAODs9_h__rcsId[] = __UFSAODs9_h__;

#include "stdio.h"
#include "unistd.h"
#include "stdlib.h"
#include "sys/types.h"
#include "iostream"
#include "strstream"

class UFSAODs9 {
public:
  inline UFSAODs9() {}
  inline ~UFSAODs9() { close(); }
  inline static int open(int frm= 1) {
    strstream s;
    s<<"/usr/local/bin/xpaset -p ds9 frame "<<frm<<ends;
    ::system(s.str()); delete s.str();
    _xpa = ::popen("/usr/local/bin/xpaset ds9 fits", "w");
    if( _xpa == 0 ) {
      clog<<"UFDs9::open> xpaset open failed"<<strerror(errno)<<endl;
      return 0;
    }
    else {
      clog<<"UFDs9::open> frame: "<<frm<<endl;
    }
    return (int)_xpa;
  }

  inline static int close() {
    if( _xpa != 0 ) {
      ::pclose( _xpa ); 
      _xpa = 0;
    }
    return (int)_xpa;
  }

  inline static int display(unsigned char* fits, int sz, int frm= 1) {
    clog<<"UFSAODs9::display> fits sz: "<<sz<<endl;
    UFRuntime::bell();
    if( _xpa == 0 ) open(frm);
    if( _xpa == 0 ) return 0;
    int nb= 0, nbsend = sz;
    while( nbsend > 0 ) {
      nb += ::write(fileno(_xpa), (fits+nb), nbsend);
      nbsend = sz - nb;
    }
    fflush(_xpa);
    // evidently we can only write one fits-file buffer at a time
    // close & re-open the pipe to xpaset:
    close();
    return nb;
  } 

protected:
  static FILE* _xpa;
};

#if defined(__UFMAIN__)
  FILE* UFSAODs9::_xpa= 0;
#endif

#endif // __UFSAODs9_h__

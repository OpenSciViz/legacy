#if !defined(__UFTReCSExecutiveAgent_cc__)
#define __UFTReCSExecutiveAgent_cc__ "$Name:  $ $Id: UFTReCSExecutiveAgent.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFTReCSExecutiveAgent_cc__;

#include "sys/types.h"
#include "sys/stat.h"
#include "fcntl.h"

#include "string"
#include "vector"

#include "UFTReCSExecutiveAgent.h"
#include "UFExecutiveConfig.h"

UFBaytech* UFTReCSExecutiveAgent::_baytech= 0;
bool UFTReCSExecutiveAgent::_boot= false; // bring up all the device agents?
bool UFTReCSExecutiveAgent::_powerOnAll= false;
bool UFTReCSExecutiveAgent::_powerOffAll= false;
int UFTReCSExecutiveAgent::_pulsePeriod= 60;

// static funcs:
void UFTReCSExecutiveAgent::sighandler(int sig) {
  if( sig == SIGCHLD ) { // presumably this is the _capipe child:
    clog<<"UFTReCSExecutiveAgent::sighandler> child-death, sig: "<<sig<<endl;
    //UFRndRobinServ::_childdeath = true;
    //::pclose(_capipe);
    //_capipe = 0;
    return;
  }
  else if( sig == SIGPIPE ) {
    clog<<"UFTReCSExecutiveAgent::sighandler> socket or pipe closed, sig: "<<sig<<endl;
    UFRndRobinServ::_lost_connection = true;
    return;
  }
  strstream s;
  s<<"/usr/bin/pkill -"<<sig<<ends;
  string sigcmd = s.str(); delete s.str();
  string chsigcmd;
  if( sig != SIGINT ) {
    clog<<"UFTReCSExecutiveAgent::sighandler> forward sig: "<<sig<<" to all agents."<<endl;
    // blindly forward all other signals to all device agents.
    string chsigcmd = sigcmd + " ufacqframed"; system(chsigcmd.c_str());
    chsigcmd = sigcmd + " ufgdhsd"; system(chsigcmd.c_str());
    chsigcmd = sigcmd + " ufgls218d"; system(chsigcmd.c_str());
    chsigcmd = sigcmd + " ufgls340d"; system(chsigcmd.c_str());
    chsigcmd = sigcmd + " ufg354vacd"; system(chsigcmd.c_str());
    chsigcmd = sigcmd + " ufgmotord"; system(chsigcmd.c_str());
    chsigcmd = sigcmd + " ufgmce4d"; system(chsigcmd.c_str());
  }
  if( sig == SIGTERM ) {
    clog<<"UFTReCSExecutiveAgent::sighandler> shutdown/terminate, sig: "<<sig<<endl;
    sigcmd = "/usr/bin/pkill -KILL"; // in case -TERM failed...
    chsigcmd = sigcmd + " ufgmce4d"; system(chsigcmd.c_str());
    chsigcmd = sigcmd + " ufgmotordd"; system(chsigcmd.c_str());
    chsigcmd = sigcmd + " ufg354vacd"; system(chsigcmd.c_str());
    chsigcmd = sigcmd + " ufgls340d"; system(chsigcmd.c_str());
    chsigcmd = sigcmd + " ufgls218d"; system(chsigcmd.c_str());
    chsigcmd = sigcmd + " ufgdhsd"; system(chsigcmd.c_str());
    chsigcmd = sigcmd + " ufacqframed"; system(chsigcmd.c_str());
    chsigcmd = sigcmd + " ufcaputarr"; system(chsigcmd.c_str());
    chsigcmd = sigcmd + " ufcaget"; system(chsigcmd.c_str());
    UFRndRobinServ::_shutdown = true;
    UFRndRobinServ::shutdown();
  }
  if( sig == SIGINT ) {
    clog<<"UFTReCSExecutiveAgent::sighandler> interrupt/terminate, sig: "<<sig<<endl;
    UFRndRobinServ::_shutdown = true;
    UFRndRobinServ::shutdown();
  }
  return UFRndRobinServ::sighandlerDefault(sig);
}

// ctors:
UFTReCSExecutiveAgent::UFTReCSExecutiveAgent(int argc,
					     char** argv,
					     char** envp) : UFGemDeviceAgent(argc, argv, envp) {
  // use ancillary function to retrieve LS340 readings
  _flush =  0.1; // 0.35; // inherited
  _Update = 1.0; // rather than 1.0 sec.
  bool *bp = new bool;
  *bp = false; // arg to ancillary func. indicates whatever
  UFRndRobinServ::_ancil = (void*) bp; // non-null indicate acillary func. should be invoked
  _confGenSub = _epics + ":eg:configG";
  _heart = _epics + ":eg:heartbeat";
  _statRec = _epics + ":eg:sysmonG.J"; // 
}

UFTReCSExecutiveAgent::UFTReCSExecutiveAgent(const string& name,
					     int argc,
					     char** argv,
					     char** envp) : UFGemDeviceAgent(name, argc, argv, envp) {
  // use ancillary function to retrieve LS340 readings
  _flush =  0.1; // 0.35; // inherited
  _Update = 1.0; // rather than 1.0 sec.
  bool *bp = new bool;
  *bp = false; // arg to ancillary func. indicates whatever
  UFRndRobinServ::_ancil = (void*) bp; // non-null indicate acillary func. should be invoked
  _confGenSub = _epics + ":eg:configG";
  _heart = _epics + ":eg:heartbeat";
  _statRec = _epics + ":eg:sysmonG.J"; // 
}

// static funcs:
int UFTReCSExecutiveAgent::main(int argc, char** argv, char** envp) {
  UFTReCSExecutiveAgent ufs("UFTReCSExecutiveAgent", argc, argv, envp); // ctor binds to listen socket
  ufs.exec((void*)&ufs);
  return argc;
}

// public virtual funcs:
// this should always return the service/agent listen port #
int UFTReCSExecutiveAgent::options(string& servlist) {
  int port = UFDeviceAgent::options(servlist);
  // _epics may have been reset:
  _confGenSub = _epics + ":eg:configG";
  _heart = _epics + ":eg:heartbeat";
  _statRec = _epics + ":eg:sysmonG.J"; // 

  _config = new UFExecutiveConfig(); // needs to be proper device subclass (LS218)
  // 218 defaults for trecs:
  _config->_tsport = 7001; // the baytech is actually telnet port 23, but this agent listens on 52001
  //_config->_tshost = "trecsbaytech";
  _config->_tshost = "false";

  string arg;
  arg = findArg("-boot");  // start all agents
  if( arg == "true" ) {
    _boot = true;
  }

  arg = findArg("-pulse");  // heartbeat period
  if( arg != "true" && arg != "false" ) {
    _pulsePeriod = atoi(arg.c_str());
  }

  arg = findArg("-on");  // turn on all outlets
  if( arg == "true" ) {
    _powerOnAll = true;
  }

  arg = findArg("-pwron"); // turn on all outlets
  if( arg == "true" ) {
    _powerOnAll = true;
  }

  arg = findArg("-powerOn"); // turn on all outlets
  if( arg == "true" ) {
    _powerOnAll = true;
  }

  arg = findArg("-off");  // turn off all outlets
  if( arg == "true" ) {
    _powerOffAll = true;
  }

  arg = findArg("-pwroff"); // turn off all outlets
  if( arg == "true" ) {
    _powerOffAll = true;
  }

  arg = findArg("-powerOff"); // turn off all outlets
  if( arg == "true" ) {
    _powerOffAll = true;
  }

  arg = findArg("-sim"); // don't connect to baytech
  if( arg == "true" ) {
    _sim = true;
  }

  arg = findArg("-noepics"); // don't connect to epics db
  if( arg == "true" ) {
    _epics = "false";
    _heart = "";
    _statRec = "";
  }

  arg = findArg("-epics"); // epics host/db name
  if( arg != "false" && arg != "true" ) {
    _epics = arg;
    _confGenSub = _epics + ":eg:configG";
    _heart = _epics + ":eg:heartbeat";
    _statRec = _epics + ":eg:sysmonG.J"; // 
  }

  arg = findArg("-heart"); // heartbeat epics record
  if( arg != "false" && arg != "true" )
    _heart = arg;

  arg = findArg("-conf");
  if( arg != "false" && arg != "true" )
    _confGenSub = arg;

  arg = findArg("-tsport");
  if( arg != "false" && arg != "true" )
    _config->_tsport = atoi(arg.c_str());

  arg = findArg("-tshost"); // interpret as baytech host, not terminal console!
  if( arg != "false" && arg != "true" )
    _config->_tshost = arg;

  arg = findArg("-baytech"); // connect to baytech, not terminole console
  if( arg != "false" && arg != "true" )
    _config->_tshost = arg;
  
  arg = findArg("-nobaytech"); // do not connect to baytech
  if( arg == "true" )
    _config->_tshost = "false";
  
  arg = findArg("-flush"); 
  if( arg != "false" && arg != "true" )
    _flush = atof(arg.c_str());

  arg = findArg("-v");
  if( arg != "false" ) {
    _verbose = true;
    //UFDeviceConfig::_verbose = true;
  }

  arg = findArg("-c");
  if( arg != "false" )
    _confirm = true;
  arg = findArg("-g");
  if( arg != "false" )
    _confirm = true;

  if( _config->_tsport ) {
    port = 52000 + _config->_tsport - 7000;
  }
  else {
    port = 52001;
  }
  if( _verbose ) 
    clog<<"UFTReCSExecutiveAgent::options> set port to TReCSExecutive port "<<port<<endl;

  return port;
}

// override base class startup here to avoid connect to term. serv
// if no motors are specified, assume simulation is desired,
// then do server listen stuff
void UFTReCSExecutiveAgent::startup() {
  setSignalHandler(UFTReCSExecutiveAgent::sighandler);
  clog << "UFTReCSExecutiveAgent::startup> established signal handler."<<endl;
  // should parse cmd-line options and start listening
  string servlist;
  int listenport = options(servlist);
  // agent that actually uses a serial device
  // attached to the annex or iocomm should initialize
  // a connection to it here:
  init();
  if( _config->_devIO == 0 ) {
    clog<<"UFTReCSExecutiveAgent::startup> no DeviceConfig, assume simulation? "<<endl;
  }
  UFSocketInfo socinfo = theServer.listen(listenport);
  clog << "UFTReCSExecutiveAgent::startup> Ok, startup completed, listening on port= " <<listenport<<endl;
  return;  
}

UFTermServ* UFTReCSExecutiveAgent::init(const string& host, int port) {
  if( _epics != "false" ) {
    _capipe = pipeToEpicsCAchild("trecs");
    if( _capipe )
      clog<<"UFTReCSExecutiveAgent> Epics CA child proc. started"<<endl;
    else
      clog<<"UFTReCSExecutiveAgent> Ecpics CA child proc. failed to start"<<endl;
  }

  //if( _config->_tshost == "" && host != "" )
  //  _config->_tshost = host;
  
  // connect to baytech:
  if( _sim || _config->_tshost == "false" ) {
    _baytech = 0;
  }
  else {
    _baytech = UFBaytech::create(_config->_tshost);
    if( _baytech != 0 )
      _config->_devIO = static_cast<UFTermServ*> (_baytech);
    else {
      clog<<"UFTReCSExecutiveAgent::init> unable to connect to "<<_config->_tshost<<endl;
      UFRndRobinServ::shutdown();
    }
  }
  // test connectivity to baytech:
  string btstat= "";
  if( _baytech )
    btstat = _baytech->status();
  else
    btstat = UFBaytech::statusSim();

  /* 
  strstream s;
  s<<"/tmp/.ufbaytech."<<getpid()<<".txt"<<ends;
  int fd = ::open(s.str(), O_RDWR | O_CREAT, 0777);
  if( fd <= 0 ) {
    clog<<"UFTReCSExecutiveAgent> unable to open "<<s.str()<<endl;
    delete s.str();
    return _config->_devIO;
  }
  delete s.str();
  //clog<<"UFTReCSExecutiveAgent> status (sim: "<<_sim<<" Baytech channels 1 - 8)"<<endl;
  //clog<<"UFTReCSExecutiveAgent> reply: "<<btstat<<ends;
  int nc = ::write(fd, btstat.c_str(), btstat.length());
  if( nc <= 0 ) {
    clog<<"UFTReCSExecutiveAgent::init> status log failed..."<<endl;
  }
  ::close(fd);
  */
  if( _verbose && _baytech )
    clog<<"UFTReCSExecutiveAgent::init> baytech status: "<<btstat<<endl;

  if( _powerOffAll && _baytech )
    _baytech->powerOff();

  //if( (_boot || _powerOnAll) && _baytech )
  if( _powerOnAll && _baytech != 0 )
    _baytech->powerOn();

  //if( _baytech ) { _baytech->close(); _baytech = 0; }

  if( _boot ) {
    string agnts;
    if( _verbose )
      agnts += " -v";
    if( _epics.empty() || _epics == "false" ) {
      agnts += " -noepics";
    }
    else if( _epics == "true" && _sim ) {
      agnts += " -epics miri";
    }
    else {
      agnts += " -epics ";
      agnts += _epics;
    }
    if( _sim )
      agnts = simAgents(agnts);
    else
      agnts = startAgents(agnts);
    if( _verbose )
      clog<<"UFTReCSExecutiveAgent::init> boot/startagents: "<<agnts<<endl;
  }

  return _config->_devIO;
}
  
void UFTReCSExecutiveAgent::shutdown(const string option) {
  if( option.find("full") != string::npos || 
      option.find("Full") != string::npos ||
      option.find("FULL") != string::npos ) {
    // full system shutdown
    string all = "All";
    clog<<"UFTReCSExecutiveAgent::shutdown> stopping all agents..."<<endl;
    string reply = stopAgents(all);
    clog<<"UFTReCSExecutiveAgent::shutdown> power off all baytech channels..."<<endl;
    reply = powerOff(all);
  }

  // disconnect from baytech
  if( _baytech )
    _baytech->close();

  delete _baytech;

  clog<<"UFTReCSExecutiveAgent::shutdown> closing all client connections and terminating..."<<endl;
  // disconnect from all clients and exit
  UFRndRobinServ::shutdown();
}

void* UFTReCSExecutiveAgent::ancillary(void* p) {
  static time_t _prev_clck = 0;
  bool block = false;
  if( p )
    block = *((bool*)p);

  // update the heartbeat:
  time_t clck = time(0);
  if( _epics != "false" && _heart != "" ) {
    strstream s; s<<clck<<ends; string val = s.str(); delete s.str();
    updateEpics(_heart, val);
  }
    
  // update the status values...
  //if( _epics != "false" && _statRec != "" )
  //  updateEpics(_statRec, v);

  if( clck - _prev_clck < _pulsePeriod ) // heartbeat period?
    return p;

  _prev_clck = clck;

  // once a minute or so, check the baytech connection
  // (baytech will close it's connection if client is inactive
  // for too long...
  // there may be other issues as well, the baytech may have 
  // it's own firmware bug which will cause us to lose the connection
  // over time, and/or it may drop out of the outlet control menu
  // and the outlet commands will then be rejected, etc.
  // so it may be best to not maintain the connection, rather
  // re-establish it upon an action request, and close.
  // this is slow, but presumably such actions are infrequent.

  // heartbeat the baytech
  string btstat= UFBaytech::statusSim();
  // always reconnect and close (keeping connection alive causes prblems with baytech?)
  if( !_sim && _config->_tshost != "false" ) {
    // use existing connection from init? 
    if( _baytech->reconnect() < 0 ) {
      btstat = "No connection to Baytech!";
      clog<<"UFTReCSExecutiveAgent::ancillary> baytech (re)connect failed..."<<endl;
    }
    else {
      // check connectivity to baytech:
      btstat = _baytech->status();
      // close connection 
      _baytech->close(); 
      //clog<<"UFTReCSExecutive::ancillary> Pinged Baytech ("<<currentTime()<<")"<<endl;
    }
  }

  if( _verbose && _baytech) {
    clog<<"UFTReCSExecutive::ancillary> Baytech status ("<<currentTime()<<"):"<<endl;
    clog<<btstat<<endl;
  }

  return p;
}

// new action signature:
int UFTReCSExecutiveAgent::action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& repv) {
  delete repv; repv = 0;
  UFProtocol* reply = action(act);
  if( reply == 0 )
    return 0;

  repv = new vector< UFProtocol* >;
  repv->push_back(reply);
  return (int)repv->size();
}

UFProtocol* UFTReCSExecutiveAgent::action(UFDeviceAgent::CmdInfo* act) {
  int stat= 0;
  bool reply_expected= true;
  if( act->clientinfo.find("trecs:") != string::npos ||
      act->clientinfo.find("miri:") != string::npos ) {
    reply_expected = false;
    clog<<"UFTReCSExecutiveAgent::action> No replies will be sent to GeminiEpics client: "
        <<act->clientinfo<<endl;
  }
  /*
  if( _verbose ) {
    clog<<"UFTReCSExecutiveAgent::action> client: "<<act->clientinfo<<endl;
    for( int i = 0; i<(int)act->cmd_name.size(); ++i ) 
      clog<<"UFTReCSExecutiveAgent::action> elem= "<<i<<" "<<act->cmd_name[i]<<" "<<act->cmd_impl[i]<<endl;
  }
  */

  string car, errmsg, agent= "";
  for( int i = 0; i < (int)act->cmd_name.size(); ++i ) {
    string cmdname= act->cmd_name[i];
    string cmdimpl= act->cmd_impl[i];

    if( _verbose ) {
      clog<<"UFTReCSExecutiveAgent::action> cmdname: "<<cmdname<<endl;
      clog<<"UFTReCSExecutiveAgent::action> cmdimpl: "<<cmdimpl<<endl;
    }

    if( cmdname.find("car") == 0 || cmdname.find("Car") == 0 || cmdname.find("CAR") == 0 ) {
      reply_expected= false;
      car = cmdimpl;
      if( _verbose ) clog<<"UFTReCSExecutiveAgent::action> CAR: "<<car<<endl;
      continue;
    }

    act->time_submitted = currentTime();
    _active = act;
    string reply = "";

    if( cmdname.find("shut") == 0 || cmdname.find("Shut") == 0 || cmdname.find("SHUT") == 0 ) {
      // NO REPLY! SHOULD CAUSE A GRACEFULL PROCESS TERMINATION
      shutdown(cmdimpl);
    }
    if( cmdname.find("sim") == 0 || cmdname.find("Sim") == 0 || cmdname.find("SIM") == 0 ) {
      reply = "Simulation Request: ";
      reply += cmdimpl + " -- ";
      reply += simAgents(cmdimpl);
      if( _DevHistory.size() >= _DevHistSz ) _DevHistory.pop_front();
        _DevHistory.push_back(reply);
      int last = (int)_DevHistory.size() - 1;
      act->cmd_reply.push_back(_DevHistory[last]);
    }
    else if( cmdname.find("start") == 0 || cmdname.find("Start") == 0 || cmdname.find("START") == 0 ) {
      reply = "StartAgent Request: ";
      reply += cmdimpl + " -- ";
      reply += startAgents(cmdimpl);
      if( _DevHistory.size() >= _DevHistSz ) _DevHistory.pop_front();
        _DevHistory.push_back(reply);
      int last = (int)_DevHistory.size() - 1;
      act->cmd_reply.push_back(_DevHistory[last]);
    }
    else if( _config->_tshost != "false" ) {
      if( cmdname.find("stop") == 0 || cmdname.find("Stop") == 0 || cmdname.find("STOP") == 0 ) {
        reply = "StopAgent Request: ";
        reply += cmdimpl + " -- ";
        reply += stopAgents(cmdimpl);
        if( _DevHistory.size() >= _DevHistSz ) _DevHistory.pop_front();
          _DevHistory.push_back(reply);
        int last = (int)_DevHistory.size() - 1;
        act->cmd_reply.push_back(_DevHistory[last]);
      }
      else if( cmdname.find("poweron") == 0 || cmdname.find("PowerOn") == 0 || cmdname.find("POWERON") == 0 ) {
        reply = "PowerOn Request: ";
        reply += cmdimpl + " -- ";
        reply += powerOn(cmdimpl);
        if( _DevHistory.size() >= _DevHistSz ) _DevHistory.pop_front();
          _DevHistory.push_back(reply);
        int last = (int)_DevHistory.size() - 1;
        act->cmd_reply.push_back(_DevHistory[last]);
      }
      else if( cmdname.find("poweroff") == 0 || cmdname.find("PowerOff") == 0 || cmdname.find("POWEROFF") == 0 ) {
        reply = "PowerOff Request: ";
        reply += cmdimpl + " -- ";
        reply += powerOff(cmdimpl);
        if( _DevHistory.size() >= _DevHistSz ) _DevHistory.pop_front();
          _DevHistory.push_back(reply);
        int last = (int)_DevHistory.size() - 1;
        act->cmd_reply.push_back(_DevHistory[last]);
      }
      else if( cmdname.find("powerstat") == 0 || cmdname.find("PowerStat") == 0 || cmdname.find("POWERSTAT") == 0 ) {
        reply = "PowerStat Request: ";
        reply += cmdimpl + " -- ";
        reply += powerStat(cmdimpl);
        if( _DevHistory.size() >= _DevHistSz ) _DevHistory.pop_front();
          _DevHistory.push_back(reply);
        int last = (int)_DevHistory.size() - 1;
        act->cmd_reply.push_back(_DevHistory[last]);
      }
    } // if baytech is available
    else { // unsupported request
      clog<<"UFTReCSExecutiveAgent::action> unsupported rquest: "<<cmdname<<", "<<cmdimpl<<endl;
      act->cmd_reply.push_back(cmdname + ", " + cmdimpl + " is not supported");
      break;
    }
  } // end for loop of cmd bundle

  if( stat < 0 || act->cmd_reply.empty() ) {
    // send error (val=3) & error message to _capipe
    act->status_cmd = "failed";
    if( _epics != "false" &&  car != "" ) sendEpics(car, errmsg);
  }
  else {
    // send success (idle val = 0) to _capipe
    act->status_cmd = "succeeded";
    if( _epics != "false" && car != "" ) sendEpics(car, "OK");
  }  
  act->time_completed = currentTime();
  _completed.insert(UFDeviceAgent::CmdList::value_type(act->cmd_time, act));

  if( reply_expected )
    return new UFStrings(act->clientinfo, act->cmd_reply);
  else
    return (UFStrings*) 0;
} // action

int UFTReCSExecutiveAgent::query(const string& q, vector<string>& qreply) {
  return UFDeviceAgent::query(q, qreply);
}

void UFTReCSExecutiveAgent::hibernate() {
  if( _queued.size() == 0 && UFRndRobinServ::theConnections->size() == 0) {
    // no connections, no queued reqs., go to sleep
    sleep(_Update);
  }
  else
    UFSocket::waitOnAll(_Update);
    // sleep(_Update);
}

string UFTReCSExecutiveAgent::status() {
  // for now just baytech status;
  // but this could also status all the agents...
  if( _sim || _baytech == 0 )
    return UFBaytech::statusSim();

  string btstat;
  if( _baytech->reconnect() < 0 ) {
    btstat = "No connection to Baytech!";
    clog<<"UFTReCSExecutiveAgent::status> baytech reconnect failed..."<<endl;
    return btstat;
  }
  btstat = _baytech->status();
  _baytech->close();

  return btstat;
}

string UFTReCSExecutiveAgent::simAgents(const string& agntargs) {
  return UFExecutiveConfig::simAgents(agntargs);
}

string UFTReCSExecutiveAgent::startAgents(const string& agntargs) {
  return UFExecutiveConfig::startAgents(agntargs);
}

string UFTReCSExecutiveAgent::stopAgents(const string& agnt) {
  return UFExecutiveConfig::stopAgents(agnt);
}

string UFTReCSExecutiveAgent::powerOn(const string& pwr) {
  string btstat, nil = "";
  // always (re)connect to baytech:
  if( !_sim ) {
    if( _baytech->reconnect() < 0 ) {
      btstat = "No connection to Baytech!";
      clog<<"UFTReCSExecutiveAgent::powerOn> baytech reconnect failed..."<<endl;
      return btstat;
    }
  }
  
  if( pwr.find("all") != string::npos || pwr.find("All") != string::npos || pwr.find("ALL") != string::npos ) {
    if( _sim ) 
      return _baytech->powerOnSim();
    else
      return _baytech->powerOn();
  }
  int chan = -1;
  const char* cs = pwr.c_str();
  if( isdigit(cs[0]) ) {
    chan = atoi(cs);
    clog<<"UFTReCSExecutiveAgent::powerOn> "<<pwr<<", chan= "<<chan<<endl;
  }
  else {
    chan = UFExecutiveConfig::chanOf(pwr);
    clog<<"UFTReCSExecutiveAgent::powerOn> "<<pwr<<", chan= "<<chan<<endl;
  }
  if( chan > 8 ) {
    clog<<"UFTReCSExecutiveAgent::powerOn> Bad implementation: "<<pwr<<endl;
    return nil;
  }
  if( _sim )
    return _baytech->powerOnSim(chan);
  else {
    btstat = _baytech->powerOn(chan);
    _baytech->close();
  }
  return btstat;
}

string UFTReCSExecutiveAgent::powerOff(const string& pwr) {
  string btstat, nil = "";
  // always (re)connect to baytech:
  if( !_sim ) {
    if( _baytech->reconnect() < 0 ) {
      btstat = "No connection to Baytech!";
      clog<<"UFTReCSExecutiveAgent::powerOff> baytech reconnect failed..."<<endl;
      return btstat;
    }
  }
  if( pwr.find("all") != string::npos || pwr.find("All") != string::npos || pwr.find("ALL") != string::npos ) {
    if( _sim ) 
      return _baytech->powerOffSim();
    else
      return _baytech->powerOff();
  }

  int chan = -1;
  const char* cs = pwr.c_str();
  if( isdigit(cs[0]) ) {
    chan = atoi(cs);
    clog<<"UFTReCSExecutiveAgent::powerOff> "<<pwr<<", chan= "<<chan<<endl;
  }
  else {
    chan = UFExecutiveConfig::chanOf(pwr);
    clog<<"UFTReCSExecutiveAgent::powerOff> "<<pwr<<", chan= "<<chan<<endl;
  }
  if( chan > 8 ) {
    clog<<"UFTReCSExecutiveAgent::powerOff> Bad implementation: "<<pwr<<endl;
    return nil;
  }

  if( _sim )
    return _baytech->powerOffSim(chan);
  else {
    btstat = _baytech->powerOff(chan);
    _baytech->close();
  }
  return btstat;
}

string UFTReCSExecutiveAgent::powerStat(const string& pwr) {
  string btstat, nil = "";
  bool pwrOnOff = true;
  // always (re)connect to baytech:
  if( !_sim ) {
    if( _baytech->reconnect() < 0 ) {
      btstat = "No connection to Baytech!";
      clog<<"UFTReCSExecutiveAgent::powerStat> baytech reconnect failed..."<<endl;
    }
  }

  if( pwr.find("all") != string::npos || pwr.find("All") != string::npos || pwr.find("ALL") != string::npos ) {
    if( _sim ) 
      return _baytech->powerStatSim();
    else {
      pwrOnOff = _baytech->powerStat(-1, btstat);
      _baytech->close();
      return btstat;
    }
  }

  int chan = -1;
  const char* cs = pwr.c_str();
  if( isdigit(cs[0]) ) {
    chan = atoi(cs);
    clog<<"UFTReCSExecutiveAgent::powerStat> "<<pwr<<", chan= "<<chan<<endl;
  }
  else {
    chan = UFExecutiveConfig::chanOf(pwr);
    clog<<"UFTReCSExecutiveAgent::powerStat> "<<pwr<<", chan= "<<chan<<endl;
  }
  if( chan > 8 ) {
    clog<<"UFTReCSExecutiveAgent::powerStat> Bad implementation: "<<pwr<<endl;
    return nil;
  }

  if( _sim )
    return _baytech->powerStatSim(chan);
  else {
    pwrOnOff = _baytech->powerStat(chan, btstat);
    _baytech->close();
  }
  return btstat;
}

#endif // UFTReCSExecutiveAgent

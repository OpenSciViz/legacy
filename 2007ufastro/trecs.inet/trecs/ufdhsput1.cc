#if !defined(__ufdhsput_cc__)
#define __ufdhsput_cc__ "$Name:  $ $Id: ufdhsput1.cc 14 2008-06-11 01:49:45Z hon $";

#include "UFRuntime.h"
#include "UFSocket.h"
#include "UFFITSClient.h"
#include "UFFITSheader.h"
#include "UFFrameClient.h"
#include "UFObsConfig.h"
#include "UFDHSTReCS.h"

static string _dataserver= "dataServerSS";
static bool _noext= false;
static bool _noprm= false;
static bool _verbose= false;
static bool _observatory= true;
static int _sleep = 0;
static int _loop= 0; // _loop <= 0 means forever
static int _w=0, _h=0, _chops=0, _nods=0, _savesets=0, _nodsets=0;
static string _agenthost;
static string _quicklook;
static UFStrings *_fits0Hdr= 0, *_fitsHdr = 0;
static UFSocket::ConnectTable _connections;

// tests individual or all q-look stream(s)
int testQlook(UFDHSTReCS& dhs, UFFrameClient& ufrm, UFFITSClient& ufits, FILE* _fsfits) {
  int imagepixcnt = dhs.width() * dhs.height();
  // quicklook should reuse the same frame-data-buffer pDhsData
  map< string, UFDHSwrap::Stream* > qlstrm; // for all q-look streams
  vector< int > offsets; // if 'frame' contains multiple images
  UFObsConfig* obscfg = 0;
  int* pDhsData = 0; 
  int frmcnt= 0, imgIdx= 0;
  int w= 0, h= 0, total= _loop;
  if( _verbose )
    clog<<"ufdhsput1> quicklook stream: "<<_quicklook<<endl;

  int idx= 0;
  while( _loop <= 0 || idx < _loop ) {
    UFInts* data = 0;
    offsets.clear(); offsets.push_back(0); // always use at least first image in frame?
    // fetch of real frames returns idx & total (by ref.) from current acq. frameconf
    // fetch of simulated frames does not (idx & total are provided here to support simulation)
    if( _fsfits == 0 ) {
      clog<<"ufdhsput1> waiting on frame idx: "<<idx<<endl;
      // need fetchFrame(const vector< string >& bufnames...) to test more than one q-look stream
      //data = ufrm.fetchFrame("simtrecs", idx, total);
      //data = ufrm.fetchFrame(_quicklook, idx, total);
      UFProtocol* ufp = ufrm.replFrame(idx, total); // recv replicated frame, incr. idx
      if( ufp == 0 ) {
        clog<<"ufdhsput1> failed to recv replication frame...abort."<<endl;
        return frmcnt;
      }
      if( ufp->typeId() == UFProtocol::_Ints_ )
        data = dynamic_cast< UFInts* >(ufp); 
      else 
	continue;
    }
    else {
      delete _fitsHdr;
      // this may have multiple images (src1,ref1,src1,ref1,...) OR (src2,ref2,src2,ref2,...)
      // depending on frmcnt % _nods == 0 OR == 1
      data = UFFITSheader::readExtData(_fsfits, _fitsHdr, w, h, _chops, _savesets);
      if( w != dhs.width() || h != dhs.height() ) {
	clog<<"testQlook> mismatch in expected image width & height, epxected: "
	    <<dhs.width()<<" x "<<dhs.height()<<"; got: "<<w<<" x "<<h<<endl;
	return -1;
      }
      ++idx;
    }
    if( data == 0 ) {
      clog<<"ufdhsput> failed to get image data."<<endl;
      dhs.close();
      return -1;
    }
    if( ! obscfg ) // once we have all the parms for the ctor:
      obscfg = new UFObsConfig(_nods, _chops, _savesets, _nodsets);
 
    vector< string > qlnames;
    if( _quicklook == "all" ) {
      int nstrms = obscfg->getBufNames(qlnames);
      for( int i = 0; i < nstrms; ++i )
        qlnames[i] = "trecs:" + qlnames[i];
    }
    else {
      qlnames.push_back(_quicklook);
    }
    int imgcnt = _chops * _savesets;
    if( imagepixcnt < data->numVals() ) { // multi-dim/image frame
      offsets.clear();
      for( int i = 1; i < imgcnt; ++i )
        offsets.push_back(i*imagepixcnt); // presumably offsets[1]-offsets[0] == numpix / image
    }
    if( pDhsData == 0 ) { // allocate one time? -- reuse data buffer?
      for( size_t i = 0; i < qlnames.size(); ++i ) {
        UFDHSwrap::Stream* qls = new UFDHSwrap::Stream(qlnames[i]);
        qlstrm[qlnames[i]] = qls;
        UFDHSwrap::newDataset(qls->_dataLabel, qls->_dataSet, (const string*) &(qls->_name)); 
        clog<<"alloc qlstrm: "<<qls->_name<<", with dataLabel: "<<qls->_dataLabel<<endl;
        //pDhsData = dhs.newFrame(*_fitsHdr, dataLabel, dataSet, dataFrame, _chops, _savesets, idx, _loop, &_quicklook); 
        // just 2-Axis (one image)
        //pDhsData = qls->_pDhsData = dhs.newFrame( *_fitsHdr, qls->_dataLabel, qls->_dataSet, qls->_dataFrame,
	//					    idx, _loop, &qlnames[i]);
        pDhsData = qls->_pDhsData = UFDHSwrap::newFrame(qls->_dataLabel, qls->_dataSet, qls->_dataFrame, 0, _loop, &qlnames[i]);
      } 
    }

    map < string, UFInts* > bufs;
    string qnam;
    bool freeFrm = false;

    if( _fsfits ) { // input images from test mef file
    // iterate through each image in the multi-image frame (MEF) via its offset
    // using obscfg->eval() to evaluate the q-look buffers that must be updated:
      for( size_t i = 0; i < offsets.size(); ++i ) {
        // shallow copy ctor
        UFInts *imgdata = new UFInts("TReCS", (const int*)(data->valData(offsets[i])), imagepixcnt);
        int nq = obscfg->evalTReCS(imgIdx++, imgdata, bufs, 0, freeFrm); // do not free image data here
        clog<<"testQlook> new qlook bufs: "<<nq<<endl; 
        map < string, UFInts* >::iterator it = bufs.begin();
        do {
          qnam = it->first;
	  if( qnam.find("trecs:") == string::npos ) // prepend it
	    qnam = "trecs:" + qnam;
          imgdata = it->second;
          if( imgdata == 0 ) {
            clog<<"?> No image data for dhs stream for "<<qnam<<endl;
            continue;
          }
          UFDHSwrap::Stream* qls = qlstrm[qnam];
          if( qls == 0 && imgdata == 0 ) {
            clog<<"?> No dhs stream for "<<qnam<<endl;
            continue;
          }
          clog<<"testQlook> putting qlook buf name: "<<qnam<<endl; 
          //if( _fitsHdr ) dhs.setAttributes(*_fitsHdr, qls->_dataSet, UFDHSwrap::Extension);
          dhs.putQlookFrame(*imgdata, qls->_dataLabel, qls->_dataSet, qls->_pDhsData, idx);
        } while( ++it != bufs.end() );
      }
    }
    else { // handle one replicated image at a time
      UFInts *imgdata = new UFInts("TReCS", (const int*)(data->valData()), imagepixcnt);
      bufs.clear();
      int nq = 1;
      if( _quicklook.find("all") != string::npos )
        nq = obscfg->evalTReCS(imgIdx++, imgdata, bufs, 0, freeFrm); // do not free image data here
      else // just one cnd line qlook stream
        bufs[_quicklook] = imgdata;
      clog<<"testQlook> new qlook bufs: "<<nq<<", _quicklook: "<<_quicklook<<endl;
      map < string, UFInts* >::iterator it = bufs.begin();
      do {
        qnam = it->first;
	if( qnam.find("trecs:") == string::npos ) // prepend it
	  qnam = "trecs:" + qnam;
        imgdata = it->second;
        if( imgdata == 0 ) {
          clog<<"?> No image data for dhs stream for "<<qnam<<endl;
          continue;
        }
        UFDHSwrap::Stream* qls = qlstrm[qnam];
        if( qls == 0 ) {
          clog<<"?> No dhs stream for "<<qnam<<endl;
          continue;
        }
        clog<<"testQlook> putting qlook buf name: "<<qnam<<endl;
        // if( _fitsHdr) dhs.setAttributes(*_fitsHdr, qls->_dataSet, UFDHSwrap::Extension);
        //dhs.putQlookFrame(*imgdata, qls->_dataLabel, qls->_dataSet, qls->_pDhsData, idx);
        dhs.putQlookFrame(*imgdata, qls->_dataLabel, qls->_dataSet, qls->_pDhsData, 0); // always idx=0
        //dhs.putQlookFrame(*imgdata, qls->_dataLabel, qls->_dataSet, qls->_pDhsData, 0, true); // always final?
      } while( ++it != bufs.end() );
    }
    delete data; // frees all image data
    frmcnt++;

    if( _verbose )
      clog<<"ufdhsput1> sent quicklook: "<<_quicklook<<", frmcnt: "<<frmcnt<<endl;
    // allow dhs time to process it all?
    if( _sleep > 0 )
      ::sleep(_sleep);
  } // for _loop

  map< string, UFDHSwrap::Stream* >::iterator it = qlstrm.begin(); // for all q-look streams
  for(; it != qlstrm.begin(); ++it ) {
    UFDHSwrap::Stream* qls = it->second;
    dhs.freeStream(qls);
  }
  qlstrm.clear();
  return frmcnt;
}

// archive, need to init. the dataLabel if not provided externally
int testArchive(UFDHSTReCS& dhs, UFFrameClient& ufrm, UFFITSClient& ufits, FILE* _fsfits) {
  DHS_BD_DATASET dataSet;
  DHS_BD_FRAME dataFrame;
  string dataLabel;
  int* pDhsData = 0; 
  int imagepixcnt = dhs.width() * dhs.height();
  int w= 0, h= 0, frmcnt= 0, total= _chops*_savesets;
 
  //UFDHSwrap::newDataset(dataLabel, dataSet); // can set primary header values with first/final frame(s)?
  dhs.newDataset(*_fits0Hdr, dataLabel, dataSet); // set primary header observation here (ala OCS)
  dhs.freeDataset(dataSet);

  // process archive file:
  if( _verbose )
    clog<<"ufdhsput1> archive dataLabel: "<<dataLabel<<endl;

  // archive should use N. Hill's algorithm, re-use the dataLabel,
  // (re)alloc. and free dataSets/dataFrames:
  int idx= 0;
  while( _loop <= 0 || idx < _loop ) {
    // get next frame
    // fetch of real frames returns idx & total (by ref.)
    // fetch of simluated frames does not (idx & total are provided here to support simulation)
    UFInts* data = 0;
    vector< UFInts* > datavec;
    if( _fsfits == 0 ) {
      UFInts* repldata = 0;
      int chopIdx= 0;
      do {
        clog<<"ufdhsput1> waiting on frame idx: "<<idx<<", chopIdx: "<<chopIdx<<endl;
        //data = ufrm.fetchFrame("simtrecs", chopIdx, total);
        UFProtocol* ufp = ufrm.replFrame(idx, total); // recv replicated frame, incr. idx
        if( ufp == 0 ) {
	  clog<<"ufdhsput1> failed to recv replication frame...abort."<<endl;
          return frmcnt;
        }
        if( ufp->typeId() == UFProtocol::_Ints_ )
          repldata = dynamic_cast< UFInts* >(ufp); 
        else 
	  continue;
        datavec.push_back(repldata);
        clog<<"ufdhsput1> got frame idx: "<<idx<<", chopIdx: "<<chopIdx<<endl;
      } while( chopIdx < total ); 
      clog<<"ufdhsput1> collected all (replicated) images for frame idx: "<<idx<<", nodIdx: "<<idx<<endl;
    }
    else { // may be multi-dimensional (multi-image) frame?
      data = UFFITSheader::readExtData(_fsfits, _fitsHdr, w, h, _chops, _savesets);
      if( w != dhs.width() || h != dhs.height() ) {
	clog<<"testArchive> mismatch in expected image width & height, epxected: "
	    <<dhs.width()<<" x "<<dhs.height()<<"; got: "<<w<<" x "<<h<<endl;
	return -1;
      }
    }
    if( data == 0 ) {
      clog<<"ufdhsput> failed to get image data from frame service."<<endl;
      dhs.close();
      return -1;
    }

    if( imagepixcnt == data->numVals() ) { // one-dim frame
      clog<<"ufshdput1> archive frame is 1-D, imagepixcnt = "<<imagepixcnt<<endl;
      if( _chops == 0 ) _chops = 1;
      if( _savesets == 0 ) _savesets = 1;
    }

    if( _noext ) 
      _fitsHdr = 0; // try no extensions...
    if( _noprm ) 
      _fits0Hdr = 0; // try no extensions or final primary...

    // dhs accepts attributes only after first frame is put?
    if( idx == 0 ) { // first frame, set initial fits hdr attributes, dataLabel gets set != ""
      if( _fitsHdr ) { // extension header
        if( _fits0Hdr != 0 ) {
          // can set primary header start values here (if OCS has not)
          //pDhsData = dhs.newFrame(*_fits0Hdr, *_fitsHdr, dataLabel, dataSet, dataFrame, chops, savesets, idx, _loop); 
          pDhsData = dhs.newFrame(*_fitsHdr, dataLabel, dataSet, dataFrame, _chops, _savesets, idx, _loop); 
          // this should be after newFrame() to insure dataSet is properly (re)initialized...
          dhs.setAttributes(*_fits0Hdr, dataSet, UFDHSTReCS::Initial); // set for dataSet not dataFrame!
        }
        else { 
          pDhsData = dhs.newFrame(*_fitsHdr, dataLabel, dataSet, dataFrame, _chops, _savesets, idx, _loop);
        }
      }
      else { // no extension header?
        pDhsData = dhs.newFrame(dataLabel, dataSet, dataFrame, _chops, _savesets, idx, _loop); 
      }
      if( data ) 
        dhs.putArchiveFrame(*data, dataLabel, dataSet, pDhsData, idx);
      else
        dhs.putArchiveFrame(datavec, dataLabel, dataSet, pDhsData, idx);
    } // not first frame
    else if( idx == _loop - 1 ) { // final frame, get final fits values (for primary header)
      if( _fsfits == 0 )
        _fits0Hdr = ufits.fetchAllFITS(_connections, 0.5, _observatory);
      if( _fits0Hdr == 0 ) {
        clog<<"ufdhsput> failed to get final FITS info from agents."<<endl;
        pDhsData = dhs.newFrame(dataLabel, dataSet, dataFrame, _chops, _savesets, idx, _loop);  
      }
      else if( _fitsHdr ) { // final frame extension header, and primary header end keywords?
        pDhsData = dhs.newFrame(*_fitsHdr, dataLabel, dataSet, dataFrame, _chops, _savesets, idx, _loop); 
        // also set primary header 'end' values here
        //pDhsData = dhs.newFrame(*_fits0Hdr, *_fitsHdr, dataLabel, dataSet, dataFrame, chops, savesets, idx, _loop); 
        // this should be after newFrame() to insure dataSet is properly (re)initialized...
        dhs.setAttributes(*_fits0Hdr, dataSet, UFDHSTReCS::Final); // set for dataSet not dataFrame!
        //pDhsData = dhs.newFrame(*_fitsHdr, dataLabel, dataSet, dataFrame, chops, savesets, idx, _loop); 
      }
      else { // no extension header or primary header available for final frame
        pDhsData = dhs.newFrame(dataLabel, dataSet, dataFrame, _chops, _savesets, idx, _loop); 
      }
      if( data )
        dhs.putArchiveFrame(*data, dataLabel, dataSet, pDhsData, idx, true); // finalframe == true
      else
        dhs.putArchiveFrame(datavec, dataLabel, dataSet, pDhsData, idx, true); // finalframe == true
    } // final frame
    else { // intermediate frame
      if( _fitsHdr ) {
        pDhsData = dhs.newFrame(*_fitsHdr, dataLabel, dataSet, dataFrame, _chops, _savesets, idx, _loop); 
      }
      else { // no extension header
        pDhsData = dhs.newFrame(dataLabel, dataSet, dataFrame, _chops, _savesets, idx, _loop); 
      }
      if( data )
        dhs.putArchiveFrame(*data, dataLabel, dataSet, pDhsData, idx);
      else
        dhs.putArchiveFrame(datavec, dataLabel, dataSet, pDhsData, idx);
    }
    if( data ) delete data; // from file
    if( datavec.size() > 0 ) { // from replication server
      for( int i=0; i < (int)datavec.size(); ++i ) { delete datavec[i]; }
      datavec.clear();
    }
    dhs.freeDataset(dataSet); // frees frame; but re-use dataLabel
    ++idx; 
    frmcnt++;
    //if( _verbose ) {
      clog<<"ufdhsput1> sent archive frmcnt: "<<frmcnt<<endl;
    //::getchar(); // allow inspection of verbose output
    //}
  } // for _loop

  return frmcnt;
} // finished archiving observation

int main(int argc, char** argv, char** envp) {
  // unit test of single dhs connection client
  // use this UFDaemon class for argv stuff:
  UFFITSClient ufits("ufdhsput1", argc, argv, envp);

  string arg = ufits.findArg("-v");
  if( arg == "true" ) { 
    _verbose = true;
    UFDHSwrap::_verbose = true;
  }

  arg = ufits.findArg("-vv");
  if( arg == "true" ) { 
    _verbose = true;
    UFDHSwrap::_verbose = true;
    UFFITSheader::_verbose = true;
  }

  arg = ufits.findArg("-nowait");
  if( arg == "true" )
    UFDHSwrap::_wait = false;

  arg = ufits.findArg("-noext");
  if( arg == "true" ) { // no extension headers
    _noext = true;
  }
  arg = ufits.findArg("-noprm");
  if( arg == "true" ) { // no primary header
    _noprm = true;
  }

  arg = ufits.findArg("-dd");
  if( arg == "true" ) { // write data dictionay to stdout in libdd.config format
    UFDHSwrap::_writeDD = true;
  }

  arg = ufits.findArg("-chops");
  if( arg != "true" && arg != "false" )
    _chops = atoi(arg.c_str());

  arg = ufits.findArg("-nods");
  if( arg != "true" && arg != "false" )
    _nods = atoi(arg.c_str());

  arg = ufits.findArg("-nodsets");
  if( arg != "true" && arg != "false" ) {
    _nodsets = atoi(arg.c_str());
    _loop = _nods * _nodsets;
  }

  arg = ufits.findArg("-ns");
  if( arg != "true" && arg != "false" ) {
    _nodsets = atoi(arg.c_str());
    _loop = _nods * _nodsets;
  }

  arg = ufits.findArg("-savesets");
  if( arg != "true" && arg != "false" )
    _savesets = atoi(arg.c_str());

  arg = ufits.findArg("-ss");
  if( arg != "true" && arg != "false" )
    _savesets = atoi(arg.c_str());

  arg = ufits.findArg("-loop");
  if( arg != "true" && arg != "false" )
    _loop = atoi(arg.c_str());

  arg = ufits.findArg("-l");
  if( arg != "true" && arg != "false" )
    _loop = atoi(arg.c_str());

  arg = ufits.findArg("-cnt");
  if( arg != "true" && arg != "false" )
    _loop = atoi(arg.c_str());

  bool _observatory= true;
  arg = ufits.findArg("-noobs");
  if( arg == "true" ) // bother with observatory stuff?
    _observatory = false;

  _agenthost = UFRuntime::hostname();
  arg = ufits.findArg("-host");
  if( arg != "true" && arg != "false" )
    _agenthost = arg;

  arg = ufits.findArg("-agents");
  if( arg != "true" && arg != "false" )
    _agenthost = arg;

  arg = ufits.findArg("-agent");
  if( arg != "true" && arg != "false" )
    _agenthost = arg;

  arg = ufits.findArg("-q");
  if( arg != "false" ) {
    _quicklook = "all";
    if( arg != "true" )
    _quicklook = "trecs:" + arg;
  }

  arg = ufits.findArg("-qlook");
  if( arg != "false" ) {
    _quicklook = "all";
    if( arg != "true" )
    _quicklook = "trecs:" + arg;
  }

  arg = ufits.findArg("-sleep");
  if( arg != "false" ) {
    if( arg != "true" )
      _sleep = atoi(arg.c_str());
    else
      _sleep = 1; // default 1 sec. sleep between frames
  }
  arg = ufits.findArg("-s");
  if( arg != "false" ) {
    if( arg != "true" )
      _sleep = atoi(arg.c_str());
    else
      _sleep = 1; // default 1 sec. sleep between frames
  }
  int _total = _loop; // total number of (multi-dim.) frames/extentions in header == _nods*_nodsets
  string _file = "";
  FILE* _fsfits= 0;;
  arg = ufits.findArg("-file");
  if( arg != "false" ) {
    if( arg == "true" ) { // use stdin
      _fsfits = stdin;
      _file = "stdin";
    }
    else {
      _file = arg;
      _fsfits = ::fopen(_file.c_str(), "r");
      if( _fsfits == 0 ) {
	clog<<"ufdhsput1> failed to open file: "<<_file<<endl;
	return -1;
      }
      _fitsHdr = _fits0Hdr = UFFITSheader::readPrmHdr(_fsfits, _total, _nods, _nodsets);
    }
  }
  if( _loop < _total) _loop = _total; // file header overrides any cmd-line opts
 
  _w= 320; _h= 240;
  arg = ufits.findArg("-w");
  if( arg != "false" && arg != "true" )
    _w = atoi(arg.c_str());

  arg = ufits.findArg("-h");
  if( arg != "false" && arg != "true" )
    _h = atoi(arg.c_str());

  UFFrameClient ufrm(_agenthost, 52000, _w, _h);
  UFSocket::ConnectTable connections;
  if( _fsfits == 0 ) {
    // connect to agents
    UFFITSClient::AgentLoc loc;
    int aidx = ufits.locateAgents(_agenthost, loc);
    if( aidx <= 0 ) {
      clog<<"ufdhsput1> no agents located"<<endl;
      return -1;
    }
    int ncon= 0;
    while( ncon <= 0 ) {
      ncon = ufits.connectAgents(loc, connections, -1, true);
      if( ncon <= 0 ) {
        if( _verbose )
          clog<<"ufdhsput> no connections yet, sleep & retry..."<<endl;
        ufits.sleep(5.0);
      }
    }
    // get current header, for start of observation
    _fits0Hdr = ufits.fetchAllFITS(connections, 0.5, _observatory);
    if( _fits0Hdr == 0 ) {
      clog<<"ufdhsput1> failed to get any FITS info from agents."<<endl;
      return -1;
    }

    // connect to Frank's generic frame server:
    int frmport = 52000;
    //int fc = ufrm.connect(_agenthost, frmport;
    // connect to the frame replication server:
    frmport = 52009;
    int fc = ufrm.replConnect(_agenthost, frmport);
    if( fc <= 0 )
      clog<<"ufdhsput1> unable to connect to Frame Server: "<<_agenthost<<", port: "<<frmport<<endl;
    else 
      clog<<"ufdhsput1> connected to Frame Server: "<<_agenthost<<", port: "<<frmport<<endl;
  }

  // connect to the dhs:
  arg = ufits.findArg("-ns");
  if( arg != "true" && arg != "false" )
    _dataserver = "dataServerNS";

  arg = ufits.findArg("-nb");
  if( arg != "true" && arg != "false" )
    _dataserver = "dataServerNB";

  string clhost = UFRuntime::hostname();
  string clhostIP = UFSocket::ipAddrOf(clhost);
  string _dhshost = clhostIP;
  arg = ufits.findArg("-dhs");
  if( arg != "false" && arg != "true" ) {
    //_dhshost = arg;
    _dhshost = UFSocket::ipAddrOf(arg);
    if( arg.find("kepler") != string::npos ) // force dataserver name:
      _dataserver = "dataServerNB";
    if( arg.find("trifid") != string::npos ) // force dataserver name:
      _dataserver = "dataServerNS";
  }
  if( _dhshost == clhostIP ) {
    if( clhost.find("kepler") != string::npos ) // force dataserver name:
      _dataserver = "dataServerNB";
    if( clhost.find("trifid") != string::npos ) // force dataserver name:
      _dataserver = "dataServerNS";
  }

  UFDHSTReCS dhs(_w, _h);
  DHS_STATUS dstat = dhs.open(_dhshost, _dataserver);
  if( dstat != DHS_S_SUCCESS )
    return -1;

  int frmcnt = 0;
  if( _quicklook != "" ) { // tests single/individual (or all) q-look stream(s)
    frmcnt = testQlook(dhs, ufrm, ufits, _fsfits);
    dhs.close();
    if( _verbose ) 
      clog<<"ufdhsput1> closed DHS connection; completed qlook put frmcnt: "<<frmcnt<<" of "<<_loop<<endl;
    return 0;
  } // quicklook

  // archive, need to init. the dataLabel if not provided externally
  frmcnt = testArchive(dhs, ufrm, ufits, _fsfits);
  dhs.close();

  if( _verbose ) 
    clog<<"ufdhsput1> closed DHS connection; completed archive put frmcnt: "<<frmcnt<<" of "<<_loop<<endl;

  return 0;
}


#endif // __ufdhsput1_cc__

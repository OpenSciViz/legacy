#if !defined(__UFTReCSExecutiveAgent_h__)
#define __UFTReCSExecutiveAgent_h__ "$Name:  $ $Id: UFTReCSExecutiveAgent.h,v 0.3 2003/02/07 14:57:31 hon Exp $"
#define __UFTReCSExecutiveAgent_H__(arg) const char arg##UFTReCSExecutiveAgent_h__rcsId[] = __UFTReCSExecutiveAgent_h__;

#include "string"
#include "vector"
#include "iostream.h"

#include "UFGemDeviceAgent.h"
#include "UFBaytech.h"

class UFTReCSExecutiveAgent: public UFGemDeviceAgent {
public:
  static int main(int argc, char** argv, char **envp);
  UFTReCSExecutiveAgent(int argc, char** argv, char **envp);
  UFTReCSExecutiveAgent(const string& name, int argc, char** argv, char **envp);
  inline virtual ~UFTReCSExecutiveAgent() {}

  static void sighandler(int sig);

  // override these UFDeviceAgent virtuals:
  virtual void startup();

  // supplemental options (should call UFDeviceAgent::options() last)
  virtual int options(string& servlist);

  // in adition to establishing the baytech connection,
  // open the pipe to the epics channel access "helper"
  virtual UFTermServ* init(const string& host= "", int port= 0);

  virtual UFProtocol* action(UFDeviceAgent::CmdInfo* act);
  virtual int action(UFDeviceAgent::CmdInfo* a, vector< UFProtocol* >*& replies);

  virtual void* ancillary(void* p);
  virtual void hibernate();
  // default base class behavior should suffice...
  virtual int query(const string& q, vector<string>& reply);

  string status();

  // full system shutdown (stop all agents, powerOff all, exit) or just self shutdown
  void shutdown(const string option);

  string simAgents(const string& agntargs= "");
  string startAgents(const string& agntargs= "");
  string stopAgents(const string& agnt= "");

  string powerOn(const string& pwr);
  string powerOff(const string& pwr);
  string powerStat(const string& pwr);

protected:
  static bool _boot, _powerOnAll, _powerOffAll;
  static int _pulsePeriod;
  static UFBaytech* _baytech;
};

#endif // __UFTReCSExecutiveAgent_h__
      

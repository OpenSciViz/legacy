#if !defined(__ufgtrecsd_cc__)
#define __ufgtrecsd_cc__ "$Name:  $ $Id: ufgtrecsd.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __ufgtrecsd_cc__;

#include "UFTReCSExecutiveAgent.h"

int main(int argc, char** argv, char** env) {
  try {
    UFTReCSExecutiveAgent::main(argc, argv, env);
  }
  catch( std::exception& e ) {
    clog<<"ufgtrecsd> exception in stdlib: "<<e.what()<<endl;
    return -1;
  }
  catch( ... ) {
    clog<<"ufgtrecsd> unknown exception..."<<endl;
    return -2;
  }
  return 0;
}
#endif // __ufgtrecsd_cc__

#if !defined(__UFGP354Config_h__)
#define __UFGP354Config_h__ "$Name:  $ $Id: UFGP354Config.h,v 0.0 2002/06/03 17:42:21 hon beta $"
#define __UFGP354Config_H__(arg) const char arg##UFGP354Config_h__rcsId[] = __UFGP354Config_h__;

#include "UFDeviceConfig.h"

class UFGP354Config : public UFDeviceConfig {
public:
  UFGP354Config(const string& name= "UnknownGP354@DefaultConfig");
  UFGP354Config(const string& name, const string& tshost, int tsport);
  inline virtual ~UFGP354Config() {}

  // override these virtuals:
  virtual vector< string >& UFGP354Config::queryCmds();
  virtual vector< string >& UFGP354Config::actionCmds();
  // return -1 if invalid, 0 if valid but no reply expected,
  // n > 0 if valid and n reply strings expected:
  virtual int validCmd(const string& c);

  virtual string terminator();
  virtual string prefix();

  // overide these:
  virtual UFStrings* status(UFDeviceAgent* da);
  virtual UFStrings* statusFITS(UFDeviceAgent* da);
};

#endif // __UFGP354Config_h__

#if !defined(__UFGranvilPhilAgent_cc__)
#define __UFGranvilPhilAgent_cc__ "$Name:  $ $Id: UFGranvilPhilAgent.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFGranvilPhilAgent_cc__;

#include "sys/types.h"
#include "sys/stat.h"
#include "fcntl.h"

#include "strstream"
#include "string"
#include "vector"

// default command table:
// this should really be in the UFGranPhilConfig UFDeviceConfig sub-class...
const char* _GranvilPhilDevQuerries__[] = { "help" };
const char* _GranvilPhilDevActions__[] = { "sim 0", "sim 1", "sim 2" };
const char* _GranvilPhilDevActImpl__[] = { " %d %d", " %d %d %d", " %d %d %d" };

#include "UFGranvilPhilAgent.h"
#include "UFGPConfig.h"

// ctors:
UFGranvilPhilAgent::UFGranvilPhilAgent(int argc, char** argv) : UFDeviceAgent(argc, argv),
								_history() { _flush = 0.35; }

UFGranvilPhilAgent::UFGranvilPhilAgent(const string& name,
			               int argc, char** argv) : UFDeviceAgent(name, argc, argv),
						                _history() { _flush = 0.35; }

// static funcs:
int UFGranvilPhilAgent::main(int argc, char** argv) {
  UFGranvilPhilAgent ufs("UFGranvilPhilAgent", argc, argv); // ctor binds to listen socket
  ufs.exec((void*)&ufs);
  return argc;
}

// public virtual funcs:
UFTermServ* UFGranvilPhilAgent::init(const string& host, int port) {
  if( _config == 0 ) {
    clog<<"UFGranvilPhilAgent> failed to init device config?"<<endl;
    return 0;
  }
  _config->connect();
  if( _config->_devIO == 0 ) {
    clog<<"UFGranvilPhilAgent> failed to connect to device port..."<<endl;
    return 0;
  }

  // test connectivity to vacuum guage
  // should put these specifics into UFDeviceConfig subclass UFPortscapConfig...
  // assuming space separated list of 1 character motor names:
  // save transaction to a file
  pid_t p = getpid();
  strstream s;
  s<<"/tmp/.granvilphil."<<p<<".txt"<<ends;
  int fd = ::open(s.str(), O_RDWR | O_CREAT, 0777);
  if( fd <= 0 ) {
    clog<<"UFGranvilPhilAgent> unable to open "<<s.str()<<endl;
    delete s.str();
    return _config->_devIO;
  }
  delete s.str();
  string granvilphil; // granvilphil reply string
  vector < string > gp;  
  gp.push_back("DS IG1"); gp.push_back("DS IG2"); gp.push_back("DS CG1");  gp.push_back("DS CG2");
  //gp.push_back("DGS"); gp.push_back("FPS"); gp.push_back("SWS");  gp.push_back("PCS");
  int nc, cnt = (int)gp.size();
  string allchan = "(" + UFRuntime::currentTime() + ") ";
  _currentval = "";
  for( int i = 0; i < cnt; ++i ) {
    nc = _config->_devIO->submit(gp[i], granvilphil, _flush); // test for a prompt
    if( nc <= 0 ) {
      clog<<"UFGranvilPhilAgent> connection to granville-phillips failed..."<<endl;
      break;
    }
    size_t p = granvilphil.rfind("\r\n");
    if( p != string::npos )
      granvilphil.erase(p, strlen("\r\n"));
    strstream s;
    s<<i<<","<<ends;
    _currentval += s.str() + granvilphil + " ";
    allchan += s.str() + granvilphil + " ";
    delete s.str();
  }
  nc = ::write(fd, allchan.c_str(), allchan.length());
  if( nc <= 0 ) {
    clog<<"UFGranvilPhilAgent> indexor full status log failed..."<<endl;
  }
  ::close(fd); // leaving it open confuses ::select
  _history.push_back(allchan);
  return _config->_devIO;
}

// this should always return the service/agent listen port #
int UFGranvilPhilAgent::options(string& servlist) {
  _config = new UFGPConfig(); // needs to be proper device subclass 
  int port = UFDeviceAgent::options(servlist);
  string arg; 
  _config->_tsport = 7004;
  _config->_tshost = "192.168.111.100";

  arg = findArg("-tsport"); // motor names
  if( arg != "false" && arg != "true" )
    _config->_tsport = atoi(arg.c_str());

  arg = findArg("-tshost"); // motor names
  if( arg != "false" && arg != "true" )
    _config->_tshost = arg;
  
  arg = findArg("-flush"); // motor names
  if( arg != "false" && arg != "true" )
    _flush = atof(arg.c_str());

  port = 52000 + _config->_tsport - 7000;

  clog<<"UFGranvilPhilAgent::options> set port to GranvilPhil port "<<port<<endl;
  return port;
}

// these are the key virtual functions to override,
// send command string to TermServ, and return response
// presumably any allocated memory is freed by the calling layer....
// for the moment assume "raw" commands and simply pass along to mce4
// new signature for action:
int UFGranvilPhilAgent::action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& rep) {
  rep = new vector< UFProtocol* >;
  rep->push_back(_allChan(act));
  return (int) rep->size();
}

// return reply
UFStrings* UFGranvilPhilAgent::query(UFDeviceAgent::CmdInfo* q) {
  return _allChan(q); 
} 

UFStrings* UFGranvilPhilAgent::_allChan(UFDeviceAgent::CmdInfo* req) {
  // use the annex/iocomm port to submit the reqion command and get reply:
  _active = req;

  // always fetch the latest values for all channels first:
  string granvilphil; // granvilphil reply string
  vector < string > gp;  
  gp.push_back("DS IG1"); gp.push_back("DS IG2"); gp.push_back("DS CG1");  gp.push_back("DS CG2");
  //gp.push_back("DGS"); gp.push_back("FPS"); gp.push_back("SWS");  gp.push_back("PCS");
  int stat= 0, cnt = (int)gp.size();
  string allchan = "(" + UFRuntime::currentTime() + ") ";
  req->time_submitted = currentTime();
  for( int i = 0; i < cnt; ++i ) {
    stat = _config->_devIO->submit(gp[i], granvilphil); // test for a prompt
    if( stat <= 0 ) {
      clog<<"UFGranvilPhilAgent> comm. to granville-phillips failed..."<<endl;
      break;
    }
    size_t p = granvilphil.rfind("\r\n");
    if( p != string::npos )
      granvilphil.erase(p, strlen("\r\n"));
    strstream s;
    s<<i<<","<<ends;
    allchan += s.str() + granvilphil + " ";
    delete s.str();
  }
  req->time_completed = currentTime();

  int ncmd = (int) req->cmd_name.size();
  for( int i = 0; i < ncmd; ++i ) {
    string cmdname = req->cmd_name[i];
    string cmdimpl = req->cmd_impl[i];
    if( cmdimpl.find("&") != string::npos ) { // stop processing list
      clog<<"UFGranvilPhilAgent::action> rejected: "<<cmdimpl<<endl;
      return (UFStrings*) 0;
    }
    bool raw= true;
    if( cmdname != "raw" && cmdname != "Raw" && cmdname != "RAW" )
      raw = false;

    // this logic also belongs in the DeviceConfig class:
    // usr this for "all" request:
    UFStrings* retval= 0;
    const char* impl = cmdimpl.c_str();
    bool all = (cmdimpl == "all" || cmdimpl == "All" || cmdimpl == "ALL");
    cnt = (int) _history.size(); // default is all
    if( !all && !isdigit(impl[0]) ) {
      req->status_cmd = "rejected";
      req->cmd_reply.push_back("illegal command, must idicate either all or count");
      retval = new UFStrings(req->clientinfo, req->cmd_reply);
      return retval;
    }
    else if( stat <= 0 ) {
      req->status_cmd = "failed";
      req->cmd_reply.push_back("comm. to granville-phillips failed...");
      retval = new UFStrings(req->clientinfo, req->cmd_reply);
      return retval;
    }
    else {
      _history.push_back(allchan);
      req->status_cmd = "succeeded";
    }

    if( !all )
      cnt = atoi(impl);

    int last = (int)_history.size() - 1;
    for( int i = 0; i < cnt; ++i ) {
      req->cmd_reply.push_back(_history[last - i]);
    }
  }
  _completed.insert(UFDeviceAgent::CmdList::value_type(req->cmd_time, req));

  return new UFStrings(req->clientinfo, req->cmd_reply);
} 
#endif // UFGranvilPhilAgent

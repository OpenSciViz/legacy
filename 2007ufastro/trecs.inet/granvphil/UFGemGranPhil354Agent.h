#if !defined(__UFGemGranPhil354Agent_h__)
#define __UFGemGranPhil354Agent_h__ "$Name:  $ $Id: UFGemGranPhil354Agent.h,v 0.4 2003/09/16 14:23:32 hon beta $"
#define __UFGemGranPhil354Agent_H__(arg) const char arg##UFGemGranPhil354Agent_h__rcsId[] = __UFGemGranPhil354Agent_h__;

#include "string"
#include "vector"
#include "iostream.h"

#include "UFGemDeviceAgent.h"
#include "UFGP354Config.h"

class UFGemGranPhil354Agent: public UFGemDeviceAgent {
public:
  static int main(int argc, char** argv);
  UFGemGranPhil354Agent(int argc, char** argv);
  UFGemGranPhil354Agent(const string& name, int argc, char** argv);
  inline virtual ~UFGemGranPhil354Agent() {}

  static void sighandler(int sig);

  // override these UFDeviceAgent virtuals:
  virtual void startup();

  // supplemental options (should call UFDeviceAgent::options() last)
  virtual int options(string& servlist);

  // in adition to establishing the iocomm/annex connection to
  // the motors, open the pipe to the epics channel access "helper"
  virtual UFTermServ* init(const string& host= "", int port= 0);

  //virtual int initTables(UFDeviceAgent::DevCmdTable& dtbl, UFDeviceAgent::QuerryCmds& qlist);

  //virtual UFProtocol* action(UFDeviceAgent::CmdInfo* act);
  // new signature for action:
  virtual int action(UFDeviceAgent::CmdInfo* act, vector< UFProtocol* >*& rep);

  virtual void* ancillary(void* p);
  virtual void hibernate();

  // default base class behavior should suffice...
  virtual int query(const string& q, vector<string>& reply);

  inline string getCurrent() { return _currentval; }


protected:
  static string _currentval;

  // override UFDevAgent virtual:
  virtual void _setDefaults(const string& instrum= "trecs");
};

#endif // __UFGemGranPhil354Agent_h__
      

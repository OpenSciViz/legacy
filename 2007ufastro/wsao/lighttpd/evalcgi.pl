#!/usr/local/bin/perl -w
use strict;
use CGI::Fast;
while( my $fcgi = new CGI::Fast ) {
  my $file = $ENV{SCRIPT_FILENAME};
  print $fcgi->header; # $fcgi->header("text/plain");
  print $fcgi->start_html("Fast CGI: ENV{SCRIPT_FILENAME} = $ENV{SCRIPT_FILENAME}\n");
  print $fcgi->body("file = $file\n");
  print $fcgi->end_html;
  delete($INC{$file}); # force require to reload file	
  eval('require("$file")'); 	
}

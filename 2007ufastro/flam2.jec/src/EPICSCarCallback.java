package uffjec;

public class EPICSCarCallback extends EPICSCallback {//implements UFCAToolkit.MonitorListener{

    EPICSCarListener carListener = null;
    private String myName = null;
    String hashKey = null;


    public EPICSCarCallback(String carName,EPICSCarListener cl) {
	try {
            if (carName.indexOf(".") == -1 && carName.indexOf(":") == -1) {
                hashKey = carName;
                carName = EPICS.recs.get(hashKey);
            }
	    if (!carName.equals("null")) EPICS.addMonitor(carName,this);
	    carListener = cl;
	    myName = carName;
	    ecVector.add(this);
	} catch (Exception ex) {
	    System.err.println("EPICSCarCallback> Problem setting up monitor:"
			       +ex.toString());
	}
    }

    public void disconnect() {
	EPICS.removeMonitor(myName, this);
	ecVector.remove(this);
    }

    public void reconnect(String dbname) {
	EPICS.removeMonitor(myName,this);
	if (hashKey != null) {
	   myName = EPICS.recs.get(hashKey);
	} else {
	   myName = dbname + myName.substring(myName.indexOf(":")+1);
	}
	if (!myName.equals("null")) EPICS.addMonitor(myName,this);
    }

  

    public void monitorChanged(String value) {
	try {
	    if (value == null) { //now this really shouldn't happen. what-up, jca?
		System.err.println("EPICSCarCallback::monitorChanged> Null event object. Can ignore if just starting up.");
		return;
	    }
	    int x = 0;
	    if (value.trim().toLowerCase().equals("null") || value.equals(""))
		x = Integer.MIN_VALUE;
	    else if (value.equals("IDLE"))
		x = EPICS.IDLE;
	    else if (value.equals("BUSY"))
		x = EPICS.BUSY;
	    else if (value.equals("ERROR"))
		x = EPICS.ERROR;
	    else 
		x = Integer.parseInt(value.trim());
	    carListener.carTransition(x);
	} catch (Exception e) {
	    System.err.println("EPICSCarCallback::monitorChanged> "+
			       myName+" : "+e.toString());
	}
    }

}

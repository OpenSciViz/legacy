package uffjec;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javaUFLib.UFColorButton;

public class FJECSubSystemPanel extends JPanel{
    
    static final long serialVersionUID = 0;
	
    EPICSApplyButton initSystemButton;
    EPICSApplyButton datumSystemButton;
    UFColorButton parkSystemButton;
    UFColorButton clearSystemButton;

    String ssprefix;

    public static final String consistantLayout = "0.21,0.92;0.50,0.08";

    int CAR_FLAG;

    public static final int INITC = 1;
    public static final int DATUMC = 2;
    public static final int SETUPC = 4;
    public static final int PARKC = 8;

    public FJECSubSystemPanel(String subsystemPrefix) {
	CAR_FLAG = 0;
	ssprefix = subsystemPrefix.trim();
	if (ssprefix.endsWith(":")) ssprefix = ssprefix.substring(0,ssprefix.length()-1);
	String ssnocolon = ssprefix;

	initSystemButton = new EPICSApplyButton("Init "+ssprefix.toUpperCase(),EPICS.prefix+ssprefix+":init.DIR");
	//initSystemButton.setFont(new Font("Serif",Font.PLAIN,10));
	//datumSystemButton = new EPICSApplyButton("Datum "+ssprefix.toUpperCase(),EPICS.prefix+ssprefix+":datum.All");
        datumSystemButton = new EPICSApplyButton("Datum "+ssprefix.toUpperCase(),ssprefix+"DatumAll");
	//datumSystemButton.setFont(new Font("Serif",Font.PLAIN,10));
	clearSystemButton = new UFColorButton("Clear "+ssprefix.toUpperCase(),UFColorButton.COLOR_SCHEME_GREEN);
        clearSystemButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                if (EPICS.prefix.equals("null:"))
                    //JOptionPane.showMessageDialog(null, "Warning: You are not connected to any database!", "Warning", JOptionPane.WARNING_MESSAGE);
                    fjecFrame.connectToEPICS();
            }
        });
	//clearSystemButton.setFont(new Font("Serif",Font.PLAIN,10));
	parkSystemButton = new UFColorButton("Park "+ssprefix.toUpperCase(),UFColorButton.COLOR_SCHEME_GREEN);
	//parkSystemButton.setFont(new Font("serif",Font.PLAIN,10));
	initSystemButton.setColorGradient(UFColorButton.COLOR_SCHEME_GREEN);
	datumSystemButton.setColorGradient(UFColorButton.COLOR_SCHEME_GREEN);
	clearSystemButton.setColorGradient(UFColorButton.COLOR_SCHEME_YELLOW);
	parkSystemButton.setColorGradient(UFColorButton.COLOR_SCHEME_YELLOW);

	ssprefix += ":";
	setLayout(new GridLayout(1,0,10,10));
	add(clearSystemButton);
	add(initSystemButton);
	add(datumSystemButton);
	add(parkSystemButton);
	clearSystemButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    clearSystem();
		}
	    });
	parkSystemButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    parkSystem();
		}
	    });
	//new EPICSCarCallback(EPICS.prefix+ssprefix+"initC",new EPICSCarListener(){
        new EPICSCarCallback(ssnocolon+"initC",new EPICSCarListener(){
		public void carTransition(int value){
		    if (value == EPICS.IDLE) {
			CAR_FLAG &= ~INITC;
			if (CAR_FLAG == 0)
			    ableButtons(true);
			signalNoInit(false);
		    } else if (value == EPICS.BUSY) {
			CAR_FLAG |= INITC;
			ableButtons(false);
			signalNoInit(false);
		    } else if (value == EPICS.ERROR) {
			CAR_FLAG &= ~INITC;
			if (CAR_FLAG == 0)
			    ableButtons(true);
			signalNoInit(false);
		    } else if (value == Integer.MIN_VALUE) {
			signalNoInit(true);
		    }else {
			System.err.println("FJECSubSystemPanel.carTransition> Bad Car Value: "+value);
		    }
		}
	    });
	//new EPICSCarCallback(EPICS.prefix+ssprefix+"datumC",new EPICSCarListener(){
        new EPICSCarCallback(ssnocolon+"datumC",new EPICSCarListener(){
		public void carTransition(int value){
		    if (value == EPICS.IDLE) {
			CAR_FLAG &= ~DATUMC;
			if (CAR_FLAG == 0)
			    ableButtons(true);
		    } else if (value == EPICS.BUSY) {
			CAR_FLAG |= DATUMC;
			ableButtons(false);
		    } else if (value == EPICS.ERROR) {
			CAR_FLAG &= ~DATUMC;
			if (CAR_FLAG == 0)
			    ableButtons(true);
		    } else if(value == Integer.MIN_VALUE) {

		    } else {
			System.err.println("FJECSubSystemPanel.carTransition> Bad Car Value: "+value);
		    }
		}
	    });
        //new EPICSCarCallback(EPICS.prefix+ssprefix+"setupC",new EPICSCarListener(){
	new EPICSCarCallback(ssnocolon+"setupC",new EPICSCarListener(){
		public void carTransition(int value){
		    if (value == EPICS.IDLE) {
			CAR_FLAG &= ~SETUPC;
			if (CAR_FLAG == 0)
			    ableButtons(true);
		    } else if (value == EPICS.BUSY) {
			CAR_FLAG |= SETUPC;
			ableButtons(false);
		    } else if (value == EPICS.ERROR) {
			CAR_FLAG &= ~SETUPC;
			if (CAR_FLAG == 0)
			    ableButtons(true);
		    } else if (value == Integer.MIN_VALUE) {

		    } else {
			System.err.println("FJECSubSystemPanel.carTransition> Bad Car Value: "+value);
		    }
		}
	    });
        //new EPICSCarCallback(EPICS.prefix+ssprefix+"parkC",new EPICSCarListener(){
	new EPICSCarCallback(ssnocolon+"parkC",new EPICSCarListener(){
		public void carTransition(int value){
		    if (value == EPICS.IDLE) {
			CAR_FLAG &= ~PARKC;
			if (CAR_FLAG == 0)
			    ableButtons(true);
		    } else if (value == EPICS.BUSY) {
			CAR_FLAG |= PARKC;
			ableButtons(false);
		    } else if (value == EPICS.ERROR) {
			CAR_FLAG &= ~PARKC;
			if (CAR_FLAG == 0)
			    ableButtons(true);
		    } else if (value == Integer.MIN_VALUE) {
			
		    } else {
			System.err.println("FJECSubSystemPanel.carTransition> Bad Car Value: "+value);
		    }
		}
	    });

    }

    public void signalNoInit(boolean noInit) {
	if (noInit) {
	    initSystemButton.setColorGradient(UFColorButton.COLOR_SCHEME_BLACK);
	} else {
	    initSystemButton.setColorGradient(UFColorButton.COLOR_SCHEME_GREEN);	    
	}
    }

    public void ableButtons(boolean enable) {
	clearSystemButton.setEnabled(enable);
	initSystemButton.setEnabled(enable);
	datumSystemButton.setEnabled(enable);
	parkSystemButton.setEnabled(enable);
    }


    public void clearSystem() {
	//EPICS.put(EPICS.applyPrefix+"apply.DIR",EPICS.CLEAR);
	//EPICS.put(EPICS.prefix+ssprefix+"initC",EPICS.IDLE);
	//EPICS.put(EPICS.prefix+ssprefix+"datumC",EPICS.IDLE);
	//EPICS.put(EPICS.prefix+ssprefix+"datum.DIR",EPICS.CLEAR);
	//EPICS.put(EPICS.applyPrefix+"applyC",EPICS.IDLE);
	//EPICS.put(EPICS.applyPrefix+"apply.DIR",EPICS.CLEAR);
	//EPICS.put(EPICS.prefix+ssprefix+"setup.DIR",EPICS.CLEAR);
        EPICS.put(EPICS.recs.get(ssprefix.replaceAll(":","")+"Setup"),EPICS.CLEAR);
	//EPICS.put(EPICS.prefix+ssprefix+"datum.DIR",EPICS.CLEAR);
        EPICS.put(EPICS.recs.get(ssprefix.replaceAll(":","")+"ClearDatum"),EPICS.CLEAR);
    }
    public void parkSystem() {
	System.err.println("FJECSubsystemPanel.parkSystem> Not Yet Implemented");
    }
    
}

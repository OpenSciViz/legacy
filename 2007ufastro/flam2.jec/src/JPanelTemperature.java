package uffjec;

import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.io.*;
import java.util.*;


import javaUFLib.*;
//===============================================================================
/**
 *This Class handles the Temperature tabbed pane
 */
public class JPanelTemperature extends JPanel {
    static final long serialVersionUID = 0;
	
    TextFile pw;
    public static final String rcsID = "$Name:  $ $Id: JPanelTemperature.java,v 0.44 2006/09/21 22:23:00 flam Exp $";
    public String fileName;                 // file containing temperature sensors config.
    public String td_filename = "cc.td"; // default temperature graph data filename is cc.td
    
    // do NOT automatically put set point,
    // can cause bad temperature setting, so use set point PUT button.
    
    EPICSTextField set_Point ;
    EPICSTextField p_Text; // = new JTextField();
    EPICSTextField i_Text; // = new JTextField();
    EPICSTextField d_Text; // = new JTextField();
    EPICSTextField m_Text;
    JCheckBox pBox;
    JCheckBox iBox;
    JCheckBox dBox;
    JCheckBox hBox;
    JCheckBox mBox;
    
    EPICSTextField set_Point2 ;
    EPICSTextField p_Text2; // = new JTextField();
    EPICSTextField i_Text2; // = new JTextField();
    EPICSTextField d_Text2; // = new JTextField();
    EPICSTextField m_Text2; // = new JTextField();
    JCheckBox pBox2;
    JCheckBox iBox2;
    JCheckBox dBox2;
    JCheckBox hBox2;
    JCheckBox mBox2;

    JButton jButtonPID = new JButton("Hide  PID1");
    JButton jButtonPID2 = new JButton("Hide  PID2");
    JTextField actualTdet = new JTextField();
    TemperatureSensorParameter graphSensorParams[];
    TemperatureSensorParameter sensorParms[];
    TemperatureSensorParameter pressureSensor[];
    TemperatureSensorParameter detectorSensor[];
    GridLayout gridLayout1 = new GridLayout();
    GridLayout gridLayout2 = new GridLayout();
    GridLayout gridLayout3 = new GridLayout();
    JPanel jPanelLeft = new JPanel();
    JPanel jSetPUnitPanel = new JPanel();
    JPanel jColorPanel = new JPanel();
    JPanel jPanelSensorLabel = new JPanel();
    JPanel jPanelSensorTemp = new JPanel();
    JPanel jPanelSensorUnits = new JPanel(); 
    JPanel jColorPanel2 = new JPanel();
    JPanel jPanelSensorLabel2 = new JPanel();
    JPanel jPanelSensorTemp2 = new JPanel();
    JPanel jPanelSensorUnits2 = new JPanel(); 
    JPanel jPanelTempControl = new JPanel();
    //JPanel jRatePanel = new JPanel();
    JPanel jPanelHeaterA = new JPanel();
    JPanel jPanelHeaterB = new JPanel();
    //JPanel jRateUnitPanel = new JPanel();
    EPICSComboBox heaterAComboBox;
    EPICSComboBox heaterBComboBox;
    EPICSComboBox lockComboBox;
    JLabel errMsgLabel = new JLabel();

    String dataOutputFormat = "CSV";
    

    //-------------------------------------------------------------------------------
    /**
     *Constructs the frame
     *@param fileName string that contains the file name
     */
    public JPanelTemperature(String fileName) {
	enableEvents(AWTEvent.WINDOW_EVENT_MASK);
	try  {
	    this.fileName = fileName;
	    jbInit();
	}
	catch(Exception e) {
	    e.printStackTrace();
	}
    } //end of JPanelTemperature

    //-------------------------------------------------------------------------------
    /**
     *Component initialization
     */
    private void jbInit() throws Exception
    {
  
	this.setLayout(new RatioLayout());

	String [] itemsA = {"Off (0)","Low (1)","Medium (2)","High (3)"};
	//heaterAComboBox = new EPICSComboBox(EPICS.prefix+"ec:setup.CamRangeA", itemsA,EPICSComboBox.INDEX );
        heaterAComboBox = new EPICSComboBox(EPICSRecs.camrange_a, itemsA,EPICSComboBox.INDEX );
	String [] itemsB = {"Off (0)","On (1)"};
	//heaterBComboBox = new EPICSComboBox(EPICS.prefix+"ec:setup.CamRangeB", itemsB,EPICSComboBox.INDEX );
        heaterBComboBox = new EPICSComboBox(EPICSRecs.camonoff_b, itemsB,EPICSComboBox.INDEX );

	String[] itemsLock = {"Unlock (0)","Lock (1)"};
	lockComboBox = new EPICSComboBox(EPICSRecs.lock, itemsLock, EPICSComboBox.INDEX);

	ReadFile( fileName );
	//set_Point = new EPICSTextField(EPICS.prefix+"ec:setup.CamSetPntA","No Description");
	//p_Text = new EPICSTextField(EPICS.prefix+"ec:setup.CamPA","No Description");
        set_Point = new EPICSTextField(EPICSRecs.camsetpoint_a,"No Description");
        p_Text = new EPICSTextField(EPICSRecs.camp_a,"No Description");
	p_Text.setHorizontalAlignment(JTextField.RIGHT);
	//i_Text = new EPICSTextField(EPICS.prefix+"ec:setup.CamIA","No Description");
        i_Text = new EPICSTextField(EPICSRecs.cami_a,"No Description");
	i_Text.setHorizontalAlignment(JTextField.RIGHT);
	//d_Text = new EPICSTextField(EPICS.prefix+"ec:setup.CamDA","No Description");
        d_Text = new EPICSTextField(EPICSRecs.camd_a,"No Description");
	d_Text.setHorizontalAlignment(JTextField.RIGHT);
        m_Text = new EPICSTextField(EPICSRecs.amanual,"No Description");
        m_Text.setHorizontalAlignment(JTextField.RIGHT);
	//set_Point2 = new EPICSTextField(EPICS.prefix+"ec:setup.CamSetPntB","No Description");
	//p_Text2 = new EPICSTextField(EPICS.prefix+"ec:setup.CamPB","No Description");
        set_Point2 = new EPICSTextField(EPICSRecs.camsetpoint_b,"No Description");
        p_Text2 = new EPICSTextField(EPICSRecs.camp_b,"No Description");
	p_Text2.setHorizontalAlignment(JTextField.RIGHT);
	//i_Text2 = new EPICSTextField(EPICS.prefix+"ec:setup.CamIB","No Description");
        i_Text2 = new EPICSTextField(EPICSRecs.cami_b,"No Description");
	i_Text2.setHorizontalAlignment(JTextField.RIGHT);
	//d_Text2 = new EPICSTextField(EPICS.prefix+"ec:setup.CamDB","No Description");
        d_Text2 = new EPICSTextField(EPICSRecs.camd_b,"No Description");
	d_Text2.setHorizontalAlignment(JTextField.RIGHT);
        m_Text2 = new EPICSTextField(EPICSRecs.bmanual,"No Description");
        m_Text2.setHorizontalAlignment(JTextField.RIGHT);

	p_Text.setEnabled(false);
	i_Text.setEnabled(false);
	d_Text.setEnabled(false);
	m_Text.setEnabled(false);
	p_Text2.setEnabled(false);
	i_Text2.setEnabled(false);
	d_Text2.setEnabled(false);
        m_Text2.setEnabled(false);
	heaterAComboBox.setEnabled(false);
	heaterBComboBox.setEnabled(false);

	pBox = new JCheckBox("P",false);
	iBox = new JCheckBox("I",false);
	dBox = new JCheckBox("D",false);
	hBox = new JCheckBox("Heater",false);
        mBox = new JCheckBox("Man Pct",false);
	pBox2 = new JCheckBox("P",false);
	iBox2 = new JCheckBox("I",false);
	dBox2 = new JCheckBox("D",false);
	hBox2 = new JCheckBox("Heater",false);
        mBox2 = new JCheckBox("Man Pct",false);
	pBox.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    p_Text.setEnabled(pBox.isSelected());
		}
	    });
	iBox.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    i_Text.setEnabled(iBox.isSelected());		    
		}
	    });
	dBox.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    d_Text.setEnabled(dBox.isSelected());		    
		}
	    });
        hBox.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent ae) {
                    heaterAComboBox.setEnabled(hBox.isSelected());
                }
            });
        mBox.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent ae) {
                    m_Text.setEnabled(mBox.isSelected());
                }
            });
	pBox2.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    p_Text2.setEnabled(pBox2.isSelected());		    
		}
	    });
	iBox2.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    i_Text2.setEnabled(iBox2.isSelected());		    
		}
	    });
	dBox2.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    d_Text2.setEnabled(dBox2.isSelected());		    
		}
	    });
        hBox2.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent ae) {
                    heaterBComboBox.setEnabled(hBox2.isSelected());
                }
            });
        mBox2.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent ae) {
                    m_Text2.setEnabled(mBox2.isSelected());
                }
            });


	actualTdet.setEditable(false);
	actualTdet.setBackground(new Color(175,175,175));
	gridLayout1.setColumns(1);
	gridLayout1.setRows(0);
	gridLayout1.setVgap(0);
	gridLayout2.setColumns(1);
	gridLayout2.setRows(0);
	gridLayout2.setVgap(0);
	gridLayout3.setColumns(1);
	gridLayout3.setRows(0);
	gridLayout3.setVgap(0);
	jPanelLeft.setLayout(gridLayout1);
	jSetPUnitPanel.setLayout(gridLayout1);
	jColorPanel.setLayout(gridLayout2);
	jPanelSensorLabel.setLayout(gridLayout2);
	jPanelSensorTemp.setLayout(gridLayout2);
	jPanelSensorUnits.setLayout(gridLayout2);
	//jRateUnitPanel.setLayout(gridLayout2);
	jColorPanel2.setLayout(gridLayout2);
	jPanelSensorLabel2.setLayout(gridLayout2);
	jPanelSensorTemp2.setLayout(gridLayout2);
	jPanelSensorUnits2.setLayout(gridLayout2);
	//jRatePanel.setLayout(gridLayout2);
	jPanelTempControl.setLayout(gridLayout1);


	jColorPanel.add(new JLabel(""));
	jPanelSensorLabel.add(new JLabel("Sensor Name"));
	jPanelSensorTemp.add(new JLabel("Temperature"));
	jPanelSensorUnits.add(new JLabel(""));
	//jRatePanel.add(new JLabel(" Rate"));
	//jRateUnitPanel.add(new JLabel(""));

	jColorPanel2.add(new JLabel(""));
	jPanelSensorLabel2.add(new JLabel("Sensor Name"));
	jPanelSensorTemp2.add(new JLabel("Temperature"));
	jPanelSensorUnits2.add(new JLabel(""));

	for (int i=0; i<sensorParms.length/2; i++) {
	    jColorPanel.add(sensorParms[i].statusColor);
	    jPanelSensorLabel.add(sensorParms[i].label);
	    jPanelSensorTemp.add(sensorParms[i].temp);
	    jPanelSensorUnits.add(sensorParms[i].units);
	    //jRatePanel.add(sensorParms[i].graphElement.rate);
	    //jRateUnitPanel.add(sensorParms[i].graphElement.units);
	}
	for (int i=sensorParms.length/2; i<sensorParms.length; i++) {
	    jColorPanel2.add(sensorParms[i].statusColor);
	    jPanelSensorLabel2.add(sensorParms[i].label);
	    jPanelSensorTemp2.add(sensorParms[i].temp);
	    jPanelSensorUnits2.add(sensorParms[i].units);
	    //jRatePanel.add(sensorParms[i].graphElement.rate);
	    //jRateUnitPanel.add(sensorParms[i].graphElement.units);
	}

   
	JPanel ls332aPanel = new JPanel();
	ls332aPanel.setLayout(new RatioLayout());
	ls332aPanel.setBorder(new EtchedBorder(0));
	ls332aPanel.add("0.01,0.01;0.99,0.08",new JLabel("LS332 Loop 1 Input A -- Camera Bench Control",JLabel.CENTER));
	ls332aPanel.add("0.25,0.09;0.20,0.09",new JLabel("Set",JLabel.CENTER));
	ls332aPanel.add("0.49,0.09;0.20,0.09",new JLabel("Last",JLabel.CENTER));
	ls332aPanel.add("0.74,0.09;0.20,0.09",new JLabel("Current",JLabel.CENTER));
	ls332aPanel.add("0.01,0.18;0.20,0.10",new JLabel("Set Pt",JLabel.CENTER));
	ls332aPanel.add("0.25,0.18;0.20,0.10",set_Point);
	//ls332aPanel.add("0.49,0.21;0.20,0.10",new EPICSLabel(EPICS.prefix+"ec:setup.VALCamSetPntA"));
        ls332aPanel.add("0.49,0.18;0.20,0.10",new EPICSLabel(EPICSRecs.valCamsetpoint_a));
	ls332aPanel.add("0.74,0.18;0.20,0.10",new EPICSLabel(EPICSRecs.sadCamASetPt));
	//ls332aPanel.add("0.74,0.21;0.20,0.10",detectorSensor[0].temp);
	//ls332aPanel.add("0.01,0.35;0.99,0.05",new JSeparator());
        ls332aPanel.add("0.01,0.29;0.20,0.10",hBox);
        ls332aPanel.add("0.25,0.29;0.20,0.10",heaterAComboBox);
        //ls332aPanel.add("0.49,0.85;0.20,0.10",new EPICSLabel(EPICS.prefix+"ec:setup.VALCamRangeA"));
        ls332aPanel.add("0.49,0.29;0.20,0.10",new EPICSLabel(EPICSRecs.valCamrange_a));
        ls332aPanel.add("0.74,0.29;0.20,0.10",new EPICSLabel(EPICSRecs.sadCamARange));

	ls332aPanel.add("0.01,0.43;0.20,0.10",pBox);
	ls332aPanel.add("0.25,0.43;0.20,0.10",p_Text);
	//ls332aPanel.add("0.49,0.41;0.20,0.10",new EPICSLabel(EPICS.prefix+"ec:setup.VALCamPA"));
        ls332aPanel.add("0.49,0.43;0.20,0.10",new EPICSLabel(EPICSRecs.valCamp_a));
	EPICSLabel sadCamAP = new EPICSLabel(EPICSRecs.sadCamAPID);
	sadCamAP.setTokenNum(1);
        ls332aPanel.add("0.74,0.43;0.20,0.10", sadCamAP);
	//ls332aPanel.add("0.74,0.41;0.20,0.10",new JLabel("dummy"));
	ls332aPanel.add("0.01,0.53;0.20,0.10",iBox);
	ls332aPanel.add("0.25,0.53;0.20,0.10",i_Text);
	//ls332aPanel.add("0.49,0.51;0.20,0.10",new EPICSLabel(EPICS.prefix+"ec:setup.VALCamIA"));
        ls332aPanel.add("0.49,0.53;0.20,0.10",new EPICSLabel(EPICSRecs.valCami_a));
        EPICSLabel sadCamAI = new EPICSLabel(EPICSRecs.sadCamAPID);
        sadCamAI.setTokenNum(2);
        ls332aPanel.add("0.74,0.53;0.20,0.10", sadCamAI);
	//ls332aPanel.add("0.74,0.51;0.20,0.10",new JLabel("dummy"));
	ls332aPanel.add("0.01,0.63;0.20,0.10",dBox);
	ls332aPanel.add("0.25,0.63;0.20,0.10",d_Text);
	//ls332aPanel.add("0.49,0.61;0.20,0.10",new EPICSLabel(EPICS.prefix+"ec:setup.VALCamDA"));
        ls332aPanel.add("0.49,0.63;0.20,0.10",new EPICSLabel(EPICSRecs.valCamd_a));
        EPICSLabel sadCamAD = new EPICSLabel(EPICSRecs.sadCamAPID);
        sadCamAD.setTokenNum(3);
        ls332aPanel.add("0.74,0.63;0.20,0.10", sadCamAD);
	//ls332aPanel.add("0.74,0.61;0.20,0.10",new JLabel("dummy"));
	ls332aPanel.add("0.01,0.74;0.20,0.10",mBox);
	ls332aPanel.add("0.25,0.74;0.20,0.10",m_Text);
        ls332aPanel.add("0.49,0.74;0.20,0.10",new EPICSLabel(EPICSRecs.valAmanual));
        ls332aPanel.add("0.74,0.74;0.20,0.10",new EPICSLabel(EPICSRecs.sadAmanual));
	JLabel currTempALabel = new JLabel("Curr Temp:", JLabel.CENTER);
	currTempALabel.setForeground(Color.BLUE);
        JLabel camAAPwrLabel = new JLabel("App. Power:", JLabel.CENTER);
        camAAPwrLabel.setForeground(Color.BLUE);
	//EPICSLabel camABenchLabel = new EPICSLabel(EPICSRecs.sadCamABench);
	detectorSensor[0].temp.setForeground(Color.BLUE);
	EPICSLabel sadAPwrALabel = new EPICSLabel(EPICSRecs.sadCamAAPwr);
	sadAPwrALabel.setForeground(Color.BLUE);
	ls332aPanel.add("0.01,0.88;0.20,0.10",currTempALabel);
        ls332aPanel.add("0.25,0.88;0.20,0.10",detectorSensor[0].temp);
        ls332aPanel.add("0.49,0.88;0.20,0.10",camAAPwrLabel);
        ls332aPanel.add("0.74,0.88;0.20,0.10",sadAPwrALabel);

	JPanel ls332bPanel = new JPanel();
	ls332bPanel.setLayout(new RatioLayout());
	ls332bPanel.setBorder(new EtchedBorder(0));
	ls332bPanel.add("0.01,0.01;0.99,0.08",new JLabel("LS332 Loop 2 Input B -- Detector Control",JLabel.CENTER));
	ls332bPanel.add("0.25,0.09;0.20,0.09",new JLabel("Set",JLabel.CENTER));
	ls332bPanel.add("0.49,0.09;0.20,0.09",new JLabel("Last",JLabel.CENTER));
	ls332bPanel.add("0.74,0.09;0.20,0.09",new JLabel("Current",JLabel.CENTER));
	ls332bPanel.add("0.01,0.18;0.20,0.10",new JLabel("Set Pt",JLabel.CENTER));
	ls332bPanel.add("0.25,0.18;0.20,0.10",set_Point2);
	//ls332bPanel.add("0.01,0.35;0.99,0.05",new JSeparator());
	//ls332bPanel.add("0.49,0.21;0.20,0.10",new EPICSLabel(EPICS.prefix+"ec:setup.VALCamSetPntB"));
        ls332bPanel.add("0.49,0.18;0.20,0.10",new EPICSLabel(EPICSRecs.valCamsetpoint_b));
        ls332bPanel.add("0.74,0.18;0.20,0.10",new EPICSLabel(EPICSRecs.sadCamBSetPt));
	//ls332bPanel.add("0.74,0.21;0.20,0.10",detectorSensor[1].temp);
        ls332bPanel.add("0.01,0.29;0.20,0.10",hBox2);
        ls332bPanel.add("0.25,0.29;0.20,0.10",heaterBComboBox);
        //ls332bPanel.add("0.49,0.85;0.20,0.10",new EPICSLabel(EPICS.prefix+"ec:setup.VALCamRangeB"));
        ls332bPanel.add("0.49,0.29;0.20,0.10",new EPICSLabel(EPICSRecs.valCamonoff_b));
	EPICSLabel sadAnalogLabel = new EPICSLabel(EPICSRecs.sadAnalog);
	sadAnalogLabel.setTokenNum(2);
        sadAnalogLabel.addDisplayValue("0","Off");
        sadAnalogLabel.addDisplayValue("3","On");
        ls332bPanel.add("0.74,0.29;0.20,0.10",sadAnalogLabel);

	ls332bPanel.add("0.01,0.43;0.20,0.10",pBox2);
	ls332bPanel.add("0.25,0.43;0.20,0.10",p_Text2);
	//ls332bPanel.add("0.49,0.41;0.20,0.10",new EPICSLabel(EPICS.prefix+"ec:setup.VALCamPB"));
        ls332bPanel.add("0.49,0.43;0.20,0.10",new EPICSLabel(EPICSRecs.valCamp_b));
        EPICSLabel sadCamBP = new EPICSLabel(EPICSRecs.sadCamBPID);
        sadCamBP.setTokenNum(1);
        ls332bPanel.add("0.74,0.43;0.20,0.10", sadCamBP);
	//ls332bPanel.add("0.74,0.41;0.20,0.10",new JLabel("dummy"));
	ls332bPanel.add("0.01,0.53;0.20,0.10",iBox2);
	ls332bPanel.add("0.25,0.53;0.20,0.10",i_Text2);
	//ls332bPanel.add("0.49,0.51;0.20,0.10",new EPICSLabel(EPICS.prefix+"ec:setup.VALCamIB"));
        ls332bPanel.add("0.49,0.53;0.20,0.10",new EPICSLabel(EPICSRecs.valCami_b));
        EPICSLabel sadCamBI = new EPICSLabel(EPICSRecs.sadCamBPID);
        sadCamBI.setTokenNum(2);
        ls332bPanel.add("0.74,0.53;0.20,0.10", sadCamBI);
	//ls332bPanel.add("0.74,0.51;0.20,0.10",new JLabel("dummy"));
	ls332bPanel.add("0.01,0.63;0.20,0.10",dBox2);
	ls332bPanel.add("0.25,0.63;0.20,0.10",d_Text2);
	//ls332bPanel.add("0.49,0.61;0.20,0.10",new EPICSLabel(EPICS.prefix+"ec:setup.VALCamDB"));
        ls332bPanel.add("0.49,0.63;0.20,0.10",new EPICSLabel(EPICSRecs.valCamd_b));
        EPICSLabel sadCamBD = new EPICSLabel(EPICSRecs.sadCamBPID);
        sadCamBD.setTokenNum(3);
        ls332bPanel.add("0.74,0.63;0.20,0.10", sadCamBD);
	//ls332bPanel.add("0.74,0.61;0.20,0.10",new JLabel("dummy"));
        ls332bPanel.add("0.01,0.74;0.20,0.10",mBox2);
        ls332bPanel.add("0.25,0.74;0.20,0.10",m_Text2);
        ls332bPanel.add("0.49,0.74;0.20,0.10",new EPICSLabel(EPICSRecs.valBmanual));
        ls332bPanel.add("0.74,0.74;0.20,0.10",new EPICSLabel(EPICSRecs.sadBmanual));
        JLabel currTempBLabel = new JLabel("Curr Temp:", JLabel.CENTER);
        currTempBLabel.setForeground(Color.BLUE);
        JLabel camBAPwrLabel = new JLabel("App. Power:", JLabel.CENTER);
        camBAPwrLabel.setForeground(Color.BLUE);
        detectorSensor[1].temp.setForeground(Color.BLUE);
        EPICSLabel sadAPwrBLabel = new EPICSLabel(EPICSRecs.sadCamBAPwr);
        sadAPwrBLabel.setForeground(Color.BLUE);
        ls332bPanel.add("0.01,0.88;0.20,0.10",currTempBLabel);
        ls332bPanel.add("0.25,0.88;0.20,0.10",detectorSensor[1].temp);
        ls332bPanel.add("0.49,0.88;0.20,0.10",camBAPwrLabel);
        ls332bPanel.add("0.74,0.88;0.20,0.10",sadAPwrBLabel);

        JPanel ls332LockPanel = new JPanel();
        ls332LockPanel.setLayout(new RatioLayout());
        ls332LockPanel.setBorder(new EtchedBorder(0));
        ls332LockPanel.add("0.01,0.01;0.99,0.20",new JLabel("LS332 Lock Status",JLabel.CENTER));
	ls332LockPanel.add("0.1,0.24;0.30,0.25",new JLabel("Set:"));
        ls332LockPanel.add("0.1,0.49;0.30,0.25",new JLabel("Last:"));
        ls332LockPanel.add("0.1,0.74;0.30,0.25",new JLabel("Current:"));
        ls332LockPanel.add("0.41,0.24;0.50,0.25",lockComboBox);
	ls332LockPanel.setToolTipText("Lock/unlock from control panel with code 123");
        ls332LockPanel.add("0.41,0.49;0.50,0.25",new EPICSLabel(EPICSRecs.valLock));
	EPICSLabel sadLockLabel = new EPICSLabel(EPICSRecs.sadLock);
	sadLockLabel.addDisplayValue("0,123","Unlocked",new Color(0,180,0));
        sadLockLabel.addDisplayValue("1,123","Locked",new Color(180,0,0));
        ls332LockPanel.add("0.41,0.74;0.50,0.25",sadLockLabel);

	JPanel tmPanelMOSDewar = new JPanel();
	tmPanelMOSDewar.setLayout(new RatioLayout());
	tmPanelMOSDewar.setBorder(new EtchedBorder(0));
	JLabel trash1 = new JLabel("LS218 -- MOS Dewar");
	trash1.setHorizontalAlignment(JLabel.CENTER);
	tmPanelMOSDewar.add("0.01,0.00;0.99,0.08", trash1);
	tmPanelMOSDewar.add("0.01,0.08;0.10,0.92", jColorPanel);
	tmPanelMOSDewar.add("0.11,0.08;0.38,0.92", jPanelSensorLabel);
	tmPanelMOSDewar.add("0.49,0.08;0.32,0.92", jPanelSensorTemp);
	tmPanelMOSDewar.add("0.81,0.08;0.10,0.92", jPanelSensorUnits);

	JPanel tmPanelCamera = new JPanel();
	tmPanelCamera.setLayout(new RatioLayout());
	tmPanelCamera.setBorder(new EtchedBorder(0));
	JLabel trash2 = new JLabel("LS218 -- Camera");
	trash2.setHorizontalAlignment(JLabel.CENTER);
	tmPanelCamera.add("0.01,0.00;0.99,0.08", trash2);
	tmPanelCamera.add("0.01,0.08;0.10,0.92", jColorPanel2);
	tmPanelCamera.add("0.11,0.08;0.38,0.92", jPanelSensorLabel2);
	tmPanelCamera.add("0.49,0.08;0.32,0.92", jPanelSensorTemp2);
	tmPanelCamera.add("0.81,0.08;0.10,0.92", jPanelSensorUnits2);

	JPanel pressurePanel = new JPanel();
	pressurePanel.setBorder(new EtchedBorder(0));
	pressurePanel.setLayout(new GridLayout(0,1));
	JPanel pressurePanel_sub0 = new JPanel();
	pressurePanel_sub0.setLayout(new RatioLayout());
	JLabel trash3 = new JLabel("Pressure");
	trash3.setHorizontalAlignment(JLabel.CENTER);
	pressurePanel_sub0.add("0.01,0.01;0.25,0.99",pressureSensor[0].label);
	pressurePanel_sub0.add("0.26,0.01;0.40,0.99",pressureSensor[0].temp);
	pressurePanel_sub0.add("0.70,0.01;0.29,0.99",pressureSensor[0].unitSelect);
	//pressurePanel_sub0.add("0.85,0.01;0.14,0.99",pressureSensor[0].graphElement.showme);
	JPanel pressurePanel_sub1 = new JPanel();
	pressurePanel_sub1.setLayout(new RatioLayout());
	trash3.setHorizontalAlignment(JLabel.CENTER);
	pressurePanel_sub1.add("0.01,0.01;0.25,0.99",pressureSensor[1].label);
	pressurePanel_sub1.add("0.26,0.01;0.40,0.99",pressureSensor[1].temp);
	pressurePanel_sub1.add("0.70,0.01;0.29,0.99",pressureSensor[1].unitSelect);
	//pressurePanel_sub1.add("0.85,0.01;0.14,0.99",pressureSensor[1].graphElement.showme);
	pressurePanel.add(trash3);
	pressurePanel.add(pressurePanel_sub0);
	pressurePanel.add(pressurePanel_sub1);


	//this.add("0.01,0.02;0.20,0.60", tcPanel);
	this.add("0.01,0.02;0.40,0.40",ls332aPanel);
	this.add("0.01,0.45;0.40,0.40",ls332bPanel);
	this.add("0.45,0.02;0.25,0.20",ls332LockPanel);
	this.add(EPICSApplyButton.consistantLayout,new EPICSApplyButton());
	this.add("0.72,0.65;0.27,0.15", pressurePanel);
	this.add("0.72,0.02;0.27,0.30", tmPanelMOSDewar);
	this.add("0.72,0.32;0.27,0.30", tmPanelCamera);
	this.add(FJECSubSystemPanel.consistantLayout, new FJECSubSystemPanel("ec:"));
    
	GregorianCalendar gc = new GregorianCalendar();
	td_filename = "fjec_temperature_graph_log_"+gc.get(Calendar.MONTH) + "_"+gc.get(Calendar.DAY_OF_MONTH) + "_"+gc.get(Calendar.YEAR);
 
    } //end of jbInit




    public void printLine(TextFile outFile, String line) {
	if (dataOutputFormat.equals("CSV")) {
	    outFile.println(line);
	} else if (dataOutputFormat.equals("XML")) {
	    outFile.println(" <row>");
	    int count = 0;
	    StringTokenizer st = new StringTokenizer(line,",");
	    while (st.hasMoreTokens()) {
		String s = st.nextToken();
		outFile.println("  <Col"+count+">"+s+"</Col"+(count++)+">");
	    }
	    outFile.println(" </row>");
	} else {
	    System.err.println("JPanelTemperature.printLine> Unknown data output format: "+dataOutputFormat);
	}
    }
    public void printHeader(TextFile outFile) {
	if (dataOutputFormat.equals("CSV")) {
	    
	} else if (dataOutputFormat.equals("XML")) {
	    outFile.println("<document>");
	} else {
	    System.err.println("JPanelTemperature.printHeader> Unknown data output format: "+dataOutputFormat);
	}
    }
    public void printFooter(TextFile outFile) {
	if (dataOutputFormat.equals("CSV")) {

	} else if (dataOutputFormat.equals("XML")) {
	    outFile.println("</document>");
	} else {
	    System.err.println("JPanelTemperature.printFooter> Unknown data output format: "+dataOutputFormat);	    
	}
    }


    //===============================================================================
    /**
     *TBD
     */
    //-------------------------------------------------------------------------------
    /**
     *Reads the temperature parameter file
     *@param filename String: contains the name of the file name used
     */

    public void ReadFile(String filename) {

	TextFile in_file = new TextFile(filename,TextFile.IN);

	try {
	    String line;
	    StringTokenizer st;
	    line = in_file.nextUncommentedLine();
	    int numSensors=0; int maxLabelSize = 0;
	    try {  numSensors = Integer.parseInt(line);
	    sensorParms = new TemperatureSensorParameter[numSensors]; }
	    catch (Exception e) {fjecError.show("Bad number of sensors parameter"); }
	    for (int i=0; i<numSensors; i++) {
		line = in_file.nextUncommentedLine();
		st = new StringTokenizer(line," \t");
		// assume that all words are part of the sensor name
		// except the last three, which should be the epics rec name and Hot/Cold Threshold Values
		int numTokens = st.countTokens();
		String s = "";
		for (int j=0; j<numTokens-3; j++)
		    s += st.nextToken() + " ";
		s = s.substring(0,s.length()-1); // strip off the last space
		if (s.length() > maxLabelSize) maxLabelSize = s.length();
		sensorParms[i] = new TemperatureSensorParameter (s,st.nextToken());
		try { sensorParms[i].hotThreshold = Double.parseDouble(st.nextToken()); }
		catch (Exception e) { fjecError.show("Bad Hot Threshold Value: "+e.toString()); }
		try { sensorParms[i].coldThreshold = Double.parseDouble(st.nextToken()); }
		catch (Exception e) { fjecError.show("Bad Cold Threshold Value: "+e.toString()); }
		sensorParms[i].checkThreshold();
	    }
	    // make label spacing uniform
	    for (int i=0; i<numSensors; i++) {
		String s = sensorParms[i].label.getText();
		while (s.length() < maxLabelSize) s += " ";
		sensorParms[i].label.setText(s);
	    }

	    pressureSensor = new TemperatureSensorParameter[2];
	    String[] pressUnits = {"Torr","mTorr","microTorr"};
	    float[] pressFactors = {1, 1000, 1000000};
	    pressureSensor[0] = new TemperatureSensorParameter("Camera","sad:CAMVAC", pressUnits, pressFactors);
	    //pressureSensor[0].units.setText("Torr");
	    pressureSensor[1] = new TemperatureSensorParameter("MOS","sad:MOSVAC", pressUnits, pressFactors);
	    //pressureSensor[1].units.setText("Torr");

	    detectorSensor = new TemperatureSensorParameter[2];
	    detectorSensor[0] = new TemperatureSensorParameter("CAM A Setp","sad:CAMABENC");
	    detectorSensor[1] = new TemperatureSensorParameter("CAM B Setp","sad:CAMBDETC");
      
	    graphSensorParams = new TemperatureSensorParameter[sensorParms.length+pressureSensor.length+detectorSensor.length];
      
	    int j = 0;
	    for (int i=0; i<detectorSensor.length; i++) graphSensorParams[j++] = detectorSensor[i];
	    for (int i=0; i<sensorParms.length; i++) graphSensorParams[j++] = sensorParms[i];
	    for (int i=0; i<pressureSensor.length; i++) graphSensorParams[j++] = pressureSensor[i];


	    in_file.close();
	}
	catch (Exception e) {
	    fjecError.show("Couldn't read temperature data file: "+e.toString());
	}
    } //end of ReadFile
} //end of class JPanelTemperature






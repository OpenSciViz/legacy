package uffjec;
import java.awt.event.*;
import javax.swing.*;
import java.awt.*;
import javaUFLib.*;

public class JPanelMotorStatus extends JPanel{
    
	static final long serialVersionUID = 0; 

    FJECMotor [] motors;

    public JPanelMotorStatus(FJECMotor [] fjecmotors){
	motors = fjecmotors;
	JPanel internalPanel = new JPanel();
	internalPanel.setLayout(new GridLayout(0,1));
	internalPanel.add(FJECMotor.getMotorStatusLabelPanel());
	for (int i=0; i<fjecmotors.length; i++) {
	    internalPanel.add(fjecmotors[i].getMotorStatusPanel());	  
	    //JPanel j = new JPanel();
	    //final FJECMotor [] jcm = fjecmotors;
	    //final int k = i;
	    //j.add(new JButton("Connect to Agent") {
	    //  public void actionPerformed(ActionEvent ae) {
	    //      jcm[k].connect();
	    //  }
	    //  });
	    //add(j);
	}
	//EPICSApplyButton originAllButton = new EPICSApplyButton("Org All",EPICS.prefix+"cc:setup.Origin","All",false,EPICSApplyButton.COLOR_SCHEME_GREEN);
        EPICSApplyButton originAllButton = new EPICSApplyButton("Org All",EPICS.recs.get(EPICSRecs.setupOrigin),"All",false,EPICSApplyButton.COLOR_SCHEME_GREEN);


	//EPICSApplyButton statusAllButton = new EPICSApplyButton("Stat All",EPICS.prefix+"cc:setup.Status","All",false,EPICSApplyButton.COLOR_SCHEME_GREEN);

	UFColorButton statusAllButton = new UFColorButton("Stat All",UFColorButton.COLOR_SCHEME_GREEN);
	statusAllButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    statusAll();
		}
	    });

	//EPICSApplyButton abortAllButton = new EPICSApplyButton("Abort all",EPICS.prefix+"cc:abort.All",EPICS.MARK+"",true,EPICSApplyButton.COLOR_SCHEME_RED);
        EPICSApplyButton abortAllButton = new EPICSApplyButton("Abort all",EPICS.recs.get(EPICSRecs.abortAll),EPICS.MARK+"",true,EPICSApplyButton.COLOR_SCHEME_RED);

	setLayout(new RatioLayout());
	add("0.01,0.01;0.99,0.80",internalPanel);
	add("0.70,0.82;0.08,0.08",originAllButton);
	add("0.80,0.82;0.08,0.08",statusAllButton);
	add("0.90,0.82;0.10,0.16",abortAllButton);
	add(EPICSApplyButton.consistantLayout,new EPICSApplyButton());
	add(FJECSubSystemPanel.consistantLayout,new FJECSubSystemPanel("cc:"));
	

    }

    public void statusAll() {
	for (int i=0; i<motors.length; i++) {
	    //EPICS.put(EPICS.prefix+"cc:"+"test."+motors[i].getEpicsName(),EPICS.MARK);
            EPICS.put(EPICS.recs.get(motors[i].getEpicsName()+"Status"),EPICS.MARK);
	    //motors[i].doStatusClick();
	}
	//EPICS.put(EPICS.prefix+"apply.DIR",EPICS.START);
        EPICS.put(EPICS.recs.get(EPICSRecs.apply),EPICS.START);
    }

}



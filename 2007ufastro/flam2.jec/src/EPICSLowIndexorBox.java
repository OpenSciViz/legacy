package uffjec;

import ufjca.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
//===============================================================================
/**
 * Handles text fields related to EPICS
 */
public class EPICSLowIndexorBox extends JComboBox implements UFCAToolkit.MonitorListener,FocusListener, KeyListener, ItemListener,ActionListener {

	static final long serialVersionUID = 0;
	
    String putRec;
    String name;
    String monVal;
    int oldSelIdx;

    public EPICSLowIndexorBox(String epicsMotorName, String stepRecname) {
	//System.out.println(stepRecname+"Hiup");
	putRec = stepRecname;
	name = epicsMotorName;
	monVal = "null";
	oldSelIdx = 0;
	addItem(new StringBuffer(""));
	addItem("Datum");
	addItem("Park");
	setEditable(true);
	setSelectedIndex(0);
	if (!EPICS.addMonitor(putRec,this))
	    setBackground(Color.red);
	//removeActionListener(this);
	addActionListener(this);
	//addFocusListener(this);
	//addItemListener(this);
	//addKeyListener(this);
    }

    public void monitorChanged(String val) {
	StringBuffer recVal = (StringBuffer) getItemAt(0);
	recVal.delete(0,recVal.length());
	recVal.append(val);
	
	if (getSelectedIndex()==0)
	    getEditor().setItem(recVal);
	    //else{
	    //removeItemAt(0);
	    //insertItemAt(new String(val),0);
	    //}	
	updateUI();
	setToolTipText(val);
	monVal = val;
    }

    public void reconnect(String dbname) {
	EPICS.removeMonitor(putRec,this);
	putRec = dbname + putRec.substring(putRec.indexOf(":")+1);
	EPICS.addMonitor(putRec,this);
    }


    public void setToolTipText(String outVal){
	super.setToolTipText( putRec +  " = " + outVal );	
    }

    public void itemStateChanged(ItemEvent ie) {
    }

    public void actionPerformed(ActionEvent ae) {
	//int idx = getSelectedIndex();
	//super.actionPerformed(ae);
	//setSelectedIndex(idx);
	//grabFocus();
	if (getSelectedIndex() == 0){
	    if (oldSelIdx != 0) {
		setEditable(true);
		EPICS.put(EPICS.prefix+"cc:datum.DIR",EPICS.CLEAR);
	    } else {
		System.out.println("EPICSLowIndexorBox.actionPerformed> User pressed enter?");
		keyTyped(new KeyEvent(this,0,0,0,KeyEvent.VK_ENTER,'\n'));
	    }
	} else if (getSelectedIndex() == 1) {
	    if (oldSelIdx != 1) {
		setEditable(false);
		//EPICS.put(EPICS.prefix+"cc:setup.DIR",EPICS.CLEAR);
		EPICS.put(EPICS.recs.get(EPICSRecs.ccSetup), EPICS.CLEAR);
	    }
	    EPICS.put(EPICS.prefix+"cc:datum."+name,EPICS.PRESET);
	} else if (getSelectedIndex() == 2) {
	    setEditable(false);
	} else {
	    System.err.println("EPICSLowIndexorBox.actionPerformed> Impossible! Bad selected index value! :"+getSelectedIndex()+"Prepare for the apocolypse!");
	}
	oldSelIdx = getSelectedIndex();
    }

    public void focusGained(FocusEvent fe) {
	System.out.println("There?");
    }

    public void focusLost(FocusEvent fe) {
	System.out.println("Here?");
	//String str = ((StringBuffer)getItemAt(0)).toString();
	//if (!str.equals(monVal))
	//  setBackground(EPICSTextField.CHANGED_COLOR);
    }

    public void keyReleased(KeyEvent ke) {
	
    }

    public void keyPressed(KeyEvent ke) {
	System.out.println("And Everywhere?");
    }

    public void keyTyped(KeyEvent ke) {
	if (getSelectedIndex()==0 && ke.getKeyCode() == KeyEvent.VK_ENTER) {
	    StringBuffer s = (StringBuffer)getItemAt(0);
	    String str = s.toString();
	    if (getEditor().getItem() != null) {
		if (getEditor().getItem() instanceof StringBuffer)
		    str = ((StringBuffer) getEditor().getItem()).toString();
		else if (getEditor().getItem() instanceof String) 
		    str = ((String) getEditor().getItem()).toString();
		else 
		    System.err.println("EPICSLowIndexorBox.keyTyped> Unknown object in combobox editor.");
	    }
	    
	    s.delete(0,s.length());
	    //getEditor().setItem(s);
	    EPICS.put(putRec,str);
	}else
	    System.out.println("EPICSLowIndexorBox.keyTyped> selected Index: "+getSelectedIndex());
    }

}

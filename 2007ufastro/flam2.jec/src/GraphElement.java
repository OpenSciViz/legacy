package uffjec;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

//===============================================================================
   /**
    *TBD
    */
   public class GraphElement {
     JTextField rate = new JTextField ("0",5);
     JLabel units = new JLabel ("K/min");
     JCheckBox showme = new JCheckBox ();

//-------------------------------------------------------------------------------
     /**
      *GraphElement constructor
      */
     GraphElement () {
       showme.setSelected(false);
       rate.setEditable(false);
       rate.setBackground(new Color(175,175,175));
       showme.addActionListener(
           new java.awt.event.ActionListener() {
              public void actionPerformed(ActionEvent e) {
		  fjecFrame.jPanelTemperature.repaint();
           }
       });

     } //end of GraphElement

   } //end of class GraphElement


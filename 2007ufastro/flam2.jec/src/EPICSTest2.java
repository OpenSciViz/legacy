package uffjec;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Vector;


public class EPICSTest2 implements gov.aps.jca.MonitorListener{

    JTextField recField;
    JTextField valField;
    JButton putButton;
    JButton getButton;
    JButton monButton;
    final double TIMEOUT = 5.0;
    Vector monVec;
    gov.aps.jca.Context ctxt; 
    gov.aps.jca.Channel ch;
    Monitor mon ;
    Thread pollThread;

    public EPICSTest2() {
	JFrame j = new JFrame();
	JPanel p = new JPanel();
	monVec = new Vector();
	recField = new JTextField("jane",10);
	valField = new JTextField("",10);
	putButton = new JButton("Put");
	getButton = new JButton("Get");
	monButton = new JButton("Mon");
	p.add(recField); p.add(valField);p.add(putButton); p.add(getButton); 
	p.add(monButton);
	j.getContentPane().add(p);
	j.setSize(400,400);
	j.setLocation(100,100);
	j.setVisible(true);
	j.setDefaultCloseOperation(3);
	try {
	ctxt = Ca.createContext("MyContext",Ca.ENABLE_PREEMPTIVE_CALLBACK, Ca.THREAD_CTRL_INTERNAL);
	} catch (Exception e) {
	    System.err.println(e.toString());
	    System.exit(-1);
	}
	putButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    put(recField.getText(),valField.getText());
		}
	    });
	getButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    valField.setText(get(recField.getText()));
		}
	    });
	monButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    addMonitor(recField.getText());
		}
	    });
	
    }

    public void addMonitor(String rec) {
	try {
	    gov.aps.jca.Channel ch = ctxt.createChannel(recField.getText());
	    ctxt.pendIO(TIMEOUT);
	    mon = ch.addMonitorListener(this,Ca.DBE_VALUE);
	    ctxt.flushIO();
	    //ch.destroy();
	} catch (Exception ex) {System.err.println(ex.toString()); }
    }

    public void monitorChanged(MonitorEvent event) {
	try {
	    DBR dbr = event.getDBR();
	    dbr.printInfo();
	    //System.out.print("Monitor occured!!");
	    //System.out.print(event.pv().name());
	    //System.out.println(event.pv().value());
	}
	catch (Exception e) {System.out.println(e.toString()); }
	//System.out.println(event.pv().name());
    }
    
    public void put(String rec, String val) {
	try {
	    //ctxt.attachCurrentThread();
	    Channel c = ctxt.createChannel(rec);
	    ctxt.pendIO(TIMEOUT);
	    c.put(val);
	    ctxt.pendIO(TIMEOUT);
	    c.destroy();
	}
	catch (Exception exp) {
	    //	    fjecError.show(exp.toString() + " " + rec);
	    System.err.println(exp.toString() + " " + rec);
	}
    } //end of put

    public synchronized String get(String rec) {
	String val = "";
	try {
	    Channel c = ctxt.createChannel(rec);
	    ctxt.pendIO(TIMEOUT);
	    DBR_String theDBR = (DBR_String) c.get();
	    ctxt.pendIO(TIMEOUT);
	    val = theDBR.getStringValue()[0];
	    c.destroy();
	}
	catch (Exception exp) {
	    //fjecError.show(exp.toString() + " " + rec);
	    System.err.println(exp.toString() + " " + rec);
	}
	return val;
    } //end of get
    
    public static void main(String [] args) {
	new EPICSTest2();
    }

}

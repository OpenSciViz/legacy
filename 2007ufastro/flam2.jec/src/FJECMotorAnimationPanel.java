package uffjec;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class FJECMotorAnimationPanel extends JPanel {
    
    static final long serialVersionUID = 0;
    
    String [] posNames;
    int [] posSteps;
    int [] xVals;
    int [] yVals;
    int totalSteps;
    int currentStep;
    int oldMouseX = -1, oldMouseY = -1;

    int red,green,blue;

    public FJECMotorAnimationPanel(String [] posiNames, int [] posiSteps,
				   int totSteps) {
	posNames = posiNames;
	posSteps = posiSteps;
	xVals = new int[posSteps.length];
	yVals = new int[posSteps.length];
	totalSteps = totSteps;
	currentStep = red = green = blue = 0;
	addMouseListener(new MouseListener() {
		public void mouseReleased(MouseEvent me) {

		}
		public void mousePressed(MouseEvent me) {}
		public void mouseExited(MouseEvent me) {}
		public void mouseEntered(MouseEvent me) {}
		public void mouseClicked(MouseEvent me) {}
	    });
	addMouseMotionListener(new MouseMotionListener() {
		public void mouseDragged(MouseEvent me) {
		    /*
		    boolean up = me.getY() < oldMouseY-10;
		    boolean left = me.getX() < oldMouseX-10;
		    boolean down = me.getY() > oldMouseY+10;
		    boolean right = me.getX() > oldMouseX+10;
		    if (oldMouseY != -1 && oldMouseX != -1) {
			if (currentStep <= totalSteps/4 ||
			    currentStep >= totalSteps*3/4){ 
			    if (left){ System.out.println("Left");}
			    else { System.out.println("Right");}
			}else if (currentStep <= totalSteps) {

			}
		    }
		    oldMouseY = me.getY();
		    oldMouseX = me.getX();
		    */
		}

		public void mouseMoved(MouseEvent me) {
		    setToolTipText(null);
		    String toolText = "<HTML>";
		    for (int i=0; i<xVals.length; i++)
			if (me.getX() < xVals[i]+3 && me.getX() > xVals[i]-3 && me.getY() < yVals[i]+3 && me.getY() > yVals[i]-3)
			    toolText+= posNames[i]+":"+posSteps[i]+"<br>";
		    if (!toolText.equals("<HTML>")) {
			toolText = toolText.substring(0,toolText.length()-4);
			toolText += "</HTML>";
			setToolTipText(toolText);
		    }
		}
	    });
    }

    public void setCurrentStep(int newStep) {
	currentStep = newStep;
	repaint();
    }

    public void paint(Graphics g) {
	super.paint(g);
	Color oldC = g.getColor();
	g.setColor(new Color(red,green,blue));
	int sp = 3;
	g.drawOval(sp,sp,getWidth()-sp-1,getHeight()-sp-1);
	double radx = (getWidth()-sp-1)/2.0;
	double rady = (getHeight()-sp-1)/2.0;
	for (int i=0; i<posSteps.length; i++) {
	    int x=(int)(Math.sin(Math.PI*2.0/totalSteps*posSteps[i])*radx+radx+sp);
	    int y=(int)((-Math.cos(Math.PI*2.0/totalSteps*posSteps[i]))*rady+rady+sp);
	    g.drawOval(x-2,y-2,4,4);
	    xVals[i] = x; yVals[i] = y;
	}
	g.drawLine((int)(radx+sp),(int)(rady+sp),(int)(Math.sin(Math.PI*2.0/totalSteps*currentStep)*radx+radx+sp),(int)(-Math.cos(Math.PI*2.0/totalSteps*currentStep)*rady+rady+sp));
	g.setColor(oldC);
    }


    public void setRed(int boolVal) {
	red = boolVal * 255;
	if (red > 255) red = 255;
	repaint();
    }

    public void setGreen(int boolVal) {
	green = boolVal * 255;
	if (green > 255) green = 255;
	repaint();
    }

    public void setBlue(int boolVal) {
	blue = boolVal * 255;
	if (blue > 255) blue = 255;
	repaint();
    }


    public static void main (String [] args) {
	JFrame j = new JFrame();
	String [] s = {"Pos1","Pos2","Pos3","Pos4","Pos5","Pos6"};
	int [] i = {0,100,200,300,400,500};
	FJECMotorAnimationPanel map = new FJECMotorAnimationPanel(s,i,600);
	j.getContentPane().add(map);
	j.setVisible(true);
	j.setDefaultCloseOperation(3);
	j.setSize(100,100);
	j.setLocation(100,100);
	for (int k=0; k<i.length; k++) {
	    map.setCurrentStep(i[k]);
	    map.repaint();
	    try { Thread.sleep(1000);}
	    catch (Exception e) { System.err.println(e.toString());}
	}
    }
}

package uffjec;
import java.awt.event.*;
import javax.swing.*;
import java.awt.*;

//===============================================================================
/**
 *Low Level tabbed pane
 */
public class JPanelMotorLowLevel extends JPanel{
	static final long serialVersionUID = 0;
	
	//-------------------------------------------------------------------------------
    /**
     *Default Constructor
     *@param fjecmotors FJECMotor[]: Array of FJECMotors 
     */
    public JPanelMotorLowLevel(FJECMotor [] fjecmotors) {
	setLayout(new RatioLayout());
	JPanel singleMotorPanel = new JPanel();
	singleMotorPanel.setLayout(new GridLayout(0,1));
	singleMotorPanel.add(FJECMotor.getMotorLowLevelLabelPanel());
	for (int i=0; i<fjecmotors.length; i++) singleMotorPanel.add(fjecmotors[i].getMotorLowLevelPanel());
	//singleMotorPanel.add(new FJECSubSystemPanel("cc:"));
	this.add("0.01,0.01;0.99,0.80", singleMotorPanel);
	this.add(EPICSApplyButton.consistantLayout,new EPICSApplyButton());
	this.add(FJECSubSystemPanel.consistantLayout, new FJECSubSystemPanel("cc:"));

	//EPICSApplyButton abortAllButton = new EPICSApplyButton("Abort all",EPICS.prefix+"cc:abort.All",EPICS.MARK+"",true,EPICSApplyButton.COLOR_SCHEME_RED);
        EPICSApplyButton abortAllButton = new EPICSApplyButton("Abort all",EPICS.recs.get(EPICSRecs.abortAll),EPICS.MARK+"",true,EPICSApplyButton.COLOR_SCHEME_RED);


// 	JButton cheatButton = new JButton("CAR Cheat");
// 	cheatButton.addActionListener(new ActionListener(){
// 		public void actionPerformed(ActionEvent ae) {
// 		    EPICS.put(EPICS.prefix+"apply.DIR",EPICS.CLEAR);
// 		    //EPICS.put(EPICS.prefix+"cc:setupBarCdC",EPICS.IDLE);
// 		    //EPICS.put(EPICS.prefix+"cc:setupC",EPICS.IDLE);
// 		    //EPICS.put(EPICS.prefix+"applyC",EPICS.IDLE);
// 		}
// 	    });
	add("0.90,0.82;0.10,0.16",abortAllButton);
// 	add("0.80,0.90;0.10,0.08",cheatButton);
    }

    
}

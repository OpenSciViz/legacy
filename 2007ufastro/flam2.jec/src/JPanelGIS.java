package uffjec;

import javax.swing.*;
import javax.swing.border.*;
import ufjca.*;

public class JPanelGIS extends JPanel {

    static final long serialVersionUID = 0;
	
    public JPanelGIS() {
	setLayout(new RatioLayout());
	JPanel inputPanel = new JPanel();
	inputPanel.setLayout(new RatioLayout());
	inputPanel.setBorder(new EtchedBorder(0));
	JPanel outputPanel = new JPanel();
	outputPanel.setLayout(new RatioLayout());
	outputPanel.setBorder(new EtchedBorder(0));
	for (int i=1; i<21; i++) {
	    inputPanel.add("0.01,"+(0.05*(i-1)) +";0.50,0.05",new JLabel("Input Line "+(i<10?"0"+i:""+i)));
	    inputPanel.add("0.80,"+(0.05*(i-1))+";0.20,0.05",new EPICSLabel(EPICS.prefix+"sad::gis.inp"+(i<10?"0"+i:""+i)+"line",""));	    
	}
	for (int i=1; i<5; i++) {
	    outputPanel.add("0.01,"+(0.25*(i-1)) +";0.50,0.25",new JLabel("Output Line 0"+i));
	    outputPanel.add("0.80,"+(0.25*(i-1))+";0.20,0.25",new EPICSLabel(EPICS.prefix+"sad::gis.out0"+i+"line",""));
	}	
	add("0.01,0.01;0.50,0.99",inputPanel);
	add("0.60,0.60;0.40,0.20",outputPanel);
    }

}

package uffjec;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
  
/** 
 *Main motor class -- contains all visual components as
 *well as parameters associated with a motor
 */ 
public class FJECMotor  {

    // Visual Components
    JCheckBox hiNameCheckBox; // for use in high & low level panels
    JCheckBox loNameCheckBox;
    JCheckBox statusNameCheckBox;
    JCheckBox hiBacklashCheckBox;
    JCheckBox loBacklashCheckBox;
    String lastBacklash;
    JLabel nameLabel; // for use in motor parameter panel
    JLabel datumNameLabel;
    JLabel hiStatusLabel;
    JLabel loStatusLabel;
    JLabel statusStatusLabel;
    EPICSLabel hiLastCommand;
    EPICSLabel loLastCommand;
    EPICSComboBox namedLocationBox;
    EPICSApplyButton loAbortButton;
    EPICSApplyButton hiAbortButton;
    EPICSApplyButton statusAbortButton;
    JRadioButton loDatumButton;
    JRadioButton hiDatumButton;
    EPICSApplyButton originButton;
    JRadioButton loParkButton;
    JRadioButton hiParkButton;
    EPICSApplyButton statusButton;

    EPICSTextField stmField; // Steps to move (low level)
    EPICSTextField isField; // Initial speed
    EPICSTextField tsField; // terminal speed
    EPICSTextField accField; // acceleration
    EPICSTextField decField; // deceleration
    EPICSTextField dcField; // drive current
    EPICSTextField hcField; //hold current
    EPICSTextField dsField; // datum speed
    EPICSTextField blField; // back lash
    EPICSComboBox datumDirBox; // datum direction

    EPICSTextField disField; // Initial speed
    EPICSTextField disvField; //InitSlow
    EPICSTextField dsvField; //Slow
    EPICSTextField ddsField; // terminal speed
    EPICSTextField daccField; // acceleration
    EPICSTextField ddecField; // deceleration
    EPICSTextField ddcField; // drive current
    EPICSTextField dhcField; //hold current
    EPICSTextField dblField; // back lash

    EPICSLabel isLabel;
    EPICSLabel tsLabel;
    EPICSLabel accLabel;
    EPICSLabel decLabel;
    EPICSLabel dcLabel;
    EPICSLabel hcLabel;
    EPICSLabel dsLabel;
    EPICSLabel blLabel;
    EPICSLabel datumDirLabel;

    EPICSLabel disLabel;
    EPICSLabel disvLabel;
    EPICSLabel dsvLabel;
    EPICSLabel ddsLabel;
    EPICSLabel daccLabel;
    EPICSLabel ddecLabel;
    EPICSLabel ddcLabel;
    EPICSLabel dhcLabel;
    EPICSLabel dblLabel;

    EPICSLabel isSadLabel;
    EPICSLabel tsSadLabel;
    EPICSLabel accSadLabel;
    EPICSLabel decSadLabel;
    EPICSLabel rcSadLabel;
    
    String name;
    String epicsName;
    String [] posNames;
    float [] posValues;
    float [] sortValues;
    String [] sortNames;
    String sadRec;
    FJECMotorAnimationPanel highAnimation;
    FJECMotorAnimationPanel lowAnimation;
    FJECMotorAnimationPanel statusAnimation;
    
    int CAR_FLAG;
    public static final int APPLYC = 1;
    public static final int SETUPC = 2;
    public static final int DATUMC = 4;
    public static final int INITC = 8;
    public static final int MOTORC = 16;

    /**
     *  bubble sort (horrible complexity, but honestly, who cares? its only like 9 records or so
     */

    public void bubbleSort() {
	if (posValues.length != posNames.length) {
	    System.err.println("FJECMotor.bubbleSort> number of position names != number of position values!");
	    return;
	}
	for (int junk=0; junk < posValues.length; junk++)
	    for (int i=0; i<posValues.length-1; i++) {
		if (posValues[i] > posValues[i+1]) {
		    float tempF; String tempS;
		    tempF = posValues[i]; posValues[i] = posValues[i+1]; posValues[i+1] = tempF;
		    tempS = posNames[i]; posNames[i] = posNames[i+1]; posNames[i+1] = tempS;
		}
	    }
    }
    

    ///////////////
    //Constructors//
    ///////////////

    public Object clone() {
	String emn = new String(epicsName);
	String hmn = new String(name);
	String [] posn = new String[posNames.length];
	float [] posv = new float[posValues.length];
	for (int i=0; i<posNames.length; i++) posn[i] = new String(posNames[i]);
	for (int i=0; i<posValues.length; i++) posv[i] = posValues[i];
	return new FJECMotor(emn,hmn,posn,posv);
    }

    public FJECMotor(String epicsMotName, String humanMotName, String [] posnames) {
	CAR_FLAG = 0;
	name = new String(humanMotName);
	epicsName = new String(epicsMotName);
	posNames = new String[posnames.length];
	posValues = new float[posnames.length];
	for (int i=0; i<posnames.length; i++) {posNames[i] = new String(posnames[i]);posValues[i]=i*100;}
	setupVisualComponents();
    }

    public FJECMotor(String epicsMotName, String humanMotName, String [] posnames, float [] posvalues) {
	CAR_FLAG = 0;
	name = new String(humanMotName);
	epicsName = new String(epicsMotName);
	if (posnames.length != posvalues.length) {
	    System.err.println("FJECMotor.FJECMotor> number of position names != number of position values! NOT creating motor "+humanMotName);
	    return;
	}
	posNames = new String[posnames.length];
	posValues = new float[posvalues.length];
	for (int i=0; i<posnames.length; i++) posNames[i] = new String(posnames[i]);
	for (int i=0; i<posvalues.length;i++) posValues[i]= posvalues[i];
	bubbleSort();
	setupVisualComponents();
    }

    public FJECMotor(String epicsMotName, String humanMotName) {
	CAR_FLAG = 0;
	//stmField = new EPICSTextField(prefix + "setup." + epicsMotName,"No Description");
	name = new String(humanMotName);
	epicsName = new String(epicsMotName);
	posNames = new String[10];
	posValues = new float[10];
	for (int i=0; i<10; i++) { posNames[i] = "Pos"+i; posValues[i] = i*100;}
	setupVisualComponents();
    }
    public FJECMotor(String motName) { this (motName,motName); }
    /**
     *Construcor helper function creates and initializes visual components
     *associated with this motor (ie, JButtons, JTextFields, etc) and sets up
     *action methods associated with button presses.
     */
    protected void setupVisualComponents() {
	hiBacklashCheckBox = new JCheckBox("",true);
	hiBacklashCheckBox.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    loBacklashCheckBox.setSelected(hiBacklashCheckBox.isSelected());
		    if (hiBacklashCheckBox.isSelected()) {
			if (lastBacklash != null)
			    EPICS.put(EPICS.recs.get(epicsName+"Backlash"), lastBacklash);
			    //EPICS.put(EPICS.prefix+"cc:" + "setup." + epicsName + "Backlash",lastBacklash);
		    } else {
                        lastBacklash = EPICS.get(EPICS.recs.get(epicsName+"Backlash"));
                        EPICS.put(EPICS.recs.get(epicsName+"Backlash"), "0");
			//lastBacklash = EPICS.get(EPICS.prefix+"cc:" + "setup." + epicsName + "Backlash");
			//EPICS.put(EPICS.prefix+"cc:" + "setup." + epicsName + "Backlash","0");
		    }
		}
	    });
	loBacklashCheckBox = new JCheckBox("",true);
	loBacklashCheckBox.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    hiBacklashCheckBox.setSelected(loBacklashCheckBox.isSelected());
		    if (loBacklashCheckBox.isSelected()) {
			if (lastBacklash != null) {
			    EPICS.put(EPICS.recs.get(epicsName+"Backlash"), lastBacklash);
/*
			    if (EPICS.PORTABLE)
				EPICS.put(EPICS.prefix+"cc:" + "setup." + epicsName + "Backlash",lastBacklash);
			    else
				EPICS.put(EPICS.prefix+"cc:" + epicsName + ":motorG.L",lastBacklash);
*/
			}
		    } else {
			lastBacklash = EPICS.get(EPICS.recs.get(epicsName+"Backlash"));
			EPICS.put(EPICS.recs.get(epicsName+"Backlash"), "0");
/*
			if (EPICS.PORTABLE) {
			    lastBacklash = EPICS.get(EPICS.prefix+"cc:" + "setup." + epicsName + "Backlash");
			    EPICS.put(EPICS.prefix+"cc:" + "setup." + epicsName + "Backlash","0");
			} else {
			    lastBacklash = EPICS.get(EPICS.prefix+"cc:" + epicsName + ":motorG.L");
			    EPICS.put(EPICS.prefix+"cc:" + epicsName + ":motorG.L","0");
			}
*/
		    }
		}
	    });
	hiStatusLabel = new JLabel();
	loStatusLabel = new JLabel();
	statusStatusLabel = new JLabel();
	hiStatusLabel.setHorizontalAlignment(JLabel.RIGHT);
	loStatusLabel.setHorizontalAlignment(JLabel.RIGHT);
	statusStatusLabel.setHorizontalAlignment(JLabel.RIGHT);
	hiStatusLabel.setBorder(new BevelBorder(BevelBorder.LOWERED));
	loStatusLabel.setBorder(new BevelBorder(BevelBorder.LOWERED));
	statusStatusLabel.setBorder(new BevelBorder(BevelBorder.LOWERED));

	hiNameCheckBox = new JCheckBox(name,true);
	loNameCheckBox = new JCheckBox(name,true);
	statusNameCheckBox = new JCheckBox(name,true);
	nameLabel = new JLabel(name);
        datumNameLabel = new JLabel(name);
	//String [] tmp1 = {"PosA","PosB","PosC","PosD","PosE","PosF"};
	int numPositions = posNames.length+1;
	for (int i=0; i<posNames.length; i++) 
	    if (posNames[i].toLowerCase().trim().equals("datum") || posNames[i].toLowerCase().trim().equals("park")) 
		numPositions--;
	String [] tmp1 = new String[numPositions];
	int [] displayedValues = new int[numPositions-1];
	String [] displayedNames = new String[numPositions-1];
	tmp1[0] = "null";
	int count = 1;
	for (int i=0; i<posNames.length; i++) 
	    if (!posNames[i].toLowerCase().trim().equals("datum") && !posNames[i].toLowerCase().trim().equals("park")) {
		displayedValues[count-1] = (int) posValues[i];
		displayedNames[count-1] = posNames[i];
		tmp1[count++] = posNames[i];
	    }
	namedLocationBox = new EPICSComboBox(epicsName+"NamedPos", tmp1, EPICSComboBox.ITEM);
	stmField = new EPICSTextField(epicsName+"Step", "", true);
	isField = new EPICSTextField(epicsName+"InitVel", "", true);
        isField.doClearOnPut(EPICSRecs.ccSetup);
        //isField.doClearOnPut(EPICS.recs.get(EPICSRecs.ccDatum));

	tsField = new EPICSTextField(epicsName+"SlewVel", "", true);
	accField = new EPICSTextField(epicsName+"Acc", "", true);
        accField.doClearOnPut(EPICSRecs.ccSetup);
        //accField.doClearOnPut(EPICS.recs.get(EPICSRecs.ccDatum));

	decField = new EPICSTextField(epicsName+"Dec", "", true);
        decField.doClearOnPut(EPICSRecs.ccSetup);
        //decField.doClearOnPut(EPICS.recs.get(EPICSRecs.ccDatum));

	dcField = new EPICSTextField(epicsName+"RunCur", "", true);
        dcField.doClearOnPut(EPICSRecs.ccSetup);
        //dcField.doClearOnPut(EPICS.recs.get(EPICSRecs.ccDatum));

        hcField = new EPICSTextField(epicsName+"HoldCur", "", true);
        hcField.doClearOnPut(EPICSRecs.ccSetup);

	dsField = new EPICSTextField(epicsName+"DatumVel", "", true);
	isLabel = new EPICSLabel(epicsName+"VALInitVel", "");

	tsLabel = new EPICSLabel(epicsName+"VALSlewVel", "");
	accLabel = new EPICSLabel(epicsName+"VALAcc", "");

	decLabel = new EPICSLabel(epicsName+"VALDec", "");

	dcLabel = new EPICSLabel(epicsName+"VALRunCur", "");
        hcLabel = new EPICSLabel(epicsName+"VALRunCur", "");

	dsLabel = new EPICSLabel(epicsName+"VALDatumVel", "");

	hiLastCommand = new EPICSLabel(epicsName+"VALNamedPos", "");
	loLastCommand = new EPICSLabel(epicsName+"VALStep", "");
        String [] tmp2 = {"0","1"};
        datumDirBox = new EPICSComboBox(epicsName+"DatumDir",tmp2,EPICSComboBox.ITEM);
        datumDirLabel = new EPICSLabel(epicsName+"VALDatumDir","");
        blField = new EPICSTextField(epicsName+"Backlash","");
        blLabel = new EPICSLabel(epicsName+"VALBacklash","");

	//Datum low-level parameters
        disField = new EPICSTextField(epicsName+"InitVelDatum", "", true);
        //disField.doClearOnPut(EPICS.recs.get(EPICSRecs.ccSetup));
        disField.doClearOnPut(EPICSRecs.ccDatum);

	//disvField = new EPICSTextField(epicsName+"InitSlowDatum", "", true);
	dsvField = new EPICSTextField(epicsName+"SlowDatum", "", true);

        ddsField = new EPICSTextField(epicsName+"DatumVel", "", true);

        daccField = new EPICSTextField(epicsName+"AccDatum", "", true);
        //daccField.doClearOnPut(EPICS.recs.get(EPICSRecs.ccSetup));
        daccField.doClearOnPut(EPICSRecs.ccDatum);

        ddecField = new EPICSTextField(epicsName+"DecDatum", "", true);
        //ddecField.doClearOnPut(EPICS.recs.get(EPICSRecs.ccSetup));
        ddecField.doClearOnPut(EPICSRecs.ccDatum);

        ddcField = new EPICSTextField(epicsName+"RunCurDatum", "", true);
        //ddcField.doClearOnPut(EPICS.recs.get(EPICSRecs.ccSetup));
        ddcField.doClearOnPut(EPICSRecs.ccDatum);

        dhcField = new EPICSTextField(epicsName+"HoldCurDatum", "", true);
        dhcField.doClearOnPut(EPICSRecs.ccDatum);

        disLabel = new EPICSLabel(epicsName+"VALInitVelDatum", "");

	//disvLabel = new EPICSLabel(epicsName+"VALInitSlowDatum", "");
        dsvLabel = new EPICSLabel(epicsName+"VALSlowDatum", "");

        ddsLabel = new EPICSLabel(epicsName+"VALDatumVel", "");
        daccLabel = new EPICSLabel(epicsName+"VALAccDatum", "");

        ddecLabel = new EPICSLabel(epicsName+"VALDecDatum", "");

        ddcLabel = new EPICSLabel(epicsName+"VALRunCurDatum", "");
        dhcLabel = new EPICSLabel(epicsName+"VALHoldCurDatum", "");

        dblField = new EPICSTextField(epicsName+"BacklashDatum","");
        dblLabel = new EPICSLabel(epicsName+"VALBacklashDatum","");




        loAbortButton = new EPICSApplyButton("Abort", EPICS.recs.get(epicsName+"Abort"),EPICS.MARK+"",true,EPICSApplyButton.COLOR_SCHEME_RED);
        hiAbortButton = new EPICSApplyButton("Abort",EPICS.recs.get(epicsName+"Abort"),EPICS.MARK+"",true,EPICSApplyButton.COLOR_SCHEME_RED);
        statusAbortButton = new EPICSApplyButton("Abort",EPICS.recs.get(epicsName+"Abort"),EPICS.MARK+"",true,EPICSApplyButton.COLOR_SCHEME_RED);
        statusButton = new EPICSApplyButton("Status",EPICS.recs.get(epicsName+"Status"),EPICS.MARK+"");
        originButton = new EPICSApplyButton("Origin",EPICS.recs.get(epicsName+"Origin"),epicsName);
        loDatumButton = new EPICSRadioButton(epicsName+"Datum");
        hiDatumButton = new EPICSRadioButton(epicsName+"Datum");

	//Shadow Records
/*
            isField.addShadowRecord(epicsName+"InitVelDatum");
            accField.addShadowRecord(epicsName+"AccDatum");
            decField.addShadowRecord(epicsName+"DecDatum");
            dcField.addShadowRecord(epicsName+"RunCurDatum");
            isLabel.addShadowRecord(epicsName+"VALInitVelDatum");
            accLabel.addShadowRecord(epicsName+"VALAccDatum");
            decLabel.addShadowRecord(epicsName+"VALDecDatum");
            dcLabel.addShadowRecord(epicsName+"VALRunCurDatum");
*/

	//if (EPICS.PORTABLE) {
	    //namedLocationBox = new EPICSComboBox(EPICS.prefix+"cc:" + "setup." + epicsName,tmp1,EPICSComboBox.ITEM);
	    //stmField = new EPICSTextField(EPICS.prefix+"cc:" + "setup." + epicsName +"Step","",true);
	    //isField = new EPICSTextField(EPICS.prefix+"cc:" + "setup." + epicsName + "InitVel","",true);
	    //isField.addShadowRecord(EPICS.prefix + "cc:datum."+epicsName+"InitVel");
	    //isField.doClearOnPut(EPICS.prefix+"cc:setup.DIR");
	    //isField.doClearOnPut(EPICS.prefix+"cc:datum.DIR");
	    // no shadow records for terminal speed, since we have seperate 
	    // fields and labels for the datum terminal speed (simply called 'datum speed' below)
	    //tsField = new EPICSTextField(EPICS.prefix+"cc:" + "setup." + epicsName + "SlewVel","",true);
	    ////tsField.addShadowRecord(EPICS.prefix + "cc:datum."+epicsName+"SlewVel");
	    //accField = new EPICSTextField(EPICS.prefix+"cc:" + "setup." + epicsName + "Acc","",true);
	    //accField.addShadowRecord(EPICS.prefix + "cc:datum."+epicsName+"Acc");
	    //accField.doClearOnPut(EPICS.prefix+"cc:setup.DIR");
	    //accField.doClearOnPut(EPICS.prefix+"cc:datum.DIR");
	    //decField = new EPICSTextField(EPICS.prefix+"cc:" + "setup." + epicsName + "Dec","",true);
	    //decField.addShadowRecord(EPICS.prefix + "cc:datum."+epicsName+"Dec");
	    //decField.doClearOnPut(EPICS.prefix+"cc:setup.DIR");
	    //decField.doClearOnPut(EPICS.prefix+"cc:datum.DIR");
	    //dcField = new EPICSTextField(EPICS.prefix+"cc:" + "setup." + epicsName + "RunCur","",true);
	    //dcField.addShadowRecord(EPICS.prefix+"cc:datum."+epicsName+"RunCur");
	    //dcField.doClearOnPut(EPICS.prefix+"cc:setup.DIR");
	    //dcField.doClearOnPut(EPICS.prefix+"cc:datum.DIR");
	    //dsField = new EPICSTextField(EPICS.prefix+"cc:" + "datum." + epicsName +"Vel","",true);
	    //isLabel = new EPICSLabel(EPICS.prefix+"cc:"+"setup.VAL"+epicsName+"InitVel","");
	    //isLabel.addShadowRecord(EPICS.prefix+"cc:datum.VAL"+epicsName+"InitVel");
	    // no shadow records for terminal speed, since we have seperate 
	    // fields and labels for the datum terminal speed (simply called 'datum speed' below)
	    //tsLabel = new EPICSLabel(EPICS.prefix+"cc:"+"setup.VAL"+epicsName+"SlewVel","");	    
	    //accLabel = new EPICSLabel(EPICS.prefix+"cc:"+"setup.VAL"+epicsName+"Acc","");
	    //accLabel.addShadowRecord(EPICS.prefix+"cc:datum.VAL"+epicsName+"Acc");
	    //decLabel = new EPICSLabel(EPICS.prefix+"cc:"+"setup.VAL"+epicsName+"Dec","");
	    //decLabel.addShadowRecord(EPICS.prefix+"cc:datum.VAL"+epicsName+"Dec");
	    //dcLabel = new EPICSLabel(EPICS.prefix+"cc:"+"setup.VAL"+epicsName+"RunCur","");
	    //dcLabel.addShadowRecord(EPICS.prefix+"cc:datum.VAL"+epicsName+"RunCur");
	    //dsLabel = new EPICSLabel(EPICS.prefix+"cc:"+"datum.VAL"+epicsName+"Vel","");
	    //hiLastCommand = new EPICSLabel(EPICS.prefix+"cc:"+"setup.VAL"+epicsName,"");
	    //loLastCommand = new EPICSLabel(EPICS.prefix+"cc:"+"setup.VAL"+epicsName+"Step","");
	    //String [] tmp2 = {"0","1"};
	    //datumDirBox = new EPICSComboBox(EPICS.prefix+"cc:" + "datum." + epicsName +"Direction",tmp2,EPICSComboBox.ITEM);
	    //datumDirLabel = new EPICSLabel(EPICS.prefix+"cc:"+"datum.VAL"+epicsName+"Direction","");
	    //blField = new EPICSTextField(EPICS.prefix+"cc:" + "setup." + epicsName + "Backlash","");
	    //blLabel = new EPICSLabel(EPICS.prefix+"cc:"+"setup.VAL"+epicsName+"Backlash","");
	    //loAbortButton = new EPICSApplyButton("Abort",EPICS.prefix+"cc:"+"abort."+epicsName,EPICS.MARK+"",true,EPICSApplyButton.COLOR_SCHEME_RED);
	    //hiAbortButton = new EPICSApplyButton("Abort",EPICS.prefix+"cc:"+"abort."+epicsName,EPICS.MARK+"",true,EPICSApplyButton.COLOR_SCHEME_RED);
	    //statusAbortButton = new EPICSApplyButton("Abort",EPICS.prefix+"cc:"+"abort."+epicsName,EPICS.MARK+"",true,EPICSApplyButton.COLOR_SCHEME_RED);
	    //statusButton = new EPICSApplyButton("Status",EPICS.prefix+"cc:"+"test."+epicsName,EPICS.MARK+"");
	    //originButton = new EPICSApplyButton("Origin",EPICS.prefix+"cc:"+"datum.Origin",epicsName);
	    //loDatumButton = new EPICSRadioButton(EPICS.prefix+"cc:datum."+epicsName);
	    //hiDatumButton = new EPICSRadioButton(EPICS.prefix+"cc:datum."+epicsName);
	//}
	/*else {
	    namedLocationBox = new EPICSComboBox(EPICS.prefix+"cc:"  + epicsName+":namedPos.A",tmp1,EPICSComboBox.ITEM);
	    stmField = new EPICSTextField(EPICS.prefix+"cc:"  + epicsName +":steps.A","",true);
	    isField = new EPICSTextField(EPICS.prefix+"cc:" + epicsName + ":motorG.N","",true);
	    tsField = new EPICSTextField(EPICS.prefix+"cc:" + epicsName + ":motorG.O","",true);
	    accField = new EPICSTextField(EPICS.prefix+"cc:" + epicsName + ":motorG.P","",true);
	    decField = new EPICSTextField(EPICS.prefix+"cc:" + epicsName + ":motorG.Q","",true);
	    dcField = new EPICSTextField(EPICS.prefix+"cc:" + epicsName + ":motorG.R","",true);
	    dsField = new EPICSTextField(EPICS.prefix+"cc:" + epicsName +":motorG.S","",true);
	    isLabel = new EPICSLabel(EPICS.prefix+"cc:"+epicsName+":motorG.VALD","");
	    tsLabel = new EPICSLabel(EPICS.prefix+"cc:"+epicsName+":motorG.VALE","");
	    accLabel = new EPICSLabel(EPICS.prefix+"cc:"+epicsName+":motorG.VALF","");
	    decLabel = new EPICSLabel(EPICS.prefix+"cc:"+epicsName+":motorG.VALG","");
	    dcLabel = new EPICSLabel(EPICS.prefix+"cc:"+epicsName+":motorG.VALH","");
	    dsLabel = new EPICSLabel(EPICS.prefix+"cc:"+epicsName+":motorG.VALI","");	    
	    
	    hiLastCommand = new EPICSLabel(EPICS.prefix+"cc:"+epicsName+":namedPos.VALA","");
	    loLastCommand = new EPICSLabel(EPICS.prefix+"cc:"+epicsName+":steps.VALA","");
	    String [] tmp2 = {"null","0","1"};
	    datumDirBox = new EPICSComboBox(EPICS.prefix+"cc:"+epicsName+":motorG.T",tmp2,EPICSComboBox.ITEM);
	    datumDirLabel = new EPICSLabel(EPICS.prefix+"cc:"+epicsName+":motorG.VALJ","");
	    blField = new EPICSTextField(EPICS.prefix+"cc:" + epicsName + ":motorG.L","");
	    blLabel = new EPICSLabel(EPICS.prefix+"cc:"+epicsName+":motorG.VALL","");
	    loAbortButton = new EPICSApplyButton("Abort",EPICS.prefix+"cc:"+epicsName+":abort.A",EPICS.MARK+"",true,EPICSApplyButton.COLOR_SCHEME_RED);
	    hiAbortButton = new EPICSApplyButton("Abort",EPICS.prefix+"cc:"+epicsName+":abort.A",EPICS.MARK+"",true,EPICSApplyButton.COLOR_SCHEME_RED);
	    statusAbortButton = new EPICSApplyButton("Abort",EPICS.prefix+"cc:"+epicsName+":abort.A",EPICS.MARK+"",true,EPICSApplyButton.COLOR_SCHEME_RED);
	    statusButton = new EPICSApplyButton("Status",EPICS.prefix+"cc:"+"test."+epicsName,EPICS.MARK+"");
	    originButton = new EPICSApplyButton("Origin",EPICS.prefix+"cc:"+epicsName+":origin.A",EPICS.MARK+"");
	    loDatumButton = new EPICSRadioButton(EPICS.prefix+"cc:"+epicsName+":datum.A");
	    hiDatumButton = new EPICSRadioButton(EPICS.prefix+"cc:"+epicsName+":datum.A");
	}
*/
	String sadMotorName = epicsName.toUpperCase();
	if (sadMotorName.length() > 6) { 
	    //mosbarcd?
	    if (sadMotorName.startsWith("MOS")) 
		sadMotorName = "MOS";
	    else // filter wheel
		sadMotorName = sadMotorName.substring(0,4) + sadMotorName.charAt(sadMotorName.length()-1);
	}

/*
	Old hard-coded values
	isSadLabel = new EPICSLabel(EPICS.prefix+"sad:"+sadMotorName+"IV","");
	tsSadLabel = new EPICSLabel(EPICS.prefix+"sad:"+sadMotorName+"SV","");
	accSadLabel = new EPICSLabel(EPICS.prefix+"sad:"+sadMotorName+"AC","");
	decSadLabel = new EPICSLabel(EPICS.prefix+"sad:"+sadMotorName+"DC","");
	rcSadLabel = new EPICSLabel(EPICS.prefix+"sad:"+sadMotorName+"RC","");
*/
        isSadLabel = new EPICSLabel(epicsName+"SADInitVel","");
        tsSadLabel = new EPICSLabel(epicsName+"SADSlewVel","");
        accSadLabel = new EPICSLabel(epicsName+"SADAcc","");
        decSadLabel = new EPICSLabel(epicsName+"SADDec","");
        rcSadLabel = new EPICSLabel(epicsName+"SADRunc","");


// 	loDatumButton = new EPICSRadioButton(EPICS.prefix+"cc:datum."+epicsName,EPICS.prefix+"cc:park."+epicsName);
// 	hiDatumButton = new EPICSRadioButton(EPICS.prefix+"cc:datum."+epicsName,EPICS.prefix+"cc:park."+epicsName);
// 	loParkButton = new EPICSRadioButton(EPICS.prefix+"cc:park."+epicsName,EPICS.prefix+"cc:datum."+epicsName);
// 	hiParkButton = new EPICSRadioButton(EPICS.prefix+"cc:park."+epicsName,EPICS.prefix+"cc:datum."+epicsName);

	loParkButton =  new JRadioButton("Park"); loParkButton.setEnabled(false);
	hiParkButton =  new JRadioButton("Park"); hiParkButton.setEnabled(false);

	loDatumButton.setText("Datum");
	hiDatumButton.setText("Datum");
	loParkButton.setText("Park");
	hiParkButton.setText("Park");
	//datumDirBox.addItem("");
	highAnimation = new FJECMotorAnimationPanel(displayedNames,displayedValues,displayedValues[displayedValues.length-1]+displayedValues[displayedValues.length-1]-displayedValues[displayedValues.length-2]);
	lowAnimation  = new FJECMotorAnimationPanel(displayedNames,displayedValues,displayedValues[displayedValues.length-1]+displayedValues[displayedValues.length-1]-displayedValues[displayedValues.length-2]); 
	statusAnimation  = new FJECMotorAnimationPanel(displayedNames,displayedValues,displayedValues[displayedValues.length-1]+displayedValues[displayedValues.length-1]-displayedValues[displayedValues.length-2]); 

	hiNameCheckBox.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    loNameCheckBox.setSelected(hiNameCheckBox.isSelected());
		    statusNameCheckBox.setSelected(hiNameCheckBox.isSelected());
		    ableButtons();
		}
	    });
	loNameCheckBox.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    hiNameCheckBox.setSelected(loNameCheckBox.isSelected());
		    statusNameCheckBox.setSelected(loNameCheckBox.isSelected());
		    ableButtons();
		}
	    });
	statusNameCheckBox.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    hiNameCheckBox.setSelected(statusNameCheckBox.isSelected());
		    loNameCheckBox.setSelected(statusNameCheckBox.isSelected());
		    ableButtons();
		}
	    });

	new EPICSCarCallback(EPICS.applyPrefix+"applyC",new EPICSCarListener(){
		public void carTransition(int transition) {
		    if (transition == EPICS.IDLE) {
			CAR_FLAG &= ~APPLYC;
			if (hiNameCheckBox.isSelected() && CAR_FLAG == 0)
			    motionStatus(false);
		    } else if(transition == EPICS.BUSY) {
			CAR_FLAG |= APPLYC;
			motionStatus(true);
		    } else if(transition == EPICS.ERROR){
			CAR_FLAG &= ~APPLYC;
			if (hiNameCheckBox.isSelected() && CAR_FLAG == 0)
			    motionStatus(false);
		    } else if (transition == Integer.MIN_VALUE) {
			
		    } else{
			System.err.println("FJECMotor::CarCallback> Unrecognized CAR value: "+transition);
		    }
		}
	    });
	//if (EPICS.PORTABLE) {
	    //new EPICSCarCallback(EPICS.applyPrefix+"cc:initC",new EPICSCarListener(){
            new EPICSCarCallback("ccinitC",new EPICSCarListener(){
		    public void carTransition(int transition) {
			if (transition == EPICS.IDLE) {
			    CAR_FLAG &= ~INITC;
			    if (hiNameCheckBox.isSelected() && CAR_FLAG == 0)
				motionStatus(false);
			} else if(transition == EPICS.BUSY) {
			    CAR_FLAG |= INITC;
			    motionStatus(true);
			} else if(transition == EPICS.ERROR){
			    CAR_FLAG &= ~INITC;
			    if (hiNameCheckBox.isSelected() && CAR_FLAG == 0)
				motionStatus(false);
			
			} else if (transition == Integer.MIN_VALUE) {
			    // do nothing
			} else {
			    System.err.println("FJECMotor::CarCallback> Unrecognized CAR value: "+transition);
			}
		    }
		});
	    //new EPICSCarCallback(EPICS.applyPrefix+"cc:setupC",new EPICSCarListener(){
            new EPICSCarCallback("ccsetupC",new EPICSCarListener(){
		    public void carTransition(int transition) {
			if (transition == EPICS.IDLE) {
			    CAR_FLAG &= ~SETUPC;
			    if (hiNameCheckBox.isSelected() && CAR_FLAG == 0)
				motionStatus(false);
			} else if(transition == EPICS.BUSY) {
			    CAR_FLAG |= SETUPC;
			    motionStatus(true);
			} else if(transition == EPICS.ERROR){
			    CAR_FLAG &= ~SETUPC;
			    if (hiNameCheckBox.isSelected() && CAR_FLAG == 0)
				motionStatus(false);
			} else if(transition == Integer.MIN_VALUE) {
			    
			} else{
			    System.err.println("FJECMotor::CarCallback> Unrecognized CAR value: "+transition);
			}
		    }
		});
	    //new EPICSCarCallback(EPICS.applyPrefix+"cc:datumC",new EPICSCarListener(){
            new EPICSCarCallback("ccdatumC",new EPICSCarListener(){
		    public void carTransition(int transition) {
			if (transition == EPICS.IDLE) {
			    CAR_FLAG &= ~DATUMC;
			    if (hiNameCheckBox.isSelected() && CAR_FLAG == 0)
				motionStatus(false);
			} else if(transition == EPICS.BUSY) {
			    CAR_FLAG |= DATUMC;
			    motionStatus(true);
			} else if(transition == EPICS.ERROR){
			    CAR_FLAG &= ~DATUMC;
			    if (hiNameCheckBox.isSelected() && CAR_FLAG == 0)
				motionStatus(false);
			} else if (transition == Integer.MIN_VALUE) {
			    
			} else{
			    System.err.println("FJECMotor::CarCallback> Unrecognized CAR value: "+transition);
			}
		    }
		});
	//} else {
	    //new EPICSCarCallback(EPICS.applyPrefix+"cc:"+epicsName+":motorC",new EPICSCarListener(){
            new EPICSCarCallback(epicsName+"MotorC",new EPICSCarListener(){
		    public void carTransition(int transition) {
			if (transition == EPICS.IDLE) {
			    CAR_FLAG &= ~MOTORC;
			    if (hiNameCheckBox.isSelected() && CAR_FLAG == 0)
				motionStatus(false);
			} else if(transition == EPICS.BUSY) {
			    CAR_FLAG |= MOTORC;
			    motionStatus(true);
			} else if(transition == EPICS.ERROR){
			    CAR_FLAG &= ~MOTORC;
			    if (hiNameCheckBox.isSelected() && CAR_FLAG == 0)
				motionStatus(false);
			} else if(transition == Integer.MIN_VALUE) {
			    
			} else{
			    System.err.println("FJECMotor::CarCallback> Unrecognized CAR value: "+transition);
			}
		    }
		});	    
	//}
	//fix for inconsistant epics rec names
	sadRec = EPICS.prefix+"sad:"+epicsName+"Steps.VAL";
	if (sadRec.toLowerCase().indexOf("barcd") != -1)
	    sadRec = EPICS.prefix+"sad:"+"MOS"+"Steps.VAL";	
	new EPICSCallback(sadRec, new EPICSCallbackListener(){
		public void valueChanged(String val) {
		    try {
			int step;
			try {
			    if (val.trim().equals("")) {
				step = 0;
			    } else step = Integer.parseInt(val.trim());
			} catch (NumberFormatException nfe) {
			    step = (int) Double.parseDouble(val.trim());
			}
			highAnimation.setCurrentStep(step);
			lowAnimation.setCurrentStep(step);
			statusAnimation.setCurrentStep(step);
			String s = getPositionString(step);
			hiStatusLabel.setText(s);
			loStatusLabel.setText(s);
			statusStatusLabel.setText(s);
			hiStatusLabel.setToolTipText(sadRec+" = "+val);
			loStatusLabel.setToolTipText(sadRec+" = "+val);
			statusStatusLabel.setToolTipText(sadRec+" = "+val);
		    } catch (Exception e) {
			System.err.println("FJECMotor.animationMonitorChanged> "+e.toString());
		    }
		}
	    });
	//if (EPICS.PORTABLE) {
	    //new EPICSCallback(EPICS.prefix+"cc:setup.MARK", new EPICSCallbackListener(){
            new EPICSCallback(EPICS.recs.ccSetupMark, new EPICSCallbackListener(){
		    public void valueChanged(String val) {
			if (val == null || val.trim().equals("") ||
			    val.trim().toLowerCase().equals("null"))
			    return;
			try {
			    int boolVal = Integer.parseInt(val.trim());
			    setAnimationColors("red",boolVal);
			} catch (Exception e) {
			    System.err.println("FJECMotor.setupRecMonitorChanged> "+e.toString());
			}		    
		    }
		});
	    //new EPICSCallback(EPICS.prefix+"cc:datum.MARK", new EPICSCallbackListener(){
            new EPICSCallback(EPICS.recs.ccDatumMark, new EPICSCallbackListener(){
		    public void valueChanged(String val) {
			if (val == null || val.trim().equals("") ||
			    val.trim().toLowerCase().equals("null"))
			    return;
			try {
			    int boolVal = Integer.parseInt(val.trim());
			    setAnimationColors("green",boolVal);
			} catch (Exception e) {
			    System.err.println("FJECMotor.datumRecMonitorChanged> "+e.toString());
			}		    
		    }
		});
	    
	    //new EPICSCallback(EPICS.prefix + "cc:park.MARK", new EPICSCallbackListener(){
            new EPICSCallback(EPICS.recs.ccParkMark, new EPICSCallbackListener(){
		    public void valueChanged(String val) {
			if (val == null || val.trim().equals("") ||
			    val.trim().toLowerCase().equals("null"))
			    return;
			try {
			    int boolVal = Integer.parseInt(val.trim());
			    setAnimationColors("blue",boolVal);
			} catch (Exception e) {
			    System.err.println("FJECMotor.parkRecMonitorChanged> "+e.toString());
			}		    
		    }
		    });
	//} else {
	    //new EPICSCallback(EPICS.prefix + "cc:"+epicsName+":motorG.MARK", new EPICSCallbackListener() {
            new EPICSCallback(epicsName+"MotorG", new EPICSCallbackListener() {
		    public void valueChanged(String val) {
			try {
			    int boolVal = Integer.parseInt(val.trim());
			    setAnimationColors("blue",boolVal);
			} catch (Exception e) {
			    System.err.println("FJECMotor> "+e.toString());
			}
		    }
		});
	    //new EPICSCallback(EPICS.prefix + "cc:"+epicsName+":datum.MARK", new EPICSCallbackListener() {
            new EPICSCallback(epicsName+"DatumMark", new EPICSCallbackListener() {
		    public void valueChanged(String val) {
			try {
			    int boolVal = Integer.parseInt(val.trim());
			    setAnimationColors("green",boolVal);
			} catch (Exception e) {
			    System.err.println("FJECMotor> "+e.toString());
			}
		    }
		});
	//}
	//loCombo = new EPICSLowIndexorBox(epicsName,prefix+"setup."+epicsName+"Step");

	/*
	loCombo.addItem(stmField);
	//loCombo.getEditor().setItem(stmField);
	loCombo.addItem(new String("Datum"));
	loCombo.addItem(new String("Park"));
	loCombo.setEditable(true);
	loCombo.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    if (loCombo.getSelectedIndex() == 0) {
			loCombo.setEditable(true);
			EPICS.put(EPICS.prefix+"datum.DIR",EPICS.CLEAR);
		    } else if (((String)loCombo.getSelectedItem()).equals("Datum")) {
			loCombo.setEditable(false);
			EPICS.put(prefix+"setup.DIR",EPICS.CLEAR);
			EPICS.put(EPICS.prefix+"datum."+epicsName,EPICS.PRESET);
		    } else if (((String)loCombo.getSelectedItem()).equals("Park")) {
			loCombo.setEditable(false);
		    }
		}
	    });
	*/
    }

    public void doStatusClick() {
	statusButton.doClick();
    }

    public void setAnimationColors(String color, int boolVal) {
	if (color.equals("red")) {
	    highAnimation.setRed(boolVal);
	    lowAnimation.setRed(boolVal);
	    statusAnimation.setRed(boolVal);
	}else if (color.equals("green")) {
	    highAnimation.setGreen(boolVal);
	    lowAnimation.setGreen(boolVal);
	    statusAnimation.setGreen(boolVal);
	}else if (color.equals("blue")) {
	    highAnimation.setBlue(boolVal);
	    lowAnimation.setBlue(boolVal);
	    statusAnimation.setBlue(boolVal);
	}
    }

    public String getName() { return name; }
    public String getEpicsName() { return epicsName; }

    //////////////////
    //Action Methods//
    //////////////////

    public String getPositionString(int f /*step value*/) {
	if (f < posValues[0]) { return(f+": "+posNames[0]+" - "+(posValues[0]-f));}
	int i = 0;
	for (i =0; i<posValues.length-1; i++)
	    if (f == posValues[i]) { return(f+": "+posNames[i]);}
	    else if (f < posValues[i+1]) {
		float midpoint = (posValues[i+1]+posValues[i])/2.0f;
		if (f <= midpoint)
		    return(f+": "+posNames[i]+" + "+(f - posValues[i]));
		else
		    return(f+": "+posNames[i+1]+" - "+(posValues[i+1]-f));
	    } 
	if (f == posValues[i]) { return(f+": "+posNames[i]);}
	return (f+": "+posNames[i]+" + "+(f-posValues[i]));	
    }

    /**
     *Enable/Disable buttons based on the state of {@link #hiNameCheckBox} and
     *{@link #loNameCheckBox} (which are always equal).
     */
    public void ableButtons() {
	hiDatumButton.setEnabled(hiNameCheckBox.isSelected());
	loDatumButton.setEnabled(hiNameCheckBox.isSelected());
	//hiParkButton.setEnabled(hiNameCheckBox.isSelected());
	//loParkButton.setEnabled(hiNameCheckBox.isSelected());
	originButton.setEnabled(hiNameCheckBox.isSelected());
	statusButton.setEnabled(hiNameCheckBox.isSelected());
	stmField.setEnabled(hiNameCheckBox.isSelected());
	namedLocationBox.setEnabled(hiNameCheckBox.isSelected());
	hiBacklashCheckBox.setEnabled(hiNameCheckBox.isSelected());
	loBacklashCheckBox.setEnabled(hiNameCheckBox.isSelected());
    }

    /**
     * Enable/Disable buttons based on motion of motors
     * @param isMotion true will disable buttons, false will enable them
     */
    public void motionStatus(boolean isMotion) {
	hiDatumButton.setEnabled(!isMotion);
	loDatumButton.setEnabled(!isMotion);
	//hiParkButton.setEnabled(!isMotion);
	//loParkButton.setEnabled(!isMotion);
	originButton.setEnabled(!isMotion);
	statusButton.setEnabled(!isMotion);
	stmField.setEnabled(!isMotion);
	namedLocationBox.setEnabled(!isMotion);
	hiBacklashCheckBox.setEnabled(hiNameCheckBox.isSelected());
	loBacklashCheckBox.setEnabled(hiNameCheckBox.isSelected());
    }


    /////////////////////////
    //Visual helper methods//
    /////////////////////////

    /** 
     *Helper method.  This method arranges all parameter-level visual components into a long, thin panel
     *(ideal for placement into a GridLayout container <p>
     *@return JPanel containing all parameter-level visual components laid out using {@link RatioLayout} 
     */
    public JPanel getMotorParameterPanel() {
	JPanel fieldPanel = new JPanel();
	JPanel panel = new JPanel();
	fieldPanel.setLayout(new GridLayout(1,0,10,0));
	panel.setLayout(new RatioLayout());
	
	JPanel isPan = new JPanel(new GridLayout(1,2));
	isPan.add(isField); isPan.add(isLabel);
	JPanel tsPan = new JPanel(new GridLayout(1,2));
	tsPan.add(tsField); tsPan.add(tsLabel);
	JPanel accPan = new JPanel(new GridLayout(1,2));
	accPan.add(accField); accPan.add(accLabel);
	JPanel decPan = new JPanel(new GridLayout(1,2));
	decPan.add(decField); decPan.add(decLabel);
	JPanel dcPan = new JPanel(new GridLayout(1,2));
	dcPan.add(dcField); dcPan.add(dcLabel);
        JPanel hcPan = new JPanel(new GridLayout(1,2));
        hcPan.add(hcField); hcPan.add(hcLabel);
	JPanel dsPan = new JPanel(new GridLayout(1,2));
	dsPan.add(dsField); dsPan.add(dsLabel);
	JPanel blPan = new JPanel(new GridLayout(1,2));
	blPan.add(blField); blPan.add(blLabel);
	JPanel datumDirPan = new JPanel(new GridLayout(1,2));
	datumDirPan.add(datumDirBox); datumDirPan.add(datumDirLabel);

	fieldPanel.add(isPan);
	fieldPanel.add(tsPan);
	fieldPanel.add(accPan);
	fieldPanel.add(decPan);
	fieldPanel.add(dcPan);
	fieldPanel.add(hcPan);
	//fieldPanel.add(dsPan);
	fieldPanel.add(blPan);
	//fieldPanel.add(datumDirPan);

// 	fieldPanel.add(isField);
// 	fieldPanel.add(isLabel);
// 	fieldPanel.add(tsField);
// 	fieldPanel.add(tsLabel);
// 	fieldPanel.add(accField);
// 	fieldPanel.add(accLabel);
// 	fieldPanel.add(decField);
// 	fieldPanel.add(decLabel);
// 	fieldPanel.add(dcField);
// 	fieldPanel.add(dcLabel);
// 	fieldPanel.add(dsField);
// 	fieldPanel.add(dsLabel);
// 	fieldPanel.add(blField);
// 	fieldPanel.add(blLabel);
// 	fieldPanel.add(datumDirBox);
// 	fieldPanel.add(datumDirLabel);
	panel.add("0.01,0.02;0.15,0.99",nameLabel);
	panel.add("0.17,0.02;0.82,0.99", fieldPanel);
	//panel.setBorder(new EtchedBorder(0));
	return panel;
    }
    /**
     *This method  is declared a static method since it
     *should not be called by each individual motor.  Rather, it should be called only once by the application
     *class when creating a motor parameter panel.
     *@return JPanel containing labels corresponding to visual components and laid out similarly to the JPanel
     *returned by {@link #getMotorParameterPanel()}
     */ 
    public static JPanel getMotorParameterLabelPanel() {
	JPanel fieldPanel = new JPanel();
	JPanel panel = new JPanel();
	fieldPanel.setLayout(new GridLayout(1,0));
	panel.setLayout(new RatioLayout());
	fieldPanel.add(new JLabel("Initial Speed",JLabel.CENTER));
	fieldPanel.add(new JLabel("Term Speed",JLabel.CENTER));
	fieldPanel.add(new JLabel("Acceleration",JLabel.CENTER));
	fieldPanel.add(new JLabel("Deceleration",JLabel.CENTER));
	fieldPanel.add(new JLabel("Run Current",JLabel.CENTER));
        fieldPanel.add(new JLabel("Hold Current",JLabel.CENTER));
	//fieldPanel.add(new JLabel("Datum Speed",JLabel.CENTER));
	fieldPanel.add(new JLabel("Back Lash",JLabel.CENTER));
	//fieldPanel.add(new JLabel("Datum Dir",JLabel.CENTER));
	//fieldPanel.add(new JLabel("",JLabel.CENTER));
	panel.add("0.01,0.02;0.15,0.99",new JLabel("Motor Name",JLabel.CENTER));
	panel.add("0.17,0.02;0.82,0.99", fieldPanel);
	return panel;
    }

    /**
     *Helper method.  This method arranges all parameter-level visual components into a long, thin panel
     *(ideal for placement into a GridLayout container <p>
     *@return JPanel containing all parameter-level visual components laid out using {@link RatioLayout}
     */
    public JPanel getMotorDatumParameterPanel() {
        JPanel fieldPanel = new JPanel();
        JPanel panel = new JPanel();
        fieldPanel.setLayout(new GridLayout(1,0,10,0));
        panel.setLayout(new RatioLayout());

        JPanel disPan = new JPanel(new GridLayout(1,2));
        disPan.add(disField); disPan.add(disLabel);
        JPanel ddsPan = new JPanel(new GridLayout(1,2));
        ddsPan.add(ddsField); ddsPan.add(ddsLabel);
        //JPanel disvPan = new JPanel(new GridLayout(1,2));
        //disvPan.add(disvField); disvPan.add(disvLabel);
        JPanel dsvPan = new JPanel(new GridLayout(1,2));
        dsvPan.add(dsvField); dsvPan.add(dsvLabel);
        JPanel daccPan = new JPanel(new GridLayout(1,2));
        daccPan.add(daccField); daccPan.add(daccLabel);
        JPanel ddecPan = new JPanel(new GridLayout(1,2));
        ddecPan.add(ddecField); ddecPan.add(ddecLabel);
        JPanel ddcPan = new JPanel(new GridLayout(1,2));
        ddcPan.add(ddcField); ddcPan.add(ddcLabel);
        JPanel dhcPan = new JPanel(new GridLayout(1,2));
        dhcPan.add(dhcField); dhcPan.add(dhcLabel);
        JPanel dblPan = new JPanel(new GridLayout(1,2));
        dblPan.add(dblField); dblPan.add(dblLabel);

        fieldPanel.add(disPan);
        fieldPanel.add(ddsPan);
        //fieldPanel.add(disvPan);
        fieldPanel.add(dsvPan);
        fieldPanel.add(daccPan);
        fieldPanel.add(ddecPan);
        fieldPanel.add(ddcPan);
        fieldPanel.add(dhcPan);
        fieldPanel.add(dblPan);
        panel.add("0.01,0.02;0.15,0.99",datumNameLabel);
        panel.add("0.17,0.02;0.82,0.99", fieldPanel);
        //panel.setBorder(new EtchedBorder(0));
        return panel;
    }

    public static JPanel getMotorDatumParameterLabelPanel() {
        JPanel fieldPanel = new JPanel();
        JPanel panel = new JPanel();
        fieldPanel.setLayout(new GridLayout(1,0));
        panel.setLayout(new RatioLayout());
        fieldPanel.add(new JLabel("Initial Speed",JLabel.CENTER));
        fieldPanel.add(new JLabel("Datum Speed",JLabel.CENTER));
        //fieldPanel.add(new JLabel("Init Slow",JLabel.CENTER));
        fieldPanel.add(new JLabel("Slow Speed",JLabel.CENTER));
        fieldPanel.add(new JLabel("Acceleration",JLabel.CENTER));
        fieldPanel.add(new JLabel("Deceleration",JLabel.CENTER));
        fieldPanel.add(new JLabel("Run Current",JLabel.CENTER));
        fieldPanel.add(new JLabel("Hold Current",JLabel.CENTER));
        //fieldPanel.add(new JLabel("Datum Speed",JLabel.CENTER));
        fieldPanel.add(new JLabel("Back Lash",JLabel.CENTER));
        //fieldPanel.add(new JLabel("Datum Dir",JLabel.CENTER));
        //fieldPanel.add(new JLabel("",JLabel.CENTER));
        panel.add("0.01,0.02;0.15,0.99",new JLabel("Motor Name",JLabel.CENTER));
        panel.add("0.17,0.02;0.82,0.99", fieldPanel);
        return panel;
    }


    public JPanel getMotorStatusPanel() {
	JPanel panel = new JPanel();
	panel.setLayout(new RatioLayout());
	panel.add("0.0,0.02;0.05,0.99",statusAnimation);
	panel.add("0.05,0.02;0.10,0.99", statusNameCheckBox);
	panel.add("0.15,0.02;0.22,0.99", statusStatusLabel);
	panel.add("0.40,0.02;0.05,0.99", isSadLabel);
	panel.add("0.46,0.02;0.05,0.99", tsSadLabel);
	panel.add("0.52,0.02;0.05,0.99", accSadLabel);
	panel.add("0.58,0.02;0.05,0.99", decSadLabel);
	panel.add("0.64,0.02;0.05,0.99", rcSadLabel);
	panel.add("0.70,0.02;0.08,0.99", originButton);
	panel.add("0.80,0.02;0.08,0.99", statusButton);
	panel.add("0.90,0.02;0.10,0.99", statusAbortButton);
	return panel;	
    }

    public static JPanel getMotorStatusLabelPanel(){
	JPanel panel = new JPanel();
	panel.setLayout(new RatioLayout());
	panel.add("0.05,0.02;0.15,0.99", new JLabel("Motor Name",JLabel.LEFT));
	panel.add("0.17,0.02;0.20,0.99", new JLabel("Current Position",JLabel.CENTER));
	panel.add("0.40,0.02;0.05,0.99", new JLabel("InitVel",JLabel.CENTER));
	panel.add("0.46,0.02;0.05,0.99", new JLabel("SlewVel",JLabel.CENTER));
	panel.add("0.52,0.02;0.05,0.99", new JLabel("Acc",JLabel.CENTER));
	panel.add("0.58,0.02;0.05,0.99", new JLabel("Dec",JLabel.CENTER));
	panel.add("0.64,0.02;0.05,0.99", new JLabel("RunCur",JLabel.CENTER));
	return panel;
    }
    /** 
     *Helper method.  This method arranges all low-level visual components into a long, thin panel
     *(ideal for placement into a GridLayout container <p>
     *@return JPanel containing all low-level visual components laid out using {@link RatioLayout} 
     */
    public JPanel getMotorLowLevelPanel() {
	//JPanel buttonPanel = new JPanel();
	final JPanel panel = new JPanel();
	/*
	String [] choiceStr = {"Datum","Park","Origin","Status"};
	final JComboBox loChoiceBox = new JComboBox(choiceStr);
	loChoiceBox.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    panel.remove(loDatumButton);
		    panel.remove(loParkButton);
		    panel.remove(originButton);
		    panel.remove(statusButton);
		    JButton butto;
		    if (loChoiceBox.getSelectedIndex() == 0)
			butto = loDatumButton;
		    else if (loChoiceBox.getSelectedIndex() == 1)
			butto = loParkButton;
		    else if (loChoiceBox.getSelectedIndex() == 2)
			butto = originButton;
		    else if (loChoiceBox.getSelectedIndex() == 3)
			butto = statusButton;
		    else {
			System.err.println("FJECMotor.getMotorLowLevelPanel.actionPerformed> Impossible! Bad combobox selected index");
			return;
		    }
		    panel.add("0.80,0.02;0.08,0.99",butto);
		    panel.repaint();
		}
	    });
	*/

	//buttonPanel.setLayout(new GridLayout(1,0));
	//ButtonGroup bg = new ButtonGroup();
	//bg.add(loDatumButton);
	//bg.add(loParkButton);
	//bg.add(loHiddenButton);
	//buttonPanel.add(loDatumButton);
	//buttonPanel.add(loAbortButton);
	//buttonPanel.add(loParkButton);
	//buttonPanel.add(originButton);
	//buttonPanel.add(statusButton);
	panel.setLayout(new RatioLayout());
	panel.add("0.0,0.02;0.05,0.99",lowAnimation);
	panel.add("0.05,0.02;0.10,0.99", loNameCheckBox);
	panel.add("0.15,0.02;0.22,0.99", loStatusLabel);
	panel.add("0.38,0.02;0.10,0.99", stmField);
	panel.add("0.51,0.02;0.04,0.99", loBacklashCheckBox);
	panel.add("0.56,0.02;0.10,0.99",loLastCommand);
	panel.add("0.70,0.02;0.08,0.99",loDatumButton);
	panel.add("0.80,0.02;0.08,0.99",loParkButton);
	panel.add("0.90,0.02;0.10,0.99", loAbortButton);
	return panel;
    }
    /**
     *This method  is declared a static method since it
     *should not be called by each individual motor.  Rather, it should be called only once by the application
     *class when creating a motor low-level panel.
     *@return JPanel containing labels corresponding to visual components and laid out similarly to the JPanel
     *returned by {@link #getMotorLowLevelPanel()}
     */ 
    public static JPanel getMotorLowLevelLabelPanel() {
	JPanel panel = new JPanel();
	panel.setLayout(new RatioLayout());
	panel.add("0.05,0.02;0.15,0.99", new JLabel("Motor Name",JLabel.LEFT));
	panel.add("0.17,0.02;0.20,0.99", new JLabel("Current Position",JLabel.CENTER));
	panel.add("0.38,0.02;0.10,0.99", new JLabel("Steps to Move",JLabel.CENTER));
	panel.add("0.48,0.02;0.08,0.99", new JLabel("Backlash",JLabel.CENTER));
	panel.add("0.56,0.02;0.10,0.99", new JLabel("Previous Steps",JLabel.CENTER));
	//panel.add("0.70,0.02;0.08,0.99", new JLabel("Action Cmd",JLabel.CENTER));
	//panel.add("0.80,0.02;0.08,0.99", new JLabel("Action Btn",JLabel.CENTER));
	return panel;
    }
    /** 
     *Helper method.  This method arranges all high-level visual components into a long, thin panel
     *(ideal for placement into a GridLayout container <p>
     *@return JPanel containing all high-level visual components laid out using {@link RatioLayout} 
     */
    public JPanel getMotorHighLevelPanel() {
	//JPanel buttonPanel = new JPanel();
	JPanel panel = new JPanel();
	//buttonPanel.setLayout(new GridLayout(1,0,10,10));
	//ButtonGroup bg = new ButtonGroup();
	//bg.add(hiDatumButton); bg.add(hiParkButton); bg.add(hiHiddenButton);
	//buttonPanel.add(hiDatumButton);
	//buttonPanel.add(hiParkButton);
	//buttonPanel.add(hiAbortButton);
	panel.setLayout(new RatioLayout());
	panel.add("0.0,0.02;0.05,0.99",highAnimation);
	panel.add("0.05,0.02;0.10,0.99", hiNameCheckBox);
	panel.add("0.15,0.02;0.22,0.99", hiStatusLabel);
	panel.add("0.38,0.02;0.10,0.99", namedLocationBox);
	panel.add("0.51,0.02;0.04,0.99", hiBacklashCheckBox);
	panel.add("0.56,0.02;0.10,0.99", hiLastCommand);
	panel.add("0.70,0.02;0.08,0.99", hiDatumButton);
	panel.add("0.80,0.02;0.08,0.99", hiParkButton);
	panel.add("0.90,0.02;0.10,0.99", hiAbortButton);
	return panel;
    }
    /**
     *This method  is declared a static method since it
     *should not be called by each individual motor.  Rather, it should be called only once by the application
     *class when creating a motor high-level panel.
     *@return JPanel containing labels corresponding to visual components and laid out similarly to the JPanel
     *returned by {@link #getMotorHighLevelPanel()}
     */ 
    public static JPanel getMotorHighLevelLabelPanel() {
	JPanel panel = new JPanel();
	panel.setLayout(new RatioLayout());
	panel.add("0.05,0.02;0.15,0.99", new JLabel("Motor Name",JLabel.LEFT));
	panel.add("0.17,0.02;0.20,0.99", new JLabel("Current Position",JLabel.CENTER));
	panel.add("0.38,0.02;0.10,0.99", new JLabel("New Position",JLabel.CENTER));
	panel.add("0.48,0.02;0.08,0.99", new JLabel("Backlash",JLabel.CENTER));
	panel.add("0.56,0.02;0.10,0.99", new JLabel("Previous Position",JLabel.CENTER));
	return panel;
    }

    public static JPanel getMotorSubSystemPanel() {
	return new FJECSubSystemPanel("cc:");
    }

}


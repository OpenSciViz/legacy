package uffjec;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;
import java.util.*;

import javaUFLib.*;

public class JPanelIS extends JPanel {
	static final long serialVersionUID = 0;
	
	
    JButton bootButton;
    JButton initButton;
    EPICSApplyButton datumButton;
    JButton testButton;
    JButton stopButton;
    JButton continueButton;
    JButton pauseButton;
    JButton abortButton;
    EPICSApplyButton observeButton;
    
    JLabel observeTimer;

    UFClock clock;

    //this component is global
    //to allow for saving configurations
    JPanel insPanel;
    private static JPanelIS me;
    HashMap historyHash;
    JFrame historyFrame;

    public JPanel getSetupPanel() {
	JPanel setupPanel = new JPanel();

	abortButton = new EPICSApplyButton("Abort", EPICS.recs.get(EPICSRecs.abort), EPICS.MARK+"", true, EPICSApplyButton.COLOR_SCHEME_RED);
	bootButton = new EPICSApplyButton("Reboot", EPICS.recs.get(EPICSRecs.reboot));
	initButton = new EPICSApplyButton("Init", EPICS.recs.get(EPICSRecs.init));
	datumButton = new EPICSApplyButton("Datum", EPICS.recs.get(EPICSRecs.datum));
	testButton = new EPICSApplyButton("Test", EPICS.recs.get(EPICSRecs.test));

        UFColorButton clearButton = new UFColorButton("Clear",UFColorButton.COLOR_SCHEME_YELLOW);
        clearButton.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent ae) {
		    if (EPICS.prefix.equals("null:")) {
			//JOptionPane.showMessageDialog(null, "Warning: You are not connected to any database!", "Warning", JOptionPane.WARNING_MESSAGE);
                        fjecFrame.connectToEPICS();
			return;
		    }
                    EPICS.put(EPICS.recs.get(EPICSRecs.apply),EPICS.CLEAR+"");
                }
            });
	JPanel buttonPanel = new JPanel();
	buttonPanel.setLayout(new GridLayout(1,0,10,10));
	buttonPanel.add(abortButton);
	buttonPanel.add(clearButton);
	buttonPanel.add(initButton);
	buttonPanel.add(datumButton);
	buttonPanel.add(testButton);
	buttonPanel.add(bootButton);

	pauseButton = new EPICSApplyButton("Pause",EPICS.recs.get(EPICSRecs.pause));
	continueButton = new EPICSApplyButton("Continue",EPICS.recs.get(EPICSRecs.continu));
	stopButton = new EPICSApplyButton("Stop", EPICS.recs.get(EPICSRecs.stop));
	//abortButton = new EPICSApplyButton("Abort", EPICS.recs.get(EPICSRecs.abort), EPICS.MARK+"", true, EPICSApplyButton.COLOR_SCHEME_RED);
	observeButton = new EPICSApplyButton("Observe",EPICS.recs.get(EPICSRecs.observe));
	observeTimer = new JLabel("0 s");
	JPanel buttonPanel2 = new JPanel();
	buttonPanel2.setLayout(new GridLayout(0,2,10,10));
	buttonPanel2.add(observeButton);
	buttonPanel2.add(pauseButton);
	buttonPanel2.add(continueButton);
	buttonPanel2.add(stopButton);
	//buttonPanel2.add(abortButton);

	insPanel = new JPanel();	
	insPanel.setLayout(new RatioLayout());



	String [] trueFalseStrs = {"null","0","1"};
	String [] beamStrs = { "null","f/16","MCAO_over","MCAO_under"};
	String [] deckerStrs = { "null","imaging","long_slit","mos"};
	String [] detFocposStrs = {"null","f/16","MCAO"};
	String [] filterStrs = {"null","open","dark","J-lo","J","H","Ks","JH","HK","TBD1"};
	String [] grismStrs = {"null","open","JH","HK","JHK","TBD1"};
	String [] lyotStrs = {"null","f/16","MCAO_over","MCAO_under","H1","H2"};
	String [] mosslitStrs = {"null","imaging","circle1","circle2","1pix-slit","2pix-slit","3pix-slit","4pix-slit","6pix-slit","8pix-slit",
				 "mos1","mos2","mos3","mos4","mos5","mos6","mos7","mos8","mos9"};
	String [] wheelBiasStrs = { "null","imaging","long_slit","mos"};
	String [] windowCoverStrs = {"null","open","closed"};

	insPanel.add("0.01,0.01;0.99,0.10",new JLabel("Instrument Setup",JLabel.CENTER));

        insPanel.add("0.01,0.11;0.33,0.05",new JLabel("WindowCover"));
        insPanel.add("0.34,0.11;0.33,0.05",new EPICSComboBox(EPICSRecs.windowCover,windowCoverStrs,EPICSComboBox.ITEM));
        insPanel.add("0.67,0.11;0.33,0.05",new EPICSLabel(EPICSRecs.valWindowCover,""));

        insPanel.add("0.01,0.16;0.33,0.05",new JLabel("MOSSlit"));
        insPanel.add("0.34,0.16;0.33,0.05",new EPICSComboBox(EPICSRecs.mosSlit,mosslitStrs,EPICSComboBox.ITEM));
        insPanel.add("0.67,0.16;0.33,0.05",new EPICSLabel(EPICSRecs.valMosSlit,""));

        insPanel.add("0.01,0.21;0.33,0.05",new JLabel("Decker"));
        insPanel.add("0.34,0.21;0.33,0.05",new EPICSComboBox(EPICSRecs.decker,deckerStrs,EPICSComboBox.ITEM));
        insPanel.add("0.67,0.21;0.33,0.05",new EPICSLabel(EPICSRecs.valDecker,""));

        insPanel.add("0.01,0.26;0.33,0.05",new JLabel("Filter"));
        insPanel.add("0.34,0.26;0.33,0.05",new EPICSComboBox(EPICSRecs.filter, filterStrs,EPICSComboBox.ITEM));
        insPanel.add("0.67,0.26;0.33,0.05",new EPICSLabel(EPICSRecs.valFilter,""));

        insPanel.add("0.01,0.31;0.33,0.05",new JLabel("Lyot"));
        insPanel.add("0.34,0.31;0.33,0.05",new EPICSComboBox(EPICSRecs.lyot,lyotStrs,EPICSComboBox.ITEM));
        insPanel.add("0.67,0.31;0.33,0.05",new EPICSLabel(EPICSRecs.valLyot,""));

        insPanel.add("0.01,0.36;0.33,0.05",new JLabel("Grism"));
        insPanel.add("0.34,0.36;0.33,0.05",new EPICSComboBox(EPICSRecs.grism,grismStrs,EPICSComboBox.ITEM));
        insPanel.add("0.67,0.36;0.33,0.05",new EPICSLabel(EPICSRecs.valGrism,""));

        insPanel.add("0.01,0.41;0.33,0.05",new JLabel("DetPosFocus"));
        insPanel.add("0.34,0.41;0.33,0.05",new EPICSComboBox(EPICSRecs.detPosFocus,detFocposStrs,EPICSComboBox.ITEM));
        insPanel.add("0.67,0.41;0.33,0.05",new EPICSLabel(EPICSRecs.valDetPosFocus,""));

	/* Removed parameters
	* insPanel.add("0.01,0.21;0.33,0.05",new JLabel("BeamMode"));
        * insPanel.add("0.34,0.21;0.33,0.05",new EPICSComboBox(EPICS.recs.get(EPICSRecs.beamMode),beamStrs,EPICSComboBox.ITEM));
        * insPanel.add("0.67,0.21;0.33,0.05",new EPICSLabel(EPICS.recs.get(EPICSRecs.valBeamMode),""));

	* insPanel.add("0.01,0.26;0.33,0.05",new JLabel("WheelBiasMode"));
	* insPanel.add("0.34,0.26;0.33,0.05",new EPICSComboBox(EPICS.recs.get(EPICSRecs.wheelBiasMode),wheelBiasStrs,EPICSComboBox.ITEM));
        * insPanel.add("0.67,0.26;0.33,0.05",new EPICSLabel(EPICS.recs.get(EPICSRecs.valWheelBiasMode),""));

        * insPanel.add("0.01,0.46;0.07,0.05",new EPICSCheckBox(EPICS.recs.get(EPICSRecs.overrideDecker),"1","0"));

	* insPanel.add("0.16,0.51;0.28,0.05",new JLabel("DetPosFocus"));
        * insPanel.add("0.01,0.51;0.07,0.05",new EPICSCheckBox(EPICS.recs.get(EPICSRecs.overrideDetPos),"1","0"));
        * insPanel.add("0.44,0.51;0.28,0.05",new EPICSComboBox(EPICS.recs.get(EPICSRecs.detPosFocus),detFocposStrs,EPICSComboBox.ITEM));
        * insPanel.add("0.72,0.51;0.28,0.05",new EPICSLabel(EPICS.recs.get(EPICSRecs.valDetPosFocus),""));

        * insPanel.add("0.01,0.56;0.07,0.05",new EPICSCheckBox(EPICS.recs.get(EPICSRecs.overrideGrism),"1","0"));

        * insPanel.add("0.01,0.61;0.07,0.05",new EPICSCheckBox(EPICS.recs.get(EPICSRecs.overrideLyot),"0","1"));

        * insPanel.add("0.16,0.66;0.28,0.05",new JLabel("MOS Set Point"));
        * insPanel.add("0.01,0.66;0.07,0.05",new EPICSCheckBox(EPICS.recs.get(EPICSRecs.overrideMosSetPoint), "0", "1"));
        * insPanel.add("0.44,0.66;0.28,0.05",new EPICSTextField(EPICS.recs.get(EPICSRecs.mosSetPoint),""));
        * insPanel.add("0.72,0.66;0.28,0.05",new EPICSLabel(EPICS.recs.get(EPICSRecs.valMosSetPoint),""));

	* insPanel.add("0.16,0.71;0.28,0.05",new JLabel("CAM Set PointA"));
	* insPanel.add("0.01,0.71;0.07,0.05",new EPICSCheckBox(EPICS.recs.get(EPICSRecs.overrideCamSetPointA), "0", "1"));
        * insPanel.add("0.44,0.71;0.28,0.05",new EPICSTextField(EPICS.recs.get(EPICSRecs.camSetPointA),""));
        * insPanel.add("0.72,0.71;0.28,0.05",new EPICSLabel(EPICS.recs.get(EPICSRecs.valCamSetPointA),""));

	* insPanel.add("0.16,0.76;0.28,0.05",new JLabel("CAM Set PointB"));
        * insPanel.add("0.01,0.76;0.07,0.05",new EPICSCheckBox(EPICS.recs.get(EPICSRecs.overrideCamSetPointB), "0", "1"));
        * insPanel.add("0.44,0.76;0.28,0.05",new EPICSTextField(EPICS.recs.get(EPICSRecs.camSetPointB),""));
        * insPanel.add("0.72,0.76;0.28,0.05",new EPICSLabel(EPICS.recs.get(EPICSRecs.valCamSetPointB),""));
*/

	JButton historyButton = new JButton("History");
	historyButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    showHistory();
		}
	    });
	insPanel.add("0.33,0.93;0.33,0.05",historyButton);

	//insPanel.add("0.01,0.91;0.49,0.05",new JLabel("instrumSetupC"));
	//insPanel.add("0.51,0.91;0.49,0.05",new EPICSLabel(EPICS.prefix+"instrumSetupC",""));
	insPanel.setBorder(new EtchedBorder(0));

	JPanel obsSetupPanel = new JPanel();
	obsSetupPanel.setLayout(new RatioLayout());
	obsSetupPanel.add("0.01,0.01;0.99,0.10",new JLabel("Observation Setup",JLabel.CENTER));

	obsSetupPanel.add("0.01,0.11;0.33,0.16",new JLabel("ExpTime"));
        obsSetupPanel.add("0.34,0.11;0.33,0.16",new EPICSTextField(EPICSRecs.expTime,""));
        obsSetupPanel.add("0.67,0.11;0.33,0.16",new EPICSLabel(EPICSRecs.valExpTime,""));

	obsSetupPanel.add("0.01,0.27;0.33,0.16",new JLabel("NumReads"));
	String []numreadsStrs = {"1","3","4","5","6","7","8","9","10","11","12","13","14","15","16"};
        obsSetupPanel.add("0.34,0.27;0.33,0.16",new EPICSComboBox(EPICSRecs.numReads,numreadsStrs,EPICSComboBox.ITEM));
        obsSetupPanel.add("0.67,0.27;0.33,0.16",new EPICSLabel(EPICSRecs.valNumReads,""));

        obsSetupPanel.add("0.01,0.43;0.33,0.16",new JLabel("ReadoutMode"));
        String [] readoutStrs = {"SCI","ENG"};
        obsSetupPanel.add("0.34,0.43;0.33,0.16",new EPICSComboBox(EPICSRecs.readoutMode,readoutStrs,EPICSComboBox.ITEM));
        obsSetupPanel.add("0.67,0.43;0.33,0.16",new EPICSLabel(EPICSRecs.valReadoutMode,""));

	obsSetupPanel.add("0.01,0.59;0.33,0.16",new JLabel("BiasMode"));
        //obsSetupPanel.add("0.01,0.70;0.09,0.14",new EPICSCheckBox(EPICS.recs.get(EPICSRecs.overrideBias),"1","0"));
        obsSetupPanel.add("0.34,0.59;0.33,0.16",new EPICSComboBox(EPICSRecs.biasMode,wheelBiasStrs,EPICSComboBox.ITEM)); 
        obsSetupPanel.add("0.67,0.59;0.33,0.16",new EPICSLabel(EPICSRecs.valBiasMode,""));

        JButton obsSetupHistoryButton = new JButton("History");
        obsSetupPanel.add("0.33,0.87;0.33,0.12",obsSetupHistoryButton);

	obsSetupPanel.setBorder(new EtchedBorder(0));

	JPanel statusPanel = new JPanel();
	statusPanel.setLayout(new GridLayout(0,2));

	statusPanel.add(new JLabel(EPICS.recs.getName(EPICSRecs.instrumSetupC)));
        statusPanel.add(new EPICSLabel(EPICSRecs.instrumSetupC,""));
        statusPanel.add(new JLabel(EPICS.recs.getName(EPICSRecs.obsSetupC)));
        statusPanel.add(new EPICSLabel(EPICSRecs.obsSetupC,""));

        statusPanel.add(new JLabel(EPICS.recs.getName(EPICSRecs.rebootC)));
        statusPanel.add(new EPICSLabel(EPICSRecs.rebootC,"rebootC:"));
        statusPanel.add(new JLabel(EPICS.recs.getName(EPICSRecs.initC)));
        statusPanel.add(new EPICSLabel(EPICSRecs.initC,"initC:"));
        statusPanel.add(new JLabel(EPICS.recs.getName(EPICSRecs.datumC)));
        statusPanel.add(new EPICSLabel(EPICSRecs.datumC,"datumC:"));
        statusPanel.add(new JLabel(EPICS.recs.getName(EPICSRecs.testC)));
        statusPanel.add(new EPICSLabel(EPICSRecs.testC,"testC:"));
        statusPanel.add(new JLabel(EPICS.recs.getName(EPICSRecs.observeC)));
        statusPanel.add(new EPICSLabel(EPICSRecs.observeC,"observeC:"));
        statusPanel.add(new JLabel(EPICS.recs.getName(EPICSRecs.pauseC)));
        statusPanel.add(new EPICSLabel(EPICSRecs.pauseC,"pauseC:"));
        statusPanel.add(new JLabel(EPICS.recs.getName(EPICSRecs.continueC)));
        statusPanel.add(new EPICSLabel(EPICSRecs.continueC,"continueC:"));
        statusPanel.add(new JLabel(EPICS.recs.getName(EPICSRecs.stopC)));
        statusPanel.add(new EPICSLabel(EPICSRecs.stopC,"stopC:"));
        statusPanel.add(new JLabel(EPICS.recs.getName(EPICSRecs.abortC)));
        statusPanel.add(new EPICSLabel(EPICSRecs.abortC,"abortC:"));

	JPanel statusPanelMain = new JPanel();
	statusPanelMain.setLayout(new RatioLayout());
	statusPanelMain.add("0.01,0.01;0.99,0.05",new JLabel("Status",JLabel.CENTER));
	statusPanelMain.add("0.01,0.10;0.99,0.90",statusPanel);
	statusPanelMain.setBorder(new EtchedBorder(0));

	JPanel obsPanel = getObservePanel();
	obsPanel.setBackground(Color.green);
	insPanel.setBackground(Color.orange);
	obsSetupPanel.setBackground(Color.cyan);
	setupPanel.setLayout(new RatioLayout());
	EPICSApplyButton applyButton = new EPICSApplyButton();
	//setupPanel.add(EPICSApplyButton.consistantLayout,new EPICSApplyButton());
        setupPanel.add(EPICSApplyButton.consistantLayout, applyButton);
	
	setupPanel.add("0.30,0.90;0.65,0.09",buttonPanel);

	setupPanel.add("0.01,0.01;0.35,0.85",insPanel);
	setupPanel.add("0.38,0.01;0.30,0.31",obsSetupPanel);
	setupPanel.add("0.70,0.01;0.25,0.42",statusPanelMain);
	setupPanel.add("0.38,0.34;0.30,0.33",obsPanel);
	setupPanel.add("0.38,0.69;0.30,0.16",buttonPanel2);

	setupPanel.add("0.01,0.88;0.98,0.01",new JSeparator());

	clock = new UFClock(200,200);
	clock.setBorder(new EtchedBorder(0));
	setupPanel.add("0.73,0.45",clock);

	applyButton.addClock(clock, 10);
	datumButton.addClock(clock, 10);
	observeButton.addClock(clock, 10);

	return setupPanel;
    }
    
    public JPanel getObservePanel() {
	
	JPanel observePanel = new JPanel();

	JPanel obsPanel = new JPanel();
	//String obsPrefix = EPICS.prefix+"observe.";
	obsPanel.setLayout(new RatioLayout());
	obsPanel.add("0.01,0.01;0.99,0.15",new JLabel("Observe", JLabel.CENTER));
	obsPanel.add("0.01,0.16;0.33,0.12",new JLabel("DataLabel"));
        obsPanel.add("0.34,0.16;0.33,0.12",new EPICSTextField(EPICSRecs.dataLabel,""));
        obsPanel.add("0.67,0.16;0.33,0.12",new EPICSLabel(EPICSRecs.valDataLabel,""));

	obsPanel.add("0.01,0.28;0.33,0.12",new JLabel("DataMode"));
        obsPanel.add("0.34,0.28;0.33,0.12",new EPICSTextField(EPICSRecs.dataMode,""));
        obsPanel.add("0.67,0.28;0.33,0.12",new EPICSLabel(EPICSRecs.valDataMode,""));

	obsPanel.add("0.01,0.40;0.33,0.12",new JLabel("DitherIndex"));
        obsPanel.add("0.34,0.40;0.33,0.12",new EPICSTextField(EPICSRecs.ditherIndex,""));
        obsPanel.add("0.67,0.40;0.33,0.12",new EPICSLabel(EPICSRecs.valDitherIndex,""));

	obsPanel.add("0.01,0.52;0.33,0.12",new JLabel("UserInfo"));
        obsPanel.add("0.34,0.52;0.33,0.12",new EPICSTextField(EPICSRecs.userInfo,""));
        obsPanel.add("0.67,0.52;0.33,0.12",new EPICSLabel(EPICSRecs.valUserInfo,""));

        obsPanel.add("0.01,0.64;0.33,0.12",new JLabel("PixLutFile"));
        obsPanel.add("0.34,0.64;0.33,0.12",new EPICSTextField(EPICSRecs.pixLutFile,""));
        obsPanel.add("0.67,0.64;0.33,0.12",new EPICSLabel(EPICSRecs.valPixLutFile,""));
/*
	if (EPICS.PORTABLE) {
	    obsPanel.add("0.01,0.81;0.33,0.15",new JLabel("PixLutFile"));
	    obsPanel.add("0.34,0.81;0.33,0.15",new EPICSTextField(obsPrefix+"PixLUTFile",""));
	    obsPanel.add("0.67,0.81;0.33,0.15",new EPICSLabel(obsPrefix+"VALPixLUTFile",""));
	}else {
	}
*/

        JButton obsHistoryButton = new JButton("History");
        obsPanel.add("0.33,0.80;0.33,0.12",obsHistoryButton);

	obsPanel.setBorder(new EtchedBorder(0));

	return obsPanel;
    }

    public JPanelIS() {

	me = this;
	historyHash = new HashMap();
	setLayout(new BorderLayout());
	add(getSetupPanel(),BorderLayout.CENTER);
    }

    public void showHistory() {
	if (historyFrame == null){
	    historyFrame = new JFrame("Instrum Setup History");
	    historyFrame.setSize(600,400);
	}
	updateHistoryFrame();
	historyFrame.setVisible(true);
    }
    
    public String [] beautifyKeySet(Object [] keySet) {
	String [] retVal = new String[keySet.length];
	for (int i=0; i<keySet.length; i++)
	    if (keySet[i] instanceof GregorianCalendar) {
		GregorianCalendar gc = (GregorianCalendar)keySet[i];
		retVal[i] = gc.get(gc.MONTH)+"/"+gc.get(gc.DAY_OF_MONTH)+" -- "+
		    ( gc.get(gc.HOUR_OF_DAY)  <10?"0"+gc.get(gc.HOUR_OF_DAY):gc.get(gc.HOUR_OF_DAY) )+":"+
		    ( gc.get(gc.MINUTE)<10?"0"+gc.get(gc.MINUTE):gc.get(gc.MINUTE) )+":"+
		    ( gc.get(gc.SECOND)<10?"0"+gc.get(gc.SECOND):gc.get(gc.SECOND) );
		if (historyHash.get(gc) instanceof GregorianCalendar) {
		    GregorianCalendar gc2 = (GregorianCalendar)historyHash.get(gc);
		    retVal[i] += " => "+gc2.get(gc.MONTH)+"/"+gc2.get(gc.DAY_OF_MONTH)+" -- "+
			(gc2.get(gc2.HOUR_OF_DAY)  <10?"0"+gc2.get(gc2.HOUR_OF_DAY):gc2.get(gc2.HOUR_OF_DAY))+":"+
			(gc2.get(gc2.MINUTE)<10?"0"+gc2.get(gc2.MINUTE):gc2.get(gc2.MINUTE))+":"+
			(gc2.get(gc2.SECOND)<10?"0"+gc2.get(gc2.SECOND):gc2.get(gc2.SECOND));
		}
	    } else {
		retVal[i] = "Unknown Key Object";
	    }
	return retVal;
    }

    public String beautifyHashMap(HashMap hashie) {
	String retVal = "";
	Iterator li = hashie.keySet().iterator();
	while (li.hasNext()) {
	    Object ooo = li.next();
	    retVal += ooo.toString()+" -- "+hashie.get(ooo).toString()+"\n";
	}
	return retVal;
    }

    public void updateHistoryFrame() {
	if (historyFrame == null) return;
	JPanel mainPanel = new JPanel(new RatioLayout());
	final JTextArea jta = new JTextArea();
	jta.setEditable(false);
	final Object [] keySet = historyHash.keySet().toArray();
	//sort keys using mergesort (n*log(n))
	Arrays.sort(keySet);
	final JList jli = new JList(beautifyKeySet(keySet));
	jli.addListSelectionListener(new ListSelectionListener(){
		public void valueChanged(ListSelectionEvent e) {
		    JList jl = (JList)e.getSource();
		    if (jl.getSelectedIndex()==-1) {
			jta.setText(""); return;
		    }
		    Object ooo= historyHash.get(keySet[jl.getSelectedIndex()]);
		    while (ooo instanceof GregorianCalendar)
			ooo = historyHash.get(ooo);
		    if (ooo instanceof HashMap)
			jta.setText(beautifyHashMap((HashMap)ooo));
		    else
			System.err.println("JPanelIS.updateHistoryFrame> Unkown object stored in history hashmap!");
		}
	    });
	UFColorButton putButton = new UFColorButton("Put Values",UFColorButton.COLOR_SCHEME_YELLOW);
	putButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    if (jli.getSelectedIndex()==-1) return;
		    Object ooo=historyHash.get(keySet[jli.getSelectedIndex()]);
		    while (ooo instanceof GregorianCalendar)
			ooo = historyHash.get(ooo);
		    if (ooo instanceof HashMap) {
			HashMap hashie = (HashMap)ooo;
			Object [] epicsKeys = hashie.keySet().toArray();
			for (int i=0; i<epicsKeys.length; i++) {
			    if (epicsKeys[i] instanceof String) {
				Object newOoo = hashie.get(epicsKeys[i]);
				if (newOoo instanceof String) {
				    EPICS.put((String)epicsKeys[i],(String)newOoo);
				}
			    }
			}
		    }
		}
	    });
	JButton removeButton = new JButton("Remove Selected");
	removeButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    if (jli.getSelectedIndex()==-1) return;
		    historyHash.remove(keySet[jli.getSelectedIndex()]);
		    //historyFrame.setVisible(false);
		    updateHistoryFrame();
		    historyFrame.setVisible(true);
		}
	    });
	mainPanel.add("0.01,0.01;0.45,0.88",new JScrollPane(jli));
	mainPanel.add("0.54,0.01;0.45,0.88",new JScrollPane(jta));
	mainPanel.add("0.11,0.90;0.25,0.09",removeButton);
	mainPanel.add("0.64,0.90;0.25,0.09",putButton);
	historyFrame.setContentPane(mainPanel);
	historyFrame.repaint();
	
    }

    private void _saveIS() {
	if (insPanel == null) return;
	Component[] comp = insPanel.getComponents();
	HashMap hashie = new HashMap();
	for (int i=0; i<comp.length; i++)
	    if (comp[i] instanceof EPICSComboBox) {
		EPICSComboBox e = (EPICSComboBox)comp[i];
		hashie.put(e.getPutRec(),e.getSelectedItem());
	    }else if (comp[i] instanceof EPICSTextField) {
		EPICSTextField e = (EPICSTextField)comp[i];
		hashie.put(e.getPutRec(),e.getText());
	    }
	Iterator it = historyHash.keySet().iterator();
	while (it.hasNext()) {
	    Object ooo = it.next();
	    if (hashie.equals(historyHash.get(ooo))){
		    historyHash.put(new GregorianCalendar(),ooo);
		    updateHistoryFrame();
		    if (historyFrame != null && historyFrame.isVisible()) historyFrame.setVisible(true);
		    return;
		}
	}
	historyHash.put(new GregorianCalendar(),hashie);
	updateHistoryFrame();
	if (historyFrame != null && historyFrame.isVisible()) historyFrame.setVisible(true);
    }

    public static void saveInstrumSetup() {
	me._saveIS();
    }

}

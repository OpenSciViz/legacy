; public static final String rcsID = "$Name:  $ $Id: status_recs.txt 14 2008-06-11 01:49:45Z hon $"
0.0. Dummy Parameters

trecs:cc:sectWhl:init.A
String
-
-
-
SectorWheel Initial Speed

trecs:cc:sectWhl:init.B
String
-
-
-
SectorWheel Terminal Speed

trecs:cc:sectWhl:init.C
String
-
-
-
SectorWheel Acceleration

trecs:cc:sectWhl:init.D
String
-
-
-
SectorWheel Deceleration

trecs:cc:sectWhl:init.E
String
-
-
-
SectorWheel Drive Current


1.1. Observing Parameters

background
String
-
BKND
Start
Current background flux condition: "nominal" || "high"

cameraMode
String
-
CAMMODE
Start
Current Camera Mode: "imaging" || "spectroscopy"

dataMode
String
-
-
-
Current Data Mode: "save" || "discard"

obsID
String
-
OBSID
Start
Current observation ID provided by OCS

obsMode
String
-
OBSMODE
Start
Current Observing Mode: "chop=nod" || "stare" || "chop" || "nod"

obsType
String
-
OBSTYPE
Start
Observation type (e.g. "image", "spectrum", "dark", "flat", ...)

onSourceTime
Float
Minutes
ONSRCTM
Start
Current total on-source exposure time

photonTime
Float
Minutes
PHOTONTM
Start
Current total photon collection time.  In "chop-nod", "chop", and "nod" modes this time is equal to twice onSourceTime since only half the time is spent on-source.  In stare mode, this time is equal to onSourceTime


1.2. Acquisition Parameters

chopFreq
Float
Hz
CHPFREQ
Start
Current secondary chopping frequency

chopDelay
Float
mS
CHPDDLY
Start
Current chopper "dead" time allowing for chopper transition.  This value is always greater than or equal to the requested chop delay

frameTime
Float
mS
FRMTM
Start
Current detector readout frame integration time

nodDelay
Float
Seconds
NODDLY
Start
Current "dead" time allowing for telescope nod.  This value is always greater than or equal to the requested nod delay

nodDwell
Float
Seconds
NODDWELL
Start
Current dwell time in each telescope beam

saveFreq
Float
Hz
SVFREQ
Start
Current save frequency


1.3. Optical Configuration

apertureName
String
-
APERTURE
Start
Aperture wheel position name (mnemonic)

filterName
String
-
FILTER
Start
Filter wheel position name (mnemonic)

gratingName
String
-
GRATING
Start
Grating wheel position name (mnemonic)

imagingMode
String
-
IMGMODE
Start
Current Imaging Mode: "field" || "window" || "pupil"

lensName
String
-
LENS
Start
Lens wheel position name (mnemonic)

lyotName
String
-
LYOT
Start
Lyot wheel position name (mnemonic)

sectorName
String
-
SECTOR
Start
Sector wheel position name (mnemonic)

slitName
String
-
SLIT
Start
Slit wheel position name (mnemonic)

windowName
String
-
WINDOW
Start
Window wheel position name (mnemonic)


1.4. Observation Progress

backgroundAdcCurrent
Integer
ADUs
-

Current filling of detector well due to background in ADUs

backgroundAdcEnd
Integer
ADUs
BKAEND
End
Filling of detector well due to background at end of observation in

backgroundAdcStart
Integer
ADUs
BKASTART
Start
Filling of detector well due to background at beginning of observation in ADUs

backgroundWellCurrent
Float
Percent
-
-
Current filling of detector well due to background (where 0% is empty well and 100% is full well)

backgroundWellEnd
Float
Percent
BKWEND
End
Filling of detector well due to background at end of observation (where 0% is empty well and 100% is full well)

backgroundWellStart
Float
Percent
BKWSTART
Start
Filling of detector well due to background at beginning of observation (where 0% is empty well and 100% is full well)

currentBeam
String
-
-
-
Contains the current beam position of the telescope

currentNodCycle
Integer
Cycles
-
-
Contains the current nod cycle for an ongoing observation

observationStatus
String
-
-
-
Status of observation sequence

sigmaPerFrameRead
Float
ADUs
SIGFRM
End
Current sigma per frame readout in units of ADUs


1.5. Instrument status       

detTempOK
Boolean
-
-
-
Flag indicates whether the detector temperature has stabilized at the temperature controller setpoint (i.e. heaterSetpoint) to within the acceptable error band

health
String
-
INHEALTH
Start
Instrument health

heartbeat
Integer
-
-
-
Continuously changing variable used to detect whether the system is still alive

historyLog
String
-
-
-
Detector Controller message log

issPort
Integer
-
INPORT
Start
Number of ISS port on which the instrument is installed

name
String
-
INSTRUME
Start
Instrument Name

state
String
-
INSTATE
Start
Instrument State


2.1. Optical Configuration Defaults

loRes10Blocker
String
-
-
-
filterWheelPosition name of blocker for loRes10 spectroscopy mode

hiRes10Blocker
String
-
-
-
filterWheelPosition name of blocker for hiRes10 spectroscopy mode

loRes10Blocker
String
-
-
-
filterWheelPosition name of blocker for loRes20 spectroscopy mode


2.2. Cryostat Window Parameters

kBrHumidityLimit
Float
Percent
-
-
Indicates maximum relative humidity permitted at the dewar window for which the KBr window can be used

windowRelativeHumidity
Float
Percent
-
-
Indicates the relative humidity corresponding to the cryostat window temperature (i.e. TSWindow) calculated using the dome environment relative humidity and temperature


2.3. World Coordinate System Information
	
wcsDec
Double
Radians
-
-
Dec of the center of projection of detector surface

wcsRA
Double
Radians
-
-
RA of the center of projection of detector surface

wcsTai
Double
Gemini raw units 
-
-
??? 

wcs11
Double
mm
-
-
World Coordinate System transformation information. The 6 coefficients which define the transformation from sky coordinates into (X, Y) coordinates at the detector surface.  See tcs-ptw-008

wcs12
Double
mm
-
-


wcs13
Double
mm
-
-


wcs21
Double
mm
-
-


wcs22
Double
mm
-
-


wcs23
Double
mm
-
-



2.4. Miscellaneous Instrument Sequencer status information

configurationTimeout
Float
Seconds
-
-
Timeout value for instrument reconfiguration time


3.1. Detector configuration
                            
chopCoaddUc
Integer
Chop cycles
CHPCOADD
Start
Number of chop cycles to coadd in hardware coadders before saving pair of chopped image buffers (i.e. a saveset) for permanent data archive.  This parameter is only used in "chop-nod" and "chop" mode

chopSettleUf
Integer
Frame readouts
CHPSTTL
End
Number of frames to discard to allow for secondary chopper to settle.  This parameter will normally only be used in an engineering mode when there is no handshake from the SCS.  This parameter is only used in "chop-nod" or "chop" mode

frameCoaddUf
Integer
Frame readouts
FRMCOADD
Start
Number of Frames to coadd in hardware coadders: (a) per half-chop cycle in "chop" or "chop-nod" mode; (b) per saveset in "stare" or "nod"

nodSets
Integer
-
NODSETS
End
Number of nod cycles per observation.  This parameter is only used in "chop-nod" or "nod" mode

nodSettleUc
Integer
Chop cycles
NODSTTLC
End
Number of chop cycles to discard to allow time for telescope to complete beamswitch and settle.  This parameter will normally only be used in an engineering mode when there is no handshake from the OCS.  This parameter is only used in "chop-nod" mode

nodSettleUF
Integer
Frame readouts
NODSTTLF
End
Number of readout frames to discard to allow time for telescope to complete beamswitch and settle.  This parameter will normally only be used in an engineering mode when there is no handshake from the OCS.  This parameter is only used in "nod" mode

pixelClocks
Integer
-
PIXCLKS
Start
Number of hardware clocks per array pixel readout.  Because the frame readout time is an integral multiple of the array clocks, only discrete values of the frame readout time are obtainable

postChopsUc
Integer
Chop cycles
POSTCHPS
End
Number of chop cycles to discard at the termination of an observation to allow the secondary chopper to stop gracefully.  This parameter depends on the SCS and may not be required for Gemini

preChopsUc
Integer
Chop cycles
PRECHPS
End
Number of chop cycles to discard at the initiation of an observation to allow the secondary chopper to stabilize.  This parameter depends on the SCS and may not be required for Gemini

saveSets
Integer
-
SAVESETS
Start
Number of image buffers (or image buffer pairs if chopping) to save: (a) per beam position of the telescope in "chop-nod" or "nod" modes; (b) per observation in "chop" or "stare" mode

readoutMode
Enumer-ation
-
DETMODE
Start
Detector readout mode: either single sample destructive read normally used in broadband imaging mode or a non-destructive read mode (in which multiple samples up the integration ramp are taken before detector reset) used in low flux conditions


3.2. Readout status

acq
Boolean
-
-
-
Flag to indicate that the Detector Controller is acquiring data.  While data is being acquired, both ACQ and RDOUT will be TRUE.  During a CHOP-NOD or NOD mode observation, RDOUT will go FALSE while ACQ remains TRUE to indicate to the OCS that a beamswitch may be initiated.  Once the beamswitch completes, RDOUT will be reset to TRUE.  When an observation completes, both ACQ and RDOUT will be reset to FALSE

backgroundCheckFlag
Boolean
-
-
-
Flag to indicate whether background flux is filling detector wells within acceptable limits (i.e. minDetWell and maxDetWell).

beamswitchHandshake
Boolean
-
-
-
Indicates whether or not beamswitch handshaking is in effect

bunit
String
ADU
BUNIT
Start
Units of quantity read from detector array

dataLabel
String

DHSLABEL
Start
Most recent DHS data label for data to be downloaded to the DHS

elapsed
Float
Seconds
ELAPSED
End
Total elapsed time.  This is equal to photonTimeActual divided by photonEfficiency

exposed
Float
Seconds
EXPTIME
End
Actual total photon collection time.  In "chop-nod", "chop", and "nod" modes this time is equal to twice onSourceTimeActual since only half the time is spent on-source.  In stare mode, this time is equal to requested total photon collection time

exposedRQ
Float
Seconds
EXPRQ
-
Requested total photon collection time.  In "chop-nod", "chop", and "nod" modes this time is equal to twice onSourceTime since only half the time is spent on-source.  In stare mode, this time is equal to onSourceTime

prep
Boolean
-
-
-
Flag to indicate that the Detector Controller is "preparing" to make an observation i.e. waiting for detector temperature to stabilize and taking a sample exposure to verify detector %well is within acceptable range

rdout
Boolean
-
-
-
Flag to indicate that the Detector Controller is acquiring data.  While data is being acquired, both ACQ and RDOUT will be TRUE.  During a CHOP-NOD or NOD mode observation, RDOUT will go FALSE while ACQ remains TRUE to indicate to the OCS that a beamswitch may be initiated.  Once the beamswitch completes, RDOUT will be reset to TRUE.  When an observation completes, both ACQ and RDOUT will be reset to FALSE

utend
String
TBD
UTEND
End
UT at end of observation

utnow
String
TBD
-
-
UT time now

utstart
String
TBD
UTSTART
Start
UT at beginning of observation


3.3. Acquisition Efficiency

chopEfficiency
Float
Percent
CHPEFF
Start
This is the chopper efficiency and is given by chop dwell time divided by half the chop period.  It accounts for chopper transition and setting

nodEfficiency
Float
Percent
NODEFF
Start
This is the telescope beamswitch efficiency and is given by nod dwell time divided by half the nod period.  It accounts for the nod transition and setting times

onSrcEfficiency
Float
Percent
SRCEFF
Start
This is the "on-source" photon collection efficiency.  In "chop", "chop-nod", or "nod" mode it is equal to half the photonEfficiency (since only half the time is spent on-source).  In "stare" mode it is equal to the photonEfficiency

photonEfficiency
Float
Percent
PHOTEFF
Start
This is the photon collection efficiency and is given by the product of all the preceding efficiencies and the Detector Controller frameEfficiency

frameEfficiency
Float
Percent
FRMEFF
Start
This is the current frame readout efficiency


3.4. Acquisition Parameter Override Flags

frameTimeOverrideFlag
Boolean
-
FTMOVRD
Start
Indicates whether or not manual override of frameTime is in effect

chopFreqOverrideFlag
Boolean
-
CFROVRD
Start
Indicates whether or not manual override of chopFreq is in effect

chopDelayOverrideFlag
Boolean
-
CDLOVRD
Start
Indicates whether or not manual override of chopDelay is in effect

nodDwellOverrideFlag
Boolean
-
NDWOVRD
Start
Indicates whether or not manual override of nodDwell is in effect

nodDelayOverrideFlag
Boolean
-
NDLOVRD
Start
Indicates whether or not manual override of nodDelay is in effect

saveFreqOverrideFlag
Boolean
-
SVFOVRD
Start
Indicates whether or not manual override of saveFreq is in effect


3.5. Detector Operating Limits

detTempErrorBand
Float
Kelvins
-
-
Acceptable deviation of detector temperature from temperature controller heater setpoint value (i.e. heaterSetpoint) for valid observation

maxDetWell
Float
Percent
-
-
Maximum permitted value to which the detector well may be filled as a percentage of the detector full well

minDetWell
Float
Percent
-
-
Minimum permitted value to which the detector well may be filled as a percentage of the detector full well


3.6. Detector Bias Values

vcurg
Float
Volts
VCURG
Start
Current "curG" bias voltage

vcurs
Float
Volts
VCURS
Start
Current "curS" bias voltage

vdd
Float
Volts
VDD
Start
Current "vdd" bias voltage

vdetsub
Float
Volts
VDETSUB
Start
Current "vdi" bias voltage

vdi
Float
Volts
VDI
Start
Current "vdi" bias voltage

vrst
Float
Volts
VRST
Start
Current "vrst" bias voltage


3.7. Detector Temperature Settings

hPowerReq
String
-
-
-
Desired heater setpoint (i.e. control) power

hPowerDefault
String
-
-
-
Heater power level for nominal background flux conditions

hPowerLowBackground
String
-
-
-
Heater power level for low background flux conditions

hSetpointReq
Float
Kelvins
-
-
Desired heater setpoint (i.e. control) temperature

hSetpointDefault
Float
Kelvins
-
-
Heater temperature setpoint for nominal background flux conditions

hSetpointLowBackground
Float
Kelvins
-
-
Heater temperature setpoint for low background flux conditions


3.8. Sub-System Status Information

detiD
String
-
DETID
Start
Detector array chip identifier

detType
String
-
DETTYPE
Start
Description of detector array type

dcHealth
String
-
CCHEALTH
Start
Detector Controller health

dcHeartbeat
Integer
-
-
-
Continuously changing variable used to detect whether the system is still alive

dcName
String
-
DCNAME
Start
Detector Controller name

dcState
String
-
DCSTATE
Start
Detector Controller state


4.1. Wheel Positions

aperturePos
Long
-
-
-
Aperture wheel position 

coldClampPos
Long
-
-
-
Cold Clamp wheel position

filter1Pos
Long
-
-
-
Filter wheel 1 position 

filter2Pos
Long
-
-
-
Filter wheel 2 position

gratingPos
Long
-
-
-
Grating wheel position 

lyotPos
Long
-
-
-
Lyot stop wheel position

pupilPos
Long
-
-
-
Pupil wheel position 

sectorPos
Long
-
-
-
Sector wheel position 

slitPos
Long
-
-
-
Slit wheel position 

windowPos
Long
-
-
-
Window changer wheel position 


4.2. Motor Raw Positions

apertureRawPos
Long
Steps
-
-
Current aperture motor position 

coldClampRawPos
Long
Steps
-
-
Current cold clamp motor position

filter1RawPos
Long
Steps
-
-
Current filter 1 motor position 

filter2RawPos
Long
Steps
-
-
Current filter 2 motor position

gratingRawPos
Long
Steps
-
-
Current grating motor position 

lyotRawPos
Long
Steps
-
-
Current lyot stop motor position

pupiRawlPos
Long
Steps
-
-
Current pupil motor position 

sectorRawPos
Long
Steps
-
-
Current sector motor position 

slitRawPos
Long
Steps
-
-
Current slit motor position 

windowRawPos
Long
Steps
-
-
Current window changer motor position 


4.3. Heater Control Status

heaterPower
String

-

Current heater power level

heaterSetpoint
Float
Kelvins
-

Current heater setpoint (i.e. control) temperature

heaterHeartbeat
Integer

-

Continuously changing variable used to detect whether the system is still alive


4.4. Temperature Sensors

tSActiveShield
Float
Kelvins
TSACSHLD

Current active shield temperature readout

tSActiveShieldHealth
String

-

Active shield temperature sensor health

tSActiveShieldLimit
Float
Kelvins
-

Highest "healthy" active shield temperature

tSActiveShieldNominal
Float
Kelvins
-

Nominal active shield temperature

tSColdhead1
Float
Kelvins
TSCH1

Current 1st stage coldhead temperature readout

tSColdhead1Health
String

-

1st stage coldhead temperature sensor health

tSColdhead1Limit
Float
Kelvins
-

Highest "healthy" 1st stage coldhead temperature

tSColdhead1Nominal
Float
Kelvins
-

Nominal 1st stage coldhead temperature

tSColdhead2
Float
Kelvins
TSCH2

Current 2nd stage coldhead temperature readout

tSColdhead2Health
String

-

2nd stage coldhead temperature sensor health

tSColdhead2Limit
Float
Kelvins
-

Highest "healthy" 2nd stage coldhead temperature

tSColdhead2Nominal
Float
Kelvins
-

Nominal 2nd stage coldhead temperature

tSDetector
Float
Kelvins
TSDET

Current detector temperature readout

tSDetectorEnd
Float
Kelvins
TSDEND
End
Detector temperature readout at end of observation

tSDetectorHealth
String

-

Detector temperature sensor health

tSDetectorLimit
Float
Kelvins
-

Highest "healthy" detector temperature

tSDetectorNominal
Float
Kelvins
-

Nominal detector temperature

tSDetectorStart
Float
Kelvins
TSDSTART
Start
Detector temperature readout at start of observation

tSOptics1
Float
Kelvins
TSOP1

Current optics bench position 1 temperature readout

tSOptics1Health
String

-

Optics bench position 1 temperature sensor health

tSOptics1Limit
Float
Kelvins
-

Highest "healthy" optics bench position 1 temperature

tSOptics1Nominal
Float
Kelvins
-

Nominal optics bench position 1 temperature

tSOptics2
Float
Kelvins
TSOP2

Current optics bench position 2 temperature readout

tSOptics2Health
String

-

Optics bench position 2 temperature sensor health

tSOptics2Limit
Float
Kelvins
-

Highest "healthy" optics bench position 2 temperature

tSOptics2Nominal`
Float
Kelvins
-

Nominal optics bench position 2 temperature

tSPassiveShield
Float
Kelvins
TSPASHLD

Current passive shield temperature readout

tSPassiveShieldHealth
String

-

Passive shield temperature sensor health

tSPassiveShieldLimit
Float
Kelvins
-

Highest "healthy" passive shield temperature

tSPassiveShieldNominal
Float
Kelvins
-

Nominal passive shield temperature

tSWindow
Float
Kelvins
TSWIN

Current window temperature readout

tSWindowEnd
Float
Kelvins
TSWEND
End
Window turret temperature readout at end of observation

tSWindowHealth
String

-

Window turret temperature sensor health

tSWindowLimit
Float
Kelvins
-

Highest "healthy" window turret temperature

tSWindowNominal
Float
Kelvins
-

Nominal window turret temperature

tSWindowStart
Float
Kelvins
TSWSTART
Start
Window turret temperature readout at start of observation

cryostatPressure
Float
Milli Torrs
-

Cryostat pressure


4.5. Sub-System Status

ccHealth
String

CCHEALTH
Start
Components Controller health

ccHeartbeat
Integer

-

Continuously changing variable used to detect whether the system is still alive

ccName
String

CCNAME

Components Controller name

ccState
String

CCSTATE

Components Controller state

wfsBeam
Boolean

WFSBEAM

Always FALSE for TReCS since no OIWFS


5.1. Internal status records provided

trecs:dc:acq
bi



Flags to indicate that the Detector Controller is acquiring data.  While data is being acquired, both acq and rdout will be TRUE.  During a CHOP-NOD or NOD mode observation, rdout will go FALSE while acq remains TRUE to indicate to the OCS that a beamswitch may be initiated.  Once the beamswitch completes, rdout will be reset to TRUE.  When an observation completes, both acq and rdout will be reset to FALSE

trecs:dc:rdout
bi





trecs:dc:prep
bi



Flag to indicate that the Detector Controller is "preparing" to make an observation i.e. waiting for detector temperature to stabilize and taking a sample exposure to verify detector %well is within acceptable range


6.1. External status records required

TBD
TBD
TCS
TBD

RA

TBD
TBD
TCS
TBD

DEC

TBD
TBD
TBD
TBD

Time

TBD
TBD
TBD
TBD

Dome Humidity


eod



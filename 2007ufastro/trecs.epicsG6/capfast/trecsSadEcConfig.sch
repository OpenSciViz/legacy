[schematic2]
uniq 47
[tools]
[detail]
s 2240 -128 100 0 Gemini Thermal Region Camera System
s 2096 -176 200 1792 T-ReCS
s 2432 -192 100 256 T-ReCS EC Configure
s 2320 -240 100 1792 Rev: A
s 2096 -240 100 1792 2001/01/25
s 2096 -272 100 1792 Author: NWR
s 2528 -240 100 1792 trecsSadEcConfigure.sch
s 2016 2064 100 1792 A
s 2240 2064 100 1792 Initial Layout
s 2480 2064 100 1792 NWR
s 2624 2064 100 1792 2001/01/25
[cell use]
use changeBar 1984 2023 100 0 changeBar#46
xform 0 2336 2064
use esirs -192 1671 100 0 esirs#40
xform 0 16 1824
p -128 1632 100 0 1 SCAN:Passive
p -32 1664 100 1024 -1 name:$(top)heaterPower
use esirs -192 1255 100 0 esirs#39
xform 0 16 1408
p -128 1216 100 0 1 SCAN:Passive
p -32 1248 100 1024 -1 name:$(top)heaterSetpoint
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: trecsSadEcConfig.sch,v 0.1 2003/02/15 03:02:14 trecs beta $

[schematic2]
uniq 47
[tools]
[detail]
s 2240 -128 100 0 Gemini Thermal Region Camera System
s 2096 -176 200 1792 T-ReCS
s 2432 -192 100 256 T-Recs CC Engineering
s 2320 -240 100 1792 Rev: A
s 2096 -240 100 1792 2000/11/12
s 2096 -272 100 1792 Author: NWR
s 2528 -240 100 1792 trecsSadCcEngineering.sch
s 2016 2064 100 1792 A
s 2240 2064 100 1792 Initial Layout
s 2480 2064 100 1792 NWR
s 2624 2064 100 1792 2000/11/12
[cell use]
use changeBar 1984 2023 100 0 changeBar#46
xform 0 2336 2064
use esirs 544 7 100 0 esirs#35
xform 0 752 160
p 608 -32 100 0 1 SCAN:Passive
p 704 0 100 1024 -1 name:$(top)winChngrRawPos
use esirs 544 423 100 0 esirs#34
xform 0 752 576
p 608 384 100 0 1 SCAN:Passive
p 704 416 100 1024 -1 name:$(top)slitWhlRawPos
use esirs 544 839 100 0 esirs#33
xform 0 752 992
p 608 800 100 0 1 SCAN:Passive
p 704 832 100 1024 -1 name:$(top)sectWhlRawPos
use esirs 544 1255 100 0 esirs#32
xform 0 752 1408
p 608 1216 100 0 1 SCAN:Passive
p 704 1248 100 1024 -1 name:$(top)pplImgRawPos
use esirs 544 1671 100 0 esirs#31
xform 0 752 1824
p 608 1632 100 0 1 SCAN:Passive
p 704 1664 100 1024 -1 name:$(top)lyotWhlRawPos
use esirs -192 7 100 0 esirs#30
xform 0 16 160
p -128 -32 100 0 1 SCAN:Passive
p -32 0 100 1024 -1 name:$(top)gratingRawPos
use esirs -192 423 100 0 esirs#29
xform 0 16 576
p -128 384 100 0 1 SCAN:Passive
p -32 416 100 1024 -1 name:$(top)filterWhl2RawPos
use esirs -192 839 100 0 esirs#28
xform 0 16 992
p -128 800 100 0 1 SCAN:Passive
p -32 832 100 1024 -1 name:$(top)filterWhl1RawPos
use esirs -192 1671 100 0 esirs#7
xform 0 16 1824
p -128 1632 100 0 1 SCAN:Passive
p -32 1664 100 1024 -1 name:$(top)aprtrWhlRawPos
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: trecsSadCcEngineering.sch,v 0.1 2003/02/11 21:42:46 rambold beta $

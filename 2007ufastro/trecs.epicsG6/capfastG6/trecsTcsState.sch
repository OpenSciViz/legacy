[schematic2]
uniq 170
[tools]
[detail]
w 104 1883 100 0 n#168 hwin.hwin#162.in 96 1872 160 1872 ewaits.ewaits#163.INAN
w 440 1819 100 0 n#167 ewaits.ewaits#163.VAL 384 1808 544 1808 eaos.eaos#169.DOL
w 472 1787 100 0 n#166 ewaits.ewaits#163.FLNK 384 1840 448 1840 448 1776 544 1776 eaos.eaos#169.SLNK
w 824 1755 100 0 n#165 eaos.eaos#169.OUT 800 1744 896 1744 hwout.hwout#164.outp
w 824 1355 100 0 n#156 ebos.ebos#160.OUT 800 1344 896 1344 hwout.hwout#157.outp
w 472 1387 100 0 n#155 ewaits.ewaits#158.FLNK 384 1440 448 1440 448 1376 544 1376 ebos.ebos#160.SLNK
w 440 1419 100 0 n#154 ewaits.ewaits#158.VAL 384 1408 544 1408 ebos.ebos#160.DOL
w 104 1483 100 0 n#153 hwin.hwin#159.in 96 1472 160 1472 ewaits.ewaits#158.INAN
w 962 1067 100 0 n#151 trecsOneShot.trecsOneShot#146.RUNNING 864 1056 1120 1056 ebos.ebos#149.DOL
w 466 1067 100 0 n#147 ewaits.ewaits#144.FLNK 384 1056 608 1056 trecsOneShot.trecsOneShot#146.RESET
w 104 1099 100 0 n#143 hwin.hwin#145.in 96 1088 160 1088 ewaits.ewaits#144.INAN
w 866 811 100 0 n#139 hwin.hwin#133.in 672 608 800 608 800 800 992 800 ecalcs.ecalcs#129.INPA
w 898 779 100 0 n#137 eaos.eaos#76.VAL 800 384 864 384 864 768 992 768 ecalcs.ecalcs#129.INPB
w 866 427 100 0 n#136 eaos.eaos#76.FLNK 800 416 992 416 ecalcs.ecalcs#129.SLNK
w 472 395 100 0 n#85 ewaits.ewaits#81.FLNK 384 448 448 448 448 384 544 384 eaos.eaos#76.SLNK
w 440 427 100 0 n#84 ewaits.ewaits#81.VAL 384 416 544 416 eaos.eaos#76.DOL
w 88 459 100 0 n#83 hwin.hwin#62.in 32 416 64 416 64 448 160 448 ewaits.ewaits#81.INBN
w 88 491 100 0 n#82 hwin.hwin#60.in 32 512 64 512 64 480 160 480 ewaits.ewaits#81.INAN
w 936 203 100 0 n#78 eaos.eaos#76.OUT 800 352 864 352 864 192 1056 192 hwout.hwout#77.outp
w 824 -21 100 0 n#58 ebos.ebos#54.OUT 800 -32 896 -32 hwout.hwout#57.outp
w 472 11 100 0 n#56 ewaits.ewaits#45.FLNK 384 64 448 64 448 0 544 0 ebos.ebos#54.SLNK
w 440 43 100 0 n#55 ewaits.ewaits#45.VAL 384 32 544 32 ebos.ebos#54.DOL
w 104 107 100 0 n#35 hwin.hwin#52.in 96 96 160 96 ewaits.ewaits#45.INAN
s 2512 -240 100 1792 trecsControl.sch
s 2096 -272 100 1792 Author: WNR
s 2096 -240 100 1792 2001/12/20
s 2320 -240 100 1792 Rev: D
s 2432 -192 100 256 T-ReCS TCS state reader
s 2096 -176 200 1792 T-ReCS
s 2240 -128 100 0 Gemini Thermal Region Camera System
s 2624 2064 100 1792 2002/02/23
s 2480 2064 100 1792 WNR
s 2240 2064 100 1792 Initial Layout
s 2016 2064 100 1792 A
[cell use]
use eaos 544 1687 100 0 eaos#169
xform 0 672 1776
p 608 1648 100 0 1 OMSL:closed_loop
p 720 1680 100 1024 1 name:$(top)windowRh
p 800 1744 75 768 -1 pproc(OUT):PP
use hwout 896 -73 100 0 hwout#57
xform 0 992 -32
p 1120 -32 100 0 -1 val(outp):$(top)dc:TCSISnodCompl.J PP NMS
use hwout 1056 151 100 0 hwout#77
xform 0 1152 192
p 1280 192 100 0 -1 val(outp):$(sad)windowRh.VAL PP NMS
use hwout 896 1303 100 0 hwout#157
xform 0 992 1344
p 1120 1344 100 0 -1 val(outp):$(top)dc:TCSISnodCompl.J PP NMS
use hwout 896 1703 100 0 hwout#164
xform 0 992 1744
p 1120 1744 100 0 -1 val(outp):$(top)dc:TCSISnodCompl.J PP NMS
use ewaits 160 903 100 0 ewaits#144
xform 0 272 1024
p 160 800 100 0 1 CALC:A
p 144 1056 100 1280 -1 INBP:No
p 144 1024 100 1280 -1 INCP:No
p 160 864 100 0 1 OOPT:On Change
p 160 832 100 0 1 SCAN:I/O Intr
p 160 896 100 0 1 name:$(top)tcsHbMonitor
use ewaits 160 295 100 0 ewaits#81
xform 0 272 416
p 160 192 100 0 1 CALC:A+B
p 144 448 100 1280 -1 INBP:Yes
p 144 416 100 1280 -1 INCP:No
p 160 256 100 0 1 OOPT:On Change
p 160 224 100 0 1 SCAN:I/O Intr
p 160 288 100 0 1 name:$(top)rhMonitor
use ewaits 160 -89 100 0 ewaits#45
xform 0 272 32
p 160 -192 100 0 1 CALC:A
p 144 64 100 1280 -1 INBP:No
p 144 32 100 1280 -1 INCP:No
p 160 -128 100 0 1 OOPT:On Change
p 160 -160 100 0 1 SCAN:I/O Intr
p 160 -96 100 0 1 name:$(top)tcsIpMonitor
use ewaits 160 1287 100 0 ewaits#158
xform 0 272 1408
p 160 1184 100 0 1 CALC:A
p 144 1440 100 1280 -1 INBP:No
p 144 1408 100 1280 -1 INCP:No
p 160 1248 100 0 1 OOPT:On Change
p 160 1216 100 0 1 SCAN:I/O Intr
p 160 1280 100 0 1 name:$(top)tcsIpMonitor
use ewaits 160 1687 100 0 ewaits#163
xform 0 272 1808
p 160 1584 100 0 1 CALC:A
p 144 1840 100 1280 -1 INBP:No
p 144 1808 100 1280 -1 INCP:No
p 160 1648 100 0 1 OOPT:On Change
p 160 1616 100 0 1 SCAN:I/O Intr
p 160 1680 100 0 1 name:$(top)tcsIpMonitor
use hwin -96 1047 100 0 hwin#145
xform 0 0 1088
p -336 1088 100 0 -1 val(in):tcs:sad:heartbeat
use hwin 480 567 100 0 hwin#133
xform 0 576 608
p 208 608 100 0 -1 val(in):$(top)kBrRhLimit.VAL
use hwin -96 55 100 0 hwin#52
xform 0 0 96
p -336 96 100 0 -1 val(in):tcs:sad:inPosition
use hwin -160 471 100 0 hwin#60
xform 0 -64 512
p -432 512 100 0 -1 val(in):tcs:sad:currentRH.VAL
use hwin -160 375 100 0 hwin#62
xform 0 -64 416
p -448 416 100 0 -1 val(in):$(top)ec:tempMonG.VALM
use hwin -96 1431 100 0 hwin#159
xform 0 0 1472
p -336 1472 100 0 -1 val(in):tcs:sad:inPosition
use hwin -96 1831 100 0 hwin#162
xform 0 0 1872
p -336 1872 100 0 -1 val(in):tcs:sad:inPosition
use ebos 1120 935 100 0 ebos#149
xform 0 1248 1024
p 1120 896 100 0 1 OMSL:closed_loop
p 1120 864 100 0 1 SCAN:1 second
p 1120 928 100 0 1 name:$(top)tcsIsAlive
p 1376 992 75 768 -1 pproc(OUT):NPP
use ebos 544 -89 100 0 ebos#54
xform 0 672 0
p 544 -128 100 0 1 OMSL:closed_loop
p 688 -96 100 1024 1 name:$(top)tcsInPosition
p 800 -32 75 768 -1 pproc(OUT):PP
use ebos 544 1287 100 0 ebos#160
xform 0 672 1376
p 544 1248 100 0 1 OMSL:closed_loop
p 688 1280 100 1024 1 name:$(top)tcsInPosition
p 800 1344 75 768 -1 pproc(OUT):PP
use changeBar 1984 1991 100 0 changeBar#140
xform 0 2336 2032
use changeBar 1984 2023 100 0 changeBar#141
xform 0 2336 2064
use trecsOneShot 608 935 100 0 trecsOneShot#146
xform 0 736 1056
p 608 880 100 0 1 setTimeout:timeout $(HB_TIMEOUT)
p 608 912 100 0 1 setTimer:timer $(top)tcsHbTimer
use ecalcs 992 327 100 0 ecalcs#129
xform 0 1136 592
p 1056 288 100 0 1 CALC:B>A
p 1056 320 100 0 1 name:$(top)rhTooHigh
use eaos 544 295 100 0 eaos#76
xform 0 672 384
p 608 256 100 0 1 OMSL:closed_loop
p 720 288 100 1024 1 name:$(top)windowRh
p 800 352 75 768 -1 pproc(OUT):PP
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: trecsTcsState.sch,v 0.1 2003/02/15 03:02:14 trecs beta $

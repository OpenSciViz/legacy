[schematic2]
uniq 129
[tools]
[detail]
w 1522 939 100 0 n#127 hwin.hwin#128.in 1440 928 1664 928 egenSub.egenSub#99.INPB
w 2050 491 100 0 n#125 egenSub.egenSub#99.OUTI 1952 480 2208 480 hwout.hwout#126.outp
w 962 1323 100 0 n#122 efanouts.efanouts#100.LNK1 704 352 864 352 864 1312 1120 1312 eseqs.eseqs#93.SLNK
w 1522 1003 100 0 n#121 hwin.hwin#120.in 1440 992 1664 992 egenSub.egenSub#99.INPA
w 330 283 100 0 n#119 ecad2.ecad2#48.STLK 256 272 464 272 efanouts.efanouts#100.SLNK
w 1154 331 100 0 n#117 efanouts.efanouts#100.LNK2 704 320 1664 320 egenSub.egenSub#99.SLNK
w 2050 555 100 0 n#116 egenSub.egenSub#99.OUTH 1952 544 2208 544 hwout.hwout#115.outp
w 2050 619 100 0 n#113 egenSub.egenSub#99.OUTG 1952 608 2208 608 hwout.hwout#114.outp
w 2050 683 100 0 n#112 egenSub.egenSub#99.OUTF 1952 672 2208 672 hwout.hwout#111.outp
w 2050 747 100 0 n#109 egenSub.egenSub#99.OUTE 1952 736 2208 736 hwout.hwout#110.outp
w 2050 811 100 0 n#108 egenSub.egenSub#99.OUTD 1952 800 2208 800 hwout.hwout#107.outp
w 2050 875 100 0 n#105 egenSub.egenSub#99.OUTC 1952 864 2208 864 hwout.hwout#106.outp
w 2050 939 100 0 n#104 egenSub.egenSub#99.OUTB 1952 928 2208 928 hwout.hwout#103.outp
w 2050 1003 100 0 n#102 egenSub.egenSub#99.OUTA 1952 992 2208 992 hwout.hwout#101.outp
w 1032 1611 100 0 n#98 eseqs.eseqs#93.DOL2 1120 1600 992 1600 992 1568 hwin.hwin#96.in
w 1032 1643 100 0 n#97 eseqs.eseqs#93.DOL1 1120 1632 992 1632 992 1664 hwin.hwin#95.in
w 1608 1643 100 0 n#94 eseqs.eseqs#93.LNK1 1440 1632 1824 1632 ecars.ecars#53.IVAL
w 1496 1611 100 0 n#94 eseqs.eseqs#93.LNK2 1440 1600 1600 1600 1600 1632 junction
w -48 891 -100 0 c#89 ecad2.ecad2#48.DIR -64 752 -128 752 -128 880 80 880 80 1200 -32 1200 inhier.DIR.P
w -80 923 -100 0 c#90 ecad2.ecad2#48.ICID -64 720 -160 720 -160 912 48 912 48 1072 -32 1072 inhier.ICID.P
w 192 891 -100 0 c#91 ecad2.ecad2#48.VAL 256 752 320 752 320 880 112 880 112 1200 256 1200 outhier.VAL.p
w 224 923 -100 0 c#92 ecad2.ecad2#48.MESS 256 720 352 720 352 912 144 912 144 1072 256 1072 outhier.MESS.p
s 1440 1008 100 0 context
s 1440 944 100 0 wavelength
s 2624 2064 100 1792 2000/12/05
s 2480 2064 100 1792 WNR
s 2240 2064 100 1792 Initial Layout
s 2016 2064 100 1792 A
s 2624 2032 100 1792 2001/12/25
s 2480 2032 100 1792 WNR
s 2240 2032 100 1792 Add WCS Calculation
s 2016 2032 100 1792 B
s 2512 -240 100 1792 trecsSetWcs.sch
s 2096 -272 100 1792 Author: WNR
s 2096 -240 100 1792 2001/12/25
s 2320 -240 100 1792 Rev: B
s 2432 -192 100 256 Trecs setWcs Command
s 2096 -176 200 1792 T-ReCS
s 2240 -128 100 0 Gemini Thermal Region Camera System
[cell use]
use hwin 1248 887 100 0 hwin#128
xform 0 1344 928
p 944 928 100 0 -1 val(in):$(top)insSetupCopyG.VALC
use hwout 2208 439 100 0 hwout#126
xform 0 2304 480
p 2432 480 100 0 -1 val(outp):$(sad)wcs23
use changeBar 1984 2023 100 0 changeBar#124
xform 0 2336 2064
use changeBar 1984 1991 100 0 changeBar#123
xform 0 2336 2032
use hwin 800 1623 100 0 hwin#95
xform 0 896 1664
p 640 1664 100 0 -1 val(in):$(CAR_BUSY)
use hwin 800 1527 100 0 hwin#96
xform 0 896 1568
p 640 1568 100 0 -1 val(in):$(CAR_IDLE)
use hwin 1248 951 100 0 hwin#120
xform 0 1344 992
p 1072 992 100 0 -1 val(in):tcs:ak:astCtx
use hwout 2208 951 100 0 hwout#101
xform 0 2304 992
p 2432 992 100 0 -1 val(outp):$(sad)wcsTai
use hwout 2208 887 100 0 hwout#103
xform 0 2304 928
p 2432 928 100 0 -1 val(outp):$(sad)wcsDec
use hwout 2208 823 100 0 hwout#106
xform 0 2304 864
p 2432 864 100 0 -1 val(outp):$(sad)wcsRA
use hwout 2208 759 100 0 hwout#107
xform 0 2304 800
p 2432 800 100 0 -1 val(outp):$(sad)wcs11
use hwout 2208 695 100 0 hwout#110
xform 0 2304 736
p 2432 736 100 0 -1 val(outp):$(sad)wcs12
use hwout 2208 631 100 0 hwout#111
xform 0 2304 672
p 2432 672 100 0 -1 val(outp):$(sad)wcs13
use hwout 2208 567 100 0 hwout#114
xform 0 2304 608
p 2432 608 100 0 -1 val(outp):$(sad)wcs21
use hwout 2208 503 100 0 hwout#115
xform 0 2304 544
p 2432 544 100 0 -1 val(outp):$(sad)wcs22
use efanouts 464 135 100 0 efanouts#100
xform 0 584 288
p 512 128 100 0 1 name:$(top)setWcsFanout
p 736 352 75 1280 -1 pproc(LNK1):PP
p 736 320 75 1280 -1 pproc(LNK2):PP
use egenSub 1664 231 100 0 egenSub#99
xform 0 1808 656
p 1728 192 100 0 1 INAM:trecsIsSetWcsGProcess
p 1441 -347 100 0 0 NOA:39
p 1728 160 100 0 1 SNAM:trecsIsNullGInit
p 1728 224 100 0 1 name:$(top)setWcsG
p 1616 746 75 0 -1 pproc(INPE):NPP
p 1616 682 75 0 -1 pproc(INPF):NPP
p 1616 618 75 0 -1 pproc(INPG):NPP
p 1616 554 75 0 -1 pproc(INPH):NPP
p 1952 1002 75 0 -1 pproc(OUTA):PP
p 1952 938 75 0 -1 pproc(OUTB):PP
p 1952 874 75 0 -1 pproc(OUTC):PP
p 1952 810 75 0 -1 pproc(OUTD):PP
p 1952 746 75 0 -1 pproc(OUTE):PP
p 1952 682 75 0 -1 pproc(OUTF):PP
p 1952 618 75 0 -1 pproc(OUTG):PP
p 1952 554 75 0 -1 pproc(OUTH):PP
p 1952 490 75 0 -1 pproc(OUTI):PP
use eseqs 1120 1223 100 0 eseqs#93
xform 0 1280 1472
p 1200 1184 100 0 1 DLY1:0.0e+00
p 1200 1152 100 0 1 DLY2:0.5e+00
p 1312 1216 100 1024 1 name:$(top)setWcsBusy
p 1088 1632 75 1280 -1 pproc(DOL1):NPP
p 1088 1600 75 1280 -1 pproc(DOL2):NPP
p 1456 1632 75 1024 -1 pproc(LNK1):PP
p 1456 1600 75 1024 -1 pproc(LNK2):PP
use outhier 272 1200 100 0 VAL
xform 0 240 1200
use outhier 272 1072 100 0 MESS
xform 0 240 1072
use inhier -96 1200 100 0 DIR
xform 0 -32 1200
use inhier -112 1072 100 0 ICID
xform 0 -32 1072
use ecars 1824 1351 100 0 ecars#53
xform 0 1984 1520
p 1984 1344 100 1024 1 name:$(top)setWcsC
use ecad2 -64 183 100 0 ecad2#48
xform 0 96 496
p 0 144 100 0 1 INAM:trecsIsNullInit
p 0 96 100 0 1 SNAM:trecsIsSetWcsProcess
p 96 176 100 1024 1 name:$(top)setWcs
p 272 272 75 1024 -1 pproc(STLK):PP
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: trecsSetWcs.sch,v 0.1 2003/02/15 03:02:14 trecs beta $

import java.io.*;
import java.net.*;
import java.util.*;
import UFJava_Protocol.*;
import UFJava_Console.*;

public class newTest{
 final static String rcsId ="$Id: newTest.java,v 0.0 2002/06/03 17:44:30 hon beta $";
  public static void main(String args[]){
    final int SIZE =256;
    int portNo;
    /*try{
        System.out.print("Please enter a port # or hit return to use 55555: ");
        DataInputStream dis = new DataInputStream(System.in);
        portNo = dis.readInt();
        }
    catch(Exception e){
        portNo =55555;
    }*/
    UFSocket soc = new UFSocket("newton.astro.ufl.edu", 55555);
    //UFSocket soc = new UFSocket("dolphinus.astro.ufl.edu", 5551);
    System.out.println("\n\n"+soc.socketInfo());
    int[] vals = new int[SIZE];
    byte[] vals2 = new byte[SIZE];
    short[] vals3 =new short[SIZE];
    short[] vals4 =new short[SIZE];
    byte x=1;
    short y=(short)1;
    short z =(short)100;
    for (int i =0; i<SIZE;i++){
        vals[i] =SIZE-i;
        vals2[i] = x++;
        vals3[i] = y++;
        vals4[i] = z++;
    }

    System.out.println("enter a selesction fo the test ");
    System.out.println("UFInts: 1, UFBytes: 2, UFShorts:    3, UFStrings:   4");
    int selection;

    try{
            selection =System.in.read()-48;

            System.out.println(selection);
    }
    catch(Exception e){
        //something wrong
        selection =1;
    }
    switch(selection){
    case 1:
        UFInts ufi =new UFInts("test", vals);
        UFInts ufi2 =new UFInts();
        UFInts ufi3 =new UFInts();
        System.out.println(ufi.toString());
        System.out.println(ufi2.toString());
        ufi.sendTo(soc);
        ufi2.recvFrom(soc);
        ufi2.sendTo(soc);
        ufi3.recvFrom(soc);
        System.out.println(ufi.toString());
        System.out.println(ufi2.toString());
        System.out.println(ufi3.toString());
    break;
    case 2:
        UFBytes ufb =new UFBytes("new byte", vals2);
        UFBytes ufb2 =new UFBytes("second byte", vals2);
        System.out.println(ufb.toString());
        System.out.println(ufb2.toString());
        ufb.sendTo(soc);
        ufb2.recvFrom(soc);
        System.out.println(ufb.toString());
        System.out.println(ufb2.toString());
    break;
    case 3:
        UFShorts ufsh =new UFShorts("first short", vals3);
        UFShorts ufsh2 =new UFShorts("second short", vals3);
        System.out.println(ufsh.toString());
        System.out.println(ufsh2.toString());
        System.out.println(ufsh.valData(SIZE-1));
        System.out.println(ufsh2.valData(SIZE-6));
        ufsh.sendTo(soc);
        ufsh2.recvFrom(soc);
        System.out.println(ufsh.toString());
        System.out.println(ufsh2.toString());
        System.out.println(ufsh.valData(1));
        //System.out.println(ufsh2.valData(1));
    break;
    case 4:
        String[] value =new String[1];
        //value[0]="c home 200 1";
        value[0]="c move +5000";
        //value[2]="c ^";

        UFStrings ufs = new UFStrings("TESTSTRING",value);
        UFStrings ufs2 =new UFStrings("test2string");
        ufs.sendTo(soc);
        ufs2.recvFrom(soc);
        System.out.println(ufs.toString());
        System.out.println(ufs2.toString());
        System.out.println(ufs2.valData(0));
    break;
    default:System.out.println("UNKNOWN OPTION");
    }//end of switch
    soc.close();
    System.out.println("End of program\n");
  }
}

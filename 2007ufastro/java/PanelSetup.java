import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.awt.image.*;
import java.util.*;
import java.math.*;
import java.io.*;
import java.io.RandomAccessFile.*;

/*This class does all the initialization for the panel used in the 
  UFGuiClient class. It uses the UFConnect class for communication with
  the server. */

public class PanelSetup extends JPanel implements ActionListener{

    //Class objects
    
    JPanel p1, p2, p3, si,image;
    JMenuBar menubar= new JMenuBar();
    TableFromFile tff; JTabbedPane jtp= new JTabbedPane();
    JTextArea sentrequest, response, serverLog;
    JTextField request;
    JComboBox commandList, servers, ports;
    JSplitPane splitPane;
    JScrollPane bigImage, serverScroll;
    JMenuItem menuItem;
    JButton apply;
    JMenu Connection =new JMenu("Connection");
    JMenu Command =new JMenu("Commands");
    static int j=0, count=0;
    final Dimension d= new Dimension(110,25);
    Random rand = new Random();
    JLabel label;
    JLabel sent =new JLabel();// to display image before sending... for test purposes
    UFConnect ufc;
    RandomAccessFile RAF; // if you want to get the data from a file
    boolean debug =true;
    /*************************************************************************************/
    /*************************************************************************************/
 
    public PanelSetup(){
	if(debug)
	    System.out.println("\n\nIn Constructor\n\n");
	p1 = new JPanel();
	p1.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
	p1.setLayout(new BorderLayout(1,1));

	si = new JPanel();// panel to let user select server and port
	image = new JPanel();// panel that has the picture on it
	tff = new TableFromFile();// also a JPanel
	
	ufc = new UFConnect();// initializes the server and port
	GridBagLayout gridbag = new GridBagLayout();
	GridBagConstraints c = new GridBagConstraints();
	/*SET overall layout and individual panel layouts*/
	setLayout(gridbag);//this is important for the components to all fit
	setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
	// want the buttons to hug the top of the panel. so set these constraints
	
	c.anchor = GridBagConstraints.NORTHWEST;
	gridbag.setConstraints(menubar,c);
	c.fill = GridBagConstraints.BOTH;
	c.anchor= GridBagConstraints.SOUTH;
	//just added line
	c.gridx =0;
	c.gridy=GridBagConstraints.RELATIVE;
	c.weightx = 1.0; c.weighty = 1.0; 
	gridbag.setConstraints(jtp,c);


	
	image.setLayout(new BorderLayout(1,1));
	
	makeComboBox();
	setButtons();
	setTextArea();
	addComponents();
    }

/*************************************************************************************************/
 /*************************************************************************************************/
    /*This adds all the components that have been initialized to the panel*/
    public void addComponents(){
	// 	try{
	// 	    RAF= new RandomAccessFile("/mnt/dum/share/image.256.data","r");
	// 	}
	// 	catch(IOException ioe){System.out.println("OPEN FAILED\n");}
 	/*Add components to the panels and add the panels to the overall Panel*/
	
	// connection and command should have short cuts
	Command.setMnemonic(KeyEvent.VK_C);
	Connection.setMnemonic(KeyEvent.VK_N);
        p1.add(splitPane);
	Connection.add(commandList);
	
	menubar.add(Command);
	menubar.add(Connection);

	GridBagLayout gridbag = new GridBagLayout();
	GridBagConstraints c = new GridBagConstraints();

	c.anchor = GridBagConstraints.NORTH;
	si.setLayout(gridbag);
	c.gridx=0;
	gridbag.setConstraints(servers,c);
	si.add(servers);

	c.gridx=1;
	gridbag.setConstraints(ports,c);
	si.add(ports);

	c.gridx=2;
	gridbag.setConstraints(apply,c);
	si.add(apply);

	c.gridwidth=3;
	c.gridx=0;
	c.gridy=1;
	c.fill = GridBagConstraints.VERTICAL;
	gridbag.setConstraints(serverScroll,c);
	si.add(serverScroll);

	jtp.addTab("TextArea", p1);
        jtp.addTab("table information",tff);
	jtp.addTab("picture", image);
	jtp.addTab("server info", si);
	add(menubar);
	add(jtp);
	if(debug)
	    System.out.println("addComponents>> done");
    }
 /*************************************************************************************************/
 /*************************************************************************************************/
 
    public void makeComboBox() {
	String add=".astro.ufl.edu";
        String[] commandStrings = { "+", "-", "X", "Y", "F" ,"nothing"};
	String[] serverList ={"dolphinus"+add, "newton"+add,"serpens"+add};
	Vector portList= new Vector();// ={55555, 33333, 57575};
	portList.addElement(new Integer(55555));
	portList.addElement(new Integer(33333));
	portList.addElement(new Integer(57575));
        // Create the combo box, select 
        // Create the combo box, select 
        // Create the combo box, select nothing
        commandList = new JComboBox(commandStrings);
        commandList.setSelectedIndex(5);
	commandList.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JComboBox cb = (JComboBox)e.getSource();
                String []commandName = {(String)cb.getSelectedItem()};
		response.append(commandName[0]+"\n");
		UFStrings ufs =new UFStrings("command", commandName);
		sendReceive(ufs);
            }
        });
       
	//commandList.setPreferredSize(new Dimension(20,20));
       	servers= new JComboBox(serverList);
	servers.setEditable(true);
	servers.setSelectedIndex(0);
	ports = new JComboBox(portList);
	ports.setEditable(true);
	ports.setSelectedIndex(0);
	if(debug)
	    System.out.println("makeComboBox>> done");
    }
	      

/*************************************************************************************************/

 /*************************************************************************************************/
    public void setTextArea(){
	request =new  JTextField("you may type in commands here");
	response =new JTextArea();
	response.setEditable(false);
	response.setLineWrap(true);
	response.setWrapStyleWord(true);

	JScrollPane scrollPane1 = new JScrollPane(request);
	JScrollPane scrollPane2 = new JScrollPane(response);
	scrollPane2.setPreferredSize(new Dimension(400, 300));
	scrollPane2.setMinimumSize(new Dimension(400, 300));
	splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT,
					     scrollPane1, scrollPane2);
        splitPane.setOneTouchExpandable(true);
	splitPane.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

	serverLog = new JTextArea();
	serverLog.setEditable(false);
	serverLog.setLineWrap(true);
	serverLog.setWrapStyleWord(true);
	serverScroll =new JScrollPane(serverLog);
	serverScroll.setMinimumSize(new Dimension(400, 300));
	serverScroll.setPreferredSize(new Dimension(400, 300));
	if(debug)
	    System.out.println("setTextArea>> done");
    }

  /*************************************************************************************************/  
  /*************************************************************************************************/
    void setButtons(){
	menuItem = new JMenuItem("connect", KeyEvent.VK_C);
	menuItem.addActionListener(this);
	Connection.add(menuItem);
	menuItem = new JMenuItem("send_text", KeyEvent.VK_S);
	menuItem.addActionListener(this);
	Connection.add(menuItem);
	menuItem = new JMenuItem("disconnect", KeyEvent.VK_D);
	menuItem.addActionListener(this);
	Connection.add(menuItem);
	menuItem = new JMenuItem("exit", KeyEvent.VK_X);
	menuItem.addActionListener(this);
	Connection.add(menuItem);
	
	menuItem = new JMenuItem("get_strings", KeyEvent.VK_G);
	menuItem.addActionListener(this);
	Command.add(menuItem);
	menuItem = new JMenuItem("images", KeyEvent.VK_I);
	menuItem.addActionListener(this);
	Command.add(menuItem);
	menuItem = new JMenuItem("one_image", KeyEvent.VK_O);
	menuItem.addActionListener(this);
	Command.add(menuItem);

	menuItem = new JMenuItem("refresh", KeyEvent.VK_R);
	menuItem.addActionListener(this);
	Command.add(menuItem);
	menuItem = new JMenuItem("help", KeyEvent.VK_H);
	menuItem.addActionListener(this);
	Command.add(menuItem);

	apply= new JButton("apply");
	apply.setPreferredSize(d);
	apply.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		// should put a text area on the panel with server info
		// append to some text box the current server and the new one
		serverLog.append("Last connected to "
				 +ufc.getLastServer()+ " at port "+ ufc.getLastPort()+"\n");
		serverLog.append("PREVIOUSLY SELECTED SERVER AND PORT "
				 +ufc.getServer()+ " at port "+ ufc.getPort()+"\n");
		ufc.setServer((String)servers.getSelectedItem());
		// this nextline seems convoluted but that is the only way to 
		// get an int out of the combobox
		ufc.setPort(((Integer)ports.getSelectedItem()).intValue());
		serverLog.append("CURRENTLY  SERVER AND PORT "
				 +ufc.getServer()+ " at port "+ ufc.getPort()+"\n");
	    }
	});
	if(debug)
	    System.out.println("setButtons>> done");
    }
    /*************************************************************************************************/
    /*************************************************************************************************/  

    public void actionPerformed(ActionEvent ae) {
	String action = ((JMenuItem)ae.getSource()).getActionCommand();
	response.append(action.toUpperCase()+">>\n");
	final char x = action.charAt(0);
	switch(x){
	case 'c':
	    {
		// init socket if there is no valid connection or if it is not to the currently
		//selected port and server.
		if(!ufc.validConnection()||!ufc.sameServerAndPort()){
		    response.append("connecting to " +
				    ufc.getServer()+" at port # "+ ufc.getPort()+"\n");
		    if(ufc.socketInit(3))  
			response.append("connection established\n");
		    else
			response.append("no connection made...try later\n");
		}
		else response.append("there is a connection established\n");
	    };
	    break;
	case 'd':
	    {
 		ufc.killSocket();
		response.append("You are disconnected from "
				+ ufc.getLastServer()+ " at port "+ ufc.getLastPort()+"\n");
		serverLog.append("Disconnected from" 
				 + ufc.getLastServer()+ " at port "+ ufc.getLastPort()+"\n");
	    };
	    break;	    
	case 'e':
	    {
		response.append("quitting application........");
		//RAF.close();// close data file
		System.exit(0);
	    };
	    break;	    
	case 's':
	    {
		String[] s ={request.getText()};
		UFStrings ufs = new UFStrings("TextBox",s);
		sendReceive(ufs);
	    };
	break;	
	case 'o':
	    {
		Image pic = null;
		int[] temp = {0,0,0};
		UFInts message = new UFInts("message", temp);
		try{
		    temp=((UFInts)sendReceive(message))._values;
		}
	        catch(Exception ex){
		    if(temp==null){
			response.append("ONE_IMAGE>> no size received\n");
			return;
		    }
		}
		int h, w;
		h=temp[0]; w =temp[1];
		
		int[] testInts = new int[h*w];
		UFInts ufi = new UFInts("New UFInts", testInts);
		response.append("Trying to get image from the server..............\n");
		int[] img =null;
		try{
		    img =((UFInts)ufc.getUFObject())._values;
		}
		catch(Exception ex){
		    if(img==null){
			response.append("ONE_IMAGE>> no image received\n");
			return;
		    }
		}
		
		pic=createImage(new MemoryImageSource(h, w, img, 0, w));
 		ImageIcon icon = new ImageIcon(pic);
		if (count==0){
		    label= new JLabel(icon);
		    label.setVisible(true);
		    count++;
		    System.out.println("\nnew label\n"); 
		    bigImage= new JScrollPane(label);
		    bigImage.setVisible(true);
		    
		}
		else
		    label.setIcon(icon);
		
		image.add(bigImage,BorderLayout.CENTER);
		image.repaint();
		response.append("image " + count++ +"\n" );
		img=null;// just for testing cause i am running out of memory
		System.out.println("\nend of image\n");
	    };
	    break;
	case 'i':
	    {
		final int dim =512;
		Image pic = null;
		int[] testInts = new int[dim*dim];
		ImageUtil.testimg(testInts, dim);// create a test image
		UFInts ufi = new UFInts("New UFInts", testInts);
		response.append("Trying to get image from the server..............\n");
		int img[]=null;
		try{
		    img =((UFInts)sendReceive(ufi))._values;

		}
		catch (Exception ex){
		    if(img==null){
			response.append("GET_IMAGE>> no image received\n");
			return;
		    }  
		}
		
		pic=createImage(new MemoryImageSource(dim,dim, img, 0, dim));
		ImageIcon icon = new ImageIcon(pic);
		if (count==0){
		    label= new JLabel(icon);
		    label.setVisible(true);
		    count++;
		    System.out.println("\nnew label\n"); 
		    bigImage= new JScrollPane(label);
		    bigImage.setVisible(true);
		}
		else
		    label.setIcon(icon);
		
		image.add(bigImage,BorderLayout.CENTER);
		image.repaint();
		response.append("image " + count++ +"\n" );
		img=null;// just for testing cause i am running out of memory
		System.out.println("\nend of image\n");
	    };
	    break;       
	case 'g':
	    {	
		String []sarray = {request.getText()};
		UFStrings ufs = new UFStrings("text from box", sarray);
		ufs = (UFStrings)sendReceive(ufs);
	    };
	    break;	
	case 'h':
	    {
		try{
		    response.append(getHelp("help"));
		}
		catch (IOException ioe){
		    response.append("get help >"+ioe+"\n");
		}
	    };
	    break;
	case 'r':
	    {
		response.setText("");
		response.append("CLEAR >>\n" );
	    };
	    break;
	default:
	   response.append("\nUnrecognized COMMAND\n");
	}
    }
    /*************************************************************************************/  
    /**************************************************************************************/
    public String getHelp(String filename) throws IOException{
	String s="", str;
	FileReader FR = new FileReader(filename);
	BufferedReader BR =new BufferedReader (FR);
	while((str=BR.readLine())!=null){
	    // System.out.println("in the while loop"); 
	    s=s+str+"\n";
	}
	System.out.println("done reading the help file");
	//System.out.println("s is "+s);
	BR.close();
	return s;
    }
    /*************************************************************************************/  
    /**************************************************************************************/
    
    public UFProtocol sendReceive(UFTimeStamp uft){
	UFProtocol ufp_ret =null;
	System.out.println("\n\nIN SENDRECEIVE\n\n");
	if(ufc.sendUFTimeStamp(uft)>0){
	    ufp_ret = ufc.getUFObject();
	    if(ufp_ret == null){
		response.append("ufp_ret == null, error receiving");
	    }
	    else{
		response.append("got object> ufp_ret = " + ufp_ret);
	    }
	}
	else{
	    response.append("Sorry, server is down, could not send....\n");
       	}
	if(debug)
	    System.out.println("sendReceive>> done");
	return ufp_ret;
    }
}






import java.io.*;
import java.util.*;
import java.net.*;
import java.applet.*;
import java.awt.*;
import java.awt.image.*;

public class TextScroller extends Canvas {
    String[] s;
    int minWidth;
    int minHeight;
    Color fgColor, bgColor;
    public TextScroller(String[] displayStrings, int min_Width, int min_Height,
			Color fg_color, Color bg_color) {
	super();
	s = displayStrings;
	bgColor = bg_color;
	fgColor = fg_color;
	minWidth = min_Width;
	minHeight = min_Height;
	this.setBackground(bgColor);
    }

    public Dimension preferredSize() {
	return new Dimension(minWidth, figureHeight());
    }

    public Dimension minimumSize() {
	return preferredSize();
    }

  public void writeWindow(String theStatus) {
    s[2] = s[1];
    s[1] = s[0];
    s[0] = theStatus;
    repaint();
  }

  public int figureHeight() {
    Graphics g = getGraphics();
    int font_ht = g.getFontMetrics().getHeight();
    return 3*font_ht + 8; // add 8 to get a little extra room at bottom
  }

  public void paint(Graphics g) {
    int font_ht = g.getFontMetrics().getHeight();

    Dimension size = this.size();
    //    g.setColor(bgColor);
    //    g.fillRect(0,0,size.width-1, size.height-1);  
    g.setColor(Color.black);
    g.drawRect(0,0,size.width-1, size.height-1);

    g.setFont( new Font("TimesRoman", Font.PLAIN, font_ht) );
    g.setColor(fgColor);
    g.drawString(s[2], 8, font_ht);
    g.drawString(s[1], 8, 2*font_ht);
    g.drawString(s[0], 8, 3*font_ht);
  }


} // end of class TextScroller

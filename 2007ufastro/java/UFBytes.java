import java.io.*;
import java.text.*;
import java.net.*;
import java.util.*; 
import java.awt.*; 

public class UFBytes extends UFTimeStamp {
  public UFBytes() {
    _length=_minLength();
    _type=MsgTyp._Bytes_;
    _timestamp = new String("yyyy:ddd:hh:mm:ss.uuuuuu");
    _elem=0; // should be increment to >= 1 
    _name = new String("");
    _seqCnt = _seqTot = 0;
    _duration = 0.0f;
    _values = null;
  }

  public UFBytes(int length) {
    _length=length; //_MinLength_;
    _type=MsgTyp._Bytes_;
    _timestamp = new String("yyyy:ddd:hh:mm:ss.uuuuuu");
    _elem=0; // should be increment to >= 1 
    _name = new String("");
    _seqCnt = _seqTot = 0;
    _duration = 0.0f;
    _values = null;
  }

  public UFBytes(String name, byte[] vals) {
    _name = new String(name);
    _type=MsgTyp._Bytes_;
    _elem=vals.length;
    _values = vals;
    _timestamp = new String("yyyy:ddd:hh:mm:ss.uuuuuu");
    _seqCnt = _seqTot = 0;
    _duration = 0.0f;
    _length = _minLength() + vals.length;
  }

  // all methods declared abstract by UFProtocal 
  // can be defined here
  public String description() { return new String("UFBytes"); }
 
  // return size of the element's value (not it's name!)
  // either string length, or sizeof(float), or sizeof(byte), sizeof(frame):
  public int valSize(int elemIdx) { 
    if( elemIdx >= _values.length )
      return 0;
    else
      return 1;
  }

  public byte[] values() {
    return _values;
  }

  public byte maxVal() {
    byte max=0;
    for( int i=0; i<_values.length; i++ )
      if( _values[i]>max ) max = _values[i];

    return max;
  }

  public int numVals() {
    return _values.length;
  }

  public byte valData(int index) {
    return _values[index];
  }

//   public String name() {
//     return _name;
//   }


///////////////////////////// protected: /////////////////////
  // additional attributes & methods (beyond base class's):
  protected byte[] _values=null;

  protected void _copyNameAndVals(String s, byte[] vals) {
    _name = new String(s);
    _elem = vals.length;
    _values = new byte[vals.length];
    _length = _minLength() + vals.length;
    for( int i=0; i<_elem; i++ ) {
      _values[i] = vals[i];
    }
  }
  public void setNameAndVals(String s, byte[] vals) {
    _name = new String(s);
    _elem = vals.length;
    _values = new byte[vals.length];
    _length = _minLength() + vals.length;
    for( int i=0; i<_elem; i++ ) {
      _values[i] = vals[i];
    }
  }
 
  protected int _dataFrom(DataInputStream inp) {
    // restore everything but length & type
    int retval=0;
    try {
	//length and type have already been read
      _elem = inp.readInt(); // should be 0
      retval += 4;
      _seqCnt = inp.readShort(); // seqcnt and seqtot (both short ints)
      retval += 2;
      _seqTot = inp.readShort();
      retval += 2;
      _duration = inp.readFloat(); // read in 'duration'
      retval+=4;
      byte[] tbuf = new byte[_timestamp.length()];
      inp.readFully(tbuf, 0, _timestamp.length());
      _timestamp = new String(tbuf);
      int namelen = inp.readInt();
      retval += 4;
      byte[] namebuf = new byte[namelen];
      if( namelen > 0 ) {
	inp.readFully(namebuf, 0, namelen);
        retval +=  namelen;
	_name = new String(namebuf);
      }

      _values = new byte[_elem];

      inp.readFully(_values, 0, _elem);

//       for( int elem=0; elem<_elem; elem++ ) {
// 	_values[elem] = inp.readByte();
//         retval += 1;
//       }
    }
    catch(EOFException eof) {
      System.err.println(eof);
    } 
    catch(IOException ioe) {
      System.err.println(ioe);
    }
    catch( Exception e ) {
      System.err.println(e);
    }
    // should compare retval to length-4
    return retval;
  }

  protected int _outputTo(DataOutputStream out) {
    // write out all attributes
    int retval=0;
    int slen=0;
    try {
      out.writeInt(_length);
      retval += 4;
      out.writeInt(_type);
      retval += 4;
      out.writeInt(_elem);
      retval += 4;
      out.writeShort(_seqCnt);//2 shorts -- seqcnt, seqtot
      retval += 2;
      out.writeShort(_seqTot);
      retval += 2;
      out.writeFloat(_duration);//duration
      retval += 4;
      out.writeBytes(_timestamp);
      retval += _timestamp.length();
      out.writeInt(_name.length());
      retval += 4;
      if( _name.length() > 0 ) {
	out.writeBytes(_name);
	retval += _name.length();
      }

      out.write(_values,0,_values.length);

//       for( int elem=0; elem<_elem; elem++ ) {
//         out.writeByte(_values[elem]);
//         retval += 1;
//       }
    }
    catch(EOFException eof) {
      System.err.println(eof);
    } 
    catch(IOException ioe) {
      System.err.println(ioe);
    }
    catch( Exception e ) {
      System.err.println(e);
    }
    return retval;
  }

    public String toString() {
	StringBuffer sb = new StringBuffer();

	sb.append("_length = " + _length + " | ");
	sb.append("_minlength() = " + _minLength() + " | ");
	sb.append("_type = " + _type + " | ");
	sb.append("_timestamp = " + _timestamp + "\n");
	sb.append("_elem = " + _elem + " | ");
	sb.append("_name = " +  _name + " | ");
        sb.append("_values =\n");

	return new String(sb);
    }

    public int width() {
	int retval = 0;

	if (_values.length == 0)
	    retval = 0;
	else if (_values.length == 128*128)
	    retval = 128;
	else if (_values.length == 256*256)
	    retval = 256;
	else
	    retval = 0;   // unrecognized frame size

	return retval;
    }

    public int height() {
	int retval = 0;

	if (_values.length == 0)
	    retval = 0;
	else if (_values.length == 128*128)
	    retval = 128;
	else if (_values.length == 256*256)
	    retval = 256;
	else
	    retval = 0;   // unrecognized frame size

	return retval;
    }

}


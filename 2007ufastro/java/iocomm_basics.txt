2000.7.10
David Nordstedt

There is very nice html documentation for this device available via a link on
our main web page at http://newton.astro.ufl.edu:8888 or http://wolf.astro.ufl.edu:8888


Iocomm Basic Setup File Instructions
====================================

IP is flamingos1 for now - 128.227.184.208

We will set up VPN in the future with flamingos1 as gateway and the iocomm will
have a private network address at 192.168.111.100.

All devices on the flamingos network will have private IP's of
192.168.111.2 - 192.168.111.100

To set up the initial IP address if needed, you can use arp.  As root, type:
arp -s a.b.c.d aa:bb:cc:dd:ee

For example to set the IP to 192.168.111.100 with a MAC address
of 00:80:D4:01:8B:42(the MAC address of our iocomm) you would type:
arp -s 192.168.111.100 00:80:D4:01:8B:42

NOTE!  Setting the IP this way is temporary!
       To write it to permanent store, log in via http and submit the IP.
       After loggin in with a web browser, go to Global Configuration
       and submit the IP and netmask


To log in from telnet, simply telnet to the IP address of the iocomm.
If IP services aren't up, you can use a serial connection on port A with
the communications set up to 9600 baud, 8 data bits, No Parity, 1 stop bit

Login as admin.
Password is xxxxxxxxxxxx
Type admin from that prompt to get to more useful setup features.
Password is also xxxxxxxxxxxx

http access to setup is available at IP address @ port 80 and allows a much nicer
interface than the shell interface.


Setting up the serial interfaces
================================

Via the web interface, select the Serial Ports Configuration and choose a port
to configure.  Choose expert mode at the bottom to set all parameters.  This is a
guideline for field choices for the basic serial devices we are using:

Description:			    Asynchronous serial device
Terminal type:			    wyse50
Attached device:		    Asynchronous terminal
Baud rate:			    9600
Character Framing:		    8 data, no parity, 1 stop
Inactivity timeout:		    No timeout
DCD ignored/not wired:		    NOT checked
DSR ignored/not wired:		    NOT checked
RTS not raised intil DCD present:   NOT checked
Hardware flow control:		    NOT checked
Software(XON/XOFF) flow control:    NOT checked
None:				    checked

Check all ports that you wish to apply the current configuration to and
be sure to check the box to make changes immediate or the changes will take
effect upon the next power recycle.


Setting up TCP ports to connect to the serial ports
===================================================

For each port to be set upto connect from the network, there must be an
entry set up using the Outbound Services Configuration available from the main
menu of the web interface.  To set each port, click on Create New entry and fill in the
fields.  These parameters are to set up serial port 1 on TCP port 7001:

Service name(any label):		serial_connection_7001
Description(any label):			serial connection on port 7001
TCP port:				7001
Full carrier indication required:	NOT checked
Output translation: Map LF to CR-LR:	NOT checked
Output only. Ignore input from port:	NOT checked

Select the port you want to communicate with this port.  More than one port
can be set to a port, but doubtful we would use that in our application.


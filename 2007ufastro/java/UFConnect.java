import java.io.*;
import java.util.*;
import java.net.*;
import java.applet.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.lang.*;
import java.lang.Integer;
//import ImageUtil;

/******************************************************************************************************
This class should make the connection and handle communication with the server
Based on the GuiClientUka class requirements( From Panel Setup) it should be able to make a connection
Send a command String (UFStrings object), receive strings(either from help or get_strings) and especially
get images (return a buffer to the calling program with the image in it)
/******************************************************************************************************/

/*******************************************************************************************************/
//  public static void main(String []args){
//      String teststring;
//      UFStrings testufs, ufs = new UFStrings("chicken feet");
//      int testint, x;
//      UFProtocol ufp;
//      UFConnect test = new UFConnect();
//      System.out.println("The default server is "+ test.getServer());
//      test.setServer("dolphinus.astro.ufl.edu");
//      System.out.println("The current server is "+ test.getServer());
//      if(test.portOK()){
//           x= test.sendUFStrings(ufs);
//           if(x!=0){// was sent.. then receive object
//               ufp = test.getUFObject();
//               testufs=(UFStrings)ufp;
//               System.out.println("testufs has the following properties ---typeId: " + testufs.typeId() +" description: "+testufs.description()+" +name: "+testufs.name()+" timeStamp:"+  testufs.timeStamp() +" elements: "+testufs.elements() +"length: "+testufs.length());
//               // when i print the above, why is the timestamp wrong or weird??
//           }
//      }
//      }

public class UFConnect {

    protected Socket _soc;
    protected DataInputStream _insoc;
    protected DataOutputStream _outsoc;
    
    static public String imgfile = "image.dat";
    static public String frmfile = "frame.dat";
    
    static final private String default_server = "dolphinus.astro.ufl.edu";
    static final private int default_port = 55555;
    //for error control
    private String last_server = "";
    private int last_port = 0;
    private String server;
    private int port;

    
    /***********************************************************************************************/
    /* This constructor uses the default server and port*/
    public UFConnect() {
	server = new String(default_server);            // do we need to do this to copy
	port = (new Integer(default_port)).intValue();  // final variables safely?
	_insoc = null;
	_outsoc = null;
	_soc = null;
	System.out.println("UFConnect()> server = " + server);
	System.out.println("UFConnect()> port = " + port);
    }
    /***********************************************************************************************/
    /*This constructor accepts a server and port name*/
    public UFConnect(String _server, int _port) {
	server = _server;
	port = _port;
	_insoc = null;
	_outsoc = null;
	_soc = null;
	System.out.println("UFConnect()> server = " + server);
	System.out.println("UFConnect()> port = " + port);
    }
    /***********************************************************************************************/
    /*Change the current Server*/
    public void setServer(String newServer) {
	server = newServer;
    }
    /***********************************************************************************************/
    /*Sets the Port if possible*/
    public boolean setPort(int newPort) {
	//there is no way to know if this socket is available till we try sending to it
	port = newPort;// this is so weird why do this
       if (port==newPort) {return true;}
       return false;
    }
    /***********************************************************************************************/
    /*Returns the value of current Server*/
    public String getServer() {
	return server;
    }
    /***********************************************************************************************/
    /*Returns the value of current Port*/
    public int getPort() {
	return port;
    }
    /***********************************************************************************************/
    /*Returns the value of last Server used for connection*/
    public String getLastServer() {
	return last_server;
    }
    /***********************************************************************************************/
    /*Returns the value of current Port*/
    public int getLastPort() {
	return last_port;
    }
    /************************************************************************************************/
    /*Returns the Socket???????*/
    public Socket getSocket() {
	return _soc;
    }
    
   
/****************************************************************************************/
/****************************************************************************************/
     
    public int sendUFTimeStamp(UFTimeStamp ufp) {
	int retval = 0; // return number of bytes written
	if (portOK() == true) {
   	    retval = ufp._outputTo(_outsoc);	
	}
	return retval;
    }
/****************************************************************************************/
/****************************************************************************************/

    public boolean validConnection() {
	// this may have to change but as long as we have an echo server that always 
	// returns something, lets send and receive a string
       
	boolean retval= false;
	System.out.println("In validConnection\n\n\n");
	if(_soc != null&& _insoc != null&& _outsoc!=null){
	    System.out.println("In the if statement in validConnection\n\n\n");
	    //retval=true;
	    UFStrings test = new UFStrings("test");
	    try{
		test._outputTo(new DataOutputStream(_soc.getOutputStream()));//_outsoc);
	    }
	    catch(IOException ioe){
		return false;
	    }
	    UFProtocol ufp = this.getUFObject();
	    if(ufp==null){
		//retval =false;
		killSocket();
	    }
	    else{
		retval =true;
	    }
	}

	System.out.println("In valid connection and retval is "+retval);
	return retval;
    }
/**************************************************************************************/
    public boolean sameServerAndPort(){
	return (last_server.equals(server) && last_port==port);
    }
/**************************************************************************************/
/**************************************************************************************/
    public boolean portOK() {
	boolean retval = true;
	if(_soc==null|| _insoc==null|| _outsoc==null) {
 	    retval = socketInit(3);
  	}
	System.out.println("In PORTOK...... and retval is "+retval+"\n");
	return retval;
    }

/***********************************************************************************************/
/***********************************************************************************************/
    //used by validConnection but is it needed????????
    public void killSocket() {
	try {
	    _soc.close();   // close socket
	    _insoc = null;   // set streams to null
	    _outsoc = null;
	}
	catch (Exception ioe) {	    
	    // who cares? do nothing
	}
       
    }

/***********************************************************************************************/
/***********************************************************************************************/
    public UFProtocol getUFObject() {
	boolean socketStatus = true;
	UFProtocol ufp = null;

	if( _soc == null || _insoc == null || _outsoc == null) {
	    System.out.println(" _soc == null || _insoc == null || _outsoc == null : doing socketInit()");
	    socketStatus = socketInit(3);
	}

	if (socketStatus == false)   // if we can`t initialize socket, return null object
	    return null;

	System.out.println("getUFObject> Before creating UFProtocol object ufp");
	ufp = UFProtocol.createFrom(_soc); // recv request
	System.out.println("getUFObject> After creating UFProtocol object ufp");

	if (ufp == null)
	    System.out.println("getUFObject> ufp == null");
	else {
	    System.out.println("getUFObject> received: "+ ufp.name()+" at " + new Date());
	    System.out.println("getUFObject> typeId = " + ufp.typeId());
	}
	return ufp;
    }

  
/************************************************************************************************/
/***********************************************************************************************/    
    
    public boolean socketInit(int numTries) {
	System.out.println("socketInit> inside method");
	
	boolean connected = false;
	int tryCount = 0;

	_soc = null;                  // make sure everything is cleaned up
	_insoc = null;                // before we reinitialize
	_outsoc = null;               // sockets and streams
	
	while( (!connected)  && (tryCount++ < numTries) ) {  // only try a certain number of times
	    System.out.println("socketInit> try connecting to "+server+" using port "+port);
	    try {
		_soc = new Socket(server, port);
		_insoc = new DataInputStream(_soc.getInputStream());
		_outsoc = new DataOutputStream(_soc.getOutputStream());
		connected = true;
		System.out.println("socketInit> connected to "+server+" using port "+port);
	    }
	    catch( Exception ioe ) {
		System.out.println(ioe.getMessage());
	    }
	    if( !connected ){
		try{
		    System.out.println("socketInit> Couldn't connect: Going to sleep for 5 seconds");
		    Thread.sleep(5000); // delay connect attempts 5 sec.
		    System.out.println("socketInit> After sleeping for 5 seconds");
		}
		catch(InterruptedException ie){
		    System.out.println(ie.getMessage());
		}
	    }
	}
	
	if (connected == true){
	    System.out.println("socketInit> Looks like we are connected!");
	    //so that we know the last valid server we used. this is the wrong place.
	    last_server =server; last_port =port;
	}	
	else
    	    System.out.println("socketInit> Couldn't connect after 3 tries");
	
	return connected;
    }
/***********************************************************************************************/
/***********************************************************************************************/

    public int readIntFile(int[] frmbuff, File file, int w, int h) {
	int max = Integer.MIN_VALUE;
	
	DataInputStream data = null;  // Stream to read from source
	try {
	    // Create input stream
	    data = new DataInputStream(new FileInputStream(file)); 
	    int val, num=0; 
	    while( num < w*h ) { // Read until EOF
		frmbuff[num++] = val = data.readInt();
		if( val > max ) max = val;
	    }
	}
	catch(FileNotFoundException nof) {
	    System.out.println(file.getPath()+file.getName()+": "+nof);
	}
	catch(EOFException eof) {
	    System.out.println(file.getPath()+file.getName()+": "+eof);
	}
	catch(IOException ioe) {
	    System.out.println(file.getPath()+file.getName()+": "+ioe);
	}
	// Always close the streams, even if exceptions were thrown
	finally {
	    if(data != null) try { data.close(); } catch (IOException e) { ; }
	}
	return max;
    }

/***********************************************************************************************/
/***********************************************************************************************/    

    public File readable(SecurityManager sm, String filenm) {
	System.out.println("check data file: "+filenm);
	try {
	    if( sm != null ) sm.checkRead(filenm); // will throw exception
	    System.out.println("can read "+filenm);
	}
	catch (SecurityException se) {
	    System.out.println(se+" cannot read "+filenm);
	    return null;
	}
	
	File file = new File(filenm);  // Get File objects from String name
	if( file == null ) {
	    System.out.println("unable to create File object for "+filenm);
	    return null;
	}
	// First make sure the source file exists, is a file, and is readable.
	if( !file.exists() ) {
	    abort("no such data file: " + filenm);
	    return null;
	}
	if( !file.isFile() ) {
	    abort("filename is directory: " + filenm);
	    return null;
	}
	if( !file.canRead() ) {
	    abort("data file is unreadable: " + filenm);
	    return null;
	}
	// If we've gotten this far, then everything is okay.
	return file;
    }
    
    public String toString() {
	StringBuffer sb = new StringBuffer();

	sb.append("server = " +  server + " | ");
        sb.append("port = " + port + "\n");

	return new String(sb);
    }

    private static void abort(String msg) { 
    System.err.println(msg);
    //Runtime.getRuntime().exit(1);
    }
} // end of class UFConnect





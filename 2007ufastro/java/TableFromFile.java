import java.io.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class TableFromFile extends JPanel{
    int x,j;
    JTable table;
    JButton reload;
    JScrollPane scrollPane;
    final String[] error = {"ERROR>>"};
    final Object[][] message ={{"The data file could not be opened"},
			     {"or could not be read"},
			     {"check the permission and try again"}};
    public TableFromFile(){

	x=0;j=0;
	try{
	    this.setTable("test.file");
	}
	catch(IOException ioe){
	    table = new JTable(message, error);
	}

	reload = new JButton("reload");
	reload.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent e){ 
		System.out.println("reload has been pressed");
		try{
		    setTable("test.file");
		}
		catch(IOException ioe){
		    table = new JTable(message, error);
		}	
		//scrollPane.repaint();
	    }
	});

	scrollPane = new JScrollPane(table);
	scrollPane.repaint();
	GridBagLayout gridbag = new GridBagLayout();
	GridBagConstraints c = new GridBagConstraints();
	setLayout(gridbag);
	c.anchor = GridBagConstraints.NORTH;
	
	gridbag.setConstraints(reload,c);
	add(reload);
	c.gridy =1;
	c.fill = GridBagConstraints.BOTH;
	gridbag.setConstraints(scrollPane, c);
	//setLayout(new BorderLayout(1,1));
	//Add the scroll pane to this window.
	add(scrollPane);
	System.out.println("\n\nat the end of making new table\n\n");

    }

    public void setTable(String filename) throws IOException{
	Vector vector =new Vector();
	StringTokenizer ST;final String cNames[];
	FileReader FR = new FileReader(filename);// to open the file
	BufferedReader BR =new BufferedReader (FR);// to get tokens
	String s; Object[][] rowdata; Object row[];
	int col=0, count=0;
	
	s=BR.readLine();
	// this is to allow comments at the beginning of the data file only!!!!!
	// comments must start with @ on every line !!!!!!!!!!!!!!1
	while (s.startsWith("@") & s!= null)
	    s=BR.readLine();
	
	ST =new StringTokenizer(s);
	col = ST.countTokens();
	cNames= new String[col];
	while(ST.hasMoreTokens())
	    cNames[count++]=ST.nextToken();
	
	while((s=BR.readLine())!=null){
	    // System.out.println("trying to read");
	    ST =new StringTokenizer(s);
	    count=0;
	    // can put this outside the while loop
	    row =new Object[col];
	    while(ST.hasMoreTokens()){
		if(count == col)
		    break;// too many enteries on one line
		row[count++]= ST.nextToken();
	    }
	    vector.addElement(row);
	}
	BR.close();// close the file
	
	rowdata =new Object[vector.size()][col];
	for(int i=0; i<vector.size();i++)
	    for(int j=0; j<col;j++){
		row =(Object[])vector.elementAt(i);
		rowdata[i][j] =row[j];
	    }	
	table =new JTable(rowdata,cNames);
	table.setPreferredScrollableViewportSize(new Dimension(550, 200));
	table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
	table.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
	System.out.println("Table has been loaded");
    }
    public static void main(String[]args){
	JFrame t= new JFrame("Table from file");
	TableFromFile tp =new TableFromFile();
	t.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
	t.setContentPane(tp);
	t.pack();
	t.setVisible(true);
    }
}

import java.io.*;
import java.text.*;
import java.net.*;
import java.util.*; 
import java.awt.*; 

public class UFStrings extends UFTimeStamp {
  public UFStrings(int length) {
    _length=length;
//    _length=_MinLength_;
    _type=MsgTyp._Strings_;
    _timestamp = new String("yyyy:ddd:hh:mm:ss.uuuuuu");
    _elem=0; // should be increment to >= 1 
    _name = new String("");
    _seqCnt = _seqTot = 0;
    _duration = 0.0f;
    _values = null;
  }

  public UFStrings(String inString) {
    _type=MsgTyp._Strings_;
    _timestamp = new String("yyyy:ddd:hh:mm:ss.uuuuuu");
    _elem=0; // should be increment to >= 1 
    _name = inString;
    _length= _minLength();
    _seqCnt = _seqTot = 0;
    _duration = 0.0f;
    _values = null;
  }

  public UFStrings(String inName, String[] inValues) {
    _values = inValues;
    _elem = inValues.length;
    _name = inName;
    int count = 0;
    for (int i = 0; i < inValues.length; i++) {
	if (inValues[i] != null)
	    count += inValues[i].length();
	count += 4;  // add 4 for integer length of string
    }
    _length= _minLength() + count;
    _type=MsgTyp._Strings_;
    _seqCnt = _seqTot = 0;
    _duration = 0.0f;
    _timestamp = new String("yyyy:ddd:hh:mm:ss.uuuuuu");
  }

  // all methods declared abstract by UFProtocal 
  // can be defined here
  public String description() { return new String("UFStrings"); }
 
  // return size of the element's value (not it's name!)
  // either string length, or sizeof(float), sizeof(frame):
  public int valSize(int elemIdx) { 
    if( elemIdx >= _values.length )
      return 0;
    else
      return _values[elemIdx].length();
  }

///////////////////////////// protected: /////////////////////
  // additional attributes & methods (beyond base class's):
  protected String[] _values=null;

    // return number of Strings in object
    public int numVals() {
	return _values.length;
    }

    // return String at index passed in
    public String valData(int elemndx) {
	return _values[elemndx];
    }

  protected void _copyNameAndVals(String s, String[] vals) {
    _name = new String(s);
    _values = new String[vals.length];
    _elem = vals.length;
    _length = _minLength();
    for( int i=0; i<_elem; i++ ) {
      _values[i] = new String(vals[i]);
      _length += 4; // each string length is recorded in protocal
      _length += _values[i].length();
    }
  } 
    public void setNameAndVals(String s, String[] vals) {
    _name = new String(s);
    _values = new String[vals.length];
    _elem = vals.length;
    _length = _minLength();
    for( int i=0; i<_elem; i++ ) {
      _values[i] = (vals[i]);
      _length += 4; // each string length is recorded in protocal
      _length += _values[i].length();
    }
  } 

  protected int _dataFrom(DataInputStream inp) {
    // restore everything but length & type
    int retval=0;
    try {
	//length and type have already been read
      _elem = inp.readInt(); // should be 0
      retval += 4;
      _seqCnt = inp.readShort(); // seqcnt and seqtot (both short ints)
      retval += 2;
      _seqTot = inp.readShort();
      retval += 2;
      _duration = inp.readFloat(); // read in 'duration'
      retval+=4;
      byte[] tbuf = new byte[_timestamp.length()];
      inp.readFully(tbuf, 0, _timestamp.length());
      _timestamp = new String(tbuf);
      int namelen = inp.readInt();
      retval += 4;
      byte[] namebuf = new byte[namelen];
      if( namelen > 0 ) {
	inp.readFully(namebuf, 0, namelen);
        retval +=  namelen;
	_name = new String(namebuf);
      }

      _values = new String[_elem];

      for( int elem=0; elem<_elem; elem++ ) {
        int vallen = inp.readInt();
        retval += 4;
	byte[] valbuf = new byte[vallen];
        if( vallen > 0 ) {
	  inp.readFully(valbuf, 0, vallen);
	  _values[elem] = new String(valbuf);
          retval += vallen;
        }
      }
    }
    catch(EOFException eof) {
      System.err.println(eof);
    } 
    catch(IOException ioe) {
      System.err.println(ioe);
    }
    catch( Exception e ) {
      System.err.println(e);
    }
    // should compare retval to length-8
    return retval;
  }

  protected int _outputTo(DataOutputStream out) {
    // write out all attributes
    int retval=0;
    int slen=0;
    try {
      out.writeInt(_length);
      retval += 4;
      out.writeInt(_type);
      retval += 4;
      out.writeInt(_elem);
      retval += 4;
      out.writeShort(_seqCnt);//2 shorts -- seqcnt, seqtot
      retval += 2;
      out.writeShort(_seqTot);
      retval += 2;
      out.writeFloat(_duration);//duration
      retval += 4;
      out.writeBytes(_timestamp);
      retval += _timestamp.length();
      out.writeInt(_name.length());
      retval += 4;
      if( _name.length() > 0 ) {
	out.writeBytes(_name);
	retval += _name.length();
      }
      for( int elem=0; elem<_elem; elem++ ) {
	slen = _values[elem].length();
        out.writeInt(slen);
        retval += 4;
        if( slen > 0 ) {
	  out.writeBytes(_values[elem]);
 	  retval += slen; 
        }
      }
    }
    catch(EOFException eof) {
      System.err.println(eof);
    } 
    catch(IOException ioe) {
      System.err.println(ioe);
    }
    catch( Exception e ) {
      System.err.println(e);
    }
    return retval;
  }

    public String toString() {
	StringBuffer sb = new StringBuffer();

	sb.append("_length = " + _length + " | ");
	sb.append("_minlength() = " + _minLength() + " | ");
	sb.append("_type = " + _type + " | ");
	sb.append("_timestamp = " + _timestamp + "\n");
	sb.append("_elem = " + _elem + " | ");
	sb.append("_name = " +  _name + " | ");
        sb.append("_values =\n");
	if (_values != null)
	    for (int i = 0; i < _values.length; i++) {
		sb.append(_values[i] + "\n");
	    }

	return new String(sb);
    }

}


import UFJava_Protocol.*;
import java.io.*;
import java.util.*;

public class PingClient
{

    protected int counter = 0 ;
    protected Random rand = new Random() ;

    public static void main(String[] args)
    { 
        new PingClient( args ) ;
    }

    public PingClient( String[] args )
    {
	if(args.length!=2)
	    {
		System.out.println("Please input two arguments: hostname,port");
		return;  
	    }

	String host = args[0];
        int port = 0;
	try 
	    {
		port = Integer.parseInt(args[1]);
	    }
	catch(NumberFormatException e)
	    {
		System.out.println("Invalid port number.");
		return;
	    }

        System.out.print("Please enter the number of times you want to loop: ");

        BufferedReader in = null ;
        int num = 0 ;
        try
            {
                in = new BufferedReader(new InputStreamReader(System.in));
                num = Integer.parseInt(in.readLine());
            }
        catch( IOException e )
            {
                System.out.println( "Error obtaining streams" ) ;
                System.exit( 0 ) ;
            }
        catch( NumberFormatException e )
            {
                System.out.println( "Invalid integer" ) ;
                System.exit( 0 ) ;
            }

	UFSocket s = new UFSocket(host,port);

	System.out.println("Connection established");
  
        for(int i=1; i<=num; i++)
            {
                UFProtocol ufp = buildObject();
                System.out.println("Sending " + ufp.description());
                if(ufp.sendTo(s)<=0)
                    {
                        System.out.println("Write Error");
                        System.exit(0);
                    }
    
                System.out.println( "Send TIMESTAMP successfully!" ) ;
                System.out.println("Send INTS successfully!");
                System.out.println( "Send BYTE  successfully!" ) ;
        
                ufp = UFProtocol.createFrom(s);
                if(null==ufp)
                    {
                        System.out.println("Read Error");
                        System.exit(0);
                    }
                System.out.println("RECEIVED OBJECT NAMED:" + ufp.name());
                System.out.println("THIS IS ITERATION # " + i + " OF THE LOOP!!"); 
                System.out.println("") ;

            }
    
    }
    
    public UFProtocol buildObject()
    {
        counter++;
        UFProtocol ufp=null;

        String name = "Object: " + counter ;
        int  randomObj = Math.abs(rand.nextInt()%3);

        switch(randomObj){
        case 0:
            ufp = new UFTimeStamp(name);
            break;
        case 1:
            ufp = new UFBytes();
            break;
        case 2:
            ufp = new UFInts();
            break;
        }
         ufp.rename(name);
        return ufp;
    }
}



 
    

    

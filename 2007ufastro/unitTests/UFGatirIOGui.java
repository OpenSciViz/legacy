// jdk1.1 - this is loosely based on the TextEventDemo example from the javasoft site

import java.applet.Applet;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

// connection to baynet annex via socket & gatir serial port
import UFGatirAnnex;

// Input from the Kevin's GatIR box is all ASCII strings generally terminated by ">>"
// Output to Kevins's  GatIR box is all ASCII strings generally terminated by CR (decimal 13).
// The exception to this is when he sends a "continue" prompt, which expects just
// a single byte that should not be followed by a CR.
// Generally any Input from the GatIR box must follow a transmission to it with
// the exception 
//
public class UFGatirIOGui extends Applet implements ActionListener {
  static final String rcsId = "$Id: UFGatirIOGui.java,v 0.0 2002/06/03 17:42:21 hon beta $";
  TextField textField;
  TextArea textArea;
  String newline;
  String cmd = "";
  String reply = "";
  Vector cmdlist;
  UFGatirAnnex g = null;
  protected String annexPrivateIP = "192.168.111.101";
  protected Integer portNo;

  public void init() {
    newline = System.getProperty("line.separator");
    cmdlist = new Vector();
    String param = getParameter("AnnexIP");
    if( param != null )
      annexPrivateIP = param;
    param = getParameter("AnnexPort");
    if( param != null )
      portNo = Integer.valueOf(param);
    System.err.println("UFGatirIOGui> connect to Annex @ "+annexPrivateIP+" portNo= "+portNo);

    setLayout(new BorderLayout());

    textField = new TextField(20); textField.setName("Text Field");
    textField.addActionListener(new MyTextActionListener(textField.getName()));
    textArea = new TextArea(5, 20); textArea.setName("Text Area");
    textArea.setText("!FIRST! Click on \"Clear\" button below to connect to Annex Socket..."+newline);
    add("North", textField);
    add("Center", textArea);

    Button clear = new Button("Clear"); clear.setName("Clear");
    clear.addActionListener(this);
    Button rboot = new Button("Read Boot"); rboot.setName("Read Boot");
    rboot.addActionListener(this);
    Button submit = new Button("Submit"); submit.setName("Submit");
    submit.addActionListener(this);

    Panel southPanel = new Panel();
    southPanel.setLayout(new BorderLayout());
    southPanel.add("East", clear);
    southPanel.add("Center", submit);
    southPanel.add("West", rboot);
    add("South", southPanel);

    textField.requestFocus();
  }

  class MyTextActionListener implements ActionListener {
    String source;

    public MyTextActionListener(String source) {
      this.source = source;
    }

    // Handle the text field Return.
    public void actionPerformed(ActionEvent e) {
      TextComponent tc = (TextComponent)e.getSource();
      textField.selectAll();
      cmd = textField.getText();
      cmdlist.addElement(cmd);
      textArea.append(cmd+newline);
      //int selStart = textArea.getSelectionStart();
      //int selEnd = textArea.getSelectionEnd();
      //textArea.replaceRange(textField.getText(), selStart, selEnd);
      textField.setText("");
    }
  }

  // Handle button click. 
  public void actionPerformed(ActionEvent e) {
    String name = ((Component)e.getSource()).getName();
    System.err.println("actionPerformed> comp. name= "+name);
    if( name.equals("Clear") ) {
      if( g == null ) g = new UFGatirAnnex(annexPrivateIP, portNo.intValue());
      cmdlist.removeAllElements();
      String info = "Connected to "+annexPrivateIP+":"+portNo+". Enter commands in the TextField above...";
      textArea.setText(info+newline);
      textField.setText("");
      textField.requestFocus();
    }
    if( name.equals("Read Boot") ) {
      reply = g.recvGatir();
      textArea.append(newline+reply);
      while( (reply.indexOf("Hit") >= 0 || reply.indexOf("hit") >= 0) &&
	     (reply.indexOf("Continue") >= 0 || reply.indexOf("continue") >= 0) ) {
	// this is a special case where we need to send a char back to complete the input
	byte b = 33; // "!" ASCII
	g.sendGatir1(b); // anything will do?
        reply = g.recvGatir();
        textArea.append(newline+reply);
      }
      textField.requestFocus();
    }
    if( name.equals("Submit") ) {
      for( int i = 0; i < cmdlist.size(); i++ ) {
        g.sendGatir((String)cmdlist.elementAt(i));
        reply = g.recvGatir();
        textArea.append(newline+reply);
        while( (reply.indexOf("Hit") >= 0 || reply.indexOf("hit") >= 0) &&
	       (reply.indexOf("Continue") >= 0 || reply.indexOf("continue") >= 0) ) {
	  // this is a special case where we need to send a char back to complete the input
	  byte b = 33; // "!" ASCII
	  g.sendGatir1(b); // anything will do?
          reply = g.recvGatir();
          textArea.append(newline+reply);
        }
      }
      textField.requestFocus();
    }
    // Scroll down, unless the peer still needs to be created.
    if ( textArea.isValid() ) {
      textArea.setCaretPosition(java.lang.Integer.MAX_VALUE);
    } 
  }
}

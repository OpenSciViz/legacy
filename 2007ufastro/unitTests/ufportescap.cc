#include "unistd.h"
#include "stdio.h"
#include "fcntl.h"
#include "termio.h"

#include "iostream.h"
#include "string"
#include "map"

using namespace std;

int main(int argc, char** argv) {
  static struct termio orig, raw;
  string dev;
  bool _verbose;
  map <char,int> _currentPosition;
#if defined(LINUX) || defined(Linux)
  dev = "/dev/ttyS0";
#else
  dev =  "/dev/term/a"; // Solaris
#endif

  int fd = ::open(dev.c_str(), O_RDWR);
  if( fd <= 0 ) {
    clog<<"open failed: "<<dev<<endl;
    return -1;
  }

  int io = ::ioctl(fd, TCGETA, &orig);
  io = ::ioctl(fd, TCGETA, &raw);

  raw.c_cflag = B9600 | CS8 | CLOCAL | CREAD;
  raw.c_lflag &= ~ICANON;
  raw.c_lflag &= ~ECHO;
  raw.c_cc[VMIN] = 1; // present each character as soon as it shows up
  //raw.c_cc[VTIME] = 1; // 0.1 second for timeout
  raw.c_cc[VTIME] = 0; // infinite timeout?

  io = ::ioctl(fd, TCSETAF, &raw);

  int nb = 0;
  char nl = 10;
  char cr = 13;
  char buf = 0;
  string ret;
  if( argc > 1 && argv[1][1] == 'n' ) {
    clog<<"send a newline..."<<endl;
    nb = ::write(fd, &nl, 1);
  }
  if( argc > 1 && argv[1][1] == 'c' ) {
    clog<<"send a carriage return..."<<endl;
    nb = ::write(fd, &cr, 1);
  }
  _verbose = 0;
  if( argc > 1 && argv[1][1] == 'v') {
    _verbose = true;
  }
  clog<<"read one command at a time, and reply..."<<endl;
  string cmdin("");
  string status("");
  string term("\n");
  string prompt("#");
  char indexor = 'A';
  while( cmdin != "shutdown" ) {
    nb = ::read( fd, &buf, 1);
    if( nb <= 0 ) {
      clog<<"read failed: "<<dev<<endl;
      return -1;
    }
    if( buf == nl && _verbose) {
      clog<<"got newline..."<<endl;
    }
    if( buf == cr && _verbose) {
      clog<<"got carriage return..."<<endl;
    }
    if( buf == cr || buf == nl ) { 
      status = "";
      if (cmdin.size() == 0) status = prompt+term;
      else {
        indexor = cmdin[0];
        cmdin = cmdin.substr(1,cmdin.size()-1);
        while (cmdin[0] == ' ') cmdin = cmdin.substr(1,cmdin.size()-1);
        char indexcmd = cmdin[0];
	if (_verbose) 
	  clog << "Indexor Name: "<<indexor << ".  Command: "<<indexcmd<<endl;
	if (indexcmd == 'Z') {
	  char buff[256];
	  sprintf(buff,"%d",_currentPosition[indexor]);
	  status =string(" ")+ buff + term;
	} else if (indexcmd == '+') {
	  cmdin = cmdin.substr(1,cmdin.size()-1);
	  while (cmdin[0] == ' ') cmdin=cmdin.substr(1,cmdin.size()-1);
	  int steps = atoi(cmdin.c_str());
	  _currentPosition[indexor] += steps;
	  status = term;
	} else if (indexcmd == '-') {
	  cmdin = cmdin.substr(1,cmdin.size()-1);
	  while (cmdin[0] == ' ') cmdin=cmdin.substr(1,cmdin.size()-1);
	  int steps = atoi(cmdin.c_str());
	  _currentPosition[indexor] -= steps;
	  status = term;
	}
      }
      // write command status return
      if (_verbose)
	clog<<"got complete command, sending back status reply: "<<status<<endl;
      if (status.size() ==0) status = prompt+term;
      nb = ::write(fd, status.c_str(), strlen(status.c_str()));
      if( nb <= 0 ) {
        clog<<"write failed: "<<dev<<endl;
        return -1;
      }
      cmdin = "";
    }
    if( buf != cr && buf != nl ) { // append to cmdin buf:
      cmdin += buf;
      if (_verbose)
	clog<<"append another character and echo back: "<<cmdin<<endl;
      nb = ::write(fd, &buf, 1);
      if (nb <= 0) {
	clog<<"write failed: "<<dev<<endl;
	return -1;
      }
    }
  }

  io = ::ioctl(fd,TCSETAF,&orig);

  close(fd);

  return 0;
}

#!/usr/local/bin/perl -w

use strict;

use ufgem qw/:all/;


my $cnt = @ARGV;
my $reply;

if( $cnt < 2 ) { # assume -help
   die "\n\tusage probeGuideConfig.pl (p1|p2) (on|off)\n\n";

}
else {
  my $probe_name = shift;
  my $state = shift;
  
  if( $probe_name =~ m/^p1$/i or $probe_name =~ m/^p2$/i ){
    if( $state =~ m/^on$/i or $state =~ m/^off$/i ){
      my $soc = ufgem::viiconnect();
      ufgem::probeGuideConfig( $soc, $probe_name, $state );
      close( $soc );

    }else{
      die "\n\tYou didn't enter either on or off for ".
	  "the probe state\n\n";
    }
  }else{
      die "\n\tYou didn't enter either p1 or p2 for ".
	  "the probe name\n\n";
  }

}#End


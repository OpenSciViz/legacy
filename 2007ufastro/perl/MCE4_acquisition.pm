package MCE4_acquisition;

require 5.005_62;
use strict;
use warnings;
use Fitsheader qw/:all/;

require Exporter;
use AutoLoader qw(AUTOLOAD);

our @ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use MCE4_acquisition ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.
our %EXPORT_TAGS = ( 'all' => [ qw( &setup_mce4
				    &ct12_setup_mce4 
				    &ct10_setup_mce4
				    &acquisition_cmd
				    &acquire_image
	
) ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw( 
	
);
our $VERSION = '0.01';


# Preloaded methods go here.
sub setup_mce4{
  my ($expt, $nreads) = @_;
  #>>>Setup MCE4 for exposure
  #>>>Read necessary header params for exposure<<<
  my $CT       = param_value( 'CYCLETYP' );
  #my $nreads_send_to_mce4 = $nreads - 1;
  #01 Oct 2001-Kevin just told me CT42 should get LDVAR 2 ($nreads - 2), 
  #because it does an extra two reads due to the way it's written
  my $nreads_send_to_mce4 = $nreads - 2;

  my $space = " ";
  my $quote = "\"";
  my $cmd_sep = $ENV{CLIENT_CMD_SEPARATOR};

  my $setup_mce4_head = $ENV{UFDC_DO_BASE} .$ENV{CLIENT_HOST}.$space.$ENV{HOST}.$space.
                        $ENV{CLIENT_PORT}.$space.$ENV{CLIENT_UFDC_PORT}.$space.
                        $ENV{CLIENT_RAW}.$space;
	      
  my $setup_mce4_body1 = $ENV{SET_EXPT_FRONT}.$expt.$ENV{SET_EXPT_BACK};#TIMER 0 EXPT S
  #my $setup_mce4_body2 = $space;
  my $setup_mce4_body2 = $space.$cmd_sep."STOP".$space;#Add MCE4 STOP command

  if( $CT == 40 ){
    #Don't reset LDVAR for nreads, as it's not used
  }elsif( $CT == 42 ){
    $setup_mce4_body2 = $cmd_sep.$ENV{SET_NREADS}.$nreads_send_to_mce4;
  }else{
    die "\n\n".
        ">>>> WARNING    WARNGIN <<<<\n".
	"This script doesn't find values for the fitsheader keyword\n".
	"CYCLETYP that it understands.  It expects 40 or 42\n\n"; 
  }

  my $set_CT = $cmd_sep . $ENV{SET_CT} . $CT;
  
  my $net_mce4_cmd = $setup_mce4_head . $quote .
                     $setup_mce4_body1 . $setup_mce4_body2 . $set_CT .
	            $quote;

  print "\a\a\a\a\a";
  print "\n\nExecuting\n$net_mce4_cmd\n\n";
  #system( $net_mce4_cmd );
  my $reply = `$net_mce4_cmd`;
  system( "ufsleep 1.0");

}#Endsub setup_mce4


sub ct12_setup_mce4{
  my ($expt, $nreads) = @_;
  #>>>Setup MCE4 for exposure
  #>>>Read necessary header params for exposure<<<
  my $CT       = 12;#should be multiple reads with no integration
  my $nreads_send_to_mce4 = $nreads - 2;

  my $space = " ";
  my $quote = "\"";
  my $cmd_sep = $ENV{CLIENT_CMD_SEPARATOR};

  my $setup_mce4_head = $ENV{UFDC_DO_BASE} .$ENV{CLIENT_HOST}.$space.$ENV{HOST}.$space.
                        $ENV{CLIENT_PORT}.$space.$ENV{CLIENT_UFDC_PORT}.$space.
                        $ENV{CLIENT_RAW}.$space;
	      
  my $setup_mce4_body1 = $ENV{SET_EXPT_FRONT}.$expt.$ENV{SET_EXPT_BACK};#TIMER 0 EXPT S
  my $setup_mce4_body2 = $space;

  $setup_mce4_body2 = $cmd_sep.$ENV{SET_NREADS}.$nreads_send_to_mce4;

  my $set_CT = $cmd_sep . $ENV{SET_CT} . $CT;
  
  my $net_mce4_cmd = $setup_mce4_head . $quote .
                     $setup_mce4_body1 . $setup_mce4_body2 . $set_CT .
	            $quote;

  print "\a\a\a\a\a";
  print "\n\nExecuting\n$net_mce4_cmd\n\n";
  #system( $net_mce4_cmd );
  my $reply = `$net_mce4_cmd`;
  system( "ufsleep 1.0");

}#Endsub ct12_setup_mce4


sub ct10_setup_mce4{
  my ($expt, $nreads) = @_;
  #>>>Setup MCE4 for exposure
  #>>>Read necessary header params for exposure<<<
  my $CT       = 10;#should be single read with no integration
  my $nreads_send_to_mce4 = $nreads - 2;

  my $space = " ";
  my $quote = "\"";
  my $cmd_sep = $ENV{CLIENT_CMD_SEPARATOR};

  my $setup_mce4_head = $ENV{UFDC_DO_BASE} .$ENV{CLIENT_HOST}.$space.$ENV{HOST}.$space.
                        $ENV{CLIENT_PORT}.$space.$ENV{CLIENT_UFDC_PORT}.$space.
                        $ENV{CLIENT_RAW}.$space;
	      
  my $setup_mce4_body1 = $ENV{SET_EXPT_FRONT}.$expt.$ENV{SET_EXPT_BACK};#TIMER 0 EXPT S
  #my $setup_mce4_body2 = $space;#original
  my $setup_mce4_body2 = $space.$cmd_sep."STOP".$space;#Add MCE4 STOP command

  $setup_mce4_body2 = $cmd_sep.$ENV{SET_NREADS}.$nreads_send_to_mce4;

  my $set_CT = $cmd_sep . $ENV{SET_CT} . $CT;
  
  my $net_mce4_cmd = $setup_mce4_head . $quote .
                     $setup_mce4_body1 . $setup_mce4_body2 . $set_CT .
	            $quote;

  print "\a\a\a\a\a";
  print "\n\nExecuting\n$net_mce4_cmd\n\n";
  #system( $net_mce4_cmd );
  my $reply = `$net_mce4_cmd`;
  system( "ufsleep 1.0");

}#Endsub ct10_setup_mce4



sub acquisition_cmd{
  my ($header, $lut_name, $file_hint, $next_index, $net_edt_timeout) = @_;
  my $space = " ";
  my $quote = "\"";

  my $start    = $ENV{UFDC_ACQ_START_CMD};

  my $ds9; my $jpeg; my $png;
  my $disp_ds9 = param_value( 'DISP_DS9' );
  my $use_jpeg = param_value( 'JPEG'     );
  my $use_png  = param_value( 'PNG'      );
  #print "ds9     = $disp_ds9\n";
  #print "jpeg    = $use_jpeg\n";
  #print "use_png = $use_png\n";

  #jpeg and png have optional scale factors

  my $cnt      = " -cnt "    .param_value( 'TAKECNT' ); 
  my $lut      = " -lut "    .$lut_name;
  my $fits     = " -fits "   .$header;
  my $file     = " -file "   .$file_hint;
  my $index    = " -index "  .$next_index;
  my $timeout  = " -timeout ".$net_edt_timeout;

  my $acq_cmd_head = $ENV{UFDC}.
                     $space.$ENV{CLIENT_HOST}.$space.$ENV{HOST}.$space.
                     $ENV{CLIENT_PORT}.$space.$ENV{CLIENT_UFDC_PORT}.$space.
		     $ENV{UFDC_ACQ}.$space;

  my $acq_cmd_body = $start.$timeout.$cnt.
                     $lut.$fits.$file.$index;

  if( $disp_ds9 ){ 
    $ds9 = " -ds9";
    $acq_cmd_body = $acq_cmd_body . $ds9;
  }

  if( $use_jpeg ){ 
    $jpeg = " -jpeg";
    $acq_cmd_body = $acq_cmd_body . $jpeg;
  }

  if( $use_png ){ $png = " -png";
    $acq_cmd_body = $acq_cmd_body . $png;
  }

  my $acq_cmd      = $acq_cmd_head.
                     $quote.$acq_cmd_body.$quote;

  #print "Acquisition command is\n\n";
  #print "$acq_cmd\n\n";

  #ufdc -host flam1a -port 52008 -acq 
  #"start -ds9 -index 1 -cnt 1 -timeout 10 -lut lutfile -fits fitsfile 
  #-file filehint
  #-jpeg [scale] -png [scale]"

  return $acq_cmd;
}#Endsub acquisition_cmd


sub acquire_image{
  my $acq_cmd = $_[0];
  my $reply;

  print "\nObservation starting.\a\a\a\a\a\n";
  print "Executing\n\n";
  print "$acq_cmd\n\n";
  print "\nIf it hangs here for more than ~6 seconds, type ufdc -q".
        " status in another $ENV{HOST} window\n\n\n";
  $reply = `$acq_cmd`;
  system("ufsleep 0.1");
  print "$reply\n";
  

}#Endsub acquire_image


# Autoload methods go after =cut, and are processed by the autosplit program.

1;
__END__
# Below is stub documentation for your module. You better edit it!

=head1 NAME

MCE4_acquisition - Perl extension for blah blah blah

=head1 SYNOPSIS

  use MCE4_acquisition qw/:all/;


=head1 DESCRIPTION

A library of MCE4 commands that need to be issued every time we
want to take a single image or a sequence of images

=head2 EXPORT



=head1 AUTHOR

SNR Sep 2001

=head1 SEE ALSO

perl(1).

=cut

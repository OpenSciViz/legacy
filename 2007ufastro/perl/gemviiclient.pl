#!/usr/bin/perl
#my $rcsId = "\$Name:  $ \$Id\$";
use IO::Socket;

# status items
# syntax: "get $gemstat[i] $terminate";

$terminate = "\r\n";

@gemstat = ( "Airmass",
             "azError",
             "Azimuth",
             "baseY",
             "baseY",
             "beam",
             "chopBeam",
             "chopDutyCycle",
             "chopfreq",
             "choppa",
             "chopping",
             "chopthrow",
             "Elevation",
             "elError",
             "focus",
             "framePA",
             "HA",
             "Health",
             "Humidity",
             "instrAA",
             "instrPA",
             "localTime",
             "LST",
             "MJD",
             "nodmode",
             "nodpa",
             "nodthrow",
             "nodtype",
             "offsetDec",
             "offsetRA",
             "offsetX",
             "offsetY",
             "rotator",
             "rotError",
             "targetName",
             "targetRA",
             "targetDec",
             "targetEpoch",
             "targetEquinox",
             "targetFrame",
             "targetRaDecSys",
             "Telescope",
             "telRA",
             "telDec",
             "userFocus",
             "UTC",
             "zd" );

# commands
# syntax: "do $gemcmd[i] $terminate";

@gemcmd = ( "chop state = On/Off",
	    "chopBeam = A/B/C",
	    "chopConfig sync = SCS/OSCIR> freq = Hz throw = arcsec PA = deg. frame = FK5/FK4/AZEL Equinox = J2000",
	    "focus offset = mm",
	    "guide state = On/Off",
	    "handset Type = radec/azel/tangent-plane dRA = arcsec dDec = arcsec",
	    "handsetAbsorb",
	    "handsetClear",
	    "m2GuideConfig source = PWFS1/PWFS2/O1WFS chopbeam A/B",
	    "mountGuide state = On/Off",
	    "nod beam = A/B",
	    "nodConfig mode = standard/offset type = radec/azel/tangent-plane throw = arcsec pa = deg.",
	    "offset type = radec/azel/tangent-plane dRA = arcsec dDec = arcsec",
	    "xyOffset dx = mm dy = mm",
	    "xyOffsetClear",
	    "xyOffsetAbsorb",
	    "rotator PA = deg.-east frame = FK5/FK4/AZEL Equinox = J2000.0 iia = deg.",
	    "source name = * frame = FK5/FK4/AZEL RA = * Dec = * Equinox = J2000.0 Epoch = J2000.0 \
                           parallax = arcsec pmRA = sec/yr pmDec = sec/yr rv = km/s" );

$host = `hostname`; chomp $host;
#($host, $aliases, $addrtyp, $length, @addr) = gethostbyname $host;
#$host = "128.227.184.161";
$viisoc = viiconnect($host);
if( defined($viisoc) ) {
  gemstatusAll($viisoc);
  close( $viisoc );
}

########################################### subroutines #################################

sub gemstatus {
  local $soc;
  local $s = "telescope";

  local $argc = @_;
  if( $argc > 0 ) { $soc = shift; }
  if( $argc > 1 ) { $s = shift; }

  local $reply;
  if( !defined ($soc) || $soc eq "" ) {
    print "?Bad socket connection to Gemini VII server\n";
    return;
  }
  foreach $i (@gemstat) {
    print $soc "get $s $terminate";
    $soc->flush();
    $reply = $soc->get_line();
    print "Gemini VII reply: $reply";
  }
  return $reply;
}

sub gemstatusAll {
  local $soc;
  local $argc = @_;
  if( $argc > 0 ) { $soc = shift; }
  local $reply;
  if( !defined ($soc) || $soc eq "" ) {
    print "bad socket connectio to gemini vii server\n";
    return;
  }
  foreach $i (@gemstat) {
    print $soc "get $i $terminate";
    $soc->flush();
    $reply = <$soc>;
    print $reply;
  }
}

sub gemcommandAll {
  foreach $i (@gemcmd) {
    print "do $i $terminate";
  }
}

sub viiconnect {
  local $host = "vii-sim.hi.gemini.edu";
  local $argc = @_;
  if( $argc > 0 ) { $host = shift; }
  local $port = 7283; # gemini visiting instrument (vii) service
  local $soc = IO::Socket::INET->new ( PeerAddr => $host, PeerPort => $port, Proto => 'tcp', Type => SOCK_STREAM );
  if( !defined($soc) ) {
    print "failed to connect to $host on port $port\n";
  }
  else {
    print "connected to $host on port $port\n";
}
  return $soc;
}


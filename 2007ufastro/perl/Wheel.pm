package Wheel;

require 5.005_62;
use strict;
use warnings;

require Exporter;
use AutoLoader qw(AUTOLOAD);

our @ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use Wheel ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.

our %EXPORT_TAGS = 
  ( 'all' => [ qw( PI  

$DECKER_REV  
@DECKER_NAMES     @DECKER_BASE     @DECKER_OFFSET     @DECKER_MDPTS 
@DECKER_NEG_NAMES @DECKER_NEG_BASE                    @DECKER_NEG_MDPTS
@DECKER           @NEG_DECKER  
   
$MOS_SLIT_REV
@MOS_SLIT_NAMES       @MOS_SLIT_BASE      @MOS_SLIT_OFFSET  @MOS_SLIT_MDPTS
@MOS_SLIT_NEG_NAMES   @MOS_SLIT_NEG_BASE                    @MOS_SLIT_NEG_MDPTS
@MOS_SLIT             @NEG_MOS_SLIT

$MOS_SLIT_PN_REV
$KL_MDPT_POS          $KL_MDPT_NEG
@MOS_SLIT_PN_NAMES    @MOS_SLIT_PN_BASE   @MOS_SLIT_PN_OFFSET @MOS_SLIT_PN_MDPTS
@PN_MOS_SLIT

@MOS_SLIT_OPOS_NAMES @MOS_SLIT_OPOS_BASE  @MOS_SLIT_OPOS_OFFSET
@OPOS_MOS_SLIT       @MOS_SLIT_OPOS_MDPTS

@MOS_SLIT_ONEG_NAMES @MOS_SLIT_ONEG_BASE  @MOS_SLIG_ONEG_OFFSET
@ONEG_MOS_SLIT       @MOS_SLIT_ONEG_MDPTS
  
$FILTER_REV       		   
@FILTER_NAMES      @FILTER_BASE      @FILTER_OFFSET   @FILTER_MDPTS
@FILTER_NEG_NAMES  @FILTER_NEG_BASE                   @FILTER_NEG_MDPTS 
@FILTER            @NEG_FILTER

$LYOT_REV
@LYOT_NAMES        @LYOT_BASE       @LYOT_OFFSET      @LYOT_MDPTS 
@LYOT_NEG_NAMES    @LYOT_NEG_BASE                     @LYOT_NEG_MDPTS
@LYOT              @NEG_LYOT

$GRISM_REV
@GRISM_NAMES       @GRISM_BASE       @GRISM_OFFSET   @GRISM_MDPTS
@GRISM_NEG_NAMES   @GRISM_NEG_BASE                   @GRISM_NEG_MDPTS  
@GRISM             @NEG_GRISM

&get_wheel_info

) ] ,

    'test' => [ qw( add_test2 ) ]
  );


our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} }, @{ $EXPORT_TAGS{'test'} } );
our @EXPORT = qw( );
our $VERSION = '0.01';


# Preloaded methods go here.

#<<<<<WHEEL POSITIONS>>>>>#
use constant PI => 4 * atan2 1, 1;

our $DECKER_REV       = 9000;
our @DECKER_NAMES     = qw( dark  imaging   slit    mos );
our @DECKER_BASE      =   (    0,    2250,  4500,  6750 );
our @DECKER_OFFSET    =   (    1,       0,    20,    20 );
our @DECKER_MDPTS     =   ( 1125,    3375,  4625,  7875 );

our @DECKER_NEG_NAMES = qw(  dark     mos   slit  imaging );
our @DECKER_NEG_BASE  =   (     0,  -2250, -4500, -6750   );
our @DECKER_NEG_MDPTS =   ( -1125,  -3375, -4625, -7875   );


our @DECKER;
for(my $i=0; $i < 4; $i++){ 
  $DECKER[$i] = $DECKER_BASE[$i] + $DECKER_OFFSET[$i];
}

our @NEG_DECKER;
for(my $i=0; $i < 4; $i++){ 
  $NEG_DECKER[$i] = $DECKER_NEG_BASE[$i] + $DECKER_OFFSET[$i];
}

####MOS_SLIT
#original Positive Direction
#scale factor error
our $MOS_SLIT_REV = 96000;
our @MOS_SLIT_NAMES = qw( imaging  3pix       Q       P      O     N       M       
			  L        K       9pix       H  20pix     F   12pix 
			  D     2pix          B    6pix       );

our @MOS_SLIT_BASE =    ( 0,       8664,  12400,  19520,  26640,  33760,  40880,  
			  48000,  55120,  58688,  62248,  65808, 70168,  72936,  
                          76496,  80056,  83616,  87464       );

our @MOS_SLIT_OFFSET =  ( 75,        75,    75,    75,      75,   75,    75,
			  75,        75,    75,    75,      75,   75,    75,
			  75,        75,    75,    75        );

our @MOS_SLIT_MDPTS =   (  4332,  10532,  15960,  23080,  30200, 37320,  44440,
			  51560,  56904, 60468,  64028,   67988, 71552,  74716,
			  78276,  81836, 85540,  91732   );

our @MOS_SLIT;
for(my $i=0; $i < 18; $i++){
  $MOS_SLIT[$i] = $MOS_SLIT_BASE[$i] + $MOS_SLIT_OFFSET[$i];
}

#original Negative Direction
#scale factor error
our @MOS_SLIT_NEG_NAMES = qw( imaging    6pix       B   2pix       D   12pix    
			            F   20pix       H   9pix       K       L  
			            M       N       O      P       Q    3pix );

our @MOS_SLIT_NEG_BASE  =   (       0,  -8536, -12384, -15944, -19504, -23064, 
			       -25832, -30192, -33752, -37312, -40880, -48000,
			       -55120, -62240, -69360, -76480,  -83600, -87336 );

our @MOS_SLIT_NEG_MDPTS =   (   -4268, -10460, -14164, -17724, -21284, -24448,
			        -28012, -31972,-35532, -39096, -44440, -51560,
			       -58680, -65800, -72920, -80040, -85468, -91668  );

our @NEG_MOS_SLIT;
for(my $i=0; $i < 18; $i++){
  $NEG_MOS_SLIT[$i] = $MOS_SLIT_NEG_BASE[$i] + $MOS_SLIT_OFFSET[$i];
}

#Motion both in Plus/Neg from Image hole
#Numbers scaled for full rev value returned by controller
our $MOS_SLIT_PN_REV   = 97558;
our $KL_MDPT_POS =  52397;
our $KL_MDPT_NEG = -45161;

our @MOS_SLIT_PN_NAMES = qw(        K    9pix       H    20pix      F   12pix  
                                    D    2pix       B     6pix imaging   3pix
                                    Q       P       O        N      M       L );

our @MOS_SLIT_PN_BASE  =    (  -41543, -37918, -34300, -30682, -26251, -23438,
			       -19821, -16203, -12585,  -8675,      0,   8805,
			        12601,  19837,  27072,  34308,  41543,  48779 );

our @MOS_SLIT_PN_MDPTS =   (   -45161, -39731, -36109, -32491, -28467, -24845,
			       -21630, -18012, -14394, -10630,  -4338,   4403,
			        10703,  16219,  23455,  30690,  37926,  45161 );

our @MOS_SLIT_PN_OFFSET  =  (       0,      0,      0,      0,      0,      0,
				    0,      0,      0,      0,      0,      0,
				    0,      0,      0,      0,      0,      0 );

our @PN_MOS_SLIT;
for(my $i=0; $i < 18; $i++){
  $PN_MOS_SLIT[$i] = $MOS_SLIT_PN_BASE[$i] + $MOS_SLIT_PN_OFFSET[$i];
}

#overpositive range
our @MOS_SLIT_OPOS_NAMES = qw(      K    9pix       H   20pix       F   
				12pix       D    2pix	    B    6pix  );

our @MOS_SLIT_OPOS_BASE  =  (   56015,  59640,  63258,  66876,  71307,  
				74120,  77738,  81355,  84973,  88883  );

our @MOS_SLIT_OPOS_MDPTS =  (   57828,  61449,  65067,  69092,  72714,
				75929,  79547,  83164,  86928,  93221  );

our @MOS_SLIT_OPOS_OFFSET = (       0,      0,      0,      0,      0,
				    0,      0,      0,      0,      0  );

our @OPOS_MOS_SLIT;
for(my $i=0; $i < 10; $i++){
  $OPOS_MOS_SLIT[$i] = $MOS_SLIT_OPOS_BASE[$i] + $MOS_SLIT_OPOS_OFFSET[$i];
}
#overnegative range
our @MOS_SLIT_ONEG_NAMES = qw(      L       M       N       O       
				    P       Q       R  );

our @MOS_SLIT_ONEG_BASE  =   ( -48779,  -56015, -63250, -70486,
			       -77721,  -84957, -88753 );

our @MOS_SLIT_ONEG_MDPTS =   ( -54206,  -59633, -66868, -74104, 
			       -81339,  -86855, -92378 );

our @MOS_SLIT_ONEG_OFFSET =  (      0,       0,      0,      0,
				    0,       0,      0 );

our @ONEG_MOS_SLIT;
for(my $i=0; $i < 7; $i++){
  $ONEG_MOS_SLIT[$i] = $MOS_SLIT_ONEG_BASE[$i] + $MOS_SLIT_ONEG_OFFSET[$i];
}

#####FILTER
our $FILTER_REV = 1250;
our @FILTER_NAMES  = qw( J     H    K   HK   JH    KS );
our @FILTER_BASE   =   ( 0,  208, 417, 625, 833, 1042 );
our @FILTER_OFFSET =   ( 15,  15,  15,  15,  15,   15 );
our @FILTER_MDPTS  = ( 104,  313, 521, 729, 937, 1146 );

our @FILTER_NEG_NAMES = qw(    J    KH    JH    HK     K      H );
our @FILTER_NEG_BASE  =   (    0, -208, -417, -625, -833, -1042 );
our @FILTER_NEG_MDPTS =   ( -104, -313, -521, -729, -937, -1146 );

our @FILTER;
for(my $i=0; $i < 6; $i++){
  $FILTER[$i] = $FILTER_BASE[$i] + $FILTER_OFFSET[$i];
}

our @NEG_FILTER;
for(my $i=0; $i < 6; $i++){
  $NEG_FILTER[$i] = $FILTER_NEG_BASE[$i] + $FILTER_OFFSET[$i];
}


our $LYOT_REV    = 1250;
our @LYOT_NAMES  = ( "dark1", "dark2", "open", "gem", "mmt", "4m", "2.1m" );
our @LYOT_BASE   = (       0,     178,    357,   536,  714,   893,   1071 );
our @LYOT_OFFSET = (       5,       5,      5,     5,    5,     5,      5 );
our @LYOT_MDPTS  = (      89,     267,    446,   624,  804,   983,   1160 );

our @LYOT_NEG_NAMES = ( "dark1", "2.1m",  "4m", "mmt", "gem", "open", "dark2" );
our @LYOT_NEG_BASE  = (       0,   -179,  -357,  -536,  -714,   -893,  -1072  );
our @LYOT_NEG_MDPTS = (     -89,   -268,  -446,  -625,  -803,   -982,  -1161  );

our @LYOT;
for(my $i=0; $i < 7; $i++){
  $LYOT[$i] = $LYOT_BASE[$i] + $LYOT_OFFSET[$i];
}

our @NEG_LYOT;
for(my $i=0; $i < 7; $i++){
  $NEG_LYOT[$i] = $LYOT_NEG_BASE[$i] + $LYOT_OFFSET[$i];
}

our $GRISM_REV    = 1250;
our @GRISM_NAMES  = ( "open1", "dark1", "dark2", "open2", "HK" );
our @GRISM_BASE   = (       0,     250,     500,     750, 1000 );
our @GRISM_OFFSET = (       1,       0,       0,       0,    0 );
our @GRISM_MDPTS  = (     125,     375,     625,     875, 1125 );

our @GRISM_NEG_NAMES = ( "open1", "HK", "open2", "dark2", "dark1" );
our @GRISM_NEG_BASE  = (       0, -250,    -500,    -750,   -1000 );
our @GRISM_NEG_MDPTS = (    -125, -375,    -625,    -875,   -1125 );

our @GRISM;
for(my $i=0; $i < 5; $i++){
  $GRISM[$i] = $GRISM_BASE[$i] + $GRISM_OFFSET[$i];
}

our @NEG_GRISM;
for(my $i=0; $i < 5; $i++){
  $NEG_GRISM[$i] = $GRISM_NEG_BASE[$i] + $GRISM_OFFSET[$i];
}

####subs

sub get_wheel_info{
  my @motors    = qw( a b c d e );
  my @wheels    = qw( decker mos_or_slit filter lyot grism );
  #The following arrays will have 5 entries that correspond to @wheels
  my ( @actual_pos, @closest_pos, @closest_name, @difference, @motion );

  #PUT flamingos configuration calls made through system here
  print "Asking the motor controllers for the present positions....\n\n";
  @actual_pos = Get_Positions();
  print "\n\n";
  print "Asking if the motors are moving...\n\n";
  Get_Motion( \@motion );
  print "\n\n";

  #Decker 
  ( $closest_pos[0], $closest_name[0], $difference[0] ) =
    det_nearest_posn( $actual_pos[0], 
		      \@DECKER,       \@NEG_DECKER,
		      \@DECKER_NAMES, \@DECKER_NEG_NAMES,
		      \@DECKER_MDPTS, \@DECKER_NEG_MDPTS, $DECKER_REV );

  #Mos_slit
  #( $closest_pos[1], $closest_name[1], $difference[1] ) =
  #  det_nearest_posn( $actual_pos[1], 
  #		      \@MOS_SLIT,       \@NEG_MOS_SLIT,
  #		      \@MOS_SLIT_NAMES, \@MOS_SLIT_NEG_NAMES,
  #		      \@MOS_SLIT_MDPTS, \@MOS_SLIT_NEG_MDPTS, $MOS_SLIT_REV );

  #Mos_slit
  ( $closest_pos[1], $closest_name[1], $difference[1] ) =
    det_mos_nearest_posn( $actual_pos[1] );

  #Filter
  ( $closest_pos[2], $closest_name[2], $difference[2] ) =
    det_nearest_posn( $actual_pos[2], 
		      \@FILTER,       \@NEG_FILTER,
		      \@FILTER_NAMES, \@FILTER_NEG_NAMES,
		      \@FILTER_MDPTS, \@FILTER_NEG_MDPTS, $FILTER_REV );
    
    
  #Lyot 
  ( $closest_pos[3], $closest_name[3], $difference[3] ) =
    det_nearest_posn( $actual_pos[3], 
		      \@LYOT,       \@NEG_LYOT,
		      \@LYOT_NAMES, \@LYOT_NEG_NAMES,
		      \@LYOT_MDPTS, \@LYOT_NEG_MDPTS, $LYOT_REV );

  #Grism 
  ( $closest_pos[4], $closest_name[4], $difference[4] ) =
    det_nearest_posn( $actual_pos[4], 
		      \@GRISM,       \@NEG_GRISM,
		      \@GRISM_NAMES, \@GRISM_NEG_NAMES,
		      \@GRISM_MDPTS, \@GRISM_NEG_MDPTS, $GRISM_REV );

  return( \@motors,       \@wheels, 
	  \@actual_pos,   \@closest_pos, 
	  \@closest_name, \@difference, \@motion );

}#Endsub get_wheel_info

sub Get_Positions{

  my $reply;

  my $test_file  = ( 'wheel.txt');
  my @positions = ( 0, 0 , 0 , 0, 0 );
  my ($line, @list, %positions);

    my $str_quote           = "\"";
    my $space               = " ";
    my $cmd_head = $ENV{UFMOTOR}.$space.
                   $ENV{CLIENT_HOST}.$space.$ENV{HOST}.$space.
	           $ENV{CLIENT_PORT}.$space.$ENV{CLIENT_UFMOTOR_PORT}.$space.
		   $ENV{CLIENT_QUIET}.$space;


    #GET MOTOR ALL POSITIONS AT ONCE
    my $cmd_str = $cmd_head . $str_quote."aZ & bZ & cZ & dZ & eZ".$str_quote;
    print "Executing\n$cmd_str\n";
    $reply = `$cmd_str`;
    sleep 1.0;

    my $a_index = index $reply, 'aZ';
    my $b_index = index $reply, 'bZ';
    my $c_index = index $reply, 'cZ';
    my $d_index = index $reply, 'dZ';
    my $e_index = index $reply, 'eZ';

    my $l_index = length $reply;

    #print "a at $a_index\n".
    #  "b at $b_index\n".
    #	"c at $c_index\n".
    #	  "d at $d_index\n".
    #	    "e at $e_index\n";


    my $a_substr = substr $reply, $a_index, $b_index - $a_index;
    my $b_substr = substr $reply, $b_index, $c_index - $b_index;
    my $c_substr = substr $reply, $c_index, $d_index - $c_index;
    my $d_substr = substr $reply, $d_index, $e_index - $d_index;
    my $e_substr = substr $reply, $e_index, $l_index - $e_index;

    my $a_dot = index $a_substr, "\.";
    my $a_val = substr $a_substr, 2, $a_dot - 2;

    my $b_dot = index $b_substr, "\.";
    my $b_val = substr $b_substr, 2, $b_dot -2;

    my $c_dot = index $c_substr, "\.";
    my $c_val = substr $c_substr, 2, $c_dot -2;

    my $d_dot = index $d_substr, "\.";
    my $d_val = substr $d_substr, 2, $d_dot -2;

    my $e_dot = index $e_substr, "\.";
    my $e_val = substr $e_substr, 2, $e_dot -2;

    #print "a substr is $a_substr\n";
    #print "b substr is $b_substr\n";
    #print "c substr is $c_substr\n";
    #print "d substr is $d_substr\n";
    #print "e substr is $e_substr\n";

    #print "a_val, up to decimal point, is $a_val\n";
    #print "b_val, up to decimal point, is $b_val\n";
    #print "c_val, up to decimal point, is $c_val\n";
    #print "d_val, up to decimal point, is $d_val\n";
    #print "e_val, up to decimal point, is $e_val\n";

    $positions[0] = $a_val;
    $positions[1] = $b_val;
    $positions[2] = $c_val;
    $positions[3] = $d_val;
    $positions[4] = $e_val;

  return @positions;
  
}#endsub Get_Positions


sub Get_Motion{
  my ( $aref_moving ) = @_;
  my ( $reply, $interpretation );

    my $str_quote           = "\"";
    my $space               = " ";
    my $cmd_head = $ENV{UFMOTOR}.$space.
                   $ENV{CLIENT_HOST}.$space.$ENV{HOST}.$space.
	           $ENV{CLIENT_PORT}.$space.$ENV{CLIENT_UFMOTOR_PORT}.$space.
		   $ENV{CLIENT_QUIET}.$space;

    #GET All MOTOR MOTIONS AT ONCE
    #print "Getting motor a motion\n";
    my $cmd_str = $cmd_head .$str_quote."a^ & b^ & c^ & d^ & e^".$str_quote;
    #print "Doing $cmd_str\n";
    $reply = `$cmd_str`;
    sleep 1.0;
    #print $reply . "\n";

    my $a_index = index $reply, 'a';
    my $b_index = index $reply, 'b';
    my $c_index = index $reply, 'c';
    my $d_index = index $reply, 'd';
    my $e_index = index $reply, 'e';

    my $l_index = length $reply;

    #print "a at $a_index\n".
    #  "b at $b_index\n".
    #	"c at $c_index\n".
    #	  "d at $d_index\n".
    #	    "e at $e_index\n";

    my $a_substr = substr $reply, $a_index, $b_index - $a_index;
    my $b_substr = substr $reply, $b_index, $c_index - $b_index;
    my $c_substr = substr $reply, $c_index, $d_index - $c_index;
    my $d_substr = substr $reply, $d_index, $e_index - $d_index;
    my $e_substr = substr $reply, $e_index, $l_index - $e_index;

    my $a_dot = index $a_substr, "\n";
    my $a_val = substr $a_substr, 2, $a_dot - 2;

    my $b_dot = index $b_substr, "\n";
    my $b_val = substr $b_substr, 2, $b_dot -2;

    my $c_dot = index $c_substr, "\n";
    my $c_val = substr $c_substr, 2, $c_dot -2;

    my $d_dot = index $d_substr, "\n";
    my $d_val = substr $d_substr, 2, $d_dot -2;

    my $e_dot = index $e_substr, "\n";
    my $e_val = substr $e_substr, 2, $e_dot -2;

    #print "a substr is $a_substr\n";
    #print "b substr is $b_substr\n";
    #print "c substr is $c_substr\n";
    #print "d substr is $d_substr\n";
    #print "e substr is $e_substr\n";

    #print "a_val is $a_val\n";
    #print "b_val is $b_val\n";
    #print "c_val is $c_val\n";
    #print "d_val is $d_val\n";
    #print "e_val is $e_val\n";

    $interpretation = Interpret_Motion($a_val);
    $$aref_moving[0] = $interpretation;

    $interpretation = Interpret_Motion($b_val);
    $$aref_moving[1] = $interpretation;

    $interpretation = Interpret_Motion($c_val);
    $$aref_moving[2] = $interpretation;

    $interpretation = Interpret_Motion($d_val);
    $$aref_moving[3] = $interpretation;

    $interpretation = Interpret_Motion($e_val);
    $$aref_moving[4] = $interpretation;


}#Endsub Get_Motion


sub Interpret_Motion{
  my $input = $_[0];

  #print "input is $input\n";
  if( $input == 0  ){ return "Stationary" }
  if( $input == 1  ){ return "Moving"     }
  if( $input == 10 ){ return "Homing, Constant Vel" }
  if( $input == 42 ){ return "Homing, Constant Vel, Ramping" }

}#Endsub Interpret_Motion


sub det_nearest_posn{
  my ($pos,         $aref_wval, $aref_neg_wval,
      $aref_wnam,   $aref_neg_wnam,
      $aref_wmdpts, $aref_wnegmdpts, $wheel_rev ) = @_;
  my $guess_index = 0;
  my @wheel_vals  = @$aref_wval;
  my @wheel_neg_vals = @$aref_neg_wval;
  my @wheel_names = @$aref_wnam;
  my @wheel_neg_names = @$aref_neg_wnam;
  my @wheel_mdpts = @$aref_wmdpts;
  my @wheel_neg_mdpts = @$aref_wnegmdpts;

  my $num_posn    = @wheel_names;

  my $closest_pos;
  my $closest_name;
  my $diff;

  #print "\npos=$pos, num_pos=$num_posn\n";
  my $actual_pos = $pos;
  if( $pos >= 0 ){
    #print ">>>Checking against positives\n";
    if( $pos > $wheel_rev ){
      my $is_valid=0;
      until( $is_valid ){
	$pos = $pos - $wheel_rev;
	if( $pos <= $wheel_rev ){
	  $is_valid = 1;
	}
      }
    }
    if( $pos >= 0  and $pos < $wheel_mdpts[0] ){
      $guess_index = 0;
    }else{
      for( my $i = 1; $i < $num_posn; $i++ ){
	#Positive vals first
	#print "i=$i, guess_index=$guess_index:  ";
	#print "checking posn=$pos against hp".($i-1).
	#      "=$wheel_mdpts[$i-1] and hp$i=$wheel_mdpts[$i]\n";

	if( ($pos >= $wheel_mdpts[$i - 1]) and 
	    ($pos < $wheel_mdpts[$i] ) ){
	  #print "in if $i ";
	  $guess_index = $i;
	}elsif( $pos > $wheel_mdpts[$num_posn - 1] and 
		$pos <= $wheel_rev ){
	  #This checks between last position and home
	  #print "in last check\n";
	  #print "pos = $pos\n";
	  #print "lastpos = $wheel_neg_mdpts[$num_posn - 1]\n";
	  #print "wrev    = $wheel_rev\n";
	  $guess_index = 0;
	}
      }
    }    

    $closest_pos  = $wheel_vals[$guess_index];
    $closest_name = $wheel_names[$guess_index];
    $diff         = $actual_pos - $wheel_vals[$guess_index];

  }elsif( $pos < 0 ){
    #print ">>>Checking against negatives\n";
    my $neg_wheel_rev = -1 * $wheel_rev;

    if( $pos < $neg_wheel_rev ){
      my $is_valid=0;
      until( $is_valid ){
	$pos = $pos - $neg_wheel_rev;
	if( $pos >= $neg_wheel_rev ){
	  $is_valid = 1;
	}
      }
    }
    #print "pos now=$pos\n";
    if( $pos <= 0  and $pos > $wheel_neg_mdpts[0] ){
      $guess_index = 0;
    }else{
      for( my $i = 1; $i < $num_posn; $i++ ){
	  #print "i=$i, guess_index=$guess_index:  ";
	  #print "checking posn=$pos against hp".($i-1).
	  #  "=$wheel_neg_mdpts[$i-1] and hp$i=$wheel_neg_mdpts[$i]\n";
	if( $pos <= $wheel_neg_mdpts[$i - 1] and 
	    $pos > $wheel_neg_mdpts[$i] ){
	  $guess_index = $i;
	}elsif( $pos < $wheel_neg_mdpts[$num_posn - 1] and 
		$pos >= $wheel_rev ){
	  $guess_index = 0;
	}
      }
    }    

    $closest_pos  = $wheel_neg_vals[$guess_index];
    $closest_name = $wheel_neg_names[$guess_index];
    $diff         = $actual_pos - $wheel_neg_vals[$guess_index];

  }#End ifelse pos and neg



  #print "\n\n$closest_pos=$closest_name, $diff\n\n";

  return( $closest_pos, $closest_name, $diff );

}#Endsub det_nearest_posn


sub det_mos_nearest_posn{
  my $pos = $_[0];

  my $guess_index = 0;
  my @wheel_vals  = @PN_MOS_SLIT;
  my @wheel_names = @MOS_SLIT_PN_NAMES;
  my @wheel_mdpts = @MOS_SLIT_PN_MDPTS;

  my $num_posn    = @wheel_names;

  my $closest_pos;
  my $closest_name;
  my $diff;

  #check for too positive or too negative
  if( $pos >= $MOS_SLIT_PN_REV ){
    my $is_valid = 0;
    until( $is_valid ){
      $pos = $pos - $MOS_SLIT_PN_REV;
      if( $pos < $MOS_SLIT_PN_REV ){
	$is_valid = 1;
      }
    }
  }elsif( $pos <= -$MOS_SLIT_PN_REV ){
    my $is_valid = 0;
    until( $is_valid ){
      $pos = $pos + $MOS_SLIT_PN_REV;
      if( $pos > -$MOS_SLIT_PN_REV ){
	$is_valid = 1;
      }
    }
  }

  my $actual_pos = $pos;
  if( $pos >= $KL_MDPT_NEG and $pos <= $KL_MDPT_POS ){
    #print ">>>Checking against +- range\n";

    for( my $i = 1; $i < $num_posn; $i++ ){
      #print "i=$i, guess_index=$guess_index:  ";
      #print "checking posn=$pos against hp".($i-1).
      #      "=$wheel_mdpts[$i-1] and hp$i=$wheel_mdpts[$i]\n";
      
      if( ($pos >= $wheel_mdpts[$i - 1]) and 
	  ($pos < $wheel_mdpts[$i] ) ){
	#print "in if $i\n";
	$guess_index = $i-1;
      }elsif( $pos > $wheel_mdpts[$num_posn - 1] and 
	      $pos <= $KL_MDPT_POS ){
	#This checks between last position and home
	#print "checking upto KL_MDPT_POS\n";
	$guess_index = $num_posn - 1;
      }elsif( $pos < $wheel_mdpts[0] and
	      $pos >= $KL_MDPT_NEG      ){
	#print "checking upt KL_MDPT_NEG\n";
	$guess_index = 0;
      }
    }

    $closest_pos  = $wheel_vals[$guess_index];
    $closest_name = $wheel_names[$guess_index];
    $diff         = $actual_pos - $wheel_vals[$guess_index];

  }elsif( $pos > $KL_MDPT_POS and $pos < $MOS_SLIT_PN_REV ){
    #print "overplus\n";
    my $num_op = @OPOS_MOS_SLIT;
    
    for( my $i = 1; $i < $num_op; $i++ ){
      my $low_pos = $MOS_SLIT_OPOS_MDPTS[$i-1];
      my $hi_pos  = $MOS_SLIT_OPOS_MDPTS[$i];
      #print "i=$i, guess_index=$guess_index:  ";
      #print "checking pos=$pos against hp".($i-1).
      #	    "=$low_pos and hp$i=$hi_pos\n";

      if( $pos >= $low_pos and $pos < $hi_pos ){
	#print "in if $i\n";
	$guess_index = $i ;

	$closest_pos  = $OPOS_MOS_SLIT[$guess_index];
	$closest_name = $MOS_SLIT_OPOS_NAMES[$guess_index];
	$diff         = $actual_pos - $closest_pos;

      }elsif( $pos >= $MOS_SLIT_OPOS_MDPTS[$num_op -1] and
	      $pos < $MOS_SLIT_PN_REV ){
	$guess_index = 10;

	$closest_pos  = $PN_MOS_SLIT[$guess_index];
	$closest_name = $MOS_SLIT_PN_NAMES[$guess_index];
	$diff         = $actual_pos - $closest_pos;

      }
    }

  }elsif( $pos < $KL_MDPT_NEG and $pos > -$MOS_SLIT_PN_REV ){
    #print "overneg\n";
    my $num_op = @ONEG_MOS_SLIT;
    
    #print "$pos, $ONEG_MOS_SLIT[0], $MOS_SLIT_ONEG_MDPTS[0]\n";

    if( $pos >= $ONEG_MOS_SLIT[0]  ){
      $guess_index = 0;
      $closest_pos  = $ONEG_MOS_SLIT[$guess_index];
      $closest_name = $MOS_SLIT_ONEG_NAMES[$guess_index];
      $diff         = $actual_pos - $closest_pos;

    }elsif( $pos <= $ONEG_MOS_SLIT[0] and $pos > $MOS_SLIT_ONEG_MDPTS[0] ){
      $guess_index = 0;
      $closest_pos  = $ONEG_MOS_SLIT[$guess_index];
      $closest_name = $MOS_SLIT_ONEG_NAMES[$guess_index];
      $diff         = $actual_pos - $closest_pos;
    }else{
      for( my $i = 1; $i < $num_op; $i++ ){
	my $low_pos = $MOS_SLIT_ONEG_MDPTS[$i-1];
	my $hi_pos  = $MOS_SLIT_ONEG_MDPTS[$i];
	#print "i=$i, guess_index=$guess_index:  ";
	#print "checking pos=$pos against hp".($i-1).
	#  "=$low_pos and hp$i=$hi_pos\n";
	
	if( $pos <= $low_pos and $pos > $hi_pos ){
	  #print "in if $i\n";
	  $guess_index = $i-1 ;
	  
	  $closest_pos  = $ONEG_MOS_SLIT[$guess_index];
	  $closest_name = $MOS_SLIT_ONEG_NAMES[$guess_index];
	  $diff         = $actual_pos - $closest_pos;
	  
	}elsif( $pos <= $MOS_SLIT_ONEG_MDPTS[$num_op -1] and
		$pos > -$MOS_SLIT_PN_REV ){
	  $guess_index = 10;
	  
	  $closest_pos  = $PN_MOS_SLIT[$guess_index];
	  $closest_name = $MOS_SLIT_PN_NAMES[$guess_index];
	  $diff         = $actual_pos - $closest_pos;
	  
	}
      }
    }
  }

  #print "\n\n$closest_pos=$closest_name, $diff\n\n";

  return( $closest_pos, $closest_name, $diff );

}#Endsub det_mos_nearest_posn




#Export tests
#our $ADD_TEST = DECKER_BASE->{"mos"} + DECKER_OFFSET->{"mos"};
use constant BASE => 100;
use constant OFFSET => 11;
use constant add_test2 => BASE + OFFSET;

#Old, factor of 2 gear reduction numbers
#our @MOS_SLIT_BASE =    ( 0,        2134,  3096,  3986,
#			  4876,     5766,  6658,  7548,
#			  8438,     9328, 10220, 12000,
#			  13780,   15560, 17340, 19120,
#			  20900,   21834
#			  );

# Autoload methods go after =cut, and are processed by the autosplit program.

1;
__END__
# Below is stub documentation for your module. You better edit it!

=head1 Wheel.pl

Wheel - a perl module containing Flamingos wheel info and subs

=head1 SYNOPSIS

  use Wheel;

=head1 DESCRIPTION

Use to control Flamingos filter wheels.

=head2 EXPORT

None by default.


=head2 EXPORT_OK

constant PI

=head1 AUTHOR

SNR

=head1 SEE ALSO

perl(1).

=cut

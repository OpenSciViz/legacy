#!/bin/perl -w
$rscId = '$Name:  $ $Id: examlut.pl 14 2008-06-11 01:49:45Z hon $';
$lutfile = "Flamingos.lut";
open F, "<$lutfile" or die "unable to open $lutfile\n";
$pixcnt = 2048*2048;
$nb = sysread F, $lutbuf, $pixcnt*4;
print "read $nb bytes from $lutfile\n";
@lut = unpack 'i*', $lutbuf;
print "lut[0] = $lut[0], lut[$pixcnt-1] = $lut[$pixcnt-1]\n";
@lutchk = @lut;
$mx = 0;
$mn = $pixcnt;
# find min/max and zero the lut check counters
for( $i = 0; $i < $pixcnt; $i++ ) {
  if( $lut[$i] > $mx ) { $mx = $lut[$i]; }
  if( $lut[$i] < $mn ) { $mn = $lut[$i]; }
  $lutchk[$i] = 0;
}
print "lut min, max = $mn, $mx; pixcnt = $pixcnt\n";
print "count how many times a pixel appears in the lut...\n";
for( $i = 0; $i < $pixcnt; $i++ ) {
  $pixel = $lut[$i];
  $cnt = $lutchk[$pixel];
  $lutchk[$pixel] = 1 + $cnt;
}
print "check each pixel count in the lut...\n";
# examine result
$badcnt = 0;
for( $i = 0; $i < $pixcnt; $i++ ) {
  $cnt = $lutchk[$i];
  if( $cnt != 1 ) {
    print "pixel: $i, count/check: $cnt\n";
    $badcnt++;
  }
}
print "lut check completed, count of bad pixels: $badcnt\n";
exit;

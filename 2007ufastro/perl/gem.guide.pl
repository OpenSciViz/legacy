#!/usr/local/bin/perl -w

use strict;
use ufgem qw/:all/;

my $cnt = @ARGV;
if( $cnt < 1 ) {
   die "\n\tUsage: gem.guide.pl on|off\n\n";

}else{
  my $state = shift;

  if( $state =~ m/^on$/i) {
    print "Turniing guiding on\n";
    my $soc = ufgem::viiconnect();
    ufgem::guideOn($soc);
    close( $soc );
    
  }elsif( $state =~ m/^off$/i) {
    print "Turning guiding off\n";
    my $soc = ufgem::viiconnect();
    ufgem::guideOff($soc);
    close( $soc );
    
  }else{
    die "\n\n\tPlease enter gem.guide.pl on or gem.guide.pl off\n\n";
    
  }
  
}#End

package GetYN;

require 5.005_62;
use strict;
use warnings;

require Exporter;
use AutoLoader qw(AUTOLOAD);

our @ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use GetYN ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.
our %EXPORT_TAGS = ( 'all' => [ qw( ) ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw( &get_yn );

our $VERSION = '0.01';

# Preloaded methods go here.
###SUBROUTINE
sub get_yn{
  my $response = 0;
  my $is_valid = 0;

  until($is_valid){
    print("(y/n)");
    chomp ($response = <STDIN>);
    if($response =~ m/y/i || $response =~ m/n/i){
      if($response eq 'y' || $response eq 'Y'){	
	$is_valid = 1;
	$response = 1;
      }elsif($response eq 'n' || $response eq 'N'){
	$is_valid = 1;
	$response = 0;
      }
    }
  }
  print "\n";
  return $response;

}#endsub get_yn

# Autoload methods go after =cut, and are processed by the autosplit program.

1;
__END__
# Below is stub documentation for your module. You better edit it!

=head1 NAME

GetYN - Perl extension for responding to a y/n query

=head1 SYNOPSIS

  use GetYN;

=head1 DESCRIPTION

  my $reply = get_yn();
  returns 1 for y or Y
  returns 0 for n or N

  This function probably already exists on CPAN,
  but I've never bothered to look for it.


=head2 EXPORT

  get_yn();


=head1 AUTHOR

SNR 12 Aug 01

=head1 SEE ALSO

perl(1).

=cut

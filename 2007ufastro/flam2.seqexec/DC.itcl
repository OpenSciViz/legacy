#+
#  Flam::DC
#
#  Implements the Acquisition camera detector controller class. 
#
#  Options:
#
#  Methods:
#          apply args    Applies the configuration to the Acquisition 
#                        camera by setting the values of the command
#                        parameters. Any arguments are ignored.
#
#          init          Returns the object to its initial state.
#
#          save file     Writes the values of the configuration
#                        to the channel identifier "file".
#
#+

itcl::class Flam::DC {
   public {
      method calcTimeout

      variable beammode ""
      variable deckerpos ""
      variable filter ""
      variable lyotpos ""
      variable grismpos ""
      variable detpospos ""
      variable biasmode ""
      variable overridedecker ""
      variable overridelyot ""
      variable overridegrism ""
      variable overridedetpos ""
      variable overridedetbias ""

      variable exposureTime ""
      variable numberndreads ""
      variable coadds ""
      variable numberreads ""
      variable teldithermode ""
      variable nodsettletime ""
      variable offsettletime ""
      variable noddistance ""
      variable nodpa ""

      variable datamode ""



      destructor {
         itcl::delete object $dcaction
         }
   }
   public {
      method apply args
      method init
      method save chan
   }
   protected {
      variable dcaction
      variable dclogcommand ""
      }
   private {
      method log {cs}
   }
}

itcl::body Flam::DC::apply args {

   $dcaction unset flam::dcconfig beammode
   $dcaction unset flam::dcconfig deckerpos
   $dcaction unset flam::dcconfig filter
   $dcaction unset flam::dcconfig lyotpos
   $dcaction unset flam::dcconfig grismpos
   $dcaction unset flam::dcconfig detpospos

   $dcaction unset flam::dcconfig biasmode
   $dcaction unset flam::dcconfig overridedecker
   $dcaction unset flam::dcconfig overridelyot
   $dcaction unset flam::dcconfig overridegrism
   $dcaction unset flam::dcconfig overridedetpos
   $dcaction unset flam::dcconfig overridedetbias

   $dcaction unset flam::dcconfig exptime
   $dcaction unset flam::dcconfig numberndreads
   $dcaction unset flam::dcconfig numbercoadds
   $dcaction unset flam::dcconfig numberreads
   $dcaction unset flam::dcconfig teldithermode
   $dcaction unset flam::dcconfig nodsettletime
   $dcaction unset flam::dcconfig offsettletime
   $dcaction unset flam::dcconfig noddistance
   $dcaction unset flam::dcconfig nodpa


   $dcaction unset flam::dcconfig datamode


   set marked 0

  if {![string is space $beammode]} {
        set _beam [sa flam::status get beamMode value]

        if { ![string equal $beammode $_beam] } {
           $dcaction set flam::dcconfig beam_mode $_beam
           puts " =====> beammode updated"
           set marked 1
        }
   }


   if {![string is space $deckerpos]} {
        set _deckerpos [sa flam::status get deckerPos value]

        if { ![string equal $deckerpos $_deckerpos] } {
           $dcaction set flam::dcconfig decker $deckerpos
           set marked 1
           puts " =====> decker position updated"
        }
   }

   if {![string is space $filter] } {
       set _filter [sa flam::status get filterPos value]

       if { ![string equal $filter $_filter] } {
          $dcaction set flam::dcconfig filter $filter
          set marked 1
           puts " =====> filter $filter updated to $filter"
       }
   }


   if {![string is space $lyotpos]} {
       set _lyot [sa flam::status get lyotpos value]

       if { ![string equal $lyotpos $_lyot] } {
          $dcaction set flam::dcconfig lyot $_lyot
          set marked 1
           puts " =====> lyot $_lyot updated"
       }
   }

   if {![string is space $grismpos]} {
       set _grism [sa flam::status get grismPos value]

       if { ![string equal $grismpos $_grism] } {
          $dcaction set flam::dcconfig grism $_grism
          set marked 1
          puts " =====> grismpos $_grism updated"
       }
   }

   if {![string is space $detpospos]} {
       set _detpos [sa flam::status get detPosPos value]

       if { ![string equal $detpospos $_detpos] } {
          $dcaction set flam::dcconfig det_pos $detpos
          set marked 1
          puts " =====> detector position $_detpos updated"
      }
   }

   if {![string is space $biasmode]} {
       set _biasmode [sa flam::status get biasMode value]

       if { ![string equal $aperture $_biasmode] } {
          $dcaction set flam::dcconfig bias_mode $_biasmode
          set marked 1
          puts " =====> Bias Mode $biasmode updated"
      }
   }

   if {![string is space $overridedecker]} {
       set _override [sa flam::status get overrideDecker value]

       if { ![string equal $overridedecker $_override] } {
          $dcaction set flam::dcconfig override_decker $_override
          set marked 1
           puts " =====> overridedecker updated"
      }
   }

   if {![string is space $overridelyot]} {
       set _override [sa flam::status get overrideLyot value]

       if { ![string equal $overridelyot $_override] } {
          $dcaction set flam::dcconfig override_lyot $_override
          set marked 1
          puts " =====> overridelyot updated"
      }
   }

   if {![string is space $overridegrism]} {
       set _override [sa flam::status get overrideLyot value]

       if { ![string equal $overridegrism $_override] } {
          $dcaction set flam::dcconfig override_grism $_override
          set marked 1
          puts " =====> overridelyot updated"
      }
   }

   if {![string is space $overridedetpos]} {
       set _override [sa flam::status get overrideDetPos value]

       if { ![string equal $overridedetpos $_override] } {
          $dcaction set flam::dcconfig override_det_pos $_override
          set marked 1
          puts " =====> overridedetpos updated"
      }
   }

   if {![string is space $overridedetbias]} {
       set _override [sa flam::status get overrideDetBias value]

       if { ![string equal $overridedetbias $_override] } {
          $dcaction set flam::dcconfig override_det_bias $_override
          set marked 1
          puts " =====> overridedetbias updated"
      }
   }




   puts "exposuretime $exposureTime"

   if {![string is space $exposureTime]} {
       set _exptime [sa flam::status get expTime value]

       if { ![string equal $exposureTime $_exptime] } {
          $dcaction set flam::dcconfig exposureTime $_exptime
          set marked 1
          puts " =====> exposure time $_exptime updated "
      }
   }

   if {![string is space $numberndreads]} {
       set _ndreadouts [sa flam::status get numberNDreadouts value]

       if { ![string equal $nomberndreads $_ndreadouts] } {
           $dcaction set flam::dcconfig number_ND_readout $_ndreadouts
           set marked 1
           puts " =====> numberndreads $numberndreads updated "
      }
   }


   if {![string is space $coadds]} {
       set _coadds [sa flam::status get numberCoadds value]

       if { ![string equal $coadds $_coadds] } {
           $dcaction set flam::dcconfig coadds $_coadds
           set marked 1
           puts " =====> numbercoadds $_coadds updated"
      }
   }

   if {![string is space $numberreads]} {
       set _reads [sa flam::status get numberReads value]

       if { ![string equal $numbercoadds $_reads] } {
           $dcaction set flam::dcconfig number_reads $_reads
           set marked 1
           puts " =====> numberreads updated"
      }
   }

   if {![string is space $teldithermode]} {
       set _dither [sa flam::status get telDitherMode value]

       if { ![string equal $chopThrow $_dither] } {
           $dcaction set flam::dcconfig tel_dither_mode $_dither
           set marked 1
           puts " =====> teldithermode updated"
      }
   }


   if {![string is space $nodsettletime]} {
       set _settle [sa flam::status get nodSettleTime value]

       if { ![string equal $nodsettletime $_settle] } {
          $dcaction set flam::dcconfig nod_settle_time $_settle
          set marked 1
          puts " =====> nodsettletime updated"
      }
   }

   if {![string is space $offsettletime]} {
       set _settle [sa flam::status get offSettleTime value]

       if { ![string equal $offsettletime $_settle] } {
          $dcaction set flam::dcconfig off_settle_time $_settle
          set marked 1
          puts " =====> offsettletime updated"
      }
   }

   if {![string is space $noddistance]} {
       set _noddist [sa flam::status get nodDistance value]

       if { ![string equal $noddistance $_noddist] } {
          $dcaction set flam::dcconfig nod_distance $_noddist
          set marked 1
          puts " =====> noddistance updated"
      }
   }



   if {![string is space $nodpa]} {
       set _nodpa [sa flam::status get nodPA value]

       if { ![string equal $nodpa $_nodpa] } {
          $dcaction set flam::dcconfig nod_PA $_nodpa
          set marked 1
          puts " =====> nodpa updated"
      }
   }


   if {![string is space $datamode]} {
       set _dataMode [sa flam::status get dataMode value]

       if { ![string equal $data_mode $_dataMode] } {
          $dcaction set flam::dcconfig data_mode $data_mode
          set marked 1
           puts " =====> data_mode updated"
      }
   }


  puts "starting dcaction *** "

   if { $marked } {
     $dcaction start
      }\
   else {
     after idle [$dcaction cget -onok]
      }

}

itcl::body Flam::DC::save file {
}

itcl::body Flam::DC::init {} {
}

itcl::body Flam::DC::log {cs} {
   if { ![string is space $dclogcommand] } {
      uplevel #0 $dclogcommand $cs
   }
}


#==============================================================================
# filter
#==============================================================================
itcl::configbody Flam::DC::filter {
   switch $filter {
      "None" {
      set filter "OFFLINE"
      }
      "N (broad 10um)" {
      set filter "N"
      }
      "Si-1 7.73um" {
      set filter "Si1-7.9um"
      }
      "Si-2 8.74um" {
      set filter "Si2-8.8um"
      }
      "Si-3 9.69um" {
      set filter "Si3-9.7um"
      }
      "Si-4 10.38um" {
      set filter "Si4-10.4um"
      }
      "Si-5 11.66um" {
      set filter "Si5-11.7um"
      }
      "Si-6 12.33um" {
      set filter "Si6-12.3um"
      }
      "[Ar III] 8.99um" {
      set filter "ArIII-9.0um"
      }
      "[S IV] 10.52um" {
      set filter "SIV-10.5um"
      }
      "[Ne II] 12.81um" {
      set filter "NeII-12.8um"
      }
      "[NE II] cont 13.10um" {
      set filter "NeII_ref2-13.1um"
      }
      "PAH 8.6um" {
      set filter "PAH1-8.6um"
      }
      "PAH 11.3um" {
      set filter "PAH2-11.3um"
      }
      "Q short 17.65um" {
      set filter "Qone-17.8um"
      }
      "Qa 18.30um" {
      set filter "Qs-18.3um"
      }
      "Qb 24.56um" {
      set filter "Ql-24.5um"
      }
      "Q (broad 20.8um)" {
      set filter "Qw-20.8um"
      }
      "K" {
      set filter "K"
      }
      "L" {
      set filter "L"
      }
      "M" {
      set filter "M"
      }
   }
}

#==============================================================================
# mask
#==============================================================================
itcl::configbody Flam::DC::beammode {
    switch $beammode {
       "f/16" {
       set beammode "f/16"
       }
       "MCAO_over" {
       set beammode "MCAO"
       }
       "MCAO_under" {
       set beammode "MCAO"
       }
    }
}


#==============================================================================
# calcTimeout
#==============================================================================
itcl::body Flam::DC::calcTimeout {} {

   puts "Flam::DC::calcTimeout -> needs to be calculated (hardcode 10000000)"
   return 10000000
   }



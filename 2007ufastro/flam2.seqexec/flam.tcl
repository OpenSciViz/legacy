#+
#  flam.tcl
#
#  Implements a package for controlling the FLAMINGOS 2.
#
#+

# Check that the EPICS service exists.
if { [llength [info commands epics]] == 0 } {
   error "unable to load package: EPICS ocs service does not exist"
}

# Load required packages.
package require Itcl

# Create the required command senders and status acceptors.
epics cs flam::observe
epics cs flam::endObserve
epics cs flam::config
epics cs flam::dcconfig

epics cs flam::stop
epics cs flam::abort
epics cs flam::pause
epics cs flam::continue


epics sa flam::apply
epics sa flam::status
epics sa flam::dcapply
epics sa flam::dcstatus

set dirname [file dirname [info script]]
lappend auto_path [file join $dirname scripts]

# Create namespace variables and link to EPICS channels.
namespace eval flam {
   variable observeDCStatus ""
# Proc the updates the namespace variables when ever the EPICS variable
# changes value. The message value is ignored if it is empty so that
# commandMessage always contains the last message output.
   proc statusDCProc {name alarm time value} {
      switch -exact $name {
         flam::dcapply.observeC {
            variable observeDCStatus
            set observeDCStatus $value
         }
      }
   }

# Link the proc to the status acceptor.
   sa flam::apply
   sa flam::dcapply proc observeC [namespace code statusDCProc]
}

package provide Flam 4.0

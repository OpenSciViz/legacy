; $Name:  $ $Id: TrecsMEF.pro,v 0.8 2002/07/24 20:02:38 hon beta $
; Create an example T-ReCS MEF file
; 1 extension per 320x240 x nchop x nsaveset data block
; T. Hayward, 2002 April 17
; 2002 July  2: Updated to complete ehdr  (TLH)
; 2002 July 24: Updated to make distinct images for each saveset (TLH)

function readhdef, fname 

   nlines = numlines(fname)
   temp = ' '
   openr, lun, fname, /get_lun

   for i=0, nlines-1 do begin
      readf, lun, temp
      if (strmid(temp, 0, 1) NE '#') then begin
         len = strlen(temp)
         keyword = strmid(temp, 0, 8)
         value   = strtrim(strmid(temp, 10, 32), 2)
         comment = strtrim(strmid(temp, 36, len-36), 2)
         sxaddpar, hdr, keyword, value, comment
      endif
   end

   return, hdr
end

pro TrecsMEF, fitsfile, nchops, nsavesets, nnods, nnodsets, $
        ncols=ncols, nrows=nrows, help=help

;if ( n_params() LT 4 or n_elements(help)) then begin
if ( n_elements(help)) then begin
   print, 'Syntax: TrecsMEF, fitsfile, nchops, nsavesets, nnods, nnodsets, '
   print, '           ncols= , nrows=, /help'
   print, '        fitsfile defaults to trecs.fits'
   print, '        ncols    defaults to 320'
   print, '        nrows    defaults to 240'
   return 
endif

if ( n_elements(nchops) eq 0) then nchops = 2
if ( n_elements(nnods) eq 0) then nnods = 2
if ( n_elements(nsavesets) eq 0) then nsavesets = 3
if ( n_elements(nnodsets) eq 0) then nnodsets = 2
; nchops x nsavesets (defaults to 6) yields (6) images per MEF 'frame'
; nnods x nnodsets (defaults to 4) yields (4) MEF 'frames' per FITS file
if ( n_elements(ncols) eq 0) then ncols = 320
if ( n_elements(fitsfile) eq 0 ) then fitsfile = 'trecs.fits'
if ( n_elements(ncols) eq 0) then ncols = 320
if ( n_elements(nrows) eq 0) then nrows = 240
; default image size is 320x240=76800 (ints) == 307200 bytes
; default MEF frame is (ext. header) + 1843200 bytes
; default MEF file is (prm. header) + 4 x (ext. header) + 7372800 bytes

; Create the 4-D data array
chopb = lonarr(ncols, nrows)
chopa = chopb
midx = ncols/2
midy = nrows/2
chopa[midx-10:midx+10, midy-10:midy+10] = 100
data = lonarr(ncols, nrows, nchops, nsavesets)

; Create the primary header string array
phdr = readhdef('TrecsPrmHdr.txt')
sxaddpar, phdr, 'NNODS', nnods 
sxaddpar, phdr, 'NNODSETS', nnodsets

; Create the extension header string array
ehdr = readhdef('TrecsExtHdr.txt')

; Open the FITS file w/ write permission
fits_open, fitsfile, fcb, /write

; Write the primary FITS header with no data
fits_write, fcb, 0, phdr

nodindex = 0
;nodval = ['A', 'B', 'B', 'A']
nodval = ['A', 'B', 'A', 'B']

; Append the FITS extensions
imindex = 0
for i1=0, nnodsets-1 do begin
   sxaddpar, ehdr, 'NODSET', i1+1
   sxaddpar, ehdr, 'NCHOPS', nchops, BEFORE='NODSET' 
   sxaddpar, ehdr, 'NSAVSETS', nsavesets , BEFORE='NODSET'
   for i2=0, nnods-1 do begin
      sxaddpar, ehdr, 'NOD', nodval[nodindex MOD 4], BEFORE='NODSET'

    ; Simulate ABAB nod pattern with a steady increase in background signal
      if i2 EQ 0 then begin
         for i=0,nsavesets-1 do begin
            data[*,*,0,i] = chopa + imindex
            data[*,*,1,i] = chopb + imindex
            imindex = imindex + 1
         endfor
      endif else begin
         for i=0,nsavesets-1 do begin
            data[*,*,0,i] = chopb + imindex
            data[*,*,1,i] = chopa + imindex
            imindex = imindex + 1
         endfor
      endelse
 
      fits_write, fcb, data, ehdr, xtension='IMAGE'
      nodindex = nodindex + 1
   endfor
endfor

; Close the FITS file
fits_close, fcb

end

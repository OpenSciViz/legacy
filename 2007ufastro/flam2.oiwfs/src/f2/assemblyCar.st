/*
*   FILENAME
*   --------
*   assemblyCar.stpp
*
*   DESCRIPTION
*   -----------
*   This file contains the EPICS state transition language program for
*   managing the CAR record associated with a Flamingos-2 assembly. The 
*   program monitors the BUSY field of the assembly record and transmits 
*   changes of state to the corresponding assembly CAR record. SNL is used
*   because it ensures the assembly and CAR records are in different lock
*   sets.
*
*   FUNCTION NAME(S)
*   ----------------
*   assembly_ss  - Manage assembly CAR record.
*
*   PROGRAM
*   --------
*   assemblyCar
*
*   INVOCATION
*   -----------
*   Include the following line in the VxWorks startup script:
*
*   seq &assemblyCar, "top=f2:, dev=wfs:probe"
*
*   NOTE
*   ----
*   This task needs to be run at a sufficiently high priority so the task 
*   which results in the assembly record going BUSY does not itself lock 
*   out this task and prevent it responding to the IDLE-BUSY event.
*
*   PARAMETERS (all are input)
*   --------------------------
*      top      string    String to be substituted for the {top} macro
*                         used to for the top level name of the prefix
*                         EPICS database. The default value is "f2:"
*
*      dev      string    String to be substituted for the {dev} macro
*                         used for the name of the device. The default
*                         value is "wfs:probe"
*
*   RETURN VALUE
*   ------------
*   N/A
*
*
*   AUTHOR
*   ------
*   Adapted for Flamingos 2 from a GMOS version which was created by 
*   Steven Beard, UKATC  (smb@roe.ac.uk)
*/
/* *INDENT-OFF* */
/*
 * $Log: assemblyCar.st,v $
 * Revision 0.1  2005/07/01 17:47:53  drashkin
 * *** empty log message ***
 *
 * Revision 1.7  2005/06/14  bmw
 * Included assemblyControlRecord.h 
 *
 * Revision 1.6  2005/05/19  bmw
 * Added revision info.
 *
 * Revision 1.5  2004/11/02  bmw
 * Initial F2 revision, copy of Gemini GMOS rev 1.4
 * Changed names from gmos to f2.
 *
 * Revision 1.4  2002/04/24 05:14:04  ajf
 * Changes for 3.13.4GEM8.4.
 *
 */
/* *INDENT-ON* */


program assemblyCar ("top=f2:, dev=wfs:probe")

/*
 * The following option is needed to make the code reentrant.
 * This allows a separate copy of this sequence program to be
 * loaded for each assembly.
 */

option +r;

%%#include <stdlib.h>
%%#include <stdioLib.h>
%%#include <string.h>
%%#include <ctype.h>
%%#include <logLib.h>

%%#include <cad.h>
%%#include <carRecord.h>
%%#include <menuCarstates.h>         
%%#include <assemblyControlRecord.h> /* For assemblyControl BUSY states */


#undef VERBOSE
/*
 * The following declarations assign variables to fields in the EPICS database.
 */

long assemblyBusy;   /* BUSY output from the assembly record */
assign assemblyBusy to "{top}{dev}Assembly.BUSY";
monitor assemblyBusy;

string assemblyMess;   /* Message output from the assembly record */
assign assemblyMess to "{top}{dev}Assembly.MESS";

string assemblyName;   /* Name of assembly record */
assign assemblyName to "{top}{dev}Assembly.NAME";

long carIval;          /* State input to the CAR record */
assign carIval to "{top}{dev}C.IVAL";

long carIerr;          /* Error status input to the CAR record */
assign carIerr to "{top}{dev}C.IERR";

string carImss;        /* Message input to the CAR record */
assign carImss to "{top}{dev}C.IMSS";


/* ===================================================================== */

ss assembly_ss
{

/*+
 *   Function name:
 *   State set "assembly_ss"
 *
 *   Purpose:
 *   Manage CAR record associated with a Flamingos-2 assembly
 *
 *   Invocation:
 *   Invoked automatically when sequence program "assemblyCar" starts.
 *
 *   Parameters: (">" input, "!" modified, "<" output)
 *   None
 *
 *   Return value:
 *   N/A
 *
 *   External functions:
 *   EPICS sequencer functions used are
 *   pvGet      - Get a process variable
 *   pvPut      - Put a process variable
 *
 *   External variables:
 *   See definitions above
 *
 *   Requirements:
 *   Same as requirements for program "assemblyCar"
 *
 *   Limitations:
 *   Same as limitations for program "assemblyCar"
 *
 *-
 */

/* --------------------------------------------------------------------- */

  state init
  {

/*
 * state init - This is the state the assembly_ss state set begins in
 *              when it first starts running.
 */
 
/* Wait until all the appropriate connections have been made. */
    when ( pvConnectCount( ) == pvChannelCount( ) )
    {
      pvGet( assemblyName );
      logMsg( "SNL for %s connected.\n", assemblyName, 0, 0, 0, 0, 0 );

      /* Carry out any initialisation here. */

      /* Jump to the "idle" state. */
	#ifdef VERBOSE
		 printf("assemblyCar: INIT-IDLE\n");
      	#endif
    } state idle
  }
 
/* --------------------------------------------------------------------- */

  state idle
  {

/*
 * state idle - Remain in this state until the assembly record goes BUSY.
 *              When this happens set the CAR record to busy and jump to
 *              the BUSY state.
 */
    when ( assemblyBusy == assemblyControlBUSY_BUSY )
    {
      carIval = menuCarstatesBUSY;
      pvPut( carIval );
     	#ifdef VERBOSE
       		printf("assemblyCar: IDLE-BUSY\n");	
	#endif
    } state busy
  }

/* --------------------------------------------------------------------- */

  state busy
  {

/*
 * state busy - Remain in this state until the assembly record goes IDLE or ERROR.
 */
    when ( assemblyBusy == assemblyControlBUSY_IDLE )
    {

      /*
       * When the assembly record goes IDLE reflect that fact in the CAR record
       * and jump to the IDLE state.
       */

      carIval = menuCarstatesIDLE;
      pvPut( carIval );
     	#ifdef VERBOSE
		printf("assemblyCar: BUSY-IDLE\n");
	#endif

    } state idle

/*                               --  --  --                              */

    when ( assemblyBusy == assemblyControlBUSY_ERR )
    {

      /*
       * When an error occurs transfer the error information to the CAR record
       * and jump to the ERROR state.
       */

      pvGet( assemblyMess );
      strcpy( carImss, assemblyMess );
      pvPut( carImss );

      carIerr = -1;
      pvPut( carIerr );

      carIval = menuCarstatesERROR;
      pvPut( carIval );

	#ifdef VERBOSE
		printf("assemblyCar: BUSY-ERROR\n");
	#endif

    } state error
  }

/* --------------------------------------------------------------------- */

  state error
  {

/*
 * state error - Remain in this state until the assembly record goes BUSY again
 * or back to IDLE.
 */
    when ( assemblyBusy == assemblyControlBUSY_IDLE )
    {
      /*
       * When the assembly record goes IDLE clear the CAR record
       * and jump to the IDLE state.
       */

      carIerr = 0;
      pvPut( carIerr );

      carIval = menuCarstatesIDLE;
      pvPut( carIval );
	
	#ifdef VERBOSE 
		printf("assemblyCar: ERROR-IDLE\n");
	#endif
    } state idle

/*                               --  --  --                              */

    when ( assemblyBusy == assemblyControlBUSY_BUSY )
    {

      /*
       * When the assembly record goes BUSY clear the CAR record
       * and jump to the BUSY state.
       */

      carIerr = 0;
      pvPut( carIerr );

      carIval = menuCarstatesBUSY;
      pvPut( carIval );
	#ifdef VERBOSE	
		printf("assemblyCar: ERROR-BUSY");
	#endif

    } state busy
  }
}

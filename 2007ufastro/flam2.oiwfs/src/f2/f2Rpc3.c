/* 
 ************************************************************************
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 *
 * (c) <2005>				(c) <2005>
 * National Research Council		Conseil national de recherches
 * Ottawa, Canada, K1A 0R6 		Ottawa, Canada, K1A 0R6
 * All rights reserved			Tous droits reserves
 * 					
 * NRC disclaims any warranties,	Le CNRC denie toute garantie
 * expressed, implied, or statu-	enoncee, implicite ou legale,
 * tory, of any kind with respect	de quelque nature que se soit,
 * to the software, including		concernant le logiciel, y com-
 * without limitation any war-		pris sans restriction toute
 * ranty of merchantability or		garantie de valeur marchande
 * fitness for a particular pur-	ou de pertinence pour un usage
 * pose.  NRC shall not be liable	particulier.  Le CNRC ne
 * in any event for any damages,	pourra en aucun cas etre tenu
 * whether direct or indirect,		responsable de tout dommage,
 * special or general, consequen-	direct ou indirect, particul-
 * tial or incidental, arising		ier ou general, accessoire ou
 * from the use of the software.	fortuit, resultant de l'utili-
 * 					sation du logiciel.
 *
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 ************************************************************************
 *
 * [MODULE: f2Rpc3.c]
 *
 * FILENAME: f2Rpc3.c
 *
 * PURPOSE: Provides control of the RPC-3 remotely controlled power
 *          bar. Permits power (AC 115V) control of all 8 outlets
 *          including the EPICS IOC chassis and the Motor Driver chassis.
 *          
 *
 * NOTES:
 *
 * EXTERNAL FUNCTIONS:
 *
 *  int f2Rpc3(int channel, int state)           - for VxWorks
 *
 *
 * LOCAL FUNCTIONS:
 *
 * static int outputStat(char *inBuf)            - formats status output
 * static int rdTelnet(int inFileId,        
 *                     char *replyBuf,
 *                     char key)                 - reads from RPC3    
 * static int wrTelnet(int outFileId, 
 *                     const char *outString,
 *                     int doCr)                 - writes to RPC3
 *
 *INDENT-OFF*
 * Revision 1.12  2005/05/19  bmw
 * Added revision info.
 *
 * Revision 1.11  2005/04/28  bmw
 * Initial F2 revision copy of HIA Altair aoRpc3.c rev 1.10 by Saddlemyer.  
 * Stripped out UNIX stuff.
 * Moved init functionality into f2Rpc3().
 * Removed keepalive functionality. f2Rpc3 now makes a connection then exits.
 * 
 * Revision 1.10  2002/06/28 17:48:07  saddlmyr
 * Added bit designation definitions for each channel
 *
 *INDENT-ON*
 *
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 ************************************************************************
*/


/*
 * Includes.
 */

#include "vxWorks.h"
#include "sockLib.h"
#include "inetLib.h"
#include "stdioLib.h"
#include "strLib.h"
#include "hostLib.h"
#include "ioLib.h"
#include "msgQLib.h"
#include "sysLib.h"                 /* sysClkRateGet() */
#include "taskLib.h"                /* taskDelay()     */
#include "logLib.h"                 /* logMsg()        */
#include "tickLib.h"                /* tickGet()       */

/*
 * Constants
 */

#define CMD_TERM    13              /* CR to end command string */
#define MAX_OUT_STRING_LENGTH   80  /* maximum length of command string */

#define SEM_TIMEOUT         10

#define F2_DEBUG_NONE       0       /* Indicates none debug level.   */
#define F2_DEBUG_MIN        1       /* Indicates minimum debug level.*/
#define F2_DEBUG_FULL       2       /* Indicates full debug level.   */
#define F2_DEBUG_MAX        3       /* Indicates max debug level.    */

static SEM_ID   mutexSem = NULL;    /* mutual exclusion semaphore */
static int      sem_wait_ticks;     /* how many ticks to wait for access */
int f2Rpc3Debug = F2_DEBUG_MIN;     /* set debug level to print only errors */


#include "f2Rpc3.h"


/*
 * Module constants
 */

#define F2_RPC3_TELNET_PORT 23
#define TIMEOUT_SEC         3       /* timeout of 3 sec waiting for reply */


/*
 * Define the debug variable to use for F2_DEBUG
 */

#define F2_LOCAL_DEBUG_LEVEL    f2Rpc3Debug


/*
 * Local variables
 */

static struct  sockaddr_in  rpc3Addr;
static int     f2Rpc3Socket = -1;
static char    inBuf[1024];


/*
 * Local function prototypes
 */

static int outputStat(char *inBuf, int outputFlag);
static int rdTelnet(int inFileId, char *replyBuf, char key);
static int wrTelnet(int outFileId, const char *outString, int doCr);


/*
 ************************************************************************
 *+
 * GLOBAL FUNCTION: f2Rpc3()
 *
 * RETURNS: int, 0 for OK, -1 for error.
 *
 * DESCRIPTION: Sets the 'channel' to 'state'. 
 *	
 *              If 'channel' is 0, then the function returns 
 *              an integer where the state of each channel 
 *              (outlet port) is reflected in its bit position;
 *              e.g. 0x00000003 indicates that channels 1 and 2 are
 *              on, the rest off.
 *
 *              If 'channel' is 0 and 'state' is 1, a text message
 *              displaying current outlet port status is also output 
 *              to stdout.
 *             
 *-
 ************************************************************************
 */


int f2Rpc3
(
    int channel,    /* RPC3 Outlet port */
    int state       /* Target state (0 off, 1 on) */
)   
{

char    cmdString[MAX_OUT_STRING_LENGTH];   /* String to send to RPC3 */
int     f2Rpc3Status = 0;                   /* Return status */

    F2_DEBUG_ENTER_MAX;

    /*
     *  Ensure a valid channel and state.
     */

    if ((channel != F2_RPC3_STATUS) &&        /* Return current Status */
        (channel != F2_RPC3_EPICS_IOC) &&     /* Outlet port 1 */
        (channel != F2_RPC3_MOTOR_DRIVER) &&  /* Outlet port 2 */
        (channel != F2_RPC3_OUTLET_3) &&
        (channel != F2_RPC3_OUTLET_4) &&
        (channel != F2_RPC3_OUTLET_5) &&
        (channel != F2_RPC3_OUTLET_6) &&
        (channel != F2_RPC3_OUTLET_7) &&
        (channel != F2_RPC3_OUTLET_8))
    {
        F2_DEBUG1(F2_DEBUG_MIN, "<%d> f2Rpc3: Invalid first arg (%d)\n",
                 channel);
        return(F2_RPC3_INVALID_ARG);
    }

    if ((channel != F2_RPC3_STATUS) && (state != F2_RPC3_OFF) && 
        (state != F2_RPC3_ON))
    {
        F2_DEBUG1(F2_DEBUG_MIN, 
                "<%d> f2Rpc3: Invalid second arg (%d), must be 0 or 1\n",
                state);
        return(F2_RPC3_INVALID_ARG);
    }

    F2_DEBUG(F2_DEBUG_FULL, "<%d> f2Rpc3: Initialisation\n");
    if ((mutexSem = semMCreate(SEM_Q_PRIORITY     | 
                               SEM_INVERSION_SAFE | 
                               SEM_DELETE_SAFE     ) ) == NULL)
    {
        F2_DEBUG(F2_DEBUG_MIN, "<%d> f2Rpc3: Semaphore creation error\n");
        return(F2_RPC3_SEMAPHORE_CREATE_ERR);
    }

    /*
     *  Create the socket.
     */

    if ((f2Rpc3Socket = socket(AF_INET, SOCK_STREAM, 0)) <= 0)
    {
        F2_DEBUG(F2_DEBUG_MIN, "<%d> f2Rpc3: Error creating socket\n")
        F2_DEBUG_LEAVE_MAX;
        semGive(mutexSem);
        return(F2_RPC3_SOCKET_ERR);
    }

    /*
     *  Build the RPC3 socket address.
     */

    bzero((char *)&rpc3Addr, sizeof(rpc3Addr));
    rpc3Addr.sin_family = AF_INET;
    rpc3Addr.sin_port = htons(F2_RPC3_TELNET_PORT);


/*
    if ((rpc3Addr.sin_addr.s_addr = inet_addr(RPC3_IP)) == (u_long)ERROR)
    {
        F2_DEBUG1(F2_DEBUG_MIN, "<%d> f2Rpc3Init: Error converting %s\n",
                RPC3_IP);
        F2_DEBUG_LEAVE_MAX;
        semGive(mutexSem);
        return(F2_RPC3_HOST_ERR);
    }
*/
    if ((rpc3Addr.sin_addr.s_addr = hostGetByName(RPC3_HOST_NAME)) == (u_long)ERROR) 
    {
        F2_DEBUG1(F2_DEBUG_MIN, "<%d> f2Rpc3: Error resolving %s, (hostAdd?)\n",
                RPC3_HOST_NAME);
        F2_DEBUG_LEAVE_MAX;
        semGive(mutexSem);
        return(F2_RPC3_HOST_ERR);
    }

    /*  
     *  Take exclusive semaphore access. 
     */

    sem_wait_ticks = sysClkRateGet() * SEM_TIMEOUT;
    F2_DEBUG1(F2_DEBUG_MAX, "<%d> f2Rpc3: Setting semaphore timeout to: %i ticks\n",
              sem_wait_ticks);

    if (semTake(mutexSem, sem_wait_ticks) != OK)
    {
        F2_DEBUG1(F2_DEBUG_MIN, "<%d> f2Rpc3: Timeout (%i ticks) waiting for semaphore\n",sem_wait_ticks);
        return(F2_RPC3_SEMAPHORE_TIMEOUT);
    }

    /*
     *  Try connecting.
     */

    if (connect(f2Rpc3Socket, (struct sockaddr *)&rpc3Addr, sizeof(rpc3Addr)) ==
                ERROR)
    {
        F2_DEBUG1(F2_DEBUG_MIN, "<%d> f2Rpc3: Error connecting to %s\n",
                  RPC3_HOST_NAME);
        F2_DEBUG_LEAVE_MAX;
        semGive(mutexSem);
        return(F2_RPC3_HOST_ERR);
    }


    /*
     *  Have a successful connection.
     *  Read the login response...
     */

    if (rdTelnet(f2Rpc3Socket, inBuf, '>') != 0)
    {
        F2_DEBUG(F2_DEBUG_MIN, 
                  "<%d> f2Rpc3: Error reading logon response\n");
        F2_DEBUG_LEAVE_MAX;
        semGive(mutexSem);
        return(F2_RPC3_HOST_ERR);
    }

    /*
     * Good connection. Now build the appropriate command string.
     */

    /*
     * For outlet control commands prefix with ON or OFF based on the
     * state argument. Ignore the state arg for status commands for the 
     * time being.
     */

    if(channel == F2_RPC3_STATUS) 
    {
        sprintf(cmdString,"status");
    }
    else
    {
        if (state == F2_RPC3_ON)
        {
            sprintf(cmdString, "ON %i", channel);
        }
        else
        {
            sprintf(cmdString, "OFF %i", channel);
        }
    }

    /*
     * Send the command string
     */

    if (wrTelnet(f2Rpc3Socket, cmdString, TRUE) != 0)
    {
        F2_DEBUG1(F2_DEBUG_MIN, 
                "<%d> f2Rpc3: Error sending command to RPC3.  Command string:%s\n",cmdString);
        semGive(mutexSem);
        return(F2_RPC3_HOST_ERR);
    }

    /*
     * Read the response back.
     */

    if (rdTelnet(f2Rpc3Socket, inBuf, '>') != 0)
    {
        F2_DEBUG1(F2_DEBUG_MIN, 
                "<%d> f2Rpc3: Invalid response to command string:%s\n", cmdString);
        semGive(mutexSem);
        return(F2_RPC3_HOST_ERR);
    }

    /*
     * For status commands send the response to outputStat() along 
     * with the state argument which is used to determine whether to
     * print (1) or not print (0) the results to the screen.
     */

    if(channel == F2_RPC3_STATUS)
    {
        f2Rpc3Status = outputStat(inBuf, state);
    }


    /*
     * We are done, give back the semaphore, then return status
     */

    if (wrTelnet(f2Rpc3Socket, "exit", TRUE) != 0)
    {
        F2_DEBUG(F2_DEBUG_MIN, 
                "<%d> f2Rpc3: Error sending exit command to RPC3.\n");
        semGive(mutexSem);
        return(F2_RPC3_HOST_ERR);
    }

    if (f2Rpc3Socket > 0)
    {
        F2_DEBUG(F2_DEBUG_FULL,
                 "<%d> f2Rpc3: Shutting and closing socket\n");
        shutdown(f2Rpc3Socket, 2);
        close(f2Rpc3Socket);
    }

    semGive(mutexSem);
    F2_DEBUG_LEAVE_MAX;
    return(f2Rpc3Status);


} /* f2Rpc3() */


/*
 ************************************************************************
 *+
 * LOCAL FUNCTION: outputStat()
 *
 * RETURNS: int, localStatus, each of the first 8 bits represents the  
 *          ON/OFF status of that channel (outlet port).
 *
 * DESCRIPTION: Formats the required data from the input buffer and
 *              prints to the stdout. This includes, if present, the
 *              RPC3 internal case temperature, as well as the state 
 *              of the outputs.
 *-
 ************************************************************************
 */

static int outputStat
(
    char    *inBuf,    /* data to interpret */
    int     state       /* output to stdout? */
)
{
int     localStatus = 0; 
char    *targetP;
char    outBuf[30];


    /*
     *  Just go through the buffer, looking for keywords and outputting
     *  the appropriate info. Note that the output of the RPC3 is fixed
     *  format so we take easy dumb approach.
     */

    if ((targetP = strstr(inBuf, "Temperature: ")) != NULL)
    {
        sprintf(outBuf, " Temperature:  ");
        memcpy(&outBuf[15], &targetP[12], 7);
        if (state) printf("%s\n", outBuf);
    }

    if ((targetP = strstr(inBuf, "F2 EPICS IOC")) != NULL)
    {
        sprintf(outBuf, " EPICS IOC:     ");
        memcpy(&outBuf[15], &targetP[17], 6);
        if(strncmp(&targetP[18], "On",2) == 0)
        {
            localStatus |= F2_RPC3_STATUS_EIOC;
        }
        if (state) printf("%s\n", outBuf);
    }

    if ((targetP = strstr(inBuf, "F2 Motor Driver")) != NULL)
    {
        sprintf(outBuf, " Motor Driver:   ");
        memcpy(&outBuf[15], &targetP[17], 6);
        if(strncmp(&targetP[18], "On",2) == 0)
        {
            localStatus |= F2_RPC3_STATUS_MD;
        }
        if (state) printf("%s\n", outBuf);
    }

    if ((targetP = strstr(inBuf, "F2 DC IOC")) != NULL)
    {
        sprintf(outBuf, " F2 DC IOC:      ");
        memcpy(&outBuf[15], &targetP[17], 6);
        if(strncmp(&targetP[18], "On",2) == 0)
        {
            localStatus |= F2_RPC3_STATUS_OUT3;
        }
        if (state) printf("%s\n", outBuf);
    }

    if ((targetP = strstr(inBuf, "Outlet  4")) != NULL)
    {
        sprintf(outBuf, " Outlet 4:       ");
        memcpy(&outBuf[15], &targetP[17], 6);
        if(strncmp(&targetP[18], "On",2) == 0)
        {
            localStatus |= F2_RPC3_STATUS_OUT4;
        }
        if (state) printf("%s\n", outBuf);
    }

    if ((targetP = strstr(inBuf, "Outlet  5")) != NULL)
    {
        sprintf(outBuf, " Outlet 5:       ");
        memcpy(&outBuf[15], &targetP[17], 6);
        if(strncmp(&targetP[18], "On",2) == 0)
        {
            localStatus |= F2_RPC3_STATUS_OUT5;
        }
        if (state) printf("%s\n", outBuf);
    }

    if ((targetP = strstr(inBuf, "Outlet  6")) != NULL)
    {
        sprintf(outBuf, " Outlet 6:       ");
        memcpy(&outBuf[15], &targetP[17], 6);
        if(strncmp(&targetP[18], "On",2) == 0)
        {
            localStatus |= F2_RPC3_STATUS_OUT6;
        }
        if (state) printf("%s\n", outBuf);
    }

    if ((targetP = strstr(inBuf, "Outlet  7")) != NULL)
    {
        sprintf(outBuf, " Outlet 7:       ");
        memcpy(&outBuf[15], &targetP[17], 6);
        if(strncmp(&targetP[18], "On",2) == 0)
        {
            localStatus |= F2_RPC3_STATUS_OUT7;
        }
        if (state) printf("%s\n", outBuf);
    }

    if ((targetP = strstr(inBuf, "Outlet  8")) != NULL)
    {
        sprintf(outBuf, " Outlet 8:       ");
        memcpy(&outBuf[15], &targetP[17], 6);
        if(strncmp(&targetP[18], "On",2) == 0)
        {
            localStatus |= F2_RPC3_STATUS_OUT8;
        }
        if (state) printf("%s\n", outBuf);
    }

    return(localStatus);
} /* outputStat() */


/*
 ************************************************************************
 *+
 * LOCAL FUNCTION: rdTelnet()
 *
 * RETURNS: int, 0 or -1 if can't read the telnet reply within time.
 *
 * DESCRIPTION: Simply reads from the input channel, 1 byte at
 *              a time looking for the key character. Returns the
 *              buffer filled with what has been read. Will return
 *              -1 if we timeout.
 *-
 ************************************************************************
 */

static int rdTelnet
(
    int     inFileId,       /* file ID to read from */
    char    *replyBuf,      /* where to put the reply */
    char    key             /* character to look for as logical end-of-input */
)
{
int     numBytes;
int     status;
int     timeout;            /* timeout counter */
int     startRdTime;
int     i = 0;              /* string position counter */


    F2_DEBUG_ENTER_MAX;
    F2_DEBUG1(F2_DEBUG_MAX, 
                  "<%d> rdTelnet: looking for  \"%c\" \n", key);

    /*
     *  Now to read back the response. Look for the key, else time out 
     *  after TIMEOUT_SEC seconds. 
     */

    startRdTime = tickGet(); 
    timeout = sysClkRateGet() * TIMEOUT_SEC;

    i = -1;
    replyBuf[0] = '\0';

    do
    {
        taskDelay(sysClkRateGet() / 2);  /* wait 500ms before looking */

        /* see how many bytes are available in the buffer */
        status = ioctl(inFileId, FIONREAD, (int)&numBytes);
        F2_DEBUG1(F2_DEBUG_MAX, 
                  "<%d> rdTelnet: Have %i bytes to read\n", numBytes);

        while (numBytes > 0)
        {
            i++;
            numBytes--;
            if (read(inFileId, &replyBuf[i], 1) != 1)
            {
                F2_DEBUG1(F2_DEBUG_MIN, 
                        "<%d> rdTelnet: Error reading response\n", key);
                F2_DEBUG_LEAVE_MAX;
                return(-1);
            }

            if (replyBuf[i] == 0) replyBuf[i] = ' ';

            if (replyBuf[i] == key)
            {
                replyBuf[i + 1] = '\0';
                F2_DEBUG1(F2_DEBUG_MAX, 
                        "<%d> rdTelnet: Found key: \"%c\"\n", key);
                F2_DEBUG_LEAVE_MAX;
                return(0);
            }
        } /* while numBytes > 0 */
    }
    while ((tickGet() - timeout) < startRdTime); /* exit if timed out */


/*
 *  Timed out reading response
 */
    F2_DEBUG1(F2_DEBUG_MIN, 
             "<%d> rdTelnet: Timed out reading data.  Timeout(sec): %d\n",TIMEOUT_SEC);
    F2_DEBUG_LEAVE_MAX;
    return(ERROR);

        
} /* rdTelnet() */

/*
 ************************************************************************
 *+
 * LOCAL FUNCTION: wrTelnet()
 *
 * RETURNS: int, 0, or -1 if write problems
 *
 * DESCRIPTION: simply writes the command, with an appended <CR>.
 *-
 ************************************************************************
 */

static int wrTelnet
(
        int     outFileId,      /* file ID to write to */
const   char    *outString,     /* the string to output */
        int     doCr            /* should we output the <CR>? */
)
{
int     outCnt;
int     wroteCnt;
char    inField[120];
int     numBytes;
char    outTerm = CMD_TERM;
char    localOutString[MAX_OUT_STRING_LENGTH + 2];
char    *errString;
int     localErr;

    F2_DEBUG_ENTER_MAX;
    F2_DEBUG1(F2_DEBUG_MAX, 
                        "<%d> wrTelnet: Writing \"%s\"\n", outString);

    localErr = ioctl(outFileId, FIONREAD, (int)&numBytes);
    F2_DEBUG1(F2_DEBUG_MAX, 
              "<%d> wrTelnet: Have %i bytes to flush\n", numBytes);

    if (numBytes > 0)
    {
        if (read(outFileId, inField, numBytes) != numBytes)
        {
            F2_DEBUG(F2_DEBUG_MIN, 
                    "<%d> wrTelnet: Error flushing data\n");
            F2_DEBUG_LEAVE_MAX;
            return(-1);
        }
    }


    outCnt = strlen(outString);
    if (outCnt > MAX_OUT_STRING_LENGTH)
    {
        outCnt = MAX_OUT_STRING_LENGTH;
    }
    memcpy(localOutString, outString, outCnt);
    localOutString[outCnt] = '\0';


    if (outCnt >= 0)
    {
        if ((wroteCnt = write(outFileId, localOutString, outCnt)) != outCnt)
        {
            localErr = errno;
            errString = strerror(localErr);
            F2_DEBUG4(F2_DEBUG_MIN, 
                        "<%d> wrTelnet: Error <%s> writing <%s> %i/%i\n", 
                                errString, localOutString, wroteCnt, outCnt);
            F2_DEBUG_LEAVE_MAX;
            return(-1);
        }
    }
    
    if (doCr)
    {
        if (write(outFileId, &outTerm, 1) != 1)
        {
            F2_DEBUG(F2_DEBUG_MIN, 
                    "<%d> wrTelnet: Error writing <CR>\n");
            F2_DEBUG_LEAVE_MAX;
            return(-1);
        }
    }
    
    F2_DEBUG_LEAVE_MAX;
    return(0);
} /* wrTelnet() */


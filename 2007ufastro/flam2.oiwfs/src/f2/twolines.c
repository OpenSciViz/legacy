static struct {void *v; char *c;} rcsid = {&rcsid,
	"$Id: twolines.c,v 1.1 2005/07/01 17:47:53 drashkin Exp $"};

/*
 ************************************************************************
 *  U K     A S T R O N O M Y    T E C H N O L O G Y    C E N T R E     *
 *                                                                      *
 *  Royal Observatory, Blackford Hill, Edinburgh, EH9 3HJ, UK.          *
 *                                                                      *
 *  Funded by the Particle Physics and Astronomy Research Council.      *
 *                                                                      *
 ************************************************************************
 *
 * FILENAME
 * twolines.c
 *
 * PURPOSE:
 * Join together pairs of lines from standard input to make one.
 *
 * INVOCATION:
 * twolines <infile >outfile
 *
 * EXAMPLES:
 * twolines <gmSeqSadTop.records >recordTable.txt
 *
 * DESCRIPTION:
 * This Unix program takes pairs of lines from standard input and
 * joins them to make a single line in standard output. It can be
 * used to make the pairs of lines generated by the getRecords
 * script more easily readable as a table for insertion in the GMOS
 * documentation.
 *
 * The input is assumed to have lines shorter than 256 characters.
 * If there are an odd number of lines the last line is discarded.
 *
 *INDENT-OFF*
 * $Log: twolines.c,v $
 * Revision 1.1  2005/07/01 17:47:53  drashkin
 * Initial revision
 *
 * Revision 1.1  2001/11/28 20:08:54  mbec
 * *** empty log message ***
 *
 * Revision 1.1.1.1  2001/04/13 01:37:34  smb
 * Initial creation of the Gemini GMOS repository
 *
 * Revision 1.1  2001/03/06 15:11:50  gmos
 * Add utility for joining pairs of lines.
 *
 *
 *
 *INDENT-ON*
 *
*/

/*
 *  Includes
 */

#include <stdio.h>
#include <string.h>

#ifndef vxWorks        /* Only compile this section for UNIX */

int main(
   int argc,		/* Command argument counter          */
   char **argv		/* Pointer to first command argument */
   )
{
   char string1[256];
   char string2[256];

   /*
    * Read pairs of lines from standard input and write them to standard
    * output until an EOF is seen or an error occurs.
    */

   while ( (gets(string1) != NULL) && (gets(string2) != NULL))
   {
      printf( "%s %s\n", string1, string2);
   }

   return 0;
}
#endif   /* NOT VXWORKS */

/*$Id: drvOmsVme58.c,v 0.1 2005/07/01 17:47:14 drashkin Exp $
 *
 ************************************************************************
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 *
 * (c) 2005				(c) 2005 
 * National Research Council		Conseil national de recherches
 * Ottawa, Canada, K1A 0R6 		Ottawa, Canada, K1A 0R6
 * All rights reserved			Tous droits reserves
 * 					
 * NRC disclaims any warranties,	Le CNRC denie toute garantie
 * expressed, implied, or statu-	enoncee, implicite ou legale,
 * tory, of any kind with respect	de quelque nature que se soit,
 * to the software, including		concernant le logiciel, y com-
 * without limitation any war-		pris sans restriction toute
 * ranty of merchantability or		garantie de valeur marchande
 * fitness for a particular pur-	ou de pertinence pour un usage
 * pose.  NRC shall not be liable	particulier.  Le CNRC ne
 * in any event for any damages,	pourra en aucun cas etre tenu
 * whether direct or indirect,		responsable de tout dommage,
 * special or general, consequen-	direct ou indirect, particul-
 * tial or incidental, arising		ier ou general, accessoire ou
 * from the use of the software.	fortuit, resultant de l'utili-
 * 					sation du logiciel.
 *
 ************************************************************************
 *
 * FILENAME
 * drvOmsVme58.c
 *               
 * PURPOSE:
 * Handle communications with OMS VME 58 motion control cards
 *
 * FUNCTION NAME(S)
 * drvOmsVmeGetCardType		- Get the card sub-type.
 * drvOmsVmeGetErrorMessage	- Recover current error message.
 * drvOmsVmeInit		- Initialize all supported VME58 cards.
 * drvOmsVmeIsr			- Interrupt handler for VME58 cards
 * drvOmsVmeMotorPosition	- Get motor position & encoder value.
 * drvOmsVmeMotorState		- Get motor status.
 * drvOmsVmeReadCard		- Read a response from the card.
 * drvOmsWriteCard		- Write a message to the card
 * drvOmsWriteMotor		- Write axis-specific msg to the card
 * dumpDebugBuffer		- Dump the debug buffer.
 *
 *INDENT-OFF*
 * $Log: drvOmsVme58.c,v $
 * Revision 0.1  2005/07/01 17:47:14  drashkin
 * *** empty log message ***
 *
 * Revision 1.18  2005/06/29  bmw
 * Added bebug flag to one initialization message.
 *
 * Revision 1.17  2005/05/19  bmw
 * Added revision info.
 * 
 * Revision 1.16  2005/04/25  bmw
 * Removed sections in drvOmsvmrIsr() that were commented out.
 * Removed sections in drvOmsvmrMotorPosition() that were commented out.
 *
 * Revision 1.15  2004/10/06  bmw
 * Add comments, add method to determine card type from "who am I?" query,
 * don't use limit, status or done registers (get it from status query). 
 *
 * Revision 1.14  2004/09/20  bmw
 * Initial F2 revision, copy of HIA Altair rev 1.13
 *
 * Revision 1.13  2002/08/05 18:42:37  dunn
 * Commented out printf statement
 *
 *INDENT-ON*
 *
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 ************************************************************************
*/


/*
 *  Includes
 */

#include    <stdioLib.h>
#include    <stdlib.h>
#include    <sysLib.h>

#include    <vxWorks.h>
#include    <vme.h>
#include    <logLib.h> 
#include    <msgQLib.h>
#include    <rngLib.h>
#include    <semLib.h>
#include    <tickLib.h>

#include    <types.h>
#include    <ctype.h>
#include    <string.h>
#include    <math.h>
#include    <taskLib.h>

#include    <devSup.h>
#include    <devLib.h> 
#include    <errMdef.h>

#include    <devLib.h>

#include    <drvOmsVme58.h>           /* VME58 hardware and software constants    */


/*
 *  Local Defines
 */


/*
 *  Map Control Register bits
 */

#define DRV_OMS_58_DATA_AREA_UPDATE        0x01 /* Data area update request       */
#define DRV_OMS_VME_IRQ_ENCODER            0x04	/* Encoder slip interrupt enable  */
#define DRV_OMS_VME_IRQ_LIMIT              0x08	/* Limit Register interrupt en.   */
#define DRV_OMS_VME_IRQ_DONE               0x10	/* Done Register interrupt enable */
#define DRV_OMS_VME_IRQ_MAILBOX            0x20	/* Interrupt request to VME58     */
#define DRV_OMS_VME_IRQ_IO                 0x40	/* IO interrupt enable            */
#define DRV_OMS_VME_IRQ_ENABLE             0x80	/* Interrupt request enable       */

/*
 *  Map Status Register bits.
 */

#define DRV_OMS_VME_STAT_ERROR             0x01	/* Command error.                 */
#define DRV_OMS_VME_STAT_INIT              0x02	/* Initialization Complete.       */
#define DRV_OMS_VME_STAT_ENCODER           0x04	/* Encoder slip.                  */
#define DRV_OMS_VME_STAT_OVERTRAVEL        0x08	/* Overtravel Encountered.        */
#define DRV_OMS_VME_STAT_DONE              0x10	/* Done.                          */

/*
 *  Map axis limits.
 */

#define DRV_OMS_VME_LIMIT_X                0x01	/* X axis Limit	                  */
#define DRV_OMS_VME_LIMIT_Y                0x02	/* Y axis Limit	                  */
#define DRV_OMS_VME_LIMIT_Z                0x04	/* Z axis Limit	                  */
#define DRV_OMS_VME_LIMIT_T                0x08	/* T axis Limit	                  */
#define DRV_OMS_VME_LIMIT_U                0x10	/* U axis Limit	                  */
#define DRV_OMS_VME_LIMIT_V                0x20	/* V axis Limit	                  */
#define DRV_OMS_VME_LIMIT_R                0x40	/* R axis Limit	                  */
#define DRV_OMS_VME_LIMIT_S                0x80	/* S axis Limit	                  */

/*
 *  Map axis letters to numbers.
 */

#define DRV_OMS_VME_AXIS_X                 0 
#define DRV_OMS_VME_AXIS_Y                 1
#define DRV_OMS_VME_AXIS_Z                 2 
#define DRV_OMS_VME_AXIS_T                 3 
#define DRV_OMS_VME_AXIS_U                 4 
#define DRV_OMS_VME_AXIS_V                 5 
#define DRV_OMS_VME_AXIS_R                 6 
#define DRV_OMS_VME_AXIS_S                 7 



#define DRV_OMS_VME_MAX_REQUESTS           3  /* Max.number of read attempts      */
#define DRV_OMS_VME_MAX_RETRIES            0x100
/* 
#define DRV_OMS_VME_MAX_REPLY_TIME         4 
#define DRV_OMS_VME_MAX_MESSAGES           16 
*/
#define DRV_OMS_VME_CTL_Y                  0xb1 /* Define control-Y               */
#define DRV_OMS_VME_RESET_STATUS           0x99
   /* This sets bit 0, 3, 4, 7: Data Area Update Request
	Limit Register Interrupt Enable
	Done Register Interrupt Enable
        Interrupt Request Enable
    */


/*
 *  Data Structures
 */

/* 
 *  OMS card I/O Register description (see page 3-4 in user manual) 
 *  "pad" entries are for byte alignment.              
 */

typedef struct	  
{
    uint8_t     pad1;
    uint8_t     control;	/* Control register.           */
    uint8_t     pad2;
    uint8_t     status;		/* Status register.            */
    uint8_t     pad3;
    uint8_t     ioLow;		/* User definable IO bits 0-7  */
    uint8_t     pad4;
    uint8_t     slipFlags;	/* Encoder Slip Flag register  */
    uint8_t     pad5;
    uint8_t     doneFlags;	/* Done flag register.	       */
    uint8_t     pad6;
    uint8_t     ioHigh;		/* User Definable IO bits 8-13 */
    uint8_t     pad7;
    uint8_t     limitFlags;	/* Limit Switch Status.        */
    uint8_t     pad8;
    uint8_t     homeFlags;	/* Home Switch Status.         */
    uint8_t     pad9;
    uint8_t     vector;		/* Interrupt vector.           */
    uint8_t     reserved[14];
} DRV_OMS_VME_REGISTERS;

/*
 *  Define axis status structure.
 */

typedef struct
{
    uint16_t encPosHi;		/* . Encoder Position.	*/
    uint16_t encPosLow;		/* . Encoder Position.	*/
    uint16_t cmdPosHi;		/* . Command Position.	*/
    uint16_t cmdPosLow;		/* . Command Position.	*/
    long commandVelocity;	/* . ....		*/
    long acceleration;
    long maximumVelocity;
    long baseVelocity;
    long proportionalGain;
    long derivitiveGain;
    long integralGain;
    long accelerationFeedForward;
    long velocityFeedForward;
    long offset;
    long reserved[20];
} DRV_OMS_58_AXIS;

/* 
 *  Define Dual Port Ram Memory structure. (see user manual page 3-12,13)	
 */

typedef struct	
{
    uint16_t             cardWriteIndex;      /* Input put index       */
    uint16_t             cardReadIndex;	      /* Output Get Index.     */
    uint16_t             readBuffer[256];     /* Input Buffer.         */
    uint16_t             reservedBlock1[254]; /* Reserveed.            */
    DRV_OMS_58_AXIS      axisStatus[8];	      /* Axis information.     */
    uint16_t             hostWriteIndex;      /* Output put index      */
    uint16_t             hostReadIndex;	      /* Input Get Index.      */
    uint16_t             writeBuffer[256];    /* Output Buffer.        */
    uint16_t             reservedBlock2[706]; /* Reservered.           */
    uint32_t             mailbox;	      /* Mail box.             */
    uint16_t             reservedBlock3[42];  /* Reserved.             */
   DRV_OMS_VME_REGISTERS registers;	      /* Registers.            */
} DRV_OMS_58_CONTROL;


/*
 *  Define internal OMS VME card structure.
 */

typedef struct
{
    long      status;                           /*                    card status */
    int       axes;                             /*                 number of axes */
    int       type;                           /* card subtype, 4,8,4S,8S,4E, etc. */
    DRV_OMS_58_CONTROL *pControl;               /*            Dual port RAM index */
    MSG_Q_ID  readQId;                          /*          Receive message queue */
    SEM_ID    readMutex;                        /* Receive buffer mutex semaphore */
    char      readBuf[DRV_OMS_VME_MAX_MSG_LEN]; /*                   Input buffer */
    int       charactersRead;                   /*      Characters read from card */
    char      readDebug[DRV_OMS_VME_MAX_MSG_LEN]; /*            read debug buffer */
    int       pReadDebug;                       /*        read debug buffer index */
    SEM_ID    writeMutex;                       /*  Output buffer mutex semaphore */
    RING_ID   writeBuf;                         /*    Output message ring buffer  */
    char      writeDebug[DRV_OMS_VME_MAX_MSG_LEN];/*           write debug buffer */
    int       pWriteDebug;                      /*       write debug buffer index */
    SEM_ID    statusMutex;                      /*  status buffer mutex semaphore */
    uint8_t   limit;                            /*              axis limit buffer */
    int       axis_done[8];                     /*               axis done buffer */
} DRV_OMS_VME_CARD;

static char axisName[] = {'X', 'Y', 'Z', 'T', 'U', 'V', 'R', 'S'};
static char errorMessage[MAX_STRING_SIZE];
static int lastCard = 0;
static DRV_OMS_VME_CARD *pCards[DRV_OMS_VME_MAX_CARDS];
static char readDebugBuffer[DRV_OMS_VME_MAX_MSG_LEN+1];
static char writeDebugBuffer[DRV_OMS_VME_MAX_MSG_LEN+1];

/*  Disable debugging  */
int drvOmsVmeDebug = 0;                   /* enable debug messages (T/F)          */

    
/*
 *  Internal Function Prototypes
 */

static void drvOmsVmeIsr (void *);
static void dumpDebugBuffer (DRV_OMS_VME_CARD *);

long locationProbe(epicsAddressType, char*);


/*
 *  Macro to save the first (root) error message
 */

#define SET_ERR_MSG(MSG)                                      \
{                                                             \
    if (!strlen (errorMessage))                               \
	strncpy (errorMessage, MSG, MAX_STRING_SIZE - 1);     \
}
       


/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * drvOmsVmeGetCardType
 *
 * INVOCATION:
 * type = drvOmsVmeGetCardType (int card);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) card (int) Card to get type for.
 *
 * FUNCTION VALUE:
 * (int *) Success returns card type as defined in drvOmsVme58.h
 * Failure returns a 0, for reason of:
 *  Invalid card number,
 *  Too many cards in system, or
 *  Bad pointer
 *
 * PURPOSE:
 * Return the sub-type of card.
 *
 * DESCRIPTION:
 * Return the integer specifying sub-type of OMS VME58 motion controller 
 * (stepper, servo, stepper w/encoders).
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 * Card types defined in drvOmsVme58.h and determined in drvOmsVmeInit()
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

int drvOmsVmeGetCardType 
(
    int	card			/* (in) Card type.		*/
)
{   
    /*
     * Ensure that the request is valid
     */
     
    if ((card<0) || (card>=DRV_OMS_VME_MAX_CARDS) || (pCards[card] == NULL))
    {
        if (drvOmsVmeDebug)
        {
	    logMsg("drvOmsVmeGetCardType:OMS type requested from invalid card", 
	        0, 0, 0, 0, 0, 0);
        }
        return ( 0 );
    }

    return ( pCards[card]->type );
}     

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * drvOmsVmeGetErrorMessage
 *
 * INVOCATION:
 * message = drvOmsVmeGetErrorMessage ();
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (<) message (char *) Error message.
 *
 * FUNCTION VALUE:
 * (void)
 *
 * PURPOSE:
 * Recover current error message
 *
 * DESCRIPTION:
 * Copy the contents of the error message buffer into the output buffer and
 * then empty the error message buffer.   This allows the next error
 * message generated to be saved in the error message buffer.
 *
 * If debugging is enabled then dump the contents of all communications
 * debugging buffers.
 *
 * Note that the SET_ERR_MSG macro will save the first (root) error message
 * received after the error message buffer is cleared and will keep that
 * message in the buffer until this function is called to recover it.
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 * SET_ERR_MSG macro and dumpDebugBuffer() function
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

void drvOmsVmeGetErrorMessage
(
    char *	message		/* (out) Returned error message.	*/
)
{
    int card;

    /*
     *  Copy the error message into the given output buffer then clear the
     *  error message buffer.
     */ 

    strcpy (message, errorMessage);
    errorMessage[0] = '\0';

    if (drvOmsVmeDebug)
    {
	/*
	 *  If in debug mode, dump the debug buffer.
	 */

	for (card = 0; card < lastCard; card++)
	{
	    dumpDebugBuffer(pCards[card]);
	}
    }
      
    return;
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * drvOmsVmeInit
 *
 * INVOCATION:
 * status = drvOmsVmeInit ()
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * None.
 * 
 * FUNCTION VALUE:
 * (long) number of cards successfully initializated, or a failure code:
 *                   DRV_OMS_VME_S_SYS_ERROR if:
 *                       address space cannot be registered
 *                       control structure creation fails
 *                       read semaphore creation fails
 *                       write semaphore creation fails
 *                       status semaphore creation fails
 *                       interrupt connection fails
 *                       interrupts cannot be enabled
 *                       card could not be initialized
 *                   DRV_OMS_VME_S_BUFFER_FULL if input buffer overflow
 *                   DRV_OMS_VME_S_UNSUPPORTED if VME58 subtype is invalid
 *
 * PURPOSE:
 * Initialize all supported OMS VME58 boards and set up drive support for each
 *
 * DESCRIPTION:
 * Initialize the OMS VME58 card driver module using the following algorithm:
 * 
 *      Return immediately initialzation setup has already been done.
 *
 *      Initialize the card control pointer structure.
 *
 *      Starting at the first card base address and exiting when error occurs
 *      While ( a card exists at this address )
 *      (
 *          Stop looking for cards if an empty spot is found
 *          Ensure that the address space is free and claim it.
 *          Create and save card control structure
 *          Create the read semaphore
 *          Create the write semaphore
 *          Create the status semaphore
 *          Initialize the done status structure
 *          Initialize the limit status structure
 *          Initialize the interface control structure
 *          Setup interrupt handler
 *          Enable card interrupt
 *          Exit with error if not initialized
 *          Setup interrupt vector
 *          Set host read/write indexes to point to each other (for startup)
 *          Enable register interrupt sources
 *          Query board type by sending a "WY" string.
 *          Read response, decode and save board type.
 *          Update card type specific control structure elements.
 *          Increment card number and point to next vector and base address.
 *      )
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 * drvOmsVme58.h for supported OMS VME58 card subtypes.
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */


long drvOmsVmeInit 
(
    void
)
{
    void *base = (void *) DRV_OMS_VME_BASE; 	/*     Base address for 1st card */
    unsigned vector = DRV_OMS_VME_VECTOR;	/*               Interupt vector */
    void *localAddress;				/*   Local (VME) address of card */
    DRV_OMS_VME_CARD *pCard;			/*         Pointer to card info. */
    DRV_OMS_58_CONTROL *pControl; 		/*            Pointer to control */
    char buffer[DRV_OMS_VME_MAX_MSG_LEN];	/*           Buffer to read into */
    char	*pFirst;			/*           Buffer char pointer */
    long status = 0;				/*        Return function status */
    int i;
    errorMessage[MAX_STRING_SIZE-1] = '\0';     /*    Clear error message string */

    /* 
     * If VME cards have been found then this function has already been
     * called and there is nothing more to be done!
     */

    if (lastCard != 0) 
    {
        return ( status );
    }

    /*
     * Otherwise this is the first invocation ... get on with it
     * Start by initializing the card control structure
     */
      
    for (lastCard=0; lastCard < DRV_OMS_VME_MAX_CARDS; lastCard++, vector++)
    {
        pCards[lastCard] = NULL;


        /* 
         * Is there something at the card base address?  Stop if you hit
	 * an empty spot.
         */
        if ( drvOmsVmeDebug )
        {
            logMsg("drvOmsVmeInit: Checking for card#:%d, at Base Addr:0x%x\n", 
		    lastCard, (int) base,0,0,0,0);
        }

        if (locationProbe(DRV_OMS_VME_ADDRS_TYPE, base) != S_dev_addressOverlap)
        {
            if ( drvOmsVmeDebug )
            {
                logMsg("drvOmsVmeInit: OMS card#: %d, not found at Base Addr:0x%x\n", lastCard, (int) base,0,0,0,0);
            }
            break;
        }

                  
        /*
         * Check that the address space is free and claim it
         */
         
        status = devRegisterAddress(__FILE__,
                                    DRV_OMS_VME_ADDRS_TYPE,
                                    base,
                                    DRV_OMS_VME_MEM_SPACE,
                                    &localAddress);
        if ( status )
        {
            logMsg("drvOmsVmeInit: Cannot register OMS Card#:%d address space!\n", 
		    lastCard,0,0,0,0,0);
            SET_ERR_MSG ("Cannot register OMS card");
            return (DRV_OMS_VME_S_SYS_ERROR);
        }
   	       
   	       
        /*
         * Create and save the interface card control structure
         */

        pCard = (DRV_OMS_VME_CARD *) malloc (sizeof (DRV_OMS_VME_CARD));
        if (!pCard)
        {
            logMsg("drvOmsVmeInit: Cannot create control structure for OMS Card#:%d\n", 
                    lastCard,0,0,0,0,0);
            SET_ERR_MSG ("Cannot create OMS card control structure");
            return (DRV_OMS_VME_S_SYS_ERROR);
        }
            
        pCards[lastCard] = pCard;
                           
                           
        /*
         * Create the read semaphore
         */

        pCard->readMutex = semMCreate (SEM_Q_PRIORITY | SEM_INVERSION_SAFE);
        if (!pCard->readMutex)
        {
            logMsg("drvOmsVmeInit: Cannot create read semaphore for OMS Card#:%d\n", 
                    lastCard,0,0,0,0,0);
            SET_ERR_MSG ("Cannot create OMS read semaphore");
            return (DRV_OMS_VME_S_SYS_ERROR);
        }
       
 
        /*
         * Create the write semaphore
         */

        pCard->writeMutex = semMCreate (SEM_Q_PRIORITY | SEM_INVERSION_SAFE);
        if (!pCard->writeMutex)
        {
            logMsg("drvOmsVmeInit: Cannot create write semaphore for OMS Card#:%d\n", 
                    lastCard,0,0,0,0,0);
            SET_ERR_MSG ("Cannot create OMS write semaphore");
            return (DRV_OMS_VME_S_SYS_ERROR);
        }

        /*
         * Create the status semaphore
         */

        pCard->statusMutex = semMCreate (SEM_Q_PRIORITY | SEM_INVERSION_SAFE);
        if (!pCard->statusMutex)
        {
            logMsg("drvOmsVmeInit: Cannot create status semaphore for OMS Card#:%d\n", 
                    lastCard,0,0,0,0,0);
            SET_ERR_MSG ("Cannot create OMS status semaphore");
            return (DRV_OMS_VME_S_SYS_ERROR);
        }
       
        /* 
         * Initialize the done status structure 
         */
        semTake (pCard->statusMutex, WAIT_FOREVER);

        for (i=0;i<8;i++)
          {
            pCard->axis_done[i]=0;
          }

        /*
         * Intialize the limit status structure
         */

        pCard->limit = 0;
        
        semGive (pCard->statusMutex);

        /*
         * Intialize the interface control structure
         */
	     
        pControl = (DRV_OMS_58_CONTROL *) localAddress;
        pControl->registers.control = 0;	    
        pCard->pControl = pControl;
        pCard->charactersRead = 0; 
        pCard->axes = 0;
	pCard->status = 0;

        
        /* 
         * Set up interrupt handler and vector stuff
         */

        status = devConnectInterrupt(DRV_OMS_VME_INTERRUPT_TYPE,
                                     vector,
                                     &drvOmsVmeIsr,
                                     (void *) pCard);
        if ( status )
        {
            logMsg("drvOmsVmeInit: Cannot connect interrupt for OMS Card#:%d\n", 
                    lastCard,0,0,0,0,0);
            SET_ERR_MSG ("Cannot connect OMS interrupt");
            pControl->registers.control = 0;
            return (DRV_OMS_VME_S_SYS_ERROR);
        }

        if ( drvOmsVmeDebug )
        {
            logMsg("drvOmsVmeInit:Connected ISR, Status Reg.= 0x%x\n", 
		    (int) pControl->registers.status,0,0,0,0,0);
        }

        status = devEnableInterruptLevel (DRV_OMS_VME_INTERRUPT_TYPE,
                                          DRV_OMS_VME_INTERRUPT);
        if ( status )
        {
            logMsg("drvOmsVmeInit: Cannot enable interrupts for OMS Card#:%d\n", 
                    lastCard,0,0,0,0,0);
            SET_ERR_MSG ("Cannot enable OMS interrupts");
            pControl->registers.control = 0;
            return (DRV_OMS_VME_S_SYS_ERROR);
        }


        /*
         *  Board must be initialized by now ... otherwise an error!
         */

        if (!(pControl->registers.status & DRV_OMS_VME_STAT_INIT ))
        {
            logMsg("drvOmsVmeInit: OMS Card#: %d could not be initialized.\n", 
                    lastCard,0,0,0,0,0);
            SET_ERR_MSG ("Card not initialized");
            return (DRV_OMS_VME_S_SYS_ERROR);
        }
        

        /*
         *  Setup card interrupt vector.
         *  Enable interrupt-when-done and input-buffer-full interrupts 
         */
         
        pControl->registers.vector = vector;


	/*
	 *  Set the indexes in the buffer to point to each other.
	 *  This is for starting out only.
	 */

	pControl->hostReadIndex = pControl->cardWriteIndex;
        pControl->hostWriteIndex = pControl->cardReadIndex;

        /*
         *  Enable the desired interupts
         */

	pControl->registers.control = DRV_OMS_VME_IRQ_ENABLE |  
                                      DRV_OMS_VME_IRQ_LIMIT |
                                      DRV_OMS_VME_IRQ_MAILBOX |
                                      DRV_OMS_VME_IRQ_DONE;
        if ( drvOmsVmeDebug )
        {
            logMsg("drvOmsVmeInit:hwIndx=%d, crIndx=%d, ctrlReg=0x%x\n", 
                (int) pControl->hostWriteIndex,
                (int) pControl->cardReadIndex,
                (int) pControl->registers.control,0,0,0);
        }

        /*
         * Ensure echo is off (EF) and find out type of board (WY)
         */

        status = drvOmsVmeWriteCard (lastCard, "EFWY");
        if ( status  != 0 )
        {
            pControl->registers.control = 0;
            return ( status );
        }

	/*
	 *  Read the response.  Task delay is necessary or else
	 *  it misses the complete message.
	 */

	taskDelay (3);
        if ( (status = drvOmsVmeReadCard (lastCard, buffer )) > 0 )
        {
            if ( drvOmsVmeDebug )
            {
                logMsg ("drvOmsVmeInit: Response to WY command: %s\n", 
                       (int) &buffer[0],0,0,0,0,0);
            }
            if (strncmp (buffer, "VME58", 5) == 0)
            {
                if ( drvOmsVmeDebug )
                {
                    logMsg ("drvOmsVmeInit: VME58 string found.\n", 0,0,0,0,0,0);
                }
                for (pFirst = buffer; *pFirst != '-' && *pFirst != '\0'; pFirst++ )
                {
                    ;  /*  Ignore everything, til you hit a - or end of line. */
                } 
                if ( isdigit ( *(++pFirst) ) )
                {
                    if ( drvOmsVmeDebug )
                    {
                        logMsg ("drvOmsVmeInit: Board type string: %s\n", 
                                (int) pFirst,0,0,0,0,0);
                    }
                    if (strncmp (pFirst, "6S2", 3) == 0)
                    {
                        pCard->type = DDR_OMS_58_6S2;
                        pCard->axes = 8;
                    }
                    else if (strncmp (pFirst, "4S4", 3) == 0)
                    {
                        pCard->type = DDR_OMS_58_4S4;
                        pCard->axes = 8;
                    }
                    else if (strncmp (pFirst, "2S6", 3) == 0)
                    {
                        pCard->type = DDR_OMS_58_2S6;
                        pCard->axes = 8;
                    }
                    else if (strncmp (pFirst, "2S2", 3) == 0)
                    {
                        pCard->type = DDR_OMS_58_2S2;
                        pCard->axes = 4;
                    }
                    else if (strncmp (pFirst, "8S", 2) == 0)
                    {
                        pCard->type = DDR_OMS_58_8S;
                        pCard->axes = 8;
                    }
                    else if (strncmp (pFirst, "4S", 2) == 0)
                    {
                        pCard->type = DDR_OMS_58_4S;
                        pCard->axes = 4;
                    }
                    else if (strncmp (pFirst, "4E", 2) == 0)
                    {
                        pCard->type = DDR_OMS_58_4E;
                        pCard->axes = 4;
                    }
                    else if (strncmp (pFirst, "8", 1) == 0)
                    {
                        pCard->type = DDR_OMS_58_8;
                        pCard->axes = 8;
                    }
                    else if (strncmp (pFirst, "4", 1) == 0)
                    {
                        pCard->type = DDR_OMS_58_4;
                        pCard->axes = 4;
                    }
                    if ( drvOmsVmeDebug )
                    {
                        logMsg ("drvOmsVmeInit: OMS VME58-%s\n", 
                                 (int) pFirst,0,0,0,0,0);
                    }
                    status = 0; /* Clear status */
                }
                else       /* First character after '-' or '\0' is not a digit  */ 
                {
                    return (DRV_OMS_VME_S_UNSUPPORTED);
                }
            } /* End of if 'VME58' */
        } /* End of if successful read from card */
        else
        {
            if ( status == DRV_OMS_VME_S_UNSUPPORTED )
            {
                logMsg("drvOmsVmeInit: Unsupported card type\n",0,0,0,0,0,0);
                SET_ERR_MSG ("Unsupported OMS board type");
            }
            else
            {
                logMsg("drvOmsVmeInit: Invalid WY response\n",0,0,0,0,0,0);
                SET_ERR_MSG ("Invalid response to WY board type");
            }
            pCard->type = DDR_OMS_58_INVALID;
            pControl->registers.control = 0;
            return ( status );
        }


        /*
         *  Valid card found and initialized
         */
        if ( 1 )
        {
            logMsg("drvOmsVmeInit: OMS card#: %d type: %d initialized at address: 0x%x, vector: %d\n", 
                   lastCard,
                   pCard->type,
                   (int) localAddress, 
                   vector, 
                   0, 0);
        }
        /*
         *  Increment base address to next possible card
         */

        base = (void *) ((char *) base + DRV_OMS_VME_MEM_SPACE);
    }/* end of For all the card */

    if (pCards[lastCard] == NULL)  return( lastCard );
    else return (++lastCard);
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * drvOmsVmeIsr
 *
 * INVOCATION:
 * drvOmsVmeIsr (pCard)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pCard (void *) Pointer to card control structure
 *
 * FUNCTION VALUE:
 * None.
 *
 * PURPOSE:
 * Interrupt handler for OMS VME motion control cards
 *
 * DESCRIPTION:
 * Set the Data Area Update bit in the Control Register to request an update.
 * Wait for the OMS VME58 to clear the Data Area Update bit.
 * Read the Status and Control Registers.
 * Determine the source of the interrupt (five possibilities).
 *  1. Done - do nothing.
 *  2. Limit - do nothing.
 *  3. Command Error, set card status to DRV_OMS_VME_S_CMD_REJECT.
 *  4. Init error - set status to DRV_OMS_VME_S_CMD_REJECT.
 *  5. Encoder Slip - do nothing.  
 * *** Encoder Slip interrupt is not currently enabled
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 * drvOmsVmeInit() to see which interrupts have been enabled.
 * drvOmsVmeMotorPosition() uses the limit information obtained here.
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */


static void drvOmsVmeIsr (void *pIsr)
{   
    uint8_t control;		              /*               Control Register */
/*    uint8_t done;		                                  Done Register */
    uint8_t intStatus;		              /*                Status Register */
/*    uint8_t limit;		                                 Limit Register */
    DRV_OMS_VME_CARD *pCard = (DRV_OMS_VME_CARD *) pIsr;     /* VME card Struct */
    DRV_OMS_58_CONTROL *pControl = pCard->pControl;        /* Control Structure */
    DRV_OMS_VME_REGISTERS *pRegisters = &( pControl->registers ); /* Reg Struct */
    int attempt;		              /*   # attempts to read registers */

     
    /*
     *  Request an update of the status words by setting the Data Area Update bit 
     *  in the Control Register.  The status can be read when the VME58 card 
     *  has cleared the Data Area Update bit.
     */

    pControl->registers.control |= DRV_OMS_58_DATA_AREA_UPDATE;
    taskDelay (1);

    /*
     *  Check if the VME58 card has cleared the Data Area Update Bit 
     *  in the Control Register
     */

    for (attempt = 0; attempt < DRV_OMS_VME_MAX_RETRIES; attempt++)
    {
	if (!(pControl->registers.control & DRV_OMS_58_DATA_AREA_UPDATE))
	{
	    break;  /* Status has been updated */
	}
        taskDelay (1);
    }

    if (attempt >= DRV_OMS_VME_MAX_RETRIES)
    {
	SET_ERR_MSG ("Status did not update" );
	return;
    }

    /*
     *  Read the VME58 status and control registers.
     *
     *  Reading the status register will cause the following bits to be cleared:
     *          bit 0: command error
     *          bit 4: done (OR of all the done flags) 
     */

    intStatus = pRegisters->status;
    control = pRegisters->control;

    if (drvOmsVmeDebug)
    {
        logMsg("drvOmsVmeIsr: Interrupt, statusReg=0x%x, controlReg=0x%x\n",
               (int) intStatus,control,0,0,0,0);
    }

    /* 
     * Determine what source caused the OMS card interrupt
     */


     if (intStatus & DRV_OMS_VME_STAT_DONE)
    {
        /*
         * A Done interrupt has occurred
         */
        if (drvOmsVmeDebug)
        {
            logMsg("drvOmsVmeIsr %d card: Done detected, control=0x%x, status=0x%x\n", 
                    pCard->type, control, intStatus, 0, 0, 0);
        }
    }

         

    if (intStatus & DRV_OMS_VME_STAT_OVERTRAVEL)
    {
        /*
         * A soft limit switch has been hit.
         */
        if (drvOmsVmeDebug)
        {
            logMsg("drvOmsVmeIsr %d card: Limit detected, control=0x%x, status=0x%x\n",
                    pCard->type, control, intStatus, 0, 0, 0);
        }
    }            

    if (intStatus & DRV_OMS_VME_STAT_ERROR)
    {
        /*
         * OMS card command error has occurred.
         */
        pCard->status = DRV_OMS_VME_S_CMD_REJECT;
        if (drvOmsVmeDebug)
        {
            logMsg("drvOmsVmeIsr %d card: Command rejected, control=0x%x,status=0x%x\n",
             pCard->type, control, intStatus, 0, 0, 0);
            dumpDebugBuffer (pCard);
        }
    }
    
    if ( !(intStatus & DRV_OMS_VME_STAT_INIT) )
    {
        /*
         * OMS is not initialized ... it is in a RESET or CONFIGURATION mode.
         */
        pCard->status = DRV_OMS_VME_S_CMD_REJECT;
        if (drvOmsVmeDebug)
        {
            logMsg("drvOmsVmeIsr %d card: Card is not initialized\n", 
                   pCard->type, 0, 0, 0, 0, 0);
        }
    }

    if ( (intStatus & DRV_OMS_VME_STAT_ENCODER) )
    {
        /*
         * OMS has detected encoder slip.  This interrupt is not currently enabled.
         */
        if (drvOmsVmeDebug)
        {
            logMsg("drvOmsVmeIsr %d card: Encoder slip detected\n", 
                   pCard->type, 0, 0, 0, 0, 0);
        }
    }
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * drvOmsVmeMotorPosition
 *
 * INVOCATION:
 * status = drvOmsVmeMotorPosition (card, axis, &position, &encoder)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) card (int) Card number.
 * (>) axis (int) Axis information.
 * (<) position (long *) Commanded position.
 * (<) encoder (long *) Encoder position.
 * (<) axisLimit (int *) Axis limit flag.
 * (<) axisDone (int *) Axis done flag.
 *
 * FUNCTION VALUE:
 * (long) Initialization success code of 0 or a failure code:
 *                 DRV_OMS_VME_S_CFG_ERROR if:
 *                           invalid card
 *                           invalid axis
 *                           too many attempts to initiate update
 *
 * PURPOSE:
 * Request current motor position and encoder values from the card.
 *
 * DESCRIPTION:
 * Read the current position and encoder value from the card.
 *
 * Ensure that the request is valid, 
 *    check the card number then the axis number.
 * Set the Data Area Update bit in the Control Register to request an update.
 * Wait for the OMS VME58 to clear the Data Area Update bit.
 * Read the motor position
 * Read the encoder value
 * 
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 * The limit status is obtained in drvOmsVmeIsr()
 *
 * DEFICIENCIES:
 * Doesn't use the home flag register.
 *-
 ************************************************************************
 */


long drvOmsVmeMotorPosition 
(
    int 	card, 		 /* (in)  Card number.             */
    int 	axis, 		 /* (in)  Axis number              */
    long 	*position,	 /* (out) Current position counter */
    long 	*encoder	 /* (out) Current Encoder value    */
/*    int         *axisLimit,      (out) Axis in a limit ?        */
/*    int         *axisDone        (out) Axis Done ?              */
)
{
    DRV_OMS_VME_CARD *pCard; 	 /* Ptr. to card information.	*/
    DRV_OMS_58_CONTROL *pControl;/* Ptr to control.		*/
    DRV_OMS_VME_REGISTERS *pRegisters ;
    int attempt;		/* # attempts to get pos.	*/
    long status = 0; 		/* Return function notice.	*/

    pRegisters = &( pControl->registers );

    /*
     *  Ensure that the request is valid, 
     *  check the card number then the axis number.
     */
     
    if ((card<0) || (card>=DRV_OMS_VME_MAX_CARDS) || (pCards[card] == NULL))
    {
        logMsg("drvOmsVmeMotorPosition: Position requested from invalid OMS Card (#%d\n", 
                card,0,0,0,0,0);
        SET_ERR_MSG ("OMS position requested from invalid card");
        return ( DRV_OMS_VME_S_CFG_ERROR );
    }
    pCard = pCards[card];

    pControl = pCard->pControl;
    if ( axis >= pCard->axes )
    {
        logMsg("drvOmsVmeMotorPosition: Position requested for invalid axis (OMS Card#: %d, axis#: %d\n", 
                card,axis,0,0,0,0);
        SET_ERR_MSG ("OMS position requested from invalid axis");
        return ( DRV_OMS_VME_S_CFG_ERROR );
    }

    /*
     *  Request an update of the status words by setting the Data Area Update bit 
     *  in the Control Register.  The status can be read when the VME58 card 
     *  has cleared the Data Area Update bit.
     */

    pControl->registers.control |= DRV_OMS_58_DATA_AREA_UPDATE;
    taskDelay (1);

    /*
     *  Check if the VME58 card has cleared the Data Area Update Bit 
     *  in the Control Register
     */

    for (attempt = 0; attempt < DRV_OMS_VME_MAX_RETRIES; attempt++)
    {
	if (!(pControl->registers.control & DRV_OMS_58_DATA_AREA_UPDATE))
	{
	    break;  /* Status has been updated */
	}
        taskDelay (1);
    }

    if (attempt >= DRV_OMS_VME_MAX_RETRIES)
    {
	SET_ERR_MSG ("Status did not update" );
	return ( DRV_OMS_VME_S_CFG_ERROR );
    }

    /*
     *  Read the VME58 registers
     */


    /*
     * Get the current position & encoder value from the OMS VME card via
     * the dual port memory. 
     */
     
    *position = (long) (pControl->axisStatus[axis].cmdPosLow + 
                        (pControl->axisStatus[axis].cmdPosHi << 16));
    *encoder = (long) (pControl->axisStatus[axis].encPosLow +
                        (pControl->axisStatus[axis].encPosHi << 16));


    return ( status );
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * drvOmsVmeMotorState
 *
 * INVOCATION:
 * status = drvOmsVmeMotorState (card, axis)
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) card (int) Card number.
 * (>) axis (int) Axis information.
 * (<) lowLimit  (int *) Determined low limit switch state.
 * (<) highLimit (int *) Determined high limit switch state.
 * (<) homeSwitch (int *) Determined home switch state.
 * (<) axisDone (int *) Done flag.
 *
 * FUNCTION VALUE:
 * (long) Initialization success code of 0 or failure code:
 *           DRV_OMS_VME_S_CFG_ERROR if:
 *                invalid card
 *                invalid axis
 *           DRV_OMS_VME_S_CMD_REJECT if command error
 *           DRV_OMS_VME_S_PARSE_ERROR QA return is invalid
 *           DRV_OMS_VME_S_SYS_ERROR if too many attempts to read status
 *           DRV_OMS_VME_S_BUFFER_FULL if input buffer overflow
 *
 * PURPOSE:
 * Get axis status information for a motor.
 *
 * DESCRIPTION:
 * Get axis status information for a motor including limit and home switch
 * state, as well as motion done status.
 *
 * Ensure that the request is valid, 
 *    check the card number then the axis number.
 * Check Status Register for a Command Error.
 * Send the "QA" command to query for axis status.
 * Parse the return string to determine:
 *        state of limit switches,
 *        state of home switch,
 *        axis done motion flag
 * Return with error if:
 *       QA write fails,
 *       QA returns invalid string, or
 *       Takes too many attempts to read.
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 * - other function name.
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */


long drvOmsVmeMotorState 
(
    int 	card, 		/* (in)	 Card number.  	            */
    int 	axis, 		/* (in)  Axis number                */
    int		*lowLimit, 	/* (out) State of low limit switch  */
    int		*highLimit,	/* (out) State of high limit switch */
    int         *homeSwitch,    /* (out) State of home switch       */
    int         *axisDone       /* (out) State of axis done flag    */
)
{
    char buffer[DRV_OMS_VME_MAX_MSG_LEN];      /* scratchpad buffer */
    DRV_OMS_VME_CARD *pCard;              /* Card control structure */
    int requests;                /* # of requests to ask for status */
    long status = 0;                     /* Return function status. */
    DRV_OMS_58_CONTROL *pControl;
        
    /*
     * Ensure that the request is valid
     */
     
    if ((card<0) || (card>=DRV_OMS_VME_MAX_CARDS) || (pCards[card] == NULL))
    {
        logMsg("drvOmsVmeMotorState: State requested from invalid OMS Card (#%d\n", 
                card,0,0,0,0,0);
        SET_ERR_MSG ("OMS state requested from invalid card");
        return ( DRV_OMS_VME_S_CFG_ERROR );
    }
    pCard = pCards[card];

    if (axis >= pCard->axes)
    {
        logMsg("drvOmsVmeMotorState: State requested from invalid OMS axis (Card#: %d, Axis: %d\n", 
                card,axis,0,0,0,0);
        SET_ERR_MSG ("OMS state requested from invalid axis");
        return ( DRV_OMS_VME_S_CFG_ERROR );
    }
    pControl = pCard->pControl;

          
    /*
     *  Check the Status Register for a Command Error
     */

    if (pControl->registers.status & DRV_OMS_VME_STAT_ERROR)
    {
        SET_ERR_MSG ("OMS state requested shows invalid command");
        return ( DRV_OMS_VME_S_CMD_REJECT );
    }


    /*
     * Ask the OMS VME card for the current state.  Drop out if write
     * or read fails.  Only keep trying if the read failure is 
     * DRV_OMS_VME_S_NO_REPLY.
     */
    
    for (requests = 0; requests < DRV_OMS_VME_MAX_REQUESTS; requests++)
    {
	/*
	 *  Query the axis (QA), status of a single axis.
	 */

        pCard->status = 0;
        status = drvOmsVmeWriteMotor (card, axis, "QA");
        if ( status != 0 )
        {
	    /*
	     *  Write failed, do not write an error message, as
	     * the lower levels already have.
	     */

            if (drvOmsVmeDebug )
            {
                logMsg("drvOmsVmeMotorState:%d QA cmd drvOmsVmeWriteMotor failed %d:%d\n", 
                    card, status, pCard->status, 0, 0, 0);
            }
            return ( status );
        }
	taskDelay (2);

	/*
	 *  Read the response, and if you get something, then break out.
	 */

        status= drvOmsVmeReadCard( card, buffer );
        if ( status > 0 && pCard->status == 0 )
        {
            /*
             * Parse string to determine state of soft limits.
	     * See pg 5-53 of users manual for QA/RA status table.
             */

            if ( drvOmsVmeDebug )
            {
		logMsg("drvOmsVmeMotorState: Parse buffer 0=%c 1=%c, 2=%c, 3=%c\n", 
		   (int)buffer[0],(int)buffer[1],(int)buffer[2],(int)buffer[3],0,0);
	    }

            /*
             *  Check the buffer to see if it is a valid return
             *
             *     [0] == M  axis moving in a negative direction
             *     [0] == P  axis moving in a positive direction
             *     [1] == D  axis motion is done
             *     [1] == N  axis motion either not done or not setup for it
             *     [2] == L  axis in limit for direction [0]
             *     [2] == N  axis not in limit for direction [0]
             *     [3] == H  home switch active 
             *     [3] == N  home switch not active
             */

            if ( (buffer[0] == 'M' || buffer[0] == 'P') &&
                 (buffer[1] == 'D' || buffer[1] == 'N') &&
                 (buffer[2] == 'L' || buffer[2] == 'N') &&
                 (buffer[3] == 'H' || buffer[3] == 'N') &&
                  buffer[4] == '\0'                        )
            {
                /*
                 *  Response is valid, extract done flag and switch 
                 *  info then exit read loop.
                 */

                if (buffer[1] == 'D')
                {
                    /* axis motion has completed */
                    *axisDone = 1;

                    /* clear done flag for this axis only */
                    status = drvOmsVmeWriteMotor (card, axis, "CA");
                    if (status != 0)
                    {
                        logMsg("OMS card:%d CA cmd writeMotor failed %d:%d\n", 
                             card, status, pCard->status, 0, 0, 0);
                        return status;
                    }
                }
                else if (buffer[1] == 'N')
                {
                    /* axis motion not complete or not enabled with ID */
                    *axisDone = 0;
                }

                *lowLimit = (int) (buffer[2] == 'L' && buffer[0] == 'M');
                *highLimit = (int) (buffer[2] == 'L' && buffer[0] == 'P');     
                *homeSwitch = (int) (buffer[3] == 'H');
            }
            else
            {
                SET_ERR_MSG ("OMS State request error");
                logMsg("drvOmsVmeMotorState: %d QA cmd returned bad character:%c\n",
                             card, buffer[0], 0, 0, 0, 0);
                return DRV_OMS_VME_S_PARSE_ERROR;
            } 
	    /*
	     *  Reset status, as status reflects # of char's from read.
	     */

	    status = 0;
            break;

	}/* end of if status indicates we got something */

	else if ( status < 0 )
	{
	    /*
	     *  Exit out if the read failed.  Don't if did not receive 
	     *  anything, i.e. status=0;
	     */

	    return( status );
	}


        /*
         * Dump the read and write buffers whenever the read request fails.
         */

        if (drvOmsVmeDebug)
        {
            if ( pCard->status != 0 )
            {
		/*
		 *  Interrupt function returned an error, or read had buffer
		 *  overflow.
		 */

                logMsg("drvOmsVmeMotorState card:%d Status register error:%d, %d/%d retry, statReg is %d\n",
                        card, pCard->status, requests, 
                        DRV_OMS_VME_MAX_REQUESTS, pControl->registers.status, 
			0);
            }
            dumpDebugBuffer (pCard);

        }/* end of if drvOmsVmeDebug */
    } /* end of for attempt loop */


    /*
     * Dump the read and write buffers when this is the first successful 
     * request following one or more failures.
     */

    if ( drvOmsVmeDebug && requests <= DRV_OMS_VME_MAX_REQUESTS )
    {
        logMsg("drvOmsVmeMotorState card:%d QA command accepted after %d requests\n",
	 pCard->type, requests, 0, 0, 0, 0);
        dumpDebugBuffer (pCard);
    }


    /* 
     * Too many attempts to read the status have failed, so bail out.
     */

    if (requests == DRV_OMS_VME_MAX_REQUESTS)
    {
        SET_ERR_MSG ("OMS State request read failure");
        logMsg("drvOmsVmeMotorState card:%d QA cmd failed after %d attempts, %d:%d\n",
	     card, requests, status, pCard->status, 0, 0);
        return ( DRV_OMS_VME_S_SYS_ERROR );
    } 
                           
    return( status );
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * drvOmsVmeReadCard
 *
 * INVOCATION:
 * status - drvOmsVmeReadCard (card, buffer);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) card (int) card number to write to
 * (<) buffer (char *) pointer to buffer that data will be copied into
 *
 * FUNCTION VALUE:
 * (long) number of characters read.  Otherwise a negative error number.
 * Only possible error is DRV_OMS_VME_S_CFG_ERROR.
 *
 * PURPOSE:
 * Read a response from an OMS VME card
 *
 * DESCRIPTION:
 * Recover data read from an OMS VME card.  Reads characters until it
 * sees a carriage return or line feed.
 *
 * Ensure that the request is valid.
 * Clear the message buffer.
 * Retrieve a message from the buffer:
 *    While there are characters to read:
 *       Increment the host read index then read the character
 *       If valid message character:
 *          Set readingMessage flag
 *          Save character in buffer then increment buffer index
 *          Increment charactersRead counter
 *       Else if end of message:
 *          Clear readingMessage flag
 *          Set messageComplete flag
 *          If message not empty
 *             Add null to end of buffer
 *             Set return status = charactersRead counter
 *             Clear charactersRead counter
 * Return status (number of characters read or negative error number
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 * None.
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */


long drvOmsVmeReadCard 
(
    int 	card, 		/* (in)  Card number.                   */
    char 	*buffer 	/* (out) String returned from card      */
)
{
    int attempt;		/* Number of attempts to read a messg	*/
    int messageComplete = FALSE;/* Message Complete.			*/
    char nextChar;		/* Character read.			*/
    int readingMessage = FALSE;	/* Currently reading the message.	*/
    DRV_OMS_VME_CARD *pCard; 	/* Ptr to card structure.		*/
    DRV_OMS_58_CONTROL *pControl;
    long status = 0;		/* Return number of char. read.		*/


    /*
     * Ensure that the request is valid
     */
   
    if ((card<0) || (card>=DRV_OMS_VME_MAX_CARDS) || (pCards[card] == NULL))
    {
        logMsg ("drvOmsVmeReadCard:Read from invalid OMS card (#: %d)\n", 
                card, 0, 0, 0, 0, 0);
        SET_ERR_MSG ("Read from invalid OMS card");
        return ( DRV_OMS_VME_S_CFG_ERROR );
    }
    pCard = pCards[card];

    pControl = pCard->pControl;
    /*
     *  Clear the message buffer
     */
    *buffer = '\0';
            
    /*
     * Retrieve a message if one is available from the readBuffer.
     * (hostReadIndex is not equal to cardWriteIndex)
     * Make up to DRV_OMS_VME_MAX_RETRIES attempts.
     */
     
    semTake (pCard->readMutex, WAIT_FOREVER);    
    for (attempt=0, pCard->charactersRead = 0; 
         !messageComplete && attempt < DRV_OMS_VME_MAX_RETRIES; 
         attempt++)
    {
	/*
	 * While there are characters to read.
	 */

        while (pControl->hostReadIndex != pControl->cardWriteIndex)
        {
	    /* 
	     *  Increment the host read index, and read the character.
	     */

            pControl->hostReadIndex = (pControl->hostReadIndex + 1) & 0xff;
            nextChar = (char) *(pControl->readBuffer + pControl->hostReadIndex);


	    /*
	     * If this is not a carriage return or line feed, then must
	     * be part of the message, so save it in the passed in buffer.
	     */

            if (nextChar != '\n' && nextChar != '\r')
            {
		/*
		 *  But first check that we haven't already saved too
		 *  much in the buffer.
		 */

		if ( pCard->charactersRead < DRV_OMS_VME_MAX_MSG_LEN ) 
		{   
		    readingMessage = TRUE;
		    *buffer++ = nextChar;
		    /*  *buffer = nextChar;
		    buffer++; **/
		    pCard->charactersRead+=1;
		    *buffer = '\0';

		    /* 
		     * Fill up the read debug buffer for diagnostics later 
		     */ 

		    if (drvOmsVmeDebug)
		    {
			pCard->readDebug[pCard->pReadDebug & 0x3f] = nextChar;
			pCard->pReadDebug++;
		    }
		}
		else
		{
		    /*
		     *  Else the message if longer than the mx msg len.
		     *  Set the card status, but return # char's read.
		     */

		    logMsg("drvOmsVmeReadCard: %d card .. OMS read buffer overflow\n",
			 pCard->type, 0, 0, 0, 0, 0);
		    pCard->status = DRV_OMS_VME_S_BUFFER_FULL;
		    messageComplete = TRUE;
		    status = pCard->charactersRead;
		    break;
		}/*end of check for message length. */

            }
            else if (readingMessage && nextChar == '\n')
            {
		/*
		 *  If you were reading a message and we now have a 
		 *  carriage return then we must be at the end of the
		 *  message.  So stop.
		 *  End the buffer with a null.  Reset characters read.
		 */

                readingMessage = FALSE;
                messageComplete = TRUE;
		if ( pCard->charactersRead != 0 )
		{
		    *buffer = '\0';
		    if (drvOmsVmeDebug)
		    {
			pCard->readDebug[pCard->pReadDebug & 0x3f] = '\0';
			/*logMsg("drvOmsVmeReadCard:%d card .. read:%s\n", 
			  (int) pCard->readDebug[pCard->pReadDebug & 0x3f],
			  0, 0, 0, 0, 0);*/
		    }
		    status = pCard->charactersRead;
		    pCard->charactersRead = 0;
		}
                break;
            }/* End of reading a char. */
        }/* end of while there are characters to read. */
    }/* end of for x number of retries of reading a message. */

    semGive (pCard->readMutex);
    if (drvOmsVmeDebug)
    {
	logMsg("drvOmsVmeReadCard:  Num read attempts:%d, msgCmpl=%d, #Chars=%d\n",
	 attempt, messageComplete, status, 0, 0, 0);
    }


    return ( status );
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * drvOmsWriteCard
 *
 * INVOCATION:
 * status = drvOmsVmeWriteCard (card, buffer);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) card (int) card number to write to
 * (>) buffer (char *) pointer to buffer that data will be read from 
 *
 * FUNCTION VALUE:
 * (long) write function success code of 0 or failure code:
 *               DRV_OMS_VME_S_CFG_ERROR if card is invalid
 *               DRV_OMS_VME_S_BUFFER_FULL if input buffer overflow
 *
 * PURPOSE:
 * Write a message to an OMS VME card
 *
 * DESCRIPTION:
 * Write a message to an OMS VME card.
 *
 * Ensure that the request is for a valid card
 * Ensure there is enough space.
 * Save the current host write index.
 * Try to copy command into the OMS card ring buffer
 *    Write till you hit a null.
 *    Update the output put index if the write was successful.
 * Check for buffer overflow.
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 * - other function name.
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

long drvOmsVmeWriteCard 
(
    int 	card,           /* (in)  Card number.                   */
    char 	*buffer         /* (in)  String to write to the card    */
)
{
    int attempt;		/* Number of attempts to read a messg	*/
    DRV_OMS_VME_CARD *pCard;
    DRV_OMS_58_CONTROL *pControl;
    DRV_OMS_VME_REGISTERS *pRegisters; 
    int msgSize;		/* Size of the message to write.	*/
    uint16_t nextCharIndex;	/* Index into the write buffer.		*/
    long status = 0;		/* Return function status.		*/

    /*
     * Ensure that the request is for a valid card
     */
     
    if ((card<0) || (card>=DRV_OMS_VME_MAX_CARDS) || (pCards[card] == NULL))
    {
        logMsg("drvOmsVmeWriteCard: Write to invalid OMS Card (#: %d)\n", 
                card,0,0,0,0,0);
        SET_ERR_MSG ("Write to invalid OMS card");
        return ( DRV_OMS_VME_S_CFG_ERROR );
    }
    pCard = pCards[card];

    pControl = pCard->pControl;
    pRegisters = &( pControl->registers );


    semTake (pCard->writeMutex, WAIT_FOREVER);    

    /*
     * Ensure there is enough space.
     */

    msgSize = strlen (buffer);

    /*
     * Save the current host write index.
     */
    
    nextCharIndex = pControl->hostWriteIndex;

    /*
     * Try to copy command into the OMS card ring buffer
     */

    for (attempt = 0; attempt < DRV_OMS_VME_MAX_RETRIES; attempt++)
    {
	/*
	 *  Write till you hit a null.
	 */

        while (*buffer != '\0')
        {
            *(pControl->writeBuffer + nextCharIndex) = (uint16_t) *buffer++;
            nextCharIndex = (nextCharIndex + 1) & 0xff;
            if (nextCharIndex == pControl->cardReadIndex)
	    {
		break;
	    }
	}

        if (*buffer == '\0')
        {
	    /* 
	     *  Must update the output put index if the write was successful.
	     */

            pControl->hostWriteIndex = nextCharIndex;
            break;
        }
    }    
    
    semGive (pCard->writeMutex);
  

    /*
     *  Check that we have gotten to the end of the buffer before the
     *  end of max tries.
     */

    if (*buffer != '\0')
    {
        SET_ERR_MSG ("OMS input buffer overflow");
        logMsg("drvOmsVmeWriteCard: OMS card:%d input buffer overflow, attempts:%d\n",
	     
               card, attempt, 0, 0, 0, 0);
        return ( DRV_OMS_VME_S_BUFFER_FULL );
    }
    
    return ( status );
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * drvOmsWriteMotor
 *
 * INVOCATION:
 * status = drvOmsVmeWriteMotor (card, axis, buffer);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) card (int) card number to write to
 * (>) axis (int) Axis information.
 * (>) buffer (char *) pointer to buffer that data will be read from 
 *
 * FUNCTION VALUE:
 * (long) write function success code of 0 or failure code:
 *              DRV_OMS_VME_S_CFG_ERROR if card or axis is invalid
 *              DRV_OMS_VME_S_BUFFER_FULL if input buffer overflow
 *
 * PURPOSE:
 * Write an axis-specific message to an OMS VME card
 *
 * DESCRIPTION:
 * Write a message to an OMS VME card
 *
 * Ensure that the request is for a valid card and axis
 * Set up pointers and determine size of the buffer.
 * Ensure there is enough space.
 * Write the axis specifiying string.
 * If axis write was successful, write the rest of the string.
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 * - other function name.
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

long drvOmsVmeWriteMotor 
(
    int 	card, 		/* (in)  Card number.		*/
    int 	axis, 		/* (in)  Axis number		*/
    char 	*buffer 	/* (in)  String to write.	*/
)
{
    char nameBuf[3];		/* Axis information to send.	*/
    DRV_OMS_VME_CARD *pCard;	/* Ptr to card.			*/
    DRV_OMS_58_CONTROL *pControl;	/* Ptr to control	*/
    DRV_OMS_VME_REGISTERS *pRegisters; 	/* Ptr to registers.	*/
    long status = 0;		/* Return function code.	*/
    long space = 0;             /* Available space in the ringbuffer */
    int attempt = 0;

    /*
     * Ensure that the request is for a valid card
     */
     
    if ((card<0) || (card>=DRV_OMS_VME_MAX_CARDS) || (pCards[card] == NULL))
    {
        logMsg("drvOmsVmeWriteMotor: Write to invalid OMS Card (#: %d)\n", 
                card,0,0,0,0,0);
        SET_ERR_MSG ("Write to invalid OMS card");
        return ( DRV_OMS_VME_S_CFG_ERROR );
    }
    pCard = pCards[card];

    pControl = pCard->pControl;


    /*
     * Ensure that the request is for a valid axis
     */
     
    if (axis >= pCard->axes)
    {
        SET_ERR_MSG ("Write to invalid OMS axis");
        logMsg("drvOmsVmeWriteMotor: OMS card:%d invalid axis:%d\n", 
                    card, axis, 0, 0, 0, 0);
        return ( DRV_OMS_VME_S_CFG_ERROR );
    }


    /*
     *  Set up pointers and determine size of the buffer.
     */

    pRegisters = &( pControl->registers );
    if ( drvOmsVmeDebug && (strlen (buffer)) == 0 )
    {
        logMsg("drvOmsVmeWriteMotor: OMS card:%d, zero message length\n", 
                card, 0, 0, 0, 0, 0);
    }

    /*
     * Check to ensure there is enough space in the ring buffer
     */

    for (attempt = 0; attempt < 10; attempt++)
    {
        space = ( pControl->cardReadIndex - pControl->hostWriteIndex  );
        if (space <= 0)
        {
            space = space + 256;
        }
        if  ( space < strlen (buffer) )
        {
            if (drvOmsVmeDebug)
            {
                logMsg("drvOmsVmeWriteMotor: OMS card:%d, hostWriteIndex: %u, cardReadIndex: %u, message length: %d\n",
                        card, pControl->hostWriteIndex, pControl->cardReadIndex, 
                        strlen (buffer), 0, 0);
            }
            taskDelay (1);
        }       
        else break;
    }
    
    /*
     * First specify the axis, then write the axis information and
     * if successful, then write the buffer passed in.
     */
        
    nameBuf[0] = 'A';
    nameBuf[1] = axisName[axis]; 
    nameBuf[2] = '\0';                 
    status = drvOmsVmeWriteCard (card, nameBuf);
    if ( status == 0)
    {
        status = drvOmsVmeWriteCard (card, buffer);
    }

    if (drvOmsVmeDebug && status != 0 )
    {
	logMsg("drvOmsVmeWriteMotor: card:%d status bad. Not writing:%s\n", 
		card, (int)buffer, 0, 0, 0, 0);
    }

    return ( status );                                      
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * dumpDebugBuffer
 *
 * INVOCATION:
 * dumpDebugBuffer (pCard);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pCard  (DRV_OMS_VME_CARD *) Ptr to card.
 *
 * FUNCTION VALUE:
 * None.
 *
 * PURPOSE:
 * Dump the read and write buffers to the screen using logMsg.
 *
 * DESCRIPTION:
 * Write the read and write debugging buffers to the logMessage output
 * after converting all characters to 6 bit ascii values.
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 * None.
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static void dumpDebugBuffer 
(
    DRV_OMS_VME_CARD 	*pCard		/* (in) Ptr. to card.	*/
)
{
    int i;

    /*
     *  Ensure that a valid card control structure has been supplied
     */

    if (pCard == NULL)
    {
        return;
    }

    /*
     *  Add null terminations to read and write buffers
     */

    readDebugBuffer[DRV_OMS_VME_MAX_MSG_LEN] = '\0';
    writeDebugBuffer[DRV_OMS_VME_MAX_MSG_LEN] = '\0';

    /*
     *  Constrain read debug buffer to 6 bit ascii and then log the message
     */

    for (i=0; i<DRV_OMS_VME_MAX_MSG_LEN; i++)
    {
	readDebugBuffer[i] = 
             pCard->readDebug[(pCard->pReadDebug + i) & 0x3f];
    }

    logMsg("drvOmsVme:Read Debug Buffer:  %s\n", 
            (int)readDebugBuffer,0,0,0,0,0);

    /*
     *  Constrain write debug buffer to 6 bit ascii and then log the message
     */

    for (i=0; i<DRV_OMS_VME_MAX_MSG_LEN; i++)
    {
	writeDebugBuffer[i] = 
            pCard->writeDebug[(pCard->pWriteDebug + i) & 0x3f];
    }

    logMsg("drvOmsVme:Write Debug Buffer:  %s\n", 
            (int)writeDebugBuffer,0,0,0,0,0);

    return;
}

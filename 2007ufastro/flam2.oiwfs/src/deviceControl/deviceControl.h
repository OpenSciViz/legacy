/*
 ************************************************************************
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 *
 * (c) 2005.                        (c) 2005
 * National Research Council        Conseil national de recherches
 * Ottawa, Canada, K1A 0R6          Ottawa, Canada, K1A 0R6
 * All rights reserved              Tous droits reserves
 *                     
 * NRC disclaims any warranties,    Le CNRC denie toute garantie
 * expressed, implied, or statu-    enoncee, implicite ou legale,
 * tory, of any kind with respect   de quelque nature que se soit,
 * to the software, including       concernant le logiciel, y com-
 * without limitation any war-      pris sans restriction toute
 * ranty of merchantability or      garantie de valeur marchande
 * fitness for a particular pur-    ou de pertinence pour un usage
 * pose.  NRC shall not be liable   particulier.  Le CNRC ne
 * in any event for any damages,    pourra en aucun cas etre tenu
 * whether direct or indirect,      responsable de tout dommage,
 * special or general, consequen-   direct ou indirect, particul-
 * tial or incidental, arising      ier ou general, accessoire ou
 * from the use of the software.    fortuit, resultant de l'utili-
 *                                  sation du logiciel.
 *
 ************************************************************************
 *
 * FILENAME
 * deviceControl.h
 *
 * PURPOSE:
 * Publish epics deviceControl record public information for the
 * deviceControl record.
 *
 *INDENT-OFF*
 * $Log: deviceControl.h,v $
 * Revision 0.1  2005/07/01 17:47:14  drashkin
 * *** empty log message ***
 *
 * Revision 1.4  2005/05/19 bmw
 * Added encoderResolution and motorResolution to structure.
 * Added revision info.
 *
 * Revision 1.3  2004/09/28 bmw
 * Moved out recChoice field stuff because it's automatically created 
 * from deviceControlRecord.dbd and placed into deviceControlRecord.h
 * 
 * Revision 1.2  2004/09/20 bmw
 * Initial F2 revision, copy of Gemini GMOS rev 1.1.
 *
 * Revision 1.1  2002/04/24 05:18:14  ajf
 * Changes for epics3.13.4GEM8.4.
 *
 *INDENT-ON*
 *
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 ************************************************************************
*/

#ifndef REC_DEVICE_CONTROL_INC
#define REC_DEVICE_CONTROL_INC


/*
 *  Definitions for deviceControl record recChoice fields
 *
 *  ********Moved out because they are automatically created from 
 *  deviceControlRecord.dbd and placed into deviceControlRecord.h
 */

/*
 *  motor control mnemonics
 */

#define DDR_MOVE_GO             0       /* start moving to position         */
#define DDR_MOVE_STOP           1       /* bring motor to a stop            */
#define DDR_MOVE_ABORT          2       /* abort motion immediately         */



/*
 * define a mnemonic for the deviceControl record database structure
 */

typedef struct deviceControlRecord DEVICE_CONTROL_RECORD;


/*
 * define the generic device support interface and motion control structure
 */

typedef struct
{
    long        acceleration;       /* acceleration rate in steps/sec/sec   */
    int         backlashMotion;     /* anti-backlash motion in progress(T/F)*/
    int         badRead;            /* invalid encoder reading (T/F)        */
    long        baseVelocity;       /* ramp starting velocity in steps/sec  */
    int         callback;           /* callback processing (T/F) flag       */
    int         checkLimits;        /* re-check limits (T/F) flag           */
    int         debug;              /* current debugging mode               */
    long        encoder;            /* current motor position (from encoder)*/
    long        encoderDeadband;    /* encoder-reported position deadband   */
    long        encoderResolution;/*  encoder resolution in quadr.cnts/deg */
    char        errorMessage[MAX_STRING_SIZE]; /* root (intial) error msg   */
    int         fault;              /* interlock line active (T/F)          */
    int         faultChange;        /* flag to show interlock line changed
                                         from previous value  (T/F)         */
    int         highLimit;          /* upper limit switch is active (T/F)   */
    long        homeOffset;         /* home position offset in steps        */
    int         homeSwitch;         /* home switch is active (T/F)          */
    int         index;              /* current indexing mode                */
    long        indexVelocity;      /* final indexing velocity in steps/sec */
    int         initializing;       /* record is initializing               */
    int         insideDeadband;     /* motor is inside deadband (T/F)       */
    int         lowLimit;           /* lower limit switch is active (T/F)   */
    long        mode;               /* current operating mode               */
    long        motorResolution; /*   motor resolution in counts per deg   */
    int         move_while_busy;    /* new move requested during move (T/F) */
    int         moving;             /* motor is currently moving (T/F)      */
    SEM_ID      mutexSem;           /* mutual exclusion semaphore           */
    long        position;           /* current motor position (from card)   */
    void        *pPrivate;          /* internal device control struct ptr   */
    short       rejectAck;        /* command rejection acknowledge flag   */
    char        rejectErrMess[MAX_STRING_SIZE]; /* cmd rejection error msg  */
    char        actionErrMess[MAX_STRING_SIZE]; /* cmd action error msg     */
    int         simmChange;         /* simulation mode has changed          */
    short       simmHpvl;           /* index valid state entering simulation*/
    int         simulation;         /* current simulation mode              */
    int         stalled_times;      /* # of identical positions before stall*/
    long        status;             /* motion status                        */
    long        target;             /* target position in steps             */
    long        timeout;            /* timeout processing (T/F) flag        */
    long        velocity;           /* motion velocity in steps/sec         */
}  DEVICE_CONTROL_PRIVATE;

 
#endif /* REC_DEVICE_CONTROL_INC */

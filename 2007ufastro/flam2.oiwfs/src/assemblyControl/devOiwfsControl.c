/*
************************************************************************
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 *
 * (c) <2005>                       (c) <2005>
 * National Research Council        Conseil national de recherches
 * Ottawa, Canada, K1A 0R6          Ottawa, Canada, K1A 0R6
 * All rights reserved              Tous droits reserves
 *                    
 * NRC disclaims any warranties,    Le CNRC denie toute garantie
 * expressed, implied, or statu-    enoncee, implicite ou legale,
 * tory, of any kind with respect   de quelque nature que se soit,
 * to the software, including       concernant le logiciel, y com-
 * without limitation any war-      pris sans restriction toute
 * ranty of merchantability or      garantie de valeur marchande
 * fitness for a particular pur-    ou de pertinence pour un usage
 * pose.  NRC shall not be liable   particulier.  Le CNRC ne
 * in any event for any damages,    pourra en aucun cas etre tenu
 * whether direct or indirect,      responsable de tout dommage,
 * special or general, consequen-   direct ou indirect, particul-
 * tial or incidental, arising      ier ou general, accessoire ou
 * from the use of the software.    fortuit, resultant de l'utili-
 *                                  sation du logiciel.
 *
 ************************************************************************
 *
 * FILENAME
 * devOiwfsControl.c
 *
 * PURPOSE:
 * EPICS Assembly Control Record device support code for the 
 * Flamingos-2 On-Instrument Wave Front Sensor (OIWFS) probe controller.  
 * Defines how the OIWFS will respond to each possible command.
 *
 * FUNCTION NAME(S)
 * oiBusyStateChange    Respond to changes in device command states.
 * oiCancelCommand      Stop a motion or command in progress.
 * oiCheckAttributes    Check command arguments for validity.
 * oiConvertTarget      Convert from X-Y to base-pickoff coordinates
 * oiDevInit            Initialize device support code.
 * oiExecuteCommand     Start base and pickoff stage command execution.
 * oiIndexMode          Re-index the base and pickoff stages.
 * oiInitDeviceSupport  Startup initialization of device support code.
 * oiInitMode           Re-initialize the code and both devices.
 * oiMoveMode           Move to a new X-Y position and shut down.
 * oiProcessFault       Kill current process in response to an interlock
 * oiTestMode           Test the assembly without moving anything.
 * oiTrackMode          Start position tracking (move without shutting down).
 * oiUpdateMode         Do nothing since there is nothing to update.
 *
 *INDENT-OFF*
 * $Log: devOiwfsControl.c,v $
 * Revision 0.2  2005/07/01 17:44:49  drashkin
 * *** empty log message ***
 *
 * Revision 1.7  2005/06/27  bmw
 * Make stage targets 0 for INDEX or PARK in oiConvertTarget()
 *
 * Revision 1.6  2005/05/19  bmw
 * Added magic to private structure.
 *
 * Revision 1.5  2005/05/19  bmw
 * Added/fixed comments, added to test mode.
 *
 * Revision 1.4  2005/04/25  bmw
 * Removed OI_X_PARK_POS and OI_Y_PARK_POS defines.
 * Removed section at beginning of oiBusyStateChange that was commented out.
 * Removed section at beginning of oiCheckAttributes that was commented out.
 *
 * Revision 1.3  2004/11/04  bmw
 * Cut'n'paste f2 references over gmos ones.
 * Removed absolute encoder stuff.
 * Removed X,Y limits defs.
 * Removed 2-part moves.
 * Greatly simplified indexing procedure.
 * 
 * Revision 1.2  2004/09/20  bmw
 * Intial F2 revision copy of Gemini GMOS rev 1.1
 *
 * Revision 1.1  2002/04/24 05:24:56  ajf
 * New directory for port to epics3.13.4GEM8.4.
 *
 *INDENT-ON*
 *
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 ************************************************************************
*/

/*
 *  Includes
 */

#include        <vxWorks.h>
#include        <types.h>
#include        <stdioLib.h>
#include        <lstLib.h>
#include        <string.h>
#include        <stdlib.h>
#include        <wdLib.h>
#include        <math.h>

#include        <alarm.h>
#include        <callback.h>
#include        <dbDefs.h>
#include        <dbAccess.h>
#include        <dbFldTypes.h>
#include        <dbEvent.h>
#include        <devSup.h>
#include        <errMdef.h>
#include        <recSup.h>
#include        <special.h>
#include        <logLib.h>
#include        <tickLib.h>
#include        <sysLib.h>
#include        <taskLib.h>

#include        <assemblyControlRecord.h>
#include        <assemblyControl.h>
#include	<darMessageLevels.h>     /* Message level definitions. */
#include        <f2OiwfsCalc.h>          /* */


/*
 *  Assembly Control Record command input field access mnemonics
 */

#define OI_X_TARGET         *(double *)par->a   /* x target position        */
#define OI_Y_TARGET         *(double *)par->b   /* y target position        */
#define OI_VELOCITY         *(double *)par->d   /* motion velocity          */


/*
 *  Assembly Control Record sensor input field access mnemonics
 */

#define OI_BAS_INCR         *(double *)par->sij /* base encoder position    */
#define OI_PKO_INCR         *(double *)par->sik /* pickoff encoder position */

/*
 *  Internal constant definitions
 */

#define OI_START_TIMEOUT    1       /* timeout for device record cmd start  */
#define OI_STOP_TIMEOUT     5       /* timeout for device record cmd stop   */
#define OI_MAX_RUN_TIMEOUT  500     /* max time for device cmd execution    */
#define OI_MIN_RUN_TIMEOUT  10      /* min time for device cmd exectuion    */

#define OI_CMD_IDLE         0       /* device is idle awaiting a command    */
#define OI_CMD_STARTING     1       /* start directive issued to device     */
#define OI_CMD_EXECUTING    2       /* device record has gone busy          */
#define OI_CMD_STOPPING     3       /* stop directive issued to device      */

#define D2R                 (M_PI/180.0)    /* degrees to radians           */
#define R2D                 (1.0/D2R)       /* radians to degrees           */

#define NUM_DEV     2           /* Number of deviceControl Records attached */
#define OI_MAGIC  0x10101010        /* magic value for testing pointers.    */

          
/*
 *  Device support function prototypes
 */

static long oiBusyStateChange( ASSEMBLY_CONTROL_RECORD * );
static long oiCancelCommand ( ASSEMBLY_CONTROL_RECORD * );
static long oiCheckAttributes( ASSEMBLY_CONTROL_RECORD * );
static long oiDevInit( unsigned );
static long oiIndexMode (ASSEMBLY_CONTROL_RECORD * );
static long oiInitDeviceSupport( ASSEMBLY_CONTROL_RECORD * );
static long oiInitMode (ASSEMBLY_CONTROL_RECORD * );
static long oiMoveMode (ASSEMBLY_CONTROL_RECORD * );
static long oiProcessFault (ASSEMBLY_CONTROL_RECORD * );
static long oiTestMode (ASSEMBLY_CONTROL_RECORD * );
static long oiTrackMode (ASSEMBLY_CONTROL_RECORD * );
static long oiUpdateMode (ASSEMBLY_CONTROL_RECORD * );


/*
 * Internal function prototypes
 */

static long oiConvertTarget (ASSEMBLY_CONTROL_RECORD *); 
static long oiExecuteCommand (ASSEMBLY_CONTROL_RECORD *); 


/*
 *  Device support function definition structure.   This is published
 *  as a public structure to allow the assembly control record to access
 *  the device support functions directly.
 */

struct {
    long            number;
    DEVSUPFUN       devReport;
    DEVSUPFUN       devInit;
    DEVSUPFUN       initDeviceSupport;
    DEVSUPFUN       devGetIoIntInfo;
    DEVSUPFUN       checkAttributes;
    DEVSUPFUN       stopDirective;
    DEVSUPFUN       initMode;
    DEVSUPFUN       moveMode;
    DEVSUPFUN       trackMode;
    DEVSUPFUN       indexMode;
    DEVSUPFUN       testMode;
    DEVSUPFUN       ackReceived;
    DEVSUPFUN       updateMode;
    DEVSUPFUN       processFault;
} devOiwfsAssembly = {
    14,                  /* Number of support functions in DSET              */
    NULL,                /* devReport is not used                            */
    oiDevInit,           /* Initialize device support code.                  */
    oiInitDeviceSupport, /* Startup initialization of device support code.   */
    NULL,                /* devGetIoIntInfo is not used                      */
    oiCheckAttributes,   /* Check command arguments for validity.            */
    oiCancelCommand,     /* Stop a motion or command in progress.            */
    oiInitMode,          /* Re-initialize the code and both devices.         */
    oiMoveMode,          /* Move to a new X-Y position and shut down.        */
    oiTrackMode,         /* Start position tracking (move w/o shutting down).*/
    oiIndexMode,         /* Re-index the base and pickoff stages.            */
    oiTestMode,          /* Test the assembly without moving anything.       */
    oiBusyStateChange,   /* Respond to changes in device command states.     */
    oiUpdateMode,        /* Do nothing since there is nothing to update.     */
    oiProcessFault       /* Kill current process in response to an interlock */
    };
       

/*
 *  Internal control structure definition.   This structure holds
 *  all of the oiwfs status information between executions of the assembly
 *  control record.
 */

typedef struct {
    SEM_ID      mutexSem;          /* mutual exclusion semaphore            */
    char        errorMessage[MAX_STRING_SIZE];  /* root error message       */
    long        baseActive;        /* current base command state            */    
    double      baseAngle;         /* current target for base stage         */
    double      baseVelocity;      /* current base stage velocity           */
    long        pickoffActive;     /* current pickoff command state         */
    double      pickoffAngle;      /* current target for pko stage          */
    double      pickoffVelocity;   /* current pickoff stage velocity        */
    long        magic;             /* Magic safety value                    */

} OI_DEV_CONTROL_PRIVATE;


/*
 *  Debugging macro to send the system time, error string and one
 *  value to the vxWorks logging system if the current debugging
 *  level exceeds the given debugging threshold.
 */

#define DEBUG(l,FMT,V)                                                  \
{                                                                       \
    int k=l;                                                            \
    if (k <= par->dbug)                                                 \
    printf ("%s: "FMT, taskName(0), tickGet(), par->name, V);           \
}


/*
 * Save the first error message received.   This will prevent subsequent
 * error messages from overwriting the root cause of the problem.
 */

#define SET_ERR_MSG(MSG)                                                \
{                                                                       \
    if (!strlen (pDevPvt->errorMessage))                                \
    strncpy (pDevPvt->errorMessage, MSG, MAX_STRING_SIZE - 1);          \
}


/*
 * Clear out the saved error message after it has been dealt with.  This
 * allows the next error message to be saved....
 */

#define CLEAR_ERR_MSG                                                   \
{                                                                       \
    pDevPvt->errorMessage[0] = '\0';                                    \
}


/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * oiBusyStateChange
 *
 * INVOCATION:
 * status = oiBusyStateChange (par);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) par  (ASSEMBLY_CONTROL_RECORD *)     assemblyControl record structure.
 *
 *
 * FUNCTION VALUE:
 * (long) function return status.
 *
 * PURPOSE:
 * Respond to a device control record command start or completion
 *
 * DESCRIPTION:
 * This function is called directly by the assemblyControl Record whenever
 * one of two things happens...
 *
 *   - The timeout timer that was set for the action in progress expires
 *         meaning that the action took too long to complete.
 *   - Either the base or pickoff stage device control records starts or
 *         finishes a commanded action.
 *
 *
 *  The deviceControl Records operate on the action model as follows:
 *      - When an action is started by issuing the START directive
 *          the action state BUSY field changes from IDLE to BUSY.
 *      - When an action completes successfully the action state
 *          BUSY field changes from BUSY to IDLE.
 *      - When an action fails for some reason during execution
 *          the action state BUSY field changes from BUSY to ERROR
 *          and an error message appears on the MESS field.
 *      - When an action is stopped by issuing a STOP directive 
 *          the action state BUSY field will change from BUSY to IDLE
 *          as soon as the action has stopped.
 *
 *  The assemblyControl Record detects changes in the deviceRecord BUSY
 *  field for both the base and pickoff stages and calls this function
 *  when they occur.
 *
 * EXTERNAL VARIABLES:
 *
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static long oiBusyStateChange
(
    ASSEMBLY_CONTROL_RECORD *par        /* assembly control record struct.  */
)
{
    OI_DEV_CONTROL_PRIVATE *pDevPvt;    /* Internal control structure       */
    int baseBusyChange;                 /* Base command state has changed   */
    int pickoffBusyChange;              /* Pickoff command state has changed*/
    int dummy;                          /* Other command state has changed  */
    long status = DAR_S_SUCCESS;        /* Function status return value.    */
    long runTimeout;                    /* Maximum motion time              */

    DEBUG(DAR_MSG_FULL, "<%ld> %s:oiBusyStateChange: entry%c\n", ' ');

    pDevPvt = (OI_DEV_CONTROL_PRIVATE *) assGetPrivateStruct (par);


    /*
     *  React immediately to timeouts generated by the assembly record.
     *  This will happen if:
     *      - a device control record does not start an action 
     *          after receiving a START directive.
     *      - a device control record does not stop an action
     *          after receiving a STOP directive. 
     *      - a device action takes longer than expected.
     *  In all cases abort the command and return an error message
     *  describing what happened.  
     *
     */

    if (assCommandTimedOut (par))
    {
        semTake (pDevPvt->mutexSem, WAIT_FOREVER);

        if (pDevPvt->baseActive == OI_CMD_STARTING)
        {
            DEBUG(DAR_MSG_ERROR, 
                  "<%ld> %s:oiBusyStateChange: Base stage did not start in time%c\n", ' ');
            SET_ERR_MSG ( "Base command did not execute" );
        }

        else if (pDevPvt->pickoffActive == OI_CMD_STARTING)
        {
            DEBUG(DAR_MSG_ERROR, 
                  "<%ld> %s:oiBusyStateChange: Pickoff stage did not start in time%c\n", ' ');
            SET_ERR_MSG ( "Pickoff command did not execute" );
        }

        else if (pDevPvt->baseActive == OI_CMD_STOPPING)
        {
            DEBUG(DAR_MSG_ERROR, 
                  "<%ld> %s:oiBusyStateChange: Base stage did not stop in time%c\n", ' ');
            SET_ERR_MSG ( "Base did not stop in time" );
        }

        else if (pDevPvt->pickoffActive == OI_CMD_STOPPING)
        {
            DEBUG(DAR_MSG_ERROR, 
                  "<%ld> %s:oiBusyStateChange: Pickoff stage did not stop in time%c\n", ' ');
            SET_ERR_MSG ( "Pickoff did not stop in time" );
        }

        else if (pDevPvt->baseActive == OI_CMD_EXECUTING)
        {
            DEBUG(DAR_MSG_ERROR, 
                  "<%ld> %s:oiBusyStateChange: Base stage did not finish in time%c\n", ' ');
            SET_ERR_MSG ( "Base motion timeout" );
        }

        else if (pDevPvt->pickoffActive == OI_CMD_EXECUTING)
        {
            DEBUG(DAR_MSG_ERROR, 
                  "<%ld> %s:oiBusyStateChange: Pickoff stage did not finish in time%c\n", ' ');
            SET_ERR_MSG ( "Pickoff motion timeout" );
        }

        else
        {
            DEBUG(DAR_MSG_ERROR, 
                  "<%ld> %s:oiBusyStateChange: Unexpected command timeout%c\n", ' ');
            SET_ERR_MSG ( "Unexpected command timeout" );
        }

        semGive (pDevPvt->mutexSem);

        return (oiCancelCommand (par));
    }


    /*
     *  No timeout, so must be a device control record action state change
     *  then.   Ask the assembly record which device states have changed
     *  since we last asked.
     */

    assGetBusResponse (par, &baseBusyChange, &pickoffBusyChange,
                       &dummy, &dummy, &dummy);


    /*
     * Respond to changes in base stage action state
     */

    if (baseBusyChange) switch (par->bus1)
    {
        /*
         * Device action has finished successfully, clear the device active 
         * flag to indicate this.
         */

        case DAR_DEV_BUSY_IDLE:
            semTake (pDevPvt->mutexSem, WAIT_FOREVER);
            pDevPvt->baseActive = OI_CMD_IDLE;
            semGive (pDevPvt->mutexSem);
            break;


        /*
         * Device action has started successfully, set the device active flag
         * from OI_CMD_STARTING to OI_CMD_EXECUTING to indicate this.
         */

        case DAR_DEV_BUSY_BUSY:
            semTake (pDevPvt->mutexSem, WAIT_FOREVER);
            pDevPvt->baseActive = OI_CMD_EXECUTING;
            semGive (pDevPvt->mutexSem);
            break;


        /*
         * Device action has failed for some reason, clear the device active 
         * flag and abort the OIWFS assembly command.
         */

        case DAR_DEV_BUSY_ERROR:
            semTake (pDevPvt->mutexSem, WAIT_FOREVER);
            pDevPvt->baseActive = OI_CMD_IDLE;
            semGive (pDevPvt->mutexSem);
            DEBUG(DAR_MSG_ERROR, 
                  "<%ld> %s:oiBusyStateChange: Base stage error%c\n", ' ');
            oiCancelCommand (par);
            break;
    }


    /*
     * Respond to changes in pickoff stage action state
     */

    if (pickoffBusyChange) switch (par->bus2)
    {
        /*
         * Device action has finished successfully, clear the device active 
         * flag to indicate this.
         */

        case DAR_DEV_BUSY_IDLE:
            semTake (pDevPvt->mutexSem, WAIT_FOREVER);
            pDevPvt->pickoffActive = OI_CMD_IDLE;
            semGive (pDevPvt->mutexSem);
            break;

        /*
         * Device action has started successfully, set the device active flag
         * from OI_CMD_STARTING to OI_CMD_EXECUTING to indicate this.
         */

        case DAR_DEV_BUSY_BUSY:
            semTake (pDevPvt->mutexSem, WAIT_FOREVER);
            pDevPvt->pickoffActive = OI_CMD_EXECUTING;
            semGive (pDevPvt->mutexSem);
            break;

        /*
         * Device action has failed for some reason, clear the device active 
         * flag and abort the OIWFS assembly command.
         */

        case DAR_DEV_BUSY_ERROR:
            semTake (pDevPvt->mutexSem, WAIT_FOREVER);
            pDevPvt->pickoffActive = OI_CMD_IDLE;
            semGive (pDevPvt->mutexSem);
            DEBUG(DAR_MSG_ERROR, 
                  "<%ld> %s:oiBusyStateChange: Pickoff stage error%c\n", ' ');
            oiCancelCommand (par);
            break;
    }


    /*
     * If both devices are busy then the motion has started successfully
     * so start the motion timeout timer.  The maximum motion time is
     * calculated based on the velocity of motion.  For very fast motions use
     * a minimum timeout value.
     */

    if (par->bus1 == DAR_DEV_BUSY_BUSY && par->bus2 == DAR_DEV_BUSY_BUSY)
    {
        assStopTimer (par, &status);
        assStartTimer(par, &status,
                     (((runTimeout = OI_MAX_RUN_TIMEOUT*OI_VELOCITY) < 0 ) ?
                                        OI_MIN_RUN_TIMEOUT : runTimeout));
    }


    /*
     *  If neither device is busy then the re-positioning action has
     *  either completed successfully or encountered an error along the
     *  way.  Either way signal the assembly record that the command
     *  has completed.
     */

    if (par->bus1 != DAR_DEV_BUSY_BUSY && 
        par->bus2 != DAR_DEV_BUSY_BUSY)
    {
        assStopTimer (par, &status);

        semTake (pDevPvt->mutexSem, WAIT_FOREVER);

        if (par->bus1 == DAR_DEV_BUSY_ERROR)
        {
            DEBUG(DAR_MSG_ERROR, 
                  "<%ld> %s:oiBusyStateChange: Base stage failed command%c\n", ' ');
            SET_ERR_MSG ( "Base error, see base status" );
        }

        if (par->bus2 == DAR_DEV_BUSY_ERROR)
        {
            DEBUG(DAR_MSG_ERROR, 
                  "<%ld> %s:oiBusyStateChange: Pickoff stage failed command%c\n", ' ');
            SET_ERR_MSG ( "Pickoff error, see pickoff status" );
        }

        status = (strlen(pDevPvt->errorMessage)) ? DAR_E_DEVICE:DAR_S_SUCCESS;

        assCommandFinish (par, status, pDevPvt->errorMessage);
        CLEAR_ERR_MSG;
        semGive (pDevPvt->mutexSem);
    }

    return (status);
}


/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * oiCancelCommand
 *
 * INVOCATION:
 * status = oiCancelCommand (par);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) par  (ASSEMBLY_CONTROL_RECORD *)     assemblyControl record structure.
 *
 * FUNCTION VALUE:
 * (long) function status return.
 *
 * PURPOSE:
 * Cancel a command in progress
 *
 * DESCRIPTION:
 * Stop the command currently in progress. This provides a means to stop the 
 * second stage after the first has stopped due to an error.
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 * DEFICIENCIES:
 * If STOP directive to Base stage fails (dbPutLink returns non-zero status) 
 * then STOP directive to Pickoff stage won't be attempted.
 *-
 ************************************************************************
 */

static long oiCancelCommand 
(
    ASSEMBLY_CONTROL_RECORD *par        /* assemblyControl record structure */
)
{
    OI_DEV_CONTROL_PRIVATE *pDevPvt;    /* Internal control structure       */
    unsigned short dir = DAR_DEV_DIR_STOP; /* deviceControl stop directive  */
    long nRequest = 1;                  /* Number of items to send on link  */
    long status = DAR_S_SUCCESS;        /* Function return status           */


    DEBUG(DAR_MSG_FULL, "<%ld> %s:oiCancelCommand: entry%c\n", ' ');

    pDevPvt = (OI_DEV_CONTROL_PRIVATE *) assGetPrivateStruct (par);


    /*
     *  If neither device is currently active then we are finished and it is
     *  safe to tell the assemblyControl record that we have cancelled the 
     *  command (unless it's a TRACK mode command). 
     */

    if ( pDevPvt->baseActive == OI_CMD_IDLE && 
         pDevPvt->pickoffActive == OI_CMD_IDLE &&
         par->mode != DAR_MODE_TRACK )
    {
        assCommandFinish (par, status, pDevPvt->errorMessage);
        CLEAR_ERR_MSG;
        return status;
    }


    /*
     *  Otherwise one or both stages are active so issue the stop command to 
     *  the active device(s), set their activity flag(s) to stopping and set 
     *  the timeout timer to ensure that the action(s) stop immediately.
     *  The assemblyControl record will be told the command has been cancelled
     *  when both devices reach to the IDLE state.
     */

    if ( pDevPvt->baseActive != OI_CMD_IDLE ||
         (par->mode == DAR_MODE_TRACK && par->bus1 != DAR_DEV_BUSY_ERROR) )
    {
        DEBUG(DAR_MSG_FULL, 
              "<%ld> %s:oiCancelCommand: stopping base stage %c\n", ' ');
        CHECKSTAT((status = dbPutLink(&(par->odr1),DBR_SHORT, &dir, nRequest)),return(status) ); 
	semTake (pDevPvt->mutexSem, WAIT_FOREVER);
        pDevPvt->baseActive = OI_CMD_STOPPING;
        semGive (pDevPvt->mutexSem);
        assStopTimer (par, &status);
        assStartTimer (par, &status, OI_STOP_TIMEOUT);
     }

    if ( pDevPvt->pickoffActive != OI_CMD_IDLE ||
         (par->mode == DAR_MODE_TRACK  && par->bus2 != DAR_DEV_BUSY_ERROR))
    {
        DEBUG(DAR_MSG_FULL, 
              "<%ld> %s:oiCancelCommand: stopping pickoff stage %c\n", ' ');
        CHECKSTAT((status = dbPutLink(&(par->odr2),DBR_SHORT, &dir, nRequest)),return(status) );
        semTake (pDevPvt->mutexSem, WAIT_FOREVER);
        pDevPvt->pickoffActive = OI_CMD_STOPPING;
        semGive (pDevPvt->mutexSem);
        assStopTimer (par, &status);
        assStartTimer (par, &status, OI_STOP_TIMEOUT);
    }

    return (status);
}


/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * oiCheckAttributes
 *
 * INVOCATION:
 * status = oiCheckAttributes (par);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) par  (ASSEMBLY_CONTROL_RECORD *)     assemblyControl record structure.
 *
 * FUNCTION VALUE:
 * attribute accept or reject flag.
 *
 * PURPOSE:
 * Check to see if the requested action is valid at this time
 *
 * DESCRIPTION:
 * Setup the operating mode by rejecting invalid modes and changing any 
 * PARK requests into INDEX ones. For all motion requests, convert the 
 * X,Y targets into stage angles.  Unless in TRACK mode, send the mode, 
 * targets and velocities to the device records to ensure they are valid. 
 * If the demands came from the TCS, they were already X,Y range-checked 
 * before they got here.
 * 
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 * DEFICIENCIES:
 * Targets are checked for INDEX mode.
 *-
 ************************************************************************
 */

static long oiCheckAttributes 
(
    ASSEMBLY_CONTROL_RECORD *par        /* assemblyControl record structure */
)
{
    OI_DEV_CONTROL_PRIVATE *pDevPvt;    /* Internal control structure       */
    unsigned short dir;                 /* deviceControl record directive   */
    unsigned short mode;                /* deviceControl record mode        */
    unsigned short returnedAck;         /* deviceControl acknowledge flag   */
    long nRequest = 1;                  /* Number of values to send on link */
    long status;                        /* Link access status               */


    DEBUG(DAR_MSG_FULL, "<%ld> %s:oiCheckAttributes: entry%c\n", ' ');

    pDevPvt = (OI_DEV_CONTROL_PRIVATE *) assGetPrivateStruct (par);

    /*
     * Set the mode up.
     *
     * Use index mode for a park request as these devices don't
     * use park position targets.
     */

    if (par->mode == DAR_MODE_PARK)
    {
        mode = DAR_MODE_INDEX;
    }
    else
    {
        mode = par->mode;
    }

    /*
     *  Reject update requests since there is nothing to update!
     */
   
    if (mode == DAR_MODE_UPDATE)
    {
        DEBUG(DAR_MSG_ERROR, 
              "<%ld> %s:oiCheckAttributes: UPDATE mode invalid for OIWFS%c\n",' ');
        assAddErrorMessage (par, 
                            "UPDATE mode invalid for OIWFS assembly");
        return DAR_ACK_VAL_REJECT;
    }


    /*
     *  If this is a motion request then check to ensure that the requested
     *  probe position can be reached.
     */

    if (mode == DAR_MODE_INDEX || 
        mode == DAR_MODE_MOVE ||
        mode == DAR_MODE_TRACK )
    {

        /*
         * Calculate the base and pickoff stage angles required to reach
         * the target position.  Reject the command if the conversion fails.
         */

        status = oiConvertTarget (par);

        if (status)
        {
            DEBUG(DAR_MSG_ERROR, 
                  "<%ld> %s:oiCheckAttributes: Angles calculated cannot be reached!%c\n",' ');
            assAddErrorMessage (par, "Probe target not accessible");
            return DAR_ACK_VAL_REJECT;
        }

        
        /*
         *  Pass the converted base and pickoff stage angles and velocities
         *  to the deviceControl records.   The CHECK directive will be issued
         *  later to ensure that they can reach these positions.
         */

        DEBUG(DAR_MSG_FULL, 
              "<%ld> %s:oiCheckAttributes: base target:%f\n", 
              pDevPvt->baseAngle);
	CHECKSTAT((status = dbPutLink(&(par->pos1),DBR_DOUBLE,&(pDevPvt->baseAngle), nRequest)),return DAR_ACK_VAL_REJECT );
        DEBUG(DAR_MSG_FULL, 
              "<%ld> %s:oiCheckAttributes: pickoff target:%f\n", 
              pDevPvt->pickoffAngle);
        CHECKSTAT((status = dbPutLink(&(par->pos2),DBR_DOUBLE,&(pDevPvt->pickoffAngle), nRequest)),return DAR_ACK_VAL_REJECT );

        DEBUG(DAR_MSG_FULL, 
              "<%ld> %s:oiCheckAttributes: base velocity:%f\n", 
              pDevPvt->baseVelocity);
        CHECKSTAT((status = dbPutLink(&(par->vel1),DBR_DOUBLE,&(pDevPvt->baseVelocity), nRequest)),return DAR_ACK_VAL_REJECT );

        DEBUG(DAR_MSG_FULL, 
              "<%ld> %s:oiCheckAttributes: pickoff velocity:%f\n", 
              pDevPvt->pickoffVelocity);
        CHECKSTAT((status = dbPutLink(&(par->vel2),DBR_DOUBLE,&(pDevPvt->pickoffVelocity), nRequest)),return DAR_ACK_VAL_REJECT );
    }



    /*
     * Pass the requested mode to the deviceControl records.
     */

    CHECKSTAT((status = dbPutLink(&(par->mod1),DBR_SHORT,&mode, nRequest)),
               return DAR_ACK_VAL_REJECT );

    CHECKSTAT((status = dbPutLink(&(par->mod2),DBR_SHORT,&mode, nRequest)),
               return DAR_ACK_VAL_REJECT );


    /*
     *  Issue the CHECK directive to both the base and pickoff stages and 
     *  look at the acknowledge (ACK) field of each to see if the requested 
     *  action is valid at this time.
     *
     *  If we are in position tracking mode then the devices must react to
     *  new target positions as quickly as possible so we skip the final check
     *  by the deviceControl records.
     */

    if (par->mode != DAR_MODE_TRACK)
    {

        dir = DAR_DEV_DIR_CHECK;

	CHECKSTAT((status = dbPutLink(&(par->odr1),DBR_SHORT,&dir,nRequest)),
                   return DAR_ACK_VAL_REJECT);
        CHECKSTAT((status = dbGetLink(&(par->ack1),DBR_USHORT,&returnedAck,0,0)),
                   return DAR_ACK_VAL_REJECT);

	if (returnedAck != DAR_DEV_VAL_ACCEPT )
        {
            DEBUG(DAR_MSG_ERROR, 
                  "<%ld> %s:oiCheckAttributes: Base command rejected%c\n",' ');
            assAddErrorMessage (par, "Command rejected, see base status");
            return DAR_ACK_VAL_REJECT;
        }

        CHECKSTAT((status = dbPutLink(&(par->odr2),DBR_SHORT,&dir,nRequest)),
                   return DAR_ACK_VAL_REJECT);
        CHECKSTAT((status = dbGetLink(&(par->ack2),DBR_USHORT,&returnedAck,0,0)),
                   return DAR_ACK_VAL_REJECT);

        if (returnedAck != DAR_DEV_VAL_ACCEPT )
        {
            DEBUG(DAR_MSG_ERROR, 
                  "<%ld> %s:oiCheckAttributes: Pickoff command rejected%c\n",' ');
            assAddErrorMessage (par, "Command rejected, see pickoff status");
            return DAR_ACK_VAL_REJECT;
        }
    }

    return (DAR_ACK_VAL_ACCEPT);
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * oiConvertTarget
 *
 * INVOCATION:
 * status = oiConvertTarget (par);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) par  (ASSEMBLY_CONTROL_RECORD *)   assemblyControl record structure.
 *
 * FUNCTION VALUE:
 * (long) conversion status flag:
 *      0 - successful conversion
 *     -1 - target outside patrol area
 *
 * PURPOSE:
 * To convert rectangular to polar coordinates for the OIWFS
 *
 * DESCRIPTION:
 * This function accepts probe positions as X-Y values, expressed in 
 * millimeters from the home position, and converts them to the base
 * and pickoff stage angles required to place the probe at that position
 * using the f2OiwfsCalculateProbeAngles().
 *
 * This function should always return successfully if the patrol area
 * is defined properly since any targets resulting from TCS commands were 
 * already checked once. However, remember that by design if a user is
 * entering targets directly at this layer, they will be accepted as 
 * long as the resulting angles can be calculated.
 *
 * The X-Y target position is taken directly from the assembly record
 * A and B field and converted to angles in degrees.  The current position
 * is compared to the target position to determine the relative distance
 * of the move and 2-axis coordinated motion is simulated by scaling back 
 * the velocity of the stage that has the least distance to move.
 *
 * Only velocity is set (to maximum slew velocity) if the mode is PARK 
 * or INDEX.

 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 * f2OiwfsCalc.c/f2OiwfsCalculateProbeAngles()
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static long oiConvertTarget 
(
    ASSEMBLY_CONTROL_RECORD *par        /* assemblyControl record structure */
)
{
    OI_DEV_CONTROL_PRIVATE *pDevPvt;    /* Internal control structure       */
    double basRelMove;                  /* Angular displacement of base     */
    double pkoRelMove;                  /* Angluar displacement of pickoff  */
    double bAngle;                      /* Calculated angle of base stage   */
    double pAngle;                      /* Calculated angle of pickoff stage*/
    double probeAngle;                  /* Angle of probe head wrt detector */
    long status = DAR_S_SUCCESS;        /* Conversion status flag           */


    DEBUG(DAR_MSG_FULL, "<%ld> %s:oiConvertTarget: entry%c\n", ' ');

    pDevPvt = (OI_DEV_CONTROL_PRIVATE *) assGetPrivateStruct (par);



    if (par->mode == DAR_MODE_INDEX ||
        par->mode == DAR_MODE_PARK   )
    {
        semTake (pDevPvt->mutexSem, WAIT_FOREVER);
        pDevPvt->baseVelocity = OI_VELOCITY;
        pDevPvt->pickoffVelocity = OI_VELOCITY;
        pDevPvt->baseAngle = 0.0;  
        pDevPvt->pickoffAngle = 0.0;
        semGive (pDevPvt->mutexSem);
    }
    else
    {

        /*
         *  Use the target position written to the assembly record
         *  A and B attribute fields
         */

        status = f2OiwfsCalculateProbeAngles (OI_X_TARGET,
                                              OI_Y_TARGET,
                                              &bAngle, &pAngle, &probeAngle);


        /*
         *  If it was not possible to convert this target position to angles
         *  then return here...
         */

        if (status != DAR_S_SUCCESS) 
        {
            DEBUG(DAR_MSG_ERROR, "<%ld> %s:oiConvertTarget: f2OiwfsCalculateProbeAngles failed%c\n", ' ');
            return status;
        }

        /*
         *  Otherwise it should be possible to reach this position so convert
         *  the angles from radians to degrees (the native units of the two
         *  device control records) and simulate a 2-axis coordinated motion by
         *  scaling the velocities so that both stages reach their target 
         *  angles at roughly the same time.
         */

        semTake (pDevPvt->mutexSem, WAIT_FOREVER);
        pDevPvt->baseAngle = bAngle * R2D;  
        pDevPvt->pickoffAngle = pAngle * R2D;

        basRelMove = fabs(pDevPvt->baseAngle - OI_BAS_INCR);
        pkoRelMove = fabs(pDevPvt->pickoffAngle - OI_PKO_INCR);

        if (basRelMove > pkoRelMove)
        {
            pDevPvt->baseVelocity = OI_VELOCITY;
            pDevPvt->pickoffVelocity = OI_VELOCITY * pkoRelMove / basRelMove;
        }

        else
        {
            pDevPvt->pickoffVelocity = OI_VELOCITY;
            pDevPvt->baseVelocity = OI_VELOCITY * basRelMove / pkoRelMove;
        }

        semGive (pDevPvt->mutexSem);
    }

    return (status);
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * oiDevInit
 *
 * INVOCATION:
 * status = oiDevInit (pass);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) pass  (int) Pass number (function called twice during initialization)
 *
 * FUNCTION VALUE:
 * (long) initialization pass success code.
 *
 * PURPOSE:
 * Initialize global device support functions
 *
 * DESCRIPTION:
 * Called before and after an EPICS database has been loaded.
 * There is nothing global to be set up for the device
 * support code.
 *
 * EXTERNAL VARIABLES:
 * None.
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */


static long oiDevInit (unsigned    after)
{

    /*
     *  Nothing to do, so we were very successful in doing it!
     */
   
    return DAR_S_SUCCESS;
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * oiExecuteCommand
 *
 * INVOCATION:
 * status = oiExecuteCommand (par);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) par  (ASSEMBLY_CONTROL_RECORD *)    assemblyControl record structure.
 *
 *
 * FUNCTION VALUE:
 * (long) function status return.
 *
 * PURPOSE:
 * Execute a pre-configured command on base and pickoff stages
 *
 * DESCRIPTION:
 * Start the execution of a probe action using the following algorithm:
 *
 *      Send the GO directive to both the base and pickoff stages.
 *      Check the ACK field of each record, if either of the actions 
 *        was rejected then cancel the command.
 *      Otherwise if good to go then set the timeout timer to the maximum 
 *        length of time the actions should take.   This allows the 
 *        assemblyControl record to abort an action that has hung up for
 *        some reason.
 *
 * EXTERNAL VARIABLES:
 *
 *
 * PRIOR REQUIREMENTS:
 * Target position, operating mode and motion velocity must have been
 * loaded into the two deviceControl records before calling this
 * function.  Mode, targets and velocities have been verified using
 * the CHECK directive.
 *
 * SEE ALSO:
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static long oiExecuteCommand 
(
    ASSEMBLY_CONTROL_RECORD *par        /* assemblyControl record structure */
)
{
    OI_DEV_CONTROL_PRIVATE *pDevPvt;    /* Internal control structure       */
    unsigned short dir;                 /* Directive value written to record*/
    long nRequest = 1;                  /* Number of items to send on link  */
    unsigned short returnedAck;         /* Command accept/reject flag       */
    long status = DAR_S_SUCCESS;        /* Function status                  */
    long runTimeout;                    /* Maximum execution time for cmd   */

    DEBUG(DAR_MSG_FULL, "<%ld> %s:oiExecuteCommand: entry%c\n", ' ');

    pDevPvt = (OI_DEV_CONTROL_PRIVATE *) assGetPrivateStruct (par);


    /*
     *  We can assume that the target position, operating mode and motion 
     *  velocity have already been written to the deviceControl records by
     *  one of the motion setup functions.  All that is required is to 
     *  write the GO directive to each record to start executing the preset
     *  action.
     */

    semTake (pDevPvt->mutexSem, WAIT_FOREVER);

    dir = DAR_DEV_DIR_GO;

    CHECKSTAT((status = dbPutLink(&(par->odr1),DBR_SHORT, &dir, nRequest)),
               return(oiCancelCommand(par)) );


    /*
     *  Check that the deviceControl records accepted the GO command.  
     *  If the command was accepted set the appropriate stageActive flag
     *  to indicate that a GO directive has been sent an we expect to see
     *  and IDLE to BUSY transition on the device BUSY field.
     *
     *  If either of the two stages rejected the command call the 
     *  oiCancelCommand function to cancel the commands sent to the stages 
     *  then return.
     */

    CHECKSTAT((status = dbGetLink(&(par->ack1),DBR_USHORT,&returnedAck,0,0)),
               return (oiCancelCommand (par)));

    if (returnedAck != DAR_DEV_VAL_ACCEPT )
    {
        DEBUG(DAR_MSG_ERROR, 
              "<%ld> %s:oiExecuteCommand: Base Stage rejected command%c\n",' ');
        SET_ERR_MSG ( "Base stage rejected command" );
        semGive (pDevPvt->mutexSem);
        return (oiCancelCommand (par));       
    }

    pDevPvt->baseActive = OI_CMD_STARTING;

    /*  Now do the same for the pickoff stage */

    CHECKSTAT((status = dbPutLink(&(par->odr2),DBR_SHORT, &dir, nRequest)),
               return(oiCancelCommand(par)) );


    CHECKSTAT((status = dbGetLink(&(par->ack2),DBR_USHORT,&returnedAck,0,0)),
               return (oiCancelCommand (par)));
    if (returnedAck != DAR_DEV_VAL_ACCEPT )
    {
        DEBUG(DAR_MSG_ERROR, 
              "<%ld> %s:oiExecuteCommand: Pickoff Stage rejected command%c\n",' ');
        SET_ERR_MSG ( "Pickoff stage rejected command" );
        semGive (pDevPvt->mutexSem);
        return (oiCancelCommand (par));
    }

    pDevPvt->pickoffActive = OI_CMD_STARTING;
    semGive (pDevPvt->mutexSem);


    /*
     *  Start the assemblyRecord timeout timer to the maximum length of
     *  time this action is expected to take.
     */
     
    /*
     *  If both of two stages are busy at this point then this is a 
     *  position update received during motion so re-calculate the motion 
     *  timeout based on the stage velocity.
     */

    if (par->bus1 == DAR_DEV_BUSY_BUSY && par->bus2 == DAR_DEV_BUSY_BUSY)
    {
        DEBUG(DAR_MSG_MAX, 
              "<%ld> %s:oiExecuteCommand: Re-calculate motion timeout%c\n",' ');
        assStopTimer (par, &status);
        assStartTimer (par, &status, 
                       (((runTimeout=OI_MAX_RUN_TIMEOUT*OI_VELOCITY)<0 ) ?
                        OI_MIN_RUN_TIMEOUT : runTimeout ) ) ;
        DEBUG(DAR_MSG_MAX, 
              "<%ld> %s:oiExecuteCommand: Motion timeout re-calculated:%ld\n",runTimeout);
    }


    /*
     *  Otherwise set the timeout for the maximum time it should take the
     *  stages to set their BUSY fields from IDLE to BUSY in response to the
     *  GO commands.
     */

    else
    {
        assStopTimer (par, &status);
        assStartTimer (par, &status, OI_START_TIMEOUT);
    }

    return status;
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * oiIndexMode
 *
 * INVOCATION:
 * status = oiIndexMode (par);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) par  (ASSEMBLY_CONTROL_RECORD *) assemblyControl record structure.
 *
 * FUNCTION VALUE:
 * (long) function status return.
 *
 * PURPOSE:
 * Setup the index mode.
 *
 * DESCRIPTION:
 * Just start the motion by calling executeCommand.
 *
 *                
 * EXTERNAL VARIABLES:
 *
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 * DEFICIENCIES:
 *-
 ************************************************************************
 */

static long oiIndexMode 
(
    ASSEMBLY_CONTROL_RECORD *par        /* assemblyControl record structure */
)
{
    DEBUG(DAR_MSG_FULL, "<%ld> %s:oiIndexMode: entry%c\n", ' ');

    /*
     *  Mode field has already been loaded so just execute the command.
     */

    return (oiExecuteCommand (par));
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * oiInitDeviceSupport
 *
 * INVOCATION:
 * status = oiInitDeviceSupport (par);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) par  (ASSEMBLY_CONTROL_RECORD *) assemblyControl record structure.
 *
 * FUNCTION VALUE:
 * (long) initialization success code.
 *
 * PURPOSE:
 * Initialize device support-specific functions
 *
 * DESCRIPTION:
 * Create an internal control structure to keep information specific to 
 * this instantiation of the record.  
 * Initialize critical record and internal control structure fields.
 *
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static long oiInitDeviceSupport 
(
    ASSEMBLY_CONTROL_RECORD *par        /* assemblyControl record structure */
)
{
    OI_DEV_CONTROL_PRIVATE *pDevPvt;    /* internal control structure       */
    long status = DAR_S_SUCCESS;        /* status return                    */

    DEBUG(DAR_MSG_FULL, "<%ld> %s:oiInitDeviceSupport: entry%c\n", ' ');


    /*
     * Create an internal control structure for this record and save it in
     * the parent record's structure.
     */

    pDevPvt = malloc (sizeof(OI_DEV_CONTROL_PRIVATE));
    if (pDevPvt == NULL)
    {
        status = DAR_E_MALLOC;
        recGblRecordError (status, par, __FILE__ ":no room: device private");
        return (status);
    }

    assAttachPrivateStruct (par, (void *) pDevPvt);


    /*
     * Create a mutual exclusion semaphore to protect the private structure
     * during asynchronous callback access.
     */
    
    pDevPvt->mutexSem = NULL;
    if (  (pDevPvt->mutexSem =
            semMCreate (SEM_Q_PRIORITY | SEM_INVERSION_SAFE) ) == NULL )
    {
        DEBUG(DAR_MSG_FATAL, 
              "<%ld> %s:oiInitDeviceSupport: mutex failed.%c\n", ' ' );
        status = DAR_E_MALLOC;
        recGblRecordError (status, par, __FILE__ ":no room for mutex creation");
        return( status );
    }


    /*
     *  Initialize the stage active and motion modifier flags
     */

    semTake (pDevPvt->mutexSem, WAIT_FOREVER);
    CLEAR_ERR_MSG;
    pDevPvt->magic = OI_MAGIC;

    pDevPvt->baseActive = OI_CMD_IDLE;
    pDevPvt->pickoffActive = OI_CMD_IDLE;
    semGive (pDevPvt->mutexSem);

    return (status);
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * oiInitMode
 *
 * INVOCATION:
 * status = oiInitMode (par);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) par  (ASSEMBLY_CONTROL_RECORD *) assemblyControl record structure.
 *
 * FUNCTION VALUE:
 * (long) initialization success code.
 *
 * PURPOSE:
 * Initialize the base and pickoff stages
 *
 * DESCRIPTION:
 * Execute the initialization mode command that has already been 
 * loaded into the base and pickoff stages.
 *
 * EXTERNAL VARIABLES:
 *
 *
 * PRIOR REQUIREMENTS:
 * INIT must have been written to the base and pickoff stage devicControl
 * record MODE fields.
 *
 * SEE ALSO:
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static long oiInitMode (ASSEMBLY_CONTROL_RECORD *par)
{
    DEBUG(DAR_MSG_FULL, "<%ld> %s:oiInitMode: entry%c\n", ' ');

    
    /*
     *  Mode field has already been loaded so just execute the command.
     */

    return (oiExecuteCommand (par));

}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * oiMoveMode
 *
 * INVOCATION:
 * status = oiMoveMode (par);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) par  (ASSEMBLY_CONTROL_RECORD *) assemblyControl record structure.
 *
 * FUNCTION VALUE:
 * (long) function return status.
 *
 * PURPOSE:
 * Move the probe to a new position
 *
 * DESCRIPTION:
 * Execute a MODE of MOVE.
 *
 * EXTERNAL VARIABLES:
 *
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static long oiMoveMode 
(
    ASSEMBLY_CONTROL_RECORD *par        /* assemblyControl record structure */
)
{
    DEBUG(DAR_MSG_FULL, "<%ld> %s:oiMoveMode: entry%c\n", ' ');

    /*
     *  Mode field has already been loaded so just execute the command.
     */

    return (oiExecuteCommand (par));
}




/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * oiProcessFault
 *
 * INVOCATION:
 * status = oiProcessFault (par); 
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) par  (ASSEMBLY_CONTROL_RECORD *) Pointer to assemblyControl record
 *
 * FUNCTION VALUE:
 * (long) returned function status.
 *
 * PURPOSE:
 * Kills the current process in response to an internal interlock
 *
 * DESCRIPTION:
 * If the record is busy processing a command, abort it immediately by 
 * calling oiCancelCommand().
 *
 * Otherwise there is nothing for device support to do so simply return.
 * 
 * EXTERNAL VARIABLES:
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 * DEFICIENCIES:
 * 
 *-
 ************************************************************************
 */

static long oiProcessFault
(
    ASSEMBLY_CONTROL_RECORD *par        /* (in)  Ptr to ass. rec.           */
)
{
    OI_DEV_CONTROL_PRIVATE *pDevPvt;    /* Internal control structure       */
    long     status = DAR_S_SUCCESS;    /* Return function status.          */

    DEBUG(DAR_MSG_MAX, "<%ld> %s:oiProcessFault: entry%c\n", ' ');

    pDevPvt = (OI_DEV_CONTROL_PRIVATE *) assGetPrivateStruct (par);


    /*
     *   Kill any active processes immediately.
     */

    if (par->busy == DAR_BUSY_BUSY)
    {
        DEBUG(DAR_MSG_LOG, "<%ld> %s:oiProcessFault: Cancelling current command%c\n", ' ');
        semTake (pDevPvt->mutexSem, WAIT_FOREVER);
        SET_ERR_MSG ( "External interlock detected!" );
        semGive (pDevPvt->mutexSem);
        status = DAR_E_FLT_CLR;
        oiCancelCommand( par );
    } 

    return (status);
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * oiTestMode
 *
 * INVOCATION:
 * status = oiTestMode (par);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) par  (ASSEMBLY_CONTROL_RECORD *) assemblyControl record structure.
 *
 * FUNCTION VALUE:
 * (long) function status return.
 *
 * PURPOSE:
 * Test the base and pickoff stages to ensure that they are functional
 *
 * DESCRIPTION:
 * o  Check the integrity of the device private structures by looking
 *    for NULL pointers and missing "MAGIC" values.
 * o  Display the private structure in MAX debug mode.
 * o  Check that position (POSn), velocity (VELn), mode (MODn), 
 *    directive (ODRn), and acknowledge (ACKn) links for both
 *    deviceControl records are NOT CONSTANT.
 * o  Execute a MODE of TEST that has already been loaded into 
 *    the base and pickoff stages by calling oiExecuteCommand().
 *
 * EXTERNAL VARIABLES:
 *
 *
 * PRIOR REQUIREMENTS:
 * TEST must have been written to the base and pickoff stage devicControl
 * record MODE fields.
 *
 * SEE ALSO:
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static long oiTestMode 
(
    ASSEMBLY_CONTROL_RECORD *par        /* assemblyControl record structure */
)
{
    OI_DEV_CONTROL_PRIVATE *pDevPvt;    /* Internal control structure       */
    long     status = DAR_S_SUCCESS;    /* Return function status.          */

    DEBUG(DAR_MSG_FULL, "<%ld> %s:oiTestMode: entry%c\n", ' ');

    pDevPvt = (OI_DEV_CONTROL_PRIVATE *) assGetPrivateStruct (par);

    /*
     *  Check the integrity of the device private structures by looking
     *  for NULL pointers and missing "MAGIC" values.
     */

    if ( pDevPvt == NULL )
    {
        DEBUG(DAR_MSG_FATAL, 
               "<%ld> %s:oiTestMode: Bad device private data structure%c\n", ' ' );
        assDisplayPrivateStruct( par );

        status = DAR_E_DEV_PTR;

        semTake (pDevPvt->mutexSem, WAIT_FOREVER);
        SET_ERR_MSG ("Bad internal data structures");
        assCommandFinish (par, status, pDevPvt->errorMessage);
        CLEAR_ERR_MSG;
        semGive (pDevPvt->mutexSem);

        return (oiCancelCommand (par));
    }
    else if ( pDevPvt->magic != OI_MAGIC )
    {
        DEBUG(DAR_MSG_FATAL, 
               "<%ld> %s:oiTestMode: No magic value seen in data structure%c\n",
               ' ' );

        status = DAR_E_DEV_PTR;

        semTake (pDevPvt->mutexSem, WAIT_FOREVER);
        SET_ERR_MSG( "No magic value seen in data structure");
        assCommandFinish (par, status, pDevPvt->errorMessage);
        CLEAR_ERR_MSG;
        semGive (pDevPvt->mutexSem);

        return (status);
    }
    else if ( par->dbug >= DAR_MSG_MAX )
    {
        /* Display the private structure in MAX debug mode. */

        assDisplayPrivateStruct( par );
    }


    /*
     *  Check that the NUM_DEV definition agrees with the number
     *  of deviceControl records found.
     */

    if ( NUM_DEV != par->nmdv )
    {
        DEBUG(DAR_MSG_FATAL, 
               "<%ld> %s:oiTestMode: Number of deviceControl records found disagrees with number specified%c\n", ' ' );

        status = DAR_E_DEVICE;

        semTake (pDevPvt->mutexSem, WAIT_FOREVER);
        SET_ERR_MSG( "NMDV not equal to NUM_DEV");
        assCommandFinish (par, status, pDevPvt->errorMessage);
        CLEAR_ERR_MSG;
        semGive (pDevPvt->mutexSem);

        return (status);
    }


    /*
     *  Check that all position, mode, directive, ack links
     *  are NOT CONSTANT.
     */

    if ( par->pos1.type == CONSTANT || par->ack1.type == CONSTANT || 
    	 par->mod1.type == CONSTANT || par->odr1.type == CONSTANT ||
	 par->vel1.type == CONSTANT )
    {
	/*
	 *  Check the Base stage links.
	 */

	DEBUG(DAR_MSG_FATAL, 
               "<%ld> %s:oiTestMode: Base stage deviceControl record links not connected.%c\n", ' ' );

        status = DAR_E_DEVICE;

        semTake (pDevPvt->mutexSem, WAIT_FOREVER);
	SET_ERR_MSG ( "Base stage links not connected" );
        assCommandFinish (par, status, pDevPvt->errorMessage);
        CLEAR_ERR_MSG;
        semGive (pDevPvt->mutexSem);

        return (status);
    }
    else if ( par->pos2.type == CONSTANT || par->ack2.type == CONSTANT || 
    	      par->mod2.type == CONSTANT || par->odr2.type == CONSTANT ||
	      par->vel2.type == CONSTANT )
    {
	/*
	 *  Check the Pickoff stage links.
	 */

	DEBUG(DAR_MSG_FATAL, 
               "<%ld> %s:oiTestMode: Pickoff stage deviceControl record links not connected.%c\n", ' ' );

        status = DAR_E_DEVICE;

        semTake (pDevPvt->mutexSem, WAIT_FOREVER);
	SET_ERR_MSG ( "Pickoff stage links not connected" );
        assCommandFinish (par, status, pDevPvt->errorMessage);
        CLEAR_ERR_MSG;
        semGive (pDevPvt->mutexSem);

        return (status);
    }




    /*
     *  Mode field has already been loaded so just execute the command.
     */

    return (oiExecuteCommand (par));
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * oiTrackMode
 *
 * INVOCATION:
 * status = oiTrackMode (par);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) par  (ASSEMBLY_CONTROL_RECORD *) Pointer to device support structure.
 *
 * FUNCTION VALUE:
 * (long) function status return.
 *
 * PURPOSE:
 * Execute a MODE of TRACK
 *
 * DESCRIPTION:
 * Move to the position that has already been loaded into the base
 * and pickoff stages.  Note that no limit checking of any sort is
 * done in tracking mode since there is no way to tell the tracking
 * source that it is making an invalid move from this level.
 *
 * EXTERNAL VARIABLES:
 *
 *
 * PRIOR REQUIREMENTS:
 * TRACK must have been written to the base and pickoff stage devicControl
 * record MODE fields.  The base and pickoff target positions and velocities
 * must have been loaded into these records as well.
 *
 * Execute a MODE of TRACK.
 *
 * EXTERNAL VARIABLES:
 *
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static long oiTrackMode 
(
    ASSEMBLY_CONTROL_RECORD *par        /* assemblyControl record structure */
)
{
    DEBUG(DAR_MSG_FULL, "<%ld> %s:oiTrackMode: entry%c\n", ' ');

    /*
     *  Mode, position and velocity fields have already been loaded so 
     *  just execute the command.
     */

    return (oiExecuteCommand (par));
}

/*
 ************************************************************************
 *+
 * FUNCTION NAME:
 * oiUpdateMode
 *
 * INVOCATION:
 * status = oiUpdateMode (par);
 *
 * PARAMETERS: (">" input, "!" modified, "<" output)
 * (>) par  (ASSEMBLY_CONTROL_RECORD *) assemblyControl record structure.
 *
 * FUNCTION VALUE:
 * (long) success code.
 *
 * PURPOSE:
 * Accept update requests without doing anything
 *
 * DESCRIPTION:
 * The OIWFS does not have any removable devices to update so simply
 * indicate that the command completed successfully.  The UPDATE mode is
 * rejected so this should never be called anyway.
 *
 * EXTERNAL VARIABLES:
 *
 *
 * PRIOR REQUIREMENTS:
 * None.
 *
 * SEE ALSO:
 *
 * DEFICIENCIES:
 * None.
 *-
 ************************************************************************
 */

static long oiUpdateMode 
(
    ASSEMBLY_CONTROL_RECORD *par        /* assemblyControl record structure */
)
{
    long     status = DAR_S_SUCCESS;    /* Returned function status.    */


    DEBUG(DAR_MSG_FULL, "<%ld> %s:oiUpdateMode: doing nothing%c\n", ' ');


    /*
     *   Nothing to update so tell record that command has completed
     *   successfully.
     */

    assCommandFinish( par, status, NULL );

    return (status);
}

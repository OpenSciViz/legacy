/* 
 ************************************************************************
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 *
 * (c) 1998.				(c) 1998
 * National Research Council		Conseil national de recherches
 * Ottawa, Canada, K1A 0R6 		Ottawa, Canada, K1A 0R6
 * All rights reserved			Tous droits reserves
 * 					
 * NRC disclaims any warranties,	Le CNRC denie toute garantie
 * expressed, implied, or statu-	enoncee, implicite ou legale,
 * tory, of any kind with respect	de quelque nature que se soit,
 * to the software, including		concernant le logiciel, y com-
 * without limitation any war-		pris sans restriction toute
 * ranty of merchantability or		garantie de valeur marchande
 * fitness for a particular pur-	ou de pertinence pour un usage
 * pose.  NRC shall not be liable	particulier.  Le CNRC ne
 * in any event for any damages,	pourra en aucun cas etre tenu
 * whether direct or indirect,		responsable de tout dommage,
 * special or general, consequen-	direct ou indirect, particul-
 * tial or incidental, arising		ier ou general, accessoire ou
 * from the use of the software.	fortuit, resultant de l'utili-
 * 					sation du logiciel.
 *
 ************************************************************************
 *
 * FILENAME
 * devBc635.h
 *
 * PURPOSE:
 * This is the include file for the use of the high-speed
 * Bancomm bc635VME board, written due to the requirements of Altair
 *
 *INDENT-OFF*
 * $Log: devBc635.h,v $
 * Revision 0.1  2005/07/01 17:48:40  drashkin
 * *** empty log message ***
 *
 * Initial revision
 *
 *INDENT-ON*
 *
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 ************************************************************************
*/

#ifndef BC635_H_INCLUDED
#define	BC635_H_INCLUDED

/* 
 * Defines 
 */

#define BC635_BASE_ADDR	((void *)0x4000)	/* in VME user short address space */


/* 
 * time sources and return values 
 */

#define BC635_MEM_MAP_ERROR  -4
#define BC635_INIT_ERROR     -3
#define BC635_OFFSET_ERROR   -2
#define	BC635_INVALID_SRC    -1
#define	BC635_SOURCE_HW       0
#define	BC635_SOURCE_SW       1


/* 
 * function prototypes 
 */

int     bc635Init(int timeSourceFlag, double *timeOffsetP);
double  bc635RawTime(void);
int     bc635Test(int timeSourceFlag, int numIterations,
                            double *averagePeriod);

#endif /* BC635_H_INCLUDED */

/*
 ************************************************************************
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 *
 * (c) 2005                         (c) 2005
 * National Research Council        Conseil national de recherches
 * Ottawa, Canada, K1A 0R6          Ottawa, Canada, K1A 0R6
 * All rights reserved              Tous droits reserves
 * 					
 * NRC disclaims any warranties,    Le CNRC denie toute garantie
 * expressed, implied, or statu-    enoncee, implicite ou legale,
 * tory, of any kind with respect   de quelque nature que se soit,
 * to the software, including       concernant le logiciel, y com-
 * without limitation any war-      pris sans restriction toute
 * ranty of merchantability or      garantie de valeur marchande
 * fitness for a particular pur-    ou de pertinence pour un usage
 * pose.  NRC shall not be liable   particulier.  Le CNRC ne
 * in any event for any damages,    pourra en aucun cas etre tenu
 * whether direct or indirect,      responsable de tout dommage,
 * special or general, consequen-   direct ou indirect, particul-
 * tial or incidental, arising      ier ou general, accessoire ou
 * from the use of the software.    fortuit, resultant de l'utili-
 *                                  sation du logiciel.
 *
 ************************************************************************
 *
 * FILENAME: testVmeCardDrivers.c
 *
 * PURPOSE: Test the device drivers without record level.
 *
 * FUNCTIONS:
 * omsBacklash 
 * omsBacklash2
 * omsDoLots 
 * omsDoLots2
 * omsDoLots3
 * omsGetCardTypes
 * omsGetLimits 
 * omsGetPoints
 * omsGetStatus 
 * omsOff 
 * omsOn 
 * omsOutLowerLimit 
 * omsOutUpperLimit 
 * omsPos 
 * omsPrintPoints
 * omsRead
 * omsServoSet 
 * omsSetup 
 * omsVmeTestTask 
 * omsWrite 
 *
 * xy240ReadIn
 * xy240ReadOut
 * xy240WriteOut
 *
 * xy566Read
 * xy566Report
 * xy566Stats
 *
 *INDENT-OFF*
 * $Log: testVmeCardDrivers.c,v $
 * Revision 0.1  2005/07/01 17:48:40  drashkin
 * *** empty log message ***
 *
 *
 * Initial revision
 *
 *INDENT-ON*
 *
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 ************************************************************************
*/

/*
 *  Local Defines
 */

#include <stdio.h>
#include <stdlib.h>
#include <taskLib.h>
#include <sysLib.h>

#include <vxWorks.h>
#include <stdio.h>
#include <ioLib.h>
#include <tickLib.h>
#include <sioLib.h>
#include <module_types.h>

#include <drvOmsVme58.h>            /* OMS VME58 card drivers               */
/*#include "devBc635.h" */          /* Bacomm bc635 time code access        */
#include "devHeidenhain.h"          /* Heidenhain measurement system access */
#include <math.h>

/*
 *  Local Types
 */


#define OMS_SCAN_TASK_PRIORITY     80           /* OMS scan task priority  */
#define OMS_SCAN_TASK_STACK        0x1000       /* 4096 byte stack         */
#define OMS_SCAN_TASK_OPTIONS      0x0          /* no floating point stuff */

#define masks(K) ((1<<K))

static int omsTaskRunning = FALSE;
static long lastEncoder, encoder;

/*
 * Dummy Function Prototypes
 */

long ai_xy566_io_report(char);
long ai_xy566_driver(short, short, unsigned int, unsigned short *);
long xy240_bi_driver(short , unsigned int, int *);
long xy240_bo_read(short , unsigned int, int *);
long xy240_bo_driver(short , int, int);
long omsGetPoints ( int , int, char *, int );
long omsPrintPoints ( int , int );
long omsPos (int, int);
long omsRead ( int);
long omsWrite (int, char *);

     
/*
 *  Function Prototypes *** ALL LOCAL FUNCTIONS TO BE PROTOTYPED ***
 */

static int omsVmeTestTask (int,int,int,int,int,int,int,int,int,int);


/*
 *  Data Structures
 */


/*
 *  Macros
 */


/*
 ************************************************************************
 *+
 * FUNCTION: omsBacklash
 *
 * RETURNS: long [function status]
 *
 * DESCRIPTION: 
 * Do a set of backlash test moves on one motor.
 *
 *-
 ************************************************************************
 */

long omsBacklash 
(
    int card, 
    int axis,
    int haxis
)
{
    long status;
    int i, status2;
    long target;
    char scratch[30];
    long position, encoder;
    float heiden;

    /*
     * First be sure to approach 0 from the positive direction.
     */

    printf("\nTesting backlash for axis %d\n",axis);

    status2 = heidenInit(1);

    status = drvOmsVmeWriteMotor (card, axis, "HN VL8500 VB300 AC10000 MA5000 GO ");
    taskDelay(10);
    status = drvOmsVmeWriteMotor (card, axis, "HN MA0 GO ");
    taskDelay(360);
    status = drvOmsVmeWriteMotor (card, axis, "LP0 ");
    status = drvOmsVmeMotorPosition (card, axis, &position, &encoder);
    if (status)
      {
        printf("Position read failed!\n");
        return status;
      }
    heiden = heidenRdOne(haxis);
    printf("Position: \tEncoder: \tHeiden: \tDiff(counts): \tDiff(mm): \n"); 
    printf("%ld\t\t%ld\t\t%ld\t\t%ld\t\t%f\n",position, encoder, 
           (long)(heiden*19605.921), (long)((heiden*19605.921)-encoder),
           heiden-(encoder/19605.921));

    /*
     * Do a loop in which we step -'ve 5000 and read position 20 times
     * then move back in the same steps reading position.
     * Add some delays to be sure that the motor has come to a complete
     * stop before reading position.
     */
    target = 0;
    for (i=0; i<40; i++)
      {
        target -= 2500;
        sprintf(scratch,"HN MA%ld GO ",target);
        status = drvOmsVmeWriteMotor (card, axis, scratch);
        if (status)
          {
            printf("Write failed!\n");
            return status;
          }
        taskDelay(100);

        status = drvOmsVmeMotorPosition (card, axis, &position, &encoder);
        if (status)
          {
            printf("Position read failed!\n");
            return status;
          }
        heiden = heidenRdOne(haxis);
        printf("%ld\t\t%ld\t\t%ld\t\t%ld\t\t%f\n",position, encoder, 
               (long)(heiden*19605.921), (long)((heiden*19605.921)-encoder),
               heiden-(encoder/19605.921));
      }

    for (i=0; i<40; i++)
      {
        target += 2500;
        sprintf(scratch,"HN MA%ld GO ",target);
        status = drvOmsVmeWriteMotor (card, axis, scratch);
        if (status)
          {
            printf("Write failed!\n");
            return status;
          }
        taskDelay(100);
        status = drvOmsVmeMotorPosition (card, axis, &position, &encoder);
        if (status)
          {
            printf("Position read failed!\n");
            return status;
          }
        heiden = heidenRdOne(haxis);
        printf("%ld\t\t%ld\t\t%ld\t\t%ld\t\t%f\n",position, encoder, 
               (long)(heiden*19605.921), (long)((heiden*19605.921)-encoder),
               heiden-(encoder/19605.921));
      }
    printf("\nBacklash testing done.\n");
    return status;
}

/*
 ************************************************************************
 *+
 * FUNCTION: omsBacklash2
 *
 * RETURNS: long [function status]
 *
 * DESCRIPTION: 
 * Do a set of backlash test moves on one motor centered around a given point.
 *
 *-
 ************************************************************************
 */

long omsBacklash2
(
    int card, 
    int axis,
    int haxis,
    int backlash,
    float center
)
{
    long status;
    int i, j, status2;
    long target, targetup, targetdown, b_comp;
    long delay;
    char scratch[60];
    long position, encoder;
    float heiden;
    /* double mres = 19605.921; */
    double mres = 219757.1666;


    printf("\nTesting backlash for axis %d using backlash compensation of %d microns\n",axis,backlash);

    b_comp = backlash*(mres/1000);
    printf("\nBacklash compensation is %ld counts\n",b_comp);
    printf("\nCentering around: %f\n",center);

    status2 = heidenInit(1);

    /* get starting position for time delay calc and backlash compensation*/
    status = drvOmsVmeMotorPosition (card, axis, &position, &encoder);
    if (status)
      {
        printf("Position read failed!\n");
        return status;
      }
    /* first move to center position (given in mm) */
    target = center * mres;
    if (position>target)
      {
        sprintf(scratch,"HN VL200000 VB10000 AC500000 MA%ld GO ",target-b_comp);
      }
    else 
      {
        sprintf(scratch,"HN VL200000 VB10000 AC500000 MA%ld GO ",target);
      }
    status = drvOmsVmeWriteMotor (card, axis, scratch);
    if (status)
      {
        printf("Write failed!\n");
        return status;
      }
    delay = (long)abs((center-(position/mres))*200);
    taskDelay(delay);

    status = drvOmsVmeMotorPosition (card, axis, &position, &encoder);
    if (status)
      {
        printf("Position read failed!\n");
        return status;
      }
    heiden = heidenRdOne(haxis);
    printf("\nTarget: \tPosition: \tEncoder: \tHeiden: \t\tDiff(counts): \tDiff(mm): \tDiff target:\n"); 
    printf("%.4f\t\t%ld  %.4f\t%ld\t\t%ld  %.4f\t\t%ld\t\t%f\t%f\n",target/mres, position, position/mres,
           encoder, (long)(heiden*mres),heiden, (long)((heiden*mres)-encoder),
           heiden-(encoder/mres), heiden-(target/mres));

    /*
     * Do a loop in which we move out and in 3 times in each direction 
     * using 7 different step sizes.
     * Add some delays to be sure that the motor has come to a complete
     * stop before reading position.
     */
    for (i=0; i<7; i++)
      {
        switch (i) 
          { 
          case 0:
            targetup = target + mres;
            targetdown = target - mres;
            delay = 200;
            break;
          case 1:
            targetup = target + (0.3 * mres);
            targetdown = target - (0.3 * mres);
            delay = 100;
            break;
          case 2:
            targetup = target + (0.1 * mres);
            targetdown = target - (0.1 * mres);
            delay = 75;
            break;
          case 3:
            targetup = target + (0.03 * mres);
            targetdown = target - (0.03 * mres);
            delay = 50;
            break;
          case 4:
            targetup = target + (0.01 * mres);
            targetdown = target - (0.01 * mres);
            delay = 50;
            break;
          case 5:
            targetup = target + (0.003 * mres);
            targetdown = target - (0.003 * mres);
            delay = 50;
            break;
          case 6:
            targetup = target + (0.001 * mres);
            targetdown = target - (0.001 * mres);
            delay = 50;
            break;
          default:
            break;
          }
        for (j=0; j<3; j++)
          {
            /* go up */
            sprintf(scratch,"HN MA%ld GO ",targetup);
            status = drvOmsVmeWriteMotor (card, axis, scratch);
            if (status)
              {
                printf("Write failed!\n");
                return status;
              }
            taskDelay(delay);
        
            status = drvOmsVmeMotorPosition (card, axis, &position, &encoder);
            if (status)
              {
                printf("Position read failed!\n");
                return status;
              }
            heiden = heidenRdOne(haxis);
            printf("%.4f\t\t%ld  %.4f\t%ld\t\t%ld  %.4f\t\t%ld\t\t%f\t%f\n", targetup/mres, position, 
                   position/mres, encoder, (long)(heiden*mres),heiden, (long)((heiden*mres)-encoder),
                   heiden-(encoder/mres), heiden-(targetup/mres));
            /* center */
            sprintf(scratch,"HN MA%ld GO ",target-b_comp);
            status = drvOmsVmeWriteMotor (card, axis, scratch);
            if (status)
              {
                printf("Write failed!\n");
                return status;
              }
            taskDelay(delay);
        
            status = drvOmsVmeMotorPosition (card, axis, &position, &encoder);
            if (status)
              {
                printf("Position read failed!\n");
                return status;
              }
            heiden = heidenRdOne(haxis);
            printf("%.4f\t\t%ld  %.4f\t%ld\t\t%ld  %.4f\t\t%ld\t\t%f\t%f\n", target/mres, position, 
                   position/mres, encoder, (long)(heiden*mres),heiden, (long)((heiden*mres)-encoder),
                   heiden-(encoder/mres), heiden-(target/mres));
            /* go down */
            sprintf(scratch,"HN MA%ld GO ",targetdown-b_comp);
            status = drvOmsVmeWriteMotor (card, axis, scratch);
            if (status)
              {
                printf("Write failed!\n");
                return status;
              }
            taskDelay(delay);
        
            status = drvOmsVmeMotorPosition (card, axis, &position, &encoder);
            if (status)
              {
                printf("Position read failed!\n");
                return status;
              }
            heiden = heidenRdOne(haxis);
            printf("%.4f\t\t%ld  %.4f\t%ld\t\t%ld  %.4f\t\t%ld\t\t%f\t%f\n", targetdown/mres, position, 
                   position/mres, encoder, (long)(heiden*mres),heiden, (long)((heiden*mres)-encoder),
                   heiden-(encoder/mres), heiden-(targetdown/mres));
            /* center */
            sprintf(scratch,"HN MA%ld GO ",target);
            status = drvOmsVmeWriteMotor (card, axis, scratch);
            if (status)
              {
                printf("Write failed!\n");
                return status;
              }
            taskDelay(delay);
        
            status = drvOmsVmeMotorPosition (card, axis, &position, &encoder);
            if (status)
              {
                printf("Position read failed!\n");
                return status;
              }
            heiden = heidenRdOne(haxis);
            printf("%.4f\t\t%ld  %.4f\t%ld\t\t%ld  %.4f\t\t%ld\t\t%f\t%f\n", target/mres, position, 
                   position/mres, encoder, (long)(heiden*mres),heiden, (long)((heiden*mres)-encoder),
                   heiden-(encoder/mres), heiden-(target/mres));
          }
      }
    printf("\nBacklash testing done.\n");
    return status;
}

/*
 ************************************************************************
 *+
 * FUNCTION: omsDoLots
 *
 * RETURNS: long [function status]
 *
 * DESCRIPTION: 
 * 
 *
 * PRIOR REQUIREMENTS:
 * 
 *-
 ************************************************************************
 */

long omsDoLots 
(
    void
)
{
    long status;
    int i;

    status = omsPos( 0, 0);
    status = omsPos( 0, 1);
    status = omsPos( 0, 2);
    status = omsPos( 0, 3);

    for ( i=0; i<=10; i++ )
    {
	/*
	 * Do a move to each motor, then check position on each motor
	 * then delay.
	 */
	status = drvOmsVmeWriteCard (0, "AT MR-300 GO ");
	status = drvOmsVmeWriteCard (0, "AU MR-300 GO ");
	status = drvOmsVmeWriteCard (0, "AX MR-300 GO ");
	status = drvOmsVmeWriteCard (0, "AY MR-300 GO ");
	status = drvOmsVmeWriteCard (0, "AZ MR-300 GO ");

	taskDelay(3);

	status = omsPos( 0, 0);
	status = omsPos( 0, 1);
	status = omsPos( 0, 2);
	status = omsPos( 0, 3);
    }


    return status;
}

/*
 ************************************************************************
 *+
 * FUNCTION: omsDoLots2
 *
 * RETURNS: long [function status]
 *
 * DESCRIPTION: 
 * 
 *
 * PRIOR REQUIREMENTS:
 * 
 *-
 ************************************************************************
 */

/* KP30 KI40 KD50 VB300 VL8500 AC10000 HD1 */
long omsDoLots2 
(
    char *buffer
)
{
    long status;

    int i;

    printf("Motor 1: ");
    status = omsPos( 0, 0);
    printf("Motor 2: ");
    status = omsPos( 0, 1);
    printf("Motor 3: ");
    status = omsPos( 0, 2);
    printf("Motor 4: ");
    status = omsPos( 0, 3);
    printf("Motor 5: ");
    status = omsPos( 0, 4);

    for ( i=0; i<=10; i++ )
    {
	/*
	 * Do a move to each motor, then check position on each motor
	 * then delay 1 sec
	 */

	status = drvOmsVmeWriteCard (0, "AT ST " );
	status = drvOmsVmeWriteCard (0, "AT " );
	status = drvOmsVmeWriteCard (0, buffer );
	status = drvOmsVmeWriteCard (0, "AT MR-300 GO ");

	status = drvOmsVmeWriteCard (0, "AU ST " );
	status = drvOmsVmeWriteCard (0, "AU " );
	status = drvOmsVmeWriteCard (0, buffer );
	status = drvOmsVmeWriteCard (0, "AU MR-300 GO ");

	status = drvOmsVmeWriteCard (0, "AX ST " );
	status = drvOmsVmeWriteCard (0, "AX " );
	status = drvOmsVmeWriteCard (0, buffer );
	status = drvOmsVmeWriteCard (0, "AX MR-300 GO ");

	status = drvOmsVmeWriteCard (0, "AY ST " );
	status = drvOmsVmeWriteCard (0, "AY " );
	status = drvOmsVmeWriteCard (0, buffer );
	status = drvOmsVmeWriteCard (0, "AY MR-300 GO ");

	status = drvOmsVmeWriteCard (0, "AZ ST " );
	status = drvOmsVmeWriteCard (0, "AZ " );
	status = drvOmsVmeWriteCard (0, buffer );
	status = drvOmsVmeWriteCard (0, "AZ MR-300 GO ");

	taskDelay(3);

        printf("Motor 1: ");
	status = omsPos( 0, 0);
        printf("Motor 2: ");
	status = omsPos( 0, 1);
        printf("Motor 3: ");
	status = omsPos( 0, 2);
        printf("Motor 4: ");
	status = omsPos( 0, 3);
        printf("Motor 5: ");
	status = omsPos( 0, 4);

    }


    return status;
}


/*
 ************************************************************************
 *+
 * FUNCTION: omsDoLots3
 *
 * RETURNS: long [function status]
 *
 * DESCRIPTION: 
 * 
 *
 * PRIOR REQUIREMENTS:
 * 
 *-
 ************************************************************************
 */

long omsDoLots3 
(
    char *buffer
)
{
    long status;
    char buffer2[30];

    int i;

    printf("Motor 1: ");
    status = omsPos( 0, 0);
    printf("Motor 2: ");
    status = omsPos( 0, 1);
    printf("Motor 3: ");
    status = omsPos( 0, 2);
    printf("Motor 4: ");
    status = omsPos( 0, 3);
    printf("Motor 5: ");
    status = omsPos( 0, 4);

    for ( i=0; i<=10; i++ )
    {
	/*
	 * Do a move to each motor, then check position on each motor
	 * then delay 1 sec
	 */

	status = drvOmsVmeWriteCard (0, "AT " );
	status = drvOmsVmeWriteCard (0, buffer );
        sprintf(buffer2,"AT MA%d GO",i*300);
	status = drvOmsVmeWriteCard (0, buffer2);

	status = drvOmsVmeWriteCard (0, "AU " );
	status = drvOmsVmeWriteCard (0, buffer );
        sprintf(buffer2,"AU MA%d GO",i*300);
	status = drvOmsVmeWriteCard (0, buffer2);

	status = drvOmsVmeWriteCard (0, "AX " );
	status = drvOmsVmeWriteCard (0, buffer );
        sprintf(buffer2,"AX MA%d GO",i*300);
	status = drvOmsVmeWriteCard (0, buffer2);

	status = drvOmsVmeWriteCard (0, "AY " );
	status = drvOmsVmeWriteCard (0, buffer );
        sprintf(buffer2,"AY MA%d GO",i*300);
	status = drvOmsVmeWriteCard (0, buffer2);

	status = drvOmsVmeWriteCard (0, "AZ " );
	status = drvOmsVmeWriteCard (0, buffer );
        sprintf(buffer2,"AZ MA%d GO",i*300);
	status = drvOmsVmeWriteCard (0, buffer2);


	taskDelay(3);

        printf("Motor 1: ");
	status = omsPos( 0, 0);
        printf("Motor 2: ");
	status = omsPos( 0, 1);
        printf("Motor 3: ");
	status = omsPos( 0, 2);
        printf("Motor 4: ");
	status = omsPos( 0, 3);
        printf("Motor 5: ");
	status = omsPos( 0, 4);

    }


    return status;
}


/*
 ************************************************************************
 *+
 * FUNCTION: omsGetCardTypes
 *
 * RETURNS: int [number of cards found]
 *
 * DESCRIPTION: 
 * Gets the response from a query axis "WY" to determine first that
 * a card exists then the card type.  Starts at card 0 and stops 
 * when the check fails.
 *
 * PRIOR REQUIREMENTS:
 * OMS VME58 driver must be initialized.  
 * Can be done with drvOmsVmeInit function.
 *-
 ************************************************************************
 */

long omsGetCardTypes 
(
    void
)
{
    char buffer[256];
    long status;
    int numCard = 0;
    int cardExists = 1;

    while ((cardExists == 1) && (numCard < DRV_OMS_VME_MAX_CARDS) )
    {
        cardExists = 0;
        status = drvOmsVmeWriteCard (numCard, "WY");
        if (status == 0)
        {
            taskDelay(3);
            status = drvOmsVmeReadCard (numCard, buffer);
            if (status > 0)
            {
                printf ("\nCard: %d type: OMS %.5s%s, version: %.4s\n", 
                        numCard, &buffer[0], &buffer[14],&buffer[10]);
                cardExists = 1;
                numCard++;
            }
            else if (status <0)
            {
                printf ("Card: %d type: INVALID\n",numCard);
            }
            else if (status == 0)
            {
                printf ("\nCard: %d No Response\n",numCard);
            }
        }
        else
        {
            printf ("\nCard: %d query failed\n",numCard);
        }
    }
    printf ("\nFound %d OMS card(s).\n",numCard);
    return numCard;
}

/*
 ************************************************************************
 *+
 * FUNCTION: omsGetLimits
 *
 * RETURNS: long [function status]
 *
 * DESCRIPTION: 
 * Query current motor state to retrieve limits.
 *
 * PRIOR REQUIREMENTS:
 * OMS VME58 driver must be initialized.  
 * Can be done with drvOmsVmeInit function.
 *-
 ************************************************************************
 */

long omsGetLimits 
(
    int card, 
    int axis
)
{
    int hlim, llim, hmsw, done;
    long status;

    status = drvOmsVmeMotorState (card, axis, &llim, &hlim, &hmsw, &done);
    if (status < 0)
    {
        printf ("omsState: Motor state unavailable for Card %d, Axis %d\n", card, axis);
    }
    else
    {
        printf ("omsState: hlim = %d, llim = %d\n", hlim, llim);
    }

    return status;
}


/*
 ************************************************************************
 *+
 * FUNCTION: omsGetPoints
 *
 *  example: omsGetPoints( 0, 7, "AS MA-1000 GO HN", 140)
 * 
 *
 * RETURNS: long [function status]
 *
 * DESCRIPTION: 
 * This will execute the *buffer passed in, and then start getting the
 * current position and encoder values and write
 * these to the file: /aoBase/develop/aoDeviceControl/test
 *  /dataPoints/omsPoints.dat for the number of ticks specified.
 *
 *-
 ************************************************************************
 */

long omsGetPoints 
(
    int card, 
    int axis,
    char *buffer,
    int numTicks

)
{
    long position, encoder;
    long status;
    int timeRightNow;
    char fileName[128];


    FILE *fp;


    sprintf(fileName, "/aoBase/develop/aoDeviceControl/test/dataPoints/omsPoints.dat");
    if ( (fp = fopen( fileName, "w" ) ) == (FILE *)NULL )
    {
	printf("Failed to open file called omsPoints.dat.\n");
	return(-1);
    }
    timeRightNow = (int)tickGet(); 

    /*
     *  Start move of Device.
     */

    if ( (status = omsWrite( card, buffer)) != 0 )
    {
	printf("omsWrite failed.\n");
	return(-1);
    }

    while ( 1)
    {

	status = drvOmsVmeMotorPosition (card, axis, &position, &encoder);
	if ( status == 0 )
	{
	    fprintf (fp, "%d\t%ld\t%ld\n", ((int)tickGet()-timeRightNow), 
		    position, encoder);
	}

	/*
	 *  Keep taking positions for at least "numTick" ticks.
	 */

	if ( ((int)tickGet()-timeRightNow) >= numTicks ) break;
    }
    fclose(fp);
    return status;
}

/*
 ************************************************************************
 *+
 * FUNCTION: omsGetStatus
 *
 * RETURNS: long [function status]
 *
 * DESCRIPTION: 
 * Gets the response from a query axis "QA"
 *
 * PRIOR REQUIREMENTS:
 * OMS VME58 driver must be initialized.  
 * Can be done with drvOmsVmeInit function.
 *-
 ************************************************************************
 */

long omsGetStatus 
(
    int card, 
    int axis
)
{
    long status;
    status = drvOmsVmeWriteMotor (card, axis, "QA ");
    if (!status)
    {
    	taskDelay(3);
        status = omsRead(card);
    }
    if (status < 0)
    {
        printf("omsGetStatus: FAILED status=%ld\n",status);
    }
    return status;
}


/*
 ************************************************************************
 *+
 * FUNCTION: omsOff
 *
 * RETURNS: long [function status]
 *
 * DESCRIPTION: 
 * Turn power on (not off)
 *
 * PRIOR REQUIREMENTS:
 * OMS VME58 driver must be initialized.  
 * Can be done with drvOmsVmeInit function.
 *-
 ************************************************************************
 */

long omsOff 
(
    int card,
    int axis
)
{
    long status;

    status = drvOmsVmeWriteMotor (card, axis, "AN ");
    if (status < 0)
    {
	printf ("omsOff: FAILED, status=%ld \n", status );
    }
    return status;
}

/*
 ************************************************************************
 *+
 * FUNCTION: omsOn
 *
 * RETURNS: long [function status]
 *
 * DESCRIPTION: 
 * Turn power off (not on).
 *
 * PRIOR REQUIREMENTS:
 * OMS VME58 driver must be initialized.  
 * Can be done with drvOmsVmeInit function.
 *-
 ************************************************************************
 */

long omsOn 
(
    card,
    axis
)
{
    long status;

    status = drvOmsVmeWriteMotor (card, axis, "AF ");
    if (status < 0)
    {
	printf ("omsOn: FAILED, status=%ld \n", status );
    }
    return status;
}


/*
 ************************************************************************
 *+
 * FUNCTION: omsOutLowerLimit
 *
 * RETURNS: long [function status]
 *
 * DESCRIPTION: 
 * Get out of the lower limit
 *
 *-
 ************************************************************************
 */

long omsOutLowerLimit 
(
    int card 
)
{
    long status;

    status = drvOmsVmeWriteCard (card, "AX LF HN MR-40000 GO LN HN ");
    return status;
}

/*
 ************************************************************************
 *+
 * FUNCTION: omsOutUpperLimit
 *
 * RETURNS: long [function status]
 *
 * DESCRIPTION: 
 * Get card out of upper limit.
 *
 *-
 ************************************************************************
 */

long omsOutUpperLimit 
(
    int card 
)
{
    long status;

    status = drvOmsVmeWriteCard (card, "AX LF HN MR40000 GO LN HN ");
    return status;
}

/*
 ************************************************************************
 *+
 * FUNCTION: omsPos
 *
 * RETURNS: long [function status]
 *
 * DESCRIPTION: 
 * Get current position and encoder value.
 *
 * PRIOR REQUIREMENTS:
 * OMS VME58 driver must be initialized.  
 * Can be done with drvOmsVmeInit function.
 *-
 ************************************************************************
 */

long omsPos 
(
    int card, 
    int axis
)
{
    long position, encoder;
    long status;

    status = drvOmsVmeMotorPosition (card, axis, &position, &encoder);
    if ( status < 0 )
    {
	printf ("omsPos: FAILED, status=%ld \n", status );
    }
    else
    {
	printf ("omsPos: pos    = %ld, enc  = %ld\n", position, encoder);
    }
    return status;
}


/*
 ************************************************************************
 *+
 * FUNCTION: omsPrintPoints
 *
 * RETURNS: long [function status]
 *
 * DESCRIPTION: 
 * Printout the current position and encoder values and write to a file 
 * called "omsPoints", for 1000 ticks.
 *
 *-
 ************************************************************************
 */

long omsPrintPoints 
(
    int card, 
    int axis
)
{
    long position, encoder;
    long status;
    int i;
    int timeRightNow;

    FILE *fp;

    if ( (fp = fopen( "omsPoints", "w" ) ) == (FILE *)NULL )
    {
	printf("Failed to open file called omsPoints.\n");
	return(-1);
    }
    timeRightNow = (int)tickGet(); 

    for ( i=0; i<= 1000; i++ )
    {

	status = drvOmsVmeMotorPosition (card, axis, &position, &encoder);
	if ( status == 0 )
	{
	    fprintf (fp, "%d\t%ld\t%ld\n", ((int)tickGet()-timeRightNow), 
		    position, encoder);
	}
    }
    fclose(fp);
    return status;
}

/*
 ************************************************************************
 *+
 * FUNCTION: omsRead
 *
 * RETURNS: long [function status]
 *
 * DESCRIPTION: 
 * Read from the card, if there is anything
 *
 * PRIOR REQUIREMENTS:
 * OMS VME58 driver must be initialized.  
 * Can be done with drvOmsVmeInit function.
 *-
 ************************************************************************
 */

long omsRead
(
    int card
)
{
    char buffer[256];
    long status;

    status = drvOmsVmeReadCard (card, buffer);
    if (status < 0)
    {
        printf ("omsRead: FAILED status=%ld\n", status);
    }
    else
    {
        printf ("omsRead: %s\n", buffer);
    }
    return status;
}


/*
 ************************************************************************
 *+
 * FUNCTION: omsServoSet
 *
 * RETURNS: long [function status]
 *
 * DESCRIPTION: 
 * Setup up coefficients, etc for the drive.
 *
 * PRIOR REQUIREMENTS:
 * OMS VME58 driver must be initialized.  
 * Can be done with drvOmsVmeInit function.
 *-
 ************************************************************************
 */

long omsServoSet 
(
    long proportional, 
    long derivitive, 
    long integral, 
    long velocity, 
    long acceleration
)
{
    int card = 0;
    int axis = 0;
    char buf[256];
    long status;
    sprintf (buf, "KP%ld KD%ld KI%ld VL%ld AC%ld ",
                  proportional, 
                  derivitive, 
                  integral, 
                  velocity, 
                  acceleration);
    status = drvOmsVmeWriteMotor (card, axis, buf);
    if (!status)
        printf ("omsServoSet: proportional = %ld\n integral = %ld\n derivitive = %ld\n velocity = %ld\n acceleration = %ld\n ", proportional, derivitive, integral, velocity, acceleration);
    else printf ("omsServoSet: could not set PID, velo & accel.!\n");
    return status;
}

/*
 ************************************************************************
 *+
 * FUNCTION: omsSetup
 *
 * RETURNS: long [function status]
 *
 * DESCRIPTION: 
 * Setup the oms card automatically.
 *
 *-
 ************************************************************************
 */

long omsSetup 
(
    void
)
{
    int card=0;
    long status;
    int omsTaskId;
printf("\nWriting AX KK2 VL5000 AC50000 HN\n");
    status = drvOmsVmeWriteCard (card, "AX KK2 VL5000 AC50000 HN");

    if (!status)
    {
/*printf("Writing AX LP0\n");*/
        status = drvOmsVmeWriteCard (card, "AX LP0 ");
    }

    if (!status)
    {
        status = drvOmsVmeWriteCard (card, "AX AF");
/*printf("Writing AX AF\n");*/
    }

if ( status  != 0 )
{
printf("WARNING, Status != 0 \n");
}

    if (!omsTaskRunning)
    {
 
        omsTaskId = taskSpawn (
            "tOmsTest",                    /* task name */
            OMS_SCAN_TASK_PRIORITY,    /* priority */
            OMS_SCAN_TASK_OPTIONS,     /* options */
            OMS_SCAN_TASK_STACK,       /* stack size */
            omsVmeTestTask,                     /* task entry point */
            0,0,0,0,0,0,0,0,0,0);          /* invocation args 1 to 10 */

        omsTaskRunning = TRUE;
  
    }
printf("Spawned task\n");
    omsRead (card);
printf("Second read\n");
    omsRead (card);
    omsRead (card);

    return status;
}


/*
 ************************************************************************
 *+
 * FUNCTION: omsVmeTestTask
 *
 * RETURNS: long [function status]
 *
 * DESCRIPTION: 
 * Task to read out the current position and encoder value via polling.
 *
 *-
 ************************************************************************
 */

static int omsVmeTestTask 
(
    int a1, int a2, int a3, int a4, int a5, int a6,
    int a7, int a8, int a9, int a10
)
{
    long position;

    while (TRUE)
    {
        drvOmsVmeMotorPosition (0, 0, &position, &encoder);

        if (encoder != lastEncoder)
        {
            printf ("pos=%ld, en=%ld\n", position, encoder);
            lastEncoder = encoder;
        }

        taskDelay (30);
/*
        taskDelay (1);*/
    }

    return 0;
}

/*
 ************************************************************************
 *+
 * FUNCTION: omsWrite
 *
 * RETURNS: long [function status]
 *
 * DESCRIPTION: 
 * Write from the card, if there is anything
 *
 * PRIOR REQUIREMENTS:
 * OMS VME58 driver must be initialized.  
 * Can be done with drvOmsVmeInit function.
 *-
 ************************************************************************
 */

long omsWrite 
(
    int card, 
    char *buffer
)
{
    long status;

    status = drvOmsVmeWriteCard (card, buffer);
    if (status < 0)
    {
        printf("omsWrite: FAILED status=%ld\n",status);
    }

    return status;
}


/*
 ************************************************************************
 *+
 * FUNCTION: xy240ReadIn
 *
 * RETURNS: long [function status]
 *
 * DESCRIPTION: 
 * Read an input bit from a XYCOM 240 card.  Remember, in EPICS the 32
 * input bits are all on the JK1 connector. 
 *
 * PRIOR REQUIREMENTS:
 * XVME-240 card must be initialized.  Can be done with xy240_init function.
 * 
 * DEFICIENCIES:
 * Underlying code does not check if bit is valid. 
 *
 *-
 ************************************************************************
 */

long xy240ReadIn
(
    short int card,
    short int signal
)
{
    long status;
    int value;

    status = xy240_bi_driver(card, masks(signal), &value);
    if (status==0)
      {
        if (value != 0) value = 1;
        printf ("xy240ReadIn Card C%d Signal S%d: %x\n", card, signal, value);
      }
    else
      {
        printf ("xy240ReadIn: Error\n");
      }
    return status;
}

/*
 ************************************************************************
 *+
 * FUNCTION: xy240ReadOut
 *
 * RETURNS: long [function status]
 *
 * DESCRIPTION: 
 * Read an output bit from a XYCOM 240 card.  Remember, in EPICS the 32
 * output bits are all on the JK2 connector. 
 *
 * PRIOR REQUIREMENTS:
 * XVME-240 card must be initialized.  Can be done with xy240_init function.
 * 
 * DEFICIENCIES:
 * Underlying code does not check if bit is valid. 
 *
 *-
 ************************************************************************
 */

long xy240ReadOut
(
    short int card,
    short int signal
)
{
    long status;
    int value;

    status = xy240_bo_read(card, masks(signal), &value);
    if (status==0)
      {
        if (value != 0) value = 1;
        printf ("xy240ReadOut Card C%d Signal S%d: %x\n", card, signal, value);
      }
    else
      {
        printf ("xy240ReadOut: Error\n");
      }
    return status;
}


/*
 ************************************************************************
 *+
 * FUNCTION: xy240WriteOut
 *
 * RETURNS: long [function status]
 *
 * DESCRIPTION: 
 * Change an output bit on a XYCOM 240 card.  Remember, in EPICS the 32
 * output bits are all on the JK2 connector. 
 *
 * PRIOR REQUIREMENTS:
 * XVME-240 card must be initialized.  Can be done with xy240_init function.
 * 
 * DEFICIENCIES:
 * Underlying code does not check if bit is valid. 
 *-
 ************************************************************************
 */

long xy240WriteOut
(
    short int card,
    short int signal,
    unsigned long value
)
{
    long status;
    int new_value;

    status = xy240_bo_driver(card, value, masks(signal));
    if (status==0)
      {
        /* check value to be sure */
        status = xy240_bo_read(card, masks(signal), &new_value);
        if (new_value != 0) new_value = 1;
        printf ("xy240WriteOut: Card C%d Signal S%d set to %x\n", card, signal, new_value);
      }
    else
      {
        printf ("xy240WriteOut: Error\n");
      }
    return status;
}

/*
 ************************************************************************
 *+
 * FUNCTION: xy566Read
 *
 * RETURNS: long [function status]
 *
 * DESCRIPTION: 
 * Read a single channel from a XYCOM 566 SE card
 *
 *-
 ************************************************************************
 */

long xy566Read
(
    short int card,
    short channel
)
{
    long status;
    unsigned short pval;
    
    status = ai_xy566_driver(card, channel, 4, &pval );
    if (status!=0)
    {
        printf ("xy566Read: Error\n");
    }
    else
    {
        printf ("xy566Read: Card %d Chan %d: %d\n", card, channel, pval);
    }
    return status;
}


/*
 ************************************************************************
 *+
 * FUNCTION: xy566Report
 *
 * RETURNS: long [function status]
 *
 * DESCRIPTION: 
 * Read all of the XYCOM 566 waveforms present
 *
 *-
 ************************************************************************
 */

long xy566Report
(
    void
)
{
    long status;

    status = ai_xy566_io_report(1);
    if (status!=0)
    {
        printf ("xy566Report: Error\n");
    }
    return status;
}

/*
 ************************************************************************
 *+
 * FUNCTION: xy566Stats
 *
 * RETURNS: long [function status]
 *
 * DESCRIPTION: 
 * Collect N values from single channel from a XYCOM 566 SE card
 * Determine standard deviation, and ave. val.
 *
 *-
 ************************************************************************
 */

long xy566Stats
(
    short int card,
    short channel,
    int   num
)
{
    long status;
    int  i;
    unsigned short pval;
    double ave, sum, stdev, var;
  
    ave = sum = stdev = 0;
    for ( i=0; i< num; i++)
    {
	if ( ai_xy566_driver(card, channel, 4, &pval ) != 0 )
	{
	    printf ("xy566Stats: Error\n");
	    return ( -1 );
	}
	sum += pval;
	stdev = stdev + ( pval * pval );
	taskDelay(1);
    }
    ave = sum/num;
    var = (stdev - sum*sum/num) / (num - 1 );
    /* was var = (stdev/num) - (ave*ave);*/
    stdev = sqrt( var ); 
    printf ("xy566Stats: Card %d Chan %d Num Values %d: \n", card, channel,
    		num);
    printf ("                mean: %f\n", ave );
    printf ("                variance: %f\n", var );
    printf ("                standard deviation: %f\n", stdev );
    
    return status;
}

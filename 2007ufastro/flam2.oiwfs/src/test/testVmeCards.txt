Testing VME card basic functionality in a MVME2700 system
=========================================================

The basic functionality tests assume that the VxWorks PROMs
are installed in the 2700 SBC and that the system has booted up
WITH NO OTHER SOFTWARE INSTALLED.

Determine Local Base Address
----------------------------

First determine the local base address for the short address space 
(VME A16).  This seems to be defined in target/config/mv2700/target.nr
but we can also use the VxWorks function sysBusToLocalAdrs() to find out.

	sysBusToLocalAdrs ( adrsSpace, busAdrs, pLocalAdrs )

where:

  adrsSpace = 0x29	/* this is the VME address modifier that specifies the
			   address as "A16 non-privileged access" as per:
			   page 2-1 of the OMS58 user manual,
			   page 2-7 of the Xycom XVME-240 user manual, and
			   table 1.3.3 on page 1-3 of the bc635 user manual 
			   (see also Table 2-2 pg65 of "The VME Handbook, 4th ed.")*/

  busAdrs = 0x0		/* actually any short address would work here */

  pLocalAdrs		/* pointer to 32bit local address */


The test would go something like this:

-> pLocalAdrs
undefined symbol: pLocalAdrs
-> pLocalAdrs = 0
new symbol "pLocalAdrs" added to symbol table.
pLocalAdrs = 0x1ffbbf8: value = 0 = 0x0
-> sysBusToLocalAdrs(0x29, 0x0, &pLocalAdrs)
value = 0 = 0x0
-> pLocalAdrs
pLocalAdrs = 0x1ffbbf8: value = -67174400 = 0xfbff0000 = xxx + 0xf9ff43c8
-> 

So the local base address is 0xfbff which must prefix all short addresses
for the VME58, the XVME-240 and the bc635.


Testing OMS VME58 cards
-----------------------

The J61 jumpers have been changed to configure the card for
base address 0x8000 (first OMS VME58 card in an EPICS IOC).

Now test the OMS VME58 card by sending a command and reading the response.

o  First, send the who am I command (WY) to the VME58 Output Buffer 
   (ASCII W is 57 hex and ASCII Y is 59 hex), then
o  Increment the VME58 Output Put Index by 2 (characters) and finally
o  Read the first 20 hex bytes of the VME58 Input Buffer


-> m 0xfbff8804,2
fbff8804:  0000-0057
fbff8806:  0000-0059
fbff8808:  0000-.

value = 1 = 0x1
-> m 0xfbff8800,1
fbff8800:  00-02
fbff8801:  00-.

value = 1 = 0x1
-> d 0xfbff8004,21
fbff8000:            000a 000d 0056 004d 0045 0035   *      ...V.M.E.5*
fbff8010:  0038 0020 0076 0065 0072 0020 0032 002e   *.8. .v.e.r. .2..*
fbff8020:  0033 0035 002d 0034 0045 000a 000d        *.3.5.-.4.E......*
value = 21 = 0x15
-> 

The return string (for this particular VME58-4E card anyway) is:

<LF><CR>VME58 ver 2.35-4E<LF><CR>


Testing Xycom XVME-240 cards
----------------------------

The S2 switch settings have been changed to configure the card for
base address 0xd000 (the first 240 card in an EPICS IOC).

Now test the XVME-240 card by reading the "module identification" data,
then turn off the "FAIL" LED and turn on the "PASS" LED.

-> d 0xfbffd000,20
fbffd000:  ff56 ff4d ff45 ff49 ff44 ff58 ff59 ff43   *.V.M.E.I.D.X.Y.C*
fbffd010:  ff32 ff34 ff30 ff20 ff20 ff20 ff20 ff31   *.2.4.0. . . . .1*
fbffd020:  ff20 ff31 ff31 ff20                       *. .1.1. ........*
value = 21 = 0x15
-> m 0xfbffd081,1
fbffd081:  00-3
fbffd082:  00-.

value = 1 = 0x1
-> 

The return ASCII string is "VMEIDXYC240    1 11 "


Testing Symmetricon bc635 card
------------------------------

The S1/S2 switch settings have been changed to configure the card for
base address 0x4000.

Now test the bc635 card by reading the time data.

-> d 0xfbff4000,11
fbff4000:  fef4 f350 XXXX XXXX ffff ffff 0070   HH   *...P.........p..*
fbff4010:  MMSS XXXX XXXX                            *.#..S...........*
value = 21 = 0x15
-> 

In the response above, XXXX can be any value, and BCD digits HH and MMSS
should be the as the time displayed at the moment the memory dump was done.



Testing Low-level drivers
=========================


omsBacklash 
omsBacklash2
omsDoLots 
omsDoLots2
omsDoLots3
omsGetCardTypes 	n/a			# cards
omsGetLimits 		card, axis		status
omsGetPoints
omsGetStatus 		card, axis		status
omsOff 			card, axis		status
omsOn 			card, axis		status
omsOutLowerLimit 
omsOutUpperLimit 
omsPos 			card, axis		status
omsPrintPoints
omsRead			card			status
omsServoSet 
omsSetup 
omsVmeTestTask 
omsWrite 		card, "string"		status

xy240ReadIn		card, signal		status
xy240ReadOut		card, signal		status
xy240WriteOut		card, singal, [0 | 1]	status

xy566Read		card, channel		status
xy566Report		n/a			status
xy566Stats		card, channel, # reads	status


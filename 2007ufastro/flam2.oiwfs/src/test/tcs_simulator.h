int setTarget(double x_orig_in_mm, double y_orig_in_mm, 
              double radius_in_mm, double period_in_sec);

int getNextTarget(double * xPos, double * yPos);

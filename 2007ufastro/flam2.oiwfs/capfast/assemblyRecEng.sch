[schematic2]
uniq 352
[tools]
[detail]
w 868 827 100 2 n#351 hwin.$(UPDATE).in 864 832 864 832 elongouts.UpdateMode.DOL
w 504 811 100 0 UPDATE_TRIGGER inhier.UPDATE_TRIGGER.P 288 800 864 800 elongouts.UpdateMode.SLNK
w 1218 1515 100 0 n#313 elongouts.ParkMode.FLNK 1120 1504 1376 1504 1376 1056 1600 1056 eseqs.ValDirSeq.SLNK
w 1218 1067 100 0 n#313 elongouts.MoveMode.FLNK 1120 1056 1376 1056 junction
w 1218 1739 100 0 n#313 elongouts.ModeTarget.FLNK 1120 1728 1376 1728 1376 1504 junction
w 1218 1291 100 0 n#313 elongouts.TrackMode.FLNK 1120 1280 1376 1280 junction
w 1218 843 100 0 n#313 elongouts.UpdateMode.FLNK 1120 832 1376 832 1376 1056 junction
w 1604 1371 100 2 n#346 hwin.hwin#336.in 1600 1376 1600 1376 eseqs.ValDirSeq.DOL1
w 868 1275 100 2 n#334 hwin.hwin#333.in 864 1280 864 1280 elongouts.TrackMode.DOL
w 504 1483 100 0 PARK_TRIGGER inhier.PARK_TRIGGER.P 288 1472 864 1472 elongouts.ParkMode.SLNK
w 504 1259 100 0 TRACK_TRIGGER inhier.TRACK_TRIGGER.P 288 1248 864 1248 elongouts.TrackMode.SLNK
w 504 1035 100 0 MOVE_TRIGGER inhier.MOVE_TRIGGER.P 288 1024 864 1024 elongouts.MoveMode.SLNK
w 868 1499 100 2 n#312 hwin.hwin#311.in 864 1504 864 1504 elongouts.ParkMode.DOL
w 868 1051 100 2 n#310 hwin.hwin#300.in 864 1056 864 1056 elongouts.MoveMode.DOL
[cell use]
use inhier 272 983 100 0 MOVE_TRIGGER
xform 0 288 1024
use inhier 272 1207 100 0 TRACK_TRIGGER
xform 0 288 1248
use inhier 272 1431 100 0 PARK_TRIGGER
xform 0 288 1472
use inhier 272 759 100 0 UPDATE_TRIGGER
xform 0 288 800
use hwin 672 1015 100 0 hwin#300
xform 0 768 1056
p 675 1048 100 0 -1 val(in):$(A_MOVE)
use hwin 672 1463 100 0 hwin#311
xform 0 768 1504
p 675 1496 100 0 -1 val(in):$(A_PARK)
use hwin 672 1239 100 0 hwin#333
xform 0 768 1280
p 675 1272 100 0 -1 val(in):$(A_TRACK)
use hwin 1408 1335 100 0 hwin#336
xform 0 1504 1376
p 1411 1368 100 0 -1 val(in):$(A_START)
use hwin 672 791 -100 0 $(UPDATE)
xform 0 768 832
p 675 824 100 0 -1 val(in):$(A_UPDATE)
use elongouts 1024 1776 100 0 ModeTarget
xform 0 992 1696
p 928 1600 100 0 1 OMSL:supervisory
p 1024 1776 100 512 -1 PV:$(top)$(assy)
p 1056 1632 70 0 1 def(OUT):$(top)$(assy)Assembly.MODE
use elongouts 1024 1104 100 0 MoveMode
xform 0 992 1024
p 896 928 100 0 1 OMSL:supervisory
p 1024 1104 100 512 -1 PV:$(top)$(assy)
p 1056 960 70 0 1 def(OUT):$(top)$(assy)Assembly.MODE
use elongouts 1024 1552 100 0 ParkMode
xform 0 992 1472
p 896 1376 100 0 1 OMSL:supervisory
p 1024 1552 100 512 -1 PV:$(top)$(assy)
p 1072 1408 70 0 1 def(OUT):$(top)$(assy)Assembly.MODE
use elongouts 1024 1328 100 0 TrackMode
xform 0 992 1248
p 896 1152 100 0 1 OMSL:supervisory
p 1024 1328 100 512 -1 PV:$(top)$(assy)
p 1072 1184 70 0 1 def(OUT):$(top)$(assy)Assembly.MODE
use elongouts 1024 880 100 0 UpdateMode
xform 0 992 800
p 896 704 100 0 1 OMSL:supervisory
p 1024 880 100 512 -1 PV:$(top)$(assy)
p 1056 736 70 0 1 def(OUT):$(top)$(assy)Assembly.MODE
use eseqs 1792 1456 100 0 ValDirSeq
xform 0 1760 1216
p 1712 976 100 0 1 DLY1:0.5
p 1760 928 100 256 0 DLY2:0.0
p 1776 1456 100 512 -1 PV:$(top)$(assy)
p 2016 1376 70 0 1 def(LNK1):$(top)$(assy)Dir.VAL
p 2016 1344 70 0 0 def(LNK2):0.0
p 1936 1376 75 1024 -1 pproc(LNK1):PP
p 1936 1344 75 1024 -1 pproc(LNK2):NPP
use f2BorderC -416 -153 100 0 f2BorderC#91
xform 0 1264 1152
p 1216 1088 100 0 0 IO:
p 2532 160 120 256 -1 Project:Gemini Flamingos-2 OIWFS
p 2244 20 150 0 1 Rev:
p 2524 96 120 256 -1 Title:Engineering Driver for assemblyControl
p 2564 32 100 1024 -1 author:B.Wooff
p 2564 0 100 1024 -1 date:May 31, 2005
p 1152 1056 100 0 0 model:
p 1152 1024 100 0 0 revision:
[comments]

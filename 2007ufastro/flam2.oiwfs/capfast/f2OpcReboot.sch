[schematic2]
uniq 120
[tools]
[detail]
w 1314 -309 100 0 n#119 ecad4.reboot.PLNK 320 128 480 128 480 -320 2208 -320 outhier.FLNK.p
w 474 107 100 0 n#116 eseqs.rebootSeq.SLNK 688 96 320 96 ecad4.reboot.STLK
w 1082 395 100 0 n#114 eseqs.rebootSeq.LNK2 1008 384 1216 384 1216 -96 1408 -96 esubs.subReboot.SLNK
w 2042 459 100 0 c#112 ecars.rebootC.OERR 2016 448 2128 448 2128 384 2208 384 outhier.OERR.p
w 2082 491 100 0 OMSS ecars.rebootC.OMSS 2016 480 2208 480 outhier.OMSS.p
w 2082 555 100 0 c#110 ecars.rebootC.VAL 2016 544 2208 544 outhier.CAR.p
w 1442 555 100 0 n#100 eseqs.rebootSeq.LNK1 1008 416 1248 416 1248 544 1696 544 ecars.rebootC.IVAL
w 692 411 100 2 n#97 hwin.hwin#96.in 688 416 688 416 eseqs.rebootSeq.DOL1
w 1240 683 100 0 n#30 ecad4.reboot.MESS 320 672 2208 672 outhier.MESS.p
w 1240 715 100 0 n#29 ecad4.reboot.VAL 320 704 2208 704 outhier.VAL.p
w -152 683 100 0 n#28 inhier.ICID.P -256 672 0 672 ecad4.reboot.ICID
w -216 779 100 0 c#101 inhier.DIR.P -256 768 -128 768 -128 704 0 704 ecad4.reboot.DIR
s 816 848 180 0 Reboot Sequence Command
[cell use]
use outhier 2256 384 100 0 OERR
xform 0 2192 384
use outhier 2256 464 100 0 OMSS
xform 0 2192 480
use outhier 2240 528 100 0 CAR
xform 0 2192 544
use outhier 2240 704 100 0 VAL
xform 0 2192 704
use outhier 2240 656 100 0 MESS
xform 0 2192 672
use outhier 2240 -320 100 0 FLNK
xform 0 2192 -320
use esubs 1472 336 100 0 subReboot
xform 0 1552 80
p 1120 -66 100 0 0 INAM:
p 1472 336 100 512 -1 PV:$(top)
p 1440 -192 100 0 1 SNAM:f2SeqReboot
use ecars 1872 592 100 0 rebootC
xform 0 1856 432
p 1856 592 100 512 -1 PV:$(top)
use hwin 496 375 100 0 hwin#96
xform 0 592 416
p 499 408 100 0 -1 val(in):$(A_BUSY)
use eseqs 864 496 100 0 rebootSeq
xform 0 848 256
p 800 304 100 0 1 DLY1:0.0
p 800 272 100 0 1 DLY2:3.0
p 864 496 100 512 -1 PV:$(top)
p 1040 368 100 0 0 def(LNK2):0.0
p 1024 416 75 1024 -1 pproc(LNK1):PP
p 1024 384 75 1024 -1 pproc(LNK2):PP
use f2BorderC -608 -1177 100 0 f2BorderC#48
xform 0 1072 128
p 2644 -1048 100 512 1 File:f2OpcReboot.sch
p 416 -1008 100 256 -1 Rev:0.1
p 2332 -928 120 256 -1 Title:Flamingos-2 IS - reboot sequence command
p 2372 -992 100 1024 -1 author:B.Wooff
p 2372 -1024 100 1024 -1 date:June 28, 2005
use ecad4 192 752 100 0 reboot
xform 0 160 384
p 192 752 100 512 -1 PV:$(top)
p 64 0 100 0 1 SNAM:f2SeqCadReboot
p 224 -224 100 0 0 def(OUTA):0.0
p 224 -256 100 0 0 def(OUTB):0.0
p 224 -288 100 0 0 def(OUTC):0.0
p 224 -320 100 0 0 def(OUTD):0.0
use inhier -320 768 100 0 DIR
xform 0 -256 768
use inhier -336 656 100 0 ICID
xform 0 -256 672
[comments]

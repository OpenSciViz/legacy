[schematic2]
uniq 295
[tools]
[detail]
w 450 715 100 0 n#277 startCounter.startCounter#274.FLNK 288 640 320 640 320 704 640 704 easscontj.Assembly.SLNK
w 258 1963 100 0 n#276 elongouts.Dir.VAL 352 2208 512 2208 512 1952 64 1952 64 736 128 736 startCounter.startCounter#274.DIR
w 258 1995 100 0 n#275 elongouts.Dir.FLNK 352 2240 544 2240 544 1984 32 1984 32 640 128 640 startCounter.startCounter#274.SLNK
w 466 2187 100 0 n#268 elongouts.Dir.OUT 352 2176 640 2176 easscontj.Assembly.DIR
w 1858 843 100 0 n#266 devctlpbv.devctlpbv#113.dbug 1664 1440 1632 1440 1632 832 2144 832 2144 544 2016 544 edfans.DebugFan.OUTB
w 1490 555 100 0 n#265 devctlpbv.devctlpbv#113.simm 1664 1472 1600 1472 1600 544 1440 544 edfans.SimulateFan.OUTB
w 1586 811 100 0 n#264 devctlpbv.devctlpbv#114.dbug 1184 1216 1120 1216 1120 800 2112 800 2112 576 2016 576 edfans.DebugFan.OUTA
w 1282 779 100 0 n#263 devctlpbv.devctlpbv#114.simm 1184 1248 1088 1248 1088 768 1536 768 1536 576 1440 576 edfans.SimulateFan.OUTA
w 1282 235 100 0 n#262 edfans.DebugFan.OUTC 2016 512 2112 512 2112 224 512 224 512 768 640 768 easscontj.Assembly.DBUG
w 978 267 100 0 n#261 edfans.SimulateFan.OUTC 1440 512 1536 512 1536 256 480 256 480 800 640 800 easscontj.Assembly.SIMM
w 738 331 100 0 DBUG inhier.DBUG.P -160 320 1696 320 1696 512 1760 512 edfans.DebugFan.SLNK
w 354 427 100 0 SIMM inhier.SIMM.P -160 416 928 416 928 512 1184 512 edfans.SimulateFan.SLNK
w 2470 1355 100 0 cur_pos f2OiwfsCalcPos.f2OiwfsCalcPos#258.CUR_POS 2400 1488 2432 1488 2432 1344 2592 1344 outhier.CUR_POS.p
w 2498 1163 100 0 n#254 ecalcs.heartbeat.VAL 2720 864 2752 864 2752 1152 2304 1152 2304 1056 2432 1056 ecalcs.heartbeat.INPA
w 222 1835 100 0 Y_M inhier.Y_M.P -160 1824 640 1824 easscontj.Assembly.B
w 414 1867 100 0 X_M inhier.X_M.P -160 1888 224 1888 224 1856 640 1856 easscontj.Assembly.A
w 216 2091 100 0 mode inhier.MODE.P -160 2080 640 2080 640 2112 easscontj.Assembly.MODE
w -50 2219 100 0 dir inhier.DIR.P -160 2208 96 2208 elongouts.Dir.SLNK
w 2182 2091 100 0 CarErrorMessage outhier.OMSS.p 2592 2080 1952 2080 ecars.C.OMSS
w 2256 2251 100 0 CarEvent ecars.C.VAL 1952 2144 2016 2144 2016 2240 2592 2240 outhier.CAR.p
w 1858 299 100 0 FLNK easscontj.Assembly.FLNK 960 704 1024 704 1024 288 2752 288 outhier.FLNK.p
w 1784 1803 100 0 HLTH easscontj.Assembly.HLTH 960 1984 1024 1984 1024 1792 2592 1792 outhier.HLTH.p
w 1800 1835 100 0 MESS easscontj.Assembly.MESS 960 2080 1056 2080 1056 1824 2592 1824 outhier.MESS.p
w 1822 1867 100 0 VAL easscontj.Assembly.VAL 960 2176 1088 2176 1088 1856 2592 1856 outhier.VAL.p
w 2188 1931 100 0 CarForwardLink ecars.C.FLNK 1952 1920 2592 1920 outhier.CFLK.p
w 2188 2059 100 0 CarErrorNumber ecars.C.OERR 1952 2048 2592 2048 outhier.OERR.p
w 2418 1563 100 0 z_cur f2OiwfsCalcPos.f2OiwfsCalcPos#258.Z_CUR 2400 1552 2496 1552 2496 1472 2592 1472 outhier.Z_CUR.p
w 1762 1227 100 0 n#196 devctlpbv.devctlpbv#114.mpos 1504 1376 1536 1376 1536 1216 2048 1216 2048 1584 2080 1584 f2OiwfsCalcPos.f2OiwfsCalcPos#258.PKO_POS
w 2492 1419 100 0 pr_cur f2OiwfsCalcPos.f2OiwfsCalcPos#258.PR_CUR 2400 1520 2464 1520 2464 1408 2592 1408 outhier.PR_CUR.p
w 2434 1595 100 0 y_cur f2OiwfsCalcPos.f2OiwfsCalcPos#258.Y_CUR 2400 1584 2528 1584 2528 1536 2592 1536 outhier.Y_CUR.p
w 2466 1627 100 0 x_cur f2OiwfsCalcPos.f2OiwfsCalcPos#258.X_CUR 2400 1616 2592 1616 2592 1600 outhier.X_CUR.p
w 2002 1627 100 0 n#132 devctlpbv.devctlpbv#113.mpos 1984 1600 1984 1616 2080 1616 f2OiwfsCalcPos.f2OiwfsCalcPos#258.BAS_POS
w 1042 1291 100 0 n#126 easscontj.Assembly.BUS2 960 1280 1184 1280 devctlpbv.devctlpbv#114.bsyl
w 1042 1323 100 0 n#125 easscontj.Assembly.ACK2 960 1312 1184 1312 devctlpbv.devctlpbv#114.ack
w 1042 1355 100 0 n#124 easscontj.Assembly.VEL2 960 1344 1184 1344 devctlpbv.devctlpbv#114.velo
w 1042 1387 100 0 n#123 easscontj.Assembly.POS2 960 1376 1184 1376 devctlpbv.devctlpbv#114.val
w 1042 1419 100 0 n#122 easscontj.Assembly.MOD2 960 1408 1184 1408 devctlpbv.devctlpbv#114.mode
w 1042 1451 100 0 n#121 easscontj.Assembly.ODR2 960 1440 1184 1440 devctlpbv.devctlpbv#114.dir
w 1282 1515 100 0 n#120 easscontj.Assembly.BUS1 960 1504 1664 1504 devctlpbv.devctlpbv#113.bsyl
w 1282 1547 100 0 n#127 easscontj.Assembly.ACK1 960 1536 1664 1536 devctlpbv.devctlpbv#113.ack
w 1282 1579 100 0 n#128 easscontj.Assembly.VEL1 960 1568 1664 1568 devctlpbv.devctlpbv#113.velo
w 1282 1611 100 0 n#129 easscontj.Assembly.POS1 960 1600 1664 1600 devctlpbv.devctlpbv#113.val
w 1282 1643 100 0 n#130 easscontj.Assembly.MOD1 960 1632 1664 1632 devctlpbv.devctlpbv#113.mode
w 1282 1675 100 0 n#115 easscontj.Assembly.ODR1 960 1664 1664 1664 devctlpbv.devctlpbv#113.dir
s 624 1760 100 512 Stage Velocity
s 640 1840 100 512 Y Target
s 640 1872 100 512 X Target
s 608 960 100 512 Current pickoff position
s 608 992 100 512 Current base position
[cell use]
use ecalcs 2496 1104 100 0 heartbeat
xform 0 2576 848
p 2560 1056 100 0 1 CALC:(1+A)%100
p 2144 734 100 0 0 EGU:counter
p 2368 1104 100 0 1 PV:$(top)
p 2560 1024 100 0 1 SCAN:1 second
use startCounter 128 583 100 0 startCounter#274
xform 0 208 704
p 128 576 100 0 -1 seta:assy $(assy)
use elongouts 256 2288 100 0 Dir
xform 0 224 2208
p 112 2112 100 0 1 OMSL:closed_loop
p 256 2288 100 512 -1 PV:$(top)$(assy)
use f2OiwfsCalcPos 2160 1319 100 0 f2OiwfsCalcPos#258
xform 0 2240 1520
use outhier 2608 2240 100 0 CAR
xform 0 2576 2240
use outhier 2768 288 100 0 FLNK
xform 0 2736 288
use outhier 2608 1920 100 0 CFLK
xform 0 2576 1920
use outhier 2608 2048 100 0 OERR
xform 0 2576 2048
use outhier 2608 2080 100 0 OMSS
xform 0 2576 2080
use outhier 2608 1856 100 0 VAL
xform 0 2576 1856
use outhier 2608 1792 100 0 HLTH
xform 0 2576 1792
use outhier 2608 1824 100 0 MESS
xform 0 2576 1824
use outhier 2592 1424 100 0 PR_CUR
xform 0 2576 1408
use outhier 2592 1552 100 0 Y_CUR
xform 0 2576 1536
use outhier 2592 1616 100 0 X_CUR
xform 0 2576 1600
use outhier 2592 1488 100 0 Z_CUR
xform 0 2576 1472
use outhier 2592 1360 100 0 CUR_POS
xform 0 2576 1344
use inhier -192 320 100 512 DBUG
xform 0 -160 320
use inhier -192 416 100 512 SIMM
xform 0 -160 416
use inhier -192 1824 100 512 Y_M
xform 0 -160 1824
use inhier -192 1888 100 512 X_M
xform 0 -160 1888
use inhier -192 2080 100 512 MODE
xform 0 -160 2080
use inhier -192 2208 100 512 DIR
xform 0 -160 2208
use edfans 1968 720 100 0 DebugFan
xform 0 1888 512
p 1968 720 100 512 1 PV:$(top)$(assy)
p 1760 656 100 0 1 SELM:All
p 2016 576 75 768 -1 pproc(OUTA):NPP
p 2016 544 75 768 -1 pproc(OUTB):NPP
p 2016 512 75 768 -1 pproc(OUTC):NPP
use edfans 1392 720 100 0 SimulateFan
xform 0 1312 512
p 1392 720 100 512 1 PV:$(top)$(assy)
p 1184 656 100 0 1 SELM:All
p 1440 576 75 768 -1 pproc(OUTA):NPP
p 1440 544 75 768 -1 pproc(OUTB):NPP
p 1440 512 75 768 -1 pproc(OUTC):NPP
use ecars 1840 2192 100 0 C
xform 0 1792 2032
p 1824 2192 100 512 1 PV:$(top)$(assy)
use snlLink 992 2071 100 0 snlLink#213
xform 0 1296 2144
use devctlpbv 1664 1383 100 0 devctlpbv#113
xform 0 1824 1552
p 1728 1376 100 0 -1 seta:dev $(dev1)
p 1728 1344 100 0 -1 setb:motor $(basMotor)
p 1728 1312 100 0 -1 setc:home $(basHome)
p 1728 1280 100 0 -1 setd:brake $(basBrake)
p 1728 1248 100 0 -1 sete:posLim $(basPosLim)
p 1728 1216 100 0 -1 setf:negLim $(basNegLim)
use devctlpbv 1184 1159 100 0 devctlpbv#114
xform 0 1344 1328
p 1248 1152 100 0 -1 seta:dev $(dev2)
p 1248 1120 100 0 -1 setb:motor $(pkoMotor)
p 1248 1088 100 0 -1 setc:home $(pkoHome)
p 1248 1056 100 0 -1 setd:brake $(pkoBrake)
p 1248 1024 100 0 -1 sete:posLim $(pkoPosLim)
p 1248 992 100 0 -1 setf:negLim $(pkoNegLim)
use easscontj 864 2224 100 0 Assembly
xform 0 800 1440
p 704 640 100 0 1 DTYP:Oiwfs
p 448 1998 100 0 0 EGU:deg.
p 816 1792 80 256 -1 FTC:LONG
p 720 992 70 0 -1 FTSJ:DOUBLE
p 720 960 70 0 -1 FTSK:DOUBLE
p 720 928 70 0 -1 FTSL:DOUBLE
p 720 896 70 0 -1 FTSM:DOUBLE
p 720 864 70 0 -1 FTSN:DOUBLE
p 704 512 100 0 1 PREC:3
p 704 2224 100 0 -1 PV:$(top)$(assy)
p 704 480 100 0 1 SIMM:$(assySimm)
p 1024 1536 80 768 -1 pproc(ACK1):NPP
p 1024 1312 80 768 -1 pproc(ACK2):NPP
p 1024 1088 80 768 -1 pproc(ACK3):NPP
p 576 1536 80 1280 -1 pproc(ACK4):NPP
p 576 1312 80 1280 -1 pproc(ACK5):NPP
p 1024 2112 80 768 -1 pproc(BSYL):NPP
p 1024 768 80 768 -1 pproc(DBGL):PP
p 1024 1664 80 768 -1 pproc(ODR1):PP
p 1024 1440 80 768 -1 pproc(ODR2):PP
p 1024 1216 80 768 -1 pproc(ODR3):PP
p 576 1664 80 1280 -1 pproc(ODR4):PP
p 576 1440 80 1280 -1 pproc(ODR5):PP
p 1024 1152 80 768 -1 pproc(POS3):NPP
p 1024 800 80 768 -1 pproc(SIML):PP
p 1024 864 80 768 -1 pproc(SOV):PP
use f2BorderC -416 -153 100 0 f2BorderC#91
xform 0 1264 1152
p 2836 -24 100 512 1 File:f2OiwfsAssembly.sch
p 2532 160 120 256 -1 Project:Gemini Flamingos-2 OIWFS
p 2244 20 150 0 1 Rev:
p 2524 96 120 256 -1 Title:OIWFS Probe Assembly
p 2564 32 100 1024 -1 author:B.Wooff
p 2564 0 100 1024 -1 date:May 31, 2005
[comments]

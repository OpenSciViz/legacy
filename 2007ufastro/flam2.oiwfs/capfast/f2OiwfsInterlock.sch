[schematic2]
uniq 319
[tools]
[detail]
w 1650 1227 100 0 n#318 hwout.hwout#317.outp 1888 1312 1888 1216 1472 1216 egenSub.InterlockDecode.OUTA
w 1680 656 100 0 pickoff_ilck_sdis egenSub.InterlockDecode.VALH 1472 800 1664 800 1664 640 2080 640 elongouts.elongouts#310.SDIS
w 1784 683 100 0 pickoff_interlock_in egenSub.InterlockDecode.OUTG 1472 832 1728 832 1728 672 2080 672 elongouts.elongouts#310.SLNK
w 1602 939 100 0 base_ilck_sdis egenSub.InterlockDecode.VALF 1472 928 1792 928 1792 864 2080 864 elongouts.elongouts#309.SDIS
w 1562 971 100 0 base_interlock_in egenSub.InterlockDecode.OUTE 1472 960 1856 960 1856 896 2080 896 elongouts.elongouts#309.SLNK
w 1618 1067 100 0 probe_ilck_sdis egenSub.InterlockDecode.VALD 1472 1056 1824 1056 1824 1088 2080 1088 elongouts.elongouts#308.SDIS
w 1776 1136 100 0 probe_interlock_in egenSub.InterlockDecode.OUTC 1472 1088 1760 1088 1760 1120 2080 1120 elongouts.elongouts#308.SLNK
w 786 683 100 0 n#302 egenSub.InterlockDecode.INPG 1184 832 960 832 960 672 672 672 elongins.li2.VAL
w 1010 907 100 0 n#301 egenSub.InterlockDecode.INPF 1184 896 896 896 896 864 768 864 elongins.li1.VAL
s 384 688 100 512 $(assy)$(dev2) E-STOP button
s 464 864 100 512 $(assy)$(dev1) E-STOP button
s 1120 976 100 512 $(assy) E-STOP button
s 1568 1248 100 0 Interlock error message
s 1568 1184 100 0 Interlock error code
[cell use]
use hwout 1888 1271 100 0 hwout#317
xform 0 1984 1312
p 2096 1312 100 0 -1 val(outp):$(sadtop)historyLog.VAL PP NMS
use elongouts 2080 583 100 0 elongouts#310
xform 0 2208 672
p 2192 640 100 256 1 DISV:-1
p 2336 608 100 0 1 def(OUT):$(top)$(assy)$(dev2)Device.FLT
p 2208 752 100 1024 1 name:$(top)$(assy)$(dev2)Interlock
p 2336 640 75 768 -1 pproc(OUT):NPP
use elongouts 2080 807 100 0 elongouts#309
xform 0 2208 896
p 2192 864 100 256 1 DISV:-1
p 2336 832 100 0 1 def(OUT):$(top)$(assy)$(dev1)Device.FLT
p 2208 976 100 1024 1 name:$(top)$(assy)$(dev1)Interlock
p 2336 864 75 768 -1 pproc(OUT):NPP
use elongouts 2080 1031 100 0 elongouts#308
xform 0 2208 1120
p 2192 1088 100 256 1 DISV:-1
p 2336 1056 100 0 1 def(OUT):$(top)$(assy)Assembly.ILCK
p 2208 1200 100 1024 1 name:$(top)$(assy)Interlock
p 2336 1088 75 768 -1 pproc(OUT):NPP
use elongins 560 752 -100 0 li2
xform 0 544 688
p 704 752 100 512 -1 name:$(top)$(assy)$(dev2)Estop
use elongins 656 944 -100 0 li1
xform 0 640 880
p 768 944 100 512 -1 name:$(top)$(assy)$(dev1)Estop
use egenSub 1312 1296 100 0 InterlockDecode
xform 0 1328 880
p 1328 416 100 256 1 EFLG:ON CHANGE
p 1264 1216 100 0 -1 FTA:LONG
p 1264 1152 100 0 -1 FTB:LONG
p 1264 1088 100 0 -1 FTC:LONG
p 1264 1024 100 0 -1 FTD:LONG
p 1264 976 100 0 -1 FTE:LONG
p 1264 928 100 0 -1 FTF:LONG
p 1264 864 100 0 -1 FTG:LONG
p 1264 800 100 0 0 FTH:LONG
p 1264 736 100 0 0 FTI:LONG
p 1264 672 100 0 0 FTJ:LONG
p 1408 1232 100 512 -1 FTVA:STRING
p 1408 1168 100 512 -1 FTVB:LONG
p 1408 1104 100 512 -1 FTVC:LONG
p 1408 1040 100 512 -1 FTVD:LONG
p 1408 992 100 512 -1 FTVE:LONG
p 1408 928 100 512 -1 FTVF:LONG
p 1408 864 100 512 -1 FTVG:LONG
p 1408 800 100 512 -1 FTVH:LONG
p 1408 736 100 512 -1 FTVI:LONG
p 1312 1296 100 512 -1 PV:$(top)$(assy)
p 1328 496 100 256 1 SCAN:.1 second
p 1328 448 100 256 1 SNAM:oiInterlock
p 1120 1216 100 512 1 def(INPA):$(top)$(assy)$(dev1)NegLim
p 1120 1152 100 512 1 def(INPB):$(top)$(assy)$(dev1)PosLim
p 1120 1088 100 512 1 def(INPC):$(top)$(assy)$(dev2)NegLim
p 1120 1024 100 512 1 def(INPD):$(top)$(assy)$(dev2)PosLim
p 1120 960 100 512 0 def(INPE):0
p 1120 896 100 512 0 def(INPF):0
p 1120 832 100 512 0 def(INPG):0
p 1568 1152 100 0 0 def(OUTB):0
p 1568 1088 100 0 0 def(OUTC):0
p 1568 1024 100 0 0 def(OUTD):0
p 961 485 100 0 0 def(OUTE):0
p 961 421 100 0 0 def(OUTG):0.000000000000000e+00
p 1136 1098 75 0 -1 pproc(INPC):NPP
p 1136 970 75 0 -1 pproc(INPE):NPP
p 1136 842 75 0 -1 pproc(INPG):NPP
p 1472 1226 75 0 -1 pproc(OUTA):PP
p 1472 1162 75 0 -1 pproc(OUTB):NPP
p 1472 1098 75 0 -1 pproc(OUTC):PP
p 1472 1034 75 0 -1 pproc(OUTD):NPP
p 1472 970 75 0 -1 pproc(OUTE):PP
p 1472 842 75 0 -1 pproc(OUTG):PP
use f2BorderC -416 -153 100 0 f2BorderC#91
xform 0 1264 1152
p 2836 -24 100 512 1 File:f2OiwfsInterlock.sch
p 2532 160 120 256 -1 Project:Gemini Flamingos-2 OIWFS
p 2244 20 150 0 1 Rev:
p 2524 96 120 256 -1 Title:OIWFS Probe Interlock
p 2564 32 100 1024 -1 author:B.Wooff
p 2560 0 100 1024 -1 date:June 21, 2005
[comments]

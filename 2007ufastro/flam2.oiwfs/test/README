Contents of the test directory:


HOW_TO_RUN_TESTS
        Help file describing basic concepts of the testing procedures.

TEMPORARY_TEST_SCRIPTS
        Will probably delete.

checkout
        The "checkout" utility is the main test script launcher.
        It runs the specified *Checkout.scr script once (see the
        "soak" utility for multiple passes of one test).  The first
        argument is required and is the name of the test script 
        (actually it's the prefix of the name - everything that is  
        before Checkout.scr).  There are also two optional arguments
        when using "checkout":

          -auto   runs the script automatically answering affirmatively
                  to all propmts.

          -debug  adds diagnostic messages useful for debugging a script

        Prior to running the test scripts, the ./logfiles directory is
        created.  After the test is run a log of the test script results 
        will be saved to ./logfiles/<day>/*Checkout.log and a copy is
        written to the same filename with the time and date appended to it
        (so that multiple logs of the same test can be distinguished). 

        The "checkout" utility uses the "wish" command and the 
        "dhsTestCmdr.tk" Tcl script file. 

configFiles/
        Directory of the configuration files where script variable names 
        are translated to EPICS records and fields.  Each record used in
        the test scripts is translated only once, so if the record names
        in the database change, corresponding changes must happen in these
        files. 

configFilesSim/
        Simulator database version of the configFiles directory.  Currently 
        no simulator configuration files exist.

dhsTestCmdr.tk
        The main Tcl script file used by "checkout", which runs input 
        scripts that will execute commands, set values and confirm results.

f2Checkout
        High level script.  Calls diffent levels of checkout on the 
        Flamingos-2 OPC.  Level 1, the default, calls the basic test
        script.  At each level increase, additional scripts are run.

        If no arguments are provided, level 1 is used.  There are two 
        optional arguments as well:

          -auto   runs the script automatically answering affirmatively
                  to all propmts.

          -debug  adds diagnostic messages useful for debugging a script

f2Checkout.asm
        Test script assumptions file for f2Checkout script.

f2PickoffThermal
        High level script.  Runs the pickoff stage thermal tests routines.
        It uses "checkout" to run pkoThermalSetupCheckout.scr once then 
        uses "soak" to run pkoThermalCheckout.scr a number of times with 
        a delay in between.  The "f2PickoffThermal" script takes two
        arguments:
          number   is the number of times pkoThermalCheckout.scr is run
          delay    is the time (in seconds) between iterations.

f2PickoffThermal.asm
        Test script assumptions file for f2PickoffThermal script.

logfiles/
        Directory of all test log files (*Checkout.log.<date>).  There is
        a subdirectory for each day tests were run.  

scripts/
        Directory containing the test scripts (*Checkout.scr) as well as 
        their associated assumptions files (*Checkout.asm) and low-level
        Solaris command shell scripts.

soak
        This sets the environment variables and runs a checkout test
        over and over again to check the reliability of a component.
         
        The "soak" utility is used instead of checkout" when the 
        user wants to execute the same test script repeatedly.   
        It operates much the same as "checkout" and in fact calls 
        "checkout" to run the actual test scripts.  In addition to
        the script name argument, the user can specify:
          - number of times the test script will be executed
          - time delay between iterations of the script,
          - flag to call wfsReset prior to each iteration 
            (except for the first pass),
          - flag to execute the 1st iteration in "-auto" mode
            (subsequent passes will always be run in "-auto" mode),
          - flag to log results of every iteration instead of only
            the final pass.

wfsReset
        Resets the OIWFS probe assembly and devices to make it look 
        as if it has just been booted.  This is useful to save rebooting 
        the VME crate when testing a checkout script when the user is
        confident that nothing else has changed in the configuration.
        Use with caution!

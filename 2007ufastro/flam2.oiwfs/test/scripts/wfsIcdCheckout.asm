Flamingos-2 OIWFS Assembly ICD Checkout.

Assumptions are ....

        * The IOC has been depowered and re-started cold, or the wfs
          assembly has been reset to its startup state.

        * The Flamingos-2 OPC database has been loaded.

        * There is nothing obstructing the OIWFS probe arm.

	If so, it should be safe to move the probe arm....

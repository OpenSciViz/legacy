
#
# DHS Test Commander
#
# Test script to test functionality of OPC as defined by the
# Flamingos OIWFS to TCS Interface Document - ICD 1.9.f/1.1.11
#

#
# Do the variable substitution thing....
#

VAR     wfsAssembly      assemblyAcknowledge     wfsAck              1
VAR     wfsAssembly      assemblyBusy            wfsBusy             1
VAR     wfsAssembly      assemblyDebug           wfsDebug            1
VAR     wfsAssembly      assemblyDevType         wfsDevType          1
VAR     wfsAssembly      assemblyDirective       wfsDir              1
VAR     wfsAssembly      assemblyIndexed         wfsIndexed          1
VAR     wfsAssembly      assemblyInitialized     wfsInitialized      1
VAR     wfsAssembly      assemblyHealth          wfsHealth           1
VAR     wfsAssembly      assemblyLutDir          wfsLutDir           1
VAR     wfsAssembly      assemblyLutFile         wfsLutFile          1
VAR     wfsAssembly      assemblyMessage         wfsMess             1
VAR     wfsAssembly      assemblyMode            wfsMode             1
VAR     wfsAssembly      assemblyNumberOfDevices wfsNumDev           1
VAR     wfsAssembly      assemblyParked          wfsParked           1
VAR     wfsAssembly      assemblySimulation      wfsSimulation       1
VAR     wfsAssembly      assemblyAttributeDMax   wfsStageMaxVelo     1
VAR     wfsAssembly      assemblyAttributeDMin   wfsStageMinVelo     1
VAR     wfsAssembly      assemblyDAttribute      wfsStageVelo        1
VAR     wfsAssembly      assemblyState           wfsState            1
VAR     wfsAssembly      assemblyAAttribute      wfsXTarget          1
VAR     wfsAssembly      assemblyBAttribute      wfsYTarget          1

VAR     wfsCommands      wfsBaseOffset           wfsBasOffset        1
VAR     wfsCommands      wfsPickoffOFfset        wfsPkoOffset        1
VAR     wfsCommands      wfsXOffset              wfsXOffset          1
VAR     wfsCommands      wfsYOffset              wfsYOffset          1
VAR     wfsCommands      wfsZOffset              wfsZOffset          1

VAR     wfsCommands      wfsXPos                 wfsXPos             1
VAR     wfsCommands      wfsYPos                 wfsYPos             1

VAR     wfsCommands      wfsProbeGis             wfsProbeGis         1
VAR     wfsCommands      wfsProbeEstop           wfsProbeEstop       1
VAR     wfsCommands      wfsProbeBaseEstop       wfsProbeBaseEstop   1
VAR     wfsCommands      wfsProbePickoffEstop    wfsProbePickoffEstop 1

# TCS Command Interface
VAR     wfsCommands      tcsDatumDir             tcsDatumDir         1
VAR     wfsCommands      tcsDatumAck             tcsDatumAck         1
VAR     wfsCommands      tcsDatumMess            tcsDatumMess        1

VAR     wfsCommands      tcsDebugDir             tcsDebugDir         1
VAR     wfsCommands      tcsDebugAck             tcsDebugAck         1
VAR     wfsCommands      tcsDebugMess            tcsDebugMess        1
VAR     wfsCommands      tcsDebugCar             tcsDebugCar         1
VAR     wfsCommands      tcsDebugTarget          tcsDebugTarget      1

VAR     wfsCommands      tcsFollowDir            tcsFollowDir        1
VAR     wfsCommands      tcsFollowAck            tcsFollowAck        1
VAR     wfsCommands      tcsFollowMess           tcsFollowMess       1
VAR     wfsCommands      tcsFollowCar            tcsFollowCar        1

VAR     wfsCommands      tcsInitDir              tcsInitDir          1
VAR     wfsCommands      tcsInitAck              tcsInitAck          1
VAR     wfsCommands      tcsInitMess             tcsInitMess         1

VAR     wfsCommands      tcsMoveDir              tcsMoveDir          1
VAR     wfsCommands      tcsMoveAck              tcsMoveAck          1
VAR     wfsCommands      tcsMoveMess             tcsMoveMess         1
VAR     wfsCommands      tcsMoveXTarget          wfsXTarget          1
VAR     wfsCommands      tcsMoveYTarget          wfsYTarget          1

VAR     wfsCommands      tcsParkDir              tcsParkDir          1
VAR     wfsCommands      tcsParkAck              tcsParkAck          1
VAR     wfsCommands      tcsParkMess             tcsParkMess         1

VAR     wfsCommands      f2SeqRebootC            f2SeqRebootC        1
VAR     wfsCommands      f2SeqRebootDir          f2SeqRebootDir      1
VAR     wfsCommands      f2SeqRebootAck          f2SeqRebootAck      1
VAR     wfsCommands      f2SeqRebootMess         f2SeqRebootMess     1

VAR     wfsCommands      tcsStopDir              tcsStopDir          1
VAR     wfsCommands      tcsStopAck              tcsStopAck          1
VAR     wfsCommands      tcsStopMess             tcsStopMess         1

VAR     wfsCommands      tcsTestDir              tcsTestDir          1
VAR     wfsCommands      tcsTestAck              tcsTestAck          1
VAR     wfsCommands      tcsTestMess             tcsTestMess         1

VAR     wfsCommands      tcsToleranceDir         tcsToleranceDir     1
VAR     wfsCommands      tcsToleranceAck         tcsToleranceAck     1
VAR     wfsCommands      tcsToleranceMess        tcsToleranceMess    1
VAR     wfsCommands      tcsToleranceTarget      tcsToleranceTarget  1
VAR     wfsCommands      tcsToleranceLoLim       tcsToleranceLoLim   1
VAR     wfsCommands      tcsToleranceHiLim       tcsToleranceHiLim   1

VAR     wfsCommands      wfsActiveC              wfsActiveC          1

# TCS target stream simulator
VAR     wfsCommands      tcsSimStreamDisable     tcsSimStreamDisable 1
VAR     wfsCommands      tcsSimXOrigin           tcsSimXOrigin       1
VAR     wfsCommands      tcsSimYOrigin           tcsSimYOrigin       1
VAR     wfsCommands      tcsSimPeriod            tcsSimPeriod        1
VAR     wfsCommands      tcsSimRadius            tcsSimRadius        1
VAR     wfsCommands      tcsSimTrackID           tcsSimTrackID       1


# Status and Alarm database
VAR     wfsCommands      sadArrayS               sadArrayS           1
VAR     wfsCommands      sadFollowS              sadFollowS          1
VAR     wfsCommands      sadHistoryLog           sadHistoryLog       1
VAR     wfsCommands      sadInPosition           sadInPosition       1
VAR     wfsCommands      sadName                 sadName             1
VAR     wfsCommands      sadPresent              sadPresent          1
VAR     wfsCommands      sadProbeCar             sadProbeCar         1
VAR     wfsCommands      sadProbeIndex           sadProbeIndex       1
VAR     wfsCommands      sadProbeInit            sadProbeInit        1
VAR     wfsCommands      sadProbePark            sadProbePark        1
VAR     wfsCommands      sadProbeState           sadProbeState       1
VAR     wfsCommands      sadProbeX               sadProbeX           1
VAR     wfsCommands      sadProbeY               sadProbeY           1
VAR     wfsCommands      sadHealth               sadHealth           1
VAR     wfsCommands      sadState                sadState            1

VAR     baseDevice       deviceAcceleration      baseAccl            1
VAR     baseDevice       deviceAcknowledge       baseAck             1
VAR     baseDevice       deviceBacklashCompOffset baseBacklashOff    1
VAR     baseDevice       deviceBrakeDelay        baseBrkDelay        1
VAR     baseDevice       deviceBusy              baseBusy            1
VAR     baseDevice       deviceBaseVelocity      baseBVelo           1
VAR     baseDevice       deviceMotionDeadband    baseDeadband        1
VAR     baseDevice       deviceDebug             baseDebugLevel      1
VAR     baseDevice       deviceDeviceType        baseDevType         1
VAR     baseDevice       deviceDirective         baseDir             1
VAR     baseDevice       deviceEncoderDeadband   baseEncDeadband     1
VAR     baseDevice       deviceEncoderResolution baseEncRes          1
VAR     baseDevice       deviceFinalIndexVelocity baseFIVelo         1
VAR     baseDevice       deviceHomePositionOffset  baseHomeOff       1
VAR     baseDevice       deviceIndexType         baseIalg            1
VAR     baseDevice       deviceIndexed           baseIndexed         1
VAR     baseDevice       deviceLUTDirectory      baseLUTDir          1
VAR     baseDevice       deviceLUTFile           baseLUTFile         1
VAR     baseDevice       deviceMaxPosition       baseMaxTarget       1
VAR     baseDevice       deviceMaxVelocity       baseMaxVelo         1
VAR     baseDevice       deviceMessage           baseMess            1
VAR     baseDevice       deviceMinPosition       baseMinTarget       1
VAR     baseDevice       deviceMinVelocity       baseMinVelo         1
VAR     baseDevice       deviceMode              baseMode            1
VAR     baseDevice       deviceMotorResolution   baseMotorRes        1
VAR     baseDevice       devicePosition          basePos             1
VAR     baseDevice       devicePowerDelay        basePwrDelay        1
VAR     baseDevice       deviceRounding          baseRound           1
VAR     baseDevice       deviceSimulation        baseSimLevel        1
VAR     baseDevice       deviceState             baseState           1
VAR     baseDevice       deviceTarget            baseTarget          1
VAR     baseDevice       deviceUseAuxPower       baseUseAuxPwr       1
VAR     baseDevice       deviceUseBrakeStatus    baseUseBrkStat      1
VAR     baseDevice       deviceUseEncoder        baseUseEncoder      1
VAR     baseDevice       deviceUsePowerStatus    baseUsePwrStat      1
VAR     baseDevice       deviceVelocity          baseVelo            1

VAR     pickoffDevice    devicePosition          pickoffPos          1
VAR     pickoffDevice    deviceDirective         pickoffDir          1
VAR     pickoffDevice    deviceMode              pickoffMode         1
VAR     pickoffDevice    deviceTarget            pickoffTarget       1
VAR     pickoffDevice    deviceAcknowledge       pickoffAck          1
VAR     pickoffDevice    deviceBusy              pickoffBusy         1
VAR     pickoffDevice    deviceMessage           pickoffMess         1
VAR     pickoffDevice    deviceHomePositionOffset  pickoffHomeOff    1
VAR     pickoffDevice    deviceVelocity          pickoffVelo         1
VAR     pickoffDevice    deviceAcceleration      pickoffAccl         1
VAR     pickoffDevice    deviceBaseVelocity      pickoffBVelo        1
VAR     pickoffDevice    deviceFinalIndexVelocity pickoffFIVelo      1
VAR     pickoffDevice    deviceIndexType         pickoffIalg         1
VAR     pickoffDevice    deviceMinPosition       pickoffMinTarget    1
VAR     pickoffDevice    deviceMaxPosition       pickoffMaxTarget    1
VAR     pickoffDevice    deviceMinVelocity       pickoffMinVelo      1
VAR     pickoffDevice    deviceMaxVelocity       pickoffMaxVelo      1
VAR     pickoffDevice    deviceSimulation        pickoffSimLevel     1
VAR     pickoffDevice    deviceDebug             pickoffDebugLevel   1
VAR     pickoffDevice    deviceDeviceType        pickoffDevType      1
VAR     pickoffDevice    deviceLUTFile           pickoffLUTFile      1
VAR     pickoffDevice    deviceLUTDirectory      pickoffLUTDir       1
VAR     pickoffDevice    deviceIndexed           pickoffIndexed      1
VAR     pickoffDevice    deviceState             pickoffState        1
VAR     pickoffDevice    deviceMotorResolution   pickoffMotorRes     1
VAR     pickoffDevice    deviceEncoderResolution pickoffEncRes       1
VAR     pickoffDevice    deviceBacklashCompOffset pickoffBacklashOff 1
VAR     pickoffDevice    deviceMotionDeadband    pickoffDeadband     1
VAR     pickoffDevice    deviceEncoderDeadband   pickoffEncDeadband  1
VAR     pickoffDevice    deviceRounding          pickoffRound        1
VAR     pickoffDevice    devicePowerDelay        pickoffPwrDelay     1
VAR     pickoffDevice    deviceBrakeDelay        pickoffBrkDelay     1
VAR     pickoffDevice    deviceUseEncoder        pickoffUseEncoder   1
VAR     pickoffDevice    deviceUseBrakeStatus    pickoffUseBrkStat   1
VAR     pickoffDevice    deviceUsePowerStatus    pickoffUsePwrStat   1
VAR     pickoffDevice    deviceUseAuxPower       pickoffUseAuxPwr    1

#***********************************************************

#
# Is it safe to proceed?
#

PROMPT "Is it safe to move the OIWFS probe"

#
# Verify initial configuration - SAD.
#

TEST "Verify initial configuration - SAD"
FATAL

DESC "Must be no GIS demand (or Kill switch) activated"
GETVAL wfsProbeGis
OUTPUT *.VALI* 0*

DESC "Probe assembly GUI emergency STOP button must NOT be activated"
GETVAL wfsProbeEstop
OUTPUT *.E * 0*

DESC "Base GUI emergency STOP button must NOT be activated"
GETVAL wfsProbeBaseEstop
OUTPUT *.VAL * 0*

DESC "Pickoff GUI emergency STOP button must NOT be activated"
GETVAL wfsProbePickoffEstop
OUTPUT *.VAL * 0*

ENDTEST

#
# Verify SIR records
#

TEST "Verify SIR's"
FATAL

DESC "Check controller name"
GETVAL sadName
OUTPUT *.VAL * F2 OPC Cerro Pachon*

DESC "Probe must be initialized"
GETVAL sadProbeInit
OUTPUT *.VAL * 1*

DESC "Probe must not be indexed"
GETVAL sadProbeIndex
OUTPUT *.VAL * 0* 

DESC "Probe must not be parked"
GETVAL sadProbePark
OUTPUT *.VAL * 0* 

DESC "Check for inPosition record"
GETVAL sadInPosition
OUTPUT *.VAL *

DESC "Check for arrayS record"
GETVAL sadArrayS
OUTPUT *.VAL *

DESC "Check followS record, should NOT be following"
GETVAL sadFollowS
OUTPUT *.VAL * 0*

DESC "Check for present record"
GETVAL sadPresent
OUTPUT *.VAL *

DESC "Check for probeX record"
GETVAL sadProbeX
OUTPUT *.VAL *

DESC "Check for probeY record"
GETVAL sadProbeY
OUTPUT *.VAL *

DESC "Health must be GOOD"
GETVAL sadHealth
OUTPUT *.VAL * GOOD*

DESC "OPC State must be RUNNING"
GETVAL sadState
OUTPUT *.VAL * RUNNING*

DESC "Check DHS historyLog record"
GETVAL sadHistoryLog
#OUTPUT *.VAL * OPC IOC startup script finished!*
OUTPUT *.VAL *

ENDTEST

#
# Verify CAR records
#

TEST "Verify CAR's"
FATAL

DESC "Probe CAR must be IDLE"
GETVAL sadProbeCar
OUTPUT *.VAL * IDLE* 

DESC "activeC CAR must be IDLE"
GETVAL wfsActiveC
OUTPUT *.VAL * IDLE* 

DESC "rebootC CAR must be IDLE"
GETVAL f2SeqRebootC
OUTPUT *.VAL * IDLE* 

ENDTEST

#
# Verify CAD records
#

TEST "Verify CAD's"
FATAL

DESC "Datum CAD"
GETVAL tcsDatumAck
OUTPUT *.VAL *

DESC "Debug CAD"
GETVAL tcsDebugAck
OUTPUT *.VAL *

DESC "Follow CAD"
GETVAL tcsFollowAck
OUTPUT *.VAL *

DESC "Init CAD"
GETVAL tcsInitAck
OUTPUT *.VAL *

DESC "Move CAD"
GETVAL tcsMoveAck
OUTPUT *.VAL *

DESC "Park CAD"
GETVAL tcsParkAck
OUTPUT *.VAL *

DESC "Reboot CAD"
GETVAL f2SeqRebootAck
OUTPUT *.VAL *

DESC "Stop CAD"
GETVAL tcsStopAck
OUTPUT *.VAL *

DESC "Test CAD"
GETVAL tcsTestAck
OUTPUT *.VAL *

DESC "Tolerance CAD"
GETVAL tcsToleranceAck
OUTPUT *.VAL *

ENDTEST

#
# Verify initial configuration - probe assembly.
#

TEST "Verify initial configuration - probe assembly."
FATAL

DESC "WFS assembly health must be GOOD"
GETVAL wfsHealth
OUTPUT *.HLTH * GOOD*

DESC "WFS assembly must be initialized"
GETVAL wfsInitialized
OUTPUT *.INIT * 1*

DESC "WFS assembly must NOT be indexed"
GETVAL wfsIndexed
OUTPUT *.INDX * 0*

DESC "WFS assembly must NOT be parked"
GETVAL wfsParked
OUTPUT *.PARK * 0* 

DESC "WFS assembly must be IDLE"
GETVAL wfsState
OUTPUT *.ASTA * IDLE* 

DESC "Assembly device layer type must be Oiwfs"
GETVAL wfsDevType
OUTPUT *.DTYP * Oiwfs*

DESC "Number of devices connected to assembly must be 2"
GETVAL wfsNumDev
OUTPUT *.NMDV * 2*

DESC "Assembly simulation level must be NONE"
GETVAL wfsSimulation
OUTPUT *.SIMM * NONE*

DESC "Assembly debug level must be NONE"
GETVAL wfsDebug
OUTPUT *.DBUG * NONE*

ENDTEST

#
# Verify initial configuration - base stage.
#

TEST "Verify initial configuration - base stage."
FATAL

DESC "Base indexing algorithm must be LOWHOME"
GETVAL baseIalg
OUTPUT *.IALG * LOWHOME*

DESC "Base simulation level must be NONE"
GETVAL baseSimLevel
#OUTPUT *.SIMM * NONE*
OUTPUT *.SIMM *

DESC "Base debug level must be NONE"
GETVAL baseDebugLevel
OUTPUT *.DBUG * NONE*

DESC "Base device layer type must be DEV CTL OMS 58"
GETVAL baseDevType
OUTPUT *.DTYP * DEV CTL OMS 58*

DESC "Base must not be indexed"
GETVAL baseIndexed
OUTPUT *.HPVL * 0*

DESC "Base must be the STOPPED state"
GETVAL baseState
OUTPUT *.MIP * STOPPED*

DESC "Base must use an encoder"
GETVAL baseUseEncoder
OUTPUT *.UEIP * YES*

DESC "Base cannot use brake status"
GETVAL baseUseBrkStat
OUTPUT *.UBSB * NO*

DESC "Base does not use power status"
GETVAL baseUsePwrStat
OUTPUT *.UPSB * NO*

DESC "Base must use the auxilliary power bit"
GETVAL baseUseAuxPwr
OUTPUT *.UAPB * YES*

ENDTEST

#
# Verify initial configuration - pickoff stage.
#

TEST "Verify initial configuration - pickoff stage."
FATAL

DESC "Pickoff indexing algorithm must be LOWHOME"
GETVAL pickoffIalg
OUTPUT *.IALG * LOWHOME*

DESC "Pickoff simulation level must be NONE"
GETVAL pickoffSimLevel
#OUTPUT *.SIMM * NONE*
OUTPUT *.SIMM *

DESC "Pickoff debug level must be NONE"
GETVAL pickoffDebugLevel
OUTPUT *.DBUG * NONE*

DESC "Pickoff device layer type must be DEV CTL OMS 58"
GETVAL pickoffDevType
OUTPUT *.DTYP * DEV CTL OMS 58*

DESC "Pickoff must not be indexed"
GETVAL pickoffIndexed
OUTPUT *.HPVL * 0*

DESC "Pickoff must be the STOPPED state"
GETVAL pickoffState
OUTPUT *.MIP * STOPPED*

DESC "Pickoff must use an encoder"
GETVAL pickoffUseEncoder
OUTPUT *.UEIP * YES*

DESC "Pickoff cannot use brake status"
GETVAL pickoffUseBrkStat
OUTPUT *.UBSB * NO*

DESC "Pickoff does not use power status"
GETVAL pickoffUsePwrStat
OUTPUT *.UPSB * NO*

DESC "Pickoff must use the auxilliary power bit"
GETVAL pickoffUseAuxPwr
OUTPUT *.UAPB * YES*

ENDTEST

#
# Log the remaining assembly configuration settings.
#

TEST "Log remaining assembly configuration settings"
FATAL

DESC "Get default stage velocity value"
GETVAL wfsStageVelo
OUTPUT *.D * 

DESC "Get minimum stage velocity value"
GETVAL wfsStageMinVelo
OUTPUT *.ADLL* 

DESC "Get maximum stage velocity value"
GETVAL wfsStageMaxVelo
OUTPUT *.ADHL* 

DESC "Get X calibration offset"
GETVAL wfsXOffset
OUTPUT *.VAL *

DESC "Get Y calibration offset"
GETVAL wfsYOffset
OUTPUT *.VAL * 

DESC "Get Z calibration offset"
GETVAL wfsZOffset
OUTPUT *.VAL * 

DESC "Get base stage calibration offset"
GETVAL wfsBasOffset
OUTPUT *.VAL * 

DESC "Get pickoff stage calibration offset"
GETVAL wfsPkoOffset
OUTPUT *.VAL * 

ENDTEST

#
# Log the remaining Base stage configuration settings.
#

TEST "Log remaining Base stage configuration settings"
FATAL

DESC "Get default base stage velocity"
GETVAL baseVelo
OUTPUT *.VELO *

DESC "Get base stage acceleration"
GETVAL baseAccl
OUTPUT *.ACCL *

DESC "Get base stage startup velocity"
GETVAL baseBVelo
OUTPUT *.VBAS *

DESC "Get base stage final indexing velocity"
GETVAL baseFIVelo
OUTPUT *.FIVL *

DESC "Get base stage minimum target position"
GETVAL baseMinTarget
OUTPUT *.PLLM *

DESC "Get base stage maximum target position"
GETVAL baseMaxTarget
OUTPUT *.PHLM *

DESC "Get base stage minimum target velocity"
GETVAL baseMinVelo
OUTPUT *.VLLM *

DESC "Get base stage maximum target velocity"
GETVAL baseMaxVelo
OUTPUT *.VHLM *

DESC "Get base stage lookup table filename"
GETVAL baseLUTFile
OUTPUT *.TFIL *

DESC "Get base stage lookup table file directory"
GETVAL baseLUTDir
OUTPUT *.TDIR *

DESC "Get base stage motor resolution"
GETVAL baseMotorRes
OUTPUT *.MRES *

DESC "Get base stage encoder resolution"
GETVAL baseEncRes
OUTPUT *.ERES *

DESC "Get base stage backlash compensation offset"
GETVAL baseBacklashOff
OUTPUT *.BLCO *

DESC "Get base stage motion deadband"
GETVAL baseDeadband
OUTPUT *.MDBD *

DESC "Get base stage encoder deadband"
GETVAL baseEncDeadband
OUTPUT *.EDBD *

DESC "Get base stage step rounding factor"
GETVAL baseRound
OUTPUT *.MRND *

DESC "Get base stage power delay"
GETVAL basePwrDelay
OUTPUT *.PTMO *

DESC "Get base stage brake delay"
GETVAL baseBrkDelay
OUTPUT *.BTMO *

DESC "Get the base stage home position offset"
GETVAL baseHomeOff
OUTPUT *.HPO *

ENDTEST

#
# Log the remaining Pickoff stage configuration settings.
#

TEST "Log remaining Pickoff stage configuration settings"
FATAL

DESC "Get default pickoff stage velocity"
GETVAL pickoffVelo
OUTPUT *.VELO *

DESC "Get pickoff stage acceleration"
GETVAL pickoffAccl
OUTPUT *.ACCL *

DESC "Get pickoff stage startup velocity"
GETVAL pickoffBVelo
OUTPUT *.VBAS *

DESC "Get pickoff stage final indexing velocity"
GETVAL pickoffFIVelo
OUTPUT *.FIVL *

DESC "Get pickoff stage minimum target position"
GETVAL pickoffMinTarget
OUTPUT *.PLLM *

DESC "Get pickoff stage maximum target position"
GETVAL pickoffMaxTarget
OUTPUT *.PHLM *

DESC "Get pickoff stage minimum target velocity"
GETVAL pickoffMinVelo
OUTPUT *.VLLM *

DESC "Get pickoff stage maximum target velocity"
GETVAL pickoffMaxVelo
OUTPUT *.VHLM *

DESC "Get pickoff stage lookup table filename"
GETVAL pickoffLUTFile
OUTPUT *.TFIL *

DESC "Get pickoff stage lookup table file directory"
GETVAL pickoffLUTDir
OUTPUT *.TDIR *

DESC "Get pickoff stage motor resolution"
GETVAL pickoffMotorRes
OUTPUT *.MRES *

DESC "Get pickoff stage encoder resolution"
GETVAL pickoffEncRes
OUTPUT *.ERES *

DESC "Get pickoff stage backlash compensation offset"
GETVAL pickoffBacklashOff
OUTPUT *.BLCO *

DESC "Get pickoff stage motion deadband"
GETVAL pickoffDeadband
OUTPUT *.MDBD *

DESC "Get pickoff stage encoder deadband"
GETVAL pickoffEncDeadband
OUTPUT *.EDBD *

DESC "Get pickoff stage step rounding factor"
GETVAL pickoffRound
OUTPUT *.MRND *

DESC "Get pickoff stage power delay"
GETVAL pickoffPwrDelay
OUTPUT *.PTMO *

DESC "Get pickoff stage brake delay"
GETVAL pickoffBrkDelay
OUTPUT *.BTMO *

DESC "Get the pickoff stage home position offset"
GETVAL pickoffHomeOff
OUTPUT *.HPO *

ENDTEST

#
# Test TCS init command
#

TEST "Test TCS init command"
FATAL

DESC "Initializing OPC"

DESC "Start the INIT command and wait for activeC car to finish"
CMD "executeCommand tcsInitDir tcsInitAck wfsActiveC"
OUTPUT *Success*

ENDTEST

#
# Verify state after command
#

TEST "Verify state after command"
FATAL

DESC "Verify state after command"

DESC "Give it a second to update the SAD"
CMD "sleep 1"
OUTPUT *

GETVAL sadProbeState
OUTPUT *.ASTA * IDLE* 

GETVAL sadProbeInit
OUTPUT *.VAL * 1* 

GETVAL sadProbeIndex
OUTPUT *.VAL * 0*

GETVAL sadProbePark
OUTPUT *.VAL * 0* 

GETVAL sadHealth
OUTPUT *.VAL * GOOD* 

GETVAL sadFollowS
OUTPUT *.VAL * 0*

ENDTEST

#
# Test TCS test command
#

TEST "Test TCS test command"
FATAL

DESC "Start the test command and wait for activeC car to finish"
CMD "executeCommand tcsTestDir tcsTestAck wfsActiveC"
OUTPUT *Success*

ENDTEST

#
# Verify state after command
#

TEST "Verify state after command"
FATAL

DESC "Verify state after command"

DESC "Give it a second to update the SAD"
CMD "sleep 1"
OUTPUT *

GETVAL sadProbeState
OUTPUT *.ASTA * IDLE* 

GETVAL sadProbeInit
OUTPUT *.VAL * 1* 

GETVAL sadProbeIndex
OUTPUT *.VAL * 0*

GETVAL sadProbePark
OUTPUT *.VAL * 0* 

GETVAL sadHealth
OUTPUT *.VAL * GOOD* 

GETVAL sadFollowS
OUTPUT *.VAL * 0*

ENDTEST

#
# Test TCS datum command
#

TEST "Test TCS datum command"
FATAL

DESC "Start the datum command"
CMD "startCommand tcsDatumDir tcsDatumAck wfsActiveC"
OUTPUT *Started*

DESC "Check the state"
CMD "checkState wfsState INDEXING"
OUTPUT *Success*

DESC "Check the Car"
GETVAL wfsActiveC
OUTPUT *.VAL * BUSY*

DESC "Look for successful completion"
CMD "finishCommand wfsActiveC"
OUTPUT *Success*


ENDTEST

#
# Verify state after command
#

TEST "Verify state after command"
FATAL

DESC "Verify state after command"

DESC "Give it a second to update the SAD"
CMD "sleep 1"
OUTPUT *

GETVAL sadProbeState
OUTPUT *.ASTA * IDLE* 

GETVAL sadProbeInit
OUTPUT *.VAL * 1* 

GETVAL sadProbeIndex
OUTPUT *.VAL * 1*

GETVAL sadProbePark
OUTPUT *.VAL * 0* 

GETVAL sadHealth
OUTPUT *.VAL * GOOD* 

GETVAL sadFollowS
OUTPUT *.VAL * 0*

ENDTEST

#
# Log the actual XY coordinates
#

TEST "Log the actual XY coordinates"
FATAL

DESC "Log the current X position"
GETVAL sadProbeX
OUTPUT *.VAL *

DESC "Log the current Y position"
GETVAL sadProbeY
OUTPUT *.VAL *

ENDTEST


#
# Test TCS park command
#

TEST "Test TCS park command"
FATAL

DESC "Start the park command"
CMD "startCommand tcsParkDir tcsParkAck wfsActiveC"
OUTPUT *Started*

DESC "Check the state"
CMD "checkState wfsState MOVING"
OUTPUT *Success*

DESC "Check the Car"
GETVAL wfsActiveC
OUTPUT *.VAL * BUSY*

DESC "Look for successful completion"
CMD "finishCommand wfsActiveC"
OUTPUT *Success*


ENDTEST

#
# Verify state after command
#

TEST "Verify state after command"
FATAL

DESC "Verify state after command"

DESC "Give it a second to update the SAD"
CMD "sleep 1"
OUTPUT *

GETVAL sadProbeState
OUTPUT *.ASTA * IDLE* 

GETVAL sadProbeInit
OUTPUT *.VAL * 1* 

GETVAL sadProbeIndex
OUTPUT *.VAL * 1*

GETVAL sadProbePark
OUTPUT *.VAL * 1* 

GETVAL sadHealth
OUTPUT *.VAL * GOOD* 

GETVAL sadFollowS
OUTPUT *.VAL * 0*

ENDTEST

#
# Log the actual XY coordinates
#

TEST "Log the actual XY coordinates"
FATAL

DESC "Log the current X position"
GETVAL sadProbeX
OUTPUT *.VAL *

DESC "Log the current Y position"
GETVAL sadProbeY
OUTPUT *.VAL *

ENDTEST


#
# Test TCS debug command
#
# Places probe assemblyControl as well as probeBas and probePko deviceControl
# in the specified debugging mode.
# Monitor the ioc console
# Should accept names or numbers

TEST "Test TCS debug command"
FATAL

DESC "Select a debug mode of FULL"
PUTVAL tcsDebugTarget "FULL"
OUTPUT *debug.A * FULL*

DESC "Execute the debug command"
CMD "executeCommand tcsDebugDir tcsDebugAck wfsActiveC"
OUTPUT *Success*

DESC "Verify probe assembly debug level"
GETVAL wfsDebug
OUTPUT *.DBUG * FULL*

DESC "Verify base device debug level"
GETVAL baseDebugLevel
OUTPUT *.DBUG * FULL*

DESC "Verify pickoff device debug level"
GETVAL pickoffDebugLevel
OUTPUT *.DBUG * FULL*

DESC "Select a debug mode of MIN"
PUTVAL tcsDebugTarget "1"
OUTPUT *debug.A * 1*

DESC "Execute the debug command"
CMD "executeCommand tcsDebugDir tcsDebugAck wfsActiveC"
OUTPUT *Success*

DESC "Verify probe assembly debug level"
GETVAL wfsDebug
OUTPUT *.DBUG * NONE*

DESC "Verify base device debug level"
GETVAL baseDebugLevel
OUTPUT *.DBUG * NONE*

DESC "Verify pickoff device debug level"
GETVAL pickoffDebugLevel
OUTPUT *.DBUG * NONE*

ENDTEST

#
# Verify state after command
#

TEST "Verify state after command"
FATAL

DESC "Verify state after command"

DESC "Give it a second to update the SAD"
CMD "sleep 1"
OUTPUT *

GETVAL sadProbeState
OUTPUT *.ASTA * IDLE* 

GETVAL sadProbeInit
OUTPUT *.VAL * 1* 

GETVAL sadProbeIndex
OUTPUT *.VAL * 1*

GETVAL sadProbePark
OUTPUT *.VAL * 1* 

GETVAL sadHealth
OUTPUT *.VAL * GOOD* 

GETVAL sadFollowS
OUTPUT *.VAL * 0*

ENDTEST

#
# Log the actual XY coordinates
#

TEST "Log the actual XY coordinates"
FATAL

DESC "Log the current X position"
GETVAL sadProbeX
OUTPUT *.VAL *

DESC "Log the current Y position"
GETVAL sadProbeY
OUTPUT *.VAL *

ENDTEST



#
# Test TCS follow command
# 
# tcsSimStreamDisable 
# tcsSimXOrigin 
# tcsSimYOrigin 
# tcsSimPeriod 
# tcsSimRadius 
# tcsSimTrackID
# 
# wfsFollowDir
# wfsFollowAck
# wfsFollowMess
# wfsFollowCar
#
#  assumes the simulator is used, set it up and enable
# send follow command
#  check for activeC busy until target is reached first time


#
# Test TCS move command
#
# tcsMoveDir 
# wfsMoveAck
# wfsMoveMess
# wfsXTarget
# wfsYTarget
# wfsActiveC


#
# Test TCS stop command
#
# wfsStopDir
# wfsStopAck
# wfsStopMess 

#
# Test TCS tolerance command
#
# wfsToleranceDir
# wfsToleranceAck
# wfsToleranceMess
# wfsToleranceCar
# wfsToleranceTarget
# wfsToleranceLoLim
# wfsToleranceHiLim
# wfsToleranceVal


#
# Try entering invalid move target attributes using preset
#
#X=0 y=0 okay
#x=-20 fail
#x=-15 okay
#y=30 fail
#y=0 okay
#x=120 okay
#x=125 fail
#x=100 okay
#y=220 okay
#y=230 fail




#
# Move to center
#


#
# Test receipt of new target while already moving
#




#
# Un-Park the wfs assembly
#





COMMENT "OPC ICD check is OK"

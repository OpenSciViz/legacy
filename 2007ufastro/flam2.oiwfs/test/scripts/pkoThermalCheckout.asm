Flamingos-2 OIWFS pickoff stage thermal checkout.

THIS SCRIPT TAKES ABOUT 30 seconds TO COMPLETE.

Assumptions are ....

        * The IOC has been depowered and re-started cold, or the 
          IOC has been rebooted.

        * The Flamingos-2 components control database has been loaded.

        * There is nothing obstructing the OIWFS probe arm.
        * Pickoff stage is indexed and sitting at 0 (see pkoThermalSetup.scr)

	If so, it should be safe to move the probe arm....

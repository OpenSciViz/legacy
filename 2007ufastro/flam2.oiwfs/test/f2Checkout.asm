
Carry out automated checks on all Flamingos-2 OIWFS components
--------------------------------------------------------------

This script makes the following assumptions:

* The Flamingos-2 OIWFS Probe Controller database is loaded and ready for use.

* It is safe to move the OIWFS Probe.



IOC Boot Parameters
===================


F2-S > bootChange

'.' = clear field;  '-' = go to previous field;  ^D = quit

boot device          : dc0 
processor number     : 0 
host name            : quadra-hme0-2 
file name            : /data/quadra/1/daoinsw/tools/tornado2.0.2/target/config/mv2700/vxWorks 
inet on ethernet (e) : 192.168.3.24:ffffff00 
inet on backplane (b): 
host inet (h)        : 192.168.3.54 
gateway inet (g)     : 
user (u)             : daoinsw 
ftp password (pw) (blank = use rsh): 
flags (f)            : 0x28 
target name (tn)     : f2ioc 
startup script (s)   : /data/quadra/1/daoinsw/dev/admin/bin/flam2.startup 
other (o)            : 

value = 0 = 0x0
F2-S > 

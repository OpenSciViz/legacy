package javaUFProtocol;

import java.io.*;
import java.util.*; 

public class UFFrameConfig extends UFTimeStamp
{
    public static final
	String rcsID = "$Name:  $ $Id: UFFrameConfig.java,v 1.12 2005/12/13 21:12:34 drashkin Exp $";

    // UFFrameConfig has no _values attribute either,
    // but it does have following attributes:

    public int width; public int height; public int depth;
    public int DMAcnt;
    public int frameGrabCnt;
    public int frameProcCnt;
    public int frameObsTotal;
    public int frameWriteCnt;
    public int frameSendCnt;
    public int coAdds;
    public int ChopBeam;
    public int SaveSet;
    public int NodBeam;
    public int NodSet;
    public int frameCoadds;
    public int chopCoadds;
    public int chopSettleFrms;
    public float frameTime;
    public float savePeriod;
    public int pixelSort; public int littleEnd;
    public int pixCnt; public int offset;
    public int bgADUs; public float bgWellpc; public float sigmaFrmNoise;
    public int rdADUs; public float rdWellpc; public float sigmaReadNoise;
    public int bgADUmin; public int bgADUmax; public float sigmaFrmMin;  public float sigmaFrmMax;
    public int rdADUmin; public int rdADUmax; public float sigmaReadMin; public float sigmaReadMax;

    protected int _Nelements = 37;

    private void _init() {
	_currentTime();
	_type = MsgTyp._FrameConfig_;
	_elem = _Nelements;
	_name = new String("UFFrameConfig");
	_length = _minLength() + _elem*4;
    }

    public UFFrameConfig() {
	_init();
    }

    public UFFrameConfig(int length) {
	_init();
	_length = length;
    }

    public UFFrameConfig(int w, int h) {
	_init();
	width = w;
	height = h;
    }

    public UFFrameConfig(int w, int h, int d) {
	_init();
	width = w;
	height = h;
	depth = d;
    }

    public UFFrameConfig(String name, int w, int h, int d) {
	this(w,h,d);
	_name = new String(name);
	_length = _minLength() + _elem*4;
    }

    public UFFrameConfig(int w, int h, int d, boolean little, int DMAcnt, int frameGrabCnt,
			 int coAdds, int pixelSort, int frameProcCnt, int frameObsTotal,
			 int ChopBeam, int SaveSet, int NodBeam, int NodSet) {
	this(w,h);
	this.depth=d; this.littleEnd = (little ? 1 : 0);
	this.DMAcnt = DMAcnt; this.frameGrabCnt = frameGrabCnt;
	this.coAdds = coAdds; this.pixelSort = pixelSort; this.frameProcCnt = frameProcCnt;
	this.frameObsTotal = frameObsTotal; this.ChopBeam = ChopBeam; this.SaveSet = SaveSet;
	this.NodBeam = NodBeam; this.NodSet = NodSet;
    }

    public String description() { return("UFFrameConfig"); }

    public int getUpdatedBuffNames(Vector bufnames, String delim){
	StringTokenizer st = new StringTokenizer(_name,delim);
	int i=0;
	bufnames.clear();
	while (st.hasMoreTokens()) {
	    bufnames.add( new String(st.nextToken()));
	}
	return bufnames.size();
    }

    public int getUpdatedBuffNames(Vector bufnames){
	return getUpdatedBuffNames(bufnames,"^");
    }

    // chop/nod buffers in the form "instrum:bufname" -- for use in client
    public int getUpdatedBuffNames( String instrum, Vector bufnames,  String delim){
	int n = getUpdatedBuffNames(bufnames, delim);
	if (n <= 0)
	    return 0;

	for (int i=0; i<n; ++i)
	    bufnames.set(i,new String(instrum + ":" + (String)bufnames.get(i)));

	return n;
    }

    public int getUpdatedBuffNames( String instrum, Vector bufnames){
	return getUpdatedBuffNames(instrum,bufnames,"^");
    }

    // set the names -- for use in server
    public int setUpdatedBuffNames( String[] bufnames,  String delim){
	int cnt = bufnames.length;
	if (cnt <= 0)
	    return 0;
	String newname = bufnames[0];
	for (int i=1; i<cnt; i++)
	    newname += delim + bufnames[i];
	_length -= _name.length();
	_name = new String(newname);
	_length += _name.length();
	return cnt;
    }

    public int setUpdatedBuffNames( String[] bufnames){
	return setUpdatedBuffNames(bufnames,"^");
    }

    public int setUpdatedBuffNames(Vector bufnames) {
	return setUpdatedBuffNames((String[])bufnames.toArray(),"^");
    }

    public int setUpdatedBuffNames(Vector bufnames, String delim) {
	return setUpdatedBuffNames((String[])bufnames.toArray(),delim);
    }

    public int recvData(DataInputStream inp) {
	// read data values (length, type, and header have already been read)
	int nbytes=0;
	try {
	    if (_elem != _Nelements) {
		System.err.println("Bad # elements for FrameConfig object: expected: "
				   + _Nelements + ", received " + _elem);
		if( _elem < _Nelements )
		    return (-1);
		else System.err.println("Excess data will be discarded.");
	    }
	    //elements must be recvd in following order:
	    this.width          = inp.readInt(); 
	    this.height         = inp.readInt(); 
	    this.depth          = inp.readInt(); 
	    this.littleEnd      = inp.readInt();
	    this.DMAcnt         = inp.readInt(); 
	    this.frameGrabCnt   = inp.readInt(); 
	    this.coAdds         = inp.readInt();
	    this.pixelSort      = inp.readInt(); 
	    this.frameProcCnt   = inp.readInt(); 
	    this.frameObsTotal  = inp.readInt();
	    this.ChopBeam       = inp.readInt(); 
	    this.SaveSet        = inp.readInt(); 
	    this.NodBeam        = inp.readInt();
	    this.NodSet         = inp.readInt(); 
	    this.frameWriteCnt  = inp.readInt(); 
	    this.frameSendCnt   = inp.readInt();
	    this.bgADUs         = inp.readInt(); 
	    this.bgWellpc       = inp.readFloat(); 
	    this.sigmaFrmNoise  = inp.readFloat();
	    this.rdADUs         = inp.readInt(); 
	    this.rdWellpc       = inp.readFloat(); 
	    this.sigmaReadNoise = inp.readFloat();
	    this.frameCoadds    = inp.readInt(); 
	    this.chopSettleFrms = inp.readInt(); 
	    this.chopCoadds     = inp.readInt();
	    this.frameTime      = inp.readFloat(); 
	    this.savePeriod     = inp.readFloat();
	    this.offset         = inp.readInt();
	    this.pixCnt         = inp.readInt();
	    this.bgADUmin       = inp.readInt();
	    this.bgADUmax       = inp.readInt();
	    this.sigmaFrmMin    = inp.readFloat();
	    this.sigmaFrmMax    = inp.readFloat();
	    this.rdADUmin       = inp.readInt();
	    this.rdADUmax       = inp.readInt();
	    this.sigmaReadMin   = inp.readFloat();
	    this.sigmaReadMax   = inp.readFloat();
	    nbytes = _Nelements*4;
	}
	catch(EOFException eof) {
	    System.err.println("UFFrameConfig::recvData> "+eof.toString());
	} 
	catch(IOException ioe) {
	    System.err.println("UFFrameConfig::recvData> "+ioe.toString());
	}
	catch( Exception e ) {
	    System.err.println("UFFrameConfig::recvData> "+e.toString());
	}
	return nbytes;
    }

    public int sendData(DataOutputStream out) {
	// write out data values (header already sent):
	int nbytes=0;
	try {
	    //elements must be sent in following order:
	    out.writeInt(this.width); out.writeInt(this.height); out.writeInt(this.depth); 
	    out.writeInt(this.littleEnd);
	    out.writeInt(this.DMAcnt); out.writeInt(this.frameGrabCnt); out.writeInt(this.coAdds);
	    out.writeInt(this.pixelSort);
	    out.writeInt(this.frameProcCnt); 
	    out.writeInt(this.frameObsTotal);
	    out.writeInt(this.ChopBeam);
	    out.writeInt(this.SaveSet);
	    out.writeInt(this.NodBeam);
	    out.writeInt(this.NodSet);
	    out.writeInt(this.frameWriteCnt);
	    out.writeInt(this.frameSendCnt);
	    out.writeInt(this.bgADUs); out.writeFloat(this.bgWellpc); out.writeFloat(this.sigmaFrmNoise);
	    out.writeInt(this.rdADUs); out.writeFloat(this.rdWellpc); out.writeFloat(this.sigmaReadNoise);
	    out.writeInt(this.frameCoadds);
	    out.writeInt(this.chopSettleFrms); 
	    out.writeInt(this.chopCoadds);
	    out.writeFloat(this.frameTime);
	    out.writeFloat(this.savePeriod);
	    out.writeInt(this.offset);
	    out.writeInt(this.pixCnt);
	    out.writeInt(this.bgADUmin);
	    out.writeInt(this.bgADUmax);
	    out.writeFloat(this.sigmaFrmMin);
	    out.writeFloat(this.sigmaFrmMax);
	    out.writeInt(this.rdADUmin);
	    out.writeInt(this.rdADUmax);
	    out.writeFloat(this.sigmaReadMin);
	    out.writeFloat(this.sigmaReadMax);
	    nbytes = _Nelements*4;
	}
	catch(EOFException eof) {
	    System.err.println("UFFrameConfig::sendData> "+eof.toString());
	} 
	catch(IOException ioe) {
	    System.err.println("UFFrameConfig::sendData> "+ioe.toString());
	}
	catch( Exception e ) {
	    System.err.println("UFFrameConfig::sendData> "+e.toString());
	}
	return nbytes;
  }
}
    
    

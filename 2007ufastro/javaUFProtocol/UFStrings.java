package javaUFProtocol;

import java.io.*;

public class UFStrings extends UFTimeStamp
{
    public static final
	String rcsID = "$Name:  $ $Id: UFStrings.java,v 1.6 2005/03/10 23:23:41 varosi Exp $";

    // additional attributes & methods (beyond base class's):
    protected String[] _values=null;

    private void _init() {
	_currentTime();
	_type = MsgTyp._Strings_;
    }

    public UFStrings() {
	_init();
    }

    public UFStrings(int length) {
	_init();
	_length=length;
    }

    public UFStrings(String name) {
	_init();
	_name = name;
	_length= _minLength();
    }

    public UFStrings(String inName, String[] inValues) {
	_init();
	_values = inValues;
	_elem = inValues.length;
	_name = inName;
	int count = 0;
	for (int i = 0; i < inValues.length; i++) {
	    if (inValues[i] != null)
		count += inValues[i].length();
	    count += 4;  // add 4 for integer length of string
	}
	_length = _minLength() + count;
    }

    // all methods declared abstract by UFProtocal 
    // can be defined here
    public String description() { return new String("UFStrings"); }
 
    // return size of the element's value (not it's name!)
    // either string length, or sizeof(float), sizeof(frame):
    public int valSize(int elemIdx) { 
	if( elemIdx >= _values.length )
	    return 0;
	else
	    return _values[elemIdx].length();
    }

    // return number of Strings in object
    public int numVals() {
	return _values.length;
    }

    // return String at index passed in
    public String valData(int elemndx) {
	return _values[elemndx];
    }

    protected void _copyNameAndVals(String s, String[] vals) {
	_name = new String(s);
	_values = new String[vals.length];
	_elem = vals.length;
	_length = _minLength();
	for( int i=0; i<_elem; i++ ) {
	    _values[i] = new String(vals[i]);
	    _length += 4; // each string length is recorded in protocal
	    _length += _values[i].length();
	}
    }

    public void setNameAndVals(String s, String[] vals) {
	_name = new String(s);
	_values = new String[vals.length];
	_elem = vals.length;
	_length = _minLength();
	for( int i=0; i<_elem; i++ ) {
	    _values[i] = (vals[i]);
	    _length += 4; // each string length is recorded in protocal
	    _length += _values[i].length();
	}
    }

    public int recvData(DataInputStream inp) {
	// read data values (length, type, and header have already been read)
	int retval=0;
	try {
	    _values = new String[_elem];

	    for( int elem=0; elem<_elem; elem++ ) {
		int vallen = inp.readInt();
		retval += 4;
		byte[] valbuf = new byte[vallen];
		if( vallen > 0 ) {
		    inp.readFully(valbuf, 0, vallen);
		    _values[elem] = new String(valbuf);
		    retval += vallen;
		}
	    }
	}
	catch(EOFException eof) {
	    System.err.println("UFStrings::recvData> "+eof.toString());
	}
	catch(IOException ioe) {
	    System.err.println("UFStrings::recvData> "+ioe.toString());
	}
	catch( Exception e ) {
	    System.err.println("UFStrings::recvData> "+  e.toString());
	}
	return retval;
    }

    public int sendData(DataOutputStream out) {
	// send data values (length, type, and header have already been sent)
	int retval=0;
	int slen=0;
	try {
	    for( int elem=0; elem<_elem; elem++ ) {
		slen = _values[elem].length();
		out.writeInt(slen);
		retval += 4;
		if( slen > 0 ) {
		    out.writeBytes(_values[elem]);
		    retval += slen; 
		}
	    }
	}
	catch(EOFException eof) {
	    System.err.println("UFStrings::sendData"+eof.toString());
	} 
	catch(IOException ioe) {
	    System.err.println("UFStrings::sendData"+ioe.toString());
	}
	catch( Exception e ) {
	    System.err.println("UFStrings::sendData"+  e.toString());
	}
	return retval;
    }

    public String toString() {
	StringBuffer sb = new StringBuffer();

	sb.append("_length = " + _length + " | ");
	sb.append("_minlength() = " + _minLength() + " | ");
	sb.append("_type = " + _type + " | ");
	sb.append("_timestamp = " + _timestamp + "\n");
	sb.append("_elem = " + _elem + " | ");
	sb.append("_name = " +  _name + " | ");
        sb.append("_values =\n");
	if (_values != null)
	    for (int i = 0; i < _values.length; i++) {
		sb.append(_values[i] + "\n");
	    }

	return new String(sb);
    }

}

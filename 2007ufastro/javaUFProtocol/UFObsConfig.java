package javaUFProtocol;

import java.util.*; 

public class UFObsConfig extends UFShorts
{
    public static final
	String rcsID = "$Name:  $ $Id: UFObsConfig.java,v 1.16 2005/08/11 18:28:22 drashkin Exp $";

    public class ObsConfig { 
	int NodBeams;
	int ChopBeams;
	int SaveSets;
	int NodSets;
	int CoaddsPerFrm;
	String ReadoutMode = new String();
	String NodPattern = new String();
    }

    protected ObsConfig _obsConfig = null;

    protected static Vector _frameBufnames = new Vector();
    protected static Hashtable _frameBufs = new Hashtable();

    // Bit mask code for defining/extracting buffer sequencing information:

    protected static class BitMaskCode {
	static final int BMC_RAW        =    3;
	static final int BMC_CLR        =    4;
	static final int BMC_D1         =    8;
	static final int BMC_D2         =   16;
	static final int BMC_D1A        =   32;
	static final int BMC_D2A        =   64;
	static final int BMC_SG         =  128;
	static final int BMC_NOD_BEAM   =  256;
	static final int BMC_CHOP_BEAM  =  512;
	static final int BMC_NOD_TOGGLE = 1024;	
    }

    private void _init() {
	_currentTime();
	_type = MsgTyp._ObsConfig_;
    }

    public UFObsConfig() { 
	_init();
    }

    public UFObsConfig(int length) {
	_init();
	_length = length;
    }

    public UFObsConfig(int nb, int cb, int ss, int ns, int cpf) {
	_init();
	_initBufSeqFlags( nb, cb, ss, ns, cpf, "A-BB-A");
    }

    public UFObsConfig(int nb, int cb, int ss, int ns, int cpf, String nodPattern) {
	_init();
	_initBufSeqFlags( nb, cb, ss, ns, cpf, nodPattern );
    }

    public UFObsConfig(String name, int nb, int cb, int ss, int ns, int cpf) {
	_init();
	_name = new String(name);
	_initBufSeqFlags( nb, cb, ss, ns, cpf, "A-BB-A" );
    }

    //UFShorts ctor makes use of shallow initializations
    public UFObsConfig(UFShorts ufs) {
	_init();
	_timestamp = new String(ufs._timestamp);
	_elem = ufs._elem;
	_name = new String(ufs.name());
	_length = _minLength() + _elem*2;
	_seqCnt = ufs._seqCnt;
	_seqTot = ufs._seqTot;
	_duration = ufs._duration;
	_values = ufs._values;
    }

    public UFObsConfig(UFObsConfig ufoc) {
	_init();
	_timestamp = new String(ufoc._timestamp);
	_elem = ufoc._elem; // should be increment to >= 1 
	_name = new String(ufoc.name());
	_length = _minLength() + _elem*2;
	_seqCnt = ufoc._seqCnt;
	_seqTot = ufoc._seqTot;
	_duration = ufoc._duration;
	_values = ufoc._values;
    }

    //Compute vector of buffer sequencing flags (the nominal values of this object):

    protected void _initBufSeqFlags(int nb, int cb, int ss, int ns, int cpf, String nodPattern)
    {
	int totalFrames = nb*cb*ss*ns;
	_elem = totalFrames;

	if( totalFrames <= 0 ) {
	    System.err.println("UFObsConfig::_initBufSeqFlags> totalFrames = " + totalFrames +
			       ", bad input args.:" + nb + "," + cb + "," + ss + "," + ns );
	    return;
	}

	_obsConfig = new ObsConfig();
	_obsConfig.NodBeams = nb;
	_obsConfig.ChopBeams = cb;
	_obsConfig.SaveSets = ss;
	_obsConfig.NodSets = ns;
	_obsConfig.CoaddsPerFrm = cpf;
	_obsConfig.ReadoutMode = "S1";
	_obsConfig.NodPattern = nodPattern;

	_initname();
	setConfig( _obsConfig ); // this resets the name

	short[] pBufSeq = new short[_elem];
	_values = pBufSeq;
	_length = _minLength() + _elem * 2;
	int totFrameNodBeam = _obsConfig.ChopBeams * _obsConfig.SaveSets;
	int totFrameNodSet = _obsConfig.NodBeams * totFrameNodBeam;

	for( int FrameIndex=0; FrameIndex < totalFrames; FrameIndex++ ) {
	    int FrameCount = 1 + FrameIndex;
	    int SaveSet = 1 + ((FrameIndex / _obsConfig.ChopBeams) % _obsConfig.SaveSets);
	    int NodBeam = (FrameIndex/totFrameNodBeam) % _obsConfig.NodBeams;
	    int ChpBeam = FrameIndex % _obsConfig.ChopBeams;

	    if( _obsConfig.NodPattern.indexOf("A-BB-A") >= 0 && _obsConfig.NodBeams == 2 )
		NodBeam = ( NodBeam + (FrameIndex/totFrameNodBeam/2) % 2 ) % 2;

	    //Initialize buffer sequence vector with buffer index:
	    pBufSeq[FrameIndex] = (short)( NodBeam * _obsConfig.ChopBeams +
					   (FrameIndex % _obsConfig.ChopBeams) );
	    //Zero bit registers:
	    short BitClr = 0;
	    short BitD1  = 0;
	    short BitD2  = 0;
	    short BitD1a = 0;
	    short BitD2a = 0;
	    short BitSg  = 0;
	    short BitNB  = 0;
	    short BitCB  = 0;
	    short BitNT  = 0;
	    if (SaveSet == 1) BitClr = BitMaskCode.BMC_CLR;

	    // Compute Dif1 or Dif2? 
	    if ( (_obsConfig.ChopBeams == 2) && ((FrameCount % 2) == 0) ) {
		if (NodBeam == 0) BitD1 = BitMaskCode.BMC_D1;
		if (NodBeam == 1) BitD2 = BitMaskCode.BMC_D2;
	    }
	  
	    // Compute D1A ?
	    if (_obsConfig.ChopBeams == 2) {
		if (BitD1 != 0) BitD1a = BitMaskCode.BMC_D1A;
	    }
	    else if (_obsConfig.NodBeams == 2) {
		if (FrameCount % (2*_obsConfig.SaveSets) == 0) BitD1a = BitMaskCode.BMC_D1A;
	    }

	    if (_obsConfig.ChopBeams == 2) { // Compute D2A?
		if (BitD2 != 0) BitD2a = BitMaskCode.BMC_D2A;
	    }
	  
	    if (_obsConfig.NodBeams == 2) { // Compute SG?
		if ((FrameCount % totFrameNodSet) == 0) BitSg = BitMaskCode.BMC_SG;
	    }
	  
	    BitNB = (short)(NodBeam*BitMaskCode.BMC_NOD_BEAM); // Indicate nod beam index
	    BitCB = (short)(ChpBeam*BitMaskCode.BMC_CHOP_BEAM); // Indicate chop beam index
	  
	    if (_obsConfig.NodBeams == 2) {  // Set nod toggle?
		if ((FrameCount % totFrameNodBeam) == 0) BitNT = BitMaskCode.BMC_NOD_TOGGLE;
	    }

	    pBufSeq[FrameIndex] += (BitClr+BitD1+BitD2+BitD1a+BitD2a+BitSg+BitNB+BitCB+BitNT);
	}
    }

    public static void beams( String obsmode, int nb, int nc ) {
	nb = nc = 1; // default is Stare mode
	if( obsmode.toUpperCase().indexOf("CHOP") >= 0 &&
	    obsmode.toUpperCase().indexOf("NOD")  >= 0 ) { // Chop-Nod mode
	    nb = nc = 2;
	    return;
	}
	if( obsmode.toUpperCase().indexOf("CHOP") >= 0 ) { // Chop-only mode
	    nb = 1; nc = 2;
	    return;
	}
	if( obsmode.toUpperCase().indexOf("NOD")  >= 0 ) { // Nod-only mode
	    nb = 2; nc = 1;
	    return;
	}  
	//any other obsmode string reduces to Stare mode...
	return;
    }

    // estimated MCE4 EDT fiber transfer time in seconds (depends on readout mode)
    public float estXferTime() {
	int nfrmPerRdout = 1; // always at least one signal det. on sample.
	String readOutMode = readoutMode();
	if( readOutMode.indexOf("S1R1") != -1 ||
	    readOutMode.indexOf("1S1R") != -1 ||
	    readOutMode.indexOf("s1r1") != -1 ||
	    readOutMode.indexOf("1s1r") != -1 ) nfrmPerRdout = 2; //one ref sample.
	if( readOutMode.indexOf("S1R3") != -1 ||
	    readOutMode.indexOf("1S3R") != -1 ||
	    readOutMode.indexOf("s1r3") != -1 ||
	    readOutMode.indexOf("1s3r") != -1 ) nfrmPerRdout = 4; //three ref samples.
	return (float)(0.04 * nfrmPerRdout);
    }

    // fetch config substr
    protected String _config() {
	String nm = name();
	int posL = nm.indexOf("||");
	int posR = nm.lastIndexOf("||");
	if (posL == posR || posL == -1 || posR == -1) // poorly initialized object?
	    return("||?|?|?|?|?|?|?||");
	else
	    return nm.substring( posL, posR + 2 );
    }

    // fetch name substr
    protected String _name() {
	String nm = name();
	int posR = nm.lastIndexOf("||") + 2;
	if( posR != -1 && posR < nm.length() ) 
	    return nm.substring( posR );
	else 
	    return "?";
    }

    // insure _name is properly formatted
    protected String _initname(){
	String nm = name();
	int posL = nm.indexOf("|");
	if (posL != 0) { // poorly initialized object?
	    String c = "||?|?|?|?|?|?|?||";
	    rename( c + nm );
	}
	return name();
    }

    // override the inherited UFShorts description:
    public String description() { return new String("UFObsConfig"); }

    // convenience functions:
  
    public int totFrameCnt() { return _elem; }
  
    public ObsConfig getConfig() {
	ObsConfig ufc = new ObsConfig();
	String c = _config();
	c = c.substring( 2, c.lastIndexOf("||") );
	String[] params = c.split("\\|");
	if( params.length > 0 ) ufc.NodBeams =     Integer.parseInt( params[0] );
	if( params.length > 1 ) ufc.ChopBeams =    Integer.parseInt( params[1] );
	if( params.length > 2 ) ufc.SaveSets =     Integer.parseInt( params[2] );
	if( params.length > 3 ) ufc.NodSets =      Integer.parseInt( params[3] );
	if( params.length > 4 ) ufc.CoaddsPerFrm = Integer.parseInt( params[4] );
	if( params.length > 5 ) ufc.ReadoutMode =  params[5];
	if( params.length > 6 ) ufc.NodPattern =   params[6];
	return ufc;
    }

    public String setConfig( ObsConfig c ){
	setNodBeams(c.NodBeams);
	setChopBeams(c.ChopBeams);
	setSaveSets(c.SaveSets);
	setNodSets(c.NodSets);
	setCoaddsPerFrm(c.CoaddsPerFrm);
	return name();
    }

    public String obsMode() {
	if( _obsConfig == null ) _obsConfig = getConfig();
	if( _obsConfig.ChopBeams > 1 ) {
	    if( _obsConfig.NodBeams > 1 )
		return("chop-nod");
	    else
		return("chop");
	}
	else if( _obsConfig.NodBeams > 1 ) return("nod");
	else return("stare");
    }
    public int nodBeams() {
	if( _obsConfig == null ) _obsConfig = getConfig();
	return _obsConfig.NodBeams;
    }
    public int chopBeams() {
	if( _obsConfig == null ) _obsConfig = getConfig();
	return _obsConfig.ChopBeams;
    }
    public int saveSets() {
	if( _obsConfig == null ) _obsConfig = getConfig();
	return _obsConfig.SaveSets;
    }
    public int nodSets() {
	if( _obsConfig == null ) _obsConfig = getConfig();
	return _obsConfig.NodSets;
    }
    public int coaddsPerFrm() {
	if( _obsConfig == null ) _obsConfig = getConfig();
	return _obsConfig.CoaddsPerFrm;
    }
    public String readoutMode() {
	if( _obsConfig == null ) _obsConfig = getConfig();
	return _obsConfig.ReadoutMode;
    }
    public String nodPattern() {
	if( _obsConfig == null ) _obsConfig = getConfig();
	return _obsConfig.NodPattern;
    }

    public String dataLabel() {
	String nm = name();
	int posR = nm.lastIndexOf("||");
	if( posR != -1 && posR+2 < nm.length() )
	    return nm.substring(posR+2);
	else
	    return ("?");
    }

    // int fieldNum starts counting with zero:

    protected int _setCfgField(int fieldNum, String fieldValue) {
	String c = _config();
	int pos = 2 + c.indexOf("||");
	for( int i=0; i<fieldNum; i++ ) pos = 1 + c.indexOf('|', pos);
	String tmp = c.substring(0,pos) + fieldValue;
	pos = c.indexOf('|', pos);
	tmp += ( c.substring(pos) + _name() );   //concatenate remainder of config string
	rename( tmp );
	return tmp.length();
    }

    protected int _setCfgField(int fieldNum, int fieldValue) {
	String c = _config();
	int pos = 2 + c.indexOf("||");
	for( int i=0; i<fieldNum; i++ ) pos = 1 + c.indexOf('|', pos);
	String tmp = c.substring(0,pos) + fieldValue;
	pos = c.indexOf('|', pos);
	tmp += ( c.substring(pos) + _name() );   //concatenate remainder of config string
	rename( tmp );
	return tmp.length();
    }

    public int setNodBeams(int nb)  { return _setCfgField( 0, nb ); }
    public int setChopBeams(int cb) { return _setCfgField( 1, cb ); }
    public int setSaveSets(int ss)  { return _setCfgField( 2, ss ); }
    public int setNodSets(int ns)   { return _setCfgField( 3, ns ); }

    public int setCoaddsPerFrm(int cpf)        { return _setCfgField( 4, cpf );   }
    public int setReadoutMode(String rdout)    { return _setCfgField( 5, rdout ); }
    protected int setNodPattern(String nodpat) { return _setCfgField( 6, nodpat ); }

    public int reLabel( String newLabel){
	rename(_config() + newLabel);
	return name().length();
    }

  // evaluate/set bits:
  public int buffId(int frmIdx) {
      return (1 + ((short) (0x03 & _values[frmIdx])));
  }
  public boolean clearBuff(int frmIdx) {
      return (((short)0x04 & _values[frmIdx]) == 0 ? false : true);
  }
  public boolean doDiff1(int frmIdx) {
      return (((short)0x08 & _values[frmIdx]) == 0 ? false : true);
  }
  public boolean doDiff2(int frmIdx) {
      return (((short)0x10 & _values[frmIdx]) == 0 ? false : true);
  }
  public boolean doAccumDiff1(int frmIdx) {
      return (((short)0x20 & _values[frmIdx]) == 0 ? false : true);
  }
  public boolean doAccumDiff2(int frmIdx) {
      return (((short)0x40 & _values[frmIdx]) == 0 ? false : true);
  }
  public boolean doSig(int frmIdx) {
      return (((short)0x80 & _values[frmIdx]) == 0 ? false : true);
  }
  public int nodPosition(int frmIdx) {
      return (((short)0x100 & _values[frmIdx]) == 0 ? 0 : 1);
  }
  public int chopPosition(int frmIdx) {
      return (((short)0x200 & _values[frmIdx]) == 0 ? 0 : 1);
  }

  public int setBuffId(int id, int frmIdx){
      short idb = (short)(id-1); // assumes id > 0 !
      short bufId = (short)(0x03 & idb);
      _values[frmIdx] = (short) (bufId & _values[frmIdx]);
      return id;
  }
  //NOTE -- java booleans are 1 bit, c booleans are 32 bits
  public boolean setClear(boolean s, int frmIdx){
      short sb0 = (short) (0x01|0x02|0x08|0x10|0x20|0x40|0x80|0x100|0x200);
      short sb1 = (short) 0x04;
      if (s)
	  _values[frmIdx] = (short) (sb1 | _values[frmIdx]);
      else
	  _values[frmIdx] = (short) (sb0 & _values[frmIdx]);
      return true;
  }
  public boolean setDiff1(boolean s, int frmIdx){
      short sb0 = (short) (0x01|0x02|0x04|0x10|0x20|0x40|0x80|0x100|0x200);
      short sb1 = (short) 0x08;
      if (s)
	  _values[frmIdx] = (short) (sb1 | _values[frmIdx]);
      else
	  _values[frmIdx] = (short) (sb0 & _values[frmIdx]);
      return true;
  }
  public boolean setDiff2(boolean s, int frmIdx){
      short sb0 = (short) (0x01|0x02|0x04|0x08|0x20|0x40|0x80|0x100|0x200);
      short sb1 = (short) 0x10;
      if (s)
	  _values[frmIdx] = (short) (sb1 | _values[frmIdx]);
      else
	  _values[frmIdx] = (short) (sb0 & _values[frmIdx]);
      return true;
  }
  public boolean setAccumDiff1(boolean s, int frmIdx){
      short sb0 = (short) (0x01|0x02|0x04|0x08|0x10|0x40|0x80|0x100|0x200);
      short sb1 = (short) 0x20;
      if (s)
	  _values[frmIdx] = (short) (sb1 | _values[frmIdx]);
      else
	  _values[frmIdx] = (short) (sb0 & _values[frmIdx]);
      return true;
  }
  public boolean setAccumDiff2(boolean s, int frmIdx){
      short sb0 = (short) (0x01|0x02|0x04|0x08|0x10|0x20|0x80|0x100|0x200);
      short sb1 = (short) 0x40;
      if (s)
	  _values[frmIdx] = (short) (sb1 | _values[frmIdx]);
      else
	  _values[frmIdx] = (short) (sb0 & _values[frmIdx]);
      return true;
  }
  public boolean setSig(boolean s, int frmIdx){
      short sb0 = (short) (0x01|0x02|0x04|0x08|0x10|0x20|0x40|0x100|0x200);
      short sb1 = (short) 0x80;
      if (s)
	  _values[frmIdx] = (short) (sb1 | _values[frmIdx]);
      else
	  _values[frmIdx] = (short) (sb0 & _values[frmIdx]);
      return true;
  }
  public int setNodPosition(int pos, int frmIdx) {
      short sb0 = (short) (0x01|0x02|0x04|0x08|0x10|0x20|0x40|0x80|0x200);
      short sb1 = (short) 0x100;
      if (pos!=0)
	  _values[frmIdx] = (short) (sb1 | _values[frmIdx]);
      else
	  _values[frmIdx] = (short) (sb0 & _values[frmIdx]);
      return pos;
  }
  public int setChopPosition(int pos, int frmIdx) {
      short sb0 = (short) (0x01|0x02|0x04|0x08|0x10|0x20|0x40|0x80|0x100);
      short sb1 = (short) 0x200;
      if (pos!=0)
	  _values[frmIdx] = (short) (sb1 | _values[frmIdx]);
      else
	  _values[frmIdx] = (short) (sb0 & _values[frmIdx]);
      return pos;
  }

    // convenience funcs. for evaluation of all frame buffers

    public static int initFrameBufs(){
	_frameBufnames.clear();
	_frameBufs.clear();
	int [] zeroImg = new int[320*240];
	//insure a zero's initial buffer:
	for (int i=0; i<320*240; i++) zeroImg[i]=0;

	//these use buf from socket:
	_frameBufnames.add("src1"); //0 == buffId(FrameCount) -1
	_frameBufnames.add("ref1"); //1 == buffId(FrameCount) -1
	_frameBufnames.add("src2"); //2 == buffId(FrameCount) -1
	_frameBufnames.add("ref2"); //3 == buffId(FrameCount) -1

	String bufnam = "accum(src1)";
	_frameBufnames.add(bufnam); // 4
	_frameBufs.put(bufnam,new UFInts(bufnam, zeroImg));
        bufnam = "accum(ref1)";
	_frameBufnames.add(bufnam); // 5
	_frameBufs.put(bufnam,new UFInts(bufnam, zeroImg));
        bufnam = "accum(src2)";
	_frameBufnames.add(bufnam); // 6
	_frameBufs.put(bufnam,new UFInts(bufnam, zeroImg));
        bufnam = "accum(ref2)";
	_frameBufnames.add(bufnam); // 7
	_frameBufs.put(bufnam,new UFInts(bufnam, zeroImg));
        bufnam = "dif1";
	_frameBufnames.add(bufnam); // 8
	_frameBufs.put(bufnam,new UFInts(bufnam, zeroImg));
        bufnam = "dif2";
	_frameBufnames.add(bufnam); // 9
	_frameBufs.put(bufnam,new UFInts(bufnam, zeroImg));
        bufnam = "accum(dif1)";
	_frameBufnames.add(bufnam); // 10
	_frameBufs.put(bufnam,new UFInts(bufnam, zeroImg));
        bufnam = "accum(dif2)";
	_frameBufnames.add(bufnam); // 11
	_frameBufs.put(bufnam,new UFInts(bufnam, zeroImg));
        bufnam = "sig";
	_frameBufnames.add(bufnam); // 12
	_frameBufs.put(bufnam,new UFInts(bufnam, zeroImg));
        bufnam = "accum(sig)";
	_frameBufnames.add(bufnam); // 13
	_frameBufs.put(bufnam,new UFInts(bufnam, zeroImg));

	return (int)_frameBufnames.size();
    }

    public int evalFrame(int frmIdx, UFInts frame){
	if (_frameBufnames.isEmpty())
	    initFrameBufs();
	if (frame == null)
	    return 0;
	int nodpos = nodPosition(frmIdx);
	int chopos = chopPosition(frmIdx);
	String timeframe = new String(frame._timestamp);
	if (nodpos == 0) { // even nod phase
	    if (chopos == 0) { // even chop phase for event nod phase is on source
		_frameBufs.put("src1",frame);
		UFInts as1 = (UFInts) _frameBufs.get("accum(src1)");
		as1._timestamp = new String(timeframe);
		as1.plusEquals(frame);
                _frameBufs.put("accum(src1)",as1);
	    }
	    else {
		_frameBufs.put("ref1",frame);
		UFInts ar1 = (UFInts) _frameBufs.get("accum(ref1)");
		ar1._timestamp = new String(timeframe);
		ar1.plusEquals(frame);
		_frameBufs.put("accum(ref1)",ar1);
	    }
	}
	else { // odd nod phase
	    if (chopos != 0) { // odd chop phase for odd nod phase is on source
		_frameBufs.put("src2",frame);
		UFInts as2 = (UFInts) _frameBufs.get("accum(src2)");
		as2._timestamp = new String(timeframe);
		as2.plusEquals(frame);
                _frameBufs.put("accum(src2)",as2);
	    }
	    else {
		_frameBufs.put("ref2",frame);
		UFInts ar2 = (UFInts) _frameBufs.get("accum(ref2)");
		ar2._timestamp = new String(timeframe);
		ar2.plusEquals(frame);
		_frameBufs.put("accum(ref2)",ar2);
	    }
	}
	if (doDiff1(frmIdx)) {
	    UFInts s1 = (UFInts) _frameBufs.get("src1");
	    UFInts r1 = (UFInts) _frameBufs.get("ref1");
	    UFInts d1 = (UFInts) _frameBufs.get("dif1");
	    d1.diff(s1,r1);
	    d1._timestamp = new String(timeframe);
	    _frameBufs.put("dif1",d1);
	    if (doAccumDiff1(frmIdx)) {
		UFInts ad1 = (UFInts) _frameBufs.get("accum(dif1)");
		ad1.plusEquals(d1);
		ad1._timestamp = new String(timeframe);
		_frameBufs.put("accum(dif1)",ad1);
	    }
	}
	if (doDiff2(frmIdx)) {
	    UFInts s2 = (UFInts) _frameBufs.get("src2");
	    UFInts r2 = (UFInts) _frameBufs.get("ref2");
	    UFInts d2 = (UFInts) _frameBufs.get("dif2");
	    d2.diff(s2,r2);
	    d2._timestamp = new String(timeframe);
	    _frameBufs.put("dif2",d2);
	    if (doAccumDiff2(frmIdx)) {
		UFInts ad2 = (UFInts) _frameBufs.get("accum(dif2)");
		ad2.plusEquals(d2);
		ad2._timestamp = new String(timeframe);
		_frameBufs.put("accum(dif2)",ad2);
	    }
	}
	if (doSig(frmIdx)) {
	    UFInts d1 = (UFInts) _frameBufs.get("dif1");
	    UFInts d2 = (UFInts) _frameBufs.get("dif2");
	    UFInts sig = (UFInts) _frameBufs.get("sig");
	    UFInts asig = (UFInts) _frameBufs.get("accum(sig)");
	    sig.sum(d1,d2);
	    sig._timestamp = new String(timeframe);
	    asig.plusEquals(sig);
	    asig._timestamp = new String(timeframe);
	    _frameBufs.put("sig",sig);
	    _frameBufs.put("accum(sig)",asig);
	}
	return (int)_frameBufs.size();
    }

    // get all buffer names:
    public static int getBufNames( String instrum, Vector names ) {
	names.clear();
	int i=0, cnt = (int) _frameBufnames.size();
	if (cnt <= 0) // need to init the buf names...
	    cnt = initFrameBufs();
	for ( ; i < cnt ; ++i) {
	    String bufnam = instrum + ':' + _frameBufnames.get(i);
	    names.add(bufnam);
	}
	return i;
    }

    public int getBufNames(Vector names) {
	names.clear();
	int i=0, cnt = (int)_frameBufnames.size();
	if (cnt <=0) // need to init the buf names...
	    cnt = initFrameBufs();
	for( ; i<cnt; i++)
	    names.add(_frameBufnames.get(i));
	return i;
    }

    // get buffer by name:
    public UFInts getBuf( String name ) {
	return (UFInts) _frameBufs.get(name);
    }
}
    
    

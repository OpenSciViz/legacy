#if !defined(__UFSerialPort_c__)
#define  __UFSerialPort_c__ "$Name:  $ $Id: UFSerialPort.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFSerialPort_c__;

#include "UFSerialPort.h"
#include "UFRuntime.h"

#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx) 
static const char* _tty[4] = { "/tyCo/0", "/tyCo/1", "/tyCo/2", "/tyCo/3" };
#elif defined(LINUX)
const char* _tty[4] = { "/dev/ttyS0",  "/dev/ttyS1",  "/dev/ttyS2", "/dev/ttyS3" };
//#elif defined(sun) || defined(solaris)|| defined(Solaris) || defined(SOLARIS)
#else
const char* _tty[2] = { "/dev/term/a", "/dev/term/b" };
#endif

// the singleton 
//static UFSerialPort _theInstance;
int UFSerialPort::_portIndex= 1;
//string UFSerialPort::_portName(_tty[_portIndex]);
UFSerialPort UFSerialPort::_theInstance;

string* UFSerialPort::_portName_ptr = 0;

UFSerialPort::UFSerialPort(int portIndex) {
  UFSerialPort::_portIndex = 1;
  if (UFSerialPort::_portName_ptr != NULL)
    delete UFSerialPort::_portName_ptr;
  UFSerialPort::_portName_ptr = new string(_tty[1]);
  //UFSerialPort::_portName = string(_tty[1]);
  //clog << "UFSerialPort> "  << " : " << *_portName_ptr <<endl;
  for( int i=0; i< __PortCnt_; ++i )
    _fd[i] = -1;
}

UFSerialPort* UFSerialPort::getInstance() {
  return &_theInstance;
}

// Find port name from number
string UFSerialPort::portName(int portIndex) const {
  if( portIndex < 0 || portIndex >= __PortCnt_ ) {
    return *_portName_ptr;
  }
  _portIndex = portIndex;
  if (_portName_ptr != NULL) delete _portName_ptr;
  _portName_ptr = new string(_tty[_portIndex]);
  return *_portName_ptr ;
}

// Find port number from name
int UFSerialPort::portNum( const string& portName ) const {
  if( portName != "/dev/ttyS0" || portName != "/dev/ttyS1" ||
      portName != "/dev/ttyS2"  ||portName != "/dev/ttyS3" ||
      portName != "/dev/term/a"  ||portName != "/dev/term/b") {
    errno = EINVAL ;
    return -1 ;
  }
  for( int i = 0 ; i < __PortCnt_ ; i++ ) {
    if( portName == _tty[i] ) {
      return i;
    }
  }
  return -1 ;
}

int UFSerialPort::available(float wait, int trycnt, int portIndex) {
  if( portIndex < 0 || portIndex >= __PortCnt_ || 0 >  _fd[ portIndex ] ) {
    return UFRuntime::available( _fd[_portIndex], wait, trycnt );
  }
  _portIndex = portIndex;
  return UFRuntime::available( _fd[_portIndex], wait, trycnt );
} // available

int UFSerialPort::close(const string& portName) {
  // portNum() will check for NULL portName
  // close() will check for invalid descriptor
  return close( portNum( portName ) ) ;
}

int UFSerialPort::close(int portIndex) {
  if( portIndex < 0 || portIndex >= __PortCnt_ || 0 >  _fd[ portIndex ] )
    portIndex = _portIndex; // use ctor/prior
  else
    _portIndex = portIndex; // ok use it and reset attrib.

#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx) 
  ioctl( _fd[portIndex], FIOSETOPTIONS, _orig[portIndex] );
#elif defined(LINUX) || defined(SOLARIS)
  ioctl( _fd[portIndex], TCSETAF, &_orig[portIndex] );
#endif

  int ret =  ::close( _fd[_portIndex] );
  _fd[ portIndex ] = -1 ;
  return ret ;

} // close

int UFSerialPort::closeAll() {
  for( int i= 0; i< __PortCnt_; ++i ) {
    // Only close the port if it's open
    if( -1 != _fd[ i ] ) {
      close(i);
      // The fd is reset in the (local) close function
      //_fd[ i ] = -1 ;
    }
  }
  return __PortCnt_;
} // closeAll

int UFSerialPort::open(const string& portName, int baud) {
  _portIndex = portNum( portName ) ;
  if( 0 >  _portIndex ) {
    return -1 ;
  }

  _fd[ _portIndex ] = open( _portIndex, baud ) ;
  return _fd[ _portIndex ] ;
}

int UFSerialPort::open(int portIndex, int baud) {
  if( portIndex < 0 || portIndex >= __PortCnt_ )
    portIndex = _portIndex;
  else
    _portIndex = portIndex;

  _fd[portIndex] = ::open( _tty[portIndex], O_RDWR, 0 );
#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx) 
  _orig[portIndex] = ioctl(_fd[portIndex], FIOGETOPTIONS, 0 );
  ioctl( _fd[portIndex], FIOBAUDRATE, baud );
  //ioctl( _fd[portIndex], FIOSETOPTIONS, OPT_TERMINAL );
  ioctl( _fd[portIndex], FIOSETOPTIONS, OPT_RAW );
#elif defined(LINUX) || defined(SOLARIS)
  ioctl( _fd[portIndex], TCGETA , &_orig[portIndex]);
  ioctl( _fd[portIndex], TCGETA, &_raw); 
  _raw.c_cflag = B9600 | CS8 | CLOCAL | CREAD;
  _raw.c_lflag &= ~ICANON;
  _raw.c_lflag &= ~ECHO;
  _raw.c_cc[VMIN] = 1; // present each character as soon as it shows up
  _raw.c_cc[VTIME] = 1; // 0.1 second for timeout
  ioctl( _fd[portIndex], TCSETAF, &_raw);
#endif
  return _fd[portIndex];
} // open

int UFSerialPort::openAll(int baud) {
  for( int i=0; i< __PortCnt_; ++i ) {
    _fd[ i ] = open(i, baud);
  }
  return __PortCnt_;
} // openAll

int UFSerialPort::hello(int portIndex) {
  if( portIndex < 0 && _fd[0] < 0 ) {
    openAll();
  }
  for( int i=0; i< __PortCnt_; ++i ) {
    string s(__FILE__);
    s += " Hello ";
    s += _tty[i];
    send(s, i);
  }
 return __PortCnt_;
}

int UFSerialPort::send(char c,  int portIndex) {
  if( portIndex < 0 || portIndex >= __PortCnt_ || 0 > _fd[ portIndex ] )
    portIndex = _portIndex;
  else
    _portIndex = portIndex;

  return ::write(_fd[portIndex], &c, sizeof(c));
}

int UFSerialPort::send(const unsigned char* buf, int len, int portIndex) {
  if( NULL == buf ) {
    errno = EINVAL;
    return -1;
  }
  if( portIndex < 0 || portIndex >= __PortCnt_ || 0 > _fd[ portIndex ] )
    portIndex = _portIndex;
  else
    _portIndex = portIndex;

  return ::write(_fd[portIndex], (const char*)buf, len);
}

int UFSerialPort::send(const unsigned char* buf, int len, const string& portName) {
  if( NULL == buf ) {
    errno = EINVAL;
    return -1 ;
  }

  // write() will handle invalid length
  // portNum() will handle valid pointer to portName
  _portIndex = portNum( portName );
  if( 0 > _portIndex ) {
    return -1;
  }

  // Return from portNum() is guaranteed to be
  // in the valid range for the _fd array
  if( 0 > _fd[ _portIndex ] ) {
     errno = EBADF;
     return -1;
  }
  return ::write( _fd[ _portIndex], (const char*) buf, len );
}

int UFSerialPort::send(const string& s, int portIndex) {
  if( portIndex < 0 || portIndex >= __PortCnt_ || 0 > _fd[ portIndex ] )
    portIndex = _portIndex;
  else
    _portIndex = portIndex;
    
  return ::write(_fd[portIndex], s.c_str(), s.length());
}

int UFSerialPort::send(const string& s, const string& portName) {
  _portIndex = portNum( portName );
  if( 0 > _portIndex ) {
    return -1 ;
  }
  if (_portName_ptr != NULL) 
    delete _portName_ptr;
  _portName_ptr = new string(portName);
  return ::write( _fd[ _portIndex ], s.c_str(), s.length() ) ;
}

int UFSerialPort::recv(char& c, int portIndex) {
  if( portIndex < 0 || portIndex >= __PortCnt_ || 0 > _fd[ portIndex ] )
    portIndex = _portIndex;
  else
    _portIndex = portIndex;

  return ::read(_fd[portIndex], &c, sizeof(c));
}

int UFSerialPort::recv(unsigned char* buf, int len, int portIndex) {
  if( NULL == buf ) {
    errno = EINVAL ;
    return -1 ;
  }
  if( portIndex < 0 || portIndex >= __PortCnt_ || 0 > _fd[ portIndex ] )
    portIndex = _portIndex;
  else
    _portIndex = portIndex;

  return ::read(_fd[portIndex], (char*)buf, len);
}

int UFSerialPort::recv(unsigned char* buf, int len, const string& portName) {
  if( NULL == buf ) {
    errno = EINVAL;
    return -1;
  }

  _portIndex = portNum( portName );
  if( 0 > _portIndex ) {
    return -1;
  }

  if( 0 > _fd[ _portIndex ] ) {
    errno = EBADF;
    return -1;
  }
  if (_portName_ptr != NULL)
    delete _portName_ptr;
  _portName_ptr = new string(portName);

  return ::write( _fd[ _portIndex ], buf, len );
}

int UFSerialPort::recv(string& s, const string& portName) {
  return recv( s, portNum( portName ) );
}

int UFSerialPort::recv(string& s, int portIndex) {
  if( portIndex < 0 || portIndex >= __PortCnt_ || 0 > _fd[ portIndex ] )
    portIndex = _portIndex;
  else
    _portIndex = portIndex;

  char* buf = new char[ s.length() + 1 ] ;
  int ret = ::read(_fd[portIndex], buf, s.length());
  if( ret < 0 ) {
    delete[] buf;
    return -1;
  }

  buf[ ret ] = 0 ;
  s = buf;

  delete[] buf;
  return ret;
}
   
int UFSerialPort::exec(void* p) {
  int portIndex = *((int*)p);
  clog << "UFSerialPort::exec> using portIndex: "<< portIndex<<endl;
  clog << "UFSerialPort::exec> name= "<<_tty[portIndex]<<endl;
  //string dev( "/dev/term/a");
  string hello("hello port ");
  hello += portName(portIndex);
  hello += " -- please type something back --";
  char nl = 10;
  char cr = 13;
  hello += cr;
  hello += nl;

  string bye("goodbye!");
  bye += cr;
  bye += nl;

  open( portIndex );
  send( hello, portIndex );
 
  string ret;
  char buf = 0;
  do {
    recv(buf, portIndex);
    ret += buf;
    //clog<<ret<<endl;
    send( ret, portIndex );
    send( hello, portIndex );
   
    if( buf == cr ) {
      //clog << "UFSerialPort::exec> got carriage return..."<<endl; 
      send( string( "UFSerialPort::exec> got carriage return..." ), portIndex); 
      send( bye, portIndex );
    }
    if( buf == nl ) {
      //clog << "UFSerialPort::exec> got newline..."<<endl;
      //clog<<ret<<endl;
      send( string( "UFSerialPort::exec> got newline..." ), portIndex );
      send( bye, portIndex );
    }     
  } while( buf != nl && buf != cr );

  close( portIndex );

  return portIndex;
}

int UFSerialPort::main(int argc, char** argv) {
  int portIndex = 0;
  if( argc > 1 ) portIndex = atoi(argv[1]);
  if( portIndex >= __PortCnt_ )  portIndex = __PortCnt_ - 1;

  UFSerialPort* sp = UFSerialPort::getInstance();
 
  return sp->exec((void*) &portIndex);

}

int UFSerialPort::main(int portIndex) {
  UFSerialPort* sp = UFSerialPort::getInstance();
  return sp->exec((void*) &portIndex);
}

/////////////////////////////////////////////////////////////////////

int UFSerialPort::_fd[__PortCnt_];

#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx) 
int UFSerialPort::_orig[__PortCnt_];
#elif defined(LINUX)
struct termio UFSerialPort::_orig[__PortCnt_];
struct termio UFSerialPort::_raw;
//#elif defined(sun) || defined(solaris)|| defined(Solaris) || defined(SOLARIS)
#else
struct termio UFSerialPort::_orig[__PortCnt_];
struct termio UFSerialPort::_raw;
#endif

/////////////////////////////////////////////////////////////////////

#endif // __UFSerialPort_c__

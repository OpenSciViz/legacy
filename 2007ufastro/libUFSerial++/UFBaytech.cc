#if !defined(__UFBaytech_cc__)
#define __UFBaytech_cc__ "$Name:  $ $Id: UFBaytech.cc 14 2008-06-11 01:49:45Z hon $";

#include "UFBaytech.h"
#include "UFRuntime.h"

// static var:
UFBaytech* UFBaytech::_btinstance= 0;
//bool UFBaytech::_verbose= false;

// public
UFBaytech::~UFBaytech() {
  _btinstance = 0;
}

// return 0 on connection failure:
int UFBaytech::connect(const string& host, float flush, int port, bool block) {
  int fd= -1;
  if( _btinstance == 0 )
    return fd;

  int bport = peerPort();
  string bhost = peerHost();
  if( host != "" )
    bhost = host;
  if( port > 0 )
    bport = port;

  //clog<<"UFBaytech::connect> host: "<<host<<", port: "<<port<<endl;
  fd = _btinstance->UFClientSocket::connect(bhost, bport);
  if( fd > 0 ) {
    UFBaytech::_btinstance->_fs = fdopen(fd, "w"); // for flush
    // once connected, enter outlet control sub-menu and stay there...
    // initial connection causes baytech to return top level menu text:
    string r;
    int n = _btinstance->recvReply(r); // main menu is presented on connect
    if( n <= 0 ) {
      clog<<"UFBaytech::create> failed to get rpc top menu..."<<endl;
      return 0;
    }
    _btinstance->_btstat += r;
    //clog<<"UFBaytech::connect> entered baytech rpc main menu, _bstat: "<<_btinstance->_btstat<<endl;
    string  menu = "1"; // select outlet control sub-menu
    //clog<<"UFBaytech::connect> enter baytech rpc outlet menu via submit of: "<<menu<<endl;
    int trycnt = 3;;
    n = UFBaytech::_btinstance->submit(menu, r, flush);
    while( n <= 0 && --trycnt > 0 ) {
      clog<<"UFBaytech::create> failed to enter rpc outlet sub-menu, try again..."<<endl;
      n = UFBaytech::_btinstance->submit(menu, r, flush);
    }
    if( n <= 0 ) {
      clog<<"UFBaytech::create> failed to enter rpc outlet sub-menu, try again..."<<endl;
      UFBaytech::_btinstance->close();
      return -1;
    }
    _btinstance->_btstat += r;

    if( _verbose )
      clog<<"UFBaytech::connect> entered baytech rpc menu, _bstat: "<<_btinstance->_btstat<<endl;
  }

  return fd;
}

// public static func:
int UFBaytech::reconnect(int port) {
  if( _btinstance == 0 )
    return 0;

  string bhost = _btinstance->peerHost();

  return _btinstance->connect(bhost, port);
}

// return class _btinstabce pointer or 0 on connection failure:
UFBaytech* UFBaytech::create(const string& host, float flush, int port) {
  // enforce singleton (single connection to baytech)
  if( _btinstance ) {
    _btinstance->close();
    delete _btinstance;
  }

  UFBaytech::_btinstance = new UFBaytech;
  // also set base class _instance?
  int fd= -1;
  // connect 
  fd = UFBaytech::_btinstance->connect(host, flush, port); // always use default port == 23 (telnet)
  if( fd < 0 ) {
    clog<<"UFBaytech::create> failed to connect..."<<endl;
    delete UFBaytech::_btinstance;
    return 0;
  }

  return UFBaytech::_btinstance;
}

string UFBaytech::_cleanReply(char* cs, int n) {
  char* c = cs;
  /*
  for( int i= 0; i < n; ++i, ++cs ) {
    if( *cs == 0 ) *cs = ' '; // replace nulls with space
    if( *cs == '\r' ) *cs = ' '; // replace carriage-returns with space
  }
  */
  UFRuntime::rmJunk(c, n);
  string s = c;
  return s;
}
 
int UFBaytech::recvReply(string& r, float flush) {
  r = "";
  int n= 0, na= 0;
  bool done= false;
  while( !done ) {
    na = UFSocket::available(flush, 5); 
    //if( na < 0 ) {
    if( na <= 0 ) {
      clog<<"UFBaytech::create> connected, but menu is not there? available() returns: "<<na<<endl;
      return (int)r.size();
    }
    char input[(const int) (1+na)]; memset(input, 0, 1+na);
    n = UFClientSocket::recv((unsigned char*)input, na);
    if( n <= 0 ) {
      //clog<<"UFBaytech::submit/reply> recv returns: "<<n<<endl;
      return n;
    }
    r += _cleanReply(input, n);
    size_t eotpos = r.rfind(_eotr);
    size_t rlen = r.length();
    size_t near = _eotr.length(); // is eot at/near the end of reply string?
    if( eotpos == string::npos ) { // no terminator, double check availability
      na = UFSocket::available(flush, 3);
      if( na == 0 ) 
	done = true;
    }
    else if( (rlen - eotpos) == near ) { // terminator
      done = true;
    }
  }
  return (int)r.size();
}

// submit string (only), return immediately without fetching reply
int UFBaytech::submit(const string& s, float flush) {
  string output = s;
  if( output.rfind(_eots) == string::npos ) output.append(_eots);
  // raw byte send:
  if( _verbose )
    clog<<"UFBaytech::submit> "<<output<<endl;

  int ns = _btinstance->UFClientSocket::send((unsigned char*) output.c_str(), output.length());
  if( ns < 0 ) { // assume connection was lost and try to reconnect and resend
    // if socket is complient with X/Open standard, ::send can return negative of:
    // EMGSIZE, ENOTCONN, EPIPE, ENETDOWN, ENETUNREACH, ...
    clog<<"UFBaytech::submit> socket send failed, try to reconnect..."<<endl;
    if( _btinstance->reconnect() != 0 )
      ns = _btinstance->UFClientSocket::send((unsigned char*) output.c_str(), output.length());
    else
      return -1;
  }

  // try flushing the socket/serial-port output stream
  // this is the only way i can think to do it:
  int stat = fflush(_fs);
  if( stat != 0 ) clog<<"UFBaytech::submit> socket flush failed? ns= "<<ns<<endl;

  if( flush > 0.0000001 ) // optionally sleep after the flush
    UFPosixRuntime::sleep(flush);

  return ns;
}

// submit string, recv reply and return reply as string
int UFBaytech::submit(const string& s, string& r, float flush) {
  r = "";
  int n = submit(s, flush);
  if( n <= 0 )
    return n;

  n = recvReply(r, flush);
  if( n <= 0 )
    return n;

  if( _verbose )
    clog<<"UFBaytech::submit> reply: "<<r<<endl;

  return r.length();
}

int UFBaytech::close() {
  string bc = "menu";
  int n = submit(bc, _btstat); // enter top menu
  bc = "6"; // baytech menu prompts for selection: 6 is disconnect
  n = submit(bc, 0.1); // no reply
  UFClientSocket::close();
  return ::fclose(_btinstance->_fs);
}

string UFBaytech::rmsAmps(const string& s, float flush) {
  string btstat = s; 
  if( s.empty() ) btstat = status(flush);
  if( btstat == "" )
    return "Unknown";

  vector< string > statv;
  int n = _statparse(statv);
  if( n <= 0 )
    return "Unknown";

  string rmsamps = statv[0];
  return rmsamps;
}

string UFBaytech::maxAmps(const string& s, float flush) {
  string btstat = s; 
  if( s.empty() ) btstat = status(flush);
  if( btstat == "" )
    return "Unknown";

  vector< string > statv;
  int n = _statparse(statv);
  if( n <= 0 )
    return "Unknown";

  string maxamps = statv[1];
  return maxamps;
}

string UFBaytech::celsius(const string& s, float flush) {
  string btstat = s; 
  if( s.empty() ) btstat = status(flush);
  if( btstat == "" )
    return "Unknown";

  vector< string > statv;
  int n = _statparse(statv);
  if( n <= 0 )
    return "Unknown";

  string celsius = statv[2];
  return celsius;
}

// chan  <=0 for all 8 channel logical AND, chan > 0 for individual chan: 
string UFBaytech::powerStat(int chan, float flush) {
  string nil;
  string btstat = status(flush);
  if( btstat == "" )
    return nil;

  vector< string > statv;
  int n = _statparse(statv);
  if( n <= 0 ) {
    btstat = "Unknown";
    return btstat;
  }
  btstat = nil;
  // elements 6-13 should correspond to chan 1-8?
  if( chan > 0 ) { // check it
    strstream sc; sc<<chan<<ends;
    string schan = sc.str(); delete sc.str();
    string onoff = statv[chan]; // sometimes off by one? indicating baytech soc buffers have not been flushed?
    size_t chpos = onoff.find(schan);
    size_t cnt= chan+1;
    while( chpos == string::npos && ++cnt < statv.size() ) { // find line containig desired channel
      onoff = statv[cnt]; chpos = onoff.find(schan);
    } 
    if( chpos != string::npos )
      onoff = onoff.substr(chpos);
    //clog<<"UFBaytech::powerStat> chan: "<<chan<<onoff<<endl;
    /*
    if( onoff.find("Off") == string::npos ) { // must be on
      strstream s; s<<chan<<" == On"<<<ends; btstat = s.str(); delete s.str();
      return btstat;
    }
    else {
      strstream s; s<<chan<<" == Off"<<ends; btstat = s.str(); delete s.str();
      return btstat;
    }
    */
    return onoff;
  }
  // check all
  btstat = nil;
  for( chan = 1; chan <= 8; ++chan ) {
    string onoff = statv[5+chan]; // this seems to work reliably
    //clog<<"UFBaytech::powerStat> chan: "<<chan<<onoff<<endl;
    strstream sc; sc<<chan<<ends;
    string schan = sc.str(); delete sc.str(); size_t chpos = onoff.find(schan);
    if( chpos != string::npos ) onoff = onoff.substr(chpos);
    /*
    if( onoff.find("Off") != string::npos ) { // off
      strstream s; s<<chan<<" == Off"<<ends; btstat += s.str(); delete s.str();
      if( chan < 8 ) btstat += ", ";
    }
    else {
      strstream s; s<<chan<<" == On"<<ends; btstat += s.str(); delete s.str();
      if( chan < 8 ) btstat += ", ";
    }
    */
    btstat += onoff;
    if( chan < 8 ) btstat += "\n";
  }
  // if we get here all outlets are on
  return btstat;
}
// chan  <=0 for all 8 channel logical AND, chan > 0 for individual chan: 
bool UFBaytech::powerStat(int chan, string& reply, float flush) {
  reply = status(flush);
  if( reply == "" || reply == "Unknown" )
    return false;

  vector< string > statv;
  int n = _statparse(statv);
  if( n <= 0 )
    return false;
 
  // elements 6-13 should correspond to chan 1-8
  if( chan > 0 ) { // check it
    string onoff =  statv[5+chan];
    // clog<<"UFBaytech::powerStat> chan: "<<chan<<onoff<<endl;
    if( onoff.find("Off") == string::npos ) // must be on
      return true;
    else
      return false;
  }
  // check all
  for( chan = 1; chan <= 8; ++chan ) {
    string onoff =  statv[5+chan];
    //clog<<"UFBaytech::powerStat> chan: "<<chan<<onoff<<endl;
    if( onoff.find("Off") != string::npos ) // off
      return false;
  }
  // if we get here all outlets are on
  return true;
}

// turn on power to all or individual chan
string UFBaytech::powerOn(int chan, float flush) {
  string nil= "";
  if( chan > 8 )
    return nil;

  if( chan > 0 ) {
    strstream s;
    s << "on "<< chan << ends;
    string on = s.str(); delete s.str();
    int n = submit(on, _btstat, flush);
    on = "y"; // baytech menu prompts for y/n confirmation..
    n = submit(on, _btstat, flush);
    if( n <= 0 )
      return nil;
    if( _btstat.find("6       LVDT        6       On") == string::npos ) {
      clog<<"UFBaytech::powerOn> failed, btech reply: "<<_btstat<<endl;
      return nil;
    }
    return _btstat;
  }
  // if we get here, default is to process all channels 
  // insure chan == 0
  chan = 0;
  strstream s;
  s << "on "<< chan << ends;
  string on = s.str(); delete s.str();
  int n = submit(on, _btstat, flush);
  on = "y"; // baytech menu prompts for y/n confirmation..
  n = submit(on, _btstat, flush);
  if( n <= 0 )
    return nil;

  /*
  for( chan = 1; chan <= 8; ++chan ) {
    strstream s;
    s << "on "<< chan << ends;
    string on = s.str(); delete s.str();
    int n = submit(on, _btstat, flush);
    on = "y"; // baytech menu prompts for y/n confirmation..
    n = submit(on, _btstat, flush);
    if( n <= 0 )
      return nil;
  }
  */

  return _btstat;
}

// turn off power to all or individual chan
string UFBaytech::powerOff(int chan, float flush) {
  string nil= "";
  if( chan > 8 )
    return nil;

  if( chan > 0 ) {
    strstream s;
    s << "off "<< chan << ends;
    string off = s.str(); delete s.str();
    int n = submit(off, _btstat, flush);
    off = "y"; // baytech menu prompts for y/n confirmation..
    n = submit(off, _btstat, flush);
    if( n <= 0 )
      return nil;
    if( _btstat.find("6       LVDT        6       Off") == string::npos ) {
      clog<<"UFBaytech::powerOff> failed, btech reply: "<<_btstat<<endl;
      return nil;
    }
    return _btstat;
  }
  // if we get here, default is to process all channels 
  // insure chan == 0
  chan = 0;
  strstream s;
  s << "off "<< chan << ends;
  string off = s.str(); delete s.str();
  int n = submit(off, _btstat, flush);
  off = "y"; // baytech menu prompts for y/n confirmation..
  n = submit(off, _btstat, flush);
  if( n <= 0 )
    return nil;

  /*
  for( chan = 1; chan <= 8; ++chan ) {
    strstream s;
    s << "off "<< chan << ends;
    string off = s.str(); delete s.str();
    int n = submit(off, _btstat, flush);
    off = "y"; // baytech menu prompts for y/n confirmation..
    n = submit(off, _btstat, flush);
    if( n <= 0 )
      return nil;
  }    
  */
  return _btstat;
}

string UFBaytech::status(float flush) {
  // send _eots to baytech and get status reply
  _btstat= "Unknown";
  string menu = "menu"; // first get back to main menu from wherever we are
  int n = submit(menu, _btstat, flush);
  if( n <= 0 )
    return _btstat;

  if( _verbose )
    clog<<"UFBaytech::status> submitted baytech main menu, reply: "<<_btstat<<endl;

  menu = "1"; // select outlet control and stay there
  n = submit(menu, _btstat, flush);
  if( n <= 0 ) {
    clog<<"UFBaytech::status> Unable to navigate baytech menu to outlet control..."<<endl;
    return _btstat;
  }
  if( _verbose )
    clog<<"UFBaytech::status> Navigated baytech menu to outlet control, reply: "<<_btstat<<endl;

  return _btstat;
}

string UFBaytech::statusSim() {
  // simulate reply
  string btreply = "True RMS current:  2.0 Amps \n";
  btreply += "Maximum Detected:  7.2 Amps \n";
  btreply += "Internal Temperature: 35.0 C \n";
  btreply += "Circuit Breaker: On \n";
  btreply += "Selection   Outlet    Outlet   Power \n";
  btreply += "Number     Name     Number   Status\n";
  btreply += "  1       flam2perle   1       On/Off \n";
  btreply += "  2       LakeSh218   2       On/Off \n";
  btreply += "  3       LakeSh332   3       On/Off \n";
  btreply += "  4       PfeifferVu  4       On/Off \n";
  btreply += "  5       SymBarcode  5       On/Off \n";
  btreply += "  6       LVDT        6       On/Off \n";
  btreply += "  7       PortescapI  7       On/Off \n";
  btreply += "  8       MCE4        8       On/Off \n";
  btreply += "Type \"Help\" for a list of commands \n";
  btreply += "RPC-3>";

  return btreply;
}

int UFBaytech::_statparse(vector< string >& statvec) {
  statvec.clear();
  if( _btstat == "" )
    return 0;

  UFRuntime::rmJunk(_btstat);
  size_t ppos= 0, pos = _btstat.find("\n", 1+ppos), len = pos-ppos;
  bool done= false;
  while( !done ) {
    len = pos-ppos;
    string line =  _btstat.substr(ppos, len); UFRuntime::rmJunk(line);
    if( line.length() > 5 ) {
    statvec.push_back(line); // ignore empty lines
      //clog<<"UFBaytech::_statparse> _bstat non-empty line: "<<line<<ends;
    }
    ppos = pos;
    pos = _btstat.find("\n", 1+ppos);
    if( pos == string::npos )
      done = true;
  }
  //clog<<"UFBaytech::_statparse> parsed non-empty line count: "<<statvec.size()<<endl;
  return statvec.size();
}

int UFBaytech::statparse(const string& btstat, vector< string >& statvec) {
  string bstat = btstat;
  UFRuntime::rmJunk(bstat);
  size_t ppos= 0, pos = bstat.find("\n", 1+ppos), len = pos-ppos;
  bool done= false;
  while( !done ) {
    len = pos-ppos;
    string line =  bstat.substr(ppos, len); UFRuntime::rmJunk(line);
    if( line.length() > 5 ) {
      statvec.push_back(line); // ignore empty lines
      //clog<<"UFBaytech::statparse> bstat non-empty line: "<<line<<endl;
    }
    ppos = pos;
    pos = btstat.find("\n", 1+ppos);
    if( pos == string::npos )
      done = true;
  }
  //clog<<"UFBaytech::statparse> parsed non-empty line count: "<<statvec.size()<<endl;
  return statvec.size();
}

int UFBaytech::main(int argc, char ** argv) {
  UFRuntime::Argv args(argc, argv);
  float flush= 0.05;
  int outlet = -1; // LVDT == 6
  string usage = "UFBaytech::main> usage: [-flush float] [-stat [1-8 or all]] [-on [1-8 or all]] [-off [1-8 or all]]";
  string host = "f2baytech";
  string arg = UFRuntime::argVal("-host", args);
  if( arg != "true" && arg != "false" )
    host = arg;

  arg = UFRuntime::argVal("-flush", args);
  if( arg != "true" && arg != "false" ) // assume a flush value has been provided
    flush = atof(arg.c_str());

  UFBaytech* baytech = UFBaytech::create(host, flush);
  if (baytech == 0) {
    clog << "Connection failed" << endl;
    return -1;
  }

  string bstat= "Unknown";
  if( argc == 0 ) { // assume -stat
    bstat = baytech->powerStat();
    cout<<bstat<<endl;
    return 0;
  }

  arg = UFRuntime::argVal("-h", args);
  if( arg == "true" ) {
    clog<<usage<<endl;
    return 0;
  }
  arg = UFRuntime::argVal("-help", args);
  if( arg == "true" ) {
    clog<<usage<<endl;
    return 0;
  }

  arg = UFRuntime::argVal("-stat", args);
  if( arg == "true" || arg == "all" ) {
    bstat = baytech->powerStat();
    cout<<bstat<<endl;
    return 0;
  }
  else if( arg != "false" ) {
    outlet = atoi(arg.c_str());
    if( outlet <= 0 || outlet >= 9 )
      bstat = baytech->powerStat();
    else
      bstat = baytech->powerStat(outlet);
    cout<<bstat<<endl;
    return 0;
  }

  arg = UFRuntime::argVal("-off", args);
  if( arg != "false" ) {
    if( arg == "true" ) { // no option value, prompt.....
      cout << "Turn off which outlet? [1-8] " << ends;
      int c = getchar();
      while( c != '\r' && c != '\n' ) c = getchar();
      char buf[2] = { '\0', '\0' }; buf[0] = (char) c;
      outlet = atoi(buf);
    }
    else {
      outlet = atoi(arg.c_str());
    }
    if( outlet >= 1 && outlet <= 8 )
      bstat = baytech->powerOff(outlet);
    else
      clog<<"ufbaytech> outlet? "<<outlet<<endl;
  }
 
  arg = UFRuntime::argVal("-on", args);
  if( arg != "false" ) {
    if( arg == "true" ) { // no option value, prompt.....
      cout << "Turn off which outlet? [1-8] " << ends;
      int c = getchar();
      while( c != '\r' && c != '\n' ) c = getchar();
      char buf[2] = { '\0', '\0' }; buf[0] = (char) c;
      outlet = atoi(buf);
    }
    else {
      outlet = atoi(arg.c_str());
    }
    if( outlet >= 1 && outlet <= 8 )
      bstat = baytech->powerOn(outlet);
    else
      clog<<"ufbaytech> outlet? "<<outlet<<endl;
  }

  // finally show current status
  bstat = baytech->powerStat(outlet);
  cout<<bstat<<endl;
  return 0;
}
#endif // __UFBaytech_cc__

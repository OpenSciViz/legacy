#if !defined(__UFAnnexIO_cc__)
#define __UFAnnexIO_cc__ "$Name:  $ $Id: UFAnnexIO.cc 14 2008-06-11 01:49:45Z hon $";

#include "UFAnnexIO.h"
__UFAnnexIO_H__(UFAnnexIO_cc);

#include "UFTermServ.h"
__UFTermServ_H__(UFAnnexIO_cc);

#include "UFPosixRuntime.h"
__UFPosixRuntime_H__(UFAnnexIO_cc);

#include "UFRuntime.h"
__UFRuntime_H__(UFAnnexIO_cc);

#ifndef __EXTENSIONS__
#define __EXTENSIONS__
#endif

#include "stdlib.h"
#include "unistd.h"
#include "stdio.h"
#include "errno.h"
#include "limits.h"
#include "strings.h"
#include "fcntl.h"
#include "time.h"
#include "termio.h"
#include "signal.h"
#include "sys/uio.h"
#include "sys/types.h"
#include "sys/stat.h"

#include "iostream"

const char _ACQ= 6, _ENQ= 5, _NL= 10, _CR= 13;
// this is for dealing with mce4's ">>" asummetric return end-of-text: 
string* _mce4 = 0; // ">>";
static bool _quiet = false;
static bool _escape= false;
static bool _telnet= false;
// Pfeiffer handshake acq & enq handled silently/automatically
// PLC handshake requires something simliar to pfeiffer
// need to test it with these:
static bool _enq= false;
static bool _acq= false;

const char* usage= "annex [-[ts]host <DN/IP>] [-[ts]port <7001-7024>] [-flush <float sec>] [-mce \"cmd\"] [-q <\"quick/_quiet cmd\">] [-s <\"submit async cmd\">] [-fifo/-sendfifo <fifoname>] [-recvfifo <fifoname>]";
// if this took an argument, might be useful...

//static void shutdown(void) {}

void UFAnnexIO::_telnetHandshake(int expect) {
  clog<<"UFAnnexIO::> dealing with binary telnet handshake..."<<endl;
  ::memset(_inputAnnex, 0, sizeof(_inputAnnex));
  // recv:
  int nb= 0, nba= 0, n1= 0, cnt= 0;
  while( (nba = available()) >= 0 && cnt < expect ) {
    if( nba > 0 ) {
      clog<<"UFAnnexIO::ctor> available: "<<nb<<endl;
      nb = recv(_inputAnnex, nba);
      if( nb <= 0 ) {
        clog<<"UFAnnexIO::ctor> socket recv error? exit..."<<endl;
        exit(1);
      }
      cnt += nb;
    }
    else {
      // just block on 1 byte read instead of sleeping or yielding
      n1 = recv(_inputAnnex, 1);
      if( n1 <= 0 ) {
        clog<<"UFAnnexIO::ctor> socket recv error? n1: "<<n1<<", "<<strerror(errno)<<endl;
        UFPosixRuntime::sleep(0.1);
      }
      else
        cnt += n1;
    }
  } // while available
  clog<<"UFAnnexIO::ctor> cnt: "<<cnt<<", fromAnnax: "<<_inputAnnex<<endl;
  // borrowed from netcat.c TELNET option: 
  unsigned char obuf[4]; ::memset(obuf, 0, sizeof(obuf));  // tiny thing to build responses into
  int x;
  unsigned char y, *p;
  y = 0;
  p = _inputAnnex;
  x = cnt;
  while( x > 0 ) {
    clog<<"UFAnnexIO::ctor> *p: "<<(int)(*p)<<endl;
    if (*p != 255) {                     // IAC?
      p++; x--; continue;
    }
    obuf[0] = 255; 
    p++; x--;
    if ((*p == 251) || (*p == 252))     // WILL or WONT 
      y = 254;                          // -> DONT
    if ((*p == 253) || (*p == 254))     // DO or DONT
      y = 252;                          // -> WONT
    if(y) {
      obuf[1] = y;
      p++; x--;
      obuf[2] = *p;                     /* copy actual option byte */
      clog<<"UFAnnexIO::ctor> send: "<<((int)obuf[0])<<" "<<((int)obuf[1])<<" "<<((int)obuf[2])<<endl;
      send(obuf, 3);
      // if one wanted to bump wrote_net or do a hexdump line, here's the place 
      y = 0;
    } // if y
  } // while x
  ::memset(_inputAnnex, 0, sizeof(_inputAnnex));
} // -t telnet option

// insure fifo info. struct has correct behavior
UFAnnexIO::FifoInfo::FifoInfo() : sendFd(-1), recvFd(-1), sendName(), recvName() {}

UFAnnexIO::FifoInfo::FifoInfo(const string& fifo_prefix) : sendFd(-1), recvFd(-1),
                                                           sendName(fifo_prefix+string("Send")),
                                                           recvName(fifo_prefix+string("Recv")) {}

UFAnnexIO::FifoInfo::FifoInfo(const UFAnnexIO::FifoInfo& rhs) : sendFd(rhs.sendFd),
                                                                recvFd(rhs.recvFd),
                                                                sendName(rhs.sendName),
                                                                recvName(rhs.recvName) {}

// static convenience function for executing annex child:
// this was cloned from the IDL "ufSerial" library:
pid_t UFAnnexIO::startChild(char* fifoname, UFAnnexIO::FifoInfo& info, int port) {
  int cnt= 0;
  char *cmd= 0, *annex = "annex";
  char* argv[7];
  argv[0] = annex;
  argv[1] = "-port";
  argv[2] = "7008";
  argv[3] = "-sendfifo";
  cnt = 1+strlen(fifoname)+strlen("Send");
  argv[4] = new char[cnt]; memset(argv[4], 0, cnt);
  strcat(argv[4], fifoname); strcat(argv[4], "Send");
  argv[5] = "-recvfifo";
  cnt = 1+strlen(fifoname)+strlen("Recv");
  argv[6] = new char[cnt]; memset(argv[6], 0, cnt);
  strcat(argv[6], fifoname); strcat(argv[6], "Recv");

  // allocate sufficient space for command string using default port number
  cnt = 1+strlen(argv[0]) + 1+strlen(argv[1]) + 1+strlen(argv[2]) +
        1+strlen(argv[3]) + 1+strlen(argv[4]) + 1+strlen(argv[5]) + 1+strlen(argv[6]);
  cmd = new char[cnt]; memset(cmd, 0, cnt);

  cnt = 0;
  while( cnt < 7 )
    strcat(cmd, argv[cnt++]); strcat(cmd, " ");

  // replace default port number
  if( port >= 7002 && port <= 7008 ) { // annex only has 7 available ports!
    sprintf(cmd, "%s %04d %s%s%s %s%s%s", "/usr/local/uf/bin/annex -port", port,
            "-sendfifo ", fifoname, "Send", "-recvfifo ", fifoname, "Recv");
  }

  // fork & exec annex child:
  pid_t child = ::fork();
  if( child == 0 ) {
    int stat = execvp(annex, argv); // child overlays new process with annex app.
    if( stat < 0 ) {
      clog<<"UFAnnexIO::startChild> could not exec annex app., errno= "<< errno << endl;
    }
  }
  // parent continues:
  delete cmd;
  UFPosixRuntime::yield(); // let the child process have some cpu
  // open the send fifo for write only and the recv for read only
  info = UFAnnexIO::openFifos(fifoname);
 
  //atexit(shutdown);
  return child;
}

// static convenience func.
UFAnnexIO::FifoInfo UFAnnexIO::openFifos(const string& fifoname) {
  UFAnnexIO::FifoInfo info(fifoname);

  struct stat s;
  if( stat(info.sendName.c_str(), &s) == 0 )
    info.sendFd = ::open(info.sendName.c_str(), O_WRONLY | O_NONBLOCK);
  else
    info.sendFd = -1;

  if( stat(info.recvName.c_str(), &s) == 0 )
    info.recvFd = ::open(info.recvName.c_str(), O_RDONLY | O_NONBLOCK);
  else
    info.recvFd = -1;

  return info;
}
 
// static main
int UFAnnexIO::main(int argc, char** argv) {
  UFRuntime::Argv args(argc, argv);
  
  vector< char* > cargs;
  int i;
  for( i = 0; i < argc; i++ ) {
    cargs.push_back(argv[i]);
  }
 
  string arg = UFRuntime::argVal("-v", args);
  if( arg == "true" )
    UFTermServ::_verbose = true;

  UFAnnexIO* annex = UFAnnexIO::create(cargs);
  if( annex == 0 ) {
    clog<<"UFAnnexIO::main> unable to create annex/perle connection..."<<endl;
    return -1;
  }
  string arg0 = argv[0];
  if( arg0.find("perle") != string::npos ) {
    UFTermServ::perleLogin(static_cast< UFClientSocket* > (annex));
  }
  return annex->mainThread(cargs, annex);
}
 
UFAnnexIO* UFAnnexIO::create(const vector< char* >& args) {
  UFAnnexIO* annex= 0; // return value
  int i=0;
  string eot = "\n";

  // annex host IP & tcp port
  string port;
  string host= args[0];
  if( host.find("perle") != string::npos ) 
    host = "flamperle";
  else
    host = "ufannex";
   
  string sendfifoname, recvfifoname, quick, submit;
  float flush= 0.0;
  for( i = 1; i < (int)args.size(); ++i ) {
    if( strcmp(args[i],"-port") == 0 || strcmp(args[i],"-tsport") == 0 )
      port = args[++i];
    else if( strcmp(args[i],"-host") == 0 || strcmp(args[i],"-tshost") == 0 )
      host = args[++i];
    else if( strcmp(args[i],"-fifo") == 0 || strcmp(args[i],"-sendfifo") == 0)
      sendfifoname = args[++i];
    else if( strcmp(args[i],"-recvfifo") == 0)
      recvfifoname = args[++i];
    else if( strcmp(args[i],"-crnl") == 0 || strcmp(args[i],"-crlf") == 0)
      eot = "\r\n";
    else if( strcmp(args[i],"-nlcr") == 0 || strcmp(args[i],"-lfcr") == 0)
      eot = "\n\r";
    else if( strcmp(args[i],"-r") == 0 || strcmp(args[i],"-cr") == 0)
      eot = "\r";
    else if( strcmp(args[i],"-mce") == 0 || strcmp(args[i],"-mce4") == 0 ) {
      eot = "\r"; // send eot.
      _mce4 = new string(">>"); // non symmetric mce4 reply eot.
      port = "7008";
      if( (i < (int)args.size() - 1) && (args[1+i][0] != '-') )
	quick = args[++i];
      _quiet = true;
    }
    else if( strcmp(args[i],"-q") == 0 ) {
      if( (i < (int)args.size() - 1) && (args[1+i][0] != '-') )
	quick = args[++i];
      _quiet = true;
    }
    else if( strcmp(args[i],"-s") == 0 ) {
      submit = args[++i];
      _quiet = true;
    }
    else if( strcmp(args[i],"-esc") == 0 ) {
      _escape = _quiet = true;
      //quick = "escape";
    }
    else if( strcmp(args[i],"-enq") == 0 ) {
      _enq = _quiet = true;
      //quick = "escape";
    }
    else if( strcmp(args[i],"-acq") == 0 ) {
      _acq = _quiet = true;
      //quick = "escape";
    }
    else if( strcmp(args[i],"-t") == 0 ) {
      _telnet = true;
    }
    else if( strcmp(args[i],"-flush") == 0 ) {
      flush = atof(args[++i]);
    }
    else if( strcmp(args[i],"-help") == 0 || strcmp(args[i],"-h") == 0) {
      cout<<usage<<endl;
      exit(0);
    }
  }
  int portNo = atoi(port.c_str());
  if( port.length() > 0 && host.length() > 0 ) 
    annex = new UFAnnexIO(portNo, host);
  else if( port.length() > 0 )
    annex = new UFAnnexIO(portNo);
  else
    annex = new UFAnnexIO();

  annex->setFlush(flush);
  annex->setEot(eot);
  annex->setQuick(quick);
  annex->setSubmit(submit);

  if( sendfifoname.length() > 0 ) {
    int stat = UFPosixRuntime::fifoCreate(sendfifoname);
    if( stat < 0 ) {
      clog<<"UFAnnexIO::create> failed to create send fifo: "<< sendfifoname<<endl;
      delete annex; return 0;
    }
    else {
      annex->setSendFifoName(sendfifoname); // mainThread must open & set fd...
      if( !_quiet ) clog<<"UFAnnexIO::create> created send fifo: "<< sendfifoname<<endl;
    }
  }

  if( recvfifoname.length() > 0 ) {
    int stat = UFPosixRuntime::fifoCreate(recvfifoname);
    if( stat < 0 ) {
      clog<<"UFAnnexIO::create> failed to create recv fifo: "<< recvfifoname<<endl;
      delete annex; return 0;
    }
    else {
      annex->setRecvFifoName(recvfifoname); // recvThread must open & set fd...
       if( !_quiet ) clog<<"UFAnnexIO::create> created recv fifo: "<< recvfifoname<<endl;
    }
  }

  return annex;
}

// ctor connects to annex
UFAnnexIO::UFAnnexIO(int portNo, const string& tcphost) : _inputAnnex(0), _outputAnnex(0),
                                                          _usrInput(0), _usrOutput(1),
  // annex sends are read from usr/stdin by defualt:                                                       
                                                          _sendfifoname("stdin"),
  // annex recv's are sent to usr/stdout by default:
                                                          _recvfifoname("stdout"),
							  _quick(), _submit(), _flush(0.0),
                                                          _eot("\n") {

  // while I/O with the Annex is always via socket, user I/O could be
  // from a terminal or from another process (like IDL) via a named pipe/fifo
  int sfd = connect(tcphost, portNo);
  if( sfd <= 0 ) {
    clog << "UFAnnexIO::> failed to connect to: "<<description()<<endl;
    return;
  }
  _inputAnnex = new unsigned char[rcvBufSize()]; /* read buff == socket buffer max size? */
  _outputAnnex = new unsigned char[sndBufSize()]; /* write buff == socket buffer max size? */
  ::memset(_inputAnnex, 0, rcvBufSize());
  ::memset(_outputAnnex, 0, sndBufSize());

  if( _telnet ) {
    _telnetHandshake(12);
    _telnetHandshake(6);
  }
  if( !_quiet )
    clog << "UFAnnexIO::> connected."<<endl;

  return;
}

UFAnnexIO::~UFAnnexIO() {
  delete [] _inputAnnex;
  delete [] _outputAnnex;
  UFClientSocket::close();
}

// static helper for O_NONBLOCK (should put this into the runtime class)
int UFAnnexIO::writeFully(int fd, unsigned char* buf, int sz) {
  FILE* session = ::fopen("perle.session", "a+");
  int n= 0, nb= 0;
  do {
    if( session ) ::fwrite(buf, 1, sz, session);
    char *cr = index((const char*)buf, _CR);
    if( cr ) *cr = ' '; // replace carriage return with blank
    char* acq = index((const char*)buf, _ACQ);
    if( acq ) *acq = ' '; // replace with space
    n = ::write(fd, buf, sz);
    if( n > 0  ) nb += n;
  } while( errno == EAGAIN || errno == EINTR );
  if( session ) ::fclose(session);
  return nb;
}

// recv from annex socket & display text
void* UFAnnexIO::recvThread(void* p) {
  UFAnnexIO* annex = (UFAnnexIO*) p;

  // check whether fifo specified
  string recvfifoname = annex->getRecvFifoName();
  if( recvfifoname != "stdout" ) {
    int oflag = O_WRONLY;
    // recvThread should write to this fd, checking errno == EAGAIN
    if( !_quiet ) clog<<"UFAnnexIO::recvThread> waiting on open of recv fifo: "<< recvfifoname<<endl;
    int fd = ::open(recvfifoname.c_str(), oflag); // this open blocks until some opens other end for read
    if( fd < 0 ) {
      clog<<"UFAnnexIO::recvThread> failed to open recv fifo: "<< recvfifoname<<", errno= "<<errno<<endl;
      exit(2);
    }
    if( !_quiet ) clog<<"UFAnnexIO::recvThread> opened recv fifo: "<< recvfifoname<<endl;
    annex->setUsrOutput(fd);
  }
  
  int usrOutput = annex->getUsrOutput();
  unsigned char* inputAnnex = annex->recvBuffPtr();
#if !defined(CYGWIN)
  struct termio _orig, _outraw;
  ::ioctl(usrOutput, TCGETA , &_orig);
  _outraw = _orig;
  _outraw.c_cflag = CLOCAL | CREAD;
  _outraw.c_lflag &= ~ICANON;
  _outraw.c_lflag &= ~ECHO; 
  _outraw.c_cc[VMIN] = 1; // present each character as soon as it shows up
  _outraw.c_cc[VTIME] = 0; // block if nothing is available (infinite timeout)
  ::ioctl(usrOutput, TCSETAF, &_outraw); // set _usrOutput to raw
#endif
  // whomever opens the other end of the pipe, for read will block until something is written
  // so send out this greeting?:
  // unsigned char* greeting = (unsigned char*) "UFAnnexIO::recvThread> started...\n";
  // writeFully(usrOutput, greeting, strlen((const char*)greeting));
  int nb= 0, n1= 0;
  clog << "UFAnnexIO::recvThread> started..."<<endl;
  while( (nb = annex->available()) >= 0 ) {
    if( _verbose )
      clog << "UFAnnexIO::recvThread> nb= "<<nb<<endl;
    if( nb < 0 ) {
      clog << "UFAnnexIO::recvThread> socket lost connection? nb: "<<nb<<endl;
      exit(1);
    }
    if( nb > 0 ) {
      int nr = annex->recv(inputAnnex, nb);
      if( nr < 0 ) {
	clog<<"UFAnnexIO::recvThread> socket recv error? exit..."<<endl;
	exit(1);
      }
      //clog<<"UFAnnexIO::recvThread> socket recv nb= "<<nb<<endl;
      char* acq = index((const char*)inputAnnex, _ACQ);
      if( acq ) {
        //*acq = ' '; // replace with space?
        annex->send((unsigned char*)&_ENQ, 1); // and complete the handshake
      }
      UFRuntime::rmJunk((char*)inputAnnex);
      writeFully(usrOutput, inputAnnex, nb);
    }
    else {
      // just block on 1 byte read instead of sleeping or yielding
      //clog<<"UFAnnexIO::recvThread> blocking recv (1 byte)..."<<endl;
      n1 = annex->recv(inputAnnex, 1);
      if( n1 <= 0 ) {
	clog<<"UFAnnexIO::recvThread> socket recv error? exit..."<<endl;
	exit(1);
      }
      unsigned char* acq = &inputAnnex[0];
      if( _ACQ == *acq ) {
        //*acq = ' '; // replace with space?
        annex->send((unsigned char*)&_ENQ, 1); // and complete the handshake
      }
      UFRuntime::rmJunk((char*)inputAnnex);
      writeFully(usrOutput, inputAnnex, 1);
    } 
    if( _mce4 != 0 && strstr((const char*)inputAnnex, _mce4->c_str()) != 0 ) {
      nb = annex->available();
      //if( nb == 0 ) clog<<"UFAnnexIO::recvThread> (mce4 transact) socket buffer empty ... "<<endl;
    }
  }
  return p;
} // recvThread

int UFAnnexIO::fetchAndSend(int fd, int n) {
  //clog<<"UFAnnexIO::fetchAndSend> read nb= "<<n<<endl;
  memset(_outputAnnex, 0, sizeof(_outputAnnex));
  if( n == 0 )
    n = 1;
  int nb = ::read(fd, _outputAnnex, n); // blocks until someone writes to the pipe
  if( nb < 0 ) {
    clog<<"UFAnnexIO::mainfetchAndSend(> ? read failed, errno= "<<errno
	<<", inputFd attribute, usr_in= "<<fd<<", fifo= "<<_sendfifoname<<endl;
    return nb;
  }
  _submit = (char*)_outputAnnex;
  // append eot terminator only if new-line is present (in usr input)
  int nlpos = _submit.find("\n");
  if( nlpos != (int)string::npos ) {
    _submit = _submit.substr(0, nlpos);
    _submit += _eot;
  }

  //clog<<"UFAnnexIO::fetchAndSend> "<<_submit<<endl;
  nb = send((unsigned char*)_submit.c_str(), _submit.length());
  if( nb < (int)_submit.length() ) {
    clog<<"UFAnnexIO::fetchAndSend> socket write error? exit..."<<endl;
  }

  return nb;
}

int UFAnnexIO::mainThread(const vector< char* >& args, UFAnnexIO* annex) {
  string q = annex->getQuick();
  string sb = annex->getSubmit();
  if( q.empty() && sb.empty() ) { // start recvthread:
    pthread_t ar = UFPosixRuntime::newThread(recvThread, annex);
    if( ar <= 0 ) {
      clog << "failed to create recv thread"<<endl;
      exit(3);
    }
    UFPosixRuntime::yield();
  }

  // check whether send fifo specified, if fifo open for read (will block)
  string sendfifoname = annex->getSendFifoName(); // Input is read from this fifo and sent out annex socket
  if( sendfifoname != "stdin" ) {
    //clog<<"UFAnnexIO::mainThread> waiting on open of send fifo: "<< sendfifoname<<endl;
    int oflag = O_RDONLY;
    int fd = ::open(sendfifoname.c_str(), oflag); // blocks until someone opens pipe's other end  for writing
    if( fd < 0 ) {
      clog<<"UFAnnexIO::mainThread> failed to open send fifo: "<< sendfifoname<<endl;
      exit(2);
    }
    if( !_quiet ) clog<<"UFAnnexIO::mainThread> opened send fifo: "<< sendfifoname<<endl;
    annex->setUsrInput(fd);
    annex->setSendFifoName(sendfifoname);
  }

  if( !q.empty() ) {
    annex->sendQuick(); // quick one-time submit & reply
    return 0;
  }
  if( !sb.empty() ) {
    annex->submit(); // one-time submit and don't read reply
    return 0;
  }

  int usr_in = annex->getUsrInput();    
  while( true ) {
    // if there is new input from the user, fetch it and notify the annexSend thread
    int nb = UFRuntime::available(usr_in);
    if( nb < 0 ) {
      clog<<"UFAnnexIO::mainThread> ? read failed, errno= "<<errno
	  <<", inputFd attribute, usr_in= "<<usr_in<<", fifo= "<<sendfifoname<<endl;
      exit(5);
    }
    else if( nb >= 0 ) {
      nb = annex->fetchAndSend(usr_in, nb);
    }
    UFPosixRuntime::yield();
  }
  return 0;
}

int UFAnnexIO::submit() {
  if( _submit.find(_eot) == string::npos )
    _submit += _eot;
  int slen = _submit.length();
  //clog<<"UFAnnexIO::submit()> "<<_submit<<endl;
  FILE* fs = fdopen(::dup(_sockinfo.fd), "w");
  int nb = send((unsigned char*)_submit.c_str(), slen);
  // try flushing the socket output stream
  // this is the only way i can think to do it:
  int stat = fflush(fs);
  if( stat != 0 ) clog<<"UFAnnexIO::submit> socket flush failed? nb= "<<nb<<endl;
  ::fclose(fs);
  float fslp = getFlush();
  return UFPosixRuntime::sleep(fslp);
}

int UFAnnexIO::escape(const string& s, float flsh) {
  int nb= 0;
  if( s != "" )
    nb += send(s);

  char esc = 27;
  nb += send((unsigned char*) &esc, 1);

  FILE* fs = fdopen(::dup(_sockinfo.fd), "w");
  // try flushing the socket output stream
  // this is the only way i can think to do it:
  int stat = fflush(fs);
  if( stat != 0 || nb <= 0 )
    clog<<"UFAnnexIO::escape> escape (27) socket send and flush failed, nb= "<<nb<<endl;
  ::fclose(fs);
  if( flsh > 0.0000001 ) 
    UFPosixRuntime::sleep(flsh);

  return nb;
}

int UFAnnexIO::sendQuick() {
  // if this is actually a perle rather than an annex, deal with 
  // security (user login) first:
  //UFTermServ::perleLogin(static_cast< UFClientSocket* > (this));

  if( _escape )
    return escape(_quick, getFlush());

  if( _quick.find(_eot) == string::npos )
    _quick += _eot;

  int qlen = _quick.length();
  if( UFTermServ::_verbose )
    clog<<"UFAnnexIO::sendQuick> "<<_quick<<endl;
  int nb = send((unsigned char*)_quick.c_str(), qlen);
  FILE* fs = fdopen(::dup(_sockinfo.fd), "w");
  // try flushing the socket output stream
  // this is the only way i can think to do it:
  int stat = fflush(fs);
  if( stat != 0 ) clog<<"UFAnnexIO::sendQuick> socket flush failed? nb= "<<nb<<endl;
  ::fclose(fs);
  float fslp = getFlush();
  if( fslp > 0.0000001 ) 
    UFPosixRuntime::sleep(fslp);

  // recv reply, show it and exit:
  int n1= 0, cnt= 0, eotcnt= 0;
  bool done= false, enq= false;
  while( (nb = available()) >= 0 && !done ) {
    if( UFTermServ::_verbose )
      clog<<"UFAnnexIO::sendQuick> available: "<<nb<<", eotcnt= "<<eotcnt<<endl;
    if( nb < 0 ) done = true;
    if( nb > 0 ) {
      int nr = recv(_inputAnnex, nb);
      if( nr < 0 ) {
	clog<<"UFAnnexIO::sendQuick> socket recv error? exit..."<<endl;
	done = true;
      }
      if( strstr((const char*)_inputAnnex, _eot.c_str()) != 0 )
        eotcnt++; 
      //clog<<"UFAnnexIO::sendQuick> socket recv nb= "<<nb<<endl;
      char* acq = index((const char*)_inputAnnex, _ACQ);
      if( acq ) {
        //*acq = ' '; // replace with space
        send((unsigned char*)&_ENQ, 1); // and complete the handshake
	enq = true;
        if( UFTermServ::_verbose ) clog<<"UFAnnexIO::sendQuick> got ACQ, sent back ENQ..."<<endl;
      }
      UFRuntime::rmJunk((char*)_inputAnnex);
      writeFully(_usrOutput, _inputAnnex, nb);
      cnt += nr;
    }
    else {
      // just block on 1 byte read instead of sleeping or yielding
      if( UFTermServ::_verbose )
	clog<<"UFAnnexIO::sendQuick> blocking on 1 char recv..."<<endl;
      n1 = recv(_inputAnnex, 1);
      if( n1 <= 0 ) {
	clog<<"UFAnnexIO::sendQuick> socket recv error? exit..."<<endl;
	done = true;
        continue;
      }
      if( strstr((const char*)_inputAnnex, _eot.c_str()) != 0 )
        eotcnt++; 
      if( _inputAnnex[0] == _ACQ ) {
        _inputAnnex[0] = ' '; // replace with space
        send((unsigned char*)&_ENQ, 1); // and complete the handshake
	enq = true;
        if( UFTermServ::_verbose ) clog<<"UFAnnexIO::sendQuick> got ACQ, sent back ENQ..."<<endl;
      }
      else {
        //write(_usrOutput, _inputAnnex, 1);
        UFRuntime::rmJunk((char*)_inputAnnex);
        writeFully(_usrOutput, _inputAnnex, 1);
      }
      cnt += n1;
    }
    if( _mce4 != 0 && strstr((const char*)_inputAnnex, _mce4->c_str()) != 0 ) {
      nb = available();
      if( nb <= 0 ) {
        //clog<<"UFAnnexIO::sendQuick> mce4 transact terminator: "<<*_mce4<<", socket buffer empty..."<<endl;
        done = true;
      }
    }
    if( eotcnt >= 2 ) {
      //clog<<"UFAnnexIO::sendQuick> eotcnt: "<<eotcnt<<endl;
      done = true;
    }
    else if( !enq && _mce4 == 0 && eotcnt >= 1 ) {
      //clog<<"UFAnnexIO::sendQuick> !enq and eotcnt: "<<eotcnt<<endl;
      done = true;
    }
  }// while !done
  if( _eot.rfind("\n") == string::npos )
    cout<<endl;
  else // just in case _usrOutput is stdout, be sure to flush before exit?
    cout<<flush;

  return cnt;
} // sendQuick
  
#endif //  __UFAnnexIO_cc__   
      

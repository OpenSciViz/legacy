#if !defined(__UFTermServ_h__)
#define __UFTermServ_h__ "$Name:  $ $Id: UFTermServ.h,v 0.16 2006/10/24 14:39:32 flam Exp $";
#define __UFTermServ_H__(arg) const char arg##TermServ_h__rcsId[] = __UFTermServ_h__;

#include "string"
#include "vector"
#include "stdio.h"

#include "UFClientSocket.h"
#include "UFSerialPort.h"

// trivial version of UFAnnexIO (which has gotten very messy and badly needs to be 
// re-visited and cleaned up). this is for use by any UFDeviceAgent service...
// it cannot be used 'stand-alone" nor does it support fifos or child procs...
// if host == localserial, then this should itself behave as the terminal
// server, i.e. it should use the systems' local serial port tty, rather
// than connecting to an actual terminal server (annex or iocomm). 
class UFTermServ : public UFClientSocket, public UFSerialPort {
public:
  static bool _verbose;
  virtual ~UFTermServ();

  // return 0 on connection failure:
  static UFTermServ* create(const string& host, int port);
  virtual int connect(const string& host, int port, bool block= true);
  virtual UFTermServ* reconnect();
  virtual int close();

  int flush(float flsh= 0.0);

  // submit string (only), return immediately without fetching reply
  virtual int submit(const string& s, float flsh= 0.0, int exact= 0); // flush output and sleep flsh sec.

  // submit string, recv reply and return reply as string
  // make this virtual to deal with firmware exceptions
  // to "normal" recv behavior
  virtual int submit(const string& s, string& r, float flsh= 0.0, int exact= 0);

  //submit with multi-line reply (some hardware seems to use \r\n as BOTH an end-of-line 
  //token and an end-of-transmission token.)
  virtual int submitMultiReply(const string& s, vector< string >& rv, float flsh= 0.0);
  // portscap extends this:
  inline virtual int submitMultiline(const string& s, vector< string >& rv, float flsh= 0.0, int numEOTRTokens=1)
    { return submitMultiReply(s, rv, flsh); }
  // return single concatenated line
  virtual int submitMultiReply(const string& s, string& r, float flsh= 0.0);
  // portscap extends this:
  inline virtual int submitMultiline(const string& s, string& r, float flsh= 0.0, int numEOTRTokens=1)
    { return submitMultiReply(s, r, flsh); }

  // support virtual escape command
  inline virtual int escape(const string& s, float flsh= 0.0) { return -1; }

  inline string getEOTR() { return _eotr; }
  inline string getSOTR() { return _sotr; }
  inline string getEOTS() { return _eots; }
  inline string getSOTS() { return _sots; }

  inline string resetEOTS(const string& eot= "\n") { _eots = eot; return _eots; }
  inline string resetEOTR(const string& eot= "\n") { _eotr = eot; return _eotr; }
  inline string resetSOTS(const string& sot= "") { _sots = sot; return _sots; }
  inline string resetSOTR(const string& sot= "") { _sotr = sot; return _sotr; }

  static int perleLogin(UFClientSocket* csoc);
  FILE* _fs; // for flushing ... this should really go into UFSocket someday

  static string bootPrompt();

protected:
  // prevent compiler from defining default or copy ctors:
  inline UFTermServ(int port= -1) : UFClientSocket(port), _fs(0),
                    _eotr("\n"), _sotr(""), _eots("\n"), _sots("") { _instance = 0; }
  inline UFTermServ(const UFClientSocket& csoc) : UFClientSocket(csoc), _fs(0),
                    _eotr("\n"), _sotr(""), _eots("\n"), _sots("") { _instance = this; }
  inline UFTermServ(const UFTermServ& rhs) : UFClientSocket((const UFClientSocket&)rhs), _fs(0),
                    _eotr("\n"), _sotr(""), _eots("\n"), _sots("") { _instance = this; }

  int _available(float wait= 0.02, int trycnt= 1);
  int _perleLogin(const string& host);
  int _recv(string& r, int exact= 0);
  string _eotr; // end-of-text in recv/reply usually either '\n' or '\r'
  string _sotr; // start-of-text in recv/reply usually empty '' (n/a) or '*'
  string _eots; // end-of-text in send/submit usually either '\n' or '\r'
  string _sots; // start-of-text in send/submit usually empty '' (n/a)  or '#'
  static UFTermServ* _instance;
};

#endif //  __UFTermServ_h__   
      

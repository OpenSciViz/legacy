#if !defined(__UFTermServ_cc__)
#define __UFTermServ_cc__ "$Name:  $ $Id: UFTermServ.cc 14 2008-06-11 01:49:45Z hon $";

#include "UFTermServ.h"
#include "UFRuntime.h"

// statics var:
bool UFTermServ::_verbose= false;
UFTermServ* UFTermServ::_instance= 0;

// public static func:
int UFTermServ::close() {
  int fd= -1;
  if( _instance == 0 )
    return fd;

  if( _instance->_host.find("localserial") == string::npos ) // connected to the annex/iocomm/perle terminal console server
    fd = _instance->UFClientSocket::close();
  else
    fd = _instance->UFSerialPort::close();

  ::fclose(_instance->_fs);
  return fd;
}

// this static func is of use externally:
int UFTermServ::perleLogin(UFClientSocket* csoc) {
  UFTermServ tserv(*csoc);
  const string host = csoc->peerHost();
  return tserv._perleLogin(host);
}

int UFTermServ::_perleLogin(const string& host) {
  time_t t0 = time(0);
  float delay = 0.05;
  //clog<<"UFTermServ::_perleLogin> perle should prompt for login..."<<endl;
  int n = UFTermServ::_available(delay, 2);
  int np = strlen("login:");
  //clog<<"UFTermServ::_perleLogin> perle should prompt for login, n: "<<n<<" expect: "<<np<<endl;
  if( n <= 0 ) {  // the Perle CS9024i may requre a login, the BayNet Annex & Chase Iocomm do not;
  // nor does the Baytech RPC...
    clog<<"UFTermServ::_perleLogin> assuming this console server is not prompting for login."<<endl;
    return 0;
  }
  else if( n < np ) {
    while( n < np ) {
      n = UFTermServ::_available(delay, 1);
      clog<<"UFTermServ::_perleLogin> perle should prompt for login, n: "<<n<<endl;
      if( n <= 0 ) {
        //clog<<"UFTermServ::_perleLogin> perle should prompt for login, n: "<<n<<endl;
	break;
      }
    }
  }
  if( n <= 0 ) {
    clog<<"UFTermServ::_perleLogin> assuming this console server is not prompting for login."<<endl;
    return n;
  }
  string reply, login, passwd, rcvbuf, echo;
  //clog<<"UFTermServ::_perleLogin> recv  prompt for login, n: "<<n<<" expect: "<<np<<endl;
  int nr = UFTermServ::_recv(login, np); // exactly n char expected (no EOT)
  if( _verbose )
    clog<<"UFTermServ::_perleLogin> perle login prompt: "<<nr<<" of "<<n<<"  "<<login<<endl;
  // login seems to require a short interval bewteen every character exchange...
  delay = 0.025;
  UFTermServ::submit("u", reply,  delay, strlen("u")); // login as ufastro, do not send eot
  echo += reply;
  UFTermServ::submit("f", reply,  delay, strlen("f"));
  echo += reply;
  UFTermServ::submit("a", reply,  delay, strlen("a"));
  echo += reply;
  UFTermServ::submit("s", reply,  delay, strlen("s"));
  echo += reply;
  UFTermServ::submit("t", reply,  delay, strlen("t"));
  echo += reply;
  UFTermServ::submit("r", reply,  delay, strlen("r"));
  echo += reply;
  UFTermServ::submit("o", reply,  delay, strlen("o"));
  echo += reply;
  UFTermServ::submit("\n", delay, 1); // login as ufastro
  if( _verbose )
    clog<<"UFTermServ::_perleLogin> perle echo: "<<echo<<endl;
  //while( (n = UFTermServ::_available(delay, 2)) < (int) strlen("password:") ) {
  //clog<<"UFTermServ::_perleLogin> perle should prompt for password, n: "<<n<<endl;
  //}
  np = strlen("password:");
  //clog<<"UFTermServ::_perleLogin> recv prompt for passwd, expect: "<<np<<endl;
  n = UFTermServ::_recv(passwd, np); // exactly n char expected (no EOT)
  if( _verbose )
    clog<<"UFTermServ::_perleLogin> perle password prompt: "<<passwd<<endl;
  // note that newer perle firmware (3.4 and above) does NOT echo password
  // characters...
  //echo = "";
  //UFTermServ::submit("u", reply,  delay, strlen("u")); // login as ufastro, do not send eot
  UFTermServ::submit("u", delay, strlen("u")); // login as ufastro, do not send eot
  //echo += reply;
  //UFTermServ::submit("f", reply,  delay, strlen("f"));
  UFTermServ::submit("f", delay, strlen("f"));
  //echo += reply;
  //UFTermServ::submit("a", reply,  delay, strlen("a"));
  UFTermServ::submit("a", delay, strlen("a"));
  //echo += reply;
  //UFTermServ::submit("s", reply,  delay, strlen("s"));
  UFTermServ::submit("s", delay, strlen("s"));
  //echo += reply;
  //UFTermServ::submit("t", reply,  delay, strlen("t"));
  UFTermServ::submit("t", delay, strlen("t"));
  //echo += reply;
  //UFTermServ::submit("r", reply,  delay, strlen("r"));
  UFTermServ::submit("r", delay, strlen("r"));
  //echo += reply;
  //UFTermServ::submit("o", reply,  delay, strlen("o"));
  UFTermServ::submit("o", delay, strlen("o"));
  //echo += reply;
  UFTermServ::submit("\n", delay, 1); // login as ufastro
  // this succeeds or the connection is closed
  if( (n = UFTermServ::_available(delay, 1)) > 0 ) { // flush any remaining characters from input buf
    n = UFTermServ::_recv(rcvbuf, n); // exactly n char expectx1ed (no EOT)
  }
  time_t t = time(0) - t0;
  if( _verbose )
    clog<<"UFTermServ::_perleLogin> perle login complete (sec): "<<t<<", "<<description()<<endl;

  return 1;
}

// return 0 on connection failure:
int UFTermServ::connect(const string& host, int port, bool block) {
  int fd= -1;
  if( _instance == 0 )
    return fd;

  // connect to an annex or iocom term serve, or perle cs9000 console server
  if( host.find("localserial") == string::npos ) { 
    //clog<<"UFTermServ::connect> host: "<<host<<", port: "<<port<<endl;
    fd = _instance->UFClientSocket::connect(host, port);
    _instance->_perleLogin(host); // is this is a Perle Console Server, must login.
  }
  else {
    _instance->_portNo = port;
    _instance->_host = host;
    fd = _instance->UFSerialPort::open(_instance->_portNo);
  }
  return fd;
}

UFTermServ* UFTermServ::reconnect() {
  if( _instance == 0 )
    return 0;

  string host = _instance->peerHost();
  int port = _instance->peerPort();
  if( _verbose )
    clog<<"UFTermServ::reconnect> host: "<<host<<", port: "<<port<<endl;
  return UFTermServ::create(host, port);
}

UFTermServ* UFTermServ::create(const string& host, int port) {
  if( _instance ) {
    if( _verbose )
      clog<<"UFTermServ::create> close host: "<<_instance->peerHost()<<", port: "<<_instance->peerPort()<<endl;
    _instance->close();
    delete _instance;
  }
  _instance = new UFTermServ;
  if( _verbose )
    clog<<"UFTermServ::create> connect host: "<<host<<", port: "<<port<<endl;
  int fd = _instance->connect(host, port);
  if( fd < 0 ) {
    delete _instance; // sets _instance to 0
  }
  else {
    _instance->_fs = fdopen(fd, "w");
  }
  return _instance;
}

// public
UFTermServ::~UFTermServ() {
  _instance = 0;
}

int UFTermServ::_available(float wait, int trycnt) {
  if( _instance == 0 )
    return -1;
  // connected to the perle/annex/iocomm term. server or local serial port?
  if( _instance->_host.find("localserial") == string::npos )
    return _instance->UFClientSocket::available(wait, trycnt);
  else // use local serial port
    return _instance->UFSerialPort::available(wait, trycnt);
}

// submit string (only), return immediately without fetching reply
int UFTermServ::submit(const string& s, float flsh, int exact) {
  string output;
  if( _sots != ""  && s.find(_sots) == string::npos && exact <= 0 )
    output = _sots + s;
  else
    output = s;

  if( output.rfind(_eots) == string::npos && exact <= 0)
    output.append(_eots);

  // raw byte send:
  if( _verbose )
    clog<<"UFTermServ::submit> (exact= "<<exact<<") "<<output<<endl;

  bool localserial = _instance->_host.find("localserial") != string::npos;
  int na= _available();
  if( na > 0 && !localserial ) {
    if( _verbose )
      clog<<"UFTermServ::submit> recv buffer has stale/lingering data to be flushed, na: "<<na<<endl;
    unsigned char stale[(const int)na+1]; memset(stale, 0, na+1);
    //float to = _instance->UFClientSocket::getTimeOut();
    _instance->UFClientSocket::setTimeOut(flsh);
    int nr = _instance->UFClientSocket::recv(stale, na);
    //_instance->UFClientSocket::setTimeOut(to);
    if( _verbose )
      clog<<"UFTermServ::submit> buffer stale recvd: "<<nr<<", "<<stale<<endl;
  }
  int ns= -1;
  if( _instance->_host.find("localserial") == string::npos ) { // socket
    // connected to the annex/iocomm/perle term. server
    ns = _instance->UFClientSocket::send((unsigned char*)output.c_str(), output.length());
    if( ns < 0 ) { // assume connection was lost and try to reconnect and resend?
      // if socket is complient with X/Open standard, ::send can return negative of:
      // EMGSIZE, ENOTCONN, EPIPE, ECONNRESET, ENETDOWN, ENETUNREACH, ...
      clog<<"UFTermServ::submit> socket send failed..."<<endl;
      //if( _instance->reconnect() != 0 )
       // ns = _instance->UFClientSocket::send((unsigned char*)output.c_str(), output.length());
    }
  }
  else { // local serial port
    ns = _instance->UFSerialPort::send((unsigned char*) output.c_str(), output.length(), _instance->_portNo);
  }
  if( ns <= 0 )
    return ns;
  // try flushing the socket/serial-port output stream
  // this is the only way i can think to do it:
  int stat = flush(flsh);
  if( stat != 0 ) clog<<"UFTermServ::submit> socket flush failed? ns= "<<ns<<endl;

  return ns;
} //submit

int UFTermServ::flush(float flsh) {
  int stat = fflush(_fs);
  if( stat != 0 )
    clog<<"UFTermServ::flush> socket flush failed... "<<UFPosixRuntime::errStr()<<endl;

  if( flsh > 0.0000001 ) // optionally sleep after the flush
    UFPosixRuntime::sleep(flsh);

  return stat;
}

int UFTermServ::_recv(string& r, int exact) {
  r = "";
  int n= 0, na= 0;
  unsigned char _acq= 6, _enq= 5;
  bool done= false, localserial= true;
  if( _instance->_host.find("localserial") == string::npos ) // connected to local serial port
    localserial = false;
  while( !done ) {
    na = _available();
    if( _verbose )
      clog<<"UFTermServ::_recv> available() returns: "<<na<<", exact= "<<exact<<endl;
    //if( na <= 0 ) {
    if( na < 0 ) {
      //clog<<"UFTermServ::_recv> available() returns: "<<na<<endl;
      return na;
    }
    else if( na == 0 )
      na = 1; // block on 1 byte (char) read, using UFSocket::recv ?

    int nr = na;
    if( exact > 0 && exact < na ) nr = exact;
    char input[(const int) (1+nr)]; memset(input, 0, 1+nr);
    if( !localserial ) { // connected to the annex/iocomm/perle terminal console server
      if( _verbose )
        clog<<"UFTermServ::_recv> attempt to recv (socket): "<<nr<<endl;
      n = _instance->UFClientSocket::recv((unsigned char*)input, nr);
      if(_verbose)
	clog<<"UFTermServ::_recv> recv'd (socket): "<<n<<endl;
    }
    else { // use local serial port
      if( _verbose )
        clog<<"UFTermServ::_recv> attempt to recv (serialport): "<<na<<endl;
      n = _instance->UFSerialPort::recv((unsigned char*)input, nr);
    }
    if( n <= 0 ) {
      //clog<<"UFTermServ::_recv> returns: "<<n<<endl;
      return n;
    }
    r += input;
    size_t eotpos = r.rfind(_eotr);
    size_t rlen = r.length();
    size_t near = _eotr.length(); // is eot at/near the end of reply string?
    //clog << "UFTermServ::_recv> eotpos: " << eotpos << " rlen: " << rlen << " near: " << near << endl;
    //clog << "UFTermServ::_recv> _eotr: " << (int)(_eotr[0]) << endl;
    if( eotpos != string::npos && ((rlen - eotpos) == near) ) // terminator
      done = true;
    // another kludge for mce4 sim cmd oddity, 
    // after echoing command, prompt will not be forthcoming
    // until sim is complete, so don't perform blocking recv...
    size_t simpos = r.find("SIM");
    size_t echodone = r.rfind("\r");
    // sim echo complete?  
    if( simpos != string::npos && ((rlen - echodone) == 1) )
      done = true; // deal with the prompt on next cmd somehow?
    if( exact > 0 && exact == (int) rlen )
      done = true;
    // one more oddity:
    // the new Pfeiffer vacuum devices require an ACQ/ENQ handshake
    // so if we are talking with a Pfeiffer we may not actually be done
    // yet!
    char* acq = index(input, _acq);
    if( acq && done ) { // not done until _enq is sent an subsequent reply recv'd
      done = false;
      if( !localserial ) { // connected to the annex/iocomm/perle terminal console server
	if( _verbose )
          clog<<"UFTermServ::_recv> send ENQ (socket): "<<nr<<endl;
        _instance->UFClientSocket::send(&_enq, 1);
      }
      else { // use local serial port
        if( _verbose )
          clog<<"UFTermServ::_recv> send ENQ (serialport): "<<nr<<endl;
        _instance->UFSerialPort::send(&_enq, 1); 
      }   
    }
  }

  UFRuntime::rmJunk(r);
  if( _verbose )
    clog<<"UFTermServ::_recv> "<<r<<endl;

  return r.length();
} // _recv

// submit string, recv reply and return reply as string
int UFTermServ::submit(const string& s, string& r, float flsh, int exact) {
  string reply;
  //clog<<"UFTermServ::submit> to device: "<<s<<endl;
  int n = submit(s, flsh, exact);
  if( n <= 0 )
    return n;

  n = _recv(reply, exact);
  //clog<<"UFTermServ::submit> reply from device: "<<n<<endl;

  if( n > 0 )
    r = reply;

  return n;
}

string UFTermServ::bootPrompt() {
  string r;
  if( _instance && _instance->_available() > 0 )
    _instance->_recv(r);
  return r;
}

// all other serial devices provide single line replies terminated by "\n"?
// mce4, however, currently provides single and multi-line replies.
// submit s, recv multi-line reply and return reply as string vec.

int UFTermServ::submitMultiReply(const string& s, vector< string >& vr, float flsh) {
  vr.clear();
  string r, snd(s);
  bool done= true;
  int ns= 0;
  do {
    ns = submit(snd, r, flsh); // flush & wait for next "line" of reply
    if( ns <= 0 ) {
      return (int) vr.size();
    }
    //clog<<r<<ends;
    // kludge to decide if mce4 is prompting for a continuation?
    // .i.e ">>" lacks "GATIR" and is near the end of the string
    // only way to see these in gdb:
    size_t eotpos = r.rfind(_eotr);
    size_t rlen = r.length();
    size_t near = _eotr.length();
    done = ( (r.rfind("GATIR") != string::npos) &&
	     (eotpos != string::npos) && 
	     (rlen - eotpos == near) );
    UFRuntime::rmJunk(r);
    vr.push_back(r);
    snd = " "; // any key except "!" to continue! 
  } while( !done );

  return vr.size();
}

int UFTermServ::submitMultiReply(const string& s, string& r, float flsh) {
  vector< string > v;
  int nr = submitMultiReply(s, v, flsh);
  r = "";
  for( int i = 0; i < nr; ++i ) r += v[i];
  return nr;
}


#endif // __UFTermServ_cc__

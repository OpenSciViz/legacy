NNODS     2                                Number of Nod Phases
NNODSETS  4                                Number of NodSets
OBSERVAT  Gemini-South                     Name of Observatory
TELESCOP  Gemini-South                     Name of Telescope
INSTRUME  T-ReCS                           Name of Instrument
#
# TIME
TIMESYS   UT1                              Time system
DATE-OBS  2002-01-01                       UT Date of Observation (YYYY-MM-DD)
TIME-OBS  00:00:00.0                       UT of Observation
DATE      2002-01-01                       UT Date of Observation (YYYY-MM-DD)
UT        00:00:00.0                       UT of Observation
UTSTART   00:00:00.0                       UT at observation start
UTEND     00:00:00.0                       UT at observation end
LT        00:57:30.3                       Local time at observation start
ST        11:39:31.6                       Sidereal time at observation start
OBSEPOCH  2002.123456789                   Epoch at observation start
MJD-OBS   52341.4570689664                 MJD at observation start
#
# Gemini Program
GEMPRGID  GS-2002B-SV-1                    Gemini Science Program ID
OBSID     GS-2002B-SV-1-1                  Gemini Observation ID
DATALAB   GS-2002B-SV-1-1-001              DHS Data label
OBSERVER  Hayward                          Gemini Observers
SSA       Hainaut                          Gemini SSAs
OBSTYPE   OBJECT                           Observation Type
RAWIQ     Null                             Raw Image Quality
RAWCC     Null                             Raw Cloud Cover
RAWBG     Null                             Raw Background
RAWWV     Null                             Raw Water Vapour/Transparency
RAWGEMQA  Null                             Gemini Quality Assessment
RAWPIREQ  Null                             PI Requirements Met
#
# Science Target
OBJECT    NGC1234                          Target name
FRAME     FK5                              Target Reference Frame
EPOCH     2000.                            Target Coordinate Epoch
RA        162.525                          Target Right Ascension (degrees)
DEC       -0.01666667                      Target Declination (degrees)
PMRA      0.                               Target Proper Motion in RA
PMDEC     0.                               Target Proper Motion in Declination
PARALLAX  0.                               Target Parallax
RADVEL    0.                               Target Heliocentric Radial Velocity
TRKEQUIN  2000.                            Tracking equinox
TRKFRAME  FK5                              Tracking co-ordinate
TRKEPOCH  2002.1234                        Differential tracking reference epoch
RATRACK   0.                               Differential tracking rate RA
DECTRACK  0.                               Differential tracking rate Dec
WAVELENG  10.1                             Effective Target Wavelength (microns)
#
# Telescope Pointing
RADECSYS  FK5                              RA/Dec coordinate system reference
EQUINOX   2000.                            Equinox of coordinate system
CTYPE1    RA---TAN                         RA in tangent plane projection
CRPIX1    160                              Ref pix of axis 1
CRVAL1    12.34567890123                   RA at Ref pix (degrees)
CTYPE2    DEC--TAN                         Dec in tangent plane projection
CRPIX2    120                              Ref pix of axis 2
CRVAL2    12.34567890123                   Dec at Ref pix (degrees)
CD1_1     0.                               WCS matrix element 1,1
CD1_2     -2.50000e-5                      WCS matrix element 1,2
CD2_1     2.50000e-5                       WCS matrix element 2,1
CD2_2     0.                               WCS matrix element 2,2
HA        +00:49:18.57                     Telescope hour angle at start 
ELEVATIO  80.0000                          Elevation at start
AZIMUTH   180.0000                         Azimuth at start
AMSTART   1.088                            Airmass at start
AMEND     1.089                            Airmass at end
AIRMASS   1.088                            Mean airmass for the observation
HAEND     +00:49:18.57                     Telescope Hour Angle at end
ELEVEND   76.0000                          Telescope Elevation at end
AZIMEND   185.0000                         Telescope Azimuth at end
#
# Telescope Configuration
M2BAFFLE  INFRARED                         Position of M2 baffle
M2CENBAF  OPEN                             Position of M2 central hole baffle
SFRT2     -181.361                         Science fold rotation angle (degrees)
SFTILT    44.83                            Science fold tilt angle (degrees)
SFLINEAR  10.03                            Science fold linear position (mm)
INPORT    3                                Number of ISS port where T-ReCS was located
TELFOC    0.                               M2 focus at start
TELFOCEN  0.                               M2 focus at end
FOCOFFS   0.                               Offset from nominal focus
CRPA      0.                               Cass Rotator Position Angle at start
CRPAEND   0.                               Cass Rotator Position Angle at end
INSTALIG  0.                               Instrument Alignment Angle (degrees)
PA        180.                             Sky Position angle at start (degrees)
PAEND     0                                Sky Position angle at end (degrees)
PARANGLE  180.                             Parallactic angle at start (degrees)
CHPPA     0.                               M2 chop PA (degrees)
CHPTHROW  0.                               M2 chop throw (arcsec)
CHPFREQ   0.                               Chop frequency (Hz)
CHPDUTY   0.                               Chop duty cycle (%)
NODMODE   standard                         Nod Mode: standard or offset
NODTYPE   radec                            Nod Type: radec or azel
NODPA     0.                               Nod PA (degrees)
NODAMP    0.                               Nod Amplitude (arcsec)

#
# P1 or P2 parameters
# Derived from GMOS OIWFS
# Will be P1* or P2* depending on WFS in use.
# May also have P2B* for nod B (usually not necessary)
P2AOBJEC  PG1047+003-GS                    Object Name for WFS                
P2AEQUIN  2000.                            Equinox for WFS guide star coordinates       
P2AEPOCH  2000.                            Epoch for WFS guide star coordinates         
P2AFRAME  FK5                              OIWFS Target co-ordinate system                
P2ARA     162.55720833                     Guide star RA
P2ADEC    -0.00916667                      Guide star Declination
P2APMRA   0.                               Guide star Proper Motion in RA
P2APMDEC  0.                               Guide star Proper Motion in Declination
P2APARAL  0.                               Guide star Parallax                      
P2ARV     0.                               Guide star Radial Velocity             
P2AWAVEL  6500.                            WFS Effective Wavelength
P2AMAG    0                                Guide star magnitude
P2ARAD    0                                Probe radius
P2AANG    0                                Probe angle
P2AFILT   V                                WFS filter
P2AFREQ   0.                               WFS frame rate
P2ABIN    1                                WFS Binning mode
#
# INSTRUMENT
OBSMODE   chop-nod                         Observing mode (chop, nod, stare, chop-nod)
READMODE  test                             Detector Readout mode
#
COMPSTAT  NULL                             Observation completion status
FRMNAME   NULL                             Name assigned to a frame of data.
DATATYP   NULL                             Type of data frame ??
AXISLAB   NULL                             Axis label
AXISSZ    NULL                             Dimensions of frame
#
FRMTIME   0                                Frame time (ms)
CHPDELAY  0                                Chop settle time (ms)
SAVEFREQ  0                                Save frequency (Hz)
PRECHPT   0                                Time before valid chopping (sec)
PSTCHPT   0                                Time after valid chopping (sec)
NODTIME   0                                Nod time (sec)
NODDELAY  0                                Nod settle time (sec)
OBJTIME   0                                On-source time (min)
#
CHPCOADD  0                                Coadded chop cycles per saveset
CHPSETTL  0                                Frames discarded per chop settle
FRMCOADD  3                                Coadded frames per chop phase
SAVESETS  1                                Savesets per nod phase
NODSETS   1                                Nod cycles per total integration
NODSETTL  0                                Chop cycles per nod settle
NODSETTR  0                                Chop cycles coadded per nod
PRECHPS   1                                Pre-valid throwaway chop cycles
PSTCHPS   1                                Post-valid throwaway chop cycles
PIXCLOCK  2                                MCE4 Pixel Base Clock (PBC)
#
EFF_FRM   0.0                              Frame duty cycle (%)
EFF_CHP   0.0                              Chop duty cycle (%)
EFF_NOD   0.0                              Nod duty cycle (%)
#
SIGFRM    0.0                              Sigma of noise per frame readout (ADUs)
SIGMAX    0.0                              Maximum sigma of noise per frame
BKASTART  0.0                              Background ADUs at start of obs.
BKAEND    0.0                              Background ADUs at end of obs.
BKWSTART  0.0                              % filling of det. wells at start of obs.
BKWEND    0.0                              % filling of det. wells at end of obs.
BKWMAX    0.0                              maximum % filling of det. wells during obs.
#
VACUUM    0.001                            Torr
COLDPLAT  +298.7                           Kelvin
COLDSCTR  +298.7                           Kelvin
COLDSEDG  +298.7                           Kelvin    
COLDSTRP  +298.7                           Kelvin
FIRSTSTG  +298.7                           Kelvin
PASVSHLD  +298.7                           Kelvin
SLITWHL   +298.7                           Kelvin
WINTURR   +298.7                           Kelvin
COLDFING  +298.222                         Kelvin
DETARRAY  +299.111                         Detector Actual Temp (Kelvin)
DETSET    +299.111                         Detector Set Temp (Kelvin)
#
COLDCLAM  0pen                             ColdClamp Name
SECTOR    Open                             SectorWheel Name
WINDOW    ZnSe                             Window Name
APERTURE  Oversized                        Aperture Name
FILTER1   Open                             Filter#1 Name
LYOT      Clear-1                          Lyot Name
FILTER2   Si1-7.9um                        Filter#2 Name
PUPILIMA  Open                             PupilImaging Name
SLIT      Open                             Slit Name
GRATING   Mirror                           Grating Name
#
COLDCLAM  0                                ColdClamp Position
SECTOR    0                                SectorWheel Position
WINDOW    0                                Window Position
APERTURE  0                                Aperture Position
FILTER1   0                                Filter#1 Position
LYOT      0                                Lyot Position
FILTER2   0                                Filter#2 Position
PUPILIMA  0                                PupilImaging Position
SLIT      0                                Slit Position
GRATING   0                                Grating Position

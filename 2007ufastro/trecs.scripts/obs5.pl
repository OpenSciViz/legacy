#!/usr/bin/perl

# obs5.pl: Chop-Nod mode image sequence, 4 filters.
# Version 2002 Dec 11 (TLH).

# Load the Trecs module
use Trecs;
my $rcsId = '$Name:  $ $Id: obs5.pl 14 2008-06-11 01:49:45Z hon $';

# Useful variables, initialize here to default value
# Change these to suit your needs...
# Any non-empty values are automatically sent to the DB.  Please only
# specify values for those variables you wish to be sent to the DB.

### Initial Instrument Configuration ###

# Commanded camera mode
# "imaging"|"spectroscopy"
my $camera_mode = "imaging";

# Commanded imaging mode
# "field"|"window"|"pupil"
my $imaging_mode = "field" ;

# Commanded grating name
# "LowRes-10"|"HighRes-10"|"LowRes-20"
my $grating = "" ;

# Commanded central wavelength
my $central_wavelength = "" ;

# Commanded sector name
my $sector_name = "" ;

# Commanded Lyot stop name
my $lyot_stop_name = "Clear-1" ;

# Commanded slit width
my $slit_width = "Clear";

# Following require overrides if manually set.
# Each override = "TRUE"|"FALSE"

# Window
my $override_window = "FALSE" ;
my $window_setting = "" ;

# Aperture
my $override_aperture = "FALSE" ;
my $aperture_name = "" ;

# Filter
my $override_filter = "FALSE" ;
my $filter = "Si-7.9um";

# Lens
my $override_lens = "FALSE" ;
my $lens_name = "" ;


### Initial Observation Configuration ###

# Observation data label
my $datalabel = "trecs" . `date +%Y.%j.%H.%M` ;
chop $datalabel ;

# Commanded observing mode
# "chop-nod"|"stare"|"chop"|"nod"
my $observing_mode = "chop-nod" ;

# Commanded source photon collection time
# Should be 4 nods at 30 sec/nod currently hardcoded
my $photon_collection_time = "120" ;

# Commanded secondary chop throw magnitude
my $chop_throw = "" ;


##############################################################################
### Execute the first observation.  Do not change anything in this section ###
##############################################################################

# Initialize global variables
print "" ;
Trecs::init_vars();

# Output configuration
print "\n" ;
print "Initializing the following configuration:\n" ;
print "\n" ;
print " Database:            $Trecs::epics\n" ;
print "\n" ;
print " Camera Mode:         $camera_mode\n" ;
print " Imaging Mode:        $imaging_mode\n" ;
print " Aperture Name:       $aperture_name\n" ;
print " Filter Name:         $filter\n" ;
print " Grating:             $grating\n" ;
print " Central wavelength:  $central_wavelength\n" ;
print " Lens Name:           $lens_name\n" ;
print " Lyot Stop Name:      $lyot_stop_name\n" ;
print " Sector Name:         $sector_name\n" ;
print " Slit Width:          $slit_width\n" ;
print " Window Setting:      $window_setting\n" ;
print " Override Aperture:   $override_aperture\n" ;
print " Override Filter:     $override_filter\n" ;
print " Override Lens:       $override_lens\n" ;
print " Override Window:     $override_window\n" ;
print "\n" ;
print " Data label:          $datalabel\n" ;
print " Observing Mode:      $observing_mode\n" ;
print " On Source Time:      $photon_collection_time\n" ;
print " Chop Throw Angle:    $secondary_chop_throw_magnitude\n" ;
print "\n" ;


### Instrument configuration ###

print "\nSetting instrumentSetup parameters.\n" ;

if( $camera_mode        ne ""    ) { `$Trecs::ufcaput -p $Trecs::epics':instrumentSetup.A='$camera_mode`       ;}
if( $imaging_mode       ne ""    ) { `$Trecs::ufcaput -p $Trecs::epics':instrumentSetup.B='$imaging_mode`      ;}
if( $aperture_name      ne ""    ) { `$Trecs::ufcaput -p $Trecs::epics':instrumentSetup.C='$aperture_name`     ;}
if( $filter             ne ""    ) { `$Trecs::ufcaput -p $Trecs::epics':instrumentSetup.D='$filter`            ;}
if( $grating            ne ""    ) { `$Trecs::ufcaput -p $Trecs::epics':instrumentSetup.E='$grating`           ;}
if( $central_wavelength ne ""    ) { `$Trecs::ufcaput -p $Trecs::epics':instrumentSetup.F='$central_wavelength`;}
if( $lens_name          ne ""    ) { `$Trecs::ufcaput -p $Trecs::epics':instrumentSetup.G='$lens_name`         ;}
if( $lyot_stop_name     ne ""    ) { `$Trecs::ufcaput -p $Trecs::epics':instrumentSetup.H='$lyot_stop_name`    ;}
if( $sector_name        ne ""    ) { `$Trecs::ufcaput -p $Trecs::epics':instrumentSetup.I='$sector_name`       ;}
if( $slit_width         ne ""    ) { `$Trecs::ufcaput -p $Trecs::epics':instrumentSetup.J='$slit_width`        ;}
if( $window_setting     ne ""    ) { `$Trecs::ufcaput -p $Trecs::epics':instrumentSetup.K='$window_setting`    ;}
if( $override_aperture  eq "TRUE") { `$Trecs::ufcaput -p $Trecs::epics':instrumentSetup.L='$override_aperture` ;}
if( $override_filter    eq "TRUE") { `$Trecs::ufcaput -p $Trecs::epics':instrumentSetup.M='$override_filter`   ;}
if( $override_lens      eq "TRUE") { `$Trecs::ufcaput -p $Trecs::epics':instrumentSetup.N='$override_lens`     ;}
if( $override_window    eq "TRUE") { `$Trecs::ufcaput -p $Trecs::epics':instrumentSetup.O='$override_window`   ;}

# Note that William changed I.Seq. to config both instrumentSetup and observationSetup
# simultaneously, and correctly feeding instrumentSetup into observationSetup.

# Configure observationSetup.
print "\nSetting observationSetup Parameters. \n" ;
`$Trecs::ufcaput -p $Trecs::epics':dataMode.A=save'` ;
if( $observing_mode         ne "" ) { `$Trecs::ufcaput -p $Trecs::epics':observationSetup.A='$observing_mode`; }
if( $photon_collection_time ne "" ) { `$Trecs::ufcaput -p $Trecs::epics':observationSetup.B='$photon_collection_time` ;}
if( $chop_throw             ne "" ) { `$Trecs::ufcaput -p $Trecs::epics':observationSetup.C='$secondary_chop_throw_magnitude`;}

# Continue by setting the External Environment Fake Data
print "\nSetting Fake External Environment Parameters. \n" ;
# skyNoise
`$Trecs::ufcaput -p $Trecs::epics':observationSetup.D=20'` ;
# skyBackground
`$Trecs::ufcaput -p $Trecs::epics':observationSetup.E=20'` ;
# airMass
`$Trecs::ufcaput -p $Trecs::epics':observationSetup.F=1'` ;
# mirror Temperature
`$Trecs::ufcaput -p $Trecs::epics':observationSetup.G=0'` ;
# emissivity
`$Trecs::ufcaput -p $Trecs::epics':observationSetup.H=0.08'` ;

# Note: these records should be true in trecsInitialize.pv
# so that frame time is alway calculated by DC agent:
# $Trecs::ufcaput -p $Trecs::epics':observationSetup.J=true' ;
# and Det.Temp. Control is used:
# $Trecs::ufcaput -p $Trecs::epics':observationSetup.L=true' ;

my $status=0;

$status = apply_config();
if( $status == 0 ) { exit; }

$status = exec_obs($datalabel, -1, -1);
if( $status == 0 ) { exit; }

# Optional exit; comment out to continue script
# exit;

######################
## Next Observation ##
######################

# Commanded filter name
my $filter = "Si-10.4um";

# Observation data label
$datalabel = "trecs" . `date +%Y.%j.%H.%M` ;
chop $datalabel ;

print "\n" ;
print "Changing the configuration:\n" ;
print " Filter Name:         $filter\n" ;
print " Data label:          $datalabel\n" ;
print "\n" ;

 `$Trecs::ufcaput -p $Trecs::epics':instrumentSetup.D='$filter` ;

$status = apply_config();
if( $status == 0 ) { exit; }

$status = exec_obs($datalabel, -1, -1);
if( $status == 0 ) { exit; }

######################
## Next Observation ##
######################

# Commanded filter name
my $filter = "Si-11.7um";

# Observation data label
$datalabel = "trecs" . `date +%Y.%j.%H.%M` ;
chop $datalabel ;

print "\n" ;
print "Changing the configuration:\n" ;
print " Filter Name:         $filter\n" ;
print " Data label:          $datalabel\n" ;
print "\n" ;

 `$Trecs::ufcaput -p $Trecs::epics':instrumentSetup.D='$filter` ;

$status = apply_config();
if( $status == 0 ) { exit; }

$status = exec_obs($datalabel, -1, -1);
if( $status == 0 ) { exit; }

######################
## Next Observation ##
######################

# Commanded filter name
my $filter = "Si-12.5um";

# Observation data label
$datalabel = "trecs" . `date +%Y.%j.%H.%M` ;
chop $datalabel ;

print "\n" ;
print "Changing the configuration:\n" ;
print " Filter Name:         $filter\n" ;
print " Data label:          $datalabel\n" ;
print "\n" ;

 `$Trecs::ufcaput -p $Trecs::epics':instrumentSetup.D='$filter` ;

$status = apply_config();
if( $status == 0 ) { exit; }

$status = exec_obs($datalabel, -1, -1);
if( $status == 0 ) { exit; }


#
#***********************************************************************
#***  C A N A D I A N   A S T R O N O M Y   D A T A   C E N T R E  *****
#
# (c) 1997				(c) 1997
# National Research Council		Conseil national de recherches
# Ottawa, Canada, K1A 0R6 		Ottawa, Canada, K1A 0R6
# All rights reserved			Tous droits reserves
# 					
# NRC disclaims any warranties,	Le CNRC denie toute garantie
# expressed, implied, or statu-	enoncee, implicite ou legale,
# tory, of any kind with respect	de quelque nature que se soit,
# to the software, including		concernant le logiciel, y com-
# without limitation any war-		pris sans restriction toute
# ranty of merchantability or		garantie de valeur marchande
# fitness for a particular pur-	ou de pertinence pour un usage
# pose.  NRC shall not be liable	particulier.  Le CNRC ne
# in any event for any damages,	pourra en aucun cas etre tenu
# whether direct or indirect,		responsable de tout dommage,
# special or general, consequen-	direct ou indirect, particul-
# tial or incidental, arising		ier ou general, accessoire ou
# from the use of the software.	fortuit, resultant de l'utili-
# 					sation du logiciel.
#
#***********************************************************************
#
# FILENAME
# dhsStatus/config/dhsStatus.config
#
# PURPOSE:
# Configuration file for the DHS status server.
#
#INDENT-OFF*
# $Log: dhsStatus.config,v $
# Revision 0.0  2002/06/03 17:44:36  hon
# initial checkin
#
# Revision 0.0  2002/02/11 16:26:50  hon
# *** empty log message ***
#
# Revision 1.10  1998/06/16 22:23:16  jaeger
# Fixed resource fullname record.
#
# Revision 1.9  1998/06/02 18:03:10  jaeger
# Added fullName record to resource records.
#
# Revision 1.8  1998/05/12 20:27:34  nhill
# Added an epics flag to the channel data.
# Added merged channel keywords.
# Added a timeout.
#
# Revision 1.7  1998/04/02 20:18:31  jaeger
# Changed temp and perm directories to a legitimate directory on salish.
#
# Revision 1.6  1998/03/16 21:56:20  cockayne
# Added Location/Site information.
#
# Revision 1.5  1998/03/06 19:38:34  jaeger
# Added config info. for monitoring resources.
#
# Revision 1.4  1998/02/17 00:26:58  jaeger
# Added config information of monitoring of resources.
#
# Revision 1.3  1997/08/06 21:23:40  dunn
# Added lines for DTS data server
#
# Revision 1.2  1997/06/13 20:45:49  nhill
# Added the quick look server.
#
# Revision 1.1  1997/04/16 22:32:55  nhill
# Initial revision
#
#INDENT-ON*
#
#***  C A N A D I A N   A S T R O N O M Y   D A T A   C E N T R E  *****
#***********************************************************************
#

#
#  Application identification
#
# Keyword	application 
#
# -------	-----------
#

identity	statusServerNS


#
#  Imp information
#
# keyword	Max connects	Client retry period
#                               (seconds)
# --------	------------	--------------
#

imp		20		30


#
#  EPICS channel access information.
#
# keyword	SAD host		timeout
# -------	--------		-------
#

epics		10.1.2.174 		10


#
#  Location/Site information. 
#
# keyword	Gemini Location		Gemini Site
#		("N"orth or "S"outh)	("S"ummit, "B"ase)
#-------	--------------------	-------------------
#
 
locationSite	N			S


#
#  DHS subsystem information.
#
# keyword	subsystem
#
# -------	---------
#

subsystem	STA
subsystem	CMD
#subsystem	HIS
subsystem	QLS
subsystem	DTS


#
#  EPICS channel information.
#
# keyword	system	channel name		alias			epics
#
# -------	------	------------		-----			-----
#

channel		-	logMessage		logMessage		no

channel		-	resourceArraySize	resourceArraySize	no
channel		-	resource%d:inUse	resource%d:inUse	no
channel		-	resource%d:max		resource%d:max		no
channel		-	resource%d:name		resource%d:name		no
channel		-	resource%d:fullname	resource%d:fullname	no
channel		-	resource%d:type		resource%d:type		no
channel		-	resource%d:used		resource%d:used		no
channel		-	resource%d:units	resource%d:units	no


channel		*	health			health			no
channel		*	healthDesc		healthDesc		no
channel		*	state			state			no
channel		*	debug			debug			no
channel		*	simulate		simulate		no

channel		DTS	numIncomplete		numIncomplete		no


#
# Control information for merging subsystem channels.
#
# Keyword	channel		values
# -------	-------		------
#

merge		health		BAD WARNING GOOD
merge		state 		TESTING INITIALIZING STARTING SHUTTINGDOWN RUNNING
merge		debug		FULL MIN NONE
merge		simulate 	FULL FAST VSM NONE


#
# Montoring information
#

timer		90


#
# keyword 	descriptive 		path
#		name
#--------	-------------		-----------------
#

md		"Temp Store"		/staging/temp
md		"Perm Store"		/staging/perm
md		"dhstrecs /tmp"		/tmp

#
# Database monitoring information
#

#
# keyword	server name
#--------	-----------
#

#dbServer	GEMINI_NORTH_SUMMIT
dbServer	xxx

#
# keyword 	descriptive 		database
#		name			name
#--------	-------------		-----------------
#

#db		"dhsDB_NS db"		dhsDB_NS

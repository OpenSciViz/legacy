#!/usr/bin/perl

# instrument setup command soak test.
# Version 2003 Feb 11 (WNR).

# Load the Miri module
use Miri;


### Instrument Configuration ###

# camera modes
my @camera_modes = ("imaging", "spectroscopy");

# imaging modes
my @imaging_modes = ("field", "window", "pupil");

# gratings
my @grating_names = ("Mirror", "LowRes-10", "LR_Ref_Mirror", "LowRes-20", "HR_Ref_Mirror", "HighRes-10");

# central wavelengths
my @wavelengths = ("7.9", "8.6", "8.8", "9.7", "10.4", "11.7", "12.3", "18.3", "24.5", "20.8");

# sectors
my @sector_names = ("Open", "BB-Low", "BB-High", "Polystyrene");

# lyot stops
my @lyot_names = ("Grid_Mask", "Spot_Mask", "Stop_F", "Stop_B", "Quakham_Mask", "Polystyrene", "Clear-1", "Clear-2", "Circular-d", "Circular-c", "Circular-a", "Circular-b", "Blank"); 

# slit widths
my @slit_widths = ("Clear", "1.32", "0.72", "0.66", "0.36", "0.31", "0.26", "0.21");

# apertures
my @aperture_names = ("Grid_Mask", "Matched", "Oversized", "Window_Imager", "Spot_Mask");

# filters
my @filter_names = ("Clear", "Si-7.9um", "PAH-8.6um", "Si-8.8um", "Si-9.7um", "Si-10.4um", "Si-11.7um", "Si-12.3um", "K", "L", "M", "N", "Qs-18.3um", "Ql-24.5um", "Qw-20.8um", "Align-Spot");

# lenses
my @lens_names = ("Clear-1", "Pupil_Imager", "Clear-2", "Clear-3");

# window names
my @window_names = ("Blankoff", "KRS-5", "ZnSe", "KBr", "KBrC");

# individual selections
my $camera_mode = "";
my $imaging_mode = "";
my $grating_name = "";
my $wavelength = "";
my $sector_name = "";
my $lyot_name = "";
my $slit_width = "";
my $aperture_name = "";
my $filter_name = "";
my $lens_name = "";
my $window_name = "";

my $camera_state = "";
my $imaging_state = "";
my $grating_state = "";
my $wavelength_state = "";
my $sector_state = "";
my $lyot_state = "";
my $slit_state = "";
my $aperture_state = "";
my $filter_state = "";
my $lens_state = "";
my $window_state = "";

my $override_window = "FALSE" ;
my $override_aperture = "FALSE" ;
my $override_filter = "FALSE" ;
my $override_lens = "FALSE" ;

##############################################################################
### Ring the changes,  this should test most configurations                ###
##############################################################################

# Initialize global variables
print "" ;
Miri::init_vars();

# First pass overrides all automatic selections so we will
# get all positions...

$override_window = "TRUE" ;
$override_aperture = "TRUE" ;
$override_filter = "TRUE" ;
$override_lens = "TRUE" ;

`$Miri::ufcaput -p $Miri::epics':instrumentSetup.L='$override_aperture`   ;
`$Miri::ufcaput -p $Miri::epics':instrumentSetup.M='$override_filter`     ;
`$Miri::ufcaput -p $Miri::epics':instrumentSetup.N='$override_lens`       ;
`$Miri::ufcaput -p $Miri::epics':instrumentSetup.O='$override_window`     ;

Miri::tsprint "\n" ;
Miri::tsprint "First pass is with all overrides set..\n\n";
Miri::tsprint "\n" ;

my $status = 0;

for ($i = 0; $i < 20; $i++) {

	# First create an instrument configuration
	$camera_mode 	= $camera_modes[$i % 2];
	$imaging_mode 	= $imaging_modes[$i % 3];
	$wavelength 	= $wavelengths[$i % 10];
	$aperture_name 	= $aperture_names[$i % 5];
	$filter_name 	= $filter_names[$i % 16];
	$grating_name 	= $grating_names[$i % 6];
	$lens_name 	= $lens_names[$i % 4];
	$lyot_name 	= $lyot_names[$i % 13];
	$sector_name 	= $sector_names[$i % 4];
	$slit_width 	= $slit_widths[$i % 8];
	$window_name 	= $window_names[$i % 5];

	#  Send the configuration to the instrumentSetup CAD record
	`$Miri::ufcaput -p $Miri::epics':instrumentSetup.A='$camera_mode`       ;
	`$Miri::ufcaput -p $Miri::epics':instrumentSetup.B='$imaging_mode`      ;
	`$Miri::ufcaput -p $Miri::epics':instrumentSetup.C='$aperture_name`     ;
	`$Miri::ufcaput -p $Miri::epics':instrumentSetup.D='$filter_name`       ;
	`$Miri::ufcaput -p $Miri::epics':instrumentSetup.E='$grating_name`      ;
	`$Miri::ufcaput -p $Miri::epics':instrumentSetup.F='$c_wavelength`	;
	`$Miri::ufcaput -p $Miri::epics':instrumentSetup.G='$lens_name`         ;
	`$Miri::ufcaput -p $Miri::epics':instrumentSetup.H='$lyot_name`         ;
	`$Miri::ufcaput -p $Miri::epics':instrumentSetup.I='$sector_name`       ;
	`$Miri::ufcaput -p $Miri::epics':instrumentSetup.J='$slit_width`        ;
	`$Miri::ufcaput -p $Miri::epics':instrumentSetup.K='$window_name`       ;

	# Then apply the configuration and see what happens
	$status = apply_config();

	# All done, for better or for worse ... see what happened
	Miri::tsprint "\n" ;
	Miri::tsprint "Results of applying the following configuration:\n" ;

	$camera_state = `$Miri::ufcaget $Miri::epics':sad:cameraMode.'`	;
	Miri::tsprint " Camera Mode:         $camera_mode: $camera_state";

	$imaging_state = `$Miri::ufcaget $Miri::epics':sad:imagingMode'`	;
	Miri::tsprint " Imaging Mode:        $imaging_mode: $imaging_state";

	$wavelength_state = `$Miri::ufcaget $Miri::epics':sad:wavelength'`	;
	Miri::tsprint " Central wavelength:  $wavelength: $wavelength_state" ;

	$aperture_state = `$Miri::ufcaget $Miri::epics':sad:apertureName'`	;
	Miri::tsprint " Aperture Name:       $aperture_name: $aperture_state" ;

	$filter_state = `$Miri::ufcaget $Miri::epics':sad:filterName'`	;
	Miri::tsprint " Filter Name:         $filter_name: $filter_state" ;

	$grating_state = `$Miri::ufcaget $Miri::epics':sad:gratingName'`	;
	Miri::tsprint " Grating:             $grating_name: $grating_state" ;

	$lens_state = `$Miri::ufcaget $Miri::epics':sad:lensName'`		;
	Miri::tsprint " Lens Name:           $lens_name: $lens_state" ;

	$lyot_state = `$Miri::ufcaget $Miri::epics':sad:lyotName'`	;
	Miri::tsprint " Lyot Stop Name:      $lyot_name: $lyot_state" ;

	$sector_state = `$Miri::ufcaget $Miri::epics':sad:sectorName'`	;
	Miri::tsprint " Sector Name:         $sector_name: $sector_state" ;

	$slit_state = `$Miri::ufcaget $Miri::epics':sad:slitName'`	;
	Miri::tsprint " Slit Width:          $slit_width: $slit_state" ;

	$window_state = `$Miri::ufcaget $Miri::epics':sad:windowName'`	;
	Miri::tsprint " Window Setting:      $window_name: $window_state" ;

	if( $status == 0 ) { exit; }

}


# Optional exit; comment out to continue script
exit;


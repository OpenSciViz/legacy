#
#***********************************************************************
#***  C A N A D I A N   A S T R O N O M Y   D A T A   C E N T R E  *****
#
# (c) 1997				(c) 1997
# National Research Council		Conseil national de recherches
# Ottawa, Canada, K1A 0R6 		Ottawa, Canada, K1A 0R6
# All rights reserved			Tous droits reserves
# 					
# NRC disclaims any warranties,	Le CNRC denie toute garantie
# expressed, implied, or statu-	enoncee, implicite ou legale,
# tory, of any kind with respect	de quelque nature que se soit,
# to the software, including		concernant le logiciel, y com-
# without limitation any war-		pris sans restriction toute
# ranty of merchantability or		garantie de valeur marchande
# fitness for a particular pur-	ou de pertinence pour un usage
# pose.  NRC shall not be liable	particulier.  Le CNRC ne
# in any event for any damages,	pourra en aucun cas etre tenu
# whether direct or indirect,		responsable de tout dommage,
# special or general, consequen-	direct ou indirect, particul-
# tial or incidental, arising		ier ou general, accessoire ou
# from the use of the software.	fortuit, resultant de l'utili-
# 					sation du logiciel.
#
#***********************************************************************
#
# FILENAME
# dhsData/config/dhsData.config
#
# PURPOSE:
# Configuration file for the DHS data server.
#
#INDENT-OFF*
# $Log: dhsData.config,v $
# Revision 0.2  2002/07/08 17:45:24  hon
# *** empty log message ***
#
# Revision 0.0  2002/06/03 17:44:36  hon
# initial checkin
#
# Revision 0.0  2002/02/11 16:26:48  hon
# *** empty log message ***
#
#INDENT-ON*
#
#***  C A N A D I A N   A S T R O N O M Y   D A T A   C E N T R E  *****
#***********************************************************************
#

#
#  Application identification
#
# Keyword	application 
#
# -------	-----------
#

identity	dataServerNS



#
#  Imp information
#
# keyword       Max connects    Client retry period
#                               (seconds)
# --------      ------------    --------------
#

imp             20              120



#
#  Database information - Warning, cannot name unique table "unique" that is
#                         a Sybase reserved word.
#
# keyword	ServerName	DatabaseName	Unique		Dataset	Process
#						Tbl		Tbl	Tbl
# --------	------------	------------	------		-------	-------
#

database	scott 		dhsDB_NS        uniqueName	dataset	process

#
#  Compression information - Data Server uncompresses stored compressed files.
#
# keyword	Compression	Compression
#		Extension	Type
# --------	-----------	-----------
#

compress	".gz"		"GZIP"


#
#  Disk Storage information
#
# keyword	Temporary	Permanent	
#		Volume Name	Volume Name 
#               (Volume names must exist in the
#		mds table and contains the path
#		to where files are written )
# --------	-----------	-----------
#

storage		TEMP		PERM	


#
#  Server Identity Names that the Data Server connects to
#
# keyword	Storage		Oldp		Status		QuickLook
# -------	-------		----		------		---------
#

servers		sim4DataNS	sim4DataNS	statusServerNS	qlServerNS


#
#  Authorized sites connection information.
#  Accepts wildcard "*" in identity for client connections only.
#
# keyword	Identity		IP Address
# -------	--------		-------
#

authorized	statusServerNS		128.227.184.39
authorized	qlServerNS              128.227.184.39
authorized	commandServerNS         128.227.184.39
authorized 	dhsPut*			128.227.184.39
authorized 	dhsGet*			128.227.184.39
# For UF instruments: TRECS, Flamingos
authorized	dhstrecs		128.227.184.39
authorized	dhsclient.39		128.227.184.39
authorized	TReCSArchive.39		128.227.184.39
authorized	TReCSQuickLook.39	128.227.184.39
authorized	FlamArchive.39		128.227.184.39
authorized	FlamQuickLook.39	128.227.184.39
authorized 	newton			128.227.184.138
authorized	dhsclient.138		128.227.184.138
authorized	TReCSArchive.138	128.227.184.138
authorized	TReCSQuickLook.138	128.227.184.138
authorized	FlamArchive.138		128.227.184.138
authorized	FlamQuickLook.138	128.227.184.138
authorized 	dhsPutIt.138		128.227.184.138
authorized 	dhsGetIt.138		128.227.184.138
authorized 	dhstrecsClient.138		128.227.184.138
authorized	ufkepler		128.227.184.204
authorized	dhsclient.204		128.227.184.204
authorized	TReCSArchive.204	128.227.184.204
authorized	TReCSQuickLook.204	128.227.184.204
authorized	FlamArchive.204		128.227.184.204
authorized	FlamQuickLook.204	128.227.184.204
authorized 	dhsPutIt.204		128.227.184.204
authorized 	dhsGetIt.204		128.227.184.204
authorized 	dhstrecsClient.204	128.227.184.204
authorized	trifid			128.227.184.237
authorized	dhsclient.237		128.227.184.237
authorized	TReCSArchive.237	128.227.184.237
authorized	TReCSQuickLook.237	128.227.184.237
authorized	FlamArchive.237		128.227.184.237
authorized	FlamQuickLook.237	128.227.184.237
authorized 	dhsPutIt.237		128.227.184.237
authorized 	dhsGetIt.237		128.227.184.237
authorized 	dhstrecsClient.237	128.227.184.237

#
#  Default dataset and unique name settings.
#
# keyword	Lifetime	Location	Source
#	     ("P"erm or"T"emp)	("N" or "S")	("O"CS, "S"ummit, "B"ase)
#-------    -----------------	------------	-------------------
#

names		P		N		S


#
#  Fits table name where fits header information is stored.
#
# keyword	Fits table name
# -------	---------------
#

fitsTable	fitsTable


#
#  Archive Segregation Directory Name
#
# keyword	Segregation Dir
# -------	----------------
#

segregation	GEMINI_N

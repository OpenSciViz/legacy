#include "stdio.h"

#include "iostream"
#include "iomanip"
#include "sstream"

using namespace std;

int main(int argc, char** argv, char** envp) {
  stringstream s; s.width(4); s.precision(4);
  const double v = 7.00;
  s << v << ends;
  clog<<s.str()<<endl; s.str("");
  s << " or " << 7.01 << ends;
  clog<<s.str()<<endl;

  stringstream ss;
  ss << setw(4) << setprecision(4) << v << ends;
  clog<< setw(4) << setprecision(4) << ss.str()<<endl;

  stringstream sc;
  char vs[10]; ::memset(vs, 0 , sizeof(vs));
  ::sprintf(vs, "%4.2f", v);
  sc << vs << ends;
  clog<<vs<<"  " <<sc.str()<<endl; 

  printf("v = %f, vs = %s\n", v, vs);

  return 0;
}

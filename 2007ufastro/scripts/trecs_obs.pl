#!/usr/bin/perl
# RCS: "$Name:  $ $Id: trecs_obs.pl 14 2008-06-11 01:49:45Z hon $"

# Useful variables, initialize here to default value
# Change these to suit your needs...
# Any non-empty values are automatically sent to the DB.  Please only
# specify values for those variables you wish to be sent to the DB.

### Database Configuration ###

# epics refers to the database to use, at present
# "trecs"|"miri"
my $epics = "miri" ;

### Instrument Configuration ###

# Commanded camera mode
# "imaging"|"spectroscopy"
my $camera_mode = "imaging";

# Commanded imaging mode
# "field"|"window"|"pupil"
my $imaging_mode = "field" ;

# Commanded aperture name
my $aperture_name = "" ;

# Commanded filter name
my $filter = "Si-11.7um";

# Commanded grating name
# "LowRes-10"|"HighRes-10"|"LowRes-20"
my $grating = "" ;

# Commanded central wavelength
my $central_wavelength = "" ;

# Commanded lens name
my $lens_name = "" ;

# Commanded Lyot stop name
my $lyot_stop_name = "" ;

# Commanded sector name
my $sector_name = "" ;

# Commanded slit width
my $slit_width = "";

# Commanded window setting
my $window_setting = "" ;

# Override automatic aperture selection
# "TRUE"|"FALSE"
my $override_aperture = "FALSE" ;

# Override automatic filter selection
# "TRUE"|"FALSE"
my $override_filter = "FALSE" ;

# Override automatic lens selection
my $override_lens = "FALSE" ;

# Override automatic window selection
my $override_window = "FALSE" ;

### Observation Configuration ###

# Observation data label
#my $datalabel = "TReCSImage" . `date +%Y.%j.%H.%M` ;
$datalabel = "moo";

# Commanded observing mode
# "chop-nod"|"stare"|"chop"|"nod"
my $observing_mode = "chop-nod" ;

# Commanded source photon collection time
my $photon_collection_time = "1" ;

# Commanded secondary chop throw magnitude
my $secondary_chop_throw_magnitude = "40" ;

############################################################
### It is unnecessary to modify anything below this line ###
############################################################

# directive enum from /gemini/epics/base/include/cad.h
# if these change this script is broken.
my $CAD_ABORT = 0;
my $CAD_MARK = 0;
my $CAD_CLEAR  = 1;
my $CAD_PRESET = 2;
my $CAD_START = 3;
my $CAD_STOP = 4;
my $CAD_ACCEPT = 0; 
my $CAD_REJECT = -1;

print "\n" ;

my $UFINSTALL = $ENV{ "UFINSTALL" } ;
if( !defined( $UFINSTALL ) )
  {
    $UFINSTALL = "/usr/local/uf" ;
    print "Setting UFINSTALL to /usr/local/uf\n" ;
  }

$ufcaget = "$UFINSTALL/bin/ufcaget";
$ufcaput = "$UFINSTALL/bin/ufcaput";
$ufsleep = "$UFINSTALL/bin/ufsleep";

if( ! -e "$ufcaget" )
  {
    print "\n";
    print "Unable to locate $ufcaget, please ensure that this program is\n";
    print "present in your $UFINSTALL directory path, or adjust the\n";
    print "$UFINSTALL environment variable accordingly.\n";
    print "\n" ;
    exit(-1);
  }

if( ! -e "$ufcaput" )
  {
    print "\n" ;
    print "Unable to locate $ufcaput, please ensure that this program is\n";
    print "present in your $UFINSTALL directory path, or adjust the\n";
    print "$UFINSTALL environment variable accordingly.\n";
    print "\n" ;
    exit(-1);
  }

if( ! -e "$ufsleep" )
  {
    print "\n" ;
    print "Unable to locate $ufsleep, please ensure that this program is\n";
    print "present in your $UFINSTALL directory path, or adjust the\n";
    print "$UFINSTALL environment variable accordingly.\n";
    print "\n" ;
    exit(-1);
  }

# Output configuration
print "\n" ;
print "Running with the following configuration:\n" ;
print "\n" ;
print " Database:            $epics\n" ;
print "\n" ;
print " Camera Mode:         $camera_mode\n" ;
print " Imaging Mode:        $imaging_mode\n" ;
print " Aperture Name:       $aperture_name\n" ;
print " Filter Name:         $filter\n" ;
print " Grating:             $grating\n" ;
print " Central wavelength:  $central_wavelength\n" ;
print " Lens Name:           $lens_name\n" ;
print " Lyot Stop Name:      $lyot_stop_name\n" ;
print " Sector Name:         $sector_name\n" ;
print " Slit Width:          $slit_width\n" ;
print " Window Setting:      $window_setting\n" ;
print " Override Aperture:   $override_aperture\n" ;
print " Override Filter:     $override_filter\n" ;
print " Override Lens:       $override_lens\n" ;
print " Override Window:     $override_window\n" ;
print "\n" ;
print " Data label:          $datalabel\n" ;
print " Observing Mode:      $observing_mode\n" ;
print " On Source Time:      $photon_collection_time\n" ;
print " Chop Throw Angle:    $secondary_chop_throw_magnitude\n" ;
print "\n" ;

### Instrument configuration begins here ###

# Start by setting up the optical config of instrument.
print "Configuring the instrument with instrumentSetup\n" ;

if( $camera_mode ne "" )
  {
    `$ufcaput -p $epics':instrumentSetup.A='$camera_mode` ;
  }
if( $imaging_mode ne "" )
  {
    `$ufcaput -p $epics':instrumentSetup.B='$imaging_mode` ;
  }
if( $aperture_name ne "" )
  {
    `$ufcaput -p $epics':instrumentSetup.C='$aperture_name` ;
  }
if( $filter ne "" )
  {
    `$ufcaput -p $epics':instrumentSetup.D='$filter` ;
  }
if( $grating ne "" )
  {
    `$ufcaput -p $epics':instrumentSetup.E='$grating` ;
  }
if( $central_wavelength ne "" )
  {
    `$ufcaput -p $epics':instrumentSetup.F='$central_wavelength` ;
  }
if( $lens_name ne "" )
  {
    `$ufcaput -p $epics':instrumentSetup.G='$lens_name` ;
  }
if( $lyot_stop_name ne "" )
  {
    `$ufcaput -p $epics':instrumentSetup.H='$lyot_stop_name` ;
  }
if( $sector_name ne "" )
  {
    `$ufcaput -p $epics':instrumentSetup.I='$sector_name` ;
  }
if( $slit_width ne "" )
  {
    `$ufcaput -p $epics':instrumentSetup.J='$slit_width` ;
  }
if( $window_setting ne "" )
  {
    `$ufcaput -p $epics':instrumentSetup.K='$window_setting` ;
  }
if( $override_aperture eq "TRUE" )
  {
    `$ufcaput -p $epics':instrumentSetup.L='$override_aperture` ;
  }
if( $override_filter eq "TRUE" )
  {
    `$ufcaput -p $epics':instrumentSetup.M='$override_filter` ;
  }
if( $override_lens eq "TRUE" )
  {
    `$ufcaput -p $epics':instrumentSetup.N='$override_lens` ;
  }
if( $override_window eq "TRUE" )
  {
    `$ufcaput -p $epics':instrumentSetup.O='$override_window` ;
  }

# Note that William changed I.Seq. to config both instrumentSetup and observationSetup
# simultaneously, and correctly feeding instrumentSetup into observationSetup.

# Go on to setting up the observation.
print "Configuring the instrument with observationSetup\n" ;
`$ufcaput -p $epics':dataMode.A=save'` ;

# Obs. Mode
`$ufcaput -p $epics':observationSetup.A=$observing_mode'` ;
# On Source Time (mins)
`$ufcaput -p $epics':observationSetup.B='$photon_collection_time` ;
# Chop Throw (secondary_chop_throw_magnitude)
`$ufcaput -p $epics':observationSetup.C='$secondary_chop_throw_magnitude` ;

# Continue by setting the External Environment Fake Data
print "Setting External Environment Fake Data\n" ;
# skyNoise
`$ufcaput -p $epics':observationSetup.D=20'` ;
# skyBackground
`$ufcaput -p $epics':observationSetup.E=20'` ;
# airMass
`$ufcaput -p $epics':observationSetup.F=1'` ;
# mirror Temperature
`$ufcaput -p $epics':observationSetup.G=0'` ;
# emissivity
`$ufcaput -p $epics':observationSetup.H=0.08'` ;

# Note: these records should be true in trecsInitialize.pv
# so that frame time is alway calculated by DC agent:
# $ufcaput -p $epics':observationSetup.J=true' ;
# and Det.Temp. Control is used:
# $ufcaput -p $epics':observationSetup.L=true' ;

# Apply the instrument configuration: check to see if the configuration
# is valid and, if so, wait for the re-configuration to complete
# (1st start):
`$ufsleep 0.5` ;
print "Applying the configuration...\n" ;

`$ufcaput -p $epics':apply.DIR='$CAD_START` ;
$ack = "" ;

# if caget times-out...
while( $ack eq "" ) 
  {
    `$ufsleep 1.0` ;
    $ack = `$ufcaget $epics':'apply.VAL` ;
  }
print "Apply result: $ack" ;

if( $ack eq "-1" )
  {
    $msg = `$ufcaget $epics':'apply.MESS` ;
    print "Instrument setup/configuration failed/rejected':' $msg" ;
    exit;
  }

$cfgstat = `$ufcaget $epics':applyC'` ;
chop $cfgstat ;
print "Configuring Instrument for $camera_mode mode, status: $cfgstat\n" ;

# wait for the instrument configuration to complete
while ( "$cfgstat" ne "IDLE" && "$cfgstat" ne "ERR" )
  {
    `$ufsleep 2.0` ;
    $cfgstat = `$ufcaget $epics':applyC'` ;
    chop $cfgstat ;
    print "Configuring Instrument for $camera_mode mode, status: $cfgstat\n" ;
  }

if( "$cfgstat" eq "ERR" )
  {
    print "Configuration status is: $cfgstat\n" ;
    $msg = `$ufcaget $epics':'apply.OMSS` ;
    chop $msg ;
    print "Instrument configuration failed, message: $msg\n" ;
    exit ;
  }

# Once the optics and observation is configured (this includes setting the detector
# temperature and waiting for it to stabilize) send the observation ID and
# start the exposure.
`$ufsleep 1.0` ;

# Observation Data Label
print "Beginning Observation\n" ;
`$ufcaput -p $epics':observe.A='$datalabel` ;

# The above should mark the observe record -- hon.
`$ufcaput -p $epics':observe.DIR='$CAD_MARK` ;

# Start the observation (exposure)
# (2nd start):
`$ufsleep 0.5` ;
`$ufcaput -p $epics':apply.DIR='$CAD_START` ;

$dt0 = `date` ;
$ack = "" ;

# if caget times-out...
while( $ack == "" ) 
  {
    `$ufsleep 1.0` ;
    $ack = `$ufcaget $epics':'apply.VAL` ;
    chop $ack ;
  }

if( $ack == "-1" )
  {
    $msg = `$ufcaget $epics':'apply.MESS` ;
    chop $msg ;
    print "Observation Start failed/rejected, message: $msg\n" ;
    exit;
  }

`$ufsleep 2.0` ;
$CARtop = `$ufcaget $epics':applyC'` ;
chop $CARtop ;
print "CAR status: $CARtop\n" ;

if( $CARtop eq "ERR" )
  {
    $msg = `$ufcaget $epics':'apply.OMSS` ;
    chop $msg ;
    print "Instrument OBS start failed: $msg\n" ;
    exit;
  }

# wait for the observation  to complete
$CARobs = `$ufcaget $epics':observeC'` ;
chop $CARobs ;
$dt0 = `date` ;
print "Imaging OBS status $CARobs\n" ;

while ( $CARobs eq "BUSY" )
  {
    `$ufsleep 3.0` ;
    $dt = `date` ;
    $CARobs = `$ufcaget $epics':observeC'` ;
    chop $CARobs ;
    print "$dt0: $dt Imaging OBS CAR: $CARobs, label: $datalabel\n" ;
    $OBSstat = `$ufcaget $epics':sad:observationStatus'` ;
    chop $OBSstat ;
    print "observationStatus = $OBSstat\n" ;
  }

if( $CARobs eq "ERR" )
  {
    print "$dt0: $dt $CARobs, Imaging Observation, label: $datalabel Failed" ;
  }

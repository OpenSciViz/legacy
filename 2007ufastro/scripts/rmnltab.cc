#include "stdio.h"
#include "stdlib.h"
#include "iostream"
#include "string"

using namespace std;
const string usage = "usage: rmcr < oldfile-with-cr > newfile-without-cr";

int main(int argc, char** argv) {
  //clog<<"argc: "<<argc<<endl;
  //clog<<"argv: "<<*argv<<endl;
  if( argc > 1 ) {
    cout<<usage<<endl;
    exit(0);
  }
  int i= 0, c= getchar();
  int c0 = c;
  while( c != EOF ) {
    if( c == '\n' && c0 == '\t' ) {
      putchar(' ');
    }
    else if( c != 13 ) { // not '\r'
      putchar(c);
    }
    else {
      ++i; putchar(10); // replace '\r' with '\n'
    }
    c0 = c; c = getchar();
  }

  clog<<"rmcr> carriage return cnt: "<<i<<endl;
  return 0;
} 

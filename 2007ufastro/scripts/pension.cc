const char rcsId[] = "$Name:  $ $Id: pension.cc 14 2008-06-11 01:49:45Z hon $";

#include "iostream.h"
#include "math.h"

int main(int argc, char** argv) {
  double inf= 1.03, raise= inf, orp= 0.15, spr= 1.07, tot=0.0, sal0= 22000.0, salf= sal0;
  double fp= 1.6, fptot=0.0;
  double yrs = 30;
  if( argc > 1 ) {
    if( argv[1][1] == 'h' ) {
      cout<<"usage: pension [years] [annual contrib. %] [raise \%] [stock yeild \%] [start salary]"<<endl;
      exit(0);
    }
  }
  if( argc > 1 ) yrs = atof(argv[1]);
  if( argc > 2 ) orp = atof(argv[2]) / 100.0;
  if( argc > 3 ) { raise = atof(argv[3]); raise = 1.0 + raise/100.0; }
  if( argc > 4 ) { spr = atof(argv[4]); spr = 1.0 + spr/100.0; }
  if( argc > 5 ) { sal0 = atof(argv[5]); salf = sal0; }
  cout<<"pension> years: "<<yrs<<", annual contrib.: "<<orp<<", raise: "<<raise<<", stock yeild: "<<spr<<", start salary: "<<sal0<<endl;
  for( int i = 0; i < yrs; ++i ) {
    fptot += fp;
    if( i == 0 ) 
      tot = orp * sal0;
    else
      // tot += orp * spr * (salf + tot) ;
      tot = orp * salf + spr*tot;
    salf = raise*salf;
    cout<<i<<".) orp total networth: "<<tot<<endl;
  }
  double incorp = 0.05*tot;
  /* since this is applied to an average salary over the final 5 yrs, approx: -2 yrs. */
  salf = salf/inf/inf;
  double pension = fptot/100.0 * salf;

  cout<<"start sal.: "<<sal0<<", final sal.: "<<salf<<", annual income from florida pension: "<<pension<<endl;
  cout<<"orp final networth: "<<tot<<", 5% annual income from networth: "<<incorp<<endl;
}

#!/usr/local/bin/perl
#
$rcsId = '$Name:  $ $Id: Flam2EnvStatEpics.cgi,v 0.1 2004/04/23 19:59:49 hon Exp $';
# supplemental perl module library directory:
use lib "/share/local/uf/perl";
use EZCA;
use CGI;
use CGI::Carp qw(fatalsToBrowser);

#package flam2;

$cgi = new CGI;
$organization = "University of Florida";
$department = "Department of Astronomy";
$refresh = 30;
$doctitle = "UF Flamingos-2 Environment Status Via Epics Database";

print $cgi->header( -type => "text/html", -expires => "now", -refresh => "$refresh" );
print $cgi->start_html( -title=> "$doctitle", -bgcolor=> "#ffffff" );
($s,$m,$h,$md,$mo,$y,$wd,$yd,$isd) = localtime(time());
$y = $y + 1900; $yd = $yd + 1;
if( $s < 10 ) { $s = "0$s"; }
if( $m < 10 ) { $m = "0$m"; }
if( $h < 10 ) { $h = "0$h"; }
$output = "$y:$yd:$h:$m:$s $doctitle ($refresh sec. Updates):";
print $cgi->p("$output");

$separate = "==================================================================";

epicstat("flam");

print $cgi->p($separate);

print $cgi->p("$organization $department, Author: ", $cgi->a({-href=>'mailto:hon@astro.ufl.edu'}, 'hon@astro.ufl.edu'));
print $cgi->p($rcsId);

$cgi->end_html();

########################################## sub epicstat() ###########################3
sub epicstat {
$instrum = shift;

$Idle = 0;
$Paused = 1;
$Busy = 2;
$Error = 3;

$flam2a = "192.168.111.222";
$ENV{'EPICS_CA_ADDR_LIST'} = $flam2a;
#EZCA::AutoErrorMessageOff();
EZCA::SetTimeout(0.01);
EZCA::SetRetryCount(3);
#$timeout = EZCA::GetTimeout();
#$retry = EZCA::GetRetryCount();
#print $cgi->p("Timeout= $timeout, Retry= $retry \n");
#$epicschan = "$instrum:sad:obsID";
#@obsID = EZCA::Get($epicschan, "ezcaLong", 1);
#print $cgi->p("Got $epicschan status= $obsID[0], val = $obsID[1]");
#print $cgi->p("$epicschan = $obsID[1]");

$epicschan = "$instrum:observeR";
@obsR = EZCA::Get($epicschan, "ezcaLong", 1);
#print $cgi->p("Got $epicschan status= $obsR[0], val = $obsR[1]");
if( $obsR[1] eq $Idle ) { $describe = "(Idle)"; }
if( $obsR[1] eq $Paused ) { $describe = "(Paused)"; }
if( $obsR[1] eq $Busy ) { $describe = "(Busy)"; }
if( $obsR[1] eq $Error ) { $describe = "(Error)"; }
$output = "$epicschan = $obsR[1] $describe, ";

$epicschan = "$instrum:ec:heartbeat";
@heart = EZCA::Get($epicschan, "ezcaLong", 1);
#print $cgi->p("Got $epicschan status= $heart[0], val= $heart[1]");
$output = $output . "$epicschan = $heart[1], ";

$epicschan = "$instrum:heartbeat";
@heart = EZCA::Get($epicschan, "ezcaLong", 1);
#print $cgi->p("Got $epicschan status= $heart[0], val= $heart[1]");
$output = $output . "($epicschan = $heart[1])";
print $cgi->p($output);

print $cgi->p($separate);

$epicschan = "$instrum:sad:MOSVacuum";
@vac = EZCA::Get($epicschan, "ezcaDouble", 1);
#print $cgi->p("Got $epicschan status= $vac[0], val= $vac[1]");
$output = "$epicschan = $vac[1], ";

$epicschan = "$instrum:sad:CamVacuum";
@vac = EZCA::Get($epicschan, "ezcaDouble", 1);
#print $cgi->p("Got $epicschan status= $vac[0], val= $vac[1]");
$output = $output . "$epicschan = $vac[1]";
print $cgi->p($output);

print $cgi->p($separate);

$epicschan = "$instrum:sad:LS332Kelvin1";
@kelvin = EZCA::Get($epicschan, "ezcaDouble", 1);
#print $cgi->p("Got $epicschan status= $kelvin[0], val= $kelvin[1]");
$output = "$epicschan = $kelvin[1], ";

$epicschan = "$instrum:sad:LS332Kelvin2";
@kelvin = EZCA::Get($epicschan, "ezcaDouble", 1);
#print $cgi->p("Got $epicschan status= $kelvin[0], val= $kelvin[1]");
$output = $output . "$epicschan = $kelvin[1]";
print $cgi->p($output);

# need sad records for currentr set points and pids,
# also need ec:datum.outK,outPIDs and ec:setup.outK, outPIDs, etc.
$epicschan = "$instrum:ec:setup.kelvin1";
@kelvin = EZCA::Get($epicschan, "ezcaDouble", 1);
#print $cgi->p("Got $epicschan status= $kelvin[0], val= $kelvin[1]");
$output = "$epicschan = $kelvin[1], ";

$epicschan = "$instrum:ec:setup.kelvin2";
@kelvin = EZCA::Get($epicschan, "ezcaDouble", 1);
#print $cgi->p("Got $epicschan status= $kelvin[0], val= $kelvin[1]");
$output = $output . "$epicschan = $kelvin[1]";
print $cgi->p($output);

print $cgi->p($separate);

$epicschan = "$instrum:sad:LS218Kelvin1";
@kelvin = EZCA::Get($epicschan, "ezcaDouble", 1);
#print $cgi->p("Got $epicschan status= $kelvin[0], val= $kelvin[1]");
$output = "$epicschan = $kelvin[1], ";

$epicschan = "$instrum:sad:LS218Kelvin2";
@kelvin = EZCA::Get($epicschan, "ezcaDouble", 1);
#print $cgi->p("Got $epicschan status= $kelvin[0], val= $kelvin[1]");
$output = $output . "$epicschan = $kelvin[1]";
print $cgi->p($output);

$epicschan = "$instrum:sad:LS218Kelvin3";
@kelvin = EZCA::Get($epicschan, "ezcaDouble", 1);
#print $cgi->p("Got $epicschan status= $kelvin[0], val= $kelvin[1]");
$output = "$epicschan = $kelvin[1], ";

$epicschan = "$instrum:sad:LS218Kelvin4";
@kelvin = EZCA::Get($epicschan, "ezcaDouble", 1);
#print $cgi->p("Got $epicschan status= $kelvin[0], val= $kelvin[1]");
$output = $output . "$epicschan = $kelvin[1]";
print $cgi->p($output);

$epicschan = "$instrum:sad:LS218Kelvin5";
@kelvin = EZCA::Get($epicschan, "ezcaDouble", 1);
#print $cgi->p("Got $epicschan status= $kelvin[0], val= $kelvin[1]");
$output = "$epicschan = $kelvin[1], ";

$epicschan = "$instrum:sad:LS218Kelvin6";
@kelvin = EZCA::Get($epicschan, "ezcaDouble", 1);
#print $cgi->p("Got $epicschan status= $kelvin[0], val= $kelvin[1]");
$output = $output . "$epicschan = $kelvin[1]";
print $cgi->p($output);

$epicschan = "$instrum:sad:LS218Kelvin7";
@kelvin = EZCA::Get($epicschan, "ezcaDouble", 1);
#print $cgi->p("Got $epicschan status= $kelvin[0], val= $kelvin[1]");
$output = "$epicschan = $kelvin[1], ";

$epicschan = "$instrum:sad:LS218Kelvin8";
@kelvin = EZCA::Get($epicschan, "ezcaDouble", 1);
#print $cgi->p("Got $epicschan status= $kelvin[0], val= $kelvin[1]");
$output = $output . "$epicschan = $kelvin[1]";
print $cgi->p($output);


#$epicschan = "$instrum:cc:heartbeat";
#@heart = EZCA::Get($epicschan, "ezcaLong", 1);
#print $cgi->p("Got $epicschan status= $heart[0], val= $heart[1]");
#print $cgi->p("$epicschan = $heart[1]");

#$epicschan = "$instrum:dc:heartbeat";
#@heart = EZCA::Get($epicschan, "ezcaLong", 1);
#print $cgi->p("Got $epicschan status= $heart[0], val= $heart[1]");
#print $cgi->p("$epicschan = $heart[1]");

#$val = EZCA::SetMonitor($epicschan, "ezcaInt");
#$val1 = EZCA::NewMonitorValue($epicschan, "ezcaInt");
#print $cgi->p("Set = $val New = $val1 ");
#EZCA::Delay(0.2);
#@values1 = EZCA::Get($epicschan, "ezcaInt", 1);
#print $cgi->p("$epicschan values[1,2]: $values1[0], $values1[1]");
#
#for (0..10) {
#  EZCA::Delay(0.2);
#  $status =EZCA::NewMonitorValue($epicschan, "ezcaInt"); 
#  print $cgi->p("$epicschan monitor returns = $status ");
#  if ($status) {
#    @values = EZCA::Get($epicschan, "ezcaInt", 1);
#    print $cgi->p("$epicschan value changed to $values[1]");
#  }
#}

}


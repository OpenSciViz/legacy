#!/usr/local/bin/perl
use IO::Socket;
use strict;

my $organization = "University of Florida";
my $department = "Department of Astronomy";
my $buffer;

# Read in the arguments
read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});

my $pair;
my $name;
my $value;
my %FORM;
#my @search_tags;
my @pairs = split(/&/, $buffer);
foreach $pair (@pairs) {
  ($name, $value) = split(/=/, $pair);
       
  # Un-Webify plus signs and %-encoding
  $value =~ tr/+/ /;
  $value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
#  push(@search_tags, $value), next if ($name eq 'search_tags');
  $FORM{$name} = $value;
} 

my $command = $FORM{'command'};
my $query = $FORM{'query'};
my $telescope = $FORM{'telescope'};

#if( $command eq "" && $query eq "" && $action eq "" ) {
#  &empty_query;
#  exit 1;
#}

if( $command eq "" ) {
  if( $query ne "" ) {
    $command = $query;
  }
  elsif( $telescope ne "" ) {
    $command = $telescope;
  }
  else {
    $command = "telpos"; 
  }
}

# my $passwd = $FORM{'passwd'};
my $passwd = "gatir";

if( $passwd ne "gatir" ) {
  &html_header("MMT Reply for $command:" );
  print "Sorry, bad password, request denied for: $command";
  &html_trailer;
  exit 2;
}

#my $output = "Sorry, no MMT capability yet...";
my $output = mmt_data($command);
$output =~ s/\r//g;
$output =~ s/\n/<br>/g;

&html_header("MMT Reply for <q>$command</q>:" );

print <<EOF;
<P>\n$output</P>
EOF

&html_trailer;

# This subroutine takes the document title as a command
# line parameter and adds header information to the top
# of the HTML document to be returned.
sub html_header {
  my $document_title = $_[0];
  print "Content-type: text/html\n\n";
  print "<html>\n";
  print "<Head>\n";
  print "<Title>$document_title</Title>\n";
  print "</Head>\n";
  print '<Body BGColor="#dddddd">';
  print "<H3><u>$document_title</H3></u>\n";
}
 
# This subroutine prints a suitable HTML trailer
sub html_trailer {
  print "<P>$organization, $department<\P>\n";
  print "</body>\n</html>\n";
  exit;
}

sub empty_query {
  &html_header( "No Arguments Given..." ) ;
  &html_trailer;
}

#------------ mmt client socket cmd  -----------------------------------------------------

sub mmt_data {
  my $mmtport = 5231;
  my $mmthost = "mmtvme1";
  my $sock = IO::Socket::INET->new (
			PeerAddr => $mmthost,
			PeerPort => $mmtport,
			Proto => 'tcp'
		    );
  my ( $cmd ) = @_;
  my $reply = "";

#  die "Unable to connect to $mmthost on port $mmtport (Reason: $!)\n" unless $sock;

  if( $sock ) {
    print $sock "$cmd\n";
    while ( defined($_ = <$sock>) ) { 
      last if /^.EOF/;
      $reply = $reply.$_;    
#      $reply = join $reply, $_;    
    }
    close ($sock);
  }
  else {
    $reply = "Unable to connect to $mmthost on port $mmtport";
  }
  return $reply;
}

#!/usr/local/bin/perl -w
use strict;
#use CGI;
#use CGI::Carp qw( fatalsToBrowser );
#my $q = new CGI;

my $organization = "University of Florida";
my $department = "Department of Astronomy";
my $ufmotor = "env LD_LIBRARY_PATH=/usr/local/lib:/opt/EDTpdv /usr/local/uf2001sgi/sbin/ufmotor -port 52004 -raw ";
my $reply = "";
my $flam = "";
my $buffer = "";

# Read in the arguments
read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
#print $ENV{'CONTENT_LENGTH'};
#print $buffer;

my $pair;
my $name;
my $value;
my %FORM;
my @pairs = split(/&/, $buffer);
foreach $pair (@pairs) {
  ($name, $value) = split(/=/, $pair);
  # Un-Webify plus signs and %-encoding
  $value =~ tr/+/ /;
  $value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
  $FORM{$name} = $value;
} 

#print %FORM;

my $command = " ";
$command = $FORM{'Command'};

my $status = " ";
$status = $FORM{'Status'};

my $imgOrdark = " ";
$imgOrdark = $FORM{'ImageDark'};

my $mos = "";
$mos = $FORM{'MOSFocalPlane'};

my $slit = " ";
$slit = $FORM{'SlitFocalPlane'};

my $filter = " ";
$filter = $FORM{'FilterWheel'};

my $lyot = " ";
$lyot = $FORM{'LyotWheel'};

my $grism = " ";
$grism = $FORM{'GrismWheel'};

if( $command eq "" && $status eq "" && $imgOrdark eq "" && $mos eq "" 
    && $slit eq "" && $filter eq "" && $lyot eq "" && $grism eq "" ) {
  &empty_req;
  exit 1;
}

my $passwd = $FORM{'passwd'};
if( $passwd ne "gatir" ) {
  &html_header("Reply:" );
  print "Sorry, bad password, request denied for: $command";
  &html_trailer;
  exit 2;
}

my $output = "Sorry, ambiguous request, please reset & try again.";
if( $command ne "" ) {
  $output = raw_cmd( $command );
}

if( $status ne "" ) {
  $output = query_status( $status );
}

if( $imgOrdark ne "" ) {
  $output = config( $imgOrdark );
}

if( $mos ne "" ) {
  $output = mos_select( $mos );
}

if( $slit ne "" ) {
  $output = slit_select( $slit );
}

if( $filter ne "" ) {
  $output = filter_select( $filter );
}

if( $lyot ne "" ) {
  $output = lyot_select( $lyot );
}

if( $grism ne "" ) {
  $output = grism_select( $grism );
}

$output =~ s/\r//g;
$output =~ s/\n/<br>/g;

&html_header("UF Flamingos Reply:" );

print <<EOF;
<p> $output </p>
EOF

&html_trailer;

# This subroutine takes the document title as a command
# line parameter and adds header information to the top
# of the HTML document to be returned.
sub html_header {
  my $document_title = $_[0];
  print "Content-type: text/html\n\n";
  print "<HTML>\n";
  print "<HEAD>\n";
  print "<TITLE>$document_title</TITLE>\n";
  print "</HEAD>\n";
  print '<Body BGColor="#ffffff">';
  print "<H3><u>$document_title</H3></u>\n";
}
 
# This subroutine prints a suitable HTML trailer
sub html_trailer {
  print "<P>\n";
  print "$organization<br>\n";
  print "$department<P>\n";
  print "</body>\n</html>\n";
  exit;
}

sub empty_req {
  &html_header( "No Arguments Given" ) ;
  &html_trailer;
}

###################### flamingos functions ########################

sub raw_cmd {
  my ( $cmd ) = @_;

  $reply = $cmd."\n";
  $flam = `$ufmotor '"$cmd"'`;
  $reply = $reply.$flam;

  return $reply;
}

sub query_status { 
# "all" or "Astatus" or "Bstatus" or "Cstatus" or "Dstatus "or "Estatus"
  my ( $query ) = @_;
  my $m = "";

  $reply = $query." ".$m."\n";

  if( $query eq "Astatus" ) {
    $m = "a";
  }
  if( $query eq "Bstatus" ) {
    $m = "b";
  }
  if( $query eq "Cstatus" ) {
    $m = "c";
  }
  if( $query eq "Dstatus" ) {
    $m = "d";
  }
  if( $query eq "Estatus" ) {
    $m = "e";
  }
  if( $m ne "" ) {
    $flam = `$ufmotor '"$m [ 1990 23"'`;
    $reply = $reply.$flam;
  }
  else {
    $flam = `$ufmotor '"a [ 1990 23"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"b [ 1990 23"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"c [ 1990 23"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"d [ 1990 23"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"e [ 1990 23"'`;
    $reply = $reply.$flam;
  }
  return $reply;    
}

sub config {
# "image" or "dark"
  my ( $imgOrdark ) = @_;
  $reply = "Unknown Configuration selection: $imgOrdark";
  if( $imgOrdark eq "image" ) {
    $reply= "";
    $flam = `$ufmotor '"aF/100"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"bF/100"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"eF/100"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"a+2250"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"e+250"'`;
    $reply = $reply.$flam;
  }
  elsif( $imgOrdark eq "dark" ) {
    $reply= "";
    $flam = `$ufmotor '"aF/100"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"cF/100"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"dF/100"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"eF/100"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"c+1040"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"e+500"'`;
    $reply = $reply.$flam;
  }
  return $reply;
}

sub mos_select {
# MOS 1 == "1540", 2 == "2440", 3 == "3320", 4 == "4220", 5 == "5110", 6 == "6000",
# MOS 7 == "6890", 8 == "7781", 9 == "8671", 10 == "9562", 11 == "10452"
  my ( $mos ) = @_;
  $reply = "Unknown MOS selection: $mos";
  if( $mos eq "1540" || $mos eq "2440" || $mos eq "3320" || $mos eq "4220" ||
      $mos eq "5110" || $mos eq "6000" || $mos eq "6890" || $mos eq "7781" ||
      $mos eq "8671" || $mos eq "9562" || $mos eq "10452" ) {
    $reply= "";
    $flam = `$ufmotor '"aF/100"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"bF/100"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"a+4500"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"b+$mos"'`;
    $reply = $reply.$flam;
  }

  return $reply;
}

sub slit_select {
# slit 2Pix == "1083", 3Pix == "10933", 1Pix == "10007", 4Pix == "9117",
# slit 6Pix == "8226", 12Pix == "7336"
  my ( $slit ) = @_;
  $reply = "Unknown Slit selection: $slit";
  if( $slit eq "1083" || $slit eq "10933" || $slit eq "10007" ||
      $slit eq "9117" || $slit eq "8226" || $slit eq "7336 " ) {
    $reply= "";
    $flam = `$ufmotor '"aF/100"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"bF/100"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"a+6750"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"b+$slit"'`;
    $reply = $reply.$flam;
  }
  return $reply;
}


sub filter_select {
# filt J == "0", H == "208", K == "416", HK == "624", JH == "832", Dark == "1040"
  my ( $filt ) = @_;
  $reply = "Unknown Fliter selection: $mos";
  if( $filt eq "0" || $filt eq "208" || $filt eq "416" ||
      $filt eq "624" || $filt eq "832" || $filt eq "1040 " ) {
    $reply= "";
    $flam = `$ufmotor '"cF/100"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"c+$filt"'`;
    $reply = $reply.$flam;
  }

  return $reply;
}

sub lyot_select {
# lyot Dark == "0", 2.1m == "178", 4m == "358", MMT == "537",
# lyot Gemini == "716", Dark2 == "895", Dark3 == "1074"
  my ( $lyot ) = @_;
  $reply = "Unknown Lyot selection: $lyot";
  if( $lyot eq "0" || $lyot eq "178" || $lyot eq "358" ||
      $lyot eq "537" || $lyot eq "716"  || $lyot eq "895" || $lyot eq "1074 " ) {
    $reply= "";
    $flam = `$ufmotor '"dF/100"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"d+$lyot"'`;
    $reply = $reply.$flam;
  }

  return $reply;
}

sub grism_select {
# grism HK == "0", Open == "250", Dark == "500", Dark1 == "750", Dark2 == "1000"
  my ( $grism ) = @_;
  $reply = "Unknown Grism selection: $lyot";
  if( $grism eq "0" || $grism eq "250" || $grism eq "500" ||
      $grism eq "750" || $grism eq "1000" ) {
    $reply= "";
    $flam = `$ufmotor '"eF/100"'`;
    $reply = $reply.$flam;
    $flam = `$ufmotor '"e+$grism"'`;
    $reply = $reply.$flam;
  }

  return $reply;
}

#!/usr/local/bin/perl
use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
package ufmcehdwr;

$ufmcehdwr::rcsId = q($Name:  $ $Id: uftrecsMCE4HdwrForm.pl 14 2008-06-11 01:49:45Z hon $);
$ufmcehdwr::cgi = new CGI;
$ufmcehdwr::organization = "University of Florida";
$ufmcehdwr::department = "Department of Astronomy";
$ufmcehdwr::refresh = 10;
$ufmcehdwr::doctitle = "TReCS Cryostat Environment";

print $ufmcehdwr::cgi->header( -type => "text/html", -expires => "now");
print $ufmcehdwr::cgi->start_html( -title=> "$ufmcehdwr::doctitle", -bgcolor=> "#ffffff" );
($ufmcehdwr::s,$ufmcehdwr::m,$ufmcehdwr::h,$ufmcehdwr::md,$ufmcehdwr::mo,$ufmcehdwr::y,$ufmcehdwr::wd,$ufmcehdwr::yd,$ufmcehdwr::isd) = localtime(time());
$ufmcehdwr::y = $ufmcehdwr::y + 1900; $ufmcehdwr::yd = $ufmcehdwr::yd + 1;
if( $ufmcehdwr::s < 10 ) { $ufmcehdwr::s = "0$ufmcehdwr::s"; }
if( $ufmcehdwr::m < 10 ) { $ufmcehdwr::m = "0$ufmcehdwr::m"; }
if( $ufmcehdwr::h < 10 ) { $ufmcehdwr::h = "0$ufmcehdwr::h"; }
$ufmcehdwr::output = "$ufmcehdwr::y:$ufmcehdwr::yd:$ufmcehdwr::h:$ufmcehdwr::m:$ufmcehdwr::s TReCS Environment (10sec. Updates):\n";
print $ufmcehdwr::cgi->p("$ufmcehdwr::output");
$ufmcehdwr::output = "==========================================\n";
print $ufmcehdwr::cgi->p("$ufmcehdwr::output");
$ufmcehdwr::cgi->end_html();

#!/usr/local/bin/perl
use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
package ufmcephys;

$ufmcephys::rcsId = q($Name:  $ $Id: uftrecsMCE4PhysForm.pl 14 2008-06-11 01:49:45Z hon $);
$ufmcephys::cgi = new CGI;
$ufmcephys::organization = "University of Florida";
$ufmcephys::department = "Department of Astronomy";
$ufmcephys::refresh = 10;
$ufmcephys::doctitle = "TReCS Cryostat Environment";

print $ufmcephys::cgi->header( -type => "text/html", -expires => "now");
print $ufmcephys::cgi->start_html( -title=> "$ufmcephys::doctitle", -bgcolor=> "#ffffff" );
($ufmcephys::s,$ufmcephys::m,$ufmcephys::h,$ufmcephys::md,$ufmcephys::mo,$ufmcephys::y,$ufmcephys::wd,$ufmcephys::yd,$ufmcephys::isd) = localtime(time());
$ufmcephys::y = $ufmcephys::y + 1900; $ufmcephys::yd = $ufmcephys::yd + 1;
if( $ufmcephys::s < 10 ) { $ufmcephys::s = "0$ufmcephys::s"; }
if( $ufmcephys::m < 10 ) { $ufmcephys::m = "0$ufmcephys::m"; }
if( $ufmcephys::h < 10 ) { $ufmcephys::h = "0$ufmcephys::h"; }
$ufmcephys::output = "$ufmcephys::y:$ufmcephys::yd:$ufmcephys::h:$ufmcephys::m:$ufmcephys::s TReCS Environment (10sec. Updates):\n";
print $ufmcephys::cgi->p("$ufmcephys::output");
$ufmcephys::output = "==========================================\n";
print $ufmcephys::cgi->p("$ufmcephys::output");
$ufmcephys::cgi->end_html();

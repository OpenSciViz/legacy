#!/usr/local/bin/perl
use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
package ufenv;

$ufenv::rcsId = q($Name:  $ $Id: uftrecsEnvForm.pl 14 2008-06-11 01:49:45Z hon $);
$ufenv::cgi = new CGI;
$ufenv::organization = "University of Florida";
$ufenv::department = "Department of Astronomy";
$ufenv::refresh = 10;
$ufenv::doctitle = "TReCS Cryostat Environment";

print $ufenv::cgi->header( -type => "text/html", -expires => "now");
print $ufenv::cgi->start_html( -title=> "$ufenv::doctitle", -bgcolor=> "#ffffff" );
($ufenv::s,$ufenv::m,$ufenv::h,$ufenv::md,$ufenv::mo,$ufenv::y,$ufenv::wd,$ufenv::yd,$ufenv::isd) = localtime(time());
$ufenv::y = $ufenv::y + 1900; $ufenv::yd = $ufenv::yd + 1;
if( $ufenv::s < 10 ) { $ufenv::s = "0$ufenv::s"; }
if( $ufenv::m < 10 ) { $ufenv::m = "0$ufenv::m"; }
if( $ufenv::h < 10 ) { $ufenv::h = "0$ufenv::h"; }
$ufenv::output = "$ufenv::y:$ufenv::yd:$ufenv::h:$ufenv::m:$ufenv::s TReCS Environment (10sec. Updates):\n";
print $ufenv::cgi->p("$ufenv::output");
$ufenv::output = "==========================================\n";
print $ufenv::cgi->p("$ufenv::output");
$ufenv::cgi->end_html();

#!/usr/local/bin/perl
use strict;
use CGI;
use CGI::Carp qw(fatalsToBrowser);
package ufacq;

$ufacq::rcsId = q($Name:  $ $Id: uftrecsAcqForm.pl 14 2008-06-11 01:49:45Z hon $);
$ufacq::cgi = new CGI;
$ufacq::organization = "University of Florida";
$ufacq::department = "Department of Astronomy";
$ufacq::refresh = 10;
$ufacq::doctitle = "TReCS Cryostat Environment";

print $ufacq::cgi->header( -type => "text/html", -expires => "now");
print $ufacq::cgi->start_html( -title=> "$ufacq::doctitle", -bgcolor=> "#ffffff" );
($ufacq::s,$ufacq::m,$ufacq::h,$ufacq::md,$ufacq::mo,$ufacq::y,$ufacq::wd,$ufacq::yd,$ufacq::isd) = localtime(time());
$ufacq::y = $ufacq::y + 1900; $ufacq::yd = $ufacq::yd + 1;
if( $ufacq::s < 10 ) { $ufacq::s = "0$ufacq::s"; }
if( $ufacq::m < 10 ) { $ufacq::m = "0$ufacq::m"; }
if( $ufacq::h < 10 ) { $ufacq::h = "0$ufacq::h"; }
$ufacq::output = "$ufacq::y:$ufacq::yd:$ufacq::h:$ufacq::m:$ufacq::s TReCS Environment (10sec. Updates):\n";
print $ufacq::cgi->p("$ufacq::output");
$ufacq::output = "==========================================\n";
print $ufacq::cgi->p("$ufacq::output");
$ufacq::cgi->end_html();

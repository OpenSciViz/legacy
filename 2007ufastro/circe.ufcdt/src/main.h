#ifndef _xpharo_h
#define _xpharo_h

#include <pthread.h>
#include <gtk/gtk.h>

/* Define to include EDT calls in compile */
/* #define INCLUDE_EDT */

#ifndef MIN
#define MIN(a,b) ( (a) < (b) ? (a) : (b) )
#define MAX(a,b) ( (a) > (b) ? (a) : (b) )
#endif

#define ORIGINVAL   "Cornell-IR"    /* organization creating the data set */
#define TELESCOPVAL "Hale 200 inch"
#define INSTRUMEVAL "CIRCE"

#define NAMESTRLEN 128

#define MINCOLS 16
#define MAXCOLS 512
#define MINROWS 16
#define MAXROWS 512
#define MINENDPTFRAMES 1
#define MAXENDPTFRAMES 14
#define MINPAUSEFRAMES 0
#define MAXPAUSEFRAMES 4095
#define MINCYCLES 1
#define MAXCYCLES 256

#define SRCDATA 0
#define BGDDATA 1

#define STOPCOLOR 0
#define GOCOLOR 1
#define DEFAULTCOLOR 2

/* Posn of Open filter in Fore Wheel */
#define OPENFILT 2
/* Posn of autofocus reference filter (K') in Aft Wheel */
#define REFFILT 3

enum auto_focus {AF_CALIBRATE, AF_OFF, AF_ON};

enum data_mode {REALDATA, FAKEDATA};

enum int_mode {STDINT, QUICKINT, CONTACQ};

enum write_mode {
   WRITE_ALL,     /* Save all frames */
   WRITE_ENDS,    /* Save avg of start and end groups */
   WRITE_DIFF,    /* Save Difference between start and end groups */
   WRITE_NONE,    /* Don't automatically write anything */
   WRITE_SUB,     /* Write Sub Buffer */
};

typedef struct _instcontrol {
   int tryfake, trynet, remote;
   int debug, net_debug;
   int data_mode, int_mode, write_mode;
   int fpga_executing, macro_executing, remote_executing;
   int interval_active, auto_focus;
   int net_connected, net_warn;
   int send_image_to_ao;
   char remote_cmd[16];
} INST_CONTROL;

extern INST_CONTROL IC;

extern GdkPixmap *Inst_Pixmap[];

extern char *Raw_Buf[];
extern int  *Sort_Buf;
extern char Data_Dir[];
extern char Object_Name[];
extern char Observer_Name[];
extern char Macro_Name[];
extern float Pixelscale;
extern pthread_t FOI_Monitor_Thread;

/* events.c */
extern void foi_pipe(int);
extern gint intremtime(void);

/* fakedata.c */
extern int fakedata_init(void);
extern int fakedata_cancel(void);
extern void *fake_monitor(void *);

/* init.c */
extern void init_main(void);
extern void init_datadir(void);
extern void init_final(void);

/* callbacks.c */
extern void fpgabuttondown(GtkButton *);
extern void fpgabuttonup(void);
extern void button_press(GtkWidget *, char *, int);


/* functions.c */
#include "image.h"
extern void takedata(image *, int, int);

#endif

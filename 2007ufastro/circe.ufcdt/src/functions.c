/*------------------------------------------------------------*
 |
 | PROJECT:
 |	Canarias Infrared Camera Experiment (CIRCE)
 |      University of Florida, Gainesville, FL
 |
 | FILE: functions.c -- ufCDT high-level functions
 |         (motor moves, detector control, telescope control)
 |
 | DESCRIPTION:
 |	This source code file contains the high-level routines
 |      for motor moves, detector control, telescope control, etc.
 |      These routines are called either by callback, macro, or
 |      remote command routines.
 |
 | REVISION HISTORY:
 | Date           By                Description
 | 2005-April-4   A. Marin-Franch   Adapted from xwirc
 |
 *------------------------------------------------------------*/ 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>
#include <gtk/gtk.h>

#include "main.h"
#include "interface.h"
#include "support.h"
#include "support2.h"
#include "cmdcodes.h"
#include "fiber.h"
#include "files.h"
#include "fits.h"
#include "plot.h"

extern time_t StartIntTime;
extern gint IntervalID;
extern pthread_mutex_t VLock;

/***** Detector Control Routines *****/

/* Take data and save to FITS file */
/* im = pointer to either the SrcImage or BgdImage structures */
/* buffer  = SOURCE (0) or BACKGROUND (1) buffers */
/* intmode = STDINT (0) for standard or QUICKINT (1) for quick integrations */

void takedata(image *im, int buffer, int intmode)
{
   unsigned int arg1, arg2, arg3;
   int ncycles;

   /* Prevent corruption of LastCmd which is shared with foi_monitor(). */
   pthread_mutex_lock(&VLock);

   /* Set parameters of current image structure */
   setimageparams(im, &ArrayClock, intmode);

   LastImage = im;
   IC.int_mode = intmode;
   if (intmode == STDINT) ncycles = ArrayClock.ncycles;
   else ncycles=1;

   intremtime();

   /* Initialize FITS header */
   constructFITSheader(im);
   logimage(buffer + 2*intmode, ncycles);
   if (ncycles > 1) logprintf("Cycle  1");

   /* Construct command */
   arg1 =  (ncycles-1) << 24;
   arg1 += 0;                         /* 1/100 sec intervals between pause frames */
   arg2 =  (im->ncols/2 - 1) << 24;
   arg2 += (im->nrows/2 - 1) << 16;
   arg2 += (im->npauseframes%256) << 8;
   arg2 += (im->npauseframes/256) << 4;
   arg2 += (im->nendptframes);
   arg3 = 0;

   /* Send command */
   foi_cmd(TAKE_DATA, arg1, arg2, arg3);

   pthread_mutex_unlock(&VLock);

}

/*------------------------------------------------------------*
 |
 | PROJECT:
 |      PHARO / CIRCE
 |      Cornell University, Ithaca, NY
 |
 | FILE: subdisp.c -- Sub-display control routines.
 |
 | DESCRIPTION:
 |      This source code file contains the routines for
 |      displaying images on the sub-display, usually shown
 |      on the main monitor.
 |
 | REVISION HISTORY:
 |   Date        By         Description
 | 1999-Dec-11  TLH   Divided display.c into 3 separate files.
 |
 *------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <math.h>
#include <gtk/gtk.h>
/* #include <Dt/Wsm.h> */

#include "main.h"
#include "support.h"
#include "support2.h"
#include "fiber.h"
#include "fits.h"
#include "display.h"
#include "plot.h"

extern image FullIm, SubIm[];
extern image *FitsDest;

int Hist[NHIST];
extern GdkFont *HistFont;

/* Set GUI horizontal and vertical scrollbars */
void set_scrollbars (int xl, int xr, int yt, int yb)
{
   int hsize, vsize;

   GtkAdjustment *hadj = GTK_HSCROLLBAR(GUI.SubDisp_hscrollbar)->scrollbar.range.adjustment;
   GtkAdjustment *vadj = GTK_VSCROLLBAR(GUI.SubDisp_vscrollbar)->scrollbar.range.adjustment;

   hsize = (xr - xl + 1);           vsize = (yb - yt + 1);

   hadj->value = xl;                vadj->value = yt;
   hadj->lower = 0;                 vadj->lower = 0;
   hadj->upper = DET_NCOLS;         vadj->upper = DET_NROWS;
   hadj->step_increment = hsize/16; vadj->step_increment = vsize/16;
   hadj->page_increment = hsize/4;  vadj->page_increment = vsize/4;
   hadj->page_size = hsize;         vadj->page_size = vsize;

   gtk_adjustment_changed (hadj);
   gtk_adjustment_changed (vadj);

}


void update_scrollbars (void)
{
   if (DC.region_mode == WHOLE) set_scrollbars(0, DET_NCOLS-1, 0, DET_NROWS-1);
   else set_scrollbars(SubIm[DC.current_sub].startcol, 
                       SubIm[DC.current_sub].startcol+SubDisp.ncols-1,
                       DET_NCOLS-1-SubIm[DC.current_sub].startrow-SubDisp.nrows+1, 
                       DET_NROWS-1-SubIm[DC.current_sub].startrow);
}


void zero_subimage(void)
{
   char *dum;

   if (SubIm[DC.current_sub].data != NULL) 
      dum = (char*) memset((void*)SubIm[DC.current_sub].data, 0, 
                           SubIm[DC.current_sub].nbytes);
}

void clear_subdisp(void)
{
   gdk_window_clear(GUI.SubDisp_darea->window);
}


int format_subimage(void)
{
   int i, j, nbytes;
   int *fptr, *sptr, foffset;
   char *bptr, *ptr1, *ptr2, *dum;
   image *c = &SubIm[DC.current_sub];
   int nrows, ncols, dmin, dmax;
   char menulabel[3][16] = {"Buffer 1", "Buffer 2", "Buffer 3"};
   int *hptr = &Hist[0], hshift=NHIST/2;

   /* Return if FullIm has not been initialized */
   if (!FullIm.init) return(FALSE);

   /* Copy Header Data */
   ptr1 = (char *) (&c->nendptframes);
   ptr2 = (char *)(&FullIm.nendptframes);
   nbytes = (char *)(&FullIm.data) - ptr2;
   for (i=0; i<nbytes; i++) *ptr1++ = *ptr2++;

   /* Memory management */
   if (c->data != NULL && 
      (c->ncols != SubDisp.ncols || c->nrows != SubDisp.nrows)) {
          free(c->data);
          c->data = NULL;
   }
   if (c->data == NULL) {
      c->npixels = SubDisp.ncols * SubDisp.nrows;
      c->nbytes = c->npixels * sizeof(int);
      c->data = (int *) malloc(c->nbytes);
      if (c->data == NULL) {
         fprintf(stderr, "formatsubimage Error: cannot allocate memory\n");
         c->ncols = c->nrows = 0;
         return(FALSE);
      }
      c->ncols = SubDisp.ncols;
      c->nrows = SubDisp.nrows;
   }

   /* Initialize Pointers */
   fptr = FullIm.data + c->startrow*FullIm.ncols + c->startcol;
   bptr = &BPM[0] + c->startrow*FullIm.ncols + c->startcol;
   sptr = c->data;

   /* Zero histogram */
   dum = (char*) memset((void *)hptr, 0, NHIST*sizeof(int));

   /* Construct data array to display */
   nrows = c->nrows;
   ncols = c->ncols;

   if (DC.region_mode == WHOLE) {            /* Display whole detector */
      foffset = QUAD_NCOLS*2;
      for (i=0; i<nrows; i++) {
         for (j=0; j<ncols; j++) {
            *sptr = *fptr;
            if (*bptr) (*(hptr+*sptr+hshift))++;
            sptr++;
            fptr += 2;
            bptr += 2;
         }
         fptr += foffset;
         bptr += foffset;
      }
   } else {                             /* Display 1 quadrant or ZoomBox */
      foffset = FullIm.ncols - c->ncols;
      for (i=0; i<nrows; i++) {
         for (j=0; j<ncols; j++) {
            *sptr = *fptr++;
            if(*bptr) (*(hptr+*sptr+hshift))++;
            sptr++;
            bptr++;
         }
         fptr += foffset;
         bptr += foffset;
      }
   } 

   hist_minmax(hptr, NHIST, &dmin, &dmax);
   c->datamax = dmax-hshift;
   c->datamin = dmin-hshift;

   hist_bottop(hptr, NHIST, 0.01, &dmin, &dmax);
   c->datatop = dmax-hshift;
   c->databot = dmin-hshift;

   /* Label SubBuffer menu */
   strcat(menulabel[DC.current_sub], "  *");
/*   XtVaSetValues(SubBuf1_tog, XmNlabelString, 
      XmStringCreateLocalized(menulabel[0]), NULL); 
   XtVaSetValues(SubBuf2_tog, XmNlabelString, 
      XmStringCreateLocalized(menulabel[1]), NULL); 
   XtVaSetValues(SubBuf3_tog, XmNlabelString, 
      XmStringCreateLocalized(menulabel[2]), NULL);
*/

   return(TRUE);
}


int scale_subimage(void)
{
   int i, j, ii, jj;
   int offset1, offset2, colorval=0;
   int maxcolor=MaxColor[0], mincolor=MinColor[0];
   int histmax=CDisp.hist_height-1, histindex;
   float cscale, offset, hscale;

   int *iptr;
   guchar *cptr, *ccptr;
   
   image *sim = &SubIm[DC.current_sub];
   displaytype *sd = &SubDisp;
   int zoom = sd->zoom;

   /* Check that the data pointer has been initialized */
   if (sim->data == NULL) return(FALSE);
   
   if (sd->stretchmode == IMAGE || sd->dispmin >= sd->dispmax) {
      switch (sd->stretchhi) {
         case 0: sd->dispmax = sim->datamax; break;
         case 1: sd->dispmax = sim->datatop; break;
      }
      switch (sd->stretchlo) {
         case 0: sd->dispmin = sim->databot; break;
         case 1: sd->dispmin = sim->datamin; break;
      }
   }
   else if (sd->stretchmode == BOX) {     /* Photometry box */
      switch (sd->stretchhi) {
         case 0: sd->dispmax = PhotMax; break;
         case 1: sd->dispmax = PhotTop; break;
      }
      switch (sd->stretchlo) {
         case 0: sd->dispmin = PhotBot; break;
         case 1: sd->dispmin = PhotMin; break;
      }
   }

   sd->dispmax = MIN(sd->dispmax, 65535);
   if (sd->dispmin >= sd->dispmax) {
      if (sd->dispmax == 65535) sd->dispmin = sd->dispmax - 1;
      else sd->dispmax = sd->dispmin + 1;
   }


   /* Initialize histogram */
   for (i=0; i<CDisp.hist_height; i++) HistogramData[i] = 0;

   /* Set destination char array pointers */
   /* 0, 0 = Upper left corner of window */
   cptr = sd->buf + sd->width*(sd->height - 1 - sd->yborder) + sd->xborder;
   iptr = sim->data;

   offset1 = (zoom + 1) * sd->width - 2 * sd->xborder;

   cscale = (float)maxcolor / (sd->dispmax - sd->dispmin);
   hscale = (float)histmax  / (sd->dispmax - sd->dispmin);
   offset = -sd->dispmin;
   
   if (zoom == 1) {
      for (i=0; i<sim->nrows; i++) {
        for (j=0; j<sim->ncols; j++) {
           histindex = (int) ((*iptr + offset) * hscale + 0.5);
           histindex = MIN(histmax, MAX(0, histindex));
           HistogramData[histindex]++;

           colorval = (int) ((*iptr++ + offset) * cscale + 0.5);
           *cptr++ = MIN(maxcolor, MAX(mincolor, colorval));
        }
        cptr -= offset1;
      }
   } else {
      offset2 = sd->width + zoom;
      for (i=0; i<sim->nrows; i++) {
         for (j=0; j<sim->ncols; j++) {
           histindex = (int) ((*iptr + offset) * hscale + 0.5);
           histindex = MIN(histmax, MAX(0, histindex));
           HistogramData[histindex]++;
           
           colorval = (int) ((*iptr++ + offset) * cscale + 0.5);
           colorval = MIN(maxcolor, MAX(mincolor, colorval));
           
           ccptr = cptr;
           for (ii=0; ii<zoom; ii++) {
 	      for (jj=0; jj<zoom; jj++) *ccptr++ = colorval;
              ccptr -= offset2;
           }
           cptr += zoom;
        }
        cptr -= offset1;
      }
   }
   
   return(TRUE);
}

void draw_subimage(void)
{
   gdk_draw_indexed_image(GUI.SubDisp_darea->window, 
      GUI.SubDisp_darea->style->fg_gc[GTK_STATE_NORMAL],
      0, 0, SubDisp.width, SubDisp.height, GDK_RGB_DITHER_NORMAL, SubDisp.buf,
      SubDisp.width, CMap);
}

void bplabel_subimage(void)
{
   char *bptr;
   int i, j, *sptr, scrx0, scrx1, scry0, scry1;
   int badinc;
   GdkWindow *win = GUI.SubDisp_darea->window;
   image *cs = &SubIm[DC.current_sub];

   gdk_gc_set_line_attributes (GC_SD, 1, GDK_LINE_SOLID, GDK_CAP_ROUND, GDK_JOIN_ROUND);

   bptr = &BPM[0] + cs->startcol + cs->startrow*DET_NCOLS;
   sptr = cs->data;
   if (DC.region_mode == WHOLE) badinc = 2;    /* Whole array */
   else badinc = 1;

   for (i=0; i<cs->nrows; i++) {
     for (j=0; j<cs->ncols; j++) {
       if (!*bptr) {
          scrx0 = j * SubDisp.zoom;
          scry0 = SubDisp.height - i * SubDisp.zoom;
          scrx1 = scrx0 + SubDisp.zoom - 1;
          scry1 = scry0 - SubDisp.zoom + 1;
	  gdk_draw_line(win, GC_SD, scrx0, scry0, scrx1, scry1);
       }
       bptr += badinc;
       sptr++;
     }
     bptr += badinc * (DET_NCOLS - cs->ncols);
   }
}


void display_subimage(void)
{
   
   if (format_subimage() == TRUE) {
      subdisp_cursor();
      subdisp_photometry();
      scale_subimage();
      draw_subimage();

      if (SubDisp.bplabel) bplabel_subimage();
      box_checksubdisp(&PhotBox, SubDisp.zoom);
      draw_box(&SubDisp, GC_SD, &PhotBox);
      
      draw_plotline(&SubDisp, GC_SD, &PlotLine);
      subdisp_plot();
      
      stuff_histogram();
      draw_histogram();
      draw_histogramlabels();
      draw_stretchlimitline(&StretchULim);
      draw_stretchlimitline(&StretchLLim);

   }
} 

/* Redisplay subimage and colorbar/histogram after a change in scaling. */
void refresh_subimage(void)
{
   if (scale_subimage() == TRUE) {
      draw_subimage();
      if (SubDisp.bplabel) bplabel_subimage();
      draw_box(&SubDisp, GC_SD, &PhotBox);
      if (DC.plot_mode) draw_plotline(&SubDisp, GC_SD, &PlotLine);

      clear_stretchlimitline(&StretchULim);
      clear_stretchlimitline(&StretchLLim);
      draw_histogram();
      draw_histogramlabels();
      draw_stretchlimitline(&StretchULim);
      draw_stretchlimitline(&StretchLLim);
   }
}


void toggle_badpixel(int scrx, int scry)
{
   char *bptr;
   int imx, imy;

   if (DC.region_mode == WHOLE) return;      /* Whole image too nasty to handle */

   imx = SubIm[DC.current_sub].startcol + scrx/SubDisp.zoom;
   imy = SubIm[DC.current_sub].startrow + (SubDisp.height - 1 - scry) / SubDisp.zoom;
   bptr = &BPM[0] + DET_NCOLS*imy + imx;

   *bptr = !*bptr;
}


/*** Stuff the Colorbar into the CBarBuf buffer ***/

void stuff_cbar()
{
   int j, ncolors, colorval;
   int inc = CDisp.width - CDisp.bar_width;
   float i, wid, hgt;   
   guchar *cptr;
   
   cptr = CDisp.buf + CDisp.width*CDisp.bar_y + CDisp.bar_x;
   wid = CDisp.bar_width;
   hgt = CDisp.bar_height;
   ncolors = NColors[0];
   
   for (i=0; i<hgt; i++) {
      colorval = (char)(ncolors - 1 - (int)(ncolors*i/hgt));
      for (j=0; j<wid; j++) *cptr++ = colorval;
      cptr += inc;
   }
}

#define MAXTICKS 64

/* Stuff the Histogram (no labels) into the CBarBuf buffer */

void stuff_histogram(void)
{
   int i, j, jmax;
   int imax, wid, histmax, hicolor, locolor;
   guchar *hptr;
   
   hptr = CDisp.buf + CDisp.width*CDisp.hist_y + CDisp.hist_x;
   wid = CDisp.hist_width;
   imax = CDisp.hist_height-1; 
   
   hicolor = CMap->colors[NColors[0] - 1];
   locolor = CMap->colors[0];

   histmax = MAX(1, HistogramData[0]);
   for (i=1; i<=imax; i++)
      if (HistogramData[i] > histmax) histmax = HistogramData[i];
      
   for (i=0; i<=imax; i++) {
      jmax = wid * HistogramData[imax-i] / histmax;
      for (j=0; j<jmax; j++) *hptr++ = hicolor;
      for (j=jmax; j<wid; j++) *hptr++ = locolor;
      hptr += CDisp.width - wid;
   }
}


/* Draw the entire CBarBuf in the colorbar drawing area.  This will draw the actual
*  colorbar and the histogram and blacken all background areas.  The histogram labels
*  are not part of the buffer and will not be drawn. */

void draw_cbarbuf(void)
{
   GtkWidget *wid = CDisp.wid;

   gdk_draw_indexed_image(wid->window, wid->style->fg_gc[GTK_STATE_NORMAL],
      0, 0, CDisp.width, CDisp.height, GDK_RGB_DITHER_NORMAL,
      CDisp.buf, CDisp.width, CMap);
}


/* Draw the colorbar only in the colorbar window */
void draw_colorbar(void)
{
   GtkWidget *wid = CDisp.wid;

   gdk_draw_indexed_image(wid->window, wid->style->fg_gc[GTK_STATE_NORMAL],
      CDisp.bar_x, CDisp.bar_y, CDisp.bar_width, CDisp.bar_height,
      GDK_RGB_DITHER_NORMAL,
      CDisp.buf+CDisp.bar_y*CDisp.width+CDisp.bar_x,
      CDisp.width, CMap);
}

/* Draw the histogram only (not incl. labels) in the colorbar window */
void draw_histogram(void)
{
   GtkWidget *wid = CDisp.wid;

   gdk_draw_indexed_image(wid->window, wid->style->fg_gc[GTK_STATE_NORMAL],
      CDisp.hist_x, CDisp.hist_y, CDisp.hist_width, CDisp.hist_height,
      GDK_RGB_DITHER_NORMAL,
      CDisp.buf+CDisp.hist_y*CDisp.width+CDisp.hist_x, CDisp.width, CMap);
}


/* Draw Histogram tickmarks and labels */
void draw_histogramlabels()
{
   int i, step, nticks, ticklen=4, lablen;
   int x1, y0, tickval[MAXTICKS];
   float trysteps, fstep, logscale, scalerange, slope;
   float hgt, dmax;
   char labstr[32];

   GdkSegment ticks[MAXTICKS];
   GtkWidget *wid = CDisp.wid;
   GdkWindow *win = wid->window;
   
   /* Determine tickmark values */
   trysteps = 10.;
   fstep = (SubDisp.dispmax - SubDisp.dispmin)/trysteps;
   logscale = (int) log10(fstep);
   scalerange = fstep / pow(10., logscale);

   if (scalerange < 1.5) step = 1;
   else if (scalerange < 3.) step = 2;
   else if (scalerange < 10.) step = 5;
   else step = 1;
   step = MAX(step * pow(10., logscale), 1.);

   tickval[0] = step*((int)(SubDisp.dispmin/step)) - 2*step;
   while (tickval[0] < SubDisp.dispmin) tickval[0] += step;
   i = 0;
   while (tickval[i]+step < SubDisp.dispmax  &&  i < MAXTICKS) {
      tickval[i+1] = tickval[i] + step;
      i++;
   }
   nticks = i+1;  

   /* Calculate tickmark coordinates */
   hgt = (float)CDisp.hist_height;
   slope = hgt / ((float)(SubDisp.dispmin - SubDisp.dispmax));
   dmax = SubDisp.dispmax;
   x1 = CDisp.bar_x - 3;  y0 = CDisp.bar_y;
   
   for (i=0; i<nticks; i++) {
      ticks[i].x1 = x1;  ticks[i].x2 = x1 - ticklen;
      ticks[i].y1 = ticks[i].y2 = y0 + (tickval[i]-dmax)*slope;
   }

   /* Initialize text area */
   gdk_draw_indexed_image(wid->window, wid->style->fg_gc[GTK_STATE_NORMAL],
      0, 0, x1, CDisp.height, GDK_RGB_DITHER_NORMAL,
      CDisp.buf, CDisp.width, CMap);

   /* Set foreground color to white */
   gdk_gc_set_foreground(GC_CB, White);

   /* Draw tickmarks and labels */
   if (nticks > 1) {
      gdk_draw_line(win, GC_CB, CDisp.bar_x-3, CDisp.bar_y,
          CDisp.bar_x-3, CDisp.bar_y+CDisp.bar_height-1);
      gdk_draw_segments (win, GC_CB, ticks, nticks);

      for (i=0; i<nticks; i++) {
         sprintf(labstr, "%d", tickval[i]);
         lablen = strlen(labstr);
         gdk_draw_text(win, HistFont, GC_CB, CDisp.bar_x-lablen*6-ticklen-5, ticks[i].y1+5,
            labstr, lablen);
      }
   }
}  


/* Draw lines indicating stretch upper and lower limits */
void draw_stretchlimitline(plottype *pl)
{
   /* Set foreground color to green */
   gdk_gc_set_foreground(GC_CB, Green);
   gdk_gc_set_line_attributes (GC_CB, 1, GDK_LINE_SOLID, GDK_CAP_ROUND, GDK_JOIN_ROUND);

   gdk_draw_line(CDisp.wid->window, GC_CB, pl->scrx0, pl->scry0, pl->scrx1, pl->scry1);

   pl->drawn = TRUE;

}

void clear_stretchlimitline(plottype *pl)
{
   gdk_draw_indexed_image(CDisp.wid->window, CDisp.wid->style->fg_gc[GTK_STATE_NORMAL],
      pl->scrx0, pl->scry0, pl->scrx1-pl->scrx0+1, pl->scry1-pl->scry0+1,
      GDK_RGB_DITHER_NORMAL,
      CDisp.buf + pl->scry0*CBARBUF_WIDTH + pl->scrx0, CBARBUF_WIDTH, CMap);

   pl->drawn = FALSE;
}


void box_checksubdisp(boxtype *b, int zoom)
{
   int offset=RLINEWID/2;
   int maxx, maxy, midx, midy;

  /* Reset box size to even number of image pixels */
   b->ncols = zoom*((b->scrx1 - b->scrx0 + 1)/zoom);
   b->nrows = zoom*((b->scry1 - b->scry0 + 1)/zoom);

  /* Reset edges of box on zoomed pixel edges, keeping center constant */
   midx = (b->scrx0+b->scrx1+1)/2;
   midy = (b->scry0+b->scry1+1)/2;
   b->scrx0 = zoom*((midx - b->ncols/2 + zoom/2)/zoom);
   b->scry0 = zoom*((midy - b->nrows/2 + zoom/2)/zoom);
   b->scrx1 = b->scrx0 + b->ncols - 1;
   b->scry1 = b->scry0 + b->nrows - 1;

  /* Make sure new box is entirely within window */
   maxx = SubDisp.width - 1;
   maxy = SubDisp.height - 1;
   
   if (b->scrx0 < offset) {
      b->scrx1 += offset-b->scrx0;
      b->scrx0 = offset;
   }
   if (b->scry0 < offset) {
      b->scry1 += offset-b->scry0;
      b->scry0 = offset;
   }
   if (b->scrx1 > maxx - offset) {
      b->scrx0 -= b->scrx1 - (maxx - offset);
      b->scrx1 = maxx - offset;
   }
   if (b->scry1 > maxy - offset) {
      b->scry0 -= b->scry1 - (maxy - offset);
      b->scry1 = maxy - offset;
   }
}



/* Calculate and display Cursor position on image and the image pixel value */

void subdisp_cursor(void)
{
   int imx, imy, imval;

   imx = Cursor_x/SubDisp.zoom;    
   imy = (SubDisp.height - 1 - Cursor_y)/SubDisp.zoom;
   imx = MAX(imx, 0);  imx = MIN(imx, SubIm[DC.current_sub].ncols-1);
   imy = MAX(imy, 0);  imy = MIN(imy, SubIm[DC.current_sub].nrows-1);

   imval = *(SubIm[DC.current_sub].data + imy*SubIm[DC.current_sub].ncols + imx);

   if (DC.region_mode == WHOLE)     /* Whole array */
      set_label(GUI.SubCursor_txt, "%4d, %4d: %5d", imx*2, imy*2, imval);
   else                         /* Quadrants or box */
      set_label(GUI.SubCursor_txt, "%4d, %4d: %5d", 
         SubIm[DC.current_sub].startcol+imx, SubIm[DC.current_sub].startrow+imy, imval);

}


/* Calculate and display Photometry box data */

void subdisp_photometry(void)
{
   int i, j, ncols, nrows, npix, badinc=1;
   int imminx, immaxx, imminy, immaxy;
   double photsum, photsigma;
   int *imptr, photmin, photmax, photbot, phottop;
   char *bptr, *dum;
   int *hptr = &Hist[0], hshift=NHIST/2;

   if (DC.region_mode == WHOLE) badinc = 2;   /* Whole image */
   else badinc = 1;

   /* Zero the histogram array */
   dum = (char*) memset((void*)hptr, 0, NHIST*sizeof(int));

   /* Calculate photometry values */
   imminx = PhotBox.scrx0 / SubDisp.zoom;
   immaxx = PhotBox.scrx1 / SubDisp.zoom;
   imminy = (SubDisp.height - 1 - PhotBox.scry1) / SubDisp.zoom;
   immaxy = (SubDisp.height - 1 - PhotBox.scry0) / SubDisp.zoom;

   ncols = PhotBox.width / SubDisp.zoom;
   nrows = PhotBox.height / SubDisp.zoom;
   npix = 0;

   photsum = photsigma = 0.;
   imptr = SubIm[DC.current_sub].data + imminy*SubIm[DC.current_sub].ncols + imminx;
   bptr = &BPM[0] + DET_NCOLS*(SubIm[DC.current_sub].startrow+imminy) + SubIm[DC.current_sub].startcol + imminx;

   for (i=0; i<nrows; i++) {
      for (j=0; j<ncols; j++) {
	 if (*bptr) {
            photsum += (double)*imptr;
            photsigma += (double)*imptr * (double)*imptr;
            (*(hptr+*imptr+hshift))++;
            npix++;
         }
         bptr += badinc;
         imptr++;
      }
      imptr += SubIm[DC.current_sub].ncols - ncols;
      bptr += badinc * (FullIm.ncols - ncols);
   }
   if (npix > 1)
      photsigma = sqrt(photsigma/(npix-1.) - pow(photsum/(double)npix, 2.));
   else
      photsigma = 0.;

   /* Find Data Min and Max from histogram */
   hist_minmax(hptr, NHIST, &photmin, &photmax);
   photmin = photmin-hshift;  photmax = photmax-hshift;

   /* Find Top and Bottom Percentage values from histogram */
   hist_bottop(hptr, NHIST, 0.01, &photbot, &phottop);
   photbot = photbot-hshift;  phottop = phottop-hshift;

  /* Display photometry values */
   imptr = SubIm[DC.current_sub].data + (imminy+nrows/2)*SubIm[DC.current_sub].ncols + imminx + ncols/2;
   if (DC.region_mode == WHOLE)     /* Whole array */
      set_label(GUI.PhotCenter_txt, "%4d , %4d", imminx*2+ncols, imminy*2+nrows);
   else                         /* Quadrants or box */
      set_label(GUI.PhotCenter_txt, "%4d , %4d", 
         SubIm[DC.current_sub].startcol+imminx+ncols/2, 
         SubIm[DC.current_sub].startrow+imminy+nrows/2);

   set_label(GUI.PhotSize_txt, "%4d x %4d", ncols, nrows);
   set_label(GUI.PhotSum_txt , "%11.3e"   , photsum);
   set_label(GUI.PhotAvg_txt , "%11.2f"   , photsum/(double)npix);
   set_label(GUI.PhotSDev_txt, "%11.2f"   , photsigma);

   PhotMax = photmax; PhotMin = photmin;
   PhotTop = phottop; PhotBot = photbot;
   PhotSum = photsum;
   PhotAvg = photsum/(double)npix;
   PhotSigma = photsigma;
}



/***** Generate Plots *****/

/* Compute mean background level in top and bottom rows of a box */
int compute_bkgd(int imx0, int imy0, int ncols, int nrows)
{
   int i, j, npix = 0, badinc;
   int background = 0;
   int *imptr;
   char *bptr;

   if (DC.region_mode == WHOLE) badinc = 2;   /* Whole image */
   else badinc = 1;

   imptr = SubIm[DC.current_sub].data + imy0*SubIm[DC.current_sub].ncols + imx0;
   bptr = &BPM[0] + DET_NCOLS*(SubIm[DC.current_sub].startrow+imy0) + 
             SubIm[DC.current_sub].startcol + imx0;

   /* Measure background from top and bottom rows */
   for (i=0; i<nrows; i+=nrows-1) {
      for (j=0; j<ncols; j++) {
	 if (*bptr) {
            background += *imptr;
            npix++;
         }
         bptr += badinc;
         imptr++;
      }
      imptr += (nrows-1)*SubIm[DC.current_sub].ncols - ncols;
      bptr += badinc * (FullIm.ncols - ncols);
   }

   return (background / npix);
}


int compute_centroid (int imx0, int imy0, int ncols, int nrows, int background,
       float *centroidx, float *centroidy, double *totflux)
{
   int i, j, npix = 0, badinc, signal;
   double accumx = 0., accumy = 0.;
   int *imptr;
   char *bptr;
   float centx, centy;
   double totsignal=0.;

   if (DC.region_mode == WHOLE) badinc = 2;   /* Whole image */
   else badinc = 1;

   imptr = SubIm[DC.current_sub].data + imy0*SubIm[DC.current_sub].ncols + imx0;
   bptr = &BPM[0] + DET_NCOLS*(SubIm[DC.current_sub].startrow+imy0) + 
              SubIm[DC.current_sub].startcol + imx0;
   *totflux = 0.;

   for (i=0; i<nrows; i++) {
      for (j=0; j<ncols; j++) {
         if (*bptr) {
            signal = (*imptr - background);
            accumx += (double) (j * signal);
            accumy += (double) (i * signal);
            totsignal += (double) signal;
            *totflux += (double) (*imptr);
            npix++;
         }
         bptr += badinc;
         imptr++;
      }
      imptr += SubIm[DC.current_sub].ncols - ncols;
      bptr += badinc * (FullIm.ncols - ncols);
   }

   if (npix > 1) {
      centx = MAX((float) (accumx / totsignal), 0.);
      centx = MIN(centx, (float) (ncols-1));
      centy = MAX((float) (accumy / totsignal), 0.);
      centy = MIN(centy, (float) (nrows-1));
   }
   else {
      centx = centy = 0.;
   }
   
   *centroidx = centx;
   *centroidy = centy;
   return(0);
}


/* Overall plotting function */
void subdisp_plot(void)
{
   int i, j, ncols, nrows, npts, badinc, ngood;
   int imx0, imx1, imy0, imy1, inc, *imptr, imncols;
   int ipeak, irad, background;
   char *bptr, labstr[128];
   float *ptrx, *ptry;
   float xdata[MAXPLOTPOINTS], ydata[MAXPLOTPOINTS];
   float xhalf[2], yhalf[2], xpeak, ypeak, yscale, ybaseline;
   float slope, centroidx, centroidy;
   double totflux;
   plotparams param;

   if (DC.region_mode == WHOLE) badinc = 2;   /* Whole image */
   else badinc = 1;
   npts = 0;
   
   switch (DC.plot_mode) {
   case NO_PLOT:  break; 

   case LINE_PLOT:

     /* Calculate plot values */
      imx0 = PlotLine.scrx0 / SubDisp.zoom;
      imx1 = PlotLine.scrx1 / SubDisp.zoom;
      imy0 = (SubDisp.height - 1 - PlotLine.scry0) / SubDisp.zoom;
      imy1 = (SubDisp.height - 1 - PlotLine.scry1) / SubDisp.zoom;

      ncols = MAX(1, PlotLine.width / SubDisp.zoom);
      nrows = MAX(1, PlotLine.height / SubDisp.zoom);
      npts = 0;
      imptr = SubIm[DC.current_sub].data;
      imncols = SubIm[DC.current_sub].ncols;
      strcpy(param.title, "Cross Section");
      strcpy(param.bxtitle, "(pixels)");
      strcpy(param.txtitle, "(arcsec)");
      param.txoffset = 0.;
      param.txscale  = SubIm[DC.current_sub].pixelscale * 3600.;
      param.ryoffset  = 0;
      param.ryscale   = 1;

      if (ncols > 0 && ncols >= nrows) {
         npts = ncols;
         ptrx = &xdata[0];  ptry = &ydata[0];
         slope = (float)(imy1-imy0)/(float)(imx1-imx0);
         for (j=imx0; j<=imx1; j++) {
            i = (int) (imy0 + (j-imx0)*slope + 0.5);
            *ptrx++ = sqrt(pow(j - imx0, 2.) + pow(i - imy0, 2.));
            *ptry++ = *(imptr + i*imncols + j);
         }
         plot_profile(npts, &xdata[0], &ydata[0], param);
      }
      else if (nrows > 0) {
         npts = nrows;
         ptrx = &xdata[0];  ptry = &ydata[0];
         slope = (float)(imx1-imx0)/(float)(imy1-imy0);
         if (imy1>imy0) inc = 1;
         else inc = -1;
         for (i=imy0; abs(i-imy0)<npts; i+=inc) {
            j = (int) (imx0 + (i-imy0)*slope + 0.5);
            *ptrx++ = sqrt(pow(j - imx0, 2.) + pow(i - imy0, 2.));
            *ptry++ = (float) *(imptr + i*imncols + j);
         }
         plot_profile(npts, &xdata[0], &ydata[0], param);
      }

      /* Plot FWHM */

      ybaseline = 0;
      for (i=0; i<3; i++) ybaseline += ydata[i];
      for (i=npts-3; i<npts; i++) ybaseline += ydata[i];
      ybaseline /= 6.;
    
      xpeak = xdata[0];
      ypeak = ydata[0]; 
      ipeak = 0;
      for (i=0; i<npts; i++) {
         if (ydata[i] > ypeak) {
            ipeak = i;
            xpeak = xdata[i];
            ypeak = ydata[i];
         }
      }

      yhalf[0] = yhalf[1] = (ypeak+ybaseline)/2.;

      /* Plot the FWHM info only if the peak is a sensible distance
       * from the endpoints.
       */
      if (ipeak < npts-3 && ipeak > 3) {
         i = ipeak;
         while ((ydata[i] >= yhalf[1]) && (i < npts)) i++;
         xhalf[1] = xdata[i-1]+(xdata[i]-xdata[i-1])*
                  (yhalf[1]-ydata[i-1])/(ydata[i]-ydata[i-1]);
         i = ipeak;
         while ((ydata[i] >= yhalf[0]) && (i >= 0)) i--;
         xhalf[0] = xdata[i]+(xdata[i+1]-xdata[i])*
                     (yhalf[0]-ydata[i])/(ydata[i+1]-ydata[i]);
   
         overplot_profile(2, xhalf, yhalf);
         sprintf(labstr, "FWHM = %6.1f", xhalf[1]-xhalf[0]);
         overplot_text(xhalf[1]+0.02*(xdata[npts-1]-xdata[0]), yhalf[1], labstr);
      }
      /* Copy plot from pixmap to screen */
      plot_put();
      break;

   case SPECTRAL_PLOT:                    /* Spectral Plot -- Avg rows or cols */

     /* Calculate plot values */
      imx0 = PlotLine.scrx0 / SubDisp.zoom;
      imx1 = PlotLine.scrx1 / SubDisp.zoom;
      imy0 = (SubDisp.height - PlotLine.scry1) / SubDisp.zoom;
      imy1 = (SubDisp.height - PlotLine.scry0) / SubDisp.zoom;

      ncols = MAX(1, PlotLine.width / SubDisp.zoom);
      nrows = MAX(1, PlotLine.height / SubDisp.zoom);
      imptr = SubIm[DC.current_sub].data;

      if (ncols > 0 && ncols >= nrows) {

         strcpy(param.title, "Spectrum (Avg across columns)");
         strcpy(param.bxtitle, " ");
         strcpy(param.txtitle, " ");
         param.txoffset = 0.;
         param.txscale  = 1.;
         param.ryoffset  = 0;
         param.ryscale   = 1;

         imy0 = 0;
         imy1 = SubIm[DC.current_sub].nrows - 1;
         ptrx = &xdata[0];
         bptr = &BPM[0] + DET_NCOLS*(SubIm[DC.current_sub].startrow+imy0) + 
                 SubIm[DC.current_sub].startcol + imx0;
         imncols = SubIm[DC.current_sub].ncols;
         npts = SubIm[DC.current_sub].nrows;

         for (j=imy0; j<=imy1; j++) {
            ydata[j] = j;
            *ptrx = 0.;
            ngood = 0;
            for (i=imx0; i<= imx1; i++) {
	       if (*bptr) *ptrx += *(imptr + j*imncols + i);
               bptr += badinc;
               ngood++;
            }
            *ptrx /= (float)(MAX(ngood, 1));
            ptrx++;
            bptr += badinc * (FullIm.ncols - ncols);
         }
      }
      else if (nrows > 0) {

         strcpy(param.title, "Spectrum (Avg across rows)");
         strcpy(param.bxtitle, "(pixels)");
         strcpy(param.txtitle, "()");
         param.txoffset = 0.;
         param.txscale  = 1.;
         param.ryoffset  = 0;
         param.ryscale   = 1;

         imx0 = 0;
         imx1 = SubIm[DC.current_sub].ncols - 1;
         if (imy1 < imy0) {
            npts = imy0;
            imy0 = imy1;
            imy1 = npts;
         }
         ptry = &ydata[0];
         npts = imncols = SubIm[DC.current_sub].ncols;
         badinc = badinc * FullIm.ncols;

         for (j=imx0; j<=imx1; j++) {
            bptr = &BPM[0] + DET_NCOLS*(SubIm[DC.current_sub].startrow+imy0) + 
                 SubIm[DC.current_sub].startcol + j;
            xdata[j] = j;
            *ptry = 0.;
            ngood = 0;
            for (i=imy0; i<= imy1; i++) {
               if (*bptr) *ptry += *(imptr + i*imncols + j);
               bptr += badinc;
               ngood++;
            }
            *ptry /= (float)(MAX(ngood, 1));
            ptry++;
         }
      }
      
      /* Draw plot in pixmap */
      plot_profile(npts, &xdata[0], &ydata[0], param);

      /* Copy pixmap to screen */
      plot_put();
      break;


   case RADIAL_PLOT:

      imx0 = PhotBox.scrx0 / SubDisp.zoom;
      imx1 = PhotBox.scrx1 / SubDisp.zoom;
      imy0 = (SubDisp.height - PhotBox.scry1) / SubDisp.zoom;
      imy1 = (SubDisp.height - PhotBox.scry0) / SubDisp.zoom;

      ncols = PhotBox.width / SubDisp.zoom;
      nrows = PhotBox.height / SubDisp.zoom;

      background = compute_bkgd(imx0, imy0, ncols, nrows);
      compute_centroid(imx0, imy0, ncols, nrows, background, 
         &centroidx, &centroidy, &totflux);
 
      npts = 0;
      imptr = SubIm[DC.current_sub].data + imy0*SubIm[DC.current_sub].ncols + imx0;
      bptr = &BPM[0] + DET_NCOLS*(SubIm[DC.current_sub].startrow+imy0) + 
                 SubIm[DC.current_sub].startcol + imx0;

      for (i=0; i<nrows; i++) {
         for (j=0; j<ncols; j++) {
	    if (*bptr && npts < MAXPLOTPOINTS) {
               xdata[npts] = sqrt(pow(j-centroidx, 2.) + pow(i-centroidy, 2.) );
               ydata[npts] = (float) *imptr;
               npts++;
            }
            bptr += badinc;
            imptr++;
         }
         imptr += SubIm[DC.current_sub].ncols - ncols;
         bptr += badinc * (FullIm.ncols - ncols);
      }

      if (npts < MAXPLOTPOINTS) {
         strcpy(param.title, "Radial Plot");
         strcpy(param.bxtitle, "radius (pixels)");
         strcpy(param.txtitle, "radius (arcsec)");
         param.txoffset = 0.;
         param.txscale  = SubIm[DC.current_sub].pixelscale * 3600.;
         param.ryoffset = 0.;
         param.ryscale  = 1000./totflux;

         plot_points(npts, &xdata[0], &ydata[0], param);
      }
      else plot_clear();

      plot_put();

      break;

   case ENCIRC_PLOT:
printf("encirc\n");
      imx0 = PhotBox.scrx0 / SubDisp.zoom;
      imx1 = PhotBox.scrx1 / SubDisp.zoom;
      imy0 = (SubDisp.height - PhotBox.scry1) / SubDisp.zoom;
      imy1 = (SubDisp.height - PhotBox.scry0) / SubDisp.zoom;

      ncols = PhotBox.width / SubDisp.zoom;
      nrows = PhotBox.height / SubDisp.zoom;

      background = compute_bkgd(imx0, imy0, ncols, nrows);
      compute_centroid(imx0, imy0, ncols, nrows, background, 
         &centroidx, &centroidy, &totflux);

      npts = MIN((int)centroidx, ncols-1-(int)centroidx);
      npts = MIN(npts, centroidy);
      npts = MIN(npts, nrows-1-(int)centroidy);

      for (i=0; i<npts; i++) {
         xdata[i] = i;
         ydata[i] = 0.;
      }
printf("measuring\n");
      imptr = SubIm[DC.current_sub].data + imy0*SubIm[DC.current_sub].ncols + imx0;
      bptr = &BPM[0] + DET_NCOLS*(SubIm[DC.current_sub].startrow+imy0) + 
                 SubIm[DC.current_sub].startcol + imx0;

      for (i=0; i<nrows; i++) {
         for (j=0; j<ncols; j++) {
	    if (*bptr) {
               irad = (int) (sqrt(pow(j-centroidx, 2.) + 
                                  pow(i-centroidy, 2.) ) + 0.5);
               ydata[irad] += (float) (*imptr - background);
            }
            bptr += badinc;
            imptr++;
         }
         imptr += SubIm[DC.current_sub].ncols - ncols;
         bptr += badinc * (FullIm.ncols - ncols);
      }

      for (i=npts-1; i>=0; i--) {
         for (j=0; j<i; j++) ydata[i] += ydata[j];
      }
      yscale = ydata[npts-1]/100;
      for (i=0; i<npts; i++) ydata[i] /= yscale;
printf("plotting\n");
      strcpy(param.title, "Encircled Energy");
      strcpy(param.bxtitle, "radius (pixels)");
      strcpy(param.txtitle, "radius (arcsec)");

      param.txoffset = 0.;
      param.txscale  = SubIm[DC.current_sub].pixelscale * 3600.;
      param.ryoffset  = 0;
      param.ryscale   = 1;

printf("plot_profile\n");
      plot_profile(npts, &xdata[0], &ydata[0], param);
      plot_put();
printf("done\n");
      break;

   case SURFACE_PLOT:           /* Surface Plot */
      break;

   }

}

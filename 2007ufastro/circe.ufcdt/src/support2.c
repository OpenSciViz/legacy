/*------------------------------------------------------------*
 |
 | PROJECT:
 |	Canarias Infrared Camera Experiment (CIRCE)
 |      University of Florida, Gainesville, FL
 |
 | File: support2.c -- ufCDT Interface support routines
 |
 | DESCRIPTION
 |	This source code file initializes the global structure
 |      in which pointers to the interface GTK widgets are saved.
 |      The pointers are set using lookup_widget(), so further
 |      calls to lookup_widget() elsewhere in the code are not
 |      necessary.
 |
 | REVISION HISTORY:
 | Date           By                Description
 | 2005-April-4   A. Marin-Franch   Adapted from xwirc
 |
 *------------------------------------------------------------*/ 

#include <stdio.h>
#include <gtk/gtk.h>

#include "support.h"
#include "support2.h"
#include "callbacks.h"

extern gboolean on_SubDisp_hscrollbar_valuechanged_event(GtkAdjustment *, gpointer *);
extern gboolean on_SubDisp_vscrollbar_valuechanged_event(GtkAdjustment *, gpointer *);

void init_gui_struct ()
{

   GUI.Msg_entry             = lookup_widget(GUI.Message_window, "Msg_entry");

   GUI.SubDisp_darea         = lookup_widget(GUI.SubDisp_window, "SubDisp_drawingarea");
   GUI.SubDisp_vscrollbar    = lookup_widget(GUI.SubDisp_window, "SubDisp_vscrollbar");
   GUI.SubDisp_hscrollbar    = lookup_widget(GUI.SubDisp_window, "SubDisp_hscrollbar");
   GUI.SubSrc_label          = lookup_widget(GUI.SubDisp_window, "SubSrc_label");
   GUI.SubBgd_label          = lookup_widget(GUI.SubDisp_window, "SubBgd_label");

   GUI.CBar_vscale1          = lookup_widget(GUI.SubDisp_window, "CBar_vscale1");   
   GUI.CDisp_darea           = lookup_widget(GUI.SubDisp_window, "ColorBar_drawingarea");

   GUI.DetSection5_tb        = lookup_widget(GUI.SubDisp_window, "DetSection5_tb");
   GUI.DetSection6_tb        = lookup_widget(GUI.SubDisp_window, "DetSection6_tb");
   
   GUI.SubSig_rb             = lookup_widget(GUI.SubDisp_window, "SubSig_rb");
   GUI.SubRef_rb             = lookup_widget(GUI.SubDisp_window, "SubRef_rb");
   GUI.SubDif_rb             = lookup_widget(GUI.SubDisp_window, "SubDif_rb");

   GUI.SubSrc_rb             = lookup_widget(GUI.SubDisp_window, "SubSrc_rb");
   GUI.SubBgd_rb             = lookup_widget(GUI.SubDisp_window, "SubBgd_rb");
   GUI.SubSB_rb              = lookup_widget(GUI.SubDisp_window, "SubSB_rb");

   GUI.SubSrc_label          = lookup_widget(GUI.SubDisp_window, "SubSrc_label");
   GUI.SubBgd_label          = lookup_widget(GUI.SubDisp_window, "SubBgd_label");
   
   GUI.SubHiStr1_rb          = lookup_widget(GUI.SubDisp_window, "SubHiStr1_rb");
   GUI.SubHiStr2_rb          = lookup_widget(GUI.SubDisp_window, "SubHiStr2_rb");
   GUI.SubLoStr1_rb          = lookup_widget(GUI.SubDisp_window, "SubLoStr1_rb");
   GUI.SubLoStr2_rb          = lookup_widget(GUI.SubDisp_window, "SubLoStr2_rb");

   GUI.SubStrMode1_rb        = lookup_widget(GUI.SubDisp_window, "SubStrMode1_rb");
   GUI.SubStrMode2_rb        = lookup_widget(GUI.SubDisp_window, "SubStrMode2_rb");
   GUI.SubStrMode1_rb        = lookup_widget(GUI.SubDisp_window, "SubStrMode1_rb");
   GUI.SubStrMode2_rb        = lookup_widget(GUI.SubDisp_window, "SubStrMode2_rb");

   GUI.SubCursor_txt         = lookup_widget(GUI.SubDisp_window, "Cursor_txt");
   GUI.PhotCenter_txt        = lookup_widget(GUI.SubDisp_window, "PhotCenter_txt");
   GUI.PhotSize_txt          = lookup_widget(GUI.SubDisp_window, "PhotSize_txt");
   GUI.PhotSum_txt           = lookup_widget(GUI.SubDisp_window, "PhotSum_txt");
   GUI.PhotAvg_txt           = lookup_widget(GUI.SubDisp_window, "PhotAvg_txt");
   GUI.PhotSDev_txt          = lookup_widget(GUI.SubDisp_window, "PhotSDev_txt");

   GUI.Plot_darea            = lookup_widget(GUI.Plot_window,    "Plot_darea");
   
   GUI.FullDisp_darea        = lookup_widget(GUI.FullDisp_window, "FullDisp_drawingarea");
   GUI.Compass_darea         = lookup_widget(GUI.FullDisp_window, "Compass_drawingarea");

   GUI.FullSig_rb            = lookup_widget(GUI.FullDisp_window, "FullSig_rb");
   GUI.FullRef_rb            = lookup_widget(GUI.FullDisp_window, "FullRef_rb");
   GUI.FullDif_rb            = lookup_widget(GUI.FullDisp_window, "FullDif_rb");
   GUI.FullSrc_rb            = lookup_widget(GUI.FullDisp_window, "FullSrc_rb");
   GUI.FullBgd_rb            = lookup_widget(GUI.FullDisp_window, "FullBgd_rb");
   GUI.FullSB_rb             = lookup_widget(GUI.FullDisp_window, "FullSB_rb");

   GUI.FullDataMin_entry     = lookup_widget(GUI.FullDisp_window, "FullDataMin_entry");
   GUI.FullDataMax_entry     = lookup_widget(GUI.FullDisp_window, "FullDataMax_entry");
   GUI.FullDisplayMin_entry  = lookup_widget(GUI.FullDisp_window, "FullDisplayMin_entry");
   GUI.FullDisplayMax_entry  = lookup_widget(GUI.FullDisp_window, "FullDisplayMax_entry");

   GUI.FullScale1_rb         = lookup_widget(GUI.FullDisp_window, "FullScale1_rb");
   GUI.FullScale2_rb         = lookup_widget(GUI.FullDisp_window, "FullScale2_rb");
   GUI.FullScale3_rb         = lookup_widget(GUI.FullDisp_window, "FullScale3_rb");

   GUI.FullZoom1_rb          = lookup_widget(GUI.FullDisp_window, "FullZoom1_rb");
   GUI.FullZoom2_rb          = lookup_widget(GUI.FullDisp_window, "FullZoom2_rb");
   GUI.FullZoom3_rb          = lookup_widget(GUI.FullDisp_window, "FullZoom3_rb");
   GUI.FullZoom4_rb          = lookup_widget(GUI.FullDisp_window, "FullZoom4_rb");
   GUI.FullZoom5_rb          = lookup_widget(GUI.FullDisp_window, "FullZoom5_rb");

   GUI.FullVerFidA_tb        = lookup_widget(GUI.FullDisp_window, "FullVerFidA_tb");
   GUI.FullVerFidB_tb        = lookup_widget(GUI.FullDisp_window, "FullVerFidB_tb");
   GUI.FullHorFidA_tb        = lookup_widget(GUI.FullDisp_window, "FullHorFidA_tb");
   GUI.FullHorFidB_tb        = lookup_widget(GUI.FullDisp_window, "FullHorFidB_tb");
   GUI.FullVerFidA_entry     = lookup_widget(GUI.FullDisp_window, "FullVerFidA_entry");
   GUI.FullVerFidB_entry     = lookup_widget(GUI.FullDisp_window, "FullVerFidB_entry");
   GUI.FullHorFidA_entry     = lookup_widget(GUI.FullDisp_window, "FullHorFidA_entry");
   GUI.FullHorFidB_entry     = lookup_widget(GUI.FullDisp_window, "FullHorFidB_entry");

   GUI.FullCrossA_tb         = lookup_widget(GUI.FullDisp_window, "FullCrossA_tb");
   GUI.FullCrossB_tb         = lookup_widget(GUI.FullDisp_window, "FullCrossB_tb");
   GUI.FullCrossACol_entry   = lookup_widget(GUI.FullDisp_window, "FullCrossACol_entry");
   GUI.FullCrossARow_entry   = lookup_widget(GUI.FullDisp_window, "FullCrossARow_entry");
   GUI.FullCrossBCol_entry   = lookup_widget(GUI.FullDisp_window, "FullCrossBCol_entry");
   GUI.FullCrossBRow_entry   = lookup_widget(GUI.FullDisp_window, "FullCrossBRow_entry");
   
   gtk_widget_set_events (GUI.FullDisp_darea, GDK_EXPOSURE_MASK
                          | GDK_ENTER_NOTIFY_MASK | GDK_LEAVE_NOTIFY_MASK
                          | GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK 
                          | GDK_POINTER_MOTION_MASK );

   gtk_widget_set_events (GUI.SubDisp_darea, GDK_EXPOSURE_MASK
                          | GDK_ENTER_NOTIFY_MASK | GDK_LEAVE_NOTIFY_MASK
                          | GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK 
                          | GDK_POINTER_MOTION_MASK );

   gtk_widget_set_events (GUI.CDisp_darea, GDK_EXPOSURE_MASK
                          | GDK_ENTER_NOTIFY_MASK | GDK_LEAVE_NOTIFY_MASK
                          | GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK 
                          | GDK_POINTER_MOTION_MASK );

   /* Can't define these signal connections using Glade */

   gtk_signal_connect (GTK_OBJECT (GTK_VSCROLLBAR(GUI.SubDisp_vscrollbar)->scrollbar.range.adjustment), 
                       "value_changed",
                       GTK_SIGNAL_FUNC (on_SubDisp_vscrollbar_valuechanged_event), NULL);

   gtk_signal_connect (GTK_OBJECT (GTK_HSCROLLBAR(GUI.SubDisp_hscrollbar)->scrollbar.range.adjustment), 
                       "value_changed",
                       GTK_SIGNAL_FUNC (on_SubDisp_hscrollbar_valuechanged_event), NULL);


}


/* Set the value of a label widget using a specified numerical format */

void set_label(GtkWidget *wid, char *fmt, ...)
{
   gchar label_str[256];
   va_list ap;

   /* Construct output string */
   va_start(ap, fmt);
   (void) vsprintf(label_str, fmt, ap);
   
   /* Set label widget */
   gtk_label_set_text (GTK_LABEL(wid), label_str);
}


void set_entry(GtkWidget *wid, char *fmt, ...)
{
   gchar entry_str[256];
   va_list ap;
   
   /* Construct output string */
   va_start(ap, fmt);
   (void) vsprintf(entry_str, fmt, ap);

   /* Set entry widget */
   gtk_entry_set_text (GTK_ENTRY(wid), entry_str);
}

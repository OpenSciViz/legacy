/*------------------------------------------------------------*
 |
 | PROJECT:
 |	Canarias Infrared Camera Experiment (CIRCE)
 |      University of Florida, Gainesville, FL
 |
 | FILE: display.c -- Initialization and control routines common
 |                    to both the Full and Sub displays.
 |
 | DESCRIPTION:
 |
 |
 | REVISION HISTORY:
 | Date           By                Description
 | 2005-April-4   A. Marin-Franch   Adapted from xwirc
 |
 *------------------------------------------------------------*/ 

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <math.h>
#include <gtk/gtk.h>

#include "main.h"
#include "support.h"
#include "support2.h"
#include "fiber.h"
#include "files.h"
#include "fits.h"
#include "display.h"
#include "plot.h"

int NColors[2], MinColor[2], MaxColor[2];
float Gamma;
int NColors_arg=MAXCOLORS;

GdkRgbCmap *CMap;
GdkGC      *GC_FD, *GC_SD, *GC_CB;
GdkColor   *Green, *White, *Black;
GdkFont    *TxtFont, *HistFont;

guchar CompassBuf;
guchar BPM[DET_NPIX];

int  HistogramData[MAXCOLORS];
image *FitsDest = &SrcImage;

double PhotSum=0., PhotAvg=0., PhotSigma=0.;
int PhotMax=0, PhotMin=0, PhotTop, PhotBot;
int Cursor_x=0, Cursor_y=0;

dispcontroltype DC = {
   NONE_ANC, QUADRANT, NO_PLOT,
   DIFFERENCE, SB,
   0, FALSE
};

image        FullIm, SubIm[NSUBIMAGES];
displaytype  FullDisp, SubDisp;
colorbartype CDisp;

boxtype  ZoomBox     = {512, 511-127, 512+127,  511,   /* Screen x, y coords (1st row,col inside drawn box) */
                          0,       0,       0,    0,   /* Screen x, y limits (set below) */
                        128,     128,     128,  128,   /* (screen) width, height; (det) ncols, nrows */
                        FALSE, FALSE};                 /* Bulb, drawn */
                        
boxtype  PhotBox     = {  2,       2,      65,   65,
                          0,       0,       0,    0,
                         64,      64,      64,   64,
                        TRUE, FALSE};

plottype PlotLine    = {496-64,    16,    496,   16,
                             0,   511,      0,  511,
                            64,     1,      0,    0,
                          TRUE, FALSE,
                             0,     0,    "\0"};

plottype StretchULim = {CBAR_X, CBAR_Y, HIST_X+HIST_WIDTH-1, CBAR_Y,               /* scrx0, scry0, scrx1, scry1 */
                        CBAR_X, HIST_X+HIST_WIDTH-1, CBAR_Y, CBAR_Y+CBAR_HEIGHT-1, /* minx,  maxx, miny, maxy */
                        0, 0, 0, 0,                                                /* width, height, ncols, nrows */
                        FALSE, FALSE,                                              /* bulb, drawn */
                        0, 0, "\0"};                                               /* labx, laby, label */

plottype StretchLLim = {CBAR_X, CBAR_Y+CBAR_HEIGHT-1, HIST_X+HIST_WIDTH-1, CBAR_Y+CBAR_HEIGHT-1, 
                        CBAR_X, HIST_X+HIST_WIDTH-1, CBAR_Y, CBAR_Y+CBAR_HEIGHT-1,                      
                        0, 0, 0, 0,
                        FALSE, FALSE,
                        0, 0, "\0"};

plottype VerFidA     = {512,    0,  512,   0,   0, 0, 0, 0,   0, 0, 0, 0,   FALSE, FALSE,  7, 14, "A\0"};
plottype VerFidB     = { 64,    0,   64,   0,   0, 0, 0, 0,   0, 0, 0, 0,   FALSE, FALSE,  7, 14, "B\0"};
plottype HorFidA     = {  0,  511,    0, 511,   0, 0, 0, 0,   0, 0, 0, 0,   FALSE, FALSE,  7, 14, "A\0"};
plottype HorFidB     = {  0,  959,    0, 959,   0, 0, 0, 0,   0, 0, 0, 0,   FALSE, FALSE,  7, 14, "B\0"};
plottype CrossA      = {512,  511,    0,   0,   0, 0, 0, 0,   0, 0, 0, 0,   FALSE, FALSE,  7,  0, "A\0"};
plottype CrossB      = { 64,  959,    0,   0,   0, 0, 0, 0,   0, 0, 0, 0,   FALSE, FALSE,  7,  0, "B\0"};


/****** Initialize Display *****/

int init_display(void)
{
   int i, lwid = RLINEWID;
   gint width, height;
   guint32 *colors;

   GdkCursor *crosscursor;
      
   /*** Initialize Global structures ***/
   /* scrx, scry are screen coordinates (0, 0 = UPPER LEFT corner) */

   FullIm.ncols       = DET_NCOLS;         FullIm.nrows    = DET_NROWS;
   FullIm.nplanes     = 1;                 FullIm.npixels  = DET_NPIX;
   FullIm.nquadpixels = FullIm.npixels/4;  FullIm.nbytes   = FullIm.npixels * sizeof(int);
   FullIm.datamin     = 0;                 FullIm.datamax  = 0;
   FullIm.data = (int *) malloc(FullIm.nbytes);
   if (FullIm.data == NULL) {
      fprintf(stderr, "init_display: Error allocating memory for FullIm.\n");
      return(-1);
   }
   
   gdk_window_get_size(GUI.FullDisp_darea->window, &width, &height);

   FullDisp.wid         = GUI.FullDisp_darea;
   FullDisp.width       = width;            FullDisp.height = height;
   FullDisp.xborder     = FULLWIN_XBORDER;  FullDisp.yborder = FULLWIN_YBORDER;
   FullDisp.nbytes      = width*height;     FullDisp.zoom = -2;    
   FullDisp.ncols       = DET_NCOLS;        FullDisp.nrows = DET_NROWS;
   FullDisp.stretchmode = BOX;
   FullDisp.buf = (guchar *) malloc(FullDisp.nbytes);
   if (FullDisp.buf == NULL) {
      fprintf(stderr, "init_display: Error allocating memory for FullDisp.\n");
      return(-1);
   }

   ZoomBox.minx = lwid;  ZoomBox.maxx = FullDisp.width - 1 - lwid;
   ZoomBox.miny = lwid;  ZoomBox.maxy = FullDisp.height - 1 - lwid;
   
   VerFidA.scry1 = VerFidB.scry1 = height-1;
   VerFidA.scrx0 = VerFidA.scrx1 = MIN(VerFidA.scrx0, width-10);
   VerFidB.scrx0 = VerFidB.scrx1 = MIN(VerFidB.scrx0, width-10);
   HorFidA.scrx1 = HorFidB.scrx1 = width-1;
   HorFidA.scry0 = HorFidA.scry1 = MIN(HorFidA.scry0, height-10);
   HorFidB.scry0 = HorFidB.scry1 = MIN(HorFidB.scry0, height-10);
   CrossA.scrx0  = MIN(CrossA.scrx0, width-32);
   CrossA.scry0  = MIN(CrossA.scry0, height-32);
   CrossB.scrx0  = MIN(CrossB.scrx0, width-32);
   CrossB.scry0  = MIN(CrossB.scry0, height-32);

   
   /***** Initialize SubDisplay *****/

   /* Initialize image params assuming zoom = 1 */   
   for (i=0; i<NSUBIMAGES; i++) {
      SubIm[i].ncols       = QUAD_NCOLS;          SubIm[i].nrows   = QUAD_NROWS;
      SubIm[i].nplanes     = 1;                   SubIm[i].npixels = SubIm[i].ncols*SubIm[i].nrows;
      SubIm[i].nquadpixels = SubIm[i].npixels;    SubIm[i].nbytes  = SubIm[i].npixels * sizeof(int);
      SubIm[i].startcol    = QUAD_NCOLS;          SubIm[i].startrow = 0;
   }

   SubDisp.wid = GUI.SubDisp_darea;
   gdk_window_get_size(GUI.SubDisp_darea->window, &width, &height);

   SubDisp.width        = width;                  SubDisp.height   = height;
   SubDisp.xborder      = SUBWIN_XBORDER;         SubDisp.yborder  = SUBWIN_YBORDER;
   SubDisp.nbytes       = width * height;         SubDisp.zoom     = 1;
   SubDisp.startrow     = 0;                      SubDisp.startcol = 0;
   SubDisp.ncols        = width-2*SUBWIN_XBORDER; SubDisp.nrows    = height-2*SUBWIN_YBORDER;
   SubDisp.stretchmode  = IMAGE;
   SubDisp.bplabel      = FALSE;
   SubDisp.stretchlo    = 1;                      SubDisp.stretchhi = 0;
   
   SubDisp.buf = (guchar *) malloc(SubDisp.nbytes);
   if (SubDisp.buf == NULL) {
      fprintf(stderr, "init_display: memory allocation error for SubDisp.\n");
      return(-1);
   }   

   PhotBox.minx = lwid;  PhotBox.maxx = SubDisp.width - 1 - lwid;
   PhotBox.miny = lwid;  PhotBox.maxy = SubDisp.height - 1 - lwid;

   PlotLine.minx = lwid/2;  PlotLine.maxx = SubDisp.width - 1 - lwid/2;
   PlotLine.miny = lwid/2;  PlotLine.maxy = SubDisp.height - 1 - lwid/2;
   
/* ColorBar & Histogram initialization */
   CDisp.wid = GUI.CDisp_darea;
   gdk_window_get_size(CDisp.wid->window, &width, &height);

   CDisp.width      = width;               CDisp.height      = height;
   CDisp.xborder    = 0;                   CDisp.yborder     = 0;
   CDisp.nbytes     = width * height;
   CDisp.bar_x      = CBAR_X;              CDisp.bar_y       = CBAR_Y;
   CDisp.bar_width  = CBAR_WIDTH;          CDisp.bar_height  = CBAR_HEIGHT;
   CDisp.hist_x     = HIST_X;              CDisp.hist_y      = HIST_Y;
   CDisp.hist_width = HIST_WIDTH;          CDisp.hist_height = HIST_HEIGHT;
   
   CDisp.buf = (guchar *) malloc(CDisp.nbytes);
   if (CDisp.buf == NULL) {
      fprintf(stderr, "init_display: memory allocation error for CDisp.\n");
      return(-1);
   }   

   
   /*** Graphics Contexts ***/
      
   GC_FD = gdk_gc_new((GdkWindow*)GUI.FullDisp_darea->window);
   GC_SD = gdk_gc_new((GdkWindow*)GUI.SubDisp_darea->window);
   GC_CB = gdk_gc_new((GdkWindow*)GUI.CDisp_darea->window);


   /*** Colormap ***/

   NColors[0] = MAX(16, MIN(MAXCOLORS, NColors_arg));
   MinColor[0] = 0;  MaxColor[0] = NColors[0]-1;  Gamma = 1;
   colors = malloc((NColors[0])*4);
   CMap = gdk_rgb_cmap_new(colors, NColors[0]);

   /* Set one color to green */
   Green = (GdkColor *)malloc(sizeof(GdkColor));
   Green->red = 0;
   Green->green = 255<<8;
   Green->blue = 0;

   /* Set other colors to white and black */
   White = (GdkColor *)malloc(sizeof(GdkColor));
   gdk_color_white(gtk_widget_get_colormap(GUI.CDisp_darea), White);

   Black = (GdkColor *)malloc(sizeof(GdkColor));
   gdk_color_black(gtk_widget_get_colormap(GUI.CDisp_darea), Black);
   
   /* Set the foreground color to green in the Sub and Full Displays */
   gdk_color_alloc(gtk_widget_get_colormap(GUI.FullDisp_darea), Green);
   gdk_gc_set_foreground(GC_FD, Green);

   gdk_color_alloc(gtk_widget_get_colormap(GUI.SubDisp_darea), Green);
   gdk_gc_set_foreground(GC_SD, Green);

   /* Allocate green in the ColorBar but set foreground color to white for now */
   gdk_color_alloc(gtk_widget_get_colormap(GUI.CDisp_darea), Green);
   gdk_gc_set_foreground(GC_CB, White);

   /* Set rest of color map to gray-scale */
   setcolors(NColors[0], MinColor[0], MaxColor[0], Gamma);


   /*** Fonts and Cursors ***/
   /* "Screen16" used for motif */
   TxtFont  = gdk_font_load("-adobe-helvetica-medium-r-normal--*-140-*-*-*-*-*-*");
   HistFont = gdk_font_load("-adobe-helvetica-medium-r-normal--*-100-*-*-*-*-*-*");

   /* This doesn't seem to work 
   gdk_gc_set_font(GC_FD, TxtFont); */

   crosscursor = gdk_cursor_new(GDK_CROSSHAIR);
   gdk_window_set_cursor(GUI.SubDisp_darea->window, crosscursor);
   gdk_window_set_cursor(GUI.FullDisp_darea->window, crosscursor);


   /*** Full Display Widgets ***/
   
   gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(GUI.FullScale2_rb), TRUE);
   gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(GUI.FullZoom3_rb), TRUE);

   set_entry(GUI.FullVerFidA_entry,   "%d", VerFidA.scrx0);
   set_entry(GUI.FullVerFidB_entry,   "%d", VerFidB.scrx0);
   set_entry(GUI.FullHorFidA_entry,   "%d", FullDisp.height - 1 - HorFidA.scry0);
   set_entry(GUI.FullHorFidB_entry,   "%d", FullDisp.height - 1 - HorFidB.scry0);
   set_entry(GUI.FullCrossACol_entry, "%d", CrossA.scrx0);
   set_entry(GUI.FullCrossARow_entry, "%d", FullDisp.height - 1 - CrossA.scry0);
   set_entry(GUI.FullCrossBCol_entry, "%d", CrossB.scrx0);
   set_entry(GUI.FullCrossBRow_entry, "%d", FullDisp.height - 1 - CrossB.scry0);
   
   
   /*** SubDisplay Widgets ***/
   
   gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(GUI.DetSection5_tb), TRUE);

   /* The FullDif_sb and FullSrc_sb rb's will be set in the Sub callbacks */
   gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(GUI.SubDif_rb), TRUE);
   gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(GUI.SubSrc_rb), TRUE);
   
   gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(GUI.SubLoStr2_rb), TRUE);
   gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(GUI.SubHiStr1_rb), TRUE);
   gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(GUI.SubStrMode1_rb), TRUE);
/* gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(GUI.ReadFitsSrc_rb), TRUE); */
   set_scrollbars (512, 1023, 512, 1023);

/* Initialize the colorbar */
/* The colorbar will be displayed by on_ColorBar_darea_expose_event() in subdisp.c */
   stuff_cbar();
   
   /*** Bad Pixel mask ***/
   readbpmfile();

   return(0);
}


/* Write Bad Pixel Mask array to disk file */

void writebpmfile(void)
{
   int fp;
   unsigned int nbytes;
   char filename[80];

   /* Open BPM file */
   strcpy(filename, getenv("HOME"));
   strcat(filename, BPMFILE);
   fp = open(filename, O_CREAT|O_WRONLY, S_IWRITE|S_IREAD);
   if (fp < 0) {
      msgprintf("Error opening %s file for writing. \n", filename);
      return;
   }
   
   /* Write BPM array */
   nbytes  = write(fp, &BPM[0], DET_NPIX);

   /* Close file */
   close(fp);
}


/* Read file containing bad pixel map */ 

int readbpmfile(void)
{
   int nbytes, expectlen=DET_NPIX;
   int i, fp;
   char filename[80];
   struct stat stbuf;

/* Attempt to open BPM file and compare size to that expected */
   strcpy(filename, getenv("HOME"));
   strcat(filename, BPMFILE);
   stat(filename, &stbuf);
   fp = open(filename, O_RDWR);
   
   if (fp <= 0 || stbuf.st_size != expectlen) {
      msgprintf("Warning: Error opening file %s\n", BPMFILE);
      for (i=0; i<expectlen; i++) BPM[i] = 1;
      return(-1);
   }

/* Read BPM file */
   nbytes = read (fp, &BPM[0], expectlen);

   close(fp);
   return(0);
}


/* Initialize Grey and Color scales according to number of allocated cells */
void setcolors (int ncolors, int mincolor, int maxcolor, float gamma)
{
   int i, j, cvalue;
   float cscale, nc;

   for (i=0; i<mincolor; i++) CMap->colors[i] = 0;

   j = 0;
   nc = maxcolor - mincolor + 1;
   cscale = 255./nc;

   for (i=mincolor; i<=maxcolor; i++) {
      cvalue = ((int)( nc*pow((float)j++/nc, gamma)*cscale + 0.5 ));
      CMap->colors[i] = (cvalue<<16) + (cvalue<<8) + cvalue;
   }

   for (i=maxcolor+1; i<ncolors; i++) CMap->colors[i] = (255<<16) + (255<<8) + 255;
}


/***** Rubber plot line control functions *****/

void draw_plotline(displaytype *disp, GdkGC *gc, plottype *pl)
{  
   int midx, midy;

   midx = (pl->scrx0+pl->scrx1)/2;
   midy = (pl->scry0+pl->scry1)/2;

   gdk_gc_set_line_attributes (gc, RLINEWID, GDK_LINE_SOLID, GDK_CAP_ROUND, GDK_JOIN_ROUND);

   gdk_draw_line(disp->wid->window, gc, pl->scrx0, pl->scry0, pl->scrx1, pl->scry1);

   gdk_draw_arc(disp->wid->window, gc, TRUE, midx-BULBDIA/2, midy-BULBDIA/2,
      BULBDIA, BULBDIA, 0, 360*64);
      
   gdk_draw_arc(disp->wid->window, gc, TRUE, pl->scrx1-BULBDIA/2, 
      pl->scry1-BULBDIA/2, BULBDIA, BULBDIA, 0, 360*64);
      
   pl->drawn = TRUE;
}


void clear_plotline(displaytype *disp, GdkGC *gc, plottype *pl)
{
   int maxx, minx, maxy, miny;
   guchar *ptr;
   
   minx = MAX(pl->minx, pl->scrx0-BULBDIA/2-1);
   maxx = MIN(pl->maxx-RLINEWID+1, pl->scrx1+BULBDIA/2+1);
   miny = MIN(pl->scry0, pl->scry1);
   maxy = MAX(pl->scry0, pl->scry1);
   miny = MAX(miny - BULBDIA/2 - 1, pl->miny);
   maxy = MIN(maxy + BULBDIA/2 + 1, pl->maxy);

   ptr = disp->buf + disp->width*miny + minx;
   
   gdk_draw_indexed_image(disp->wid->window, disp->wid->style->fg_gc[GTK_STATE_NORMAL],
      minx, miny, maxx-minx+1, maxy-miny+1, GDK_RGB_DITHER_NORMAL, ptr, disp->width, CMap);

   pl->drawn = FALSE;
}


/******* Rubber box control functions *******/

void draw_box(displaytype *disp, GdkGC *gc, boxtype *box)
{
  int x, y, width, height;
  int linewid = RLINEWID;

  /* Set screen coords of inner edge of the line */
  x      = box->scrx0 - 1;
  y      = box->scry0 - 1;
  width  = box->scrx1 - box->scrx0 + 3;
  height = box->scry1 - box->scry0 + 3;

  gdk_gc_set_line_attributes (gc, linewid, GDK_LINE_SOLID, GDK_CAP_ROUND, GDK_JOIN_ROUND);

  gdk_draw_rectangle(disp->wid->window, gc, FALSE, x, y, width, height); 

  if (box->bulb) gdk_draw_arc(disp->wid->window, gc, TRUE,
               x+width-BULBDIA/2-linewid, y+height-BULBDIA/2-linewid,
               BULBDIA, BULBDIA, 0, 360*64);
               
  box->drawn = TRUE;
}

void clear_box(displaytype *disp, GdkGC *gc, boxtype *box)
{
   int putw, puth;
   int maxx, minx, maxy, miny;
   int linewid = RLINEWID;
   guchar *ptr;
   
   if (box->drawn == FALSE) return;

   /* Compute upper & left edges of the lines to be erased */
   /* scrx0, etc. refer to first column & row inside line */
   minx = box->scrx0 - linewid; 
   maxx = box->scrx1 + 1;
   miny = box->scry0 - linewid;
   maxy = box->scry1 + 1;

   puth = maxy-miny+linewid+1;                    /* left vertical */
   ptr = disp->buf + disp->width*miny + minx;
   gdk_draw_indexed_image(disp->wid->window, disp->wid->style->fg_gc[GTK_STATE_NORMAL],
         minx, miny, linewid, puth, GDK_RGB_DITHER_NORMAL, ptr, disp->width, CMap);

   ptr = disp->buf + disp->width*miny + maxx;
   gdk_draw_indexed_image(disp->wid->window, disp->wid->style->fg_gc[GTK_STATE_NORMAL],  /* right */
         maxx, miny, linewid, puth, GDK_RGB_DITHER_NORMAL, ptr, disp->width, CMap);

   putw = maxx-minx+linewid+1;                    /* top horizontal */
   ptr = disp->buf + disp->width*miny + minx;
   gdk_draw_indexed_image(disp->wid->window, disp->wid->style->fg_gc[GTK_STATE_NORMAL], 
         minx, miny, putw, linewid, GDK_RGB_DITHER_NORMAL, ptr, disp->width, CMap);
         
   ptr = disp->buf + disp->width*maxy + minx;
   gdk_draw_indexed_image(disp->wid->window, disp->wid->style->fg_gc[GTK_STATE_NORMAL],   /* bottom */
         minx, maxy, putw, linewid, GDK_RGB_DITHER_NORMAL, ptr, disp->width, CMap);

   if (box->bulb) {
      ptr = disp->buf + disp->width*(maxy-BULBDIA) + maxx-BULBDIA;
      gdk_draw_indexed_image(disp->wid->window, disp->wid->style->fg_gc[GTK_STATE_NORMAL], 
         maxx-BULBDIA, maxy-BULBDIA, BULBDIA*2, BULBDIA*2, GDK_RGB_DITHER_NORMAL,
         ptr, disp->width, CMap);
   }
         
   box->drawn = FALSE;
}

/* Fiducial Line control functions */

void draw_line(displaytype *disp, GdkGC *gc, plottype *pl)
{
   
   gdk_gc_set_line_attributes (gc, 1, GDK_LINE_ON_OFF_DASH, GDK_CAP_ROUND, GDK_JOIN_ROUND);

   gdk_draw_line(disp->wid->window, gc, pl->scrx0, pl->scry0, pl->scrx1, pl->scry1);

   /* I can't figure out how to save and use a font in gc. */
   gdk_draw_text(disp->wid->window, TxtFont, gc, pl->scrx0+pl->labx, pl->scry0+pl->laby,
      pl->label, strlen(pl->label));

}


void clear_line(displaytype *disp, GdkGC *gc, plottype *pl)
{
   int x, y;
   int nxpix, nypix;
   guchar *ptr;

   /* Erase line */
   nxpix = pl->scrx1 - pl->scrx0 + 1;
   nypix = pl->scry1 - pl->scry0 + 1;
   ptr = disp->buf + disp->width*pl->scry0 + pl->scrx0;

   gdk_draw_indexed_image(disp->wid->window, disp->wid->style->fg_gc[GTK_STATE_NORMAL],
       pl->scrx0, pl->scry0, nxpix, nypix, GDK_RGB_DITHER_NORMAL, ptr, disp->width, CMap);

   /* Erase label */
   x = pl->scrx0 + pl->labx;
   y = pl->scry0 + pl->laby - 16;
   nxpix = 10;
   nypix = 17;
   ptr = disp->buf + disp->width*y + x;
   
   gdk_draw_indexed_image(disp->wid->window, disp->wid->style->fg_gc[GTK_STATE_NORMAL],
       x, y, nxpix, nypix, GDK_RGB_DITHER_NORMAL, ptr, disp->width, CMap);
}


/* Crosshair control functions */

void draw_cross(displaytype *disp, GdkGC *gc, plottype *pl)
{
   int crosssize = 34;
   int xl, xr, yt, yb;

   xl = MAX(0, pl->scrx0-crosssize);
   xr = MIN(disp->width-1, pl->scrx0+crosssize);
   yt = MAX(0, pl->scry0-crosssize);
   yb = MIN(disp->height-1, pl->scry0+crosssize);
   
   gdk_gc_set_line_attributes (gc, 1, GDK_LINE_ON_OFF_DASH, GDK_CAP_ROUND, GDK_JOIN_ROUND);
                                               
   gdk_draw_line(disp->wid->window, gc, pl->scrx0, yb, pl->scrx0, yt);
   gdk_draw_line(disp->wid->window, gc, xl, pl->scry0, xr, pl->scry0);

   gdk_draw_text(disp->wid->window, TxtFont, gc, pl->scrx0+pl->labx, yb+pl->laby,
      pl->label, strlen(pl->label));
}

void clear_cross(displaytype *disp, GdkGC *gc, plottype *pl)
{
   int crosssize=34;
   int x, y, nxpix, nypix;
   int xl, xr, yb, yt;
   guchar *ptr;
   
   xl = MAX(0, pl->scrx0-crosssize);
   xr = MIN(disp->width-1, pl->scrx0+crosssize);
   yt = MAX(0, pl->scry0-crosssize);
   yb = MIN(disp->height-1, pl->scry0+crosssize);

   ptr = disp->buf + disp->width*yt + pl->scrx0;
   gdk_draw_indexed_image(disp->wid->window, disp->wid->style->fg_gc[GTK_STATE_NORMAL],
       pl->scrx0, yt, 1, yb-yt+1, GDK_RGB_DITHER_NORMAL, ptr, disp->width, CMap);
       
   ptr = disp->buf + disp->width*pl->scry0 + xl;
   gdk_draw_indexed_image(disp->wid->window, disp->wid->style->fg_gc[GTK_STATE_NORMAL],
       xl, pl->scry0, xr-xl+1, 1, GDK_RGB_DITHER_NORMAL, ptr, disp->width, CMap);

   x = pl->scrx0 + pl->labx;
   y = yb - 16;
   nxpix = 10;
   nypix = 17;
   ptr = disp->buf + disp->width*y + x;
   
   gdk_draw_indexed_image(disp->wid->window, disp->wid->style->fg_gc[GTK_STATE_NORMAL],
       x, y, nxpix, nypix, GDK_RGB_DITHER_NORMAL, ptr, disp->width, CMap);
}


/* Find Data min and max values in histogram */
void hist_minmax (int hist[], int nelements, int *dmin, int *dmax)
{
   int i, nn=nelements-1;

   i = 0;
   while (!hist[i] && i < nn) i++;
   *dmin = i;
   i = nelements-1;
   while (!hist[i] && i > 0) i--;
   *dmax = i;
}

/* Find Top and Bottom Percentage values from histogram */
void hist_bottop (int hist[], int nelements, float frac, int *dbot, int *dtop)
{
   int i, npix, nlimit;
   int nn = nelements-1;

   npix = 0;
   for (i=0; i<nelements; i++) npix += hist[i];
   nlimit = MIN((int)((float)npix * frac), nelements);

   i = npix = 0;
   while (npix < nlimit && i < nn) {npix += hist[i]; i++; }
   *dbot = i-1;

   i = nelements-1; npix = 0;
   while (npix < nlimit && i > 0) {npix += hist[i]; i--; }
   *dtop = i+1;
}


/* Change what's displayed in full and sub displays, used for remote commands */

void set_displaymode(int sigref, int buf)
{
   int sigrefstate[] = {0, 0, 0};
   int bufstate[]  = {0, 0, 0};

   if (DC.sigref_mode == sigref && DC.buf_mode == buf) return;
   DC.sigref_mode = sigref;
   DC.buf_mode = buf;

   /* Toggle SubDisplay controls */
   sigrefstate[sigref] = 1;
   bufstate[buf] = 1;

   gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(GUI.SubSig_rb),  sigrefstate[0]);
   gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(GUI.SubRef_rb),  sigrefstate[1]);
   gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(GUI.SubDif_rb),  sigrefstate[2]);

   gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(GUI.SubSrc_rb),  bufstate[0]);
   gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(GUI.SubBgd_rb),  bufstate[1]);
   gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(GUI.SubSB_rb),   bufstate[2]);

   gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(GUI.FullSig_rb), sigrefstate[0]);
   gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(GUI.FullRef_rb), sigrefstate[1]);
   gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(GUI.FullDif_rb), sigrefstate[2]);

   gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(GUI.FullSrc_rb), bufstate[0]);
   gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(GUI.FullBgd_rb), bufstate[1]);
   gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(GUI.FullSB_rb),  bufstate[2]);

   displayfullimage();
   display_subimage();

}

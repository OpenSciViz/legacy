#ifndef _display_h
#define _display_h

#include <gtk/gtk.h>
#include "image.h"

#define FULLWIN_WIDTH    1024
#define FULLWIN_HEIGHT   1024
#define FULLWIN_XBORDER     0
#define FULLWIN_YBORDER     0
#define SUBWIN_WIDTH      512
#define SUBWIN_HEIGHT     512
#define SUBWIN_XBORDER      0
#define SUBWIN_YBORDER      0
#define RLINEWID            2
#define BULBDIA             8
#define MAXPLANES           8
#define MAXCOLORS         256
#define CBARBUF_WIDTH     140
#define CBARBUF_HEIGHT    280
#define CBAR_X             50  /* Position of ColorBar within Buffer */
#define CBAR_Y             12 
#define CBAR_WIDTH         30
#define CBAR_HEIGHT       256 
#define HIST_X     CBAR_X+CBAR_WIDTH+3  /* Position of Histogram within Buffer */
#define HIST_Y             12
#define HIST_WIDTH         50
#define HIST_HEIGHT  CBAR_HEIGHT
#define NHIST          131072

#define BPMFILE "/.ufcdt/bpm.dat"
#define NSUBIMAGES 3

enum cursor_mode {
   NONE_ANC, ZOOM_ANC, VFID_ANC, HFID_ANC, CROSS_ANC,
   PLOT_ANC, PLOT_BULB, PHOT_ANC, PHOT_BULB, ULIM_ANC, LLIM_ANC
};

enum region_mode {QUADRANT, WHOLE, ZOOMBOX};   

enum plot_mode {NO_PLOT, LINE_PLOT, SPECTRAL_PLOT, RADIAL_PLOT, ENCIRC_PLOT, SURFACE_PLOT};

enum sigref_mode {SIGNAL, REFERENCE, DIFFERENCE};

enum buf_mode {SOURCE, BACKGROUND, SB};

enum stretch_mode {IMAGE, BOX, MANUAL};

typedef struct _dispconttype {
   int cursor_mode, region_mode, plot_mode;
   int sigref_mode, buf_mode;
   int current_sub, flat_divide;
} dispcontroltype;  

typedef struct _displaytype {
   int width, height;          /* size of window in pixels */
   int xborder, yborder;       /* X and Y border widths in pixels */
   int nbytes;                 /* number of bytes in char buffer */
   int startrow, startcol;     /* Start col and row of displayed section of image */
   int ncols, nrows, zoom;     /* Number of displayed det. cols and rows, zoom factor */
   int datamin, datamax, datatop, databot, dispmin, dispmax;
   int stretchmode, bplabel;
   int stretchlo, stretchhi;
   GtkWidget *wid;
   guchar *buf;
} displaytype;

typedef struct _colorbartype {
   int width, height;       /* Number of screen pixels */
   int xborder, yborder;    /* X and Y border widths in pixels */
   int nbytes;              /* Number of bytes in char buffer */
   int bar_x, bar_y;
   int bar_width, bar_height;
   int hist_x, hist_y;
   int hist_width, hist_height;
   GtkWidget *wid;
   guchar *buf;
} colorbartype;

typedef struct {
   int scrx0, scry0, scrx1, scry1;
   int minx, maxx, miny, maxy;
   int width, height;
   int ncols, nrows;
   int bulb, drawn;
} boxtype;

typedef struct {
   int scrx0, scry0, scrx1, scry1;
   int minx, maxx, miny, maxy;
   int width, height;
   int ncols, nrows;
   int bulb, drawn;
   int labx, laby;
   char label[8];
} plottype;

extern int          HistogramData[];
extern int          NColors[2], MinColor[2], MaxColor[2];
extern int          NColors_arg;
extern float        Gamma;

extern double       PhotSum, PhotAvg, PhotSigma;
extern int          PhotMax, PhotMin, PhotTop, PhotBot;
extern int          Cursor_x, Cursor_y;

extern dispcontroltype DC;
extern displaytype  FullDisp, SubDisp;
extern colorbartype CDisp;
extern boxtype      ZoomBox, PhotBox;
extern plottype     PlotLine, StretchULim, StretchLLim;
extern plottype     VerFidA, VerFidB, HorFidA, HorFidB, CrossA, CrossB;

extern guchar       BPM[], HistBuf[];
extern GdkRgbCmap   *CMap;
extern GdkGC        *GC_FD, *GC_SD, *GC_CB;
extern GdkColor     *Green, *White;

/**** Function Prototypes ****/

/* display.c functions */
extern int  init_display(void);
extern void writebpmfile(void);
extern int  readbpmfile(void);
extern void setcolors(int, int, int, float);

extern void draw_plotline  (displaytype *, GdkGC *, plottype *);
extern void clear_plotline (displaytype *, GdkGC *, plottype *);
extern void draw_box       (displaytype *, GdkGC *, boxtype  *);
extern void clear_box      (displaytype *, GdkGC *, boxtype  *);
extern void draw_line      (displaytype *, GdkGC *, plottype *);
extern void clear_line     (displaytype *, GdkGC *, plottype *);
extern void draw_cross     (displaytype *, GdkGC *, plottype *);
extern void clear_cross    (displaytype *, GdkGC *, plottype *);
extern void hist_minmax    (int *, int, int *, int *dmax);
extern void hist_bottop    (int *, int, float frac, int *, int *);
extern void set_displaymode(int, int);

/* fulldisp.c funtions */
extern void zerofullimage(void);
extern int  formatfullimage(void);
extern void scalefullimage(void);
extern void clearfulldisp(void);
extern void putfullimage(void);
extern void displayfullimage(void);
extern void draw_fiducials();
extern void draw_compass(float);


/* subdisp.c functions */

extern void set_scrollbars(int, int, int, int);
extern void update_scrollbars(void);
extern void zero_subimage(void);
extern void clear_subdisp(void);
extern int  format_subimage(void);
extern int  scale_subimage(void);
extern void draw_subimage(void);
extern void bplabel_subimage(void);
extern void display_subimage(void);
extern void refresh_subimage(void);
extern void toggle_badpixel(int, int);
extern void stuff_cbar(void);
extern void stuff_histogram(void);
extern void draw_cbarbuf(void);
extern void draw_colorbar(void);
extern void draw_histogram(void);
extern void draw_histogramlabels(void);
extern void draw_stretchlimitline(plottype *);
extern void clear_stretchlimitline(plottype *);
extern void box_checksubdisp(boxtype *, int);

extern void subdisp_cursor(void);
extern void subdisp_photometry(void);
extern int  compute_bkgd(int, int, int, int);
extern int  compute_centroid(int, int, int, int, int, float*, float*, double*);
extern void subdisp_plot(void);

#endif




/* cmdcodes.h -- Command Definitions for CIRCE */

#define FORE_MOVE             0x00000002
#define AFT_MOVE              0x00000004
#define TAKE_DATA             0x00000100
#define FOI_NULL_CMD          0x10000000
#define INIT_DMA              0x80000000

#define FOI_VERIFY      0xFFFEFDFC
#define MOTOR_STOP      1  
#define INTEG_COMPLETE  2
#define NEWFRAME        3



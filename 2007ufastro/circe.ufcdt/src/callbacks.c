/*------------------------------------------------------------*
 |
 | PROJECT:
 |	Canarias Infrared Camera Experiment (CIRCE)
 |      University of Florida, Gainesville, FL
 |
 | FILE: callbacks.c -- ufCDT callback routines
 |
 | DESCRIPTION:
 |	This source code file contains the callback routines
 |      for widget events in all the ufCDT windows.
 |
 | REVISION HISTORY:
 | Date           By                Description
 | 2005-April-4   A. Marin-Franch   Adapted from xwirc
 |
 *------------------------------------------------------------*/ 

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>
#include <gtk/gtk.h>

#include "main.h"
#include "interface.h"
#include "support.h"
#include "support2.h"
#include "callbacks.h"
#include "cmdcodes.h"
#include "display.h"
#include "fiber.h"
#include "files.h"
#include "fits.h"
#include "image.h"
#include "plot.h"

/* PHARO*/
extern image FullIm, SubIm[];
extern image *FitsDest;


extern time_t StartIntTime;
extern gint IntervalID;
extern pthread_mutex_t VLock;

extern void writebpmfile(void);


/* Utility functions to change FPGA Function button colors when pressed */
void fpgabuttondown (GtkButton *button)
{
/* XtVaSetValues(w, XtNforeground, Fgd_color[GOCOLOR], NULL);
   XtVaSetValues(w, XtNbackground, Bgd_color[GOCOLOR], NULL);
*/
   GUI.Pressed_but = button;
}


void fpgabuttonup (void)
{
   if (GUI.Pressed_but != NULL) {
/*    XtVaSetValues(Pressed_but, XtNbackground, Bgd_color[2], NULL); 
      XtVaSetValues(Pressed_but, XtNforeground, Fgd_color[2], NULL);
*/
      GUI.Pressed_but = NULL;
   }
}


/* Utility function to change button colors and labels */
void button_press (GtkWidget *w, char *labelstr, int color)
{
/* XtVaSetValues(w, XmNlabelString, XmStringCreateLocalized(labelstr), NULL);
   XtVaSetValues(w, XtNbackground, Bgd_color[color], NULL);
   XtVaSetValues(w, XtNforeground, Fgd_color[color], NULL);
*/
}

gint delete_event( GtkWidget *widget,
                   GdkEvent  *event,
                   gpointer   data )
{
    /* If you return FALSE in the "delete_event" signal handler,
     * GTK will emit the "destroy" signal. Returning TRUE means
     * you don't want the window to be destroyed.
     * This is useful for popping up 'are you sure you want to quit?'
     * type dialogs. */

    g_print ("delete event occurred\n");

    /* Change TRUE to FALSE and the main window will be destroyed with
     * a "delete_event". */

    return(TRUE);
}


/***** File Menu *****/

/* Exit ufCDT */
void on_Exit_item_activate( GtkMenuItem *menuitem, gpointer user_data )
{
   logprintf("ufCDT Exiting");

   writebpmfile();

   closelog();

   foi_monitor_cancel();
   if (IC.data_mode == REALDATA) sdvfoi_close();
   else fakedata_cancel();

   gtk_main_quit();
}


/***** Button Bar Callbacks *****/

void on_QuickSrc_but_clicked(GtkButton *button, gpointer user_data)
{
   if (IC.net_connected == FALSE && IC.net_warn == TRUE)
      msgprintf("Warning: No network connection.  AO & TCS header data not available.");

   /* Check status of FPGA before executing */
   if (fpga_status()) {
      fpgabuttondown(button);
      takedata(&SrcImage, SRCDATA, QUICKINT);
   }
}

/***** Message Window Callbacks *****/

void on_MsgOK_but_clicked (GtkButton *button, gpointer user_data)
{
   gtk_widget_hide(GUI.Message_window);
}

/***** Display Callbacks *****/

extern image FullIm, SubIm[];
extern image *FitsDest;

extern time_t StartIntTime;
extern gint IntervalID;
extern pthread_mutex_t VLock;

extern void writebpmfile(void);

int Old_x, Old_y;
plottype *Fiducial;
GtkWidget *Entry1, *Entry2;


/*** Callbacks common to Sub and Full Displays ***/

void on_SigRef_toggled (GtkToggleButton *togglebutton, gpointer user_data)
{
   int origin;    /* = 0 if SubDisp radiobut was clicked, 1 for FullDisp */
   int mode;

   /* Ignore calls when a button is turned off (which happens
      whenever a radio button is toggled). */
   if (gtk_toggle_button_get_active(togglebutton) == FALSE) return;

   mode = atoi(user_data);
   origin = mode / 3;
   mode = mode % 3;
   
   if (mode >= 0 && mode <= 2) DC.sigref_mode = mode;
   else return;

   /* Toggle Sub and FullDisplay controls */
   /* Must be done carefully to avoid infinite loop because this callback is
    * called by set_active()!
    */

   if (origin == 0) {
      switch (mode) {   
      case SIGNAL:     if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(GUI.FullSig_rb)) == FALSE)
                           gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(GUI.FullSig_rb), TRUE);
                       break;
      case REFERENCE:  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(GUI.FullRef_rb)) == FALSE)
                           gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(GUI.FullRef_rb), TRUE);
                       break;
      case DIFFERENCE: if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(GUI.FullDif_rb)) == FALSE)
                           gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(GUI.FullDif_rb), TRUE);
                       break;
      }
   } else {
      switch (mode) {   
      case SIGNAL    : if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(GUI.SubSig_rb)) == FALSE)
                           gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(GUI.SubSig_rb), TRUE);
                       break;
      case REFERENCE : if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(GUI.SubRef_rb)) == FALSE)
                           gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(GUI.SubRef_rb), TRUE);
                       break;
      case DIFFERENCE: if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(GUI.SubDif_rb)) == FALSE)
                           gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(GUI.SubDif_rb), TRUE);
                       break;
      }
   }

   displayfullimage();
   display_subimage();
   update_scrollbars();
}

void on_BufMode_toggled (GtkToggleButton *togglebutton, gpointer user_data)
{
   int origin, mode;

   /* Ignore calls when a button is turned off (which happens
      whenever a radio button is toggled). */
   if (gtk_toggle_button_get_active(togglebutton) == FALSE) return;
   
   mode = atoi(user_data);
   origin = mode / 3;
   mode = mode % 3;
   if (mode >= 0 && mode <= 2) DC.buf_mode = mode;
   else return;

   /* Toggle FullDisplay controls */
   /* Must be done carefully to avoid infinite loop because this callback is
    * called by set_active()!
    */

   if (origin == 0) {
      switch (mode) {   
      case SOURCE    : if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(GUI.FullSrc_rb)) == FALSE)
                           gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(GUI.FullSrc_rb), TRUE);
                       break;
      case BACKGROUND: if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(GUI.FullBgd_rb)) == FALSE)
                           gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(GUI.FullBgd_rb), TRUE);
                       break;
      case SB:         if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(GUI.FullSB_rb)) == FALSE)
                           gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(GUI.FullSB_rb), TRUE);
                       break;
      }
   } else {
      switch (mode) {   
      case SOURCE    : if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(GUI.SubSrc_rb)) == FALSE)
                           gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(GUI.SubSrc_rb), TRUE);
                       break;
      case BACKGROUND: if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(GUI.SubBgd_rb)) == FALSE)
                           gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(GUI.SubBgd_rb), TRUE);
                       break;
      case SB        : if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(GUI.SubSB_rb)) == FALSE)
                           gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(GUI.SubSB_rb), TRUE);
                       break;
      }
   }

   displayfullimage();
   display_subimage();
   update_scrollbars();
}


/*** Full Display Callbacks ***/

void on_FullDisplayMinMax_entry_activate (GtkEditable *editable, gpointer user_data)
{
   gchar *text;
   int value, mode;

   text = gtk_entry_get_text(GTK_ENTRY(editable));
   value = atoi(text);
   mode = atoi(user_data);

   switch (mode) {
      case 0: FullDisp.dispmax = value; break;
      case 1: FullDisp.dispmin = value; break;
      default: return;
   }
   
   FullDisp.stretchmode = 2;

   gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(GUI.FullScale1_rb), FALSE);
   gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(GUI.FullScale2_rb), FALSE);
   gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(GUI.FullScale3_rb), TRUE);

   scalefullimage();
   putfullimage();
   draw_box(&FullDisp, GC_FD, &ZoomBox);
   draw_fiducials();
}


void on_FullScale_rb_toggled (GtkToggleButton *togglebutton,
                              gpointer         user_data)
{
   int mode;

/* Ignore calls when a button is turned off (which happens
   whenever radio box is toggled). */
   if (gtk_toggle_button_get_active(togglebutton) == FALSE) return;

   mode = atoi(user_data);
   if (mode >= 0 || mode <= 2) FullDisp.stretchmode = mode;
   else return;

   if (FullIm.init == TRUE) {
      scalefullimage();
      putfullimage();
      draw_box(&FullDisp, GC_FD, &ZoomBox);
      draw_fiducials();
   }
}


void on_FullZoom_rb_toggled (GtkToggleButton *togglebutton, gpointer user_data)
{
   int width, height, midx, midy;

   /* Ignore calls when a button is turned off (which happens
      whenever radio box is toggled). */
   if (gtk_toggle_button_get_active(togglebutton) == FALSE) return;

   width = height = atoi(user_data);
   
   if ((width != 32 && width != 64 && width != 128 && width != 256 && width != 512)
     || width == ZoomBox.width) return;

   if (FullIm.init == TRUE)
      clear_box(&FullDisp, GC_FD, &ZoomBox);

   ZoomBox.width = ZoomBox.height = ZoomBox.ncols = ZoomBox.nrows = width;

   if (FullIm.init == FALSE) return;
   
   midx = (ZoomBox.scrx0+ZoomBox.scrx1)/2;
   midy = (ZoomBox.scry0+ZoomBox.scry1)/2;
   ZoomBox.scrx0 = midx - width/2 + 1;
   ZoomBox.scry0 = midy - height/2 + 1;
   ZoomBox.scrx1 = ZoomBox.scrx0 + width - 1;
   ZoomBox.scry1 = ZoomBox.scry0 + height - 1;

   /* Make sure new box is entirely within window */
   if (ZoomBox.scrx0 < ZoomBox.minx) {
      ZoomBox.scrx0 = ZoomBox.minx;
      ZoomBox.scrx1 = ZoomBox.scrx0 + ZoomBox.width - 1;
   }
   if (ZoomBox.scry0 < ZoomBox.miny) {
      ZoomBox.scry0 = ZoomBox.miny;
      ZoomBox.scry1 = ZoomBox.miny + ZoomBox.nrows - 1;
   }
   if (ZoomBox.scrx1 > ZoomBox.maxx) {
      ZoomBox.scrx1 = ZoomBox.maxx;
      ZoomBox.scrx0 = ZoomBox.scrx1 - ZoomBox.width + 1;
   }
   if (ZoomBox.scry1 > ZoomBox.maxy) {
      ZoomBox.scry1 = ZoomBox.maxy;
      ZoomBox.scry0 = ZoomBox.scry1 - ZoomBox.width + 1;
   }

  /* Redraw image with new stretch from Zoom Box */
   if (FullDisp.stretchmode == 1) {
      scalefullimage();
      putfullimage();
      draw_box(&FullDisp, GC_FD, &ZoomBox);
      draw_fiducials();
   }

  /* Redraw new box */
   draw_box(&FullDisp, GC_FD, &ZoomBox);

   if (DC.region_mode == ZOOMBOX) {
      SubIm[DC.current_sub].startcol = ZoomBox.scrx0;
      SubIm[DC.current_sub].startrow = FullDisp.height - 1 - ZoomBox.scry1;
      SubDisp.ncols = ZoomBox.ncols;
      SubDisp.nrows = ZoomBox.nrows;
      SubDisp.zoom = SubDisp.width/SubDisp.ncols;
      display_subimage();
      update_scrollbars();
   }
}

void on_FullFiducial_toggled (GtkToggleButton *togglebutton, gpointer user_data)
{
   int index; 
   plottype *fiducial;

   index = atoi(user_data);
   switch(index) {
      case 0: fiducial = &VerFidA; break;
      case 1: fiducial = &VerFidB; break;
      case 2: fiducial = &HorFidA; break;
      case 3: fiducial = &HorFidB; break;
      default: return;
   }
   
   if (gtk_toggle_button_get_active(togglebutton) == TRUE) {
      draw_line (&FullDisp, GC_FD, fiducial);
      fiducial->drawn = TRUE;
   } else {
      clear_line (&FullDisp, GC_FD, fiducial);
      fiducial->drawn = FALSE;
   }
}


void on_FullFiducial_entry_activate (GtkEditable *editable, gpointer user_data)
{
   int value, index, mode;
   gchar *text;
   plottype *fiducial;

   text = gtk_entry_get_text(GTK_ENTRY(editable));
   value = atoi(text);

   index = atoi(user_data);
   switch(index) {
      case 0: fiducial = &VerFidA; mode = 0; break;
      case 1: fiducial = &VerFidB; mode = 0; break;
      case 2: fiducial = &HorFidA; mode = 1; break;
      case 3: fiducial = &HorFidB; mode = 1; break;
      default: return;
   }

   if (fiducial->drawn == TRUE) clear_line (&FullDisp, GC_FD, fiducial);
   
   if (mode == 0) fiducial->scrx0 = fiducial->scrx1 = value;
   else fiducial->scry0 = fiducial->scry1 = DET_NROWS-value;
   
   if (fiducial->drawn == TRUE) draw_line (&FullDisp, GC_FD, fiducial);
}


void on_FullCross_toggled (GtkToggleButton *togglebutton, gpointer user_data)
{
   int index;
   plottype *cross;

   index = atoi(user_data);
   switch(index) {
      case 0: cross = &CrossA; break;
      case 1: cross = &CrossB; break;
      default: return;
   }

   if (gtk_toggle_button_get_active(togglebutton) == TRUE) {
      draw_cross (&FullDisp, GC_FD, cross);
      cross->drawn = TRUE;
   }
   else {
      clear_cross (&FullDisp, GC_FD, cross);
      cross->drawn = FALSE;
   }
}

void on_FullCross_entry_activate (GtkEditable *editable, gpointer user_data)
{
   gchar *text;
   int value, index, mode;
   plottype *cross;

   text = gtk_entry_get_text(GTK_ENTRY(editable));
   value = atoi(text);

   index = atoi(user_data);
   switch(index) {
      case 0: cross = &CrossA; mode = 0; break;
      case 1: cross = &CrossA; mode = 1; break;
      case 2: cross = &CrossB; mode = 0; break;
      case 3: cross = &CrossB; mode = 1; break;
      default: return;
   }

   if (CrossA.drawn == TRUE) clear_cross (&FullDisp, GC_FD, &CrossA);

   if (mode == 0) CrossA.scrx0 = CrossA.scrx1 = value;
   else CrossA.scry0 = CrossA.scry1 =  DET_NROWS-value;
   
   if (CrossA.drawn == TRUE) draw_cross (&FullDisp, GC_FD, &CrossA);
}



/*** FullDisplay Drawing Area Callbacks ***/

gboolean on_FullDisp_drawingarea_expose_event (GtkWidget       *widget,
                                               GdkEventExpose  *event,
                                               gpointer         user_data)
{
   if (!IC.remote) {
      putfullimage();
      draw_box(&FullDisp, GC_FD, &ZoomBox);
      draw_fiducials();
      draw_compass(0.);
   }

   return FALSE;
}


gboolean on_FullDisp_drawingarea_enter_notify_event (GtkWidget        *widget,
                                                     GdkEventCrossing *event,
                                                     gpointer         user_data)
{
  return FALSE;
}


gboolean on_FullDisp_drawingarea_leave_notify_event (GtkWidget        *widget,
                                                     GdkEventCrossing *event,
                                                     gpointer         user_data)
{
  return FALSE;
}


gboolean on_FullDisp_drawingarea_button_press_event (GtkWidget       *widget,
                                                     GdkEventButton  *event,
                                                     gpointer         user_data)
{
   static int thresh = 5;
   
   Old_x = event->x;
   Old_y = event->y;

   if      (VerFidA.drawn && abs(Old_x-VerFidA.scrx0) < thresh) {
      DC.cursor_mode = VFID_ANC; Fiducial = &VerFidA; Entry1 = GUI.FullVerFidA_entry;
   }   
   else if (VerFidB.drawn && abs(Old_x-VerFidB.scrx0) < thresh) {
      DC.cursor_mode = VFID_ANC; Fiducial = &VerFidB; Entry1 = GUI.FullVerFidB_entry;
   }   
   else if (HorFidA.drawn && abs(Old_y-HorFidA.scry0) < thresh) {
      DC.cursor_mode = HFID_ANC; Fiducial = &HorFidA; Entry1 = GUI.FullHorFidA_entry;
   }
   else if (HorFidB.drawn && abs(Old_y-HorFidB.scry0) < thresh) {
      DC.cursor_mode = HFID_ANC; Fiducial = &HorFidB; Entry1 = GUI.FullHorFidB_entry;
   }
   else if (CrossA.drawn  && abs(Old_x-CrossA.scrx0)  < thresh 
                          && abs(Old_y-CrossA.scry0)  < thresh) {
      DC.cursor_mode = CROSS_ANC; Fiducial = &CrossA;
      Entry1 = GUI.FullCrossACol_entry;  Entry2 = GUI.FullCrossARow_entry;
   }
   else if (CrossB.drawn  && abs(Old_x-CrossB.scrx0)  < thresh
                          && abs(Old_y-CrossB.scry0)  < thresh) {
      DC.cursor_mode = CROSS_ANC; Fiducial = &CrossB;
      Entry1 = GUI.FullCrossBCol_entry;  Entry2 = GUI.FullCrossBRow_entry;
   }
   else if (ZoomBox.scrx0 < Old_x && Old_x < ZoomBox.scrx1 &&
            ZoomBox.scry0 < Old_y && Old_y < ZoomBox.scry1) {
      DC.cursor_mode = ZOOM_ANC;
   }
   else DC.cursor_mode = NONE_ANC;

   return FALSE;
}


gboolean on_FullDisp_drawingarea_button_release_event (GtkWidget       *widget,
                                                       GdkEventButton  *event,
                                                       gpointer         user_data)
{

   /* Left button released, Redraw Image and overlays */

   if (DC.cursor_mode != NONE_ANC) {

      if (FullDisp.stretchmode == 1) {
         scalefullimage();
         putfullimage();
      }

      draw_box(&FullDisp, GC_FD, &ZoomBox);
      draw_fiducials();

      if (DC.region_mode == ZOOMBOX) {
         SubIm[DC.current_sub].startcol = ZoomBox.scrx0;
         SubIm[DC.current_sub].startrow = FullDisp.height - 1 - ZoomBox.scry1;
         SubDisp.ncols = ZoomBox.ncols;
         SubDisp.nrows = ZoomBox.nrows;
         SubDisp.zoom = SubDisp.width/SubDisp.ncols;
         display_subimage();
         update_scrollbars();
      }

      DC.cursor_mode = NONE_ANC;
   }
  
  return FALSE;
}


gboolean on_FullDisp_drawingarea_motion_notify_event (GtkWidget       *widget,
                                                      GdkEventMotion  *event,
                                                      gpointer         user_data)
{
   int newx=event->x, newy=event->y;
   int minx = 0, miny = 0;
   int maxx = FullDisp.width - 1;
   int maxy = FullDisp.height - 1;

   /* scrx, scry refer to 1st pixel inside drawn box */
   
   switch(DC.cursor_mode) {

      case NONE_ANC: break;
   
      case ZOOM_ANC:
         clear_box(&FullDisp, GC_FD, &ZoomBox);
         ZoomBox.scrx0 = MAX(ZoomBox.minx, ZoomBox.scrx0 + newx - Old_x);
         ZoomBox.scrx0 = MIN(ZoomBox.maxx - ZoomBox.width + 1, ZoomBox.scrx0);
         ZoomBox.scry0 = MAX(ZoomBox.miny, ZoomBox.scry0 + newy - Old_y);
         ZoomBox.scry0 = MIN(ZoomBox.maxy - ZoomBox.height + 1, ZoomBox.scry0);
         ZoomBox.scrx1 = ZoomBox.scrx0 + ZoomBox.width - 1;
         ZoomBox.scry1 = ZoomBox.scry0 + ZoomBox.height - 1;
         draw_box (&FullDisp, GC_FD, &ZoomBox);
         Old_x = newx;
         Old_y = newy;
         break;

   /* Mouse motion, one of the Fiducial Lines anchored */

      case VFID_ANC:
         clear_line(&FullDisp, GC_FD, Fiducial);
         Fiducial->scrx0 = MAX(minx, Fiducial->scrx0 + newx - Old_x);
         Fiducial->scrx0 = Fiducial->scrx1 = MIN(maxx, Fiducial->scrx0);
         draw_line (&FullDisp, GC_FD, Fiducial);
         Old_x = newx;
         set_entry(Entry1, "%d", Fiducial->scrx0);
         break;
   
      case HFID_ANC:
         clear_line(&FullDisp, GC_FD, Fiducial);
         Fiducial->scry0 = MAX(miny, Fiducial->scry0+newy-Old_y);
         Fiducial->scry0 = Fiducial->scry1 = MIN(maxy, Fiducial->scry0);
         draw_line (&FullDisp, GC_FD, Fiducial);
         Old_y = newy;
         set_entry(Entry1, "%d", maxy-Fiducial->scry0);
         break;
         
      case CROSS_ANC:
         clear_cross(&FullDisp, GC_FD, Fiducial);
         Fiducial->scrx0 = MAX(minx, Fiducial->scrx0+newx-Old_x);
         Fiducial->scrx0 = Fiducial->scrx1 = MIN(maxx, Fiducial->scrx0);
         Fiducial->scry0 = MAX(minx, Fiducial->scry0+newy-Old_y);
         Fiducial->scry0 = Fiducial->scry1 = MIN(maxy, Fiducial->scry0);
         draw_cross (&FullDisp, GC_FD, Fiducial);
         Old_x = newx;
         Old_y = newy;
         set_entry(Entry1, "%d", Fiducial->scrx0);
         set_entry(Entry2, "%d", DET_NROWS-1-Fiducial->scry0);
         break;
         
      default: break;
   }
   
   return FALSE;
}



/*** SubDisplay Callbacks ***/


void on_SubFileRead_but_activate (GtkMenuItem *menuitem, gpointer user_data)
{

/* Pop up FITS file Select dialog */
   gtk_widget_show(GUI.FITSSelect_window);

}


void on_LabelBadPixel_toggled (GtkCheckMenuItem *checkmenuitem, gpointer user_data)
{
   if (checkmenuitem->active) {
      SubDisp.bplabel = TRUE;
      bplabel_subimage();
   }  
   else {
      SubDisp.bplabel = FALSE;
      refresh_subimage();
   }
}


void on_SubFileWriteSub_but_activate (GtkMenuItem *menuitem, gpointer user_data)
{
#ifdef XXX
   int nn, nrecords, saveintmode, savewritemode;
   image *imbuf=NULL;
   char bufname[2][16] = {"Sub Display", "Full Display"};
   char shortname[16];

   nn = (int)client_data;
   switch(nn) {
      case 0 : imbuf = &SubIm[DC.current_sub];
               break;
      case 1 : imbuf = &FullIm;
               break;
      default: return;
   }

   /* Save current integration and write modes, then
    * temporarily switch to the subbuffer write mode (which
    * is needed to set nplanes = 1 in the FITS header).
    */
   saveintmode = IC.int_mode;
   savewritemode = IC.write_mode;
   IC.int_mode = STDINT;
   IC.write_mode = WRITE_SUB;

   /* Create and write the FITS file */
   createFITS(&SubFits, IC.debug);
   nrecords = constructFITSheader(imbuf);
   writeFITSheader(&SubFits, HdrBuf, nrecords);
   writeFITSdata(&SubFits, imbuf->data, imbuf->nbytes, IC.debug);
   closeoutFITS(&SubFits, imbuf->nbytes, IC.debug);

   sprintf(shortname, "%s%03d.%s", SubFits.prefix, SubFits.index, 
      SubFits.suffix);
   logprintf("Wrote %s buffer to %s\n", bufname[nn], shortname);

   /* Reset globals to saved mode */
   IC.int_mode = saveintmode;
   IC.write_mode = savewritemode;
#endif
}

void on_SubFileWriteFull_but_activate (GtkMenuItem *menuitem, gpointer user_data)
{
}

void on_SubBuffer_but_activate (GtkMenuItem *menuitem, gpointer user_data)
{
#ifdef XXX
   int state[3] = {0, 0, 0};
   XmToggleButtonCallbackStruct *call_data = 
      (XmToggleButtonCallbackStruct *) xt_call_data ;

   /* Ignore calls when a button is turned off (which happens
      whenever radio box is toggled). Redundant for these menu toggles? */
   if (call_data->set == False) return;
   DC.current_sub = (int)client_data;

   /* Toggle SubDisplay controls */
   state[DC.current_sub] = 1;
   XmToggleButtonSetState(SubBuf1_rb, state[0], False);
   XmToggleButtonSetState(SubBuf2_rb, state[1], False);
   XmToggleButtonSetState(SubBuf3_rb, state[2], False);


   /* Display the new buffer */
   if (SubIm[DC.current_sub].data != NULL) {
      SubDisp.nrows = SubIm[DC.current_sub].nrows;
      SubDisp.ncols = SubIm[DC.current_sub].ncols;
      SubDisp.zoom = SubDisp.width / SubDisp.ncols;

      subdisp_cursor();
      subdisp_photometry();
      scalesubimage();
      putsubimage();
      puthistogramimage();

      if (DC.region_mode == ZOOMBOX) setscrollbars(0, FullDisp.maxx, 0, FullDisp.maxy);
      else setscrollbars(SubIm[DC.current_sub].startcol, 
                      SubIm[DC.current_sub].startcol+SubDisp.ncols-1,
                      FullDisp.maxy-SubIm[DC.current_sub].startrow-SubDisp.nrows+1, 
                      FullDisp.maxy-SubIm[DC.current_sub].startrow);

      if (SubDisp.bplabel) bplabelsubimage();
      box_checksubdisp(&PhotBox, SubDisp.zoom);
      draw_box(display, GC1, &PhotBox);

      if (DC.plot_mode) {
         draw_plotline(display, GC1, &PlotLine);
         subdisp_plot();
      }
   }
   else clearsubdisp();
#endif
}


void on_SubFlatDivide_but_activate (GtkMenuItem *menuitem, gpointer user_data)
{
   DC.flat_divide = !DC.flat_divide;
}


/* Set plot mode */

void on_SubPlot_rb_activate (GtkMenuItem *menuitem, gpointer user_data)
{

   DC.plot_mode = atoi(user_data);

   /* Update plot display */
   if (DC.plot_mode == 0) {
      plot_clear();
      plot_put();
   }
   else subdisp_plot();

}


/*** Vertical and Horizontal Scrollbars ***/

gboolean on_SubDisp_vscrollbar_motion_notify_event (GtkWidget       *widget,
                                                    GdkEventMotion  *event,
                                                    gpointer         user_data)
{
return(FALSE);
}

gboolean on_SubDisp_hscrollbar_motion_notify_event (GtkWidget       *widget,
                                                    GdkEventMotion  *event,
                                                    gpointer         user_data)
{
return(FALSE);
}

gboolean on_SubDisp_vscrollbar_valuechanged_event (GtkAdjustment   *vadj,
                                                   gpointer        user_data)
{
   image *cs = &SubIm[DC.current_sub];

   if (DC.region_mode == ZOOMBOX) {   /* ZoomBox mode */

      clear_box(&FullDisp, GC_FD, &ZoomBox);
      ZoomBox.scry0 = vadj->value;
      ZoomBox.scry1 = ZoomBox.scry0 + ZoomBox.nrows - 1;
      cs->startrow = DET_NROWS - 1 - vadj->value - cs->nrows + 1; 
      draw_box(&FullDisp, GC_FD, &ZoomBox);

      display_subimage();
      update_scrollbars();
   }

   set_scrollbars(cs->startcol, cs->startcol+SubDisp.ncols-1,
      DET_NROWS-1-cs->startrow-SubDisp.nrows+1,
      DET_NROWS-1-cs->startrow);

  return (TRUE);
}


gboolean on_SubDisp_hscrollbar_valuechanged_event (GtkAdjustment    *hadj,
                                                   gpointer         user_data)
{
   image *cs = &SubIm[DC.current_sub];

   if (DC.region_mode == ZOOMBOX) {    /* Zoom box mode */
      clear_box(&FullDisp, GC_FD, &ZoomBox);
      ZoomBox.scrx0 = cs->startcol = hadj->value;
      ZoomBox.scrx1 = ZoomBox.scrx0 + ZoomBox.ncols - 1;
      draw_box(&FullDisp, GC_FD, &ZoomBox);

      display_subimage();
      update_scrollbars();
   }

   set_scrollbars(cs->startcol, cs->startcol+SubDisp.ncols-1,
      DET_NROWS-1-cs->startrow-SubDisp.nrows+1,
      DET_NROWS-1-cs->startrow);

   return(TRUE);
}


void on_ObjName_entry_activate (GtkEditable *editable, gpointer user_data)
{
   gchar *text;

   text = gtk_entry_get_text(GTK_ENTRY(editable));
   strcpy(Object_Name, text);
}


void on_DetSection_toggled (GtkToggleButton *togglebutton, gpointer user_data)
{
   int dmode;
   static GtkToggleButton *lastbut=NULL;
   image *sim = &SubIm[DC.current_sub];

   if (gtk_toggle_button_get_active(togglebutton) == FALSE) return;

   /* Turn off the previously set button */
   if (lastbut != NULL) gtk_toggle_button_set_active(lastbut, FALSE);
   lastbut = togglebutton;

   /* Return if no data is present */
   if (SubIm[DC.current_sub].data == NULL) return;
   
   dmode = atoi(user_data);

   if (dmode <= 4) {
      SubDisp.ncols = SrcImage.ncols;
      SubDisp.nrows = SrcImage.nrows;
      DC.region_mode = QUADRANT;
   }

   switch(dmode) {
      case 1: sim->startcol = SrcImage.startcol + QUAD_NCOLS;  /* Quad 1 (LR) */
              sim->startrow = SrcImage.startrow;
              break;
      case 2: sim->startcol = SrcImage.startcol;               /* Quad 2 (LL) */
              sim->startrow = SrcImage.startrow;
              break;
      case 3: sim->startcol = SrcImage.startcol;               /* Quad 3 (UL) */
              sim->startrow = SrcImage.startrow + QUAD_NROWS;
              break;
      case 4: sim->startcol = SrcImage.startcol + QUAD_NCOLS;  /* Quad 4 (UR) */
              sim->startrow = SrcImage.startrow + QUAD_NROWS;
              break;
      case 5: sim->startcol = sim->startrow = 0;  /* Whole array */
              SubDisp.ncols = QUAD_NCOLS;
              SubDisp.nrows = QUAD_NROWS;
              DC.region_mode = WHOLE;
              break;
      case 6: sim->startcol = ZoomBox.scrx0;                /* Data in ZoomBox */
              sim->startrow = DET_NROWS - ZoomBox.scry1 - 1;
              SubDisp.ncols = ZoomBox.ncols;
              SubDisp.nrows = ZoomBox.nrows;
              DC.region_mode = ZOOMBOX;
              break;
      default: return;
   }

   SubDisp.zoom = SubDisp.width / SubDisp.ncols;

   if (format_subimage() == FALSE) return;
   subdisp_cursor();
   subdisp_photometry();
   scale_subimage();
   draw_subimage();
   stuff_histogram();
   draw_histogram();
   draw_histogramlabels();
   draw_stretchlimitline(&StretchULim);
   draw_stretchlimitline(&StretchLLim);

   if (SubDisp.bplabel) bplabel_subimage();
   
   draw_box(&SubDisp, GC_SD, &PhotBox);

   if (DC.region_mode == WHOLE) set_scrollbars(0, DET_NCOLS-1, 0, DET_NROWS-1);
   else set_scrollbars(sim->startcol, sim->startcol+SubDisp.ncols-1,
           DET_NROWS-1-sim->startrow-SubDisp.nrows+1,
           DET_NROWS-1-sim->startrow);
           
   if (DC.plot_mode) {
      draw_plotline(&SubDisp, GC_SD, &PlotLine);
      subdisp_plot();
   }
}


/* Stretch Mode */
void on_SubStrMode_toggled (GtkToggleButton *togglebutton, gpointer user_data)
{
   int dmin, dmax;
   int mode;

   /* Ignore calls when a button is turned off (which happens
      whenever radio box is toggled). */
   if (gtk_toggle_button_get_active(togglebutton) == FALSE) return;
   
   mode = atoi(user_data); 
   if (mode >= 0 && mode <= 2) SubDisp.stretchmode = mode;
   else return;

   switch (mode) {

   /* Autostretch on entire displayed image or data in photometry box */
   case 0:
   case 1:

      MinColor[0] = 0;
      MaxColor[0] = NColors[0]-1;
      setcolors(NColors[0], MinColor[0], MaxColor[0], Gamma);

   /* Reset Min and Max sliders */
   /*
      XtVaSetValues(SubMin_sc, XmNvalue, MinColor[0], NULL);
      XtVaSetValues(SubMax_sc, XmNvalue, MaxColor[0], NULL);
      */
      break;
   
   /* Manual Stretch */
   case 2: 
      clear_stretchlimitline(&StretchULim);
      clear_stretchlimitline(&StretchLLim);

   /* Reset the SubDisplay Min and Max used to scale the data (in scale_subimage) */
      dmin = SubDisp.dispmin;
      dmax = SubDisp.dispmax;
      SubDisp.dispmin = dmin+(dmax-dmin)*(float)MinColor[0]/(float)NColors[0];
      SubDisp.dispmax = dmin+(dmax-dmin)*(float)MaxColor[0]/(float)NColors[0];

   /* Reset the colormap */
      MinColor[0] = 0;
      MaxColor[0] = NColors[0]-1;
      setcolors(NColors[0], MinColor[0], MaxColor[0], Gamma
);

   /* Reset Min and Max Stretch marker lines. 
    * They are redrawn in refresh_subimage.
    */
      StretchULim.scry0 = StretchULim.scry1 = StretchULim.miny;
      StretchLLim.scry0 = StretchLLim.scry1 = StretchLLim.maxy;

      break;
   }

   refresh_subimage();
}

void on_SubHiStr_toggled (GtkToggleButton *togglebutton, gpointer user_data)
{
   int mode;
   
   /* Ignore calls when a button is turned off (which happens
      whenever radio box is toggled). */
   if (gtk_toggle_button_get_active(togglebutton) == FALSE) return;
   
   mode = atoi(user_data); 
   if (mode >= 0 && mode <= 1) SubDisp.stretchhi = mode;
   else return;

   refresh_subimage();
}

void on_SubLoStr_toggled (GtkToggleButton *togglebutton, gpointer user_data)
{
   int mode;
   
   /* Ignore calls when a button is turned off (which happens
      whenever radio box is toggled). */
   if (gtk_toggle_button_get_active(togglebutton) == FALSE) return;
   
   mode = atoi(user_data); 
   if (mode >= 0 && mode <= 1) SubDisp.stretchlo = mode;
   else return;

   refresh_subimage();
}


/* Gamma scale bar */

gboolean on_CBar_vscale1_motion_notify_event (GtkWidget       *widget,
                                              GdkEventMotion  *event,
                                              gpointer         user_data)
{
   GtkAdjustment *vadj = GTK_VSCALE(widget)->scale.range.adjustment;

   /* Scale bar value = 0 (top) -> 100 (bot). Gamma will be from 10. to 0.1. */
   Gamma = pow(10., (50. - vadj->value)/50.);
   setcolors(NColors[0], MinColor[0], MaxColor[0], Gamma);

   /* Redraw only the colorbar itself. */
   /* This gives very smooth results so do not change !! */
   draw_colorbar();
   draw_stretchlimitline(&StretchULim);
   draw_stretchlimitline(&StretchLLim);

   /* Redraw the Subdisplay drawing area. */
   draw_subimage();
   if (SubDisp.bplabel) bplabel_subimage();
   draw_box(&SubDisp, GC_SD, &PhotBox);
   if (DC.plot_mode) draw_plotline(&SubDisp, GC_SD, &PlotLine);

   return FALSE;
}



/*** SubDisp Drawing Area Callbacks ***/


gboolean on_SubDisp_drawingarea_expose_event   (GtkWidget       *widget,
                                                GdkEventExpose  *event,
                                                gpointer         user_data)
{
  if (!IC.remote) {
     draw_subimage();
     if (SubDisp.bplabel) bplabel_subimage();
     draw_box(&SubDisp, GC_SD, &PhotBox);
     draw_plotline(&SubDisp, GC_SD, &PlotLine);
  }
  
  return FALSE;
}


gboolean on_SubDisp_drawingarea_enter_notify_event (GtkWidget        *widget,
                                                    GdkEventCrossing *event,
                                                    gpointer         user_data)
{

  return FALSE;
}


gboolean on_SubDisp_drawingarea_leave_notify_event (GtkWidget        *widget,
                                                    GdkEventCrossing *event,
                                                    gpointer         user_data)
{

  return FALSE;
}


gboolean on_SubDisp_drawingarea_button_press_event (GtkWidget       *widget,
                                                    GdkEventButton  *event,
                                                    gpointer        user_data)
{
   int midx, midy;
   int bulbrad = BULBDIA/2;
   
   /* Return if SubImage has not been initialized */
   if (SubIm[DC.current_sub].data == NULL) return(FALSE);

   /* Anchor initial point when button 1 is pressed */

   Old_x = event->x;
   Old_y = event->y;
   midx = (PlotLine.scrx0 + PlotLine.scrx1)/2;
   midy = (PlotLine.scry0 + PlotLine.scry1)/2;

   if (DC.plot_mode && abs(Old_x-midx) < bulbrad && abs(Old_y-midy) < bulbrad) {
      DC.cursor_mode = PLOT_ANC;
   }
   else if (DC.plot_mode && abs(Old_x-PlotLine.scrx1) < bulbrad && 
            abs(Old_y-PlotLine.scry1) < bulbrad) {
      DC.cursor_mode = PLOT_BULB;
   }
   else if (PhotBox.scrx0 < Old_x && Old_x < PhotBox.scrx1 &&
            PhotBox.scry0 < Old_y && Old_y < PhotBox.scry1) {
      DC.cursor_mode = PHOT_ANC;
   } 
   else if (abs(Old_x-PhotBox.scrx1) < bulbrad && 
            abs(Old_y-PhotBox.scry1) < bulbrad) {
      DC.cursor_mode = PHOT_BULB;
   }

   /* Label bad pixels */
/*   else if (!strcmp(args[0], "b2down")) {
      togglebadpixel(newx, newy);
      displaysubimage();
   }
*/
   return FALSE;
}


gboolean on_SubDisp_drawingarea_button_release_event (GtkWidget       *widget,
                                                      GdkEventButton  *event,
                                                      gpointer         user_data)
{

   /* Return if SubImage has not been initialized */
   if (SubIm[DC.current_sub].data == NULL) return(FALSE);

   /* Refresh the bad pixel labels */ 
   if (SubDisp.bplabel) bplabel_subimage();
   
   /* Refesh photometry box, snapped to edges of pixels */
   clear_box(&SubDisp, GC_SD, &PhotBox);
   box_checksubdisp(&PhotBox, SubDisp.zoom);
   draw_box(&SubDisp, GC_SD, &PhotBox);

   /* Refresh plotting */
   draw_plotline(&SubDisp, GC_SD, &PlotLine);
   subdisp_plot();
   
   DC.cursor_mode = NONE_ANC;

   return FALSE;
}


gboolean on_SubDisp_drawingarea_motion_notify_event (GtkWidget       *widget,
                                                     GdkEventMotion  *event,
                                                     gpointer         user_data)
{
   int newx = event->x, newy = event->y;
   int miny, midx, midy;
   int bulbrad = BULBDIA/2;
   
   plottype *pl = &PlotLine;
   boxtype *ph = &PhotBox;
   
   /* Return if SubImage has not been initialized */
   if (SubIm[DC.current_sub].data == NULL) return(FALSE);

   Cursor_x = newx;
   Cursor_y = newy;

   /* Update printed cursor position and value for all motions */
   subdisp_cursor();

   if (DC.cursor_mode == PLOT_ANC || DC.cursor_mode == PLOT_BULB) {
      clear_plotline(&SubDisp, GC_SD, &PlotLine);
      
      if (DC.cursor_mode == PLOT_ANC) {
         pl->scrx0 = MAX(pl->minx, pl->scrx0 + newx - Old_x);
         pl->scrx0 = MIN(pl->maxx - pl->width - 1, pl->scrx0);
         pl->scrx1 = pl->scrx0 + pl->width - 1;

         miny = MIN(pl->scry0, pl->scry1);
         miny = MAX(pl->miny, miny + newy - Old_y);
         miny = MIN(pl->maxy - pl->height, miny);
         if (pl->scry0 <= pl->scry1) {
            pl->scry0 = miny;
            pl->scry1 = pl->scry0 + pl->height - 1;
         }
         else {
            pl->scry1 = miny;
            pl->scry0 = pl->scry1 + pl->height - 1;
         }
      }
      
      else if (DC.cursor_mode == PLOT_BULB) {
         midx = (pl->scrx0 + pl->scrx1)/2;
         midy = (pl->scry0 + pl->scry1)/2;
         pl->scrx0  = MAX(pl->minx, pl->scrx0 - newx + Old_x);
         pl->scrx0  = MIN(pl->scrx0, midx);
         pl->scry0  = MAX(pl->miny, pl->scry0 - newy + Old_y);
         pl->scrx1  = MIN(pl->maxx, pl->scrx1 + newx - Old_x);
         pl->scrx1  = MAX(pl->scrx0, pl->scrx1);
         pl->scry1  = MIN(pl->maxy, pl->scry1 + newy - Old_y);
         pl->width  = pl->scrx1 - pl->scrx0 + 1;
         pl->height = abs(pl->scry1 - pl->scry0) + 1;
         if ((newx - pl->scrx0) < -bulbrad) DC.cursor_mode = NONE_ANC; 
      }

      Old_x = newx;
      Old_y = newy;
   }

   else if (DC.cursor_mode == PHOT_ANC || DC.cursor_mode == PHOT_BULB) {
      clear_box(&SubDisp, GC_SD, &PhotBox);
      if (DC.cursor_mode == PHOT_ANC) {
         ph->scrx0 = MAX(ph->minx, ph->scrx0 + newx - Old_x);
         ph->scrx0 = MIN(ph->maxx - ph->ncols + 1, ph->scrx0);
         ph->scry0 = MAX(ph->miny, ph->scry0 + newy - Old_y);
         ph->scry0 = MIN(ph->maxy - ph->nrows + 1, ph->scry0);
         ph->scrx1 = ph->scrx0 + ph->ncols - 1;
         ph->scry1 = ph->scry0 + ph->nrows - 1;
      }
      else if (DC.cursor_mode == PHOT_BULB) {
         ph->scrx0  = MAX(ph->minx, ph->scrx0 - newx + Old_x);
         ph->scry0  = MAX(ph->miny, ph->scry0 - newy + Old_y);
         ph->scrx1  = MAX(ph->scrx0 + SubDisp.zoom, ph->scrx1);
         ph->scry1  = MAX(ph->scry0 + SubDisp.zoom, ph->scry1);
         ph->scrx1  = MIN(ph->maxx, ph->scrx1 + newx-Old_x);
         ph->scry1  = MIN(ph->maxy, ph->scry1 + newy-Old_y);
         ph->width  = ph->scrx1 - ph->scrx0 + 1;
         ph->height = ph->scry1 - ph->scry0 + 1;
      }
      subdisp_cursor();
      subdisp_photometry();

      Old_x = newx;
      Old_y = newy;
   }

   draw_box(&SubDisp, GC_SD, &PhotBox);
   draw_plotline(&SubDisp, GC_SD, &PlotLine);

   return FALSE;
}



gboolean on_ColorBar_drawingarea_expose_event (GtkWidget       *widget,
                                               GdkEventExpose  *event,
                                               gpointer        user_data)
{
   draw_cbarbuf();
   draw_histogramlabels();
   draw_stretchlimitline(&StretchULim);
   draw_stretchlimitline(&StretchLLim);
   return FALSE;
}


gboolean on_ColorBar_drawingarea_enter_notify_event (GtkWidget       *widget,
                                                     GdkEventCrossing *event,
                                                     gpointer         user_data)
{
  return FALSE;
}


gboolean on_ColorBar_drawingarea_leave_notify_event (GtkWidget       *widget,
                                                     GdkEventCrossing *event,
                                                     gpointer         user_data)
{
  return FALSE;
}


gboolean on_ColorBar_drawingarea_button_press_event (GtkWidget       *widget,
                                                     GdkEventButton  *event,
                                                     gpointer         user_data)
{
   int mindist = 5;
   
   /* Return if SubImage has not been initialized */
   if (SubIm[DC.current_sub].data == NULL) return(FALSE);

   /* Anchor initial point when button 1 is pressed */

   Old_y = event->y;

   if (abs(Old_y-StretchULim.scry1) < mindist) {
      DC.cursor_mode = ULIM_ANC;
      Cursor_y = Old_y;
   }
   else if (abs(Old_y-StretchLLim.scry1) < mindist) {
      DC.cursor_mode = LLIM_ANC;
      Cursor_y = Old_y;
   }

  return FALSE;
}


gboolean on_ColorBar_drawingarea_button_release_event (GtkWidget       *widget,
                                                       GdkEventButton  *event,
                                                       gpointer         user_data)
{
   /* Return if SubImage has not been initialized */
   if (SubIm[DC.current_sub].data == NULL) return(FALSE);

   if (SubDisp.bplabel) bplabel_subimage();

   DC.cursor_mode = NONE_ANC;

   return FALSE;
}


gboolean on_ColorBar_drawingarea_motion_notify_event (GtkWidget       *widget,
                                                      GdkEventMotion  *event,
                                                      gpointer         user_data)
{
   int newy = event->y;

   /* This incredibly clever statement is needed so this function simply
    * returns if there are events pending in the queue.  Events accumulate
    * in the queue when normal mouse movement generates events faster than
    * images can be redrawn.  So, this function will only execute after the
    * queue has been cleared (by successive calls to this function), which
    * greatly reduces the number of image redisplays and increases performance.
    */
   if (gdk_events_pending() == TRUE) return(FALSE);

   /* Return if SubImage has not been initialized */
   if (SubIm[DC.current_sub].data == NULL) return(FALSE);

   /* Must be anchored and must have moved in y to continue */
   if (DC.cursor_mode == NONE_ANC  || newy == Cursor_y) return(FALSE);
   
   Cursor_y = newy;
   
   if (DC.cursor_mode == ULIM_ANC) {
      clear_stretchlimitline(&StretchULim);
      StretchULim.scry0 = MIN(StretchULim.maxy, MAX(StretchULim.miny, newy));
      StretchULim.scry1 = StretchULim.scry0;
   }
   else if (DC.cursor_mode == LLIM_ANC) {
      clear_stretchlimitline(&StretchLLim);
      StretchLLim.scry0 = MIN(StretchLLim.maxy, MAX(StretchLLim.miny, newy));
      StretchLLim.scry1 = StretchLLim.scry0;
   }

   MinColor[0] = (CBAR_HEIGHT - StretchLLim.scry0 + CBAR_Y)*NColors[0]/CBAR_HEIGHT;
   MaxColor[0] = (CBAR_HEIGHT - StretchULim.scry0 + CBAR_Y)*NColors[0]/CBAR_HEIGHT;
   setcolors(NColors[0], MinColor[0], MaxColor[0], Gamma);

   /* Redraw only the colorbar itself and the limit lines */
   /* This gives very smooth results so do not change!! */
   draw_colorbar();
   draw_stretchlimitline(&StretchULim);
   draw_stretchlimitline(&StretchLLim);
   
   /* Redraw the SubDisplay */
   draw_subimage();
   draw_box(&SubDisp, GC_SD, &PhotBox);
   if (DC.plot_mode) draw_plotline(&SubDisp, GC_SD, &PlotLine);

   Old_y = newy;

   return FALSE;
}


/* ReadFITS Dialog Callbacks */

void on_FITSOk_but_clicked (GtkButton *button, gpointer user_data)
{

   int i, status, nhdrlines;
   int naxes, ncols, nrows, nplanes, nbytes;
   char shortname[32];
   char *dest, *ptr;
   gchar *strbuf;

   char *errmsg[] = {"", "Cannot open file", "Bad FITS header", 
                      "Invalid BITPIX value", "Invalid NAXIS values",
                      "FITS file and buffer dimensions don't match",
                      "Cannot read data"};

   strbuf = gtk_file_selection_get_filename(
      GTK_FILE_SELECTION(GUI.FITSSelect_window));

   if (strbuf == NULL) {
      puts("No file selected.");
      return;
   }
   strcpy(shortname, strrchr(strbuf, '/')+1);

   /* Open the FITS file and read the header */
   status = openFITS(strbuf);
   if (!status) status = readFITShdr(HdrBuf, MAXHDRLINES, &nhdrlines);

   /* Extract required header keyword values */
   if (!status) status = getFITSint(HdrBuf, "NAXIS",  &naxes);
   if (!status) {
      if (naxes > 0) status = getFITSint(HdrBuf, "NAXIS1", &ncols);
      else ncols = 1;
   }
   if (!status) {
      if (naxes > 1) status = getFITSint(HdrBuf, "NAXIS2", &nrows);
      else nrows = 1;
   }
   if (!status) {
      if (naxes > 2) status = getFITSint(HdrBuf, "NAXIS3", &nplanes);
      else nplanes = 1;
   }

   /* Get other FITS header values */
   status = getFITSexp(HdrBuf, "CDELT1", &FitsDest->pixelscale);

   nbytes = ncols * nrows * nplanes * 4;
   printf("ncols = %d, %d, %d, %d\n", ncols, nrows, nplanes, nbytes);

   /* Verify size and set up destination for read */
   if (ncols   != FitsDest->ncols || nrows != FitsDest->nrows || 
       nplanes  > FitsDest->nplanes) {
      msgprintf("FITS data size incompatible with buffer.");
      closeFITS();
      return;
   }

   /* If only 4 FITS planes exist, the file contains a "Dif" image.
    * Therefore, read it into the "Signal" section of the buffer
    * and clear the "Reference" section.
    */
   if ((FitsDest == &SrcImage || FitsDest == &BgdImage) && nplanes == 4) {
      dest = ((char *)FitsDest->data) + nbytes;
      ptr = (char *)FitsDest->data;
      for (i=0; i<nbytes; i++) *ptr++ = 0;
   }
   else dest = (char *)(FitsDest->data);

   /* Read the data */
   if (!status) {
      printf("reading fits data\n");
      status = readFITSdata(nhdrlines, nbytes, dest);
   }
   closeFITS();

   /* Update Display */
   if (status == 0) {
      if (FitsDest == &SrcImage) {
         displayfullimage();
         display_subimage();
         update_scrollbars();
         gtk_label_set_text(GTK_LABEL(GUI.SubSrc_label), shortname);
      }
      else if (FitsDest == &BgdImage) {
         displayfullimage();
         display_subimage();
         update_scrollbars();
         gtk_label_set_text(GTK_LABEL(GUI.SubBgd_label), shortname);
      }
   }

   if (status != 0) msgprintf("Error reading %s: %s", shortname, errmsg[status]);
}


void on_FITSCancel_but_clicked (GtkButton       *button,
                                gpointer         user_data)
{
   gtk_widget_hide(GUI.FITSSelect_window);
}

void on_SubRescale_but_clicked (GtkButton       *button,
                                gpointer         user_data)
{

}

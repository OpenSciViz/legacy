#ifndef _image_h
#define _image_h

#include <sys/types.h>
#include <time.h>

#define DET_NCOLS      1024
#define DET_NROWS      1024
#define DET_NPIX       DET_NROWS * DET_NCOLS
#define QUAD_NCOLS      512
#define QUAD_NROWS      512
#define NRAWPLANES        1
#define BYTESPERPIX       2
#define RAWDATASIZE  DET_NPIX * NRAWPLANES * BYTESPERPIX
#define SORTDATASIZE DET_NPIX * 4
#define IMDATASIZE   DET_NPIX * NRAWPLANES * 8
#define DATAINVERT 1
#define MAXHDRLINES  128

/*typedef struct _tcstype {
   int connected;
   char utc[16], lst[16];
   float equinox, ra, dec, ha, ra_offset, dec_offset, ra_rate, dec_rate;
   float airmass, cr_angle, focus_mm, tubelength_mm;
} tcstype;*/

typedef struct _image {
   int init, startcol, startrow, ncols, nrows, nplanes;
   int nquadpixels, npixels, nbytes;
   int datamin, datamax, databot, datatop;
   int nendptframes, npauseframes;
   int t_scan, t_int, t_eff, t_frame, t_framegap, t_shutter_open;
   char mechname[6][20];
   int mechpos[6], mechstep[6];
   int crpix[2];
   float pixelscale;
   char objname[32], ttfname[128];
   time_t start_time;   
/*   tcstype tcs;*/
   int *data;
} image;

typedef struct _arrayclock {
   int ncols, nrows;
   int t_frame, t_framegap;
   int t_scan, t_int, t_eff;
   int t_quickscan, t_quickint, t_quickeff;
   int nendptframes, npauseframes, nquickpauseframes;
   int ncycles;
} arrayclock;

extern image SrcImage, BgdImage, FlatImage, *LastImage;
extern arrayclock ArrayClock;
extern char HdrBuf[][81];

/* functions */
extern int  setimageformat(arrayclock*);
extern void setimageparams(image*, arrayclock*, int);
extern int  constructFITSheader(image*);
extern int  logimage(int, int);
extern void extractDq(image*, int*);
extern void extractWq(int*, unsigned short*, image*);
extern void copyframe(image*, int*, int);
extern void coaddframe(image*, int*, int);
extern void avggroups(image*);
extern void difgroups(int*, image*);
extern void newframe(int);

#endif




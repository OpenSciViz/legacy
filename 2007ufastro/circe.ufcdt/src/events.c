/*------------------------------------------------------------*
 |
 | PROJECT:
 |	Canarias Infrared Camera Experiment (CIRCE)
 |      University of Florida, Gainesville, FL
 |
 | FILE: events.c -- XPHARO Event Handler routines
 |
 | DESCRIPTION
 |	This source code file contains routines which are called
 |	by the X event handler due to gdk_input_add and 
 |      gdk_XXXXXX calls.
 |
 | REVISION HISTORY:
 | Date           By                Description
 | 2005-April-4   A. Marin-Franch   Adapted from xwirc
 |
 *------------------------------------------------------------*/ 

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <pthread.h>
/*#include <network.h>  */
#include <gtk/gtk.h>

#include "main.h"
#include "support2.h"
#include "fiber.h"
#include "cmdcodes.h"
#include "fits.h"
#include "files.h"

extern gint IntervalID;
extern fitsfile MainFits;

/* This routine is called by the X server (due to the gdk_input_add
 * call in main.c) whenever data appear on the pipe FOI_Pipe.  This
 * happens upon the completion of a command by the FPGA (e.g. a motor
 * move or detector integration).  Data are written to the pipe
 * by the routine foi_monitor() in fiber.c which runs in a 2nd thread.
 */
void foi_pipe(int dumarg)
{
   int inbytes;
   int buffer[3];
   unsigned int arg4;

   inbytes = read(FOI_Pipe[0], (char *)buffer, sizeof buffer);

/*   if (IC.debug) fprintf(stderr, "foi_pipe        : Received %d %d %d (%d bytes)\n\n", 
 *     buffer[0], buffer[1], buffer[2], inbytes);
 */

   if (IC.fpga_executing == 0) {
      msgprintf("Warning: Unexpected data received from FOI");
   } 
   else {
      switch (buffer[0]) {

      /* New frame from FPGA.  Write to disk and display as desired. */
      case NEWFRAME:
         newframe(buffer[1]);
         break;

      /* Clean up end of an integration.  Data writing and display still
	 handled by newframe() call above. */
      case INTEG_COMPLETE: 

	/* Send new command to FPGA if in Cont. Acq. */
	 if (IC.int_mode == CONTACQ) {
            if (FOI_NWORDS > 4) arg4 = FOI_ContAcqCmd.i[4];
            else arg4 = 0;
            foi_cmd(FOI_ContAcqCmd.i[1], FOI_ContAcqCmd.i[2], 
               FOI_ContAcqCmd.i[3], arg4);  
         }

	 /* Otherwise integration is done */
         else {
            fprintf(stderr, "\007");      /* Bell */
            fpgabuttonup();               /* Change button color */

            /* Stop the Remaining Int. Time indicator */
            if (IC.interval_active == 1) gtk_timeout_remove(IntervalID);
            IC.interval_active = 0;

            IC.fpga_executing -= TAKE_DATA;

         }
         break;
      }
   }
}


/* Print remaining integration time to screen */
gint intremtime(void)
{
   time_t tloc;
   int trem;
   char strbuf[64];
   static char format[64] = "\0";

   if (format[0] == '\0') 
      sprintf(format, "%%s%%0%dd: %%3d sec left", MainFits.ndigits);

   time(&tloc);
   trem = LastImage->t_scan/1000 - (tloc - LastImage->start_time) + 1;

   if (IC.int_mode == STDINT && IC.write_mode < WRITE_NONE)
      sprintf(strbuf, format, MainFits.prefix, MainFits.index+1, trem);
   else if (IC.int_mode == QUICKINT)
      sprintf(strbuf, "Quick: %3d sec left", trem);
      
   if (IC.interval_active == 0) {
      IntervalID = gtk_timeout_add(1000, (GtkFunction)intremtime, NULL);
      IC.interval_active = 1;
   }

   return(TRUE);      /* return TRUE to keep calling intremtime */
}

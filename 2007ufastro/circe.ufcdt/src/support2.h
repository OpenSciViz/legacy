#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gtk/gtk.h>

typedef struct _pharo_gui {
   GtkWidget *SubDisp_window, *FullDisp_window;
   GtkWidget *Message_window;
   GtkWidget *About_window, *FITSSelect_window, *Plot_window;
   GtkButton *Pressed_but;

   /* Message window */
   GtkWidget *Msg_entry;

   /* SubDisp window */
   GtkWidget *SubDisp_darea, *CDisp_darea;
   GtkWidget *SubDisp_vscrollbar, *SubDisp_hscrollbar, *CBar_vscale1;
   GtkWidget *DetSection5_tb,*DetSection6_tb;
   GtkWidget *SubSig_rb, *SubRef_rb, *SubDif_rb;
   GtkWidget *SubSrc_rb, *SubBgd_rb, *SubSB_rb;
   GtkWidget *SubSrc_label, *SubBgd_label;
   GtkWidget *SubHiStr1_rb, *SubHiStr2_rb, *SubLoStr1_rb, *SubLoStr2_rb;
   GtkWidget *SubStrMode1_rb, *SubStrMode2_rb, *SubStrMode3_rb, *SubStrMode4_rb;
   GtkWidget *SubCursor_txt, *PhotCenter_txt, *PhotSize_txt, *PhotSum_txt, *PhotAvg_txt, *PhotSDev_txt;

   /* Plot window */
   GtkWidget *Plot_darea;
   
   /* FullDisp window */
   GtkWidget *FullDisp_darea, *Compass_darea;
   GtkWidget *FullSig_rb, *FullRef_rb, *FullDif_rb;
   GtkWidget *FullSrc_rb, *FullBgd_rb, *FullSB_rb;
   GtkWidget *FullDataMin_entry, *FullDataMax_entry, *FullDisplayMin_entry, *FullDisplayMax_entry;
   GtkWidget *FullScale1_rb, *FullScale2_rb, *FullScale3_rb;
   GtkWidget *FullZoom1_rb, *FullZoom2_rb, *FullZoom3_rb, *FullZoom4_rb, *FullZoom5_rb;
   GtkWidget *FullVerFidA_tb, *FullVerFidB_tb, *FullHorFidA_tb, *FullHorFidB_tb;
   GtkWidget *FullVerFidA_entry, *FullVerFidB_entry, *FullHorFidA_entry, *FullHorFidB_entry;
   GtkWidget *FullCrossA_tb, *FullCrossB_tb;
   GtkWidget *FullCrossACol_entry, *FullCrossARow_entry, *FullCrossBCol_entry, *FullCrossBRow_entry;
} INST_GUI;

extern INST_GUI GUI;

/* Public functions */

extern void init_gui_struct();
extern void set_label(GtkWidget *label, char *fmt, ...);
extern void set_entry(GtkWidget *entry, char *fmt, ...);


/*------------------------------------------------------------*
 |
 | PROJECT:
 |	Canarias Infrared Camera Experiment (CIRCE)
 |      University of Florida, Gainesville, FL
 |
 | FILE: image.c -- Set up and manipulate image structures
 |
 | DESCRIPTION:
 |      This source code file contains the routines for
 |      managing image arrays, constructing their FITS headers,
 |      and writing to FITS files.
 |
 | REVISION HISTORY:
 | Date           By                Description
 | 2005-April-4   A. Marin-Franch   Adapted from xwirc
 |
 *------------------------------------------------------------*/ 

#include <sys/types.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <gtk/gtk.h>

#include "main.h"
#include "support2.h"
#include "fits.h"
#include "files.h"
#include "display.h"

char HdrBuf[MAXHDRLINES][BUFWIDTH];
char Logstr[128];


extern plottype CrossA;

int setimageformat(arrayclock *ac)
{
   int nplanes, nbytes;
   int startcol, startrow;
   float timescale;

   startcol = startrow = 0;
   nplanes = 8;
   nbytes = ac->ncols * ac->nrows * nplanes * sizeof(int);
  
   if (nbytes > IMDATASIZE) {
      msgprintf("Error: Image format too big");
      return(-1);
   }

   /* Zero the Data arrays */
   memset(SrcImage.data, 0, IMDATASIZE);
   memset(BgdImage.data, 0, IMDATASIZE);

   /* Set structure elements */
   SrcImage.startcol     = BgdImage.startcol     = startcol;
   SrcImage.startrow     = BgdImage.startrow     = startrow;
   SrcImage.ncols        = BgdImage.ncols        = ac->ncols;
   SrcImage.nrows        = BgdImage.nrows        = ac->nrows;
   SrcImage.nplanes      = BgdImage.nplanes      = nplanes;
   SrcImage.nquadpixels  = BgdImage.nquadpixels  = ac->ncols * ac->nrows;
   SrcImage.npixels      = BgdImage.npixels      = ac->ncols * ac->nrows * nplanes;
   SrcImage.nbytes       = BgdImage.nbytes       = nbytes;

   /* Calculate readout time per frame and total int. time in msec */
   /* Formula from B. Pirger, 1998 Aug 17 */
   timescale = 0.00325;  /* phgus217x.ttf */
   /* timescale = 0.00275;    faster54.ttf */
   /* timescale = 0.00240;    faster48.ttf */
   ac->t_frame = timescale * ((16 + 4*ac->ncols) * ac->nrows/2 + 15);

   logprintf("Image Format = %d x %d x %d", ac->ncols, ac->nrows, nplanes);

   return(0);
}


/*
 * Load an image structure with header data from other structures,
 * and with Tel and AO data obtained via ethernet.  This is done at the
 * start of an integration to record the Instrument, Tel, and (for PHARO) 
 * AO status at that time, and to save that information until the FITS file 
 * is written, which may not occur for many seconds depending on the
 * integration time and other parameters.
 */

void setimageparams(image *im, arrayclock *ac, int mode)
{
   char unix_utc_str[16];
   
   im->t_frame        = ac->t_frame;
   im->t_framegap     = ac->t_framegap;
   im->t_shutter_open = 0;

   if (mode == 0) {               /* Normal integration */
      im->t_scan         = ac->t_scan;
      im->t_int          = ac->t_int;
      im->t_eff          = ac->t_eff;
      im->nendptframes   = ac->nendptframes;
      im->npauseframes   = ac->npauseframes;
   } 
   else if (mode == 1) {        /* Quick integration */
      im->t_scan         = ac->t_quickscan;
      im->t_int          = ac->t_quickint;
      im->t_eff          = ac->t_quickeff;
      im->nendptframes   = 1;
      im->npauseframes   = ac->nquickpauseframes;
   }
   
   im->pixelscale = Pixelscale;
   im->crpix[0] = CrossA.scrx0;
   im->crpix[1] = DET_NROWS-1-CrossA.scry0;

   strcpy(im->objname, Object_Name);

   /* Current Time from UNIX */
   time(&im->start_time);
   strftime(unix_utc_str, 16, "%T", gmtime(&im->start_time));

}


/* Construct the FITS header string array */
int constructFITSheader(image *im)
{
   int i, nplanes=0, writemode;
   char *cptr;
   char dateobs[16], timeobs[16];
   char dateobscomment[16], timeobscomment[16];

   /* Get date and time from Unix */
   strftime(dateobs, 16, "%Y-%m-%d", gmtime(&im->start_time));
   strcpy(dateobscomment, "UTC from UNIX");
   strftime(timeobs, 16, "%T", gmtime(&im->start_time));
   strcpy(timeobscomment, "UTC from UNIX");

   writemode = IC.write_mode;
   if (IC.int_mode == STDINT) {
      if (IC.write_mode == WRITE_ALL)  nplanes = 4 * 2 * im->nendptframes;
      else if (IC.write_mode == WRITE_ENDS) nplanes = 8;
      else if (IC.write_mode == WRITE_DIFF) nplanes = 4;
      else if (IC.write_mode == WRITE_SUB)  nplanes = 1;
      else {
          nplanes = 8;
          writemode = WRITE_ENDS;
      }
   } 
   else if (IC.int_mode == QUICKINT || IC.int_mode == CONTACQ) {
      nplanes = 8;
      writemode = WRITE_ENDS;
   }

   /* ****** Write header string array ******* */
   /* === Required Records === */
   i = 0;
   logicalCardImage (HdrBuf[i++], "SIMPLE", TRUE);
   integerCardImage (HdrBuf[i++], "BITPIX", 32);  
   integerCardImage (HdrBuf[i++], "NAXIS",   3);
   integerCardImage (HdrBuf[i++], "NAXIS1", im->ncols);
   integerCardImage (HdrBuf[i++], "NAXIS2", im->nrows);
   integerCardImage (HdrBuf[i++], "NAXIS3", nplanes);

   /* === Observing Information === */
   stringCardImage  (HdrBuf[i++], "OBJECT", Object_Name);
   stringCardImage  (HdrBuf[i++], "ORIGIN", ORIGINVAL);

   stringCardImage  (HdrBuf[i]  , "DATE-OBS", dateobs);
   annotateCardImage(HdrBuf[i++], dateobscomment);
   stringCardImage  (HdrBuf[i  ], "TIME-OBS", timeobs);
   annotateCardImage(HdrBuf[i++], timeobscomment);

   stringCardImage  (HdrBuf[i++], "OBSERVER", Observer_Name);
   stringCardImage  (HdrBuf[i++], "INSTRUME", INSTRUMEVAL);
   stringCardImage  (HdrBuf[i++], "TELESCOP", TELESCOPVAL);

   /* === Instrument Settings === */

   cptr = im->ttfname + strlen(im->ttfname) - 1;
   while (*cptr != '/') cptr--;

   integerCardImage (HdrBuf[i  ], "WRITEMOD", writemode);
   annotateCardImage(HdrBuf[i++], "0=ALL,1=END,2=DIF,3=SUB");
   integerCardImage (HdrBuf[i++], "STARTROW", im->startrow);
   integerCardImage (HdrBuf[i++], "STARTCOL", im->startcol);
   integerCardImage (HdrBuf[i  ], "T_SCAN  ", im->t_scan);
   annotateCardImage(HdrBuf[i++], "Detector total scan time (msec)");
   integerCardImage (HdrBuf[i  ], "T_INT   ", im->t_int);
   annotateCardImage(HdrBuf[i++], "Detector integration time (msec)");
   integerCardImage (HdrBuf[i  ], "T_EFF   ", im->t_eff);
   annotateCardImage(HdrBuf[i++], "Effective integration time (msec)");
   integerCardImage (HdrBuf[i  ], "T_FRAME ", im->t_frame);
   annotateCardImage(HdrBuf[i++], "Frame clocking time");
   integerCardImage (HdrBuf[i  ], "T_FRGAP ", im->t_framegap);
   annotateCardImage(HdrBuf[i++], "Pause time between frames");
   integerCardImage (HdrBuf[i  ], "T_SHUTOP", im->t_shutter_open);
   annotateCardImage(HdrBuf[i++], "Shutter open time");
   integerCardImage (HdrBuf[i  ], "NENDPTFR", im->nendptframes);
   annotateCardImage(HdrBuf[i++], "N End Pt. Frames");
   integerCardImage (HdrBuf[i  ], "NPAUSEFR", im->npauseframes);
   annotateCardImage(HdrBuf[i++], "N Pause Frames");

   stringCardImage (HdrBuf[i  ], "FORE",    im->mechname[0]);
   annotateCardImage(HdrBuf[i++], "Stepper mech names");
   stringCardImage (HdrBuf[i++], "AFT",     im->mechname[1]);

   integerCardImage (HdrBuf[i  ], "FOREPOS", im->mechpos[0]);
   annotateCardImage(HdrBuf[i++], "Stepper mech positions");
   integerCardImage (HdrBuf[i++], "AFTPOS", im->mechpos[1]);

   integerCardImage (HdrBuf[i  ], "FORESTEP", im->mechstep[0]);
   annotateCardImage(HdrBuf[i++], "Stepper mech step numbers");
   integerCardImage (HdrBuf[i++], "AFTSTEP", im->mechstep[1]);

   /* === Coordinate System === */
   stringCardImage  (HdrBuf[i]  , "RADECSYS", "FK4");
   annotateCardImage(HdrBuf[i++], "SYSTEM OF REF. COORD");
   stringCardImage  (HdrBuf[i]  , "CTYPE1  ", "RA---TAN");
   annotateCardImage(HdrBuf[i++], "AXIS TYPE");
   stringCardImage  (HdrBuf[i]  , "CTYPE2  ", "DEC--TAN");
   annotateCardImage(HdrBuf[i++], "AXIS TYPE");
   floatCardImage   (HdrBuf[i]  , "CD001001", 1.0, 5);
   annotateCardImage(HdrBuf[i++], "UNITLESS");
   floatCardImage   (HdrBuf[i]  , "CD002001", 0.0, 5);
   annotateCardImage(HdrBuf[i++], "UNITLESS");
   floatCardImage   (HdrBuf[i]  , "CD001002", 0.0, 5);
   annotateCardImage(HdrBuf[i++], "UNITLESS");
   floatCardImage   (HdrBuf[i]  , "CD002002", 1.0, 5);
   annotateCardImage(HdrBuf[i++], "UNITLESS");
   expCardImage     (HdrBuf[i]  , "CDELT1  ", -im->pixelscale, 3);
   annotateCardImage(HdrBuf[i++], "DEGREES/PIXEL");
   expCardImage     (HdrBuf[i]  , "CDELT2  ", im->pixelscale, 3);
   annotateCardImage(HdrBuf[i++], "DEGREES/PIXEL");
   integerCardImage (HdrBuf[i]  , "CRPIX1  ", im->crpix[0]);
   annotateCardImage(HdrBuf[i++], "REFERENCE PIXEL IN X");
   integerCardImage (HdrBuf[i]  , "CRPIX2  ", im->crpix[1]);
   annotateCardImage(HdrBuf[i++], "REFERENCE PIXEL IN Y");

   /* === END required === */
   blankCard(HdrBuf[i++]);
   strncpy(HdrBuf[i-1],"END",3);

   if (i > MAXHDRLINES) 
      fprintf(stderr, "**** WARNING: Too many lines in header ****\n");

   return(i);

   /* ******** end of header ********* */
}


/* Write image summary to log file */
int logimage(int scantype, int ncycles)
{ 
   char scanstr[4][16] = {"Source", "Background", "Quick Src", "Quick Bgd"};
   char posstr[6][32], padstr[16] = "               ";
   char formatstr[128];
   int j, t_int;

   logprintf("");
   if (ncycles > 1) logprintf("Start %s scan, %d cycles:  Object = %s", 
      scanstr[scantype], ncycles, Object_Name);
   else logprintf("Start %s scan:  Object = %s", scanstr[scantype], Object_Name);

   logprintf(Logstr);

   if (scantype == 2) t_int = ArrayClock.t_quickint;
   else t_int = ArrayClock.t_int;

   j = 0;
   sprintf(&formatstr[0]+j, " %%5d");
   logprintf(formatstr, padstr, posstr[0], padstr,
                        padstr, posstr[1], padstr, 
                        t_int);

   return(0);
}



/* *************************************
    Convert incoming data format to separate quadrants.

    (qn = quadrant n, n=1,2,3,4)
    Given source in format src:
       q1a,q2a,q3a,q4a,q1b,q2b,q3b,q4b,...
    convert it to 
       q1a,q1b,...,q2a,q2b,... ...
    at the dst argument.
*/

void extractWq(int *dst, unsigned short *src, image *im)
{
  int i, j;
  int qL = im->ncols*im->nrows;  /* length of quadrant */
  int *q1, *q2, *q3, *q4;
  int ff = 65535;

  q1 = dst;
  q2 = q1+qL;
  q3 = q2+qL;
  q4 = q3+qL;

  for (j=0; j<im->nrows; j++) {
     for (i=0; i<im->ncols; i++) {
#if DATAINVERT == 1
        *q1++ = ff-(int)*src++;
        *q2++ = ff-(int)*src++;
        *q3++ = ff-(int)*src++;
        *q4++ = ff-(int)*src++;
#else
        *q1++ = (int)*src++;
        *q2++ = (int)*src++;
        *q3++ = (int)*src++;
        *q4++ = (int)*src++;
#endif
     }
  }
}


void copyframe (image *im, int *buf, int subbuf)
{
  int npix;
  size_t nbytes;
  void *ptr1, *ptr2;

  npix = im->ncols*im->nrows*4;
  nbytes = npix*sizeof(int);
  ptr1 = (void *) (im->data + npix * subbuf);
  ptr2 = (void *) buf;

  memcpy(ptr1, ptr2, nbytes);
}


void coaddframe (image *im, int *buf, int subbuf)
{
  int i, npix; 
  int *ptr1, *ptr2;

  npix = im->ncols*im->nrows*4;
  ptr1 = im->data + npix * subbuf;
  ptr2 = buf;

  for (i=0; i<npix; i++) *ptr1++ += *ptr2++;
}


void avggroups (image *im)
{
  int i, npix = im->ncols*im->nrows*4;
  int nf0 = im->nendptframes, nf1 = im->nendptframes;
  int *ptr1, *ptr2;

  ptr1 = im->data;
  ptr2 = im->data + npix;

  for (i=0; i<npix; i++) {
     *ptr1++ /= nf0;
     *ptr2++ /= nf1;
  }
}


void difgroups (int *buf, image *im)
{
  int i, npix = im->ncols*im->nrows*4;
  int *ptr1, *ptr2, *ptr3;

  ptr1 = buf;
  ptr2 = im->data;
  ptr3 = im->data + npix;

  for (i=0; i<npix; i++) {
     *ptr1++ = *ptr3++ - *ptr2++;
  }
}


/* Process file writing and display of new frame */

void newframe(int bufindex)
{
   char bufname[32], shortname[32], formatstr[32];
   int subbuf, nrecords, status;
   int nframes, lastframe, ncycles;
   static int ndatabytes, framecount=0, cyclecount=1;

   /* Clear control variables */
   if (bufindex < 0) {
      framecount = 0;
      cyclecount = 1;
      return;
   }

   nframes = 2 * LastImage->nendptframes;
   lastframe = nframes - 1;
   if (IC.int_mode == STDINT) ncycles = ArrayClock.ncycles;
   else ncycles = 1;

   /* Sort the Raw Data buffer */
   extractWq(Sort_Buf, (unsigned short *)Raw_Buf[bufindex], LastImage);

   /* Copy or coadd sorted buffer into image structure */
   if (framecount < LastImage->nendptframes) subbuf = 0;
   else subbuf = 1;

   if (framecount == 0 || framecount == LastImage->nendptframes)
      copyframe(LastImage, Sort_Buf, subbuf);
   else 
      coaddframe(LastImage, Sort_Buf, subbuf);

   /* Avg frames at end */
   if (framecount == lastframe && nframes > 2) {
      avggroups(LastImage);
   }

   LastImage->init = TRUE;
   
   /***** Write data to FITS as desired ******/

   if (IC.int_mode == STDINT) {         /* Don't auto-write quick ints */
      status = 0;

      if (IC.write_mode == WRITE_ALL) {
         if (framecount == 0) {
            createFITS(&MainFits, IC.debug);
            nrecords = constructFITSheader(LastImage);
            status=writeFITSheader(&MainFits, HdrBuf, nrecords);
            ndatabytes = 0;
         }
         if (status == 0) 
            status = writeFITSdata(&MainFits, Sort_Buf, LastImage->nbytes/2, IC.debug);
         ndatabytes += LastImage->nbytes/2;
      }
      else if (IC.write_mode == WRITE_ENDS && framecount == lastframe) {
         createFITS(&MainFits, IC.debug);
         nrecords = constructFITSheader(LastImage);
         status = writeFITSheader(&MainFits, HdrBuf, nrecords);
         ndatabytes = LastImage->nbytes;
         if (status == 0) 
            status = writeFITSdata(&MainFits, LastImage->data, ndatabytes, IC.debug);
      }
      else if (IC.write_mode == WRITE_DIFF && framecount == lastframe) {
         difgroups(Sort_Buf, LastImage);
         createFITS(&MainFits, IC.debug);
         nrecords = constructFITSheader(LastImage);
         status = writeFITSheader(&MainFits, HdrBuf, nrecords);
         ndatabytes = LastImage->nbytes/2;
         if (status == 0) writeFITSdata(&MainFits, Sort_Buf, ndatabytes, IC.debug);
      }
  
      if (status < 0) msgprintf("WARNING: Error writing FITS file!");
   }

   /* Increment frame counter global variable */
   framecount++;

   /* Last frame of scan */
   if (framecount == nframes) {

      /* Prepare for next scan in multiple cycles */
      if (ncycles > 1) time(&LastImage->start_time);

      /* Clean up */
      framecount = 0;
      if (IC.int_mode != CONTACQ) logprintf("End");

      if (IC.write_mode != WRITE_NONE && IC.int_mode == STDINT) {
         closeoutFITS(&MainFits, ndatabytes, IC.debug);

         sprintf(formatstr, "%%s%%0%dd.%%s", MainFits.ndigits);
         sprintf(shortname, formatstr, MainFits.prefix, MainFits.index, MainFits.suffix);

         if (LastImage == &SrcImage) {
            strcpy(bufname, "Source");
            set_label(GUI.SubSrc_label, "%s", bufname);
         }
         else if (LastImage == &BgdImage) {
            strcpy(bufname, "Background");
            set_label(GUI.SubBgd_label, "%s", bufname);
         }        
         logprintf("Wrote %s buffer to %s", bufname, shortname);
      }
      else {
          if (LastImage == &SrcImage) set_label(GUI.SubSrc_label, "");
          else if (LastImage == &BgdImage) set_label(GUI.SubBgd_label, "");
      }

      if (ncycles > 1) {
         cyclecount++;
         if (cyclecount <= ncycles) logprintf("Cycle %2d", cyclecount);
         else cyclecount = 1;
      }

      /* Display image in full and sub displays */
      displayfullimage();
      display_subimage();
   }
}


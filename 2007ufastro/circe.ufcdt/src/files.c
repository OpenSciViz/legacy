/*  File : files.c
 *  Read and write common structures from and to files.
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdarg.h>
#include <time.h>
#include <string.h>
#include <gtk/gtk.h>

#include "main.h"
#include "interface.h"
#include "support.h"
#include "support2.h"
#include "files.h"

/* Log file */
file_struct Log  = {"", "circe", "log", 0, 2, 99, 0, (FILE*)NULL};

int openlog(void)
{
   char filename[120], format[32];
   time_t tloc;
   char datestr[16];

   sprintf(format, "%%s/%%s.%%s");

   sprintf(filename, format, Log.dir, Log.prefix, Log.suffix);

   if ((Log.fd = fopen(filename, "a")) == NULL) {
      msgprintf("Warning: Can't open log file %s for writing\n", filename);
      return(-1);
   }

   time(&tloc);
   strftime(datestr, 16, "%Y-%m-%d", gmtime(&tloc));
   logprintf("ufCDT startup: %s", datestr);
   logprintf("Data Directory: %s", Data_Dir);
   logprintf("Opened Log File %s.%s", Log.prefix, Log.suffix);

   return(0);
}


int closelog(void)
{

   /* Make sure file is open */
   if (Log.fd == NULL) {
      msgprintf("Warning: Log file already closed\n");
      return(-1);
   }

   logprintf("Closed Log File %s.%s", Log.prefix, Log.suffix);
   logprintf("");
   logprintf("---------------------");
   logprintf("");

   fclose(Log.fd);
   Log.fd = NULL;

   return(0);
}


/* Write to log window and log file using variable argument list */
/* See man page for va_start */

void logprintf(char *fmt, ...)
{
   char msgbuf[256];
   va_list ap;
   time_t tloc;

   /* Put current time in output string */
   time(&tloc);
   strcpy(msgbuf, "\n");
   strftime(msgbuf+1, 10, "%T", gmtime(&tloc));
   sprintf(msgbuf+9, ": ");

   /* Construct remainder of output string */
   va_start(ap, fmt);
   (void) vsprintf(msgbuf+11, fmt, ap);
   va_end(ap);

}


/* Print Messages in popup window using variable argument list */

void msgprintf(char *fmt, ...)
{
   char msgbuf[256];
   va_list ap;

   /* Construct output string */
   va_start(ap, fmt);
   (void) vsprintf(msgbuf, fmt, ap);
   va_end(ap);

   /* Display in Error Message window */
   gtk_entry_set_text (GTK_ENTRY(GUI.Msg_entry), msgbuf);

   gtk_widget_show(GUI.Message_window);
   
   /* Pass string to log file */
   logprintf(msgbuf);
}

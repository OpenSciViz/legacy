#include <gtk/gtk.h>


void
on_Exit_item_activate                  (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_QuickSrc_but_clicked                (GtkButton       *button,
                                        gpointer         user_data);

void
on_SubFileRead_but_activate            (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_SubFileWriteSub_but_activate        (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_SubFileWriteFull_but_activate       (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_LabelBadPixel_toggled               (GtkCheckMenuItem *checkmenuitem,
                                        gpointer         user_data);

void
on_SubBuffer_but_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_SubPlot_rb_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

gboolean
on_SubDisp_vscrollbar_motion_notify_event
                                        (GtkWidget       *widget,
                                        GdkEventMotion  *event,
                                        gpointer         user_data);

gboolean
on_SubDisp_hscrollbar_motion_notify_event
                                        (GtkWidget       *widget,
                                        GdkEventMotion  *event,
                                        gpointer         user_data);

gboolean
on_SubDisp_drawingarea_expose_event    (GtkWidget       *widget,
                                        GdkEventExpose  *event,
                                        gpointer         user_data);

gboolean
on_SubDisp_drawingarea_enter_notify_event
                                        (GtkWidget       *widget,
                                        GdkEventCrossing *event,
                                        gpointer         user_data);

gboolean
on_SubDisp_drawingarea_leave_notify_event
                                        (GtkWidget       *widget,
                                        GdkEventCrossing *event,
                                        gpointer         user_data);

gboolean
on_SubDisp_drawingarea_button_press_event
                                        (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data);

gboolean
on_SubDisp_drawingarea_button_release_event
                                        (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data);

gboolean
on_SubDisp_drawingarea_motion_notify_event
                                        (GtkWidget       *widget,
                                        GdkEventMotion  *event,
                                        gpointer         user_data);

gboolean
on_CBar_vscale1_motion_notify_event    (GtkWidget       *widget,
                                        GdkEventMotion  *event,
                                        gpointer         user_data);

gboolean
on_ColorBar_drawingarea_expose_event   (GtkWidget       *widget,
                                        GdkEventExpose  *event,
                                        gpointer         user_data);

gboolean
on_ColorBar_drawingarea_enter_notify_event
                                        (GtkWidget       *widget,
                                        GdkEventCrossing *event,
                                        gpointer         user_data);

gboolean
on_ColorBar_drawingarea_leave_notify_event
                                        (GtkWidget       *widget,
                                        GdkEventCrossing *event,
                                        gpointer         user_data);

gboolean
on_ColorBar_drawingarea_button_press_event
                                        (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data);

gboolean
on_ColorBar_drawingarea_button_release_event
                                        (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data);

gboolean
on_ColorBar_drawingarea_motion_notify_event
                                        (GtkWidget       *widget,
                                        GdkEventMotion  *event,
                                        gpointer         user_data);

void
on_ObjName_entry_activate              (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_SubStrMode_toggled                  (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_SubHiStr_toggled                    (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_SubLoStr_toggled                    (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_DetSection_toggled                  (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_SigRef_toggled                      (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_BufMode_toggled                     (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_SubRescale_but_clicked              (GtkButton       *button,
                                        gpointer         user_data);

void
on_FullDisplayMinMax_entry_activate    (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_FullScale_rb_toggled                (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_FullZoom_rb_toggled                 (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_FullFiducial_toggled                (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

void
on_FullFiducial_entry_activate         (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_FullCross_entry_activate            (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_FullCross_toggled                   (GtkToggleButton *togglebutton,
                                        gpointer         user_data);

gboolean
on_FullDisp_drawingarea_expose_event   (GtkWidget       *widget,
                                        GdkEventExpose  *event,
                                        gpointer         user_data);

gboolean
on_FullDisp_drawingarea_button_press_event
                                        (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data);

gboolean
on_FullDisp_drawingarea_button_release_event
                                        (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data);

gboolean
on_FullDisp_drawingarea_motion_notify_event
                                        (GtkWidget       *widget,
                                        GdkEventMotion  *event,
                                        gpointer         user_data);

gboolean
on_FullDisp_drawingarea_enter_notify_event
                                        (GtkWidget       *widget,
                                        GdkEventCrossing *event,
                                        gpointer         user_data);

gboolean
on_FullDisp_drawingarea_leave_notify_event
                                        (GtkWidget       *widget,
                                        GdkEventCrossing *event,
                                        gpointer         user_data);

void
on_MsgOK_but_clicked                   (GtkButton       *button,
                                        gpointer         user_data);

void
on_FITSOk_but_clicked                  (GtkButton       *button,
                                        gpointer         user_data);

void
on_FITSCancel_but_clicked              (GtkButton       *button,
                                        gpointer         user_data);

gboolean
on_Plot_darea_expose_event             (GtkWidget       *widget,
                                        GdkEventExpose  *event,
                                        gpointer         user_data);

void
on_SubPlot_rb_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

/*------------------------------------------------------------*
 |
 | PROJECT:
 |	Canarias Infrared Camera Experiment (CIRCE)
 |      University of Florida, Gainesville, FL
 |
 | FILE: fakedata.c -- ufCDT simulated data generation routines.
 |
 | DESCRIPTION:
 |      This source code file contains the routines for
 |      generating simulated data and sending it to the normal
 |      ufCDT data handling routine through a pipe. 
 |
 | REVISION HISTORY:
 | Date           By                Description
 | 2005-April-4   A. Marin-Franch   Adapted from xwirc
 |
 *------------------------------------------------------------*/ 

#include <sys/mman.h>   /* for MAP_..., etc. */
#include <string.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "main.h"
#include "fiber.h"
#include "cmdcodes.h"

extern int NColors;
extern long int Pixels[];
extern unsigned char *FIFO_Base;   /* Points to sdvfoi registers */
extern pthread_t Fake_Data_Thread;
extern int Fake_Pipe_Out[];
extern int Fake_Pipe_In[];


/* Create the fake data mode thread */

int fakedata_init(void)
{
   char *ptr = NULL;

   if (pipe(Fake_Pipe_Out) < 0 || pipe(Fake_Pipe_In) < 0) {
      fprintf(stderr, "fakedata_init Error: Fake Data Pipes not created.\n");
      return(-1);
   }

   pthread_create(&Fake_Data_Thread, NULL, fake_monitor, (void *) ptr);
   return(0);
}


/* Cancel the fake data mode thread */

int fakedata_cancel(void)
{
   int err;

   err = pthread_cancel(Fake_Data_Thread); 
   err = pthread_join(Fake_Data_Thread, NULL);

   close(Fake_Pipe_Out[0]);
   close(Fake_Pipe_Out[1]);
   close(Fake_Pipe_In[0]);
   close(Fake_Pipe_In[1]);

   return(0);
}


/* This routine is run in the fake data thread to simulate the
   response of the Pirger FPGA card over the fiber */

void *fake_monitor(void *arg)
{
   int i, j, npix, nbytes, inbytes;
   int ncols, nrows, nendptframes, npauseframes, ncycles, nframes;
   int icycle, iframe;
   unsigned int checksum;
   foi_packet outbuf;
   foi_packet inbuf1, inbuf2;
   unsigned short ff = 65535;

#if BYTESPERPIX == 2
   unsigned short *dbuf, *ptr, imval, initval, incval;
#elif BYTESPERPIX == 4
   int *dbuf, *ptr, imval, initval, incval;
#endif

   while (1) {
      if (IC.debug) fprintf(stderr, "fake_monitor     : Awaiting a command\n");

      /* Read command from Fake pipe */
      inbytes = read(Fake_Pipe_Out[0], &(inbuf1.c[0]), FOI_NBYTES);

      /* Check number of bytes */
      if (inbytes != FOI_NBYTES ) {
         fprintf(stderr, "fake_monitor Error: read returned with incorrect data\n");
         fprintf(stderr, "nbytes = %d, should be %d\n", inbytes, FOI_NBYTES);
         continue;
      }

      /* Calculate Checksum */
      checksum = 0;
      for (i=0; i<FOI_NBYTES; i++) checksum += (unsigned int)inbuf1.c[i];

      if (IC.debug) {
         fprintf(stderr, 
            "fake_monitor     : Received %08x %08x %08x %08x %08x (hex) from pipe\n",  
            inbuf1.i[0], inbuf1.i[1], inbuf1.i[2], inbuf1.i[3], inbuf1.i[4]);
         fprintf(stderr, "fake_monitor     : Checksum = %04x\n", checksum);
      }

      /* Echo command packet */
      /* inbuf1.i[3] += 1;  Introduce an error */
      write(Fake_Pipe_In[1], &inbuf1.c[0], FOI_NBYTES);

      /* Wait for checksum */
      inbuf2.i[0] = 0;
      inbytes = read(Fake_Pipe_Out[0], &inbuf2.c[0], 2);
      inbuf2.c[2] = inbuf2.c[0];
      inbuf2.c[3] = inbuf2.c[1];
      write(Fake_Pipe_In[1], &inbuf2.c[0], 4);

      /* Execute command if checksum OK */
      inbuf2.i[0] = inbuf2.i[0] >> 16;
      if (inbuf2.i[0] == checksum) {

         if (IC.debug) fprintf(stderr, 
            "fake_monitor     : Checksum %d received, executing command\n", 
            inbuf2.i[0]);

         /* Construct return image for TAKE_DATA command.
          * Avoid using the inbuf1.c because the byte order is different
          * between SPARC and Intel architectures.
          */
         if (inbuf1.i[1] == TAKE_DATA) {

            ncycles = (inbuf1.i[2] >> 24)+1;
            ncols = inbuf1.i[3] >> 24;
            nrows = (inbuf1.i[3] - (ncols<<24)) >> 16;
            ncols = (ncols + 1) * 2;
            nrows = (nrows + 1) * 2;

            /* Note that these next two are fudged for now */
            npauseframes = 0;
            nendptframes = 1;
            
            nframes = nendptframes * 2;

            if (IC.debug) fprintf(stderr, 
               "fake_monitor     : TAKE_DATA received, generating %dx%dx4 image\n", 
               ncols, nrows);

            dbuf = (ushort *) malloc(RAWDATASIZE);

#if DATAINVERT == 1
            initval = ff;
            incval = -1;
#else
            initval = 0;
            incval = 1;
#endif

            for (icycle = 0; icycle < ncycles; icycle++) {
  	      for (iframe=0; iframe<nframes; iframe++) {
                imval = initval;
                ptr = dbuf;
                npix = 0;
                for (i=0; i < nrows; i++) {
     	          for (j=0; j < ncols; j++ ) {
                    *ptr++ = imval;
                    *ptr++ = imval;
                    *ptr++ = imval;
                    *ptr++ = imval;
                    if (iframe == 1) imval += incval;
                    npix += 4;
                  }
		}
                sleep(1);
                nbytes = npix * BYTESPERPIX;
                write(Fake_Pipe_In[1], dbuf, nbytes);
              }
            }
            free(dbuf);
         }

         /* Respond to other commands */
	 else {
            switch(inbuf1.i[1]) {
               case FORE_MOVE    : outbuf.i[0] = MOTOR_STOP; break;
               case AFT_MOVE     : outbuf.i[0] = MOTOR_STOP; break;
            }
            outbuf.i[1] = outbuf.i[2] = outbuf.i[3] = 0;
            
            sleep(2);
            if (IC.debug) fprintf(stderr,
               "fake_monitor     : Execution complete, returning %d %d %d %d %d\n",
                outbuf.i[0], outbuf.i[1], outbuf.i[2], outbuf.i[3], outbuf.i[4]);
            write(Fake_Pipe_In[1], &outbuf.c[0], FOI_NBYTES);
         }
      } /* End if */

      else {
         if (IC.debug) fprintf(stderr, 
            "fake_monitor Error: Checksum doesn't match\n");
      }

   } /* End while */
}

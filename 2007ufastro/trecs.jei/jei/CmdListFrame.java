package ufjei;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;


//===============================================================================
/**
 * Window frame for the recent commands
 */
public class CmdListFrame extends JFrame {
  JScrollPane jScrollPane1 = new JScrollPane();
  JTextArea jTextAreaCmds = new JTextArea();
  String win_name;


//-------------------------------------------------------------------------------
  /**
   *Default Constructor
   */
  public CmdListFrame() {
    try {
      jbInit();
    }
    catch(Exception e) {
      e.printStackTrace();
    }
  } //end of CmdListFrame

  //int i = cmdListFrame.jTextAreaCmds.getLineStartOffset(1);
  //cmdListFrame.jTextAreaCmds.replaceRange("", 0, i);


//-------------------------------------------------------------------------------
  /**
   *Component Initialization
   */
  private void jbInit() throws Exception {
    win_name = "Recent Command History";
    this.setTitle(win_name);
    this.setLocation(jei.get_screen_loc(win_name));
    this.setSize(jei.get_screen_size(win_name));
    addComponentListener(new java.awt.event.ComponentAdapter() {
      public void componentResized(ComponentEvent e) {
        jei.set_screen_loc(win_name, getLocation(), getSize());
      }
    });
    addComponentListener(new java.awt.event.ComponentAdapter() {
      public void componentMoved(ComponentEvent e) {
        jei.set_screen_loc(win_name, getLocation(), getSize());
      }
    });
    ///jTextAreaCmds.setToolTipText("qaz");
    this.getContentPane().add(jScrollPane1, BorderLayout.CENTER);
    jTextAreaCmds.setEditable(false);
    jScrollPane1.getViewport().add(jTextAreaCmds, null);
  } //end of jbInit
} //end of class CmdListFrame
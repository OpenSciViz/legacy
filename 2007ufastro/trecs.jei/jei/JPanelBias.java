package ufjei ;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;
import jca.event.MonitorEvent;
import jca.dbr.DBR_DBString;
import jca.dbr.DBR_DBDouble;
import jca.*;
import jca.Ca;
import jca.event.*;
import javax.swing.SwingConstants;
import javax.swing.border.*;
import java.text.DecimalFormat;

   class JPanelTitle extends JPanel {
       public JPanelTitle() {

	   RatioLayout gridbag = new RatioLayout();
	   setLayout(gridbag);
	   JLabel DLabel = new JLabel("Default",JLabel.CENTER);
	   JLabel CLabel = new JLabel("Last Read",JLabel.CENTER);
	   JLabel NLabel = new JLabel("Name",JLabel.CENTER);	   

	   this.add("0.01,0.01;0.20,0.98",DLabel);
	   this.add("0.23,0.01;0.22,0.98",CLabel);
	   this.add("0.45,0.01;0.50,0.98",NLabel);
       }
   }
//-------------------------------------------------------------------

   class JPanelVoltage extends JPanel
   {  
       public  String Name;
       public  int Register ;
       public  int LatchOrder;
       public  double BiasVoltage ;
       public  double DacScaleFactor ;
       public  int DacOffset;
       public  JTextField Default;
       private  JTextField Current ;
       private  JButton BiasVoltageName ;
	
       public JPanelVoltage(String name ,String register, String latchorder,
			     String biasvoltage, String dacscalefactor, String dacoffset)
       {
	   Name = name ;
	   Register = Integer.parseInt( register );
	   LatchOrder= Integer.parseInt( latchorder );
	   BiasVoltage = Double.valueOf( biasvoltage.trim() ).doubleValue();
	   DacScaleFactor = Double.valueOf( dacscalefactor.trim() ).doubleValue();
	   DacOffset = Integer.parseInt( dacoffset );
	   setLayout(new RatioLayout());
	   Default = new JTextField( biasvoltage, JTextField.CENTER );
	   Default.setToolTipText(biasvoltage);
	   Default.setEditable(false);
	   Default.setBackground(Color.white);
	   Current = new JTextField();
	   Current.setEditable(false);
	   Current.setBackground(Color.white);
	   BiasVoltageName = new JButton(Name);
	   BiasVoltageName.setDefaultCapable(false);
	   BiasVoltageName.setToolTipText(Name);
	   this.add("0.01,0.01;0.2,0.98",Default);
	   this.add("0.23,0.01;0.2,0.98",Current);
	   this.add("0.45,0.01;0.5,0.98",BiasVoltageName);
       }

       public void showVolts( double Voltage ) 
       {
	String volts = Double.toString( Voltage );
	   int ndigits = volts.indexOf(".") + 4;
	   if( ndigits < 4 ) ndigits = 4;
	   if( ndigits > volts.length() ) ndigits = volts.length();
	   Current.setText( volts.substring(0,ndigits) );
       }

       public String getVolts() { return Current.getText(); }

       public String getVdefault() { return Default.getText(); }

       public JButton get_Button() { return BiasVoltageName; }

       public String get_Name() { return Name; }

    } //end of Class JPanelVoltage.
//---------------------------------------------------------------------

    public class JPanelBias  extends JPanel  implements MonitorListener 
    {
	public static final String rcsID = "$Name:  $ $Id: JPanelBias.java,v 0.18 2003/06/13 22:28:14 varosi beta $";
	public static final int NvBias=24; //must be divisible by 3, for equal panels: Left, Middle, Right.
	final private JPanelVoltage jPanelVoltage[];
	private JPanel LeftPanel, MidPanel, RightPanel;
	private JButton readAllButton, resetButton, LatchButton;
	private RatioLayout  JPanelBiasLayout;
	public  JPanel jPanelVoltageSet;
	private JButton jButtonConnect = new JButton("Connect");
	private JButton jButtonCLEAR = new JButton("CLEAR  EPICS");
	private JButton jButtonPassword = new JButton("Send  Password");
	JPasswordField jTextPassword = new JPasswordField();
	public boolean sentPassword = false;
	public JButton powerStatus = new JButton();
	public JButton powerON = new JButton("ON");
	public JButton powerOFF = new JButton("OFF");
	JLabel powerLabel = new JLabel("POWER",JLabel.RIGHT);
	JLabel jLabelPassword = new JLabel("Enter Password ",JLabel.RIGHT);
	public String powerPVname = EPICS.prefix + "dc:DCBiasG.VALC";
	public jeiCmd jeiCommand = new jeiCmd();

	// read_only EPICS info labels ( added at bottom of frame ) 
	EPICSLabel CAR_DCBias = new EPICSLabel(EPICS.prefix + "dc:DCBiasC.VAL","CAR =");
	EPICSLabel errMessCAR = new EPICSLabel(EPICS.prefix + "dc:DCBiasC.OMSS","Message:",jButtonConnect);
	EPICSLabel CAD_DCBias = new EPICSLabel(EPICS.prefix + "dc:BiApply.VAL","CAD =");
	EPICSLabel errMessCAD = new EPICSLabel(EPICS.prefix + "dc:BiApply.MESS","Message:");

	JLabel jLabelWellDepth = new JLabel("Detector Well Depth");
	JLabel jLabelBiasLevel = new JLabel("V_DetGrv Bias Level");

	String [] combobox_items_Well = {"Shallow","Deep"};
	EPICSComboBox jComboBoxWellDepth = new EPICSComboBox(EPICS.prefix + "dc:bivWell.A",
							     EPICS.prefix + "dc:bivWell.VALA",
							     combobox_items_Well, EPICSComboBox.INDEX);

	String [] combobox_items_Bias = {"OFF","Low","Medium","High"};
	EPICSComboBox jComboBoxBiasLevel = new EPICSComboBox(EPICS.prefix + "dc:bivBias.A",
							     EPICS.prefix + "dc:bivBias.VALA",
							     combobox_items_Bias, EPICSComboBox.INDEX);

	EPICSLabel jMonitWellDepth = new EPICSLabel( EPICS.prefix + "dc:DCBiasG.VALI","Well Depth = " );
	EPICSLabel jMonitBiasLevel  = new EPICSLabel( EPICS.prefix + "dc:DCBiasG.VALJ","Bias Level = " );
	private JButton jButtonAPPLY = new JButton("APPLY");

	PV pv;
	Monitor outMon = null;
	int powerState ;

	public JPanelBias()
	{
	    JButton readAllButton = new JButton("Read All");
            readAllButton.setDefaultCapable(false);
	    JButton resetButton = new JButton("Reset MCE defaults");
	    resetButton.setDefaultCapable(false);
	    JButton defaultButton = new JButton("Set to Defaults");
	    defaultButton.setDefaultCapable(false);
	    JButton LatchButton = new JButton("LATCH");
	    LatchButton.setDefaultCapable(false);

	    JPanel  jPanelVoltageSet = new JPanel();
	    jPanelVoltageSet.setBorder(new EtchedBorder(0));
	    final JLabel BiasName = new JLabel("Bias Name",JLabel.CENTER);
	    final JTextField BiasValue = new JTextField(" ");
	    final JTextField BiasIndex = new JTextField("-1");
	    JButton applyBiasVoltageButton = new JButton("APPLY");
	    applyBiasVoltageButton.setDefaultCapable(false);
	    jPanelVoltageSet.setLayout(new GridLayout(1,3));
	    jPanelVoltageSet.add(BiasName);
	    jPanelVoltageSet.add(BiasValue);
	    jPanelVoltageSet.add(applyBiasVoltageButton);
	    jPanelVoltage = new JPanelVoltage[NvBias];

	    setLayout(new RatioLayout());
	    int ixv = 0;
	    String record = null ;

	    //read file of parameters and create buttons for each bias voltage:
	    try	{
		FileReader fin = new FileReader(jei.data_path + "dc_bias_param.txt");
		BufferedReader bin = new BufferedReader(fin);
		LeftPanel = new JPanel();
		LeftPanel.setBorder(new EtchedBorder(0));
		LeftPanel.setLayout(new GridLayout(9,1));
		LeftPanel.add(new JPanelTitle());
		record = bin.readLine();
		while( record.indexOf("V_") < 0 ) record = bin.readLine(); //read until first voltage.	 

		for(int i=1 ; i <= NvBias/3; i++) 
		    {
			StringTokenizer tokens = new StringTokenizer(record);
			jPanelVoltage[ixv] = new JPanelVoltage(tokens.nextToken(),tokens.nextToken(),
							       tokens.nextToken(),tokens.nextToken(),
							       tokens.nextToken(),tokens.nextToken());
			final int index = ixv;

			jPanelVoltage[ixv].get_Button().addActionListener( new ActionListener() {
				public void actionPerformed(ActionEvent e) {
				    BiasName.setText(jPanelVoltage[index].Name); 
				    BiasIndex.setText(Integer.toString(jPanelVoltage[index].Register));
				    BiasValue.setText(jPanelVoltage[index].getVolts());
				}
			    } );
		      
			LeftPanel.add(jPanelVoltage[ixv++]);
			record =  bin.readLine();
		    }
		    
		MidPanel = new JPanel();
		MidPanel.setBorder(new EtchedBorder(0));
		MidPanel.setLayout(new GridLayout(9,1));
		MidPanel.add(new JPanelTitle());
		    
		for(int i=1 ; i <= NvBias/3; i++) 
		    {
			StringTokenizer tokens = new StringTokenizer(record);
			jPanelVoltage[ixv] = new JPanelVoltage(tokens.nextToken(),tokens.nextToken(),
							       tokens.nextToken(),tokens.nextToken(),
							       tokens.nextToken(),tokens.nextToken());
			final int index = ixv;

			jPanelVoltage[ixv].get_Button().addActionListener ( new ActionListener() {
				public void actionPerformed(ActionEvent e) {
				    BiasName.setText(jPanelVoltage[index].Name); 
				    BiasIndex.setText(Integer.toString(jPanelVoltage[index].Register));
				    BiasValue.setText(jPanelVoltage[index].getVolts());
				}
			    } );

			MidPanel.add(jPanelVoltage[ixv++]);
			record =  bin.readLine();
		    }
		    
		RightPanel = new JPanel();
		RightPanel.setBorder(new EtchedBorder(0));
		RightPanel.setLayout(new GridLayout(9,1));
		RightPanel.add(new JPanelTitle());

		for(int i=1 ; i <= NvBias/3; i++) 
		    {
			StringTokenizer tokens = new StringTokenizer(record); 
			jPanelVoltage[ixv] = new JPanelVoltage(tokens.nextToken(),tokens.nextToken(),
							       tokens.nextToken(),tokens.nextToken(),
							       tokens.nextToken(),tokens.nextToken());
			final int index = ixv;

			jPanelVoltage[ixv].get_Button().addActionListener ( new ActionListener() {
				public void actionPerformed(ActionEvent e) {
				    BiasName.setText(jPanelVoltage[index].Name); 
				    BiasIndex.setText(Integer.toString(jPanelVoltage[index].Register));
				    BiasValue.setText(jPanelVoltage[index].getVolts());
				}
			    } );

			RightPanel.add(jPanelVoltage[ixv++]);
			record =  bin.readLine();
		    }
		bin.close();  //done reading file and creating buttons for each bias voltage.
	    }
	    catch(Exception e)
		{ JOptionPane.showMessageDialog(null,e,"Error",JOptionPane.ERROR_MESSAGE); }    
	    
	    applyBiasVoltageButton.addActionListener( new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
			jeiCommand.execute("PUT " + EPICS.prefix + "dc:biSet.A " + BiasIndex.getText() );
			jeiCommand.execute("PUT " + EPICS.prefix + "dc:biSet.B " + BiasValue.getText() );
			jeiCommand.execute("PUT " + EPICS.prefix + "dc:BiApply.DIR START");
		    }
		} );

  	    resetButton.addActionListener( new ActionListener() {
		    public void actionPerformed( ActionEvent e) { resetMCE_action(); } } );

  	    defaultButton.addActionListener( new ActionListener() {
		    public void actionPerformed( ActionEvent e) { defaultVoltages_action(); } } );

	    readAllButton.addActionListener( new ActionListener() {
		    public void actionPerformed( ActionEvent e) { readAll_action(); } } );					    
	    jButtonConnect.addActionListener( new ActionListener() {
		    public void actionPerformed(ActionEvent e) { jButtonConnect_action(e); } } );

	    jButtonPassword.addActionListener( new ActionListener() {
		    public void actionPerformed(ActionEvent e) { jButtonPassword_action(e); } } );

	    LatchButton.addActionListener( new ActionListener() {
		    public void actionPerformed(ActionEvent e) { LatchButton_action(e); } } );

	    powerON.addActionListener( new ActionListener() { 
		    public void actionPerformed(ActionEvent e) { powerON_action(e); } } );

	    powerOFF.addActionListener( new ActionListener() { 
		    public void actionPerformed(ActionEvent e) { powerOFF_action(e); } } );

	    jButtonCLEAR.addActionListener( new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
			jeiCommand.execute("PUT " + EPICS.prefix + "dc:BiApply.DIR CLEAR"); 
		    }
		} );

	    jButtonAPPLY.addActionListener( new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
			jeiCommand.execute("PUT " + EPICS.prefix + "dc:BiApply.DIR START");
		    }
		} );

	    try { // set up monitor of the Bias Gensub output for Power On/Off .
		pv = new PV(powerPVname);
		Ca.pendIO(jeiFrame.TIMEOUT);
		outMon =  pv.addMonitor(new DBR_DBString(),this,null,Monitor.VALUE);
		Ca.pendIO(jeiFrame.TIMEOUT);
	    }
	    catch (Exception e)
		{ jeiError.show("error setting up monitor " + powerPVname + " " + e.toString()); }

	    this.add("0.02,0.03;0.15,0.06",jButtonConnect) ;
	    this.add("0.22,0.03;0.15,0.06",jButtonCLEAR) ;
	    this.add("0.45,0.01;0.15,0.06",jLabelWellDepth);
	    this.add("0.45,0.07;0.15,0.06",jLabelBiasLevel);
	    this.add("0.45,0.13;0.15,0.06",jButtonAPPLY) ;
	    this.add("0.60,0.01;0.15,0.06",jComboBoxWellDepth);
	    this.add("0.60,0.07;0.15,0.06",jComboBoxBiasLevel);
	    this.add("0.77,0.01;0.23,0.06",jMonitWellDepth);
	    this.add("0.77,0.07;0.23,0.06",jMonitBiasLevel);
	    this.add("0.01,0.12;0.13,0.06",jLabelPassword);
	    this.add("0.14,0.12;0.10,0.06",jTextPassword);
	    this.add("0.24,0.12;0.16,0.06",jButtonPassword) ;
	    this.add("0.01,0.2;0.32,0.55",LeftPanel);
	    this.add("0.34,0.2;0.32,0.55",MidPanel);
	    this.add("0.67,0.2;0.32,0.55",RightPanel);
	    this.add("0.01,0.8;0.45,0.07",jPanelVoltageSet);
	    this.add("0.01,0.9;0.15,0.07",readAllButton);
	    this.add("0.17,0.9;0.15,0.07",resetButton);
	    this.add("0.33,0.9;0.15,0.07",defaultButton);
	    this.add("0.50,0.87;0.10,0.06",CAD_DCBias);
	    this.add("0.60,0.87;0.40,0.06",errMessCAD);
	    this.add("0.50,0.93;0.10,0.06",CAR_DCBias);
	    this.add("0.60,0.93;0.40,0.06",errMessCAR);
	    this.add("0.50,0.8;0.12,0.07",LatchButton);
	    this.add("0.60,0.8;0.10,0.07",powerLabel);
	    this.add("0.70,0.8;0.05,0.07",powerStatus);
	    this.add("0.76,0.8;0.09,0.07",powerON);
	    this.add("0.86,0.8;0.09,0.07",powerOFF);

	    checkPowerStatus();
	    checkConnection(true);

	} //end of method JPanelBias().

//-------------------------------------------------------------------------------
  /**
   * Event Handling Method
   *@param me MonitorEvent: TBD
   */
	public void monitorChanged(MonitorEvent me) {
   	    try {
		if( me.pv().name().equals(powerPVname) ) 
		    {
			checkPowerStatus();
		    }
	    } catch (Exception e) {
		jeiError.show("error processing monitorChanged: "+e.toString()+"; "+me.pv().name());
	    }
	}// end of monitorChanged
    
//-------------------------------------------------------------------------------

	void checkPowerStatus()
	{
	    String value = EPICS.get( powerPVname );

	    if( value.trim().toUpperCase().equals("ON") ) {
		powerState = 1;
		powerStatus.setBackground(Color.green);
	    }
	    else if( value.trim().toUpperCase().equals("OFF") ) {
		powerState = 0;
		powerStatus.setBackground(Color.red);
	    } else {
		powerState = 2;
		powerStatus.setBackground(Color.yellow);
	    }
	}
//-------------------------------------------------------------------------------

	/**
	 * JPanelBias#Connect button action performed
	 * Sends commands to EPICS dc:bias records
	 *  directing EPICS gensubs to connect via sockets to the Detector Control Agent,
	 *  allowing direct engineering access to detector control bias voltage parameters.
	 *@param e not used
	 */

	void jButtonConnect_action(ActionEvent e)
	{
	    jButtonConnect.setBackground(Color.yellow);
	    jButtonConnect.setForeground(Color.black);
	    jButtonConnect.setText("Connecting");
	    jeiCommand.execute("PUT " + EPICS.prefix + "dc:biInit.A 0" );
	    jeiCommand.execute("PUT " + EPICS.prefix + "dc:BiApply.DIR START");
	    try { Thread.sleep(1000);} catch ( Exception _e) {}
	    checkConnection(true);
	}
//-------------------------------------------------------------------------------

	/**
	 * JPanelBias#Password button action performed
	 *@param e not used
	 */

	void jButtonPassword_action(ActionEvent e)
	{
	    String put = "PUT " + EPICS.prefix;
	    jeiCommand.execute( put + "dc:biPassword.A " + new String(jTextPassword.getPassword()) );
	    jeiCommand.execute( put + "dc:BiApply.DIR  START");
	    jTextPassword.setText("");
	    sentPassword = true;
	}
//-------------------------------------------------------------------------------

	void checkConnection( boolean indicateOK )
	{
	    String socbias = EPICS.get(EPICS.prefix + "dc:DCBiasG.VALB"); 
	    
	    if( socbias.equals("-1") || 
		checkforBadSocket(errMessCAR.getText()) )
		{
		    jButtonConnect.setBackground(Color.red);
		    jButtonConnect.setForeground(Color.white);
		    jButtonConnect.setText("Connect");
		}
	    else if( indicateOK ) {
		jButtonConnect.setBackground(Color.green);
		jButtonConnect.setForeground(Color.black);
		jButtonConnect.setText("Connected");
	    }
	}
//-------------------------------------------------------------------------------

	boolean checkforBadSocket( String EPICSmessage )
	{
	    if( EPICSmessage.toUpperCase().indexOf("BAD SOCKET") >= 0 ||
		EPICSmessage.toUpperCase().indexOf("TIMED OUT") > 0   ||
		EPICSmessage.toUpperCase().indexOf("ERROR CONNECTING") >= 0 )
		return true;
	    else
		return false;
	}
//-------------------------------------------------------------------------------

        void resetMCE_action()  
        { 
	    int no = JOptionPane.showConfirmDialog(this,"Reset Bias Voltages to MCE defaults ?","Warning",
						  JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
	    if( no != 1 ) {
		jeiCommand.execute("PUT " + EPICS.prefix + "dc:biPark.A 1");
		jeiCommand.execute("PUT " + EPICS.prefix + "dc:BiApply.DIR START");
	    }
	}
//-------------------------------------------------------------------------------

        void defaultVoltages_action()  
        { 
	    String put = "PUT " + EPICS.prefix;

	    if( !sentPassword ) {
		JOptionPane.showMessageDialog(this, "Must first Send Password to MCE.",
					      "ERROR", JOptionPane.ERROR_MESSAGE);
		return;
	    }

	    int no = JOptionPane.showConfirmDialog(this,"Set Bias Voltages to Defaults shown?","Warning",
						  JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
	    if( no != 1 ) {
		jeiCommand.execute("PUT " + EPICS.prefix + "dc:biDatum.A 1");
		jeiCommand.execute("PUT " + EPICS.prefix + "dc:BiApply.DIR START");
	    }
	}
//-------------------------------------------------------------------------------

        void readAll_action()  
	{ 
	    jeiCommand.execute("PUT " + EPICS.prefix + "dc:biReadAll.A 1" );
	    jeiCommand.execute("PUT " + EPICS.prefix + "dc:BiApply.DIR START");

	    try { Thread.sleep(3500);} catch ( Exception _e) {}
	    PV pv = new PV(EPICS.prefix + "dc:DCBiasG.VALF"); 

	    try { Ca.pendIO(10.0); } catch (jca.TimeOutException x) { System.out.println(x); } ;
	    DBR_DBDouble pv_value = new DBR_DBDouble( pv.elementCount() );

	    try {
		try {
		    try {
			pv.get( pv_value );
		    } catch (jca.GetFailException gf) { System.out.println(gf); };
		} catch (jca.BadCountException bc) { System.out.println(bc);  };
	    } catch (jca.BadTypeException c) { System.out.println(c); };

	    try { Ca.pendIO(10.0); } catch (jca.TimeOutException x) { System.out.println(x); };
						   
	    for(int i = 0 ; i < NvBias ; i++)
		jPanelVoltage[i].showVolts( pv_value.valueAt(i) );

	} //end of readAll_action

//-------------------------------------------------------------------------------

        void LatchButton_action( ActionEvent e )  
        { 
	    jeiCommand.execute("PUT " + EPICS.prefix + "dc:biLatch.A 1");
	    jeiCommand.execute("PUT " + EPICS.prefix + "dc:BiApply.DIR START");
	}
//-------------------------------------------------------------------------------

        void powerON_action( ActionEvent e) 
        {
	    jeiCommand.execute("PUT " + EPICS.prefix + "dc:biPower.A on");
	    jeiCommand.execute("PUT " + EPICS.prefix + "dc:BiApply.DIR START");
	}
//-------------------------------------------------------------------------------

        void powerOFF_action( ActionEvent e) 
        {
	    jeiCommand.execute("PUT " + EPICS.prefix + "dc:biPower.A off");
	    jeiCommand.execute("PUT " + EPICS.prefix + "dc:BiApply.DIR START");
	}

} //end of Class JPanelBias.


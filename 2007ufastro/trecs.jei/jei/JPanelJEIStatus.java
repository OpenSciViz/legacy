package ufjei;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.border.*;


//===============================================================================
/**
 * Status panel
 */
public class JPanelJEIStatus extends JPanel {
  public static final String rcsID = "$Name:  $ $Id: JPanelJEIStatus.java,v 0.0 2002/06/03 17:42:30 hon beta $";
  JPanel northPanel = new JPanel();
  JPanel southPanel = new JPanel();
  JPanel jPanel1 = new JPanel();
  JPanel jPanel2 = new JPanel();
  JPanel jPanel3 = new JPanel();
  JPanel jPanel4 = new JPanel();
  JPanel jPanel5 = new JPanel();
  JTextField statusText = new JTextField("",5);
  JTextField dataText = new JTextField("",5);
  JTextField tempText = new JTextField("",5);
  JLabel statusLabel = new JLabel("Status:");
  JLabel dataLabel = new JLabel("Data (Mb):");
  JLabel tempLabel = new JLabel("Temp (K):");
  JLabel obsTimeLabel = new JLabel("Obs. Time (min):");
  JTextField obsTimeText = new JTextField("",5);
  JLabel timeToGoLabel = new JLabel("----Time To Go----");
  JLabel minLabel = new JLabel("min:");
  JLabel secLabel = new JLabel("sec:");
  JTextField minText = new JTextField("",5);
  JTextField secText = new JTextField("",5);


//-------------------------------------------------------------------------------
  /**
   *Default constructor
   */
  public JPanelJEIStatus () {
    this.setLayout(new BorderLayout());
    northPanel.setLayout(new GridLayout(1,0,10,10));
    southPanel.setLayout(new FlowLayout());

    jPanel1.setLayout(new FlowLayout());
    jPanel2.setLayout(new GridLayout(0,2,10,10));

    jPanel1.add(statusLabel);
    jPanel1.add(statusText);
    jPanel1.add(dataLabel);
    jPanel1.add(dataText);
    jPanel1.add(tempLabel);
    jPanel1.add(tempText);
    jPanel2.add(obsTimeLabel);
    jPanel2.add(obsTimeText);
    jPanel2.add(timeToGoLabel);
    jPanel2.add(minLabel);
    jPanel2.add(minText);
    jPanel2.add(secLabel);
    jPanel2.add(secText);
    jPanel1.setBorder(new EtchedBorder(0));
    jPanel2.setBorder(new EtchedBorder(0));
    jPanel3.setBorder(new EtchedBorder(0));
    jPanel4.setBorder(new EtchedBorder(0));
    jPanel5.setBorder(new EtchedBorder(0));

    northPanel.add (jPanel1);
    northPanel.add (jPanel2);
    northPanel.add (jPanel3);
    northPanel.add (jPanel4);
    northPanel.add (jPanel5);
    southPanel.add (new JTextField("",30));
    this.add (northPanel,BorderLayout.CENTER);
    this.add (southPanel,BorderLayout.SOUTH);
  } //end of JPanelJEIStatus

} //end of class JPanelJEIStatus

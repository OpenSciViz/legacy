package ufjei;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import jca.*;

//===============================================================================
/**
 *
 */

public class JPanelSystem extends JPanel {
 public static final String rcsID = "$Name:  $ $Id: JPanelSystem.java,v 0.6 2003/10/24 21:19:42 varosi beta $";
    JPanel jPanelAgents = new JPanel();
    JPanel jPanelPower = new JPanel();   
    GridLayout gridLayoutAgents = new GridLayout(9,2);
    GridLayout gridLayoutPower = new GridLayout(9,2);

  JPanel jPanelOptions = new JPanel();
  GridLayout gridLayOptions = new GridLayout();
  JLabel jLabel1 = new JLabel("debug");
  JLabel jLabel2 = new JLabel("init");
  JLabel jLabel3 = new JLabel("set DHS info");
  EPICSTextField jTextField1 = new EPICSTextField( EPICS.prefix + "setDhsInfo.A",
						   EPICS.prefix + "setDhsInfo.VALA",
						   "DHS quick-look stream ID");
  JComboBox jComboBox1 = new JComboBox();
  JComboBox jComboBox2 = new JComboBox();
//  EPICSComboBox jComboBox1 = new EPICSComboBox(EPICS.prefix + "debug.A", EPICS.prefix + "debug.VALA");
//  EPICSComboBox jComboBox2 = new EPICSComboBox(EPICS.prefix + "init.A", EPICS.prefix + "init.VALA");

  JPanel jPanelButtons = new JPanel();
  JButton jButtonSetWCS = new JButton("set WCS");
  JButton jButtonReboot = new JButton("REBOOT  Epics and all Device Agents");
  JButton jButtonApply = new JButton("APPLY");
  GridLayout gridLayButtons = new GridLayout();

  JButton jButtonViewCmds = new JButton("View Recent Commands");
  JButton jButtonViewErrorWin = new JButton("View Error Window");
  public jeiCmd jeiCommand = new jeiCmd();

//-------------------------------------------------------------------------------
  /**
   *Construct the frame
   */
  public JPanelSystem() {
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    try  {
      jbInit();
    }
    catch(Exception e) {
      e.printStackTrace();
    }
  } //end of JPanelSystem


//-------------------------------------------------------------------------------
  /**
   *Component initialization
   */
  private void jbInit() throws Exception  {

    setLayout(new RatioLayout());
    jPanelOptions.setBorder(BorderFactory.createEtchedBorder());
    jPanelOptions.setBounds(new Rectangle(19, 34, 286, 186));
    jPanelOptions.setLayout(gridLayOptions);
    gridLayOptions.setColumns(2);
    gridLayOptions.setRows(0);
    jPanelButtons.setBorder(BorderFactory.createEtchedBorder());
    jPanelButtons.setBounds(new Rectangle(19, 34, 286, 186));
    jPanelButtons.setLayout(gridLayButtons);
    gridLayButtons.setColumns(1);
    gridLayButtons.setRows(0);
    jComboBox1.addItem("");
    jComboBox1.addItem("NONE");
    jComboBox1.addItem("MIN");
    jComboBox1.addItem("FULL");
    jComboBox2.addItem("");
    jComboBox2.addItem("NONE");
    jComboBox2.addItem("VSM");
    jComboBox2.addItem("FAST");
    jComboBox2.addItem("FULL");
    jTextField1.setText("");

    jButtonSetWCS.addActionListener(new java.awt.event.ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		String cmd = "PUT " + EPICS.prefix + "setWcs.DIR MARK ; setWcs";
		jeiCommand.execute(cmd);
	    }
	});

    jButtonReboot.setBackground(Color.red);
    jButtonReboot.setForeground(Color.white);

    jButtonReboot.addActionListener( new java.awt.event.ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		if( JOptionPane.showConfirmDialog(null,"Are you sure you want to REBOOT?")
		    == JOptionPane.YES_OPTION )
		    {
			String cmd = "PUT " + EPICS.prefix + "apply.DIR CLEAR";
			jeiCommand.execute(cmd);
			cmd = "PUT " + EPICS.prefix + "reboot.DIR MARK ; reboot";
			jeiCommand.execute(cmd);
		    }
	    }
	});

    jButtonApply.addActionListener(new java.awt.event.ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		String cmd = "PUT " + EPICS.prefix + "apply.DIR START ; apply";
		jeiCommand.execute(cmd);
	    }
	});

    jButtonViewCmds.setBounds(new Rectangle(312, 34, 177, 25));
    jButtonViewCmds.addActionListener(new java.awt.event.ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		String cmd = "PUSH View Recent Commands";
		jeiCommand.execute(cmd);
	    }
	});

    jButtonViewErrorWin.setBounds(new Rectangle(312, 65, 177, 25));
    jButtonViewErrorWin.addActionListener(new java.awt.event.ActionListener() {
	    public void actionPerformed(ActionEvent e) { jeiError.show(); } });

    this.add(".02,.05;.25,.20",jPanelOptions);
    jPanelOptions.add(jLabel1, null);
    jPanelOptions.add(jComboBox1, null);
    jPanelOptions.add(jLabel2, null);
    jPanelOptions.add(jComboBox2, null);
    jPanelOptions.add(jLabel3, null);
    jPanelOptions.add(jTextField1, null);

    this.add(".01,.30;.27,.30",jPanelButtons);
    jPanelButtons.add(jButtonSetWCS, null);
    jPanelButtons.add(jButtonReboot, null);
    jPanelButtons.add(jButtonApply, null);

    this.add(".02,.80;.25,.07",jButtonViewCmds);
    this.add(".02,.90;.25,.07",jButtonViewErrorWin);

    String[] Power_Status  = { "","POWEROFF" , "POWERON" } ;
    String[] Agent_Modes   = { "","SIMAGENTS", "STOPAGENTS", "STARTAGENTS" } ;
    
    JLabel Alldevices    = new JLabel("All Devices");
    JLabel Annex         = new JLabel("Annex");
    JLabel Lakeshore218  = new JLabel("Lakeshore 218");
    JLabel Lakeshore340  = new JLabel("Lakeshore 340");
    JLabel Vacuum        = new JLabel("Vacuum Sensor");
    JLabel Indexors      = new JLabel("Motor Indexors");
    JLabel CryoCooler    = new JLabel("Cryo-Cooler");
    JLabel ppcVME        = new JLabel("ppcVME (Epics)");
    JLabel MCE4          = new JLabel("MCE-4");

    JLabel Allagents     = new JLabel("All Agents");
    JLabel executive     = new JLabel("Executive");   
    JLabel ufacqframed   = new JLabel("Data Acquis. Server");
    JLabel ufgdhsd       = new JLabel("DHS Client");
    JLabel ufgls218      = new JLabel("Temperature Monitor");
    JLabel ufgls340d     = new JLabel("Temperature Control");
    JLabel ufg354vacd    = new JLabel("Pressure Monitor");
    JLabel ufgmotord     = new JLabel("Motor Control");
    JLabel ufgmce4d      = new JLabel("Detector Control");

    EPICSComboBox jCombAnnex           = new EPICSComboBox(EPICS.prefix + "reboot.C",
							   EPICS.prefix + "rebootG.VALC",      
							   Power_Status,EPICSComboBox.ITEM);
    EPICSComboBox jCombLakeshore218    = new EPICSComboBox(EPICS.prefix + "reboot.D",
							   EPICS.prefix + "rebootG.VALD",	      
							   Power_Status,EPICSComboBox.ITEM);
    EPICSComboBox jCombLakeshore340    = new EPICSComboBox(EPICS.prefix + "reboot.E",
							   EPICS.prefix + "rebootG.VALE",	      
							   Power_Status,EPICSComboBox.ITEM);
    EPICSComboBox jCombVacuum          = new EPICSComboBox(EPICS.prefix + "reboot.F",
							   EPICS.prefix + "rebootG.VALF",	      
							   Power_Status,EPICSComboBox.ITEM);
    EPICSComboBox jCombIndexors        = new EPICSComboBox(EPICS.prefix + "reboot.G",
							   EPICS.prefix + "rebootG.VALG",
							   Power_Status,EPICSComboBox.ITEM);
    EPICSComboBox jCombCryoCooler      = new EPICSComboBox(EPICS.prefix + "reboot.H",
							   EPICS.prefix + "rebootG.VALH",
							   Power_Status,EPICSComboBox.ITEM);
    EPICSComboBox jCombppcVME          = new EPICSComboBox(EPICS.prefix + "reboot.I",
							   EPICS.prefix + "rebootG.VALI",
							   Power_Status,EPICSComboBox.ITEM);
    EPICSComboBox jCombMCE4            = new EPICSComboBox(EPICS.prefix + "reboot.J",
							   EPICS.prefix + "rebootG.VALJ",    
							   Power_Status,EPICSComboBox.ITEM);

    EPICSComboBox jCombexecutive       = new EPICSComboBox(EPICS.prefix + "reboot.L",
							   EPICS.prefix + "rebootG.VALK",
							   Agent_Modes,EPICSComboBox.ITEM);
    EPICSComboBox jCombufacqframed     = new EPICSComboBox(EPICS.prefix + "reboot.M",
							   EPICS.prefix + "rebootG.VALL",
							   Agent_Modes,EPICSComboBox.ITEM);
    EPICSComboBox jCombufgdhsd         = new EPICSComboBox(EPICS.prefix + "reboot.N",
							   EPICS.prefix + "rebootG.VALM",
							   Agent_Modes,EPICSComboBox.ITEM);
    EPICSComboBox jCombufgls218d        = new EPICSComboBox(EPICS.prefix + "reboot.O",
							   EPICS.prefix + "rebootG.VALN",
							   Agent_Modes,EPICSComboBox.ITEM);
    EPICSComboBox jCombufgls340d       = new EPICSComboBox(EPICS.prefix + "reboot.P",
							   EPICS.prefix + "rebootG.VALO",
							   Agent_Modes,EPICSComboBox.ITEM);
    EPICSComboBox jCombufg354vacd      = new EPICSComboBox(EPICS.prefix + "reboot.Q",
							   EPICS.prefix + "rebootG.VALP",
							   Agent_Modes,EPICSComboBox.ITEM);
    EPICSComboBox jCombufgmotord       = new EPICSComboBox(EPICS.prefix + "reboot.R",
							   EPICS.prefix + "rebootG.VALQ",
							   Agent_Modes,EPICSComboBox.ITEM);
    EPICSComboBox jCombufgmce4d        = new EPICSComboBox(EPICS.prefix + "reboot.S",
							   EPICS.prefix + "rebootG.VALR",
							   Agent_Modes,EPICSComboBox.ITEM);
    jPanelAgents.setLayout(gridLayoutAgents);
    jPanelPower.setLayout(gridLayoutPower);

    this.add("0.75,0.05;0.20,0.05",new JLabel("Power On/Off"));
    this.add("0.70,0.10;0.27,0.50",jPanelPower);
    jPanelPower.add( new JLabel("Device") );
    jPanelPower.add( new JLabel("Status") );
    jPanelPower.add(Annex);
    jPanelPower.add(jCombAnnex);
    jPanelPower.add(CryoCooler);
    jPanelPower.add(jCombCryoCooler);
    jPanelPower.add(ppcVME);
    jPanelPower.add(jCombppcVME);
    jPanelPower.add(Lakeshore218);
    jPanelPower.add(jCombLakeshore218);
    jPanelPower.add(Lakeshore340);
    jPanelPower.add(jCombLakeshore340);
    jPanelPower.add(Vacuum);
    jPanelPower.add(jCombVacuum);
    jPanelPower.add(Indexors);
    jPanelPower.add(jCombIndexors);
    jPanelPower.add(MCE4);
    jPanelPower.add(jCombMCE4);

    this.add("0.40,0.05;0.20,0.05",new JLabel("Device Agents") );
    this.add("0.35,0.10;0.27,0.50",jPanelAgents);
    jPanelAgents.add( new JLabel("Name") );
    jPanelAgents.add( new JLabel("Desired Mode") );
    jPanelAgents.add(executive);
    jPanelAgents.add(jCombexecutive);
    jPanelAgents.add(ufacqframed);
    jPanelAgents.add(jCombufacqframed);
    jPanelAgents.add(ufgdhsd);
    jPanelAgents.add(jCombufgdhsd);
    jPanelAgents.add(ufgls218);
    jPanelAgents.add(jCombufgls218d);
    jPanelAgents.add(ufgls340d);
    jPanelAgents.add(jCombufgls340d);
    jPanelAgents.add(ufg354vacd);
    jPanelAgents.add(jCombufg354vacd);
    jPanelAgents.add(ufgmotord);
    jPanelAgents.add(jCombufgmotord);
    jPanelAgents.add(ufgmce4d);
    jPanelAgents.add(jCombufgmce4d);

    EPICSTextField agentsHostIPaddr = new EPICSTextField(EPICS.prefix + "sad:agenthostIP",
							 "IP address of host running agents", true);
    this.add("0.30,0.7;0.15,0.05", new JLabel("Agents Host IP addr:") );   
    this.add("0.45,0.7;0.20,0.05", agentsHostIPaddr );

    EPICSLabel monitorDatumCnt = new EPICSLabel(EPICS.prefix + "sad:DatumCnt","Datum Count =");
    this.add("0.35,0.85;0.20,0.05", monitorDatumCnt );
  }
} //end of class JPanelSystem

package ufjei;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;


/**
 *Title:      T-ReCS Java Engineering Interface
 *Version:
 *Copyright:  Copyright (c) 1999
 *Author:     Thomas Kisko
 *Company:    University of Florida
 *Description:T-ReCS is the Mid IR instrument for the Gemini South telescope. This is the T-ReCS Java Engineering Interface.
 */


//===============================================================================
/**
 * Window to display "About" contents
 */
public class jeiFrameAboutBox extends JDialog implements ActionListener {
  public static final String rcsID = "$Name:  $ $Id: jeiFrameAboutBox.java,v 0.0 2002/06/03 17:44:36 hon beta $";

  JPanel panel1 = new JPanel();
  JPanel panel2 = new JPanel();
  JPanel insetsPanel1 = new JPanel();
  JPanel insetsPanel2 = new JPanel();
  JPanel insetsPanel3 = new JPanel();
  JButton button1 = new JButton();
  JLabel imageControl1 = new JLabel();
  ImageIcon imageIcon;
  BorderLayout borderLayout1 = new BorderLayout();
  BorderLayout borderLayout2 = new BorderLayout();
  FlowLayout flowLayout1 = new FlowLayout();
  FlowLayout flowLayout2 = new FlowLayout();
  GridLayout gridLayout1 = new GridLayout();
  String product = "T-ReCS Java Engineering Interface";
  String copyright = "Copyright (c) 1999";
  String comments = "T-ReCS is the Mid IR instrument for the Gemini South telescope. This is the Java Engineering Interface.";
  String authors = "Written by Tom Kisko, Latif Albusairi, David Rashkin, Frank Varosi, and Ziad Saleh";


//-------------------------------------------------------------------------------
  /**
   *Constructor from a given parent frame
   *@param parent Frame: parent frame
   */
  public jeiFrameAboutBox(Frame parent) {
    super(parent);
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    try  {
      jbInit();
    }
    catch(Exception e) {
      e.printStackTrace();
    }
    pack();
  } //end of jeiFrameAboutBox


//-------------------------------------------------------------------------------
  /**
   *Overriden so that we can close on system exit
   *@param e TBD
   */
  protected void processWindowEvent(WindowEvent e) {
    super.processWindowEvent(e);
    if(e.getID() == WindowEvent.WINDOW_CLOSING) {
      cancel();
    }
  } //end of processWindowEvent


//-------------------------------------------------------------------------------
  /**
   *Component initialization
   */
  private void jbInit() throws Exception  {
    //imageIcon = new ImageIcon(getClass().getResource("your image name goes here"));
    this.setTitle("About");
    setResizable(false);
    panel1.setLayout(new BorderLayout());
    panel2.setLayout(new BorderLayout());
    insetsPanel1.setLayout(new FlowLayout());
    insetsPanel2.setLayout(new FlowLayout());
    insetsPanel3.setLayout(new GridLayout(5,1));
    insetsPanel2.setBorder(new EmptyBorder(10, 10, 10, 10));
    insetsPanel3.setBorder(new EmptyBorder(10, 60, 10, 10));
    button1.setText("OK");
    button1.addActionListener(this);
    insetsPanel2.add(imageControl1, null);
    panel2.add(insetsPanel2, BorderLayout.WEST);
    insetsPanel3.add(new JLabel(product), null);
    insetsPanel3.add(new JLabel("Version " + version.version), null);
    insetsPanel3.add(new JLabel(copyright), null);
    insetsPanel3.add(new JLabel(comments), null);
    insetsPanel3.add(new JLabel(authors), null);
    panel2.add(insetsPanel3, BorderLayout.CENTER);
    insetsPanel1.add(button1, null);
    panel1.add(panel2, BorderLayout.NORTH);
    panel1.add(insetsPanel3, BorderLayout.CENTER);
    panel1.add(insetsPanel1, BorderLayout.SOUTH);
    this.getContentPane().add(panel1, null);
  } //end of jbInit


//-------------------------------------------------------------------------------
  /**
   *method to dispose (close) this dialog window
   */
  void cancel() {
    dispose();
  } //end of cancel


//-------------------------------------------------------------------------------
  /**
   *ActionPerformed method calls the 'cancel' method when the button is pressed.
   *@param e TBD
   */
  public void actionPerformed(ActionEvent e) {
    if(e.getSource() == button1) {
      cancel();
    }

  } //end of actionPerformed

} //end of class jeiFrameAboutBox


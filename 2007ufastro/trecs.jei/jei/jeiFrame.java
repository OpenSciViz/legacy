package ufjei;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.lang.Integer;
import java.io.* ;
import java.util.* ;
import java.net.*;
import java.text.* ;
import ufjei.epicsSim;

/**
 *Title:      T-ReCS Java Engineering Interface
 *Version:
 *Copyright:  Copyright (c) 1999
 *Author:     David Rashkin, Thomas Kisko, Frank Varosi, and Ziad Saleh.
 *Company:    University of Florida
 *Description:T-ReCS is the Mid IR instrument for the Gemini South telescope.
 *This is the T-ReCS Java Engineering Interface.
 */

//===============================================================================
/**
 * The main frame
 */
public class jeiFrame extends JFrame {
  public static final String rcsID = "$Name:  $ $Id: jeiFrame.java,v 0.8 2003/05/09 21:17:20 varosi beta $";
  public static final double TIMEOUT = 5.0 ;
  public static jeiMotorParameters jeiMotorParameters ;
  public static jeiOpath Opathframe = null;

  JMenuBar menuBar1 ;
  JMenu menuFile ;
  JMenuItem menuFileExit  ;
  JMenuItem menuFileEdit ;
  JMenuItem menuFileUpdateRecList ;
  JMenu menuHelp  ;
  JMenuItem menuHelpAbout  ;
  JMenuItem menuHelpEdit  ;
  public static JLabel statusBar  ;
  BorderLayout borderLayout1 ;
  JTabbedPane jTabbedPane  ;
  public static JPanelMotorParameters jPanelParameters  ;
  public static JPanelMotorLowLevel jPanelLowLevel  ;
  public static JPanelMotorHighLevel jPanelHighLevel  ;
  public static JPanelTemperature jPanelTemperature;
  public static JPanelEPICSRaw jPanelRawCmd;
  public static JPanelEPICSRecs jPanelEPICSRecs;
  public static JPanelDetector jPanelDetector;
  public static JPanelScripts jPanelScripts;
    //  public static JPanelEPICSStatus jPanelEPICSStatus;
  public static JPanelMaster jPanelMaster;
  public static JPanelSystem jPanelSystem;
  public static JPanelBias jPanelBias;  
  public static JPanelPreamp jPanelPreamp;                                                  
  public static String motorFileName;
  public static String temperatureFileName;
  public static String detectorFileName;
  public static JCheckBox jCheckBoxPanelLock = new JCheckBox("Panel Lock",false);
  public static int panelLockFlags = 0;
  public static JPanel southPanel = new JPanel();        
  static EPICSHeartBeat hb = new EPICSHeartBeat();
  static EPICSMotorStatus epicsMotorStatus;
    EPICSLabel heartbeatLabel;
    EPICSLabel EDTcamTypeLabel;

  /**
   *Construct the frame
   *@param inSimulatinMode boolean: in simulatin mode (yes or no)
   *@param prefix system prefix??
   */
  public jeiFrame(boolean inSimulationMode, String prefix) {
    System.out.println("Initializing.");
    EPICS.prefix = prefix;
    motorFileName = "mot_param.txt";
    detectorFileName = "detector_params.txt";
    temperatureFileName = "tmp_ctrl_params.txt";
    epicsSim.in_simulation_mode = inSimulationMode;
    if (epicsSim.in_simulation_mode) {
      System.out.println("Running local epics simulation mode.");
      epicsSim.load();
    }
    hb.start();
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    try  {
      jbInit();
    }
    catch(Exception e) {
      e.printStackTrace();
    }
  } //end of jeiFrame


//-------------------------------------------------------------------------------
  /**
   *Component initialization from parameter file
   *Component initialization from EPICS or UFlib
   */
  private void jbInit() throws Exception  {
      /*UIDefaults dd = UIManager.getDefaults();
        Enumeration enu = dd.keys();
        while (enu.hasMoreElements())
	System.out.println(enu.nextElement());
      */
    //UIManager.put("Label.font",new Font("SansSerif",Font.PLAIN,10));
    //UIManager.put("TextField.font",new Font("SansSerif",Font.PLAIN,10));
    //UIManager.put("ComboBox.font",new Font("SansSerif",Font.PLAIN,10));
    //UIManager.put("Button.font",new Font("SansSerif",Font.PLAIN,10));
    jeiMotorParameters = new jeiMotorParameters(motorFileName);
    menuBar1 = new JMenuBar();
    menuFile = new JMenu();
    menuFileExit = new JMenuItem();
    menuFileEdit = new JMenuItem();
    menuFileUpdateRecList = new JMenuItem();
    menuHelp = new JMenu();
    menuHelpAbout = new JMenuItem();
    menuHelpEdit = new JMenuItem();
    statusBar = new JLabel();
    borderLayout1 = new BorderLayout();
    jTabbedPane = new JTabbedPane();

    JMenu menuOption = new JMenu();
    JMenuItem menuOption1 = new JMenuItem();
    JMenuItem menuOption2 = new JMenuItem();
    JMenuItem menuOption3 = new JMenuItem();
    menuOption.setText("Look & Feel");
    menuOption1.setText("Motif Look");
    menuOption2.setText("Metal Look");
    menuOption3.setText("Windows Look ");
    menuOption.add(menuOption1);
    menuOption.add(menuOption2);
    menuOption.add(menuOption3);

    menuOption1.addActionListener(new ActionListener()  {
	    public void actionPerformed(ActionEvent e) {
		change_look(e);
	    }
	});

    menuOption2.addActionListener(new ActionListener()  {
	    public void actionPerformed(ActionEvent e) {
		change_look(e);
	    }
	});

    menuOption3.addActionListener(new ActionListener()  {
	    public void actionPerformed(ActionEvent e) {
		change_look(e);
	    }
	});

    heartbeatLabel = new EPICSLabel(EPICS.prefix + "heartbeat", EPICS.prefix + "heartbeat:");
    heartbeatLabel.setHorizontalAlignment(JLabel.CENTER);
    EDTcamTypeLabel = new EPICSLabel(EPICS.prefix + "sad:dcState"," Det.Control State:");

    southPanel.setLayout(new BorderLayout());
    southPanel.add(jCheckBoxPanelLock, BorderLayout.EAST);
    southPanel.add(heartbeatLabel, BorderLayout.CENTER);
    southPanel.add(EDTcamTypeLabel, BorderLayout.WEST);

    jPanelParameters = new JPanelMotorParameters(jeiMotorParameters);
    jPanelHighLevel = new JPanelMotorHighLevel(jeiMotorParameters);
    jPanelLowLevel = new JPanelMotorLowLevel(jeiMotorParameters);
    jPanelTemperature = new JPanelTemperature(temperatureFileName);
    jPanelDetector = new JPanelDetector();
    jPanelEPICSRecs = new JPanelEPICSRecs();
    //    jPanelRawCmd = new JPanelEPICSRaw();
    //    jPanelEPICSStatus = new JPanelEPICSStatus();
    jPanelMaster = new JPanelMaster( jPanelDetector, heartbeatLabel );
    jPanelSystem = new JPanelSystem();
    jPanelBias= new JPanelBias();
    jPanelPreamp = new JPanelPreamp();
    epicsMotorStatus = new EPICSMotorStatus();
    
    this.getContentPane().setLayout(borderLayout1);
    this.setLocation(jei.screen_loc[0]);
    this.setSize(jei.screen_size[0]);

    this.addComponentListener(new java.awt.event.ComponentAdapter() {
      public void componentResized(ComponentEvent e) {
        this_componentResized(e);
      }
    });
    this.addComponentListener(new java.awt.event.ComponentAdapter() {
      public void componentMoved(ComponentEvent e) {
        this_componentMoved(e);
      }
    });

    this.setTitle("T-ReCS Java Engineering Interface (ver " + version.version + ")");
    statusBar.setText(" ");
    menuFile.setText("File");
    menuFileEdit.setText("Edit a file");
    menuFileEdit.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        fileEdit_action(e);
      }
    });
    menuFileExit.setText("Exit");
    menuFileExit.addActionListener(new ActionListener()  {
      public void actionPerformed(ActionEvent e) {
        fileExit_action(e);
      }
    });
    menuFileUpdateRecList.setText("Process apfd file");
    menuFileUpdateRecList.addActionListener(new ActionListener()  {
      public void actionPerformed(ActionEvent e) {
        fileUpdateRecList_action(e);
      }
    });
    menuHelp.setText("Help");
    menuHelpAbout.setText("About");
    menuHelpAbout.addActionListener(new ActionListener()  {
      public void actionPerformed(ActionEvent e) {
        helpAbout_action(e);
      }
    });
    menuHelpEdit.setText("Edit jei_doc.txt");
    menuHelpEdit.addActionListener(new ActionListener()  {
      public void actionPerformed(ActionEvent e) {
        helpEdit_action(e);
      }
    });
    jCheckBoxPanelLock.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
	      panelLock_action(e);
      }
    });
    jTabbedPane.addChangeListener(new ChangeListener() {
      public void stateChanged(ChangeEvent e) {
        jTabbedPane_stateChanged(e);
      }
    });
    menuFile.add(menuFileEdit);
    menuFile.add(menuFileUpdateRecList);
    menuFile.add(menuFileExit);
    menuHelp.add(menuHelpAbout);
    menuHelp.add(menuHelpEdit);
    menuBar1.add(menuFile);
    menuBar1.add(menuHelp);
    menuBar1.add(menuOption);
    menuBar1.add(statusBar);

    this.setJMenuBar(menuBar1);
    this.getContentPane().add(southPanel, BorderLayout.SOUTH);
    this.getContentPane().add(jTabbedPane, BorderLayout.CENTER);

    // do scripts last so autoexec sees loaded data
    jPanelScripts = new JPanelScripts();

    jTabbedPane.add(jPanelMaster,"Master");
    jTabbedPane.add(jPanelDetector,"Detector");
    jTabbedPane.add(jPanelBias,"Bias");
    jTabbedPane.add(jPanelPreamp,"PreAmp");
    jTabbedPane.add(jPanelTemperature,"Temperature");
    //jTabbedPane.add(jPanelEPICSStatus,"Status");
    jTabbedPane.add(jPanelHighLevel,"Motor High-Level");
    jTabbedPane.add(jPanelLowLevel,"Motor Low-Level");
    jTabbedPane.add(jPanelParameters,"Motor Parameters");
    jTabbedPane.add(jPanelScripts,"Scripts");
    jTabbedPane.add(jPanelEPICSRecs,"EPICS Recs");
    //jTabbedPane.add(jPanelRawCmd,"Raw");
    jTabbedPane.add(jPanelSystem,"System");

  } //end of jbInit

//-------------------------------------------------------------------------------
    /**
     * Check flags for panel lock. Check/uncheck panel Lock checkbox based on current
     *  state of flags and current tab selected
     *@param e TBD
     */
    public void jTabbedPane_stateChanged(ChangeEvent e)
    {
	int pindex = jTabbedPane.getSelectedIndex();

	if(( panelLockFlags & (int)Math.pow(2,pindex) ) == 0)
	    jCheckBoxPanelLock.setSelected(false);
	else
	    jCheckBoxPanelLock.setSelected(true);

	if(jTabbedPane.getTitleAt(pindex)=="Detector")
	    jPanelDetector.checkConnection(true);

	if(jTabbedPane.getTitleAt(pindex)=="Temperature")
	    jPanelTemperature.checkConnection(true);

	if(jTabbedPane.getTitleAt(pindex)=="Bias") {
	    jPanelBias.checkConnection(true);
	    //Always Lock the Bias Panel:
	    //recurseLock( jPanelBias, false );
	    //jCheckBoxPanelLock.setSelected(true);
	}

	if(jTabbedPane.getTitleAt(pindex)=="PreAmp") {
	    jPanelPreamp.checkConnection(true);
	    //Always Lock the PreAmp Panel:
	    //recurseLock( jPanelPreamp, false );
	    //jCheckBoxPanelLock.setSelected(true);
	}
    } //end of jTabbedPane_stateChanged

//-------------------------------------------------------------------------------
  /**
   * Locks components of a Panel
   *@param jp JPanel to be locked
   *@param enable boolean: lock(yes or no)
   */
  public void recurse_update(JPanel jp) {
    Component [] cmp = jp.getComponents();
    for (int i=0; i<cmp.length; i++) {
      try {
        if (!cmp[i].getClass().getName().equals("javax.swing.JButton"))
	    {
		JButton _jb = (JButton) cmp[i];
		_jb.setDefaultCapable(false);
	    }
        JPanel _jp = (JPanel) cmp[i];
        recurse_update(_jp);
      }
      catch(ClassCastException cce) {
      }
      catch(Exception e) {
        jeiError.show(e.toString());
      }
    }
  } //end of recurseLock

//-------------------------------------------------------------------------------
  /**
   * Locks components of a Panel
   *@param jp JPanel to be locked
   *@param enable boolean: lock(yes or no)
   */
  public void recurseLock(JPanel jp, boolean enable) {
    Component [] cmp = jp.getComponents();
    for (int i=0; i<cmp.length; i++) {
      try {
        if (!cmp[i].getClass().getName().equals("javax.swing.JLabel") &&
            cmp[i].getName()==null)  cmp[i].setEnabled(enable);
        JPanel _jp = (JPanel) cmp[i];
        recurseLock(_jp,enable);
      }
      catch(ClassCastException cce) {
      }
      catch(Exception e) {
        jeiError.show(e.toString());
      }
    }
  } //end of recurseLock


//-------------------------------------------------------------------------------
  /**
   * Action performed for the lock checkbox
   *@param e TBD
   */
  public void panelLock_action(ActionEvent e) {
    JPanel cm = (JPanel) jTabbedPane.getSelectedComponent();
    int s = jTabbedPane.getSelectedIndex();
    panelLockFlags ^= (int)Math.pow(2,s);
    if( jCheckBoxPanelLock.isSelected() )
	recurseLock(cm,false);
    else
	recurseLock(cm,true);
    cm.repaint();
  } //end of panelLock_action


//-------------------------------------------------------------------------------
  /**
   *File | Exit action performed
   * Simply exits the application
   *@param e not used
   */
  public void fileExit_action(ActionEvent e) {
    /* close socket if != 0
    try {
      if (SocketComm.in != null)
        SocketComm.in.close();          // close the input stream
      if (SocketComm.out != null)
        SocketComm.out.close();         // close the output stream
      if (SocketComm.mysocket != null)
        SocketComm.mysocket.close();    // close the socket
    }
    catch (IOException ee) {
      System.out.println("SocketComm Error 7 : " + ee);
      jeiError.show("SocketComm Error 7 : " + ee);
    }*/
    jei.save_screen_locs();
    System.exit(0);
  } //end of fileExit_action

  boolean in_TEXT = false;
  TextFile in_file ;
  TextFile jpdf_file ;
  TextFile wpdf_file ;
  Vector s1 = new Vector();
  Vector s2 = new Vector();


//-------------------------------------------------------------------------------
  /**
   *File | Save action performed
   *Brings up the save dialog window
   *@param e TBD
   */
  public void fileUpdateRecList_action(ActionEvent e) {
    // read apfd.txt, create jpdf.txt and wpdf.txt
    String line;
    // open apdf file for input
    in_file = new TextFile(jei.data_path + "apdf.txt", TextFile.IN);
    if (!in_file.ok()) jeiError.show("Error opening apdf.txt");
    else if (in_file.ok()) {
      // open jpdf file for output
      jpdf_file = new TextFile (jei.data_path + "jpdf.txt", TextFile.OUT);
      if (!jpdf_file.ok()) jeiError.show("error opening jpdf.txt");
      else if (jpdf_file.ok()) {
        // open wpdf file for output
        wpdf_file = new TextFile (jei.data_path + "wpdf.txt", TextFile.OUT);
        if (!wpdf_file.ok()) jeiError.show("error opening wpdf.txt");
        else if (wpdf_file.ok()) {
          // read the file
          do {
            line = in_file.readLine();
            process_apdf_line(line);
          } while(!(line.toUpperCase ().equals("EOD")) && in_file.ok() && jpdf_file.ok() && wpdf_file.ok());
          in_file.close();
          jpdf_file.close();
          wpdf_file.close();
        }
      }
    }
  } //end of fileUpdateRecList_action


//-------------------------------------------------------------------------------
  /**
   *Process line of apdf commands
   *@param line String: line to be processed
   */
  void process_apdf_line(String line) {
    if (line.trim().equals("")) ;// ignore it
    else if (line.charAt(0) == ';') ; // ignore it
    else if (line.charAt(0) == '!' ) { // ! cmd
      StringTokenizer st = new StringTokenizer(line);
      String cmd = st.nextToken();
      // !SUB s1 WITH s1   replaces future occurences of s1 with s2
      if (cmd.equals("!SUB")) {
        int p = line.indexOf(" WITH ");
        if (p == -1) {
          jeiError.show("!SUB without a WITH in apdf.txt");
        }
        else {
          s1.add(line.substring(5, p));  // search string
          s2.add(line.substring(p + 6));  // replace string
        }
      }
      // !REPLICATE rep WITH s1 && s2 ...   replicates lines with unique sub for each
      else if (cmd.equals("!REPLICATE")) {
        // extract tokens from !REPLICATE line
        String rep;
        Vector s = new Vector();
        StringTokenizer st2 = new StringTokenizer(line);
        st2.nextToken(); // !REPLICATE
        rep = st2.nextToken();
        st2.nextToken(); // WITH
        while (st2.hasMoreElements()) {
          s.add(st2.nextToken("&&").trim());
        }
        // load lines to !ENDREPLICATE into a vector, rep_lines
        Vector rep_lines = new Vector();
        boolean done = false;
        do {
          if (in_file.ok()){
            line = in_file.readLine();
            done = line.trim().toUpperCase().equals("!ENDREPLICATE") || line == null;
            if (!done) rep_lines.add(line);
          }
          else jeiError.show("error replicating in apdf");
        } while(!done && in_file.ok());
        // replicate the lines in rep_lines
        for (int i = 0; i < s.size(); i++) {
          String si = (String)s.elementAt(i);
          for (int j = 0; j < rep_lines.size(); j++) {
            line = (String)rep_lines.elementAt(j);
            line = rep_all(line, rep, si);
            process_apdf_line(line);
          }
        }
      }
      // !TEXT             starts a text block for word document
      else if (cmd.equals("!TEXT")) {
        in_TEXT = true;
        wpdf_file.println(cmd);
      }
      // !ENDTEXT             starts a text block for word document
      else if (cmd.equals("!ENDTEXT")) {
        in_TEXT = false;
        wpdf_file.println(cmd);
      }
    }
    else { // normal line
      // sub and write line
      for (int i = 0; i < s1.size(); i++) {
        line = rep_all(line, (String)s1.elementAt(i), (String)s2.elementAt(i));
      }
      if (line.startsWith("SIR ")) {
        jpdf_file.println(line.substring(4));
      }
      else {
        if (!in_TEXT) jpdf_file.println(line);
        if (line.startsWith("  ")) {
          wpdf_file.println("  field," + line);
        }
        else {
          wpdf_file.println(line);
        }
      }
    }
  } // end of process_line

//-------------------------------------------------------------------------------
  /**
   *Replaces all occurrences of s1 with s2 in line
   *@param line String: line for replacment
   *@param s1 String: to be replaced
   *@param s2 String: replacment to s1
   */
  String rep_all(String line, String s1, String s2) {
    int p;
    while ((p = line.indexOf(s1)) > -1) {
      line = line.substring(0, p) + s2 + line.substring(p + s1.length());
    }
    return line;
  } // end of rep_all

//-------------------------------------------------------------------------------
  /**
   *Help | About action performed
   *Brings up the about dialog window
   *@param e not used
   */
  public void helpAbout_action(ActionEvent e) {
    jeiFrameAboutBox dlg = new jeiFrameAboutBox(this);
    Dimension dlgSize = dlg.getSize();
    Dimension frmSize = getSize();
    dlg.setLocation(100,100);
    dlg.setModal(true);
    dlg.show();
  } //end of helpAbout_action


//-------------------------------------------------------------------------------
  /**
   *Help | Edit action performed
   *Brings up the jei_doc.txt document for viewing and editing
   *@param e not used
   */
  public void helpEdit_action(ActionEvent e) {
    new jeiEditorFrame(jei.data_path + "jei_doc.txt");
  } //end of helpEdit_action


//-------------------------------------------------------------------------------
  /**
   *File | Edit action performed
   *brings up the jei_doc.txt document for viewing and editing
   *@param e not used
   */
  public void fileEdit_action(ActionEvent e) {
    new jeiEditorFrame(jei.data_path);
  } //end of helpEdit_action


//-------------------------------------------------------------------------------
  /**
   *Overridden so we can exit on System Close
   *@param e TBD
   */
  protected void processWindowEvent(WindowEvent e) {
    super.processWindowEvent(e);
    if(e.getID() == WindowEvent.WINDOW_CLOSING) {
      fileExit_action(null);
    }
  } //end of processWindowEvent

//-------------------------------------------------------------------------------
  /**
   * Component Event Handler??
   *@param e not used
   */
  void this_componentMoved(ComponentEvent e) {
    jei.screen_loc[0] = this.getLocation();
    // System.out.println("screen_loc = " + jei.screen_loc[0].toString());
  } //end of this_componentMoved

//-------------------------------------------------------------------------------
  /**
   * Component Event Handler
   *@param e not used
   */
  void this_componentResized(ComponentEvent e) {
    jei.screen_size[0] = this.getSize();
    // System.out.println("size = " + jei.screen_size[0].toString()); // java.awt.Dimension[width=859,height=534]
  } // end of this_componentResized

//---------------------------------------------------------------------------------------

 void change_look(ActionEvent e) {
    if (e.getActionCommand()=="Motif Look") 
	try {
	    UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
	    SwingUtilities.updateComponentTreeUI(this);
	} 
    catch (Exception exc) {}

    if (e.getActionCommand()=="Metal Look") 
	try {
	    UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
	    SwingUtilities.updateComponentTreeUI(this);
	} 
    catch (Exception exc) {}

    if (e.getActionCommand()=="Windows Look") 
	try {
	    UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
	    SwingUtilities.updateComponentTreeUI(this);
	} 
    catch (Exception exc) {}
 }
} //end of class jeiFrame





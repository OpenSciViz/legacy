package ufjei;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

//===============================================================================
/**
 * Handles the Optical Path window
 */
public class jeiOpath extends JFrame {

  String win_name;
  final int x0 = 10;
  final int xw = 90;
  final int xs = 20;
  final int dx = 2;
  final int y0 = 20;
  final int yr = 19; // y per row
  final int nm = 9;
  BorderLayout borderLayout1 = new BorderLayout();
  GraphicsPanel jPanel1 = new GraphicsPanel();
    //XYLayout xYLayout1 = new XYLayout();
  JLabel motor[][];
  JLabel jLabel[];
  int x1[] = new int[nm + 1];
  int x2[] = new int[nm + 1];
  int y[] = new int[nm + 1];
  int hilighted[] = new int[nm + 1];
  Color on_path_color = Color.white;
  Color off_path_color = Color.lightGray;

//-------------------------------------------------------------------------------
  /**
   *Construct the frame
   */
  public jeiOpath() {
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    try  {
      jbInit();
    }
    catch(Exception e) {
      e.printStackTrace();
    }
  } //end of jeiOpath


//-------------------------------------------------------------------------------
  /**
   *Component initialization
   */
  private void jbInit() throws Exception  {
    this.getContentPane().setLayout(borderLayout1);
    this.setSize(new Dimension(2*x0+nm*xw+(nm-1)*xs, 350));
    win_name = "Optical Path";
    this.setTitle(win_name);
    this.setLocation(jei.get_screen_loc(win_name));
    this.setSize(jei.get_screen_size(win_name, new Dimension(2*x0+nm*xw+(nm-1)*xs, 350)));
    addComponentListener(new java.awt.event.ComponentAdapter() {
      public void componentResized(ComponentEvent e) {
        jei.set_screen_loc(win_name, getLocation(), getSize());
      }
    });
    addComponentListener(new java.awt.event.ComponentAdapter() {
      public void componentMoved(ComponentEvent e) {
        jei.set_screen_loc(win_name, getLocation(), getSize());
      }
    });
    this.getContentPane().add(jPanel1, BorderLayout.CENTER);
    jPanel1.setLayout(null);
    motor = new JLabel[nm + 1][];
    jLabel = new JLabel[nm + 1];
    for (int i = 1; i <  nm; i++) {
      x1[i] = x0 + xw + dx + (i - 1) * (xw + xs);
      x2[i] = x0 + xw + xs - dx + (i - 1) * (xw + xs);
      y[i] = y0 + yr / 2;
    }
    y[nm] = y0 + yr / 2;
    for (int i = 1; i <=  nm; i++) {
      jLabel[i] = new JLabel(jeiMotorParameters.getMotorName(i));
      jLabel[i].setBounds(x0 + (i - 1) * (xw + xs), 0, xw, 20);
      jPanel1.add(jLabel[i]);
      motor[i] = new JLabel[jeiMotorParameters.getNumLocations(i)+1];
      int j = 0;
      motor[i][j] = new JLabel("UNKNOWN");
      motor[i][j].setBounds(x0 + (i - 1) * (xw + xs), y0 + j * yr, xw, 20);
      motor[i][j].setOpaque(true);
      motor[i][j].setBackground(on_path_color);
      hilighted[i] = 0;
      jPanel1.add(motor[i][j]);
      for (j = 1; j <=  jeiMotorParameters.getNumLocations(i); j++) {
        motor[i][j] = new JLabel(jeiMotorParameters.getNamedLocations(i,j));
        motor[i][j].setBounds(x0 + (i - 1) * (xw + xs), y0 + j * yr + yr, xw, 20);
        motor[i][j].setOpaque(true);
        motor[i][j].setBackground(off_path_color);
        jPanel1.add(motor[i][j]);
      }
      update_motor_status(i);
    }
    this.addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) {
        jeiFrame.Opathframe.dispose();
        jeiFrame.Opathframe = null;
      }
    });
  } //end of jbInit

//-------------------------------------------------------------------------------
  /**
   * Sets y for the motor that moved
   *@param mot_num int: motor number
   */
  public void update_motor_status(int mot_num) {
    int j = 1; // position_num
    int steps_from_home = (int)EPICSMotorStatus.currPos[mot_num - 1];
    int steps_per_pos, steps_beyond_pos;
    boolean done = false;
    if (steps_from_home == -999999) { // unknown
      y[mot_num] = yr + yr /2;
      return;
    }
    while (!done) {
      if (steps_from_home < jeiMotorParameters.getLocationOffset(mot_num, j)) {
        if (hilighted[mot_num] != -1) {
          motor[mot_num][hilighted[mot_num]].setBackground(off_path_color);
        }
        hilighted[mot_num] = -1;
        if (j == 1) {
          steps_per_pos = jeiMotorParameters.getLocationOffset(mot_num, 2) -
                          jeiMotorParameters.getLocationOffset(mot_num, 1);
        }
        else {
          steps_per_pos = jeiMotorParameters.getLocationOffset(mot_num, j) -
                          jeiMotorParameters.getLocationOffset(mot_num, j - 1);
        }
        steps_beyond_pos = steps_from_home -
                          jeiMotorParameters.getLocationOffset(mot_num, j - 1);
        if (steps_beyond_pos < 0) steps_beyond_pos = 0;
        y[mot_num] = y0 +  yr /2 + j * yr + yr * steps_beyond_pos / steps_per_pos;
        done = true;
      }
      else if (steps_from_home == jeiMotorParameters.getLocationOffset(mot_num, j)) {
        y[mot_num] = y0 + yr / 2 + (j + 1) * yr;
        if (hilighted[mot_num] != j) {
          if (hilighted[mot_num] != -1) {
            motor[mot_num][hilighted[mot_num]].setBackground(off_path_color);
          }
          hilighted[mot_num] = j;
          motor[mot_num][hilighted[mot_num]].setBackground(on_path_color);
        }
        done = true;
      }
      if (j == jeiMotorParameters.getNumLocations(mot_num) && !done) {
        if (hilighted[mot_num] != -1) {
          motor[mot_num][hilighted[mot_num]].setBackground(off_path_color);
        }
        hilighted[mot_num] = -1;
        steps_per_pos = jeiMotorParameters.getLocationOffset(mot_num, j) -
                        jeiMotorParameters.getLocationOffset(mot_num, j - 1);
        steps_beyond_pos = steps_from_home -
                          jeiMotorParameters.getLocationOffset(mot_num, j);
        if (steps_beyond_pos > steps_per_pos) steps_beyond_pos = steps_per_pos;
        y[mot_num] = y0 +  yr /2 + (j + 1) * yr + yr * steps_beyond_pos / steps_per_pos;
        done = true;
      }
      j++;
    }
    repaint();
  } //end of update_motor_status

//===============================================================================
  /**
   * Graphical Interpretation of the Optical path
   */
  class GraphicsPanel extends JPanel {


//-------------------------------------------------------------------------------
    /**
     * paints components??
     *@param g Graphics: TBD
     */
    public void paintComponent(Graphics g) {
      super.paintComponent(g);
      for (int i = 1; i < nm; i++) {
        g.drawLine(x1[i], y[i], x2[i], y[i+1]);
      }
    } //end of paintComponent
  } //end of class GraphicsPanel

} //end of class jeiOpath
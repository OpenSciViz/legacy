package ufjei;
   import java.awt.event.*;
   import javax.swing.*;
   import java.awt.*;
   import java.net.*;
   import java.applet.*;
   import java.io.*;
   import java.util.*;
   import jca.*;
   import jca.dbr.*;
   import jca.event.*;


//===============================================================================
/**
 *Parameters tabbed pane
 */
public class JPanelMotorParameters extends JPanel {
  public static final String rcsID = "$Name:  $ $Id: JPanelMotorParameters.java,v 0.1 2002/07/25 23:20:15 varosi beta $";
  public static JPanelMotorNames jPanelMotorNames  ;
  public static JPanelInitialSpeed jPanelInitialSpeed ;
  public static JPanelTerminalSpeed jPanelTerminalSpeed  ;
  public static JPanelAcceleration jPanelAcceleration  ;
  public static JPanelDeceleration jPanelDeceleration ;
  public static JPanelDriveCurrent jPanelDriveCurrent ;
  public static JPanelDatumSpeed jPanelDatumSpeed ;
  public static JPanelBackLash jPanelBackLash;
  public static JPanelDatumDirection jPanelDatumDirection ;
  RatioLayout layout = new RatioLayout();
  GridLayout fieldLayout = new GridLayout();
  JPanel jPanelFields = new JPanel();
  JButton jButtonConnect = new JButton("Connect  EPICS  to  Motor  Agent");
  JButton jButtonApply = new JButton("Apply  Parameters");
  EPICSLabel HeartBeatLabel = new EPICSLabel(EPICS.prefix + "cc:heartbeat",
					     EPICS.prefix + "cc:HeartBeat:");
  EPICSLabel monitorCAR = new EPICSLabel(EPICS.prefix + "cc:applyC.VAL", "CAR:");
  EPICSLabel monitorIMSS = new EPICSLabel(EPICS.prefix + "cc:applyC.IMSS", "Msg:");

//-------------------------------------------------------------------------------
  /**
   *Default constructor
   */
  public JPanelMotorParameters(jeiMotorParameters jeiMotorParameters) {
    fieldLayout.setColumns(0);
    fieldLayout.setRows(1);
    this.setLayout(layout);
    jPanelFields.setLayout(fieldLayout);
    jPanelMotorNames = new JPanelMotorNames(jeiMotorParameters);
    jPanelInitialSpeed = new JPanelInitialSpeed(jeiMotorParameters);
    jPanelTerminalSpeed = new JPanelTerminalSpeed(jeiMotorParameters);
    jPanelAcceleration = new JPanelAcceleration(jeiMotorParameters);
    jPanelDeceleration = new JPanelDeceleration(jeiMotorParameters);
    jPanelDriveCurrent = new JPanelDriveCurrent(jeiMotorParameters);
    jPanelDatumSpeed = new JPanelDatumSpeed(jeiMotorParameters);
    jPanelBackLash = new JPanelBackLash(jeiMotorParameters);
    jPanelDatumDirection = new JPanelDatumDirection(jeiMotorParameters);

    this.add("0.01,0.02;0.15,0.66", jPanelMotorNames);
    jPanelFields.add(jPanelInitialSpeed, null);
    jPanelFields.add(jPanelTerminalSpeed, null);
    jPanelFields.add(jPanelAcceleration, null);
    jPanelFields.add(jPanelDeceleration, null);
    jPanelFields.add(jPanelDriveCurrent, null);
    jPanelFields.add(jPanelDatumSpeed, null);
    jPanelFields.add(jPanelBackLash, null);
    jPanelFields.add(jPanelDatumDirection, null);
    this.add("0.17,0.02;0.82,0.66", jPanelFields);
    this.add("0.17,0.80;0.30,0.10", jButtonConnect);
    this.add("0.50,0.80;0.20,0.10", jButtonApply);
    this.add("0.75,0.70;0.25,0.10", HeartBeatLabel);
    HeartBeatLabel.setHorizontalAlignment(JLabel.LEFT);
    this.add("0.05,0.70;0.15,0.10", monitorCAR);
    this.add("0.20,0.70;0.40,0.10", monitorIMSS);

    jButtonApply.addActionListener(new ActionListener() {
       public void actionPerformed(ActionEvent ae) {
	   jeiFrame.jPanelHighLevel.jPanelStatus.queryMotorPositions();
       }
    });

    jButtonConnect.addActionListener(new java.awt.event.ActionListener()
	{
	    public void actionPerformed(ActionEvent ae) {
		monitorCAR.setText("CAR: BUSY");
		monitorIMSS.setText("Msg: Epics connecting to Motor Agent...");
		try { Thread.sleep(100); } catch (Exception _e) {}
		jeiFrame.jPanelHighLevel.jPanelStatus.connectEPICStoMotorAgent();
	    }
	});

  } //end of JPanelMotorParameters

    public void putParams(int mot_num) {
	String val;
	val = jPanelInitialSpeed.InitialSpeed[mot_num-1].getText();
	jPanelInitialSpeed.InitialSpeed[mot_num-1].putcmd(val);

	val = jPanelTerminalSpeed.TerminalSpeed[mot_num-1].getText();
	jPanelTerminalSpeed.TerminalSpeed[mot_num-1].putcmd(val);

	val = jPanelAcceleration.Acceleration[mot_num-1].getText();
	jPanelAcceleration.Acceleration[mot_num-1].putcmd(val);

	val = jPanelDeceleration.Deceleration[mot_num-1].getText();
	jPanelDeceleration.Deceleration[mot_num-1].putcmd(val);

	val = jPanelDriveCurrent.DriveCurrent[mot_num-1].getText();
	jPanelDriveCurrent.DriveCurrent[mot_num-1].putcmd(val);

	val = jPanelDatumSpeed.DatumSpeed[mot_num-1].getText();
	jPanelDatumSpeed.DatumSpeed[mot_num-1].putcmd(val);

        val = jPanelDatumDirection.DatumDirection[mot_num-1]._getSelectedItem();
	jPanelDatumDirection.DatumDirection[mot_num-1].putcmd(val);
    }

} //end of class JPanelMotorParameters


//===============================================================================
/**
 *Array of initial speed text fields
 */
class JPanelBackLash  extends JPanel {
  public static EPICSTextField BackLash[] ;
  private static String buffer;


//-------------------------------------------------------------------------------
  /**
   *Default constructor
   */
  public JPanelBackLash(jeiMotorParameters mot_param) {
    String prefix;
    BackLash = new EPICSTextField[mot_param.getNumberMotors()];
    GridLayout gridLayout = new GridLayout();
    setLayout(gridLayout);
    gridLayout.setRows(mot_param.getNumberMotors()+1);
    gridLayout.setVgap(5);
    add(new JLabel("Back Lash",JLabel.CENTER), null);
    for (int i=0; i < mot_param.getNumberMotors(); i++) {
      prefix = jeiMotorParameters.getPVmotorPrefix(i+1);
      BackLash[i] = new EPICSTextField(prefix + "motorG.L",
                                           prefix + "motorG.VALL", "Back Lash", true); //doPuts=true.
      add (BackLash[i]);
    }
  } //end of JPanelInitialSpeed

} //end of class JPanelInitialSpeed


//===============================================================================
/**
 *Array of initial speed text fields
 */
class JPanelInitialSpeed extends JPanel {
  public static EPICSTextField InitialSpeed[] ;
  private static String buffer;


//-------------------------------------------------------------------------------
  /**
   *Default constructor
   */
  public JPanelInitialSpeed(jeiMotorParameters mot_param) {
    String prefix;
    InitialSpeed = new EPICSTextField[mot_param.getNumberMotors()];
    GridLayout gridLayout = new GridLayout();
    setLayout(gridLayout);
    gridLayout.setRows(mot_param.getNumberMotors()+1);
    gridLayout.setVgap(5);
    add(new JLabel("Initial Speed",JLabel.CENTER), null);
    for (int i=0; i < mot_param.getNumberMotors(); i++) {
      prefix = jeiMotorParameters.getPVmotorPrefix(i+1);
      InitialSpeed[i] = new EPICSTextField(prefix + "motorG.N",
                                           prefix + "motorG.VALD", "Initial Speed", true); //doPuts=true.
      add (InitialSpeed[i]);
    }
  } //end of JPanelInitialSpeed

} //end of class JPanelInitialSpeed


//===============================================================================
/**
 *Array of terminal speed text fields
 */
class JPanelTerminalSpeed extends JPanel {
  public static EPICSTextField TerminalSpeed[] ;
  private static String buffer;


//-------------------------------------------------------------------------------
  /**
   *Default constructor
   */
  public JPanelTerminalSpeed(jeiMotorParameters mot_param) {
    String prefix;
    TerminalSpeed = new EPICSTextField[mot_param.getNumberMotors()];
    GridLayout gridLayout = new GridLayout();
    setLayout(gridLayout);
    gridLayout.setRows(mot_param.getNumberMotors()+1);
    gridLayout.setVgap(5);
    add(new JLabel("Term Speed", JLabel.CENTER), null);
    for (int i=0; i < mot_param.getNumberMotors(); i++) {
      prefix = jeiMotorParameters.getPVmotorPrefix(i+1);
      TerminalSpeed[i] = new EPICSTextField(prefix + "motorG.O",
					    prefix + "motorG.VALE", "Terminal Speed", true);
      add (TerminalSpeed[i]);
    }
  } //end of JPanelTerminalSpeed

} //end of class JPanelTerminalSpeed


//===============================================================================
/**
 *Array of acceleration text fields
 */
class JPanelAcceleration extends JPanel {
  public static EPICSTextField Acceleration[] ;
  private static String buffer;


//-------------------------------------------------------------------------------
  /**
   *Default constructor
   */
  public JPanelAcceleration(jeiMotorParameters mot_param) {
    String prefix;
    Acceleration = new EPICSTextField[mot_param.getNumberMotors()];
    GridLayout gridLayout = new GridLayout();
    setLayout(gridLayout);
    gridLayout.setRows( mot_param.getNumberMotors()+1);
    gridLayout.setVgap(5);
    add(new JLabel("Acceleration",JLabel.CENTER), null);
    for (int i=0; i <  mot_param.getNumberMotors(); i++) {
      prefix = jeiMotorParameters.getPVmotorPrefix(i+1);
      Acceleration[i] = new EPICSTextField(prefix + "motorG.P",
                                           prefix + "motorG.VALF", "Acceleration", true);
      add (Acceleration[i]);
    }
  } //end of JPanelAcceleration

} //end of class JPanelAcceleration


//===============================================================================
/**
 *Array of Deceleration text fields
 */
class JPanelDeceleration extends JPanel {
  public static EPICSTextField Deceleration[];
  private static String buffer;


//-------------------------------------------------------------------------------
  /**
   *Default constructor
   */
  public JPanelDeceleration(jeiMotorParameters mot_param) {
    String prefix;
    Deceleration = new EPICSTextField[mot_param.getNumberMotors()];
    GridLayout gridLayout = new GridLayout();
    setLayout(gridLayout);
    gridLayout.setRows(mot_param.getNumberMotors()+1);
    gridLayout.setVgap(5);
    add(new JLabel("Deceleration",JLabel.CENTER), null);
    for (int i=0; i < mot_param.getNumberMotors(); i++) {
      prefix = jeiMotorParameters.getPVmotorPrefix(i+1);
      Deceleration[i] = new EPICSTextField(prefix + "motorG.Q",
                                           prefix + "motorG.VALG", "Deceleration", true);
      add (Deceleration[i]);
    }
  } //end of JPanelDeceleration

} //end of class JPanelDeceleration


//===============================================================================
/**
 *Array of drive current text fields
 */
class JPanelDriveCurrent extends JPanel {
  public static EPICSTextField DriveCurrent[];
  private static String buffer;


//-------------------------------------------------------------------------------
  /**
   *Default constructor
   */
  public JPanelDriveCurrent(jeiMotorParameters mot_param) {
    String prefix;
    DriveCurrent = new EPICSTextField[mot_param.getNumberMotors()];
    GridLayout gridLayout = new GridLayout();
    setLayout(gridLayout);
    gridLayout.setRows(mot_param.getNumberMotors()+1);
    gridLayout.setVgap(5);
    add(new JLabel("Drive Current",JLabel.CENTER), null);
    for (int i=0; i < mot_param.getNumberMotors(); i++) {
      prefix = jeiMotorParameters.getPVmotorPrefix(i+1);
      DriveCurrent[i] = new EPICSTextField(prefix + "motorG.R",
                                           prefix + "motorG.VALH", "Drive Current", true);
      add (DriveCurrent[i]);
    }
  } //end of JPanelDriveCurrent

} //end of class JPanelDriveCurrent

//===============================================================================
/**
 *Array of datum speed text fields
 */
class JPanelDatumSpeed extends JPanel {
  public static EPICSTextField DatumSpeed[];
  private static String buffer;


//-------------------------------------------------------------------------------
  /**
   *Default constructor
   */
  public JPanelDatumSpeed(jeiMotorParameters mot_param) {
    String prefix;
    DatumSpeed = new EPICSTextField[mot_param.getNumberMotors()];
    GridLayout gridLayout = new GridLayout();
    setLayout(gridLayout);
    gridLayout.setRows(mot_param.getNumberMotors()+1);
    gridLayout.setVgap(5);
    add(new JLabel("Datum Speed",JLabel.CENTER), null);
    for (int i=0; i < mot_param.getNumberMotors(); i++) {
      prefix = jeiMotorParameters.getPVmotorPrefix(i+1);
      DatumSpeed[i] = new EPICSTextField(prefix + "motorG.S",
					 prefix + "motorG.VALI", "Datum Speed", true);
      add (DatumSpeed[i]);
    }
  } //end of JPanelDatumSpeed

} //end of class JPanelDatumSpeed


//===============================================================================
/**
 *Array of datum direction text fields
 */
class JPanelDatumDirection extends JPanel {
  public static EPICSComboBox DatumDirection[];
  private static String buffer;


//-------------------------------------------------------------------------------
  /**
   *Default constructor
   */
  public JPanelDatumDirection(jeiMotorParameters mot_param) {
    String prefix;
    String [] values = {"","0","1"};
    DatumDirection = new EPICSComboBox[mot_param.getNumberMotors()];
    GridLayout gridLayout = new GridLayout();
    setLayout(gridLayout);
    gridLayout.setRows(mot_param.getNumberMotors()+1);
    gridLayout.setVgap(5);
    add(new JLabel("Datum Direction",JLabel.CENTER), null);
    for (int i=0; i < mot_param.getNumberMotors(); i++) {
      prefix = jeiMotorParameters.getPVmotorPrefix(i+1);
      DatumDirection[i] = new EPICSComboBox(prefix + "motorG.T",
					    prefix + "motorG.VALJ", values, EPICSComboBox.ITEM, true);
      add (DatumDirection[i]);
    }
  } //end of JPanelDatumDirection

} //end of class JPanelDatumDirection

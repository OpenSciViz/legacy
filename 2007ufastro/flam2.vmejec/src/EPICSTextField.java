
//Title:        JAVA Engineering Console
//Version:      1.0
//Copyright:    Copyright (c) Latif Albusairi and Tom Kisko
//Author:       David Rashkin, Latif Albusairi and Tom Kisko
//Modified:     by Frank Varosi, 2001 (to make it work right with epics).
//Company:      University of Florida
//Description:

package uffjec;

import ufjca.*;

import java.awt.*;
import java.awt.event.*;
import javax.swing.JTextField;
import javax.swing.*;
//===============================================================================
/**
 * Handles text fields related to EPICS
 */
public class EPICSTextField extends JTextField implements UFCAToolkit.MonitorListener,FocusListener, KeyListener, MouseListener {
    public static final String rcsID = "$Name:  $ $Id: EPICSTextField.java,v 0.20 2005/08/05 19:04:13 drashkin Exp $";
    String putRec;
    String monitorRec;
    String prev_text = "";
    String new_text = "";
    String desc;
    String outVal = "";
    boolean dual = true; // true if putRec and monitorRec are two different recs
    boolean doPuts = true;
    boolean userEvent = false;
    long timeStamp = 0;

//-------------------------------------------------------------------------------
  /**
   *Default Constructor
   */
  public EPICSTextField() {
    try  {
      jbInit("", "", "");
    }
    catch(Exception ex) {
      fjecError.show("Error in creating an EPICSTextField: " + ex.toString());
    }
  } //end of EPICSTextField

//-------------------------------------------------------------------------------
  /**
   * Constructor
   *@param rec String: record field
   */
  public EPICSTextField(String rec) {
    try  {
      jbInit(rec, rec, "");
    }
    catch(Exception ex) {
      fjecError.show("Error in creating EPICSTextField " + rec + ": " + ex.toString());
    }
  } //end of EPICSTextField
//-------------------------------------------------------------------------------
  /**
   * Constructor
   *@param rec String: record field
   *@param description String: Information regarding the record field
   */
  public EPICSTextField(String rec, String description) {
    try  {
      jbInit(rec, rec, description);
    }
    catch(Exception ex) {
      fjecError.show("Error in creating EPICSTextField " + rec + ": " + ex.toString());
    }
  } //end of EPICSTextField

//-------------------------------------------------------------------------------
  /**
   * Constructor
   *@param rec String: record field
   *@param description String: Information regarding the record field
   */
  public EPICSTextField(String rec, String description, boolean doPuts) {
    try  {
      this.doPuts = doPuts;
      jbInit(rec, rec, description);
    }
    catch(Exception ex) {
      fjecError.show("Error in creating EPICSTextField " + rec + ": " + ex.toString());
    }
  } //end of EPICSTextField

//-------------------------------------------------------------------------------
  /**
   * Constructor
   *@param putRec String: Input record field for monitoring
   *@param monitorRec String: Output record field for monitoring
   *@param description String: Information regarding the record field
   */
  public EPICSTextField(String putRec, String monitorRec, String description, boolean doPuts) {
    try  {
      this.doPuts = doPuts;
      jbInit(putRec, monitorRec, description);
    }
    catch(Exception ex) {
      fjecError.show("Error in creating EPICSTextField " + putRec + "/" + monitorRec + ": " + ex.toString());
    }
  } //end of EPICSTextField

//-------------------------------------------------------------------------------
  /**
   * Constructor
   *@param putRec String: Input record field for monitoring
   *@param monitorRec String: Output record field for monitoring
   *@param description String: Information regarding the record field
   */
  public EPICSTextField(String putRec, String monitorRec, String description) {
    try  {
      jbInit(putRec, monitorRec, description);
    }
    catch(Exception ex) {
      fjecError.show("Error in creating EPICSTextField " + putRec + "/" + monitorRec + ": " + ex.toString());
    }
  } //end of EPICSTextField

//-------------------------------------------------------------------------------
  /**
   *Component initialization
   *@param putRec String: Input record field for monitoring
   *@param monitorRec String: Output record field for monitoring
   *@param description String: Information regarding the record field
   */
  private void jbInit(String putRec, String monitorRec, String description) throws Exception {
    this.dual = !putRec.equals(monitorRec);
    this.putRec = putRec;
    this.monitorRec = monitorRec;
    this.desc = description;
    this.setToolTipText();
    this.timeStamp = 0;
    this.addMouseListener(this);

    if( dual || doPuts ) {
	this.addFocusListener(this);
	this.addKeyListener(this);
    }

    if( putRec.trim().equals("") || monitorRec.trim().equals("") ) return;

    if (!EPICS.addMonitor(monitorRec,this))
	setBackground(Color.red);
    /*
    // setup monitor(s)
    int mask = Monitor.VALUE;
    try {
	gov.aps.jca.Channel theChannel = EPICS.theContext.createChannel(monitorRec);
	EPICS.theContext.pendIO(EPICS.TIMEOUT);
	outMon = theChannel.addMonitor(mask,this);
	EPICS.theContext.flushIO();
	//EPICS.monitorCheat(outMon,this).start();
	//theContext.destroy();
	//theChannel.destroy();
	//	outMon = pv.addMonitor(new DBR_DBString(), this, new Integer(0), mask);
    }
    catch (Exception ex) { this.setBackground( Color.red ); }
    */
  } //end of jbInit

    public void reconnect(String dbname) {
	EPICS.removeMonitor(monitorRec,this);
	putRec = dbname + putRec.substring(putRec.indexOf(":")+1);
	monitorRec = dbname + monitorRec.substring(monitorRec.indexOf(":")+1);
	EPICS.addMonitor(monitorRec,this);
    }


    public long getTimeStamp() { return timeStamp; }

    public String getPutRec() { return putRec; }
    public String getMonRec() { return monitorRec; }

    public String toString() { return getText();}

//-------------------------------------------------------------------------------
  /**
   * Sets the text for the tool tip -- returns no arguments
   */
  void setToolTipText() {
    if( dual )
	this.setToolTipText( putRec + " -> " + monitorRec + " = " + outVal );
    //this.setToolTipText( monitorRec + " = " + outVal
    //		     + "  : old=" + prev_text
    //		     + "  : new=" + new_text );
    else
	this.setToolTipText( monitorRec + " = " + outVal );
  }

//-------------------------------------------------------------------------------
  /**
   * Event Handling Method: overloads the virtual method in abstract MonitorListener class (jca)
   *@param event MonitorEvent: TBD
   */
  public void monitorChanged(String value) {
    try {
	//dbstr.printInfo();
	//Ca.pendIO(EPICS.TIMEOUT);

	//	if( event.getChannel().getName().equals(this.monitorRec) ) {
	    outVal = value.trim(); 
	    setText( outVal.trim() );
	    prev_text = value.trim();
	    if( this.getText().indexOf("WARN") >= 0 ) Toolkit.getDefaultToolkit().beep();
	    if( this.getText().indexOf("ERR") >= 0 ) {
		Toolkit.getDefaultToolkit().beep();
	      Toolkit.getDefaultToolkit().beep();
	    }
	    //outMon.clear();
	    //outMon = event.pv().addMonitor(new DBR_DBString(),this,null,Monitor.VALUE);
	    //}
	
	setToolTipText();
	
	if( dual && doPuts && userEvent )
	    {
		userEvent = false;
		if( new_text.equals( getText() ) )
		    this.setBackground( Color.white );
		else 
		    this.setBackground( Color.yellow );
	    } 
	else this.setBackground( Color.white );
	this.timeStamp = System.currentTimeMillis();
	this.invalidate();
    }
    catch (Exception ex) {
	fjecError.show("Error in monitor changed for EPICSTextField: " + monitorRec + " " + ex.toString());
    }
  } //end of monitorChanged

//------------------------------------------------------------------
    public void mouseReleased(MouseEvent me) {}
    public void mousePressed(MouseEvent me) {}
    public void mouseExited(MouseEvent me) {}
    public void mouseEntered(MouseEvent me) {}
    public void mouseClicked(MouseEvent me) {
	if (me.getClickCount() == 2 && me.getButton() == MouseEvent.BUTTON3) {
	    String inp = "";
	    inp = JOptionPane.showInputDialog(this,"New monitor rec for this "+
					      "component? (Current pvName = "+
					      monitorRec+")");
	    if (inp != null && inp.trim() != null) {
		try {
		    EPICS.removeMonitor(monitorRec,this);
		    monitorRec = inp.trim();
		    if (!dual)
			putRec = monitorRec;
		    EPICS.addMonitor(monitorRec,this);
		}  catch (Exception e) {
		    System.err.println("EPICSTextField::mouseClicked> "+
				       e.toString());
		}
	    }
	}
    }

//-------------------------------------------------------------------------------

    public void focusGained(FocusEvent e) 
    { 
	if( prev_text.trim().equals("") )
	    prev_text = this.getText().trim();
    }

//-------------------------------------------------------------------------------

    public void focusLost(FocusEvent e) 
    { 
	new_text = getText().trim();

	if( ! new_text.equals( prev_text ) ) {
	    setBackground( Color.yellow );
	    if( doPuts ) putcmd( new_text );
	}
	else setBackground( Color.white );
    }
//-------------------------------------------------------------------------------
  /**
   * Event Handling Method
   *@param ke KeyEvent: TBD
   */
  public void keyTyped (KeyEvent ke) {
      userEvent = true;
  }

//-------------------------------------------------------------------------------
  /**
   * Event Handling Method
   *@param ke KeyEvent: TBD
   */
  public void keyReleased (KeyEvent ke) {
      userEvent = true;
  }

//-------------------------------------------------------------------------------
  /**
   * Event Handling Method
   *@param ke KeyEvent: TBD
   */
    public void keyPressed (KeyEvent ke)
    {
	userEvent = true;

	if( ke.getKeyChar() == '\n' )    // we got the enter key
	    {
		new_text = getText().trim();

		if( ! new_text.equals( prev_text ) ) {
		    setBackground( Color.yellow );
		    if( doPuts ) putcmd( new_text );
		}
		else setBackground( Color.white );
	    }
	else if( ke.getKeyChar() == 27 )  // we got the escape key
	    {
		setText( prev_text );
	    }
    }

//-------------------------------------------------------------------------------
  /**
   * Puts the current contents of the textfield to the EPICS record field
   *@param val String: Value to be put
   */
    void forcePut() {
      putcmd( this.getText() );
    }

//-------------------------------------------------------------------------------
  /**
   * Puts the val to the EPICS record field
   *@param val String: Value to be put
   */
  void putcmd(String val) {
    if( val.trim().equals("") || putRec.trim().equals("") ) return;
    //String cmd = new String("PUT " + putRec + " " + "\""+val+ "\"" + " ;" + desc) ;
    //command.execute(cmd);
    EPICS.put(putRec,val);
    prev_text = val.trim();
  } //end of putcmd

//-------------------------------------------------------------------------------
  /**
   * Sets the text and puts the value in EPICS
   *@param val String: value to be set and put
   */
  public void set_and_putcmd(String val) {
    setText( val );
    putcmd( val );
  } //end of set_and_putcmd

//-------------------------------------------------------------------------------
  /**
   * Window Event Handling Method
   *@param e WindowEvent: TBD
   */
  protected void processWindowEvent(WindowEvent e) {
    if(e.getID() == WindowEvent.WINDOW_CLOSING) {
      try {
	  EPICS.removeMonitor(monitorRec,this);
      }
      catch (Exception ee) {
        fjecError.show(e.toString());
      }
      ///dispose();
    }
    ///super.processWindowEvent(e);
  } //end of processWindowEvent

//-------------------------------------------------------------------------------
  /**
   *  This method allows the user to reset the
   *  EPICS record names and re-establish
   *  monitors to the new record names.
   *@param putRec String: Input record field for monitoring
   *@param monRec String: Output record field for monitoring
   *@param desc String: description of the record field
   */
  public void setEPICSRecords(String putRec, String monRec, String desc) {
    this.setText("");
    this.setBackground(Color.white);
    this.putRec = putRec;
    this.monitorRec = monRec;
    this.desc = desc;
    this.prev_text = this.getText();
    this.doPuts = true;

    try {
	//EPICS.removeMonitor(putRec,this);
	EPICS.removeMonitor(monRec,this);
      // setup monitor
	//EPICS.addMonitor(putRec,this);
	EPICS.addMonitor(monRec,this);
      this.setToolTipText();
    }
    catch (Exception ee) {
      this.setBackground(Color.red);
    }

  } // end of setEPICSRecords

//-------------------------------------------------------------------------------
  /**
   *  This method allows the user to reset the
   *  EPICS record names and re-establish
   *  monitors to the new record names.
   *@param rec String: Record field
   *@param desc String: Record description
   */
  public void setEPICSRecords(String rec, String desc) {
      this.setEPICSRecords(rec,rec,desc);
  } // end of setEPICSRecords

} //end of class EPICSTextField


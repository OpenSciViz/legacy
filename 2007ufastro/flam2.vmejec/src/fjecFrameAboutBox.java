package uffjec;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.applet.*;


/**
 *Title:      Flamingos 2 Java Engineering Interface
 *Version:
 *Copyright:  Copyright (c) 1999
 *Author:     David Rashkin
 *Company:    University of Florida
 *Description:CanaryCam
 */


//===============================================================================
/**
 * Window to display "About" contents
 */
public class fjecFrameAboutBox extends JDialog implements ActionListener {
  public static final String rcsID = "$Name:  $ $Id: fjecFrameAboutBox.java,v 1.3 2005/02/01 15:50:36 drashkin Exp $";

  JPanel panel1 = new JPanel();
  JPanel panel2 = new JPanel();
  JPanel insetsPanel1 = new JPanel();
  JPanel insetsPanel2 = new JPanel();
  JPanel insetsPanel3 = new JPanel();
  JButton button1 = new JButton();
  JLabel imageControl1 = new JLabel();
  ImageIcon imageIcon;
  BorderLayout borderLayout1 = new BorderLayout();
  BorderLayout borderLayout2 = new BorderLayout();
  FlowLayout flowLayout1 = new FlowLayout();
  FlowLayout flowLayout2 = new FlowLayout();
  GridLayout gridLayout1 = new GridLayout();
  String product = "Flamingos 2 Java Engineering Interface";
  String copyright = "Copyright (c) 2003";
  String comments = "Flamingos 2 is neat";
  String authors = "Written by David Rashkin";


//-------------------------------------------------------------------------------
  /**
   *Constructor from a given parent frame
   *@param parent Frame: parent frame
   */
  public fjecFrameAboutBox(Frame parent) {
      super(parent);
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    try  {
      jbInit();
    }
    catch(Exception e) {
      e.printStackTrace();
    }
    pack();
  } //end of fjecFrameAboutBox


//-------------------------------------------------------------------------------
  /**
   *Overriden so that we can close on system exit
   *@param e TBD
   */
  protected void processWindowEvent(WindowEvent e) {
    super.processWindowEvent(e);
    if(e.getID() == WindowEvent.WINDOW_CLOSING) {
      cancel();
    }
  } //end of processWindowEvent


//-------------------------------------------------------------------------------
  /**
   *Component initialization
   */
  private void jbInit() throws Exception  {
    //imageIcon = new ImageIcon(getClass().getResource("your image name goes here"));
    this.setTitle("About");
    setResizable(false);
    panel1.setLayout(new BorderLayout());
    panel2.setLayout(new BorderLayout());
    insetsPanel1.setLayout(new FlowLayout());
    insetsPanel2.setLayout(new FlowLayout());
    insetsPanel3.setLayout(new GridLayout(5,1));
    insetsPanel2.setBorder(new EmptyBorder(10, 10, 10, 10));
    insetsPanel3.setBorder(new EmptyBorder(10, 60, 10, 10));
    button1.setText("OK");
    button1.addActionListener(this);
    insetsPanel2.add(imageControl1, null);
    panel2.add(insetsPanel2, BorderLayout.WEST);
    insetsPanel3.add(new JLabel(product), null);
    insetsPanel3.add(new JLabel("Version " + version.version), null);
    insetsPanel3.add(new JLabel(copyright), null);
    insetsPanel3.add(new JLabel(comments), null);
    insetsPanel3.add(new JLabel(authors), null);
    panel2.add(insetsPanel3, BorderLayout.CENTER);
    insetsPanel1.add(button1, null);
    panel1.add(panel2, BorderLayout.NORTH);
    panel1.add(insetsPanel3, BorderLayout.CENTER);
    panel1.add(insetsPanel1, BorderLayout.SOUTH);
    this.getContentPane().add(panel1, null);
  } //end of jbInit


//-------------------------------------------------------------------------------
  /**
   *method to dispose (close) this dialog window
   */
  void cancel() {
    dispose();
  } //end of cancel


//-------------------------------------------------------------------------------
  /**
   *ActionPerformed method calls the 'cancel' method when the button is pressed.
   *@param e TBD
   */
  public void actionPerformed(ActionEvent e) {
    if(e.getSource() == button1) {
      cancel();
    }

  } //end of actionPerformed

} //end of class fjecFrameAboutBox


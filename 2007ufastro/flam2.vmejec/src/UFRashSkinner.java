package uffjec;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.lang.Integer;
import java.io.* ;
import java.util.* ;
import java.net.*;
import java.text.* ;
import java.beans.*;
import javax.swing.border.*;
import java.lang.reflect.*;




public class UFRashSkinner {

    JFrame rootContainer;
    JTabbedPane rootTabbedPane;
    
    public UFRashSkinner(JFrame rootContainer, JTabbedPane rootTabbedPane) {
	this.rootContainer = rootContainer;
	this.rootTabbedPane = rootTabbedPane;
    }

    private interface RashkInterface extends ComponentListener {}

    public void writeHashToFile(String filename, Hashtable hash) {
	try{
	    BufferedWriter bw = new BufferedWriter(new FileWriter(filename));
	    bw.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>", 0,"<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>".length() );
	    bw.newLine();
	    bw.write("<theme>");
	    bw.newLine();
	    Enumeration e = hash.keys();
	    while (e.hasMoreElements()) {
		String s = (String)e.nextElement();
		bw.write("<"+s+">",0,s.length()+2); bw.newLine();
		Hashtable ht = (Hashtable) hash.get(s);
		if (ht != null) {
		    Enumeration ee = ht.keys();
		    while (ee.hasMoreElements()) {
			String ss = (String) ee.nextElement();
			bw.write("\t<"+ss+">",0,ss.length()+3); bw.newLine();
			ThemeObject to = (ThemeObject) ht.get(ss);
			if (to != null) {
			    bw.write("\t\t<fg>",0,6);bw.write(to.fg,0,to.fg.length());bw.write("</fg>",0,5); bw.newLine();
			    bw.write("\t\t<bg>",0,6);bw.write(to.bg,0,to.bg.length());bw.write("</bg>",0,5); bw.newLine();
			    if (to.imageFileName != null){
				bw.write("\t\t<imageFileName>",0,17);bw.write(to.imageFileName,0,to.imageFileName.length());bw.write("</imageFileName>",0,16); bw.newLine();
			    }
			    if (to.pressedImageFileName != null){
				bw.write("\t\t<pressedImageFileName>",0,24);bw.write(to.pressedImageFileName,0,to.pressedImageFileName.length());bw.write("</pressedImageFileName>",0,23); bw.newLine();
			    }
			    if (to.selectedImageFileName != null){
				bw.write("\t\t<selectedImageFileName>",0,25);bw.write(to.selectedImageFileName,0,to.selectedImageFileName.length());bw.write("</selectedImageFileName>",0,24); bw.newLine();
			    }
			    if (to.disabledImageFileName != null){
				bw.write("\t\t<disabledImageFileName>",0,25);bw.write(to.disabledImageFileName,0,to.disabledImageFileName.length());bw.write("</disabledImageFileName>",0,24); bw.newLine();
			    }
			    if (to.disabledSelectedImageFileName != null){
				bw.write("\t\t<disabledSelectedImageFileName>",0,33);bw.write(to.disabledSelectedImageFileName,0,to.disabledSelectedImageFileName.length());bw.write("</disabledSelectedImageFileName>",0,32); bw.newLine();
			    }
			    if (to.rolloverImageFileName != null){
				bw.write("\t\t<rolloverImageFileName>",0,25);bw.write(to.rolloverImageFileName,0,to.rolloverImageFileName.length());bw.write("</rolloverImageFileName>",0,24); bw.newLine();
			    }
			    if (to.rolloverSelectedImageFileName != null){
				bw.write("\t\t<rolloverSelectedImageFileName>",0,33);bw.write(to.rolloverSelectedImageFileName,0,to.rolloverSelectedImageFileName.length());bw.write("</rolloverSelectedImageFileName>",0,32); bw.newLine();
			    }
			    if (to.border != null) {
				bw.write("\t\t<border>",0,10);bw.write(to.border,0,to.border.length());bw.write("</border>",0,9); bw.newLine();
			    }
			    bw.write("\t\t<fontName>",0,12);bw.write(to.fontName,0,to.fontName.length());bw.write("</fontName>",0,11); bw.newLine();
			    bw.write("\t\t<fontSize>",0,12);bw.write(to.fontSize,0,to.fontSize.length());bw.write("</fontSize>",0,11); bw.newLine();
			    bw.write("\t\t<fontStyle>",0,13);bw.write(to.fontStyle,0,to.fontStyle.length());bw.write("</fontStyle>",0,12); bw.newLine();
			}
			bw.write("\t</"+ss+">",0,ss.length()+4); bw.newLine();
		    }
		}
		bw.write("</"+s+">",0,s.length()+3); bw.newLine();
	    }
	    bw.write("</theme>");
	    bw.newLine();
	    bw.close();
	} catch (FileNotFoundException fnfe) {
	    System.err.println("fjecFrame.saveTheme> "+fnfe.toString());
	} catch (IOException ioe) {
	    System.err.println("fjecFrame.saveTheme> "+ioe.toString());
	}
    }

    public void showSaveDialog() {
	JFileChooser jfc = new JFileChooser();
	if (jfc.showSaveDialog(rootContainer) == jfc.APPROVE_OPTION) {
	    saveTheme(jfc.getSelectedFile().getAbsolutePath());
	}
    }

    public void saveTheme(String filename) {
	System.out.println("Saving theme "+filename+"...");
	Hashtable hash = new Hashtable();
	recurseSaveTheme(rootContainer,"main",hash);
	writeHashToFile(filename,hash);
    }

    private class ThemeObject {
	public String fg;
	public String bg;
	public String imageFileName;
	public String pressedImageFileName;
	public String selectedImageFileName;
	public String disabledImageFileName;
	public String disabledSelectedImageFileName;
	public String rolloverImageFileName;
	public String rolloverSelectedImageFileName;
	public String fontName;
	public String fontSize;
	public String fontStyle;
	public String border;
	public ThemeObject() { }
	public void put(String key, String value) {
	    if (key.equals("fg")) fg = value;
	    else if (key.equals("bg")) bg = value;
	    else if (key.equals("imageFileName")) imageFileName = value;
	    else if (key.equals("pressedImageFileName")) pressedImageFileName = value;
	    else if (key.equals("selectedImageFileName")) selectedImageFileName = value;
	    else if (key.equals("disabledImageFileName")) disabledImageFileName = value;
	    else if (key.equals("disabledSelectedImageFileName")) disabledSelectedImageFileName = value;
	    else if (key.equals("rolloverImageFileName")) rolloverImageFileName = value;
	    else if (key.equals("rolloverSelectedImageFileName")) rolloverSelectedImageFileName = value;
	    else if (key.equals("fontName")) fontName = value;
	    else if (key.equals("fontSize")) fontSize = value;
	    else if (key.equals("fontStyle")) fontStyle = value;
	    else if (key.equals("border")) border = value;
	}
	public Color getForeground(){
	    StringTokenizer st = new StringTokenizer(fg,":");
	    Color c = new Color(Integer.parseInt(st.nextToken()),Integer.parseInt(st.nextToken()),
				Integer.parseInt(st.nextToken()));
	    return c;
	}
	public Color getBackground(){
	    StringTokenizer st = new StringTokenizer(bg,":");
	    Color c = new Color(Integer.parseInt(st.nextToken()),Integer.parseInt(st.nextToken()),
				Integer.parseInt(st.nextToken()));
	    return c;
	}
	public Font getFont(){
	    try {
		return new Font(fontName,Integer.parseInt(fontStyle),Integer.parseInt(fontSize));
	    } catch (Exception e) {
		System.err.println("fjecFrame.ThemeObject.getFont> "+e.toString());
		return null;
	    }
	}
    }

    public void recurseSaveTheme(Container jp, String title, Hashtable hash) {
	Hashtable ht = (Hashtable)hash.get(title);
	if (ht == null) 
	    ht = new Hashtable();
	if (ht.get(jp.getClass().getName()) == null) {
	    Color fg = jp.getForeground();
	    Color bg = jp.getBackground();
	    Font fnt = jp.getFont();
	    
	    ThemeObject to = new ThemeObject();
	    to.fg = fg.getRed()+":"+fg.getGreen()+":"+fg.getBlue();
	    to.bg = bg.getRed()+":"+bg.getGreen()+":"+bg.getBlue();
	    to.fontName = fnt.getName();
	    to.fontSize = fnt.getSize()+"";
	    to.fontStyle= fnt.getStyle()+"";
	    if (jp instanceof AbstractButton) {
		AbstractButton ab = (AbstractButton)jp;			
		if (ab.getIcon() != null && ab.getIcon() instanceof ImageIcon) {
		    to.imageFileName = new String( ((ImageIcon)ab.getIcon()).getDescription());		    
		}
		if (ab.getPressedIcon() != null && ab.getPressedIcon() instanceof ImageIcon) {
		    to.pressedImageFileName = new String( ((ImageIcon)ab.getPressedIcon()).getDescription());		    
		}
		if (ab.getSelectedIcon() != null && ab.getSelectedIcon() instanceof ImageIcon) {
		    to.selectedImageFileName = new String( ((ImageIcon)ab.getSelectedIcon()).getDescription());		    
		}
		if (ab.getDisabledIcon() != null && ab.getDisabledIcon() instanceof ImageIcon) {
		    to.disabledImageFileName = new String( ((ImageIcon)ab.getDisabledIcon()).getDescription());		    
		}
		if (ab.getDisabledSelectedIcon() != null && ab.getDisabledSelectedIcon() instanceof ImageIcon) {
		    to.disabledSelectedImageFileName = new String( ((ImageIcon)ab.getDisabledSelectedIcon()).getDescription());		    
		}
		if (ab.getRolloverIcon() != null && ab.getRolloverIcon() instanceof ImageIcon) {
		    to.rolloverImageFileName = new String( ((ImageIcon)ab.getRolloverIcon()).getDescription());		    
		}
		if (ab.getRolloverSelectedIcon() != null && ab.getRolloverSelectedIcon() instanceof ImageIcon) {
		    to.rolloverSelectedImageFileName = new String( ((ImageIcon)ab.getRolloverSelectedIcon()).getDescription());		    
		}
	    }
	    if (jp instanceof JComponent) {
		Border bor = ((JComponent)jp).getBorder();
		if (bor instanceof EtchedBorder || bor instanceof BevelBorder) 
		    to.border = bor.getClass().getName();
	    }
	    ht.put(jp.getClass().getName(), to);
	    hash.put(title,ht);
	}
	Component [] cmp = jp.getComponents();
	for (int i=0; i<cmp.length; i++) {
	    if (cmp[i] instanceof Container) {
		Container _jp = (Container) cmp[i];
		if (jp instanceof JTabbedPane) {
		    recurseSaveTheme(_jp,((JTabbedPane)jp).getTitleAt(i),hash);
		} else
		    recurseSaveTheme(_jp,title,hash);
	    }		    
	}
    }

    public String readXMLTag(BufferedReader br) {
	try {
	    String s="";
	    char [] ch = new char[1];
	    while (br.ready() && ch[0] != '<') br.read(ch,0,1);
	    s += ch[0];
	    while (br.ready() && !s.endsWith(">")){
		br.read(ch,0,1);
		s += ch[0];
	    }
	    return s.trim().substring(1,s.trim().length()-1);
	} catch (Exception e) {
	    System.err.println("fjecFrame.readXMLTag> "+e.toString());
	    return null;
	}
    }

    public String readXMLData(BufferedReader br) {
	try {
	    String s = "";
	    char [] ch = new char[1];
	    while (br.ready() && !s.endsWith("<")) {
		br.mark(1);
		br.read(ch,0,1);
		s += ch[0];
	    }
	    br.reset();
	    return s.trim().substring(0,s.trim().length()-1);
	} catch(Exception e) {
	    System.err.println("fjecFrame.readXMLData> "+e.toString());
	    return null;
	}
    }

    public boolean complimentXMLTags(String tag1, String tag2) {
	//if ( tag1.equals(tag2) || tag2.equals(tag1)) return true;
	if ( tag2.equals("/"+tag1)) return true;
	else return false;
    }

    public void showLoadDialog() {
	JFileChooser jfc = new JFileChooser();
	if (jfc.showOpenDialog(rootContainer) == jfc.APPROVE_OPTION) {
	    loadTheme(jfc.getSelectedFile().getAbsolutePath());
	}
    }

    public void loadTheme(String filename) {
	try {
	    Hashtable hash = new Hashtable();
	    BufferedReader br = new BufferedReader(new FileReader(filename));
	    readXMLTag(br); // skip over xml spec.
	    String rootTag = readXMLTag(br); // skip over root tag
	    String paneTag = readXMLTag(br);		
	    while (br.ready() && !complimentXMLTags(rootTag,paneTag)) {
		Hashtable ht = new Hashtable();
		String componentTag = readXMLTag(br);		
		while (br.ready() && !complimentXMLTags(paneTag,componentTag)) {
		    ThemeObject to = new ThemeObject();
		    if (complimentXMLTags(paneTag,componentTag)) continue;
		    String nextTag = readXMLTag(br);
		    while (br.ready() && !complimentXMLTags(componentTag,nextTag)) {
			String data = readXMLData(br);
			String compTag = readXMLTag(br);
			if (!complimentXMLTags(nextTag,compTag))
			    System.err.println("fjecFrame.loadTheme> Non-matching XML tags encountered: "+nextTag+","+compTag);
			to.put(nextTag,data);
			nextTag = readXMLTag(br);
		    }
		    ht.put(componentTag,to);
		    componentTag = readXMLTag(br);
		}
		hash.put(paneTag,ht);
		paneTag = readXMLTag(br);
	    }
	    recurseTheme(rootContainer,"main",hash);
	} catch (Exception e) {
	    System.err.println("fjecFrame.loadTheme> "+e.toString());
	}
    }

    public void applyButtonIcons(AbstractButton ab, String [] iconFileNames) {
	if (ab instanceof JMenuItem || ab instanceof JCheckBox) return;
	if (iconFileNames.length != 7) {
	    System.err.println("UFRashSkinner.applyButtonIcons> Incorrect number of iconFileNames!");
	    return;
	}
	final String [] fn = new String[7];
	for (int i=0; i<7; i++)
	    fn[i] = new String(iconFileNames[i]);
	//first remove all other rashkinterfaces
	ComponentListener [] cl = ab.getComponentListeners();
	for (int i=0; i<cl.length; i++)
	    if (cl[i] instanceof RashkInterface)
		ab.removeComponentListener(cl[i]);


	ab.setHorizontalTextPosition(SwingConstants.CENTER);
		 
	//this is necessary to prevent the component from continuing to grow 
	//as it's icon gets set
	if (ab.getParent().getLayout() instanceof FlowLayout || ab.getParent().getLayout() instanceof BorderLayout) {
	    ab.setMargin(new Insets(0,0,0,0));
	    ab.setBorder(null);
	}
	
	int w = ab.getWidth(), h = ab.getHeight();
	if (ab.getWidth() > 0 && ab.getHeight() > 0) {
	    ab.setIcon( new ImageIcon((new ImageIcon(fn[0])).getImage().getScaledInstance(w,h,Image.SCALE_DEFAULT),fn[0]));
	    ab.setPressedIcon( new ImageIcon((new ImageIcon(fn[1])).getImage().getScaledInstance(w,h,Image.SCALE_DEFAULT),fn[1]));
	    ab.setSelectedIcon( new ImageIcon((new ImageIcon(fn[2])).getImage().getScaledInstance(w,h,Image.SCALE_DEFAULT),fn[2]));
	    ab.setDisabledIcon( new ImageIcon((new ImageIcon(fn[3])).getImage().getScaledInstance(w,h,Image.SCALE_DEFAULT),fn[3]));
	    ab.setDisabledSelectedIcon( new ImageIcon((new ImageIcon(fn[4])).getImage().getScaledInstance(w,h,Image.SCALE_DEFAULT),fn[4]));
	    ab.setRolloverIcon( new ImageIcon((new ImageIcon(fn[5])).getImage().getScaledInstance(w,h,Image.SCALE_DEFAULT),fn[5]));
	    ab.setRolloverSelectedIcon( new ImageIcon((new ImageIcon(fn[6])).getImage().getScaledInstance(w,h,Image.SCALE_DEFAULT),fn[6]));
	}
	final AbstractButton fab = ab;
	ab.addComponentListener(new RashkInterface() {
		public void componentHidden(ComponentEvent e) {}
		public void componentMoved(ComponentEvent e) {}
		public void componentShown(ComponentEvent e) {}
		public void componentResized(ComponentEvent e) {
		    if (fab.getWidth() > 0 && fab.getHeight() > 0) {
			fab.setIcon( new ImageIcon((new ImageIcon(fn[0])).getImage().
						  getScaledInstance(fab.getWidth(),fab.getHeight(),Image.SCALE_DEFAULT),fn[0]));
			fab.setPressedIcon( new ImageIcon((new ImageIcon(fn[1])).getImage().
						  getScaledInstance(fab.getWidth(),fab.getHeight(),Image.SCALE_DEFAULT),fn[1]));
			fab.setSelectedIcon( new ImageIcon((new ImageIcon(fn[2])).getImage().
						  getScaledInstance(fab.getWidth(),fab.getHeight(),Image.SCALE_DEFAULT),fn[2]));
			fab.setDisabledIcon( new ImageIcon((new ImageIcon(fn[3])).getImage().
						  getScaledInstance(fab.getWidth(),fab.getHeight(),Image.SCALE_DEFAULT),fn[3]));
			fab.setDisabledSelectedIcon( new ImageIcon((new ImageIcon(fn[4])).getImage().
						  getScaledInstance(fab.getWidth(),fab.getHeight(),Image.SCALE_DEFAULT),fn[4]));
			fab.setRolloverIcon( new ImageIcon((new ImageIcon(fn[5])).getImage().
						  getScaledInstance(fab.getWidth(),fab.getHeight(),Image.SCALE_DEFAULT),fn[5]));
			fab.setRolloverSelectedIcon( new ImageIcon((new ImageIcon(fn[6])).getImage().
						  getScaledInstance(fab.getWidth(),fab.getHeight(),Image.SCALE_DEFAULT),fn[6]));
		    }
		}
	    });
	
    }

    public void applyButtonIcons(AbstractButton ab, JLabel [] iconFileNames) {
	if (iconFileNames.length != 7) {
	    System.err.println("UFRashSkinner.applyButtonIcons> Incorrect number of iconFileNames!");
	    return;
	}
	String [] s = new String[7];
	for (int i=0; i<7; i++) s[i] = iconFileNames[i].getText();
	applyButtonIcons(ab,s);
    }

    public void recurseTheme(Container jp, String title,  Hashtable hash) {
	Hashtable ht = (Hashtable) hash.get(title);
	if (ht != null && jp != null){ 
	    ThemeObject tho = (ThemeObject)ht.get(jp.getClass().getName());
	    if (tho != null) {
		jp.setForeground(tho.getForeground());
		jp.setBackground(tho.getBackground());
		jp.setFont(tho.getFont());
		if (jp instanceof AbstractButton && !(jp instanceof JCheckBox) && !(jp instanceof JMenuItem) ){
		    final AbstractButton c = (AbstractButton)jp;
		    String [] iconNames = new String[7];
		    for (int i=0; i<7; i++) iconNames[i] = "Null";
		    if (tho.imageFileName != null) iconNames[0] = tho.imageFileName;
		    if (tho.pressedImageFileName != null) iconNames[1] = tho.pressedImageFileName;
		    if (tho.selectedImageFileName != null) iconNames[2] = tho.selectedImageFileName;
		    if (tho.disabledImageFileName != null) iconNames[3] = tho.disabledImageFileName;
		    if (tho.disabledSelectedImageFileName != null) iconNames[4] = tho.disabledSelectedImageFileName;
		    if (tho.rolloverImageFileName != null) iconNames[5] = tho.rolloverImageFileName;
		    if (tho.rolloverSelectedImageFileName != null) iconNames[6] = tho.rolloverSelectedImageFileName;
		
		    applyButtonIcons(c,iconNames);
	    
		    /*
		      c.setHorizontalTextPosition(SwingConstants.CENTER);
		 
		      //this is necessary to prevent the component from continuing to grow 
		      //as it's icon gets set
		      if (jp.getParent().getLayout() instanceof FlowLayout) {
		      c.setMargin(new Insets(0,0,0,0));
		      c.setBorder(null);
		      }
		      int w = c.getWidth(), h = c.getHeight();
		      if (c.getWidth() > 0 && c.getHeight() > 0) 
		      c.setIcon( new ImageIcon((new ImageIcon(fn)).getImage().getScaledInstance(w,h,Image.SCALE_DEFAULT),fn));
		      c.addComponentListener(new ComponentListener() {
		      public void componentHidden(ComponentEvent e) {}
		      public void componentMoved(ComponentEvent e) {}
		      public void componentShown(ComponentEvent e) {}
		      public void componentResized(ComponentEvent e) {
		      if (c.getWidth() > 0 && c.getHeight() > 0) {
		      c.setIcon( new ImageIcon((new ImageIcon(fn)).getImage().getScaledInstance(c.getWidth(),c.getHeight(),Image.SCALE_DEFAULT),fn));
		      }
		      }
		      });
		    */
		}
	    
	    }
	}
	Component [] cmp = jp.getComponents();
	if (cmp != null)
	for (int i=0; i<cmp.length; i++) {
	    if (cmp[i] != null && cmp[i] instanceof Container) {
		if (jp != null && jp instanceof JTabbedPane)
		    recurseTheme((Container)cmp[i],((JTabbedPane)jp).getTitleAt(i),hash);
		else
		    recurseTheme((Container)cmp[i],title,hash);
	    }
	}
    }



    public void recurseRashkIntense(Container jp, String component) {
	Component [] cmp = jp.getComponents();
	for (int i=0; i<cmp.length; i++) {
	    try {		
		if (cmp[i] instanceof JPanel || cmp[i] instanceof JComboBox || cmp[i] instanceof JCheckBox || cmp[i] instanceof JMenuBar
		    || cmp[i] instanceof JMenuItem || cmp[i] instanceof JMenu || cmp[i] instanceof JLabel) {
		    cmp[i].setBackground(new Color(0x10,0x20,0x40));
		    cmp[i].setForeground(new Color(0xFF,0xFF,0xFF));
		}
		if (cmp[i] instanceof JTabbedPane) {
		    cmp[i].setBackground(new Color(0x10,0x20,0x40));
		    cmp[i].setForeground(new Color(0x40,0x80,0x40));
		}
		if (cmp[i] instanceof JComponent && ((JComponent)cmp[i]).getBorder() instanceof javax.swing.border.EtchedBorder) {
		    ((JComponent)cmp[i]).setBorder(new javax.swing.border.BevelBorder(javax.swing.border.BevelBorder.RAISED));
		}
		if (cmp[i] instanceof AbstractButton && !(cmp[i] instanceof JCheckBox) && !(cmp[i] instanceof JMenuItem) &&
		    !( cmp[i].getParent().getLayout() instanceof FlowLayout)) {
		    final AbstractButton c = (AbstractButton)cmp[i];
		    c.setHorizontalTextPosition(SwingConstants.CENTER);
		    c.setForeground(Color.blue);
		    if (c.getWidth() > 0 && c.getHeight() > 0)
			c.setIcon( new ImageIcon((new ImageIcon("images/button2.jpg")).getImage().
						 getScaledInstance(c.getWidth(),c.getHeight(),Image.SCALE_DEFAULT),"images/button2.jpg"));
		    c.addComponentListener(new ComponentListener() {
			    public void componentHidden(ComponentEvent e) {}
			    public void componentMoved(ComponentEvent e) {}
			    public void componentShown(ComponentEvent e) {}
			    public void componentResized(ComponentEvent e) {
				if (c.getWidth() > 0 && c.getHeight() > 0)
				    c.setIcon( new ImageIcon((new ImageIcon("images/button2.jpg")).getImage().
							     getScaledInstance(c.getWidth(),c.getHeight(),Image.SCALE_DEFAULT),"images/button2.jpg"));
			    }
			});
		}
		Container _jp = (Container) cmp[i];
		recurseRashkIntense(_jp,component);
	    }
	    catch(ClassCastException cce) {
	    }
	    catch(Exception e) {
		fjecError.show(e.toString());
	    }
	}	
    }

    
    public void recurseFont(Container jp, Font f, String component) {
	Component [] cmp = jp.getComponents();
	for (int i=0; i<cmp.length; i++) {
	    try {		
		if (component.equals("All") || cmp[i].getClass().getName().indexOf(component) != -1)  
		    cmp[i].setFont(f);
		Container _jp = (Container) cmp[i];
		recurseFont(_jp,f,component);
	    }
	    catch(ClassCastException cce) {
	    }
	    catch(Exception e) {
		fjecError.show(e.toString());
	    }
	}
    } //end of recurseLock

    public void recurseColor(Component jp, Color fg, Color bg, String component) {
	if (component.equals("All") || jp.getClass().getName().indexOf(component) != -1) { 
	    jp.setForeground(fg);
	    jp.setBackground(bg);
	}	
	if (jp instanceof Container) {
	    Component [] cmp = ((Container)jp).getComponents();
	    for (int i=0; i<cmp.length; i++) {
		recurseColor(cmp[i],fg,bg,component);
	    }
	}
    } //end of recurseLock

    public void recurseIcon(Component jp, JLabel [] iconFileNames, String component) {
	if (component.equals("All") || jp.getClass().getName().indexOf(component) != -1) { 
	    if (jp instanceof AbstractButton)
		applyButtonIcons((AbstractButton)jp,iconFileNames);
	}else if ((component.indexOf("ComboBox") != -1)	&& jp.getParent()!=null &&
		  (jp.getParent().getClass().getName().indexOf(component) != -1)){
	    if (jp instanceof AbstractButton)
		applyButtonIcons((AbstractButton)jp,iconFileNames);
	}
	if (jp instanceof Container) {
	    Component [] cmp = ((Container)jp).getComponents();
	    for (int i=0; i<cmp.length; i++) {
		recurseIcon(cmp[i],iconFileNames,component);
	    }
	}
    } //end of recurseLock
    
    void change_fonts() {
	//String [] strs = new String[jTabbedPane.getTabCount()+1];
    }


    public void showThemeWindow() {
	final JDialog jd = new JDialog(rootContainer);
	
	final JTabbedPane jtp = new JTabbedPane();
	final JPanel fontPanel = new JPanel();
	final JPanel iconPanel = new JPanel();
	final JPanel colorPanel = new JPanel();
	jtp.add("Fonts",fontPanel);
	jtp.add("Icons",iconPanel);
	jtp.add("Colors",colorPanel);
	
	Vector v = new Vector();
	v.add("Global");
	for (int i=0; i<rootTabbedPane.getTabCount(); i++) {
	    v.add(rootTabbedPane.getTitleAt(i));
	    if (rootTabbedPane.getComponentAt(i) instanceof JTabbedPane) {
		JTabbedPane _jtp = (JTabbedPane)rootTabbedPane.getComponentAt(i);
		for (int k = 0; k < _jtp.getTabCount(); k ++) 
		    v.add("-> "+_jtp.getTitleAt(k));
	    }
	}
    
	

	String [] strs2 = { "All", "JTextField", "EPICSTextField", "JLabel", "EPICSLabel", "JButton", "EPICSApplyButton", "JComboBox", "EPICSComboBox",
			    "JCheckBox", "JRadioButton", "JList","JTabbedPane" };


	//Font Panel

	final JComboBox fontBox1 = new JComboBox(v);
	final JComboBox fontBox2 = new JComboBox(strs2);
	String [] fontNames = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
	final JComboBox fontBox3 = new JComboBox(fontNames);
	final JTextField fontField1 = new JTextField("12",5);
	String []strs3 = { "Plain", "Bold", "Italic"};
	final JComboBox fontBox4 = new JComboBox(strs3);
	final JTextField previewField = new JTextField("ASDFGHJKL:asdfjkl;1234!@#$");
	fontBox3.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    try {
			previewField.setFont(new Font((String)fontBox3.getSelectedItem(),
						      fontBox4.getSelectedIndex(),
						      Integer.parseInt(fontField1.getText())));
		    } catch (Exception e) {
			//System.err.println("fjecFrame.change_fonts> "+e.toString());
		    }
		}
	    });
	fontBox4.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    try {
			previewField.setFont(new Font((String)fontBox3.getSelectedItem(),
						      fontBox4.getSelectedIndex(),
						      Integer.parseInt(fontField1.getText())));
		    } catch (Exception e) {
			//System.err.println("fjecFrame.change_fonts> "+e.toString());
		    }
		}
	    });
	fontField1.addCaretListener(new CaretListener() {
		public void caretUpdate(CaretEvent ae) {
		    try {
			previewField.setFont(new Font((String)fontBox3.getSelectedItem(),
						      fontBox4.getSelectedIndex(),
						      Integer.parseInt(fontField1.getText())));
		    } catch (Exception e) {
			//System.err.println("fjecFrame.change_fonts> "+e.toString());
		    }
		}
	    });
	try {
	    previewField.setFont(new Font((String)fontBox3.getSelectedItem(),
					  fontBox4.getSelectedIndex(),
					  Integer.parseInt(fontField1.getText())));
	} catch (Exception e) {System.err.println("fjecFrame.change_fonts> "+e.toString());}
	fontPanel.setLayout(new RatioLayout());
	fontPanel.add("0.01,0.01;0.60,0.10",new JLabel("Where to apply font changes: "));
	fontPanel.add("0.61,0.01;0.39,0.10",fontBox1);
	fontPanel.add("0.01,0.21;0.60,0.10",new JLabel("Which components to apply font changes to:"));
	fontPanel.add("0.61,0.21;0.39,0.10",fontBox2);
	fontPanel.add("0.01,0.41;0.10,0.10",new JLabel("New Font: "));
	fontPanel.add("0.11,0.41;0.25,0.10",fontBox3);
	fontPanel.add("0.37,0.41;0.10,0.10",new JLabel("Size:"));
	fontPanel.add("0.47,0.41;0.20,0.10",fontField1);
	fontPanel.add("0.68,0.41;0.10,0.10",new JLabel("Style:"));
	fontPanel.add("0.78,0.41;0.22,0.10",fontBox4);
	fontPanel.add("0.01,0.61;0.10,0.10",new JLabel("Preview:"));
	fontPanel.add("0.11,0.61;0.89,0.20",previewField);


	//Icon Panel

	final JComboBox iconBox1 = new JComboBox(v);
	String []strs4 = {"All","JButton","EPICSApplyButton","JComboBox","EPICSComboBox"};
	final JComboBox iconBox2 = new JComboBox(strs4);
	final JButton [] iconButton = new JButton[7];
	final JLabel [] iconFileName = new JLabel[7];
	final JButton previewButton = new JButton("Preview");
	for (int i=0; i<7; i++) { 
	    final int j = i;
	    iconButton[i] = new JButton("..."); iconFileName[i] = new JLabel("Null");
	    iconButton[i].setMargin(new Insets(0,0,0,0));
	    iconButton[i].addActionListener(new ActionListener(){
		    public void actionPerformed(ActionEvent ae) {
			JFileChooser jfc = new JFileChooser();
			if (jfc.showOpenDialog(null) == jfc.APPROVE_OPTION) {
			    iconFileName[j].setText(jfc.getSelectedFile().getAbsolutePath());
			    applyButtonIcons(previewButton,iconFileName);
			}
		    }
		});
	}
	iconPanel.setLayout(new RatioLayout());
	iconPanel.add("0.01,0.01;0.60,0.10",new JLabel("Where to apply icon changes: "));
	iconPanel.add("0.61,0.01;0.39,0.10",iconBox1);
	iconPanel.add("0.01,0.21;0.60,0.10",new JLabel("Which components to apply icon changes to:"));
	iconPanel.add("0.61,0.21;0.39,0.10",iconBox2);
	iconPanel.add("0.01,0.31;0.40,0.10",new JLabel("Normal Icon Image:"));
	iconPanel.add("0.01,0.41;0.40,0.10",new JLabel("Pressed Icon Image:"));
	iconPanel.add("0.01,0.51;0.40,0.10",new JLabel("Selected Icon Image:"));
	iconPanel.add("0.01,0.61;0.40,0.10",new JLabel("Disabled Icon Image:"));
	iconPanel.add("0.01,0.71;0.40,0.10",new JLabel("Disabled Selected Icon Image:"));
	iconPanel.add("0.01,0.81;0.40,0.10",new JLabel("Rollover Icon Image:"));
	iconPanel.add("0.01,0.91;0.40,0.10",new JLabel("Rollover Selected Icon Image:"));
	for (int i=0; i<7; i++) {
	    String placement = "0.41,0."+(i+3)+"1;0.30,0.10";
	    iconPanel.add(placement,iconFileName[i]);
	    placement = "0.71,0."+(i+3)+"1;0.05,0.10";
	    iconPanel.add(placement,iconButton[i]);
	}
	iconPanel.add("0.81,0.65;0.14,0.15",previewButton);


	//Color Panel

	final JComboBox colorBox1 = new JComboBox(v);
	final JComboBox colorBox2 = new JComboBox(strs2);

	final JButton colorField1 = new JButton();
	colorField1.setBackground(Color.blue);
	final JButton colorField2 = new JButton();
	colorField2.setBackground(Color.orange);

	colorField1.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    try {
			Color c = JColorChooser.showDialog(colorField1,"Foreground Color",colorField1.getBackground());
			if (c != null)
			    colorField1.setBackground(c);
		    } catch (Exception e) {
			System.err.println("UFRashSkinner.showThemeWindow> "+e.toString());
		    }
		}
	    });
	colorField2.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    try {
			Color c = JColorChooser.showDialog(colorField2,"Background Color",colorField2.getBackground());
			if (c != null)
			    colorField2.setBackground(c);
		    } catch (Exception e) {
			System.err.println("UFRashSkinner.showThemeWindow> "+e.toString());
		    }
		}
	    });

	colorPanel.setLayout(new RatioLayout());
	colorPanel.add("0.01,0.01;0.60,0.10",new JLabel("Where to apply color changes: "));
	colorPanel.add("0.61,0.01;0.39,0.10",colorBox1);
	colorPanel.add("0.01,0.21;0.60,0.10",new JLabel("Which components to apply color changes to:"));
	colorPanel.add("0.61,0.21;0.39,0.10",colorBox2);
	colorPanel.add("0.01,0.41;0.15,0.10",new JLabel("Foreground:"));
	colorPanel.add("0.16,0.41;0.10,0.10",colorField1);
	colorPanel.add("0.37,0.41;0.15,0.10",new JLabel("Background:"));
	colorPanel.add("0.53,0.41;0.10,0.10",colorField2);



	final JButton applyButton = new JButton("Apply Fonts");
	applyButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    try {
			String containerName = null;
			String componentName = null;
			if (jtp.getSelectedComponent().equals(fontPanel)) {
			    containerName = (String)fontBox1.getSelectedItem();
			    componentName = (String)fontBox2.getSelectedItem();			
			} else if (jtp.getSelectedComponent().equals(iconPanel)) {
			    containerName = (String)iconBox1.getSelectedItem();
			    componentName = (String)iconBox2.getSelectedItem();			
			} else if (jtp.getSelectedComponent().equals(colorPanel)) {
			    containerName = (String)colorBox1.getSelectedItem();
			    componentName = (String)colorBox2.getSelectedItem();			
			} else {
			    System.err.println("UFRashSkinner.showThemeWindow> I have no knowledge of the panel you selected.");
			    return;
			}			
			Container c = null;
			if (containerName.equals("Global")) 
			    c = rootContainer;
			else {
			    for (int i=0; i<rootTabbedPane.getTabCount(); i++) {
				if (rootTabbedPane.getTitleAt(i).equals(containerName)) {
				    c = (Container) rootTabbedPane.getComponentAt(i);
				    i = Integer.MAX_VALUE-1;
				    break;
				}
				if (rootTabbedPane.getComponentAt(i) instanceof JTabbedPane) {
				    JTabbedPane jtp = (JTabbedPane)rootTabbedPane.getComponentAt(i);
				    for (int k = 0; k < jtp.getTabCount(); k ++) 
					if ( ("-> "+jtp.getTitleAt(k)).equals(containerName) ) {
					    c = (Container) jtp.getComponentAt(k);
					    i = Integer.MAX_VALUE-1;
					    break;
					}
				}
			    }
			}
			if (c == null) {
			    System.err.println("fjecFrame.change_fonts> Could not find component named "
					       + containerName);
			} else {
			    if (jtp.getSelectedComponent().equals(fontPanel)) {
				Font f = new Font((String)fontBox3.getSelectedItem(),
						  fontBox4.getSelectedIndex(),
						  Integer.parseInt(fontField1.getText()));
				recurseFont(c,f,componentName);
			    } else if (jtp.getSelectedComponent().equals(iconPanel)) {
				recurseIcon(c,iconFileName,componentName);
			    } else if (jtp.getSelectedComponent().equals(colorPanel)) {
				Color fg = colorField1.getBackground();
				Color bg = colorField2.getBackground();
				recurseColor(c,fg,bg,componentName);
			    } else {
				System.err.println("UFRashSkinner.showThemeWindow> Somehow I have lost knowledge of the panel you selected.");
				return;
			    }
			}
		    } catch (Exception e) {
			System.err.println("fjecFrame.change_fonts> "+e.toString());
		    }
		    //jd.dispose();
		}
	    });

	JButton doneButton = new JButton("Done");
	doneButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    jd.dispose();
		}
	    });

	jtp.addChangeListener(new ChangeListener(){
		public void stateChanged(ChangeEvent ce) {
		    String s = jtp.getTitleAt(jtp.getSelectedIndex());
		    applyButton.setText("Apply "+s);
		}
	    });


	JPanel buttonPanel = new JPanel();
	buttonPanel.add(applyButton);
	buttonPanel.add(doneButton);

	JPanel mainPan = new JPanel();
	mainPan.setLayout(new BorderLayout());
	mainPan.add(jtp,BorderLayout.CENTER);
	mainPan.add(buttonPanel,BorderLayout.SOUTH);
	//jd.add("0.25,0.90;0.15,0.10",applyButton);
	//jd.add("0.65,0.90;0.15,0.10",doneButton);


	jd.setSize(600,300);
	jd.setLocation(rootContainer.getLocationOnScreen());
	jd.setContentPane(mainPan);       
	jd.setVisible(true);
    }



}

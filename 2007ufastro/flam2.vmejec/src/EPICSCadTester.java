package uffjec;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import ufjca.*;

public class EPICSCadTester extends JPanel implements UFCAToolkit.MonitorListener {

    JTextField cadNameField;
    JTextField carNameField;
    JLabel monitorLabel;
    EPICSCarCallback carCallback;
    String carName;

    JRadioButton cadType1;
    JRadioButton cadType2;
    JRadioButton cadType3;

    JPanel fieldPanel;

    public void startTest() {
	String s = cadNameField.getText();
	if (s != null && !s.trim().equals("")){
	    if (s.indexOf(".") == -1) {
		s += ".DIR";
	    }
	    if (carName != null && !carName.equals(""))
		EPICS.removeMonitor(carName,this);
	    carName = carNameField.getText();
	    System.out.println("EPICSCadTester.startTest> Cad name: "+s);
	    System.out.println("EPICSCadTester.startTest> Car name: "+carName);
	    EPICS.put(s,EPICS.START);
	    if (carName != null && !carName.equals(""))
		EPICS.addMonitor(carName,this);
	}
    }


    public void populateFieldPanel(String cadName) {
	char [] fieldNames = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T'};
	fieldPanel.removeAll();
	int numFields = 0;
	if (cadType1.isSelected()) 
	    numFields = 10;
	else if (cadType2.isSelected())
	    numFields = 15;
	else if (cadType3.isSelected())
	    numFields = 20;
	else {
	    System.err.println("EPICSCadTester.populateFieldPanel> No Radio button selected!");
	    return;
	}
	fieldPanel.add("0.01,0.01;0.25,0.04",new JLabel(".DIR",JLabel.CENTER));
	fieldPanel.add("0.26,0.01;0.25,0.04",new EPICSTextField(cadName+".DIR",""));
	fieldPanel.add("0.51,0.01;0.25,0.04",new JLabel(".VAL",JLabel.CENTER));
	fieldPanel.add("0.76,0.01;0.25,0.04",new EPICSLabel(cadName+".VAL",""));
	for (int i=0; i<numFields; i++) {
	    fieldPanel.add("0.01,"+(0.04*(i+1))+";0.25,0.04",new JLabel("."+fieldNames[i]+":",JLabel.CENTER));
	    fieldPanel.add("0.26,"+(0.04*(i+1))+";0.25,0.04",new EPICSTextField(cadName+"."+fieldNames[i],""));
	    fieldPanel.add("0.51,"+(0.04*(i+1))+";0.25,0.04",new JLabel(".VAL"+fieldNames[i]+":",JLabel.CENTER));
	    fieldPanel.add("0.76,"+(0.04*(i+1))+";0.25,0.04",new EPICSLabel(cadName+".VAL"+fieldNames[i],""));
	}

	fieldPanel.revalidate();
	repaint();
    }
    

    public EPICSCadTester () {
	setLayout(new RatioLayout());
	fieldPanel = new JPanel();
	fieldPanel.setLayout(new RatioLayout());
	cadNameField = new JTextField();
	carNameField = new JTextField();
	monitorLabel = new JLabel();
	
	cadType1 = new JRadioButton("Type 1",true);
	cadType2 = new JRadioButton("Type 2");
	cadType3 = new JRadioButton("Type 3");
	ButtonGroup bg = new ButtonGroup();
	bg.add(cadType1);
	bg.add(cadType2);
	bg.add(cadType3);

	String [] strs = {"MARK","CLEAR","PRESET","START"};
	JComboBox jcb = new JComboBox(strs);
	JButton startButton = new JButton("Start");
	startButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    startTest();
		}
	    });
	JButton populateButton = new JButton("Setup Monitors");
	populateButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    populateFieldPanel(cadNameField.getText());
		}
	    });
	
	add("0.01,0.01;0.10,0.05",new JLabel("Cad Name:"));
	add("0.11,0.01;0.10,0.05",cadNameField);
	add("0.01,0.11;0.10,0.05",new JLabel("Car Name:"));
	add("0.11,0.11;0.10,0.05",carNameField);
	add("0.01,0.21;0.10,0.05",new JLabel("Car Value:"));
	add("0.11,0.21;0.10,0.05",monitorLabel);
	add("0.11,0.41;0.10,0.05",populateButton);
	add("0.41,0.01;0.10,0.05",cadType1);
	add("0.51,0.01;0.10,0.05",cadType2);
	add("0.61,0.01;0.10,0.05",cadType3);
	add("0.41,0.16;0.40,0.80",fieldPanel);
	
    }

    public void reconnect(String dbname) {
	//prefixLabel = EPICS.prefix;
	//remove(prefixLabel);
	//add("",prefixLabel);
    }

    public void monitorChanged(String val) {
	monitorLabel.setText(val);
    }

    public static void main(String [] args) {
	EPICS.prefix = "foo:";
	if (args != null && args.length > 0) {
	    EPICS.prefix = args[0];
	}
	EPICS.setupStaticVars();
	JFrame j = new JFrame();
	j.setContentPane(new EPICSCadTester());
	j.setSize(800,600);
	j.setDefaultCloseOperation(3);
	j.setVisible(true);
	UFCAToolkit.startMonitorLoop();
    }

}

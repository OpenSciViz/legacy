package uffjec;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

//===============================================================================
  /**
   *Handles the Graph on the JPanelTemperature
   */
public  class JGraphPanel extends JPanel {
    // array of linked lists to store data points for the sensors.
    LinkedList [] xValues;
    LinkedList [] yValues;
    //number of points currently stored.
    int size;
    long currTime;
    //maximum number of points
    int panelWidth, panelHeight, panelMinX, panelMinY;
    double timeMin, timeMax, tempMin, tempMax;
    double xTickDelta, xLabelDelta, yTickDelta, yLabelDelta;
    int pollPeriod;
    TemperatureSensorParameter [] SensorParms;
    boolean drawGrid;
    boolean autoscale;

//-------------------------------------------------------------------------------
    /**
     *Constructor Function for JGraphPanel
     */
    JGraphPanel (TemperatureSensorParameter[] SensorParms) {
      this.SensorParms = SensorParms;
      drawGrid = true;
      autoscale = true;
      xValues = new LinkedList[SensorParms.length];
      yValues = new LinkedList[SensorParms.length];
      size = 0; 
      xTickDelta = 60; yTickDelta = 10;
      xLabelDelta = 120; yLabelDelta = 20;
      panelMinX = 10; panelMinY = 10;
      tempMin = 0; tempMax = 100;
      timeMin = 0; timeMax = 600;
      pollPeriod = 5;
      for (int i=0; i<SensorParms.length; i++) {
        xValues[i] = new LinkedList();
        yValues[i] = new LinkedList();
      }
    } //end of JGraphPanel constructor fn

//-------------------------------------------------------------------------------
/**
 *Sets whether to draw grid on graph or not
 */

    public void setDrawGrid(boolean drawGrid) {
	this.drawGrid = drawGrid;
    }
//-------------------------------------------------------------------------------
/**
 *Sets whether to autoscale an empty graph
 */

    public void setAutoscale(boolean autoscale) {
	this.autoscale = autoscale;
    }

//-------------------------------------------------------------------------------
      /**
       *Clears the xValues array and the yValues array for the graph data
       */
      public void eraseGraphData () {
         size = 0;
         for (int i=0; i<SensorParms.length; i++) {
           xValues[i].clear();
           yValues[i].clear();
         }
      } //end of eraseGraphData

//-------------------------------------------------------------------------------
      /**
       *reverses ??
       *@param arrayit TBD
       */
      public int[] reverse (int [] arrayit) {
        int [] temper = new int[arrayit.length];
        int k = arrayit.length-1;
        for (int i=0; i<arrayit.length; i++) {
            temper[k--] = arrayit[i];
        }
        return temper;
      } //end of reverse

//-------------------------------------------------------------------------------
      /**
       *TBD
       *@param x double x coordinate
       *@param y double y coordinate
       */
      public Point xform(double x, double y) {
         final double scr_org_x = panelWidth;
         final double scr_org_y = panelHeight;
         final double scr_x_per_x = -( panelWidth-panelMinX) / (timeMax-timeMin);
         final double scr_y_per_y = -( panelHeight-panelMinY)/ (tempMax-tempMin);
         y -= tempMin; x -= timeMin;
         Point scr_loc = new Point();
         scr_loc.x = Math.max((int)(scr_org_x + x * scr_x_per_x), panelMinX);
         scr_loc.y = Math.max((int)(scr_org_y + y * scr_y_per_y), panelMinY);
         scr_loc.y = Math.min( scr_loc.y, panelHeight );
         return scr_loc;
      } //end of xform
//-------------------------------------------------------------------------------
      /**
       *TBD
       *@param x double x coordinate
       *@param y double y coordinate
       */
      public Point xform3(double x, double y) {
         final double scr_org_x = panelWidth;
         final double scr_org_y = panelHeight;
         final double scr_x_per_x = -( panelWidth-panelMinX) / (timeMax-timeMin);
         final double scr_y_per_y = -( panelHeight-panelMinY)/ (tempMax-tempMin);
         y -= tempMin; x -= timeMin;
         Point scr_loc = new Point();
         scr_loc.x = Math.max((int)(scr_org_x + x * scr_x_per_x), panelMinX);
         scr_loc.y = Math.max((int)(scr_org_y + y * scr_y_per_y), panelMinY);
         scr_loc.y = Math.min( scr_loc.y, panelHeight );
         return scr_loc;
      } //end of xform

//-------------------------------------------------------------------------------
      /**
       *TBD
       *@param x double x coordinate
       *@param y double y coordinate
       */
      public Point xform2(double x, double y)
      {
         final double scr_org_x = 0;
         final double scr_org_y = panelHeight;
         final double scr_x_per_x = (panelWidth) / (timeMax-timeMin);
         final double scr_y_per_y = -( panelHeight-panelMinY) / (tempMax-tempMin);
         y -= tempMin;// x += timeMin;
         Point scr_loc = new Point();
         scr_loc.x = (int)(scr_org_x + x * scr_x_per_x);
         scr_loc.y = Math.max((int)(scr_org_y + y * scr_y_per_y), panelMinY);
         scr_loc.y = Math.min( scr_loc.y, panelHeight );
         return scr_loc;
      } //end of xform2

//-------------------------------------------------------------------------------
      /**
       *TBD
       *@param g Graphics TBD
       */
      public synchronized void paint(Graphics g) {
        panelWidth = (int)this.getWidth() - 38;
        panelHeight = (int)this.getHeight() - 18;
        g.setFont(new Font(g.getFont().getName(),g.getFont().getStyle(),10));
        g.drawLine( panelMinX, panelHeight, panelWidth, panelHeight );
        g.drawLine( panelWidth, panelMinY, panelWidth, panelHeight );

        double d = timeMin;
        while (d <= timeMax) {
	    Point p1 = xform(d,1); Point p2 = xform(d,-1);
	    if( drawGrid ) {
		g.setColor( Color.gray );
		g.drawLine( p1.x, panelHeight, p2.x, 0 );
	    }
	    g.setColor( Color.black );
	    g.drawLine( p1.x, panelHeight+1, p2.x, panelHeight-1 );
	    d+=xTickDelta;
        }

        d = timeMin;
        while (d <=timeMax) {
          Point p1 = xform(d,-15);
	  g.drawString( String.valueOf(-d), p1.x-9, panelHeight+15 );
          d+=xLabelDelta;
        }

        double y = tempMin;
        while (y <= tempMax) {
	    if( drawGrid ) {
		g.setColor( Color.gray );
		g.drawLine( panelMinX, xform2(panelMinX+1,y).y, panelWidth, xform2(panelMinX-1,y).y );
	    }
	    g.setColor( Color.black );
	    g.drawLine( panelWidth-1, xform2(0,y).y, panelWidth+1, xform2(0,y).y );
	    y+=yTickDelta;
        }

        y = tempMin;
        while (y <= tempMax) {
          g.drawString( String.valueOf(y), panelWidth+6, xform2(0,y).y+3 );
          y+=yLabelDelta;
        }

	size = xValues[0].size();
        int []tempx = new int[size];
        int []tempy = new int[size];
	Point srcLoc;
	Point avgTrash = new Point();
	// Compute screen Locations and use polylines to draw the graphs.

        for (int i=0; i<SensorParms.length; i++) {
          if (SensorParms[i].graphElement.showme.isSelected()) {
             for (int j=0; j<size; j++){
	        srcLoc = xform3( ( (currTime - ((Long)xValues[i].get(j)).longValue()))/1000,
				((Integer)yValues[i].get(j)).intValue()/100.0 );
                tempx[j] = srcLoc.x;
                tempy[j] = srcLoc.y;
		if (autoscale && size == 1) {
		    avgTrash.y += ((Integer)yValues[i].get(j)).intValue()/100.0;
		    avgTrash.x++;
		}
             }
	     g.setColor( SensorParms[i].statusColor.getBackground() );
             g.drawPolyline(tempx,tempy,size);
          }
        }
	if (autoscale && size == 1 && avgTrash.x!=0) {
	    tempMin = avgTrash.y/avgTrash.x - 50;
	    tempMax = tempMin + 100;
	}
	g.setColor( Color.black );
      } //end of paint

   } //end of class JGraphPanel


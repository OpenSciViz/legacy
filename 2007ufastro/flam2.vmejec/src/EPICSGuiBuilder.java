package uffjec;

import ufjca.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.io.*;
import java.util.*;
import java.beans.*;
import java.lang.reflect.*;

public class EPICSGuiBuilder {
    

    JFrame currentFrame;
    JPanel currentPanel;

    boolean DRAGGING = false;
    int fudgeFactorX, fudgeFactorY;
    int DRAGMODE;

    String selectedLayout = "0.5,0.5;0.1,0.08";

    MouseMotionAdapter mma = new MouseMotionAdapter(){
	    public void mouseMoved(MouseEvent me) {
		int x = me.getX(); int y = me.getY(); 
		int w = me.getComponent().getWidth(); int h = me.getComponent().getHeight();
		
		if (x < 10 && x > 0 && y < 10 && y > 0)
		    currentPanel.setCursor(new Cursor(Cursor.NW_RESIZE_CURSOR));
		else if (x < 10 &&  y < h && x >0 && y > h-10)
		    currentPanel.setCursor(new Cursor(Cursor.SW_RESIZE_CURSOR));
		else if (x < w &&  y < 10 && x > w-10 && y > 0)
		    currentPanel.setCursor(new Cursor(Cursor.NE_RESIZE_CURSOR));
		else if (x < w &&  y < h && x > w-10 && y > h-10)
		    currentPanel.setCursor(new Cursor(Cursor.SE_RESIZE_CURSOR));
		else if (x < w && y < 10 && x > 0 && y > 0)
		    currentPanel.setCursor(new Cursor(Cursor.N_RESIZE_CURSOR));
		else if (x < w && y < h && x > 0 && y > h-10)
		    currentPanel.setCursor(new Cursor(Cursor.S_RESIZE_CURSOR));
		else if (x < 10 && y < h && x > 0 && y > 0)
		    currentPanel.setCursor(new Cursor(Cursor.W_RESIZE_CURSOR));
		else if (x < w && y < h && x > w-10 && y > 0)
		    currentPanel.setCursor(new Cursor(Cursor.E_RESIZE_CURSOR));
		else
		    currentPanel.setCursor(new Cursor(Cursor.MOVE_CURSOR));
		DRAGMODE = currentPanel.getCursor().getType();
	    }
	    public void mouseDragged(MouseEvent me) {
		int componentX = (int)me.getComponent().getX();
		int componentY = (int)me.getComponent().getY();
		int componentW = me.getComponent().getWidth();
		int componentH = me.getComponent().getHeight();
		int mx = me.getX()+componentX-fudgeFactorX;
		int my = me.getY()+componentY-fudgeFactorY;
		if (!DRAGGING) {
		    fudgeFactorX = me.getX();
		    fudgeFactorY = me.getY();
		}
		Graphics g = currentPanel.getGraphics();
		currentPanel.paintAll(g);
		if (DRAGMODE == Cursor.MOVE_CURSOR) {
		    g.drawRect(mx,my,componentW,componentH);
		} else if (DRAGMODE == Cursor.E_RESIZE_CURSOR) {
		    g.drawRect(componentX,componentY,me.getX(),componentH);
		} else if (DRAGMODE == Cursor.W_RESIZE_CURSOR) {
		    g.drawRect(componentX+me.getX(),componentY,componentW-me.getX(),componentH);
		} else if (DRAGMODE == Cursor.N_RESIZE_CURSOR) {
		    g.drawRect(componentX,componentY+me.getY(),componentW,componentH-me.getY());
		} else if (DRAGMODE == Cursor.S_RESIZE_CURSOR) {
		    g.drawRect(componentX,componentY,componentW,me.getY());
		} else if (DRAGMODE == Cursor.NE_RESIZE_CURSOR) {
		    g.drawRect(componentX,componentY+me.getY(),me.getX(),componentH-me.getY());
		} else if (DRAGMODE == Cursor.NW_RESIZE_CURSOR) {
		    g.drawRect(componentX+me.getX(),componentY+me.getY(),componentW-me.getX(),componentH-me.getY());
		} else if (DRAGMODE == Cursor.SE_RESIZE_CURSOR) {
		    g.drawRect(componentX,componentY,me.getX(),me.getY());
		} else if (DRAGMODE == Cursor.SW_RESIZE_CURSOR) {
		    g.drawRect(componentX+me.getX(),componentY,componentW-me.getX(),me.getY());
		} 
		DRAGGING = true;
	    }
	};

    MouseAdapter ma = new MouseAdapter(){
	    public void mousePressed(MouseEvent me){}
	    public void mouseReleased(MouseEvent me){
		System.out.println("Mouse Released.");
		if (DRAGGING) {
		    Point p = currentFrame.getLocation();
		    int w = currentFrame.getWidth();
		    int h = currentFrame.getHeight();
		    int x = (int)p.getX();
		    int y = (int)p.getY();
		    int mx = me.getX()+(int)currentFrame.getLocation().getX()+(int)me.getComponent().getLocation().getX();
		    int my = me.getY()+(int)currentFrame.getLocation().getY()+(int)me.getComponent().getLocation().getY();
		    if (mx > x && mx < x+w &&
			my > y && my < y+h) {
			mx -= (int)currentFrame.getLocation().getX()+fudgeFactorX;
			my -= (int)currentFrame.getLocation().getY()+fudgeFactorY;		
			System.out.println("Placing Widget at: "+mx+","+my);
			double xx=0,yy=0,ww=0,hh=0;
			if (DRAGMODE == Cursor.MOVE_CURSOR) {
			    xx = mx / (double)currentPanel.getWidth(); yy = my / (double)currentPanel.getHeight();
			    ww = me.getComponent().getWidth() / (double)currentPanel.getWidth();
			    hh = me.getComponent().getHeight()/ (double)currentPanel.getHeight();
			} else if (DRAGMODE == Cursor.E_RESIZE_CURSOR) {
			    xx = me.getComponent().getLocation().getX() / currentPanel.getWidth();
			    yy = me.getComponent().getLocation().getY() / currentPanel.getHeight();
			    ww = (me.getX()) / (double)currentPanel.getWidth();
			    hh = me.getComponent().getHeight()/ (double)currentPanel.getHeight();
			} else if (DRAGMODE == Cursor.W_RESIZE_CURSOR) {
			    xx = (me.getComponent().getLocation().getX()+me.getX()) / currentPanel.getWidth();
			    yy = me.getComponent().getLocation().getY() / currentPanel.getHeight();
			    ww = (me.getComponent().getWidth()-me.getX()) / (double)currentPanel.getWidth();
			    hh = me.getComponent().getHeight()/ (double)currentPanel.getHeight();
			} else if (DRAGMODE == Cursor.S_RESIZE_CURSOR) {
			    xx = (me.getComponent().getLocation().getX()) / currentPanel.getWidth();
			    yy = me.getComponent().getLocation().getY() / currentPanel.getHeight();
			    ww = (me.getComponent().getWidth()) / (double)currentPanel.getWidth();
			    hh = me.getY()/ (double)currentPanel.getHeight();
			} else if (DRAGMODE == Cursor.N_RESIZE_CURSOR) {
			    xx = (me.getComponent().getLocation().getX()) / currentPanel.getWidth();
			    yy = (me.getComponent().getLocation().getY()+me.getY()) / currentPanel.getHeight();
			    ww = (me.getComponent().getWidth()) / (double)currentPanel.getWidth();
			    hh = (me.getComponent().getHeight()-me.getY())/ (double)currentPanel.getHeight();
			} else if (DRAGMODE == Cursor.NE_RESIZE_CURSOR) {
			    xx = (me.getComponent().getLocation().getX()) / currentPanel.getWidth();
			    yy = (me.getComponent().getLocation().getY()+me.getY()) / currentPanel.getHeight();
			    ww = (me.getX()) / (double)currentPanel.getWidth();
			    hh = (me.getComponent().getHeight()-me.getY())/ (double)currentPanel.getHeight();
			} else if (DRAGMODE == Cursor.NW_RESIZE_CURSOR) {
			    xx = (me.getComponent().getLocation().getX()+me.getX()) / currentPanel.getWidth();
			    yy = (me.getComponent().getLocation().getY()+me.getY()) / currentPanel.getHeight();
			    ww = (me.getComponent().getWidth()-me.getX()) / (double)currentPanel.getWidth();
			    hh = (me.getComponent().getHeight()-me.getY())/ (double)currentPanel.getHeight();
			} else if (DRAGMODE == Cursor.SE_RESIZE_CURSOR) {
			    xx = (me.getComponent().getLocation().getX()) / currentPanel.getWidth();
			    yy = me.getComponent().getLocation().getY() / currentPanel.getHeight();
			    ww = (me.getX()) / (double)currentPanel.getWidth();
			    hh = me.getY()/ (double)currentPanel.getHeight();
			} else if (DRAGMODE == Cursor.SW_RESIZE_CURSOR) {
			    xx = (me.getComponent().getLocation().getX()+me.getX()) / currentPanel.getWidth();
			    yy = me.getComponent().getLocation().getY() / currentPanel.getHeight();
			    ww = (me.getComponent().getWidth()-me.getX()) / (double)currentPanel.getWidth();
			    hh = me.getY()/ (double)currentPanel.getHeight();
			}
			String lyt = ((int)(xx*1000))/1000.0+","+((int)(yy*1000))/1000.0+";"+((int)(ww*1000))/1000.0+","+((int)(hh*1000))/1000.0;
			((JComponent)me.getComponent()).putClientProperty("layout",lyt);
			removeAddComponent((JComponent)me.getComponent());
			UFCAToolkit.put(EPICS.prefix+"sad:DHSLABEL",lyt);
		    }
		    currentPanel.setCursor(Cursor.getDefaultCursor());
		}
		DRAGGING = false;
	    }
	    
	    public JDialog getComboBoxInputDialog(JComponent egg) {
		final JComponent eg = egg;
		final JDialog jd = new JDialog(currentFrame);
		String [] strs = (String [])eg.getClientProperty("dataarray");
		final JComboBox jc= new JComboBox(strs);
		final JTextField jt = new JTextField("",10);
		final JTextField jtt= new JTextField("",10);
		JButton jb= new JButton("Add");
		JButton ub= new JButton("Del");
		JButton db = new JButton("Done");
		db.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae) {
			    JComboBox jjc = (JComboBox)eg;
			    String [] s = new String[jc.getItemCount()];
			    jjc.removeAllItems();
			    for (int i=0; i<s.length; i++) {
				s[i] = (String)jc.getItemAt(i);
				jjc.addItem(s[i]);
			    }
			    jjc.putClientProperty("dataarray",s);
			    if (eg.getClass().getName().indexOf("EPICS") != -1) 
				if (jtt.getText().trim() != "")
				    eg.putClientProperty("data",jtt.getText());
			    
			    jd.dispose();
			}
		    });
		jb.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae) {
			    if (jt.getText().trim() != "")
				jc.addItem(jt.getText());
			    jc.showPopup();
			}
		    });
		ub.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae) {
			    if (jt.getText().trim() != "")
				jc.removeItem(jt.getText());
			    jc.showPopup();
			}
		    });
		JPanel northPanel = new JPanel();
		northPanel.setLayout(new GridLayout(0,1));
		if (eg.getClass().getName().indexOf("EPICS") != -1){
		    JPanel rr= new JPanel();
		    rr.add(new JLabel("EPICS Recname:"));
		    jtt.setText((String)eg.getClientProperty("data"));
		    rr.add(jtt);
		    northPanel.add(rr);
		}
		northPanel.add(new JLabel("ComboBox Contents:"));
		jd.getContentPane().add(northPanel,BorderLayout.NORTH);
		JPanel centerPanel = new JPanel();
		centerPanel.add(jc);
		jd.getContentPane().add(centerPanel,BorderLayout.CENTER);
		JPanel southPanel = new JPanel();
		southPanel.add(jt);
		southPanel.add(jb);
		southPanel.add(ub);
		southPanel.add(db);
		jd.getContentPane().add(southPanel,BorderLayout.SOUTH);
		//jd.setVisible(true);
		jd.setSize(350,200);
		//jc.showPopup();
		return jd;
	    }
	    public void mouseClicked(MouseEvent me){
		final JComponent eg = (JComponent)me.getComponent();
		if (me.isShiftDown()){ 
		    String lp = (String)eg.getClientProperty("layout");
		    if (me.getButton() == MouseEvent.BUTTON1){
			String newLayout = lp.substring(0,lp.indexOf(";"))+selectedLayout.substring(selectedLayout.indexOf(";"),selectedLayout.length());
			eg.putClientProperty("layout",newLayout);
			removeAddComponent(eg);
		    } else if (me.getButton() == MouseEvent.BUTTON2) {
			String newLayout = selectedLayout.substring(0,selectedLayout.indexOf(","))+lp.substring(lp.indexOf(","),lp.length()) ;
			eg.putClientProperty("layout",newLayout);
			removeAddComponent(eg);
		    } else if (me.getButton() == MouseEvent.BUTTON3) {
			String newLayout = lp.substring(0,lp.indexOf(","))+selectedLayout.substring(selectedLayout.indexOf(","),selectedLayout.indexOf(";"))+
			    lp.substring(lp.indexOf(";"),lp.length());
			eg.putClientProperty("layout",newLayout);
			removeAddComponent(eg);
		    }		
		} else if (me.getButton() == MouseEvent.BUTTON1) {
		    selectedLayout = (String)eg.getClientProperty("layout");
		} else if (me.getButton() == MouseEvent.BUTTON3) {
		    if (eg instanceof Box.Filler) return;
		    else if (eg instanceof JComboBox) {
			JDialog jd = getComboBoxInputDialog(eg);
			jd.setVisible(true);
		    } else {
			String s = "text";
			if (eg.getClass().getName().indexOf("EPICS") != -1)
			    s = "record name";
			String r = JOptionPane.showInputDialog(currentFrame,
							       "Enter "+s+" for this component",
							       (String)eg.getClientProperty("data"));
			if (r != null && r.trim() != "") {
			    eg.putClientProperty("data",r);
			    if (eg instanceof JLabel) ((JLabel)eg).setText(r);
			    else if (eg instanceof JTextField) ((JTextField)eg).setText(r);
			    else if (eg instanceof JButton) ((JButton)eg).setText(r);
			}
		    }
		}
	    }
	    public void mouseEntered(MouseEvent me){ /*((JComponent)me.getComponent()).grabFocus();*/}
	    public void mouseExited(MouseEvent me){}
	};

    public void removeAddComponent(JComponent egg) {
	currentPanel.remove(egg);
	currentPanel.add((String)egg.getClientProperty("layout"),egg);
	currentFrame.setContentPane(currentPanel);
	currentFrame.repaint();
    }

    public JFrame getNewWorkFrame() {
	final JFrame workFrame = new JFrame("EPICS Panel");
	final JPanel workPanel = new JPanel();

	workPanel.setLayout(new RatioLayout());

	workPanel.addMouseMotionListener(new MouseMotionListener(){
		public void mouseDragged(MouseEvent me){}
		public void mouseMoved(MouseEvent me){ currentPanel.setCursor(Cursor.getDefaultCursor());}
	    });
	workPanel.addMouseListener(new MouseListener() {
		public void mousePressed(MouseEvent me) {}
		public void mouseReleased(MouseEvent me) {}
		public void mouseEntered(MouseEvent me) {}
		public void mouseExited(MouseEvent me) {}
		public void mouseClicked(MouseEvent me) {
		    currentPanel = workPanel;
		    currentFrame = workFrame;
		}
	    });
	workFrame.addMouseListener(new MouseListener() {
		public void mousePressed(MouseEvent me) {}
		public void mouseReleased(MouseEvent me) {}
		public void mouseEntered(MouseEvent me) {}
		public void mouseExited(MouseEvent me) {}
		public void mouseClicked(MouseEvent me) {
		    currentPanel = workPanel;
		    currentFrame = workFrame;
		}
	    });
	currentPanel = workPanel;
	currentFrame = workFrame;
	workFrame.setSize(800,600);
	workFrame.setLocation(300,100);
	workFrame.setContentPane(workPanel);
	return workFrame;
    }

    public EPICSGuiBuilder(){
	JFrame toolFrame = new JFrame("EPICS Gui Builder");
	//workPanel.setBorder(new EtchedBorder(0));
	//workPanel.add(new JButton("New"));
	JButton etButton = new JButton("-->");
	etButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    EPICSTextField t = new EPICSTextField(EPICS.prefix+"test");
		    t.putClientProperty("layout","0.50,0.50;0.10,0.10");
		    t.putClientProperty("data","test");
		    t.addMouseListener(ma);
		    t.addMouseMotionListener(mma);
		    currentPanel.add("0.50,0.50;0.10,0.10",t);
		    currentFrame.setContentPane(currentPanel);
		    currentFrame.repaint();
		}
	    });
	JButton elButton = new JButton("-->");
	elButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    EPICSLabel t = new EPICSLabel(EPICS.prefix+"test");
		    t.putClientProperty("layout","0.50,0.50;0.10,0.10");
		    t.putClientProperty("data","test");
		    t.setBorder(new EtchedBorder(0));
		    t.addMouseListener(ma);
		    t.addMouseMotionListener(mma);
		    currentPanel.add("0.50,0.50;0.10,0.10",t);
		    currentFrame.setContentPane(currentPanel);
		    currentFrame.repaint();
		}
	    });
	JButton ecButton = new JButton("-->");
	ecButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    String [] strs = {};
		    EPICSComboBox t = new EPICSComboBox(EPICS.prefix+"test",strs,EPICSComboBox.ITEM);
		    t.putClientProperty("layout","0.50,0.50;0.10,0.10");
		    t.putClientProperty("data","test");
		    t.putClientProperty("dataarray",strs);
		    t.putClientProperty("itemorindex","EPICSComboBox.ITEM");
		    t.addMouseListener(ma);
		    t.addMouseMotionListener(mma);
		    currentPanel.add("0.50,0.50;0.10,0.10",t);
		    currentFrame.setContentPane(currentPanel);
		    currentFrame.repaint();
		}
	    });	
	JButton jbButton = new JButton("-->");
	jbButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    final JButton t = new JButton("Button");
		    t.putClientProperty("layout","0.50,0.50;0.10,0.10");
		    t.putClientProperty("data","Button");
		    t.addMouseListener(ma);
		    t.addMouseMotionListener(mma);
		    currentPanel.add("0.50,0.50;0.10,0.10",t);
		    currentFrame.setContentPane(currentPanel);
		    currentFrame.repaint();
		    System.out.println("Adding monitor");
		    UFCAToolkit.addMonitor(EPICS.prefix+"sad:DHSLABEL",new UFCAToolkit.MonitorListener(){
			    public void monitorChanged(String value) {
				System.out.println("Monitor Changed");
				if (value != null && value.trim() != "") {
				    t.putClientProperty("layout",value);
				    removeAddComponent(t);
				}
			    }
			});
		}
	    });
	JButton jtButton = new JButton("-->");
	jtButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    JTextField t = new JTextField("TextField");
		    t.putClientProperty("layout","0.50,0.50;0.10,0.10");
		    t.putClientProperty("data","TextField");
		    t.addMouseListener(ma);
		    t.addMouseMotionListener(mma);
		    currentPanel.add("0.50,0.50;0.10,0.10",t);
		    currentFrame.setContentPane(currentPanel);
		    currentFrame.repaint();
		}
	    });
	JButton jlButton = new JButton("-->");
	jlButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    JLabel t = new JLabel("Label");
		    t.putClientProperty("layout","0.50,0.50;0.10,0.10");
		    t.putClientProperty("data","Label");
		    t.setBorder(new EtchedBorder(0));
		    t.addMouseListener(ma);
		    t.addMouseMotionListener(mma);
		    currentPanel.add("0.50,0.50;0.10,0.10",t);
		    currentFrame.setContentPane(currentPanel);
		    currentFrame.repaint();
		}
	    });
	JButton jcButton = new JButton("-->");
	jcButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    String [] strs = {};
		    JComboBox t = new JComboBox(strs);
		    t.putClientProperty("layout","0.50,0.50;0.10,0.10");
		    t.putClientProperty("dataarray",strs);
		    t.addMouseListener(ma);
		    t.addMouseMotionListener(mma);
		    currentPanel.add("0.50,0.50;0.10,0.10",t);
		    currentFrame.setContentPane(currentPanel);
		    currentFrame.repaint();
		}
	    });
	JButton jpButton = new JButton("-->");
	jpButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    Box.Filler t = new Box.Filler(null,null,null);
		    t.setBorder(new EtchedBorder(0));
		    t.putClientProperty("layout","0.50,0.50;0.10,0.10");
		    t.putClientProperty("data","null,null,null");
		    t.addMouseListener(ma);
		    t.addMouseMotionListener(mma);
		    currentPanel.add("0.50,0.50;0.10,0.10",t);
		    currentFrame.setContentPane(currentPanel);
		    currentFrame.repaint();
		}
	    });
	JButton jCustom = new JButton("-->");
	jCustom.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    try {
			String s = JOptionPane.showInputDialog(currentFrame,"Fully qualified name of class:");
			if (s != null && s.trim() != "") {
			    Class c = ClassLoader.getSystemClassLoader().loadClass(s);
			    System.out.println(c.getName());
			    Constructor ctor = (Constructor)JOptionPane.showInputDialog(currentFrame,"Select Constructor","Select Constructor",
											JOptionPane.PLAIN_MESSAGE,null,c.getConstructors(),null); 
			    if (ctor == null) return;
			    Class [] pts = ctor.getParameterTypes();
			    if (pts == null || pts.length ==0) {
				Object o = ctor.newInstance();
				if (o instanceof JComponent) {
				    JComponent j = (JComponent)o;
				    j.putClientProperty("layout","0.50,0.50;0.10,0.10");
				    j.putClientProperty("data","null");
				    j.addMouseListener(ma);
				    j.addMouseMotionListener(mma);
				    currentPanel.add("0.50,0.50;0.10,0.10",j);
				    currentFrame.setContentPane(currentPanel);
				    currentFrame.repaint();
				} else {
				    System.err.println("EPICSGuiBuilder.jCustom> Specified class is not a JComponent! Cannot use in gui");
				    return;
				}
			    } else {
				for (int j=0; j<pts.length; j++)
				    System.out.println(c.getName()+"::"+pts[j].getName());
			    }
			}
		    } catch (Exception e) {
			System.err.println("EPICSGuiBuilder.jCustom> "+e.toString());
		    }
		}
	    });
	JButton saveButton = new JButton("Save to file");
	saveButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    JFileChooser jfc = new JFileChooser(".");
		    if (jfc.showSaveDialog(currentFrame) == jfc.APPROVE_OPTION)
			writeSaveFile(jfc.getSelectedFile().getAbsolutePath());
		}
	    });
	JButton readButton = new JButton("Read from file");
	readButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    JFileChooser jfc = new JFileChooser(".");
		    if (jfc.showOpenDialog(currentFrame) == jfc.APPROVE_OPTION)
			readSaveFile(jfc.getSelectedFile().getAbsolutePath());
		}
	    });
	JButton generateButton = new JButton("Generate Source Code");
	generateButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    JFileChooser jfc = new JFileChooser(".");
		    if (jfc.showOpenDialog(currentFrame) == jfc.APPROVE_OPTION)
			writeSourceFile(jfc.getSelectedFile());
		}
	    });
	JPanel toolyPanel = new JPanel();
	toolyPanel.setBorder(new EtchedBorder(0));
	toolyPanel.setLayout(new GridLayout(0,2));
	toolyPanel.add(new JTextField("EPICSTextField"));
	toolyPanel.add(etButton);
	toolyPanel.add(new JLabel("EPICSLabel"));
	toolyPanel.add(elButton);
	String [] strs2 = {"EPICSComboBox"};
	toolyPanel.add(new JComboBox(strs2));
	toolyPanel.add(ecButton);
	JPanel toolxPanel = new JPanel();
	toolxPanel.setBorder(new EtchedBorder(0));
	toolxPanel.setLayout(new GridLayout(0,2));
	toolxPanel.add(new JTextField("JTextField"));
	toolxPanel.add(jtButton);
	toolxPanel.add(new JLabel("JLabel"));
	toolxPanel.add(jlButton);
	String [] strs = {"JComboBox"};
	toolxPanel.add(new JComboBox(strs));
	toolxPanel.add(jcButton);
	toolxPanel.add(new JButton("JButton"));
	toolxPanel.add(jbButton);
	toolxPanel.add(new JLabel("Border"));
	toolxPanel.add(jpButton);
	toolxPanel.add(new JLabel("Custom"));
	toolxPanel.add(jCustom);
	JPanel toolPanel = new JPanel();
	toolPanel.add(new JLabel("EPICS Components:"));
	toolPanel.add(toolyPanel);
	toolPanel.add(new JLabel("Normal Components:"));
	toolPanel.add(toolxPanel);
	toolPanel.add(generateButton);
	toolPanel.add(saveButton);
	toolPanel.add(readButton);
	toolFrame.setSize(200,400);
	toolFrame.setContentPane(toolPanel);
	toolFrame.setDefaultCloseOperation(3);
	toolFrame.setLocation(100,100);
	toolFrame.setVisible(true);
	JFrame workFrame = getNewWorkFrame();
	workFrame.setVisible(true);
	currentFrame = workFrame;
    }

    public void writeLine(BufferedWriter bw, String s)throws IOException {
	if (s == null) return;
	bw.write(s,0,s.length());bw.newLine();
    }

    public void writeSaveFile(String filename) {
	try {
	    BufferedWriter bw = new BufferedWriter(new FileWriter(filename));
	    Component [] c = currentPanel.getComponents();
	    for (int i=0; i<c.length; i++) {
		if (c[i] instanceof JComponent) {
		    JComponent j = (JComponent)c[i];
		    String clname = j.getClass().getName();
		    Object o = j.getClientProperty("layout");
		    String s = clname + "%"+ o +"%"+j.getClientProperty("data");
		    if (j instanceof JComboBox) {
			String [] st = (String []) j.getClientProperty("dataarray");
			if (st != null && st.length > 0) {
			    s += "%"+st[0];
			    for (int k=1; k<st.length; k++)
				s += ","+st[k];
			}
		    } 
		    bw.write(s,0,s.length());
		    bw.newLine();
		}
	    }
	    bw.flush();
	    bw.close();
	} catch (Exception e) {
	    System.err.println("EPICSGuiBuilder.writeSaveFile> "+e.toString());
	}
    }

    public void readSaveFile(String filename) {
	try {
	    getNewWorkFrame().setVisible(true);
	    BufferedReader br = new BufferedReader(new FileReader(filename));
	    while (br.ready()) {
		StringTokenizer st = new StringTokenizer(br.readLine(),"%");
		JComponent j = null;
		String clname = st.nextToken();
		String layout = st.nextToken();
		String data = st.nextToken();
		if (clname.indexOf("EPICSTextField")!=-1) {
		    j = new EPICSTextField(data);
		} else if (clname.indexOf("EPICSLabel")!=-1) {
		    j = new EPICSLabel(data);
		    j.setBorder(new EtchedBorder(0));
		} else if (clname.indexOf("EPICSComboBox")!=-1) {
		    String [] strs = {};
		    if (st.hasMoreTokens()) {
			StringTokenizer tu = new StringTokenizer(st.nextToken(),",");
			strs = new String[tu.countTokens()];
			for (int i=0; i<strs.length; i++)
			    strs[i] = tu.nextToken();
		    }
		    j = new EPICSComboBox(data,strs,EPICSComboBox.ITEM);
		    j.putClientProperty("dataarray",strs);
		} else if (clname.indexOf("JTextField")!=-1) {
		    j = new JTextField(data);
		} else if (clname.indexOf("JLabel")!=-1) {
		    j = new JLabel(data);
		    j.setBorder(new EtchedBorder(0));
		} else if (clname.indexOf("JComboBox")!=-1) {
		    String [] strs = {};
		    if (st.hasMoreTokens()) {
			StringTokenizer tu = new StringTokenizer(st.nextToken(),",");
			strs = new String[tu.countTokens()];
			for (int i=0; i<strs.length; i++)
			    strs[i] = tu.nextToken();
		    }
		    j = new JComboBox(strs);
		    j.putClientProperty("dataarray",strs);
		} else if (clname.indexOf("JButton")!=-1) {
		    j = new JButton(data);
		} else if (clname.indexOf("Filler")!=-1) {
		    j = new Box.Filler(null,null,null);
		    j.setBorder(new EtchedBorder(0));
		}
		if (j != null) {
		    j.putClientProperty("layout",layout);
		    j.putClientProperty("data",data);
		} else {
		    System.err.println("EPICSGuiBuilder.readSaveFile> Unknown object stored in savefile: "+clname);
		}
		j.addMouseListener(ma);
		j.addMouseMotionListener(mma);
		currentPanel.add(layout,j);
	    }
	    currentFrame.setContentPane(currentPanel);
	    currentFrame.repaint();
	} catch (Exception e) {
	    System.err.println("EPICSGuiBuilder.readSaveFile> "+e.toString());
	}
    }

    public void writeSourceFile(File file) {
	try {
	    String stri = file.getAbsolutePath();
	    String nam = file.getName();
	    if (!stri.endsWith(".java")) stri += ".java";
	    if (nam.endsWith(".java")) nam = nam.substring(0,nam.length()-5);
	    BufferedWriter bw = new BufferedWriter(new FileWriter(stri));
	    writeLine(bw,"package uffjec;");
	    writeLine(bw,"import ufjca.*;");
	    writeLine(bw,"import java.awt.*;");
	    writeLine(bw,"import java.awt.event.*;");
	    writeLine(bw,"import javax.swing.*;");
	    writeLine(bw,"import javax.swing.border.*;");
	    bw.newLine();
	    writeLine(bw,"public class "+nam+" extends JPanel {");
	    writeLine(bw,"\tpublic "+nam+"() {");
	    writeLine(bw,"\t\tsetLayout(new RatioLayout());");
	    Component [] c = currentPanel.getComponents();
	    for (int i=0; i<c.length; i++){
		if (c[i] instanceof JComponent) {
		    JComponent j = (JComponent)c[i];
		    String clname = j.getClass().getName();
		    Object o = j.getClientProperty("layout");
		    if (o == null) continue;
		    if (j instanceof JComboBox) {
			Object p = j.getClientProperty("dataarray");
			String s = "";
			if (p != null && ((String[])p).length > 0) {
			    String [] st = (String[])p;
			    s += "\""+st[0]+"\"";
			    for (int k=1; k<st.length; k++)
				s +=",\""+st[k]+"\"";
			}
			writeLine(bw,"\t\t{");
			writeLine(bw,"\t\tString [] strs = {"+s+"};");
			if (clname.indexOf("EPICS") != -1)
			    writeLine(bw,"\t\tadd(\""+(String)j.getClientProperty("layout")+"\",new EPICSComboBox(EPICS.prefix+\""+
				      j.getClientProperty("data")+"\",strs,"+j.getClientProperty("itemorindex")+"));");
			else
			    writeLine(bw,"\t\tadd(\""+(String)j.getClientProperty("layout")+"\",new JComboBox(strs));");
			writeLine(bw,"\t\t}");
		    } else if (j instanceof Box.Filler) {
			writeLine(bw,"{");
			writeLine(bw,"\t\tBox.Filler bf = new Box.Filler(null,null,null);");
			writeLine(bw,"\t\tbf.setBorder(new EtchedBorder(0));");
			writeLine(bw,"\t\tadd(\""+(String)o+"\", bf);");
			writeLine(bw,"}");
		    } else {
			Object p = j.getClientProperty("data");
			if (p == null) continue;
			String con = "";
			if (clname.indexOf("EPICS") != -1) con = "EPICS.prefix+";
			con += "\""+(String)p+"\"";
			writeLine(bw,"\t\tadd(\""+(String)o+"\",new "+clname+"(" + con + "));");
		    }
		}
	    }	    
	    writeLine(bw,"\t}");
	    bw.newLine();
	    writeLine(bw,"\t\t//Unit Test");
	    writeLine(bw,"\tpublic static void main(String [] args){");
	    writeLine(bw,"\t\tEPICS.prefix = \"foo:\";");
	    writeLine(bw,"\t\tif (args != null && args.length > 0)");
	    writeLine(bw,"\t\t\tEPICS.prefix = args[0];");
	    writeLine(bw,"\t\tJFrame x = new JFrame();");
	    writeLine(bw,"\t\tx.setContentPane(new "+nam+"());");
	    writeLine(bw,"\t\tx.setSize(400,400);");
	    writeLine(bw,"\t\tx.setVisible(true);");
	    writeLine(bw,"\t\tx.setDefaultCloseOperation(3);");
	    writeLine(bw,"\t\tUFCAToolkit.startMonitorLoop();");
	    writeLine(bw,"\t}");
	    writeLine(bw,"}");
	    bw.flush();
	    bw.close();
	} catch (Exception e) {
	    System.err.println("EPICSGuiBuilder.writeOutputFile> "+e.toString());
	}
    }

    public static void main(String [] args) {
	EPICS.prefix = "foo:";
	new EPICSGuiBuilder();
	UFCAToolkit.startMonitorLoop();
    }

}

package uffjec;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.awt.*;
import java.net.*;
import java.applet.*;
import java.io.*;
import java.util.*;
import ufjca.*;
  
/** 
 *Main motor class -- contains all visual components as
 *well as parameters associated with a motor
 */ 
public class FJECMotor {

    // Visual Components
    JCheckBox hiNameCheckBox; // for use in high & low level panels
    JCheckBox loNameCheckBox;
    JCheckBox hiBacklashCheckBox;
    JCheckBox loBacklashCheckBox;
    String lastBacklash;
    JLabel nameLabel; // for use in motor parameter panel
    EPICSLabel hiStatusLabel;
    EPICSLabel loStatusLabel; 
    EPICSLabel hiLastCommand;
    EPICSLabel loLastCommand;
    EPICSComboBox namedLocationBox;
    JButton loAbortButton;
    JButton hiAbortButton;
    JButton loDatumButton;
    JButton hiDatumButton;
    JButton originButton;
    JButton loParkButton;
    JButton hiParkButton;
    JButton statusButton;
    EPICSTextField stmField; // Steps to move (low level)
    EPICSTextField isField; // Initial speed
    EPICSTextField tsField; // terminal speed
    EPICSTextField accField; // acceleration
    EPICSTextField decField; // deceleration
    EPICSTextField dcField; // drive current
    EPICSTextField dsField; // datum speed
    EPICSTextField blField; // back lash

    EPICSLabel isLabel; // Initial speed
    EPICSLabel tsLabel; // terminal speed
    EPICSLabel accLabel; // acceleration
    EPICSLabel decLabel; // deceleration
    EPICSLabel dcLabel; // drive current
    EPICSLabel dsLabel; // datum speed
    EPICSLabel blLabel; // back lash
    //EPICSTextField fdsField; // final datum speed
    EPICSComboBox datumDirBox; // datum direction
    EPICSLabel datumDirLabel;
    String name;
    String epicsName;
    String [] posNames;
    float [] posValues;
    float [] sortValues;
    String [] sortNames;
    String sadRec;
    String prefix = EPICS.prefix+"cc:";
    EPICSLowIndexorBox loCombo;
    FJECMotorAnimationPanel highAnimation;
    FJECMotorAnimationPanel lowAnimation;

    /**
     *  bubble sort (horrible complexity, but honestly, who cares? its only like 9 records or so
     */

    public void bubbleSort() {
	if (posValues.length != posNames.length) {
	    System.err.println("FJECMotor.bubbleSort> number of position names != number of position values!");
	    return;
	}
	for (int junk=0; junk < posValues.length; junk++)
	    for (int i=0; i<posValues.length-1; i++) {
		if (posValues[i] > posValues[i+1]) {
		    float tempF; String tempS;
		    tempF = posValues[i]; posValues[i] = posValues[i+1]; posValues[i+1] = tempF;
		    tempS = posNames[i]; posNames[i] = posNames[i+1]; posNames[i+1] = tempS;
		}
	    }
    }
    

    ///////////////
    //Constructors//
    ///////////////

    public FJECMotor clone() {
	String emn = new String(epicsName);
	String hmn = new String(name);
	String [] posn = new String[posNames.length];
	float [] posv = new float[posValues.length];
	for (int i=0; i<posNames.length; i++) posn[i] = new String(posNames[i]);
	for (int i=0; i<posValues.length; i++) posv[i] = posValues[i];
	return new FJECMotor(emn,hmn,posn,posv);
    }

    public FJECMotor(String epicsMotName, String humanMotName, String [] posnames) {
	name = new String(humanMotName);
	epicsName = new String(epicsMotName);
	posNames = new String[posnames.length];
	posValues = new float[posnames.length];
	for (int i=0; i<posnames.length; i++) {posNames[i] = new String(posnames[i]);posValues[i]=i*100;}
	setupVisualComponents();
    }
    public FJECMotor(String epicsMotName, String humanMotName, String [] posnames, float [] posvalues) {
	name = new String(humanMotName);
	epicsName = new String(epicsMotName);
	if (posnames.length != posvalues.length) {
	    System.err.println("FJECMotor.FJECMotor> number of position names != number of position values! NOT creating motor "+humanMotName);
	    return;
	}
	posNames = new String[posnames.length];
	posValues = new float[posvalues.length];
	for (int i=0; i<posnames.length; i++) posNames[i] = new String(posnames[i]);
	for (int i=0; i<posvalues.length;i++) posValues[i]= posvalues[i];
	bubbleSort();
	setupVisualComponents();
    }
    public FJECMotor(String epicsMotName, String humanMotName) {
	//stmField = new EPICSTextField(prefix + "setup." + epicsMotName,"No Description");
	name = new String(humanMotName);
	epicsName = new String(epicsMotName);
	posNames = new String[10];
	posValues = new float[10];
	for (int i=0; i<10; i++) { posNames[i] = "Pos"+i; posValues[i] = i*100;}
	setupVisualComponents();
    }
    public FJECMotor(String motName) { this (motName,motName); }
    /**
     *Construcor helper function creates and initializes visual components
     *associated with this motor (ie, JButtons, JTextFields, etc) and sets up
     *action methods associated with button presses.
     */
    protected void setupVisualComponents() {
	//if (epicsName.trim().toLowerCase().equals("mosbarcd"))
	//  epicsName = "MOS";
	hiBacklashCheckBox = new JCheckBox("Backlash",true);
	hiBacklashCheckBox.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    loBacklashCheckBox.setSelected(hiBacklashCheckBox.isSelected());
		    if (hiBacklashCheckBox.isSelected()) {
			if (lastBacklash != null)
			    EPICS.put(prefix + epicsName + ":motorG.L",lastBacklash);
		    } else {
			lastBacklash = EPICS.get(prefix + epicsName + ":motorG.L");
			EPICS.put(prefix +epicsName + "motorG.L","0");
		    }
		}
	    });
	loBacklashCheckBox = new JCheckBox("Backlash",true);
	loBacklashCheckBox.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    hiBacklashCheckBox.setSelected(loBacklashCheckBox.isSelected());
		    if (loBacklashCheckBox.isSelected()) {
			if (lastBacklash != null)
			    EPICS.put(prefix +epicsName + "motorG.L",lastBacklash);
		    } else {
			lastBacklash = EPICS.get(prefix +epicsName + "motorG.L");
			EPICS.put(prefix +epicsName + "motorG.L","0");
		    }
		}
	    });
	//fix for inconsistant epics rec names
	sadRec = EPICS.prefix+"sad:"+epicsName+"Steps.VAL";
	if (sadRec.toLowerCase().indexOf("barcd") != -1)
	    sadRec = EPICS.prefix+"sad:"+"MOS"+"Steps.VAL";
	hiStatusLabel = new EPICSLabel(sadRec,""){
		public void monitorChanged(String val) {
		    if (val == null || val.trim().equals("") || val.trim().toLowerCase().equals("null")){
			hiStatusLabel.setText("null");
			return;
		    }
		    try {
			float f = Float.parseFloat(val);
			if (f < posValues[0]) { hiStatusLabel.setText(f+": "+posNames[0]+" - "+(posValues[0]-f));return;}
			int i = 0;
			for (i =0; i<posValues.length-1; i++)
			    if (f == posValues[i]) { hiStatusLabel.setText(f+": "+posNames[i]); return;}
			    else if (f < posValues[i+1]) {
				float midpoint = (posValues[i+1]+posValues[i])/2.0f;
				if (f <= midpoint)
				    hiStatusLabel.setText(f+": "+posNames[i]+" + "+(f - posValues[i]));
				else
				    hiStatusLabel.setText(f+": "+posNames[i+1]+" - "+(posValues[i+1]-f));
				return;
			    } 
			if (f == posValues[i]) { hiStatusLabel.setText(f+": "+posNames[i]);return;}
			hiStatusLabel.setText(f+": "+posNames[i]+" + "+(f-posValues[i]));
		    } catch (Exception e) {
			System.err.println("FJECMotor.EPICSLabel.monitorChanged> "+e.toString());
		    }
		}
	    };
	hiStatusLabel.setHorizontalAlignment(JLabel.RIGHT);
	loStatusLabel = new EPICSLabel(sadRec,""){
		public void monitorChanged(String val) {
		    if (val == null || val.trim().equals("") || val.trim().toLowerCase().equals("null")){
			loStatusLabel.setText("null");
			return;
		    }
		    try {
			float f = Float.parseFloat(val);
			if (f < posValues[0]) { loStatusLabel.setText(f+": "+posNames[0]+" - "+(posValues[0]-f));return;}
			int i = 0;
			for (i =0; i<posValues.length-1; i++)
			    if (f == posValues[i]) { loStatusLabel.setText(f+": "+posNames[i]); return;}
			    else if (f < posValues[i+1]) {
				float midpoint = (posValues[i+1]+posValues[i])/2.0f;
				if (f <= midpoint)
				    loStatusLabel.setText(f+": "+posNames[i]+" + "+(f - posValues[i]));
				else
				    loStatusLabel.setText(f+": "+posNames[i+1]+" - "+(posValues[i+1]-f));
				return;
			    } 
			if (f == posValues[i]) { loStatusLabel.setText(f+": "+posNames[i]);return;}
			loStatusLabel.setText(f+": "+posNames[i]+" + "+(f-posValues[i]));
		    } catch (Exception e) {
			System.err.println("FJECMotor.EPICSLabel.monitorChanged> "+e.toString());
		    }		    
		}
	    };
	loStatusLabel.setHorizontalAlignment(JLabel.RIGHT);
	hiNameCheckBox = new JCheckBox(name,true);
	loNameCheckBox = new JCheckBox(name,true);
	nameLabel = new JLabel(name);
	//String [] tmp1 = {"PosA","PosB","PosC","PosD","PosE","PosF"};
	String [] tmp1 = new String[posNames.length+1];
	tmp1[0] = "null"; for (int i=0; i<posNames.length; i++) tmp1[i+1] = posNames[i];
	namedLocationBox = new EPICSComboBox(prefix + epicsName+":namedPos.A",tmp1,EPICSComboBox.ITEM);
	stmField = new EPICSTextField(prefix + epicsName + ":steps.A","",true);
	isField = new EPICSTextField(prefix  + epicsName + ":motorG.N","",true);
	isLabel = new EPICSLabel(prefix + epicsName + ":motorG.VALD");
	tsField = new EPICSTextField(prefix  + epicsName + ":motorG.O","",true);
	tsLabel = new EPICSLabel(prefix + epicsName + ":motorG.VALE");
	accField = new EPICSTextField(prefix + epicsName + ":motorG.P","",true);
	accLabel = new EPICSLabel(prefix + epicsName + ":motorG.VALF");
	decField = new EPICSTextField(prefix + epicsName + ":motorG.Q","",true);
	decLabel = new EPICSLabel(prefix + epicsName + ":motorG.VALG");
	dcField = new EPICSTextField(prefix  + epicsName + ":motorG.R","",true);
	dcLabel = new EPICSLabel(prefix + epicsName + ":motorG.VALH");
	dsField = new EPICSTextField(prefix  + epicsName + ":motorG.S","",true);
	dsLabel = new EPICSLabel(prefix + epicsName + ":motorG.VALI");
	//fdsField = new EPICSTextField(prefix + "datum." + epicsName+"Vel","");
	loCombo = new EPICSLowIndexorBox(epicsName,prefix+epicsName+":steps.A");

	hiLastCommand = new EPICSLabel(prefix+epicsName+":namedPos.VALA","");
	loLastCommand = new EPICSLabel(prefix+epicsName+":steps.VALA","");
	String [] tmp2 = {"0","1"};
	datumDirBox = new EPICSComboBox(prefix + epicsName +":motorG.T",tmp2,EPICSComboBox.ITEM);
	datumDirLabel = new EPICSLabel(prefix + epicsName + ":motorG.VALJ");
	blField = new EPICSTextField(prefix + epicsName + ":motorG.L","");
	blLabel = new EPICSLabel(prefix + epicsName + ":motorG.VALL");
	loAbortButton = new JButton("Abort");
	hiAbortButton = new JButton("Abort");
	loDatumButton = new JButton("Datum");
	hiDatumButton = new JButton("Datum");
	originButton = new JButton("Origin");
	loParkButton = new JButton("Park");
	hiParkButton = new JButton("Park");
	statusButton = new JButton("Status");
	//datumDirBox.addItem("");
	int [] tmp3 = new int[posNames.length];
	for (int i=0; i<tmp3.length; i++) tmp3[i] = i*100;
	highAnimation = new FJECMotorAnimationPanel(posNames,tmp3,tmp3[tmp3.length-1]+100);
	lowAnimation  = new FJECMotorAnimationPanel(posNames,tmp3,tmp3[tmp3.length-1]+100); 
	EPICS.addMonitor(sadRec, new UFCAToolkit.MonitorListener(){

		public void reconnect(String dbname) {
		    EPICS.removeMonitor(sadRec,this);
		    sadRec = dbname + sadRec.substring(sadRec.indexOf(":"));
		    EPICS.addMonitor(sadRec,this);
		}

		public void monitorChanged(String val) {
		    try {
			if (val != null && !val.trim().equals("") && !val.trim().toLowerCase().equals("null")){
			    int step = Integer.parseInt(val.trim());
			    highAnimation.setCurrentStep(step);
			    lowAnimation.setCurrentStep(step);
			}
		    } catch (Exception e) {
			System.err.println("FJECMotor.animationMonitorChanged> "+e.toString());
		    }
		}
	    });
	hiAbortButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    EPICS.put(EPICS.prefix+"cc:"+epicsName+":abort.A",EPICS.PRESET);
		    EPICS.put(EPICS.prefix+"cc:"+epicsName+":motorApply.DIR",EPICS.START);
		}
	    });
	loAbortButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    EPICS.put(EPICS.prefix+"cc:"+epicsName+":abort.A",EPICS.PRESET);
		    EPICS.put(EPICS.prefix+"cc:"+epicsName+":motorApply.DIR",EPICS.START);
		}
	    });
	hiDatumButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    EPICS.put(EPICS.prefix+"cc:"+epicsName+":datum.A",EPICS.PRESET);
		    EPICS.put(EPICS.prefix+"cc:"+epicsName+":motorApply.DIR",EPICS.START);
		}
	    });
	loDatumButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    EPICS.put(EPICS.prefix+"cc:"+epicsName+":datum.A",EPICS.PRESET);
		    EPICS.put(EPICS.prefix+"cc:"+epicsName+":motorApply.DIR",EPICS.START);
		}
	    });
	hiParkButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    EPICS.put(prefix+"park."+epicsName,EPICS.MARK);
		    EPICS.put(EPICS.applyPrefix+"apply.DIR",EPICS.START);
		}
	    });
	loParkButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    EPICS.put(prefix+"park."+epicsName,EPICS.MARK);
		    EPICS.put(EPICS.applyPrefix+"apply.DIR",EPICS.START);
		}
	    });
	hiNameCheckBox.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    loNameCheckBox.setSelected(hiNameCheckBox.isSelected());
		    ableButtons();
		}
	    });
	loNameCheckBox.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    hiNameCheckBox.setSelected(loNameCheckBox.isSelected());
		    ableButtons();
		}
	    });
	originButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    EPICS.put(EPICS.prefix+"cc:"+epicsName+":origin.A",EPICS.PRESET);
		    EPICS.put(EPICS.prefix+"cc:"+epicsName+":motorApply.DIR",EPICS.START);
		}
	    });
	statusButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    EPICS.put(prefix+"setup.Status",epicsName);
		    EPICS.put(EPICS.applyPrefix+"apply.DIR",EPICS.START);		    
		}
	    });
	new EPICSCarCallback(EPICS.applyPrefix+"applyC",new EPICSCarListener() {
		public void carTransition(int transition) {
		    if (transition == EPICS.IDLE) {
			motionStatus(false);
		    } else if(transition == EPICS.BUSY) {
			motionStatus(true);
		    } else if(transition == EPICS.ERROR) {
			
		    } else {
			System.err.println("FJECMotor::CarCallback> Unrecognized CAR value: "+transition);
		    }
		}
	    });

	/*
	loCombo.addItem(stmField);
	//loCombo.getEditor().setItem(stmField);
	loCombo.addItem(new String("Datum"));
	loCombo.addItem(new String("Park"));
	loCombo.setEditable(true);
	loCombo.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    if (loCombo.getSelectedIndex() == 0) {
			loCombo.setEditable(true);
			EPICS.put(EPICS.prefix+"datum.DIR",EPICS.CLEAR);
		    } else if (((String)loCombo.getSelectedItem()).equals("Datum")) {
			loCombo.setEditable(false);
			EPICS.put(prefix+"setup.DIR",EPICS.CLEAR);
			EPICS.put(EPICS.prefix+"datum."+epicsName,EPICS.PRESET);
		    } else if (((String)loCombo.getSelectedItem()).equals("Park")) {
			loCombo.setEditable(false);
		    }
		}
	    });
	*/
    }

    public String getName() { return name; }

    //////////////////
    //Action Methods//
    //////////////////

    /**
     *Enable/Disable buttons based on the state of {@link #hiNameCheckBox} and
     *{@link #loNameCheckBox} (which are always equal).
     */
    public void ableButtons() {
	hiDatumButton.setEnabled(hiNameCheckBox.isSelected());
	loDatumButton.setEnabled(hiNameCheckBox.isSelected());
	hiParkButton.setEnabled(hiNameCheckBox.isSelected());
	loParkButton.setEnabled(hiNameCheckBox.isSelected());
	originButton.setEnabled(hiNameCheckBox.isSelected());
	namedLocationBox.setEnabled(hiNameCheckBox.isSelected());
    }

    /**
     * Enable/Disable buttons based on motion of motors
     * @param isMotion true will disable buttons, false will enable them
     */
    public void motionStatus(boolean isMotion) {
	hiDatumButton.setEnabled(!isMotion);
	loDatumButton.setEnabled(!isMotion);
	originButton.setEnabled(!isMotion);
	namedLocationBox.setEnabled(!isMotion);
    }

    /////////////////////////
    //Visual helper methods//
    /////////////////////////

    /** 
     *Helper method.  This method arranges all parameter-level visual components into a long, thin panel
     *(ideal for placement into a GridLayout container <p>
     *@return JPanel containing all parameter-level visual components laid out using {@link RatioLayout} 
     */
    public JPanel getMotorParameterPanel() {
	JPanel fieldPanel = new JPanel();
	JPanel panel = new JPanel();
	fieldPanel.setLayout(new GridLayout(2,0));
	panel.setLayout(new RatioLayout());
	fieldPanel.add(isField);
	fieldPanel.add(tsField);
	fieldPanel.add(accField);
	fieldPanel.add(decField);
	fieldPanel.add(dcField);
	fieldPanel.add(dsField);
	fieldPanel.add(blField);
	fieldPanel.add(datumDirBox);

	fieldPanel.add(isLabel);
	fieldPanel.add(tsLabel);
	fieldPanel.add(accLabel);
	fieldPanel.add(decLabel);
	fieldPanel.add(dcLabel);
	fieldPanel.add(dsLabel);
	fieldPanel.add(blLabel);
	fieldPanel.add(datumDirLabel);

	panel.add("0.01,0.02;0.15,0.99",nameLabel);
	panel.add("0.17,0.02;0.82,0.99", fieldPanel);
	panel.setBorder(new EtchedBorder(0));
	return panel;
    }
    /**
     *This method  is declared a static method since it
     *should not be called by each individual motor.  Rather, it should be called only once by the application
     *class when creating a motor parameter panel.
     *@return JPanel containing labels corresponding to visual components and laid out similarly to the JPanel
     *returned by {@link #getMotorParameterPanel()}
     */ 
    public static JPanel getMotorParameterLabelPanel() {
	JPanel fieldPanel = new JPanel();
	JPanel panel = new JPanel();
	fieldPanel.setLayout(new GridLayout(1,0));
	panel.setLayout(new RatioLayout());
	fieldPanel.add(new JLabel("Initial Speed",JLabel.CENTER));
	fieldPanel.add(new JLabel("Term Speed",JLabel.CENTER));
	fieldPanel.add(new JLabel("Acceleration",JLabel.CENTER));
	fieldPanel.add(new JLabel("Deceleration",JLabel.CENTER));
	fieldPanel.add(new JLabel("Drive Current",JLabel.CENTER));
	fieldPanel.add(new JLabel("Datum Speed",JLabel.CENTER));
	fieldPanel.add(new JLabel("Back Lash",JLabel.CENTER));
	fieldPanel.add(new JLabel("Datum Dir",JLabel.CENTER));
	//fieldPanel.add(new JLabel("",JLabel.CENTER));
	panel.add("0.01,0.02;0.15,0.99",new JLabel("Motor Name",JLabel.CENTER));
	panel.add("0.17,0.02;0.82,0.99", fieldPanel);
	return panel;
    }
    /** 
     *Helper method.  This method arranges all low-level visual components into a long, thin panel
     *(ideal for placement into a GridLayout container <p>
     *@return JPanel containing all low-level visual components laid out using {@link RatioLayout} 
     */
    public JPanel getMotorLowLevelPanel() {
	JPanel buttonPanel = new JPanel();
	JPanel panel = new JPanel();
	buttonPanel.setLayout(new GridLayout(1,0));
	//buttonPanel.add(loDatumButton);
	buttonPanel.add(loAbortButton);
	//buttonPanel.add(loParkButton);
	buttonPanel.add(originButton);
	buttonPanel.add(statusButton);
	panel.setLayout(new RatioLayout());
	panel.add("0.0,0.02;0.05,0.99",lowAnimation);
	panel.add("0.05,0.02;0.15,0.99", loNameCheckBox);
	panel.add("0.17,0.02;0.20,0.99", loStatusLabel);
	panel.add("0.38,0.02;0.10,0.99", loCombo);
	panel.add("0.48,0.02;0.08,0.99", loBacklashCheckBox);
	panel.add("0.56,0.02;0.10,0.99",loLastCommand);
	panel.add("0.76,0.02;0.24,0.99", buttonPanel);
	return panel;
    }
    /**
     *This method  is declared a static method since it
     *should not be called by each individual motor.  Rather, it should be called only once by the application
     *class when creating a motor low-level panel.
     *@return JPanel containing labels corresponding to visual components and laid out similarly to the JPanel
     *returned by {@link #getMotorLowLevelPanel()}
     */ 
    public static JPanel getMotorLowLevelLabelPanel() {
	JPanel panel = new JPanel();
	panel.setLayout(new RatioLayout());
	panel.add("0.05,0.02;0.15,0.99", new JLabel("Motor Name",JLabel.LEFT));
	panel.add("0.17,0.02;0.20,0.99", new JLabel("Current Position",JLabel.RIGHT));
	panel.add("0.38,0.02;0.10,0.99", new JLabel("Steps to Move",JLabel.CENTER));
	panel.add("0.56,0.02;0.10,0.99", new JLabel("Previous Steps",JLabel.CENTER));
	return panel;
    }
    /** 
     *Helper method.  This method arranges all high-level visual components into a long, thin panel
     *(ideal for placement into a GridLayout container <p>
     *@return JPanel containing all high-level visual components laid out using {@link RatioLayout} 
     */
    public JPanel getMotorHighLevelPanel() {
	JPanel buttonPanel = new JPanel();
	JPanel panel = new JPanel();
	buttonPanel.setLayout(new GridLayout(1,0));
	//buttonPanel.add(hiDatumButton);
	//buttonPanel.add(hiParkButton);
	buttonPanel.add(hiAbortButton);
	panel.setLayout(new RatioLayout());
	panel.add("0.0,0.02;0.05,0.99",highAnimation);
	panel.add("0.05,0.02;0.15,0.99", hiNameCheckBox);
	panel.add("0.17,0.02;0.20,0.99", hiStatusLabel);
	panel.add("0.38,0.02;0.10,0.99", namedLocationBox);
	panel.add("0.48,0.02;0.08,0.99", hiBacklashCheckBox);
	panel.add("0.56,0.02;0.10,0.99", hiLastCommand);
	panel.add("0.86,0.02;0.14,0.99", buttonPanel);
	return panel;
    }
    /**
     *This method  is declared a static method since it
     *should not be called by each individual motor.  Rather, it should be called only once by the application
     *class when creating a motor high-level panel.
     *@return JPanel containing labels corresponding to visual components and laid out similarly to the JPanel
     *returned by {@link #getMotorHighLevelPanel()}
     */ 
    public static JPanel getMotorHighLevelLabelPanel() {
	JPanel panel = new JPanel();
	panel.setLayout(new RatioLayout());
	panel.add("0.05,0.02;0.15,0.99", new JLabel("Motor Name",JLabel.LEFT));
	panel.add("0.17,0.02;0.20,0.99", new JLabel("Current Position",JLabel.RIGHT));
	panel.add("0.38,0.02;0.10,0.99", new JLabel("New Position",JLabel.CENTER));
	panel.add("0.56,0.02;0.10,0.99", new JLabel("Previous Position",JLabel.CENTER));
	return panel;
    }

    public static JPanel getMotorSubSystemPanel() {
	return new FJECSubSystemPanel("cc:");
    }

}


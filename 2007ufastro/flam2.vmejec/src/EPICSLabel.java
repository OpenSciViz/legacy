
//Title:        JAVA Engineering Console
//Version:      1.0
//Copyright:    Copyright (c) Latif Albusairi and Tom Kisko
//Author:       David Rashkin, Latif Albusairi and Tom Kisko
//Company:      University of Florida
//Description:

package uffjec;

import ufjca.*;

import java.awt.*;
import java.awt.event.*;
import java.awt.Toolkit;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

//===============================================================================
/**
 * Handles Labels related to EPICS
 */
public class EPICSLabel extends JLabel implements UFCAToolkit.MonitorListener {
  public static final String rcsID = "$Name:  $ $Id: EPICSLabel.java,v 0.17 2005/08/05 19:04:35 drashkin Exp $";
  String monitorRec;
  String prefix;
  boolean showRec;
  JButton button = null;
  long timeStamp = 0;
    String outVal = "";

//-------------------------------------------------------------------------------
  /**
   * Default Constructor
   * Initializes variables using the method jbInit
   *@param rec String: Record field
   */
  public EPICSLabel(String rec) {
    try  {
      jbInit(rec,"");
    }
    catch(Exception ex) {
      fjecError.show("Error in creating an EPICSTextField: " + ex.toString());
    }
  } //end of EPICSLabel
//-------------------------------------------------------------------------------
  /**
   * Default Constructor
   * Initializes variables using the method jbInit
   *@param rec String: Record field
   *@param prefix String: Text to preceed record value in the label text
   */
  public EPICSLabel(String rec, String prefix) {
    try  {
      jbInit(rec,prefix);
    }
    catch(Exception ex) {
      fjecError.show("Error in creating an EPICSTextField: " + ex.toString());
    }
  } //end of EPICSLabel

//-------------------------------------------------------------------------------
  /**
   * Constructor used to associate this EPICSLabel with
   * a JButton
   * Initializes variables using the method jbInit
   *@param rec String: Record field
   *@param prefix String: Text to preceed record value in the label text
   *@param button JButton: Button to associate with this EPICSLabel
   */
  public EPICSLabel(String rec, String prefix, JButton button) {
    try  {
      jbInit(rec,prefix,button);
    }
    catch(Exception ex) {
      fjecError.show("Error in creating an EPICSTextField: " + ex.toString());
    }
  } //end of EPICSLabel

//-------------------------------------------------------------------------------
  /**
   *Component initialization
   *@param Rec String: Record field
   *@param prefix String: Text to preceed record value in the label text
   *@param button JButton: Button to associate with this EPICSLabel
   */
  private void jbInit(String rec, String prefix, JButton button) throws Exception {
    this.monitorRec = rec;
    this.prefix = prefix;
    this.button = button;
    this.timeStamp = 0;
    if (monitorRec.trim().equals("")) return;
 
    if (!EPICS.addMonitor(monitorRec,this)) 
	setBackground(Color.red);
    else
	setForeground(new JTextField().getForeground());
    /*
   // setup monitor(s)
    int mask = 7;
    
    try {
	gov.aps.jca.Channel theChannel = EPICS.theContext.createChannel(rec);
	
	EPICS.theContext.pendIO(EPICS.TIMEOUT);
	outMon = theChannel.addMonitor(mask,this);
	//EPICS.theContext.pendIO(EPICS.TIMEOUT);
	EPICS.theContext.flushIO();
	//EPICS.monitorCheat(outMon,this).start();
	//theChannel.destroy();
	//outMon = pv.addMonitor(new DBR_DBString(), this, null, mask);
	this.setForeground(new JTextField().getForeground());
    }
    catch (Exception ex) {
	this.setBackground(Color.red);
	}
    */
    this.setHorizontalAlignment(JLabel.LEFT);
  } //end of jbInit

    public void reconnect(String dbname) {
	EPICS.removeMonitor(monitorRec,this);
	monitorRec = dbname + monitorRec.substring(monitorRec.indexOf(":")+1);
	EPICS.addMonitor(monitorRec,this);
    }

    private void setToolTipText() {
	this.setToolTipText(monitorRec + " = " + outVal);
    }


//-------------------------------------------------------------------------------
  /**
   *Component initialization
   *@param Rec String: Record field
   *@param prefix String: Text to preceed record value in the label text
   */
  private void jbInit(String Rec, String prefix) throws Exception {
      jbInit(Rec, prefix, null);
  } //end of jbInit


    public long getTimeStamp() { return timeStamp; }

//-------------------------------------------------------------------------------
  /**
   * Event Handling Method
   *@param event MonitorEvent: TBD
   */
  public void monitorChanged(String value) {
    try {
      

      //if (event.pv().name().equals(this.monitorRec)) 
      {
	  outVal = value.trim();
	  this.setText(prefix + "  " + outVal + "   ");
	  if (button != null) {
	      String dbval = value.trim();
	      if( prefix.indexOf("CAR") >= 0 ) {
		  if( dbval.equals("BUSY") )
		      button.setEnabled(false);
		  else
		      button.setEnabled(true);
	      }
	      else if (checkforBadSocket(dbval)) {
		  button.setBackground(Color.red);
		  button.setForeground(Color.white);
		  button.setText("Reconnect");
	      }
	  }
          if( this.getText().indexOf("WARN") >= 0 ) Toolkit.getDefaultToolkit().beep();
          if( this.getText().indexOf("ERR") >= 0 ) {
	      Toolkit.getDefaultToolkit().beep();
	      Toolkit.getDefaultToolkit().beep();
	  }
      } 
      timeStamp = System.currentTimeMillis();
      setToolTipText();
    }
    catch (Exception ex) {
       fjecError.show("Error in monitor changed for EPICSLabel: " + monitorRec + " " + ex.toString());
    } 
       
  } //end of monitorChanged
    
//-------------------------------------------------------------------------------

  boolean checkforBadSocket( String EPICSmessage ) {	
    if( EPICSmessage.toUpperCase().indexOf("BAD SOCKET") >= 0 ||
	EPICSmessage.toUpperCase().indexOf("TIMED OUT") > 0   ||
	EPICSmessage.toUpperCase().indexOf("ERROR CONNECTING") >= 0 )
      return true;
    else
      return false;
  }

//-------------------------------------------------------------------------------
  /**
   * Event Handling Method
   *@param e WindowEvent: TBD
   */
  protected void processWindowEvent(WindowEvent e) {
    if(e.getID() == WindowEvent.WINDOW_CLOSING) {
      try {
        EPICS.removeMonitor(monitorRec,this);
      }
      catch (Exception ee) {
        fjecError.show(e.toString());
      }
      ///dispose();
    }
    ///super.processWindowEvent(e);
  } //end of processWindowEvent

} //end of class EPICSTextField


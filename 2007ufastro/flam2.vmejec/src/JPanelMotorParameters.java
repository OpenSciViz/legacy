package uffjec;
   import java.awt.event.*;
   import javax.swing.*;
   import java.awt.*;
   import java.net.*;
   import java.applet.*;
   import java.io.*;
   import java.util.*;


//===============================================================================
/**
 *Parameters tabbed pane
 */
public class JPanelMotorParameters extends JPanel {

    /**
     *Default constructor
     *@param fjecmotors FJECMotor[]: Array of FJECMotors
     */
    public JPanelMotorParameters(FJECMotor [] fjecmotors) {
	JPanel internalPanel = new JPanel();
	internalPanel.setLayout(new GridLayout(0,1));
	internalPanel.add(FJECMotor.getMotorParameterLabelPanel());
	for (int i=0; i<fjecmotors.length; i++) {
	    internalPanel.add(fjecmotors[i].getMotorParameterPanel());	  
	    //JPanel j = new JPanel();
	    //final FJECMotor [] jcm = fjecmotors;
	    //final int k = i;
	    //j.add(new JButton("Connect to Agent") {
	    //  public void actionPerformed(ActionEvent ae) {
	    //      jcm[k].connect();
	    //  }
	    //  });
	    //add(j);
	}
	setLayout(new RatioLayout());
	add("0.01,0.01;0.99,0.85",internalPanel);
	add(EPICSApplyButton.consistantLayout,new EPICSApplyButton());
	add(FJECSubSystemPanel.consistantLayout,new FJECSubSystemPanel("cc:"));
    }
      
}

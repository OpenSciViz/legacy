package uffjec;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Vector;


public class EPICSTest implements MonitorListener{

    JTextField recField;
    JTextField valField;
    JButton putButton;
    JButton getButton;
    JButton monButton;
    final double TIMEOUT = 5.0;
    Vector monVec;

    public EPICSTest() {
	JFrame j = new JFrame();
	JPanel p = new JPanel();
	monVec = new Vector();
	recField = new JTextField("flam:ec:setup:sad:LS218Kelvin1",10);
	valField = new JTextField("",10);
	putButton = new JButton("Put");
	getButton = new JButton("Get");
	monButton = new JButton("Mon");
	p.add(recField); p.add(valField);p.add(putButton); p.add(getButton); 
	p.add(monButton);
	putButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    put(recField.getText(),valField.getText());
		}
	    });
	getButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    valField.setText(get(recField.getText()));
		}
	    });
	monButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    addMonitor();
		}
	    });
	Thread pollThread = new Thread() {
		public void run () {
		    while (true) {
			get("flam:ec:heartbeat");
			try { Thread.currentThread().sleep(1000);}
			catch (Exception e) {}
		    }
		}
	    };
	//	pollThread.start();
	j.getContentPane().add(p);
	j.setSize(400,400);
	j.setLocation(100,100);
	j.setVisible(true);
	j.setDefaultCloseOperation(3);
    }

    public void addMonitor() {
	try {
	    int mask = Monitor.VALUE;
	    PV pv = new PV(recField.getText());
	    Ca.pendIO(TIMEOUT);
	    monVec.add(pv.addMonitor(new DBR_DBString(), this, null, mask));
	    Ca.pendIO(TIMEOUT);
	}
	catch (Exception ex) {System.out.println(ex.toString()); }
    }

    public void monitorChanged(MonitorEvent event) {
	try {
	    JOptionPane.showMessageDialog(null,event.pv().name());
	    event.pv().addMonitor(new DBR_DBString(),this,null,Monitor.VALUE);
	    Ca.pendIO(TIMEOUT);
	}
	catch (Exception e) {System.out.println(e.toString()); }
	//System.out.println(event.pv().name());
    }
    
    public void put(String rec, String val) {
	try {
	    PV pv = new PV(rec);
	    Ca.pendIO(TIMEOUT);
	    pv.put(val);
	    Ca.pendIO(TIMEOUT);
	}
	catch (Exception exp) {
	    fjecError.show(exp.toString() + " " + rec);
	}
    } //end of put

    public synchronized String get(String rec) {
	String val = "";
	try {
	    PV pv = new PV(rec);
	    Ca.pendIO(TIMEOUT);
	    DBR_DBString dbr = new DBR_DBString("");
	    pv.get(dbr);
	    Ca.pendIO(TIMEOUT);
	    val = dbr.valueAt(0);
	}
	catch (Exception exp) {
	    fjecError.show(exp.toString() + " " + rec);
	    //System.out.println(exp.toString());
	}
	return val;
    } //end of get
    
    public static void main(String [] args) {
	new EPICSTest();
    }

}

package uffjec;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.imageio.ImageIO;
import java.io.File;

public abstract class UFGraphicsPanel extends JPanel {

    protected String filename;
    private Image img;

    public void paint(Graphics g) {
	try {
	    if (img == null && filename != null && filename != "") 
		img = ImageIO.read(new File(filename));
	    if (img != null)
		g.drawImage(img,0,0,this);
	} catch (java.io.IOException ioe) {
	    System.err.println("UFGraphicsPanel.paint> "+ioe.toString());
	}
	paintComponents(g);
    }

}

package uffjec;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.event.*;

//===============================================================================
  /**
   *Object storing and handling temperature sensor parameters
   */
  public class TemperatureSensorParameter
  {
    EPICSTextField temp;
    JTextField temp2;
    JLabel label = new JLabel();
    JLabel units = new JLabel("K");
    JTextField statusColor = new JTextField("",2);
    GraphElement graphElement = new GraphElement();
    double currentTemp;
    double hotThreshold;
    double coldThreshold;
    String channel;
    int model;
    String prefix = EPICS.prefix;

//-------------------------------------------------------------------------------
      /**
       *Constructor for TemperatureSensorParameter
       */
      TemperatureSensorParameter( String name , String epicsRecName) {
         label.setText(name);
         temp = new EPICSTextField(prefix + epicsRecName,"No Description");
	 //JTextField temp = new JTextField();
	 temp2 = null;
         statusColor.setBackground(new Color(255,0,0));
         statusColor.setEditable(false);
	 statusColor.setToolTipText("<html>Red -- Warm, OK to open<br>Yellow -- In Transition<br>Blue -- Cold, OK for Observation</html>");
         temp.setEditable(false);
         temp.setBackground(new Color(175,175,175));

         temp.getDocument().addDocumentListener(new DocumentListener() {
            public void insertUpdate(DocumentEvent e) {
              setTemperature (e);
            }
            public void removeUpdate(DocumentEvent e) {
		//setTemperature (e);
            }
            public void changedUpdate(DocumentEvent e) {
		//setTemperature (e);
            }
         });
	 checkThreshold();
         currentTemp = 0;
         hotThreshold = 0;
         coldThreshold = 0;
         channel = "0";
         model = 0;
      } //end of TemperatureSsensorParameter

//-------------------------------------------------------------------------------
      /**
       *Sets the temperature
       *@param e not used
       */
      public void setTemperature(DocumentEvent e) {
        String s = temp.getText();
	if (temp2 != null) temp2.setText(s);
	if( s.trim().length() > 0 ) {
	    try { currentTemp = Double.parseDouble(s); }
	    catch (Exception ex) { } //fjecError.show("Bad Temperature Reading"); }
	    checkThreshold();
	}
      } //end of setTemperature

//-------------------------------------------------------------------------------
      /**
       *TBD
       */
      public void checkThreshold()
      {
	  if (currentTemp > hotThreshold)
	      statusColor.setBackground( Color.red );
	  else
	      if (currentTemp < coldThreshold)
		  statusColor.setBackground( Color.blue );
	      else statusColor.setBackground( Color.yellow );
      } //end of checkThreshold

   } //end of class TemperatureSensorParameter

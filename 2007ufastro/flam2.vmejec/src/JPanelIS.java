package uffjec;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.io.File;

import ufjca.*;

public class JPanelIS extends JPanel {

    JButton bootButton;
    JButton initButton;
    JButton datumButton;
    JButton testButton;
    JButton stopButton;
    JButton continueButton;
    JButton pauseButton;
    JButton abortButton;
    JButton observeButton;
    
    JLabel observeTimer;

    public JPanel getSetupPanel() {
	JPanel setupPanel = new JPanel();

	bootButton = new EPICSApplyButton("Reboot",EPICS.prefix+"reboot.DIR");
	initButton = new EPICSApplyButton("Init",EPICS.prefix+"init.DIR");
	datumButton = new EPICSApplyButton("Datum",EPICS.prefix+"datum.DIR");
	testButton = new EPICSApplyButton("Test",EPICS.prefix+"test.DIR");
	JPanel buttonPanel = new JPanel();
	buttonPanel.setLayout(new GridLayout(2,4,10,10));
	buttonPanel.add(bootButton);
	buttonPanel.add(initButton);
	buttonPanel.add(datumButton);
	buttonPanel.add(testButton);
	buttonPanel.add(new EPICSLabel(EPICS.prefix+"rebootC","rebootC:"));
	buttonPanel.add(new EPICSLabel(EPICS.prefix+"initC","initC:"));
	buttonPanel.add(new EPICSLabel(EPICS.prefix+"datumC","datumC:"));
	buttonPanel.add(new EPICSLabel(EPICS.prefix+"testC","testC:"));

	String insPrefix = EPICS.prefix+"instrumentSetup.";
	JPanel insPanel = new JPanel();	
	insPanel.setLayout(new RatioLayout());
	insPanel.add("0.01,0.01;0.99,0.05",new JLabel("Instrument Setup",JLabel.CENTER));
	insPanel.add("0.01,0.05;0.33,0.05",new JLabel("Name",JLabel.CENTER));
	insPanel.add("0.33,0.05;0.33,0.05",new JLabel("Input",JLabel.CENTER));
	insPanel.add("0.66,0.05;0.33,0.05",new JLabel("Output",JLabel.CENTER));
	insPanel.add("0.01,0.11;0.33,0.05",new JLabel("BeamMode"));
	String [] strs0 = {"f/16","MCAO_over","MCAO_under"};
	insPanel.add("0.33,0.11;0.33,0.05",new EPICSComboBox(insPrefix+"A",strs0,EPICSComboBox.ITEM));
	insPanel.add("0.66,0.11;0.33,0.05",new EPICSLabel(insPrefix+"VALA",""));
	insPanel.add("0.01,0.16;0.33,0.05",new JLabel("WheelBiasMode"));
	String [] strs1 = {"imaging","long_slit","mos"};
	insPanel.add("0.33,0.16;0.33,0.05",new EPICSComboBox(insPrefix+"B",strs1,EPICSComboBox.ITEM));
	insPanel.add("0.66,0.16;0.33,0.05",new EPICSLabel(insPrefix+"VALB",""));
	insPanel.add("0.01,0.21;0.33,0.05",new JLabel("WindowCover"));
	String [] strs2 = {"open","closed"};
	insPanel.add("0.33,0.21;0.33,0.05",new EPICSComboBox(insPrefix+"C",strs2,EPICSComboBox.ITEM));
	insPanel.add("0.66,0.21;0.33,0.05",new EPICSLabel(insPrefix+"VALC",""));
	insPanel.add("0.01,0.26;0.33,0.05",new JLabel("Decker"));
	insPanel.add("0.33,0.26;0.33,0.05",new EPICSComboBox(insPrefix+"D",strs1,EPICSComboBox.ITEM));
	insPanel.add("0.66,0.26;0.33,0.05",new EPICSLabel(insPrefix+"VALD",""));
	insPanel.add("0.01,0.31;0.33,0.05",new JLabel("MOSSlit"));
	String [] strs3 = {"imaging","circle1","circle2","1pix-slit","2pix-slit","3pix-slit","4pix-slit","6pix-slit","8pix-slit","mos1",
			   "mos2","mos3","mos4","mos5","mos6","mos7","mos8","mos9"};
	insPanel.add("0.33,0.31;0.33,0.05",new EPICSComboBox(insPrefix+"E",strs3,EPICSComboBox.ITEM));
	insPanel.add("0.66,0.31;0.33,0.05",new EPICSLabel(insPrefix+"VALE",""));
	insPanel.add("0.01,0.36;0.33,0.05",new JLabel("Filter"));
	String [] strs4 = {"open","dark","J-lo","J","H","Ks","JH","HK","TBD1"};
	insPanel.add("0.33,0.36;0.33,0.05",new EPICSComboBox(insPrefix+"F",strs4,EPICSComboBox.ITEM));
	insPanel.add("0.66,0.36;0.33,0.05",new EPICSLabel(insPrefix+"VALF",""));
	insPanel.add("0.01,0.41;0.33,0.05",new JLabel("Lyot"));
	String [] strs5 = {"f/16","MCAO_over","MCAO_under","H1","H2"};
	insPanel.add("0.33,0.41;0.33,0.05",new EPICSComboBox(insPrefix+"G",strs5,EPICSComboBox.ITEM));
	insPanel.add("0.66,0.41;0.33,0.05",new EPICSLabel(insPrefix+"VALG",""));
	insPanel.add("0.01,0.46;0.33,0.05",new JLabel("Grism"));
	String [] strs6 = {"open","JH","HK","JHK","TBD1"};
	insPanel.add("0.33,0.46;0.33,0.05",new EPICSComboBox(insPrefix+"H",strs6,EPICSComboBox.ITEM));
	insPanel.add("0.66,0.46;0.33,0.05",new EPICSLabel(insPrefix+"VALH",""));
	insPanel.add("0.01,0.51;0.33,0.05",new JLabel("DetPosFocus"));
	String [] strs7 = {"f/16","MCAO"};
	insPanel.add("0.33,0.51;0.33,0.05",new EPICSComboBox(insPrefix+"I",strs7,EPICSComboBox.ITEM));
	insPanel.add("0.66,0.51;0.33,0.05",new EPICSLabel(insPrefix+"VALI",""));
	insPanel.add("0.01,0.56;0.33,0.05",new JLabel("BiasMode"));
	String [] strs8 = {"Imaging","Spectroscopy"};
	insPanel.add("0.33,0.56;0.33,0.05",new EPICSComboBox(insPrefix+"J",strs8,EPICSComboBox.ITEM));
	insPanel.add("0.66,0.56;0.33,0.05",new EPICSLabel(insPrefix+"VALJ",""));
	insPanel.add("0.01,0.61;0.33,0.05",new JLabel("OverrideDecker"));
	String [] strs9 = {"FALSE","TRUE"};
	insPanel.add("0.33,0.61;0.33,0.05",new EPICSComboBox(insPrefix+"K",strs9,EPICSComboBox.ITEM));
	insPanel.add("0.66,0.61;0.33,0.05",new EPICSLabel(insPrefix+"VALK",""));
	insPanel.add("0.01,0.66;0.33,0.05",new JLabel("OverrideLyot"));
	insPanel.add("0.33,0.66;0.33,0.05",new EPICSComboBox(insPrefix+"L",strs9,EPICSComboBox.ITEM));
	insPanel.add("0.66,0.66;0.33,0.05",new EPICSLabel(insPrefix+"VALL",""));
	insPanel.add("0.01,0.71;0.33,0.05",new JLabel("OverrideGrism"));
	insPanel.add("0.33,0.71;0.33,0.05",new EPICSComboBox(insPrefix+"M",strs9,EPICSComboBox.ITEM));
	insPanel.add("0.66,0.71;0.33,0.05",new EPICSLabel(insPrefix+"VALM",""));
	insPanel.add("0.01,0.76;0.33,0.05",new JLabel("OverrideDetPos"));
	insPanel.add("0.33,0.76;0.33,0.05",new EPICSComboBox(insPrefix+"N",strs9,EPICSComboBox.ITEM));
	insPanel.add("0.66,0.76;0.33,0.05",new EPICSLabel(insPrefix+"VALN",""));
	insPanel.add("0.01,0.81;0.33,0.05",new JLabel("OverrideDetBias"));
	insPanel.add("0.33,0.81;0.33,0.05",new EPICSComboBox(insPrefix+"O",strs9,EPICSComboBox.ITEM));
	insPanel.add("0.66,0.81;0.33,0.05",new EPICSLabel(insPrefix+"VALO",""));

	
// 	insPanel.add("0.01,0.86;0.49,0.05",new JLabel("CamPowerA"));
// 	insPanel.add("0.51,0.86;0.49,0.05",new EPICSTextField(insPrefix+"P",""));
// 	insPanel.add("0.01,0.91;0.49,0.05",new JLabel("CamSetPointA"));
// 	insPanel.add("0.51,0.91;0.49,0.05",new EPICSTextField(insPrefix+"Q",""));
// 	insPanel.add("0.01,0.96;0.49,0.05",new JLabel("CamPowerB"));
// 	insPanel.add("0.51,0.96;0.49,0.05",new EPICSTextField(insPrefix+"R",""));
// 	insPanel.add("0.01,0.46;0.49,0.05",new JLabel("CamSetPointB"));
// 	insPanel.add("0.51,0.46;0.49,0.05",new EPICSTextField(insPrefix+"S",""));
// 	insPanel.add("0.01,0.66;0.49,0.05",new JLabel("MosPower"));
// 	insPanel.add("0.51,0.66;0.49,0.05",new EPICSTextField(insPrefix+"T",""));
// 	insPanel.add("0.01,0.66;0.49,0.05",new JLabel("MosSetPoint"));
// 	insPanel.add("0.51,0.66;0.49,0.05",new EPICSTextField(insPrefix+"U",""));


	insPanel.setBorder(new EtchedBorder(0));

	String obsPrefix = EPICS.prefix+"observationSetup.";
	JPanel obsSetupPanel = new JPanel();
	obsSetupPanel.setLayout(new RatioLayout());
	obsSetupPanel.add("0.01,0.01;0.99,0.10",new JLabel("Observation Setup",JLabel.CENTER));
	obsSetupPanel.add("0.01,0.11;0.33,0.15",new JLabel("ExpTime"));
	obsSetupPanel.add("0.33,0.11;0.33,0.15",new EPICSTextField(obsPrefix+"A",""));
	obsSetupPanel.add("0.66,0.11;0.33,0.15",new EPICSLabel(obsPrefix+"VALA",""));
	obsSetupPanel.add("0.01,0.26;0.33,0.15",new JLabel("NumReads"));
	String [] strs10 = {"1","3","4","5","6","7","8","9","10","11","12","13","14","15","16"};
	obsSetupPanel.add("0.33,0.26;0.33,0.15",new EPICSComboBox(obsPrefix+"B",strs10,EPICSComboBox.ITEM));
	obsSetupPanel.add("0.66,0.26;0.33,0.15",new EPICSLabel(obsPrefix+"VALB",""));
	obsSetupPanel.add("0.01,0.41;0.33,0.15",new JLabel("readoutMode"));
	String [] strs11 = {"SCI","ENG"};
	obsSetupPanel.add("0.33,0.41;0.33,0.15",new EPICSComboBox(obsPrefix+"C",strs11,EPICSComboBox.ITEM));
	obsSetupPanel.add("0.66,0.41;0.33,0.15",new EPICSLabel(obsPrefix+"VALC",""));
	obsSetupPanel.add("0.01,0.56;0.33,0.15",new JLabel("ObsModeBias"));
	obsSetupPanel.add("0.33,0.56;0.33,0.15",new EPICSTextField(obsPrefix+"D",""));
	obsSetupPanel.add("0.66,0.56;0.33,0.15",new EPICSLabel(obsPrefix+"VALD",""));
	obsSetupPanel.add("0.01,0.71;0.33,0.15",new JLabel("OverrideBias"));
	obsSetupPanel.add("0.33,0.71;0.33,0.15",new EPICSTextField(obsPrefix+"E",""));
	obsSetupPanel.add("0.66,0.71;0.33,0.15",new EPICSLabel(obsPrefix+"VALE",""));
	obsSetupPanel.setBorder(new EtchedBorder(0));

	JPanel statusPanel = new JPanel();
	statusPanel.setLayout(new RatioLayout());
	statusPanel.add("0.01,0.01;0.99,0.33",new JLabel("Status",JLabel.CENTER));
	statusPanel.add("0.01,0.34;0.49,0.33",new JLabel("instrumentSetupC"));
	statusPanel.add("0.51,0.34;0.49,0.33",new EPICSLabel(EPICS.prefix+"instrumentSetupC",""));
	statusPanel.add("0.01,0.67;0.49,0.33",new JLabel("observationSetupC"));
	statusPanel.add("0.51,0.67;0.49,0.33",new EPICSLabel(EPICS.prefix+"observationSetupC",""));
	statusPanel.setBorder(new EtchedBorder(0));

	setupPanel.setLayout(new RatioLayout());
	setupPanel.add(EPICSApplyButton.consistantLayout,new EPICSApplyButton());
	setupPanel.add("0.35,0.80;0.50,0.20",buttonPanel);
	setupPanel.add("0.10,0.01;0.35,0.75",insPanel);
	setupPanel.add("0.55,0.01;0.35,0.35",obsSetupPanel);
	setupPanel.add("0.55,0.40;0.25,0.20",statusPanel);

	return setupPanel;
    }
    
    public JPanel getObservePanel() {
	
	JPanel observePanel = new JPanel();

	pauseButton = new EPICSApplyButton("Pause",EPICS.prefix+"pause.DIR");
	continueButton = new EPICSApplyButton("Continue",EPICS.prefix+"continue.DIR");
   	stopButton = new EPICSApplyButton("Stop",EPICS.prefix+"stop.DIR");
	abortButton = new EPICSApplyButton("Abort",EPICS.prefix+"abort.DIR");
	observeButton = new EPICSApplyButton("Observe",EPICS.prefix+"observe.DIR");
	observeTimer = new JLabel("0 s");

	JPanel obsPanel = new JPanel();
	String obsPrefix = EPICS.prefix+"observe.";
	obsPanel.setLayout(new RatioLayout());
	obsPanel.add("0.01,0.01;0.99,0.20",new JLabel("Observe", JLabel.CENTER));
	obsPanel.add("0.01,0.21;0.49,0.20",new JLabel("DataLabel"));
	obsPanel.add("0.51,0.21;0.49,0.20",new EPICSTextField(obsPrefix+"A",""));
// 	obsPanel.add("0.01,0.41;0.49,0.20",new JLabel("DataMode"));
// 	obsPanel.add("0.51,0.41;0.49,0.20",new EPICSTextField(obsPrefix+"B",""));
// 	obsPanel.add("0.01,0.61;0.49,0.20",new JLabel("DitherIndex"));
// 	obsPanel.add("0.51,0.61;0.49,0.20",new EPICSTextField(obsPrefix+"C",""));
// 	obsPanel.add("0.01,0.81;0.49,0.20",new JLabel("UserInfo"));
// 	obsPanel.add("0.51,0.81;0.49,0.20",new EPICSTextField(obsPrefix+"D",""));
	obsPanel.setBorder(new EtchedBorder(0));

	//obsPanel.add("0.01,0.76;0.33,0.15",stopButton);
	//obsPanel.add("0.34,0.76;0.33,0.15",abortButton);
	//obsPanel.add("0.66,0.76;0.33,0.15",observeButton);
	//obsPanel.add("0.01,0.91;0.49,0.09",new JLabel("Time remaining:"));
	//obsPanel.add("0.51,0.91;0.49,0.09",observeTimer);

	JPanel buttonPanel = new JPanel();
	buttonPanel.setLayout(new GridLayout(2,5,10,10));
	buttonPanel.add(observeButton);
	buttonPanel.add(pauseButton);
	buttonPanel.add(continueButton);
	buttonPanel.add(stopButton);
	buttonPanel.add(abortButton);
	buttonPanel.add(new EPICSLabel(EPICS.prefix+"observeC","observeC:"));
	buttonPanel.add(new EPICSLabel(EPICS.prefix+"pauseC","pauseC:"));
	buttonPanel.add(new EPICSLabel(EPICS.prefix+"continueC","continueC:"));
	buttonPanel.add(new EPICSLabel(EPICS.prefix+"stopC","stopC:"));
	buttonPanel.add(new EPICSLabel(EPICS.prefix+"abortC","abortC:"));

	observePanel.setLayout(new RatioLayout());
	observePanel.add("0.35,0.10;0.35,0.35",obsPanel);
	observePanel.add("0.10,0.80;0.80,0.20",buttonPanel);

	return observePanel;
    }

    public JPanelIS() {

	JTabbedPane rootPane = new JTabbedPane();
	
	rootPane.add(getObservePanel(),"Observe");
	rootPane.add(getSetupPanel(),"Setup");
	setLayout(new BorderLayout());
	add(rootPane,BorderLayout.CENTER);
    }

    public static void main(String [] args) {
	EPICS.prefix = "foo:";
	if (args != null && args.length > 0)
	    EPICS.prefix = args[0];
	JFrame x = new JFrame();
	x.setContentPane(new JPanelIS());
	x.setSize(400,400);
	x.setVisible(true);
	x.setDefaultCloseOperation(3);
	UFCAToolkit.startMonitorLoop();
    }

}

============================
Filename  : UFJava.doc
============================
Written by: David Nordstedt
Date      : October 7, 1999
============================

The UFProtocol classes are to be used as the data transfer facility when
sending data across the network.  Using these classes, we can send and receive
these objects using a standard socket connection.

The java source code files that make up the UF protocol instances are a mirror
of the C++ files of the same object type.  Adjustments have been made because
of the differences in language constructs and security features in java versus C++.

The java protocol files in the ufdev/www directory are:

UFProtocol.java
UFTimeStamp.java
UFStrings.java
UFBytes.java
UFInts.java
UFFloats.java

Some classes used as utility classes for constructing applications with these
protocol classes are:

ImageCanvas.java
ImageUtil.java
MultiLineLabel.java
TextScroller.java
UFConnect.java
YesNoDialog.java

Some of the java test applications that have been used for testing during
production of the UF protocol classes are:

FetchFrame.java
UFGuiClient.java
clientCmd.java
ufping.java


While Work on the basic protocol classes is finished, enhancements/bug fixes
are still proceeding.  A known future addition to the UF protocol classes
will be a generic frame class that will store multiple frames of data instead
of one large array of data as our byte, int, and float classes now store the
data.

============================
End of File UFJava.doc
============================

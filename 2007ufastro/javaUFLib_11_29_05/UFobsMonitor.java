package javaUFLib;

//Title:        UFobsMonitor for Java Control Interface (JCI) using UFLib Protocol
//Version:      (see rcsID)
//Copyright:    Copyright (c) 2003-5
//Author:       Frank Varosi
//Company:      University of Florida
//Description:  for control and monitoring of CanariCam infrared camera system.

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import java.io.*;
import java.net.*;
import java.util.*;
import javaUFProtocol.*;

//===============================================================================
/**
 * Observation Status Monitor Panel
 * Custom panel for obs status monitoring of Detector Control Agent or Data Acq. Server.
 * @author Frank Varosi
 */

public class UFobsMonitor extends UFLibPanel
{
    public static final
	String rcsID = "$Name:  $ $Id: UFobsMonitor.java,v 1.65 2005/08/11 18:21:06 drashkin Exp $";

    //used by recvStatus() method:
    private int nullCnt = 0;
    private DataInputStream NotifyStream;
    private StatusMonitor obsMonitor;
    private int _obsCount = 0;
    private boolean _obsNow = false;

    protected final int maxProgBar = 500;
    protected JProgressBar obsProgressBar = new JProgressBar(0, maxProgBar);

    protected UFFrameConfig[] seqFrameConfs = new UFFrameConfig[100];
    protected UFFrameConfig[] saveFrameConfs = new UFFrameConfig[65536];

    protected UFStrings _FITSheader;
    protected String _FITSfile;

    protected UFObsConfig _ObsConfig;
    protected UFTextField acqServerStatus  = new UFTextField("Data Server:",false); //editable = false.

    protected UFTextPanel frmTotal = new UFTextPanel(0.3,"Total Frames:"); // no active fields:
    protected UFTextPanel frmDMAcnt = new UFTextPanel(0.3,"# DMAs:");
    protected UFTextPanel frmGrabCnt = new UFTextPanel(0.3,"# grabbed:");
    protected UFTextPanel frmProcCnt = new UFTextPanel(0.3,"# processed:");
    protected UFTextPanel frmWritCnt = new UFTextPanel(0.3,"# written:");
    protected UFTextPanel frmSendCnt = new UFTextPanel(0.3,"# sent:");

    protected UFTextPanel remainFrms = new UFTextPanel(0.4,"# Rem:");
    protected UFTextPanel nodBeam = new UFTextPanel(0.3," Beam:");
    protected UFTextPanel nodSet = new UFTextPanel(0.5,"  Nod:");
    protected UFTextPanel nodTotal = new UFTextPanel(0.5," of:");
    private String[] BeamNames = {"A","B"};

    protected UFTextMinMax bgADUs = new UFTextMinMax("Bkgr.ADUs:");
    protected UFTextMinMax rdADUs = new UFTextMinMax("Read ADUs:");
    protected UFTextMinMax bgSigma = new UFTextMinMax("Bkgr.Noise:");
    protected UFTextMinMax rdSigma = new UFTextMinMax("Read Noise:");

    private HashMap statusDisplay = new HashMap(4);  //container of following UFTextField objects:
    protected UFTextField observStatus = new UFTextField("observStatus",false);
    protected UFTextField dcState      = new UFTextField("dcState",false);
    protected UFTextField detType      = new UFTextField("detType",false);
    protected UFTextField archiveFile  = new UFTextField("FITS_FileName",false);

    protected UFLibPanel MasterPanel;   //used to get new params status.
    protected UFLibPanel DetectorPanel; //used to get same host and port as in Detector Panel.
    protected UFLibPanel BiasPanel;     //used to set status of bias power and levels.
    protected UFLibPanel DataDisplay;   //option for quick-look data display updates.
//-------------------------------------------------------------------------------
    /**
     * Default constructor.
     */
    public UFobsMonitor() {
	super( "kepler", 52008 ); //to give default host and port to UFLibPanel.
	initStatusPanel();
    } 
//-------------------------------------------------------------------------------
    /**
     * Special constructor to get pointer to Detector panel in JCI.
     */
    public UFobsMonitor( UFLibPanel DetectorPanel )
    {
	this.setHostAndPort( DetectorPanel._Host, DetectorPanel._Port );
	this.DetectorPanel = DetectorPanel;
	initStatusPanel();
    }
//-------------------------------------------------------------------------------
    /**
     * Special constructor to get pointers to Detector and Bias panels in JCI.
     */
    public UFobsMonitor( UFLibPanel DetectorPanel, UFLibPanel BiasPanel )
    {
	this.setHostAndPort( DetectorPanel._Host, DetectorPanel._Port );
	this.DetectorPanel = DetectorPanel;
	this.BiasPanel = BiasPanel;
	initStatusPanel();
    }
//-------------------------------------------------------------------------------
    /**
     * Special constructor to get pointers to Master, Detector and Bias panels in JCI.
     */
    public UFobsMonitor( UFLibPanel MasterPanel, UFLibPanel DetectorPanel, UFLibPanel BiasPanel )
    {
	this.setHostAndPort( DetectorPanel._Host, DetectorPanel._Port );
	this.MasterPanel = MasterPanel;
	this.DetectorPanel = DetectorPanel;
	this.BiasPanel = BiasPanel;
	initStatusPanel();
    }
//-------------------------------------------------------------------------------
    /**
     * Special constructor to get pointer to data display/access client.
     */
    public UFobsMonitor( String hostName, int portNum, UFLibPanel dataDispClient )
    {
	this.setHostAndPort( hostName, portNum );
	this.DataDisplay = dataDispClient;
	initStatusPanel();
    }
//-------------------------------------------------------------------------------
    /**
     * Private method: initStatusPanel()
     * Calls private method createStatusPanel() to do all component initialization,
     *  and creates StatusMonitor object to wait for and recv status from agent/server.
     */
    private void initStatusPanel()
    {
	try {
	    createStatusPanel();
	}
	catch (Exception e) {
	    e.printStackTrace();
	    System.err.println("UFobsMonitor> Failed creating Obs.Status display panel!");
	}

	try {
	    obsMonitor = new StatusMonitor(1000);
	}
	catch (Exception e) {
	    e.printStackTrace();
	    System.err.println("UFobsMonitor> Failed creating agent/server status monitor!");
	}
    }
//-------------------------------------------------------------------------------
    /**
     *Component initialization
     */
    private void createStatusPanel() throws Exception
    {
	obsProgressBar.setValue(0);
	obsProgressBar.setBackground( Color.white );

	statusDisplay.put( observStatus.name(), observStatus );
	statusDisplay.put( dcState.name(), dcState );
	statusDisplay.put( detType.name(), detType );
	statusDisplay.put( archiveFile.name(), archiveFile );


	JPanel frmCntsPanel = new JPanel();
	frmCntsPanel.setLayout(new GridLayout(0,1));
	frmCntsPanel.setBorder(new EtchedBorder(0));
	frmCntsPanel.add(frmTotal);
	frmCntsPanel.add(frmDMAcnt);
	frmCntsPanel.add(frmGrabCnt);
	frmCntsPanel.add(frmProcCnt);
	frmCntsPanel.add(frmWritCnt);
	frmCntsPanel.add(frmSendCnt);

	JPanel nodbeamPanel = new JPanel();
	nodbeamPanel.setLayout(new GridLayout(1,0));
	nodbeamPanel.add( remainFrms );
	nodbeamPanel.add( nodBeam );
	nodbeamPanel.add( nodSet );
	nodbeamPanel.add( nodTotal );

	JPanel statisticsPanel = new JPanel();
	statisticsPanel.setLayout(new GridLayout(0,1));
	statisticsPanel.setBorder(new EtchedBorder(0));
	JPanel titlePanel = new JPanel();
	titlePanel.setLayout(new RatioLayout());
	titlePanel.add("0.01,0.01;0.30,0.98", new JLabel(" Statistics:") );
	titlePanel.add("0.31,0.01;0.22,0.98", new JLabel(" current") );
	titlePanel.add("0.54,0.01;0.22,0.98", new JLabel(" min") );
	titlePanel.add("0.77,0.01;0.22,0.98", new JLabel(" max") );
	statisticsPanel.add( titlePanel );
	statisticsPanel.add( bgADUs );
	statisticsPanel.add( rdADUs );
	statisticsPanel.add( bgSigma );
	statisticsPanel.add( rdSigma );

	this.setLayout(new RatioLayout());
	this.add("0.01,0.03;0.09,0.17", new JLabel("obs. STATUS:") );
	this.add("0.10,0.03;0.43,0.17", observStatus );
	this.add("0.01,0.23;0.09,0.17", new JLabel("obs. Progress:") );
	this.add("0.10,0.23;0.43,0.17", obsProgressBar );
	this.add("0.01,0.43;0.09,0.17", new JLabel("DC - STATE:") );
	this.add("0.10,0.43;0.43,0.17", dcState );
	this.add("0.01,0.63;0.09,0.17", new JLabel(acqServerStatus.Label()) );
	this.add("0.10,0.63;0.43,0.17", acqServerStatus );
	this.add("0.01,0.83;0.09,0.17", new JLabel("FITS  File:") );
	this.add("0.10,0.83;0.43,0.17", archiveFile );
	this.add("0.54,0.01;0.15,0.99", frmCntsPanel );
	this.add("0.70,0.01;0.30,0.18", nodbeamPanel );
	this.add("0.70,0.19;0.30,0.81", statisticsPanel );

	// Add the connect status socket action to Detector Panel connect button,
	// so when user re-connects to DC agent it will do both cmdSocket and statusSocket:

	if( DetectorPanel != null ) {
	    DetectorPanel.connectButton.addActionListener( new ActionListener()
		{ public void actionPerformed(ActionEvent e) { connectToStatus(); } });
	}
	else if( DataDisplay != null ) {
	    DataDisplay.connectButton.addActionListener( new ActionListener()
		{ public void actionPerformed(ActionEvent e) { connectToStatus(); } });
	}
    }
//-------------------------------------------------------------------------------
    /**
     * UFobsMonitor#connectToStatus
     *  Connect socket to Detector Control Agent or Data Acq.Server and restart the status monitor.
     */
    public boolean connectToStatus()
    {
	obsMonitor.stopRunning();

	if( connectStatusSocket() )
	    {
		obsMonitor.resumeRunning();
		return true;
	    }
	else return false;
    }

    public boolean connectToAgent() { return connectToStatus(); }
    public boolean connectToServer() { return connectToStatus(); }

    protected boolean reConnect() {
	if( DetectorPanel != null ) {
	    DetectorPanel.connectToAgent();
	}
	else if( DataDisplay != null ) {
	    DataDisplay.connectToServer();
	}
	return connectToStatus();
    }
//-------------------------------------------------------------------------------
    /**
     * UFobsMonitor#connectStatusSocket
     *  Connect via socket to the ancillary automatic status feature Detector Control Agent,
     *  for recving (only) status messages.
     */
    protected boolean connectStatusSocket()
    {
	if( _socket != null ) { // close connection if one exists
	    try {
		System.out.println(className+".connectStatusSocket> closing status socket...");
		_socket.close();
		_socket = null;
		indicateConnectStatus(false);
	    }
	    catch (IOException ioe) {
		String message = className+".connectStatusSocket> " + ioe.toString();
		System.err.println( message );
	    }
	    try { Thread.sleep(100);} catch ( Exception _e) {}
	}

	try {
	    if( DetectorPanel != null ) {
		String h = DetectorPanel.getHostField();
		if( h != null ) { if( h.length() > 0 ) _Host = h; }
		int p = DetectorPanel.getPortField();
		if( p > 0 ) _Port = p;
	    }
	    else if( DataDisplay != null ) {
		String h = DataDisplay.getHostField();
		if( h != null ) { if( h.length() > 0 ) _Host = h; }
		int p = DataDisplay.getPortField();
		if( p > 0 ) _Port = p;
	    }
	    String message = className + ".connectStatusSocket> port=" + _Port + " @ host = " + _Host;
	    System.out.println( message );
	    InetSocketAddress agentIPsoca = new InetSocketAddress( _Host, _Port );
	    _socket = new Socket();
	    _socket.connect( agentIPsoca, CONNECT_TIMEOUT );
	    _socket.setSoTimeout( HANDSHAKE_TIMEOUT );

	    //must send DC agent a timestamp with string "status" in it:
	    UFTimeStamp uft = new UFTimeStamp(clientName + ":STATUS");

	    //but if talking to Data Acq.Server then request the CameraType:
	    if( DataDisplay != null ) uft.rename("CT");

	    if( uft.sendTo(_socket) <= 0 ) {
	        connectError("Handshake Send");
		return false;
	    }

	    //get response from agent
	    UFProtocol ufp = null;

	    if( (ufp = UFProtocol.createFrom(_socket)) == null ) {
	        connectError("Handshake Read");
		return false;
	    }

	    if( DataDisplay != null ) {  //special case: request notification stream of FrameConfigs:
		message = "CameraType = " + ufp.name();
		dcState.setNewState( message );
		//now request current obs & frame configs and status monitor thread will recv:
		uft.rename("OC");
		if( uft.sendTo(_socket) <= 0 ) {
		    sendError("connectStatusSocket","obs. config");
		    return false;
		}
		uft.rename("FH");
		if( uft.sendTo(_socket) <= 0 ) {
		    sendError("connectStatusSocket","FITS header");
		    return false;
		}
		uft.rename("FC");
		if( uft.sendTo(_socket) <= 0 ) {
		    sendError("connectStatusSocket","frame config");
		    return false;
		}
		//request notification stream of FrameConfig updates and status monitor thread will recv:
		uft.rename("NOTIFY");
		if( uft.sendTo(_socket) <= 0 ) {
		    sendError("connectStatusSocket","notification stream");
		    return false;
		}
		//create input stream object for recvStatus() method to check for available notifies:
		NotifyStream = new DataInputStream( _socket.getInputStream() );
	    }
	    else message = className + ".connectStatusSocket> " + ufp.name();

	    System.out.println( message );
	    indicateConnectStatus(true);
	    //set normal infinite timeout for socket so it blocks on recv:
	    _socket.setSoTimeout(0);
	    return true;
	}
	catch (Exception x) {
	    indicateConnectStatus(false);
	    _socket = null;
	    String message = className+".connectStatusSocket> " + x.toString();
	    System.err.println(message);
	    Toolkit.getDefaultToolkit().beep();
	    return false;
	}
    }
//-------------------------------------------------------------------------------

    protected void connectError( String errmsg )
    {
	String message = className + ".connectStatusSocket> " + errmsg + " ERROR";
	System.err.println(message);
	Toolkit.getDefaultToolkit().beep();
	indicateConnectStatus(false);

	if( _socket != null ) { // close connection if one exists
	    try {
		_socket.close();
		_socket = null;
	    }
	    catch (IOException ioe) {
		message = "connectError> " + ioe.toString();
		System.err.println(className + "::" + message);
	    }
	}
    }
//-------------------------------------------------------------------------------

    protected void sendError( String method, String errmsg )
    {
	String message = className + "." + method + "> " + errmsg + " request Send ERROR";
	System.err.println(message);
	Toolkit.getDefaultToolkit().beep();
	indicateConnectStatus(false);

	if( _socket != null ) { // close connection if one exists
	    try {
		_socket.close();
		_socket = null;
	    }
	    catch (IOException ioe) {
		message = "sendError> " + ioe.toString();
		System.err.println(className + "::" + message);
	    }
	}
    }
//-------------------------------------------------------------------------------

    public void indicateConnectStatus( boolean connStatus )
    {
	String target = "?";

	if( DetectorPanel != null ) {
	    DetectorPanel.indicateConnectStatus( connStatus );
	    target = "DC Agent";
	}
	else if( DataDisplay != null ) {
	    DataDisplay.indicateConnectStatus( connStatus );
	    target = "Data Acquisition Server";
	}

	if( connStatus )
	    target = "Connected to " + target;
	else
	    target = "Failed connecting to " + target;

	target += " on port=" + _Port + " @ host = " + _Host;
	observStatus.setNewState( target );
    }
//-------------------------------------------------------------------------------
    /**
     * Method used by inner class StatusMonitor (see next below).
     */
    protected void recvStatus()
    {
	UFProtocol ufp = UFProtocol.createFrom( _socket );

	if( ufp == null )
	    {
		String errmsg = className+"::recvStatus> recvd null object.";
		System.err.println( errmsg );
		System.out.println( errmsg );
		if( ++nullCnt > 2 ) {
		    nullCnt = 0;
		    reConnect();
		}
		else try { Thread.sleep( 700 ); } catch( Exception x ) {}
	    }
	else if( ufp instanceof UFFrameConfig && DataDisplay != null )
	    {
		UFFrameConfig frameConf =  (UFFrameConfig)ufp;
		checkObsStartOrEnd( frameConf );
		int notifyCnt = 0;
		seqFrameConfs[notifyCnt++] = frameConf;

		// Check for more FrameConfigs that may be queued in notification stream:
		// (set to short timeout so cannot get stuck, but > 0.3 sec for reliability)

		try {
		    while( NotifyStream.available() > 0 && notifyCnt < seqFrameConfs.length )
			{
			    ufp = UFProtocol.createFrom( _socket );

			    if( ufp == null ) {
				System.err.println( className+"::recvStatus> null object!" );
				break;
			    }

			    if( ufp instanceof UFFrameConfig )
				seqFrameConfs[notifyCnt++] = (UFFrameConfig)ufp;
			    else
				procStatusInfo( ufp );
			}
		}
		catch( IOException ioe ) {
		    System.err.println( className+"::recvStatus>"+ioe.toString() );
		}
		catch( Exception x ) {
		    System.err.println( className+"::recvStatus>" + x.toString() );
		}

		if( notifyCnt == 1 ) {
		    procFrameConfig( frameConf );
		    seqFrameConfs[0] = null;
		    return;
		}
		else if( notifyCnt >= seqFrameConfs.length )
		    System.err.println( className + "::recvStatus> notifyCnt=" + notifyCnt );

		// Process the sequence of FrameConfig notifications,
		// but check for duplicate name() of each and process only the final one
		// (since name() tells which frames are updated):

		for( int i=0; i < notifyCnt; i++ ) {
		    UFFrameConfig fc = seqFrameConfs[i];
		    if( fc != null ) {
			int k = i;
			for( int j=i+1; j < notifyCnt; j++ ) {
			    if( seqFrameConfs[j] != null ) {
				if( seqFrameConfs[j].name().equals( fc.name() ) ) {
				    seqFrameConfs[k] = null;
				    k = j;
				    fc = seqFrameConfs[j];
				}
			    }
			}
			procFrameConfig( fc );
			checkObsStartOrEnd( fc );
			seqFrameConfs[i] = null;
		    }
		}
	    }
	else procStatusInfo( ufp );
    }
//-------------------------------------------------------------------------------
    /**
     * Method used by recvStatus() to check the FrameConfig for Start/Abort/End of an obs.
     */
    protected void checkObsStartOrEnd( UFFrameConfig fc )
    {
	if( fc.frameProcCnt == 1 || fc.frameGrabCnt == 1 ) { //new obs: request ObsConfig and File info...
	    ++_obsCount;
	    String msg = "STARTED obs: # " + _obsCount;
	    observStatus.setNewState(msg);
	    System.out.println( msg + " ...requesting ObsConfig and FITS header...");
	    UFTimeStamp uft = new UFTimeStamp("OC");
	    if( uft.sendTo(_socket) <= 0 )
		sendError("checkObsStartOrEnd","obs. config");
	    else {
		uft.rename("FH");
		if( uft.sendTo(_socket) <= 0 ) sendError("checkObsStartOrEnd","FITS header");
	    }
	    for( int i=1; i < saveFrameConfs.length; i++ ) saveFrameConfs[i] = null;
	}
	else if( fc.frameObsTotal < 0 ) {
	    observStatus.setNewState("ABORTED.");
	}
	else if( fc.frameGrabCnt == fc.frameObsTotal ||
		 fc.frameProcCnt == fc.frameObsTotal ) {
	    observStatus.setNewState("COMPLETED.");
	}
	else if( fc.frameGrabCnt > 1 || fc.frameProcCnt > 1 )
	    {
		if( _ObsConfig.nodBeams() > 1 ) { //check for nodding...
		    int nfsave = _ObsConfig.chopBeams() * _ObsConfig.saveSets();
		    if( (fc.frameGrabCnt % nfsave) == 0 ) {
			_obsNow = false;
			observStatus.setNewState("Nod beam switch...");
		    }
		    else if( !_obsNow ) {
			_obsNow = true;
			observStatus.setNewState("Observation # " + _obsCount);
		    }
		}
		else if( !_obsNow ) {
		    _obsNow = true;
		    observStatus.setNewState("Observation # " + _obsCount);
		}
	    }

	if( fc.frameProcCnt >= 0 &&
	    fc.frameProcCnt < saveFrameConfs.length ) saveFrameConfs[fc.frameProcCnt] = fc;
    }
//-------------------------------------------------------------------------------
    /**
     * Method used by recvStatus() to process a UFFrameConfig object.
     */
    protected void procFrameConfig( UFFrameConfig newFC )
    {
	int totalFrames = newFC.frameObsTotal;
	if( totalFrames < 0 ) totalFrames = -totalFrames;
	if( totalFrames > 0 ) {
	    if( newFC.frameProcCnt > newFC.frameGrabCnt )
		obsProgressBar.setValue( ( maxProgBar * newFC.frameProcCnt )/totalFrames );
	    else
		obsProgressBar.setValue( ( maxProgBar * newFC.frameGrabCnt )/totalFrames );
	}
	else obsProgressBar.setValue(0);

	frmTotal.setValue( newFC.frameObsTotal );
	frmDMAcnt.setValue( newFC.DMAcnt );
	frmGrabCnt.setValue( newFC.frameGrabCnt );
	frmProcCnt.setValue( newFC.frameProcCnt );
	frmWritCnt.setValue( newFC.frameWriteCnt );
	frmSendCnt.setValue( newFC.frameSendCnt );
	nodSet.setValue( newFC.NodSet );

	if( _ObsConfig != null ) {
	    int nfsave = _ObsConfig.chopBeams() * _ObsConfig.saveSets();
	    int nfremain = nfsave - (newFC.frameGrabCnt % nfsave);
	    if( nfremain == nfsave ) nfremain = 0;
	    remainFrms.setValue( nfremain );
	}

	if( newFC.NodBeam >= 0 && newFC.NodBeam < BeamNames.length )
	    nodBeam.setValue( BeamNames[newFC.NodBeam] );
	else nodBeam.setValue("?");

	bgADUs.setCurrMinMax( newFC.bgADUs, newFC.bgADUmin, newFC.bgADUmax );
	rdADUs.setCurrMinMax( newFC.rdADUs, newFC.rdADUmin, newFC.rdADUmax );
	bgSigma.setCurrMinMax( newFC.sigmaFrmNoise, newFC.sigmaFrmMin, newFC.sigmaFrmMax );
	rdSigma.setCurrMinMax( newFC.sigmaReadNoise, newFC.sigmaReadMin, newFC.sigmaReadMax );

	if( DataDisplay != null ) {
	    acqServerStatus.setNewState( newFC.name() + "  @  " + newFC.timeStamp().substring(0,20) );
	    DataDisplay.updateFrames( newFC );
	}
	else {
	    acqServerStatus.setNewState( newFC.name() );
	    if( MasterPanel != null ) MasterPanel.updateFrames( newFC );
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Method used by recvStatus() to process the status info object.
     */
    protected void procStatusInfo( UFProtocol ufp )
    {
	if( ufp == null ) return;

	if( ufp instanceof UFFrameConfig )
	    {
		procFrameConfig( (UFFrameConfig)ufp );
	    }
	else if( ufp instanceof UFObsConfig )
	    {
		_ObsConfig = (UFObsConfig)ufp;
		nodTotal.setValue( _ObsConfig.nodSets() );

		if( DataDisplay != null ) {
		    if( _obsCount <= 0 ) _obsCount = 1;
		    System.out.println(" ObsConfig(" + _obsCount + "): " + _ObsConfig.name());
		    String obsinfo = "ObsMode = " + _ObsConfig.obsMode()
			+ "   :   ReadoutMode = " + _ObsConfig.readoutMode()
			+ "   :    NodPattern = " + _ObsConfig.nodPattern();
		    String msg = "STARTED obs: # " + _obsCount + ": " + obsinfo;
		    observStatus.setNewState( msg );
		    dcState.setNewState( obsinfo );
		}
	    }
	else if( ufp instanceof UFStrings )
	    {
		UFStrings ufs = (UFStrings)ufp;
		UFTextField statusToUpdate = (UFTextField)statusDisplay.get( ufs.name() );

		if( statusToUpdate == null )
		    {
			String name = ufs.name();

			if( name.indexOf("getStatus") >= 0 ) //check if new DC params available:
			    {
				String what = ufs.valData(0);
				if( what.indexOf("Param") > 0 ) {
				    if( MasterPanel != null )
					MasterPanel.getNewParams( clientName );
				    else if( DetectorPanel != null )
					DetectorPanel.getNewParams( clientName );
				    return;
				}
			    }
			else if( BiasPanel != null && name.indexOf("dcBias") >= 0 )
			    {
				BiasPanel.setNewStatus( ufs ); //status is for Bias Panel display
				return;
			    }
			else if( name.indexOf("HEADER") >= 0 )
			    {
				_FITSheader = ufs;
				if( name.indexOf("/") > 0 )
				    _FITSfile = name.substring( name.indexOf("/") );
				else
				    _FITSfile = "none";
				archiveFile.setNewState( _FITSfile );
				return;
			    }
			else System.out.println( className+"::recvStatus> unknown UFStrings:\n"
						 +" name = "+ufs.name()+", value = "+ufs.valData(0) );
		    }
		else statusToUpdate.setNewState( ufs.valData(0) );
	    }
	else {
	    String errmsg = className + "::recvStatus> unexpected object:	"
		+ ufp.description() + ": Length=" + ufp.length() + "\n name = " + ufp.name() + ".";
	    System.err.println( errmsg );
	    if( ufp.name().length() == 0 || ufp.length() <= 0 ) {
		if( ++nullCnt > 2 ) {
		    nullCnt = 0;
		    reConnect();
		}
		else try { Thread.sleep( 700 ); } catch( Exception x ) {}
	    }
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Inner class to monitor agent/server status messages in seperate thread
     */
    private class StatusMonitor extends Thread
    {
	private boolean keepRunning = false;
	private boolean didStart = false;
	private int sleepAmount = 1000;

	// Public constructor:

	public StatusMonitor( int sleepAmount ) {
	    this.sleepAmount = sleepAmount;
	    //if create and connect status socket to agent/server succeeds then start thread:
	    if( connectStatusSocket() ) {
		keepRunning = true;
		this.start();
		didStart = true;
	    }
	}

	// method to be run in seperate thread to recv status messages:

	public synchronized void run()
	{
	    while( true ) {
		while( keepRunning ) {
		    try {
			recvStatus();
		    }
		    catch (Exception e) {
			e.printStackTrace();
			System.err.println(className+"::StatusMonitor.run> "+e.toString());
			try { this.sleep( sleepAmount ); } catch( Exception x ) {}
		    }
		}
		try { this.sleep( sleepAmount ); } catch( Exception x ) {}
	    }
	}

	public void stopRunning() { keepRunning = false; }
	public boolean isRunning() { return keepRunning;}
 
	public void resumeRunning() {
	    keepRunning = true;
	    if( !didStart ) {
		this.start();
		didStart = true;
	    }		
	}
   } // end of private class StatusMonitor.

//-------------------------------------------------------------------------------
//***** special public accessors for desirable objects: *****
//-------------------------------------------------------------------------------

    public JProgressBar obsProgressBar() { return this.obsProgressBar;  }
    public UFTextField acqServerStatus() { return this.acqServerStatus; }
    public UFTextField observStatus()    { return this.observStatus;    }
    public UFTextField archiveFile()     { return this.archiveFile;     }
    public UFTextField dcState()         { return this.dcState;         }
    public UFTextField detType()         { return this.detType;         }
} // end of UFobsMonitor class.

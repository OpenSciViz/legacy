//Title:        UFTextMinMax for Java Control Interface (JCI)
//Version:      (see rcsID)
//Copyright:    Copyright (c) 2003
//Author:       Frank Varosi
//Company:      University of Florida
//Description:  for monitoring of CanariCam infrared camera system.

package javaUFLib;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.JTextField;

//===============================================================================
/**
 * Creates 3 text fields with Label all in one Panel: text fields for current, min, and max values.
 * With methods for set/getting values for convenience, and yellow color indicates current hit min/max.
 */

public class UFTextMinMax extends JPanel
{
    public static final
	String rcsID = "$Name:  $ $Id: UFTextMinMax.java,v 1.6 2005/03/31 23:33:17 varosi Exp $";

    double currVal = 0;
    double minVal = 0;
    double maxVal = 0;

    String currText = "";
    String minText = "";
    String maxText = "";

    String Name, Units;
    JLabel paramName;
    boolean userEvent = false;

    public JTextField currField = new JTextField();
    public JTextField minField = new JTextField();
    public JTextField maxField = new JTextField();

//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param description String: Information regarding the field
     */
    public UFTextMinMax(String description) {
	try  {
	    jbInit( description );
	}
	catch(Exception ex) {
	    System.out.println("Error creating UFTextMinMax: " + description + " : " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     *Component initialization
     *@param description String: Information regarding the field
     */
    private void jbInit( String description ) throws Exception
    {
	if( description.indexOf("(") > 0 ) {
	    int pos = description.indexOf("(");
	    this.Name = description.substring( 0, pos ).trim();
	    this.Units = description.substring( pos+1, description.indexOf(")") ).trim();
	}
	else if( description.indexOf(":") > 0 )
	    this.Name = description.substring( 0, description.indexOf(":") ).trim();
	else
	    this.Name = description.trim();
	
	paramName = new JLabel( description );

	this.setToolTipText();

	currField.setBackground(Color.white);
	currField.setEditable(false);
	minField.setBackground(Color.white);
	minField.setEditable(false);
	maxField.setBackground(Color.white);
	maxField.setEditable(false);
	setLayout(new RatioLayout());

	this.add("0.01,0.01;0.30,0.98", paramName);
	this.add("0.31,0.01;0.22,0.98", currField );
	this.add("0.54,0.01;0.22,0.98", minField );
	this.add("0.77,0.01;0.22,0.98", maxField );
    }
//-------------------------------------------------------------------------------
    /**
     * Sets the text for the tool tip -- returns no arguments
     */
    void setToolTipText() {
	this.setToolTipText( Name + ": Min=" + minText + ", Curr=" + currText + ", Max=" + maxText );
    }
//-------------------------------------------------------------------------------

    public void setMax( String newVal )
    {
	maxText = newVal;
	setToolTipText();
	maxField.setBackground( Color.white );
	maxField.setText( maxText );
    }
//-------------------------------------------------------------------------------

    public void setMin( String newVal )
    {
	minText = newVal;
	setToolTipText();
	minField.setBackground( Color.white );
	minField.setText( minText );
    }

//-------------------------------------------------------------------------------

    public void setMin( int newVal ) {
	minVal = newVal;
	setMin( Integer.toString(newVal) );
    }
    public void setMin( float newVal ) {
	minVal = newVal;
	setMin( UFLabel.truncFormat(newVal) );
    }
    public void setMin( double newVal ) {
	minVal = newVal;
	setMin( UFLabel.truncFormat(newVal) );
    }

    public void setMax( int newVal ) {
	maxVal = newVal;
	setMax( Integer.toString(newVal) );
    }
    public void setMax( float newVal ) {
	maxVal = newVal;
	setMax( UFLabel.truncFormat(newVal) );
    }
    public void setMax( double newVal ) {
	maxVal = newVal;
	setMax( UFLabel.truncFormat(newVal) );
    }
//-------------------------------------------------------------------------------

    public void setCurrent( String newVal )
    {
	currText = newVal;
	setToolTipText();
	currField.setText( currText );

	if( minVal < maxVal )
	    {
		if( currVal <= minVal ) {
		    currField.setBackground( Color.yellow );
		    minField.setBackground( Color.yellow );
		}
		else if( currVal >= maxVal ) {
		    currField.setBackground( Color.yellow );
		    maxField.setBackground( Color.yellow );
		}
		else currField.setBackground( Color.white );
	    }
    }
//-------------------------------------------------------------------------------

    public void setCurrent( int newVal ) {
	currVal = newVal;
	setCurrent( Integer.toString(newVal) );
    }
    public void setCurrent( float newVal ) {
	currVal = newVal;
	setCurrent( UFLabel.truncFormat(newVal) );
    }
    public void setCurrent( double newVal ) {
	currVal = newVal;
	setCurrent( UFLabel.truncFormat(newVal) );
    }
//-------------------------------------------------------------------------------

    public void setCurrMinMax( int current, int minval, int maxval )
    {
	setMin( minval );
	setMax( maxval );
	setCurrent( current );
    }

    public void setCurrMinMax( float current, float minval, float maxval )
    {
	setMin( minval );
	setMax( maxval );
	setCurrent( current );
    }

    public void setCurrMinMax( double current, double minval, double maxval )
    {
	setMin( minval );
	setMax( maxval );
	setCurrent( current );
    }
//-------------------------------------------------------------------------------

    public String name() { return Name; }
    public String nameUp() { return Name.toUpperCase(); }
    public String nameLow() { return Name.toLowerCase(); }

//-------------------------------------------------------------------------------

    public String getCurrent() { return currText.trim(); }
    public String getMax() { return maxText.trim(); }
    public String getMin() { return minText.trim(); }

} //end of class UFTextMinMax


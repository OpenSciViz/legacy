package javaUFLib;

/**
 * Title:        UFMessageLog.java: extends UFTextArea (which extends JTextArea)
 * Version:      (see rcsID)
 * Copyright:    Copyright (c) 2005
 * Authors:      Craig Warner and Frank Varosi
 * Company:      University of Florida
 * Description:  Holds a log of past messages in Vector and displays in text area.
 */

import java.util.*;
import javax.swing.*;
import java.awt.*;
import java.sql.Time;

public class UFMessageLog extends UFTextArea {
    public static final
	String rcsID = "$Name:  $ $Id: UFMessageLog.java,v 1.14 2005/03/10 23:17:48 varosi Exp $";

    int maxAllowSize = 10000; //max allowed size of Vector containing Log of UFMessage objects.
    int maxLogSize = 100;     //current max Log size.
    Vector V;
    boolean timestamp = true, showDups = false, addDups = false;
    String lastMessage = "";
    String prevMessage = "";
    int NmsgRecvd = 0;
//--------------------------------------------------------------------------------------------------
    /**
     * Constructors
     *@param  nMegaBytes  int: max number of Mega-Bytes allowed for document length.
     *@param  maxLogSize  int: max number of UFMessage objects that will be saved in Vector.
     */
    public UFMessageLog(int nMegaBytes, int maxLogSize, int rows, int cols)
    {
	super( nMegaBytes, rows, cols );
	this.setEditable(false);
	if( maxLogSize > maxAllowSize ) maxLogSize = maxAllowSize;
	this.maxLogSize = maxLogSize;
	this.V = new Vector(maxLogSize,10); //use non-zero capacity increment in case more is needed.
    }

    public UFMessageLog(int maxLogSize, int rows, int cols) {
	this(1, maxLogSize, rows, cols);
    }

    public UFMessageLog(int nMegaBytes, int maxLogSize)
    {
	super( nMegaBytes );
	this.setEditable(false);
	if( maxLogSize > maxAllowSize ) maxLogSize = maxAllowSize;
	this.maxLogSize = maxLogSize;
	this.V = new Vector(maxLogSize,10); //use non-zero capacity increment in case more is needed.
    }

    public UFMessageLog(int maxLogSize) { this(1, maxLogSize); }

    public UFMessageLog() { this(100); }
//--------------------------------------------------------------------------------------------------

    public void setTimestamps(boolean b) { this.timestamp = b; }

    public void showDuplicates(boolean b) { this.showDups = b; }

    public void addDuplicates(boolean b) { this.addDups = b; }

    public int getLogSize() { return V.size(); }

    public void setMaxLogSize(int n) {
	if( n > maxAllowSize ) n = maxAllowSize;
	this.maxLogSize = n;
    }

    private void setToolTipText() {
	this.setToolTipText("# msgs displayed="+NmsgDisp+", # recvd="+NmsgRecvd+", saved="+V.size());
    }
//--------------------------------------------------------------------------------------------------
    /*
     * UFMessageLog#addMessage
     * Adds message to vector Log, first checking for duplicates.
     * Default is to keep just the oldest and newest (current) of duplicate messages,
     * removing all the middle duplicates.
     *@param  message  String: text to add into vector storage of message Log.
     */
    public void addMessage(String message) {
      ++NmsgRecvd;
      while( V.size() >= maxLogSize ) V.removeElementAt(0);
      boolean duplicate = false;

      if( message.equals(lastMessage) && message.equals(prevMessage) ) duplicate = true;
      else {
	  prevMessage = lastMessage;
	  lastMessage = message;
      }

      if( !addDups && duplicate ) { //remove the older middle duplicate (keeping newest and oldest).
	  int size = V.size();
	  if (size > 0) V.removeElementAt(size-1);
      }

      V.add(new UFMessage(message, duplicate));
    }

    public void addMessageAt(int x, String message) {
	++NmsgRecvd;
	if (x >= 0 && x < V.size()) V.add(x, new UFMessage(message));
    }

    // Display all messages in text area.
    // If showDups==false then display just the first and last in each sequence of duplicates.
    // This is done by checking if next message is also a duplicate.

    public void showAllMessages() {
	int size = V.size();
	if( size < 1 ) return;
	String messages = "";
	UFMessage M = (UFMessage)V.firstElement();
	NmsgDisp = 0;
	for( int i=1; i < size; i++ ) {
	    UFMessage nextM = (UFMessage)V.elementAt(i);
	    if( showDups || !M.duplicate || !nextM.duplicate  ) {
		messages += M.getMessage(timestamp) ; 
		++NmsgDisp;
	    }
	    M = nextM;
	}
	//always include the last message:
	messages += M.getMessage(timestamp); 
	++NmsgDisp;
	this.setText(messages);
	this.setToolTipText();
    }

    public String[] getAllMessages() {
	int size = V.size();
	if( size < 1 ) return null;
	UFMessage[] Mall = new UFMessage[size];
	UFMessage M = (UFMessage)V.firstElement();
	int n = 0;
	for( int i=1; i < size; i++ ) {
	    UFMessage nextM = (UFMessage)V.elementAt(i);
	    if( showDups || !M.duplicate || !nextM.duplicate  ) Mall[n++] = M;
	    M = nextM;
	}
	//always include the last message:
	Mall[n++] = M;
	String[] messages = new String[n];
	for (int j = 0; j < n; j++) messages[j] = Mall[j].getMessage(timestamp); 
	return messages;
    }

    public void appendLastMessage() {
	if( V.size() < 1 ) return;
	UFMessage M = (UFMessage)V.lastElement();
	this.append( M.getMessage(timestamp) );
	this.setToolTipText();
    }

    public String getLastMessage() {
	if( V.size() < 1 ) return null;
	UFMessage M = (UFMessage)V.lastElement();
	return M.getMessage(timestamp);
    }

    public void showLastMessage() {
	if( V.size() < 1 ) return;
	UFMessage M = (UFMessage)V.lastElement();
	NmsgDisp = 1;
	this.setText( M.getMessage(timestamp) );
	this.setToolTipText();
    }

    public String[] getLastMessages(int x) {
	int size = V.size();
	if( size < 1 ) return null;
	if( x > size ) x = size;
	UFMessage[] M = new UFMessage[x];
	int n = 0;
	for (int j = size-x; j < size; j++) {
	    UFMessage temp = (UFMessage)V.elementAt(j);
	    if( showDups || !temp.duplicate || j==size-1 ) M[n++] = temp;
	}
	String[] messages = new String[n];
	for (int j = 0; j < n; j++) messages[j] = M[j].getMessage(timestamp); 
	return messages;
    }

    public void showLastMessages(int x) {
	int size = V.size();
	if( size < 1 ) return;
	if( x > size ) x = size;
	String messages = "";
	NmsgDisp = 0;
	for (int j = size-x; j < size; j++) {
	    UFMessage M = (UFMessage)V.elementAt(j);
	    if( showDups || !M.duplicate || j==size-1 ) {
		messages += M.getMessage(timestamp);
		++NmsgDisp;
	    }
	}
	this.setText(messages);
	this.setToolTipText();
    }

    public void appendLastMessages(int x) {
	int size = V.size();
	if( size < 1 ) return;
	if( x > size ) x = size;
	for (int j = size-x; j < size; j++) {
	    UFMessage M = (UFMessage)V.elementAt(j);
	    if( showDups || !M.duplicate || j==size-1 ) {
		this.append( M.getMessage(timestamp) );
	    }
	}
    }

    public String getFirstMessage() {
	if( V.size() < 1 ) return null;
	UFMessage M = (UFMessage)V.firstElement();
	return M.getMessage(timestamp);
    }

    public void showFirstMessage() {
	if( V.size() < 1 ) return;
	UFMessage M = (UFMessage)V.firstElement();
	NmsgDisp = 1;
	this.setText( M.getMessage(timestamp) );
	this.setToolTipText();
    }

    public void appendFirstMessage() {
	if( V.size() < 1 ) return;
	UFMessage M = (UFMessage)V.firstElement();
	this.append( M.getMessage(timestamp) );
	this.setToolTipText();
    }

    public String[] getFirstMessages(int x) {
	int size = V.size();
	if( size < 1 ) return null;
	if( x > size ) x = size;
	UFMessage[] M = new UFMessage[x];
	UFMessage temp;
	int n = 0;
	for (int j = 0; j < x; j++) {
	    temp = (UFMessage)V.elementAt(j);
	    if (showDups || !temp.duplicate) {
		M[n] = temp; 
		n++;
	    }
	}
	String[] messages = new String[n];
	for (int j = 0; j < n; j++) messages[j] = M[j].getMessage(timestamp); 
	return messages;
    }

    public void showFirstMessages(int x) {
	int size = V.size();
	if( size < 1 ) return;
	if( x > size ) x = size;
	String messages = "";
	NmsgDisp = 0;
	for (int j = 0; j < x; j++) {
	    UFMessage M = (UFMessage)V.elementAt(j);
	    if (showDups || !M.duplicate) {
		messages += M.getMessage(timestamp) ;
		++NmsgDisp;
	    }
	}
	this.setText(messages);
	this.setToolTipText();
    }

    public void appendFirstMessages(int x) {
	int size = V.size();
	if( size < 1 ) return;
	if( x > size ) x = size;

	for (int j = 0; j < x; j++) {
	    UFMessage M = (UFMessage)V.elementAt(j);
	    if( showDups || !M.duplicate )
		this.append( M.getMessage(timestamp) );
	}

	this.setToolTipText();
    }

   public String getMessageAt(int x) {
      UFMessage M = (UFMessage)V.elementAt(x);
      return M.getMessage(timestamp);
   }

   public void showMessageAt(int x) {
      UFMessage M = (UFMessage)V.elementAt(x);
      NmsgDisp = 1;
      this.setText( M.getMessage(timestamp) );
      this.setToolTipText();
   }

   public void appendMessageAt(int x) {
      UFMessage M = (UFMessage)V.elementAt(x);
      this.append( M.getMessage(timestamp) );
      this.setToolTipText();
   }

   public String[] getMessagesBetween(int x, int y) {
      if (x > y) {
	int temp = x;
	x = y;
	y = temp;
      }
      UFMessage[] M = new UFMessage[y-x];
      UFMessage temp;
      int n = 0;
      for (int j = x; j <= y; j++) {
	  if (j >= 0 && j < V.size()) {
	      temp=(UFMessage)V.elementAt(j);
	      if (showDups || !temp.duplicate) M[n++] = temp;
	  }
      }
      String[] messages = new String[n];
      for (int j = 0; j < n; j++) messages[j] = M[j].getMessage(timestamp); 
      return messages;
   }

   public void showMessagesBetween(int x, int y) {
      if (x > y) {
        int temp = x;
        x = y;
        y = temp;
      }
      String messages = "";
      NmsgDisp = 0;
      for (int j = x; j <= y; j++) {
        if (j >= 0 && j < V.size()) {
	   UFMessage M = (UFMessage)V.elementAt(j);
           if (showDups || !M.duplicate) {
              messages += M.getMessage(timestamp) ;
	      ++NmsgDisp;
           }
        }
      }
      this.setText(messages);
      this.setToolTipText();
   }

   public void appendMessagesBetween(int x, int y) {
      if (x > y) {
        int temp = x;
        x = y;
        y = temp;
      }

      for (int j = x; j <= y; j++) {
        if (j >= 0 && j < V.size()) {
	   UFMessage M = (UFMessage)V.elementAt(j);
	   if( showDups || !M.duplicate )
	       this.append( M.getMessage(timestamp) );
        }
      }

      this.setToolTipText();
   }

   public void removeLastMessages(int x) {
      int size = V.size();
      for (int j = size-x; j < size; j++) {
	if (j >=0 && j < V.size()) V.removeElementAt(j);
      }
   }

   public void removeFirstMessages(int x) {
      for (int j = 0; j < x; j++) {
	if (j >=0 && j < V.size()) V.removeElementAt(j);
      }
   }

   public void removeMessagesBetween(int x, int y) {
      if (x > y) {
        int temp = x;
        x = y;
        y = temp;
      }
      for (int j = x; j <= y; j++) {
        if (j >=0 && j < V.size()) V.removeElementAt(j);
      }
   }

   public static void main(String[] args) {
      UFMessageLog log = new UFMessageLog();
      JFrame f = new JFrame();
      JPanel p = new JPanel();
      p.setPreferredSize(new Dimension(450,300));
      p.add(log);
      f.getContentPane().add(p);
      f.pack();
      f.setVisible(true);
      log.addMessage("This is a test.");
      log.addMessage("Test 2");
      log.addMessage("Test 2");
      log.addMessage("Test 3");
      log.showMessageAt(1);
   }
}
//----------------------------------------------------------------------------------------

class UFMessage {
   public String message, timestamp;
   public boolean duplicate;

   public UFMessage(String message, String timestamp, boolean duplicate) {
      this.message = message;
      this.timestamp = timestamp;
      this.duplicate = duplicate;
   }

   public UFMessage(String message, String timestamp) {
      this(message, timestamp, false);
   }

   public UFMessage(String message) {
      this(message, new Time(System.currentTimeMillis()).toString(), false);
   }

   public UFMessage(String message, boolean duplicate) {
      this(message, new Time(System.currentTimeMillis()).toString(), duplicate);
   }

    public String getMessage(boolean withTimeStamp)
    {
	if( withTimeStamp )
	    return new String(timestamp + "\t" + message + "\n");
	else
	    return new String(message + "\n");
    }
}

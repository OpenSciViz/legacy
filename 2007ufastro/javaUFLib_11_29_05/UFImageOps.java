package javaUFLib;

//Title:        UFImageOps.java
//Version:      (see rcsID)
//Copyright:    Copyright (c) 2004-5
//Author:       Craig Warner and Frank Varosi
//Company:      University of Florida
//Description:  For analysis of image data: Centroids and Full-Width Half-Max stats.

import java.math.*;
import java.io.*;
import java.util.*;

public class UFImageOps {

   public static final
	String rcsID = "$Name:  $ $Id: UFImageOps.java,v 1.13 2005/08/11 18:21:06 drashkin Exp $";

   public static float[] getCentroid(int[][] img, int x, int y, float fwhm) {
      int[] ax = {x};
      int[] ay = {y};
      float[][] cen = getCentroid(img, ax, ay, fwhm);
      return UFArrayOps.map2DByRow(cen);
   }

   public static float[] getCentroid(double[][] img, int x, int y, float fwhm) {
      double[] ax = {(double)x};
      double[] ay = {(double)y};
      float[][] cen = getCentroid(img, ax, ay, fwhm);
      return UFArrayOps.map2DByRow(cen);
   }

   public static float[][] getCentroid(int[][] img, int[] x, int[] y, float fwhm) {
      double[][] fimg = new double[img.length][img[0].length];
      for (int j = 0; j < img.length; j++) {
	for (int l = 0; l < img[0].length; l++) {
	   fimg[j][l] = (double)img[j][l];
	} 
      }
      double[] fx = new double[x.length];
      double[] fy = new double[y.length];
      for (int j = 0; j < x.length; j++) {
	fx[j]=(double)x[j];
        fy[j]=(double)y[j];
      }
      return getCentroid(fimg, fx, fy, fwhm);
   }

   public static float[][] getCentroid(double[][] img, double[] x, double[] y, float fwhm) {
      int ysize = img.length;
      int xsize = img[0].length;
      int nhalf = (int)Math.floor(0.637*fwhm);
      if (nhalf < 2) nhalf = 2;
      int nbox = 2*nhalf+1;
      int nhalfbig = nhalf+3;
      int nbig = nbox+6;
      int npts = x.length;
      float[][] cen = new float[npts][2];
      int[] ix = new int[npts];
      int[] iy = new int[npts];
      //double[][] bigbox, strbox, deriv;
      //double[] dd, w, derivtot, ddsq;
      //double mx, sumc, sumd, sumxd, sumxsq, dx, dy;
      //      int Nmax, idx, idy, xmax, ymax, ir;

      for (int j = 0; j < npts; j++) {
	ix[j] = (int)Math.floor(x[j] + 0.5);
	iy[j] = (int)Math.floor(y[j] + 0.5);
      }
      
      for (int j = 0; j < npts; j++) {

	String pos = "" + x[j] + " " + y[j];

	if( (ix[j] < nhalfbig) || ((ix[j] + nhalfbig) > xsize-1) ||
	    (iy[j] < nhalfbig) || ((iy[j] + nhalfbig) > ysize-1) ) {
	   System.out.println("Position " + pos + " too near edge of image");
	   cen[j][0] = -1;
	   cen[j][1] = -1;
	   continue;
	}

	double[][] bigbox = UFArrayOps.extractValues( img,
						      iy[j]-nhalfbig, iy[j]+nhalfbig,
						      ix[j]-nhalfbig, ix[j]+nhalfbig);
	double max = UFArrayOps.maxValue(bigbox);
	int[][] max_pos = UFArrayOps.where(bigbox, "==", max);
	int Nmax = max_pos.length;
	int[] temp_idx = new int[Nmax];
        int[] temp_idy = new int[Nmax];

        for (int l=0; l < Nmax; l++) {
	   temp_idx[l] = max_pos[l][1];
	   temp_idy[l] = max_pos[l][0];
	}

	int idx = temp_idx[0];
	int idy = temp_idy[0];

	if (Nmax > 1) {
	   idx = Math.round(UFArrayOps.avgValue(temp_idx));
	   idy = Math.round(UFArrayOps.avgValue(temp_idy));
	}

	int xmax = ix[j] - (nhalf+3) + idx;
	int ymax = iy[j] - (nhalf+3) + idy;

	if( (xmax < nhalf) || ((xmax + nhalf) > xsize-1) ||
	    (ymax < nhalf) || ((ymax + nhalf) > ysize-1) ) {
           System.out.println("Position " + pos + " too near edge of image");
           cen[j][0] = -1;
           cen[j][1] = -1;
           continue;
	}

	double[][] strbox = UFArrayOps.extractValues( img, ymax-nhalf, ymax+nhalf, xmax-nhalf, xmax+nhalf );
	int ir = Math.max(nhalf-1, 1);
	double[] dd = new double[nbox-1];
	double[] ddsq = new double[nbox-1];
	for (int l=0; l < dd.length; l++) {
	    dd[l] = (double)(l+0.5-nhalf);
	    ddsq[l] = dd[l]*dd[l];
	}
	double[] w = new double[nbox-1];
	for (int l=0; l < w.length; l++)
	   w[l] = (double)(1.0 - 0.5 * (Math.abs(dd[l])-0.5) / (nhalf-0.5));
	double sumc = UFArrayOps.totalValue(w);

	// Y partial derivative:
	double[][] deriv = UFArrayOps.shift(strbox,-1,0);
	deriv = UFArrayOps.subArrays(deriv, strbox);
	deriv = UFArrayOps.extractValues(deriv, 0, nbox-2, nhalf-ir, nhalf+ir);
	double[] derivtot = UFArrayOps.totalValue(deriv, 1);
	double sumd = UFArrayOps.totalValue(UFArrayOps.multArrays(w, derivtot));
	double sumxd = UFArrayOps.totalValue(UFArrayOps.multArrays(UFArrayOps.multArrays(w, dd), derivtot)); 
	double sumxsq = UFArrayOps.totalValue(UFArrayOps.multArrays(w, ddsq));

	if (sumxd > 0) {
	   System.out.println("Unable to compute Y centroid around position" + pos);
	   cen[j][0] = -1;
	   cen[j][1] = -1;
	   continue;
	}

	double dy = sumxsq*sumd/(sumc*sumxd);

	if (Math.abs(dy) > nhalf) {
	   System.out.println("Computed Y centroid for position " + pos + " out of range");
	   cen[j][0] = -1;
           cen[j][1] = -1;
           continue;
	}
	
	cen[j][1] = (float)( ymax - dy );

	// X partial derivative:
        deriv = UFArrayOps.shift(strbox,0,-1);
        deriv = UFArrayOps.subArrays(deriv, strbox);
        deriv = UFArrayOps.extractValues(deriv, nhalf-ir, nhalf+ir, 0, nbox-2);
        derivtot = UFArrayOps.totalValue(deriv, 2);
        sumd = UFArrayOps.totalValue(UFArrayOps.multArrays(w, derivtot));
        sumxd = UFArrayOps.totalValue(UFArrayOps.multArrays(UFArrayOps.multArrays(w, dd), derivtot));
        sumxsq = UFArrayOps.totalValue(UFArrayOps.multArrays(w, ddsq));

        if (sumxd > 0) {
           System.out.println("Unable to compute Y centroid around position" + pos);
           cen[j][0] = -1;
           cen[j][1] = -1;
           continue;
        }

        double dx = sumxsq*sumd/(sumc*sumxd);

        if (Math.abs(dx) > nhalf) {
           System.out.println("Computed Y centroid for position " + pos + " out of range");
           cen[j][0] = -1;
           cen[j][1] = -1;
           continue;
        }

        cen[j][0] = (float)( xmax - dx );
      }

      return cen;
   }

   public static double[][] smooth( int[][] data, int width ) {
      int m = data.length;
      int n = data[0].length;
      double[][] sdata = new double[m][n];

      if( width < 2 ) {
	  for (int i = 0; i < m; i++)
	      for (int j = 0; j < m; j++) sdata[i][j] = data[i][j];
	  return sdata;
      }

      int[][] tot = new int[m][n];
      if (width % 2 == 0) width++;
      int w = width/2;
      int a = 0, b = 0, c = 0, d = 0; 
      double pts;

      for (int j = 0; j < m; j++) {
	  for (int k = 0; k < n; k++) {
	      if (k > 0)
		  tot[j][k] = tot[j][k-1] + data[j][k];
	      else
		  tot[j][k] = data[j][k];
	  }
      }

      for (int j = m-2; j >=0 ; j--) {
	  for (int k = 0; k < n; k++) tot[j][k] += tot[j+1][k];
      }

      for (int j = 0; j < m; j++) {
	  for (int k = 0; k < n; k++) {
	      if (j-w >=0 && k+w < n) a = tot[j-w][k+w];
	      if (j+w+1 < m && k+w < n) b = tot[j+w+1][k+w];
	      if (j-w >= 0 && k-(w+1) >= 0) c = tot[j-w][k-(w+1)];
	      if (j+w+1 < m && k-(w+1) >= 0) d = tot[j+w+1][k-(w+1)];
	      if (k-(w+1) < 0) {
		c = 0;
		d = 0;
	      }
	      if (j+w+1 >= m) {
		b = 0;
		d = 0;
	      }
	      if (k+w >= n || j-w < 0) {
		a = tot[Math.max(j-w,0)][Math.min(k+w,n-1)];
		if (j+w+1 < m) b = tot[j+w+1][Math.min(k+w,n-1)];
		if (k-(w+1) >= 0) c = tot[Math.max(j-w,0)][k-(w+1)];
	      }
	      pts = (Math.min(j+w+1,m) - Math.max(j-w,0)) * (Math.min(k+w,n-1) - Math.max(k-(w+1),-1));
	      sdata[j][k] = (a-b-c+d)/pts;
	  }
      }

      return sdata;
   }

   public static void smooth( double[][] data, int width ) {
      int m = data.length;
      int n = data[0].length;

      if( width < 2 ) return;
      if (width % 2 == 0) width++;
      int w = width/2;
      //replace data array with moving boxcar average values:

      double[][] tot = new double[m][n];
      double a = 0, b = 0, c = 0, d = 0; 
      double pts;

      for (int j = 0; j < m; j++) {
	  for (int k = 0; k < n; k++) {
	      if (k > 0)
		  tot[j][k] = tot[j][k-1] + data[j][k];
	      else
		  tot[j][k] = data[j][k];
	  }
      }

      for (int j = m-2; j >=0 ; j--) {
	  for (int k = 0; k < n; k++) tot[j][k] += tot[j+1][k];
      }

      for (int j = 0; j < m; j++) {
	  for (int k = 0; k < n; k++) {
	      if (j-w >=0 && k+w < n) a = tot[j-w][k+w];
	      if (j+w+1 < m && k+w < n) b = tot[j+w+1][k+w];
	      if (j-w >= 0 && k-(w+1) >= 0) c = tot[j-w][k-(w+1)];
	      if (j+w+1 < m && k-(w+1) >= 0) d = tot[j+w+1][k-(w+1)];
	      if (k-(w+1) < 0) {
		c = 0;
		d = 0;
	      }
	      if (j+w+1 >= m) {
		b = 0;
		d = 0;
	      }
	      if (k+w >= n || j-w < 0) {
		a = tot[Math.max(j-w,0)][Math.min(k+w,n-1)];
		if (j+w+1 < m) b = tot[j+w+1][Math.min(k+w,n-1)];
		if (k-(w+1) >= 0) c = tot[Math.max(j-w,0)][k-(w+1)];
	      }
	      pts = (Math.min(j+w+1,m) - Math.max(j-w,0)) * (Math.min(k+w,n-1) - Math.max(k-(w+1),-1));
	      data[j][k] = (a-b-c+d)/pts;
	  }
      }
   }

   public static double[][] smooth( int[][] data, int width, int niter ) {
      int m = data.length;
      int n = data[0].length;

      if( niter <= 0 ) return smooth( data, 0 );

      double[][] sdata = smooth( data, width );

      for( int i=1; i < niter; i++ ) smooth( sdata, width );

      return sdata;
   }

   public static float[] fwhm2D(double[][] data, int xpos, int ypos) {
      return fwhm2D(data, xpos, ypos, 0, 0, data[0].length-1, data.length-1);
   }

   public static float[] fwhm2D(double[][] data, int xpos, int ypos, int xmin, int ymin, int xmax, int ymax)
   {
      float[] fwhm = new float[6];
      float[] fwhm1ds = new float[4];

      double[] data1d = new double[ymax-ymin+1];
      for (int j = ymin; j <= ymax; j++) data1d[j-ymin] = data[j][xpos];
      fwhm1ds[1] = fwhm1D(data1d);

      data1d = new double[xmax-xmin+1];
      for (int j = xmin; j <= xmax; j++) data1d[j-xmin] = data[ypos][j];
      fwhm1ds[0] = fwhm1D(data1d);

      int start = Math.min(xpos-xmin, ypos-ymin);
      int end = Math.min(xmax-xpos, ymax-ypos);
      data1d = new double[start+end+1];
      for (int j = 0; j <= start+end; j++) {
	data1d[j] = data[ypos-start+j][xpos-start+j];
      }
      fwhm1ds[2] = fwhm1D(data1d);

      start = Math.min(xpos-xmin, ymax-ypos);
      end = Math.min(xmax-xpos, ypos-ymin);
      data1d = new double[start+end+1];
      for (int j = 0; j <= start+end; j++) {
	data1d[j] = data[ypos+start-j][xpos-start+j];
      }
      fwhm1ds[3] = fwhm1D(data1d);

      fwhm[0] = UFArrayOps.avgValue(fwhm1ds);
      fwhm[1] = UFArrayOps.stddev(fwhm1ds);
      for (int j = 0; j < 4; j++) fwhm[j+2] = fwhm1ds[j];
      return fwhm;
   }

   public static float fwhm1D(double[] data) {
      int pos = UFArrayOps.whereMaxValue( data );
      double max = data[pos];
      int hml1 = 0, hml2 = pos, hmr1 = pos, hmr2 = data.length-1;
      float x1, x2, hml, hmr, fwhm;
      for (int j = 0; j < pos && j < data.length-3 && hml1 == 0; j++) {
        if (data[j] >= max/2 && data[j+3] >= max/2) hml1 = j;
      }
      for (int j = pos; j > 3 && hml2 == pos; j--) {
        if (data[j] <= max/2 && data[j-3] <= max/2) hml2 = j;
      }
      for (int j = pos; j < data.length-3 && hmr1 == pos; j++) {
        if (data[j] <= max/2 && data[j+3] <= max/2) hmr1 = j;
      }
      for (int j = data.length-1; j > pos && j > 2 && hmr2 == data.length-1; j--) {
        if (data[j] >= max/2 && data[j-3] >= max/2) hmr2 = j;
      }
      x1 = (float)( 0.5f*max - data[hml2] );
      x2 = (float)( data[hml1] - 0.5f*max );
      if (x1+x2 == 0) hml = hml1; else hml = (x1*hml1 + x2*hml2)/(x1+x2);
      x1 = (float)( data[hmr2] - 0.5f*max );
      x2 = (float)( 0.5f*max - data[hmr1] );
      if (x1+x2 == 0) hmr = hmr1; else hmr = (x1*hmr1 + x2*hmr2)/(x1+x2);
      fwhm = hmr-hml;
      return fwhm;
   }

   public static void main(String[] args) {
      double[][] a = new double[530][530];
      String s;
      try {
        BufferedReader r = new BufferedReader(new FileReader("NGC3031.txt"));
        for (int j = 0; j < 530; j++) {
           for (int l = 0; l < 530; l++) {
              s = r.readLine();
              a[j][l] = Double.parseDouble(s);
           }
        }
      } catch(IOException e) {}

      float[] c = getCentroid(a, 39, 440, 4);
      System.out.println(UFArrayOps.arrayToString(c));
      int[][] data = {{0,1,2,3,4},{5,6,7,8,9},{10,17,12,13,14},{15,16,17,25,19},{20,21,22,23,24,25}};
      System.out.println(UFArrayOps.arrayToString(smooth(data, 3, 1)));
   }

}

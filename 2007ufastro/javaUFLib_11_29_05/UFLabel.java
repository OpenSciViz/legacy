//Title:        UFLabel.java
//Version:      2.0
//Copyright:    Copyright (c) Frank Varosi
//Author:       Frank Varosi, 2003
//Company:      University of Florida
//Description:  Extension of JLabel class.

package javaUFLib;

import java.awt.*;
import java.awt.event.*;
import java.awt.Toolkit;
import javax.swing.JLabel;

//===============================================================================
/**
 * Creates text Labels with always same prefix text and color blue.
 * Beeps and changes color to red if text "ERR" or "WARN" is displayed.
 */
public class UFLabel extends JLabel
{
    public static final
	String rcsID = "$Name:  $ $Id: UFLabel.java,v 1.12 2005/08/11 18:21:06 drashkin Exp $";

    protected String _prefix = "";
    protected boolean _doBeep = true;
    protected boolean _needReset = false;
    protected int _minDecDigits = 1;
    protected Color _darkBlue = new Color(0,0,144); //default color is dark blue (almost black).
    protected Color _darkRed = new Color(155,0,0);
    protected Color _color = _darkBlue;
//-------------------------------------------------------------------------------
    /**
     * Basic Constructor
     *@param prefix String: Text to preceed record value in the label text
     */
    public UFLabel(String prefix) {
	try  {
	    super.setText( prefix );
	    super.setForeground( _color );
	    _prefix = prefix;
	}
	catch(Exception ex) {
	    System.out.println("Error creating UFLabel with prefix "+prefix+": " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param prefix String: Text to preceed record value in the label text
     */
    public UFLabel( String prefix, Color color ) {
	try  {
	    super.setText( prefix );
	    super.setForeground( color );
	    _prefix = prefix;
	    _color = color;
	}
	catch(Exception ex) {
	    System.out.println("Error creating UFLabel with prefix "+prefix+": " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------
    /**
     * Constructor
     *@param prefix String: Text to preceed record value in the label text
     */
    public UFLabel( String prefix, int minDecDigits ) {
	try  {
	    super.setText(prefix);
	    super.setForeground( _color );
	    _prefix = prefix;
	    _minDecDigits = minDecDigits;
	    if( _minDecDigits < 0 ) _minDecDigits = 2;
	}
	catch(Exception ex) {
	    System.out.println("Error creating UFLabel with prefix "+prefix+": " + ex.toString());
	}
    }
//-------------------------------------------------------------------------------

    public void newPrefix( String prefix ) { _prefix = prefix; }
    public void newColor( Color color ) { _color = color; setForeground( _color ); }

//-------------------------------------------------------------------------------
  /**
   * Override setText method to always use _prefix.
   */
    public void setText( String text )
    {
	if( text.indexOf("ERR") >= 0 || text.indexOf("WARN") >= 0 ||
	    text.indexOf("FAIL") >= 0 || text.indexOf("Fail") >= 0 )
	    {
		setForeground( _darkRed );
		_needReset = true;
		if( _doBeep ) Toolkit.getDefaultToolkit().beep();
	    }
	else if( _needReset )
	    {
		setForeground( _color );
		_needReset = false;
	    }

	super.setText( _prefix + "  " + text );
    }
//-------------------------------------------------------------------------------

    public void setText( String text, boolean noBeep )
    {
	if( noBeep ) _doBeep = false;
	setText( text );
	_doBeep = true;
    }
//-------------------------------------------------------------------------------

    public String getSubText()
    {
	String text = super.getText();
	if( _prefix == null ) return text;
	return text.substring( _prefix.length() ).trim();
    }
//-------------------------------------------------------------------------------

    public void setText( int value ) { setText( Integer.toString( value ) ); }

    public void setText( float value ) { setText( truncFormat( value, _minDecDigits ) ); }

    public void setText( double value ) { setText( truncFormat( value, _minDecDigits ) ); }

//-------------------------------------------------------------------------------

    public static String truncFormat( double value ) { return truncFormat( value, 1 ); }

    public static String truncFormat( double value, int minDecDigits )
    {
	String valTxt = Double.toString( value ).trim();

	if( valTxt.indexOf(".") < 0 ) return valTxt;

	int ndigits = valTxt.indexOf(".") + minDecDigits + 1;
	double vabs = Math.abs( value );
	if( vabs < 100 ) ++ndigits;
	if( vabs < 10 ) ++ndigits;
	if( vabs < 1 ) ++ndigits;

	if( ndigits < valTxt.length() ) {
	    String vText = valTxt.substring( 0, ndigits );
	    if( valTxt.indexOf("E") > 0 )
		return( vText + valTxt.substring( valTxt.indexOf("E"), valTxt.length() ) );
	    else
		return vText;
	}
	else return valTxt;
    }
} //end of class UFLabel


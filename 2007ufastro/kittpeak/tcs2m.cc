const char rcsId[] = "$Id: tcs2m.cc 14 2008-06-11 01:49:45Z hon $";

#include <pthread.h>
#include <signal.h>
#include <sys/stat.h>
#include <unistd.h>

#include <new>
#include <string>
#include <iostream>
#include <strstream>

#include <cstdio>
#include <ctime>
#include <cstdlib>

#include "tcs2m.h"

using std::string ;
using std::strstream ;
using std::cin ;
using std::endl ;
using std::cout ;
using std::ends ;
using std::clog ;
using std::nothrow ;

tcs2m* tcs2m::_instance = 0 ;

int main( int argc, char** argv ) {
  return tcs2m::main( argc, argv );
}

int tcs2m::commandTcs(const string& command, string& result) {
  //clog << "tcs2m::commandTcs> Sending: " << command << endl ;

  int status = ::tcpClnCmd(const_cast< char* >(command.c_str()), 1);
  result = ::tcpClnReply();
  return status;
}

int tcs2m::main( int argc, char** argv ) {
  tcs2m* tcs = tcs2m::create( "cyan.kpno.noao.edu" ) ;
  if( NULL == tcs ) {
    clog << "Unable to connect to cyan.kpno.noao.edu\n" ;
    return -1;
  }

  string result;
  string command;
  int status = 0 ;

  if( argc > 1 ) {
    //clog <<argv[0]<<" "<<argv[1]<<endl;
    if( argv[1][strlen(argv[1])-1] == '"' )
      argv[1][strlen(argv[1])-1] = '\0';

    if( argv[1][0] == '"' )
      command = &argv[1][1];
    else
      command = &argv[1][0];

    status = tcs->commandTcs( command, result );
    //printf( "%s (%d) -> %s\n", command.c_str(), status, result.c_str());
    cout<<result<<endl;
    delete tcs ;
    return 0;
  }

  while ( true ) {
    cout << "tcs> " ;
    string line ;
    getline( cin, line ) ;
    if( (line == "exit") || (line == "quit") ) {
      cout << "Exiting...\n" ;
      break ;
    }
    command = line;
    status = tcs->commandTcs(command, result);
    printf("%s (%d) -> %s\n",command.c_str(), status, result.c_str());
  } // while( true )
	
  delete tcs;
  return 0;
}

tcs2m::~tcs2m() {
  if( _instance != NULL ) {
    ::tcpClnClose();
    tcs2m* tmp = _instance;
    _instance = 0;
    delete tmp;
  }
}

tcs2m* tcs2m::create( const string& hostName ) {
  if( _instance != 0 ) {
    return _instance ;
  }

  if( ERROR == ::tcpClnOpen( const_cast< char* >( hostName.c_str() ) ) ) {
    clog << "tcs2m::create> Failed to connect to: "
	 << hostName << endl ;
    return 0 ;
  }

  _instance = new (nothrow) tcs2m( hostName ) ;
  return _instance ;
}

string tcs2m::getFocus() {
  string command("tele focus");
  string result;

  int status = commandTcs(command, result) ;
  if( ERROR == status ) {
    return string("ERROR") ;
  }

  return result ;
}

string tcs2m::getInformation() {
  string command("tele info");
  string result;

  int status = commandTcs(command, result) ;
  if( ERROR == status ) {
    return string() ;
  }

  return result ;
}

string tcs2m::getOffset() {
  string command("tele offset");
  string result( __ResultSize__, 0 ) ;

  int status = commandTcs( command, result ) ;
  if( ERROR == status ) {
    return string("ERROR") ;
  }

  return result ;
}

string tcs2m::setFocus( int relative ) {
  strstream s ;
  s	<< "tele focus " ;
  s	<< ((relative >= 0) ? '+' : '-') << '=' ;
  s	<< ' ' << ::abs( relative ) << ends ;

  string command = s.str() ;
  delete[] s.str() ;

  string result;

  int status = commandTcs(command, result) ;
  if( ERROR == status )	{
    return string("ERROR") ;
  }

  return result ;
}

string tcs2m::setFocus( unsigned int absolute ) {
  strstream s ;
  s	<< "tele focus = " << absolute << ends ;

  string command = s.str() ;
  delete[] s.str() ;

  string result;

  int status = commandTcs(command, result) ;
  if( ERROR == status ) {
    return string("ERROR") ;
  }

  return result ;
}

string tcs2m::zeroOffset() {
  strstream s ;
  s	<< "tele offset = 0" << ends ;

  string command = s.str() ;
  delete[] s.str() ;

  string result;

  int status = commandTcs(command, result) ;
  if( ERROR == status ) {
    return string("ERROR") ;
  }

  return result ;
}

// relative
string tcs2m::setOffsetRel( unsigned int east, unsigned int north,
			    char polarity ) {
  strstream s ;
  s	<< "tele offset " ;
  s	<< polarity << '=' ;
  s	<< east << ", " ;
  s	<< north << ends ;

  string command = s.str() ;
  delete[] s.str() ;

  string result;

  int status = commandTcs(command, result) ;
  if( ERROR == status ) {
    return string("ERROR") ;
  }

  return result ;
}

// absolute
string tcs2m::setOffsetAbs( unsigned int east, unsigned int north ) {
  strstream s ;
  s	<< "tele offset = " << east << ", " << north << ends ;

  string command = s.str() ;
  delete[] s.str() ;

  string result;

  int status = commandTcs(command, result) ;
  if( ERROR == status ) {
    return string("ERROR") ;
  }

  return result ;
}

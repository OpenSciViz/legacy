#ifndef __TCS4M_H__
#define __TCS4M_H__

#include	<string>

using std::string ;

extern "C" {
  #include "tcpclnLib.h"
}

static const size_t __ResultSize__ = 4096 ;

class tcs4m {

public:
	virtual ~tcs4m() ;

	static int main( int, char** ) ;
	static tcs4m* create( const string& ) ;
	static int commandTcs( const string& command, string& result );

	string getFocus() ; // microns
	string getOffset() ; // arc-sec. east, west
	string getInformation() ; // all

	string setFocus( int ) ; // relative +/- (microns)
	string setFocus( unsigned int ) ; // absolute (microns)

	string zeroOffset() ;
	string setOffsetRel( unsigned int east, unsigned int north,
		char polarity = '+' ) ;
	string setOffsetAbs( unsigned int, unsigned int ) ;

protected:
	tcs4m() {}
	tcs4m( const tcs4m& ) {}
	tcs4m( const string& host )
		: _host( host ) {}


	string	_host ;
	static tcs4m* _instance ;

} ;

#endif // __TCS4M_H__

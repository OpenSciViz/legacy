[schematic2]
uniq 51
[tools]
[detail]
s 2624 2064 100 1792 2001/01/25
s 2480 2064 100 1792 NWR
s 2240 2064 100 1792 Initial Layout
s 2016 2064 100 1792 A
s 2528 -240 100 1792 flamSadEcConfigure.sch
s 2096 -272 100 1792 Author: NWR
s 2096 -240 100 1792 2001/01/25
s 2320 -240 100 1792 Rev: A
s 2432 -192 100 256 Flamingos EC Configure
s 2096 -176 200 1792 FLAMINGOS
s 2240 -128 100 0 FLAMINGOS
[cell use]
use esirs -192 1255 100 0 esirs#39
xform 0 16 1408
p -256 960 100 0 0 FTVL:FLOAT
p -128 1216 100 0 1 SCAN:Passive
p -32 1248 100 1024 -1 name:$(top)mosHeaterSetpoint
use esirs -192 1671 100 0 esirs#40
xform 0 16 1824
p -128 1632 100 0 1 SCAN:Passive
p -32 1664 100 1024 -1 name:$(top)mosHeaterPower
use esirs 384 1255 100 0 esirs#47
xform 0 592 1408
p 320 960 100 0 0 FTVL:FLOAT
p 448 1216 100 0 1 SCAN:Passive
p 544 1248 100 1024 -1 name:$(top)camHeaterSetpoint1
use esirs 384 1671 100 0 esirs#48
xform 0 592 1824
p 448 1632 100 0 1 SCAN:Passive
p 544 1664 100 1024 -1 name:$(top)camHeaterPower1
use esirs 960 1671 100 0 esirs#49
xform 0 1168 1824
p 1024 1632 100 0 1 SCAN:Passive
p 1120 1664 100 1024 -1 name:$(top)camHeaterPower2
use esirs 960 1255 100 0 esirs#50
xform 0 1168 1408
p 896 960 100 0 0 FTVL:FLOAT
p 1024 1216 100 0 1 SCAN:Passive
p 1120 1248 100 1024 -1 name:$(top)camHeaterSetpoint2
use changeBar 1984 2023 100 0 changeBar#46
xform 0 2336 2064
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamSadEcConfig.sch,v 0.0 2004/10/04 18:05:31 hon Developmental $

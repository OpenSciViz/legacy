#include "stdio.h"
#include "sys/types.h"
#include "sys/stat.h"
#include "iostream.h"

int main(int argc, char** argv, char** envp) {
  char c = 0;
  int nr= 0;
  char* file= "flamDB.txt";
  clog<<"argc: "<<argc<<endl;
  if( argc > 1 ) file = argv[1];
  clog<<"file: "<<file<<endl;
  FILE* f = fopen(file, "r");
  if( f == 0 ) { clog<<"failed to open file, abort.."<<endl; return 1; }
  while( c != EOF ) {
    c = getc(f);
    if( c != '\r' ) putc(c, stdout);
  }
}

#!/bin/tcsh -f
# gemini cad/car enumerations:
set Mark = 0
set Clear = 1
set Preset = 2
set Start = 3
set Idle = 0
set Busy = 2
set Error = 3
#
#set db = flam:eng
set db = flam
if( "$1" != "" ) set db = "$1"
set apply = ${db}:apply
#set apply = ${db}:eng:apply
#ufcaput ${apply}C.ival=0
#ufcaput ${db}:cc:initC.ival=0
ufcaput ${db}:cc:initC.dir=$Clear
#ufcaput ${db}:cc:datumC.ival=0
ufcaput ${db}:cc:datumC.dir=$Clear
#ufcaput ${db}:cc:setupC.ival=0
ufcaput ${db}:cc:setupC.dir=$Clear
ufcaput ${apply}.dir=$Clear
#ufcaput ${apply}.dir=$Start

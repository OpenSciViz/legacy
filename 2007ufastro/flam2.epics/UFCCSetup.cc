#if !defined(__UFCCSETUP_CC__)
#define __UFCCSETUP_CC__ "$Name:  $ $Id: UFCCSetup.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFCCSETUP_CC__;

#include "UFCCSetup.h"
#include "UFCAServ.h"
#include "UFApply.h"
#include "UFSIR.h"
#include "UFPVAttr.h"
#include "UFPosixRuntime.h"
#include "UFStrings.h"

#include "algorithm"

// from portescap motor indexor logic:
#include "ufflam2mech.h"
#include "ufF2mechMotion.h" // provide  F2 compound motion logic

// global (static) attributes:
string UFCCSetup::_host; // agent location
int UFCCSetup::_miport;
int UFCCSetup::_bcport;
UFClientSocket* UFCCSetup::_motoragntsoc= 0;
UFClientSocket* UFCCSetup::_barcdagntsoc= 0;
deque< string > UFCCSetup::_mechNames; // mech. cnt and names
// 
std::map< string, vector< UFCCSetup::MechPos* >*  > UFCCSetup::_mechPos; // mech. named-positions
//
// key == full indexor mechanism name, val == A-z portescap partyline name:
std::map< string, string > UFCCSetup::_indexorNames; 
// key == A-z portescap partyline, val == full indexor mechanism name:
std::map< string, string > UFCCSetup::_mechNameOfIndexor; 
//
std::map< string, UFPVAttr* > UFCCSetup::_indexorSAD; 
// to evaluate +- offsets for the backlash Not/Near Home command,
// need to kep track of current step counts for each indexor;
// this should be double-checked against the values in the SAD:
std::map< string, int > UFCCSetup::_indexorSteps;

// save initial and slew vel. settings for reuse during step commands
int UFCCSetup::_ivel= 100; // defaults?
int UFCCSetup::_vel= 100;

// ctors:

// static class funcs:
void UFCCSetup::cmdOrigin(const string& option, std::map< string, UFCCSetup::MotParm* >* mparms,
			  UFGem* rec, UFCAR* car) {
  // note that the option string may or may not include "origin", but should indicate either
  // an individual mechanism name or indexor name or "all/All/ALL":
  std::map< string, UFCCSetup::MotParm* >::const_iterator mit;
  deque< string > cv;  // CAR, SAD, Raw N+-, Raw +-
  string cmd, cmdpar, carname = UFCAServ::_instrum + ":cc:setupC";
  if( rec != 0 )
    if( rec->hasCAR() ) carname = rec->hasCAR()->name();
  if( car != 0 )
    carname = car->name();
  carname += ".IVAL";
  // origin all indexors?
  char* cs = (char*)option.c_str();
  while( *cs == ' ' || *cs == '\t' ) ++cs;
  string mechname = cs;
  size_t pos = mechname.find("in"); // remove leading "Origin"
  if( pos != string::npos ) mechname = mechname.substr(2+pos);
  clog<<"UFCCSetup::cmdOrigin> options: "<<option<<", mechname: "<<mechname<<endl;
  if( mechname.find("all") != string::npos || mechname.find("All") != string::npos ||
      mechname.find("ALL") != string::npos ) {
    for( mit = mparms->begin(); mit != mparms->end(); ++mit ) {
      cv.clear();
      string mname = mit->first;
      UFCCSetup::MotParm* mp = mit->second; // _posit should be either posname or +/- steps
      if( mp == 0 )
        continue;
      cmd = mp->_indexor; cmd += "CAR";
      cmdpar = carname;
      cv.push_back(cmd); cv.push_back(cmdpar); //2
      //if( _verbose )
        clog<<"UFCCSetup::cmdOrigin> cmd: "<<cmd<<", cmdpar: "<<cmdpar<<endl;

      cmd = mp->_indexor; cmd += "SAD";
      cmdpar = sadChan(mname); cmdpar += ".INP";
      cv.push_back(cmd); cv.push_back(cmdpar); //4
      //if( _verbose )
        clog<<"UFCCSetup::cmdOrigin> cmd: "<<cmd<<", cmdpar: "<<cmdpar<<endl;

      cmd = mp->_indexor; cmd += "Raw Origin";
      cmdpar = mp->_indexor; cmdpar += "O";
      cv.push_back(cmd); cv.push_back(cmdpar); //6
      //if( _verbose )
        clog<<"UFCCSetup::cmdOrigin> cmd: "<<cmd<<", cmdpar: "<<cmdpar<<endl;
      UFStrings req("CCOrigin", cv);
      int ns = UFCCSetup::_motoragntsoc->send(req);
      if( ns <= 0 )
        clog<<"UFCCSetup::cmdOrigin> failed send req: "<< req.name() << " " << req.timeStamp() <<endl;
    }
    return;
  }
  // origin just the single mechanism indexor indicated
  cv.clear();
  map<string, string>::const_iterator it = UFCCSetup::_indexorNames.find(mechname);
  if( it == UFCCSetup::_indexorNames.end() ) {
    clog<<"UFCCSetup::cmdOrigin> unknown indexor for mechname: "<<mechname<<endl;
    return;
  }
  string indexor = UFCCSetup::_indexorNames[mechname];
  cmd = indexor; cmd += "CAR";
  cmdpar = carname;
  cv.push_back(cmd); cv.push_back(cmdpar); //2
  //if( _verbose )
    clog<<"UFCCSetup::cmdOrigin> cmd: "<<cmd<<", cmdpar: "<<cmdpar<<endl;

  cmd = indexor; cmd += "SAD";
  cmdpar = sadChan(mechname); cmdpar += ".INP";
  cv.push_back(cmd); cv.push_back(cmdpar); //4
  //if( _verbose )
    clog<<"UFCCSetup::cmdOrigin> cmd: "<<cmd<<", cmdpar: "<<cmdpar<<endl;

  cmd = indexor; cmd += "Raw Origin";
  cmdpar = indexor; cmdpar += "O";
  cv.push_back(cmd); cv.push_back(cmdpar); //6
  UFStrings req("CCOrigin", cv);
  int ns = UFCCSetup::_motoragntsoc->send(req);
  if( ns <= 0 )
    clog<<"UFCCSetup::cmdOrigin> failed send req: "<< req.name() << " " << req.timeStamp() <<endl;
  return;
} // cmdOrigin

void UFCCSetup::cmdStatus(const string& option, std::map< string, UFCCSetup::MotParm* >* mparms,
			  UFGem* rec, UFCAR* car) {
  // note that the option string may or may not include "origin", but should indicate either
  // an individual mechanism name or indexor name or "all/All/ALL":
  std::map< string, UFCCSetup::MotParm* >::const_iterator mit;
  deque< string > cv;  // CAR, SAD, Raw N+-, Raw +-
  string cmd, cmdpar, carname = UFCAServ::_instrum + ":cc:setupC";
  if( rec != 0 )
    if( rec->hasCAR() ) carname = rec->hasCAR()->name();
  if( car != 0 )
    carname = car->name();
  carname += ".IVAL";
  // status all indexors?
  char* cs = (char*)option.c_str();
  while( *cs == ' ' || *cs == '\t' ) ++cs;
  string mechname = cs;  
  size_t pos = mechname.find("us"); // remove leading "Status"
  if( pos != string::npos ) mechname = mechname.substr(2+pos);
  clog<<"UFCCSetup::cmdStatus> options: "<<option<<", mechname: "<<mechname<<endl;
  if( mechname.find("all") != string::npos || mechname.find("All") != string::npos ||
      mechname.find("ALL") != string::npos ) {
    for( mit = mparms->begin(); mit != mparms->end(); ++mit ) {
      cv.clear();
      string mname = mit->first;
      UFCCSetup::MotParm* mp = mit->second; // _posit should be either posname or +/- steps
      if( mp == 0 )
        continue;
      cmd = mp->_indexor; cmd += "CAR";
      cmdpar = carname;
      cv.push_back(cmd); cv.push_back(cmdpar); //2
      //if( _verbose )
        clog<<"UFCCSetup::cmdStatus> cmd: "<<cmd<<", cmdpar: "<<cmdpar<<endl;

      cmd = mp->_indexor; cmd += "SAD";
      cmdpar = sadChan(mname); cmdpar += ".INP";
      cv.push_back(cmd); cv.push_back(cmdpar); //4
      //if( _verbose )
        clog<<"UFCCSetup::cmdOrigin> cmd: "<<cmd<<", cmdpar: "<<cmdpar<<endl;

      cmd = mp->_indexor; cmd += "Raw Status";
      cmdpar = mp->_indexor; cmdpar += "Z";
      cv.push_back(cmd); cv.push_back(cmdpar); //6
      //if( _verbose )
        clog<<"UFCCSetup::cmdstatus> cmd: "<<cmd<<", cmdpar: "<<cmdpar<<endl;

      cmd = mp->_indexor; cmd += "Raw Status X";
      cmdpar = mp->_indexor; cmdpar += "X";
      cv.push_back(cmd); cv.push_back(cmdpar); //6
      //if( _verbose )
        clog<<"UFCCSetup::cmdstatus> cmd: "<<cmd<<", cmdpar: "<<cmdpar<<endl;
      UFStrings req("CCStatus", cv);
      int ns = UFCCSetup::_motoragntsoc->send(req);
      if( ns <= 0 )
        clog<<"UFCCSetup::cmdStatus> failed send req: "<< req.name() << " " << req.timeStamp() <<endl;
    }
    return;
  }
  // status just the individual indexor(s) indicated?
  cv.clear();
  map<string, string>::const_iterator it = UFCCSetup::_indexorNames.find(mechname);
  if( it == UFCCSetup::_indexorNames.end() ) {
    clog<<"UFCCSetup::cmdStatus> unknown indexor for mechname: "<<mechname<<endl;
    return;
  }
  string indexor = UFCCSetup::_indexorNames[mechname];
  cmd = indexor; cmd += "CAR";
  cmdpar = carname;
  cv.push_back(cmd); cv.push_back(cmdpar); //2
  //if( _verbose )
    clog<<"UFCCSetup::cmdStatus> cmd: "<<cmd<<", cmdpar: "<<cmdpar<<endl;

  cmd = indexor; cmd += "SAD";
  cmdpar = sadChan(mechname); cmdpar += ".INP";
  cv.push_back(cmd); cv.push_back(cmdpar); //4
  //if( _verbose )
    clog<<"UFCCSetup::cmdOrigin> cmd: "<<cmd<<", cmdpar: "<<cmdpar<<endl;

  cmd = indexor; cmd += "Raw Status";
  cmdpar = indexor; cmdpar += "Z";
  cv.push_back(cmd); cv.push_back(cmdpar); //6
  UFStrings req("CCStatus", cv);
  int ns = UFCCSetup::_motoragntsoc->send(req);
  if( ns <= 0 )
    clog<<"UFCCSetup::cmdStatus> failed send req: "<< req.name() << " " << req.timeStamp() <<endl;
  return;
} // cmdStatus

string UFCCSetup::sadChan(const string& mechname) {
  // returns the channel record name (wo .field)
  // deduce the instrument name prefix and formulate 'insrtum:sad:' string:
  string sad = UFCAServ::_instrum + ":sad:";
  if( mechname.find("BarC") == string::npos )
    sad += mechname;
  else
    sad += "MOS";

  sad += "Steps";

  return sad;
}

double UFCCSetup::fetchCurrentSteps(const string& mechname) {
  double steps= 0.0;
  string sadname = UFCCSetup::sadChan(mechname);
  UFSIR* sad = UFSIR::_theSAD[sadname];
  if( sad == 0 )
    return steps;
  string pvname = sad->name() + ".VAL";
  UFPVAttr *pva= (*sad)[pvname];
  if( pva == 0 )
    return steps;

  steps = pva->getNumeric();
  return steps;
}

// helper for comotion & genExec
string UFCCSetup::_mosPosNames(int pos, int idx, string& posname, const string& instrum) {
  std::map< string, int > _mossteps;
  std::map< string, string > _mosbarcdsad;
  // use names from ufflam2mech.h:
  string sad = "";
  UFFLAM2MechPos* mech= 0;
  char* indexor = infoFLAM2MechOf(idx, &mech); // infoFLAM2MechOf(UFF2MOSBarCdIdx, &mech);
  if( indexor == 0 || mech == 0) return sad;
  int pcnt = mech->posCnt;
   for( int i = 0; i < pcnt; ++i ) {
    _mossteps[mech->positions[i]] = (int)mech->steps[i];
    if( idx == UFF2MOSBarCdIdx ) // also set sadname for mos barcode reading
      _mosbarcdsad[mech->positions[i]] = instrum + ":sad:" + mech->positions[i];
  }
  std::map< string, int >::iterator it = _mossteps.begin();
  int mindiff = INT_MAX;
  for( it = _mossteps.begin(); it != _mossteps.end(); ++it ) {
    posname = it->first;
    int steps = it->second;
    int diff = abs(steps - pos);
    if( diff < mindiff ) {
      mindiff = diff;
      if( idx == UFF2MOSBarCdIdx ) // also set sadname for mos barcode reading
        sad = _mosbarcdsad[posname];
    }
  }
  clog<<"UFCCSetup::_mosPosNames> mossteps: "<<pos<<", nearest named pos.: "<<posname<<", sad: "<<sad<<endl;
   
  return sad; // return sad
}

int UFCCSetup::backlashSeq(const UFCCSetup::MotParm& mp, deque<string>& cmds) {
  // if backlash input is non-zero assume backlash sequence
  // else assume atomic (raw) step motion only:
  int steps = mp._steps;
  bool relative= true;
  string mechname = _mechNameOfIndexor[mp._indexor];
  //vector< UFCCSetup::MechPos* >* posvec = UFCCSetup::getMechPos(const string& mechname);
  int currentpos = (int) fetchCurrentSteps(mechname); // how?
  double desiredpos = INT_MIN; // figure 
  if( mp._pos != INT_MIN ) {
    desiredpos = mp._pos;
  }
  else { // this may be a problem if abs(currentpos) > 360 deg.?
    desiredpos = currentpos + steps;
  }
    
  if( steps == 0 ) {
    clog<<"UFCCSetup::backlashSeq> mechname: "<<mechname<<", steps: "<<steps
	<<",  posname: "<<mp._posit<<", possteps: "<<mp._pos<<endl;
    if( mp._posit.empty() || mp._posit == "null" ) relative = false;
    if( mp._pos != INT_MIN ) {
      steps = mp._pos - currentpos; // set step count
    }
  }
  else 
  clog<<"UFCCSetup::backlashSeq> mechname: "<<mechname<<", steps: "<<steps
      <<",  posname: "<<mp._posit<<", possteps: "<<mp._pos<<endl;
  //if( mp._steps != 0 && (mp._posit == "" || mp._posit == "null") ) {
  if( mp._blash == 0 || mp._blash == INT_MIN ) { // atomic raw step motion
    if( steps == 0 ) {
      //if( _verbose )
        clog<<"UFCCSetup::backlashSeq> neither atomic motion nor backlash motion indicated for named-position, indexor: "
	    <<mp._indexor<<", posname: "<<mp._posit<<", current pos: "<<currentpos<<", steps from current: "<<steps<<endl;
	return (int)cmds.size();
    }      
    if( !relative ) {
      //if( _verbose )
        clog<<"UFCCSetup::backlashSeq> atomic motion for named-position, indexor: "<<mp._indexor<<", posname: "
            <<mp._posit<<", current pos: "<<currentpos<<", steps from current: "<<steps<<endl;
    }
    else if( _verbose )
      //if( _verbose )
        clog<<"UFCCSetup::backlashSeq> atomic motion for (relative) steps, indexor: "<<mp._indexor<<", posname: "
            <<mp._posit<<", current pos: "<<currentpos
	    <<", steps from current: "<<steps<<endl;
    string posname = mp._posit;
    if( posname.empty() ) posname = "null";
    string cmd = "Raw "; cmd += " !!Err. step indexor: "; cmd += mp._indexor ;cmd += " to ";  cmd += posname;
    strstream s;
    s << mp._indexor;
    if( steps > 0 )
      s << " +" << abs(steps) << ends;
    else 
      s << " -" << abs(steps) << ends;

    string cmdpar = s.str(); delete s.str();
    cmds.push_back(cmd); cmds.push_back(cmdpar); //6
    _indexorSteps[mp._indexor] += steps;
    //if( _verbose ) {
      clog<<"UFCCSetup::backlashSeq> cmd: "<<cmd<<", cmdpar: "<<cmdpar<<endl;
      clog<<"UFCCSetup::backlashSeq> indexor: "<<mp._indexor<<", final step cnt should be: "
	  <<_indexorSteps[mp._indexor]<<endl;
    //}
    return (int)cmds.size();
  }

  //if( _verbose ) 
    clog<<"UFCCSetup::backlashSeq> compound motion for indexor: "<<mp._indexor<<", posname: "
        <<mp._posit<<", current position: "<<currentpos<<", desired: "<<desiredpos<<endl;

  if( fabs(desiredpos - currentpos) <= 0.01 )
    clog<<"UFCCDatum::_homeF2> desired and current position steps are identical?"<<endl;

  // use the new  ufF2mechMotion functions 'trusted cmd seq.' syntax:
  char cmdname[BUFSIZ], cmdimpl[BUFSIZ];
  ::ufF2mechMotionP((char*)mechname.c_str(), currentpos, desiredpos,
			     mp._blash, mp._vel, mp._slow, (char*)cmdimpl);
  ::cmdTransaction((char*)cmdimpl, (char*)cmdname);
  cmds.push_back(cmdname); cmds.push_back(cmdimpl);

  /* the following has been deprecated by the new ufF2mechMotion functions:
  // use mp._steps in N cmd to position indexor at intermediate 'backlash' position
  // then step remaining delta to desireed named position:
  string cmd = "Raw "; cmd += " !!Err. step indexor: "; cmd += mp._indexor ;cmd += " to "; cmd += mp._posit;
  string cmdpar;
  int delta = mp._blash;
  if( delta == INT_MIN ) {
    clog<<"UFCCSetup::backlashSeq> backlash supplied is not allowed: "<<delta<<", just perform raw step..."<<endl;
    strstream sD;
    sD << mp._indexor;
    if( steps > 0 )
      sD << " +" << abs(steps) << ends;
    else 
      sD << " -" << abs(steps) << ends;

    string cmdpar = sD.str(); delete sD.str();
    cmds.push_back(cmd); cmds.push_back(cmdpar); //8 => +- step delta to named position
  
    return (int)cmds.size();
  }

  steps += delta;
  strstream sN;
  sN << mp._indexor;
  if( mp._steps > 0 )
    sN << "N +" << abs(steps) << ends;
  else 
    sN << "N -" << abs(steps) << ends;

  cmdpar = sN.str(); delete sN.str();
  cmds.push_back(cmd); cmds.push_back(cmdpar); //6 => near/not home
  
  strstream sD;
  sD << mp._indexor;
  if( delta > 0 )
    sD << " +" << abs(delta) << ends;
  else 
    sD << " -" << abs(delta) << ends;

  cmdpar = sD.str(); delete sD.str();
  cmds.push_back(cmd); cmds.push_back(cmdpar); //8 => +- step delta to named position
  */

  return (int)cmds.size();
}


// should be moved to UFCCInit or test?
void* UFCCSetup::nomotion(void* p) {
  UFCCSetup::CCThrdArg* arg = (UFCCSetup::CCThrdArg*) p;
  std::map< string, UFCCSetup::MotParm* >* mparms = arg->_mparms;
  std::map< string, UFCCSetup::MotParm* >::const_iterator mit;
  string option = arg->_option;
  UFCAR* car = arg->_car;
  UFGem* rec = arg->_rec; // should be either a cad or apply
  string name = "anon";
  string carname = UFCAServ::_instrum + ":cc:setupC";
  if( rec ) {
    name = rec->name();
    UFCAR* rcar = rec->hasCAR();
    if( rcar )
      carname = rcar->name();
  }
  if( car ) carname = car->name();
  carname += ".IVAL";

  clog<<"UFCCSetup::nomotion> option: "<<option<<", mparms cnt: "<<mparms->size()<<endl;

  for( mit = mparms->begin(); mit != mparms->end(); ++mit ) {
    deque< string > cv;  // CAR, SAD, Raw V, I, K
    string cmd, cmdpar, mname = mit->first;
    UFCCSetup::MotParm* mp = mit->second; // _posit should be either posname or +/- steps
    if( mp == 0 )
      continue;
    int sadsteps = INT_MIN;
    UFPVAttr* pva = _indexorSAD[mp->_indexor];
    if( pva ) sadsteps = (int)pva->getNumeric();
    //if( _verbose ) {
      clog<<"UFCCSetup::nomotion> mname: "<<mname<<", indexor: "<<mp->_indexor
	  <<", _pos (steps): "<<mp->_pos<<", _steps (relative): "<<mp->_steps<<endl;
      clog<<"UFCCSetup::nomotion> indexor: "<< mp->_indexor<<" current Steps: "<<_indexorSteps[mp->_indexor]
	  <<", SAD shows: "<<sadsteps<<endl;
    //}

    // if agent should set car on completion or error, indicate car channel (pvname):
    cmd = mp->_indexor; cmd += "CAR";
    cmdpar = carname;
    cv.push_back(cmd); cv.push_back(cmdpar); //2
    //if( _verbose )
      clog<<"UFCCSetup::nomotion> cmd: "<<cmd<<", cmdpar: "<<cmdpar<<endl;

    cmd = mp->_indexor; cmd += "SAD";
    cmdpar = sadChan(mname); cmdpar += ".INP";
    cv.push_back(cmd); cv.push_back(cmdpar); //4
    //if( _verbose )
      clog<<"UFCCSetup::nomotion> cmd: "<<cmd<<", cmdpar: "<<cmdpar<<endl;

    // do the firmware motion parameters need to be set?
    if( !mp->_firmware ) { // firmware has not yet been set and needs it?
      cmd = mp->_indexor; cmd += "Raw";
      cmdpar = mp->_indexor; cmdpar += "V "; cmdpar += UFRuntime::numToStr(mp->_vel);
      cv.push_back(cmd); cv.push_back(cmdpar); //6
      //if( _verbose )
        clog<<"UFCCSetup::nomotion> cmd: "<<cmd<<", cmdpar: "<<cmdpar<<endl;
 
      cmd = mp->_indexor; cmd += "Raw";
      cmdpar = mp->_indexor; cmdpar += "I "; cmdpar += UFRuntime::numToStr(mp->_ivel);
      cv.push_back(cmd); cv.push_back(cmdpar); //8
      //if( _verbose )
        clog<<"UFCCSetup::nomotion> cmd: "<<cmd<<", cmdpar: "<<cmdpar<<endl;

      cmd = mp->_indexor; cmd += "Raw";
      cmdpar = mp->_indexor; cmdpar += "Y 0 "; cmdpar += UFRuntime::numToStr(mp->_runc); 
      cv.push_back(cmd); cv.push_back(cmdpar); //10
      //if( _verbose )
        clog<<"UFCCSetup::nomotion> cmd: "<<cmd<<", cmdpar: "<<cmdpar<<endl;

      cmd = mp->_indexor; cmd += "Raw";
      cmdpar = mp->_indexor; cmdpar += "K ";
      cmdpar += UFRuntime::numToStr(mp->_acc); cmdpar += " "; cmdpar += UFRuntime::numToStr(mp->_dec);
      cv.push_back(cmd); cv.push_back(cmdpar); //12
      //if( _verbose )
        clog<<"UFCCSetup::nomotion> cmd: "<<cmd<<", cmdpar: "<<cmdpar<<endl;
    }
    size_t nc = cv.size();
    if( _vverbose )
      for( size_t i = 0; i < nc; ++i )
        clog<<"UFCCSetup::nomotion> cmdque "<<(i+1)<<" of "<<nc<<": "<<cv[i]<<endl;

    UFStrings req(name, cv); 
    if( _verbose )
      clog<<"UFCCSetup::nomotion> send (to motoragent) req: "<<req.name()<<" "<< req.timeStamp()<<", cmd: " <<cv[1]<<endl;
    int ns = UFCCSetup::_motoragntsoc->send(req);
    if( ns <= 0 ) {
      clog<<"UFCCSetup::nomotion> failed send req: "<< req.name() << " " << req.timeStamp() <<endl;
      break;
    }
    //if( _verbose )
      clog<<"UFCCSetup::nomotion> sent req: "<< req.name() << " " << req.timeStamp() <<", cmd: " <<cv[1]<<endl;
    //int nr = UFCCSetup::_motoragntsoc->recv(reqt);
  } // for each mechanism

  // and free the motparms?
  for( mit = mparms->begin(); mit != mparms->end(); ++mit ) {
    UFCCSetup::MotParm* mp = mit->second;
    delete mp;
  }
  // free arg
  delete arg;

  // exit daemon thread
  return p;
} // nomotion

/* moved to UFCCTest (and renamed)?
int UFCCSetup::originAndOrStatus(const string& option, std::map< string, UFCCSetup::MotParm* >* mparms,
				 int timeout, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec ) {
    rec->setBusy(timeout); // this will also set its _car busy
  }
  // create and run pthread to status or origin mechanisms and then either reset car to idle or error
  // thread should free MotParm* allocations...
  // pthread_t pmot;
  CCThrdArg* arg = new CCThrdArg(mparms, rec, car, option);
  if( _verbose )
    clog<<"UFCCSetup::motion> start motion thread for nmech: "<<mparms->size()<<endl;

  pthread_t thrid = UFPosixRuntime::newThread(UFCCSetup::nomotion, arg);

  return (int) thrid;
} // originOrStatus (nomotion)
*/

void* UFCCSetup::comotion(void* p) {
  UFCCSetup::CCThrdArg* arg = (UFCCSetup::CCThrdArg*) p;
  std::map< string, UFCCSetup::MotParm* >* mparms = arg->_mparms;
  std::map< string, UFCCSetup::MotParm* >::const_iterator mit;
  string option = arg->_option;
  UFCAR* car = arg->_car;
  UFGem* rec = arg->_rec; // should be either a cad or apply
  string name = "anon";
  string carname = UFCAServ::_instrum + "cc:setupC";
  string mosbcarname;
  if( rec ) {
    name = rec->name();
    UFCAR* rcar = rec->hasCAR();
    if( rcar )
      carname = rcar->name();
  }
  if( car ) {
    string nm = car->name();
    if( nm.find("bar") != string::npos || nm.find("bar") != string::npos ||nm.find("bar") != string::npos )
      mosbcarname = nm + ".IVAL";
  }
  carname += ".IVAL";

  clog<<"UFCCSetup::comotion> "<<name<<", mparms cnt: "<<mparms->size()<<endl;
  if( _sim ) { // simple simulation... 
    sleep(1);
    // and free the motparms
    for( mit = mparms->begin(); mit != mparms->end(); ++mit ) {
      UFCCSetup::MotParm* mp = mit->second;
      delete mp;
    }
    // free arg
    delete arg;

    // set cad/car idle:
    if( car )
      car->setIdle();
    if( rec ) {
      UFCAR* c = rec->hasCAR();
      if(c != 0 && c != car ) c->setIdle();
    }
    return p;
  }

  // test it to be sure non-blocking connection is functional/selectable (connection has completed)
  bool validsoc = connected(_motoragntsoc);
  if( !validsoc ) {
    clog<<"UFCCSetup::comotion> not connected to motor indexor agent, need to (re)init..."<<endl;
    // and free the motparms
    for( mit = mparms->begin(); mit != mparms->end(); ++mit ) {
      UFCCSetup::MotParm* mp = mit->second;
      delete mp;
    }
    // free arg
    delete arg;
    return p;
  }
 
  string mosbarcode, cmd, cmdpar;
  for( mit = mparms->begin(); mit != mparms->end(); ++mit ) {
    deque< string > cv;  // CAR, SAD, Raw N+-, Raw +-
    string mname = mit->first;
    UFCCSetup::MotParm* mp = mit->second; // _posit should be either posname or +/- steps
    if( mp == 0 ) {
      clog<<"UFCCSetup::comotion> mname: "<<mname<<" no paramaeters provided, skip..."<<endl;
      continue;
    }

    string sadname= sadChan(mname); string sadpvname= sadname + ".VAL";
    UFPVAttr* sadpva = _indexorSAD[mp->_indexor];
    int sadsteps = 0;
    if( sadpva ) {
      sadsteps = (int)sadpva->getNumeric(); sadpvname = sadpva->name();
      clog<<"UFCCSetup::comotion> mname: "<<mname<<", indexor: "<<mp->_indexor
	  <<", sadpvname: "<<sadpvname<<", current step cnt: "<<sadsteps<<endl;
    }
    else {
      clog<<"UFCCSetup::comotion> mname: "<<mname<<", indexor: "<<mp->_indexor
	  <<" unable to fetch current step cnt from SAD, skip..."<<endl;
      continue;
    }
    
     //if( _verbose ) {
     clog<<"UFCCSetup::comotion> mname: "<<mname<<", indexor: "<<mp->_indexor
	  <<", _pos (namedpos. steps): "<<mp->_pos<<", _steps (relative): "<<mp->_steps<<endl;
      clog<<"UFCCSetup::comotion> indexor: "<< mp->_indexor<<" current Steps: "<<_indexorSteps[mp->_indexor]
	  <<", "<<sadname<<" (SAD) shows: "<<sadsteps<<endl;
    //}
    
    if( mp->_indexor.find("A") == string::npos && mp->_indexor.find("B") == string::npos && 
	mp->_indexor.find("C") == string::npos && mp->_indexor.find("D") == string::npos && 
	mp->_indexor.find("E") == string::npos && mp->_indexor.find("F") == string::npos && 
	mp->_indexor.find("G") == string::npos && mp->_indexor.find("H") == string::npos ) {
      clog<<"UFCCSetup::comotion> mname: "<<mname<<", unrecognized indexor: "<<mp->_indexor<<endl;
      continue;
    }
    if( (mp->_pos == INT_MIN || mp->_pos == 0) &&
	(mp->_steps == INT_MIN || mp->_steps == 0) &&
	(mp->_blash == INT_MIN || mp->_blash == 0) ) {
      clog<<"UFCCSetup::comotion> mname: "<<mname<<", no step cnt or backlash provided?"<<endl;
      continue;
    }      
	
    if( sadsteps != _indexorSteps[mp->_indexor] ) {
      // note this may ultimately have to query indexor...
      clog<<"UFCCSetup::comotion> indexor: "<< mp->_indexor<<" forcing step count sync..."<<endl;
      _indexorSteps[mp->_indexor] = sadsteps;
    }
    if( mp->_pos > 0 && mp->_indexor == "B" ) {
      // is this a named position or relative step for mos barcode action?
      string posname;
      if( !mosbcarname.empty() && mosbcarname != "null" ) {
	// for barcode? set posname & return for barcode post cmd
        mosbarcode = _mosPosNames(mp->_pos, UFF2MOSBarCdIdx, posname);
      }
      else { // or optical path? set posname & return "" for barcode post
        mosbarcode = _mosPosNames(mp->_pos, UFF2MOSIdx, posname);
      }
      clog<<"UFCCSetup::comotion> mosbarcode SAD: "<<mosbarcode<<", posname: "<<posname<<endl;
    }

    // if agent should set car on completion or error, indicate car channel (pvname):
    cmd = mp->_indexor; cmd += "CAR";
    cmdpar = carname;
    cv.push_back(cmd); cv.push_back(cmdpar); //2
    //if( _verbose )
      clog<<"UFCCSetup::comotion> cmd: "<<cmd<<", cmdpar: "<<cmdpar<<endl;

    cmd = mp->_indexor; cmd += "SAD";
    cmdpar = sadChan(mname); cmdpar += ".INP";
    cv.push_back(cmd); cv.push_back(cmdpar); //4
    if( _verbose )
      clog<<"UFCCSetup::comotion> cmd: "<<cmd<<", cmdpar: "<<cmdpar<<endl;

    // always set initial and slew velocities to insure reliable motion
    cmd = mp->_indexor; cmd += "Raw";
    cmdpar = mp->_indexor; cmdpar += "I "; cmdpar += UFRuntime::numToStr(mp->_ivel);
    cv.push_back(cmd); cv.push_back(cmdpar); //8
    //if( _verbose )
      clog<<"UFCCSetup::comotion> cmd: "<<cmd<<", cmdpar: "<<cmdpar<<endl;

    cmd = mp->_indexor; cmd += "Raw";
    cmdpar = mp->_indexor; cmdpar += "V "; cmdpar += UFRuntime::numToStr(mp->_vel);
    cv.push_back(cmd); cv.push_back(cmdpar); //6
    //if( _verbose )
      clog<<"UFCCSetup::comotion> cmd: "<<cmd<<", cmdpar: "<<cmdpar<<endl;

    // do the entire set of firmware motion parameters need to be set?
    if( mp->_firmware ) {
      //run & hold current (hold current is fixed to 0)
      cmd = mp->_indexor; cmd += "Raw";
      cmdpar = mp->_indexor; cmdpar += "Y 0 "; cmdpar += UFRuntime::numToStr(mp->_runc); 
      cv.push_back(cmd); cv.push_back(cmdpar); //10
      //if( _verbose )
        clog<<"UFCCSetup::comotion> cmd: "<<cmd<<", cmdpar: "<<cmdpar<<endl;

      // acceleration
      cmd = mp->_indexor; cmd += "Raw";
      cmdpar = mp->_indexor; cmdpar += "K ";
      cmdpar += UFRuntime::numToStr(mp->_acc); cmdpar += " "; cmdpar += UFRuntime::numToStr(mp->_dec);
      cv.push_back(cmd); cv.push_back(cmdpar); //12
      //if( _verbose )
        clog<<"UFCCSetup::comotion> cmd: "<<cmd<<", cmdpar: "<<cmdpar<<endl;
    }

    // need yet another helper function here to decide whether this 
    // is an atomic (raw) step request or a name-position request that
    // must be decomposed into a near/not-home N+-cnt followed by
    // a raw +-bcnt step (backlash) request
    // this will also decide to perform a relative motion from 'current' position
    // if a named position (and its corresponding steps from home) is supplied
    // or an explcit non-relative motion:

    int nc = backlashSeq(*mp, cv);

    // current version of agent can't handle entire  bundle for multiple indexors,
    // need to send bundle for earch indexor, and be sure that indexor name is 
    // at position 0 of each car:
    clog<<"UFCCSetup::comotion> cmdque cnt: "<<nc<<endl;
    if( nc <= 0 )
      break;

    //if( _vverbose )
      for( int i = 0; i < nc; ++i )
        clog<<"UFCCSetup::comotion> cmdque "<<(i+1)<<" of "<<nc<<": "<<cv[i]<<endl;

    UFStrings req(name, cv); 
    if( _verbose )
      clog<<"UFCCSetup::comotion> send (to motoragent) req: "<<req.name()<<" "<< req.timeStamp()<<", cmd: " <<cv[1]<<endl;
    int ns = UFCCSetup::_motoragntsoc->send(req);
    if( ns <= 0 ) {
      clog<<"UFCCSetup::comotion> failed send req: "<< req.name() << " " << req.timeStamp() <<endl;
      break;
    }
    //if( _verbose )
      clog<<"UFCCSetup::comotion> sent req: "<< req.name() << " " << req.timeStamp() <<", cmd: " <<cv[1]<<endl;
    //int nr = UFCCSetup::_motoragntsoc->recv(reqt);
  } // for each mechanism

  if( mosbarcode.empty() || mosbarcode == "null" ) {
    clog<<"UFCCSetup::comotion> NO Barcode scan expected (BarCdCAR should remain Idle)..."<<endl;  
  }
  else {
    // if we get here the motoragent MOS transaction succeeded, so send a post request to the barcodeagent
    validsoc = connected(_barcdagntsoc);
    if( !validsoc ) {
      clog<<"UFCCSetup::comotion> not connected to motor indexor agent, need to init..."<<endl;
      // and free the motparms
      for( mit = mparms->begin(); mit != mparms->end(); ++mit ) {
        UFCCSetup::MotParm* mp = mit->second;
        delete mp;
      }
      // free arg
      delete arg;
      return p;
    }
    vector< string > cv;
    cv.push_back("CAR"); cv.push_back(mosbcarname);
    cv.push_back("POST"); cv.push_back(mosbarcode);
    UFStrings req("MOSBarCodePost", cv); 
    if( _verbose )
      clog<<"UFCCSetup::comotion> send (to barcodagent) req: "<<req.name()<<" "<<req.timeStamp()<<", cmd: "<<cv[1]<<endl;
    int ns = UFCCSetup::_barcdagntsoc->send(req);
    if( ns <= 0 )
      clog<<"UFCCSetup::comotion> failed send req: "<< req.name() << " " << req.timeStamp() <<endl;
    //if( _verbose )
      clog<<"UFCCSetup::comotion> sent(to barcodagent) req: "<< req.name() << " " << req.timeStamp() <<", cmd: " <<cv[1]<<endl;
    //int nr = UFCCSetup::_motoragntsoc->recv(reqt);
  }

  // and free the motparms
  for( mit = mparms->begin(); mit != mparms->end(); ++mit ) {
    UFCCSetup::MotParm* mp = mit->second;
    delete mp;
  }
  // free arg
  delete arg;

  // exit daemon thread
  return p;
}

int UFCCSetup::setDefaults(const string& name) {
  // trivial means of mapping mechname to indexor name, note MOS == MASBarCd == 'B'
  static char* _indexors[27] = { "A", "B", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", 
			         "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
  /*
  _mechNames.push_back("Window"); 
  _mechNames.push_back("MOS");
  _mechNames.push_back("MOSBarCd");
  _mechNames.push_back("Decker"); 
  _mechNames.push_back("Filter1");
  _mechNames.push_back("Lyot");
  _mechNames.push_back("Filter2");
  _mechNames.push_back("Grism");
  _mechNames.push_back("Focus");
  */

  _mechNames.clear();
  UFFLAM2Mech *_TheFLAM2Mech = getTheUFFLAM2Mech();
  UFFLAM2MechPos* mech;
  for(int m = 0; m < _TheFLAM2Mech->mechCnt; ++m ) {
    string indexor = infoFLAM2MechOf(m, &mech); // sets mech ptr
    //UFFLAM2MechPos mech = _TheFLAM2Mech->mech[m];
    string mname = mech->name;
    _mechNames.push_back(mname);
    _mechNameOfIndexor[indexor] = mname;
    _indexorNames[mname] = indexor;
    _indexorSteps[indexor] = 0;
    string sadrecname = sadChan(mname); string sadpvname = sadrecname + ".VAL";
    //clog<<"UFCCSetup::setDefaults> "<<indexor<<": "<<mname<<", sad: "<<sadrecname<<endl;
    UFGem* sad = _theRecList[sadrecname]; // or use UFSIR::_theSAD[sadrecname]
    if( sad != 0 ) {
      UFPVAttr* pva = _indexorSAD[_indexors[m]] = (*sad)[sadpvname]; // val UFPVAttr*
      clog<<"UFCCSetup::setDefaults> "<<indexor<<": "<<mname<<", sad: "<<sadrecname<<" sadpva name: "<<pva->name()<<endl;
    }
    else {
      UFSIR* ssad = UFSIR::_theSAD[sadrecname];
      if( ssad != 0 ) {
        UFPVAttr* pva = _indexorSAD[_indexors[m]] = (*ssad)[sadpvname]; // val UFPVAttr*
        clog<<"UFCCSetup::setDefaults> "<<indexor<<": "<<mname<<", sad: "<<sadrecname<<" sadpva name: "<<pva->name()<<endl;
      }
      else {
        clog<<"UFCCSetup::setDefaults> ?No SAD record for: "<<indexor<<": "<<mname<<", sad: "<<sadrecname<<endl;
      }
    }
    _mechPos[mname] = new vector< UFCCSetup::MechPos* >;
    vector< UFCCSetup::MechPos* >& posvec = (*_mechPos[mname]);
    for( int p = 0; p < mech->posCnt; ++p ) {
      string pname = mech->positions[p];
      int steps = (int) mech->steps[p];
      UFCCSetup::MechPos* mp = new UFCCSetup::MechPos(pname, steps);
      posvec.push_back(mp);
      //clog<<"UFCCSetup::setDefaults> "<<indexor<<": "<<mname<<", posname: "<<pname<<", possteps: "<<steps<<endl;
    }
  }

  return (int) _mechNames.size();
} // setDefaults

bool UFCCSetup::mechPosNameExists(const string& mechname, const string& posname) {
  // check initialization (defaults?)
  if( _mechPos.size() <= 0 ) return false;
  if( _mechPos.find(mechname) == _mechPos.end() ) return false;
  vector< UFCCSetup::MechPos* >& posvec = (*_mechPos[mechname]);
  for( size_t i = 0; i < posvec.size(); ++i ) {
    UFCCSetup::MechPos *mp = posvec[i];
    if( mp == 0 ) continue;
    if( mp->_posname == posname ) { // exact match!
      clog<<"UFCCSetup::mechPosNameExists> found: "<<mechname<<" "<<posname<<endl;
      return true;
    }
    string name =  mp->_posname;
    if( name.find(posname) != string::npos ) {
      clog<<"UFCCSetup::mechPosNameExists> found: "<<mechname<<" "<<posname<<endl;
      return true;
    }
    UFRuntime::upperCase(name);
    if( name.find(posname) != string::npos ) {
      clog<<"UFCCSetup::mechPosNameExists> found: "<<mechname<<" "<<posname<<endl;
      return true;
    }
    UFRuntime::lowerCase(name);
    if( name.find(posname) != string::npos ) {
      clog<<"UFCCSetup::mechPosNameExists> found: "<<mechname<<" "<<posname<<endl;
      return true;
    }
  }
  clog<<"UFCCSetup::mechPosNameExists> not found: "<<mechname<<" "<<posname<<endl;
  return false;
}
  
// public static helper funcs.
// allocate & return (deque) list of named-positions for given mechanism:
deque< string >* UFCCSetup::allocPosNamesList(const string& mechname, const string& also) {
  deque< string >* list = new deque< string >;
  // parse mech namelookup
  if( mechname.length() < 1 ) {
    clog<<"UFCCSetup::allocPosNamesList> ambiguous mechname: "<<mechname<<endl;
    return list;
  }
  vector< UFCCSetup::MechPos* >* posvec = UFCCSetup::_mechPos[mechname];
  if( posvec == 0 ) {
    clog<<"UFCCSetup::allocPosNamesList> mechname: "<<mechname<<" is unknown...not found in mech. list?"<<endl;
    //return list;
  }
  else {
    for( size_t i = 0; i < posvec->size(); ++i ) {
      UFCCSetup::MechPos* mp = (*posvec)[i];
      if( mp )
        list->push_back(mp->_posname);
    }
  }
  if( also.length() < 1 )
    return list;

  posvec = UFCCSetup::_mechPos[also];
  if( posvec == 0 ) {
    clog<<"UFCCSetup::allocPosNamesList> mechname also: "<<also<<" is unknown...not found in mech. list?"<<endl;
    return list;
  }
  for( size_t i = 0; i < posvec->size(); ++i ) {
    UFCCSetup::MechPos* mp = (*posvec)[i];
    if( mp )
      list->push_back(mp->_posname);
  }

  return list;
}

// return step count of named-position of named mechanism: 
int UFCCSetup::posSteps(const string& mechname, const string& posname) {
  // either parse posname as explicit step count or as named-position lookup
  // of optical path steps-from-datum for each(all) mechanism(s)
  if( mechname.length() < 1 || posname.length() < 1 ) {
    clog<<"UFCCSetup::posSteps> ambiguous mechname: "<<mechname<<" and/or ambiguous posname: "<<posname<<endl;
    return INT_MIN;
  }
  // is the name provided actually a step count?
  int sign = +1;
  char* cs = (char*)mechname.c_str();
  while( *cs == ' ' || *cs == '\t' || *cs == '+' ) ++cs; // elim. whites
  if( *cs == '-' ) { sign = -1; ++cs; }
  if( *cs == 0 ) // end of text?
    return INT_MIN;
  if( isdigit(*cs) )
    return sign * atoi(cs);

  vector< UFCCSetup::MechPos* >* posvec = UFCCSetup::_mechPos[mechname];
  if( posvec == 0 ) {
    clog<<"UFCCSetup::posSteps> mechname: "<<mechname<<" is unknown...not found in mech. list?"<<endl;
    return INT_MIN;
  }

  for( size_t i = 0; i < posvec->size(); ++i ) {
    UFCCSetup::MechPos* mp = (*posvec)[i];
    if( mp == 0 )
      continue;
    if( mp->_posname == posname ) // exact match!
      return mp->_steps;

    string name =  mp->_posname;
    string pname = posname;
    if( name.find(posname) != string::npos|| posname.find(name) != string::npos )
      return mp->_steps; // substring
    UFRuntime::upperCase(name); UFRuntime::upperCase(pname);
    if( name.find(pname) != string::npos || pname.find(name) != string::npos )
      return mp->_steps; // uppercase substring
    UFRuntime::lowerCase(name); UFRuntime::lowerCase(pname);
    if( name.find(pname) != string::npos || pname.find(name) != string::npos )
      return mp->_steps; // lowercase substring
  }
  clog<<"UFCCSetup::posSteps> mechname: "<<mechname<<" position name: "<<posname<<" is unknown...not found in position list?"<<endl;
  return INT_MIN;
}

/// general purpose motion func. (non datum)
/// arg: mechName, motparm*
int UFCCSetup::motion(std::map< string, UFCCSetup::MotParm* >* mparms,
		      int timeout, UFGem* rec, UFCAR* mosbarcar) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  string mosbarc;
  if( mosbarcar ) {
    mosbarc = mosbarcar->name();
    clog<<"UFCCSetup::motion> set MOSBarCode CAR to busy: "<<mosbarc<<endl;
    mosbarcar->setBusy(timeout);
  }
  else {
    clog<<"UFCCSetup::motion> MOSBarCode CAR is (hopefully) Idle... "<<endl;
  }
  /* setBusy is now called before genExec from start() func...
  if( rec ) {
    if( rec->name() != mosbarc )
      rec->setBusy(timeout); // this will also set its _car busy?
  }
  */
  // create and run pthread to move mechanisms and then either reset car to idle or error
  // thread should free MotParm* allocations...
  //pthread_t pmot;
  CCThrdArg* arg = new CCThrdArg(mparms, rec, mosbarcar);
  if( _verbose )
    clog<<"UFCCSetup::motion> start motion thread for nmech: "<<mparms->size()<<endl;

  pthread_t thrid = UFPosixRuntime::newThread(UFCCSetup::comotion, arg);

  return (int) thrid;
} // motion (comotion)

// non static, non virtual funcs:
// protected ctor helper:
void UFCCSetup::_create(const string& host, int miport, int bcport) {
  _host = host; _miport = miport; _bcport = bcport; // set global statics
  if( _host == "" ) _host = UFRuntime::hostname();
  /// pvs should look something like:
  /// flam:cc:setup.mech1 == string containing either "+/- steps" or "named-position"
  /// flam:cc:setup.mech1acc == acceleration setting for mech1
  /// flam:cc:setup.mech1runc == run current for mech1
  /// flam:cc:setup.mech1vel == slew velocity for mech1
  /// ... thru mechNvel

  string pvpar, pvname;
  UFPVAttr* pva= 0;

  pvname= _name + ".WindowLimit";
  pva = (*this)[pvname] = new UFPVAttr(pvname, "null"); // non null values indicate step cnt
  attachInOutPV(this, pva);

  pvname = _name + ".FocusLimit";
  pva = (*this)[pvname] = new UFPVAttr(pvname, "null"); // non null values indicate step cnt
  attachInOutPV(this, pva);

  pvname = _name + ".SetFirmware";
  pva = (*this)[pvname] = new UFPVAttr(pvname, "null"); // non null values indicate step cnt
  attachInOutPV(this, pva);

  int mcnt = setDefaults(_name);
  for( int i = 0; i < mcnt; ++i ) {
    pvname = _name + "."; pvname += _mechNames[i];
    pva = (*this)[pvname] = new UFPVAttr(pvname, "null"); // non null values indicate "named-position"
    attachInOutPV(this, pva);

    pvpar = pvname + "Step";
    pva = (*this)[pvpar] = new UFPVAttr(pvpar, INT_MIN ); // non null values indicate step cnt
    attachInOutPV(this, pva);

    pvpar = pvname + "Backlash";
    pva = (*this)[pvpar] = new UFPVAttr(pvpar, INT_MIN); // non null values indicate step cnt
    attachInOutPV(this, pva);

    pvpar = pvname + "Direction";
    pva = (*this)[pvpar] = new UFPVAttr(pvpar, "null"); // non null values indicate step cnt
    attachInOutPV(this, pva);

    pvpar = pvname + "Acc";
    pva = (*this)[pvpar] = new UFPVAttr(pvpar, INT_MIN);
    attachInOutPV(this, pva);

    pvpar = pvname + "Dec";
    pva = (*this)[pvpar] = new UFPVAttr(pvpar, INT_MIN);
    attachInOutPV(this, pva);

    pvpar = pvname + "RunCur";
    pva = (*this)[pvpar] = new UFPVAttr(pvpar, INT_MIN);
    attachInOutPV(this, pva);

    pvpar = pvname + "HoldCur";
    pva = (*this)[pvpar] = new UFPVAttr(pvpar, INT_MIN);
    attachInOutPV(this, pva);

    pvpar = pvname + "InitVel";
    pva = (*this)[pvpar] = new UFPVAttr(pvpar, INT_MIN);
    attachInOutPV(this, pva);

    pvpar = pvname + "SlewVel";
    pva = (*this)[pvpar] = new UFPVAttr(pvpar, INT_MIN);
    attachInOutPV(this, pva);
    /*
    (For MOS Wheel BarCode Positioning and scanning of MOS plates) -- assume it is in list
    if( _mechNames[i] == "MOS" ) {
      pvname += "BarCd"; 
      pva = (*this)[pvname] = new UFPVAttr(pvname, "null"); // non null values indicate "named-position"
      attachInOutPV(pva);
    }
    */
    // automatically create a SIR/SAD for each mech. step count pos:
    string sadname = sadChan(_mechNames[i]);
    UFSIR::_theSAD[sadname] = new UFSIR(sadname, 0.0);
    //clog<< "UFCCSetup::_create> SAD allocate: "<<sadname<<endl;
  }

  registerRec(); // self registration

  if( !_car ) {
    // default car for each cad...
    string carname = _name + "C";
    _car = new (nothrow) UFCAR(carname, this);
  }

  if( !_mosbarcar ) {
    // also create a MOS Barcode Reader CAR:
    string mosbcarname = _name + "BarCdC";
    _mosbarcar = new (nothrow) UFCAR(mosbcarname, this);
    //clog<<"UFCCSetup::_create> "<<mosbcarname<<endl;
    //_printMOSInfo();
  }
} // _create

/// virtual funcs:
int UFCCSetup::validate(UFPVAttr* pva) {
  if( pva == 0 )
    return 0;

  string input = pva->valString();
  if( input.find("+") != string::npos ) {
    clog<<"UFCCSetup::validate> + steps ok"<<endl;
    return 0; // step + ok
  }
  if( input.find("-") != string::npos ) {
    clog<<"UFCCSetup::validate> - steps ok"<<endl;
    return 0; // step - ok
  }  

  string pvname = pva->name();
  size_t pos = pvname.find(".");
  if( pos == string::npos ) {
    clog<<"UFCCSetup::validate> input pvname has no rec.field element? "<<pvname<<endl;
    return -1;
  }

  string mechname = pvname.substr(1+pos); // get the mechainism name from the input pv name
  vector< UFCCSetup::MechPos* >* posvec = _mechPos[mechname];
  if( posvec == 0 )
    return -1;
  else
    return 0;
  //  if( mechpos->_posname == mechname && mechpos->_steps != 0 ) // all ok
  //return 0;

  return -1;
} // validate

// helpers for genExec
int UFCCSetup::_mosSteps(const string& mos, int idx) {
  std::map< string, int > _mossteps;
  UFFLAM2MechPos* mech= 0;
  char* indexor = infoFLAM2MechOf(UFF2MOSBarCdIdx, &mech);
  if( indexor == 0 || mech == 0) return 0 ;
  int pcnt = mech->posCnt;
  for( int i = 0; i < pcnt; ++i ) {
    _mossteps[mech->positions[i]] = (int)mech->steps[i];
  }

  // named position may start with a numeric?
  if( _mossteps.find(mos) != _mossteps.end() ) {
    int steps = _mossteps[mos];
    clog<<"UFCCSetup::_mosSteps> mos: "<<mos<<", steps: "<<steps<<endl;
    return steps;
  }

  // try to evaluate as numeric?
  const char* c = mos.c_str();
  if( isdigit(c[0]) )
    return atoi(c);
  if( mos.length() > 1 && (c[0] == '+' || c[0] == '-') && isdigit(c[1]) )
    return atoi(c);

  return 0; 
}

// helpers for genExec
void UFCCSetup::_printMOSInfo() {
  std::map< string, int > _mossteps;
  UFFLAM2MechPos* mech= 0;
  char* indexor = infoFLAM2MechOf(UFF2MOSIdx, &mech);
  if( indexor == 0 || mech == 0) return;
  int pcnt = mech->posCnt;
  for( int i = 0; i < pcnt; ++i ) {
    clog<<"UFCCSetup::_printMOSInfo> Optical posname: "<<mech->positions[i]<<", steps: "<<(int)mech->steps[i]<<endl;
  }
  indexor = infoFLAM2MechOf(UFF2MOSBarCdIdx, &mech);
  if( indexor == 0 || mech == 0) return;
  pcnt = mech->posCnt;
  for( int i = 0; i < pcnt; ++i ) {
    clog<<"UFCCSetup::_printMOSInfo> BarCode posname: "<<mech->positions[i]<<", steps: "<<(int)mech->steps[i]<<endl;
  }
}

/// perform setup motion for those mechanisms with non-null step cnts or named-position inputs
int UFCCSetup::genExec(int timeout) {
  // copy inputs to outputs performed in UFCAD::setBusy()...
  size_t pvcnt = size();

  if( pvcnt == 0 ) {
    clog<<"UFCCSetup::genExec> np pvs?"<<endl;
    return (int) pvcnt;
  }
  int mcnt = _mechNames.size();
  string instrum = UFCAServ::_instrum;
  
  // which pv mechNames are non null?
  std::map< string, UFCCSetup::MotParm* >* mechs = new std::map< string, UFCCSetup::MotParm* >; // setup these
  string pvname, pvpar, input;
  string option= "null";
  UFPVAttr* pva = 0;

  bool allfirmware= false;
  pvname = _name + ".SetFirmware";
  pva = (*this)[pvname];
  if( pva != 0 ) {
    int setfirmw = (int) pva->getNumeric();
    if( setfirmw > INT_MIN ) {
      allfirmware = true;
      pva->clearVal();
    }
  }

  pvname = _name + ".WindowLimit";
  pva = (*this)[pvname];
  if( pva != 0 ) {
    input = pva->valString(); // input is anything non-null
    if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
      int vel = (int) pva->getNumeric();
      string mechName = "Window";
      if( std::find(_mechNames.begin(), _mechNames.end(), mechName) == _mechNames.end() ) {
	clog<<"UFCCSetup::genExec> bad mech. name?"<<endl;
      }
      string indexor = UFCCSetup::_indexorNames[mechName]; // indexor
    //if( _verbose )
      clog<<"UFCCSetup::genExec> expect motion for: "<<indexor<<", "<<pvname
	  <<", seeklimit velocity: "<<vel<<endl;
      //mp->_firmware = firmware;
      UFCCSetup::MotParm* mp = new UFCCSetup::MotParm(indexor, 0, 0, 0, 25, vel); // use default posname "datum"
      (*mechs)[mechName] = mp; // use default motion "datum"
      pva->clearVal();
    }
  }

  pvname =  _name + ".FocusLimit";
  pva = (*this)[pvname];
  if( pva != 0 ) {
    // this pv is indicates the agent host for connection
    input = pva->valString(); // input is anything non-null
    if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
      int vel = (int) pva->getNumeric();
      string mechName = "Focus";
      if( std::find(_mechNames.begin(), _mechNames.end(), mechName) == _mechNames.end() ) {
	clog<<"UFCCSetup::genExec> bad mech. name?"<<endl;
      }
      string indexor = UFCCSetup::_indexorNames[mechName]; // indexor
    //if( _verbose )
      clog<<"UFCCSetup::genExec> expect motion for: "<<indexor<<", "<<pvname
	  <<", seeklimit velocity: "<<vel<<endl;
      //mp->_firmware = firmware;
      UFCCSetup::MotParm* mp = new UFCCSetup::MotParm(indexor, 0, 0, 0, 25, vel); // use default posname "datum"
      (*mechs)[mechName] = mp; // use default motion "datum"
      pva->clearVal();
    }
  }

  string sad, mosbarcd, oldpvname, oldpvpar;
  for( int i = 0; i < mcnt; ++i ) {
    bool firmware= allfirmware;
    int steps= 0, blash= 0, mos= 0, pos= INT_MIN, ivel= 0, vel= 0, slow= INT_MIN, dir= 0, acc= 0, dec= 0, runc= 0, holdc= 0;
    string mname = _mechNames[i];
    pvname = _name + "."; pvname += mname;
    oldpvname =  _name + ".VAL"; oldpvname += mname; // this assumes output field naming == .VALwhatever
    pva = (*this)[pvname]; // non null values indicate "named-position"
    if( pva == 0 )
      continue;

    string posname;
    //if( _verbose )
    clog<<"UFCCSetup::genExec> pvname: "<<pvname<<", posname: "<<posname<<endl;

    if( pva->isSet() ) {
      posname = pva->valString();
      if( pvname.find("MOSBarCd") != string::npos &&
       	  pvname.find("Step") == string::npos ) { // barcode named position
        timeout += 30; // allow user plenty of time to pull the barcode trigger...
        UFApply::sysApply()->setTO(timeout); // and reset system timout
        mosbarcd = posname;
        mos = pos = _mosSteps(mosbarcd, UFF2MOSBarCdIdx);
        //if( _verbose )
	  clog<<"UFCCSetup::genExec> MOS BarCode (named) Step Request: "<<pos<<" "<<mosbarcd<<endl;
      }
      else if( pvname.find("MOS") != string::npos &&
	       pvname.find("Step") == string::npos ) { // mos optical path named position
        mos = pos = _mosSteps(posname, UFF2MOSIdx);
        //if( _verbose )
	  clog<<"UFCCSetup::genExec> MOS (named) Step Request: "<<pos<<" "<<mosbarcd<<endl;
      }
      else if( pvname.find("Step") == string::npos ) {
	// all other mechanisms have only one table of position names
	pos = (int)posSteps(mname, posname);
        //if( _verbose )
	clog<<"UFCCSetup::genExec> (named) Step Request: "<<pos<<", posname: "<<posname<<", mech: "<<mname<<endl;
      }
      pva->clearVal(); //clear position name
    }

    pvpar = pvname + "Step";
    pva = (*this)[pvpar]; // non null values indicate "named-position"
    if( pva != 0 ) {
      if( pva->isSet() )
        steps = (int) pva->getNumeric();
        //if( _verbose )
          clog<<"UFCCSetup::genExec> mechname: "<<mname<<", steps: "<<steps<<",  posname: "<<posname<<", possteps: "<<pos<<endl;
      pva->clearVal(); //clear steps
    }
    // if steps has been set, check whether it is for barcode activitiy or optical path:
    if( steps != 0 && pvpar.find("MOSBarCdStep") != string::npos ) {
      sad = _mosPosNames(steps, UFF2MOSBarCdIdx, posname, instrum);
      if( mosbarcd.empty() && posname != "null" || !posname.empty() ) {
        mosbarcd = posname;
        timeout += 30; // allow user plenty of time to pull the barcode trigger...
        UFApply::sysApply()->setTO(timeout); // and reset system timout
        //if( _verbose )
          clog<<"UFCCSetup::genExec> MOS BarCode (relative) Step Request: "<<steps<<" "<<mosbarcd<<endl;
      }
    }
    else if( pvpar.find("MOSStep") != string::npos ) { 
      //if( _verbose )
  	clog<<"UFCCSetup::genExec> MOS wheel (relative) step Request: "<<steps<<endl;
    }

    // remaining inputs are processed without clearing...
    pvpar = pvname + "Backlash";
    pva = (*this)[pvpar];
    if( pva ) {
      if( pva->isSet() )
        blash = (int) pva->getNumeric();
      pva->clearVal(); //clear input
    }

    // the following inputs can remain set (never cleared)
    pvpar = pvname + "Direction";
    pva = (*this)[pvpar];
    if( pva ) {
      if( pva->isSet() ) {
        input = pva->valString();  // 0 or 1 (- or +)
        dir = (input.find("-") == string::npos) ? 1 : 0;
      }
    }

    pvpar = pvname + "Acc"; 
    pva = (*this)[pvpar];
    if( pva ) {
      if( pva->isSet() ) {
        acc = (int) pva->getNumeric();
        oldpvpar = oldpvname + "Acc";
        UFPVAttr* oldpva = (*this)[oldpvpar];
        if( acc != (int)oldpva->getNumeric() )
          firmware = true;
      }
    }

    pvpar = pvname + "Dec";
    pva = (*this)[pvpar];
    if( pva ) {
      if( pva->isSet() ) {
        dec = (int) pva->getNumeric();
        oldpvpar = oldpvname + "Dec";
        UFPVAttr* oldpva = (*this)[oldpvpar];
        if( dec != (int)oldpva->getNumeric() )
	  firmware = true;
      }
    }

    pvpar = pvname + "RunCur";
    pva = (*this)[pvpar];
    if( pva ) {
      if( pva->isSet() ) {
        runc = (int) pva->getNumeric();
        oldpvpar = oldpvname + "RunCur";
        UFPVAttr* oldpva = (*this)[oldpvpar];
        if( runc != (int)oldpva->getNumeric() )
	  firmware = true;
      }
    }

    pvpar = pvname + "HoldCur";
    pva = (*this)[pvpar];
    if( pva ) {
      if( pva->isSet() ) {
        holdc = (int) pva->getNumeric();
        oldpvpar = oldpvname + "HoldCur";
        UFPVAttr* oldpva = (*this)[oldpvpar];
        if( holdc != (int)oldpva->getNumeric() )
	  firmware = true;
      }
    }

    // the ivel and vel should always get sent:
    pvpar = pvname + "InitVel";
    pva = (*this)[pvpar];
    if( pva ) {
      if( pva->isSet() ) {
        ivel = (int) pva->getNumeric();
        _ivel = ivel;
      }
    }

    pvpar = pvname + "SlewVel";
    pva = (*this)[pvpar];
    if( pva ) {
      if( pva->isSet() ) {
        vel = (int) pva->getNumeric();
        _vel = vel;
      }
    }
 
    string indexor = UFCCSetup::_indexorNames[_mechNames[i]]; // indexor
    //if( _verbose )
      clog<<"UFCCSetup::genExec> "<<_mechNames[i]<<input<<" "<<steps<<" "<<vel<<" "<<dir
	  <<" "<<ivel<<" "<<acc<<" "<<dec<<" "<<runc<<" "<<holdc<<endl;

    //if( (mos == 0 || steps == 0) && (posname == "" || posname == "null") ) {
    if( mos == 0 && steps == 0 && pos == INT_MIN ) {
      //if( _verbose )
        clog<<"UFCCSetup::genExec> No motion for: "<<_mechNames[i]
            <<", steps: "<<steps<<", posname: "<<posname<<endl;
      continue;
    }
    else if( mechs->find(_mechNames[i]) != mechs->end() ) {
      clog<<"UFCCSetup::genExec> Limit motion for: "<<_mechNames[i]
            <<" has been requested..."<<endl;
      continue; // already in motion list...
    }
      
    //if( _verbose )
      clog<<"UFCCSetup::genExec> expect motion for: "<<indexor<<", "<<_mechNames[i]
          <<", (raw/override) steps: "<<steps<<", mos (steps): "<<mos
          <<", posname: "<<posname<<", posnamesteps: "<<pos<<endl;
      UFCCSetup::MotParm* mp = new UFCCSetup::MotParm(indexor, posname, steps, blash, pos, _ivel, _vel, slow, 
						      dir, acc, dec, runc, holdc);
    mp->_firmware = firmware;
    (*mechs)[_mechNames[i]] = mp;
  }

  int stat= 0, nm = (int) mechs->size();
  // perform requested motion...
  if( nm > 0 ) {
    if( mosbarcd.empty() || mosbarcd == "null" ) {
      clog<<"UFCCSetup::genExec> expect motion for "<<nm<<" indexors..."<<endl;
      stat = motion(mechs, timeout, this, 0);
    }
    else {
      clog<<"UFCCSetup::genExec> expect (MOS BarCode) motion, "<<nm<<" indexors..."<<endl;
      stat = motion(mechs, timeout, this, _mosbarcar);
    }
  }
  else if( _verbose )
    clog<<"UFCCSetup::genExec> motion list is empty: "<<nm<<endl;

  if( stat < 0 ) { 
    clog<<"UFCCSetup::genExec> failed to start motion pthread..."<<endl;
    return stat;
  }

  return nm;
} // genExec

#endif // __UFCCSETUP_CC__

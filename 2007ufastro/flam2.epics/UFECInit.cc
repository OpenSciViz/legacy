#if !defined(__UFECINIT_EC__)
#define __UFECINIT_EC__ "$Name:  $ $Id: UFECInit.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFECINIT_EC__;

#include "UFECInit.h"
#include "UFInit.h"
#include "UFECSetup.h"
#include "UFPVAttr.h"
#include "UFPosixRuntime.h"
#include "UFPosixRuntime.h"
#include "UFTimeStamp.h"

// ctors:

// static class funcs:
// pthread static func:
void* UFECInit::connection(void* p) {
  UFInit::_CCthrdId = pthread_self();
  // just connects, if needed, to portescap motor indexor agent
  UFECSetup::ECThrdArg* arg = (UFECSetup::ECThrdArg*) p;
  UFCAR* car = arg->_car;
  UFGem* rec = arg->_rec; // should be either a cad or apply
  string name = "anon";
  if( rec )
    name = rec->name(); 

  // free arg
  delete arg;

  int ls332port= 0, lvdtport= 0;
  string host = UFECSetup::agentHostAndPorts(ls332port, lvdtport);
  //if( _verbose )
  clog<<"UFECInit::connecting> "<<host<<", on ports: "<<ls332port<<" (LS332), and "<<lvdtport<<" (LVDT)"<<endl;
  if( UFECSetup::_ls332d != 0 ) {
    UFECSetup::_ls332d->close();
    delete UFECSetup::_ls332d; UFECSetup::_ls332d= 0;
  }
  if( UFECSetup::_lvdtd != 0 ) {
    UFECSetup::_lvdtd->close();
    delete UFECSetup::_lvdtd; UFECSetup::_lvdtd= 0;
  }

  int err= 1;
  string errmsg = "Failed Agent Connect/Greeting";
  UFECSetup::_ls332d = new UFClientSocket;
  bool block= false, validsoc= true;
  int socfd = UFECSetup::_ls332d->connect(host, ls332port, block);
  if( socfd < 0 ) {
    validsoc = false;
    clog<<"UFECInit::connection> to Lakeshore 332 failed"<<endl;
  }
  // test it to be sure non-blocking connection is functional/selectable (connection has completed)
  validsoc = UFECSetup::_ls332d->validConnection();
  int cnt = 3;
  while( --cnt > 0 && !validsoc ) { // try a few times
    validsoc = UFECSetup::_ls332d->validConnection();
    if( !validsoc ) {
      clog<<"UFECInit::connection> Lakeshore 332 failed validation? trycnt: "<<cnt<<endl;
      UFRuntime::mlsleep(0.25);
    }
  }

  if( validsoc ) {
    //UFTimeStamp greet(name.c_str());
    UFTimeStamp greet(name);
    int ns = UFECSetup::_ls332d->send(greet);
    ns = UFECSetup::_ls332d->recv(greet);
    //if( _verbose )
      clog<<"UFECInit::connect> LS332 greeting: "<< greet.name() << " " << greet.timeStamp() <<endl;
    err = 0;
  }
  else {
    clog<<"UFECInit::connect> unable to transact greeting with Lakeshore 332 agent due to invalid connection..."<<endl;
  }                                                                             

  UFECSetup::_lvdtd = new UFClientSocket;
  block= false; validsoc= true;
  socfd = UFECSetup::_lvdtd->connect(host, lvdtport, block);
  if( socfd < 0 ) {
    validsoc = false;
    clog<<"UFECInit::connection> to LVDT failed"<<endl;
  }
  // test it to be sure non-blocking connection is functional/selectable (connection has completed)
  validsoc = UFECSetup::_lvdtd->validConnection();
  cnt = 3;
  while( --cnt > 0 && !validsoc ) { // try a few times
    validsoc = UFECSetup::_lvdtd->validConnection();
    if( !validsoc ) {
      clog<<"UFECInit::connection> LVDt failed validation? trycnt: "<<cnt<<endl;
      UFRuntime::mlsleep(0.25);
    }
  }

  if( validsoc ) {
    //UFTimeStamp greet(name.c_str());
    UFTimeStamp greet(name);
    int ns = UFECSetup::_lvdtd->send(greet);
    ns = UFECSetup::_lvdtd->recv(greet);
    //if( _verbose )
      clog<<"UFECInit::connect> LVDT greeting: "<< greet.name() << " " << greet.timeStamp() <<endl;
    err = 0;
  }
  else {
    clog<<"UFECInit::connect> unable to transact greeting with LVDT agent due to invalid connection..."<<endl;
  }                                                                             

  // set car idle 
  if( car ) {
    if( err )
      car->setErr(errmsg, err);
    else
      car->setIdle();
  }
  else if( rec ) {
    rec->setIdle();
  }

  UFInit::_CCthrdId = 0;
  // exit daemon thread
  return p;
}

/// general purpose abort func. 

/// arg: mechName, motparm*
int UFECInit::connect(UFECSetup::EnvParm* parms, int timeout, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec && car ) {
    UFCAR* c = rec->hasCAR();
    if( car != c ) car->setBusy(timeout);
  }
  UFECSetup::ECThrdArg* arg = new UFECSetup::ECThrdArg(parms, rec, car);
  //if( _verbose )
    clog<<"UFECInit::connect> start connection thread , "<<rec->name()<<endl;
  pthread_t thrid = UFPosixRuntime::newThread(UFECInit::connection, arg);

  return (int) thrid;
}

// non static, non virtual funcs:
// protected ctor helper:
void UFECInit::_create() {
  string host = UFRuntime::hostname();
  UFPVAttr* pva= 0;
  string pvname = _name + ".host";
  pva = (*this)[pvname] = new UFPVAttr(pvname, host); // host
  attachInOutPV(this, pva);

  pvname = _name + ".portLS332";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 52003); // lakeshore 332 port
  attachInOutPV(this, pva);

  pvname = _name + ".portLVDT";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 52006); // mos cryostat pfeiffer vac port
  attachInOutPV(this, pva);

  registerRec();
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
}

/// virtual funcs:
/// perform initial connection to portescap motor agent
/// virtual funcs:
/// perform initial connection too all agents

int UFECInit::genExec(int timeout) {
  size_t pvcnt = size();

  if( pvcnt == 0 ) {
    clog<<"UFECInit::genExec> np pvs?"<<endl;
    return (int) pvcnt;
  }

  string pvname = _name + ".host";
  UFPVAttr* pva = (*this)[pvname];
  string input = pva->valString(); // input is anything non-null
  if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
    clog<<"UFECInit::genExec> connect host: "<<input<<endl;
    UFECSetup::setHost(input);
    //pva->clearVal(); // clear?
  }
  else {
    UFECSetup::setHost(UFRuntime::hostname());
  }
  int ls332= 52003, lvdt= 52006;
  pvname = _name + ".portLS332"; // only need to command the pid & setp on lakeshore 332
  pva = (*this)[pvname];
  if( pva != 0 ) {
    input = pva->valString(); // input is anything non-null
    if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
      ls332 = (int)pva->getNumeric();
      clog<<"UFECInit::genExec> port: "<<input<<" == "<<UFECSetup::_ls332port<<endl;
      //pva->clearVal(); // clear?
    }
  }
  pvname = _name + ".portLVDT"; // only need to command the pid & setp on lakeshore 332
  pva = (*this)[pvname];
  if( pva != 0 ) {
    input = pva->valString(); // input is anything non-null
    if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
      lvdt = (int)pva->getNumeric();
      clog<<"UFECInit::genExec> port: "<<input<<" == "<<UFECSetup::_lvdtport<<endl;
      //pva->clearVal(); // clear?
    }
  }
  UFECSetup::setPorts(ls332, lvdt);
  UFECSetup::EnvParm* p = new UFECSetup::EnvParm;

  //if( _verbose )
    clog<<"UFECInit::genExec> start LS332 and LVDT agent connection pthread..."<<endl;
  int stat = connect(p, timeout, this, _car);  // perform requested abort

  if( stat < 0 ) { 
    clog<<"UFECInit::genExec> failed to start connection pthread..."<<endl;
    return stat;
  }

  return stat;
} // genExec

int UFECInit::validate(UFPVAttr* pva) {
  return 0;
} // validate


#endif // __UFECINIT_EC__

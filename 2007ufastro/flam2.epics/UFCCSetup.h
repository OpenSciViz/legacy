#if !defined(__UFCCSetup_h__)
#define __UFCCSetup_h__ "$Name:  $ $Id: UFCCSetup.h,v 0.33 2006/09/12 19:01:36 hon Exp $"
#define __UFCCSetup_H__(arg) const char arg##CCSetup_h__rcsId[] = __UFCCSetup_h__;

#include "UFCAD.h"

#include "vector"
#include "deque"
#include "map"

using namespace std;

class UFCCSetup : public UFCAD {
public:
  // mechanism motion paramaters -- note that _posit can be "+/- steps" or "position-name" 
  struct MotParm { 
    // inidcate current indexor firmware motion parms have been set and should be re-used (if false, reset firmware):
    bool _firmware;
    string _indexor, _posit;
    // is motion is for mos plate insertion, set _mos > 0 
    // note _steps should be relative to current position, while _pos should be steps from home
    int _steps, _blash, _pos, _ivel, _vel, /*_islow,*/ _slow, _acc, _dec, _runc, _holdc, _direction;

    // hold current is == 0, no need to supply a val.
    inline MotParm(const string& indexor, int steps= INT_MIN, int blash= 0,
		   int pos= INT_MIN, int ivel= 25, int vel= 100, /*int islow= 25,*/ int slow= 25,
		   int dir= 1, int acc= 100, int dec= 100,
		   int runc= 25, int holdc= 0, const string& posit= "Datum") : 
      _firmware(false), _indexor(indexor), _posit(posit), _steps(steps), _blash(blash),
      _pos(pos), _ivel(ivel), _vel(vel), /*_islow(islow),*/ _slow(slow), _acc(acc), _dec(dec), 
      _runc(runc), _holdc(holdc), _direction(dir) {}

    inline MotParm(const string& indexor, const string& posit, int steps= INT_MIN, int blash= 0,
		   int pos= INT_MIN, int ivel= 25, int vel= 100, /*int islow= 25,*/ int slow= 25,
		   int dir= 1, int acc= 100, int dec= 100, int runc= 25, int holdc= 0) : 
      _firmware(false), _indexor(indexor), _posit(posit), _steps(steps), _blash(blash),
      _pos(pos), _ivel(ivel), _vel(vel), /*_islow(islow),*/ _slow(slow),  _acc(acc), _dec(dec),
      _runc(runc), _holdc(holdc), _direction(dir) {}

    inline MotParm(bool firmware, const string& indexor, const string& posit, int step= INT_MIN, int blash= 0,
		   int pos= INT_MIN, int ivel= 25, int vel= 100, /*int islow= 25,*/ int slow= 25,
		   int dir= 1, int acc= 100, int dec= 100, int runc= 25, int holdc= 0) : 
      _firmware(firmware), _indexor(indexor), _posit(posit), _steps(step), _blash(blash),
      _pos(pos), _ivel(ivel), _vel(vel), /*_islow(islow),*/ _slow(slow), _acc(acc), _dec(dec),
      _runc(runc),  _holdc(holdc),_direction(dir) {}
  };
  // mechanism "position-name" steps-from-datum 
  struct MechPos { string _posname; int _steps;
    inline MechPos(const string& name, int step= INT_MIN) : _posname(name), _steps(step) {}
  };

  // thread arg:
  struct CCThrdArg {
    string _option;
    std::map< string, UFCCSetup::MotParm* >* _mparms; UFGem* _rec; UFCAR* _car;
    inline CCThrdArg(std::map< string, UFCCSetup::MotParm* >* mp= 0,
		     UFGem* r= 0, UFCAR* c= 0, const string& opt= "null") :
      _option(opt), _mparms(mp), _rec(r), _car(c) {}
    inline ~CCThrdArg() { delete _mparms; }
  };

  inline UFCCSetup(const string& instrum= "instrum", const string& host= "", int miport= 52024, int bcport= 52005,
		   UFCAR* car= 0) : UFCAD(instrum+":cc:setup", car) { _create(host, miport, bcport); }

  inline virtual ~UFCCSetup() {}

  static bool mechPosNameExists(const string& mechname, const string& posname);

  // mech. named-positions
  static std::map< string, vector< UFCCSetup::MechPos* >*  > UFCCSetup::_mechPos;
  static deque< string > _mechNames; // mech. cnt and names
  // key == full indexor mechanism name, val == A-z portescap partyline name
  static std::map< string, string > _indexorNames;
  // key == A-z portescap partyline name, val ==  full indexor mechanism name
  static std::map< string, string > _mechNameOfIndexor;
  // to evaluate +- offsets for the backlash Not/Near Home command,
  // need to kep track of current step counts for each indexor;
  // this should be double-checked against the values in the SAD:
  static std::map< string, int > _indexorSteps;
  // SAD record.field values should agree with above:
  static std::map< string, UFPVAttr* > _indexorSAD;
  // connection to motor and barcode agents
  static UFClientSocket *_motoragntsoc, *_barcdagntsoc;

  virtual int validate(UFPVAttr* pva= 0);

  /// perform setup motion for those mechanisms with non-null step cnts or named-position inputs
  virtual int genExec(int val= 0);

  /// pvs should look something like:
  /// flam:cc:setup.mech1 == string containing either "+/- steps" or "named-position"
  /// flam:cc:setup.mech1acc == acceleration setting for mech1
  /// flam:cc:setup.mech1runc == run current for mech1
  /// flam:cc:setup.mech1vel == slew velocity for mech1
  /// ... thru mechNvel
  static int setDefaults(const string& name);

  // thread entry func. for origin or status commands (no motion):
  static void* nomotion(void* p= 0); // should be moved to UFCCInit? 

  /// non motion function creates pthread
  //static int originAndOrStatus(const string& option, std::map< string, UFCCSetup::MotParm* >* mparms,
  //int timeout, UFGem* rec= 0, UFCAR* car= 0);

  // thread entry func. for motion command(s)
  static void* comotion(void* p= 0);

  ///general purpose motion func. (non datum) creates pthread
  // of use here and from IS. starts a new daemon thread for every propper motion seq.
  static int motion(std::map< string, UFCCSetup::MotParm* >* mechs, int timeout= 10, UFGem* rec= 0, UFCAR* car= 0); 

  inline static deque< string >* getMechNames() { return &_mechNames; }
  // positions usually refer to optical paths, with the exception the mos wheel, which has 2 lists
  // of positions. _mos > 0 indicating barcode mos plate position (not optical position):
  inline static vector< UFCCSetup::MechPos* >* getMechPos(const string& mechname) { return _mechPos[mechname]; }
  
  inline static void setHost(const string& host) { _host = host; }
  inline static void setPort(int miport= 52024, int bcport= 52005) { _miport = miport; _bcport = bcport;}
  inline static string agentHostAndPort(int& miport, int& bcport)
    { miport = _miport; bcport = _bcport; return _host; }


  void _printMOSInfo();

  static string sadChan(const string& mechname); // SAD of mechanism
  static int backlashSeq(const UFCCSetup::MotParm& mp, deque<string>& cmds);

  // fetch current steps from home position of specified mechanism via _theSAD global: 
  static double fetchCurrentSteps(const string& mechname);

  // these two cmdFuncs can be used my IS or by CCInit, CCDatum, and CCTest:
  // optional origin on setup (or init or datum or test) -- option can be "Origin All" or individual indexor name "Origin A-H"
  static void cmdOrigin(const string& option, std::map< string, UFCCSetup::MotParm* >* mparms,
			UFGem* rec= 0, UFCAR* car= 0);
  // optional origin on setup (or init) -- option can be "Status All" or "Status A-H"
  static void cmdStatus(const string& option, std::map< string, UFCCSetup::MotParm* >* mparms,
			UFGem* rec= 0, UFCAR* car= 0);

  // allocate & return (deque) list of named-positions for given mechanism:
  static deque< string >* allocPosNamesList(const string& mechname, const string& alsomech= "");
  // return step count of named-position of named mechanism:
  static int posSteps(const string& mechname, const string& posname);

  // save initial and slew vel. settings for reuse during step commands
  static int _ivel, _vel;

protected:
  UFCAR* _mosbarcar; // supplemental car for mos plate insertions
  static string _host; // agent location
  static int _miport, _bcport;

  // ctor helper
  void _create(const string& host, int miport= 52024, int bcport= 52005);

  // genExec helper
  static int _mosSteps(const string& mos, int idx);

  // genExec & comotion helpers
  static string _mosPosNames(int pos, int idx, string& nearest, const string& instrum= "flam");
};

#endif // __UFCCSetup_h__

#if !defined(__UFInit_h__)
#define __UFInit_h__ "$Name:  $ $Id: UFInit.h,v 0.10 2005/07/28 17:30:07 hon Exp $"
#define __UFInit_H__(arg) const char arg##Init_h__rcsId[] = __UFInit_h__;

#include "UFCAD.h"

// provide cc, dc, and ec socket connection objects
#include "UFInstrumSetup.h"
//#include "UFCCSetup.h"
//#include "UFDCSetup.h"
//#include "UFECSetup.h"

// initialize socket connections to agents 
class UFInit : public UFCAD {
public:
  struct Parm {
    std::map<string, string >* _hostmap; std::map<string, int>* _portmap;
    Parm(const std::map<string, string>& hosts, const std::map< string, int>& ports);
    inline Parm() { _hostmap = new std::map<string, string>; _portmap = new std::map<string, int>; }
    inline ~Parm() { delete _hostmap; delete _portmap; }
  };

  // thread arg:
  struct ThrdArg {
    string _option;
    int _timeout;
    Parm* _parms; UFGem* _rec; UFCAR* _car;
    inline ThrdArg(int to= 10, Parm* p= 0, UFGem* r= 0, UFCAR* c= 0, const string& opt= "null") :
      _option(opt), _timeout(to), _parms(p), _rec(r), _car(c) {}
    inline ~ThrdArg() { delete _parms; }
  };

  inline UFInit(const string& instrum= "instrum") : UFCAD(instrum+":init") { _create("instrum"); }
  inline virtual ~UFInit() {}

  virtual int validate(UFPVAttr* pva= 0);
  virtual int genExec(int timeout= 0);

  // connect to executive and to all device agents
  static void* connectAll(void* p);
  static int connect(Parm* p, int timeout, UFGem* rec= 0, UFCAR* car= 0);

  // for executive connection
  inline static void setHost(const string& host, const string& instrum= "flam")
    { if( _hostmap ) (*_hostmap)[instrum] = host; }
  inline static void setPort(int port= 3720, const string& instrum= "flam")
    { if( _portmap ) (*_portmap)[instrum] = port; }
  inline static string agentHostAndPort(int& port, const string& instrum= "flam") 
    { if( _portmap ) port = (*_portmap)[instrum]; if( _hostmap ) return (*_hostmap)[instrum]; return ""; }
   
  // public child thread ids:
  static pthread_t _CCthrdId;
  static pthread_t _DCthrdId;
  static pthread_t _ECthrdId;

protected:
  static std::map<string, string>* _hostmap; // default hostmap, all agents
  static std::map<string, int>* _portmap; // default portmap, all agents
  // ctor helper
  void _create(const string& host, int port= 3720);
};

#endif // __UFInit_h__

#if !defined(__UFDCDatum_h__)
#define __UFDCDatum_h__ "$Name:  $ $Id: UFDCDatum.h,v 0.6 2005/12/16 22:16:44 hon Exp $"
#define __UFDCDatum_H__(arg) const char arg##DCDatum_h__rcsId[] = __UFDCDatum_h__;

#include "UFCAD.h"
#include "UFDCSetup.h"

#include "deque"

class UFDCDatum : public UFCAD {
public:
  inline UFDCDatum(const string& instrum= "instrum") : UFCAD(instrum+":dc:datum") { _create(); }
  inline UFDCDatum(const string& instrum, UFCAR* car) : UFCAD(instrum+":dc:datum", car) { _create(); }
  inline virtual ~UFDCDatum() {}

  virtual int validate(UFPVAttr* pva= 0);
  virtual int genExec(int timeout= 0);

  // genExec invokes this, and so can DCSetup, and IS ...
  static int datum(UFDCSetup::DetParm* parms, int timeout, UFGem* rec= 0, UFCAR* car= 0);

  // default (is probably all we need?)
  static int datum(int timeout, int lab= 0, UFGem* rec= 0, UFCAR* car= 0);

  // pthread func.
  static void* datumMCE4(void* p);

protected:
  // ctor helper
  void _create();

  // cmd req. helpers:
  static int _parmCmds(UFDCSetup::DetParm* parm, std::deque< string >& cmds);
  static int _defaultCmdsLab(std::deque< string >& cmds);
};

#endif // __UFDCDatum_h__

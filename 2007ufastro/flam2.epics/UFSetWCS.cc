#if !defined(__UFSetWCS_CC__)
#define __UFSetWCS_CC__ "$Name:  $ $Id: UFSetWCS.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFSetWCS_CC__;

#include "UFSetWCS.h"
#include "UFCAR.h"
#include "UFPVAttr.h"
#include "UFPosixRuntime.h"

// ctors:

// static class funcs:
void* UFSetWCS::coordination(void* p) {
  // coordinate really just needs the mech. names
  UFSetWCS::ThrdArg* arg = (UFSetWCS::ThrdArg*) p;
  UFSetWCS::Parm* parms = arg->_parms;
  UFCAR* car = arg->_car;
  UFGem* rec = arg->_rec; // should be either a cad or apply
  string name = "anon";
  if( rec )
    name = rec->name();

  // simple simulation...
  clog<<"UFSetWCS::coordinateing>... "<<name<<endl;
  if( parms && parms->_op && parms->_osp && parms->_isp ) {
    clog<<"UFSetWCS::coordination> obs timeout: "<<parms->_op->_timeout<<", exptime: "<<parms->_osp->_exptime<<endl;
    if( parms->_isp->_mparms && !parms->_isp->_mparms)
      clog<<"UFSetWCS::coordination> motors: "<<parms->_isp->_mparms->size()<<endl;
    if( parms->_isp->_eparms )
      clog<<"UFSetWCS::coordination> environment: "<<parms->_isp->_eparms->_MOSKelv
	  <<", "<<parms->_isp->_eparms->_CamAKelv<<", "<<parms->_isp->_eparms->_CamBKelv<<endl;
    if( parms->_osp->_dparms )
      clog<<"UFSetWCS::coordination> detector pixels: "<<parms->_osp->_dparms->_w*parms->_osp->_dparms->_h<<endl;
  }
  // free arg
  delete arg;

  if( _sim ) {
    // set car idle after 1/2 sec:
    UFPosixRuntime::sleep(0.5);
    if( car )
      car->setIdle();
    if( rec ) {
      UFCAR* c = rec->hasCAR();
      if( c != 0 && c != car ) c->setIdle();
    }
  }

  // exit daemon thread
  return p;
}

/// general purpose coordinate func. 

/// arg: mechName, motparm*
int UFSetWCS::coordinate(UFSetWCS::Parm* parms, int timeout, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec && car ) {
    UFCAR* c = rec->hasCAR();
    if( car != c ) car->setBusy(timeout);
  }

  // create and run pthread to coordinate whatever is currently being performed then either reset car to idle or error
  // thread should free the tParm* allocations...
  if( _sim ) {
    UFSetWCS::ThrdArg* arg = new UFSetWCS::ThrdArg(parms, rec, car);
    pthread_t thrid = UFPosixRuntime::newThread(UFSetWCS::coordination, arg);
    return (int) thrid;
  }

  return 0;
}

// non static, non virtual funcs:
// protected ctor helper:
void UFSetWCS::_create() {
  string pvname = _name + ".mode";
  UFPVAttr* pva = (*this)[pvname] = new UFPVAttr(pvname); // any non null string should indicate coordinate action 
  attachInputPV(pva); 
  registerRec();
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
}

/// virtual funcs:
/// perform coordinate mechanisms with non-null step cnts or named-position inputs
int UFSetWCS::genExec(int timeout) {
  size_t pvcnt = size();

  if( pvcnt == 0 ) {
    clog<<"UFSetWCS::genExec> np pvs?"<<endl;
    return (int) pvcnt;
  }

  string pvname = _name + ".mode";
  UFPVAttr* pva = (*this)[pvname];
  string input = pva->valString(); // input is anything non-null
  if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
   clog<<"UFSetWCS::genExec> mode: "<<input<<endl;
    pva->clearVal(); // clear
  }
  UFSetWCS::Parm* p = new UFSetWCS::Parm;
  int stat = coordinate(p, timeout, this, _car);  // perform requested coordinate

  if( stat < 0 ) { 
    clog<<"UFSetWCS::genExec> failed to start coordinate pthread..."<<endl;
    return stat;
  }

  return stat;
} // genExec

int UFSetWCS::validate(UFPVAttr* pva) {
  return 0;
} // validate


#endif // __UFSetWCS_CC__

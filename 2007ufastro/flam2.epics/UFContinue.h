#if !defined(__UFContinue_h__)
#define __UFContinue_h__ "$Name:  $ $Id: UFContinue.h,v 0.2 2005/06/02 18:18:24 hon Exp $"
#define __UFContinue_H__(arg) const char arg##Continue_h__rcsId[] = __UFContinue_h__;

#include "UFCAD.h"
#include "UFInstrumSetup.h"
#include "UFObsSetup.h"
#include "UFObserve.h"
#include "UFCCSetup.h"
#include "UFDCSetup.h"
#include "UFECSetup.h"

// either abort an instrument setup or an obssetup or the ongoing observation
class UFContinue : public UFCAD {
public:
  struct Parm { UFObserve::Parm* _op; UFObsSetup::Parm* _osp; UFInstrumSetup::Parm* _isp;
    inline Parm(UFObserve::Parm* op= 0, UFObsSetup::Parm* osp= 0, UFInstrumSetup::Parm* isp= 0): _op(op), _osp(osp), _isp(isp) {}
    inline ~Parm() { delete _op; delete _osp; delete _isp; }
  };

  // thread arg:
  struct ThrdArg { Parm* _parms; UFGem* _rec; UFCAR* _car;
    inline ThrdArg(Parm* p= 0, UFGem* r= 0, UFCAR* c= 0) : _parms(p), _rec(r), _car(c) {}
    inline ~ThrdArg() {}
  };
  inline UFContinue(const string& instrum= "instrum") : UFCAD(instrum+":continue") { _create(); }
  inline virtual ~UFContinue() {}

  virtual int validate(UFPVAttr* pva= 0);
  virtual int start(int timeout= 1800); // max 30 min. observation?
  virtual void setBusy(size_t timeout= 1800);
  virtual void setIdle();
  virtual int genExec(int timeout= 0);

  static void* contSys(void* p);
  static int cont(UFContinue::Parm* p, int timeout, UFGem* rec= 0, UFCAR* car= 0);

protected:
  // ctor helper
  void _create();
};

#endif // __UFContinue_h__

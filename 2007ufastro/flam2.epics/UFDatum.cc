#if !defined(__UFDATUM_CC__)
#define __UFDATUM_CC__ "$Name:  $ $Id: UFDatum.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFDATUM_CC__;

#include "UFDatum.h"
#include "UFCCDatum.h"
#include "UFDCDatum.h"
#include "UFECDatum.h"
#include "UFPVAttr.h"
#include "UFPosixRuntime.h"

int UFDatum::_DatumCnt= 0;
// ctors:

// static class funcs:
void* UFDatum::datumAll(void* p) {
  // datum really just needs the mech. names
  UFDatum::ThrdArg* arg = (UFDatum::ThrdArg*) p;
  UFDatum::Parm* parms = arg->_parms;
  UFCAR* car = arg->_car;
  UFGem* rec = arg->_rec; // should be either a cad or apply
  string name = "anon";
  if( rec )
    name = rec->name();

  // simple simulation...
  clog<<"UFDatum::datuming>... "<<endl;
  if( parms && parms->_op && parms->_osp && parms->_isp ) {
    clog<<"UFDatum::datuming> obs timeout: "<<parms->_op->_timeout<<", exptime: "<<parms->_osp->_exptime<<endl;
    if( parms->_isp->_mparms && !parms->_isp->_mparms)
      clog<<"UFDatum::datuming> motors: "<<parms->_isp->_mparms->size()<<endl;
    if( parms->_isp->_eparms )
      clog<<"UFDatum::datuming> environment: "<<parms->_isp->_eparms->_MOSKelv
	  <<", "<<parms->_isp->_eparms->_CamAKelv<<", "<<parms->_isp->_eparms->_CamBKelv<<endl;
    if( parms->_osp->_dparms )
      clog<<"UFDatum::datuming> detector pixels: "<<parms->_osp->_dparms->_w*parms->_osp->_dparms->_h<<endl;
  }
  // free arg
  delete arg;

  // set car idle after 1/2 sec:
  UFPosixRuntime::sleep(0.5);
  if( car )
    car->setIdle();
  if( rec ) {
    UFCAR* c = rec->hasCAR();
    if( c != 0 && c != car ) c->setIdle();
  }

  // exit daemon thread
  return p;
}

/// general purpose datum func. 

/// arg: mechName, motparm*
int UFDatum::datum(UFDatum::Parm* parms, int timeout, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec && car ) {
    UFCAR* c = rec->hasCAR();
    if( car != c ) car->setBusy(timeout);
  }

  // create and run pthread to datum whatever is currently being performed then either reset car to idle or error
  // thread should free the tParm* allocations...
  //
  if( _sim ) {
    UFDatum::ThrdArg* arg = new UFDatum::ThrdArg(parms, rec, car);
    pthread_t thrid = UFPosixRuntime::newThread(UFDatum::datumAll, arg);
    return (int) thrid;
  }
  int cnt = 0;
  string subsys;
  if( parms != 0 ) subsys = parms->_subsys;
 
  // or use convenience funcs. provided by subsystem classes:
  if( subsys.empty() || 
      subsys.find("all") != string::npos || subsys.find("All") != string::npos || subsys.find("ALL") != string::npos ||
      subsys.find("cc") != string::npos || subsys.find("Cc") != string::npos || subsys.find("CC") != string::npos ) {
    clog<<"UFDatum::datum> CC..."<<endl;
    UFCCDatum::homeAll(timeout, rec, car); ++cnt;
  }
  else {
    clog<<"UFDatum::datum> CC NOT!..."<<endl;
  }

  if( subsys.empty() || 
      subsys.find("all") != string::npos || subsys.find("All") != string::npos || subsys.find("ALL") != string::npos ||
      subsys.find("ec") != string::npos || subsys.find("Ec") != string::npos || subsys.find("EC") != string::npos ) {
    clog<<"UFDatum::datum> EC..."<<endl;
    UFECDatum::datum(timeout, rec, car); ++cnt;
  }
  else {
    clog<<"UFDatum::datum> EC NOT!..."<<endl;
  }

  if( subsys.empty() || 
      subsys.find("all") != string::npos || subsys.find("All") != string::npos || subsys.find("ALL") != string::npos ||
      subsys.find("dc") != string::npos || subsys.find("Dc") != string::npos || subsys.find("DC") != string::npos ) {
    int lab= 1;
    clog<<"UFDatum::datum> DC, lab: "<<lab<<" ..."<<endl;
    UFDCDatum::datum(timeout, lab, rec, car); ++cnt;
  }
  else {
    clog<<"UFDatum::datum> DC NOT!..."<<endl;
  }

  delete parms;

  return cnt;
}

// non static, non virtual funcs:
// protected ctor helper:
void UFDatum::_create() {
  UFPVAttr* pva= 0;
  string pvname = _name + ".subsys";
  pva = (*this)[pvname] = new UFPVAttr(pvname); // any non null value should indicate datum action 
  attachInOutPV(this, pva);

  registerRec();
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
}

/// virtual funcs:
/// perform datum mechanisms with non-null step cnts or named-position inputs
int UFDatum::genExec(int timeout) {
  size_t pvcnt = size();

  if( pvcnt == 0 ) {
    clog<<"UFDatum::genExec> np pvs?"<<endl;
    return (int) pvcnt;
  }

  string subsys, pvname = _name + ".subsys";
  UFPVAttr* pva = (*this)[pvname];
  if( pva != 0 ) {
    string input = pva->valString(); // input is anything non-null
    if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
      clog<<"UFDatum::genExec> subsys: "<<input<<endl;
      subsys = input;
      pva->clearVal(); // clear
    }
  }
  UFDatum::Parm* p = new UFDatum::Parm(subsys);
  int stat = datum(p, timeout, this, _car);  // perform requested datum

  if( stat < 0 ) { 
    clog<<"UFDatum::genExec> failed to start datum pthread..."<<endl;
    return stat;
  }

  return stat;
} // genExec

int UFDatum::validate(UFPVAttr* pva) {
  return 0;
} // validate


#endif // __UFDATUM_CC__

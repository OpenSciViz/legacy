#if !defined(__UFEnvSIR_h__)
#define __UFEnvSIR_h__ "$Name:  $ $Id: UFEnvSIR.h,v 0.4 2005/09/26 18:52:41 hon Exp $"
#define __UFEnvSIR_H__(arg) const char arg##EnvSIR_h__rcsId[] = __UFEnvSIR_h__;

#include "UFSIR.h"
#include "vector"

/// in principle a single sir class instance can accommodate all of the status
/// pvs for an IS, and ditto for the device layer sirs. but it is cleaner
/// parcel out the SAD into small pieces. This specialization of the SIR
/// should override the genExec to deal with the additional inputs defined
/// by the ctor vector args below:
class UFEnvSIR : public UFSIR {
public:
  // all ctors rely on base classs funcs:
  inline UFEnvSIR(const string& recname= "instrum:sad:whatever") : UFSIR(recname) {}
  inline UFEnvSIR(const string& recname, int val) : UFSIR(recname, val) {}
  inline UFEnvSIR(const string& recname, double val) : UFSIR(recname, val) {}
  inline UFEnvSIR(const string& recname, const string& sval) : UFSIR(recname, sval) {}
  inline UFEnvSIR(const string& recname, const string& sval,
		  const vector< UFPVAttr* >& inputs,
	          vector< UFPVAttr* >& outputs) : UFSIR(recname, sval, inputs, outputs) {}
  inline UFEnvSIR(const string& recname, int val,
		  const vector< UFPVAttr* >& inputs,
	          vector< UFPVAttr* >& outputs) : UFSIR(recname, val, inputs, outputs) {}
  inline virtual ~UFEnvSIR() {}

  virtual int genExec(int timeout= 0);

  // environment sir only
  inline UFPVAttr* barcd() { string p= _name+".barcd"; if( find(p) != end() ) return (*this)[p]; return 0; }
  inline UFPVAttr* lvdt() { string p= _name+".lvdt"; if( find(p) != end() ) return (*this)[p]; return 0; }
  inline UFPVAttr* plc() { string p= _name+".plc"; if( find(p) != end() ) return (*this)[p]; return 0; }
  inline UFPVAttr* vacuum() { string p= _name+".vacuum"; if( find(p) != end() ) return (*this)[p]; return 0; }
  inline UFPVAttr* montemp() { string p= _name+".montemp"; if( find(p) != end() ) return (*this)[p]; return 0; }
  inline UFPVAttr* ctrltemp() { string p= _name+".ctrltemp"; if( find(p) != end() ) return (*this)[p]; return 0; }

  static UFEnvSIR* create(const string& instrum);

protected:
  // use base class _create
};

#endif // __UFEnvSIR_h__

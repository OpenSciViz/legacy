#if !defined(__UFAPPLY_CC__)
#define __UFAPPLY_CC__ "$Name:  $ $Id: UFApply.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFAPPLY_CC__;
  
#include "UFApply.h"
#include "UFPVAttr.h"

UFApply* UFApply::_sys= 0; // global systel top-level apply

// ctor:

// static class funcs:

// non static funcs:

// virtuals 
void UFApply::setBusy(size_t timeout) {
  _timeout = timeout;
  if( _car ) { // force car to process in/out
    _car->setIVAL(UFGem::_Busy); 
    _car->genExec(1); // should set car (output) busy
  }
}

void UFApply::setIdle() {
  _timeout = 0;
  if( _car) { // force car to process in/out
    _car->setIVAL(UFGem::_Idle);
    _car->genExec(1); // should set car (output) idle
  }
}

// override to porcess linked CADs
int UFApply::directive(int d) {
  // the finish it -- since setBusy is called above, setIdle here if not start?
  switch( d ) {
  case _Clear: genExec(); clear(); setIdle(); //clog<<"UFApply::directive> clear: "<<name()<<endl; 
     break;
  case _Preset: genExec(); preset(); setIdle(); //clog<<"UFApply::directive> preset: "<<name()<<endl;
    break;
  case _Start: start(); //clog<<"UFApply::directive> start: "<<name()<<endl;
    break;
  case _Stop: genExec(); stop(); setIdle(); //clog<<"UFApply::directive> stop: "<<name()<<endl;
    break;
  case _Mark:
  default: mark(); genExec(); setIdle(); //clog<<"UFApply::directive> mark: "<<name()<<endl;
    break;
  }

  return d;
}

// (protected) helper  funcs:
void UFApply:: _create(UFCAR* car) {
  _type = UFGem::_Apply;
  _mark = true;
  _car = car;
  string recname = _name; 
  string pvname = recname + ".DIR";

  // input to apply is "dir"
  UFPVAttr* pva = (*this)[pvname] = new UFPVAttr(pvname); // int enum
  attachInputPV(pva);

  // client id and message input
  pvname = recname + ".CLID";
  pva = (*this)[pvname] = new UFPVAttr(pvname);
  attachInputPV(pva);

  pvname = recname + ".MESS";
  pva = (*this)[pvname] = new UFPVAttr(pvname);
  attachInputPV(pva);

  pvname = recname + ".VAL";
  pva = new (nothrow) UFPVAttr(pvname, (int)0); // int outval
  attachOutputPV(pva);

  registerRec();
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }

  if( _sys == 0 ) _sys = this;
}

int UFApply::priorityUnMark(const string& recname) {
  int nr= 0;
  if( recname.find("abort") == string::npos &&
      recname.find("datum") == string::npos &&
      recname.find("init") == string::npos &&
      recname.find("reboot") == string::npos &&
      recname.find("stop") == string::npos )
    return nr;

  // if this is an abort or datum or init or stop or reboot unmark all other cad records
  int nrl = _reclinks.size(); // list of other records linked to this one
  //if( _verbose )
  //clog<<"UFApply::priorityUnMark> "<<_name<<", links: "<<nrl<<endl;
  for( int i = 0; i < nrl; ++i ) {
    UFGem* rec = _reclinks[i];
    if( rec == 0 ) continue;
    string name = rec->name();
    if( rec->marked() && name != recname ) {
      clog<<"UFApply::priorityUnMark> "<<recname<<" overriding: "<<name<<endl;
      rec->unmark();
      ++nr;
    }
  }
  return nr;
}
// virtuals 
int UFApply::genExec(int timeout) {
  UFPVAttr* directive = pdir();
  int cmd = (int)directive->getNumeric();
  //setBusy(timeout); // should be setBusy via start func.
  // just clear all attached/linked records:
  int nrl = _reclinks.size(); // list of other records linked to this one
  //if( _verbose )
  clog<<"UFApply::genExec> "<<_name<<", directive: "<<cmd<<", links: "<<nrl<<endl;
  int val= 0, markcnt= 0;
  UFGem* rec= 0;
  for( int i = 0; i < nrl; ++i ) {
    rec = _reclinks[i];
    if( rec == 0 ) continue;
    string recname = rec->name();
    //if( _verbose )
    //clog<<"UFApply::genExec> check attached record: "<<recname<<", directive: "<<cmd<<endl;
    if( cmd < 0 ) continue;
    switch( cmd ) { 
    case _Mark: rec->mark(); break;
    case _Clear: rec->clear(); rec->setIdle();
      clog<<"UFApply::genExec> cleared/setIdle attached record: "<<recname<<endl;
      break;
    case _Preset: rec->preset(); break;
    case _Start: // start default
    default: 
      if( rec->marked() ) {
        clog<<"UFApply::genExec> processing attached record: "<<recname<<cmd<<endl;
	priorityUnMark(recname); // check if other records must be unmaked
        val = rec->start();
        if( val < 0 ) {
	  string msg= rec->errMsg(val); rec->setErr(msg,val);
	}
        else {
	  ++markcnt;
	}
      }
      break;
    }
    if( val < 0 ) break; // stop on first error
    if( markcnt > 0 ) break; // stop after processing first marked record?  
  }
  if( cmd > _Preset && markcnt <= 0 ) {
    clog<<"UFApply::genExec> No attached CAD record where marked for processing..."<<endl;
  }
  else if( cmd > _Preset && rec != 0 ) {
    clog<<"UFApply::genExec> attached CAD record is processing: "<<rec->name()<<endl;
  }
    
  setPV(val);
  if( val < 0 ) {
    string msg= errMsg(val);
    setErr(msg, val);
  }
  else if( cmd == _Clear || markcnt <= 0 || rec == 0 ) { // || timeout <= 0 ) { 
    // clear if no cads where marked
    // if there is a timeout, the heartbeat thread should setIdle...
    //clear();
    setIdle();
  }

  return val;
}

#endif // __UFAPPLY_CC__

#if !defined(__UFECTEST_h__)
#define __UFECTEST_h__ "$Name:  $ $Id: UFECTest.h,v 0.6 2006/08/29 20:16:49 hon Exp $"
#define __UFECTEST_H__(arg) const char arg##ECTest_h__rcsId[] = __UFECTEST_h__;

#include "UFCAD.h"
#include "UFECSetup.h"

class UFECTest : public UFCAD {
public:
  inline UFECTest(const string& instrum= "instrum") : UFCAD(instrum+":ec:test") { _create(); }
  inline virtual ~UFECTest() {}

  virtual int validate(UFPVAttr* pva= 0);
  virtual int genExec(int val= 0);

  // thread that performs portescap agent connection test
  static void* thrdFunc(void* p);

  // start connection thread
  static int newThrd(UFECSetup::EnvParm* parms, int timeout, UFGem* rec= 0, UFCAR* car= 0);

  // start connection thread
  inline static int statAll(UFECSetup::EnvParm* parms, int timeout, UFGem* rec= 0, UFCAR* car= 0)
    { return newThrd(parms, timeout, rec, car); }

protected:
  // ctor helper
  void _create();
};

#endif//  __UFECTEST_h__

#if !defined(__UFDCMCECMD_CC__)
#define __UFDCMCECMD_CC__ "$Name:  $ $Id: UFDCMCECmd.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFDCMCECMD_CC__;

#include "UFDCMCECmd.h"
#include "UFDCInit.h" // reconnect?
#include "UFPVAttr.h"
#include "UFPosixRuntime.h"
#include "UFClientSocket.h"
#include "UFStrings.h"
#include "UFCAServ.h"

// globals can be set by obssetup, setdhs, and or observe cads
int UFDCMCECmd::_index= 1;
int UFDCMCECmd::_wcpix1= 1023;
int UFDCMCECmd::_wcpix2= 1023;
int UFDCMCECmd::_expcnt= 1;
int UFDCMCECmd::_ds9= 0;
float UFDCMCECmd::_exptime= 1;
string UFDCMCECmd::_dhslabel;
string UFDCMCECmd::_dhsqlook;
string UFDCMCECmd::_nfshost;
string UFDCMCECmd::_datadir;
string UFDCMCECmd::_fitsfile;
string UFDCMCECmd::_lutfile;
string UFDCMCECmd::_pngjpegfile;
string UFDCMCECmd::_datamode;
string UFDCMCECmd::_usrinfo;
string UFDCMCECmd::_comment;

// ctors:

// helper
int UFDCMCECmd::obsConfCmds(deque< string >& obscmds) {
  string cmd = "OBSCONF", cmdimpl = "AcqStart";
  obscmds.push_back(cmd); obscmds.push_back(cmdimpl);

  cmd = "DATALABEL"; cmdimpl = _dhslabel;
  obscmds.push_back(cmd); obscmds.push_back(cmdimpl);

  cmd = "DHSQLOOK"; cmdimpl = _dhsqlook;
  obscmds.push_back(cmd); obscmds.push_back(cmdimpl);

  cmd = "DATAMODE"; cmdimpl = _datamode;
  obscmds.push_back(cmd); obscmds.push_back(cmdimpl);

  cmd = "USRINFO"; cmdimpl = _usrinfo;
  obscmds.push_back(cmd); obscmds.push_back(cmdimpl);

  cmd = "COMMENT"; cmdimpl = _comment;
  obscmds.push_back(cmd); obscmds.push_back(cmdimpl);

  cmd = "FILENAME"; cmdimpl = _fitsfile;
  obscmds.push_back(cmd); obscmds.push_back(cmdimpl);

  cmd = "LUT"; cmdimpl = _lutfile;
  obscmds.push_back(cmd); obscmds.push_back(cmdimpl);

  if( _pngjpegfile.find(".png") != string::npos || _pngjpegfile.find(".PNG") != string::npos ) {
    cmd = "PNG"; cmdimpl = _pngjpegfile;
    obscmds.push_back(cmd); obscmds.push_back(cmdimpl);
  }
  else if( _pngjpegfile.find(".jpg") != string::npos || _pngjpegfile.find(".JPG") != string::npos ||
	   _pngjpegfile.find(".jpeg") != string::npos || _pngjpegfile.find(".JPEG") != string::npos ) {
    cmd = "JPG"; cmdimpl = _pngjpegfile;
    obscmds.push_back(cmd); obscmds.push_back(cmdimpl);
  }

  cmd = "DATADIR"; cmdimpl = _datadir;
  obscmds.push_back(cmd); obscmds.push_back(cmdimpl);

  cmd = "NFSHOST"; cmdimpl = _nfshost;
  obscmds.push_back(cmd); obscmds.push_back(cmdimpl);

  cmd= "INDEX"; cmdimpl = UFRuntime::numToStr(_index);
  obscmds.push_back(cmd); obscmds.push_back(cmdimpl);

  cmd= "DS9"; cmdimpl = UFRuntime::numToStr(_ds9);
  obscmds.push_back(cmd); obscmds.push_back(cmdimpl);

  cmd = "WCPIX1"; cmdimpl = UFRuntime::numToStr(_wcpix1);
  obscmds.push_back(cmd); obscmds.push_back(cmdimpl);

  cmd = "WCPIX2"; cmdimpl = UFRuntime::numToStr(_wcpix2);
  obscmds.push_back(cmd); obscmds.push_back(cmdimpl);

  cmd = "EXPTIME"; cmdimpl = UFRuntime::numToStr(_exptime);
  obscmds.push_back(cmd); obscmds.push_back(cmdimpl);

  cmd = "EXPCNT"; cmdimpl = UFRuntime::numToStr(_expcnt);
  obscmds.push_back(cmd); obscmds.push_back(cmdimpl);

  return (int) obscmds.size();
}


// static class funcs:
// for use by IS observation thread:
int UFDCMCECmd::submitAcq(const string& opt, const string& datalabel, int index, int timeout, UFGem* rec, UFCAR* car) {
  // for use by IS: opt == start, stop, abort
  // opt == start, stop, abort
  if( opt.find("sta") == string::npos && opt.find("Sta") == string::npos && opt.find("STA") == string::npos &&
      opt.find("sto") == string::npos && opt.find("Sto") == string::npos && opt.find("STO") == string::npos &&
      opt.find("abo") == string::npos && opt.find("Abo") == string::npos && opt.find("ABO") == string::npos )
    return -1;

  string name = "anon";
  if( rec )
    name = rec->name(); 

  string carname = UFCAServ::_instrum + ":dc:mcecmdC";
  if( rec ) {
    UFCAR* rcar = rec->hasCAR();
    if( rcar )
      carname = rcar->name();
  }
  else if( car ) {
    carname = car->name();
  }
  carname += ".IVAL";

  if( _sim ) {
    return timeout;
  }

  // test socket to be sure non-blocking connection is functional/selectable
  // (connection has completed succesfully)
  string errmsg, cmd, cmdimpl;
  int err;
  bool validsoc = connected(UFDCSetup::_connection);
  if( !validsoc ) {
    if( UFDCSetup::_connection == 0 ) {
      clog<<"UFDCMCECmd::submitACQ> never connected to UFMCE agent? DC subsystem not Initiliazed!"<<endl;
      // and free the arg & parms
      return -1;
    }
    clog<<"UFDCMCECmd::submitACQ> lost connection to UFMCE agent? trying re-init/reconnect...!"<<endl;
    int fdsoc = UFDCInit::reconnect(name, errmsg, err);
    if( fdsoc < 0 ) {
      clog<<"UFDCMCECmd::submitACQ> re-connect to UFMCE agent failed!"<<endl;
      return -2;
    }
  }  

  deque< string > obsacqcmds;
  cmd = "CAR";
  cmdimpl = carname;
  obsacqcmds.push_back(cmd); obsacqcmds.push_back(cmdimpl);
  //if( _verbose )
    clog<<"UFDCMCECmd::submitACQ> cmd: "<<cmd<<", cmdimpl: "<<cmdimpl<<endl;

  cmd = "SAD";
  cmdimpl = UFCAServ::_instrum + ":sad:MCEReply.INP";
  //obscmds.push_back(cmd); obscmds.push_back(cmdimpl);
  obsacqcmds.push_back(cmd); obsacqcmds.push_back(cmdimpl);
  //if( _verbose )
    clog<<"UFDCMCECmd::submitACQ> cmd: "<<cmd<<", cmdimpl: "<<cmdimpl<<endl;

  // first insure obsconfig/obssetup has been completed by (re)sending 
  if( !datalabel.empty() ) _dhslabel = datalabel; // allow reset of datalabel?
  if( _dhsqlook.empty() ) _dhsqlook = "All"; // allow reset of datalabel?

  obsConfCmds(obsacqcmds); // append deque

  cmd = "Acq "; cmd += "Acq!!ErrMCEAcq ";
  cmdimpl = opt;
  obsacqcmds.push_back(cmd); obsacqcmds.push_back(cmdimpl);
  //if( _verbose )
    clog<<"UFDCMCECmd::submitACQ> cmd: "<<cmd<<", cmdimpl: "<<cmdimpl<<endl;

  UFStrings obsacq(name, obsacqcmds);
  int ns = UFDCSetup::_connection->send(obsacq);
  if( ns <= 0 ) {
    clog<<"UFDCMCECmd::submitACQ> failed obsacq send req: "<< obsacq.name()<< " "<<obsacq.timeStamp()<<endl;
    return ns;
  }
  else {
    //if( _verbose )
    clog<<"UFDCMCECmd::submitACQ> sent obsacq: "<<obsacq.name()<<" "<<obsacq.timeStamp()<<", car: "<<obsacqcmds[1]<<endl;
    //int nr = UFDCMCECmd::_connection->recv(req);
  }

  return timeout;
}

void* UFDCMCECmd::submitMCE4(void* p) {
  UFDCMCECmd::MCEThrdArg* arg = (UFDCMCECmd::MCEThrdArg*) p;
  string rawcmd = arg->_rawcmd;
  int eoftimer = arg->_EOFTimer;
  UFCAR* car = arg->_car;
  UFGem* rec = arg->_rec; // should be either a cad or apply
  string name = "anon";
  if( rec )
    name = rec->name(); 

  string carname = UFCAServ::_instrum + ":dc:mcecmdC";
  if( rec ) {
    UFCAR* rcar = rec->hasCAR();
    if( rcar )
      carname = rcar->name();
  }
  else if( car ) {
    carname = car->name();
  }
  carname += ".IVAL";

  delete arg;

  int err= 0;
  string errmsg;
  deque< string > cmdvec;
  if( _sim ) { // simple simulation...
    clog<<"UFDCMCECmd::submitMCE4> (sim) rawcmd: "<<rawcmd<<endl;
    // set cad/car idle after 1/2:
    UFPosixRuntime::sleep(0.5);
    if( car ) {
      if( err != 0 )
        car->setErr(errmsg);
      else
        car->setIdle();
    }
    if( rec ) {
      UFCAR* c = rec->hasCAR();
      if( c != 0 && c != car ) {
        if( err != 0 )
  	  c->setErr(errmsg);	
        else
          c->setIdle();
      }
    }
    // exit daemon thread
    return p;
  }
  // test socket to be sure non-blocking connection is functional/selectable
  // (connection has completed succesfully)
  bool validsoc = connected(UFDCSetup::_connection);
  if( !validsoc ) {
    if( UFDCSetup::_connection != 0 ) {
      clog<<"UFDCMCECmd::submitMCE4> never connected to UFMCE agent? DC subsystem not Initiliazed!"<<endl;
      // and free the arg & parms
      return p;
    }
    clog<<"UFDCMCECmd::submitMCE4> lost connection to UFMCE agent? trying re-init/reconnect...!"<<endl;
    int fdsoc = UFDCInit::reconnect(name, errmsg, err);
    if( fdsoc < 0 ) {
      clog<<"UFDCMCECmd::submitMCE4> re-connect to UFMCE agent failed!"<<endl;
      return p;
    }
  }  

  string cmd = "CAR";
  string cmdimpl = carname;
  cmdvec.push_back(cmd); cmdvec.push_back(cmdimpl);
  //if( _verbose )
    clog<<"UFDCMCECmd::submitMCE4> cmd: "<<cmd<<", cmdimpl: "<<cmdimpl<<endl;

  cmd = "SAD";
  cmdimpl = UFCAServ::_instrum + ":sad:MCEReply.INP";
  cmdvec.push_back(cmd); cmdvec.push_back(cmdimpl);
  //if( _verbose )
    clog<<"UFDCMCECmd::submitMCE4> cmd: "<<cmd<<", cmdimpl: "<<cmdimpl<<endl;

  cmd = "Raw "; cmd += "UserCmd!!ErrMCECmd ";
  // check if timer EOF cmd should be sent after approp. delay..
  if( eoftimer > 0 &&
	(rawcmd.find("start") != string::npos || rawcmd.find("Start") != string::npos ||
	 rawcmd.find("START") != string::npos || rawcmd.find("run") != string::npos ||
	 rawcmd.find("Run") != string::npos || rawcmd.find("RUN") != string::npos ) ) {
    cmdimpl = "LDVAR 0 "; cmdimpl += UFRuntime::numToStr(eoftimer);
    cmdvec.push_back(cmd); cmdvec.push_back(cmdimpl);
    //if( _verbose )
    clog<<"UFDCMCECmd::submitMCE4> cmd: "<<cmd<<", cmdimpl: "<<cmdimpl<<endl;
  }
  cmdimpl = rawcmd;
  cmdvec.push_back(cmd); cmdvec.push_back(cmdimpl);
  //if( _verbose )
    clog<<"UFDCMCECmd::submitMCE4> cmd: "<<cmd<<", cmdimpl: "<<cmdimpl<<endl;

  UFStrings req(name, cmdvec);
  int ns = UFDCSetup::_connection->send(req);
  if( ns <= 0 ) {
    clog<<"UFDCMCECmd::submitMCE4> failed send req: "<< req.name() << " " << req.timeStamp() <<endl;
    err = 1;
    errmsg = "Failed to submit MCE4 RawCmd";
  }
  else {
    //if( _verbose )
    clog<<"UFDCMCECmd::submitMCE4> sent req: "<< req.name() << " " << req.timeStamp() <<", cmd: " <<cmdvec[2]<<endl;
    //int nr = UFDCMCECmd::_connection->recv(req);
  }
  if( eoftimer > 0 ) {
    cmdvec.clear();
    cmd = "Raw "; cmd += "UserCmd!!ErrMCECmd ";
    cmdimpl = "P 1200034 28";
    cmdvec.push_back(cmd); cmdvec.push_back(cmdimpl);
    UFStrings reqEOF(name, cmdvec);
    UFPosixRuntime::sleep(eoftimer);
    int ns = UFDCSetup::_connection->send(reqEOF);
    if( ns <= 0 ) {
    clog<<"UFDCMCECmd::submitMCE4> failed send reqEOF: "<< reqEOF.name() << " " << reqEOF.timeStamp() <<endl;
    err = 1;
    errmsg = "Failed to submit MCE4 RawCmd";
    }
    else {
    //if( _verbose )
    clog<<"UFDCMCECmd::submitMCE4> sent reqEOF: "<< reqEOF.name() << " " << reqEOF.timeStamp() <<", cmd: " <<cmdvec[2]<<endl;
    //int nr = UFDCMCECmd::_connection->recv(reqEOF);
    }
  }

  return p;
} // submitMCE4

/// general mce4 config func. (non datum)
int UFDCMCECmd::startThread(const string& rawcmd, int eoftimer, int timeout, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec && car ) {
    UFCAR* c = rec->hasCAR();
    if( car != c ) car->setBusy(timeout);
  }
  // create and run pthread to configure mce4 and then either reset car to idle or error
  // thread should free DetParm* allocations...
  UFDCMCECmd::MCEThrdArg* arg = new UFDCMCECmd::MCEThrdArg(rawcmd, eoftimer, rec, car);
  clog<<"UFDCMCECmd::startThread> start new MCE4 raw cmd thread for: "<<rawcmd<<endl;
  pthread_t thrid = UFPosixRuntime::newThread(UFDCMCECmd::submitMCE4, arg);

  return (int) thrid;
}

// non static, non virtual funcs:
// protected ctor helper:
void UFDCMCECmd::_create(const string& host, int port) {
  setHost(host); setPort(port); // set global statics
  UFPVAttr* pva= 0;

  string pvname = _name + ".Action"; // (raw) MCE4 action command
  pva = (*this)[pvname] = new UFPVAttr(pvname, "null");
  attachInOutPV(this, pva);

  pvname = _name + ".Query"; // (raw) MCE4 query command
  pva = (*this)[pvname] = new UFPVAttr(pvname, "null");
  attachInOutPV(this, pva);

  // force EndOfFrame on timer "LDVAR 0 A" and "START" and sleep specified millisec. then "P 1200034 28" 
  pvname = _name + ".EOFMilliSecTimer"; 
  pva = (*this)[pvname] = new UFPVAttr(pvname, INT_MIN);
  attachInOutPV(this, pva);

  registerRec();
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
}

/// virtual funcs:
int UFDCMCECmd::validate(UFPVAttr* pva) {
  if( pva == 0 )
    return 0;

  string input = pva->valString();
  char* cs = (char*) input.c_str();
  while( *cs == ' ' && *cs == '\t' )
    ++cs;

  if( isdigit(cs[0]) )
    return 0;

  return -1;
}

/// perform setup motion for those mechanisms with non-null step cnts or named-position inputs
int UFDCMCECmd::genExec(int timeout) {
  size_t pvcnt = size();

  if( pvcnt == 0 ) {
    clog<<"UFDCMCECmd::genExec> np pvs?"<<endl;
    return (int) pvcnt;
  }

  // which pvs are non null?
  string rawcmd, input, query, action, pvname = _name + ".Action";
  int eoftimer= -1;
  UFPVAttr* pva = (*this)[pvname];
  if( pva != 0 ) {
    input = action = pva->valString(); // input is anything non-null
    if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
      clog<<"UFDCMCECmd::genExec> action: "<<action<<endl;
      rawcmd = action;
      pva->clearVal(); // clear
    }
  }

  pvname = _name + ".Query";
  pva = (*this)[pvname];
  if( pva != 0 ) {
    input = query = pva->valString(); // input is anything non-null
    if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
      clog<<"UFDCMCECmd::genExec> action: "<<query<<endl;
      rawcmd = query;
      pva->clearVal(); // clear
    }
  }

  pvname = _name + ".EOFMilliSecTimer"; 
  pva = (*this)[pvname];
  if( pva != 0 ) {
    input = pva->valString(); // input is anything non-null
    eoftimer = (int)pva->getNumeric(); // input is anything non-null
    if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
      clog<<"UFDCMCECmd::genExec> EOFTimer: "<<eoftimer<<endl;
      pva->clearVal(); // clear
    }
  }

  if( !rawcmd.empty() ) {
    int stat= startThread(rawcmd, eoftimer, timeout, this, _car);
    if( stat < 0 )
      clog<<"UFDCMCECmd::genExec> failed to start mce4 rawcmd pthread..."<<endl;
    return stat;
  }
  return 0;
} // genExec

#endif // __UFDCMCECMD_CC__

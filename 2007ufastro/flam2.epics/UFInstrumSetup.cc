#if !defined(__UFINSTRUMSETUP_CC__)
#define __UFINSTRUMSETUP___ "$Name:  $ $Id: UFInstrumSetup.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFINSTRUMSETUP___;

#include "UFInstrumSetup.h"
#include "UFCAServ.h"
#include "UFPVAttr.h"
#include "UFCCSetup.h"
#include "UFDCSetup.h"
#include "UFECSetup.h"
#include "UFPVAttr.h"
#include "UFRuntime.h"
#include "UFClientSocket.h"
//#include "ufflam2mech.h"

#include "algorithm" // find operator

UFClientSocket* UFInstrumSetup::_connection; // to instrument executive agent
string UFInstrumSetup::_BiasMode;
string UFInstrumSetup::_BeamMode;
string UFInstrumSetup::_host;
int UFInstrumSetup::_ccport= 52024; // portescaps agent 52024
int UFInstrumSetup::_ecport= 52003; // lakeshoew 332 agent
map< string, deque< string >* > UFInstrumSetup::_allowed; // named configurations

// ctors:
// virtual: 
// override CAD::setBusy to NOT preprocess record input->outputs 
// the genExec should do this after checking defaults and input overrides
void UFInstrumSetup::setBusy(size_t timeout, UFGem* proxy) {
  if( _timeout > 0 ) {
    //if( _verbose )
      clog<<"UFInstrumSetup::setBusy> _timeout: "<<_timeout<<" already busy..."<<endl;
    return;
  }
  _timeout = timeout; 
  if( _verbose ) {
    clog<<"UFInstrumSetup::setBusy> "<<_name<<", busy until completion or timeout in heartbeat ticks: "<<timeout<<endl;
  }

  if( _car ) {
    _car->setBusy(timeout, this);
    if( proxy != 0 && proxy != this )
      _car->setBusy(timeout, proxy);
  }
}

// static class funcs:
bool UFInstrumSetup::allowable(const string& pvname, const string& val) {
  deque< string >* list = _allowed[pvname];
  if( list == 0 )
    return true; // anything goes?

  if( std::find(list->begin(), list->end(), val) != list->end() )
    return true;

  // also try case insensitive and substrings?
  string aval = val;
  for( size_t i = 0; i < list->size(); ++i ) {
    string name = (*list)[i];
    if( name.find(val) != string::npos || aval.find(name) != string::npos )
      return true; // substring
    UFRuntime::upperCase(name); UFRuntime::upperCase(aval);
    if( name.find(aval) != string::npos || aval.find(name) != string::npos )
      return true; // uppercase substring
    UFRuntime::lowerCase(name); UFRuntime::lowerCase(aval);
    if( name.find(aval) != string::npos || aval.find(name) != string::npos )
      return true; // lowercase substring
  }

  clog<<"UFInstrumSetup::allowable> ? val: "<<val<<", not in allowed list for: "<<pvname<<endl;
  return false;
}

bool UFInstrumSetup::allowable(const string& pvname, double val) {
  if( val < 300 )
    return true;

  return false;
}

UFInstrumSetup::Parm* UFInstrumSetup::allocParm(const map< string, string >& cconf,
						const map< string, double >& ecsetp) {
  UFInstrumSetup::Parm* p = new UFInstrumSetup::Parm; // all null subsys ptrs....
  // allocate and set UFECSetup::EnvParm* p->_eparms
  clog<<"UFInstrumSetup::allocParm> allocated/preset set point confs: "<<ecsetp.size()<<endl;
  p->_eparms = new UFECSetup::EnvParm;
  map< string, double >::const_iterator itec = ecsetp.begin();
  while( itec != ecsetp.end() ) {
    string thermname = itec->first;
    double setp = itec->second;
    clog<<"UFInstrumSetup::allocParm> allocated/preset: "<<thermname<<" set point: "<<setp<<endl;
    if( thermname.find("MOS") != string::npos )
      p->_eparms->_MOSKelv = setp;
    else if( thermname.find("A") != string::npos ) 
      p->_eparms->_CamAKelv = setp;
    else
      p->_eparms->_CamBKelv = setp; // according to jeff this is the detector and likely the only controlled element
    ++itec;
  }

  // allocate and set map< string, UFCCSetup::MotParm* >* p->_mparms;
  clog<<"UFInstrumSetup::allocParm> allocated/preset ccmech confs: "<<cconf.size()<<endl;
  p->_mparms = new map< string, UFCCSetup::MotParm* >;
  map< string, string >::const_iterator itcc = cconf.begin();
  for( ; itcc != cconf.end(); ++itcc ) {
    string mechname = itcc->first;
    string posname = itcc->second;
    if( UFCCSetup::_indexorNames.find(mechname) == UFCCSetup::_indexorNames.end() )
      continue; // not a mechanism name!
    string indexor = UFCCSetup::_indexorNames[mechname]; // indexor
    int pos = (int) UFCCSetup::posSteps(mechname, posname);
    clog<<"UFInstrumSetup::allocParm> "<<mechname<<", indexor: "<<indexor<<" position: "<<posname<<", possteps: "<<pos<<endl;
    bool firmware= false; // do not set indexor firmware motion parameters (acc, dec, runc, etc), use current settings
    int step= 0, blash= 0; // insure possteps rather than relative steps are indicated in parm ctor....
    UFCCSetup::MotParm* mp = new UFCCSetup::MotParm(firmware, indexor, posname, step, blash, pos); 
    (*p->_mparms)[mechname] = mp;
  }
 
  return p;
}

void* UFInstrumSetup::simAll(void* p) {
  // instrumsetup really just needs ?
  UFInstrumSetup::ISThrdArg* arg = (UFInstrumSetup::ISThrdArg*) p;
  UFInstrumSetup::Parm* parms = arg->_parms;
  UFCAR* car = arg->_car;
  UFGem* rec = arg->_rec; // should be either a cad or apply
  string name = "anon";
  if( rec )
    name = rec->name();

  // simple simulation...
  clog<<"UFInstrumSetup::simAll>... "<<endl;
  if( parms ) {
    if( parms->_mparms )
      clog<<"UFInstrumsetup::simAll> motors: "<<parms->_mparms->size()<<endl;
    if( parms->_eparms )
      clog<<"UFInstrumsetup::simAll> environment: "<<parms->_eparms->_MOSKelv
	  <<", "<<parms->_eparms->_CamAKelv<<", "<<parms->_eparms->_CamBKelv<<endl;
  }

  // free arg
  delete arg;

  //UFSIR::setPrep(true);
  // set car idle after 1/2 sec:
  UFPosixRuntime::sleep(0.5);
  if( car )
    car->setIdle();
  if( rec ) {
    UFCAR* c = rec->hasCAR();
    if(c != 0 && c != car ) c->setIdle();
  }
  
  //UFSIR::setPrep(false);
  // exit daemon thread
  return p;
}

/// general purpose instrumsetup func. 

/// arg:
int UFInstrumSetup::config(UFInstrumSetup::Parm* parms, int timeout, UFGem* rec, UFCAR* car) {
  if( _sim ) {
    // the rec's setBusy will also call its optional car's setBusy
    // while the optional supplementary car can be set busy here
    if( rec && car ) {
      UFCAR* c = rec->hasCAR();
      if( car != c ) car->setBusy(timeout);
    }
    clog<<"UFInstrumSetup::config> simulated timeout: "<<timeout<<endl;
    UFInstrumSetup::ISThrdArg* arg = new UFInstrumSetup::ISThrdArg(parms, rec, car);
    pthread_t thrid = UFPosixRuntime::newThread(UFInstrumSetup::simAll, arg);
    return (int) thrid;
  }

  // create and run pthread to instrumsetup whatever is currently being performed then either reset car to idle or error
  // thread should free the tParm* allocations...

  if( parms == 0 ) {
    clog<<"UFInstrumSetup::config> non-sim., but No parms!"<<endl;
    return 0;
  }

  int stat= 0;
  if( parms->_mparms ) {
    clog<<"UFInstrumSetup::config> invoking CCSetup::motion with CC parms."<<endl;
    // this should set CAR(s) to busy and start comotion thread to transact with agent, then free the _mparms alloc:
    stat = UFCCSetup::motion(parms->_mparms, timeout, rec, 0); // need another signature that does not use barcd car!
    if( stat < 0 ) {
      clog<<"UFInstrumSetup::config> CC setup thread problem."<<endl;
      return stat;
    }
  }
  else {
    clog<<"UFInstrumSetup::config> No CC parms."<<endl;
  }
  /*
  if( parms->_eparms ) {
    clog<<"UFInstrumSetup::config> EC parms."<<endl;
    // this shoulde free the _eparms alloc:
    stat = UFECSetup::environ(parms->_eparms, timeout, rec, car);
    if( stat < 0 ) {
      clog<<"UFInstrumSetup::config> EC setup thread problem."<<endl;
      return stat;
    }
  }
  else {
    clog<<"UFInstrumSetup::config> No EC parms."<<endl;
  }
  */
  return stat;
}

// non static, non virtual funcs:
// protected ctor helper:
void UFInstrumSetup::_create(const string& host, int ccport, int ecport) {
  UFPVAttr *pva= 0, *opva= 0;
  _host = host; _ccport = ccport, _ecport = ecport;
  if( _host == "" ) _host = UFRuntime::hostname();

  string aliasname, pvname = _name + ".BeamMode"; // "f/16", "MCAO_Over", "MCAO_Under"
  deque< string >* list = _allowed[pvname] = new deque< string >; //UFCCSetup::allocPosNamesList("Beam");
  //pva = (*this)[pvname] = new UFPVAttr(pvname, "f/16"); 
  pva = (*this)[pvname] = new UFPVAttr(pvname); 
  opva = attachInOutPV(this, pva); // and return outval pva
  // other allowed values:
  list->push_back("f/16"); list->push_back("MCAO_Over"); list->push_back("MCAO_Under");
  list->push_back("f/16"); list->push_back("MCAO_Over"); list->push_back("MCAO_Under");
  list->push_back("Open1"), list->push_back("Open2"), list->push_back("Hartmann1"), list->push_back("Hartmann2");  
  // aliases:
  aliasname = _name + ".A";
  _pvalias[aliasname] = pva;
  aliasname = _name + ".VALA";
  _pvalias[aliasname] = opva;
  aliasname = _name + ".OUTA";
  _pvalias[aliasname] = opva;

  pvname = _name + ".WheelBiasMode"; // "imaging", "long_slit", "MOS"
  //pva = (*this)[pvname] = new UFPVAttr(pvname, "imaging");
  pva = (*this)[pvname] = new UFPVAttr(pvname);
  opva = attachInOutPV(this, pva); // and return outval pva
  // allowed values:
  list = _allowed[pvname] = UFCCSetup::allocPosNamesList("Decker");
  list->push_back("imaging"); list->push_back("long_slit"); list->push_back("MOS");
  // aliases:
  aliasname = _name + ".B";
  _pvalias[aliasname] = pva;
  aliasname = _name + ".VALB";
  _pvalias[aliasname] = opva;
  aliasname = _name + ".OUTB";
  _pvalias[aliasname] = opva;

  pvname = _name + ".Filter"; // "open", "dark", "J-Low", "J", "H", "K", "JH", "HK"
  //pva = (*this)[pvname] = new UFPVAttr(pvname, "open");
  pva = (*this)[pvname] = new UFPVAttr(pvname);
  opva = attachInOutPV(this, pva);
  // allowed values:
  list = _allowed[pvname] = UFCCSetup::allocPosNamesList("Filter1", "Filter2");
  list->push_back("open"); list->push_back("dark"); list->push_back("J-Low"); list->push_back("J"); 
  list->push_back("H"); list->push_back("K"); list->push_back("JH"); list->push_back("HK"); 
  // aliases:
  aliasname = _name + ".F";
  _pvalias[aliasname] = pva;
  aliasname = _name + ".VALF";
  _pvalias[aliasname] = opva;
  aliasname = _name + ".OUTF";
  _pvalias[aliasname] = opva;

  //
  // all other inputs also include boolean override indicators:
  //
  pvname = _name + ".MOSSlit"; // "imaging", "circle1", "circle2",
  // "1pix-slit", "2pix-slit", "3pix-slit", "4pix-slit", "6pix-slit", "8pix-slit",
  // "MOS1", "MOS2", "MOS3", "MOS4", "MOS5", "MOS6", "MOS7", "MOS8", "MOS9"
  //pva = (*this)[pvname] = new UFPVAttr(pvname, "imaging");
  pva = (*this)[pvname] = new UFPVAttr(pvname);
  opva = attachInOutPV(this, pva);
  // allowed values:
  list = _allowed[pvname] = UFCCSetup::allocPosNamesList("MOS");
  list->push_back("imaging"); // synonomous? list->push_back("open");
  list->push_back("circle1"); list->push_back("circle2"); 
  list->push_back("1pix-slit"); list->push_back("2pix-slit"); list->push_back("3pix-slit"); 
  list->push_back("4pix-slit"); list->push_back("6pix-slit"); list->push_back("8pix-slit"); 
  list->push_back("MOS1"); list->push_back("MOS2"); list->push_back("MOS3"); 
  list->push_back("MOS4"); list->push_back("MOS5"); list->push_back("MOS6"); 
  list->push_back("MOS7"); list->push_back("MOS8"); list->push_back("MOS9"); 
  // aliases:
  aliasname = _name + ".E";
  _pvalias[aliasname] = pva;
  aliasname = _name + ".VALE";
  _pvalias[aliasname] = opva;
  aliasname = _name + ".OUTE";
  _pvalias[aliasname] = opva;

  /*
  pvname = _name + ".OverrideMOS";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 0); 
  opva = attachInOutPV(this, pva);
  _allowed[pvname] = list; // same as above
  // aliases:
  aliasname = _name + ".N";
  _pvalias[aliasname] = pva;
  aliasname = _name + ".VALN";
  _pvalias[aliasname] = opva;
  aliasname = _name + ".OUTN";
  _pvalias[aliasname] = opva;
  */

  // Overrides:
  pvname = _name + ".Decker"; // "imaging", "long_slit", "MOS"
  //pva = (*this)[pvname] = new UFPVAttr(pvname, "imaging"); 
  pva = (*this)[pvname] = new UFPVAttr(pvname); 
  opva = attachInOutPV(this, pva);
  list = _allowed[pvname] = UFCCSetup::allocPosNamesList("Decker");
  list->push_back("imaging"); list->push_back("long_slit"); list->push_back("MOS"); 
  // aliases:
  aliasname = _name + ".D";
  _pvalias[aliasname] = pva;
  aliasname = _name + ".VALD";
  _pvalias[aliasname] = opva;
  aliasname = _name + ".OUTD";
  _pvalias[aliasname] = opva;
  pvname = _name + ".OverrideDecker";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 0); 
  opva = attachInOutPV(this, pva);
  // aliases:
  aliasname = _name + ".J";
  _pvalias[aliasname] = pva;
  aliasname = _name + ".VALJ";
  _pvalias[aliasname] = opva;
  aliasname = _name + ".OUTJ";
  _pvalias[aliasname] = opva;


  pvname = _name + ".Lyot"; // "f/16", "MCAO_Over", "MCAO_Under", "H1", "H2"
  //pva = (*this)[pvname] = new UFPVAttr(pvname, "f/16"); 
  pva = (*this)[pvname] = new UFPVAttr(pvname); 
  opva = attachInOutPV(this, pva);
  list = _allowed[pvname] = UFCCSetup::allocPosNamesList("Lyot");
  list->push_back("f/16"); list->push_back("MCAO_Over"); list->push_back("MCAO_Under");
  list->push_back("H1"); list->push_back("H2"); 
  // aliases:
  aliasname = _name + ".G";
  _pvalias[aliasname] = pva;
  aliasname = _name + ".VALG";
  _pvalias[aliasname] = opva;
  aliasname = _name + ".OUTG";
  _pvalias[aliasname] = opva;
  pvname = _name + ".OverrideLyot";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 0); 
  opva = attachInOutPV(this, pva);
  // aliases:
  aliasname = _name + ".K";
  _pvalias[aliasname] = pva;
  aliasname = _name + ".VALK";
  _pvalias[aliasname] = opva;
  aliasname = _name + ".OUTK";
  _pvalias[aliasname] = opva;

  pvname = _name + ".Grism"; // "open", "JH", "HK", "JHK"
  //pva = (*this)[pvname] = new UFPVAttr(pvname, "open"); 
  pva = (*this)[pvname] = new UFPVAttr(pvname); 
  opva = attachInOutPV(this, pva);
  list = _allowed[pvname] = UFCCSetup::allocPosNamesList("Grism");
  list->push_back("open"); list->push_back("JH"); list->push_back("HK"); list->push_back("JHK");
  // aliases:
  aliasname = _name + ".H";
  _pvalias[aliasname] = pva;
  aliasname = _name + ".VALH";
  _pvalias[aliasname] = opva;
  aliasname = _name + ".OUTH";
  _pvalias[aliasname] = opva;
  pvname = _name + ".OverrideGrism";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 0); 
  opva = attachInOutPV(this, pva);
  // aliases:
  aliasname = _name + ".L";
  _pvalias[aliasname] = pva;
  aliasname = _name + ".VALL";
  _pvalias[aliasname] = opva;
  aliasname = _name + ".OUTL";
  _pvalias[aliasname] = opva;

  pvname = _name + ".DetPosFocus"; // "f/16", "MCAO" ("MCAO_over" and "MCAO_under" agrees better with Beam)
  //pva = (*this)[pvname] = new UFPVAttr(pvname, "f/16");
  pva = (*this)[pvname] = new UFPVAttr(pvname);
  opva = attachInOutPV(this, pva);
  list = _allowed[pvname] = UFCCSetup::allocPosNamesList("Focus");
  list->push_back("f/16"); list->push_back("MCAO_over"); list->push_back("MCAO_under");
  // aliases:
  aliasname = _name + ".I";
  _pvalias[aliasname] = pva;
  aliasname = _name + ".VALI";
  _pvalias[aliasname] = opva;
  aliasname = _name + ".OUTI";
  _pvalias[aliasname] = opva;
  pvname = _name + ".OverrideDetPos";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 0);
  opva = attachInOutPV(this, pva);
  // aliases:
  aliasname = _name + ".M";
  _pvalias[aliasname] = pva;
  aliasname = _name + ".VALM";
  _pvalias[aliasname] = opva;
  aliasname = _name + ".OUTM";
  _pvalias[aliasname] = opva;

  pvname = _name + ".WindowCover"; // "open" or "closed"
  //pva = (*this)[pvname] = new UFPVAttr(pvname, "open");
  pva = (*this)[pvname] = new UFPVAttr(pvname);
  opva = attachInOutPV(this, pva);
  list = _allowed[pvname] = UFCCSetup::allocPosNamesList("Window");
  list->push_back("open"); list->push_back("closed"); 
  // aliases:
  aliasname = _name + ".C";
  _pvalias[aliasname] = pva;
  aliasname = _name + ".VALC";
  _pvalias[aliasname] = opva;
  aliasname = _name + ".OUTC";
  _pvalias[aliasname] = opva;
  pvname = _name + ".OverrideWindowCover";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 0);
  opva = attachInOutPV(this, pva);
  // aliases:
  aliasname = _name + ".O";
  _pvalias[aliasname] = pva;
  aliasname = _name + ".VALO";
  _pvalias[aliasname] = opva;
  aliasname = _name + ".OUTO";
  _pvalias[aliasname] = opva;

  pvname = _name + ".MOSSetPoint";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 70.0);
  opva = attachInOutPV(this, pva);
  pvname = _name + ".OverrideMOSSetPoint";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 0);
  opva = attachInOutPV(this, pva);

  pvname = _name + ".CamSetPointA";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 70.0);
  opva = attachInOutPV(this, pva);
  pvname = _name + ".OverrideCamSetPointA";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 0);
  opva = attachInOutPV(this, pva);

  pvname = _name + ".CamSetPointB";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 70.0);
  opva = attachInOutPV(this, pva);
  pvname = _name + ".OverrideCamSetPointB";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 0);
  opva = attachInOutPV(this, pva);

  registerRec();
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
}

/// virtual funcs:
/// perform instrumsetup
int UFInstrumSetup::genExec(int timeout) {
  size_t pvcnt = size();
  if( pvcnt == 0 ) {
    clog<<"UFInstrumSetup::genExec> np pvs?"<<endl;
    return (int) pvcnt;
  }
  /* according to nick:
1) Selection of any flam:instrumentSetup.WheelBiasMode configuration
   should set flam:instrumentSetup.WindowCover = open.

2) We also need a flam:instrumentSetup.OverrideWindowCover field, which is
   not in the ICD.
  */
  int override= 1; // are now always true, i.e. require mandatory user input (June 1, 2006) // INT_MIN; // (cleared) overrides
  string input, pvname = _name + ".WheelBiasMode";
  UFPVAttr *opva= 0, *pva = (*this)[pvname];
  bool newBias= false, newBeam= false;
  map< string, string > conf; // all name/text values (overriden or not)
  map< string, double > setp; // set point overrides, if any

  if( pva != 0 ) {
    input  = pva->valString(); // input is anything non-null
    string wheelbias = input;
    clog<<"UFInstrumSetup::genExec> "<<pvname<<" == "<<input<<endl;
    if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
      string obssetup = UFCAServ::_instrum + ":obsSetup.WheelBiasMode";
      std::map<string, UFPVAttr*>::const_iterator it = UFPVAttr::_thePVList.find(obssetup);
      if( it != UFPVAttr::_thePVList.end() ) {
        UFPVAttr* obspva = it->second;
        clog<<"UFInstrumSetup::genExec> copy value to "<<obssetup<<" => "<<wheelbias<<endl;
        obspva->copyVal(pva);	
      }
      //if( pva->isSet() )
        copyInOut(pva);
      if( allowable(pvname, input) ) {
	newBias = true;
        conf["WheelBias"] = _BiasMode = wheelbias;
        conf["Window"] = "open";
      }
      else {
        clog<<"UFInstrumSetup::genExec> WheelBias value not allowed: "<<wheelbias<<endl;
      }
      //pva->clearVal(); // clear
    }
    else {
      clog<<"UFInstrumSetup::genExec> No WheelBias input value..."<<endl;
    }
  }
  else {
    clog<<"UFInstrumSetup::genExec> No WheelBias DBinput found?"<<endl;
  }

  string pvfilter = pvname = _name + ".Filter";
  pva = (*this)[pvname];
  if( pva != 0 ) {
    string filter = input = pva->valString(); // input is anything non-null
    clog<<"UFInstrumSetup::genExec> "<<pvname<<" == " <<filter<<endl;
    if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
      if( allowable(pvname, input) ) { 
	// need to select which filter wheel to move to "open" and which to to move to filter!
	bool f1= UFCCSetup::mechPosNameExists("Filter1", filter);
	bool f2= UFCCSetup::mechPosNameExists("Filter2", filter);
	if( f1 ) {
          conf["Filter2"] = "Open";
	  conf["Filter1"] = filter;
	}
	else if( f2 ) {
          conf["Filter1"] = "Open";
	  conf["Filter2"] = filter;
	}
	else {
          conf["Filter1"] = "Open";
	  conf["Filter2"] = "Open";
	}
        //if( pva->isSet() )
          copyInOut(pva);
      }
      else {
        clog<<"UFInstrumSetup::genExec> Filter value not allowed: "<<filter<<endl;
      }
      //pva->clearVal(); // clear
    }
    else {
      clog<<"UFInstrumSetup::genExec> No Filter input value..."<<endl;
    }
  }
  else {
    clog<<"UFInstrumSetup::genExec> No Filter DB input found?"<<endl;
  }
   
  pvname = _name + ".BeamMode";
  pva = (*this)[pvname];
  if( pva != 0 ) {
    string beam = input = pva->valString(); // input is anything non-null
    clog<<"UFInstrumSetup::genExec> "<<pvname<<" == "<<beam<<endl;
    if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
      if( allowable(pvname, beam) ) {
        conf["BeamMode"] = beam;
        _BeamMode = beam;
        clog<<"UFInstrumSetup::genExec> BeamMode value not allowed: "<<_BeamMode<<endl;
      }
      else {
        clog<<"UFInstrumSetup::genExec> BeamMode value not allowed: "<<beam<<endl;
      }
      //if( pva->isSet() )
	copyInOut(pva);
    }
    else {
      clog<<"UFInstrumSetup::genExec> No BeamMode input value..."<<endl;
    }
    //pva->clearVal(); // clear?
  }
  else {
    clog<<"UFInstrumSetup::genExec> No BeamMode DBinput found?"<<endl;
  }

  string pvdecker  = _name + ".Decker";
  string pvmosslit = _name + ".MOSSlit";
  string pvlyot  = _name + ".Lyot";
  string pvgrism = _name + ".Grism";
  string pvfocus  = _name + ".DetPosFocus";
  // note that the name strings in the conf key must mach ufflam2mech.h mechnames!
  if( !_BiasMode.empty() && newBias) {
    if( _BiasMode.find("imaging") != string::npos ||
        _BiasMode.find("Imaging") != string::npos ||
	_BiasMode.find("IMAGING") != string::npos ) {
      conf["Decker"] = "Imaging";
      conf["MOS"] = "Imaging";
      conf["Lyot"] = "f/16";
      conf["Grism"] = "open";
      conf["Focus"] = "f/16";
    }
    if( _BiasMode.find("long_slit") != string::npos ||
        _BiasMode.find("Long_slit") != string::npos ||
	_BiasMode.find("LONG_SLIT") != string::npos ) {
      conf["Decker"] = "long_slit";
      conf["MOS"] = "Slit";
    }
    if( _BiasMode.find("mos") != string::npos ||
        _BiasMode.find("Mos") != string::npos ||
	_BiasMode.find("MOS") != string::npos ) {
      conf["Decker"] = "MOS";
    }
  }

  if( !_BeamMode.empty() && newBeam ) {
    if( _BeamMode.find("null") != string::npos ||
        _BeamMode.find("Null") != string::npos ||
	_BeamMode.find("NULL") != string::npos ) {
      if( allowable(pvfocus, _BeamMode) )
	conf["Focus"] = _BeamMode;
      if( allowable(pvlyot, _BeamMode) )
	conf["Lyot"] = _BeamMode;
    }
  }

  // mosslit:
  pva = (*this)[pvmosslit];
  if( pva != 0 ) {
    string moslit = input = pva->valString(); // input is > 0
    clog<<"UFInstrumSetup::genExec> "<<pvmosslit<<" == "<<moslit<<endl;
    if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
      if( allowable(pvmosslit, input) ) {
        conf["MOS"] = moslit;
        //if( pva->isSet() )
          copyInOut(pva);
      }
      else {
        clog<<"UFInstrumSetup::genExec> MOS value not allowed: "<<moslit<<endl;
      }
      //pva->clearVal(); // don't clear?
    }
  }

  // lyot
  pva = (*this)[pvlyot];
  pvname = _name + ".OverrideLyot";
  opva = (*this)[pvname];
  if( pva != 0 && opva != 0 ) {
    //override = (int)opva->getNumeric(); opva->clearVal();
    if( override > 0 ) {
      string lyot = input = pva->valString(); // input is > 0
      clog<<"UFInstrumSetup::genExec> "<<pvlyot<<" == "<<lyot<<endl;
      if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
        if( allowable(pvlyot, input) ) {
          clog<<"UFInstrumSetup::genExec> Lyot: "<<lyot<<endl;
          conf["Lyot"] = lyot;
          //if( pva->isSet() )
	    copyInOut(pva);
	}
	else {
	  clog<<"UFInstrumSetup::genExec> Lyot value not allowed: "<<lyot<<endl;
	}
        pva->clearVal(); // clear
      }
      else {
	 clog<<"UFInstrumSetup::genExec> No lyot input value..."<<endl;
      }
    }
  } // override beammode lyot selection
  
  // det. focus
  pvname = _name + ".OverrideDetPosFocus";
  opva = (*this)[pvname];
  pva = (*this)[pvfocus];
  if( pva != 0 && opva != 0 ) {
    //override = (int)opva->getNumeric(); opva->clearVal();
    if( override > 0 ) {
      string focus = input = pva->valString(); // input is anything non-null
      clog<<"UFInstrumSetup::genExec> "<<pvfocus<<" == "<<input<<endl;
      if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
        if( allowable(pvfocus, input) ) {
          clog<<"UFInstrumSetup::genExec> Detfocus: "<<focus<<endl;
          conf["Focus"] = focus;
          //if( pva->isSet() )
	    copyInOut(pva);
   	}
	else {
	  clog<<"UFInstrumSetup::genExec> Detfocus value not allowed: "<<focus<<endl;
	}
        pva->clearVal(); // clear
      }
      else {
	 clog<<"UFInstrumSetup::genExec> No detfocus input value..."<<endl;
      }
    }
  } // override det focus position selection

  // window cover
  pvname = _name + ".OverrideWindowCover";
  opva = (*this)[pvname];
  pvname = _name + ".WindowCover";
  pva = (*this)[pvname];
  if( pva != 0 && opva != 0 ) {
    //override = (int)opva->getNumeric(); opva->clearVal();
    if( override > 0 ) {
      string windowcover = input = pva->valString(); // input is anything non-null
      clog<<"UFInstrumSetup::genExec> "<<pvname<<" == "<<input<<endl;
      if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
        if( allowable(pvname, input) ) {
          clog<<"UFInstrumSetup::genExec> window: "<<windowcover<<endl;
          conf["Window"] = windowcover;
          //if( pva->isSet() )
	    copyInOut(pva);
	}
	else {
	  clog<<"UFInstrumSetup::genExec> Window value not allowed: "<<windowcover<<endl;
	}
      }
      else {
	 clog<<"UFInstrumSetup::genExec> No window input value..."<<endl;
      }
      pva->clearVal(); // clear
    }
  } // override window pre-selection

  // decker override
  pvname = _name + ".OverrideDecker";
  opva = (*this)[pvname];
  pva = (*this)[pvdecker];
  if( pva != 0 && opva != 0 ) {
    //override = (int)opva->getNumeric(); opva->clearVal();
    if( override > 0 ) {
      string decker = input = pva->valString(); // input is anything non-null
      clog<<"UFInstrumSetup::genExec> "<<pvname<<" == "<<input<<endl;
      if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
	if( allowable(pvdecker, input) ) {
          clog<<"UFInstrumSetup::genExec> Ddecker: "<<decker<<endl;
          conf["Decker"] = decker;
          //if( pva->isSet() )
	    copyInOut(pva);
	}
	else {
	  clog<<"UFInstrumSetup::genExec> Decker value not allowed: "<<decker<<endl;
	}
      }
      else {
	 clog<<"UFInstrumSetup::genExec> No decker input value..."<<endl;
      }
      pva->clearVal(); // clear
    }
  } // override decker pre-selection


  pvname = _name + ".OverrideGrism";
  opva = (*this)[pvname];
  string overridegrism = input = pva->valString(); // input is anything non-null
  pva = (*this)[pvgrism];
  if( pva != 0 && opva != 0 ) {
    //override = (int)opva->getNumeric(); opva->clearVal();
    if( override > 0 ) {
      string grism = input = pva->valString(); // input is anything non-null
      clog<<"UFInstrumSetup::genExec> "<<pvgrism<<" == "<<input<<endl;
      if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
        if( allowable(pvgrism, input) ) {
          clog<<"UFInstrumSetup::genExec> Grism: "<<grism<<endl;
          conf["Grism"] = grism;
          //if( pva->isSet() )
	    copyInOut(pva);
	}
	else {
	  clog<<"UFInstrumSetup::genExec> Grism value not allowed: "<<grism<<endl;
	}
      }
      else {
	 clog<<"UFInstrumSetup::genExec> No grism input value..."<<endl;
      }
      pva->clearVal(); // clear
    }
  }

  /*
  pvname = _name + ".OverrideMOSSetPoint";
  opva = (*this)[pvname];
  pvname = _name + ".MOSSetPoint";
  pva = (*this)[pvname];
  if( pva != 0 && opva != 0 ) {
    override = (int)opva->getNumeric(); opva->clearVal();
    if( override > 0 ) {
      input = pva->valString(); // input is anything non-null
      double mossetp = pva->getNumeric();
      clog<<"UFInstrumSetup::genExec> "<<pvname<<" == "<<mossetp<<endl;
      if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
        //pva->clearVal(); // clear
        if( allowable(pvname, mossetp) ) {
          clog<<"UFInstrumSetup::genExec> overridemossetp: "<<mossetp<<endl;
          setp[pvname] = mossetp;
          //if( pva->isSet() )
	    copyInOut(pva);
	}
	else {
	  clog<<"UFInstrumSetup::genExec> overridemossetpvalue not allowed: "<<mossetp<<endl;
	}
      }
      else {
	 clog<<"UFInstrumSetup::genExec> No overridemossetp input value..."<<endl;
      }
      pva->clearVal(); // clear
    }
    else {
       clog<<"UFInstrumSetup::genExec> No overridemossetp specified..."<<endl;
    }	  
  }

  pvname = _name + ".OverrideCamSetPointA";
  opva = (*this)[pvname];
  pvname = _name + ".CamSetPointA";
  pva = (*this)[pvname];
  if( pva != 0 && opva != 0 ) {
    override = (int)opva->getNumeric(); opva->clearVal();
    if( override > 0 ) {
      input = pva->valString(); // input is anything non-null
      double camsetpA = pva->getNumeric();
      clog<<"UFInstrumSetup::genExec> "<<pvname<<" == "<<camsetpA<<endl;
      if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
        if( allowable(pvname, camsetpA) ) {
          setp[pvname] = camsetpA;
          clog<<"UFInstrumSetup::genExec> overridecamsetpA: "<<camsetpA<<endl;
          //if( pva->isSet() )
	    copyInOut(pva);
 	}
	else {
	  clog<<"UFInstrumSetup::genExec> overridecamsetpAvalue not allowed: "<<camsetpA<<endl;
	}
      }
      else {
	 clog<<"UFInstrumSetup::genExec> No overridecamsetpA input value..."<<endl;
      }
      pva->clearVal(); // clear
    }
    else {
       clog<<"UFInstrumSetup::genExec> No overridecamsetpA specified..."<<endl;
    }	  
  }

  pvname = _name + ".OverrideCamSetPointB";
  opva = (*this)[pvname];
  pvname = _name + ".CamSetPointB";
  pva = (*this)[pvname];
  if( pva != 0 && opva != 0 ) {
    override = (int)opva->getNumeric(); opva->clearVal();
    if( override > 0 ) {
      input = pva->valString(); // input is anything non-null
      double camsetpB = pva->getNumeric();
      clog<<"UFInstrumSetup::genExec> "<<pvname<<" == "<<camsetpB<<endl;
      if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
        //pva->clearVal(); // clear
        if( allowable(pvname, camsetpB) ) {
          setp[pvname] = camsetpB;
          clog<<"UFInstrumSetup::genExec> overridecamsetpB: "<<camsetpB<<endl;
          //if( pva->isSet() )
	    copyInOut(pva);
 	}
	else {
	  clog<<"UFInstrumSetup::genExec> overridecamsetpBvalue not allowed: "<<camsetpB<<endl;
	}
      }
      else {
	 clog<<"UFInstrumSetup::genExec> No overridecamsetpB input value..."<<endl;
      }
      pva->clearVal(); // clear
    }
    else {
       clog<<"UFInstrumSetup::genExec> No overridecamsetpB specified..."<<endl;
    }	  
  }
  */

  UFInstrumSetup::Parm* p = allocParm(conf, setp);
  int stat = config(p, timeout, this, _car);  // perform requested instrumsetup configuration

  if( stat < 0 ) { 
    clog<<"UFInstrumSetup::genExec> failed to start instrumsetup pthread..."<<endl;
    return stat;
  }

  return stat;
} // genExec

int UFInstrumSetup::validate(UFPVAttr* pva) {
  return 0;
} // validate


#endif // __UFINSTRUMSETUP_CC__

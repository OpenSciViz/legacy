#if !defined(__UFENGOBSERVE_CC__)
#define __UFENGOBSERVE_CC__ "$Name:  $ $Id: UFEngObserve.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFENGOBSERVE_CC__;

#include "UFEngObserve.h"
#include "UFCAServ.h"
#include "UFPVAttr.h"
#include "UFPosixRuntime.h"
#include "UFRuntime.h"
#include "UFTimeStamp.h"
#include "UFStrings.h"
#include "UFDCSetup.h"
#include "UFClientSocket.h"

// global (static) attributes:
string UFEngObserve::_dhshost; // dhs location
string UFEngObserve::_edtdhost; // agent location
int UFEngObserve::_edtdport;
UFClientSocket* UFEngObserve::_edtdsoc= 0;
//string UFEngObserve::_dcdhost; // agent location
//int UFEngObserve::_dcdport;
//UFClientSocket* UFEngObserve::_dcdsoc= 0;

// ctors: (inlined)

// static class funcs:
// edt connection helper:
bool UFEngObserve::_EDTConnection(const string& edtinit, const string& name) {
  bool validsoc = connected(UFEngObserve::_edtdsoc);
  if( validsoc ) {
    return validsoc;
  }
  else if( !edtinit.empty() && edtinit != "null" ) {
    clog<<"UFEngObserve> connect to ufgedtd..."<<endl;
    // (re) init connection to edtd:
    if( validsoc ) UFEngObserve::_edtdsoc->close(); delete UFEngObserve::_edtdsoc;
    UFEngObserve::_edtdsoc = new UFClientSocket();
    UFEngObserve::_edtdsoc->connect(UFEngObserve::_edtdhost, UFEngObserve::_edtdport);
    validsoc = connected(UFEngObserve::_edtdsoc);
    UFTimeStamp greet(name); 
    clog<<"UFEngObserve> send greet to ufgedtd: "<<greet.name()<<endl;
    int ns = UFEngObserve::_edtdsoc->send(greet); // send the greeting
    if( ns <= 0 ) {
      clog<<"UFEngObserve> error on greeting to ufgedtd"<<endl;
      UFEngObserve::_edtdsoc->close(); delete UFEngObserve::_edtdsoc; UFEngObserve::_edtdsoc= 0;
      return false;
    }
    UFProtocol* greeting = UFProtocol::createFrom(*UFEngObserve::_edtdsoc);
    clog<<"UFEngObserve> reply from ufgedtd: "<<greeting->name()<<endl;
    delete greeting;
  }
  return false;
}

// static pthread func:
void* UFEngObserve::observation(void* p) {
  // by default all these parameters are sent to the ufgmce4 detector agent
  // however, there (is/will be) a debug option to send edt related parms
  // directly to the ufgedtd EDT daemon.
  UFEngObserve::EObsThrdArg* arg = (UFEngObserve::EObsThrdArg*) p;
  UFEngObserve::EObsParm* parms = arg->_parms;
  double exptime = parms->_exptime;
  string edtinit = parms->_edtinit;
  string dhsinit = parms->_dhsinit; // dhs server host?
  string dhslabel = parms->_dhslabel;
  // edt daemon should keep track of any/all luts it has loaded to avoid reloading:
  string lut = parms->_lut; 
  string fits = parms->_fitsfile; // need to have an mef option too
  string png = parms->_pngfile;
  int frmmode = parms->_frmmode;
  int idx = parms->_index;
  int ds9 = parms->_ds9;
  int simmode = parms->_simmode;
  UFCAR* car = arg->_car;
  UFGem* rec = arg->_rec; // should be either a cad or apply
  string name = "anon";

  clog<<"UFEngObserve::observation> "<<exptime<<", "<<edtinit<<", "<<dhsinit<<", "<<dhslabel<<", "<<frmmode<<", "<<idx<<", "<<ds9<<", "<<simmode<<endl;
  if( rec )
    name = rec->name();

  string carname = "flam:eng:observeC";
  if( rec ) {
    UFCAR* rcar = rec->hasCAR();
    if( rcar )
      carname = rcar->name();
  }
  else if( car ) {
    carname = car->name();
  }
  carname += ".IVAL";

  // free args
  delete arg;

  if( _sim ) { // simple simulation...
    clog<<"UFEngObserve::observation> "<<name<<", exptime: "<<parms->_exptime<<endl;
    // set cad/car idle:
    UFPosixRuntime::sleep(0.5);
    if( car )
      car->setIdle();
    if( rec ) {
      UFCAR* c = rec->hasCAR();
      if( c != car ) c->setIdle();
    }
    return p;
  }

  int err= 2;
  bool validsoc = _EDTConnection(edtinit, name);
  string errmsg = "Bad ufedtd socket";
  if( !validsoc ) {
    clog<<"UFEngObserve::observation> invalid socket connect to ufgedtd..."<<endl;
   if( car )
      car->setErr(errmsg, err);
    if( rec ) {
      UFCAR* c = rec->hasCAR();
      if( c != car ) c->setErr(errmsg, err);
    }
    return p;
  }

  vector< string > req;
  req.push_back("CAR"); req.push_back(carname);
  string sad = UFCAServ::_instrum; sad += ":sad:EDTFRAME";
  req.push_back("SAD"); req.push_back(sad);
  req.push_back("ACQ"); req.push_back("start");
  req.push_back("EXPTIME"); req.push_back(UFRuntime::concat("", exptime));
  req.push_back("DHSLabel"); req.push_back(dhslabel);
  req.push_back("LUT"); req.push_back(lut);
  req.push_back("FILE"); req.push_back(fits);
  req.push_back("PNG"); req.push_back(png);
  req.push_back("CNT"); req.push_back(UFRuntime::concat("", frmmode));
  req.push_back("INDEX"); req.push_back(UFRuntime::concat("", idx));
  req.push_back("DS9"); req.push_back(UFRuntime::concat("", ds9));
  // send an 'acq' bundle to the ufedtd:
  //if( UFCAServ::_verbose )
    for( int i = 0; i < (int) (req.size() - 1); i += 2 )
      clog<<"UFEngObserve::observation> submit req: "<<req[i]<<" : "<<req[i+1]<<endl;
  
  UFStrings ufs("flam:eng:observe", req);
  int ns = UFEngObserve::_edtdsoc->send(ufs);
  if( ns <= 0 )
    clog<<"UFEngObserve::observation> edtd submit failed..."<<endl;
  else
    clog<<"UFEngObserve::observation> edtd req. submitted, no reply expected..."<<endl;
 
  // free args
  delete arg;

  // exit daemon thread
  return p;
} // UFEngObserve::observation

int UFEngObserve::observe(UFEngObserve::EObsParm* parms, int timeout, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec && car ) {
    UFCAR* c = rec->hasCAR();
    if( car != c ) car->setBusy(timeout);
  }
  // create and run pthread to move mechanisms and then either reset car to idle or error
  // thread should free MotParm* allocations...
  //pthread_t pmot;
  EObsThrdArg* arg = new EObsThrdArg(parms, rec, car);
  pthread_t thrid = UFPosixRuntime::newThread(UFEngObserve::observation, arg);

  return (int) thrid;
}

// non static, non virtual funcs:
// protected ctor helper:
void UFEngObserve::_create(const string& dhshost, const string& edthost, const string& dchost) {
  _edtdhost = edthost; _edtdport = 52001; // set global statics
  if( _edtdhost == "" ) _edtdhost = UFRuntime::hostname();
  //_dcdhost = dchost; _dcdport = 52008;
  //if( _dcdhost == "" ) _dcdhost = UFRuntime::hostname();
  _dhshost = dhshost;
  if( _dhshost == "" ) _dhshost = UFRuntime::hostname();
  UFPVAttr* pva= 0;

  string pvname = _name + ".ExpTime";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 0.0);
  attachInOutPV(this, pva);

  pvname = _name + ".SimMode";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 1);
  attachInOutPV(this, pva);

  pvname = _name + ".FrameMode";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 1);
  attachInOutPV(this, pva);

  pvname = _name + ".EDTInit";
  pva = (*this)[pvname] = new UFPVAttr(pvname, "test_2048.cfg");
  attachInOutPV(this, pva);

  pvname = _name + ".DHSInit";
  pva = (*this)[pvname] = new UFPVAttr(pvname, "GeminiNorth");
  attachInOutPV(this, pva);

  pvname = _name + ".DHSLabel";
  pva = (*this)[pvname] = new UFPVAttr(pvname, "RichardElston");
  attachInOutPV(this, pva);

  // Flamingos-1 ufgtake options ...
  pvname = _name + ".LUT";
  pva = (*this)[pvname] = new UFPVAttr(pvname, "RichardElston.lut");
  attachInOutPV(this, pva);

  pvname = _name + ".FITSFile";
  pva = (*this)[pvname] = new UFPVAttr(pvname, "RichardElston.fits");
  attachInOutPV(this, pva);

  pvname = _name + ".PNGFile"; 
  pva = (*this)[pvname] = new UFPVAttr(pvname, "RichardElston.png");
  attachInOutPV(this, pva);

  pvname = _name + ".Index";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 0);
  attachInOutPV(this, pva);

  pvname = _name + ".DS9";
  pva = (*this)[pvname] = new UFPVAttr(pvname, 0);
  attachInOutPV(this, pva);

  registerRec();
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
}

/// virtual funcs:
int UFEngObserve::validate(UFPVAttr* pva) {
  if( pva == 0 )
    return 0;

  string input = pva->valString();
  char* cs = (char*) input.c_str();
  while( *cs == ' ' && *cs == '\t' )
    ++cs;

  if( isdigit(cs[0]) )
    return 0;

  return -1;
}

/// perform setup motion for those mechanisms with non-null step cnts or named-position inputs
int UFEngObserve::genExec(int timeout) {
  size_t pvcnt = size();

  if( pvcnt == 0 ) {
    clog<<"UFEngObserve::genExec> np pvs?"<<endl;
    return (int) pvcnt;
  }

  // which pvs are non null?
  double expt= 0;

  string pvname = _name + ".ExpTime";
  string pvoname = pvname + "O";
  UFPVAttr* pva = (*this)[pvname];
  if( pva ) {
    expt = pva->getNumeric();
    pva->clearVal(); // clear input
  }

  string dhslabel = "RichardElston";
  pvname = _name + ".DHSLabel";
  pvoname = pvname + "O";
  pva = (*this)[pvname];
  if( pva ) {
    dhslabel = pva->valString(); // is this is empty/null don't archive to dhs?
    pva->clearVal(); // clear input
  }

  string dhsinit = "";
  pvname = _name + ".DHSInit";
  pvoname = pvname + "O";
  pva = (*this)[pvname];
  if( pva ) {
    dhsinit = pva->valString();
    pva->clearVal(); // clear input
  }

  string edtinit = "";
  pvname = _name + ".EDTInit"; // initcam config file
  pvoname = pvname + "O";
  pva = (*this)[pvname];
  if( pva ) {
    edtinit = pva->valString();
    pva->clearVal(); // clear input
  }

  int simmode= 1;
  pvname = _name + ".SimMode";
  pvoname = pvname + "O";
  pva = (*this)[pvname];
  if( pva ) {
    simmode = (int)pva->getNumeric();
    UFPVAttr* pvo = (*this)[pvoname]; // outval
    if( pvo ) pvo->copyVal(pva); // copy to output field
    //pva->clearVal(); // clear input
  }

  int frmmode= 1;
  pvname = _name + ".FrameMode";
  pvoname = pvname + "O";
  pva = (*this)[pvname];
  if( pva ) {
    frmmode = (int)pva->getNumeric(); // number/count of frames to expect for this observation
    //pva->clearVal(); // (don't) clear input
  }

  // these are some of the more useful options inherited from Flamignos-1 ufgtake:
  string lut = "none"; // or "null"
  pvname = _name + ".LUT"; // local fits filename (if different from dhs name)
  pvoname = pvname + "O";
  pva = (*this)[pvname];
  if( pva ) {
    lut = pva->valString();
     //pva->clearVal(); // (don't) clear input
  }

  string fitsfile = dhslabel;
  pvname = _name + ".FITSFile"; // local fits filename (different from dhs name)
  pvoname = pvname + "O";
  pva = (*this)[pvname];
  if( pva ) {
    fitsfile = pva->valString();
    //pva->clearVal(); // (don't) clear input
  }

  string pngfile = dhslabel;
  pvname = _name + ".PNGFile"; // local fits filename (if different from dhs name)
  pvoname = pvname + "O";
  pva = (*this)[pvname];
  if( pva ) {
    pngfile = pva->valString();
    //pva->clearVal(); // (don't) clear input
  }
  
  int index= 0; //  observation sequence (dither pattern) index
  pvname = _name + ".Index";
  pvoname = pvname + "O";
  pva = (*this)[pvname];
  if( pva ) {
    index = (int)pva->getNumeric();
    //pva->clearVal(); // (don't) clear input
  }

  int ds9= 0; // display to ds9 (1-N) 
  pvname = _name + ".DS9";
  pvoname = pvname + "O";
  pva = (*this)[pvname];
  if( pva ) {
    ds9 = (int)pva->getNumeric();
    //pva->clearVal(); // (don't) clear input
  }

  UFEngObserve::EObsParm* parms = new UFEngObserve::EObsParm(expt, dhslabel, dhsinit, edtinit, 
							     lut, fitsfile, pngfile, frmmode,
							     index, ds9, simmode);
  //int stat= observe(parms, timeout, this, _car);
  int stat= observe(parms, timeout, this);
  if( stat < 0 ) { 
    clog<<"UFEngObserve::genExec> failed to start observation pthread..."<<endl;
    return stat;
  }

  return stat;
} // genExec

#endif // __UFENGOBSERVE_CC__

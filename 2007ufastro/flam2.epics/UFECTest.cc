#if !defined(__UFECTEST_CC__)
#define __UFECTEST_CC__ "$Name:  $ $Id: UFECTest.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFECTEST_CC__;

#include "UFECTest.h"
#include "UFECSetup.h"
#include "UFPVAttr.h"

#include "UFPosixRuntime.h"

// ctors:

// static class funcs:
void* UFECTest::thrdFunc(void* p) {
  // test really just needs?
  UFECSetup::ECThrdArg* arg = (UFECSetup::ECThrdArg*) p;
  UFECSetup::EnvParm* parms = arg->_parms;
  UFCAR* car = arg->_car;

  // simple simulation...
  clog<<"UFECTest::thrdFunc> ..."<<endl;
  if( parms ) {
    clog<<"UFECTest::thrdFunc> environment: "<<parms->_MOSKelv<<", "<<parms->_CamAKelv<<", "<<parms->_CamBKelv<<endl;
  }

  // free arg
  delete arg;

  // set car idle after 1/2 sec:
  UFPosixRuntime::sleep(0.5);
  if( car )
    car->setIdle();

  // exit daemon thread
  return p;
}

/// general purpose test func. 

/// arg:
int UFECTest::newThrd(UFECSetup::EnvParm* parms, int timeout, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec && car ) {
    UFCAR* c = rec->hasCAR();
    if( car != c ) car->setBusy(timeout);
  }

  // create and run pthread and then either reset car to idle or error
  // thread should free MotParm* allocations...
  //pthread_t pmot;
  UFECSetup::ECThrdArg* arg = new UFECSetup::ECThrdArg(parms, car);
  pthread_t thrid = UFPosixRuntime::newThread(UFECTest::thrdFunc, arg);

  return (int) thrid;
}

// non static, non virtual funcs:
// protected ctor helper:
void UFECTest::_create() {
  UFPVAttr* pva= 0;
  string pvname = _name + ".mode";
  pva = (*this)[pvname] = new UFPVAttr(pvname); // any non null value should indicate test action
  attachInOutPV(this, pva);

  registerRec();
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
}

/// virtual funcs:
/// perform test
int UFECTest::genExec(int timeout) {
  size_t pvcnt = size();

  if( pvcnt == 0 ) {
    clog<<"UFECTest::genExec> np pvs?"<<endl;
    return (int) pvcnt;
  }

  string pvname = _name + ".mode";
  UFPVAttr* pva = (*this)[pvname];
  string input = pva->valString(); // input is anything non-null
  if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
    clog<<"UFECTest::genExec> mode: "<<input<<endl;
    pva->clearVal(); // clear
  }
  UFECSetup::EnvParm* p = new UFECSetup::EnvParm;
  int stat = newThrd(p, timeout, this, _car);  // perform requested test

  if( stat < 0 ) { 
    clog<<"UFTest::genExec> failed to start test pthread..."<<endl;
    return stat;
  }

  return stat;
} // genExec

int UFECTest::validate(UFPVAttr* pva) {
  return 0;
} // validate

#endif // __UFECTEST_CC__

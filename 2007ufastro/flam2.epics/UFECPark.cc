#if !defined(__UFECPark_DC__)
#define __UFECPark_DC__ "$Name:  $ $Id: UFECPark.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFECPark_DC__;

#include "UFECPark.h"
#include "UFDCSetup.h"
#include "UFPVAttr.h"

#include "UFPosixRuntime.h"

// ctors:

// static class funcs:
void* UFECPark::envThrd(void* p) {
  // seek really just needs the mech. names
  UFECSetup::ECThrdArg* arg = (UFECSetup::ECThrdArg*) p;
  UFECSetup::EnvParm* parms = arg->_parms;
  UFCAR* car = arg->_car;
  UFGem* rec = arg->_rec; // should be either a cad or apply
  string name = "anon";
  if( rec )
    name = rec->name(); 

  /*
  int err= 0;
  string errmsg;
  */
  // simple simulation...
  clog<<"UFECPark::envThrd> ..."<<endl;
  if( parms ) {
    clog<<"UFSeek::envThrd> LVDT On/Off: "<<parms->_OnOffLVDT<<", MOSKelv: "<<parms->_MOSKelv
	<<", CamAKelv: "<<parms->_CamAKelv<<", CamBKelv: "<<parms->_CamBKelv<<endl;
  }

  // free arg
  delete arg;

  // set car idle after 1/2 sec:
  UFPosixRuntime::sleep(0.5);
  if( car )
    car->setIdle();

  // exit daemon thread
  return p;
}

/// general purpose seek func. 

/// mce4 params
int UFECPark::env(UFECSetup::EnvParm* parms, int timeout, UFGem* rec,  UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec && car ) {
    UFCAR* c = rec->hasCAR();
    if( car != c ) car->setBusy(timeout);
  }

  // create and run pthread to send commands to env. ctrl (lakeshore temp. ctrl)
  // thread should free MotParm* allocations...
  //pthread_t pmot;
  UFECSetup::ECThrdArg* arg = new UFECSetup::ECThrdArg(parms, rec, car);
  pthread_t thrid = UFPosixRuntime::newThread(UFECPark::envThrd, arg);

  return (int) thrid;
}

// non static, non virtual funcs:
// protected ctor helper:
void UFECPark::_create() {
  UFPVAttr* pva= 0;
  string pvname = _name + ".mode"; // sim or not?, immeddiate or differed? warm or cold or whatever
  pva = (*this)[pvname] = new UFPVAttr(pvname); // any non null value should indicate seek action
  attachInOutPV(this, pva);

  registerRec();
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
}

/// virtual funcs:
int UFECPark::genExec(int timeout) {
  size_t pvcnt = size();

  if( pvcnt == 0 ) {
    clog<<"UFECPark::genExec> np pvs?"<<endl;
    return (int) pvcnt;
  }

  UFPVAttr* pva = pdir(); //(*this)[pvname];
  if( pva != 0 ) {
    string input = pva->valString(); // input is anything non-null
    if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
      clog<<"UFECPark::genExec> park directive..."<<endl;
      pva->clearVal(); // clear
    }
  }

  string pvname = _name + ".mode";
  pva = (*this)[pvname];
  if( pva != 0 ) {
    string input = pva->valString(); // input is anything non-null
    if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
      clog<<"UFECPark::genExec> park mode: "<<input<<endl;
      pva->clearVal(); // clear
    }
  }
  UFECSetup::EnvParm* p = new UFECSetup::EnvParm;
  int stat = env(p, timeout, _car);  // perform requested seek

  if( stat < 0 ) { 
    clog<<"UFSeek::genExec> failed to start seek park pthread..."<<endl;
    return stat;
  }

  return stat;
} // genExec

int UFECPark::validate(UFPVAttr* pva) {
  return 0;
} // validate

#endif // __UFECPark_DC__

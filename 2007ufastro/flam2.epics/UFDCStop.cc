#if !defined(__UFDCSTOP_DC__)
#define __UFDCSTOP_DC__ "$Name:  $ $Id: UFDCStop.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFDCSTOP_DC__;

#include "UFDCStop.h"
#include "UFDCSetup.h"
#include "UFPVAttr.h"

#include "UFPosixRuntime.h"

// ctors:

// static class funcs:
void* UFDCStop::stopit(void* p) {
  // stop really just needs ?
  UFDCSetup::DCThrdArg* arg = (UFDCSetup::DCThrdArg*) p;
  UFDCSetup::DetParm* parms = arg->_parms;
  //UFGem* rec = arg->_rec;
  UFCAR* car = arg->_car;

  // simple simulation...
  clog<<"UFDCStop::stopping> ..."<<endl;
  if( parms ) {
    clog<<"UFStop::stopping> detector pixels: "<<parms->_w * parms->_h<<endl;
  }

  // free arg
  delete arg;

  // set car idle after 1/2 sec:
  UFPosixRuntime::sleep(0.5);
  if( car )
    car->setIdle();

  // exit daemon thread
  return p;
}

/// general purpose stop func. 

/// arg:
int UFDCStop::stop(UFDCSetup::DetParm* parms, int timeout, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec && car ) {
    UFCAR* c = rec->hasCAR();
    if( car != c ) car->setBusy(timeout);
  }

  // create and run pthread and then either reset car to idle or error
  // thread should free MotParm* allocations...
  //pthread_t pmot;
  UFDCSetup::DCThrdArg* arg = new UFDCSetup::DCThrdArg(parms, rec, car);
  pthread_t thrid = UFPosixRuntime::newThread(UFDCStop::stopit, arg);

  return (int) thrid;
}

// non static, non virtual funcs:
// protected ctor helper:
void UFDCStop::_create() {
  UFPVAttr* pva= 0;
  string pvname = _name + ".DataLabel";
  pva = (*this)[pvname] = new UFPVAttr(pvname); // any non null value should indicate stop action
  attachInOutPV(this, pva);

  registerRec();
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
}

/// virtual funcs:
/// perform dc stop
int UFDCStop::genExec(int timeout) {
  size_t pvcnt = size();

  if( pvcnt == 0 ) {
    clog<<"UFDCStop::genExec> np pvs?"<<endl;
    return (int) pvcnt;
  }

  string pvname = _name + ".DataLabel";
  UFPVAttr* pva = (*this)[pvname];
  if( pva != 0 ) {
    string input = pva->valString(); // input is anything non-null
    if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
      clog<<"UFDCStop::genExec> datalabel: "<<input<<endl;
      pva->clearVal(); // clear
    }
  }
  UFDCSetup::DetParm* p = new UFDCSetup::DetParm;
  int stat = stop(p, timeout, this, _car);  // perform requested stop

  if( stat < 0 ) { 
    clog<<"UFStop::genExec> failed to start stop pthread..."<<endl;
    return stat;
  }

  return stat;
} // genExec

int UFDCStop::validate(UFPVAttr* pva) {
  return 0;
} // validate

#endif // __UFDCABORT_DC__

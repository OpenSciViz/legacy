#if !defined(__UFCCABORT_CC__)
#define __UFCCABORT_CC__ "$Name:  $ $Id: UFCCAbort.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFCCABORT_CC__;

#include "UFCCAbort.h"
#include "UFCAServ.h"
#include "UFCCSetup.h"
#include "UFPVAttr.h"
#include "UFPosixRuntime.h"
#include "UFStrings.h"

// ctors:

// static class funcs:
void* UFCCAbort::abortion(void* p) {
  // abort really just needs the mech. names
  UFCCSetup::CCThrdArg* arg = (UFCCSetup::CCThrdArg*) p;
  std::map< string, UFCCSetup::MotParm* >* mparms = arg->_mparms;
  std::map< string, UFCCSetup::MotParm* >::const_iterator mit;
  UFCAR* car = arg->_car;
  UFGem* rec = arg->_rec; // should be either a cad or apply
  string name = "anon";
  if( rec )
    name = rec->name(); 

  string carname = "instrum:cc:abortC";
  if( rec ) {
    UFCAR* rcar = rec->hasCAR();
    if( rcar )
      carname = rcar->name();
  }
  else if( car ) {
    carname = car->name();
  }
  carname += ".IVAL";

  deque< string > cmdvec;
  // req. bundle is pairs of strings:

  // if agent should set car on completion or error, indicate car channel (pvname):
  // current agents will not do the right thing for this yet...
  //cmdvec.push_back("CAR"); cmdvec.push_back(carname);

  string cmd = "CAR";// cmd += mname;
  string cmdpar = carname;
  cmdvec.push_back(cmd); cmdvec.push_back(cmdpar); 
  for( mit = mparms->begin(); mit != mparms->end(); ++mit ) {
    string mname = mit->first;
    UFCCSetup::MotParm* mp = mit->second; 
    //if( _verbose )
      clog<<"UFCCAbort::abortion> "<<mname<<" indexor: "<<mp->_indexor<<endl;
    // abort indexor and indicate suggested error message,
    // followed by full syntax command and parameter:
    cmd = mp->_indexor; cmd += "Raw "; cmd += " @ !!Error Abort "; cmd += mname;
    cmdpar = mp->_indexor; cmdpar += "@"; // portescap soft stop
    cmdvec.push_back(cmd); cmdvec.push_back(cmdpar); 
    //if( _verbose )
    clog<<"UFCCAbort::abortion> cmd: "<<cmd<<"; cmdimpl: "<<cmdpar<<endl;
  }

  int err= 0;
  string errmsg;
  if( !_sim && UFCCSetup::_motoragntsoc != 0 ) {
    // test it to be sure non-blocking connection is functional/selectable (connection has completed)
    bool validsoc= false, connected = ( UFCCSetup::_motoragntsoc != 0 );
    if( connected ) validsoc = UFCCSetup::_motoragntsoc->validConnection();
    int cnt = 3;
    while( --cnt > 0 && connected && !validsoc ) { // try a few times
      validsoc = UFCCSetup::_motoragntsoc->validConnection();
      if( !validsoc ) {
        clog<<"UFCCAbort::abortion> failed validation? trycnt: "<<cnt<<endl;
        UFPosixRuntime::sleep(0.25);
      }
    }
    if( validsoc ) {
      UFStrings req(name, cmdvec); 
      int ns = UFCCSetup::_motoragntsoc->send(req);
      if( ns <= 0 )
        clog<<"UFCCAbort::abortion> failed send req: "<< req.name() << " " << req.timeStamp() <<endl;
      //int nr = UFCCSetup::_motoragntsoc->recv(reqt);
      //if( _verbose )
      for( int i= 0; i < req.elements(); ++i )
	clog<<"UFCCAbort::abortion> sent: "<<req[i]<<endl;
    }
    else {
      clog<<"UFCCAbort::abortion> unable to transact request with portescap motor agent due to invalid connection..."<<endl;
      err = 1; errmsg = "Error Aborting";
    }
  }                                                                                           

  // and free the motparms
  for( mit = mparms->begin(); mit != mparms->end(); ++mit ) {
    UFCCSetup::MotParm* mp = mit->second;
    delete mp;
  }

  // free arg
  delete arg;

  if( _sim || err ) {
    // set car idle after 1/2 sec:
    UFPosixRuntime::sleep(0.5);
    if( car && !err )
      car->setIdle();
    else if( car && err ) {
      car->setErr(errmsg);
    }
    if( rec ) {
      UFCAR* c = rec->hasCAR();
      if( c != 0 && c != car && !err) 
	c->setIdle();
      else if( c != 0 && c != car && err ) {
        c->setErr(errmsg);
      }
    }
  }

  // exit daemon thread
  return p;
}

/// general purpose abort func. 

/// arg: mechName, motparm*
int UFCCAbort::abort(std::map< string, UFCCSetup::MotParm* >* mparms, int timeout, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec ) {
    rec->setBusy(timeout);
    UFCAR* c = rec->hasCAR();
    if( car != 0 && car != c ) car->setBusy(timeout);
  }

  // create and run pthread to move mechanisms and then either reset car to idle or error
  // thread should free MotParm* allocations...
  //pthread_t pmot;
  UFCCSetup::CCThrdArg* arg = new UFCCSetup::CCThrdArg(mparms, rec, car);
  pthread_t thrid = UFPosixRuntime::newThread(UFCCAbort::abortion, arg);

  return (int) thrid;
}

// non static, non virtual funcs:
// protected ctor helper:
void UFCCAbort::_create() {
  /// pvs should look something like:
  /// flam:cc:abort.mech1-N == anything non null
  int mcnt = UFCCSetup::setDefaults(_name); 
  deque< string >* mnames = UFCCSetup::getMechNames();

  // all:
  string pvname = _name + ".All";
  UFPVAttr* pva = (*this)[pvname] = new UFPVAttr(pvname, -1);
  attachInOutPV(this, pva);

  // each:
  for( int i = 0; i < mcnt; ++i ) {
    pvname = _name + "."; pvname += (*mnames)[i]; 
    pva = (*this)[pvname] = new UFPVAttr(pvname, -1);
    attachInOutPV(this, pva);
  }
  registerRec();
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
}

// available to IS
int UFCCAbort::abortAll(int timeout, UFGem* rec, UFCAR* car) {
  string recname = UFCAServ::_instrum + ":cc:abort";
  UFCAD* ccabort = dynamic_cast<UFCAD*>(_theRecList[recname]);
  if( ccabort == 0 ) 
    return 0;
  deque< string >& _mechNames = *(UFCCSetup::getMechNames());
  // this is a rather clumsy way to examine all the inputs and determine
  // which ones have been set for processing... 
  // which pv mechNames are non zero? (assume all mech inputs are ints)
  // abort these:
  std::map< string, UFCCSetup::MotParm* >* mechs = new std::map< string, UFCCSetup::MotParm* >;
  string mechName, indexor, pvname;
  for( size_t i= 0; i < _mechNames.size(); ++i ) {
    mechName= _mechNames[i]; indexor= UFCCSetup::_indexorNames[mechName];
    pvname = ccabort->name() + "." + mechName;
    UFPVAttr* pva = (*ccabort)[pvname]; // only interested in mechName
    if( pva == 0 )
      continue;
    clog<<"UFCCAbort::genExec> "<<mechName<<", indexor: "<<indexor<<endl;
    UFCCSetup::MotParm* mp = new UFCCSetup::MotParm(indexor);
    (*mechs)[mechName] = mp;
    pva->clearVal(); // clear mechName input
  }

  int stat= 0, nm = (int) mechs->size();
  if( nm > 0 )
    stat = abort(mechs, timeout, rec, ccabort->hasCAR());  // perform requested abort

  if( stat < 0 ) { 
    clog<<"UFCCAbort::genExec> failed to start abort pthread..."<<endl;
    return stat;
  }

  return nm;
}

/// virtual funcs:
/// perform abort mechanisms with non-null step cnts or named-position inputs
int UFCCAbort::genExec(int timeout) {
  size_t pvcnt = size();
  if( pvcnt == 0 ) {
    clog<<"UFCCAbort::genExec> np pvs?"<<endl;
    return (int) pvcnt;
  }

  bool all = false;
  string pvname = name() + ".All";
  UFPVAttr* pva = (*this)[pvname]; // only interested in mechName
  if( pva != 0 ) {
    int input = (int)pva->getNumeric();
    if( input >= 0 ) {
      all = true;
      //if( _verbose )
      clog<<"UFCCAbort::genExec> "<<pvname<<", input: "<<input<<endl;
    }
    pva->clearVal();
  }

  if( all )
    return UFCCAbort::abortAll(timeout, this, _car);

  deque< string >& _mechNames = *(UFCCSetup::getMechNames());
  // this is a rather clumsy way to examine all the inputs and determine
  // which ones have been set for processing... 
  // which pv mechNames are non zero? (assume all mech inputs are ints)
  // abort these:
  std::map< string, UFCCSetup::MotParm* >* mechs = new std::map< string, UFCCSetup::MotParm* >;
  string mechName, indexor;
  for( size_t i= 0; i < _mechNames.size(); ++i ) {
    mechName= _mechNames[i]; indexor= UFCCSetup::_indexorNames[mechName];
    pvname = name() + "." + mechName;
    pva = (*this)[pvname]; // only interested in mechName
    if( pva == 0 )
      continue;

    int input = (int)pva->getNumeric();
    if( _verbose )
      clog<<"UFCCAbort::genExec> "<<pvname<<", input: "<<input<<endl;
    if( input < 0 )
      continue;

    clog<<"UFCCAbort::genExec> "<<mechName<<", indexor: "<<indexor<<endl;
    UFCCSetup::MotParm* mp = new UFCCSetup::MotParm(indexor);
    (*mechs)[mechName] = mp;
    pva->clearVal(); // clear mechName input
  }

  int stat= 0, nm = (int) mechs->size();
  if( nm > 0 )
    stat = abort(mechs, timeout, this, _car);  // perform requested abort

  if( stat < 0 ) { 
    clog<<"UFCCAbort::genExec> failed to start abort pthread..."<<endl;
    return stat;
  }

  return nm;
} // abort genExec

int UFCCAbort::validate(UFPVAttr* pva) {
  if( pva == 0 )
    return 0;

  string pvname = pva->name();
  string input = pva->valString();
  deque< string >* mnames = UFCCSetup::getMechNames();
  size_t nm = mnames->size();
  for( size_t i = 0; i < nm; ++i ) {
    if( pvname.find((*mnames)[i]) != string::npos ) {
      clog<<"UFCCAbort::validate> "<<pvname<<" == "<<input<<endl;
    }
  }

  return 0;
} // validate


#endif // __UFCCABORT_CC__

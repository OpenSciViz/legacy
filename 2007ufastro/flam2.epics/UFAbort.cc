#if !defined(__UFABORT_CC__)
#define __UFABORT_CC__ "$Name:  $ $Id: UFAbort.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFABORT_CC__;

#include "UFAbort.h"
#include "UFCAR.h"
#include "UFCCAbort.h"
#include "UFDCAbort.h"
#include "UFECAbort.h"
#include "UFPVAttr.h"
#include "UFPosixRuntime.h"

// ctors:

// static class funcs:
void* UFAbort::abortion(void* p) {
  // abort really just needs the mech. names
  UFAbort::ThrdArg* arg = (UFAbort::ThrdArg*) p;
  UFAbort::Parm* parms = arg->_parms;
  UFCAR* car = arg->_car;
  UFGem* rec = arg->_rec; // should be either a cad or apply
  string name = "anon";
  if( rec )
    name = rec->name();

  // simple simulation...
  clog<<"UFAbort::aborting>... "<<name<<endl;
  if( parms && parms->_op && parms->_osp && parms->_isp ) {
    clog<<"UFAbort::aborting> obs timeout: "<<parms->_op->_timeout<<", exptime: "<<parms->_osp->_exptime<<endl;
    if( parms->_isp->_mparms && !parms->_isp->_mparms)
      clog<<"UFAbort::aborting> motors: "<<parms->_isp->_mparms->size()<<endl;
    if( parms->_isp->_eparms )
      clog<<"UFAbort::aborting> environment: "<<parms->_isp->_eparms->_MOSKelv
	  <<", "<<parms->_isp->_eparms->_CamAKelv<<", "<<parms->_isp->_eparms->_CamBKelv<<endl;
    if( parms->_osp->_dparms )
      clog<<"UFAbort::aborting> detector pixels: "<<parms->_osp->_dparms->_w*parms->_osp->_dparms->_h<<endl;
  }
  // free arg
  delete arg;

  if( _sim ) {
    // set car idle after 1/2 sec:
    UFPosixRuntime::sleep(0.5);
    if( car )
      car->setIdle();
    if( rec ) {
      UFCAR* c = rec->hasCAR();
      if( c != 0 && c != car ) c->setIdle();
    }
  }

  // exit daemon thread
  return p;
}

/// general purpose abort func. 

/// arg: mechName, motparm*
int UFAbort::abort(UFAbort::Parm* parms, int timeout, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec && car ) {
    UFCAR* c = rec->hasCAR();
    if( car != c ) car->setBusy(timeout);
  }

  // create and run pthread to abort whatever is currently being performed then either reset car to idle or error
  // thread should free the tParm* allocations...
  if( _sim ) {
    UFAbort::ThrdArg* arg = new UFAbort::ThrdArg(parms, rec, car);
    pthread_t thrid = UFPosixRuntime::newThread(UFAbort::abortion, arg);
    return (int) thrid;
  }

  UFCCAbort::abortAll(timeout, rec, car);
  UFDCAbort::abortAll(timeout, rec, car);
  return UFECAbort::abortAll(timeout, rec, car);
}

// non static, non virtual funcs:
// protected ctor helper:
void UFAbort::_create() {
  string pvname = _name + ".subsys";
  UFPVAttr* pva = (*this)[pvname] = new UFPVAttr(pvname); // any non null string should indicate abort action 
  attachInOutPV(this, pva); 
  registerRec();
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
}

/// virtual funcs:
/// perform abort mechanisms with non-null step cnts or named-position inputs
int UFAbort::genExec(int timeout) {
  size_t pvcnt = size();

  if( pvcnt == 0 ) {
    clog<<"UFAbort::genExec> np pvs?"<<endl;
    return (int) pvcnt;
  }

  string input, 
         pvname = _name + ".mode";
  UFPVAttr* pva = (*this)[pvname];
  if( pva != 0 ) {
    input = pva->valString(); // input is anything non-null
    if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
      clog<<"UFAbort::genExec> abort mode: "<<input<<endl;
      pva->clearVal(); // clear
    }
  }
  
  pvname = _name + ".subsys";
  pva = (*this)[pvname];
  if( pva != 0 ) {
    input = pva->valString(); // input is anything non-null
    if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
      clog<<"UFAbort::genExec> abort subsys: "<<input<<endl;
      pva->clearVal(); // clear
    }
  }
  UFAbort::Parm* p = new UFAbort::Parm;
  int stat = abort(p, timeout, this, _car);  // perform requested abort

  if( stat < 0 ) { 
    clog<<"UFAbort::genExec> failed to start abort pthread..."<<endl;
    return stat;
  }

  return stat;
} // genExec

int UFAbort::validate(UFPVAttr* pva) {
  return 0;
} // validate


#endif // __UFABORT_CC__

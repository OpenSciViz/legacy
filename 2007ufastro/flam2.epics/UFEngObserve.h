#if !defined(__UFEngObserve_h__)
#define __UFEngObserve_h__ "$Name:  $ $Id: UFEngObserve.h,v 0.6 2005/12/13 20:45:48 hon Exp $"
#define __UFEngObserve_H__(arg) const char arg##EngObserve_h__rcsId[] = __UFEngObserve_h__;

#include "UFCAD.h"
#include "UFRuntime.h"
#include "vector"

class UFEngObserve : public UFCAD {
public:
  enum { NOSIM= 0, EDTSIM= -1, MCESIM= -2, SCIMODE= 1, ENGMODE= 2 };
  // engineering obervation paramaters
  struct EObsParm { double _exptime; int _simmode, _frmmode, _index, _ds9;
                    string _edtinit, _dhsinit, _dhslabel, _lut, _fitsfile, _pngfile;
  inline EObsParm(double exptime= 4.0, const string& dhslabel= "RichardElston",
		  const string& dhsinit= "", const string& edtinit= "", const string& lut= "default.lut",
		  const string& fitsfile= "RichardElston.fits", const string& pngfile= "RichardElston.png",
		  int frmmode= 1, int index= 0, int ds9= 2, int simmode= 1) : 
                  _exptime(exptime), _simmode(simmode), _frmmode(frmmode), _index(index), _ds9(ds9),
                  _edtinit(edtinit), _dhsinit(dhsinit), _dhslabel(dhslabel),
                  _lut(lut), _fitsfile(fitsfile), _pngfile(pngfile) {}
    inline ~EObsParm() {}
  };
  // thread arg:
  struct EObsThrdArg { EObsParm* _parms; UFGem* _rec; UFCAR* _car;
    inline EObsThrdArg(EObsParm* eop, UFGem* r= 0, UFCAR* c= 0) : _parms(eop), _rec(r), _car(c) {}
    inline ~EObsThrdArg() { delete _parms; }
  };

  inline UFEngObserve(const string& instrum= "instrum",
		      const string& dhshost= "", const string& edthost= "", const string& dchost= "",
		      UFCAR* car= 0) : UFCAD(instrum+":eng:observe", car) 
    { _create(dhshost, edthost, dchost); }

  inline virtual ~UFEngObserve() {}

  virtual int validate(UFPVAttr* pva= 0);

  /// perform setup motion for those mechanisms with non-null step cnts or named-position inputs
  virtual int genExec(int timeout= 0);

  // thread entry func.
  static void* observation(void* p= 0);
  ///general purpose mce4 func. (non datum)
  // of use here and from IS. starts a new daemon thread for every observation/exposure/acquisition
  static int observe(UFEngObserve::EObsParm* parms, int timeout= 10, UFGem* rec= 0, UFCAR* car= 0); 

  inline static void setHosts(const string& dhshost="", const string& edthost="", const string& dchost="") {
    _edtdhost = edthost.empty() ? UFRuntime::hostname() : edthost; 
    // _dcdhost = dchost.empty() ? edthost : dchost; 
    _dhshost = dhshost;
  }
  inline static void setPorts(int edtport= 52000, int dcport= 52008)
    { _edtdport= edtport; _dcdport= dcport; }
  inline static string edtHostAndPort(int& port) { port= _edtdport; return _edtdhost; }
  //inline static string dcHostAndPort(int& port) { port= _dcdport; return _dcdhost; }
  inline static string dhsHost() { return _dhshost; }

  // edt connection helper (DC connections via UFDCSetup::_connection, _port, _host)
  static bool _EDTConnection(const string& edtinit, const string& name);

protected:
  static string _edtdhost; // _dcdhost; // agent hosts
  static string _dhshost; // dhs server host
  static int _edtdport, _dcdport;
  static UFClientSocket* _edtdsoc;
  //static UFClientSocket* _dcdsoc;

  // ctor helper
  void _create(const string& dhshost= "", const string& edthost= "", const string& dchost= "");
};

#endif // __UFEngObserve_h__

#if !defined(__UFPVATTR_CC__)
#define __UFPVATTR_CC__ "$Name:  $ $Id: UFPVAttr.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFPVATTR_CC__;

// UF
#include "UFRuntime.h" // system & posix conv. funcs.
#include "UFPVAttr.h"
#include "UFCAServ.h"
#include "UFPV.h"
#include "UFGem.h"

// class globals:
UFCAServ* UFPVAttr::_cas= 0; /// allow posting of cas events on PV writes via PV ctors
std::map<string, UFPVAttr*> UFPVAttr::_thePVList;

UFPVAttr::UFPVAttr(const string& pName,
		   const string& val) : _numeric(INT_MIN), //_numeric(FP_NAN),
					_highoperat(1.0), _lowoperat(0),
                                        _high_alarm(1.10), _low_alarm(0.01),
                                        _high_warn(1.05), _low_warn(0.05),
                                        _high_ctrl_lim(0.9), _low_ctrl_lim(0.1),
                                        _precision(10), _name(pName.c_str()),
					_units("NoUnits"), 
					_indbr(0), _outdbr(0), _assocIn(0), _assocOut(0) {
  //_pVal = new (nothrow) gddScalar(gddAppType_value, aitEnumString);
  _pVal = new gddScalar(gddAppType_value, aitEnumString);
  char* cs = (char*) val.c_str();
  if( val.empty() ) {
    //_pVal->putConvert(pName);
    _pVal->putConvert("null");
    return;
  }
  // elim. whites
  while( *cs == ' ' || *cs == '\t' || *cs == '\n' || *cs == '\r' ) 
    ++cs;

  _pVal->putConvert(cs);

  errno = 0;
  _numeric = atof(cs);
  if( errno != 0 ) // atof failed
    _numeric = INT_MIN; 

  return;
}

UFPVAttr::UFPVAttr(const string& pName,
		   int val) : _numeric(val), //_numeric(FP_NAN),
			      _highoperat(1.0), _lowoperat(0),
                              _high_alarm(1.10), _low_alarm(0.01),
                              _high_warn(1.05), _low_warn(0.05),
                              _high_ctrl_lim(0.9), _low_ctrl_lim(0.1),
                              _precision(10), _name(pName.c_str()),
			      _units("Normal"),
			      _indbr(0), _outdbr(0), _assocIn(0), _assocOut(0) {
  //_pVal = new (nothrow) gddScalar(gddAppType_value, aitEnumInt32);
  _pVal = new gddScalar(gddAppType_value, aitEnumInt32);
  _pVal->putConvert(val);
}

UFPVAttr::UFPVAttr(const string& pName,
		   double val) : _numeric(val), //_numeric(FP_NAN),
		                 _highoperat(1.0), _lowoperat(0),
                                 _high_alarm(1.10), _low_alarm(0.01),
                                 _high_warn(1.05), _low_warn(0.05),
                                 _high_ctrl_lim(0.9), _low_ctrl_lim(0.1),
                                 _precision(10), _name(pName.c_str()),
				 _units("Normal"),
				 _indbr(0), _outdbr(0), _assocIn(0), _assocOut(0){
  //_pVal = new (nothrow) gddScalar(gddAppType_value, aitEnumFloat64);
  _pVal = new gddScalar(gddAppType_value, aitEnumFloat64);
  _pVal->putConvert(val);
}

UFPVAttr::UFPVAttr(const string& pvname, const UFPVAttr* pva) {
  UFPVAttr* newpva= 0;
  if( pva == 0 ) { // assume string (default "null" ctor)
    newpva = new UFPVAttr(pvname);
  }
  gdd* pValue = pva->getVal();
  aitEnum primtyp = pValue->primitiveType();
  if( primtyp == aitEnumString ) {
    aitString as; pValue->getConvert(as);
    string name = as.string();
    newpva = new UFPVAttr(pvname, name);
  }
  else if( primtyp == aitEnumInt32  ) {
    int iv; pValue->getConvert(iv);
    newpva = new UFPVAttr(pvname, iv);
  }
  else {
    double dv; pValue->getConvert(dv);
    newpva = new UFPVAttr(pvname, dv);
  }
  *this = *newpva; // simply use compiler's (shallow) assigment op.
}

// provide means to copy the value (but not the other attributes):
void UFPVAttr::copyVal(const UFPVAttr* inpva) {
  if( inpva == 0 ) {
    clog<<"UFPVAttr::copyVal> null PVAttr* inpva..."<<endl;
    return;
  }
  gdd* inv = inpva->getVal();
  if( inv == 0 ) {
    clog<<"UFPVAttr::copyVal> null gdd* for pvname: "<<inpva->name()<<endl;
    return;
  }
  //clog<<"UFPVAttr::copyVal> "<<inpva->name()<<" to "<<name()<<endl;
  // insure monitor is posted: 
  UFPV::write(*inv, *this); // but should not cause associated record to be marked
  return;
}

void UFPVAttr::clearVal(bool writepost) {
  _numeric = INT_MIN;
  if( _pVal == 0 ) {
    clog<<"UFPVAttr::clearVal> "<<name()<<" has no data ptr?"<<endl;
    return;
  }
  aitEnum primtyp = _pVal->primitiveType();
  if( primtyp == aitEnumString ) {
    _pVal->putConvert("null");
    //clog<<"UFPVAttr::clearVal> "<<name()<<" nulled..."<<endl;
  }
  else {
    _pVal->putConvert(INT_MIN);
    //clog<<"UFPVAttr::clearVal> "<<name()<<" nulled (INT_MIN)"<<endl;
  }
  // insure monitor is posted:
  if( writepost )
    UFPV::write(*_pVal, *this); // but should not cause associated record to be marked

  return;
}

string UFPVAttr::setVal(const string& val, bool writepost) {
  string vs; 
  if( _pVal == 0 ) {
    clog<<"UFPVAttr::setVal> "<<name()<<" has no (gdd*) data struct..."<<endl;
    return vs;
  }
  vs= "null"; 
  if( !val.empty() ) vs= val; 
  aitString s = vs.c_str(); _pVal->putConvert(s); vs = s.string();
  if( writepost )
    UFPV::write(*_pVal, *this); 
 
  return vs;
}

int UFPVAttr::setVal(int val, bool writepost) { 
  if( _pVal == 0 ) {
    clog<<"UFPVAttr::setVal> "<<name()<<" has no (gdd*) data struct..."<<endl;
    return INT_MIN;
  }
  _pVal->putConvert(val); _numeric = val; 
  if( writepost )
    UFPV::write(*_pVal, *this); 

  return val;
}

double UFPVAttr::setVal(double val, bool writepost) { 
  if( _pVal == 0 ) {
    clog<<"UFPVAttr::setVal> "<<name()<<" has no (gdd*) data struct..."<<endl;
    return INT_MIN;
  }
  _pVal->putConvert(val); _numeric = val;
  if( writepost )
    UFPV::write(*_pVal, *this); 

  return val;
}


bool UFPVAttr::isSet() {
  gdd* pValue = getVal();
  if( pValue == 0 )
    return false;
  aitEnum primtyp = pValue->primitiveType();
  if( primtyp == aitEnumString ) {
    aitString as; pValue->getConvert(as);
    string s = as.string();
    if( s == "null" )
      return false;
  }
  else if( primtyp == aitEnumFloat32  ) {
    float fv; pValue->getConvert(fv);
    int iv = (int)fv;
    if( iv == INT_MIN )
      return false;
  }
  else if( primtyp == aitEnumFloat64  ) {
    double dv; pValue->getConvert(dv);
    int iv = (int)dv;
    if( iv == INT_MIN )
      return false;
  }
  else {
    int iv; pValue->getConvert(iv);
    if( iv == INT_MIN )
      return false;    
  }
  return true;
}

bool UFPVAttr::isClear() {
  gdd* pValue = getVal();
  if( pValue == 0 )
    return false;
  aitEnum primtyp = pValue->primitiveType();
  if( primtyp == aitEnumString ) {
    aitString as; pValue->getConvert(as);
    string s = as.string();
    if( s.empty() || s == "null" )
      return true;
  }
  else if( primtyp == aitEnumFloat32  ) {
    float fv; pValue->getConvert(fv);
    int iv = (int) fv;
    if( iv == INT_MIN )
      return true;
  }
  else if( primtyp == aitEnumFloat64  ) {
    double fv; pValue->getConvert(fv);
    int iv = (int) fv;
    if( iv == INT_MIN )
      return true;
  }
  else {
    int iv; pValue->getConvert(iv);
    if( iv == INT_MIN )
      return true;    
  }
  return false;
}

/// allow automatice processing of CAR on new input:
/// restrict to new ival input (assume any errmss & num 
/// are set before ival?)
UFGem* UFPVAttr::isCADInput() { 
  string nm = name();
  if( _indbr == 0 ) {
    //clog<<"UFPVAttr::isCADInput> _indbr not set? "<<nm<<endl;
    return 0;
  }
  if( _indbr->type() != UFGem::_CAD ) {
    if( UFPV::_verbose )
      clog<<"UFPVAttr::isCARInput> this is not a CAD input: "<<nm<<", rectype: "<<_indbr->type()<<endl;
    return 0;
  }

  return _indbr;
}

UFGem* UFPVAttr::isCARInput() { 
  string nm = name();
  if( _indbr == 0 ) {
    //clog<<"UFPVAttr::isCARInput> _indbr not set? "<<nm<<endl;
    return 0;
  }
  if( _indbr->type() != UFGem::_CAR ) {
    return 0;
  }
  if( nm.find(".VAL") != string::npos ) {
    if( UFPV::_verbose )
      clog<<"UFPVAttr::isCARInput> this is not a CAR input: "<<nm<<", rectype: "<<_indbr->type()<<endl;
    return 0;
  }
  return _indbr;
}

/// allow automatice processing of SAD on new input:
UFGem* UFPVAttr::isSADInput() { 
  string nm = name();
  if( _indbr == 0 ) {
    //clog<<"UFPVAttr::isSADInput> _indbr not set? "<<nm<<endl;
    return 0;
  }
  if( UFPV::_vverbose )
    clog<<"UFPVAttr::isSADInput> this is an input: "<<nm<<", rectype: "<<_indbr->type()<<endl;
  if( _indbr->type() != UFGem::_SIR ) {
    return 0;
  }
  if( nm.find(".VAL") != string::npos ) {
    if( UFPV::_verbose )
      clog<<"UFPVAttr::isSADInput> this is not a SAD input: "<<nm<<", rectype: "<<_indbr->type()<<endl;
    return 0;
  }
  return _indbr;
}

/// allow automatice processing of CAD or Apply on new Directive input:
UFGem* UFPVAttr::isDirectiveInput() {
  string nm = name();
  size_t dpos = nm.rfind("."); 
  if( dpos == string::npos )
    return 0;

  nm = nm.substr(dpos);
  if( nm != ".DIR" )
    return 0;

  if( _indbr == 0 ) {
    clog<<"UFPVAttr::isDirective> _indbr not set? "<<nm<<endl;
    return 0;
  }

  return _indbr;
}

/// on new value, if valid input (not output!), mark this pv's container record (presumably a CAD)
// return rec. type
int UFPVAttr::mark() { 
  if( _indbr == 0 ) {
    //if( name().find("heart") == string::npos )
     // clog<<"UFPVAttr::mark> "<<name()<<" not an input..."<<endl;
    return -1; // not an input attribute!
  }
  // if attribute is being cleared (nulled), do not mark!
  if( isClear() ) {
    if( name().find("heart") == string::npos )
      clog<<"UFPVAttr::mark> "<<name()<<" cleared?"<<endl;
    return 0; // cleared input!
  }
  _indbr->mark(this);
  return _indbr->type();
}
 
void UFPVAttr::clear() const { if( _indbr ) _indbr->clear(); }
int UFPVAttr::preset() const { if( _indbr ) return _indbr->preset(); return -1; }
int UFPVAttr::directive(int d) const { if( _indbr ) return _indbr->directive(d); return -1; }
int UFPVAttr::start(int timeout) const { if( _indbr ) return _indbr->start(timeout); return -1; }

#endif // __UFPVATTR_CC__

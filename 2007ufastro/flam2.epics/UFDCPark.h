#if !defined(__UFDCPark_h__)
#define __UFDCPark_h__ "$Name:  $ $Id: UFDCPark.h,v 0.2 2006/08/29 20:16:47 hon Exp $"
#define __UFDCPark_H__(arg) const char arg##DCPark_h__rcsId[] = __UFDCPark_h__;

#include "UFCAD.h"
#include "UFDCSetup.h"

class UFDCPark : public UFCAD {
public:
  inline UFDCPark(const string& instrum= "instrum") : UFCAD(instrum+":dc:park") { _create(); }
  inline virtual ~UFDCPark() {}

  virtual int validate(UFPVAttr* pva= 0);
  virtual int genExec(int val= 0);

  static void* thrdFunc(void* p);
  static int newThrd(UFDCSetup::DetParm* parms, int timeout, UFGem* rec= 0, UFCAR* car= 0);
  // available to IS
  inline static int mce4(int timeout, UFGem* rec= 0, UFCAR* car= 0)
    { UFDCSetup::DetParm* p= new UFDCSetup::DetParm; return newThrd(p, timeout, rec, car); }

protected:
  // ctor helper
  void _create();
};

#endif // __UFDCPark_h__

#if !defined(__UFCCSTOP_CC__)
#define __UFCCSTOP_CC__ "$Name:  $ $Id: UFCCStop.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFCCSTOP_CC__;

#include "UFCCStop.h"
#include "UFCCSetup.h"
#include "UFPVAttr.h"
#include "UFPosixRuntime.h"
#include "UFStrings.h"

// ctors:

// static class funcs:
void* UFCCStop::stopit(void* p) {
  // stop really just needs the mech. names
  UFCCSetup::CCThrdArg* arg = (UFCCSetup::CCThrdArg*) p;
  std::map< string, UFCCSetup::MotParm* >* mparms = arg->_mparms;
  std::map< string, UFCCSetup::MotParm* >::const_iterator mit;
  UFCAR* car = arg->_car;
  UFGem* rec = arg->_rec; // should be either a cad or apply
  string name = "anon";
  if( rec )
    name = rec->name(); 

  string carname = "instrum:cc:stopC";
  if( rec ) {
    UFCAR* rcar = rec->hasCAR();
    if( rcar )
      carname = rcar->name();
  }
  else if( car ) {
    carname = car->name();
  }
  carname += ".IVAL";

  deque< string > cmdvec;
  // req. bundle is pairs of strings:

  // if agent should set car on completion or error, indicate car channel (pvname):
  // current agents will not do the right thing for this yet...
  //cmdvec.push_back("CAR"); cmdvec.push_back(carname);

  string cmd = "CAR";// cmd += mname;
  string cmdpar = carname;
  cmdvec.push_back(cmd); cmdvec.push_back(cmdpar); 
  for( mit = mparms->begin(); mit != mparms->end(); ++mit ) {
    string mname = mit->first;
    UFCCSetup::MotParm* mp = mit->second; 
    //if( _verbose )
      clog<<"UFCCStop::stopit> "<<mname<<" indexor: "<<mp->_indexor<<endl;
    // stop indexor and indicate suggested error message,
    // followed by full syntax command and parameter:
    cmd = mp->_indexor; cmd += "Raw "; cmd += " @ !!Error Stop "; cmd += mname;
    cmdpar = mp->_indexor; cmdpar += "@"; // portescap soft stop
    cmdvec.push_back(cmd); cmdvec.push_back(cmdpar); 
    //if( _verbose )
    clog<<"UFCCStop::stopit> cmd: "<<cmd<<"; cmdimpl: "<<cmdpar<<endl;
  }

  int err= 0;
  string errmsg;
  if( !_sim && UFCCSetup::_motoragntsoc != 0 ) {
    // test it to be sure non-blocking connection is functional/selectable (connection has completed)
    bool validsoc= false, connected = ( UFCCSetup::_motoragntsoc != 0 );
    if( connected ) validsoc = UFCCSetup::_motoragntsoc->validConnection();
    int cnt = 3;
    while( --cnt > 0 && connected && !validsoc ) { // try a few times
      validsoc = UFCCSetup::_motoragntsoc->validConnection();
      if( !validsoc ) {
        clog<<"UFCCStop::stopit> failed validation? trycnt: "<<cnt<<endl;
        UFPosixRuntime::sleep(0.25);
      }
    }
    if( validsoc ) {
      UFStrings req(name, cmdvec); 
      int ns = UFCCSetup::_motoragntsoc->send(req);
      if( ns <= 0 )
        clog<<"UFCCStop::stopit> failed send req: "<< req.name() << " " << req.timeStamp() <<endl;
      //int nr = UFCCSetup::_motoragntsoc->recv(reqt);
      //if( _verbose )
      for( int i= 0; i < req.elements(); ++i )
	clog<<"UFCCStop::stopit> sent: "<<req[i]<<endl;
    }
    else {
      clog<<"UFCCStop::stopit> unable to transact request with portescap motor agent due to invalid connection..."<<endl;
      err = 1; errmsg = "Error Stoping";
    }
  }                                                                                           

  // and free the motparms
  for( mit = mparms->begin(); mit != mparms->end(); ++mit ) {
    UFCCSetup::MotParm* mp = mit->second;
    delete mp;
  }

  // free arg
  delete arg;

  if( _sim || err ) {
    // set car idle after 1/2 sec:
    UFPosixRuntime::sleep(0.5);
    if( car && !err )
      car->setIdle();
    else if( car && err ) {
      car->setErr(errmsg);
    }
    if( rec ) {
      UFCAR* c = rec->hasCAR();
      if( c != 0 && c != car && !err) 
	c->setIdle();
      else if( c != 0 && c != car && err ) {
        c->setErr(errmsg);
      }
    }
  }

  // exit daemon thread
  return p;
}

/// general purpose stop func. 

/// arg: mechName, motparm*
int UFCCStop::stop(std::map< string, UFCCSetup::MotParm* >* mparms, int timeout, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec ) {
    rec->setBusy(timeout);
    UFCAR* c = rec->hasCAR();
    if( car != 0 && car != c ) car->setBusy(timeout);
  }

  // create and run pthread to move mechanisms and then either reset car to idle or error
  // thread should free MotParm* allocations...
  //pthread_t pmot;
  UFCCSetup::CCThrdArg* arg = new UFCCSetup::CCThrdArg(mparms, rec, car);
  pthread_t thrid = UFPosixRuntime::newThread(UFCCStop::stopit, arg);

  return (int) thrid;
}

// non static, non virtual funcs:
// protected ctor helper:
void UFCCStop::_create() {
  /// pvs should look something like:
  /// flam:cc:stop.mech1-N == anything non null
  vector< string > posvec;
  int mcnt = UFCCSetup::setDefaults(_name); 
  deque< string >* mnames = UFCCSetup::getMechNames();
  UFPVAttr* pva= 0;
  for( int i = 0; i < mcnt; ++i ) {
    string pvname = _name + "."; pvname += (*mnames)[i];
    pva = (*this)[pvname] = new UFPVAttr(pvname, -1);
    attachInputPV(pva);
    pvname += 'O';
    pva = (*this)[pvname] = new UFPVAttr(pvname, -1);
    attachOutputPV(pva);
  }

  registerRec();
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
}

/// virtual funcs:
/// perform stop mechanisms with non-null step cnts or named-position inputs
int UFCCStop::genExec(int timeout) {
  size_t pvcnt = size();
  if( pvcnt == 0 ) {
    clog<<"UFCCStop::genExec> np pvs?"<<endl;
    return (int) pvcnt;
  }


  deque< string >& _mechNames = *(UFCCSetup::getMechNames());
  // this is a rather clumsy way to examine all the inputs and determine
  // which ones have been set for processing... 
  // which pv mechNames are non zero? (assume all mech inputs are ints)
  // stop these:
  std::map< string, UFCCSetup::MotParm* >* mechs = new std::map< string, UFCCSetup::MotParm* >;
  string mechName, indexor;
  for( size_t i= 0; i < _mechNames.size(); ++i ) {
    mechName= _mechNames[i]; indexor= UFCCSetup::_indexorNames[mechName];
    string pvname = name() + "." + mechName;
    UFPVAttr* pva = (*this)[pvname]; // only interested in mechName
    if( pva == 0 )
      continue;
    int input = (int)pva->getNumeric();
    if( _verbose )
      clog<<"UFCCDatum::genExec> "<<pvname<<", input: "<<input<<endl;
    if( input <= 0 )
      continue;

    clog<<"UFCCStop::genExec> "<<mechName<<", indexor: "<<indexor<<endl;
    UFCCSetup::MotParm* mp = new UFCCSetup::MotParm(indexor);
    (*mechs)[mechName] = mp;
    pva->clearVal(); // clear mechName input
  }

  int stat= 0, nm = (int) mechs->size();
  if( nm > 0 )
    stat = stop(mechs, timeout, this, _car);  // perform requested stop

  if( stat < 0 ) { 
    clog<<"UFCCStop::genExec> failed to start stop pthread..."<<endl;
    return stat;
  }

  return nm;
} // stop genExec

int UFCCStop::validate(UFPVAttr* pva) {
  if( pva == 0 )
    return 0;

  string pvname = pva->name();
  string input = pva->valString();
  deque< string >* mnames = UFCCSetup::getMechNames();
  size_t nm = mnames->size();
  for( size_t i = 0; i < nm; ++i ) {
    if( pvname.find((*mnames)[i]) != string::npos ) {
      clog<<"UFCCStop::validate> "<<pvname<<" == "<<input<<endl;
    }
  }

  return 0;
} // validate


#endif // __UFCCSTOP_CC__

#if !defined(__UFFOCUS_CC__)
#define __UFFOCUS_CC__ "$Name:  $ $Id: UFFocus.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFFOCUS_CC__;

#include "UFFocus.h"
#include "UFCCSetup.h"
#include "UFECSetup.h"
#include "UFPVAttr.h"
#include "UFPosixRuntime.h"
#include "UFStrings.h"

// ctors:
// none here

// static helpers:
// LVDT connection helper:
bool UFFocus::_LVDTConnection(const string& name) {
  bool validsoc = connected(UFECSetup::_lvdtd);
  if( validsoc )
    return validsoc;

  string errmsg = "Bad uflvdtd socket";
  clog<<"UFFocus> connect to ufglvdtd..."<<endl;
  UFECSetup::_lvdtd = new UFClientSocket();
  UFECSetup::_lvdtd->connect(UFECSetup::_host, UFECSetup::_lvdtport);
  validsoc = connected(UFECSetup::_lvdtd);
  UFTimeStamp greet(name); 
  clog<<"UFFocus> send greet to ufglvdtd: "<<greet.name()<<endl;
  int ns = UFECSetup::_lvdtd->send(greet); // send the greeting
  if( ns <= 0 ) {
    clog<<"UFFocus> error on greeting to ufglvdtd"<<endl;
    UFECSetup::_lvdtd->close(); delete UFECSetup::_lvdtd; UFECSetup::_lvdtd= 0;
    return false;
  }
  UFProtocol* greeting = UFProtocol::createFrom(*UFECSetup::_lvdtd);
  clog<<"UFFocus> reply from ufglvdtd: "<<greeting->name()<<endl;
  delete greeting;

  return false;
}

void UFFocus::UFFocus::_disableLVDT(const string& name) {
  bool validsoc = connected(UFECSetup::_lvdtd);
  if( validsoc )
    return;

  int err= 0;
  string errmsg;
  deque< string > cmdvec;
  string cmd = "Raw !! ErrLVDTHz";
  string cmdpar = "SL 55 OFF"; // excitation freq. 2.5KHz
  cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
  cmd = "Raw !! ErrLVDTVolts";
  cmdpar = "SL 56 OFF"; // excitation voltage 3v
  cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
  cmd = "PowerOff !! ErrLVDTPwr";
  cmdpar = "PowerOff";
  cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);

  UFStrings req(name, cmdvec);
  int ns = UFECSetup::_lvdtd->send(req);
  if( ns <= 0 ) {
      clog<<"UFFocus::_enableLVDT> failed send req: "<< req.name() << " " << req.timeStamp() <<endl;
      err = -1;
  }
  else {
    if( _verbose )
      clog<<"UFFocus::_enableLVDT> sent req: "<< req.name() << " " << req.timeStamp() <<", cmd: " <<cmdvec[1]<<endl;
  //int nr = UFCCSetup::_lvdtd->recv(req);
  }

  return;
}

void UFFocus::_enableLVDT(const string& name) { 
  bool validsoc = _LVDTConnection(name);
  if( !validsoc ) 
    return;

  int err= 0;
  string errmsg;
  deque< string > cmdvec;
  string cmd = "PowerOn !! ErrLVDTPwr";
  string cmdpar = "PowerOn";
  cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
  cmd = "Raw !! ErrLVDTCoil";
  cmdpar = "SA 62 1"; // select secondary coil reading
  cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
  cmd = "Raw !! ErrLVDTVolts";
  cmdpar = "SL 56 ON"; // excitation voltage 5v
  cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
  cmd = "Raw !! ErrLVDTHz";
  cmdpar = "SL 55 ON"; // excitation freq. 5KHz
  cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);

  UFStrings req(name, cmdvec);
  int ns = UFECSetup::_lvdtd->send(req);
  if( ns <= 0 ) {
      clog<<"UFFocus::_enableLVDT> failed send req: "<< req.name() << " " << req.timeStamp() <<endl;
      err = -1;
  }
  else {
    if( _verbose )
      clog<<"UFFocus::_enableLVDT> sent req: "<< req.name() << " " << req.timeStamp() <<", cmd: " <<cmdvec[1]<<endl;
  //int nr = UFCCSetup::_motord->recv(req);
  }
  return;
}

string UFFocus::seekLimitOrStep(UFCCSetup::MotParm* mp) {
  string seekOrstep = "";
  if( mp == 0 )
    return seekOrstep;

  string slim = mp->_posit;
  //UFRuntime::lowerCase(slim);
  strstream s;
  s<<mp->_indexor;
  if( slim.find("limit") == string::npos ) { // step cmd
    if( mp->_direction < 0 ) {
      int step = abs(mp->_steps);
      s<<" -"<<step<<" "<<ends;
    }
    else if( mp->_direction > 0 ) {
      int step = abs(mp->_steps);
      s<<" +"<<step<<" "<<ends;
    }
    else if( mp->_steps < 0 ) {
      int step = abs(mp->_steps);
      s<<" -"<<step<<" "<<ends;
    }
    else {
      int step = abs(mp->_steps);
      s<<" +"<<step<<" "<<ends;
    }
    seekOrstep = s.str(); delete s.str();
    clog<<"UFFocus::seekLimitOrStep> step cmd: "<<seekOrstep<<endl;
    return seekOrstep;
  }

  if( mp->_direction < 0 ) {
    int vel = abs(mp->_vel);
    s<<"M -"<<vel<<" "<<ends;
  }
  else if( mp->_direction > 0 ) {
    int vel = abs(mp->_vel);
    s<<"M +"<<vel<<" "<<ends;
  }
  else if( mp->_vel < 0 ) {
    int vel = abs(mp->_vel);
    s<<"M -"<<vel<<" "<<ends;
  }
  else {
    int vel = abs(mp->_vel);
    s<<"M +"<<vel<<" "<<ends;
  }

  seekOrstep = s.str(); delete s.str();
  clog<<"UFFocus::seekLimitOrStep> seek limit cmd: "<<seekOrstep<<endl;
  return seekOrstep;
}
  
// statics:
// this is an atomic step +/- with no backlash 
void* UFFocus::seekit(void* p) {
  // home specified indexor in 0 or 1 direction, at specified velocity
  UFCCSetup::CCThrdArg* arg = (UFCCSetup::CCThrdArg*) p;
  std::map< string, UFCCSetup::MotParm* >* mparms = arg->_mparms;
  std::map< string, UFCCSetup::MotParm* >::const_iterator mit;
  UFCAR* car = arg->_car;
  UFGem* rec = arg->_rec; // should be either a cad or apply
  string name = "anon";
  if( rec )
    name = rec->name(); 

  string carname = "instrum:eng:focusC";
  if( rec ) {
    UFCAR* rcar = rec->hasCAR();
    if( rcar )
      carname = rcar->name();
  }
  else if( car ) {
    carname = car->name();
  }
  carname += ".IVAL";

  int nm = (int) mparms->size();
  if( nm == 0 ) {
    clog<<"UFFocus::seekit> expect one indexor, nm: "<<nm<<", assume simulation..."<<endl;
    _sim = true;
  }
  else if( nm > 1 ) {
    clog<<"UFFocus::seekit> expected only one indexor, ignoring others, nm: "<<nm<<endl;
  }

  // make sure lvdt is powered on and configured
  _enableLVDT(name);

  // test motor indexor agent socket to be sure non-blocking connection is functional/selectable
  // (connection has completed succesfully)
  bool validsoc = connected(UFCCSetup::_motoragntsoc);
  if( !_sim && !validsoc ) {
    clog<<"UFFocus::seekit> not connected to motor indexor agent, need to (re)init..."<<endl;
    // and free the motparms
    for( mit = mparms->begin(); mit != mparms->end(); ++mit ) {
      UFCCSetup::MotParm* mp = mit->second;
      delete mp;
    }
    // free arg
    delete arg;
    return p;
  }

  int err= 0;
  string errmsg;
  deque< string > cmdvec;
  // req. bundle is pairs of strings:
  mit = mparms->begin();
  string mname = mit->first;
  UFCCSetup::MotParm* mp = mit->second; // _posit should be "datum" default posname
  if( mp != 0 && !_sim ) {
    // datum indexor 'I', and indicate suggested error message,
    // followed by full syntax command and parameter:
    // if agent should set car on completion or error, indicate car channel (pvname):
    string cmd = mp->_indexor; cmd += "CAR";
    string cmdpar = carname;
    cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
    //if( _verbose )
      clog<<"UFFocus::seekit> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;

    cmd = mp->_indexor; cmd += "SAD";
    cmdpar = UFCCSetup::sadChan(mname);
    cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
   
    //if( _verbose )
      clog<<"UFFocus::seekit> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;

    cmdpar = seekLimitOrStep(mp);
    if( cmdpar.find("M") == string::npos ) { // step
      // first set slew vel?
      strstream sv;
      sv<<mp->_indexor<<" V "<<abs(mp->_vel)<<ends;
      string slewvel = sv.str(); delete sv.str();
      cmd = "Raw "; cmd += mp->_indexor; cmd += "V !!ErrSlewVel "; cmd += mname;
      cmdvec.push_back(cmd); cmdvec.push_back(slewvel);
      cmd = "Raw "; cmd += mp->_indexor; cmd += " +/-step !!ErrStep "; cmd += mname;
      cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
     }
    else { // limit find/move
      cmd = "Raw "; cmd += mp->_indexor; cmd += "LimitMove +/-vel !!ErrLimMove "; cmd += mname;
      cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
    }

    //if( _verbose )
      clog<<"UFFocus::seekit> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;
 
    UFStrings req(name, cmdvec);
    int ns = UFCCSetup::_motoragntsoc->send(req);
    if( ns <= 0 ) {
      clog<<"UFFocus::seekit> failed send req: "<< req.name() << " " << req.timeStamp() <<endl;
      err = -1;
    }
    else {
      if( _verbose )
        clog<<"UFFocus::seekit> sent req: "<< req.name() << " " << req.timeStamp() <<", cmd: " <<cmdvec[1]<<endl;
    //int nr = UFCCSetup::_motoragntsoc->recv(req);
    }
  }

  // and free the motparms and arg
  delete mp; delete arg;
  
  if( _sim || err != 0 ) {
    clog<<"UFFocus::seekit> error or sim..."<<endl;
    // set car idle after 1/2 sec:
    if( _sim ) UFPosixRuntime::sleep(0.5);
    if( car && !err )
      car->setIdle();
    else if( car && err ) {
      car->setErr(errmsg, err);
    }
    if( rec ) {
      UFCAR* c = rec->hasCAR();
      if( c != 0 && c != car && err == 0 ) 
	c->setIdle();
      else if( c != 0 && c != car && err != 0 ) {
        c->setErr(errmsg, err);
      }
    }
  }

  return p; // exit daemon thread
} // seekit

/// general purpose datum func.
/// arg: mechName, motparm*
int UFFocus::seek(std::map< string, UFCCSetup::MotParm* >* mparm, int timeout, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec ) {
    rec->setBusy(timeout);
    UFCAR* c = rec->hasCAR();
    if( car != 0 && car != c ) car->setBusy(timeout);
  }

  // create and run pthread to move focus mechanism and then either reset car to idle or error
  // thread should free MotParm* allocations...
  //pthread_t pmot;
  UFCCSetup::CCThrdArg* arg = new UFCCSetup::CCThrdArg(mparm, rec, car);
  pthread_t thrid = UFPosixRuntime::newThread(UFFocus::seekit, arg);

  return (int) thrid;
} // home

// non static, non virtual funcs:
// protected ctor helper:
void UFFocus::_create() {
  /// pvs should look something like:
  /// flam:eng:focus.LimitVel == limit velocity
  /// flam:eng:focus.LimitDirection == limit direction
  /// flam:eng:focus.Steps == seek focus step count
  /// flam:eng:focus.StepVel == seek focus step velocity
  /// flam:eng:focus.StepDirection == seek focus step direction

  UFPVAttr* pva= 0;
  string pvname = name() + ".LVDTPwrOnOff"; // 0 == off, 1 == on
  pva = (*this)[pvname] = new UFPVAttr(pvname, (int)0);
  attachInOutPV(this, pva);

  pvname = name() + ".LVDTExcVoltsHiLow"; //  0 == low, 1 == hi
  pva = (*this)[pvname] = new UFPVAttr(pvname, (int)0);
  attachInOutPV(this, pva);

  pvname = name() + ".LVDTExcFreqHiLow"; // 0 == low, 1 == hi
  pva = (*this)[pvname] = new UFPVAttr(pvname, (int)0);
  attachInOutPV(this, pva);

  pvname = name() + ".Step"; // +/-
  pva = (*this)[pvname] = new UFPVAttr(pvname, (int)0);
  attachInOutPV(this, pva);

  pvname = name() + ".StepVel";
  pva = (*this)[pvname] = new UFPVAttr(pvname, (int)0);
  attachInOutPV(this, pva);

  pvname = name() + ".LimitVel"; // +/-
  pva = (*this)[pvname] = new UFPVAttr(pvname, (int)0);
  attachInOutPV(this, pva);

  registerRec();
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
} // _create

/// virtual funcs:
/// perform datum for those mechanisms with non-null inputs
int UFFocus::genExec(int timeout) {
  size_t pvcnt = size();

  if( pvcnt == 0 ) {
    clog<<"UFFocus::genExec> np pvs?"<<endl;
    return (int) pvcnt;
  }

  // deque< string >& _mechNames = *(UFCCSetup::getMechNames());
  // this is a rather clumsy way to examine all the inputs and determine
  // which ones have been set for processing... 
  // which pv mechNames are non zero? (assume all mech inputs are ints)
  std::map< string, UFCCSetup::MotParm* >* mech = new std::map< string, UFCCSetup::MotParm* >;
  int step= 0, vel= 0, dir= 0, limvel= 0, onoff= 0, vlts= 0, freq= 0;
  string mechName("Focus"), indexor("H");
  string pvname = name() + ".LVDTPwrOnOff";
  UFPVAttr* pva = (*this)[pvname]; // only interested in steps, velocity, and direction values (for now)
  if( pva != 0 ) {
    onoff = (int)pva->getNumeric();
    pva->clearVal(); // clear input
  }
  pvname = name() + ".LVDTExcVoltsHiLow";
  pva = (*this)[pvname]; // only interested in steps, velocity, and direction values (for now)
  if( pva != 0 ) {
    vlts = (int)pva->getNumeric();
    //pva->clearVal(); // do not clear input
  }
  pvname = name() + ".LVDTExcFreqHiLow";
  pva = (*this)[pvname]; // only interested in steps, velocity, and direction values (for now)
  if( pva != 0 ) {
    freq = (int)pva->getNumeric();
    pva->clearVal(); // do not clear input
  }
  pvname = name() + ".Step";
  pva = (*this)[pvname]; // only interested in steps, velocity, and direction values (for now)
  if( pva != 0 ) {
    step = (int)pva->getNumeric();
    if( step > 0 ) dir = 1; else dir = -1;
    pva->clearVal(); // clear input
    //if( _verbose )
      clog<<"UFFocus::genExec> "<<pvname<<", step: "<<step<<", vel: "<<vel<<", dir: "<<dir<<endl;
   
  }
  pvname = name() + ".StepVel";
  pva = (*this)[pvname]; // only interested in steps, velocity, and direction values (for now)
  if( pva != 0 ) {
    vel = (int)pva->getNumeric();
    pva->clearVal(); // clear input
  }
  /*
  pvname = name() + ".StepDirection";
  pva = (*this)[pvname]; // only interested in steps, velocity, and direction values (for now)
  if( pva != 0 ) {
    vel = (int)pva->getNumeric();
    pva->clearVal(); // clear input
  }

  pvname = name() + ".LimitDirection";
  pva = (*this)[pvname]; // only interested in steps, velocity, and direction values (for now)
  if( pva != 0 ) {
    limdir = (int)pva->getNumeric();
    pva->clearVal(); // clear input
  }
  */

  pvname = name() + ".LimitVel";
  pva = (*this)[pvname]; // only interested in steps, velocity, and direction values (for now)
  if( pva != 0 ) {
    limvel = (int)pva->getNumeric();
    if( limvel > 0 ) dir = 1; else dir = -1;
    pva->clearVal(); // clear input
  }

  if( step != 0 ) { // focus step motion parm:
  // use this ctor:
  // inline MotParm(const string& indexor, int steps= 0, int blash= 0,
  //                int pos= 0, int ivel= 25, int vel= 100, int dir= 1,
  //                int acc= 100, int dec= 100, int runc= 25, const string& posit= "Datum") : 
    UFCCSetup::MotParm* mp = new UFCCSetup::MotParm(indexor, step, 0, INT_MIN, 25, vel, vel, dir, 100, 100, 25, 0, "f/16");
    (*mech)[mechName] = mp;
    //if( _verbose )
      clog<<"UFFocus::genExec> "<<pvname<<", step vel: "<<vel<<", step dir: "<<dir<<endl;
  }
  else { // limit motion parm:
  // use this ctor:
  //inline MotParm(const string& indexor, const string& posit, int steps= 0, int blash= 0,
  //               int pos= 0, int ivel= 25, int vel= 100, int dir= 1,
  //               int acc= 100, int dec= 100, int runc= 25) : 
    UFCCSetup::MotParm* mp = new UFCCSetup::MotParm(indexor, "limit", 0, 0, 0, 25, limvel, dir);
    (*mech)[mechName] = mp;
    //if( _verbose )
      clog<<"UFFocus::genExec> "<<pvname<<", seek limit vel: "<<limvel<<", limit dir: "<<dir<<endl;
  }

  // perform requested motion...
  clog<<"UFFocus::genExec> starting thread to seek focus..."<<endl;
  int stat = seek(mech, timeout, this, _car);
  if( stat < 0 ) { 
    clog<<"UFFocus::genExec> failed to start seek focus pthread..."<<endl;
    return stat;
  }

  return 1;
} // genExec

int UFFocus::validate(UFPVAttr* pva) {
  if( pva == 0 )
    return 0;

  string pvname = pva->name();
  string input = pva->valString();

  if( pvname.find("Direction") != string::npos ) {
    if( input.find("+") != string::npos || input.find("1") != string::npos ) 
      return 0; // home + ok

    if( input.find("-") != string::npos || input.find("0") != string::npos ) 
      return 0; // home - ok
  }

  deque< string >* mnames = UFCCSetup::getMechNames();
  size_t nm = mnames->size();
  for( size_t i = 0; i < nm; ++i ) {
    if( pvname.find((*mnames)[i]) != string::npos ) {
      if( input == "" )
        return 0; // clear ok
      if( input.find("null") != string::npos || input.find("Null") != string::npos || input.find("NULL") != string::npos )
        return 0; // null + ok

      char* hv = (char*)input.c_str();
      while( *hv == ' ' || *hv == '\t' )
        ++hv; // ignore white spaces

      if( isdigit(hv[0]) ) // home velocity must be alphanumric
        return 0;
    }
  }

  return -1;
} // validate


#endif // __UFFOCUS_CC__

#if !defined(__UFDCSetup_h__)
#define __UFDCSetup_h__ "$Name:  $ $Id: UFDCSetup.h,v 0.18 2006/08/29 20:16:48 hon Exp $"
#define __UFDCSetup_H__(arg) const char arg##DCSetup_h__rcsId[] = __UFDCSetup_h__;

#include "UFCAD.h"
#include "UFClientSocket.h"

#include "vector"
#include "map"

class UFDCSetup : public UFCAD {
public:
  // mce4 detector config paramaters
  // F1 had 4 array bias attributes and 32 preamp dc offsets
  // F2 will likely be the same...
  struct DetParm {
    int _sim, _bootquery, _lab, _cycletype, _expcnt, _sec, _msec, _cdsreads, _w, _h, _d, _pbc, _presets, _postsets;
    double _k; string _passwd;
    std::map<string, double>* _bias; std::map<string, double>* _preamp;
    inline DetParm(int sim= 0, int bootq= -1, int lab= 1, int ct= 10, int cnt= 1, int s= 2, int ms= -1, int rds= -1,
		   int w= 2048, int h= 2048, int d= 32, int pbc= 10, int presets= 10, int postsets= 1000,
		   double k= 75.5, const string& passwd= "",
		   std::map<string, double>* biases= 0, std::map<string, double>* preamps= 0) : 
      _sim(sim), _bootquery(bootq), _lab(lab), _cycletype(ct), _expcnt(cnt), _sec(s), _msec(ms), _cdsreads(rds),
      _w(w), _h(h), _d(d), _pbc(pbc), _presets(presets), _postsets(postsets),
      _k(k), _passwd(passwd), _bias(biases), _preamp(preamps) {}
    inline ~DetParm() { delete _bias; delete _preamp; }
    inline void clear() {
      _sim=_bootquery=_lab=_cycletype=_expcnt=_sec=_msec=_cdsreads=_w=_h=_d=_pbc=_presets=_postsets = INT_MIN;
      _k = INT_MIN; _passwd = ""; _bias = 0; _preamp = 0;
    }
    inline static DetParm* parkParm(const string& passwd= "Disable", double kelvin= 300.0)
      { DetParm* p = new DetParm(); p->clear(); p->_k= kelvin; p->_passwd= passwd; return p; }
  };

  struct BiasPreampCnt {
    size_t _nb, _np;
    inline BiasPreampCnt(size_t nb, size_t np) : _nb(nb), _np(np) {}
  };

  // thread arg:
  struct DCThrdArg { DetParm* _parms; UFGem* _rec; UFCAR* _car;
    inline DCThrdArg(DetParm* dp, UFGem* r= 0, UFCAR* c= 0) : _parms(dp), _rec(r), _car(c) {}
    inline ~DCThrdArg() { delete _parms; }
  };

  // alloc and return list of established bias and preamp values for "imaging" or "spect" modes
  // values should be 0 -- 255:
  static struct BiasPreampCnt allocBiasAndPreamps(const string& modename,
						  map<string, double>*& biases, map<string, double>*& preamps);

  inline UFDCSetup(const string& instrum= "instrum",
		   const string& host= "", int port= 52008, UFCAR* car= 0) : UFCAD(instrum+":dc:setup", car)
  { _create(host, port); }

  inline virtual ~UFDCSetup() {}

  virtual int validate(UFPVAttr* pva= 0);

  /// perform setup motion for those mechanisms with non-null step cnts or named-position inputs
  virtual int genExec(int timeout= 0);

  // thread entry func.
  static void* configMCE4(void* p= 0);
  ///general purpose mce4 func. (non datum)
  // of use here and from IS. starts a new daemn thread for every propper motion seq.
  static int config(UFDCSetup::DetParm* parms, int timeout= 10, UFGem* rec= 0, UFCAR* car= 0); 

  inline static void setHost(const string& host) { _host = host; }
  inline static void setPort(int port) { _port = port; }
  inline static string agentHostAndPort(int& port) { port = _port; return _host; }

  // available to IS:
  static UFClientSocket* _connection;

protected:
  static string _host; // agent location
  static int _port;

  // bias cmd helper
  static int _indexOfMCE4Bias(const string& name);

  // ctor helper
  void _create(const string& host, int port= 52008);
};

#endif // __UFDCSetup_h__

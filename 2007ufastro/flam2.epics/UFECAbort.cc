#if !defined(__UFECABORT_CC__)
#define __UFECABORT_CC__ "$Name:  $ $Id: UFECAbort.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFECABORT_CC__;

#include "UFECAbort.h"
#include "UFECSetup.h"
#include "UFPVAttr.h"

#include "UFPosixRuntime.h"

// ctors:

// static class funcs:
void* UFECAbort::abortion(void* p) {
  // abort really just needs?
  UFECSetup::ECThrdArg* arg = (UFECSetup::ECThrdArg*) p;
  UFECSetup::EnvParm* parms = arg->_parms;
  UFCAR* car = arg->_car;

  // simple simulation...
  clog<<"UFECAbort::aborting> ..."<<endl;
  if( parms ) {
    clog<<"UFECAbort::aborting> environment: "<<parms->_MOSKelv<<", "<<parms->_CamAKelv<<", "<<parms->_CamBKelv<<endl;
  }

  // free arg
  delete arg;

  // set car idle after 1/2 sec:
  UFPosixRuntime::sleep(0.5);
  if( car )
    car->setIdle();

  // exit daemon thread
  return p;
}

/// general purpose abort func. 

/// arg:
int UFECAbort::abort(UFECSetup::EnvParm* parms, int timeout, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec && car ) {
    UFCAR* c = rec->hasCAR();
    if( car != c ) car->setBusy(timeout);
  }

  // create and run pthread then either reset car to idle or error
  // thread should free MotParm* allocations...
  //pthread_t pmot;
  UFECSetup::ECThrdArg* arg = new UFECSetup::ECThrdArg(parms, rec, car);
  pthread_t thrid = UFPosixRuntime::newThread(UFECAbort::abortion, arg);

  return (int) thrid;
}

// non static, non virtual funcs:
// protected ctor helper:
void UFECAbort::_create() {
  UFPVAttr* pva= 0;
  string pvname = _name + ".ctrl1";
  pva = (*this)[pvname] = new UFPVAttr(pvname); // non null - abort control loop 1
  attachInOutPV(this, pva);

  pvname = _name + ".ctrl2";
  pva = (*this)[pvname] = new UFPVAttr(pvname); // non null - abort control loop 2
  attachInOutPV(this, pva);

  registerRec();
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
}

/// virtual funcs:
/// perform abort 
int UFECAbort::genExec(int timeout) {
  size_t pvcnt = size();

  if( pvcnt == 0 ) {
    clog<<"UFECAbort::genExec> np pvs?"<<endl;
    return (int) pvcnt;
  }

  string pvname = _name + ".ctrl1";
  UFPVAttr* pva = (*this)[pvname];
  string input = pva->valString(); // input is anything non-null
  if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
   clog<<"UFECAbort::genExec> abort control loop 1"<<endl;
    pva->clearVal(); // clear
  }

  pvname = _name + ".ctrl2";
  pva = (*this)[pvname];
  input = pva->valString(); // input is anything non-null
  if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
   clog<<"UFECAbort::genExec> abort control loop 2"<<endl;
    pva->clearVal(); // clear
  }

  UFECSetup::EnvParm* p = new UFECSetup::EnvParm;
  int stat = abort(p, timeout, _car);  // perform requested abort

  if( stat < 0 ) { 
    clog<<"UFAbort::genExec> failed to start abort pthread..."<<endl;
    return stat;
  }

  return stat;
} // genExec

int UFECAbort::validate(UFPVAttr* pva) {
  return 0;
} // validate

#endif // __UFECABORT_CC__

#!/bin/tcsh -f
# gemini cad/car enumerations:
set Mark = 0
set Clear = 1
set Preset = 2
set Start = 3
set Idle = 0
set Busy = 2
set Error = 3
#
set db = 'flam'
#set db = 'flam:eng'
if( "$1" != "" ) set db = "$1"
set apply = ${db}:apply
#set apply = ${db}:eng:apply
#ufcaput ${apply}.dir=1
#ufcaput ${apply}C.ival=0
#ufcaput ${db}:cc:setupC.ival=0
set carec = ${apply}C
set car = `ufcaget $carec`
if( $status != 0 ) then
  echo unable to check val. of $carec
  exit
else
  echo $carec ==  $car
endif
if( $car != 0 ) then 
  echo system busy or in error state...
  exit
endif
set carec = ${db}:cc:setupC
set car = `ufcaget $carec`
if( $status != 0 ) then
  echo unable to check val. of $carec
  exit
else
  echo $carec ==  $car
endif
if( $car != 0 ) then 
  echo system busy or in error state...
  exit
endif
ufcaput ${db}:cc:setup.DeckerStep=1111
ufcaput ${db}:cc:setup.DeckerSlewVel=200
ufcaput ${db}:cc:setup.MOSStep=2222
ufcaput ${db}:cc:setup.MOSSlewVel=200
ufcaput ${db}:cc:setup.Filter1Step=1010
ufcaput ${db}:cc:setup.Filter1SlewVel=200
ufcaput ${db}:cc:setup.Filter2Step=2020
ufcaput ${db}:cc:setup.Filter2SlewVel=200
ufcaput ${db}:cc:setup.LyotStep=3333
ufcaput ${db}:cc:setup.LyotSlewVel=200
ufcaput ${db}:cc:setup.GrismStep=4444
ufcaput ${db}:cc:setup.GrismSlewVel=200
ufcaput ${db}:cc:setup.FocusStep=5555
ufcaput ${db}:cc:setup.FocusSlewVel=200
ufcaput $apply.dir=$Start
set car = `ufcaget $carec`
while( $car == $Busy )
  ufcaget ${db}:sad:DeckerSteps
  ufcaget ${db}:sad:MOSSteps
  ufcaget ${db}:sad:Filter1Steps
  ufcaget ${db}:sad:Filter2Steps
  ufcaget ${db}:sad:LyotSteps
  ufcaget ${db}:sad:GrismSteps
  ufcaget ${db}:sad:FocusSteps
  set car = `ufcaget $carec`
end

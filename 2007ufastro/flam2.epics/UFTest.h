#if !defined(__UFTest_h__)
#define __UFTest_h__ "$Name:  $ $Id: UFTest.h,v 0.5 2005/06/03 15:09:23 hon Exp $"
#define __UFTest_H__(arg) const char arg##Test_h__rcsId[] = __UFTest_h__;

#include "UFCAD.h"

// provide cc, dc, and ec socket connection objects to test
#include "UFInstrumSetup.h"
#include "UFObsSetup.h"
#include "UFObserve.h"
#include "UFCCTest.h"
#include "UFDCTest.h"
#include "UFECTest.h"

// either abort an instrument setup or an obssetup or the ongoing observation
class UFTest : public UFCAD {
public:
  struct Parm { string _sys; UFObserve::Parm* _op; UFObsSetup::Parm* _osp; UFInstrumSetup::Parm* _isp;
    inline Parm(const string& sys= "all", 
		UFObserve::Parm* op= 0,
		UFObsSetup::Parm* osp= 0,
		UFInstrumSetup::Parm* isp= 0): _sys(sys), _op(op), _osp(osp), _isp(isp) {}
    inline ~Parm() { delete _op; delete _osp; delete _isp; }
  };

  // thread arg:
  struct ThrdArg { Parm* _parms; UFGem* _rec; UFCAR* _car;
    inline ThrdArg(Parm* p= 0, UFGem* r= 0, UFCAR* c= 0) : _parms(p), _rec(r), _car(c) {}
    inline ~ThrdArg() {}
  };
  inline UFTest(const string& instrum= "instrum") : UFCAD(instrum+":test") { _create(); }
  inline virtual ~UFTest() {}

  virtual int validate(UFPVAttr* pva= 0);
  virtual int genExec(int timeout= 0);

  static void* testAll(void* p);
  static int test(Parm* p, int timeout, UFGem* rec= 0, UFCAR* car= 0);

protected:
  // ctor helper
  void _create();
};

#endif // __UFTest_h__

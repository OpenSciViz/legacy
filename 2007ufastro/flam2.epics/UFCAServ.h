#if !defined(__UFCAServ_h__)
#define __UFCAServ_h__ "$Name:  $ $Id: UFCAServ.h,v 0.11 2006/10/10 14:21:52 hon Exp $"
#define __UFCAServ_H__(arg) const char arg##CServA_h__rcsId[] = __UFCAServ_h__;

#include "sys/types.h"
#include "sys/stat.h"
#include "fcntl.h"
#include "stdlib.h"
//#define __USE_ISOC99
#include "math.h"
//#include "ieee754.h"
 
// stdc++
#include "cstdio"
#include "iostream"
#include "string"
#include "map"
#include "multimap.h"

// portable cas -- already included from UFPV.h
#include "UFPV.h" // which includes UFPVAttr.h, which does notinclude UFGem.h

using namespace std;

/// The server class. Provides the pvExistTest() and createPV() functions as well as the function table
/** base on simple scalar example
 * all container records should register their respective pvs by insertion into global pv list
 * accessed by findPV via pvExistTest()
 */
class UFCAServ : public caServer {
public:
  UFCAServ(const char* name= 0);
  inline ~UFCAServ() {}

  // these 2 virtuals are now deprecated in the base class caServer,
  // but still of use here:
  virtual pvExistReturn pvExistTest (const casCtx& ctx, const char* pPVName);
  virtual pvCreateReturn createPV(const casCtx& ctx, const char* pPVName);
  // 'replacement' virtual:
  virtual pvAttachReturn pvAttach(const casCtx& ctx, const char* pPVName);
  //statc create helper:
  static UFPV* createPV(const string& pvname, UFCAServ* cas);

  inline static gddAppFuncTableStatus read(UFPV& pv, gdd& val) { return UFCAServ::_funcTable.read(pv, val); }
  static const UFPVAttr *findPV(const char* Name);

  // which of these are of use?
  static int createPVsfrom(std::map<string, UFPVAttr*>& pvlist);
  static int createPVsfrom(const string& file, std::map<string, UFPVAttr*>& pvlist);
  static int createPVsfrom(const std::map<string, string>& dblist, std::map<string, UFPVAttr*>& pvlist);
  //static int createPVsfrom(const string& instrum, vector< UFGem* > dbrecs);
  static int applysfrom(const string& file, std::map<string, string>& applylist);
  static int cadsfrom(const string& file, std::map<string, string>& cadlist);
  static int carsfrom(const string& file, std::map<string, string>& carlist);
  static int sadsfrom(const string& file, std::map<string, string>& sadlist);

  inline static int registerPV(UFPVAttr* pva) { return UFPVAttr::registerPV(pva); }

  // main entry into server app.:
  static int main(int argc, char** argv, char** envp);

  static bool _verbose, _vverbose;
  static string _instrum;
  static UFCAServ* _instance;

protected:
  static pthread_mutex_t* _mutex; // protect the static tables below?
  static gddAppFuncTable< UFPV > _funcTable; /// read functions
  static std::map< string, UFPV* > _createTable; /// insure unique instance
};

#endif // __UFCAServ_h__

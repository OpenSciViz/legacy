#!/bin/tcsh -f
set cwd = `pwd`
if( ! $?UFINSTALL ) then 
  setenv UFINSTALL ${cwd}/ufinst
  echo $path | grep $cwd
  if( $status != 0 ) then
    set path = ( . ${UFINSTALL}/bin $path )
  endif
endif
setenv EPICS /gemini/epics
setenv EPICS_BASE ${EPICS}/base
setenv EPICS_EXT ${EPICS}/extensions

setenv ARCH `arch`
if( "$ARCH" == "sun4" ) then
  setenv ARCH 'solaris-sparc-gnu'
endif
if( "$ARCH" == "i86pc" ) then
  setenv ARCH 'solaris-x86-gnu'
endif
if( "$ARCH" == "i686" || "$ARCH" == "i586" ) then
  setenv ARCH 'linux-x86'
endif

setenv LD_LIBRARY_PATH /share/local/lib:${EPICS_BASE}/lib/${ARCH}:${EPICS_EXT}/lib/${ARCH}:${UFINSTALL}/lib:/share/local/lib:/opt/EDTpdv

rehash

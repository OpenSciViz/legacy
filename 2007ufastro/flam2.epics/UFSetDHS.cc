#if !defined(__UFSetDHS_CC__)
#define __UFSetDHS_CC__ "$Name:  $ $Id: UFSetDHS.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFSetDHS_CC__;

#include "UFSetDHS.h"
#include "UFCAR.h"
#include "UFPVAttr.h"
#include "UFPosixRuntime.h"

// ctors:

// static class funcs:
void* UFSetDHS::datalabel(void* p) {
  // label really just needs the mech. names
  UFSetDHS::ThrdArg* arg = (UFSetDHS::ThrdArg*) p;
  UFSetDHS::Parm* parms = arg->_parms;
  UFCAR* car = arg->_car;
  UFGem* rec = arg->_rec; // should be either a cad or apply
  string name = "anon";
  if( rec )
    name = rec->name();

  // simple simulation...
  clog<<"UFSetDHS::labeling>... "<<name<<endl;
  if( parms && parms->_op && parms->_osp && parms->_isp ) {
    clog<<"UFSetDHS::labeling> obs timeout: "<<parms->_op->_timeout<<", exptime: "<<parms->_osp->_exptime<<endl;
    if( parms->_isp->_mparms && !parms->_isp->_mparms)
      clog<<"UFSetDHS::labeling> motors: "<<parms->_isp->_mparms->size()<<endl;
    if( parms->_isp->_eparms )
      clog<<"UFSetDHS::labeling> environment: "<<parms->_isp->_eparms->_MOSKelv
	  <<", "<<parms->_isp->_eparms->_CamAKelv<<", "<<parms->_isp->_eparms->_CamBKelv<<endl;
    if( parms->_osp->_dparms )
      clog<<"UFSetDHS::labeling> detector pixels: "<<parms->_osp->_dparms->_w*parms->_osp->_dparms->_h<<endl;
  }
  // free arg
  delete arg;

  if( _sim ) {
    // set car idle after 1/2 sec:
    UFPosixRuntime::sleep(0.5);
    if( car )
      car->setIdle();
    if( rec ) {
      UFCAR* c = rec->hasCAR();
      if( c != 0 && c != car ) c->setIdle();
    }
  }

  // exit daemon thread
  return p;
}

/// general purpose label func. 

/// arg: mechName, motparm*
int UFSetDHS::label(UFSetDHS::Parm* parms, int timeout, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec && car ) {
    UFCAR* c = rec->hasCAR();
    if( car != c ) car->setBusy(timeout);
  }

  // create and run pthread to label whatever is currently being performed then either reset car to idle or error
  // thread should free the tParm* allocations...
  if( _sim ) {
    UFSetDHS::ThrdArg* arg = new UFSetDHS::ThrdArg(parms, rec, car);
    pthread_t thrid = UFPosixRuntime::newThread(UFSetDHS::datalabel, arg);
    return (int) thrid;
  }

  return 0; 
}

// non static, non virtual funcs:
// protected ctor helper:
void UFSetDHS::_create() {
  string pvname = _name + ".DataLabel";
  UFPVAttr* pva = (*this)[pvname] = new UFPVAttr(pvname); // any non null string should indicate abort action mark
  attachInputPV(pva); 

  pvname = _name + ".Qlook";
  pva = (*this)[pvname] = new UFPVAttr(pvname); // any non null string should indicate abort action mark
  attachInputPV(pva); 

  registerRec();
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
}

/// virtual funcs:
/// perform abort mechanisms with non-null step cnts or named-position inputs
int UFSetDHS::genExec(int timeout) {
  size_t pvcnt = size();

  if( pvcnt == 0 ) {
    clog<<"UFSetDHS::genExec> np pvs?"<<endl;
    return (int) pvcnt;
  }

  string pvname = _name + ".DataLabel";
  UFPVAttr* pva = (*this)[pvname];
  string input = pva->valString(); // input is anything non-null
  if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
   clog<<"UFSetDHS::genExec> datalabel: "<<input<<endl;
    pva->clearVal(); // clear
  }
  pvname = _name + ".Qlook";
  pva = (*this)[pvname];
  input = pva->valString(); // input is anything non-null
  if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
   clog<<"UFSetDHS::genExec> qlook: "<<input<<endl;
    pva->clearVal(); // clear
  }

  UFSetDHS::Parm* p = new UFSetDHS::Parm;
  int stat = label(p, timeout, this, _car);  // perform requested abort

  if( stat < 0 ) { 
    clog<<"UFSetDHS::genExec> failed to start label pthread..."<<endl;
    return stat;
  }

  return stat;
} // genExec

int UFSetDHS::validate(UFPVAttr* pva) {
  return 0;
} // validate


#endif // __UFSetDHS_CC__

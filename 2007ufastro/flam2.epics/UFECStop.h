#if !defined(__UFECSTOP_h__)
#define __UFECSTOP_h__ "$Name:  $ $Id: UFECStop.h,v 0.4 2004/06/22 14:46:56 hon beta $"
#define __UFECSTOP_H__(arg) const char arg##ECStop_h__rcsId[] = __UFECSTOP_h__;

#include "UFCAD.h"
#include "UFECSetup.h"

class UFECStop : public UFCAD {
public:
  inline UFECStop(const string& instrum= "instrum:ec:stop") : UFCAD(instrum+":ec:stop") { _create(); }
  inline virtual ~UFECStop() {}

  virtual int validate(UFPVAttr* pva= 0);
  virtual int genExec(int val= 0);

  static void* stopit(void* p);
  static int stop(UFECSetup::EnvParm* mparms, int timeout, UFGem* rec= 0, UFCAR* car= 0);

protected:
  // ctor helper
  void _create();
};

#endif // __UFECSTOP_h__

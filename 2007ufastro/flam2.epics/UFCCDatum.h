#if !defined(__UFCCDatum_h__)
#define __UFCCDatum_h__ "$Name:  $ $Id: UFCCDatum.h,v 0.9 2007/01/18 19:13:40 hon Exp $"
#define __UFCCDatum_H__(arg) const char arg##CCDatum_h__rcsId[] = __UFCCDatum_h__;

#include "UFCAD.h"
#include "UFCCSetup.h"

class UFCCDatum : public UFCAD {
public:
  inline UFCCDatum(const string& instrum= "instrum") : UFCAD(instrum+":cc:datum") { _create(); }
  inline UFCCDatum(const string& instrum, UFCAR* car) : UFCAD(instrum+":cc:datum", car) { _create(); }
  inline virtual ~UFCCDatum() {}

  virtual int validate(UFPVAttr* pva= 0);
  virtual int genExec(int timeout= 0);

  // helper for input parms:
  int homeListAlloc(std::map< string, UFCCSetup::MotParm* >& mechs, bool all= false);

  // pthread func.
  // atomic datum (backlach == 0)
  // the void* param and return value is the required signature for a posix thread entry func:
  static void* gohome(void* p);
  static void* originate(void* p);

  // support compound home/datum (backlach != 0):
  // this is deprecated for F2
  static int _homestephome(const string& mechname, const UFCCSetup::MotParm& mp, deque<string>& cmds);
  // wrapper for F2 compound motion funcs written by crag warner, helper for gohome  
  static int _homeF2(const string& mechname, const UFCCSetup::MotParm& mp, deque<string>& cmds);
  
  // used by genExec to start datum thread
  static int home(std::map< string, UFCCSetup::MotParm* >* mparms, int timeout, UFGem* rec= 0, UFCAR* car= 0);


  // available to IS
  static int homeAll(int timeout, UFGem* rec= 0, UFCAR* car= 0);
  static int origin(const string& option, int timeout, UFGem* rec= 0, UFCAR* car= 0);

  static int _DatumCnt;

protected:
  // ctor helper
  void _create();
};

#endif // __UFCCDatum_h__

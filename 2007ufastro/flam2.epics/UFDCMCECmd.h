#if !defined(__UFDCMCECmd_h__)
#define __UFDCMCECmd_h__ "$Name:  $ $Id: UFDCMCECmd.h,v 0.3 2006/02/20 21:52:34 hon Exp $"
#define __UFDCMCECmd_H__(arg) const char arg##DCMCECmd_h__rcsId[] = __UFDCMCECmd_h__;

#include "UFCAD.h"
#include "UFDCSetup.h"

#include "vector"

class UFDCMCECmd : public UFCAD {
  // support mce4 detector agent command seq. -- individual 'raw mce4 cmds', as well as
  // obsconf and acq 'compound seq.' cmds 
public:
  // mce4 detector controller 'raw' commad text thread arg:
  struct MCEThrdArg { string _rawcmd; int _EOFTimer; UFGem* _rec; UFCAR* _car;
    inline MCEThrdArg(const string cmd, int timer= -1,
		      UFGem* r= 0, UFCAR* c= 0) : _rawcmd(cmd), _EOFTimer(timer), _rec(r), _car(c) {}
    inline ~MCEThrdArg() {}
  };
  // allow ObsSetup and setDHS, and Observe CADs to (re)set these:
  // note that all of these except the wcpix are mced/edtd/dhs acq-exposure/observation options
  // that can/should be passed between epics db and agent daemons as either command bundles or obsconf objects.
  // the wcpix values need only be put into the epics db sad... 
  static float _exptime;
  static string _dhslabel, _dhsqlook, _nfshost, _datadir, _fitsfile, _lutfile, _pngjpegfile, _datamode, _usrinfo, _comment;
  static int _expcnt, _index, _ds9, _wcpix1, _wcpix2;

  // for use by IS: opt == start, stop, abort
  static int obsConfCmds(deque< string >& obscmds);
  static int submitAcq(const string& opt= "Start", const string& datalabel= "",
		       int index= 1, int timeout= 10, UFGem* rec= 0, UFCAR* car= 0);

  inline UFDCMCECmd(const string& instrum= "instrum", const string& host= "", int port= 52008,
		    UFCAR* car= 0) : UFCAD(instrum+":dc:mcecmd", car) { _create(host, port); }
 
  inline virtual ~UFDCMCECmd() {}

  virtual int validate(UFPVAttr* pva= 0);
  virtual int genExec(int timeout= 0);

  // thread entry (posix) func.
  static void* submitMCE4(void* p= 0);
  ///general purpose mce4 func. (non datum)
  // of use here and from IS. starts a new daemn thread for every propper motion seq.
  static int startThread(const string& cmd, int eoftimer, int timeout= 10, UFGem* rec= 0, UFCAR* car= 0); 

  inline static void setHost(const string& host) { UFDCSetup::setHost(host); }
  inline static void setPort(int port) { UFDCSetup::setPort(port); }
  inline static string agentHostAndPort(int& port) { return UFDCSetup::agentHostAndPort(port); }

protected:
  // ctor helper
  void _create(const string& host, int port= 52008);
};

#endif // __UFDCMCECmd_h__

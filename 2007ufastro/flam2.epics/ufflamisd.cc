#if !defined(__ufflamisd_cc__)
#define __ufflamisd_cc__ "$Name:  $ $Id: ufflamisd.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __ufflamisd_cc__;
 
#include "UFCAServ.h"
//__UFCAServ_H__(ufflamisd_cc);
 
int main(int argc, char** argv, char** envp) {
  // just call UFFlamisd main:
  return UFCAServ::main(argc, argv, envp);
}
 
#endif // __ufflamisd_cc__

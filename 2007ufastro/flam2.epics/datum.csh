#!/bin/tcsh -f
# gemini cad/car enumerations:
set Mark = 0
set Clear = 1
set Preset = 2
set Start = 3
set Idle = 0
set Busy = 2
set Error = 3
set mechname = all
#
#set db = 'flam:eng'
set db = 'flam'
if( "$1" == "-help" ) then
  echo 'datum.csh [-help] [dbname] [all/mechname]'
  exit
endif
#
if( "$1" != "" ) set db = "$1"
set apply = ${db}:apply
#set apply = ${db}:eng:apply
set carec = ${apply}C
set car = `ufcaget $carec`
if( $status != 0 ) then
  echo unable to check val. of $carec
  exit
else
  echo $carec ==  $car
endif
if( $car != 0 ) then 
  echo system busy or in error state...
  exit
endif
set carec = ${db}:cc:datumC
set car = `ufcaget $carec`
if( $status != 0 ) then
  echo unable to check val. of $carec
  exit
else
  echo $carec ==  $car
endif
if( $car != 0 ) then 
  echo system busy or in error state...
  exit
endif
#
if( $mechname == "all" ) then
  ufcaput ${db}:cc:datum.Decker=$Preset
  ufcaput ${db}:cc:datum.DeckerVel=100
  ufcaput ${db}:cc:datum.MOS=$Preset
  ufcaput ${db}:cc:datum.MOSVel=100
  ufcaput ${db}:cc:datum.Filter1=$Preset
  ufcaput ${db}:cc:datum.Filter1Vel=100
  ufcaput ${db}:cc:datum.Filter2=$Preset
  ufcaput ${db}:cc:datum.Filter2Vel=100
  ufcaput ${db}:cc:datum.Focus=$Preset
  ufcaput ${db}:cc:datum.FocusVel=100
  ufcaput ${db}:cc:datum.Grism=$Preset
  ufcaput ${db}:cc:datum.GrismVel=100
  ufcaput ${db}:cc:datum.Lyot=$Preset
  ufcaput ${db}:cc:datum.LyotVel=100
endif
ufcaput ${apply}.dir=$Start

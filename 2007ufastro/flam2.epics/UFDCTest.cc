#if !defined(__UFDCTEST_CC__)
#define __UFDCTEST_CC__ "$Name:  $ $Id: UFDCTest.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFDCTEST_CC__;

#include "UFDCTest.h"
#include "UFDCSetup.h"
#include "UFPVAttr.h"

#include "UFPosixRuntime.h"

// ctors:

// static class funcs:
void* UFDCTest::thrdFunc(void* p) {
  // sim == 0 cmd transaction will stop sim 1 (continous), sim 2 (single frame),
  // sim 3 (continuous) and sim 4 (continuous
  // sims 5-8 must be started with a 'run' command, which must be stopped via 
  // the stop command
  UFDCSetup::DCThrdArg* arg = (UFDCSetup::DCThrdArg*) p;
  UFDCSetup::DetParm* parm = arg->_parms;
  //UFGem* rec = arg->_rec;
  UFCAR* car = arg->_car;

  // simple simulation...
  clog<<"UFDCTest::simMCE4>..."<<endl;
  if( parm ) {
    clog<<"UFTest::simMCE4> detector pixels: "<<parm->_w * parm->_h<<endl;
  }

  // free arg
  delete arg;

  // set car idle after 1/2 sec:
  UFPosixRuntime::sleep(0.5);
  if( car )
    car->setIdle();

  // exit daemon thread
  return p;
}

/// general purpose test/sim func. 

/// arg:
int UFDCTest::newThrd(UFDCSetup::DetParm* parms, int timeout, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec && car ) {
    UFCAR* c = rec->hasCAR();
    if( car != c ) car->setBusy(timeout);
  }

  // create and run pthread and then either reset car to idle or error
  // thread should free MotParm* allocations...
  //pthread_t pmot;
  UFDCSetup::DCThrdArg* arg = new UFDCSetup::DCThrdArg(parms, rec, car);
  pthread_t thrid = UFPosixRuntime::newThread(UFDCTest::thrdFunc, arg);

  return (int) thrid;
}

// non static, non virtual funcs:
// protected ctor helper:
void UFDCTest::_create() {
  string pvname = _name + ".MCESimMode"; // 0 -- turns off sim, 1 -- 8 are currently supported by mce4
  UFPVAttr* pva = (*this)[pvname] = new UFPVAttr(pvname, 0);
  attachInOutPV(this, pva);

  pvname = _name + ".MCEBootQuery"; // set SAD: flam:sad:MCEBooted -1, 0, 1, ?
  pva = (*this)[pvname] = new UFPVAttr(pvname, 0);
  attachInOutPV(this, pva);

  registerRec();
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
}

/// virtual funcs:
/// perform detector subsystem test
int UFDCTest::genExec(int timeout) {
  size_t pvcnt = size();

  if( pvcnt == 0 ) {
    clog<<"UFDCTest::genExec> np pvs?"<<endl;
    return (int) pvcnt;
  }

  // sim == 0 cmd transaction will stop sim 1 (continous), sim 2 (single frame),
  // sim 3 (continuous) and sim 4 (continuous
  // sims 5-8 must be started with a 'run' command, which must be stopped via 
  // the stop command
  int sim= INT_MIN;
  string pvname = _name + ".MCESimMode";
  UFPVAttr* pva = (*this)[pvname];
  if( pva != 0 ) {
    if( pva->isSet() ) {
      sim = (int)pva->getNumeric();
      clog<<"UFDCTest::genExec> sim mode: "<<sim<<endl;
      pva->clearVal(); // clear
    }
  }
  int bootq= INT_MIN;
  pvname = _name + ".MCEBootQuery";
  pva = (*this)[pvname];
  if( pva != 0 ) {
    if( pva->isSet() ) {
      bootq = (int)pva->getNumeric();
      clog<<"UFDCTest::genExec> sim mode: "<<bootq<<endl;
      pva->clearVal(); // clear
    }
  }

  UFDCSetup::DetParm* p = new UFDCSetup::DetParm(sim, bootq);
  int stat = newThrd(p, timeout, this, _car);  // perform requested simulation test

  if( stat < 0 ) { 
    clog<<"UFTest::genExec> failed to start sim test pthread..."<<endl;
    return stat;
  }

  return stat;
} // genExec

int UFDCTest::validate(UFPVAttr* pva) {
  return 0;
} // validate

#endif // __UFDCTEST_CC__

#if !defined(__UFECDatum_h__)
#define __UFECDatum_h__ "$Name:  $ $Id: UFECDatum.h,v 0.3 2003/11/13 21:11:41 hon beta $"
#define __UFECDatum_H__(arg) const char arg##ECDatum_h__rcsId[] = __UFECDatum_h__;

#include "UFCAD.h"
#include "UFECSetup.h"

class UFECDatum : public UFCAD {
public:
  inline UFECDatum(const string& instrum= "instrum") : UFCAD(instrum+":ec:datum") { _create(); }
  inline UFECDatum(const string& instrum, UFCAR* car) : UFCAD(instrum+":ec:datum", car) { _create(); }
  inline virtual ~UFECDatum() {}

  virtual int validate(UFPVAttr* pva= 0);
  virtual int genExec(int timeout= 0);

  // genExec invokes this, and so can ECSetup...
  static int datum(UFECSetup::EnvParm* parms, int timeout, UFGem* rec= 0, UFCAR* car= 0);
  // defaults
  static int datum(int timeout, UFGem* rec= 0, UFCAR* car= 0);

  // pthread func.
  static void* datumEnv(void* p);

protected:
  // ctor helper
  void _create();
};

#endif // __UFECDatum_h__

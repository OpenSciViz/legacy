#if !defined(__UFCCInit_h__)
#define __UFCCInit_h__ "$Name:  $ $Id: UFCCInit.h,v 0.7 2005/07/28 17:30:06 hon Exp $"
#define __UFCCInit_H__(arg) const char arg##CCInit_h__rcsId[] = __UFCCInit_h__;

#include "UFCAD.h"
#include "UFSIR.h"

// init the _connection socket here
#include "UFCCSetup.h"

class UFCCInit : public UFCAD {
public:
  inline UFCCInit(const string& instrum= "instrum") : UFCAD(instrum+":cc:init")
    { if( _datumCnt == 0 ) _datumCnt = new string(instrum+":sad:DatumCnt"); _create(); }

  inline virtual ~UFCCInit() {}

  virtual int validate(UFPVAttr* pva= 0);
  virtual int genExec(int val= 0);

  // thread that performs portescap agent connection
  static void* connection(void* p);

  // start connection thread
  static int connect(std::map< string, UFCCSetup::MotParm* >* mparms,
		     int timeout, UFGem* rec= 0, UFCAR* car= 0, const string& option= "null");

  // name of DatumCnt SAD:
  static string* _datumCnt; // make this a string ptr to avoid linux coredump of static string attributes
  static void _clearDatumCnt();

protected:
  // ctor helper
  void _create();
};

#endif//  __UFCCInit_h__

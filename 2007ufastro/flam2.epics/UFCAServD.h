#if !defined(__UFCAServD_h__)
#define __UFCAServD_h__ "$Name:  $ $Id: UFCAServD.h,v 0.1 2004/10/13 19:36:02 hon Exp $"
#define __UFCAServD_H__(arg) const char arg##CServA_h__rcsId[] = __UFCAServD_h__;

#include "sys/types.h"
#include "sys/stat.h"
#include "fcntl.h"
#include "stdlib.h"
//#define __USE_ISOC99
#include "math.h"
//#include "ieee754.h"
 
// stdc++
#include "cstdio"
#include "iostream"
#include "string"
#include "map"
#include "multimap.h"

#include "UFDaemon.h" // provide convenient runtime funcs. from uflib
#include "UFCAServ.h" // which include UFPV.h which includes UFPVAttr.h, which does notinclude UFGem.h

class UFCAServD : public virtual UFDaemon, public virtual UFCAServ {
public:
  inline UFCAServD(int argc, char** argv, char** envp, char* name= 0) : UFDaemon(argc, argv, envp), UFCAServ(name) {}
  inline ~UFCAServD() {}

  // main entry into server app.:
  static int main(int argc, char** argv, char** envp, char* name= 0);
  inline virtual string description() const { return __UFCAServD_h__; }

};

#endif // __UFCAServD_h__

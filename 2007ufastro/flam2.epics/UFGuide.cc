#if !defined(__UFGuide_CC__)
#define __UFGuide_CC__ "$Name:  $ $Id: UFGuide.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFGuide_CC__;

#include "UFGuide.h"
#include "UFCAR.h"
//#include "UFCCGuide.h"
//#include "UFDCGuide.h"
//#include "UFECGuide.h"
#include "UFPVAttr.h"
#include "UFPosixRuntime.h"

// ctors:

// static class funcs:
void* UFGuide::seekAll(void* p) {
  // seek really just needs the mech. names
  UFGuide::ThrdArg* arg = (UFGuide::ThrdArg*) p;
  UFGuide::Parm* parms = arg->_parms;
  UFCAR* car = arg->_car;
  UFGem* rec = arg->_rec; // should be either a cad or apply
  string name = "anon";
  if( rec )
    name = rec->name();

  // simple simulation...
  clog<<"UFGuide::seeking>... "<<name<<endl;
  if( parms && parms->_op && parms->_osp && parms->_isp ) {
    clog<<"UFGuide::seeking> obs timeout: "<<parms->_op->_timeout<<", exptime: "<<parms->_osp->_exptime<<endl;
    if( parms->_isp->_mparms && !parms->_isp->_mparms)
      clog<<"UFGuide::seeking> motors: "<<parms->_isp->_mparms->size()<<endl;
    if( parms->_isp->_eparms )
      clog<<"UFGuide::seeking> environment: "<<parms->_isp->_eparms->_MOSKelv
	  <<", "<<parms->_isp->_eparms->_CamAKelv<<", "<<parms->_isp->_eparms->_CamBKelv<<endl;
    if( parms->_osp->_dparms )
      clog<<"UFGuide::seeking> detector pixels: "<<parms->_osp->_dparms->_w*parms->_osp->_dparms->_h<<endl;
  }
  // free arg
  delete arg;

  if( _sim ) {
    // set car idle after 1/2 sec:
    UFPosixRuntime::sleep(0.5);
    if( car )
      car->setIdle();
    if( rec ) {
      UFCAR* c = rec->hasCAR();
      if( c != 0 && c != car ) c->setIdle();
    }
  }

  // exit daemon thread
  return p;
}

/// general purpose seek func. 

/// arg: mechName, motparm*
int UFGuide::seek(UFGuide::Parm* parms, int timeout, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec && car ) {
    UFCAR* c = rec->hasCAR();
    if( car != c ) car->setBusy(timeout);
  }

  // create and run pthread to seek whatever is currently being performed then either reset car to idle or error
  // thread should free the tParm* allocations...
  if( _sim ) {
    UFGuide::ThrdArg* arg = new UFGuide::ThrdArg(parms, rec, car);
    pthread_t thrid = UFPosixRuntime::newThread(UFGuide::seekAll, arg);
    return (int) thrid;
  }

  //UFCCGuide::seekAll(timeout, rec, car);
  //UFDCGuide::seekAll(timeout, rec, car);
  //UFECGuide::seekAll(timeout, rec, car);
  return 0;
}

// non static, non virtual funcs:
// protected ctor helper:
void UFGuide::_create() {
  string pvname = _name + ".mode";
  UFPVAttr* pva = (*this)[pvname] = new UFPVAttr(pvname); // any non null string should indicate seek action
  attachInOutPV(this, pva); 
  registerRec();
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
}

/// virtual funcs:
/// perform seek mechanisms with non-null step cnts or named-position inputs
int UFGuide::genExec(int timeout) {
  size_t pvcnt = size();

  if( pvcnt == 0 ) {
    clog<<"UFGuide::genExec> np pvs?"<<endl;
    return (int) pvcnt;
  }

  string pvname = _name + ".mode";
  UFPVAttr* pva = (*this)[pvname];
  string input = pva->valString(); // input is anything non-null
  if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
    clog<<"UFGuide::genExec> mode: "<<input<<endl;
    pva->clearVal(); // clear
  }
  UFGuide::Parm* p = new UFGuide::Parm;
  int stat = seek(p, timeout, this, _car);  // perform requested seek

  if( stat < 0 ) { 
    clog<<"UFGuide::genExec> failed to start seek pthread..."<<endl;
    return stat;
  }

  return stat;
} // genExec

int UFGuide::validate(UFPVAttr* pva) {
  return 0;
} // validate


#endif // __UFGuide_CC__

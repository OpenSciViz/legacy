#if !defined(__UFPause_CC__)
#define __UFPause_CC__ "$Name:  $ $Id: UFPause.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFPause_CC__;

#include "UFPause.h"
#include "UFCAR.h"
//#include "UFCCPause.h"
//#include "UFDCPause.h"
//#include "UFECPause.h"
#include "UFPVAttr.h"
#include "UFPosixRuntime.h"

// ctors:

// static class funcs:
void* UFPause::pauseSys(void* p) {
  // pause really just needs the mech. names
  UFPause::ThrdArg* arg = (UFPause::ThrdArg*) p;
  UFPause::Parm* parms = arg->_parms;
  UFCAR* car = arg->_car;
  UFGem* rec = arg->_rec; // should be either a cad or apply
  string name = "anon";
  if( rec )
    name = rec->name();

  // simple simulation...
  clog<<"UFPause::pauseing>... "<<name<<endl;
  if( parms && parms->_op && parms->_osp && parms->_isp ) {
    clog<<"UFPause::pauseing> obs timeout: "<<parms->_op->_timeout<<", exptime: "<<parms->_osp->_exptime<<endl;
    if( parms->_isp->_mparms && !parms->_isp->_mparms)
      clog<<"UFPause::pauseing> motors: "<<parms->_isp->_mparms->size()<<endl;
    if( parms->_isp->_eparms )
      clog<<"UFPause::pauseing> environment: "<<parms->_isp->_eparms->_MOSKelv
	  <<", "<<parms->_isp->_eparms->_CamAKelv<<", "<<parms->_isp->_eparms->_CamBKelv<<endl;
    if( parms->_osp->_dparms )
      clog<<"UFPause::pauseing> detector pixels: "<<parms->_osp->_dparms->_w*parms->_osp->_dparms->_h<<endl;
  }
  // free arg
  delete arg;

  if( _sim ) {
    // set car idle after 1/2 sec:
    UFPosixRuntime::sleep(0.5);
    if( car )
      car->setIdle();
    if( rec ) {
      UFCAR* c = rec->hasCAR();
      if( c != 0 && c != car ) c->setIdle();
    }
  }

  // exit daemon thread
  return p;
}

/// general purpose pause func. 

/// arg: mechName, motparm*
int UFPause::pause(UFPause::Parm* parms, int timeout, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec && car ) {
    UFCAR* c = rec->hasCAR();
    if( car != c ) car->setBusy(timeout);
  }

  // create and run pthread to pause whatever is currently being performed then either reset car to idle or error
  // thread should free the tParm* allocations...
  if( _sim ) {
    UFPause::ThrdArg* arg = new UFPause::ThrdArg(parms, rec, car);
    pthread_t thrid = UFPosixRuntime::newThread(UFPause::pauseSys, arg);
    return (int) thrid;
  }

  //UFCCPause::pauseAll(timeout, rec, car);
  //UFDCPause::pauseAll(timeout, rec, car);
  //UFECPause::pauseAll(timeout, rec, car);
  return 0;
}

// non static, non virtual funcs:
// protected ctor helper:
void UFPause::_create() {
  string pvname = _name + ".DataLabel";
  UFPVAttr* pva = (*this)[pvname] = new UFPVAttr(pvname); // any non null string should indicate pause action 
  attachInOutPV(this, pva); 
  registerRec();
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
}

/// virtual funcs:
void UFPause::setBusy(size_t timeout) { 
  if( _paused )
    clog<<"UFPause::setBusy> note that system is currenlty paused, so this just increases/resets the timeout, pending completion or timeout in heartbeat ticks: "<<timeout<<endl;
  _paused = true;
  if( timeout > 0 ) _timeout = timeout;
  //insertTO(this); only CARs in TOlist
}

int UFPause::start(int timeout) { 
  if( _timeout > 0 ) {
    clog<<"UFPause::start> "<<_name<<", busy? timeout: "<<_timeout<<endl;
    return -1;
  }
  if( !_mark ) {
    clog<<"UFPause::start> "<<_name<<", not marked! timeout: "<<_timeout<<endl;
    return 0;
  }
  if( _paused ) {
    clog<<"UFPause::start> "<<_name<<", system is allready  paused, timeout: "<<_timeout<<endl;
    return 0;
  }

  // Ok to start processing, clear mark and call genExec
  // note these are virtual funcs...
  unmark();
  // marking this should have called validate, no need to do it here:
  /*
  if(timeout > 0) val = validate(); // otherwise assume valid input 
  if( val < 0 ) { // invalid input
    string msg = errMsg(val); setErr(msg, val);
    return val; 
  }
  */
  // calling setBusy before genExec allows all inputs to be copied to their outputs readily
  // before genExec (optionally) clears input(s).
  setBusy(timeout);
  // lock(); int val = genExec(timeout); unlock(); // safer to wrap the genExec in mutex lock?
  int val = genExec(timeout); // don't wrap the genExec in mutex lock?
  if( timeout <= 0 && val >= 0 )
    setIdle();

  return val;
} // start

/// perform pause mechanisms with non-null step cnts or named-position inputs
int UFPause::genExec(int timeout) {
  size_t pvcnt = size();

  if( pvcnt == 0 ) {
    clog<<"UFPause::genExec> np pvs?"<<endl;
    return (int) pvcnt;
  }

  string pvname = _name + ".DataLabel";
  UFPVAttr* pva = (*this)[pvname];
  if( pva != 0 ) {
    string input = pva->valString(); // input is anything non-null
    if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
      clog<<"UFPause::genExec> datalabel: "<<input<<endl;
      pva->clearVal(); // clear
    }
  }
  UFPause::Parm* p = new UFPause::Parm;
  int stat = pause(p, timeout, this, _car);  // perform requested pause

  if( stat < 0 ) { 
    clog<<"UFPause::genExec> failed to start pause pthread..."<<endl;
    return stat;
  }

  return stat;
} // genExec

int UFPause::validate(UFPVAttr* pva) {
  return 0;
} // validate


#endif // __UFPause_CC__

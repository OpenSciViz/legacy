#if !defined(__UFDCPark_DC__)
#define __UFDCPark_DC__ "$Name:  $ $Id: UFDCPark.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFDCPark_DC__;

#include "UFDCPark.h"
#include "UFDCSetup.h"
#include "UFPVAttr.h"
#include "UFCAServ.h"
#include "UFPosixRuntime.h"
#include "UFStrings.h"

// ctors:

// static class funcs:
void* UFDCPark::thrdFunc(void* p) {
  // seek really just needs the mech. names
  UFDCSetup::DCThrdArg* arg = (UFDCSetup::DCThrdArg*) p;
  UFDCSetup::DetParm* parm = arg->_parms;
  UFCAR* car = arg->_car;
  UFGem* rec = arg->_rec; // should be either a cad or apply
  string name = UFCAServ::_instrum + ":dc:park";
  if( rec )
    name = rec->name(); 

  string carname = UFCAServ::_instrum + ":dc:parkC";
  if( rec ) {
    UFCAR* rcar = rec->hasCAR();
    if( rcar )
      carname = rcar->name();
  }
  else if( car ) {
    carname = car->name();
  }
  carname += ".IVAL";

  /*
  int err= 0;
  string errmsg;
  */
  // simple simulation...
  //clog<<"UFDCPark::thrdFunc> ..."<<endl;
  if( parm ) {
    clog<<"UFSeek::thrdFUnc> kelivn: "<<parm->_k<<", passwd: "<<parm->_passwd<<endl;
  }

  if( _sim ) {
    // set car idle after 1/2 sec:
    UFPosixRuntime::sleep(0.5);
    if( car )
      car->setIdle();
    delete arg;
    return p;
  }
  bool validsoc = connected(UFDCSetup::_connection);
  if( !_sim && !validsoc ) {
    clog<<"UFDCSetup::thrdFunc> not connected to UFMCE agent, need to (re)init..."<<endl;
    // and free the arg & parms
    delete arg;
    return p;
  }
  
  string cmd = "CAR";
  string cmdpar = carname;
  deque< string > cmdvec;
  if( !parm->_passwd.empty() && parm->_passwd != "null" ) {
    cmd = "Raw "; cmd += "ExpCnt!!ErrSetup ";
    cmdpar = "APWB ENGINEERING";
    strstream s; s<<"APWB "<<parm->_passwd<<ends;
    cmdpar = s.str(); delete s.str();
    cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
    //if( _verbose )
    clog<<"UFDCSetup::thrdFunc> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;
  }

  cmd = "Raw "; cmd += "Kelvin!!ErrDatum ";
  cmdpar = "ARRAY_TEMP 300.0"; // inform mce4 that array is cold and can be turned on and clocked
  if( parm->_k > 0 ) {
    strstream s; s<<"ARRAY_TEMP "<<parm->_k<<ends;
    cmdpar = s.str(); delete s.str();
  }
  cmdvec.push_back(cmd); cmdvec.push_back(cmdpar);
  //if( _verbose )
    clog<<"UFDCDatum::thrdFunc> cmd: "<<cmd<<", cmdimpl: "<<cmdpar<<endl;

  if( cmdvec.size() > 0 ) {
    cmdvec.push_front(cmdpar); cmdvec.push_front(cmd);
  }

  string errmsg;
  UFStrings req(name, cmdvec);
  int ns = UFDCSetup::_connection->send(req);
  if( ns <= 0 ) {
    clog<<"UFDCSetup::thrdFunc> failed send req: "<< req.name() << " " << req.timeStamp() <<endl;
    errmsg = "Failed MCE4 Park";
    if( car )
      car->setErr(errmsg);
  }

  // free arg
  delete arg;

  // exit daemon thread
  return p;
}

/// general purpose seek func. 

/// arg: mechName, motparm*
int UFDCPark::newThrd(UFDCSetup::DetParm* parms, int timeout, UFGem* rec,  UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec && car ) {
    UFCAR* c = rec->hasCAR();
    if( car != c ) car->setBusy(timeout);
  }

  // create and run pthread to move mechanisms and then either reset car to idle or error
  // thread should free MotParm* allocations...
  //pthread_t pmot;
  UFDCSetup::DCThrdArg* arg = new UFDCSetup::DCThrdArg(parms, rec, car);
  pthread_t thrid = UFPosixRuntime::newThread(UFDCPark::thrdFunc, arg);

  return (int) thrid;
}

// non static, non virtual funcs:
// protected ctor helper:
void UFDCPark::_create() {
  UFPVAttr* pva= 0;
  string pvname = _name + ".Passwd";
  pva = (*this)[pvname] = new UFPVAttr(pvname, "Disable"); // diable any prior passwd (disallow bias changes)
  attachInOutPV(this, pva);
  
  pvname = _name + ".Kelvin";
  pva = (*this)[pvname] = new UFPVAttr(pvname, "300.0"); // high temperature reading should turn off biases
  attachInOutPV(this, pva);

  registerRec();
  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
}

/// virtual funcs:
/// stop current MCE4 action set cycle type to 0 (CT 0)
int UFDCPark::genExec(int timeout) {
  size_t pvcnt = size();

  if( pvcnt == 0 ) {
    clog<<"UFDCPark::genExec> np pvs?"<<endl;
    return (int) pvcnt;
  }

  UFPVAttr* pva= 0;
  string pvname = _name + ".Passwd";
  pva = (*this)[pvname];
  string pwd;
  if( pva != 0 ) {
    if( pva->isSet() ) {
      pwd = pva->valString();
      clog<<"UFDCPark::genExec> passwd: "<<pwd<<endl;
      //pva->clearVal(); // clear
    }
  }

  pvname = _name + ".Kelvin";
  pva = (*this)[pvname];
  double k = INT_MIN;
  if( pva != 0 ) {
    if( pva->isSet() ) {
      double k = pva->getNumeric();
      clog<<"UFDCPark::genExec> kelvin: "<<k<<endl;
      //pva->clearVal(); // clear
    }
  }


  UFDCSetup::DetParm* p = UFDCSetup::DetParm::parkParm(pwd, k);
  int stat = newThrd(p, timeout, _car);  // perform requested seek

  if( stat < 0 ) { 
    clog<<"UFSeek::genExec> failed to start seek park pthread..."<<endl;
    return stat;
  }

  return stat;
} // genExec

int UFDCPark::validate(UFPVAttr* pva) {
  return 0;
} // validate

#endif // __UFDCPark_DC__

#if !defined(__UFFocus_h__)
#define __UFFocus_h__ "$Name:  $ $Id: UFFocus.h,v 0.3 2005/05/25 16:18:58 hon Exp $"
#define __UFFocus_H__(arg) const char arg##Focus_h__rcsId[] = __UFFocus_h__;

#include "UFCAD.h"
#include "UFCCSetup.h"

class UFFocus : public UFCAD {
public:
  inline UFFocus(const string& instrum= "instrum") : UFCAD(instrum+":eng:focus") { _create(); }
  inline UFFocus(const string& instrum, UFCAR* car) : UFCAD(instrum+":eng:focus", car) { _create(); }
  inline virtual ~UFFocus() {}

  virtual int validate(UFPVAttr* pva= 0);
  virtual int genExec(int timeout= 0);

  // pthread func.
  // atomic move/motion to specified step relative count or step to limit, no backlash motion (+/- steps):
  static void* seekit(void* p);
  static int seek(std::map< string, UFCCSetup::MotParm* >* mparms, int timeout, UFGem* rec= 0, UFCAR* car= 0);

  // atomic move/motion to chosen limit switch at specified veloscity, no backlash motion
  // (M +/-vel):
  static string seekLimitOrStep(UFCCSetup::MotParm* mp); 
  // atomic move/motion to chosen limit switch? no backlash motion?
  // static string _seekdatum(UFCCSetup::MotParm* mp);

  // this can make use of the UFCCSetup::_motoragntsoc
  // and the UFECSetup lvdt connection:

  // lvdt connection helper
  static bool _LVDTConnection(const string& name);
  static void _enableLVDT(const string& name);
  static void _disableLVDT(const string& name);

protected:
  // ctor helper
  void _create();
};

#endif // __UFFocus_h__

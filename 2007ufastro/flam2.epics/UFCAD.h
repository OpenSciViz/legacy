#if !defined(__UFCAD_h__)
#define __UFCAD_h__ "$Name:  $ $Id: UFCAD.h,v 0.12 2005/10/20 19:53:58 hon Exp $"
#define __UFCAD_H__(arg) const char arg##CAD_h__rcsId[] = __UFCAD_h__;

#include "UFGem.h"
#include "UFCAR.h"
#include "UFClientSocket.h"

const int _UFCADTO = 10;

class UFCAD : public UFGem {
public:
  inline UFCAD(const string& recname) : UFGem(recname) { _create(recname); }
  // this allows cads to share same car, if possible:
  inline UFCAD(const string& recname, UFCAR* car) : UFGem(recname) { _create(recname, car); }
  inline virtual ~UFCAD() {}

  inline void setCar(UFCAR* car= 0) { _car = car; }

  inline virtual UFCAR* hasCAR() { return _car; }

  // these are meant to be overriden by any record that has an associated _car (apply, cad, ?)?
  //inline virtual int start(int timeout= _UFCADTO) { return UFGem::start(timeout); }
  inline virtual int genExec(int timeout= _UFCADTO) { return UFGem::genExec(timeout); }
  virtual void setBusy(size_t timeout, UFGem* proxy); // may a cad ever have another proxy? 
  inline virtual void setBusy(size_t timeout= _UFCADTO) { return setBusy(timeout, 0); }
  virtual void setIdle();

  inline virtual void setErr(const string& errmsg, int err= 1)
    { _timeout = 0; clog<<"UFCAD::setErr> "<<_name<<", msg: "<<errmsg<<endl; }
  inline virtual string errMsg(int val) 
    { string msg = "generic error.";  clog<<"UFCAD::errMsg> "<<_name<<", val: "<<val<<", msg: "<<msg<<endl; return msg; }
 
  static string carChan(UFGem* rec= 0, UFCAR* car= 0);
  static bool connected(UFClientSocket* agntsoc, int trycnt= 3);

protected:
  UFCAR* _car;  // cad (and apply) should have a car
  // ctor helper
  void _create(const string& recname, UFCAR* car= 0);
};

#endif // __UFCAD_h__

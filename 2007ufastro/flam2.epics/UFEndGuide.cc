#if !defined(__UFGETLOST_CC__)
#define __UFGETLOST_CC__ "$Name:  $ $Id: UFEndGuide.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFGETLOST_CC__;

#include "UFEndGuide.h"
#include "UFCAR.h"
//#include "UFCCEndGuide.h"
//#include "UFDCEndGuide.h"
//#include "UFECEndGuide.h"
#include "UFPVAttr.h"
#include "UFPosixRuntime.h"

// ctors:

// static class funcs:
void* UFEndGuide::loseAll(void* p) {
  // getlost really just needs the mech. names
  UFEndGuide::ThrdArg* arg = (UFEndGuide::ThrdArg*) p;
  UFEndGuide::Parm* parms = arg->_parms;
  UFCAR* car = arg->_car;
  UFGem* rec = arg->_rec; // should be either a cad or apply
  string name = "anon";
  if( rec )
    name = rec->name();

  // simple simulation...
  clog<<"UFEndGuide::loseAll>... "<<name<<endl;
  if( parms && parms->_op && parms->_osp && parms->_isp ) {
    clog<<"UFEndGuide::loseAll> obs timeout: "<<parms->_op->_timeout<<", exptime: "<<parms->_osp->_exptime<<endl;
    if( parms->_isp->_mparms && !parms->_isp->_mparms)
      clog<<"UFEndGuide::loseAll> motors: "<<parms->_isp->_mparms->size()<<endl;
    if( parms->_isp->_eparms )
      clog<<"UFEndGuide::loseAll> environment: "<<parms->_isp->_eparms->_MOSKelv
	  <<", "<<parms->_isp->_eparms->_CamAKelv<<", "<<parms->_isp->_eparms->_CamBKelv<<endl;
    if( parms->_osp->_dparms )
      clog<<"UFEndGuide::loseAll> detector pixels: "<<parms->_osp->_dparms->_w*parms->_osp->_dparms->_h<<endl;
  }
  // free arg
  delete arg;

  if( _sim ) {
    // set car idle after 1/2 sec:
    UFPosixRuntime::sleep(0.5);
    if( car )
      car->setIdle();
    if( rec ) {
      UFCAR* c = rec->hasCAR();
      if( c != 0 && c != car ) c->setIdle();
    }
  }

  // exit daemon thread
  return p;
}

/// general purpose getlost func. 

/// arg: mechName, motparm*
int UFEndGuide::getlost(UFEndGuide::Parm* parms, int timeout, UFGem* rec, UFCAR* car) {
  // the rec's setBusy will also call its optional car's setBusy
  // while the optional supplementary car can be set busy here
  if( rec && car ) {
    UFCAR* c = rec->hasCAR();
    if( car != c ) car->setBusy(timeout);
  }

  // create and run pthread to getlost whatever is currently being performed then either reset car to idle or error
  // thread should free the tParm* allocations...
  if( _sim ) {
    UFEndGuide::ThrdArg* arg = new UFEndGuide::ThrdArg(parms, rec, car);
    pthread_t thrid = UFPosixRuntime::newThread(UFEndGuide::loseAll, arg);
    return (int) thrid;
  }

  //UFCCEndGuide::loseAll(timeout, rec, car);
  //UFDCEndGuide::loseAll(timeout, rec, car);
  //UFECEndGuide::loseAll(timeout, rec, car);
  return 0;
}

// non static, non virtual funcs:
// protected ctor helper:
void UFEndGuide::_create() {
  string pvname = _name + ".mode";
  UFPVAttr* pva = (*this)[pvname] = new UFPVAttr(pvname); // any non null string should indicate getlost action 
  attachInOutPV(this, pva); 
  registerRec();

  // default car for each cad...
  if( !_car ) {
    string carname = _name + "C";
    _car = new UFCAR(carname, this);
  }
}

/// virtual funcs:
/// perform getlost mechanisms with non-null step cnts or named-position inputs
int UFEndGuide::genExec(int timeout) {
  size_t pvcnt = size();

  if( pvcnt == 0 ) {
    clog<<"UFEndGuide::genExec> np pvs?"<<endl;
    return (int) pvcnt;
  }

  string pvname = _name + ".mode";
  UFPVAttr* pva = (*this)[pvname];
  if( pva != 0 ) {
    string input = pva->valString(); // input is anything non-null
    if( input != "" && input != "null" && input != "Null" && input != "NULL" ) {
      clog<<"UFEndGuide::genExec> mode: "<<input<<endl;
      pva->clearVal(); // clear
    }
  }
  UFEndGuide::Parm* p = new UFEndGuide::Parm;
  int stat = getlost(p, timeout, this, _car);  // perform requested getlost

  if( stat < 0 ) { 
    clog<<"UFEndGuide::genExec> failed to start getlost pthread..."<<endl;
    return stat;
  }

  return stat;
} // genExec

int UFEndGuide::validate(UFPVAttr* pva) {
  return 0;
} // validate


#endif // __UFGETLOST_CC__

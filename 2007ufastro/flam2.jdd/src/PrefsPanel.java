package ufjdd;
/**
 * Title:        Java Data Display (JDD) : PrefsPanel 
 * Version:      (see rcsID)
 * Copyright:    Copyright (c) 2006
 * Author:       Craig Warner
 * Company:      University of Florida
 * Description:  For changing preferences 
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javaUFLib.*;
import java.io.*;
import java.util.*;
//===================================================================================

public class PrefsPanel extends JPanel {

    public static final String rcsID = "";
    public JComboBox zoomScale, zoomFactor, smoothIter, fullScale, colorMaps;
    public JComboBox buffer, zoomBox, lineCuts;
    public JButton bSetPrefs, bCancel;
    public ZoomPanel zoomPanel;
    public ColorMapDialog colorMapDialog;
    public boolean fileExists;
    public JFrame frame;
    public File f;

    public PrefsPanel(String file, ZoomPanel zoomPanel, ColorMapDialog colorMapDialog) {
	this(file, zoomPanel, colorMapDialog, null);
    }

    public PrefsPanel(String file, ZoomPanel zoomPanel, ColorMapDialog colorMapDialog, JFrame theFrame) {
	String currZoomScale, currZoomFactor, currSmoothIter, currFullScale, currColorMap;
	String currBuffer, currZoomBox, currLineCut;
	this.zoomPanel = zoomPanel;
	this.colorMapDialog = colorMapDialog;
 	this.frame = theFrame;
	currZoomFactor = (String)zoomPanel.zoomFacSelect.getSelectedItem();
	currSmoothIter = (String)zoomPanel.smoothIterSel.getSelectedItem();
	currZoomScale = zoomPanel.adjustZoom.scaleMode;
	currFullScale = (String)InitDataDisplay.jddFullFrame.zModeBox.getSelectedItem();
	currColorMap = (String)colorMapDialog.restoreList.getSelectedItem();
	currBuffer = (String)InitDataDisplay.jddFullFrame.bufferView.getSelectedItem();
        String[] zoomBoxColors = {"Translucent","More Translucent","Transparent"};
	currZoomBox = zoomBoxColors[InitDataDisplay.jddFullFrame.imagePanels[0].imageDisplay.getZoomBoxColor()]; 
	String[] lineCutTypes = {"Hand Drawn", "Nice Angles Only"};
	currLineCut = zoomPanel.zoomImage.lineCutStyle;
	fileExists = false;
	f = new File(file);
	if (f.exists()) try {
	   BufferedReader r = new BufferedReader(new FileReader(f));
	   String s = " ";
	   while (s != null) {
	      s = r.readLine();
	      if (s == null) break;
	      if (s.startsWith("ZOOM_FACTOR")) {
		currZoomFactor = s.substring(s.indexOf("=")+1).trim();
	      } else if (s.startsWith("SMOOTH_ITER")) {
		currSmoothIter = s.substring(s.indexOf("=")+1).trim();
	      } else if (s.startsWith("ZOOM_SCALE")) {
		currZoomScale = s.substring(s.indexOf("=")+1).trim();
	      } else if (s.startsWith("FULL_SCALE")) {
		currFullScale = s.substring(s.indexOf("=")+1).trim();
	      } else if (s.startsWith("COLOR_MAP")) {
		currColorMap = s.substring(s.indexOf("=")+1).trim();
	      } else if (s.startsWith("BUFFER")) {
		currBuffer = s.substring(s.indexOf("=")+1).trim();
	      } else if (s.startsWith("ZOOM_BOX")) {
		currZoomBox = s.substring(s.indexOf("=")+1).trim();
	      } else if (s.startsWith("LINE_CUTS")) {
		currLineCut = s.substring(s.indexOf("=")+1).trim();
	      }
	   }
	   r.close();
	} catch (IOException e) {
	   System.err.println(e.toString());
	}
        String zooms[] = {"1","2","3","4","5","6","8","10","12","15","16","20","24","30","32","40","48","60","80","96"}; //divisors of 480.
        zoomFactor = new JComboBox(zooms);
	zoomFactor.setSelectedItem(currZoomFactor);

        String smoothFacts[] = {"0","1","2","3","4","6","9","12","16","20","25"};
        smoothIter = new JComboBox(smoothFacts);
	smoothIter.setSelectedItem(currSmoothIter);
        String[] zoomScaleModes = {"Linear", "Log", "Power", "Tied"};
	zoomScale = new JComboBox(zoomScaleModes);
	zoomScale.setSelectedItem(currZoomScale);

	String[] fullScaleModes = {"Auto","Manual","Zoom Box","Zscale","Zmax","Zmin"};
	fullScale = new JComboBox(fullScaleModes);
	fullScale.setSelectedItem(currFullScale);

	String[] restoreList = setupRestoreList();
	colorMaps = new JComboBox(restoreList);
	colorMaps.setSelectedItem(currColorMap);

	String[] bufferModes = {"Src Buffer","Bgd Buffer","Dif Buffer"};
	buffer = new JComboBox(bufferModes);
	buffer.setSelectedItem(currBuffer);

        //String[] zoomBoxColors = {"Translucent","More Translucent","Transparent"};
        zoomBox = new JComboBox(zoomBoxColors);
        zoomBox.setSelectedItem(currZoomBox);

	lineCuts = new JComboBox(lineCutTypes);
	lineCuts.setSelectedItem(currLineCut);

	SpringLayout layout = new SpringLayout();
	this.setLayout(layout);
	JLabel lZoomFactor = new JLabel("ZOOM Factor = ");
	this.add(lZoomFactor);
	layout.putConstraint(SpringLayout.WEST, lZoomFactor, 5, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, lZoomFactor, 10, SpringLayout.NORTH, this);
	this.add(zoomFactor);
        layout.putConstraint(SpringLayout.WEST, zoomFactor, 150, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, zoomFactor, 5, SpringLayout.NORTH, this);

	JLabel lSmoothIter = new JLabel("Smooth Iterations = ");
	this.add(lSmoothIter);
	layout.putConstraint(SpringLayout.WEST, lSmoothIter, 5, SpringLayout.WEST, this);
	layout.putConstraint(SpringLayout.NORTH, lSmoothIter, 17, SpringLayout.SOUTH, zoomFactor);
        this.add(smoothIter);
        layout.putConstraint(SpringLayout.WEST, smoothIter, 150, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, smoothIter, 12, SpringLayout.SOUTH, zoomFactor);

	JLabel lZoomScale = new JLabel("Zoom Box Scale = ");
	this.add(lZoomScale);
        layout.putConstraint(SpringLayout.WEST, lZoomScale, 5, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, lZoomScale, 17, SpringLayout.SOUTH, smoothIter);
        this.add(zoomScale);
        layout.putConstraint(SpringLayout.WEST, zoomScale, 150, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, zoomScale, 12, SpringLayout.SOUTH, smoothIter);

        JLabel lFullScale = new JLabel("Full Image Scale = ");
        this.add(lFullScale);
        layout.putConstraint(SpringLayout.WEST, lFullScale, 5, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, lFullScale, 17, SpringLayout.SOUTH, zoomScale);
        this.add(fullScale);
        layout.putConstraint(SpringLayout.WEST, fullScale, 150, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, fullScale, 12, SpringLayout.SOUTH, zoomScale);

	JLabel lColorMaps = new JLabel("Color Map = ");
	this.add(lColorMaps);
        layout.putConstraint(SpringLayout.WEST, lColorMaps, 5, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, lColorMaps, 17, SpringLayout.SOUTH, fullScale);
        this.add(colorMaps);
        layout.putConstraint(SpringLayout.WEST, colorMaps, 150, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, colorMaps, 12, SpringLayout.SOUTH, fullScale);

        JLabel lBuffer = new JLabel("Buffer = ");
        this.add(lBuffer);
        layout.putConstraint(SpringLayout.WEST, lBuffer, 5, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, lBuffer, 17, SpringLayout.SOUTH, colorMaps);
        this.add(buffer);
        layout.putConstraint(SpringLayout.WEST, buffer, 150, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, buffer, 12, SpringLayout.SOUTH, colorMaps);

        JLabel lZoomBox = new JLabel("Zoom Box = ");
        this.add(lZoomBox);
        layout.putConstraint(SpringLayout.WEST, lZoomBox, 5, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, lZoomBox, 17, SpringLayout.SOUTH, buffer);
        this.add(zoomBox);
        layout.putConstraint(SpringLayout.WEST, zoomBox, 150, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, zoomBox, 12, SpringLayout.SOUTH, buffer);

        JLabel lLineCuts = new JLabel("Line Cuts = ");
        this.add(lLineCuts);
        layout.putConstraint(SpringLayout.WEST, lLineCuts, 5, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, lLineCuts, 17, SpringLayout.SOUTH, zoomBox);
        this.add(lineCuts);
        layout.putConstraint(SpringLayout.WEST, lineCuts, 150, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, lineCuts, 12, SpringLayout.SOUTH, zoomBox);

	bSetPrefs = new JButton("Set Preferences");
	bSetPrefs.addActionListener(new ActionListener() {
	   public void actionPerformed(ActionEvent ev) {
	      setPrefs();
	      if (frame != null) frame.dispose();
	   }
	});
	this.add(bSetPrefs);
        layout.putConstraint(SpringLayout.WEST, bSetPrefs, 5, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, bSetPrefs, 17, SpringLayout.SOUTH, lineCuts);
	bCancel = new JButton("Cancel");
        bCancel.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent ev) {
              if (frame != null) frame.dispose();
           }
        });
	this.add(bCancel);
        layout.putConstraint(SpringLayout.WEST, bCancel, 10, SpringLayout.EAST, bSetPrefs);
        layout.putConstraint(SpringLayout.NORTH, bCancel, 17, SpringLayout.SOUTH, lineCuts);
	

	layout.putConstraint(SpringLayout.EAST, this, 25, SpringLayout.EAST, colorMaps);
	layout.putConstraint(SpringLayout.SOUTH, this, 25, SpringLayout.SOUTH, bSetPrefs);
    }

    public void setFrame(JFrame frame) {
	this.frame = frame;
    }

    public void setPrefs() {
	try {
	   PrintWriter p = new PrintWriter(new FileOutputStream(f));
	   p.println("ZOOM_FACTOR = " + (String)zoomFactor.getSelectedItem());
	   p.println("SMOOTH_ITER = " + (String)smoothIter.getSelectedItem());
	   p.println("ZOOM_SCALE = " + (String)zoomScale.getSelectedItem());
	   p.println("FULL_SCALE = " + (String)fullScale.getSelectedItem());
	   p.println("COLOR_MAP = " + (String)colorMaps.getSelectedItem());
           p.println("BUFFER = " + (String)buffer.getSelectedItem());
           p.println("ZOOM_BOX = " + (String)zoomBox.getSelectedItem());
	   p.println("LINE_CUTS = " + (String)lineCuts.getSelectedItem());
	   p.close();
	} catch(IOException e) { System.err.println(e.toString()); }
        zoomPanel.zoomFacSelect.setSelectedItem(zoomFactor.getSelectedItem());
        zoomPanel.smoothIterSel.setSelectedItem(smoothIter.getSelectedItem());
	String currZoomScale = (String)zoomScale.getSelectedItem();
	for (Enumeration e = zoomPanel.adjustZoom.scalingModeBuGrp.getElements(); e.hasMoreElements();) {
	   JRadioButton b = (JRadioButton)e.nextElement();
	   if (b.getText().equalsIgnoreCase(currZoomScale)) b.setSelected(true);
	}
	zoomPanel.adjustZoom.scaleMode = (String)zoomScale.getSelectedItem();
	InitDataDisplay.jddFullFrame.zModeBox.setSelectedItem(fullScale.getSelectedItem());
	colorMapDialog.restoreList.setSelectedItem(colorMaps.getSelectedItem());
	InitDataDisplay.jddFullFrame.bufferView.setSelectedItem(buffer.getSelectedItem());
	InitDataDisplay.jddFullFrame.imagePanels[0].imageDisplay.setZoomBoxColor(zoomBox.getSelectedIndex());
        zoomPanel.zoomImage.setZoomBoxColor(zoomBox.getSelectedIndex());
	zoomPanel.zoomImage.lineCutStyle = (String)lineCuts.getSelectedItem();
    }

    public String[] setupRestoreList() {
       Properties props = new Properties(System.getProperties());
       String data_path = props.getProperty("jdd.data_path");

      File file = new File(data_path + "ColorMaps.dat");
      Vector vals = new Vector();
      String[] labels, values;
      int n = 1;
      int[] data = new int[9];
      String currLine = " ";
      Vector v = new Vector();
      if (file.exists()) {
        try {
           BufferedReader r = new BufferedReader(new FileReader(file));
           while (currLine != null) {
              currLine = r.readLine();
              if (currLine != null) {
                v.addElement(currLine);
                vals.addElement(r.readLine());
                n++;
              }
           }
           r.close();
        } catch(IOException e) {
           System.err.println("Error Reading From File.");
        }
      }
      int[][] restoreValues = new int[vals.size()][9];
      for (int j = 0; j < vals.size(); j++) {
        currLine = (String)vals.get(j);
        values = currLine.split(" ");
        for (int l = 0; l < 9; l++)
           restoreValues[j][l] = (int)Float.parseFloat(values[l]);
      }
      labels = new String[v.size()+1];
      labels[0] = "Restore a Color Map";
      for (int j = 1; j < n; j++) labels[j] = (String)v.get(j-1);
      return labels;
   }
}

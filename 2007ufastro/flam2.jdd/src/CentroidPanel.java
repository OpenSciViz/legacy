package ufjdd;
/**
 * Title:        CentroidPanel.java
 * Version:      (see rcsID)
 * Authors:      Frank Varosi, Craig Warner
 * Company:      University of Florida
 * Description:  For displaying the values of centroid coordinates and Full Width Half Max of a PSF.
 */
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javaUFLib.*;

public class CentroidPanel extends JFrame {

    public static final
	String rcsID = "$Name:  $ $Id: CentroidPanel.java,v 1.2 2005/08/01 14:55:23 amarin Exp $";

    UFLabel bufName = new UFLabel(" Name:", Color.BLACK);
    UFLabel cxLabel = new UFLabel(" Centroid X =", 2);
    UFLabel cyLabel = new UFLabel(" Centroid Y =", 2); //always show at least 2 digits after decimal point.
    UFLabel fwhmValue = new UFLabel(" FWHM avg. =", Color.BLACK);
    UFLabel fwhmStdev = new UFLabel("  +/-  ");
    UFLabel fwhmX = new UFLabel(" FWHM: X cut =", Color.BLACK);
    UFLabel fwhmY = new UFLabel(" FWHM: Y cut =", Color.BLACK);
    UFLabel fwhmXY = new UFLabel(" FWHM: X-Y cut =", Color.BLACK);
    UFLabel fwhmYX = new UFLabel(" FWHM: X+Y cut =", Color.BLACK);
    boolean first = true;

   public CentroidPanel() {
      super("Centroid");
      this.setSize( 230, 250 );
      cxLabel.newColor( Color.BLACK );
      cyLabel.newColor( Color.BLACK );
      Container content = getContentPane();
      content.setLayout(new GridLayout(0,1));
      content.add(bufName);
      content.add( cxLabel );
      content.add( cyLabel );
      JPanel FWHM = new JPanel();
      FWHM.setLayout(new RatioLayout());
      FWHM.add("0.0, 0.0; 0.6, 1.0", fwhmValue );
      FWHM.add("0.6, 0.0; 0.4, 1.0", fwhmStdev );
      content.add(FWHM);
      content.add(fwhmX);
      content.add(fwhmY);
      content.add(fwhmXY);
      content.add(fwhmYX);
      first = true;
      addWindowListener(new WindowAdapter() {
	      public void windowClosing(WindowEvent e) { e.getWindow().dispose(); } });
   }

    public void reShow()
    {
        if( ! first ) {
	    setVisible(true);
	    setState( Frame.NORMAL );
	}
    }

    public void update(float cx, float cy, float[] fwhm, String bufname)
    {
	bufName.setText( bufname );
	cxLabel.setText( cx );
	cyLabel.setText( cy );
	fwhmValue.setText( fwhm[0] );
	fwhmStdev.setText( fwhm[1] );
	fwhmX.setText( fwhm[2] );
	fwhmY.setText( fwhm[3] );
	fwhmXY.setText( fwhm[4] );
	fwhmYX.setText( fwhm[5] );
	if( first ) {
	    first = false;
	    setVisible(true);
	    setState( Frame.NORMAL );
	}
    }
}

package ufjdd;
/**
 * Title:        CentroidPlots.java
 * Version:      (see rcsID)
 * Authors:      Frank Varosi and Craig Warner
 * Company:      University of Florida
 * Description:  Plot image data pixel values along x & y line cuts thru centroid of ZoomImage.
 */
import javaUFLib.*;
import java.awt.*;
import java.awt.image.*;
import java.awt.geom.*;
import javax.swing.*;

public class CentroidPlots {

    public static final
	String rcsID = "$Name:  $ $Id: CentroidPlots.java,v 1.3 2005/07/28 19:44:11 varosi Exp $";

    GatorPlot gp;
    boolean first;
    
    public CentroidPlots()
    {
	gp = new GatorPlot("Centroid X and Y Line Cuts");
	first = true;
	gp.autoSetVisible(false);
    }
    
    public void show()
    {
        if( ! first ) {
	    gp.setVisible(true);
	    gp.setState( Frame.NORMAL );
	}
    }

    public void update( double[][] data, float[] centrd, float[] fwhm,
			int xStart, int yStart, int yMax, double scalefactor, String dname )
    {
	int xc = (int)Math.floor( centrd[0] );
	int yc = (int)Math.floor( centrd[1] );

	if( first ) {
	   first = false;
	   gp.showPlot(false);
	   gp.setState( Frame.NORMAL );
	}

	if( xc < 0 || yc < 0 ) return;

        int xRange = data[0].length;
        int yRange = data.length;
        int mRange = Math.max( xRange, yRange );

	float[] pixPlot = new float[xRange];
	float[] dataPlot = new float[xRange];
        
	for( int i=0; i < xRange; i++ ) {
	    pixPlot[i] = i;
	    dataPlot[i] = (float)( data[yc][i] * scalefactor );
	}

        gp.plot( pixPlot, dataPlot,
		 "*xtitle=Distance( pixels ) : Xstart=" + xStart + " : Ystart=" + (yMax - yStart - yRange)
		 + ", *ytitle=Pixel Values, *xrange=[0," + mRange + "], *psym=-4, *symsize=4, *title=" + dname
		 + ", *xtickinterval=10, *xminor=10");

	float[] cenPlotX = new float[3];
	float[] cenPlotY = new float[3];
	cenPlotX[0] = centrd[0];
	cenPlotX[1] = cenPlotX[0];
	cenPlotX[2] = cenPlotX[0];
	cenPlotY[0] = UFArrayOps.minValue( dataPlot );
	cenPlotY[1] = 0;
	cenPlotY[2] = UFArrayOps.maxValue( dataPlot );
	float cran = cenPlotY[2] - cenPlotY[0];
	cenPlotY[0] -= cran;
	cenPlotY[2] += cran;
	gp.overplot( cenPlotX, cenPlotY,"*psym=-4, *symsize=4");

	if( yRange != xRange ) {
	    pixPlot = new float[yRange];
	    dataPlot = new float[yRange];
	}
        
	for( int i=0; i < yRange; i++ ) {
	    pixPlot[i] = yRange - i;
	    dataPlot[i] = (float)( data[i][xc] * scalefactor );
	}

        gp.overplot( pixPlot, dataPlot,"*psym=-6, *symsize=3");
	cenPlotX[0] = yRange - centrd[1];
	cenPlotX[1] = cenPlotX[0];
	cenPlotX[2] = cenPlotX[0];
	gp.overplot( cenPlotX, cenPlotY,"*psym=-6, *symsize=3");
    }
}

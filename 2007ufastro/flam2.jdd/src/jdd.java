package ufjdd;

/**
 * Title:        Java Data Display  (JDD)
 * Version:      (see rcsID)
 * Copyright:    Copyright (c) 2005
 * Author:       Antonio Marin-Franch, Ziad Saleh, Frank Varosi
 * Company:      University of Florida
 * Description:  For quick-look image display and analysis of Near-IR Camera data stream.
 */

import java.awt.*;
import javax.swing.*;

public class jdd {
    
    public static final
	String rcsID = "$Name:  $ $Id: jdd.java,v 1.10 2006/03/29 23:02:19 warner Exp $";

    private boolean packFrame = false;
    GraphicsConfiguration gc;
    
    public jdd(String hostname, int hostport, String tcshost, String tcsport,  String args[], String jddtype)
    {
       GraphicsDevice[] gs = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices();

	// Initializing JDD components... 
       InitDataDisplay jddFrame = new InitDataDisplay(gs, hostname, hostport, tcshost, tcsport, args, jddtype);
       jddFrame.validate();               
       
    }
    
    public static void main(String[] args)
    {
        String hostname = "flam2sparc";
        String tcshost = "perseus";
        String tcsport = "12344";
	String jddtype = "flam2jdd";
        
        int hostport = 52001;
        
        for (int i = 0; i < args.length; i++) {
            if (args[i].equals("-host")) {
                hostname = args[++i];
            } else if(args[i].equals("-tcshost")) {
                tcshost = args[++i];
            } else if(args[i].equals("-tcsport")) {
                tcsport = args[++i];
            } else if(args[i].equals("-NIR")) {
                jddtype = "NIRjdd";
            } else if(args[i].equals("-flam2")) {
                jddtype = "flam2jdd";
            }

        }

        new jdd(hostname, hostport, tcshost, tcsport, args, jddtype);
    }
}


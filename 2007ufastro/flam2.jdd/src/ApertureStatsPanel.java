package ufjdd;
/**
 * Title:        Java Data Display (JDD) : ApertureStatsPanel.java
 * Version:      (see rcsID)
 * Copyright:    Copyright (c) 2005
 * Author:       Frank Varosi
 * Company:      University of Florida
 * Description:  For displaying values of statistics in zoomed image aperture.
 */
import java.awt.*;
import javax.swing.*;
import javaUFLib.*;
//===================================================================================

public class ApertureStatsPanel extends JPanel {
    
    public static final
	String rcsID = "$Name:  $ $Id: ApertureStatsPanel.java,v 1.7 2006/07/24 16:29:56 warner Exp $";

    protected UFLabel totalFlux = new UFLabel("Total =");
    protected UFLabel avgFlux = new UFLabel("Mean =");
    protected UFLabel stdevFlux = new UFLabel("Std.Dev. =");
    protected UFLabel stdevPerFrm = new UFLabel("S.Dev / Frm =");
    protected UFLabel rangeFlux = new UFLabel("Max - min =");
    protected UFLabel bufferInfo = new UFLabel("Name:");
    //protected UFLabel coaddsInfo = new UFLabel("# CoAdds =");

    ApertureStatsPanel()
    {
	this.setLayout(new GridLayout(0,1));
	this.add( new JLabel("Stats in Zoom Aperture:") );
	this.add( totalFlux );
	this.add( avgFlux );
	this.add( stdevFlux );
	this.add( stdevPerFrm );
	this.add( rangeFlux );
	this.add( bufferInfo );
	//this.add( coaddsInfo );
        this.setBorder( BorderFactory.createLoweredBevelBorder() );
    }

    void showArrayStats( double[][] zoomData, ImageBuffer zoomBuffer, double scaleFactor )
    {
	if( zoomData == null ) return;

	double ztot = UFArrayOps.totalValue( zoomData ) * scaleFactor;
	totalFlux.setText( ztot );
	totalFlux.setToolTipText(totalFlux.getText());
	int npix = zoomData.length * zoomData[0].length;
	avgFlux.setText( ztot/npix );
        avgFlux.setToolTipText(avgFlux.getText());
	double stdev = UFArrayOps.stddev( zoomData ) * scaleFactor;
	stdevFlux.setText( stdev );
        stdevFlux.setToolTipText(stdevFlux.getText());
	stdevPerFrm.setText( stdev/Math.sqrt( scaleFactor * zoomBuffer.Ndifs ) );
        stdevPerFrm.setToolTipText(stdevPerFrm.getText());
	rangeFlux.setText( (zoomBuffer.s_max - zoomBuffer.s_min) * scaleFactor );
        rangeFlux.setToolTipText(rangeFlux.getText());
	bufferInfo.setText( zoomBuffer.getShortNameNum() );
	bufferInfo.setToolTipText(zoomBuffer.nameNum);
	//coaddsInfo.setText( zoomBuffer.CoAdds );
    }
}

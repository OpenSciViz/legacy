package ufjdd;
/**
 * Title:        ImageBuffer.java
 * Version:      (see rcsID)
 * Authors:      Antonio Marin-Franch, Frank Varosi and Ziad Saleh
 * Company:      University of Florida
 * Description:  Object for storing frameConfig and frameInts objects from NIR Acq. Server.
 */
import javaUFProtocol.*;
import javaUFLib.*;

public class ImageBuffer {
    
    public static final
	String rcsID = "$Name:  $ $Id: ImageBuffer.java,v 1.9 2006/07/24 16:29:56 warner Exp $";

    protected UFFrameConfig frameConfig;
    protected UFFrameConfig frmConfigRef;
    protected UFFrameConfig frmConfCoAdd;
    protected String name, nameNum, nameCoAdd;
    protected String timeStamp;
    public int width;
    public int height;
    public int min =  2147483647;
    public int max = -2147483648;
    public int Ndifs = 1;
    public int CoAdds = 1;
    public double scaleFactor = 1.0;
    public float s_min=0;
    public float s_max=0;
    protected double[] ZscaleRange = null;
    public int pixels[];
    public double chanMeans[], chanStdevs[], chanSkews[], chanCurts[], chanMins[], chanMaxs[];
    //public int zoomFactor = 1;
    public float zoomFactor = 1;
    public int xStart, yStart;
    public int image[][];

    public ImageBuffer( UFFrameConfig frameConfig ) { this.frameConfig = frameConfig; }

    public ImageBuffer( UFFrameConfig frameConfig, UFProtocol frameData) {
	this(frameConfig, castAsUFInts(frameData));
    }

    public static UFInts castAsUFInts (UFProtocol frameData) {
	if (frameData instanceof UFInts) {
	   return (UFInts)frameData;
	} else if (frameData instanceof UFShorts) {
	   return new UFInts(frameData.name(), UFArrayOps.castAsInts(((UFShorts)frameData).values()));
	} else if (frameData instanceof UFBytes) {
	   return new UFInts(frameData.name(), UFArrayOps.castAsInts(((UFBytes)frameData).values()));
	} else if (frameData instanceof UFFloats) {
	   return new UFInts(frameData.name(), UFArrayOps.castAsInts(((UFFloats)frameData).values()));
	}
	System.err.println("Invalid data type specified");
	return null;
    }

    public ImageBuffer( UFFrameConfig frameConfig, UFInts frameData )
    {
        this.frameConfig = frameConfig;
        this.width = frameConfig.width;
        this.height = frameConfig.height;

	frameData.calcMinMax();
        this.min = frameData.minVal();
        this.max = frameData.maxVal();
        s_min = min;
        s_max = max;

	this.CoAdds = frameConfig.frameCoadds * frameConfig.coAdds;
	if( frameConfig.chopCoadds > 0 ) this.CoAdds = this.CoAdds * frameConfig.chopCoadds;
        this.scaleFactor = 1.0/(double)CoAdds;

	this.timeStamp = frameData.timeStamp();
        this.name = frameData.name();
	if( name.indexOf("||") > 0 ) name = name.substring( 0, name.indexOf("||") );
	if( name.toLowerCase().indexOf("dif") >= 0 ) this.Ndifs = 2;
	if( name.toLowerCase().indexOf("sig") >= 0 ) this.Ndifs = 2;
	this.nameNum = this.name + ( "(" + frameConfig.frameProcCnt + ")" );

	frameData.flipFrame( frameConfig ); //because Java coordinate sys of (Left,Top) = (0,0)
        this.pixels = frameData.values();
    }
//----------------------------------------------------------------------------------------------

    public synchronized void Zscale()
    {
        if( image != null )
            this.ZscaleRange = UFImageOps.Zscale( image );
        else
            this.ZscaleRange = UFImageOps.Zscale( pixels );
    }
//----------------------------------------------------------------------------------------------

    public ImageBuffer zoom( int xZoomCen, int yZoomCen, int zoomSize, float zoomFactor )
    {
        int xStart = xZoomCen - zoomSize/2 ;
        int yStart = yZoomCen - zoomSize/2 ;

	if( xStart < 0 ) xStart = 0;
	if( yStart < 0 ) yStart = 0;

        if( (xStart + zoomSize) > this.width  && xStart > 0 ) xStart--;
        if( (yStart + zoomSize) > this.height && yStart > 0 ) yStart--;

	int[][] zoomPixels = new int[zoomSize][zoomSize];
	int zmin =  2147483647;
	int zmax = -2147483648;

	for( int i = 0; i < zoomSize; i++ ) {
	    int isw = xStart + (i + yStart) * this.width;
	    for( int j = 0; j < zoomSize; j++ ) {
		int k = isw + j;
		if( k < this.pixels.length ) {
		    int data = this.pixels[k];
		    zoomPixels[i][j] = data;
		    if( data < zmin ) zmin = data;
		    if( data > zmax ) zmax = data;
		}
	    }
	}

	ImageBuffer zoomBuffer = new ImageBuffer( this.frameConfig );

        zoomBuffer.image = zoomPixels;
	zoomBuffer.zoomFactor = zoomFactor;
	zoomBuffer.xStart = xStart;
	zoomBuffer.yStart = yStart;
        zoomBuffer.name = new String( this.name );
        zoomBuffer.nameNum = new String( this.nameNum );
        zoomBuffer.width = zoomSize;
        zoomBuffer.height = zoomSize;
	zoomBuffer.Ndifs = this.Ndifs;
	zoomBuffer.CoAdds = this.CoAdds;
	zoomBuffer.scaleFactor = this.scaleFactor;
        zoomBuffer.min = zmin;
        zoomBuffer.max = zmax;
        zoomBuffer.s_min = zmin;
        zoomBuffer.s_max = zmax;

	return zoomBuffer;
    }
//----------------------------------------------------------------------------------------------
    public ImageBuffer rescale1k( )
    {
	int[][] rescale1k = new int[1024][1024];
	int zmin =  2147483647;
	int zmax = -2147483648;
	int[] rescale1kPixels = new int[1024*1024];

	for( int i = 0; i < 1024; i++ ) {
	    int isw = 2*i * this.width;
	    for( int j = 0; j < 1024; j++ ) {
		int k = isw + 2*j;
		int l = i*1024+j;
		if( k < this.pixels.length ) {
		    int data = this.pixels[k];
		    rescale1kPixels[l] = this.pixels[k];
		    rescale1k[i][j] = data;
		    if( data < zmin ) zmin = data;
		    if( data > zmax ) zmax = data;
		}
	    }
	}

	ImageBuffer rescale1kBuffer = new ImageBuffer( this.frameConfig );

        rescale1kBuffer.image = rescale1k;
        rescale1kBuffer.name = new String( this.name );
        rescale1kBuffer.nameNum = new String( this.nameNum );
        rescale1kBuffer.width = 1024;
        rescale1kBuffer.height = 1024;
        rescale1kBuffer.pixels = rescale1kPixels;
	rescale1kBuffer.Ndifs = this.Ndifs;
	rescale1kBuffer.CoAdds = this.CoAdds;
	rescale1kBuffer.scaleFactor = this.scaleFactor;
        rescale1kBuffer.min = zmin;
        rescale1kBuffer.max = zmax;
        rescale1kBuffer.s_min = zmin;
        rescale1kBuffer.s_max = zmax;

	return rescale1kBuffer;
    }
//----------------------------------------------------------------------------------------------
    public ImageBuffer rescale256( )
    {
	int[][] rescale256 = new int[256][256];
	int zmin =  2147483647;
	int zmax = -2147483648;
	int[] rescale256Pixels = new int[256*256];

	for( int i = 0; i < 256; i++ ) {
	    int isw = 8*i * this.width;
	    for( int j = 0; j < 256; j++ ) {
		int k = isw + 8*j;
		int l = i*256+j;
		if( k < this.pixels.length ) {
		    int data = this.pixels[k];
		    rescale256Pixels[l] = this.pixels[k];
		    rescale256[i][j] = data;
		    if( data < zmin ) zmin = data;
		    if( data > zmax ) zmax = data;
		}
	    }
	}

	ImageBuffer rescale256Buffer = new ImageBuffer( this.frameConfig );

        rescale256Buffer.image = rescale256;
        rescale256Buffer.name = new String( this.name );
        rescale256Buffer.nameNum = new String( this.nameNum );
        rescale256Buffer.width = 256;
        rescale256Buffer.height = 256;
        rescale256Buffer.pixels = rescale256Pixels;
	rescale256Buffer.Ndifs = this.Ndifs;
	rescale256Buffer.CoAdds = this.CoAdds;
	rescale256Buffer.scaleFactor = this.scaleFactor;
        rescale256Buffer.min = zmin;
        rescale256Buffer.max = zmax;
        rescale256Buffer.s_min = zmin;
        rescale256Buffer.s_max = zmax;

	return rescale256Buffer;
    }
//----------------------------------------------------------------------------------------------
    public ImageBuffer rescale(int size) 
    {
	double scale = (double)this.width/size;
        int[][] rescaleData = new int[size][size];
        int zmin =  2147483647;
        int zmax = -2147483648;
        int[] rescalePixels = new int[size*size];

        for( int i = 0; i < size; i++ ) {
            int isw = (int)Math.floor(scale*i) * this.width;
            for( int j = 0; j < size; j++ ) {
                int k = isw + (int)Math.floor(scale*j);
                int l = i*size+j;
                if( k < this.pixels.length ) {
                    int data = this.pixels[k];
                    rescalePixels[l] = this.pixels[k];
                    rescaleData[i][j] = data;
                    if( data < zmin ) zmin = data;
                    if( data > zmax ) zmax = data;
                }
            }
        }

        ImageBuffer rescaleBuffer = new ImageBuffer( this.frameConfig );

        rescaleBuffer.image = rescaleData;
        rescaleBuffer.name = new String( this.name );
        rescaleBuffer.nameNum = new String( this.nameNum );
        rescaleBuffer.width = size;
        rescaleBuffer.height = size;
        rescaleBuffer.pixels = rescalePixels;
        rescaleBuffer.Ndifs = this.Ndifs;
        rescaleBuffer.CoAdds = this.CoAdds;
        rescaleBuffer.scaleFactor = this.scaleFactor;
        rescaleBuffer.min = zmin;
        rescaleBuffer.max = zmax;
        rescaleBuffer.s_min = zmin;
        rescaleBuffer.s_max = zmax;

        return rescaleBuffer;
    }

    public ImageBuffer subtractReference( ImageBuffer refimage )
    {
	return subtractReference( refimage, false );
    }

    public ImageBuffer subtractReference( ImageBuffer refimage, boolean fromRef )
    {
	if( refimage == null ) return this;

	ImageBuffer difimage = new ImageBuffer( this.frameConfig );

        difimage.frmConfigRef = refimage.frameConfig;
        difimage.width = this.width;
	difimage.height = this.height;
	difimage.Ndifs = this.Ndifs + refimage.Ndifs;
	difimage.CoAdds = this.CoAdds;
	difimage.scaleFactor = this.scaleFactor;

	if( fromRef ) {
	    difimage.name = refimage.name + " - " + this.name;
	    difimage.nameNum = refimage.nameNum + " - " + this.nameNum;
	}
	else {
	    difimage.name = this.name + " - " + refimage.name;
	    difimage.nameNum = this.nameNum + " - " + refimage.nameNum;
	}
	int npix = refimage.pixels.length;
	if( npix > this.pixels.length ) npix = this.pixels.length;

	difimage.pixels = new int[npix]; //java.lang.OutOfMemoryError!!
	difimage.min =  2147483647;
	difimage.max = -2147483648;
	int difpix;
	boolean reScale = false;
	double scaleFac = 1.0;

	if( this.CoAdds != refimage.CoAdds ) {
	    reScale = true;
	    scaleFac = (double)this.CoAdds/(double)refimage.CoAdds;
	}

	for( int i=0; i < npix; i++ )
	    {
		if( fromRef ) {
		    if( reScale )
			difpix = (int)Math.round( scaleFac*refimage.pixels[i] - this.pixels[i] );
		    else
			difpix = refimage.pixels[i] -this.pixels[i];
		}
		else {
		    if( reScale )
			difpix = (int)Math.round( this.pixels[i] - scaleFac*refimage.pixels[i] );
		    else
			difpix = this.pixels[i] - refimage.pixels[i];
		}

		difimage.pixels[i] = difpix;
		if( difpix < difimage.min ) difimage.min = difpix;
		if( difpix > difimage.max ) difimage.max = difpix;
	    }

        difimage.s_min = difimage.min;
        difimage.s_max = difimage.max;

	return difimage;
    }
//----------------------------------------------------------------------------------------------

    public void coAdd( ImageBuffer imgBuf )
    {
	if( imgBuf == null ) {
	    System.out.println("ImageBuffer.coadd> null arg. !");
	    return;
	}

	int npix = imgBuf.pixels.length;

	if( npix != this.pixels.length ) {
	    System.out.println("ImageBuffer.coadd> number of pixels not the same!");
	    return;
	}

	this.CoAdds += imgBuf.CoAdds;
        this.scaleFactor = 1.0/this.CoAdds;

	this.frmConfCoAdd = imgBuf.frameConfig;
	this.nameCoAdd = imgBuf.name;
	this.nameNum = "acc" + this.name + "("+frameConfig.frameProcCnt+"+"+frmConfCoAdd.frameProcCnt+")";

	this.min = this.pixels[0] + imgBuf.pixels[0];
	this.max = this.min;

	for( int i=0; i < npix; i++ )
	    {
		int sumpix = this.pixels[i] + imgBuf.pixels[i];
		this.pixels[i] = sumpix;
		if( sumpix < this.min ) this.min = sumpix;
		if( sumpix > this.max ) this.max = sumpix;
	    }

        this.s_min = this.min;
        this.s_max = this.max;
    }
//----------------------------------------------------------------------------------------------

    public void channelStatistics(int Nchannels)
    {
	chanMeans = new double[Nchannels];
	chanStdevs = new double[Nchannels];
	chanSkews = new double[Nchannels];
	chanCurts = new double[Nchannels]; //for Curtosis of distribution.
	chanMins = new double[Nchannels];
	chanMaxs = new double[Nchannels];
	int chanWidth = this.width/Nchannels;
	int ipix = 0;

	for( int irow=0; irow < this.height; irow++ ) {
	    for( int kchan=0; kchan < Nchannels; kchan++ ) {
		chanMins[kchan] = (double)this.pixels[ipix];
		chanMaxs[kchan] = chanMins[kchan];
		for( int kpix=0; kpix < chanWidth; kpix++ )
		    chanMeans[kchan] += (double)this.pixels[ipix++];
	    }
	}

	double Npixchan = (double)( chanWidth * this.height );
	for( int kchan=0; kchan < Nchannels; kchan++ ) chanMeans[kchan] /= Npixchan;
	ipix = 0;

	for( int irow=0; irow < this.height; irow++ ) {
	    for( int kchan=0; kchan < Nchannels; kchan++ ) {
		for( int kpix=0; kpix < chanWidth; kpix++ ) {
		    double data = (double)this.pixels[ipix++];
		    if( data < chanMins[kchan] ) chanMins[kchan] = data;
		    if( data > chanMaxs[kchan] ) chanMaxs[kchan] = data;
		    double dev = data - chanMeans[kchan];
		    double dev2 = dev * dev;
		    chanStdevs[kchan] += dev2;
		    chanSkews[kchan] += ( dev2 * dev );
		    chanCurts[kchan] += ( dev2 * dev2 );
		}
	    }
	}

	for( int kchan=0; kchan < Nchannels; kchan++ ) {
	    chanMins[kchan] /= this.CoAdds;
	    chanMaxs[kchan] /= this.CoAdds;
	    chanMeans[kchan] /= this.CoAdds;
	    double sigma = Math.sqrt( chanStdevs[kchan]/Npixchan );
	    chanStdevs[kchan] = sigma/Math.sqrt( this.Ndifs * this.CoAdds );
	    chanSkews[kchan] = chanSkews[kchan]/Npixchan;
	    chanCurts[kchan] = chanCurts[kchan]/Npixchan;
	    if( sigma > 0 ) {
		chanSkews[kchan] /= Math.pow( sigma, 3 );
		chanCurts[kchan] /= Math.pow( sigma, 4 );
		chanCurts[kchan] -= 3;
	    }
	}
    }

    public String getShortNameNum() {
	int pos = nameNum.lastIndexOf("/");
	if (pos == -1) return nameNum;
	return nameNum.substring(pos+1);
    }
}




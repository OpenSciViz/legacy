package ufjdd;
/**
 * Title:        HistogramPanel.java
 * Version:      (see rcsID)
 * Authors:      Ziad Saleh and Frank Varosi
 * Company:      University of Florida
 * Description:  Object for displaying vertical histogram of byte values in an image.
 */
import java.util.*;
import javax.swing.*;
import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;
import javaUFLib.*;

class HistogramPanel extends JPanel {

    public static final
	String rcsID = "$Name:  $ $Id: HistogramPanel.java,v 1.2 2006/07/24 16:29:56 warner Exp $";

    public HistogramBar histoBar;
    public ColorBar colorBar;
    public JToggleButton histnormButton;
    public int xSize, ySize;

    protected UFLabel overMaxLab;
    private ColorMapDialog colorMapControl;
    private ZoomImage zoomImage;

    public HistogramPanel( ColorMapDialog colorMap, int colBarWidth, int histoBarWidth )
    {
	this.colorMapControl = colorMap;
	int Ncolors = colorMap.Ncolors;
        colorBar = new ColorBar( colorMapControl.colorModel, colBarWidth, Ncolors );
        overMaxLab  = new UFLabel("Colormap Histogram");
	histoBar = new HistogramBar( histoBarWidth, Ncolors, overMaxLab);

        overMaxLab.setBorder(BorderFactory.createLoweredBevelBorder());
        histnormButton = new JToggleButton("Apply Histog Eq");

	xSize = colBarWidth + histoBarWidth + 9;
	ySize = 1;
        histnormButton.setBounds( 1, ySize, xSize, 40);
	ySize += 50;
        overMaxLab.setBounds( 1, ySize, xSize, 20);
	ySize += 25;
        colorBar.setBounds( 1, ySize, colBarWidth, Ncolors );
        histoBar.setBounds( 9 + colBarWidth, ySize, histoBarWidth, Ncolors );
	ySize += (Ncolors+5);
	ySize += 11;

	this.setLayout(null);
        this.add(histnormButton);
        this.add(overMaxLab);
        this.add(colorBar);
        this.add(histoBar);

        histnormButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e)
 		{
		    if( histnormButton.getText().toUpperCase().indexOf("APPLY") >= 0 ){
			histnormButton.setText("Undo Histog Eq");
			int[] eqHist = histoBar.equalizeHisto();
			IndexColorModel normColorModel = colorMapControl.normColorModel( eqHist );
			zoomImage.updateColorMap( normColorModel );
			colorBar.updateColorMap( normColorModel );
		    }
		    else {
			histnormButton.setText("Apply Histog Eq");
			histoBar.applyOrigHisto();
			zoomImage.updateColorMap( colorMapControl.colorModel );
			colorBar.updateColorMap( colorMapControl.colorModel );
		    }
		}
	    });
    }

    //method to set zoomImage pointer, used by histnormButton action Listener:
    public void setZoomImage( ZoomImage zoomImage ) { this.zoomImage = zoomImage; }
}


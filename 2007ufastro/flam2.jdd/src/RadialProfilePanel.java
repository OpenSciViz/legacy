package ufjdd;
/**
 * Title:        RadialProfilePanel.java
 * Version:      (see rcsID)
 * Authors:      Antonio Marin-Franch
 * Company:      University of Florida
 * Description:  For plotting image data pixel values along a user selected radial profile thru image in ZoomPanel.
 */
import javaUFLib.*;
import java.awt.*;
import java.awt.image.*;
import java.awt.geom.*;
import javax.swing.*;

public class RadialProfilePanel {

    public static final
	String rcsID = "$Name:  $ $Id: RadialProfilePanel.java,v 1.4 2006/08/30 20:04:06 warner Exp $";

    boolean first;
    NIRplotPanel plotPanel;
    
    public RadialProfilePanel(NIRplotPanel plotPanel)
    {
        this.plotPanel = plotPanel;
	first = true;
    }
    
    public void show()
    {
        if( ! first ) {
	    plotPanel.setVisible(true);
	}
    }

    public void update(double[][] data, float[] fwhm, boolean isCentrd, String dname, float[] cenPnt) {

	int nPix = data[0].length*data[1].length;
	int xCntr = (int)data[0].length/2;
	int yCntr = (int)data[1].length/2;
	float[] pixPlot  = new float[nPix];
	float[] dataPlot = new float[nPix];

	
	for(int i=0; i < data[0].length; i++) {
	   for (int j=0; j < data[1].length; j++) {
	      pixPlot[j+i*data[1].length] = (float)Math.sqrt( (xCntr-i)*(xCntr-i) + (yCntr-j)*(yCntr-j) );
	      dataPlot[j+i*data[1].length] = (float)( data[i][j] );	   
	   };
	};
	
	QuickSort(pixPlot, dataPlot, 0, nPix-1);
	
	  plotPanel.plot( pixPlot, dataPlot,
		   "*xtitle=Distance (pixels), *ytitle=DATA, *xminval=0, *psym=4, *symsize=2, *title="
		   + dname + "  FWHM = " + UFLabel.truncFormat(fwhm[0]) + ", *xminor=10, *xtickinterval=10, *xmaxval=" +  xCntr);

        if (isCentrd) {
	  plotPanel.xyouts(0.65f, 0.10f, "  FWHM = " + UFLabel.truncFormat(fwhm[0]), "*normal, *color=0, 0, 255, *charsize=14");
          plotPanel.xyouts(0.65f, 0.14f, "  std dev = " + UFLabel.truncFormat(fwhm[1]), "*normal, *color=51, 51, 51, *charsize=14");
	  plotPanel.xyouts(0.65f, 0.18f, "  X Cut FWHM = " + UFLabel.truncFormat(fwhm[2]), "*normal, *color=51, 51, 51, *charsize=14");
          plotPanel.xyouts(0.65f, 0.22f, "  Y Cut FWHM = " + UFLabel.truncFormat(fwhm[3]), "*normal, *color=51, 51, 51, *charsize=14");
          plotPanel.xyouts(0.60f, 0.26f, "Cntrd=(" + cenPnt[0]+","+cenPnt[1]+")", "*normal, *color=51, 51, 51, *charsize=14");

	  float[] x = new float[nPix];
	  float[] y = new float[nPix];
	  float ymax = (float)UFArrayOps.maxValue( dataPlot ); 
	  for (int j = 0; j < nPix; j++) y[j] = ymax * (float)Math.exp( -j*j/(1.44*fwhm[0]/2*fwhm[0]/2) );
	  for (int j = 0; j < nPix; j++) x[j] = j;
	  plotPanel.overplot(x, y, ""); 
	}
    }

//--------------------------------------------------------------------------------------
    void QuickSort(float[] pixPlot, float[] dataPlot,int left, int right)  {
    
       int i = left;
       int j = right;
       float mediun = pixPlot[(left+right)/2];
       do
       {
          while(pixPlot[i] < mediun) { i++; }
          while(pixPlot[j] > mediun) { j--; }
          if ( i <= j)
             {
             float aux1 = pixPlot[i];
             float aux2 = dataPlot[i];
             pixPlot[i] = pixPlot[j];
             pixPlot[j] = aux1;
             dataPlot[i] = dataPlot[j];
             dataPlot[j] = aux2;
             i++;
             j--;
             }   
       } while (i <= j);
       if( j > left )
       QuickSort(pixPlot, dataPlot, left, j );
       if( i < right )
       QuickSort(pixPlot, dataPlot, i, right );
    }
    
}

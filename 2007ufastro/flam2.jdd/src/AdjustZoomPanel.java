package ufjdd;
/**
 * Title:       AdjustZoomPanel.java
 * Version:     (see rcsID)
 * Authors:     Frank Varosi, Craig Warner
 * Company:     University of Florida
 * Description: Object for adjusting the display scaling of image in the ZoomPanel.
 */
import javaUFLib.*;

public class AdjustZoomPanel extends AdjustPanel {
    
    public static final
	String rcsID = "$Name:  $ $Id: AdjustZoomPanel.java,v 1.3 2005/11/29 21:08:10 drashkin Exp $";

    private ZoomImage zoomImage;
    
    public AdjustZoomPanel() {
        super(null);
    }

    public void setZoomImage( ZoomImage zoomImage ) { this.zoomImage = zoomImage; }

    //just need to override the basic image display methods:

    public void updateMinMax() {
	if( zoomImage.zoomBuffer == null ) return;
        this.CoAdds = zoomImage.zoomBuffer.CoAdds;
        this.scaleFactor = zoomImage.zoomBuffer.scaleFactor;
	if( CoAdds <= 0 ) CoAdds = 1;
	if( scaleFactor <= 0 ) scaleFactor = 1;
	this.imin = zoomImage.zoomBuffer.min;
	this.imax = zoomImage.zoomBuffer.max;
	this.fmin = zoomImage.zoomBuffer.s_min;
	this.fmax = zoomImage.zoomBuffer.s_max;
	displayMinMax();
    }


    public void updateMinMax(int newMin, int newMax) {
        if( zoomImage.zoomBuffer == null ) return;
        this.CoAdds = zoomImage.zoomBuffer.CoAdds;
        //this.scaleFactor = zoomImage.zoomBuffer.scaleFactor;
        if( CoAdds <= 0 ) CoAdds = 1;
        if( scaleFactor <= 0 ) scaleFactor = 1;
        this.imin = newMin;
        this.imax = newMax;
        this.fmin = (float)(newMin);
        this.fmax = (float)(newMax);
        displayMinMax();
    }

    public void displayMinMax()
    {
	if( useScaleFactor ) {
	    minValLabel.setText( UFLabel.truncFormat( fmin * scaleFactor ) );
	    maxValLabel.setText( UFLabel.truncFormat( fmax * scaleFactor ) );
	}
	else {
	    minValLabel.setText( UFLabel.truncFormat( fmin ) );
	    maxValLabel.setText( UFLabel.truncFormat( fmax ) );
	}
    }

    protected void reDrawImage() { zoomImage.applyLinearScale(); }

    protected void reDrawImage(float min, float max)
    {
	if( useScaleFactor )
	    zoomImage.applyLinearScale( min * CoAdds, max * CoAdds );
	else
	    zoomImage.applyLinearScale( min, max );
    }

    protected void reDrawImage(double threshHold)
    {
	if( useScaleFactor )
	    zoomImage.applyLogScale( threshHold * CoAdds );
	else
	    zoomImage.applyLogScale( threshHold );
    }

    protected void reDrawImage(double threshHold, double power)
    {
	if( useScaleFactor )
	    zoomImage.applyPowerScale( threshHold * CoAdds, power );
	else
	    zoomImage.applyPowerScale( threshHold, power );
    }
}

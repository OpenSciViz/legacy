package ufjdd;
/**
 * Title:        HistogramBar.java
 * Version:      (see rcsID)
 * Authors:      Ziad Saleh and Frank Varosi
 * Company:      University of Florida
 * Description:  Object for displaying vertical histogram of byte values in an image.
 */
import java.awt.*;
import javax.swing.*;
import javaUFLib.*;

class HistogramBar extends JPanel {

    public static final
	String rcsID = "$Name:  $ $Id: HistogramBar.java,v 1.3 2006/07/24 16:29:56 warner Exp $";

    protected int[] histogram;
    protected int[] histoDisp;
    protected int width = 100;
    protected int Ncolors = 256;
    protected int histoMax;
    protected int zero_index = -1 ;
    protected int underMinCnt = 0;
    protected int overMaxCnt  = 0;
    protected double minVal, maxVal;
    protected JLabel overMaxLab;
    
    public HistogramBar( int width, int Ncolors, JLabel overMaxLab)
    {
	if( width > 1 ) this.width = width;
	if( Ncolors > 1 ) this.Ncolors = Ncolors;
	this.overMaxLab = overMaxLab;
        this.setPreferredSize( new Dimension( width, Ncolors ) );
        this.setBackground(Color.WHITE);
	histogram = new int[Ncolors];
	histoDisp = new int[Ncolors];
    }

    public void paintComponent( Graphics g ) {

        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D)g;
	g2d.setPaint(Color.black);
        
        for( int i=0; i < histoDisp.length; i++ )
	    {
		if( zero_index == i ) {
		    g2d.setPaint(Color.red);
		    g2d.drawRect(0, Ncolors-i, width, 1);
		    g2d.setPaint(Color.black);
		}
		else g2d.drawRect(0, Ncolors-i, histoDisp[i], 1);
	    }
    }
    
    public void updateHistogram( byte dat[][], int zeroIndex, double dmin, double dmax, double sf ) {

        for( int i=0; i < histogram.length; i++ ) histogram[i] = 0;
        
        for( int i=0; i < dat.length; i++ ) {
            for( int j=0; j < dat.length; j++ ) {
		int bv = (int)dat[i][j];
		if( bv < 0 ) bv += 256;
		++histogram[ bv ];
	    }
	}
        
	minVal = dmin * sf;
	maxVal = dmax * sf;

	scaleHistogram();
	zero_index = zeroIndex;
        repaint();
    }
    
    public void updateHistogram( byte dat[], int zeroIndex, double dmin, double dmax, double sf ) {

        for( int i=0; i < histogram.length; i++ ) histogram[i] = 0;
        
	for( int j=0; j < dat.length; j++ ) {
	    int bv = (int)dat[j];
	    if( bv < 0 ) bv += 256;
	    ++histogram[ bv ];
	}

	minVal = dmin * sf;
	maxVal = dmax * sf;

	scaleHistogram();
	zero_index = zeroIndex;
        repaint();
    }
    
    private void scaleHistogram() {

	underMinCnt = histogram[0];
	histogram[0] = 0;
	overMaxCnt = histogram[histogram.length-1];
	histogram[histogram.length-1] = 0;
        //overMaxLab.setText( overMaxCnt + ">=Max: " + UFLabel.truncFormat( maxVal ) );

        histoMax = (int)UFArrayOps.maxValue( histogram );

	String s = "<html>"+overMaxCnt + " points &gt;= Max: " + UFLabel.truncFormat( maxVal );
        s += "<br>"+underMinCnt + " points &lt;= Min: " + UFLabel.truncFormat( minVal );
	s += "<br>Max Freq: "+histoMax+"</html>";
	this.setToolTipText(s);

        for(int i=0; i<histogram.length; i++)
            histoDisp[i] = Math.round( width * (float)histogram[i]/(float)histoMax );
    }

    public int[] equalizeHisto() {
        int accum = 0;
        int[] accHist = new int[Ncolors];
        
        for(int i=0; i<histogram.length; i++) {
            accum += histogram[i];
            accHist[i] = accum;
        }

        int accMax = accHist[accHist.length-1];
	int[] eqHist = new int[Ncolors];

        for( int i=0; i < accHist.length; i++ )
	{
            float f = (float)accHist[i]/(float)accMax;

            if(f<0) {
                eqHist[i] = 0; 
            }
            else if(f>1) {
                eqHist[i] = 255;
            }
            else
                eqHist[i] = Math.round(f*255);
        }
        
        for( int i=0; i<eqHist.length; i++ ) histoDisp[i] = ( width * eqHist[i] )/255;
        
        repaint();
        return eqHist;
    }
    
    public void applyOrigHisto() {

        for( int i=0; i < histogram.length; i++ )
            histoDisp[i] = Math.round( width * (float)histogram[i]/(float)histoMax );

        repaint();
    }
}


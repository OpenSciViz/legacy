package ufjdd;
/**
 * Title:        ContourPanel.java
 * Version:      (see rcsID)
 * Authors:      Antonio Marin-Franch
 * Company:      University of Florida
 * Description:  For plotting image data pixel values along a user selected radial profile thru image in ZoomPanel.
 */
import javaUFLib.*;
import java.awt.*;
import java.awt.image.*;
import java.awt.geom.*;
import javax.swing.*;

public class ContourPanel {

    public static final
	String rcsID = "$Name:  $ $Id: ContourPanel.java,v 1.1 2005/11/29 21:08:10 drashkin Exp $";

    boolean first;
    NIRplotPanel plotPanel;
    
    public ContourPanel(NIRplotPanel plotPanel)
    {
        this.plotPanel = plotPanel;
	first = true;
    }
    
    public void show()
    {
        if( ! first ) {
	    plotPanel.setVisible(true);
	}
    }

    public void update(double[][] data, String dname) {

	int nPix = data[0].length*data[1].length;
	float[] xPlot = new float[nPix];
	float[] yPlot = new float[nPix];
	float[] dataValue = new float[nPix];
	
	for(int i=0; i < data[0].length; i++) {
	   for (int j=0; j < data[1].length; j++) {
	      dataValue[j+i*data[1].length] = (float)data[i][j];	   
	   };
	};
		
	float maxVal = UFArrayOps.maxValue( dataValue );
	float minVal = UFArrayOps.minValue( dataValue );
	
	// isocurve values
	float gap = (maxVal-minVal)/20;
	float isoVal1 = minVal;
	float isoVal2 = minVal+1*gap;
	float isoVal3 = minVal+2*gap;
	float isoVal4 = minVal+3*gap;
	float isoVal5 = minVal+4*gap;
	float isoVal6 = minVal+5*gap;
	float isoVal7 = minVal+6*gap;
	float isoVal8 = minVal+7*gap;
	float isoVal9 = minVal+8*gap;
	float isoVal10= minVal+9*gap;
	float isoVal11= minVal+10*gap;
	float isoVal12= minVal+11*gap;
	float isoVal13= minVal+12*gap;
	float isoVal14= minVal+13*gap;
	float isoVal15= minVal+14*gap;
	float isoVal16= minVal+15*gap;
	float isoVal17= minVal+16*gap;
	float isoVal18= minVal+17*gap;
	float isoVal19= minVal+18*gap;
	float isoVal20= minVal+19*gap;
	
	for(int i=0; i < data[0].length; i++) {
	   for (int j=0; j < data[1].length; j++) {
	     if ( isoVal5  < data[i][j] && data[i][j] < isoVal7 ) {
	          xPlot[j+i*data[1].length] = j;
	          yPlot[j+i*data[1].length] = data[0].length-i;
	     }
	   };
	};

	plotPanel.plotCont( xPlot, yPlot,
		   "*xtitle=pixels, *ytitle=pixels, *xminval=0, *psym=14, *symsize=3, *title="
		   + dname + ", *xminor=10, *xtickinterval=10, *xrange=[0,28], *yrange=[4,24]");

	for(int i=0; i < data[0].length; i++) {
	   for (int j=0; j < data[1].length; j++) {
	     if ( isoVal9  < data[i][j] && data[i][j] < isoVal11 ) {
	          xPlot[j+i*data[1].length] = j;
	          yPlot[j+i*data[1].length] = data[0].length-i;
	     }
	   };
	};

	plotPanel.plotCont( xPlot, yPlot, "");

	for(int i=0; i < data[0].length; i++) {
	   for (int j=0; j < data[1].length; j++) {
	     if ( isoVal15  < data[i][j] && data[i][j] < isoVal17 ) {
	          xPlot[j+i*data[1].length] = j;
	          yPlot[j+i*data[1].length] = data[0].length-i;
	     }
	   };
	};

	plotPanel.plotCont( xPlot, yPlot, "");

    }
    
}

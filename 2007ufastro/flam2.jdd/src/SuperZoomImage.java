package ufjdd;
/**
 * Title:        SuperZoomImage.java
 * Version:      (see rcsID)
 * Copyright:    Copyright (c) 2006
 * Author:       Craig Warner, Frank Varosi, David Rashkin
 * Company:      University of Florida
 * Description:  For quick-look image display and analysis of Near-IR Camera data stream.
 */
import java.awt.*;
import java.awt.image.*;
import java.awt.geom.*;
import java.awt.event.*;
import javax.swing.*;
import javaUFLib.*;
import javaUFProtocol.*;

public class SuperZoomImage extends JPanel {

    public static final
	String rcsID = "$Name:  $ $Id: SuperZoomImage.java,v 1.3 2006/07/25 18:50:45 warner Exp $";

    protected double zoomData[][];
    public int xySize, centerxy;
    protected byte pixBuffer[];
    protected Image pixImage = null;
    protected int xStart, yStart, zoomFactor=4;
    protected double scaleFactor;
    protected boolean useScaleFactor = false;
    protected IndexColorModel colorModel;
    protected final AdjustZoomPanel adjustZoom;
    protected ImageBuffer zoomBuffer;

    public SuperZoomImage( int zoomSize, IndexColorModel colorModel, AdjustZoomPanel adjZoom)
    {
	this.xySize = zoomSize;
	this.centerxy = (xySize/2)-2;
        this.colorModel = colorModel;
        this.adjustZoom = adjZoom;
        this.setBorder(BorderFactory.createLineBorder(Color.black, 1));
        this.setMinimumSize(new Dimension(xySize, xySize));
	
        pixBuffer = new byte[xySize*xySize];
        
    }

    public synchronized void updateImage(ImageBuffer zoomBuff, IndexColorModel colorModel)
    {
	this.zoomBuffer = zoomBuff;
        this.xStart = zoomBuff.xStart;
        this.yStart = zoomBuff.yStart;
        //this.zoomFactor = zoomBuff.zoomFactor;

	if( useScaleFactor )
	    this.scaleFactor = zoomBuffer.scaleFactor;
	else
	    this.scaleFactor = 1.0;

	adjustZoom.updateMinMax();
	adjustZoom.scaleAndDraw();
    }
    
    public void updateColorMap(IndexColorModel colorModel) {
        this.colorModel = colorModel;
        pixImage = createImage(new MemoryImageSource(xySize, xySize, colorModel, pixBuffer, 0, xySize));
        repaint();
    }
    
    public void paintComponent( Graphics g ) {
        super.paintComponent(g);
	pixImage = createImage(new MemoryImageSource(xySize, xySize, colorModel, pixBuffer, 0, xySize));
        
        if( pixImage == null ) return;

	g.drawImage( pixImage, 0, 0, null );
	g.setColor(Color.BLACK);
	g.drawRect(centerxy-1, centerxy-1, 6, 6);
	g.setColor(Color.WHITE);
	g.drawRect(centerxy, centerxy,4,4);
    }

    // magnify the array by a zoomFactor and convert from 2-D array into 1-D pixBuffer:

    public void zoomExpand( byte array[][] ) {
        for( int i=0; i < array.length; i++ ) {
	    int zi = i*zoomFactor;
            for( int j=0; j < array[i].length; j++ ) {
		int zj = j*zoomFactor;
		byte data = array[i][j];
                for( int k=0; k < zoomFactor; k++ ) {
		    int zindex = (k + zi) * xySize + zj;
                    for( int m=0; m < zoomFactor; m++ ) pixBuffer[zindex++] = data;
		}
	    }
	}
    }

}

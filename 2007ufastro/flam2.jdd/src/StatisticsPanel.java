package ufjdd;
/**
 * Title:        StatisticsPanel.java
 * Version:      (see rcsID)
 * Authors:      Craig Warner, Frank Varosi
 * Company:      University of Florida
 * Description:  For plotting histogram of image data pixel values in a box.
 */
import javaUFLib.*;
import java.awt.*;

public class StatisticsPanel {
    
    public static final
	String rcsID = "$Name:  $ $Id: StatisticsPanel.java,v 1.6 2006/11/01 20:31:43 warner Exp $";

    boolean first;
    NIRplotPanel plotPanel;

    public StatisticsPanel(NIRplotPanel plotPanel)
    {
        this.plotPanel = plotPanel;
	first = true;
    }

    public void show()
    {
        if( ! first ) {
	    plotPanel.setVisible(true);
	}
    }

    public void update(double[][] data, int x1, int y1, int x2, int y2, double scaleFac, int Ndifs, String name)
    {
        double[][] eData = UFArrayOps.extractValues( data, y1, y2, x1, x2 );
	int ny = eData.length;
	int nx = eData[0].length;
	float[] evData = new float[ nx * ny ];
	int k=0;
	for( int i=0; i < ny; i++ ) {
	    for( int j=0; j < nx; j++ ) evData[k++] = (float)( eData[i][j] * scaleFac );
	}
	plotHistogram( evData, scaleFac, Ndifs, name );
    }

    public void update( double[][] data, int x1, int y1, int x2, int y2,
			float min, float max, double scaleFac, int Ndifs, String name )
    {
        double[][] eData = UFArrayOps.extractValues( data, y1, y2, x1, x2 );
	int ny = eData.length;
	int nx = eData[0].length;
	float[] evData = new float[ nx * ny ];
	int kp=0;
	for( int i=0; i < ny; i++ ) {
	    for( int j=0; j < nx; j++ ) {
		float edat = (float)( eData[i][j] * scaleFac );
		if( edat >= min && edat <= max ) evData[kp++] = edat;
	    }
	}
	plotHistogram( UFArrayOps.extractValues( evData, 0, kp-1 ), scaleFac, Ndifs, name );
    }

    private void plotHistogram( float[] data, double scaleFac, int Ndifs, String name )
    {
        double mean = UFArrayOps.avgValue( data );
	double stddev = UFArrayOps.stddev( data );
	if( Ndifs < 1 ) Ndifs = 1;
	double stdevFT = stddev/Math.sqrt( scaleFac * Ndifs );
	int npts = data.length;
	int nbins = 10*(int)(Math.floor( Math.sqrt( npts )/42 ) + 1 );
	float[] histY = plotPanel.hist( data, "*nbins=" + nbins + ", *xtitle=Data Values, *ytitle=Frequency, *title=" + name + "  :  Npts = " + npts);
	plotPanel.xyouts(0.15f,0.12f,"Mean = " + UFLabel.truncFormat(mean)+"  :  Std. Dev. = " + UFLabel.truncFormat( stddev ) +"  :  Std. Dev. / Frame = " + UFLabel.truncFormat( stdevFT ), "*charsize=12,*normal");
	float max = (float)UFArrayOps.maxValue(data);
	float min = (float)UFArrayOps.minValue(data);
        //System.out.println("StatisticsPanel.plotHistogram> min=" + min + ", max=" + max);
	float rng = max - min;
	int npg = 200;
	float[] x = new float[npg];
	float[] y = new float[npg];
	float nplot = (float)npg;
	for (int j = 0; j < npg; j++) x[j] = min + j*rng/nplot;
	float[] z = UFArrayOps.divArrays( UFArrayOps.subArrays( x, (float)mean ), (float)stddev );
	float ymax = (float)UFArrayOps.maxValue( histY ); 
	for (int j = 0; j < npg; j++) y[j] = ymax * (float)Math.exp( -z[j]*z[j]/2 );
	for (int j = 0; j < npg; j++) x[j] = j*nbins/nplot;
	plotPanel.overplot(x, y, ""); 
    }
}

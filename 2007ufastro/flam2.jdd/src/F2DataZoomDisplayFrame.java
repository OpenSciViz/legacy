package ufjdd;
/**
 * Title:        Java Data Display  (JDD) main Frame.
 * Version:      (see rcsID)
 * Copyright:    Copyright (c) 2005
 * Author:       Antonio Marin-Franch, Frank Varosi, Ziad Saleh
 * Company:      University of Florida
 * Description:  For quick-look image display and analysis of Near-IR Camera data stream.
 */
import javaUFLib.*;
import javaUFProtocol.*;
import ufjca.*;
//import TelescopeServer.*;

import java.util.*;
import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;
import java.net.*;

public class F2DataZoomDisplayFrame extends DataZoomDisplayFrame {
    
    public static final
	String rcsID = "$Name:  $ $Id: F2DataZoomDisplayFrame.java,v 1.17 2006/08/28 17:14:46 flam Exp $";

    JButton ufgtakeStartButton;
    JButton ufgtakeStopButton;
    String BAKremoteDir, BAKlut, BAKcnt, BAKcmd;
    String defDir = "/nfs/flam2sparc/data/2006_b", defCopyDir = "/home/flam/Data/2006";
    String defLutDir = "/LUT_FILE_DIR";

    public F2DataZoomDisplayFrame(GraphicsConfiguration gc, String tcshost, String tcsport,  final ZoomPanel zoomPanel, final ColorMapDialog colorMapDialog, DataAccessPanel fullDisplPanel, final NIRplotPanel plotPanel, String args[])
    {
        super(gc, tcshost, tcsport, zoomPanel, colorMapDialog, fullDisplPanel, plotPanel, args);
	setupMenuBar();

	BAKcnt = "0"; BAKlut = ""; BAKcmd = ""; BAKremoteDir = "";
	final DataAccessPanel classForInners = fullDisplPanel;
        this.setTitle("Flamingos-2 Java  Data  Display (version Beta 1)");
		
	ufgtakePanel = new JPanel();
	ufgtakePanel.setLayout(new GridLayout(1,2,0,20));
	ufgtakeStartButton = new JButton("ufgtake Start");
	ufgtakeStartButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    String epicsPrefix = classForInners.getEPICSPrefix().trim();
		    if (!epicsPrefix.endsWith(":")) epicsPrefix += ":";
		    UFCAToolkit.put(epicsPrefix+"dc:mcecmd.Action","start");
		    UFCAToolkit.put(epicsPrefix+"apply.DIR","3");
		}
	    });
	ufgtakeStopButton = new JButton("ufgtake Stop");
	ufgtakeStopButton.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ae) {
		    String epicsPrefix = classForInners.getEPICSPrefix().trim();
		    if (!epicsPrefix.endsWith(":")) epicsPrefix += ":";
		    UFCAToolkit.put(epicsPrefix+"dc:mcecmd.Action","stop");
		    UFCAToolkit.put(epicsPrefix+"apply.DIR","3");
		}
	    });
	ufgtakePanel.add(ufgtakeStartButton);
	ufgtakePanel.add(ufgtakeStopButton);

	ufgtakePanel.setBounds(zoomPanel.xSize+53,520,230,40);
	
	zoomDisplPanel.add( ufgtakePanel );
        
	//setupUFGTAKEVisuals(false);
	ufgtakeStartButton.setVisible(false);
	ufgtakeStopButton.setVisible(false);
    }
    
//-------------------------------------------------------------------------------

    private void setupMenuBar() {
	JMenuBar menuBar = new JMenuBar();
	JMenu menuFile = new JMenu("File");
	JMenuItem menuFileLoad = new JMenuItem("Load FITS");
	menuFile.add(menuFileLoad);

	menuFileLoad.addActionListener(new ActionListener() {
	   public void actionPerformed(ActionEvent ev) {
	      JFileChooser jfc = new JFileChooser(loadDir);
	      int returnVal = jfc.showOpenDialog((Component)ev.getSource());
              if (returnVal == JFileChooser.APPROVE_OPTION) {
		String filename = jfc.getSelectedFile().getAbsolutePath();
	        loadDir = jfc.getCurrentDirectory();
		UFFITSheader imgFITShead = new UFFITSheader();
		UFProtocol data = imgFITShead.readFITSfile(filename);
                InitDataDisplay.jddFullFrame.setSrcFileName(filename);
                InitDataDisplay.jddFullFrame.setTitleBarText();
		if (fullDisplPanel.mode == fullDisplPanel.MODE_UFGTAKE) {
                   fullDisplPanel.changeMode(fullDisplPanel.MODE_NOHOST);
		   fullDisplPanel.updateFrames(new ImageBuffer(fullDisplPanel.frameConfig, data));
		   fullDisplPanel.changeMode(fullDisplPanel.MODE_UFGTAKE);
		}
		else fullDisplPanel.updateFrames(new ImageBuffer(fullDisplPanel.frameConfig, data));
	      }
	   }
	});

	JMenuItem menuFileBgd = new JMenuItem("Load into bgd");
	menuFile.add(menuFileBgd);
	menuFileBgd.addActionListener(new ActionListener() {
	   public void actionPerformed(ActionEvent ev) {
              JFileChooser jfc = new JFileChooser(loadDir);
              int returnVal = jfc.showOpenDialog((Component)ev.getSource());
              if (returnVal == JFileChooser.APPROVE_OPTION) {
                String filename = jfc.getSelectedFile().getAbsolutePath();
                loadDir = jfc.getCurrentDirectory();
                UFFITSheader imgFITShead = new UFFITSheader();
                UFProtocol data = imgFITShead.readFITSfile(filename);
		ImageBuffer imgBuff = new ImageBuffer(fullDisplPanel.frameConfig, data);
		InitDataDisplay.jddFullFrame.imagePanels[0].imageDisplay.sendToBgd(imgBuff);
                InitDataDisplay.jddFullFrame.imagePanels[0].imageDisplay.updateBackgroundPreview(imgBuff);
                String zModeBoxSel = (String)InitDataDisplay.jddFullFrame.zModeBox.getSelectedItem();
                InitDataDisplay.jddFullFrame.updateBuffer(InitDataDisplay.jddFullFrame.imagePanels, zModeBoxSel);
                InitDataDisplay.jddFullFrame.setBgdFileName(filename);
	      }
	   }
	});

	menuFile.add(new JSeparator());
	JMenuItem menuFilePrefs = new JMenuItem("Preferences");
	menuFile.add(menuFilePrefs);
	menuFilePrefs.addActionListener(new ActionListener() {
	   public void actionPerformed(ActionEvent ev) {
	      JFrame prefsFrame = new JFrame("Preferences");
	      preferences.setFrame(prefsFrame);
	      prefsFrame.getContentPane().add(preferences);
	      prefsFrame.pack();
	      prefsFrame.setVisible(true);
	   }
	});
	menuFile.add(new JSeparator());
	JMenuItem menuFileExit = new JMenuItem("Exit");
        menuFile.add( menuFileExit );
	JMenu menuHelp = new JMenu("Help");
	JMenuItem menuHelpAbout = new JMenuItem("About");
        menuHelp.add( menuHelpAbout );
        
        menuFileExit.addActionListener( new ActionListener() {
	    public void actionPerformed(ActionEvent e) {
		Object[] exitOptions = {"Exit","Cancel"};
                int n = JOptionPane.showOptionDialog(F2DataZoomDisplayFrame.this, "Are you sure you want to quit?", "Exit JDD?", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, exitOptions, exitOptions[1]);
		if (n == 0) {
		    System.out.println("JDD exiting...");
                    System.exit(0);
                }
	    }
	});

	JMenu menuMode = new JMenu("Mode");
	final JMenuItem menuModeNohost = new JMenuItem("No Host");
        menuModeNohost.setBackground(Color.GREEN);
	final JMenuItem menuModeRegular = new JMenuItem("Regular");
	final JMenuItem menuModeReplicant = new JMenuItem("Replicant");
	final JMenuItem menuModeUfgtake = new JMenuItem("ufgtake");
	final Color defaultBackground = menuModeRegular.getBackground();
	menuMode.add(menuModeNohost);
	menuMode.add(menuModeRegular);
	menuMode.add(menuModeReplicant);
	menuMode.add(menuModeUfgtake);

	menuModeNohost.addActionListener(new ActionListener() {
	   public void actionPerformed(ActionEvent ev) {
	      final JFrame popupFrame = new JFrame("Change Mode?");
	      Container contentPane = popupFrame.getContentPane();
              SpringLayout layout = new SpringLayout();
	      contentPane.setLayout(layout);
	      JLabel lConfirm = new JLabel("Switch mode to: No host?");
	      JButton bChange = new JButton("Change Mode");
	      JButton bCancel = new JButton("Cancel");
	      contentPane.add(lConfirm);
	      layout.putConstraint(SpringLayout.WEST, lConfirm, 30, SpringLayout.WEST, contentPane);
	      layout.putConstraint(SpringLayout.NORTH, lConfirm, 10, SpringLayout.NORTH, contentPane);
	      contentPane.add(bChange);
	      bChange.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ev) {
		   fullDisplPanel.changeMode(fullDisplPanel.MODE_NOHOST);
		   popupFrame.dispose();
                   menuModeNohost.setBackground(Color.GREEN);
                   menuModeRegular.setBackground(defaultBackground);
                   menuModeReplicant.setBackground(defaultBackground);
                   menuModeUfgtake.setBackground(defaultBackground);
		   setupUFGTAKEVisuals(false);
		}
	      });
	      layout.putConstraint(SpringLayout.WEST, bChange, 10, SpringLayout.WEST, contentPane);
	      layout.putConstraint(SpringLayout.NORTH, bChange, 15, SpringLayout.SOUTH, lConfirm);
	      contentPane.add(bCancel);
              bCancel.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ev) {
                   popupFrame.dispose();
                }
              });
              layout.putConstraint(SpringLayout.WEST, bCancel, 15, SpringLayout.EAST, bChange);
              layout.putConstraint(SpringLayout.NORTH, bCancel, 15, SpringLayout.SOUTH, lConfirm);
	      layout.putConstraint(SpringLayout.EAST, contentPane, 10, SpringLayout.EAST, bCancel);
              layout.putConstraint(SpringLayout.SOUTH, contentPane, 10, SpringLayout.SOUTH, bChange);
	      popupFrame.pack();
	      popupFrame.setVisible(true);
	   }
	});

        menuModeReplicant.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent ev) {
              final JFrame popupFrame = new JFrame("Change Mode?");
              Container contentPane = popupFrame.getContentPane();
              SpringLayout layout = new SpringLayout();
              contentPane.setLayout(layout);
              JLabel lConfirm = new JLabel("Switch mode to: Replicant?");
	      JLabel lHost = new JLabel("Host:");
	      final JTextField tHost = new JTextField(15);
	      tHost.setText(fullDisplPanel.getHost());
	      JLabel lPort = new JLabel("Port:");
	      final JTextField tPort = new JTextField(6);
	      tPort.setText(""+fullDisplPanel.getPort());
              JButton bChange = new JButton("Change Mode");
              JButton bCancel = new JButton("Cancel");
              contentPane.add(lConfirm);
              layout.putConstraint(SpringLayout.WEST, lConfirm, 30, SpringLayout.WEST, contentPane);
              layout.putConstraint(SpringLayout.NORTH, lConfirm, 10, SpringLayout.NORTH, contentPane);
	      contentPane.add(lHost);
              layout.putConstraint(SpringLayout.WEST, lHost, 10, SpringLayout.WEST, contentPane);
              layout.putConstraint(SpringLayout.NORTH, lHost, 15, SpringLayout.SOUTH, lConfirm);
	      contentPane.add(tHost);
              layout.putConstraint(SpringLayout.WEST, tHost, 10, SpringLayout.EAST, lHost);
              layout.putConstraint(SpringLayout.NORTH, tHost, 15, SpringLayout.SOUTH, lConfirm);
	      contentPane.add(lPort);	
              layout.putConstraint(SpringLayout.WEST, lPort, 10, SpringLayout.WEST, contentPane);
              layout.putConstraint(SpringLayout.NORTH, lPort, 15, SpringLayout.SOUTH, lHost);
	      contentPane.add(tPort);
              layout.putConstraint(SpringLayout.WEST, tPort, 12, SpringLayout.EAST, lPort);
              layout.putConstraint(SpringLayout.NORTH, tPort, 15, SpringLayout.SOUTH, lHost);
              contentPane.add(bChange);
              bChange.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ev) {
		   String theHost = tHost.getText().trim();
		   int thePort = Integer.parseInt(tPort.getText().trim());
		   fullDisplPanel.setHostAndPort(theHost, thePort);
                   fullDisplPanel.changeMode(fullDisplPanel.MODE_REPLICANT);
                   popupFrame.dispose();
                   menuModeNohost.setBackground(defaultBackground);
                   menuModeRegular.setBackground(defaultBackground);
                   menuModeReplicant.setBackground(Color.GREEN);
                   menuModeUfgtake.setBackground(defaultBackground);
		   setupUFGTAKEVisuals(false);
                }
              });
              layout.putConstraint(SpringLayout.WEST, bChange, 10, SpringLayout.WEST, contentPane);
              layout.putConstraint(SpringLayout.NORTH, bChange, 15, SpringLayout.SOUTH, lPort);
              contentPane.add(bCancel);
              bCancel.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ev) {
                   popupFrame.dispose();
                }
              });
              layout.putConstraint(SpringLayout.WEST, bCancel, 15, SpringLayout.EAST, bChange);
              layout.putConstraint(SpringLayout.NORTH, bCancel, 15, SpringLayout.SOUTH, lPort);
              layout.putConstraint(SpringLayout.EAST, contentPane, 50, SpringLayout.EAST, tHost);
              layout.putConstraint(SpringLayout.SOUTH, contentPane, 10, SpringLayout.SOUTH, bChange);
              popupFrame.pack();
              popupFrame.setVisible(true);
           }
        });

        menuModeRegular.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent ev) {
              final JFrame popupFrame = new JFrame("Change Mode?");
              Container contentPane = popupFrame.getContentPane();
              SpringLayout layout = new SpringLayout();
              contentPane.setLayout(layout);
              JLabel lConfirm = new JLabel("Switch mode to: Regular?");
              JLabel lHost = new JLabel("Host:");
              final JTextField tHost = new JTextField(15);
              tHost.setText(fullDisplPanel.getHost());
              JLabel lPort = new JLabel("Port:");
              final JTextField tPort = new JTextField(6);
              tPort.setText(""+fullDisplPanel.getPort());
	      JLabel lEpics = new JLabel("EPICS Prefix:");
	      final JTextField tEpics = new JTextField(10);
	      tEpics.setText(fullDisplPanel.getEPICSPrefix());
              JButton bChange = new JButton("Change Mode");
              JButton bCancel = new JButton("Cancel");
              contentPane.add(lConfirm);
              layout.putConstraint(SpringLayout.WEST, lConfirm, 30, SpringLayout.WEST, contentPane);
              layout.putConstraint(SpringLayout.NORTH, lConfirm, 10, SpringLayout.NORTH, contentPane);
              contentPane.add(lHost);
              layout.putConstraint(SpringLayout.WEST, lHost, 10, SpringLayout.WEST, contentPane);
              layout.putConstraint(SpringLayout.NORTH, lHost, 15, SpringLayout.SOUTH, lConfirm);
              contentPane.add(tHost);
              layout.putConstraint(SpringLayout.WEST, tHost, 10, SpringLayout.EAST, lHost);
              layout.putConstraint(SpringLayout.NORTH, tHost, 15, SpringLayout.SOUTH, lConfirm);
              contentPane.add(lPort);
              layout.putConstraint(SpringLayout.WEST, lPort, 10, SpringLayout.WEST, contentPane);
              layout.putConstraint(SpringLayout.NORTH, lPort, 15, SpringLayout.SOUTH, lHost);
              contentPane.add(tPort);
              layout.putConstraint(SpringLayout.WEST, tPort, 12, SpringLayout.EAST, lPort);
              layout.putConstraint(SpringLayout.NORTH, tPort, 15, SpringLayout.SOUTH, lHost);
              contentPane.add(lEpics);
              layout.putConstraint(SpringLayout.WEST, lEpics, 10, SpringLayout.WEST, contentPane);
              layout.putConstraint(SpringLayout.NORTH, lEpics, 15, SpringLayout.SOUTH, lPort);
              contentPane.add(tEpics);
              layout.putConstraint(SpringLayout.WEST, tEpics, 12, SpringLayout.EAST, lEpics);
              layout.putConstraint(SpringLayout.NORTH, tEpics, 15, SpringLayout.SOUTH, lPort);
              contentPane.add(bChange);
              bChange.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ev) {
                   String theHost = tHost.getText().trim();
                   int thePort = Integer.parseInt(tPort.getText().trim());
                   fullDisplPanel.setHostAndPort(theHost, thePort);
		   fullDisplPanel.setEPICSPrefix(tEpics.getText().trim());
                   fullDisplPanel.changeMode(fullDisplPanel.MODE_REGULAR);
                   popupFrame.dispose();
                   menuModeNohost.setBackground(defaultBackground);
                   menuModeRegular.setBackground(Color.GREEN);
                   menuModeReplicant.setBackground(defaultBackground);
                   menuModeUfgtake.setBackground(defaultBackground);
		   setupUFGTAKEVisuals(false);
                }
              });
              layout.putConstraint(SpringLayout.WEST, bChange, 10, SpringLayout.WEST, contentPane);
              layout.putConstraint(SpringLayout.NORTH, bChange, 15, SpringLayout.SOUTH, lEpics);
              contentPane.add(bCancel);
              bCancel.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ev) {
                   popupFrame.dispose();
                }
              });
              layout.putConstraint(SpringLayout.WEST, bCancel, 15, SpringLayout.EAST, bChange);
              layout.putConstraint(SpringLayout.NORTH, bCancel, 15, SpringLayout.SOUTH, lEpics);
              layout.putConstraint(SpringLayout.EAST, contentPane, 10, SpringLayout.EAST, tHost);
              layout.putConstraint(SpringLayout.SOUTH, contentPane, 10, SpringLayout.SOUTH, bChange);
              popupFrame.pack();
              popupFrame.setVisible(true);
           }
        });

	menuModeUfgtake.addActionListener(new ActionListener() {
	   public void actionPerformed(ActionEvent ev) {
	      final JFrame popupFrame = new JFrame("Change Mode?");
              Container contentPane = popupFrame.getContentPane();
              SpringLayout layout = new SpringLayout();
              contentPane.setLayout(layout);
              JLabel lConfirm = new JLabel("Switch mode to: ufgtake?");
	      JLabel lHostname = new JLabel();
	      try {
		lHostname.setText("Current host is: "+InetAddress.getLocalHost().getHostName());
	      } catch(Exception e) {}
              JButton bChange = new JButton("Change Mode");
              JButton bCancel = new JButton("Cancel");
              contentPane.add(lConfirm);
              layout.putConstraint(SpringLayout.WEST, lConfirm, 30, SpringLayout.WEST, contentPane);
              layout.putConstraint(SpringLayout.NORTH, lConfirm, 10, SpringLayout.NORTH, contentPane);
              JLabel lDir = new JLabel("<html>Directory:<br>(on flam2sparc)</html>");
              final JTextField tDir = new JTextField(40);
              final JTextField rshDir = new JTextField(35);
              final JTextField tCopyDir = new JTextField(40);
              tCopyDir.setText(defCopyDir);
	      if (new File(defDir).exists()) {
		fullDisplPanel.setUfgtakeDirectory(defDir);
	      } else {
		defDir = fullDisplPanel.getUfgtakeDirectory(); 
	      }
	      tDir.setText(fullDisplPanel.getUfgtakeDirectory());
              JButton bDir = new JButton("Browse");
	      bDir.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ev) {
		   JFileChooser jfc = new JFileChooser(defDir);
		   jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                   int returnVal = jfc.showOpenDialog((Component)ev.getSource());
                   if (returnVal == JFileChooser.APPROVE_OPTION) {
		      String filename = jfc.getSelectedFile().getAbsolutePath();
		      tDir.setText(filename);
		      if (filename.startsWith("/nfs/")) {
			int n = filename.indexOf("/",6);
			rshDir.setText(filename.substring(n));
		      }
		      if (filename.startsWith("/nfs/flam2sparc/data/2006_b/")) {
			int n = filename.indexOf("2006_b/")+7;
			if (tCopyDir.getText().startsWith("/home/flam/Data/2006")) {
			   tCopyDir.setText("/home/flam/Data/2006/"+filename.substring(n));
			   defCopyDir = "/home/flam/Data/2006/"+filename.substring(n);
			}
		      }
		      defDir = filename;
		   }
		}
	      });
              JLabel lPrefix = new JLabel("Prefix:");
              final JTextField tPrefix = new JTextField(16);
	      tPrefix.setText(fullDisplPanel.getUfgtakePrefix());
	      JLabel lIndex = new JLabel("Start Index:");
	      final JTextField tIndex = new JTextField(5);
	      tIndex.setText(""+fullDisplPanel.getUfgtakeStartIndex());
              JLabel lEpics = new JLabel("EPICS Prefix:");
              final JTextField tEpics = new JTextField(12);
              tEpics.setText(fullDisplPanel.getEPICSPrefix());
              JLabel lCopyDir = new JLabel("<html>Backup copy dir<br>(on dwarf)</html>:");
	      JButton bCopyDir = new JButton("Browse");
              bCopyDir.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ev) {
                   JFileChooser jfc = new JFileChooser(defCopyDir);
                   jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                   int returnVal = jfc.showOpenDialog((Component)ev.getSource());
                   if (returnVal == JFileChooser.APPROVE_OPTION) {
                      String filename = jfc.getSelectedFile().getAbsolutePath();
                      tCopyDir.setText(filename);
		      defCopyDir = filename;
                   }
                }
              });
	      JLabel lSpawn = new JLabel("Spawn ufgtake?");
	      ButtonGroup bgSpawn = new ButtonGroup();
	      final JRadioButton ySpawn = new JRadioButton("Yes");
	      final JRadioButton nSpawn = new JRadioButton("No", true);
	      bgSpawn.add(ySpawn);
	      bgSpawn.add(nSpawn);
	      final JLabel lDoRsh = new JLabel("Use RSH?");
	      ButtonGroup bgRsh = new ButtonGroup();
              final JRadioButton yRsh = new JRadioButton("Yes", true);
              final JRadioButton nRsh = new JRadioButton("No");
	      bgRsh.add(yRsh);
	      bgRsh.add(nRsh);
	      final JLabel lRshHost = new JLabel("RSH Host:");
	      final JTextField tRshHost = new JTextField(12);
	      tRshHost.setText("flam2sparc");
	      final JLabel lRshDir = new JLabel("<html>ufgtake dir:<br>(local on flam2sparc)</html>");
	      rshDir.setText(BAKremoteDir);
	      String tempFilename = tDir.getText();
	      if (tempFilename.startsWith("/nfs/")) {
		int n = tempFilename.indexOf("/",6);
	        rshDir.setText(tempFilename.substring(n));
	      }
              final JButton bRshDir = new JButton("Browse");
	      bRshDir.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ev) {
		   JFileChooser jfc = new JFileChooser(".");
		   jfc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                   int returnVal = jfc.showOpenDialog((Component)ev.getSource());
                   if (returnVal == JFileChooser.APPROVE_OPTION) {
		      String filename = jfc.getSelectedFile().getAbsolutePath();
		      rshDir.setText(filename);
		   }
		}
	      });
	      
              JLabel lCount = new JLabel("Count:");
              final JTextField tCount = new JTextField(5);
              tCount.setText(BAKcnt);
              JLabel lLut = new JLabel("LUT:");
              final JTextField tLut = new JTextField(40);
	      tLut.setText(BAKlut);
              final JButton bLut = new JButton("Browse");
              bLut.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ev) {
                   JFileChooser jfc = new JFileChooser(defLutDir);
                   int returnVal = jfc.showOpenDialog((Component)ev.getSource());
                   if (returnVal == JFileChooser.APPROVE_OPTION) {
                      String filename = jfc.getSelectedFile().getAbsolutePath();
                      tLut.setText(filename);
		      defLutDir = filename.substring(0, filename.lastIndexOf("/"));
                   }
                }
              });
              JLabel lEndian = new JLabel("LUT Type:");
              ButtonGroup bgEndian = new ButtonGroup();
	      final JRadioButton noLUT = new JRadioButton("None", true);
              final JRadioButton bigEndian = new JRadioButton("Big Endian");
              final JRadioButton litEndian = new JRadioButton("Little Endian");
	      bgEndian.add(noLUT);
              bgEndian.add(bigEndian);
              bgEndian.add(litEndian);
	      bigEndian.doClick();
	      JLabel lCommand = new JLabel("Options:");
	      final JTextField tCommand = new JTextField(25);
	      tCommand.setText(BAKcmd);
	      nRsh.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ev) {
		   if (nRsh.isSelected()) {
		      tRshHost.setEnabled(false);
		   }
		}
	      });
              yRsh.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ev) {
                   if (yRsh.isSelected()) {
                      tRshHost.setEnabled(true);
                   }
                }
              });
	      nSpawn.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent ev) {
		   boolean state = true;
		   if (nSpawn.isSelected()) {
		      state = false;
		   }
                   tRshHost.setEnabled(state);
                   yRsh.setEnabled(state);
                   nRsh.setEnabled(state);
		   tCount.setEnabled(state);
		   tLut.setEnabled(state);
                   bLut.setEnabled(state);
		   noLUT.setEnabled(state);
		   bigEndian.setEnabled(state);
		   litEndian.setEnabled(state);
		   tCommand.setEnabled(state);
		   rshDir.setEnabled(state);
		   bRshDir.setEnabled(state);
		}
	      });

              ySpawn.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ev) {
                   boolean state = false;
                   if (ySpawn.isSelected()) {
                      state = true; 
                   }
              	   tRshHost.setEnabled(state);
              	   yRsh.setEnabled(state);
              	   nRsh.setEnabled(state);
                   tCount.setEnabled(state);
                   tLut.setEnabled(state);
                   bLut.setEnabled(state);
                   noLUT.setEnabled(state);
                   bigEndian.setEnabled(state);
                   litEndian.setEnabled(state);
                   tCommand.setEnabled(state);
		   rshDir.setEnabled(state);
		   bRshDir.setEnabled(state);
                }
              });

	      //Disable spawning options
	      boolean state = false;
	      tRshHost.setEnabled(state);
	      yRsh.setEnabled(state);
	      nRsh.setEnabled(state);
              tCount.setEnabled(state);
              tLut.setEnabled(state);
              bLut.setEnabled(state);
              noLUT.setEnabled(state);
              bigEndian.setEnabled(state);
              litEndian.setEnabled(state);
              tCommand.setEnabled(state);
	      rshDir.setEnabled(state);
	      bRshDir.setEnabled(state);
	      contentPane.add(lConfirm);
	      

              layout.putConstraint(SpringLayout.WEST, lConfirm, 30, SpringLayout.WEST, contentPane);
              layout.putConstraint(SpringLayout.NORTH, lConfirm, 10, SpringLayout.NORTH, contentPane);
              contentPane.add(lHostname);
              layout.putConstraint(SpringLayout.WEST, lHostname, 30, SpringLayout.WEST, contentPane);
              layout.putConstraint(SpringLayout.NORTH, lHostname, 10, SpringLayout.SOUTH, lConfirm);
              contentPane.add(lDir);
              layout.putConstraint(SpringLayout.WEST, lDir, 10, SpringLayout.WEST, contentPane);
              layout.putConstraint(SpringLayout.NORTH, lDir, 15, SpringLayout.SOUTH, lHostname);
              contentPane.add(tDir);
              layout.putConstraint(SpringLayout.WEST, tDir, 30, SpringLayout.EAST, lDir);
              layout.putConstraint(SpringLayout.NORTH, tDir, 15, SpringLayout.SOUTH, lHostname);
              contentPane.add(bDir);
              layout.putConstraint(SpringLayout.WEST, bDir, 5, SpringLayout.EAST, tDir);
              layout.putConstraint(SpringLayout.NORTH, bDir, 15, SpringLayout.SOUTH, lHostname);
              contentPane.add(lPrefix);
              layout.putConstraint(SpringLayout.WEST, lPrefix, 10, SpringLayout.WEST, contentPane);
              layout.putConstraint(SpringLayout.NORTH, lPrefix, 15, SpringLayout.SOUTH, lDir);
              contentPane.add(tPrefix);
              layout.putConstraint(SpringLayout.WEST, tPrefix, 30, SpringLayout.EAST, lDir);
              layout.putConstraint(SpringLayout.NORTH, tPrefix, 15, SpringLayout.SOUTH, lDir);
              contentPane.add(lIndex);
              layout.putConstraint(SpringLayout.WEST, lIndex, 10, SpringLayout.WEST, contentPane);
              layout.putConstraint(SpringLayout.NORTH, lIndex, 15, SpringLayout.SOUTH, lPrefix);
              contentPane.add(tIndex);
              layout.putConstraint(SpringLayout.WEST, tIndex, 30, SpringLayout.EAST, lDir);
              layout.putConstraint(SpringLayout.NORTH, tIndex, 15, SpringLayout.SOUTH, lPrefix);
              contentPane.add(lEpics);
              layout.putConstraint(SpringLayout.WEST, lEpics, 10, SpringLayout.WEST, contentPane);
              layout.putConstraint(SpringLayout.NORTH, lEpics, 15, SpringLayout.SOUTH, lIndex);
              contentPane.add(tEpics);
              layout.putConstraint(SpringLayout.WEST, tEpics, 30, SpringLayout.EAST, lDir);
              layout.putConstraint(SpringLayout.NORTH, tEpics, 15, SpringLayout.SOUTH, lIndex);
              contentPane.add(lCopyDir);
              layout.putConstraint(SpringLayout.WEST, lCopyDir, 10, SpringLayout.WEST, contentPane);
              layout.putConstraint(SpringLayout.NORTH, lCopyDir, 15, SpringLayout.SOUTH, lEpics);
              contentPane.add(tCopyDir);
              layout.putConstraint(SpringLayout.WEST, tCopyDir, 30, SpringLayout.EAST, lDir);
              layout.putConstraint(SpringLayout.NORTH, tCopyDir, 15, SpringLayout.SOUTH, lEpics);
              contentPane.add(bCopyDir);
              layout.putConstraint(SpringLayout.WEST, bCopyDir, 5, SpringLayout.EAST, tCopyDir);
              layout.putConstraint(SpringLayout.NORTH, bCopyDir, 15, SpringLayout.SOUTH, lEpics);
	      contentPane.add(lSpawn);
              layout.putConstraint(SpringLayout.WEST, lSpawn, 10, SpringLayout.WEST, contentPane);
              layout.putConstraint(SpringLayout.NORTH, lSpawn, 30, SpringLayout.SOUTH, lCopyDir);
              contentPane.add(ySpawn);
              layout.putConstraint(SpringLayout.WEST, ySpawn, 12, SpringLayout.EAST, lSpawn);
              layout.putConstraint(SpringLayout.NORTH, ySpawn, 30, SpringLayout.SOUTH, lCopyDir);
              contentPane.add(nSpawn);
              layout.putConstraint(SpringLayout.WEST, nSpawn, 12, SpringLayout.EAST, ySpawn);
              layout.putConstraint(SpringLayout.NORTH, nSpawn, 30, SpringLayout.SOUTH, lCopyDir);
	      contentPane.add(lDoRsh);
              layout.putConstraint(SpringLayout.WEST, lDoRsh, 10, SpringLayout.WEST, contentPane);
              layout.putConstraint(SpringLayout.NORTH, lDoRsh, 15, SpringLayout.SOUTH, ySpawn);
              contentPane.add(yRsh);
              layout.putConstraint(SpringLayout.WEST, yRsh, 12, SpringLayout.EAST, lDoRsh);
              layout.putConstraint(SpringLayout.NORTH, yRsh, 15, SpringLayout.SOUTH, ySpawn);
              contentPane.add(nRsh);
              layout.putConstraint(SpringLayout.WEST, nRsh, 12, SpringLayout.EAST, yRsh);
              layout.putConstraint(SpringLayout.NORTH, nRsh, 15, SpringLayout.SOUTH, ySpawn);
              contentPane.add(lRshHost);
              layout.putConstraint(SpringLayout.WEST, lRshHost, 10, SpringLayout.WEST, contentPane);
              layout.putConstraint(SpringLayout.NORTH, lRshHost, 15, SpringLayout.SOUTH, yRsh);
              contentPane.add(tRshHost);
              layout.putConstraint(SpringLayout.WEST, tRshHost, 12, SpringLayout.EAST, lRshHost);
              layout.putConstraint(SpringLayout.NORTH, tRshHost, 15, SpringLayout.SOUTH, yRsh);
              contentPane.add(lRshDir);
              layout.putConstraint(SpringLayout.WEST, lRshDir, 10, SpringLayout.WEST, contentPane);
              layout.putConstraint(SpringLayout.NORTH, lRshDir, 15, SpringLayout.SOUTH, tRshHost);
              contentPane.add(rshDir);
              layout.putConstraint(SpringLayout.WEST, rshDir, 30, SpringLayout.EAST, lRshDir);
              layout.putConstraint(SpringLayout.NORTH, rshDir, 15, SpringLayout.SOUTH, tRshHost);
              contentPane.add(bRshDir);
              layout.putConstraint(SpringLayout.WEST, bRshDir, 5, SpringLayout.EAST, rshDir);
              layout.putConstraint(SpringLayout.NORTH, bRshDir, 15, SpringLayout.SOUTH, tRshHost);
              contentPane.add(lCount);
              layout.putConstraint(SpringLayout.WEST, lCount, 10, SpringLayout.WEST, contentPane);
              layout.putConstraint(SpringLayout.NORTH, lCount, 15, SpringLayout.SOUTH, lRshDir);
              contentPane.add(tCount);
              layout.putConstraint(SpringLayout.WEST, tCount, 12, SpringLayout.EAST, lRshHost);
              layout.putConstraint(SpringLayout.NORTH, tCount, 15, SpringLayout.SOUTH, lRshDir);
              contentPane.add(lLut);
              layout.putConstraint(SpringLayout.WEST, lLut, 10, SpringLayout.WEST, contentPane);
              layout.putConstraint(SpringLayout.NORTH, lLut, 15, SpringLayout.SOUTH, lCount);
              contentPane.add(tLut);
              layout.putConstraint(SpringLayout.WEST, tLut, 12, SpringLayout.EAST, lRshHost);
              layout.putConstraint(SpringLayout.NORTH, tLut, 15, SpringLayout.SOUTH, lCount);
              contentPane.add(bLut);
              layout.putConstraint(SpringLayout.WEST, bLut, 5, SpringLayout.EAST, tLut);
              layout.putConstraint(SpringLayout.NORTH, bLut, 15, SpringLayout.SOUTH, lCount);
              contentPane.add(lEndian);
              layout.putConstraint(SpringLayout.WEST, lEndian, 10, SpringLayout.WEST, contentPane);
              layout.putConstraint(SpringLayout.NORTH, lEndian, 15, SpringLayout.SOUTH, lLut);
              contentPane.add(noLUT);
              layout.putConstraint(SpringLayout.WEST, noLUT, 12, SpringLayout.EAST, lRshHost);
              layout.putConstraint(SpringLayout.NORTH, noLUT, 15, SpringLayout.SOUTH, lLut);
              contentPane.add(bigEndian);
              layout.putConstraint(SpringLayout.WEST, bigEndian, 12, SpringLayout.EAST, noLUT);
              layout.putConstraint(SpringLayout.NORTH, bigEndian, 15, SpringLayout.SOUTH, lLut);
              contentPane.add(litEndian);
              layout.putConstraint(SpringLayout.WEST, litEndian, 12, SpringLayout.EAST, bigEndian);
              layout.putConstraint(SpringLayout.NORTH, litEndian, 15, SpringLayout.SOUTH, lLut);
              contentPane.add(lCommand);
              layout.putConstraint(SpringLayout.WEST, lCommand, 10, SpringLayout.WEST, contentPane);
              layout.putConstraint(SpringLayout.NORTH, lCommand, 15, SpringLayout.SOUTH, lEndian);
              contentPane.add(tCommand);
              layout.putConstraint(SpringLayout.WEST, tCommand, 15, SpringLayout.EAST, lRshHost);
              layout.putConstraint(SpringLayout.NORTH, tCommand, 15, SpringLayout.SOUTH, lEndian);
              contentPane.add(bChange);
              bChange.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ev) {
		    //back up values into global vars
		    BAKremoteDir = rshDir.getText();
		    BAKlut = tLut.getText();
		    BAKcnt = tCount.getText();
		    BAKcmd = tCommand.getText();

		    //Start ufgtake if requested
		   if (ySpawn.isSelected()) {
		      boolean doRsh = false;
		      if (yRsh.isSelected()) doRsh = true;
		      String options = "";
		      String epicsdb = tEpics.getText().trim();
		      if (epicsdb.endsWith(":")) epicsdb = epicsdb.substring(0,epicsdb.length()-1);
		      //options+="-epics "+epicsdb;
		      options+="-epics";
		      String theDir = rshDir.getText().trim();
		      if (!theDir.endsWith("/")) theDir += "/";
		      options+=" -file "+theDir+tPrefix.getText().trim();
		      options+=" -index "+tIndex.getText().trim();
		      options+=" -cnt "+tCount.getText().trim();
		      String firstFilename = tDir.getText().trim();
		      if (!firstFilename.endsWith("/")) firstFilename+="/";
		      firstFilename+=tPrefix.getText().trim()+".";
		      String indStr = ""+tIndex.getText().trim();
		      indStr = ("0000").substring(0,4-indStr.length())+indStr;
		      firstFilename = firstFilename+indStr+".fits";
		      File firstFile = new File(firstFilename);
		      if (firstFile.exists()) {
			Object[] exitOptions = {"Overwrite","Abort"};
                	int n = JOptionPane.showOptionDialog(F2DataZoomDisplayFrame.this, "File "+firstFilename+" exists!", "Overwrite file?", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, exitOptions, exitOptions[1]);
                	if (n == 1) {
			   return;
                	}
		      }
		      String lutFile = tLut.getText().trim();
		      if (doRsh && lutFile.startsWith("/nfs")) {
			lutFile = lutFile.substring(lutFile.indexOf("/",6));
		      }
		      if (bigEndian.isSelected()) options+=" -lut "+lutFile;
		      else if (litEndian.isSelected()) options+=" -lutLE "+lutFile;
		      String ufgtakeDir = UFExecCommand.getEnvVar("UFINSTALL");
                      String ldLibPath = UFExecCommand.getEnvVar("LD_LIBRARY_PATH");
                      ldLibPath = ldLibPath.replaceAll("jdk","junk");
                      UFExecCommand startUfgtake = new UFExecCommand("ufgtake",options+" "+tCommand.getText().trim()+" -nosem",ufgtakeDir+"/bin/",doRsh, tRshHost.getText().trim());
		      System.out.println("ufgtake "+options+" "+tCommand.getText().trim()+" -nosem");
                      startUfgtake.setEnv("LD_LIBRARY_PATH",ldLibPath);
		      final JFrame ufgtakeFrame = new JFrame("ufgtake");
		      ufgtakeFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		      Container content = ufgtakeFrame.getContentPane();
		      content.add(startUfgtake);
		      ufgtakeFrame.pack();
		      ufgtakeFrame.setVisible(true);
		      startUfgtake.start();
		   }
		   String dir = tDir.getText().trim();
		   if (! dir.endsWith("/")) dir = dir+"/";
		   String copyDir = tCopyDir.getText().trim();
		   if (!copyDir.endsWith("/")) copyDir = copyDir+"/";
		   fullDisplPanel.copyDir = copyDir; 
		   fullDisplPanel.setUfgtakeDirectory(dir);
		   fullDisplPanel.setUfgtakePrefix(tPrefix.getText().trim());
		   fullDisplPanel.setUfgtakeStartIndex(Integer.parseInt(tIndex.getText().trim()));
		   fullDisplPanel.setEPICSPrefix(tEpics.getText());
                   fullDisplPanel.changeMode(fullDisplPanel.MODE_UFGTAKE);
                   popupFrame.dispose();
                   menuModeNohost.setBackground(defaultBackground);
                   menuModeRegular.setBackground(defaultBackground);
                   menuModeReplicant.setBackground(defaultBackground);
                   menuModeUfgtake.setBackground(Color.GREEN);
		   setupUFGTAKEVisuals(true);
                }
              });
              layout.putConstraint(SpringLayout.WEST, bChange, 10, SpringLayout.WEST, contentPane);
              layout.putConstraint(SpringLayout.NORTH, bChange, 25, SpringLayout.SOUTH, lCommand);
              contentPane.add(bCancel);
              bCancel.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ev) {
                   popupFrame.dispose();
                }
              });
              layout.putConstraint(SpringLayout.WEST, bCancel, 15, SpringLayout.EAST, bChange);
              layout.putConstraint(SpringLayout.NORTH, bCancel, 25, SpringLayout.SOUTH, lCommand);
              layout.putConstraint(SpringLayout.EAST, contentPane, 40, SpringLayout.EAST, bDir);
              layout.putConstraint(SpringLayout.SOUTH, contentPane, 10, SpringLayout.SOUTH, bChange);
              popupFrame.pack();
              popupFrame.setVisible(true);
           }
        });

	JMenu menuStat = new JMenu("Statistics");
	final JMenuItem menuStatChan = new JMenuItem("Show Stats By Channel");
        final JMenuItem menuNostatChan = new JMenuItem("Hide Stats By Channel");
	menuStat.add(menuStatChan);
	menuStatChan.addActionListener(new ActionListener() {
	   public void actionPerformed(ActionEvent ev) {
	      if (fullDisplPanel.imagePanels[0].imageDisplay.imageBuffer == null) {
		System.err.println("Channel Stats Frame Error > No Image in Buffer");
		return;
	      }
	      if (csf == null) {
		csf = new ChannelStatFrame(fullDisplPanel.imagePanels[0].imageDisplay.imageBuffer.pixels, plotPanel);
		fullDisplPanel.setCsf(csf);
	      } else {
		csf.updateStats(fullDisplPanel.imagePanels[0].imageDisplay.imageBuffer.pixels, true);
	      }
              menuStatChan.setBackground(Color.GREEN);
	      menuNostatChan.setBackground(defaultBackground);
	   }
	});
        menuStat.add(menuNostatChan);
	menuNostatChan.setBackground(Color.GREEN);
        menuNostatChan.addActionListener(new ActionListener() {
	   public void actionPerformed(ActionEvent ev) {
	      if (csf != null) {
		csf.dispose();
		fullDisplPanel.removeCsf();
		csf = null;
		menuNostatChan.setBackground(Color.GREEN);
                menuStatChan.setBackground(defaultBackground);
	      }
	   }
	});
	
        JMenu lnfMenu = new JMenu("Look & Feel");
	String[] LnF = {"Motif Look","Metal Look"};

        for( int i=0; i < LnF.length; i++ ) {
            JMenuItem menuItem = new JMenuItem(LnF[i]);
            lnfMenu.add(menuItem);
            menuItem.addActionListener( new ActionListener() {
		    public void actionPerformed(ActionEvent e) { change_look(e); }
		});
        }
        
        menuHelpAbout.addActionListener( new ActionListener() {
		public void actionPerformed(ActionEvent e) { helpAbout_action(e); }
	    });

        menuBar.add(menuFile);
	menuBar.add(menuMode);
	menuBar.add(menuStat);
        menuBar.add(lnfMenu);
        menuBar.add(menuHelp);
        this.setJMenuBar( menuBar );
    }
    
    private void setupUFGTAKEVisuals(boolean ufgtakeMode) {
	ufgtakeStartButton.setVisible(ufgtakeMode);
	ufgtakeStopButton.setVisible(ufgtakeMode);
	zoomDisplPanel.repaint();
    }

}    

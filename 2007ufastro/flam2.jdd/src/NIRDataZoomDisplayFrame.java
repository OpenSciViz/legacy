package ufjdd;
/**
 * Title:        Java Data Display  (JDD) main Frame.
 * Version:      (see rcsID)
 * Copyright:    Copyright (c) 2005
 * Author:       Antonio Marin-Franch, Frank Varosi, Ziad Saleh
 * Company:      University of Florida
 * Description:  For quick-look image display and analysis of Near-IR Camera data stream.
 */
import javaUFLib.*;
import javaUFProtocol.*;
//import TelescopeServer.*;

import java.util.*;
import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;

public class NIRDataZoomDisplayFrame extends DataZoomDisplayFrame {
    
    public static final
	String rcsID = "$Name:  $ $Id: NIRDataZoomDisplayFrame.java,v 1.1 2006/03/24 23:02:46 warner Exp $";

    //TelescopePanel telescopeControl;
    
    public NIRDataZoomDisplayFrame(GraphicsConfiguration gc, String tcshost, String tcsport,  final ZoomPanel zoomPanel, final ColorMapDialog colorMapDialog, DataAccessPanel fullDisplPanel, final NIRplotPanel plotPanel, String args[])
    {
        super(gc, tcshost, tcsport, zoomPanel, colorMapDialog, fullDisplPanel, plotPanel, args);
	setupMenuBar();
		
        this.setTitle("CIRCE Java  Data  Display (version 2005/08/09)");
		
        //telescopeControl = new TelescopePanel(tcshost, tcsport, args);
        JButton telescopeConButton = new JButton("Telescope  Offset  Control");
        //telescopeConButton.setBounds(640, 460, 230, 40);
	telescopeConButton.setBounds(zoomPanel.xSize+53,520,230,40);
	
        telescopeConButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //telescopeControl.setVisible(true);
		//telescopeControl.setState( Frame.NORMAL );
            }
        });

        zoomDisplPanel.add( telescopeConButton );
        
    }
    
//-------------------------------------------------------------------------------

    private void setupMenuBar() {
	JMenuBar menuBar = new JMenuBar();
	JMenu menuFile = new JMenu("File");
        JMenuItem menuFileLoad = new JMenuItem("Load FITS");
        menuFile.add(menuFileLoad);

        menuFileLoad.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent ev) {
              JFileChooser jfc = new JFileChooser(loadDir);
              int returnVal = jfc.showOpenDialog((Component)ev.getSource());
              if (returnVal == JFileChooser.APPROVE_OPTION) {
                String filename = jfc.getSelectedFile().getAbsolutePath();
                loadDir = jfc.getCurrentDirectory();
                UFFITSheader imgFITShead = new UFFITSheader();
                UFProtocol data = imgFITShead.readFITSfile(filename);
                InitDataDisplay.jddFullFrame.setSrcFileName(filename);
                InitDataDisplay.jddFullFrame.setTitleBarText();
                fullDisplPanel.updateFrames(new ImageBuffer(fullDisplPanel.frameConfig, data));
              }
           }
        });

        menuFile.add(new JSeparator());
        JMenuItem menuFilePrefs = new JMenuItem("Preferences");
        menuFile.add(menuFilePrefs);
        menuFilePrefs.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent ev) {
              JFrame prefsFrame = new JFrame("Preferences");
              preferences.setFrame(prefsFrame);
              prefsFrame.getContentPane().add(preferences);
              prefsFrame.pack();
              prefsFrame.setVisible(true);
           }
        });
        menuFile.add(new JSeparator());
	JMenuItem menuFileExit = new JMenuItem("Exit");
        menuFile.add( menuFileExit );
	JMenu menuHelp = new JMenu("Help");
	JMenuItem menuHelpAbout = new JMenuItem("About");
        menuHelp.add( menuHelpAbout );

        menuFileExit.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Object[] exitOptions = {"Exit","Cancel"};
                int n = JOptionPane.showOptionDialog(NIRDataZoomDisplayFrame.this, "Are you sure you want to quit?", "Exit JDD?", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, exitOptions, exitOptions[1]);
                if (n == 0) {
                    System.out.println("JDD exiting...");
                    System.exit(0);
                }
            }
        });

        JMenu menuMode = new JMenu("Mode");
        final JMenuItem menuModeNohost = new JMenuItem("No Host");
	if (fullDisplPanel.mode == DataAccessPanel.MODE_NOHOST) {
	   menuModeNohost.setBackground(Color.GREEN);
	}
        final JMenuItem menuModeRegular = new JMenuItem("Regular");
        final Color defaultBackground = menuModeRegular.getBackground();
        if (fullDisplPanel.mode == DataAccessPanel.MODE_REGULAR) {
           menuModeRegular.setBackground(Color.GREEN);
	}
        menuMode.add(menuModeNohost);
        menuMode.add(menuModeRegular);

        menuModeNohost.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent ev) {
              final JFrame popupFrame = new JFrame("Change Mode?");
              Container contentPane = popupFrame.getContentPane();
              SpringLayout layout = new SpringLayout();
              contentPane.setLayout(layout);
              JLabel lConfirm = new JLabel("Switch mode to: No host?");
              JButton bChange = new JButton("Change Mode");
              JButton bCancel = new JButton("Cancel");
              contentPane.add(lConfirm);
              layout.putConstraint(SpringLayout.WEST, lConfirm, 30, SpringLayout.WEST, contentPane);
              layout.putConstraint(SpringLayout.NORTH, lConfirm, 10, SpringLayout.NORTH, contentPane);
              contentPane.add(bChange);
              bChange.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ev) {
                   fullDisplPanel.changeMode(fullDisplPanel.MODE_NOHOST);
                   popupFrame.dispose();
                   menuModeNohost.setBackground(Color.GREEN);
                   menuModeRegular.setBackground(defaultBackground);
                }
              });
              layout.putConstraint(SpringLayout.WEST, bChange, 10, SpringLayout.WEST, contentPane);
              layout.putConstraint(SpringLayout.NORTH, bChange, 15, SpringLayout.SOUTH, lConfirm);
              contentPane.add(bCancel);
              bCancel.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ev) {
                   popupFrame.dispose();
                }
              });
              layout.putConstraint(SpringLayout.WEST, bCancel, 15, SpringLayout.EAST, bChange);
              layout.putConstraint(SpringLayout.NORTH, bCancel, 15, SpringLayout.SOUTH, lConfirm);
              layout.putConstraint(SpringLayout.EAST, contentPane, 10, SpringLayout.EAST, bCancel);
              layout.putConstraint(SpringLayout.SOUTH, contentPane, 10, SpringLayout.SOUTH, bChange);
              popupFrame.pack();
              popupFrame.setVisible(true);
           }
        });

        menuModeRegular.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent ev) {
              final JFrame popupFrame = new JFrame("Change Mode?");
              Container contentPane = popupFrame.getContentPane();
              SpringLayout layout = new SpringLayout();
              contentPane.setLayout(layout);
              JLabel lConfirm = new JLabel("Switch mode to: Regular?");
              JLabel lHost = new JLabel("Host:");
              final JTextField tHost = new JTextField(15);
              tHost.setText(fullDisplPanel.getHost());
              JLabel lPort = new JLabel("Port:");
              final JTextField tPort = new JTextField(6);
              tPort.setText(""+fullDisplPanel.getPort());
              JButton bChange = new JButton("Change Mode");
              JButton bCancel = new JButton("Cancel");
              contentPane.add(lConfirm);
              layout.putConstraint(SpringLayout.WEST, lConfirm, 30, SpringLayout.WEST, contentPane);
              layout.putConstraint(SpringLayout.NORTH, lConfirm, 10, SpringLayout.NORTH, contentPane);
              contentPane.add(lHost);
              layout.putConstraint(SpringLayout.WEST, lHost, 10, SpringLayout.WEST, contentPane);
              layout.putConstraint(SpringLayout.NORTH, lHost, 15, SpringLayout.SOUTH, lConfirm);
              contentPane.add(tHost);
              layout.putConstraint(SpringLayout.WEST, tHost, 10, SpringLayout.EAST, lHost);
              layout.putConstraint(SpringLayout.NORTH, tHost, 15, SpringLayout.SOUTH, lConfirm);
              contentPane.add(lPort);
              layout.putConstraint(SpringLayout.WEST, lPort, 10, SpringLayout.WEST, contentPane);
              layout.putConstraint(SpringLayout.NORTH, lPort, 15, SpringLayout.SOUTH, lHost);
              contentPane.add(tPort);
              layout.putConstraint(SpringLayout.WEST, tPort, 12, SpringLayout.EAST, lPort);
              layout.putConstraint(SpringLayout.NORTH, tPort, 15, SpringLayout.SOUTH, lHost);
              contentPane.add(bChange);
              bChange.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ev) {
                   String theHost = tHost.getText().trim();
                   int thePort = Integer.parseInt(tPort.getText().trim());
		   //InitDataDisplay.obsMonitor = new UFobsMonitor( theHost, thePort, fullDisplPanel );
                   fullDisplPanel.setHostAndPort(theHost, thePort);
                   fullDisplPanel.changeMode(fullDisplPanel.MODE_REGULAR);
                   popupFrame.dispose();
                   menuModeNohost.setBackground(defaultBackground);
                   menuModeRegular.setBackground(Color.GREEN);
                }
              });
              layout.putConstraint(SpringLayout.WEST, bChange, 10, SpringLayout.WEST, contentPane);
              layout.putConstraint(SpringLayout.NORTH, bChange, 15, SpringLayout.SOUTH, lPort);
              contentPane.add(bCancel);
              bCancel.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ev) {
                   popupFrame.dispose();
                }
              });
              layout.putConstraint(SpringLayout.WEST, bCancel, 15, SpringLayout.EAST, bChange);
              layout.putConstraint(SpringLayout.NORTH, bCancel, 15, SpringLayout.SOUTH, lPort);
              layout.putConstraint(SpringLayout.EAST, contentPane, 10, SpringLayout.EAST, tHost);
              layout.putConstraint(SpringLayout.SOUTH, contentPane, 10, SpringLayout.SOUTH, bChange);
              popupFrame.pack();
              popupFrame.setVisible(true);
           }
        });

        JMenu menuStat = new JMenu("Statistics");
        final JMenuItem menuStatChan = new JMenuItem("Show Stats By Channel");
        final JMenuItem menuNostatChan = new JMenuItem("Hide Stats By Channel");
        menuStat.add(menuStatChan);
        menuStatChan.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent ev) {
              if (fullDisplPanel.imagePanels[0].imageDisplay.imageBuffer == null) {
                System.err.println("Channel Stats Frame Error > No Image in Buffer");
                return;
              }
              if (csf == null) {
                csf = new ChannelStatFrame(fullDisplPanel.imagePanels[0].imageDisplay.imageBuffer.pixels, plotPanel);
                fullDisplPanel.setCsf(csf);
              } else {
                csf.updateStats(fullDisplPanel.imagePanels[0].imageDisplay.imageBuffer.pixels, true);
              }
              menuStatChan.setBackground(Color.GREEN);
              menuNostatChan.setBackground(defaultBackground);
           }
        });
        menuStat.add(menuNostatChan);
        menuNostatChan.setBackground(Color.GREEN);
        menuNostatChan.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent ev) {
              if (csf != null) {
                csf.dispose();
                fullDisplPanel.removeCsf();
                csf = null;
                menuNostatChan.setBackground(Color.GREEN);
                menuStatChan.setBackground(defaultBackground);
              }
           }
        });

        JMenu lnfMenu = new JMenu("Look & Feel");
	String[] LnF = {"Motif Look","Metal Look"};

        for( int i=0; i < LnF.length; i++ ) {
            JMenuItem menuItem = new JMenuItem(LnF[i]);
            lnfMenu.add(menuItem);
            menuItem.addActionListener( new ActionListener() {
		    public void actionPerformed(ActionEvent e) { change_look(e); }
		});
        }
        
        menuHelpAbout.addActionListener( new ActionListener() {
		public void actionPerformed(ActionEvent e) { helpAbout_action(e); }
	    });

        menuBar.add(menuFile);
        menuBar.add(menuMode);
        menuBar.add(menuStat);
        menuBar.add(lnfMenu);
        menuBar.add(menuHelp);
        this.setJMenuBar( menuBar );
    }
}    

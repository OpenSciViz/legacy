package ufjdd;
/**
 * Title:        Java Data Display  (JDD) main Frame.
 * Version:      (see rcsID)
 * Copyright:    Copyright (c) 2005
 * Author:       Antonio Marin-Franch, Frank Varosi, Ziad Saleh
 * Company:      University of Florida
 * Description:  For quick-look image display and analysis of Near-IR Camera data stream.
 */
import javaUFLib.*;
import javaUFProtocol.*;

import java.util.*;
import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;
import javax.swing.*;

public class InitDataDisplay extends JFrame {
    
    public static final
	String rcsID = "$Name:  $ $Id: InitDataDisplay.java,v 1.32 2006/11/20 17:01:37 warner Exp $";

    public static String jddtype;

    GraphicsConfiguration gc;
    GraphicsDevice[] gs;
    
    IndexColorModel colorModel;
    ColorMapDialog colorMapDialog;
    
    ImagePanel[] imagePanels;
    ZoomPanel zoomPanel;
    NIRplotPanel plotPanel;
    DataAccessPanel fullDisplPanel;
    static UFobsMonitor obsMonitor;
    static DataFullDisplayFrame jddFullFrame;
    static DataZoomDisplayFrame jddZoomFrame;

    ButtonGroup countsModeBuGrp = new ButtonGroup();
    
    public static boolean replicant = false;
    public static boolean nohost = true;

    public InitDataDisplay(GraphicsDevice[] gs, String dasHost, int dasPort, String tcsHost, String tcsPort, String args[], String jddtype)
    {
        System.out.println("Initializing JDD components... ");
	this.jddtype = jddtype;
	
	this.gs=gs;
	
        colorMapDialog = new ColorMapDialog(this);
        colorModel = colorMapDialog.colorModel;
	

        //plotPanel = new NIRplotPanel(540, 400);
	plotPanel = new NIRplotPanel(540, 460);
        zoomPanel = new ZoomPanel( colorMapDialog, plotPanel);	

	imagePanels = new ImagePanel[1]; //need this pointer first for dataDisplay
	
	//connect to Frame Data Acq. Server notification monitor and hook to fullDisplPanel panel
	// for automatic update of image panels with frame buffer updates:

	replicant = false;

	if (jddtype.equals("flam2jdd")) {
	    for (int i=0; i<args.length; i++) {
	    	if (args[i].toLowerCase().equals("-epics") && i < args.length-1) {
		    DataAccessPanel.setEPICSPrefix(args[i+1]);
	    	} else
		    if (args[i].toLowerCase().equals("-r") || args[i].toLowerCase().equals("-replicant") || args[i].toLowerCase().equals("-repl")) {
		        replicant = true;
		        nohost = false;
		    } else if (args[i].toLowerCase().equals("-host")&&i<args.length-1){
		    	dasHost = args[i+1];  nohost = false;
		    } else if (args[i].toLowerCase().equals("-port")&&i<args.length-1)
		    	try { dasPort = Integer.parseInt(args[i+1].trim()); nohost = false;}
		        catch (Exception e) { System.err.println("Bad port number");}
	    }

            //mode dependent code:

            if (nohost)
            	fullDisplPanel = new F2DataAccessPanel(imagePanels);
            else if (replicant)
            	fullDisplPanel = new F2DataAccessPanel( dasHost, dasPort, "replication", imagePanels );
            else
            	fullDisplPanel = new F2DataAccessPanel( dasHost, dasPort, "normal", imagePanels );

            fullDisplPanel.setHostAndPort(dasHost, dasPort);

	} else if (jddtype.equals("NIRjdd")) {
	    for (int i=0; i<args.length; i++) {
                if (args[i].toLowerCase().equals("-host")&&i<args.length-1) {
                    dasHost = args[i+1];  nohost = false;
                } else if (args[i].toLowerCase().equals("-port")&&i<args.length-1)
                    try { dasPort = Integer.parseInt(args[i+1].trim()); nohost = false;}
                    catch (Exception e) { System.err.println("Bad port number");}
            }
	
            //mode dependent code:

            if (nohost) {
            	dasHost = "";
            	fullDisplPanel = new NIRDataAccessPanel(dasHost, dasPort, imagePanels);
		fullDisplPanel.mode = DataAccessPanel.MODE_NOHOST;
            }
            else {
            	fullDisplPanel = new NIRDataAccessPanel( dasHost, dasPort, imagePanels);
		fullDisplPanel.mode = DataAccessPanel.MODE_REGULAR;
            }
            obsMonitor = new UFobsMonitor( dasHost, dasPort, fullDisplPanel );
	}
	imagePanels[0] = new ImagePanel( imagePanels, colorModel, fullDisplPanel, zoomPanel );

	
	if (gs.length == 1) {	
	     
	    gc = gs[0].getDefaultConfiguration();
        	     
	    if (jddtype.equals("flam2jdd")) {
		jddZoomFrame = new F2DataZoomDisplayFrame(gc,tcsHost, tcsPort, zoomPanel, colorMapDialog, fullDisplPanel, plotPanel,args);
	    } else if (jddtype.equals("NIRjdd")) {
                jddZoomFrame = new NIRDataZoomDisplayFrame(gc,tcsHost, tcsPort, zoomPanel, colorMapDialog, fullDisplPanel, plotPanel,args);
	    }
	    jddZoomFrame.validate(); 
	    jddZoomFrame.setVisible(true);
	    
	    jddFullFrame = new DataFullDisplayFrame(gc, colorModel, imagePanels, fullDisplPanel,args);
	    jddFullFrame.validate(); 
	    jddFullFrame.setVisible(true);              
	    jddZoomFrame.setPrefs();
	}  
	
	if (gs.length == 2) {
       
	    int j;
	     
     	     j = 0; // left monitor	     
     	       //DataDisplayFrame jddFrame = new DataDisplayFrame(hostname, hostport, tcshost, tcsport, args);
	        gc = gs[j].getDefaultConfiguration();
	        if (jddtype.equals("flam2jdd")) {
		    jddZoomFrame = new F2DataZoomDisplayFrame(gc,tcsHost, tcsPort, zoomPanel, colorMapDialog, fullDisplPanel, plotPanel,args);
                } else if (jddtype.equals("NIRjdd")) {
		    jddZoomFrame = new NIRDataZoomDisplayFrame(gc,tcsHost, tcsPort, zoomPanel, colorMapDialog, fullDisplPanel, plotPanel,args);
                }
                jddZoomFrame.validate(); 
                jddZoomFrame.setVisible(true);

     	     j = 1; // right monitor
	       gc = gs[j].getDefaultConfiguration();
	       jddFullFrame = new DataFullDisplayFrame(gc, colorModel, imagePanels, fullDisplPanel,args);
	       jddFullFrame.validate(); 
               jddFullFrame.setVisible(true);  
               jddZoomFrame.setPrefs();
       }    

	
    }
//-------------------------------------------------------------------------------
    //methods invoked by colorMapCntrl object:
    
    public void changeColorModel(IndexColorModel colorModel) { changeColorModel( colorModel, true ); }

    public void changeColorModel(IndexColorModel colorModel, boolean fullReDraw)
    {
        zoomPanel.changeColorModel( colorModel );
	if( fullReDraw )
	    for( int i=0; i < imagePanels.length; i++ )
		imagePanels[i].imageDisplay.updateImage( colorModel );
    }

//------------------------------------------------------------------------------------------------------------

    class RadioListener implements ActionListener
    {
	ButtonGroup buttonGroup;

	RadioListener( ButtonGroup buttonGroup ) {
	    this.buttonGroup = buttonGroup;
	}

        public void actionPerformed(ActionEvent e) {
           
            String countsMode = getSelection();

	    zoomPanel.zoomImage.useScaleFactor( false );
	    zoomPanel.zoomImage.showPixelValue();
	    zoomPanel.adjustZoom.useScaleFactor( false );
	    zoomPanel.adjustZoom.displayMinMax();
	    for( int i=0; i < imagePanels.length; i++ ) {
	    	imagePanels[i].adjustPanel.useScaleFactor( false );
	    	imagePanels[i].adjustPanel.displayMinMax();
	    }
	}

	String getSelection() {
	    for( Enumeration e = buttonGroup.getElements(); e.hasMoreElements(); )
		{
		    JRadioButton b = (JRadioButton)e.nextElement();
		    if( b.getModel() == buttonGroup.getSelection() ) return b.getText();
		}
	    return null;
	}
    }
}

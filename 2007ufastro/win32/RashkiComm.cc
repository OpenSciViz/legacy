#include <windows>
#include <iostream.h>
#include <conio>

#define BUFLEN 256

HANDLE comport;
HANDLE datamutex;
HANDLE threadly;
HANDLE threadbare;

BOOL keepGoing;
BOOL LOCALECHO;
BOOL BAILONWRITEERROR;
BOOL BAILONREADERROR;
BOOL BAILONOVERFLOW;

int dataReadIndex;
int dataWriteIndex;
BOOL dataOverflow;

char sharedData[BUFLEN];
BOOL overflowFlags[BUFLEN];

void threadedCommReadFunc() {
  char readBuf[BUFLEN];
  DWORD dwBytesRead;  
  BOOL overflow;

  overflow = false;

  while (keepGoing) {
    memset(readBuf,0,BUFLEN);
    if(ReadFile(comport,readBuf,sizeof(readBuf),&dwBytesRead,NULL)){
      WaitForSingleObject(datamutex,INFINITE);

      for (unsigned int i=0; i<dwBytesRead; i++) {
        sharedData[dataWriteIndex] = readBuf[i];

        //if this location's flag hasn't been cleared, then we must be overwriting
        //unread data
        if (overflowFlags[dataWriteIndex]) overflow = true;
        else overflowFlags[dataWriteIndex] = true;

        dataWriteIndex = ++dataWriteIndex % BUFLEN;
      }
      ReleaseMutex(datamutex);
    } else {
      cerr << "Error reading from comm port" << endl;
      if (BAILONREADERROR) {
        cout << "Press any key to exit" << endl;
        ExitThread(-1);
      }
    }  
    if (overflow) {
      cerr << "Detected data overflow" << endl;
      if (BAILONOVERFLOW) {
        cout << "Press any key to exit" << endl;
        ExitThread(-1);
      }
    }
  }
  ExitThread(0);
}

void threadedDataReadFunc() {
  while (keepGoing) {
    WaitForSingleObject(datamutex,INFINITE);
    int bytestoread = dataWriteIndex - dataReadIndex;
    if ( (dataWriteIndex < dataReadIndex) && !dataOverflow) bytestoread += BUFLEN; 
    for (int i=0; i<bytestoread; i++) {
      cout << sharedData[dataReadIndex];
      
      //let other threads know that this data has been read
      overflowFlags[dataReadIndex] = false;

      dataReadIndex = ++dataReadIndex % BUFLEN;
    }
    ReleaseMutex(datamutex);
  }
  ExitThread(0);
}

int checkCmdLineArgs(int argc, char ** argv) {
    for (int i=1; i<argc; i++) {
      if (!strcmpi(argv[i],"-l")) LOCALECHO = true;
      else if (!strcmpi(argv[i],"-nl"))LOCALECHO = false;
 
      else if (!strcmpi(argv[i],"-o")) BAILONOVERFLOW = true;
      else if (!strcmpi(argv[i],"-no"))BAILONOVERFLOW = false;

      else if (!strcmpi(argv[i],"-w")) BAILONWRITEERROR = true;
      else if (!strcmpi(argv[i],"-nw"))BAILONWRITEERROR = false;

      else if (!strcmpi(argv[i],"-r")) BAILONREADERROR = true;
      else if (!strcmpi(argv[i],"-nr"))BAILONREADERROR = false;
 
      else {      
        cerr << "Invalid command line argument: "<< argv[i] << endl;
        return 0;
      }
    }
    return 1;
}

void printUsage() {
  cout << "Syntax: RashkiComm [options ...]" << endl;
  cout << "\t-l\t\tLocal Echo on" << endl;
  cout << "\t-nl\t\tLocal Echo off" << endl;
  cout << "\t-o\t\tExit Program on data overflow" << endl;
  cout << "\t-no\t\tDo not exit on data overflow" << endl;
  cout << "\t-w\t\tExit Program on serial port write error" << endl;
  cout << "\t-nw\t\tDo not exit on serial port write error" << endl;
  cout << "\t-r\t\tExit Program on serial port read error" << endl;
  cout << "\t-nr\t\tDo not exit on serial port read error" << endl;
  cout << endl;
  cout << "Defaults: Local Echo off, Exit Program on any error" << endl;
}

void main(int argc, char** argv) {
    DWORD dwBytesWritten;

    keepGoing = true;
    LOCALECHO = false;
    BAILONWRITEERROR = true;
    BAILONREADERROR = true;
    BAILONOVERFLOW = true;

    dataReadIndex = 0;
    dataWriteIndex = 0;
    dataOverflow = false;

    if (!checkCmdLineArgs(argc,argv)) {
      printUsage();
      return;
    }

    //initialize shared data to 0
    memset(sharedData,0,BUFLEN);

    //initialize overflow flag registers to FALSE
    memset(overflowFlags,0,BUFLEN);

    datamutex = CreateMutex(NULL,false,NULL);
    comport = CreateFile( "COM1:",GENERIC_READ | GENERIC_WRITE,0,NULL, OPEN_EXISTING, 0,NULL);
    
    if (comport == INVALID_HANDLE_VALUE) {
      cerr << "Could not open com1 serial port. Bailing out ..." << endl;
      return;
    }

    DCB dcb = {0};
    dcb.BaudRate = CBR_9600;
    dcb.Parity = EVENPARITY;
    dcb.StopBits=ONESTOPBIT;
    dcb.ByteSize=7;

    if (!SetCommState(comport,&dcb)) {
      cerr << "Error setting comm port parameters. Bailing out..." << endl;
      return;
    }

    // set up non-bocking reads and blocking writes 
    COMMTIMEOUTS cto;
    cto.ReadIntervalTimeout = MAXDWORD;
    cto.ReadTotalTimeoutConstant = 0;
    cto.ReadTotalTimeoutMultiplier = 0;
    cto.WriteTotalTimeoutConstant = 0;
    cto.WriteTotalTimeoutMultiplier = 0;
    if (!SetCommTimeouts(comport,&cto)) {
      cerr << "Error setting comm port timeouts. Could not set up non-blocking reads. "
           << "Continuing, but program might hang..." << endl;
    }

    //start 'serial port read'/'shared data write' thread
    threadly = CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)threadedCommReadFunc,0,0,NULL);              

    //start shared data read thread
    threadbare=CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)threadedDataReadFunc,0,0,NULL);

    cout << "Welcome to RashkiComm.  Type ctrl-c to exit." <<endl;
    cout << "Local Echo is " << (LOCALECHO ? "on" : "off") << endl;

    while (1) {
      cout.flush();

      //WriteFile needs a string, so we'll use a 1 character string
      char ch[1];
      
      ch[0] = getch();
      if (LOCALECHO)
        cout << ch[0];
 
      //exit if user types ctrl-c
      if (ch[0] == 0x03)
        break;
       
      if(!WriteFile(comport,ch,1,&dwBytesWritten,NULL)) {
        cerr << "Error writing "<<ch<<" to comm port."<<endl;
        if (BAILONWRITEERROR) {
          cerr << "Bailing out..." << endl;
          break;
        }
      }

      //check to see if our threads are still running
      DWORD ec;
      if (!GetExitCodeThread(threadly,&ec)) {
        cerr << "Error getting comm thread status.  Bailing out..." << endl;
        break;
      }
      if (ec != STILL_ACTIVE) {
        cerr << "Comm Thread no longer active. Bailing out ..." << endl;
        break;
      }
      if (!GetExitCodeThread(threadbare,&ec)) {
        cerr << "Error getting data thread status.  Bailing out ..."<<endl;
        break;
      }
      if (ec != STILL_ACTIVE) {
        cerr << "Data Thread no longer active. Bailing out ..." << endl;
        break;
      }
    }

    keepGoing = false;
    //wait 300 ms for threads to finish up
    Sleep(300);
    CloseHandle(comport);
    return;
}
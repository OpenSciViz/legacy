#!/usr/local/bin/perl -w

my $rcdId = q($Name:  $ $Id: test.2.pl 14 2008-06-11 01:49:45Z hon $);

use strict;
use Tk;

my $mw = MainWindow->new;

$mw -> title( "Grid Test" );

my $frame = $mw -> Frame( -relief => 'groove',
			  -borderwidth => '2' ) -> pack;

$mw -> Button( -text => "mw DoesNothing" ) -> pack;

$frame -> Button( -text => "Done", -command => sub { exit } )
    -> grid;

#$frame -> Button( -text => "DoesNothing" )
#    -> grid;

MainLoop;

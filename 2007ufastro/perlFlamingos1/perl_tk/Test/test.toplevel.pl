#!/usr/local/bin/perl -w

my $rcdId = q($Name:  $ $Id: test.toplevel.pl 14 2008-06-11 01:49:45Z hon $);

use strict;
use Tk;
use Tk::WaitBox;

my $mw = MainWindow -> new;
$mw -> title( "Main" );
$mw -> withdraw;

$mw -> Button( -text => "Do toplevel",
	       -command => \&do_toplevel
	     ) -> pack;
my $tl;
my $wb;

sleep 5;
$mw -> deiconify();

MainLoop;

sub do_toplevel{
  if( ! Exists($tl) ){
    $tl = $mw -> Toplevel();
    $tl -> title( "Toplevel" );
    $tl -> Button( -text => "Close",
		   -command => sub{ $tl -> withdraw
				  }
		   ) -> pack;

    $wb = $mw -> WaitBox();
    $wb -> Show;
    sleep 5;
    $wb -> unShow;

  }else{
    $tl -> deiconify();
    $tl -> raise();

    $wb -> deiconify();
    $wb -> raise();
  }

}#Endsub do_toplevel

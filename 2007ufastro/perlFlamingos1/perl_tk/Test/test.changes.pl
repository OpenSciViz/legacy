#!/usr/local/bin/perl -w

my $rcdId = q($Name:  $ $Id: test.changes.pl 14 2008-06-11 01:49:45Z hon $);

use strict;
use Tk;
use Tk::WaitBox;

my $mw = MainWindow -> new;
$mw -> title( "Changes Tests" );
#$mw -> withdraw;

my $var = "variable";
my $msg = "";

my $var_l = $mw -> Label( -textvariable => \$var,
			) -> pack;

my $fixed_b = $mw -> Button(
			    -command => \&Change1,
			    -text => "Do Change1",
			   ) -> pack;

my $var2_l = $mw -> Label( -text => "Var2",
			) -> pack;

my $fixed2_b = $mw -> Button(
			    -command => \&Change2,
			    -text => "Do Change2",
			   ) -> pack;


my $msg_l = $mw -> Label( -textvariable => \$msg,
			) -> pack;

my $fixed3_b = $mw -> Button(
			     -command => \&Change3,
			     -text    => "Do Change3",
			     ) -> pack;


my $wb = $mw -> WaitBox();
$wb -> unShow;

#$mw -> deiconify();
MainLoop;


sub Change1{
  my $new_text = "changed";

  $var = $new_text;

  #$var_l -> configure( -textvariable => \$var );

  for( my $i = 0; $i < 5; $i++ ){
    #print "should be waiting...\n";
    sleep 1;
  }

  $var = "original variable";
  #$var_l -> configure( -textvariable => \$var );

}#Endsub Change1


sub Change2{
  my $new_text = "Executing Change2";

  $var2_l -> configure( -text=> \$new_text );

  for( my $i = 0; $i < 5; $i++ ){
    print "should be waiting...\n";
    sleep 1;
  }

  $var2_l -> configure( -text => "Change2 Done" );

}#Endsub Change2


sub Change3{
  print "Executing Change3\n";

  $wb -> Show;
  for( my $i = 0; $i < 5; $i++ ){
    print "i = $i\n";
    sleep 1;
    $msg = "Have slept ".($i+1)." seconds";
    $msg_l -> update();
    print "msg = $msg\n";
  }
  $msg = "Done";
  $msg_l -> update();
  $wb -> unShow;

}#Endsub Change3

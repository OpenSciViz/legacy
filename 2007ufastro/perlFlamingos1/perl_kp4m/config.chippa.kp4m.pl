#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


###############################################################################
#
#Configure fits header parameters for next mos dither sequence
#
##############################################################################
use strict;
use Getopt::Long;
use GetYN;
use Dither_patterns qw/:all/;

my $DEBUG = 0;#0 = not debugging; 1 = debugging
GetOptions( 'debug' => \$DEBUG );

my $header = select_header($DEBUG);

use Fitsheader qw/:all/;
Fitsheader::Find_Fitsheader($header);

#readFitsHeader is exported from Fitsheader.pm
#and loads the following arrays from the input header
#@fh_params @fh_pvalues @fh_comments
Fitsheader::readFitsHeader($header);

print_msg();

print "\n\n".
      ">>>---------------------The present chip pa on the sky is".
      "---------------------<<<\n\n";

Fitsheader::printFitsHeader( "CHIP-PA");
print "\n\n";

print "Change value of CHIP-PA ";
my $reply = GetYN::get_yn();
if( !$reply ){ die "Exiting\n\n" }

my $didmod = 0;
my $redo = 0;
until( $redo ){
  my $chip_pa_on_sky = chg_chip_pa( "CHIP-PA" );
  if( $chip_pa_on_sky != 720 ){
    Fitsheader::setNumericParam( "CHIP-PA", $chip_pa_on_sky  );
    $didmod += 1;
  }


  if( $didmod > 0 ){
    print "\n\nThe new value is: \n";
    Fitsheader::printFitsHeader( "CHIP-PA");
    print "_____________________________________________________________".
      "__________________\n";
    print "\nAccept change?";
    $redo = get_yn();
  }else{
    $redo = 1;
  }
}

if( $didmod > 0 ){
  if( !$DEBUG ){
    Fitsheader::writeFitsHeader( $header );

  }elsif( $DEBUG ){
    #print "\nUpdating FITS header in: $header ...\n";
    Fitsheader::writeFitsHeader( $header );
  }
}elsif( $didmod == 0 ){
  print "\nNo changes made to default header.\n\n";
}

#Final print
print "\n";
###End of main

###subs
sub print_msg{
  print "\n\n";
  print "------------------------------------------------------------------------------\n";
  print "This will compute the Chip PA on the sky for ROT_PA = 0, ";
  print "and update the CHIP-PA header field.\n";
  print "It assumes that you have measured the XY cordinates of a star imaged before and\n";
  print "after applying a DEC offset.\n\n";
  print "1: It will first ask for the Rotator PA when the two images were taken.\n";
  print "   (Probably best to do this with at Rotator PA close to 0)\n";
  print "2: Then it will ask for the star's xy coordinates in the two images.\n\n";
}#Endsub print_msg


sub chg_chip_pa{
  my $pa_chip_on_sky = 720;
  my $is_valid = 0;

  my $iparm = param_index( 'CHIP-PA' );
  print  "\n\n".$fh_comments[$iparm] . "\n";
  print $fh_params[$iparm ] . ' = ' . $fh_pvalues[$iparm]."\n\n";

  my $rotpa = input_rotpa();
  my $star_angle_on_chip = compute_star_angle_on_chip();

  $pa_chip_on_sky = $rotpa - $star_angle_on_chip;
  print "rotpa              = $rotpa\n";
  print "star angle on chip = $star_angle_on_chip\n";
  print "pa of chip on sky  = $pa_chip_on_sky\n";

  return $pa_chip_on_sky;
}#Endsub chg_chip_pa


sub input_rotpa{
  my $response = 0;
  my $is_valid = 0;

  print "Enter the ROTATOR's PA used for the measurement (default = 0 degrees)\n";
  print "(Valid values are in the range >= -0.6 and <= +180 degrees):  ";

  until($is_valid){
    chomp ($response = <STDIN>);

    my $input_len = length $response;
    if( $input_len > 0 ){

      if( $response =~ m/[0-9]{0,}[a-z,A-Z][0-9]{0,}/ ){
	#print "matched (#)alpha(#)\n";
	print "Please enter a real value:  ";
      }elsif( $response =~ m/^\.$/ ){
	print "$response is not valid\n";
	print "Please enter a real value:  ";
      }else{
	if( $response =~ m/\d{0}\.\d{1}/ || $response =~ m/\d{1}/ ){
	  #look for matches with (.#, .##, #.#, #.##) or (#., #.#, #.##) or (0) or (1)
	  if( $response >= -0.6 and $response <= 180 ){
	    print "Valid response was $response\n";
	    $is_valid = 1;
	  }else{ print "Please enter a value between -0.6 and +180 degrees\n";}
	}
      }

    }else{
      print "Using Rotator PA of 0 degrees\n";
      $response = 0;
      $is_valid = 1;
    }
  }
  print"\n";

  return $response;
}#Endsub input_rotpa


sub compute_star_angle_on_chip{
  print "Enter the XY coordinates of the star imaged at the\n".
        "top of the array (larger Y values)\n";
  print "X-coordinate:  ";
  my $xtop = input_pos_real();

  print "Y-coordinate:  ";
  my $ytop = input_pos_real();

  print "\n";
  print "Enter the XY coordinates of the star imaged at the\n".
         "bottom of the array (smaller Y values)\n";
  print "X-coordinate:  ";
  my $xbot = input_pos_real();

  print "Y-coordinate:  ";
  my $ybot = input_pos_real();

  if( $ybot == $ytop ){ 
    die "\n\nExiting\n".
        "Please use images of the same star imaged at two different Y values.\n\n";
  }

  print "\n";
  print "xy top = $xtop, $ytop; xy bot = $xbot, $ybot\n";

  my $dx = $xtop - $xbot; my $dy = $ytop - $ybot;
  use Math::Trig;
  my $star_angle_on_chip_rad = atan( $dx / $dy );
  my $star_angle_on_chip_deg = rad2deg( $star_angle_on_chip_rad );
  $star_angle_on_chip_deg = sprintf "%.2f", $star_angle_on_chip_deg;

  return $star_angle_on_chip_deg
}#Endsub compute_star_angle_on_chip


sub input_pos_real{
  my $response = 0; my $is_valid = 0;
  until( $is_valid ){
    chomp( $response = <> );

    if( $response =~ m/[a-zA-Z]/ ){
      print "Enter a positive real number:  ";
    }elsif( $response =~ m/^\.$/ ){
      print "Enter a positive real number:  ";
    }else{
	if( $response =~ m/\d{0}\.\d{1}/ || $response =~ m/\d{1}/ ){
	  #look for matches with (.#, .##, #.#, #.##) or (#., #.#, #.##) or (0) or (1)
	  if( $response > 0 ){
	    #print "Valid response was $response\n";
	    $is_valid = 1;
	  }else{ print "Please enter a value > 0\n";}
	}
      }
  }
  return $response;
}#Endsub input_pos_real

__END__

=head1 NAME

config.chippa.kp4m.pl

=head1 Description

Use to setup the instrument alignment angle.
Important if you need to do very precise and accurate
large motions down the length of the slit.


=head1 REVISION & LOCKER

$Name:  $

$Id: config.chippa.kp4m.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut

#!/usr/local/bin/perl -w

my $rcsId = q($Name:  $ $Id: date.time.pl 14 2008-06-11 01:49:45Z hon $);

use strict;

my $tcstel = $ENV{UF_KPNO_TCS_PROG};
my $quote  = "\"";
my $space  = " ";
my $tele_date_time_call = $tcstel.$space.$quote."hdwe utClock".$quote;

my $tele_date_time = `$tele_date_time_call`;

my ($word, $value)  = split( /= /, $tele_date_time);
my ($time, $date) = split( /, /, $value);

my $slash = index $date, "/";
my $month = substr $date,0, 2;

my $day   = substr $date, $slash+1;
   $slash = index $day, "/";
   $day   = substr $day, 0, 2;

my $last_slash = rindex $date, "/";
my $year       = 2000 + substr $date, $last_slash+1, 2;

my $utc_date = $year."-".$month."-".$day;

print "utc-time = $time\n";
print "utc-date = $utc_date\n";

#!/usr/local/bin/perl -w

my $rcsId = q($Name:  $ $Id: dither.4.4m.pl 14 2008-06-11 01:49:45Z hon $);

use strict;

my $msg0 = "\n";
my $msg1 = "\n\n\t2x2 grid of absolute offsets from the pointing center\n";
my $msg2 = "\tFurthest corner is at +/-45\"\n";
my $msg3 = "\tApply scale factor for different offset\n\n";

die $msg0.'usage: dither.4.4m.pl 
       [-file name]
       [-expt exposure time] 
       [-index starting image number]
       [-repeats # of images per position]
       [-scale optional offset scale factor]
       [-reads # of reads]'.$msg1.$msg2.$msg3 unless @ARGV > 0;

my $hostnm = `hostname`;
$hostnm =~ s/\n//g;
my $ctlhost = "flamingos1a"; # default mechanism & detector controller
my $frmhost = "flamingos1b"; # default frame grabber;
my $reply;


my $filenm = argval('-file');
if( $filenm eq "false" || $filenm eq "true" ) { # use default file name
  $filenm = "/data1/data";
}
die "please provide full path for image file!" unless index($filenm, '/') == 0;

my $expt = argval('-expt');
if( $expt eq "false" || $expt eq "true" ) { # use default exposure time
  $expt = 2;
}

my $index = argval('-index');
if( $index eq "false" || $index eq "true" ) { # use default
  $index = 1;
}

my $repeatcnt = argval('-repeats');
if( $repeatcnt eq "false" || $repeatcnt eq "true"  ) { # use default frame cnt
  $repeatcnt = 1;
}

my $scale = argval('-scale');
if( $scale eq "false" || $scale eq "true"  ) { # use default frame cnt
  $scale = 1;
}

my $nreads = argval('-reads');
if( $nreads eq "false" || $nreads eq "true" ) { # use default read cnt
  $nreads = 1;
}

my $frmcnt = argval('-frames');#option not offered to user
if( $frmcnt eq "false" || $frmcnt eq "true" ) { # use default read cnt
  $frmcnt = 1;
}

if( !$filenm && !$expt && !$index && !$repeatcnt && !$scale && !$nreads && !$frmcnt ) {
  &usage;
  exit 1;
}

die "reads == 2 not supported\n" unless $nreads != 2; 
die "exposure time ( $expt ) too short for reads == $nreads\n" unless $expt >= (2 * $nreads);

#******End of arg checking******#

#***Scale the pointing offsets***#
my $i;
my @scaled_pcen=();
@scaled_pcen=ScalePcen($scale);
my $gridsize = @scaled_pcen - 1;

   #***Diagnostic printout***#
   #for($i = 0; $i < $gridsize; $i += 1){
   #  print "$i, $scaled_pcen[$i]\n";
   #}

my $startoffsetelement= 0;
my $currentval = 0;

my $getoffset = "tcs4m \"tele offset\"";
my $setoffset = "tcs4m \"tele offset = ";

my $setposition;
print "$gridsize, $repeatcnt, " . $gridsize * $repeatcnt . "\n";
my $gridposition = 1;

for($currentval = $index; $currentval <= ($gridsize * $repeatcnt + $index -  1); $currentval += 1){

  print "index and cval = $index, $currentval\n";
  print "\nOffsetting telescope to position $gridposition of $gridsize = $scaled_pcen[$gridposition]\n";
  $setposition = "$setoffset $scaled_pcen[$gridposition]\"";
  
  print "$setposition\n\n";
  
  OffsetAndTake($setposition, $frmcnt, $expt, $nreads, $filenm, $currentval, $repeatcnt);
  if( $repeatcnt > 1 ) {
    $currentval += ($repeatcnt - 1);
  }
  $gridposition += 1;
}

FinishUp();

exit;

######################### subs ################################
sub ScalePcen{
  my $scale = $_[0];
  my $factor4m = 0.5; #Scale factor to apply to table used at KPNO 2.1m for the 4#
  my $factor2m = 1.0; #Scale factor to apply to table used at KPNO 2.1m for the 2m#
  my $factor = $factor4m;

  my @pcen2x2 = ( 0,0, 90,-90, -90, 90, -90,-90,  90, 90);
  my $double_gridsize=@pcen2x2;

  #***Scale the data for the 4m***#
  my $i;
  my $tmp;
  my @scaled_pcen=();
  for($i = 0; $i < $double_gridsize; $i += 1){
    $tmp = $factor * $scale * $pcen2x2[$i];
    @scaled_pcen = (@scaled_pcen, $tmp);
    #print "$i, $pcen2x2[$i], $scaled_pcen[$i]\n";
  }

  #***Now form strings of the x-y pairs***#
  my @offset_string = ();
  for($i = 0; $i < $double_gridsize; $i += 2){
    @offset_string =  (@offset_string,"$scaled_pcen[$i], $scaled_pcen[$i+1]");
  }

  #***Diagnostic print out***#
  my $gridsize = @offset_string;
  print "gridsize = $gridsize\n";
  for($i=0; $i < $gridsize; $i += 1){
    print "$offset_string[$i]\n";
  }

  return(@offset_string);

}#End sub ScalePcen


sub OffsetAndTake{
  my $setposition = $_[0];
  my $frmcnt = $_[1];
  my $expt = $_[2];
  my $nreads = $_[3];
  my $filenm = $_[4];
  my $currentoffset = $_[5];
  my $repeats = $_[6];

  #print "At  # " . $currentoffset . "\n";

  $reply = `$setposition`;  
  print "$reply";

  my $waitcnt = 2;
  while( $waitcnt-- >= 0 ) { print " ..telescope moving.. \n"; sleep 1; }

  my $i;
  my $oi;
  if( $repeats == 1){
      print "Starting take/frame-grab for image number $currentoffset...\n";
      $oi = "oneimage.pl -frames $frmcnt -expt $expt -reads $nreads -file $filenm -index $currentoffset";
      print "Executing $oi\n";
      system($oi);
  }elsif( $repeats > 1 ){
    for( $i = 0; $i <= ($repeats - 1) ; $i++ ){
      #print("# " . ($currentoffset + $i) . "\n");
      $currentoffset = $currentoffset +  $i;
      $oi = "oneimage.pl -frames $frmcnt -expt $expt -index $currentoffset -reads $nreads -file $filenm";
      print "Executing $oi\n";
      system($oi);
    }
  }

}#End of OffsetAndTake

sub FinishUp {

  my $recenter;
  $recenter = "tcs4m \"tele offset = 0, 0\"";

  my $waitcnt = 3;
  while( $waitcnt-- >= 0 ) { print " ..telescope moving back to offset 0,0.. \n"; sleep 1; }

  $reply = `$recenter`;  

  for($i=0;$i<4;$i+=1){
    my $beeps = "\a";
    my $msg = "Dither sequence is done!\n";
    print "$msg $beeps";
    sleep 1;
  }

}#End of FinishUp

sub argval {
  my ( $arg ) = @_;
  my $argc = @ARGV;
  my $a;
  my $b;
  my $idx = index($arg, '-');
  die "please use dashes for all args!\n" unless $idx == 0;
  my $i;
  for( $i = 0; $i < $argc; $i++ ) {
    $a = $ARGV[$i];
    if( $a eq $arg ) {
      if( $i == $argc - 1 ) {
	return "true"; # last one
      }
      $b = $ARGV[1 + $i]; # check next for dash
      $idx = index($b, '-');
      if( $idx == 0 ) {
	return "true"; # no value supplied, assume boolean
      }
      else {
	return $b; # return value;
      }
    } # found $arg
  } # search for loop
  return "false"; # never found it
}

#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;

my $tcs = $ENV{UF_KPNO_TCS_PROG};
my $tcs_offset_head = $ENV{TCS_OFFSET_HEAD};
my $tcs_offset_tail = $ENV{TCS_OFFSET_TAIL};
my $zero = "zero";
my $space = " ";

my $cmd = $tcs.$space.$tcs_offset_head.$zero.$tcs_offset_tail;

use GetYN;
print "This will define the present location as your new pointing center\n";
print "Are you sure?  ";
my $reply = get_yn();
if( $reply ){
  print "Executing $cmd\n";
  system( $cmd );
}elsif( $reply ){
  die "\nExiting\n\n";
}

__END__

=head1 NAME

clear.offsets.kpno.pl

=head1 Description

Moves the telescope back to where the offsets are (0, 0)


=head1 REVISION & LOCKER

$Name:  $

$Id: clear.offsets.kpno.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut

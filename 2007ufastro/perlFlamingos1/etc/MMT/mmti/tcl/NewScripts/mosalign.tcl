
#----------------------------------------------
# Parameter routines
#my $rcsId = q($Name:  $ $Id: mosalign.tcl,v 0.1 2003/05/22 14:57:11 raines Exp $);

proc ParamShow { paramname } {
    global $paramname
    set w .$paramname
    upvar #0 $paramname P
    if [winfo exists $w] {destroy $w}
    toplevel $w 

    set menubar [menu $w.menubar]
    $w config -menu $menubar

    menu $menubar.configs
    $menubar add cascade -menu $menubar.configs -label Configs
    foreach pset $P(_list) {
        $menubar.configs add command -label $pset -command "ParamSet $paramname $pset"
    }

    foreach {name value} [array get P] {
        if { [ regexp ^_ $name ] == 0 } {
            grid \
                [ label $w.${name}_l -text $name] \
                [ entry $w.${name}_e -textvariable ${paramname}($name) ]
        }
    }
}

proc ParamList { pset name values } {
    upvar #0 $pset P
    if {![info exists P(_list)]} { set P(_list) {} }
    if { [lsearch -exact $P(_list) $name] == -1 } {
        lappend P(_list) $name
    }
    global ${pset}_$name
    array set ${pset}_$name $values
}

proc ParamSet { pset name } {
    global $pset ${pset}_$name
    array set $pset [array get ${pset}_$name]
}

proc ParamAttr { pset attr } {
}

proc stripargs {arglist argkey} {

    if {[lsearch -exact $arglist "-help"] != -1} {
        foreach k $argkey {
            puts stderr $k
        }
        exit
    }
    
    foreach key $argkey {
        set flag [lindex $key 0]
        set index [lsearch -exact $arglist $flag]
        set index1 [expr $index + 1]
        if {$index >= 0} {
            set varname [lindex $key 1]
            upvar $varname var
            set var [lindex $arglist $index1]
            set arglist [lreplace $arglist $index $index1]
        }
    }
    return $arglist
}

ParamList caminfo flamingos_mmt {
    parity 1
    scale  0.18
    angle  0
}

ParamList caminfo minicam_f9 {
    parity 1
    scale  0.0455
    angle  -90
}

ParamSet caminfo flamingos_mmt

proc PixToArcsec { instname x y { angle 0 } } {
    upvar #0 $instname caminfo
    set angle [expr ($angle + $caminfo(angle)) / 180. * 3.14159]
    set x [expr $x * $caminfo(parity)]
    set xx [expr ($x *  cos($angle) + $y * sin($angle)) * $caminfo(scale)]
    set yy [expr ($x * -sin($angle) + $y * cos($angle)) * $caminfo(scale)]
    return "$xx $yy"
}

proc ArcsecToPix { instname x y { angle 0 } } {
    upvar #0 $instname caminfo
    set angle [expr -($angle + $caminfo(angle)) / 180. * 3.1415926535]
    set xx [expr ($x *  cos($angle) + $y * sin($angle)) / $caminfo(scale)]
    set yy [expr ($x * -sin($angle) + $y * cos($angle)) / $caminfo(scale)]
    set xx [expr $xx * $caminfo(parity)]
    return "$xx $yy"
}


    

    


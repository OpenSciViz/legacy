#include <stdio.h>
#include <stddef.h>
#include <math.h>

#define DEGSPERRAD (180./M_PI)

#define MAXBOXES 100
#define MAXLINE  256

/*
#my $rcsId = q($Name:  $ $Id: rotsolve.c,v 0.1 2003/05/22 14:57:11 raines Exp $);
*/

typedef struct {
    double x,y;
} Point;

typedef struct {
    int   nstar;
    Point rotcen;
    int   rotparity;
    Point moscen;
    int   mosparity;
    Point box[MAXBOXES];
    Point star[MAXBOXES];
} Data;

char *getline (char *line, int len, FILE *f)
{
    do {
	fgets(line, len, f);
    } while (line[0] == '#');
    return line;
}

void ReadConfig (FILE *f, Data *d)
{
    char line[MAXLINE];
    getline(line, MAXLINE, f);
    sscanf(line, "%lf %lf", &d->rotcen.x, &d->rotcen.y);
    getline(line, MAXLINE, f);
    sscanf(line, "%lf %lf", &d->moscen.x, &d->moscen.y);
    getline(line, MAXLINE, f);
}    


void ReadData (FILE *f, Data *d)
{
    int i;
    char line[MAXLINE];
    getline (line, MAXLINE, f);
    sscanf(line, "%d", &d->nstar);

    for (i=0; i < d->nstar; i++) {
	getline(line, MAXLINE, f);
	sscanf(line, "%lf %lf %lf %lf", &d->box[i].x, &d->box[i].y, 
	       &d->star[i].x, &d->star[i].y); 
    }
}

double MeritRot (Data *d, double theta, double *dx, double *dy)
{

    double x[MAXBOXES];
    double y[MAXBOXES];
    double xdiff=0.0, ydiff=0.0;
    double merit=0.0;
    int i;

    theta /= DEGSPERRAD;
    for (i=0; i< d->nstar; i++) {
	x[i] = (d->star[i].x - d->rotcen.x) * cos(theta) 
	  + (d->star[i].y - d->rotcen.y) * sin(theta) + d->rotcen.x;

	y[i] = -(d->star[i].x - d->rotcen.x) * sin(theta) 
	  + (d->star[i].y - d->rotcen.y) * cos(theta) + d->rotcen.y;

	xdiff += x[i] - d->box[i].x;
	ydiff += y[i] - d->box[i].y;
    }

    xdiff /= d->nstar;
    ydiff /= d->nstar;

    for (i=0; i< d->nstar; i++) {
	x[i] -= xdiff;
	y[i] -= ydiff;
	merit += (x[i] - d->box[i].x) * (x[i] - d->box[i].x) 
	  + (y[i] - d->box[i].y) * (y[i] - d->box[i].y);
    }
    *dx = xdiff;
    *dy = ydiff;

    return merit;

}
    
double MeritMos (Data *d, double theta, double *dx, double *dy)
{

    double x[MAXBOXES];
    double y[MAXBOXES];
    double xdiff=0.0, ydiff=0.0;
    double merit=0.0;
    int i;

    theta /= DEGSPERRAD;
    for (i=0; i< d->nstar; i++) {
	x[i] = (d->box[i].x - d->moscen.x) * cos(theta) 
	  + (d->box[i].y - d->moscen.y) * sin(theta) + d->moscen.x;

	y[i] = -(d->box[i].x - d->moscen.x) * sin(theta) 
	  + (d->box[i].y - d->moscen.y) * cos(theta) + d->moscen.y;

	xdiff += d->star[i].x - x[i];
	ydiff += d->star[i].y - y[i];
    }

    xdiff /= d->nstar;
    ydiff /= d->nstar;

    for (i=0; i< d->nstar; i++) {
	x[i] += xdiff;
	y[i] += ydiff;
	merit += (d->star[i].x - x[i])*(d->star[i].x - x[i]) 
	  + (d->star[i].y - y[i])*(d->star[i].y - y[i]);

    }
    *dx = xdiff;
    *dy = ydiff;

    return merit;

}
    
main (int argc, char *argv[])
{
    Data d;
    double dx, dy;
    double minmerit, minangle;
    double merit;
    double angle;
    double resolution=0.001;
    FILE *fconfig;

    if (argc != 2) {
	fprintf(stderr, "Usage:  rotsolve configfile < datafile\n");
	exit(1);
    }
    
    if ((fconfig = fopen(argv[1], "r")) == NULL) {
	fprintf(stderr, "Error opening config file %s\n", argv[1]);
	exit (1);
    }

    ReadConfig(fconfig, &d);
    ReadData(stdin, &d);

    minmerit = 1e37;
    
    for (angle = 0; angle < 360; angle+=1) {
	if ((merit = MeritRot(&d,angle, &dx, &dy)) < minmerit) {
	    minmerit = merit;
	    minangle = angle;
	}
    }

    for (angle = minangle - 1.0; angle < minangle + 1.0; angle+=resolution) {
	if ((merit = MeritRot(&d,angle, &dx, &dy)) < minmerit) {
	    minmerit = merit;
	    minangle = angle;
	}
    }

    while (minangle >  180.) minangle-=360.;
    while (minangle < -180.) minangle+=360.;

    printf("Angle: %g\n", minangle);

    MeritRot(&d,minangle, &dx, &dy);
    printf("Telrot_offset: %g %g\n", dx, dy);

    MeritMos(&d, -minangle, &dx, &dy);
    printf("Moswheel_offset: %g %g\n",dx, dy);

}

    

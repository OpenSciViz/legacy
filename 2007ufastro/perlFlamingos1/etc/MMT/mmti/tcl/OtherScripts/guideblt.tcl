
blt::vector xdata($graphlen)

for { set i 0 } { $i < $graphlen } { incr i } {
    set xdata($i) $i
}

proc settotal { err value index op } {
		upvar #0 ${err}errol e
		upvar #0 ${err}total t
		upvar #0      $value v

		global	graphlen

	set e [format  "% 7.3f" $v]
	set t [format  "% 7.3f" [expr $t + $v]]

	global ${err}totv

	if { [${err}totv length] >= $graphlen } {
		${err}totv delete 0
	}
	${err}totv append  $t

	if { [${err}errv length] >= $graphlen } {
		${err}errv delete 0
	}
	${err}errv append  $e
}

proc mkgraphs { w typ } {
    grid \
	[mkgraph $w az $typ RA]	\
	[mkgraph $w el $typ Dec]
    grid \
	[mkgraph $w rt $typ Rot]	\
	[mkgraph $w fc $typ Focus]
    #grid \
	#[mkgraph $w ax $typ "Astig X"]	\
	#[mkgraph $w ay $typ "Astig Y"]
    grid \
	[mkgraph $w cx $typ "Coma X"]	\
	[mkgraph $w cy $typ "Coma Y"]
}

proc mkgraph { w err typ title} {
	set graph [blt::graph $w.${err}graph		\
			-height 2.25i -width 3i\
			-borderwidth  0		\
			-topmargin    20	\
			-bottommargin 15	\
			-rightmargin  5		\
			-leftmargin   40	\
			-plotborderwidth 0	\
			-title $title]

	$graph xaxis configure -title "" -color blue -ticklength 3
	$graph yaxis configure -title "" -color blue -ticklength .05i

	#$graph legend configure -mapped no

	global ${err}${typ}v xdata
	blt::vector ${err}${typ}v 
	$graph element create ${err}${typ} -xdata xdata -ydata ${err}${typ}v

	#Blt_ZoomStack $graph

	return $w.${err}graph
}
mkgraphs [Toplevel .rgraphs "Raw Error Graphs"  +[expr $X + $W + $BD]+$RY] rawerr
mkgraphs [Toplevel .xgraphs "Raw Total Graphs"  +[expr $X + $W + $BD]+$RY] rawtot
mkgraphs [Toplevel .egraphs "Current Error Graphs" +[expr $X + $W + $BD]+$RY] err
mkgraphs [Toplevel .tgraphs "Total Error Graphs"  +[expr $X + $W + $BD]+$RY] tot

#my $rcsId = q($Name:  $ $Id: guideblt.tcl,v 0.1 2003/05/22 14:57:47 raines Exp $);

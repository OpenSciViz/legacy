#!/bin/sh
#\
#my $rcsId = q($Name:  $ $Id: guidedither.tcl,v 0.1 2003/05/22 14:57:47 raines Exp $);

exec tclsh "$0" "$@"

source $env(MMTITCL)/msg.tcl

# Topbox
set scale -0.392

# Minicam
#set scale -0.0455

if {$argc != 2} {
    puts stderr "Usage: guidedither azamt elamt"
}

set azamt [lindex $argv 0]
set elamt [lindex $argv 1]

msg_client GUIDSERV
msg_client TELESCOPE

msg_variable GUIDSERV image1x image1x rw
msg_variable GUIDSERV image2x image2x rw
msg_variable GUIDSERV image1y image1y rw
msg_variable GUIDSERV image2y image2y rw

proc guideoffset { azamt elamt } {
    global image1y image2y image1x image2x scale
    msg_cmd GUIDSERV stop

# Make sure we round the same way for + and -
    if  { $azamt < 0 } { set sign 1 } else { set sign -1 }
    set image1x [expr $image1x + $sign * int(abs($azamt) / $scale)]
    set image2x [expr $image2x + $sign * int(abs($azamt) / $scale)]

    if  { $elamt < 0 } { set sign -1 } else { set sign 1 }
    set image1y [expr $image1y + $sign * int(abs($elamt) / $scale)]
    set image2y [expr $image2y + $sign * int(abs($elamt) / $scale)]

    msg_cmd TELESCOPE "instrel $azamt $elamt"
    after 1000
    msg_cmd GUIDSERV start
}

guideoffset $azamt $elamt





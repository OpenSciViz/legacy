# /data/mmti/src/mmtitcl/arguments.tcl
#

proc arguments { args } {
	global argv argc

	for { set i 0 } { $i < $argc } { incr i } {
	 set arg [lindex $argv $i]

	 switch -regexp $arg {
	  -* {
	    set name [string range $i 2 end]
	    if { [lsearch $args $name] != -1 } {
		:w

	    }
	 }
	}
}

#my $rcsId = q($Name:  $ $Id: arguments.tcl,v 0.1 2003/05/22 14:57:47 raines Exp $);
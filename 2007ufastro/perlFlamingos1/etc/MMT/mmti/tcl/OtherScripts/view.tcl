# The view is a list of lists which maps table columns to data array, data
# column name pairs.
#
#	Where Catalog and Fibers are global arrays containing table
#	drivers a view might look like this:
#
#	set view { { Catalog RA }
#		   { Catalog Dec }
#		   { Catalog Mag }
#		   { Fibers  X }
#		   { Fibers  Y }
#		}
#
#my $rcsId = q($Name:  $ $Id: view.tcl,v 0.1 2003/05/22 14:57:47 raines Exp $);

proc tab_viewcreate { V viewlist nrows } {
	upvar #0 $V view

    set view(rows) $nrows
    set view(view) $viewlist
    set i 1
    foreach c $viewlist {
	set col [string trim [lindex $c 1]]


	if { [llength $c] >= 3 } {
	    set name [string trim [lindex $c 2]]
	} else {
	    set name $col
	}

	set view($col)  $i
	set view($name) $i

	set view(0,$i) $name	
	lappend view(Header) $name

	incr i
    }
}

proc tab_viewlist { t } {
	upvar $t T

    set viewlist {}
    foreach c [starbase_columns T] {
	lappend viewlist "$t [string trim $c]"
    }

    return $viewlist
}

proc tab_viewtables  { V tables } { upvar $V view;  set view(tables) $tables 	}
proc tab_viewcolumns { V } 	  { upvar $V view;  return $view(Header) }

proc tabview_mapdat  { view col } {		# View column to data array
	lindex [lindex $view [expr $col-1]] 0
}
proc tabview_mapcol  { view col } {		# View column to data column
	set T [tabview_mapdat $view $col]
	upvar #0 $T D

	$D(colnum) D [lindex [lindex $view [expr $col-1]] 1]
}


# Table driver wrappers
#
proc tab_viewnrows  { V }		{ upvar $V view; return $view(rows)		}
proc tab_viewncols  { V }		{ upvar $V view; llength $view(view)		}
proc tab_viewset    { V row col value }	{
	upvar $V view;
	tab_dataset [tabview_mapdat $view(view) $col] $row [tabview_mapcol $view(view) $col] $value
}
proc tab_viewget    { V row col } 	{
	upvar $V view;

	set data    [tabview_mapdat $view(view) $col]
	upvar #0 $data D

	if { ![string compare [string index $data 0] "!"] } {
	    eval [string range $data 1 end] $row $col [lindex [lindex $view(view) [expr $col-1]] 1]
	} else {
	    $D(get) D $row [tabview_mapcol $view(view) $col]
	}
}
proc tab_viewcolnum { V name } { upvar $V view; return $view([string trim $name])		}
proc tab_viewcolname { V col } {
	upvar $V view

	lindex [lindex $view(view) [expr $col-1]] 1
}

proc tab_viewcoldel { V col }		{ }
proc tab_viewcolins { V name here }	{ }
proc tab_viewcolapp { V name }		{ }

proc tab_viewrowdel { V row }		{
	upvar $V view;

    foreach T $view(tables) {
	upvar #0 $T D;  $D(rowdel) D $row	
    }
    incr view(rows) -1
}
proc tab_viewrowins { V row }		{
	upvar $V view;

    foreach T $view(tables) {
	upvar #0 $T D;  $D(rowins) D $row	
    }
    incr view(rows)
}
proc tab_viewrowapp { V row }		{
	upvar $V view;

    foreach T $view(tables) {
	upvar #0 $T D;  $D(rowapp) D $row	
    }
    incr view(rows)
}

# Set up a tabview data array for use with tab
#
proc tabview_driver { Dr } {
	upvar $Dr driver

	set driver(nrows)	tab_viewnrows
	set driver(ncols)	tab_viewncols
	set driver(get)		tab_viewget
	set driver(set)		tab_viewset
	set driver(colname)	tab_viewcolname
	set driver(colnum)	tab_viewcolnum
	set driver(colins)	tab_viewcolins
	set driver(coldel)	tab_viewcoldel
	set driver(colapp)	tab_viewcolapp
	set driver(rowins)	tab_viewrowins
	set driver(rowdel)	tab_viewrowdel
	set driver(rowapp)	tab_viewrowapp
	set driver(columns)	tab_viewcolumns
}
tabview_driver TabView

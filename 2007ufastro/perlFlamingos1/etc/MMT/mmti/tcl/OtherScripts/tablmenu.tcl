# Simple table menu commands
#
#my $rcsId = q($Name:  $ $Id: tablmenu.tcl,v 0.1 2003/05/22 14:57:47 raines Exp $);

proc tablmenu { w } {
	upvar #0 $w.tablmenu tabl

    menu $w.tablmenu -tearoff 0
    $w add cascade -menu $w.tablmenu -label "Edit"
    $w.tablmenu add command -label "Columns ..."		\
	-command "tablcolumns $w.tablmenu"

    return $w.tablmenu
}

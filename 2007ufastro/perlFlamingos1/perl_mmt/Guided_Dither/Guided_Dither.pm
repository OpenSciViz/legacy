package Guided_Dither;

require 5.005_62;
use strict;
use warnings;

require Exporter;
use AutoLoader qw(AUTOLOAD);

our @ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use Guided_Dither ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.
our %EXPORT_TAGS = 
  ( 'all' => [ qw(
		  &guided_instrel_offset
		  &guider_connect
		  $socket_undefined
) ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(
	
);

use vars qw/ $VERSION $LOCKER /;
'$Revision: 0.1 $ ' =~ /.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ ' =~ /.*:\s(.*)\s\$/ && ($LOCKER = $1);



# Preloaded methods go here.
our $socket_undefined = "socket undefined";

sub guided_instrel_offset{
  my ( $sock, $d_x, $d_y ) = @_;
  #$d_x = +az if rotator not tracking, = -E if PA = 0
  #$d_y = +el if rotator not tracking, = +N if PA = 0

  my $space = " ";
  my $offset_cmd = "1 ditherinst".$space.$d_x.$space.$d_y;

  if( $sock eq $socket_undefined ){
    die "Invalid socket address: $!\n.  Cannot send guided dither command.\n\n";
  }

  print "submit submit_guider_cmd: $offset_cmd\n";
  print $sock "$offset_cmd\n";
  my $retval = "";
  while ( defined($_ = <$sock>) ) {
    $retval = $retval . $_;
    last if /\n/;
    #print;
  }
  close( $sock );

  return $retval;
}#Endsub guided_instrel_offset


sub guider_connect {
  my $guider_host_name = $ENV{GUIDE_SERVER_NAME};
  my $guider_host_ip   = $ENV{GUIDE_SERVER_IP};
  my $guider_host_port = $ENV{GUIDE_SERVER_PORT};

  my $sock = IO::Socket::INET->new (
			PeerAddr => $guider_host_ip,
			PeerPort => $guider_host_port,
			Proto => 'tcp');

  if( !defined($sock) ){
    $sock = $socket_undefined;
    die "Cannot connect to $guider_host_name:$guider_host_port.\n\n";
  }

  return $sock;
}#Endsub tcsops_connect



# Autoload methods go after =cut, and are processed by the autosplit program.

1;
__END__
# Below is stub documentation for your module. You better edit it!

=head1 NAME

Guided_Dither - Perl extension for Flamingos

=head1 SYNOPSIS

  use Guided_Dither;


=head1 DESCRIPTION

Module for interacting with the MMT guider.


=head1 REVISION & LOCKER

$Name:  $

$Id: Guided_Dither.pm,v 0.1 2003/05/22 15:54:24 raines Exp $

$Locker:  $

=head2 EXPORT

None by default.


=head1 AUTHOR

SNR

=head1 SEE ALSO

perl(1).

=cut

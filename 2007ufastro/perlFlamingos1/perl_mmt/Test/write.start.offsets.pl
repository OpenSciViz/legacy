#!/usr/local/bin/perl -w

my $rcsId = q($Name:  $ $Id: write.start.offsets.pl 14 2008-06-11 01:49:45Z hon $);

use strict;
use mmtTCSinfo qw/:all/;


my $soc = make_socket_connection();
my ( $X_start_offset, $Y_start_offset ) = get_start_offsets( $soc);
close $soc;

my $last_offsets_file = $ENV{FITS_HEADER_LUT_DEBUG}."mmt_last_start_offsets.txt";
open FH, $last_offsets_file or die "$last_offsets_file not found";

my @input;
while( defined (my $line = <FH>) ){
  chomp $line; 
  #print "input line is $line\n";
  push @input, $line;
}

#foreach my $item (@input){
#  print "$item\n";
#}

close FH;

my $old_x = shift @input;
my $old_y = shift @input;

print "\n";
print "The last used offsets and the present value of the offsets are:\n";
print "OLD OFFSETS:  $old_x, $old_y\n";
print "NEW OFFSETS:  $X_start_offset, $Y_start_offset\n\n";

my $x_choice;
my $y_choice;

my $choice; my $is_valid = 1;
open FH, ">$last_offsets_file" or die "not able to write to $last_offsets_file\n";

while( $is_valid ){
  print "Type OLD or NEW to pick which offests to use: ";
  $choice = <>;
  chomp $choice;

  if( $choice =~ m/old/i ){
    $x_choice = $old_x;
    $y_choice = $old_y;
    $is_valid = 0;

  }elsif( $choice =~ m/new/i ){
    $x_choice = $X_start_offset;
    $y_choice = $Y_start_offset;
    $is_valid = 0;
    print FH $x_choice."\n";
    print FH $y_choice."\n";

  }
}

print "You chose $x_choice, $y_choice\n";
close FH;

sub make_socket_connection{
  my $soc;
  my $VIIDO = 1;
  if( $VIIDO ){
    print "\n";
    print "Connecting to the tcs\n";
    $soc = mmtTCSinfo::tcsops_connect();
    if( $soc eq $mmtTCSinfo::socket_undefined ) {
      die "\n\n\tWARNING WARNING WARNING\n".
        "\tCannot connect to $ENV{THIS_TELESCOP} TCSserver\n".
	  "\tsocket = $soc; $!\n\n";
    }
  }else{
    print "variable viido = $VIIDO. Should be making tcs connection\n\n";
  }

  return $soc;
}#Endsub make_socket_connection



sub get_start_offsets{
  my $sock = $_[0];

  my $get_instazoff = "1 get instazoff";
  my $get_insteloff = "1 get insteloff";

  my $instazoff = mmtTCSinfo::tcsops( $get_instazoff, $sock, "not_quiet" );
  my $insteloff = mmtTCSinfo::tcsops( $get_insteloff, $sock, "not_quiet" );

  $instazoff = mmtTCSinfo::parse_tcs_return( $instazoff );
  $insteloff = mmtTCSinfo::parse_tcs_return( $insteloff );

  return ( $instazoff, $insteloff );
}#Endsub get_start_offsets


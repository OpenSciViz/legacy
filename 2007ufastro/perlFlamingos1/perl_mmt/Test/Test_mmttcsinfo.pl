#!/usr/local/bin/perl -w

my $rcsId = q($Name:  $ $Id: Test_mmttcsinfo.pl 14 2008-06-11 01:49:45Z hon $);

use strict;
use Fitsheader qw/:all/;

my $header = q(/export/home/raines/Perl/flamingos.headers.lut/Flamingos.fitsheader);
Fitsheader::Find_Fitsheader($header);
Fitsheader::readFitsHeader($header);

use mmtTCSinfo qw/:all/;

mmtTCSinfo::getMMTtcsinfo( $header );

package mmtTCSinfo;

require 5.005_62;
use strict;
use warnings;
use Fitsheader qw/:all/;
use IO::Socket;
use WCS_Info qw( &set_wcs_info );
#use WCS_Info qw/:all/;

require Exporter;
use AutoLoader qw(AUTOLOAD);

our @ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use mmtTCSinfo ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.
our %EXPORT_TAGS = 
  ( 'all' => [ qw(
		  &check_tcs_returned_ack
		  &getMMTtcsinfo
		  &get_position_angle
		  &instoff_offset
		  &instrel_offset
		  &offset_wait
		  $socket_undefined
		  &tcsops_connect &tcs_info
		  &tcsops &parse_tcs_return
		 ) ]
  );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(
	
);

use vars qw/ $VERSION $LOCKER /;
'$Revision: 0.1 $ ' =~ /.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ ' =~ /.*:\s(.*)\s\$/ && ($LOCKER = $1);


# Preloaded methods go here.
our $socket_undefined = "socket undefined";

sub check_tcs_returned_ack{
  my $ops_reply = $_[0];

  chomp $ops_reply;
  print "\n";
  if( $ops_reply =~ m/^1 ack(\s*)$/ ){
    print "Command received & acknowledged by tcs.\n";
  }else{
    print "Command not acknowledged as expected:\n$ops_reply.\n";
  }
  print "\n";

}#Endsub check_tcs_returned_ack


sub getMMTtcsinfo{
  my $header = $_[0];

  ####TCS CONNECT###
  my $ops_sock = tcsops_connect();
  if( $ops_sock eq $socket_undefined){
    print "Not getting TCS info\n";
    return;
  }
  ###GET TCS INFO  
  my $quiet = "quiet";
  my @ops_reply;
  @ops_reply = tcs_info( \$ops_sock, $quiet );
  #print "tcsops reply:\n @ops_reply\n";
  close( $ops_sock );

  my $EPOCH    = $ops_reply[0];
  my $RA       = $ops_reply[1];  my $DEC      = $ops_reply[2];
  my $UTC      = $ops_reply[3];  my $LST      = $ops_reply[4];
  my $HA       = $ops_reply[5];  my $AIRMASS  = $ops_reply[6];
  my $ZEN_DIST = $ops_reply[7];  my $INSTAZ   = $ops_reply[8];
  my $INSTEL   = $ops_reply[9];  my $ROTANGLE = $ops_reply[10];
  my $TELFOCUS = $ops_reply[11]; my $TELESCOP = $ops_reply[12];
  my $PA       = $ops_reply[13]; my $MJD      = $ops_reply[14];
  my $PARANGLE = $ops_reply[15]; my $UTCDATE  = $ops_reply[16];
  my $OBJECT   = $ops_reply[17];

  setNumericParam( "EPOCH",    $EPOCH    );
  setStringParam(  "RA",       $RA       );
  setStringParam(  "DEC",      $DEC      );
  setStringParam(  "UTC",      $UTC      );
  setStringParam(  "LST",      $LST      );
  setStringParam(  "HA",       $HA       );
  setNumericParam( "AIRMASS",  $AIRMASS  );
  setNumericParam( "ZEN_DIST", $ZEN_DIST );
  setNumericParam( "INSTAZ",   $INSTAZ   );
  setNumericParam( "INSTEL",   $INSTEL   );
  setNumericParam( "ROTANGLE", $ROTANGLE );
  setNumericParam( "TELFOCUS", $TELFOCUS );
  #setStringParam(  "TELESCOP", $TELESCOP );#Screwed up header, so don't write it
  setNumericParam( "PA      ", $PA       );
  setNumericParam( "MJD     ", $MJD      );
  setNumericParam( "PARANGLE", $PARANGLE );
  setStringParam(  "UTCDATE",  $UTCDATE  );
  #setStringParam(  "OBJECT",   $OBJECT   );

  set_my_wcs_info( $RA, $DEC, $header );

  print "\n";
  print "Inside mmtTCSinfo::getMMTtcsinfo.\n";
  printFitsHeader("RA","DEC");
  printFitsHeader("UTC");
  printFitsHeader( "PARANGLE", "ROTANGLE", "PA" );

  print "\nWriting $ENV{THIS_TELESCOP} TCS info into header\n";
  print "$header\n\n";
  writeFitsHeader( $header );
}#Endsub getMMTtcsinfo


sub get_position_angle{
  my $parangle       = 9.42;
  my $rotangle       = 6.28;
  my $position_angle = 3.14;
  my $flag = "success";

  my $sock = tcsops_connect();
  if( $sock eq $socket_undefined ){
    print "Not able to connect to the TCS\n";
    print "Cannot get present value of PARANGLE, ROTANGLE, or PA\n";
    $flag = "failure";
  }else{

    ### Ask tcs for parangle and rotangle
    my $want = "1 get pa";  $parangle = tcsops( $want, $sock, "quiet" );
       $want = "1 get rot"; $rotangle = tcsops( $want, $sock, "quiet" );

    ### Get rid of extraneous characters
    $parangle = parse_tcs_return( $parangle );
    $rotangle = parse_tcs_return( $rotangle );

    ### Truncate long numbers
    #my $decimal_location;

    #$decimal_location = index( $parangle, "." );
    #$parangle        = substr $parangle, 0, ($decimal_location + 3);

    #$decimal_location = index( $rotangle, "." );
    #$rotangle        = substr $rotangle, 0, ($decimal_location + 3);

    $parangle = sprintf "%.3f", $parangle;
    $rotangle = sprintf "%.3f", $rotangle;

    $position_angle = $parangle - $rotangle;
    $position_angle = sprintf "%.3f", $position_angle;

    print "\n";
    print "Inside mmtTCSinfo::get_position_angle.\n";
    print "parangle = $parangle\n";
    print "rotangle = $rotangle\n";
    print "pa       = $position_angle\n";
  }

  return( $position_angle, $parangle, $rotangle, $flag );
}#Endsub get_position_angle


sub instoff_offset{
  my ( $sock, $d_x, $d_y ) = @_;
  #$d_x = +az if rotator not tracking, = -E if PA = 0
  #$d_y = +el if rotator not tracking, = +N if PA = 0

  my $space = " ";
  my $offset_cmd = "1 instoff".$space.$d_x.$space.$d_y;

  if( $sock eq $socket_undefined ){
    print "\n\tWARNING WARNING WARNING\n";
    print "\tTCS server socket is $sock.  Reason = $!\n\n";
    return;
  }

  #print "\nExecuting $offset_cmd\n";
  my $offset_rval = tcsops( $offset_cmd, $sock, "not_quiet");

}#Endsub instoff_offset


sub instrel_offset{
  my ( $sock, $d_x, $d_y ) = @_;
  #$d_x = +az if rotator not tracking, = -E if PA = 0
  #$d_y = +el if rotator not tracking, = +N if PA = 0

  my $space = " ";
  my $offset_cmd = "1 instrel".$space.$d_x.$space.$d_y;

  if( $sock eq $socket_undefined ){
    print "\n\tWARNING WARNING WARNING\n";
    print "\tTCS server socket is $sock.  Reason = $!\n\n";
    return;
  }

  #print "\nExecuting $offset_cmd\n";
  my $offset_rval = tcsops( $offset_cmd, $sock, "not_quiet" );
}#Endsub instrel_offset


sub offset_wait{
  my $wait = $ENV{OFFSET_WAIT};

  #print "Waiting $wait sec for offset\n";
  #sleep( $wait );

  my $cnt = 1;
  print "\n";
  until( $cnt > $wait ){
    print "\tWait $cnt seconds out of $wait seconds for telescope\r";
    sleep 1;
    $cnt++;
  }
  print "\n\n";
}#Endsub offset_wait


sub parse_tcs_return{
  my $input = $_[0];

  $input =~ s/^(\s*)1 ack //;
  my @field = split( /(\s+)/, $input );

  return $field[0];
}#Endsub parser_tcs_return


sub set_my_wcs_info{
  my ( $RA, $DEC, $header ) = @_;

  use WCS_Info qw/:all/;

  my $ra_deg  = WCS_Info::hms2deg( $RA );
  my $deg_deg = WCS_Info::dms2deg( $DEC );


  setNumericParam( "CRVAL1", $ra_deg  );
  setNumericParam( "CRVAL2", $deg_deg );

  my $chip_pa = param_value( 'CHIP-PA' );
  my $pa_sky  = param_value( 'PA'  );

  my $crota1  = 180 - ($pa_sky + $chip_pa);
  $crota1 = sprintf "%.3f", $crota1;

  print "Inside mmtTCSinfo::set_my_wcs_info.\n";
  print "chip_pa = $chip_pa (from header)\n";
  print "pa_sky  = $pa_sky  (from_header)\n";
  print "crota1  = $crota1  (about to write to header)\n";

  setNumericParam( "CROTA1", $crota1 );
}#Endsub set_wcs_info


sub tcsops_connect {
  my $tcs_host_name = $ENV{TCS_SERVER_NAME};
  my $tcs_host_ip   = $ENV{TCS_SERVER_IP};
  my $tcs_host_port = $ENV{TCS_SERVER_PORT};

  my $sock = IO::Socket::INET->new (
			PeerAddr => $tcs_host_ip,
			PeerPort => $tcs_host_port,
			Proto => 'tcp');

  if( !defined($sock) ){
    $sock = $socket_undefined;
  }
  #print "\n";
  #print "socket = $sock\n";
  return $sock;
}#Endsub tcsops_connect


sub tcs_info {
  #my $sock = $_;
  my ($stref, $quiet ) = @_;
  my $sock = $$stref;
  print "tcs_info sock is $sock\n";

  ###Get the data###
  my $want;
  #the tcsops lines had \$want, $sock, don't know why pass by ref
  print "\n";
  print "       RAW TCS INFO\n";
  print "----------------------------\n";
  $want = "1 get epoch";     my $epoch     = tcsops( $want, $sock, $quiet );  print "epoch     = $epoch\n";
  $want = "1 get ra";        my $ra        = tcsops( $want, $sock, $quiet );  print "ra        = $ra\n";
  $want = "1 get dec";       my $dec       = tcsops( $want, $sock, $quiet );  print "dec       = $dec\n";
  #$want = "1 get rastr";     $ra        = tcsops( $want, $sock, $quiet );
  #$want = "1 get decstr";    $dec       = tcsops( $want, $sock, $quiet );
  $want = "1 get ut";        my $utime     = tcsops( $want, $sock, $quiet );  print "utime     = $utime\n";
  $want = "1 get lst";       my $stime     = tcsops( $want, $sock, $quiet );  print "stime     = $stime\n";
  $want = "1 get ha";        my $ha        = tcsops( $want, $sock, $quiet );  print "ha        = $ha\n";
  $want = "1 get airmass";   my $airmass   = tcsops( $want, $sock, $quiet );  print "airmass   = $airmass\n";
  $want = "1 get thetaz";    my $zen_dist  = tcsops( $want, $sock, $quiet );  print "zen_dist  = $zen_dist\n";
  $want = "1 get instazoff"; my $instazoff = tcsops( $want, $sock, $quiet );  print "instazoff = $instazoff\n";
  $want = "1 get insteloff"; my $insteloff = tcsops( $want, $sock, $quiet );  print "insteloff = $insteloff\n";
  $want = "1 get rot";       my $rot       = tcsops( $want, $sock, $quiet );  print "rot       = $rot\n";
  $want = "1 get focus";     my $telfocus  = tcsops( $want, $sock, $quiet );  print "focus     = $telfocus\n";
  $want = "1 get telname";   my $telname   = tcsops( $want, $sock, $quiet );  print "telname   = $telname\n";
  $want = "1 get pa     ";   my $parangle  = tcsops( $want, $sock, $quiet );  print "parangle  = $parangle\n";
  $want = "1 get mjd    ";   my $mjd       = tcsops( $want, $sock, $quiet );  print "mjd       = $mjd\n";
  $want = "1 get dateobs";   my $utcdate   = tcsops( $want, $sock, $quiet );  print "utcdate   = $utcdate\n";
  $want = "1 get cat_id";    my $object    = tcsops( $want, $sock, $quiet );  print "object    = $object\n";


  ###Get rid of extraneous characters###
  my @field;
  $epoch     = parse_tcs_return( $epoch     );
  $ra        = parse_tcs_return( $ra        );
  $dec       = parse_tcs_return( $dec       );
  $utime     = parse_tcs_return( $utime     );
  $stime     = parse_tcs_return( $stime     );
  $ha        = parse_tcs_return( $ha        );
  $airmass   = parse_tcs_return( $airmass   );
  $zen_dist  = parse_tcs_return( $zen_dist  );
  $instazoff = parse_tcs_return( $instazoff );
  $insteloff = parse_tcs_return( $insteloff );
  $rot       = parse_tcs_return( $rot       );
  $telfocus  = parse_tcs_return( $telfocus  );
  $telname   =~ s/^(\s*)1 ack //;  #This acutally screwed up header, so won't be written
  $parangle  = parse_tcs_return( $parangle  );
  $mjd       = parse_tcs_return( $mjd       );
  $utcdate   = parse_tcs_return( $utcdate   );
  $object    = parse_tcs_return( $object   );
  #print "object = $object\n";

  my $obj_len = length $object;
  #print "obj_len = $obj_len\n";
  if( ($obj_len eq 2) && ($object =~ m/^\{/) && ($object =~ m/\}$/) ){
    #print "found leading left curly bracket and trailing right curly bracket\n";
    $object = "none";
  }
  print "object = $object\n";

  ###Trucate long number###
  #my $dec_loc;
  #$dec_loc = index($airmass, ".");
  #$airmass = substr $airmass, 0, ($dec_loc + 4);
  $airmass = sprintf "%.4f", $airmass;

  #$dec_loc = index($zen_dist, ".");
  #$zen_dist = substr $zen_dist, 0, ($dec_loc + 4);
  $zen_dist = sprintf "%.4f", $zen_dist;

  #$dec_loc = index($instazoff, ".");
  #$instazoff = substr $instazoff, 0, ($dec_loc + 4);
  $instazoff = sprintf "%.4f", $instazoff;

  #$dec_loc = index($insteloff, ".");
  #$insteloff = substr $insteloff, 0, ($dec_loc + 4);
  $insteloff = sprintf "%.4f", $insteloff;

  #$dec_loc = index($rot, ".");
  #$rot = substr $rot, 0, ($dec_loc + 4);
  $rot = sprintf "%.4f", $rot;

  #$dec_loc = index($telfocus, "."); 
  #$telfocus = substr $telfocus, 0, ($dec_loc + 3);
  $telfocus = sprintf "%.3f", $telfocus;

  #$dec_loc = index($parangle, "."); 
  #$parangle = substr $parangle, 0, ($dec_loc + 3);
  $parangle = sprintf "%.3f", $parangle;

  my $pos_angle = $parangle - $rot;
  $pos_angle = sprintf "%.3f", $pos_angle;

  print "\n";
  print "Inside mmtTCSinfo::tcs_info.\n";
  print "parangle = $parangle\n";
  print "rotangle = $rot\n";
  print "pa       = $pos_angle\n";

  my @return = ($epoch, $ra, $dec, $utime, $stime, $ha, $airmass, $zen_dist,
	        $instazoff, $insteloff, $rot, $telfocus, $telname,
		$pos_angle, $mjd, $parangle, $utcdate, $object );

  return @return;
}#Endsub tcs_info


sub tcsops {
  my ($cmd, $sock, $quiet) = @_;

  if( $sock eq $socket_undefined ){
    die "Invalid socket address: $!\n\n";
  }

  unless( $quiet eq "quiet" ){
    print "submit tcsops: $cmd\n";
  }
  print $sock "$cmd\n";
  my $retval = "";
  while ( defined($_ = <$sock>) ) { 
    $retval = $retval . $_;
    last if /\n/;
    #print;
  }
  return $retval;
}#Endsub tcsops


# Autoload methods go after =cut, and are processed by the autosplit program.

1;
__END__
# Below is stub documentation for your module. You better edit it!

=head1 NAME

mmtTCSinfo - Perl extension for taking Flamingos data at the MMT

=head1 SYNOPSIS

  use mmtTCSinfo qw/:all/;

=head1 DESCRIPTION

  Routines to get MMT TCS information

=head2 EXPORT


=head1 REVISION & LOCKER

$Name:  $

$Id: mmtTCSinfo.pm,v 0.1 2003/05/22 15:55:20 raines Exp $

$Locker:  $


=head1 AUTHOR

S. N. Raines, 09 Jan 2002

=head1 SEE ALSO

perl(1).

=cut

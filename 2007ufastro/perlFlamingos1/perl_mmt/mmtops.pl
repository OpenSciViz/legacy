#!/usr/local/bin/perl -w

#use IO::Socket;

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use mmtTCSinfo qw/:all/;
use strict;

if( @ARGV < 1 ) {
  print "\n\nUsage: mmtops.pl <parameter>\n\n";
  print "Valid parameters:\n";
  print "-info                    (print out present tcs info)\n";
  print "-getfocus                (print out present focus value)\n";
  print "\n";
  print "-focus   d_focus         (increment focus by delta_focus)\n";
  print "-instrel d_x d_y         (relative ra-dec offset telescope in instrument space)\n";
  print "-instoff d_x d_y         (absolute ra-dec offset telescope in instrument space)\n";
  print "-azeloff d_az d_el d_rot (relative offest in azimuth, elevation, rotation)\n";
  print "\n";
  print "-ops 'some_tcs_command'  (e.g. -ops '1 get telname')\n";
  print "\n\n";
  exit ( 1 );
}

my $cmd = shift;
my $ops_sock = mmtTCSinfo::tcsops_connect();

if( $ops_sock eq "do_not_update_tcs_info"){
  print "Unable to connect to tcsops host ($!)\n\n";
  exit;
}

my $ops_reply = "";
if( $cmd eq "-ops" ){
  my $raw = shift;
  if( $raw ){
    $ops_reply = mmtTCSinfo::tcsops( $raw, $ops_sock, "not_quiet" );
    #print "tcs reply = $ops_reply\n";

  }else{
    $ops_reply = "please specify a complete command.";
  }

}elsif( $cmd eq "-info" ){
  get_and_print_tcs_info();
  exit;

}elsif( $cmd eq "-getfocus" ){
  my $foc = $ops_reply = mmtTCSinfo::tcsops("1 get focus", $ops_sock, "not_quiet" );
  $foc =~ s/1 ack //;
  print "$foc\n";
  close ($ops_sock);
  exit;

}elsif( $cmd eq "-focus" ){
  my $foc = shift;
  if( !$foc ){ $foc = 0; }
  $ops_reply = mmtTCSinfo::tcsops("1 focuserr $foc", $ops_sock, "not_quiet" );

}elsif( $cmd eq "-instoff" ) {
  my $x = shift; if( !$x ) { $x = 0; }
  my $y = shift; if( !$y ) { $y = 0; }

  $ops_reply = mmtTCSinfo::tcsops("1 instoff $x $y", $ops_sock, "not_quiet" );

}elsif( $cmd eq "-instrel" ) {
  my $x = shift; if( !$x ){ $x = 0; }
  my $y = shift; if( !$y ){ $y = 0; }

  $ops_reply = mmtTCSinfo::tcsops("1 instrel $x $y", $ops_sock, "not_quiet" );

}elsif( $cmd eq "-azeloff" ){
  my $az = shift;  if( !$az ){ $az = 0; }
  my $el = shift;  if( !$el ){ $el = 0; }
  my $rot = shift; if( !$rot ){ $rot = 0; }

  $ops_reply = mmtTCSinfo::tcsops("1 azelerr $az $el $rot", $ops_sock, "not_quiet" );

}#End big if

if( $cmd ne "-tcs" ){
  print "\n\ntcsops reply:\n $ops_reply\n";
  close ($ops_sock);
}


#------- subroutines  -----------------------------------------------------

sub get_and_print_tcs_info {
  my $quiet = "not_quiet";

  my @ops_reply = mmtTCSinfo::tcs_info( \$ops_sock, $quiet );
  close( $ops_sock );

  my $EPOCH    = $ops_reply[0];
  my $RA       = $ops_reply[1]; my $DEC      = $ops_reply[2];
  my $UTC      = $ops_reply[3]; my $LST      = $ops_reply[4];
  my $HA       = $ops_reply[5]; my $AIRMASS  = $ops_reply[6];
  my $ZEN_DIST = $ops_reply[7]; my $INSTAZ   = $ops_reply[8];
  my $INSTEL   = $ops_reply[9]; my $ROTANGLE = $ops_reply[10];
  my $TELFOCUS = $ops_reply[11];
  my $TELESCOP = $ops_reply[12]; my $OBSERVAT = $TELESCOP . "O";
  my $PA       = $ops_reply[13]; my $MJD      = $ops_reply[14];
  my $PARANGLE = $ops_reply[15];

  print "RA  = $RA\n";
  print "Dec = $DEC\n";
  print "\n";
  print "UTC = $UTC\n";
  print "LST = $LST\n";
  print "MJD = $MJD\n";
  print "\n";
  print "AIRMASS = $AIRMASS\n";
  print "HA      = $HA\n";
  print "ZD      = $ZEN_DIST\n";
  print "EPOCH   = $EPOCH\n";
  print "\n";
  print "PA       = $PA\n";
  print "ROTANGLE = $ROTANGLE\n";
  print "PARANGLE = $PARANGLE\n";
  print "TELFOCUS = $TELFOCUS\n";
  print "INSTAZ   = $INSTAZ\n";
  print "INSTEL   = $INSTEL\n";
  print "OBSERVAT = $OBSERVAT\n";
}#Endsub tcsinfo


__END__

=head1 NAME

mmtops.pl

=head1 Description

Use to talk to the mmt TCS.


=head1 REVISION & LOCKER

$Name:  $

$Id: mmtops.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut

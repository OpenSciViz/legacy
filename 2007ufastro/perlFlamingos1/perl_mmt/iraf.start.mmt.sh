#!/usr/bin/sh

# $Name:  $
# $Id: iraf.start.mmt.sh 14 2008-06-11 01:49:45Z hon $
# $Locker:  $

cd /data0/mmtguest/
ds9 -display packrat:0.1 -geometry 1280x1024-0+0 &
cd $HOME/iraf
xgterm -sb -sl 2000 -T 'flamingos iraf' -n iraf -display packrat:0.0 -geometry 80x40-0+0 &
echo 'Type cl in the flamingos_iraf window to start iraf'

#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;
use Getopt::Long;
use MosWheel  qw/:all/;
use Constants qw/:all/;

#----Load Variables-----------------------------------------------------------#
my $DEBUG = 0;#0 = not debugging; 1 = debugging
GetOptions( 'debug' => \$DEBUG );

use Fitsheader qw/:all/;
my $header = Fitsheader::select_header($DEBUG);
#$header = "/export/home/raines/Perl/flamingos.headers.lut/Flamingos.test.header";
#$header = "/home/raines/Perl/flamingos.headers.lut/Flamingos.test.header";
Fitsheader::Find_Fitsheader($header);
Fitsheader::readFitsHeader($header);
my $home_type = Fitsheader::param_value( 'HT_1' );
print "home_type is $home_type\n";
my $motor    = qw( b );
my $wheel    = qw( mos_or_slit );
my ( $actual_pos, $closest_pos, $closest_name, $difference, $motion, $closest_index )
   = ( 0, 0, "unknown", 0, "unknown", 0 );

#----Get Status, ask if want to change wheel----------------------------------#
my $mosnames_fitsfragment;
if( !$DEBUG ){
  $mosnames_fitsfragment = $ENV{FITS_HEADER_LUT}.$ENV{MOS_NAMES};
}else{
  $mosnames_fitsfragment = $ENV{FITS_HEADER_LUT_DEBUG}.$ENV{MOS_NAMES};
}

my $ar_mosplate_names_vector= main::Load_Mos_Names( $mosnames_fitsfragment );
my @mosplate_names_vector = @$ar_mosplate_names_vector;


($actual_pos,  $motion, $closest_pos, $closest_name, 
 $difference, $closest_index) = MosWheel::get_mos_status();

my $closest_likely_plate_name = $mosplate_names_vector[$closest_index];

#MosWheel::print_mos_info( $motor, $wheel ,
main::my_test_print_mos_info( $motor, $wheel, 
			  $actual_pos, $closest_pos, $closest_name,
			  $difference, $motion, $home_type, $closest_likely_plate_name );

use GetYN;
print "\n\nChange the mos-slit wheel configuration? ";
my $change = GetYN::get_yn();
if( $change == 0 ){
  die "Exiting\n\n";
}

#----Proceed with wheel change------------------------------------------------#
my $new_pos    = PI;
my $new_index  =  0;
my $new_name   = qw( nc );
my $mosplate_name = Fitsheader::param_value( "MOS_NAME" );
my $selected_mos_plate_name = $mosplate_name;

main::selection_loop();

main::set_plate_name_in_selected_fragment( $selected_mos_plate_name );
main::update_header();

#die;

#----Build home and motion command strings------------------------------------#
my $num_waits = 0;
my $case = "unknown";
my $backlash = $ENV{BACKLASH_1};

my $i_v_cmd  = $ENV{"I_1"}.$MosWheel::ampersand.$ENV{"V_1"}.
                           $MosWheel::ampersand;
my $home_cmd = $ENV{"NEW_FV_0"};

my @home_and_move;
my @motion_amount;

#main::test_limit_cases(); die "\n\n";
#main::test_near_home_cases();   die "\n\n";
#main::test_case_inputs(2);
#main::test_near_home_case_inputs(13);

if( $home_type == 1 ){
  #USE LIMIT SWTICH
  my ($ar_home_and_move, $ar_motion_amount) = main::build_limit_home_and_move();
  @home_and_move = @$ar_home_and_move;
  @motion_amount = @$ar_motion_amount;

  #----Following loop for test purposes
  $num_waits = $home_and_move[0];
  print "\n\nCase = $case: Starting position = $actual_pos, ".
        "Requested new position = $new_pos\n\n";
  print "This request will take $home_and_move[0] steps,\n";
  print "consisting of the following motor controller sequences:\n\n";
  for(my $i = 1; $i <= $num_waits; $i++){
    #print "$home_and_move[$i]; motion amount = $motion_amount[$i]\n";
    print "$home_and_move[$i]\n";
  }
  print "\n\n";

}elsif( $home_type == 0 ){
  #USE NEAR HOME METHOD
  my ($ar_home_and_move, $ar_motion_amount) = main::build_near_home_and_move();
  @home_and_move = @$ar_home_and_move;
  @motion_amount = @$ar_motion_amount;


  #----Following loop for test purposes
  $num_waits = $home_and_move[0];
  print "\n\nCase = $case\n";
  print "Starting position = $actual_pos, ".
        "Requested new position = $new_pos\n\n";
  print "This request will take $home_and_move[0] steps,\n";
  print "consisting of the following motor controller sequences:\n\n";
  for(my $i = 1; $i <= $num_waits; $i++){
    #print "$home_and_move[$i]; motion amount = $motion_amount[$i]\n";
    print "$home_and_move[$i]\n";
  }
  print "\n\n";

}
#----Execute each motion and then wait for stationary twice in a row----------#
#die;
move_and_wait_loop( \@home_and_move, \@motion_amount );


####SUBS######################################################################
sub selection_loop{

  my $redo =  0;
  until( $redo ){
    ($new_pos, $new_index, $selected_mos_plate_name) = main::my_select_new_position();
    #($new_pos, $new_index) = MosWheel::select_new_position();

    $new_name = $MosWheel::MOS_SLIT_PN_NAMES[$new_index];

    MosWheel::print_mos_exec_msg();
    #MosWheel::print_mos_selection( $motor, $wheel, $closest_name, $new_name, $new_pos);

    main::my_test_print_mos_selection( $closest_name, $closest_likely_plate_name, 
				       $new_name, $new_pos, $selected_mos_plate_name);

    print "\nEnter y to update the fits header template and ".
	  "move the mos-slit wheel, or\n";
    print "Enter n to redo your selection, or\n";
    print "Enter Ctrl-C to cancel\n";
    print "Entry:  ";

    $redo = GetYN::get_yn();
  }
}#Endsub selection_loop


sub update_header{
  main::set_MOS_SLIT_params();

  print "new =  $selected_mos_plate_name \n";
  main::set_plate_name_in_header( "MOS_NAME", $selected_mos_plate_name );

  print "\n";
  Fitsheader::writeFitsHeader( $header );
}#Endsub update_header


sub set_MOS_SLIT_params{
  my $name = $new_name;
  my $pos  = $new_pos;

  if( $name =~ m/imaging/ ){
    setStringParam( "SLIT", $name );
    setStringParam( "MOS",  $name );
    setNumericParam( "POS_B", $pos );
    print "New header param: SLIT  =$name, ";
    print "\tPOS_B=$pos\n";
    print "New header param: MOS   =$name, ";
    print "\tPOS_B=$pos\n";
  }elsif( $name =~ m/pix/ ){
    setStringParam( "SLIT",   $name );
    setStringParam( "MOS",   'long-slit' );
    setNumericParam( "POS_B", $pos );
    print "New header param: SLIT  =$name, ";
    print "\t\tPOS_B=$pos\n";
  }else{
    setStringParam( "MOS",  $name );
    setStringParam( "SLIT", 'mos' );
    setNumericParam( "POS_B", $pos );
    print "New header param: MOS   =$name, ";
    print "\t\tPOS_B=$pos\n";
  }

}#Endsub set_MOS_SLIT_params


sub build_limit_home_and_move{
  my @home_and_move;
  my @motion_amount;

  if( ($actual_pos < 0) and (abs($actual_pos) >= $backlash) and ($new_pos < 0) ){
    limit_1a( \@home_and_move, \@motion_amount);

  }elsif( ($actual_pos < 0) and (abs($actual_pos) >= $backlash) and 
	  ($new_pos >= 0) ){
    limit_1bc( \@home_and_move, \@motion_amount);

  }elsif( ($actual_pos < 0) and (abs($actual_pos) <= $backlash) and 
	  ($new_pos < 0 )){
    limit_2a( \@home_and_move, \@motion_amount);

  }elsif( ($actual_pos < 0) and (abs($actual_pos) <= $backlash) and
	  ($new_pos >= 0 )){
    limit_2bc( \@home_and_move, \@motion_amount);

  }elsif( ($actual_pos == 0) and ($new_pos < 0) ){
    limit_3a( \@home_and_move, \@motion_amount);

  }elsif( ($actual_pos == 0) and ($new_pos == 0) ){
    #case 3b
    #take out backlash and move by 0
    limit_3bc( \@home_and_move, \@motion_amount);

  }elsif( ($actual_pos == 0) and ($new_pos > 0) ){
    limit_3bc( \@home_and_move, \@motion_amount);

  }elsif( ($actual_pos > 0) and ($new_pos < 0) ){
    limit_4a( \@home_and_move, \@motion_amount);

  }elsif( ($actual_pos > 0) and ($new_pos == 0) ){
    limit_4bc( \@home_and_move, \@motion_amount);

  }elsif( ($actual_pos > 0) and ($new_pos > 0) ){
    #case 4b and 4c do the same thing
    limit_4bc( \@home_and_move, \@motion_amount);

  }#end ifelse case block

  return( \@home_and_move, \@motion_amount );
}#Endsub build_limit_home_and_move


sub build_near_home_and_move{
  my @home_and_move;
  my @motion_amount;

  if( ($actual_pos < 0) and ($new_pos <= $actual_pos) ){
    near_home_1ab( \@home_and_move, \@motion_amount);

  }elsif( ($actual_pos < 0) and ($new_pos > $actual_pos) and ($new_pos < 0) ){
    near_home_1c( \@home_and_move, \@motion_amount);
    #-----------
  }elsif( ($actual_pos < 0) and ($new_pos == 0) ){
    near_home_2( \@home_and_move, \@motion_amount);

  }elsif( ($actual_pos < 0) and ($new_pos > 0) ){
    near_home_3( \@home_and_move, \@motion_amount);
  #============
  }elsif( ($actual_pos == 0) and ($new_pos < 0) ){
    near_home_4( \@home_and_move, \@motion_amount);

  }elsif( ($actual_pos == 0) and ($new_pos == 0) ){
    near_home_5( \@home_and_move, \@motion_amount);

  }elsif( ($actual_pos == 0) and ($new_pos > 0) ){
    near_home_6( \@home_and_move, \@motion_amount);
  #===========
  }elsif( ($actual_pos > 0) and ($new_pos < 0) ){
    near_home_7( \@home_and_move, \@motion_amount);

  }elsif( ($actual_pos > 0) and ($new_pos == 0) ){
    near_home_8( \@home_and_move, \@motion_amount);
    #---------
  }elsif( ($actual_pos > 0) and ($new_pos <= $actual_pos) and ($new_pos > 0  )){
    near_home_9ab( \@home_and_move, \@motion_amount);

  }elsif( ($actual_pos > 0) and ($new_pos > $actual_pos) ){
    near_home_9c( \@home_and_move, \@motion_amount);

  }#end ifelse case block

  return( \@home_and_move, \@motion_amount );
}#Endsub build_near_home_and_move


sub limit_1a{
  my ( $ar_hm, $ar_m ) = @_;
  #actual < backlash < 0; new < 0
  $case = "1a";

  $$ar_hm[0] = 3; #number of waits

  my $motion1 = abs( $actual_pos ) - $backlash;
  $$ar_m[1]  = $motion1;
  $$ar_hm[1] = $i_v_cmd . "b+$motion1";

  my $motion2 = abs( $new_pos ) + $backlash;
  $$ar_m[2]  = $motion2;
  $$ar_hm[2] = $home_cmd.$MosWheel::ampersand."b-$motion2";

  my $motion3 = $backlash;
  $$ar_m[3]  = $motion3;
  $$ar_hm[3] = "b+$motion3";

}#Endsub limit_1a


sub limit_1bc{
  my ( $ar_hm, $ar_m ) = @_;
  #actual < backlash < 0; new >= 0
  $case = "1bc";

  $$ar_hm[0] = 2; #number of waits

  my $motion1 = abs( $actual_pos ) - $backlash;
  $$ar_m[1]  = $motion1;
  $$ar_hm[1] = $i_v_cmd . "b+$motion1";

  my $motion2 = $new_pos;
  $$ar_m[2]  = $motion2;
  $$ar_hm[2] = $home_cmd.$MosWheel::ampersand."b+$motion2";

}#Endsub limit_1bc


sub limit_2a{
  #my $ar_hm = $_[0];
  my ( $ar_hm, $ar_m ) = @_;
  #backlash < actual < 0; new < 0
  $case = "2a";

  $$ar_hm[0] = 2; #number of waits

  my $motion1 = abs( $new_pos ) + $backlash;
  $$ar_m[1]  = $motion1;
  $$ar_hm[1] = $i_v_cmd.$home_cmd.$MosWheel::ampersand. "b-$motion1";

  my $motion2 = $backlash;
  $$ar_m[2]  = $motion2;
  $$ar_hm[2] = "b+$motion2";

}#Endsub limit_2a


sub limit_2bc{
  #my $ar_hm = $_[0];
  my ( $ar_hm, $ar_m ) = @_;
  #backlash < actual < 0; new >= 0
  $case = "2bc";

  $$ar_hm[0] = 1; #number of waits

  my $motion1 = $new_pos;
  $$ar_m[1]  = $motion1;
  $$ar_hm[1] = $i_v_cmd.$home_cmd.$MosWheel::ampersand."b+$motion1";


}#Endsub limit_2bc


sub limit_3a{
  #my $ar_hm = $_[0];
  my ( $ar_hm, $ar_m ) = @_;
  #backlash < actual = 0; new < 0
  $case = "3a";

  $$ar_hm[0] = 3; #number of waits

  my $motion1 = $backlash;
  $$ar_m[1]  = $motion1;
  $$ar_hm[1] = $i_v_cmd."b-$motion1";

  my $motion2 = abs( $new_pos ) + $backlash;
  $$ar_m[2]  = $motion2;
  $$ar_hm[2] = $home_cmd.$MosWheel::ampersand."b-$motion2";

  my $motion3 = $backlash;
  $$ar_m[3]  = $motion3;
  $$ar_hm[3] = "b+$motion3";

}#Endsub limit_3a


sub limit_3bc{
  #my $ar_hm = $_[0];
  my ( $ar_hm, $ar_m ) = @_;
  #backlash < actual = 0; new >= 0
  $case = "3bc";

  $$ar_hm[0] = 2; #number of waits

  my $motion1 = $backlash;
  $$ar_m[1]  = $motion1;
  $$ar_hm[1] = $i_v_cmd."b-$motion1";

  my $motion2 = $new_pos;
  $$ar_m[2]  = $motion2;
  $$ar_hm[2] = $home_cmd.$MosWheel::ampersand."b+$motion2";

}#Endsub limit_3c


sub limit_4a{
  #my $ar_hm = $_[0];
  my ( $ar_hm, $ar_m ) = @_;
  #backlash < 0 < actual; new < 0
  $case = "4a";

  $$ar_hm[0] = 3; #number of waits

  my $motion1 = abs( $actual_pos ) + $backlash;
  $$ar_m[1]  = $motion1;
  $$ar_hm[1] = $i_v_cmd . "b-$motion1";

  my $motion2 = abs( $new_pos ) + $backlash;
  $$ar_m[2]  = $motion2;
  $$ar_hm[2] = $home_cmd.$MosWheel::ampersand."b-$motion2";

  my $motion3 = $backlash;
  $$ar_m[3]  = $motion3;
  $$ar_hm[3] = "b+$motion3";

}#Endsub limit_4a


sub limit_4bc{
  #my $ar_hm = $_[0];
  my ( $ar_hm, $ar_m ) = @_;
  #backlash < 0 < actual; new >= 0
  $case = "4bc";

  $$ar_hm[0] = 2; #number of waits

  my $motion1 = $actual_pos + $backlash;
  $$ar_m[1]  = $motion1;
  $$ar_hm[1] = $i_v_cmd."b-$motion1";

  my $motion2 = $new_pos;
  $$ar_m[2]  = $motion2;
  $$ar_hm[2]  = $home_cmd.$MosWheel::ampersand."b+$motion2";

}#Endsub limit_4bc

#-----
sub near_home_1ab{
  my ( $ar_hm, $ar_m ) = @_;
  #new < actual <= 0
  $case = "near_home_1ab; new < actual < 0";

  $$ar_hm[0] = 1; #number of steps = number of waits - 1

  my $motion1 = abs( $new_pos ) + $backlash;
  $$ar_m[1]   = $motion1;
  $$ar_hm[1]  = $i_v_cmd . "bN-$motion1".$MosWheel::ampersand."b+$backlash";

}#Endsub near_home_1ab


sub near_home_1c{
  my ( $ar_hm, $ar_m ) = @_;
  #actual < new < 0
  $case = "near_home_1c; actual < new < 0";

  $$ar_hm[0] = 1;

  my $motion1 = abs( $new_pos );
  $$ar_m[1]   = $motion1;
  $$ar_hm[1]  = $i_v_cmd . "bN-$motion1";

}#Endsub near_home_1c


sub near_home_2{
  my ( $ar_hm, $ar_m ) = @_;
  #actua < new = 0
  $case = "near_home_2; actual < new = 0";

  $$ar_hm[0] = 1; #number of steps = number of waits - 1

  my $motion1 = abs( $actual_pos );
  $$ar_m[1]   = $motion1;
  $$ar_hm[1]  = $i_v_cmd . "b+$motion1";;

}#Endsub near_home_2a


sub near_home_3{
  my ( $ar_hm, $ar_m ) = @_;
  #actual < 0 < new
  $case = "near_home_3; actual < 0 < new";

  $$ar_hm[0] = 1; #number of steps = number of waits - 1

  my $motion1 = abs( $actual_pos ) + $new_pos;
  $$ar_m[1]   = $motion1;
  $$ar_hm[1]  = $i_v_cmd . "b+$motion1";;

}#Endsub near_home_2b


sub near_home_4{
  my ( $ar_hm, $ar_m ) = @_;
  #new < actual = 0
  $case = "near_home_4; new < actual = 0";

  $$ar_hm[0] = 1; #number of steps = number of waits - 1


  my $motion1 = abs( $new_pos ) + $backlash;
  $$ar_m[1]   = $motion1;
  $$ar_hm[1]  = $i_v_cmd . "bN-$motion1".$MosWheel::ampersand."b+$backlash";

}#Endsub near_home_4


sub near_home_5{
  my ( $ar_hm, $ar_m ) = @_;
  #new = actual = 0
  $case = "near_home_5; new = actual = 0";

  $$ar_hm[0] = 1; #number of steps = number of waits - 1


  my $motion1 = $backlash;
  $$ar_m[1]   = $motion1;
  $$ar_hm[1]  = $i_v_cmd . "bN-$motion1".$MosWheel::ampersand."b+$backlash";

}#Endsub near_home_5


sub near_home_6{
  my ( $ar_hm, $ar_m ) = @_;
  #actual = 0 < new 
  $case = "near_home_6; actual = 0 < new";

  $$ar_hm[0] = 1; #number of steps = number of waits - 1

  my $motion1 = $new_pos;
  $$ar_m[1]   = $motion1;
  $$ar_hm[1]  = $i_v_cmd . "b+$motion1";

}#Endsub near_home_6


sub near_home_7{
  my ( $ar_hm, $ar_m ) = @_;
  #new < 0 < actual
  $case = "near_home_7; new < 0 < actual";

  $$ar_hm[0] = 1; #number of steps = number of waits - 1

  my $motion1 = abs( $new_pos ) + $backlash;
  $$ar_m[1]   = $motion1;
  $$ar_hm[1]  = $i_v_cmd . "bN-$motion1".$MosWheel::ampersand."b+$backlash";

}#Endsub near_home_7


sub near_home_8{
  my ( $ar_hm, $ar_m ) = @_;
  #new = 0 < actual
  $case = "near_home_8; new = 0 < actual";

  $$ar_hm[0] = 1; #number of steps = number of waits - 1

  my $motion1 = $backlash;
  $$ar_m[1]   = $motion1;
  $$ar_hm[1]  = $i_v_cmd . "bN-$motion1".$MosWheel::ampersand."b+$backlash";

}#Endsub near_home_8


sub near_home_9ab{
  my ( $ar_hm, $ar_m ) = @_;
  #0 < new <= actual
  $case = "near_home_9ab; 0 < new <= actual";

  $$ar_hm[0] = 1; #number of steps = number of waits - 1

  my $motion1 = $new_pos - $backlash;
  $$ar_m[1]   = $motion1;
  $$ar_hm[1]  = $i_v_cmd . "bN+$motion1".$MosWheel::ampersand."b+$backlash";

}#Endsub near_home_9ab


sub near_home_9c{
  my ( $ar_hm, $ar_m ) = @_;
  #0 < actual < new
  $case = "near_home_9c; 0 < actual < new";

  $$ar_hm[0] = 1; #number of steps = number of waits - 1

  my $motion1 = $new_pos;
  $$ar_m[1]   = $motion1;
  $$ar_hm[1]  = $i_v_cmd . "bN+$motion1";

}#Endsub near_home_9c


#-----
sub test_limit_cases{

  for( my $i = 1; $i <= 12; $i++ ){
    test_case_inputs( $i );

    my ( $ar_home_and_move, $ar_motion_amount ) = main::build_limit_home_and_move();
    my @home_and_move = @$ar_home_and_move;
    my @motion_amount = @$ar_motion_amount;
    
    $num_waits = $home_and_move[0];
    print "---------\nCase $i = $case\n";
    print "actual = $actual_pos; new = $new_pos; bl = $backlash\n";
    print "Number of wait = $home_and_move[0]\n";
    for(my $i = 1; $i <= $num_waits; $i++){
      print "$home_and_move[$i]; motion amount = $motion_amount[$i]\n";
    }
  }
}#End test_limit_cases


sub test_case_inputs{
  my $i = $_[0];

  #set 1 cases; P << backlash
  if   ( $i == 1 ){ #case 1a
    $actual_pos = -1000; $new_pos = -2000; }
  elsif( $i == 2 ){ #case 1b
    $actual_pos = -1000; $new_pos = 0;     }
  elsif( $i == 3 ){ #case 1c
    $actual_pos = -1000; $new_pos = 2000;  }

  #set 2 cases; P > backlash
  elsif( $i == 4 ){ #case 2a
    $actual_pos = -50; $new_pos = -2000; }
  elsif( $i == 5 ){ #case 2b
    $actual_pos = -50; $new_pos = 0;     }
  elsif( $i == 6 ){ #case 2c
    $actual_pos = -50; $new_pos = 2000;  }

  #set 3 cases; P == 0;
  elsif( $i == 7 ){ #case 3a
    $actual_pos = 0; $new_pos = -2000; }
  elsif( $i == 8 ){ #case 3b
    $actual_pos = 0; $new_pos = 0;     }
  elsif( $i == 9 ){ #case 3c
    $actual_pos = 0; $new_pos = 2000;  }

  #set 4 cases; P > 0;
  elsif( $i == 10 ){ #case 4a
    $actual_pos = 1000; $new_pos = -2000; }
  elsif( $i == 11 ){ #case 4b
    $actual_pos = 1000; $new_pos = 0;     }
  elsif( $i == 12 ){ #case 4c
    $actual_pos = 1000; $new_pos = 2000; }

}#Endsub test_case_inputs


#-----
sub test_near_home_cases{

  for( my $i = 1; $i <= 13; $i++ ){
    test_near_home_case_inputs( $i );

    my ( $ar_home_and_move, $ar_motion_amount ) = main::build_near_home_and_move();
    my @home_and_move = @$ar_home_and_move;
    my @motion_amount = @$ar_motion_amount;
    
    $num_waits = $home_and_move[0];
    print "---------\nCase $i = $case\n";
    print "actual = $actual_pos; new = $new_pos; bl = $backlash\n";
    print "Number of wait = $home_and_move[0]\n";
    for(my $i = 1; $i <= $num_waits; $i++){
      print "$home_and_move[$i]; motion amount = $motion_amount[$i]\n";
    }
  }
}#End test_near_home_cases


sub test_near_home_case_inputs{
  my $i = $_[0];

  if   ( $i == 1 ){ #case 1ab
    $actual_pos = -1000; $new_pos = -2000; }

  elsif( $i == 2 ){ #case 1ab
    $actual_pos = -1000; $new_pos = -1000;     }

  elsif( $i == 3 ){ #case 1c
    $actual_pos = -1000; $new_pos = -500;  }

  elsif( $i == 4 ){ #case 2
    $actual_pos = -1000; $new_pos = 0; }

  elsif( $i == 5 ){ #case 3
    $actual_pos = -1000; $new_pos = 2000;     }

  elsif( $i == 6 ){ #case 4
    $actual_pos = 0; $new_pos = -2000;  }


  elsif( $i == 7 ){ #case 5
    $actual_pos = 0; $new_pos = 0; }

  elsif( $i == 8 ){ #case 6
    $actual_pos = 0; $new_pos = 2000;     }

  elsif( $i == 9 ){ #case 7
    $actual_pos = 1000; $new_pos = -2000;  }


  elsif( $i == 10 ){ #case 8
    $actual_pos = 1000; $new_pos = 0; }

  elsif( $i == 11 ){ #case 9ab
    $actual_pos = 2000; $new_pos = 1000;     }

  elsif( $i == 12 ){ #case 9ab
    $actual_pos = 2000; $new_pos = 2000;     }

  elsif( $i == 13 ){ #case 9c
    $actual_pos = 1000; $new_pos = 2000; }

}#Endsub test_near_home_case_inputs


sub move_and_wait_loop{
  my ($ar_home_and_move, $ar_motion_amount) = @_;
  my @home_and_move = @$ar_home_and_move;
  my @motion_amount = @$ar_motion_amount;
  
  my $wait = $home_and_move[0];
  
  for( my $which_wait = 1; $which_wait <= $wait; $which_wait++ ){
    my $timer = 0;
    my $sleep_time = 5; #seconds
    my $stationary_twice = 0;
    my $time_elapsed = 0;
    my $time_estimate = 
      compute_time_estimate($motion_amount[$which_wait], $sleep_time);

    my $motion_cmd = $MosWheel::cmd_head.$MosWheel::str_quote.
                     $home_and_move[ $which_wait ].$MosWheel::str_quote;

    print "****************************************************************\n";
    print "SHOULD BE EXECUTING:\n";
    print "Motion # $which_wait of $wait\n";
    print "$motion_cmd\n\n";
    my $reply = `$motion_cmd`;###This does the motion

    until( $stationary_twice == 2 or $time_elapsed == $time_estimate ){
      print "This should not take longer than ";
      printf "%d", $time_estimate;
      print " seconds.\n";

      print ">>Sleeping $sleep_time before checking status\n\n";
      sleep $sleep_time; $timer += $sleep_time;

      print "Have waited $timer seconds so far.\n\n";

      ($actual_pos,  $motion,
       $closest_pos, $closest_name, $difference) = MosWheel::get_mos_status();
      
      MosWheel::print_mos_info( $motor, $wheel, 
				$actual_pos, $closest_pos, $closest_name,
				$difference, $motion, $home_type );
      if( $motion =~ m/Stationary/ ){ $stationary_twice += 1; }

      if( $timer >= ($time_estimate + 2*$sleep_time) ){
	print "warning: time elapsed\n";die;
      }

    }#End until loop

  }#End for loop
  print "\t\t>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<\n";
  print "\t\t>>>>>Mos-Slit Wheel Motion Done<<<<<\n";
  print "\t\t>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<\n\n";
}#Endsub move_and_wait_loop


sub compute_time_estimate{
  my ($motion, $sleep_time) = @_;
  my $vel = $ENV{MOVE_V_1};
  my $full_turn = $MosWheel::MOS_SLIT_PN_REV;

  my $time = $vel * ( $motion / $full_turn );
  if( $time <= $sleep_time ){
    $time = $sleep_time;
  }

  return ($time + $sleep_time + 10);
}#Endsub compute_time_estimate


sub printout_position_headings{
  format PLATE_HEADINGS =
     Position                       Plate
  Number    Name                    Name
 ----------------------------------------------------------------
.

  #Print to stdout with same format
  my $old_stdout_format = $~;
  $~ = "PLATE_HEADINGS";
  write STDOUT;
  $~ = $old_stdout_format;

}#Endsub printout_positions


sub printout_position_info{
  my ($position_number, $position_name, $position_plate_name) = @_;

  format PLATE_INFO =
@##       @<<<<<<   @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
$position_number, $position_name, $position_plate_name
.

  #Print to stdout with same format
  my $old_stdout_format = $~;
  $~ = "PLATE_INFO";
  write STDOUT;
  $~ = $old_stdout_format;

}#Endsub printout_positions


sub my_select_new_position{
  my $wheel_name = "MOS_SLIT";
  my @POS_NAMES = @MOS_SLIT_PN_NAMES;
  my @POS_VALS  = @PN_MOS_SLIT;
  my $num_pos   = @POS_NAMES;
  my $num_pos0  = $num_pos - 1;

  my $new_pos_val = PI;#this is the value returned if choose not to change
  my $new_pos_num = 0;

  my $is_valid = 0;

  printout_position_headings();

  for( my $i = 0; $i < $num_pos; $i++ ){
    printout_position_info( $i, $POS_NAMES[$i], $mosplate_names_vector[$i] );
  }

  until( $is_valid ){
    print "\n";
    print "Select a position number: ";
    $new_pos_num = <STDIN>; chomp $new_pos_num;

    my $input_len = length $new_pos_num;
    if( $input_len == 0 ){
      print "Not changing $wheel_name position.\n";
      $is_valid = 1;
    }else{
      if( $new_pos_num !~ m/[a-zA-Z]/ ){
	if( $new_pos_num >= 0 and $new_pos_num < $num_pos ){
	  #print "if = $new_pos_num\n";
	  $new_pos_val = $POS_VALS[$new_pos_num];
	  $is_valid    = 1;
	}else{
	  print "Please enter a number between 0 and $num_pos0:\n";
	}
      }else{
	print "Please enter a number between 0 and $num_pos0:\n";
      }
    }
  }#End until is_valid
  my $new_plate_name  = $mosplate_names_vector[$new_pos_num];

  return ( $new_pos_val, $new_pos_num, $new_plate_name );
}#Endsub my_select_new_position


sub Load_Mos_Names{
  #one arg. is name of file containing FITS header, then
  # read header into arrays: 
  my @mosnames_params;
  my ( @mosnames_pvalues, @mosnames_comments );

  my $fitsheader;
  open( IN, $_[0] ) || die "cannot open $_[0]\n";
  while( <IN> ) { $fitsheader = $_; }
  close( IN );

  my $slen = length( $fitsheader );
  my $nrec = $slen/80;

  my @mosnames_records;
  for( my $i=0; $i<$nrec; $i++ ){
    push( @mosnames_records, substr( $fitsheader, $i*80, 80 ) );
  }

  #for( my $i=0; $i<$nrec; $i++){
  #  print "$i: $mosnames_records[$i]\n";
  #}

  my $nrecs = @mosnames_records;
  if( $nrec ne $nrecs ){
    print "number of records is not what was expected\n";
    print "expected=$nrec , obtained=$nrecs\n";
  }

  my $loceq = index($mosnames_records[0],'=');

  for( my $key = 0; $key < $nrecs; $key++ ){
    my $record = $mosnames_records[$key];
    my $loccom = index($mosnames_records[$key],' / ');

    my $param_keyword = substr( $record, 0, $loceq );
    my $param_value   = substr( $record, $loceq+2, $loccom-$loceq-2);
    my $param_comment = substr( $record, $loccom );

    #print "record : $record\n";
    #print "loccom : $loccom\n";
    #print "keyword: $param_keyword\n";
    #print "value  : $param_value\n";
    #print "comment: $param_comment\n";

    push( @mosnames_params,   $param_keyword );
    push( @mosnames_pvalues,  $param_value );
    push( @mosnames_comments, $param_comment );
  }

  my @mosplate_keys_vector;
  my @mosplate_names_vector;
  for( my $i = 0; $i < 18; $i++ ){
    $mosplate_keys_vector[$i] = " ";
    $mosplate_names_vector[$i] = " ";
    my $mos_id = "MOS_".$i."_";

    foreach my $record (@mosnames_params){
      if( $record =~ $mos_id ){
	for( my $j = 0; $j < $nrecs; $j++ ){
	  $mosplate_keys_vector[$i] = $record;

	  if( $mosnames_params[$j] eq $record ){
	    $mosplate_names_vector[$i] = $mosnames_pvalues[$j];
	  }
	}
      }
    }
  }

  my $img_id = "IMG_APER";
  for( my $k = 0; $k < $nrecs; $k++){
    if( $mosnames_params[$k] =~ $img_id ){ 
      $mosplate_keys_vector[$k] = $img_id;
      $mosplate_names_vector[$k] = $mosnames_pvalues[$k];
    }
  }


  $mosplate_names_vector[1] = "'None                                     '";
  $mosplate_names_vector[3] = "'None                                     '";
  $mosplate_names_vector[5] = "'None                                     '";
  $mosplate_names_vector[7] = "'None                                     '";
  $mosplate_names_vector[9] = "'None                                     '";
  $mosplate_names_vector[11] = "'None                                     '";

  #for( my $i = 0; $i < 18; $i++ ){
  #  print "$i: $mosplate_keys_vector[$i]:$mosplate_names_vector[$i]:\n";
  #}

  return \@mosplate_names_vector;
}#Endsub Load_Mos_Names


sub my_test_print_mos_info{
  my ( $motor, $wheel, $actual_pos, $closest_pos, $closest_name, 
       $difference, $motion, $home_type, $closest_likely_plate_name ) = @_;


  my $this_home_type;
  if( $home_type == 0 ){
    $this_home_type = "Near-home\n";
  }elsif( $home_type == 1 ){
    $this_home_type = "Limit-switch\n";
  }


  format HEADING_1 =
>>>>>>>>>>>>>>>>>>>>>>> PRESENT MOS-SLIT WHEEL STATUS <<<<<<<<<<<<<<<<<<<<<<<
                    CLOSEST
          ACTUAL    LIKELY
 MOTOR   POSITION   POSITION     DIFFERENCE       MOTION           HOME-TYPE
_____________________________________________________________________________
   @<    @<<<<<<    @<<<<<<<<    @<<<<<<<      @<<<<<<<<<<<<    @<<<<<<<<<<<<
$motor, $actual_pos, $closest_pos, $difference, $motion, $this_home_type
.

  format HEADING_2 =
      CLOSEST LIKELY   CLOSEST_LIKELY
      POSITION NAME    MOS PLATE NAME
      _____________________________________________________________
      @<<<<<<<         @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
      $closest_name, $closest_likely_plate_name
_____________________________________________________________________________
.

  #Print to stdout with same format
  my $old_stdout_format = $~;
  $~ = "HEADING_1";
  write STDOUT;
  $~ = $old_stdout_format;
  print "\n";
  $~ = "HEADING_2";
  write STDOUT;
  $~ = $old_stdout_format;

}#Endsub my_test_print_mos_info


sub my_test_print_mos_selection{
  my ( $closest_name, $closest_likely_plate_name, 
       $new_name, $new_pos, $new_plate_name) = @_;

  format WHEEL_CHANGE_1 =
             POSITION    MOS-PLATE NAME,                           WHEEL POSITION
             NAME        IF ANY                                        VALUE
  -------------------------------------------------------------------------------
  MOVE FROM: @<<<<<<<   @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  $closest_name, $closest_likely_plate_name
.

  format WHEEL_CHANGE_2 =
  MOVE TO:   @<<<<<<<   @<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< @<<<<<<<
  $new_name, $new_plate_name, $new_pos
.

  #Print to stdout with same format
  my $old_stdout_format = $~;
  $~ = "WHEEL_CHANGE_1";
  write STDOUT;
  $~ = "WHEEL_CHANGE_2";
  write STDOUT;
  $~ = $old_stdout_format;

}#Endsub my_test_print_mos_seletction


sub set_plate_name_in_header{
  my $iparm = param_index( $_[0] );

  my $max_length_plate_name = 41;

  if( $iparm < @fh_pvalues ){
    my $new_pvalue = $_[1];

    if( defined $new_pvalue ){
      $fh_pvalues[$iparm] = $new_pvalue;
    }
  }else{
    print "$_[0] not in FITS header\n";
  }
} #Endsub set_plate_name_in_header


sub set_plate_name_in_selected_fragment{
  my $selected_plate_name = $_[0];

  my $mos_name_fitsfragment;
  if( !$DEBUG ){
    $mos_name_fitsfragment = $ENV{FITS_HEADER_LUT}.$ENV{MOS_SELECTED_NAME};
  }else{
    $mos_name_fitsfragment = $ENV{FITS_HEADER_LUT_DEBUG}.$ENV{MOS_SELECTED_NAME};
  }

  my $fitsfragment;
  open( IN, $mos_name_fitsfragment ) || die "Cannot open $mos_name_fitsfragment\n";
  while( <IN> ){ $fitsfragment = $_; }
  close IN;

  my $slen = length( $fitsfragment );
  my $nrec = $slen/80;

  my @fitsfragment_records;
  for( my $i=0; $i<$nrec; $i++ ){
    push( @fitsfragment_records, substr( $fitsfragment, $i*80, 80 ) );
  }

  #for( my $i=0; $i<$nrec; $i++){
  #  print "$i: $fitsfragment_records[$i]\n";
  #}

  my $iparm = param_index( "MOS_NAME" );
  my $max_length_plate_name = 41;



}#Endsub set_plate_name_in_selected_fragment

__END__

=head1 NAME

config.mos.wheel.pl

=head1 DESCRIPTION

Use to move the Mos wheel only in FLAMINGOS-1.

=head1 REVISION & LOCKER

$Name:  $

$Id: config.mos.wheel.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $

=head1 REQUIRES

All of the other original FLAMINGOS-1 perl scripts & modules.

=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut

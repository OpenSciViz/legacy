#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


###############################################################################
#
#Configure fits header parameters for next exposure
#exp_time, and nreads
#
##############################################################################
use strict;
use Getopt::Long;
use GetYN;
use ImgTests;
use Fitsheader qw/:all/;

my $DEBUG = 0;#0 = not debugging; 1 = debugging
GetOptions( 'debug' => \$DEBUG );

my $header = select_header($DEBUG);

Find_Fitsheader($header);

#readFitsHeader is exported from Fitsheader.pm
#and loads the following arrays from the input header
#@fh_params @fh_pvalues @fh_comments
readFitsHeader($header);

#print "\n>>>         Present exposure parameters are:                    <<<\n";
#print "_____________________________________________________________________\n";
#printFitsHeader( "EXP_TIME",  "NREADS");

my $didmod = 0;
my $redo = 0;
my $change_bias = 0;

#print "\n\nChange any parameter? ";
#my $change = get_yn();
#if( $change == 0 ){
#  die "Exiting\n\n";
#}

until( $redo ){
  my $expt = set_expt();
  if( $expt > 0 ){
    setNumericParam("EXP_TIME", $expt);
    $didmod += 1;
  }

  my $nreads ;
  if( $expt > 0 ){
    $nreads = set_nreads($expt);
  }else{
    my $unchanged_expt = param_value('EXP_TIME');
    #print "Unchanged expt is $unchanged_expt\n";
    $nreads = set_nreads($unchanged_expt);
  }

  #print "set_nreads returned $nreads\n";
  if( $nreads > 0 ){
    setNumericParam("NREADS", $nreads);
    $didmod += 1;
  }

  set_CT();

  if( $didmod > 0 ){
    print "\n\nThe new set of exposure parameters are:\n";
    printFitsHeader( "EXP_TIME",  "NREADS" );

    print "\nAccept changes?";
    $redo = get_yn();
  }else{
    $redo = 1;
  }
}


if( $didmod > 0 ){
  if( !$DEBUG ){

    writeFitsHeader( $header );

    #my $dothis1 = "cp -p $fitsheader_file /data1/flamingos ";
    #my $dothis2 = "cp -p $fitsheader_file /data2/flamingos ";
    #system($dothis1);
    #print "Copying FITS header to /data1/flamingos \n";
    #system($dothis2);
    #print "Copying FITS header to /data2/flamingos \n";

  }elsif( $DEBUG ){
    #print "\nUpdating FITS header in: $header ...\n";
    writeFitsHeader( $header );
  }
}elsif( $didmod == 0 ){
  print "\nNo changes made to default header.\n\n";
}

#Final print
print "\n";
###End of main

###subs
sub set_expt{
  my $iparm = param_index( 'EXP_TIME' );
  #print "exptime index is $iparm\n";
  print $fh_comments[$iparm] . "\n";

  my $response = 0;
  my $is_valid = 0;

  until($is_valid){
    print $fh_params[$iparm ]. '= ' . $fh_pvalues[$iparm] . ' ? ';
    my $new_value = <STDIN>;
    chomp( $new_value );
    #print "Chomped input is $new_value\n";

    my $input_len = length $new_value ;
    if( $input_len > 0 and $input_len <= 18 ){

      if( $new_value =~ /\D+/ ){
	print "Please enter an integer number of seconds\n";
      }elsif( $new_value >= 2 ){
	$response = $new_value;
	$is_valid = 1;
      }else{
	print "Please enter an exposure time >= 2 seconds\n";
      }

    }elsif( $input_len >= 18 ){
      print "Enter an exposure time less than 18 characters long\n";
    }else{
      #print "Leaving EXP_TIME unchanged\n";
      $is_valid = 1;
    }
  }
  print "\n";
  return $response;
}#Endsub set_expt


sub set_nreads{
  my $expt = $_[0];
  #print "Input expt is $expt\n";
  my $iparm = param_index( 'NREADS' );
  print $fh_comments[$iparm] . "\n";
  my $unchanged_nreads = $fh_pvalues[$iparm];
  #print "my unchanged nreads is $unchanged_nreads\n";

  my $response = 0;
  my $is_valid = 0;

  until($is_valid){
    print $fh_params[$iparm ]. '= ' . $fh_pvalues[$iparm] . ' ? ';
    my $new_value = <STDIN>;
    chomp( $new_value );
    #print "Chomped input is $new_value\n";

    my $input_len = length $new_value ;
    #print "input len is $input_len\n";
    if( $input_len > 0 and $input_len <= 18 ){

      if( $new_value =~ /\D+/ ){
	print "Please enter an integer number of reads\n";
      }elsif( $new_value <= $expt/2 ){
	#print ">expt is $expt; nreads is $new_value\n";
	$response = $new_value;
	$is_valid = 1;
      }else{
	print "Please enter an integer number of reads <= EXP_TIME/2\n";
      }

    }elsif( $input_len >= 18 ){
      print "Enter an exposure time less than 18 characters long\n";
    }elsif( $input_len == 0  ){
	#print "nreads was unchanged; check if allowed by expt\n";
	if( $unchanged_nreads > $expt/2 ){
	  print "Please enter an integer number of reads <= EXP_TIME/2\n";
	}
	$is_valid = 1;
    }else{
      #print "Leaving NREADS unchanged\n";
      $is_valid = 1;
    }
  }
  return $response;
}#Endsub set_nreads


sub set_CT{
  my $nreads = param_value('NREADS');
  #my $nreads = get_unchanged_param('NREADS');
  #print "nreads in set_ct is $nreads\n";
  if( $nreads == 1 ){
    setNumericParam("CYCLETYP", 40);
  }elsif( $nreads > 1 ){
    setNumericParam("CYCLETYP", 42);
  }else{
    print ">>>>> WARNING   WARNING <<<<<\n";
    print "Unable to set CYCLETYP in Fitsheader\n";
    print "nreads is not valid:  $nreads\n\n";
}

}#Endsub set_CT

__END__

=head1 NAME

set.exposuretime.pl

=head1 DESCRIPTION

For FLAMINGOS-1.  Sets the exposure time and number of reads.

=head1 REVISION & LOCKER

$Name:  $

$Id: set.exposuretime.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $

=head1 REQUIRES

All of the other original FLAMINGOS-1 perl scripts & modules.

=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut

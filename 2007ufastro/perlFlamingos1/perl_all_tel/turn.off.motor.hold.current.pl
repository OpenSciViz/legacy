#!/usr/local/bin/perl -w
# Set the hold current to 0 for the camera motors

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;
use AllbutMosWheel qw/:all/;

my $ampersand = " & ";
my $quote  = "\"";

my $c_y_cmd = $ENV{NOT_MOVING_CURRENT_C};
my $d_y_cmd = $ENV{NOT_MOVING_CURRENT_D};
my $e_y_cmd = $ENV{NOT_MOVING_CURRENT_E};

my $all_y_cmd = $c_y_cmd.$ampersand.$d_y_cmd.$ampersand.$e_y_cmd;
my $net_cmd = $AllbutMosWheel::cmd_head.$quote.$all_y_cmd.$quote;

print "\n\n";
print "Setting the hold current to the camera motors to zero:\n";
print "$net_cmd\n\n";

my $reply = `$net_cmd`;

print "\n***>>> DONE <<<***\n\n";


__END__

=head1 NAME

turn.off.motor.hold.current.pl

=head1 DESCRIPTION

For FLAMINGOS-1.  Sends commands to the motor controllers for
the filter, lyot, and grism wheels to turn off the hold current on 
the motors.

Run this script on the command line, or through the engineering
pulldown on the UFSTATUS_GUI, to turn off the hold current if you
for some reason kill the config.filter.grism.decker.wheels.pl script
and do not also cycle the power on the motor controller box itsel.

=head1 REVISION & LOCKER

$Name:  $

$Id: turn.off.motor.hold.current.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $

=head1 REQUIRES

All of the other original FLAMINGOS-1 perl scripts & modules.

=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut

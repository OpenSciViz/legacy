package RW_Header_Params;

require 5.005_62;
use strict;
use warnings;

require Exporter;
use AutoLoader qw(AUTOLOAD);

our @ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use RW_Header_Params ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.
our %EXPORT_TAGS = ( 'all' => [ qw(
				   &get_telescope_and_use_tcs_params
				   &get_expt_params
				   &get_dither_params
				   &get_mos_dither_params
				   &get_MMT_mos_dither_params
				   &get_filename_params
				   &write_filename_header_param
				   &write_rel_offset_to_header
) ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(
	
);

use vars qw/ $VERSION $LOCKER /;
'$Revision: 0.1 $ ' =~ /.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ ' =~ /.*:\s(.*)\s\$/ && ($LOCKER = $1);


# Preloaded methods go here.
sub get_dither_params{
  #my $dpattern =  'pat_2x2           ';
  #my $startpos = 0;

  my $dpattern = Fitsheader::param_value( 'DPATTERN' );#general pattern
  my $startpos = Fitsheader::param_value( 'STARTPOS' );

  my $default_dither_scale = $ENV{DEFAULT_DITHER_SCALE};
  my $d_scale  = Fitsheader::param_value( 'D_SCALE'  );
  my $net_scale = $default_dither_scale * $d_scale;

  my $d_rptpos = Fitsheader::param_value( 'D_RPTPOS' );
  my $d_rptpat = Fitsheader::param_value( 'D_RPTPAT' );#$d_rptpat= 2;

  my $usenudge = Fitsheader::param_value( 'USENUDGE' );
  my $nudgesiz = Fitsheader::param_value( 'NUDGESIZ' );

  return( $dpattern, $startpos, $net_scale, $d_rptpos, $d_rptpat, $usenudge, $nudgesiz );
}#Endsub get_dither_params


sub get_mos_dither_params{
  my $m_rptpat                = Fitsheader::param_value( 'M_RPTPAT' );
  my $m_ndgsz                 = Fitsheader::param_value( 'M_NDGSZ' );
  my $m_throw                 = Fitsheader::param_value( 'M_THROW' );
  my $usemnudg                = Fitsheader::param_value( 'USEMNUDG' );
  my $chip_pa_on_sky_for_rot0 = Fitsheader::param_value( 'CHIP-PA' );
  my $pa_rotator              = Fitsheader::param_value( 'ROT_PA' );
  my $pa_slit_on_chip         = Fitsheader::param_value( 'SLIT-CPA' );

  my $chip_pa_on_sky_this_rotator_pa = $pa_rotator + $chip_pa_on_sky_for_rot0;

  return( $m_rptpat, $m_ndgsz, $m_throw, $usemnudg,
          $chip_pa_on_sky_for_rot0, $pa_rotator,
	  $pa_slit_on_chip,
          $chip_pa_on_sky_this_rotator_pa );
}#Endsub get_mos_dither_params


sub get_MMT_mos_dither_params{
  my $m_rptpat        = Fitsheader::param_value( 'M_RPTPAT' );
  my $m_ndgsz         = Fitsheader::param_value( 'M_NDGSZ' );
  my $m_throw         = Fitsheader::param_value( 'M_THROW' );
  my $usemnudg        = Fitsheader::param_value( 'USEMNUDG' );

  my $chip_pa_for_pa0 = Fitsheader::param_value( 'CHIP-PA' );
  my $slit_pa_on_chip = Fitsheader::param_value( 'SLIT-CPA' );

  return( $m_rptpat,
	  $m_ndgsz,
	  $m_throw,
	  $usemnudg,
          $chip_pa_for_pa0,
	  $slit_pa_on_chip);
}#Endsub get_MMT_mos_dither_params


sub get_telescope_and_use_tcs_params{
  my $use_tcs  = Fitsheader::param_value( 'USE_TCS' );
  my $telescop = Fitsheader::param_value( 'TELESCOP' );
  my $tlen     = length $telescop;
  print "\n";
  print "telescop is $telescop, len is $tlen\n";
  print "\n";
  #print "viido = $VIIDO\n";

  return( $use_tcs, $telescop );
}#Endsub get_telescope_and_use_tcs_params


sub get_expt_params{
  #>>> Read necessary header params for exposure      <<<
  #>>> And make the necessary additions/concatonations<<<

  my $expt            = Fitsheader::param_value( 'EXP_TIME' );
  my $nreads          = Fitsheader::param_value( 'NREADS'   );
  my $extra_timeout   = $ENV{EXTRA_TIMEOUT};
  my $net_edt_timeout = $expt + $nreads  + $extra_timeout;

  return( $expt, $nreads, $extra_timeout, $net_edt_timeout );
}#Endsub get_expt_params


sub get_filename_params{
  #>>> Read necessary header params for file name
  #>>> And make concatenated filename hint.

  my $orig_dir  = Fitsheader::param_value( 'ORIG_DIR' );
  my $filebase  = Fitsheader::param_value( 'FILEBASE' );
  my $file_hint = $orig_dir . $filebase;

  return( $orig_dir, $filebase, $file_hint );
}#Endsbu get_filename_params


sub write_filename_header_param{
  my ( $file_base, $this_index, $header ) = @_;
  my $index_sep = $ENV{DESIRED_INDEX_SEPARATOR};
  my $file_ext  = $ENV{FITS_SUFFIX};

  my $index_str = ImgTests::parse_index( $this_index );
  my $filename = $file_base.$index_sep.$index_str.$file_ext;

  print "setting FILENAME to $filename\n";
  ImgTests::setFilenameParam( $filename );
  Fitsheader::writeFitsHeader( $header );

}#Endsub write_filename_header_param


sub write_rel_offset_to_header{
  my ( $header, $xoffset, $yoffset ) = @_;

  Fitsheader::setNumericParam( 'XOFFSET', $xoffset );
  Fitsheader::setNumericParam( 'YOFFSET', $yoffset );

  #print "SHOULD BE: ";
  #print "\nWriting XOFFSET = $xoffset; YOFFSET = $yoffset to header\n";
  print "\nWriting XOFFSET = $xoffset; YOFFSET = $yoffset to header\n";
  Fitsheader::writeFitsHeader( $header );
  print "\n\n";

}#Endsub write_rel_offset_to_header


# Autoload methods go after =cut, and are processed by the autosplit program.

1;
__END__
# Below is stub documentation for your module. You better edit it!

=head1 NAME

RW_Header_Params - Perl extension for Flamingos data acquisition

=head1 SYNOPSIS

  use RW_Header_Params qw/:all/;

=head1 DESCRIPTION

  Read and Write various header parameters


=head2 EXPORT


=head1 REVISION & LOCKER

$Name:  $

$Id: RW_Header_Params.pm,v 0.1 2003/05/22 15:29:27 raines Exp $

$Locker:  $


=head1 AUTHOR

S. N. Raines, 18 Jan 2002

=head1 SEE ALSO

perl(1).

=cut

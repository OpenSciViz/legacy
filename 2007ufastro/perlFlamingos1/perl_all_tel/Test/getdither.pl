#!/usr/local/bin/perl -w

use strict;
use Dither_patterns qw/:all/;


print "scales\n$scale_gem2by2\n$scale_gem\n$scale_mmt\n$scale_4m\n$scale_2m\n";

my $num_pts = @pat_5x5_X;
my @x_pos = @pat_5x5_X;
my @y_pos = @pat_5x5_Y;

my @scales = ( $scale_gem2by2, $scale_gem, $scale_mmt, $scale_4m, $scale_2m );
my $num_scales = @scales;

print "num_pts, num_scales = $num_pts, $num_scales\n";
my $xp; my $yp;
for( my $i = 0; $i < $num_pts; $i++){
  if( $i == 0 ){
      print "               x\ty\tx\ty\tx\ty\tx\ty\tx\ty\n";
  }
  print "Position $i:  ";
  for( my $j = 0; $j < $num_scales; $j++ ){
    $xp = $x_pos[$i] * $scales[$j];
    $yp = $y_pos[$i] * $scales[$j];
    #print  "$xp, $yp; ";
    printf "%+2.2d", $xp; printf "\t"; printf "%+2.2d", $yp; printf "\t";
  }
  print "\n";
}

#rcsId = q($Name:  $ $Id: getdither.pl 14 2008-06-11 01:49:45Z hon $);

#!/usr/local/bin/perl -w

use strict;

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";

print "\n";
print "Compute the center of rotation on the array, given the centroid postions\n";
print "of the same star taken at three diferent rotator angles.\n";
print "Note this will only work if the star is imaged at three positions\n";
print "cover an included angle _greater_ than 180 degrees.\n";
print "\n";
print "The algorithm came from http://mathworld.wolfram.com/Circle.html\n";
print "\n";

print "Enter the coordinates of the star at the first position:\n";
my ($x1, $y1) = input_coords();

print "Enter the coordinates of the star at the second position:\n";
my ($x2, $y2) = input_coords();

print "Enter the coordinates of the star at the third position:\n";
my ($x3, $y3) = input_coords();

print "x1, y1 = $x1, $y1\n";
print "x2, y2 = $x2, $y2\n";
print "x3, y3 = $x3, $y3\n";

my $sq_1 = $x1*$x1 + $y1*$y1;
my $sq_2 = $x2*$x2 + $x2*$x2;
my $sq_3 = $x3*$x3 + $x3*$x3;


my $a = compute_a();
my $d = compute_d();
my $e = compute_e();
my $f = compute_f();

print "a = $a\n";
print "d = $d\n";
print "e = $e\n";
print "f = $f\n";


my $x_center = -1 * $d / (2 * $a );
   $x_center = sprintf "%0.3f", $x_center;

my $y_center = -1 * $e / (2 * $a );
   $y_center = sprintf "%0.3f", $y_center;

my $radius_term1 = ($d*$d + $e*$e)/ (4 * $a*$a);
my $radius_term2 = $f / $a;

my $radius = sqrt( $radius_term1 - $radius_term2 );
   $radius = sprintf "%0.3f", $radius;

print "The center of rotation should be at:\n";
print "x_center = $x_center\n";
print "y_center = $y_center\n";
print "\n";
print "The radius of the circle defined by these\n";
print "three positions is $radius.\n";
print "\n";

#SUBS
sub compute_a{
  my $a;

  $a =  $x1 * ($y2     - $y3    )
       -$y1 * ($x2     - $x3    )
       +  1 * ($x2*$y3 - $y2*$x3);

  return $a;
}#Endsub compute_a


sub compute_d{
  my $d;

  $d =   $sq_1 * ($y2         - $y3         )
       - $y1   * ($sq_2       - $sq_3       )
       + 1     * ($sq_2 * $y3 - $y2   * $sq_3);

  return (-1*$d);
}#Endsub compute_d


sub compute_e{
  my $e;

  $e =  $sq_1 * ($x2         - $x3         )
       -  $x1 * ($sq_2       - $sq_3       )
       +    1 * ($sq_2 * $x3 - $x2 * $sq_3 );

  return $e;
}#Endsub compute_e


sub compute_f{
  my $f;

  $f =   $sq_1 * ($x2   * $y3 - $y2 *$x3  )
       - $x1   * ($sq_2 * $y3 - $y2 *$sq_3)
       + $y1   * ($sq_2 * $x3 - $x2 *$sq_3);

  return (-1*$f);
}#Endsub compute_f


sub input_coords{

  my $x_response = 0;
  my $y_response = 0;

  my $is_valid = 0;
  until($is_valid){
    print "Enter the x coordinate: ";

    $x_response = <STDIN>;
    chomp( $x_response );

    if( $x_response =~ m/[a-z,A-Z]/ ){
      print "Please enter a real value.\n";

    }elsif( $x_response =~ m/^\.$/ ){
      print "Please enter a real value.\n";

    }else{
      if( $x_response =~ m/\d{0}\.\d{1}/ || $x_response =~ m/\d{1}/ ){
	#look for matches with (.#, .##, #.#, #.##) or (#., #.#, #.##) or (0) or (1)
	$is_valid = 1;
      }
    }
  }

  $is_valid = 0;
  until($is_valid){
    print "Enter the y coordinate: ";

    $y_response = <STDIN>;
    chomp( $y_response );

    if( $y_response =~ m/[a-z,A-Z]/ ){
      print "Please enter a real value.\n";

    }elsif( $y_response =~ m/^\.$/ ){
      print "Please enter a real value.\n";

    }else{
      if( $y_response =~ m/\d{0}\.\d{1}/ || $y_response =~ m/\d{1}/ ){
	#look for matches with (.#, .##, #.#, #.##) or (#., #.#, #.##) or (0) or (1)
	$is_valid = 1;
      }
    }
  }

  my $formated_x = sprintf "%0.3f", $x_response;
  my $formated_y = sprintf "%0.3f", $y_response;
  print "\n";
  return ($formated_x, $formated_y);
}#Endsub input_coords


__END__

=head1 NAME

center.of.rotation.large.angle.pl

=head1 DESCRIPTION

Take an image of the same star with the telescope at three
different Position Angles.  Measure the centroids, and input
them to this script to get the two possible solutions for
the center of rotation.  It tells you which solution it thinks
is valid.

=head1 REVISION & LOCKER

$Name:  $

$Id: center.of.rotation.large.angle.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $

=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut

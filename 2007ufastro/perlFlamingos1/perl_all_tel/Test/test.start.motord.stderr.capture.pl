#!/usr/local/bin/perl -w

use strict;

my $xtp       = "xterm -fn 6x12 -sb -sl 2000 ";

#my $ls208_str = ($xtp . "-n TEMP -T TEMP -e " .
#		 "$ENV{UFLS208D} -chan \"1, 2, 6\" " .
#		 "$ENV{DAEMON_TSHOST} $ENV{IOCOMM} " .
#		 "$ENV{DAEMON_TSPORT} $ENV{UFLS208D_PORT} &"
#		  );


my $motor_str = ($xtp . "-iconic -n MOTOR -T MOTOR -e " .
		 "$ENV{SH_MOTORD} /tmp/xt_motor.txt &"
		);

my $mce4_str = ($xtp . "-n MCE4 -T MCE4 -e " .
		"$ENV{SH_MCE4D} /tmp/xt_mce4.txt &"
	       );



my $motord_cmd = "/bin/sh -c ". q(xterm -fn 6x12 -sb -sl 2000 -n MOTOR -T MOTOR_DEMON -e '/usr/local/uf/bin/ufgmotord -noepics -tshost iocomm -tsport 7004 -motors "a b c d e" 2>&1');


print "motord_cmd = $motord_cmd\n";


my $reply = `$motord_cmd`;
print "\nreply = $reply\n";

#rcsId = q($Name:  $ $Id: test.start.motord.stderr.capture.pl 14 2008-06-11 01:49:45Z hon $);

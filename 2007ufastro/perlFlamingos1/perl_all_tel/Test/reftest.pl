#!/usr/local/bin/perl -w

use strict;

my $main_strvar = "in main";
my $second_str = "second string";
print $main_strvar . "\n" . $second_str . "\n\n";

my $sub_ret = pbval_sub($main_strvar, $second_str);
print "sub_ret pbval is $sub_ret; $main_strvar, $second_str\n\n";

$sub_ret = pbref_sub(\$main_strvar, \$second_str);
print "sub_ret pbref is $sub_ret, and main_strvar is $main_strvar\n\n";




###End main


sub pbval_sub{
  my ($incoming, $instr2) = @_;
  print "pbval_sub gets $incoming and $instr2\n";

  $incoming = "this last variable will be returned by default from pbval_sub\n" . "and the original variable will not be changed";
}#endsub pbval_sub


sub pbref_sub{
  my ($stref, $stref2) = @_;
  my $incoming = $$stref;
  my $in2      = $$stref2;
  print "pbref_sub gets $incoming and $in2\n";
  $incoming = "changing incoming will not the source variable ";
  $$stref = "but this will change it";
}#endsub pbref_sub



#rcsId = q($Name:  $ $Id: reftest.pl 14 2008-06-11 01:49:45Z hon $);

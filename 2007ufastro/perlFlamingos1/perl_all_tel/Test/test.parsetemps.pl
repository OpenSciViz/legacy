#!/usr/local/bin/perl -w

use strict;

my $input = "(2002:087:20:01:47.850127) 1,01.09 2, OL   6, OL";

print "input is $input\n";

my $endpar = index $input, ")";
my $data = substr $input, ($endpar+1);
print "data =:$data\n";

my $ind1 = index $data, "1,";
my $ind2 = index $data, "2,";
my $ind6 = index $data, "6,";

my $start1_val = $ind1+2;
my $start2_val = $ind2+2;
my $start6_val = $ind6+2;
print "start1 = $start1_val\n";
print "start2 = $start2_val\n";
print "start6 = $start6_val\n";

my $inval1 = substr $data, $start1_val, ($ind2-$start1_val);
my $inval2 = substr $data, $start2_val, ($ind6-$start2_val);
my $inval6 = substr $data, $start6_val;

print "inval1 = $inval1\n";
print "inval2 = $inval2\n";
print "inval6 = $inval6\n";

if( $inval1 =~ /01\./ ){
  print "inval1 matches 01.0#\n";
}

if( $inval2 =~ /OL/ ){
  print "inval2 matches OL\n";
}

#rcsId = q($Name:  $ $Id: test.parsetemps.pl 14 2008-06-11 01:49:45Z hon $);

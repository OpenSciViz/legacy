#!/usr/local/bin/perl -w

use strict;
use MosWheel qw/:all/;


my $cnt = @ARGV;

if( $cnt < 1 ){ #assume -help
  die "\n\tUSAGE:\n".
      "\n\ttweak.mos.pl (+|-)offset".
      "\n\teg, tweak.mos.pl +5, or tweak.mos.pl -5\n\n";
}
else{
  my $offset = shift;

  print "Offset by $offset\n";

}




#rcsId = q($Name:  $ $Id: tweak.mos.pl 14 2008-06-11 01:49:45Z hon $);

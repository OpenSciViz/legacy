#!/usr/local/bin/perl -w

use strict;

my $str_quote           = "\"";
my $space               = " ";
my $cmd_head = $ENV{UFMOTOR}.$space.
               $ENV{CLIENT_HOST}.$space.$ENV{HOST}.$space.
	       $ENV{CLIENT_PORT}.$space.$ENV{CLIENT_UFMOTOR_PORT}.$space.
               $ENV{CLIENT_QUIET}.$space;

#print "Try getting all 5 at once\n";
#my $cmd_str = $cmd_head . $str_quote."aZ & bZ & cZ & dZ & eZ".$str_quote;
#print "Executing\n$cmd_str\n";
#my $reply = `$cmd_str`;
#sleep 1.0;
#print "\n\nReply \n$reply\n";

my $reply = "aZ 2250.00

bZ 93750.00

cZ 1042.00

dZ 657.00

eZ 250.00


";
print "Reply is\n$reply\n";

my $a_index = index $reply, 'aZ';
my $b_index = index $reply, 'bZ';
my $c_index = index $reply, 'cZ';
my $d_index = index $reply, 'dZ';
my $e_index = index $reply, 'eZ';

my $l_index = length $reply;

print "a at $a_index\n".
      "b at $b_index\n".
      "c at $c_index\n".
      "d at $d_index\n".
      "e at $e_index\n";


my $a_substr = substr $reply, $a_index, $b_index - $a_index;
my $b_substr = substr $reply, $b_index, $c_index - $b_index;
my $c_substr = substr $reply, $c_index, $d_index - $c_index;
my $d_substr = substr $reply, $d_index, $e_index - $d_index;
my $e_substr = substr $reply, $e_index, $l_index - $e_index;


my $a_dot = index $a_substr, "\.";
my $a_val = substr $a_substr, 2, $a_dot - 2;

my $b_dot = index $b_substr, "\.";
my $b_val = substr $b_substr, 2, $b_dot -2;

my $c_dot = index $c_substr, "\.";
my $c_val = substr $c_substr, 2, $c_dot -2;

my $d_dot = index $d_substr, "\.";
my $d_val = substr $d_substr, 2, $d_dot -2;

my $e_dot = index $e_substr, "\.";
my $e_val = substr $e_substr, 2, $e_dot -2;

print "a substr is $a_substr\n";
print "b substr is $b_substr\n";
print "c substr is $c_substr\n";
print "d substr is $d_substr\n";
print "e substr is $e_substr\n";

print "a_val, up to decimal point, is $a_val\n";
print "b_val, up to decimal point, is $b_val\n";
print "c_val, up to decimal point, is $c_val\n";
print "d_val, up to decimal point, is $d_val\n";
print "e_val, up to decimal point, is $e_val\n";

#rcsId = q($Name:  $ $Id: motortest.pl 14 2008-06-11 01:49:45Z hon $);

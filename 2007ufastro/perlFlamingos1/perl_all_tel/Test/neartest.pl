#!/usr/local/bin/perl -w

use strict;

use Wheel qw/:all/;

#'Wheel'->require_version(0.01);
#print "Wheel constant is " . Wheel::PI . "\n";
#print "Wheel constant is " . PI . "\n";

my @motors    = qw( a b c d e );
my @wheels    = qw( decker mos_or_slit filter lyot grism );
#The following arrays will have 5 entries that correspond to @wheels
my ( @actual_pos, @closest_pos, @closest_name, @difference, @motion );

@actual_pos = qw( 4520 -37918 1042 545 1000 );

#Decker 
( $closest_pos[0], $closest_name[0], $difference[0] ) =
  det_nearest_posn( $actual_pos[0], 
		    \@DECKER,       \@NEG_DECKER,
		    \@DECKER_NAMES, \@DECKER_NEG_NAMES,
		    \@DECKER_MDPTS, \@DECKER_NEG_MDPTS, $DECKER_REV );

##Mos_slit
#( $closest_pos[1], $closest_name[1], $difference[1] ) =
#  det_nearest_posn( $actual_pos[1], 
#		    \@MOS_SLIT,       \@NEG_MOS_SLIT,
#		    \@MOS_SLIT_NAMES, \@MOS_SLIT_NEG_NAMES,
#		    \@MOS_SLIT_MDPTS, \@MOS_SLIT_NEG_MDPTS, $MOS_SLIT_REV );

#Mos_slit
( $closest_pos[1], $closest_name[1], $difference[1] ) =
  det_mos_nearest_posn( $actual_pos[1] );


#Filter
( $closest_pos[2], $closest_name[2], $difference[2] ) =
  det_nearest_posn( $actual_pos[2], 
		    \@FILTER,       \@NEG_FILTER,
		    \@FILTER_NAMES, \@FILTER_NEG_NAMES,
		    \@FILTER_MDPTS, \@FILTER_NEG_MDPTS, $FILTER_REV );


#Lyot 
( $closest_pos[3], $closest_name[3], $difference[3] ) =
  det_nearest_posn( $actual_pos[3], 
		    \@LYOT,       \@NEG_LYOT,
		    \@LYOT_NAMES, \@LYOT_NEG_NAMES,
		    \@LYOT_MDPTS, \@LYOT_NEG_MDPTS, $LYOT_REV );

#Grism 
( $closest_pos[4], $closest_name[4], $difference[4] ) =
  det_nearest_posn( $actual_pos[4], 
		    \@GRISM,       \@NEG_GRISM,
		    \@GRISM_NAMES, \@GRISM_NEG_NAMES,
		    \@GRISM_MDPTS, \@GRISM_NEG_MDPTS, $GRISM_REV );



print "\nThink match is:\n";

for( my $i = 0; $i < 5; $i++ ){
  print "$i: closest position=$closest_pos[$i]=$closest_name[$i]; diff=$difference[$i]\n";
}


sub det_mos_nearest_posn{
  my $pos = $_[0];

  my $guess_index = 0;
  my @wheel_vals  = @PN_MOS_SLIT;
  my @wheel_names = @MOS_SLIT_PN_NAMES;
  my @wheel_mdpts = @MOS_SLIT_PN_MDPTS;

  my $num_posn    = @wheel_names;

  my $closest_pos;
  my $closest_name;
  my $diff;

  #check for too positive or too negative
  if( $pos >= $MOS_SLIT_PN_REV ){
    my $is_valid = 0;
    until( $is_valid ){
      $pos = $pos - $MOS_SLIT_PN_REV;
      if( $pos < $MOS_SLIT_PN_REV ){
	$is_valid = 1;
      }
    }
  }elsif( $pos <= -$MOS_SLIT_PN_REV ){
    my $is_valid = 0;
    until( $is_valid ){
      $pos = $pos + $MOS_SLIT_PN_REV;
      if( $pos > -$MOS_SLIT_PN_REV ){
	$is_valid = 1;
      }
    }
  }

  my $actual_pos = $pos;
  if( $pos >= $KL_MDPT_NEG and $pos <= $KL_MDPT_POS ){
    #print ">>>Checking against +- range\n";

    for( my $i = 1; $i < $num_posn; $i++ ){
      print "i=$i, guess_index=$guess_index:  ";
      print "checking posn=$pos against hp".($i-1).
            "=$wheel_mdpts[$i-1] and hp$i=$wheel_mdpts[$i]\n";
      
      if( ($pos >= $wheel_mdpts[$i - 1]) and 
	  ($pos < $wheel_mdpts[$i] ) ){
	print "in if $i\n";
	$guess_index = $i-1;
      }elsif( $pos > $wheel_mdpts[$num_posn - 1] and 
	      $pos <= $KL_MDPT_POS ){
	#This checks between last position and home
	print "checking upto KL_MDPT_POS\n";
	$guess_index = $num_posn - 1;
      }elsif( $pos < $wheel_mdpts[0] and
	      $pos >= $KL_MDPT_NEG      ){
	print "checking upt KL_MDPT_NEG\n";
	$guess_index = 0;
      }
    }

    $closest_pos  = $wheel_vals[$guess_index];
    $closest_name = $wheel_names[$guess_index];
    $diff         = $actual_pos - $wheel_vals[$guess_index];

  }elsif( $pos > $KL_MDPT_POS and $pos < $MOS_SLIT_PN_REV ){
    print "overplus\n";
    my $num_op = @OPOS_MOS_SLIT;
    
    for( my $i = 1; $i < $num_op; $i++ ){
      my $low_pos = $MOS_SLIT_OPOS_MDPTS[$i-1];
      my $hi_pos  = $MOS_SLIT_OPOS_MDPTS[$i];
      print "i=$i, guess_index=$guess_index:  ";
      print "checking pos=$pos against hp".($i-1).
	    "=$low_pos and hp$i=$hi_pos\n";

      if( $pos >= $low_pos and $pos < $hi_pos ){
	print "in if $i\n";
	$guess_index = $i ;

	$closest_pos  = $OPOS_MOS_SLIT[$guess_index];
	$closest_name = $MOS_SLIT_OPOS_NAMES[$guess_index];
	$diff         = $actual_pos - $closest_pos;

      }elsif( $pos >= $MOS_SLIT_OPOS_MDPTS[$num_op -1] and
	      $pos < $MOS_SLIT_PN_REV ){
	$guess_index = 10;

	$closest_pos  = $PN_MOS_SLIT[$guess_index];
	$closest_name = $MOS_SLIT_PN_NAMES[$guess_index];
	$diff         = $actual_pos - $closest_pos;

      }
    }

  }elsif( $pos < $KL_MDPT_NEG and $pos > -$MOS_SLIT_PN_REV ){
    print "overneg\n";
    my $num_op = @ONEG_MOS_SLIT;
    
    print "$pos, $ONEG_MOS_SLIT[0], $MOS_SLIT_ONEG_MDPTS[0]\n";

    if( $pos >= $ONEG_MOS_SLIT[0]  ){
      $guess_index = 0;
      $closest_pos  = $ONEG_MOS_SLIT[$guess_index];
      $closest_name = $MOS_SLIT_ONEG_NAMES[$guess_index];
      $diff         = $actual_pos - $closest_pos;

    }elsif( $pos <= $ONEG_MOS_SLIT[0] and $pos > $MOS_SLIT_ONEG_MDPTS[0] ){
      $guess_index = 0;
      $closest_pos  = $ONEG_MOS_SLIT[$guess_index];
      $closest_name = $MOS_SLIT_ONEG_NAMES[$guess_index];
      $diff         = $actual_pos - $closest_pos;
    }else{
      for( my $i = 1; $i < $num_op; $i++ ){
	my $low_pos = $MOS_SLIT_ONEG_MDPTS[$i-1];
	my $hi_pos  = $MOS_SLIT_ONEG_MDPTS[$i];
	print "i=$i, guess_index=$guess_index:  ";
	print "checking pos=$pos against hp".($i-1).
	  "=$low_pos and hp$i=$hi_pos\n";
	
	if( $pos <= $low_pos and $pos > $hi_pos ){
	  print "in if $i\n";
	  $guess_index = $i-1 ;
	  
	  $closest_pos  = $ONEG_MOS_SLIT[$guess_index];
	  $closest_name = $MOS_SLIT_ONEG_NAMES[$guess_index];
	  $diff         = $actual_pos - $closest_pos;
	  
	}elsif( $pos <= $MOS_SLIT_ONEG_MDPTS[$num_op -1] and
		$pos > -$MOS_SLIT_PN_REV ){
	  $guess_index = 10;
	  
	  $closest_pos  = $PN_MOS_SLIT[$guess_index];
	  $closest_name = $MOS_SLIT_PN_NAMES[$guess_index];
	  $diff         = $actual_pos - $closest_pos;
	  
	}
      }
    }
  }


    
  #print "\n\n$closest_pos=$closest_name, $diff\n\n";

  return( $closest_pos, $closest_name, $diff );

}#Endsub det_mos_nearest_posn



sub det_nearest_posn{
  my ($pos,         $aref_wval, $aref_neg_wval,
      $aref_wnam,   $aref_neg_wnam,
      $aref_wmdpts, $aref_wnegmdpts, $wheel_rev ) = @_;
  my $guess_index = 0;
  my @wheel_vals  = @$aref_wval;
  my @wheel_neg_vals = @$aref_neg_wval;
  my @wheel_names = @$aref_wnam;
  my @wheel_neg_names = @$aref_neg_wnam;
  my @wheel_mdpts = @$aref_wmdpts;
  my @wheel_neg_mdpts = @$aref_wnegmdpts;

  my $num_posn    = @wheel_names;

  my $closest_pos;
  my $closest_name;
  my $diff;

  #print "\npos=$pos, num_pos=$num_posn\n";
  my $actual_pos = $pos;
  if( $pos >= 0 ){
    #print ">>>Checking against positives\n";
    if( $pos > $wheel_rev ){
      my $is_valid=0;
      until( $is_valid ){
	$pos = $pos - $wheel_rev;
	if( $pos <= $wheel_rev ){
	  $is_valid = 1;
	}
      }
    }
    if( $pos >= 0  and $pos < $wheel_mdpts[0] ){
      $guess_index = 0;
    }else{
      for( my $i = 1; $i < $num_posn; $i++ ){
	#Positive vals first
	#print "i=$i, guess_index=$guess_index:  ";
	#print "checking posn=$pos against hp".($i-1).
	#      "=$wheel_mdpts[$i-1] and hp$i=$wheel_mdpts[$i]\n";

	if( ($pos >= $wheel_mdpts[$i - 1]) and 
	    ($pos < $wheel_mdpts[$i] ) ){
	  #print "in if $i ";
	  $guess_index = $i;
	}elsif( $pos > $wheel_mdpts[$num_posn - 1] and 
		$pos <= $wheel_rev ){
	  #This checks between last position and home
	  #print "in last check\n";
	  #print "pos = $pos\n";
	  #print "lastpos = $wheel_neg_mdpts[$num_posn - 1]\n";
	  #print "wrev    = $wheel_rev\n";
	  $guess_index = 0;
	}
      }
    }    

    $closest_pos  = $wheel_vals[$guess_index];
    $closest_name = $wheel_names[$guess_index];
    $diff         = $actual_pos - $wheel_vals[$guess_index];

  }elsif( $pos < 0 ){
    #print ">>>Checking against negatives\n";
    my $neg_wheel_rev = -1 * $wheel_rev;

    if( $pos < $neg_wheel_rev ){
      my $is_valid=0;
      until( $is_valid ){
	$pos = $pos - $neg_wheel_rev;
	if( $pos >= $neg_wheel_rev ){
	  $is_valid = 1;
	}
      }
    }
    #print "pos now=$pos\n";
    if( $pos <= 0  and $pos > $wheel_neg_mdpts[0] ){
      $guess_index = 0;
    }else{
      for( my $i = 1; $i < $num_posn; $i++ ){
	  #print "i=$i, guess_index=$guess_index:  ";
	  #print "checking posn=$pos against hp".($i-1).
	  #  "=$wheel_neg_mdpts[$i-1] and hp$i=$wheel_neg_mdpts[$i]\n";
	if( $pos <= $wheel_neg_mdpts[$i - 1] and 
	    $pos > $wheel_neg_mdpts[$i] ){
	  $guess_index = $i;
	}elsif( $pos < $wheel_neg_mdpts[$num_posn - 1] and 
		$pos >= $wheel_rev ){
	  $guess_index = 0;
	}
      }
    }    

    $closest_pos  = $wheel_neg_vals[$guess_index];
    $closest_name = $wheel_neg_names[$guess_index];
    $diff         = $actual_pos - $wheel_neg_vals[$guess_index];

  }#End ifelse pos and neg



  #print "\n\n$closest_pos=$closest_name, $diff\n\n";

  return( $closest_pos, $closest_name, $diff );

}#Endsub det_nearest_posn



#rcsId = q($Name:  $ $Id: neartest.pl 14 2008-06-11 01:49:45Z hon $);

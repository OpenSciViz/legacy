#!/usr/local/bin/perl -w

use strict;


my $str = "foo";

print "in main; str=$str\n";

modstr();
print "out of modstr; now str=$str\n";

print "\ndefine ar in main\n";
my @ar = q( 0 1 2 3 4 );
print @ar;print "\n\n";

modar();
print "after calling modar\n";
print @ar;print "\n";

lookatar();

sub modstr{
  print "in modstr; str=$str\n";
  print "try to change it\n";
  $str = "not_foo";
}

sub modar{
  print "now in modar; try to change it\n";
  $ar[1] = -1;
}

sub lookatar{
  print "\nin lookatar\n";
  print @ar;print "\n";
}

#rcsId = q($Name:  $ $Id: vartest.pl 14 2008-06-11 01:49:45Z hon $);

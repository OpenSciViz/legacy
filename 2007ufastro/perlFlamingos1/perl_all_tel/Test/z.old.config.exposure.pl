#!/usr/local/bin/perl -w

my $rcsId = q($Name:  $ $Id: z.old.config.exposure.pl 14 2008-06-11 01:49:45Z hon $);

###############################################################################
#
#Configure fits header parameters for next exposure
#
##############################################################################
use strict;
use Getopt::Long;

my $DEBUG = 0;#0 = not debugging; 1 = debugging
GetOptions( 'debug' => \$DEBUG );

my $header = select_header();

use Fitsheader qw/:all/;
Find_Fitsheader($header);

#This must be done first in order to load some variables
#the module uses; remnant logic from Frank
readFitsHeader($header);


print "\nPresent exposure parameter settings are:\n";
printFitsHeader( "OBJECT", "FILEBASE",
		 "EXP_TIME",  "NREADS" );

#print "telescope is " . param_value( "TELESCOP" ) . "\n";
#print "Integration time is " . param_value( "INT_S" ) . "\n";


my $didmod = 0;
if( modStringParams( "OBJECT", "FILEBASE",
		     "EXP_TIME",  "NREADS") > 0 ){ 
  $didmod = 1 ;
###Check values here; need to mod each param individually
###Need print out entered values, and offer chance to change them

}


if( $didmod > 0 ){
  if( !$DEBUG ){

    print "\nUpdating FITS header in: $header ...\n";
    writeFitsHeader( $header );

    #my $dothis1 = "cp -p $fitsheader_file /data1/flamingos ";
    #my $dothis2 = "cp -p $fitsheader_file /data2/flamingos ";
    #system($dothis1);
    #print "Copying FITS header to /data1/flamingos \n";
    #system($dothis2);
    #print "Copying FITS header to /data2/flamingos \n";

  }elsif( $DEBUG ){
    print "\nUpdating FITS header in: $header ...\n";
    writeFitsHeader( $header );
  }
}elsif( $didmod == 0 ){
  print "No changes made to default header.\n\n";
}

sub select_header{
  my $fitsheader_path;
  my $fitsheader_file = $ENV{FITS_HEADER_NAME};
  my $header;
  if( !$DEBUG ){
    $fitsheader_path = $ENV{FITS_HEADER_LUT};
    $header =  $fitsheader_path . $fitsheader_file;
  }elsif( $DEBUG ){
    $fitsheader_path = $ENV{FITS_HEADER_LUT_DEBUG};
    $header          = $fitsheader_path . $fitsheader_file;
    print "In debug mode\n";
    print "Header is $header\n";
  }
  return $header;
}

exit;




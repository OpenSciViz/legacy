#!/usr/local/bin/perl -w

my $rcsId = q($Name:  $ $Id: config.wheels.pl 14 2008-06-11 01:49:45Z hon $);

use strict;
use Getopt::Long;
use GetYN;
use Fitsheader qw/:all/;
use Wheel qw/:all/;


my $DEBUG = 0;#0 = not debugging; 1 = debugging
GetOptions( 'debug' => \$DEBUG );

my $header = select_header($DEBUG);

Find_Fitsheader($header);

#readFitsHeader is exported from Fitsheader.pm
#and loads the following arrays from the input header
#@fh_params @fh_pvalues @fh_comments
readFitsHeader($header);



##>>>>Get Present Positions and Motion of Wheels<<<<<##
#The following arrays will have 5 entries that correspond to @wheels

my ( $ar1, $ar2, $ar3, $ar4, $ar5, $ar6, $ar7 ) = get_wheel_info();
#####****Have it return the correct index#####
my @motors       = @$ar1;
my @wheels       = @$ar2;
my @actual_pos   = @$ar3;
my @closest_pos  = @$ar4;
my @closest_name = @$ar5;
my @difference   = @$ar6;
my @motion       = @$ar7;

Print_Out( \@motors, \@wheels, 
	   \@actual_pos, \@closest_pos, 
	   \@closest_name, \@difference, \@motion,
	  );


print "\n\nChange any wheel configuration? ";
my $change = get_yn();
if( $change == 0 ){
  die "Exiting\n\n";
}

#Get home direction for mos
my $default_mos_home_dir = 0;
my $neg_mos_home_dir = 1;
my $mos_home_dir = $default_mos_home_dir; 
if( $actual_pos[1] > 0 ){
  $mos_home_dir = 1;
}

##      motor     =      a   b   c   d   e
my @new_pos       =   ( PI, PI, PI, PI, PI );
my @new_index     =   (  0,  0,  0,  0,  0 );
my @new_name      = qw( nc  nc  nc  nc  nc );
my @home_types;

my $redo = 0;
until( $redo ){
  query_change_what( \@new_pos, \@new_index, \@new_name );
  get_home_types( \@home_types );

  print_selections( \@motors,       \@wheels,
		    \@closest_name, \@new_name, \@home_types );


  print_exec_msg();
  print "Update the header to the target positions and move the wheels? (y)\n";
  print "Or, redo the new wheel configuration?                          (n)  ";

  #print "\nAccept changes? ";
  $redo = get_yn();
}

update_header( \@new_pos, \@new_name,
	       \@closest_pos, \@closest_name, $header );

my $motion = build_home_and_move_commands( \@motors, \@new_pos, \@home_types );

my $remove_backlash_cmd = build_remove_backlash_cmds( \@motors, \@home_types,
						      \@new_pos);
print "remove backlash cmd is $remove_backlash_cmd\n";

#my $ar_mos_remove_backlash_cmds = build_remove_backlash_mos_neg_home( 
#	     	                  $motors[1], $home_types[1], $mos_home_dir );
#my @mos_remove_backlash_cmds = @$ar_mos_remove_backlash_cmds;
#print "$mos_remove_backlash_cmds[0], $mos_remove_backlash_cmds[1]\n";

my $max_time_est = compute_time_est( \@new_pos );
#print "max time is $max_time_est\n";
execute_motion( $motion, $max_time_est, $remove_backlash_cmd );

####>>>>>>> SUBS <<<<<<<<####
sub query_change_what{
  my ( $ar_new_pos, $ar_new_index, $ar_new_name ) = @_;
  my $ready = 0;

  until( $ready ){
    print "Enter a motor name (a-e) to configure\n";
    print "or enter q to quit without making changes:  ";
    my $new_motor = <STDIN>; chomp $new_motor;
    
    if( $new_motor =~ m/q/i ){
      die "Exiting\n\n";
    }
    
    if( $new_motor =~ m/[a-eA-E]/ ){
      if( $new_motor =~ m/a/i ){
	
	( $$ar_new_pos[0], $$ar_new_index[0] )
	  = select_new_position( \@DECKER_NAMES, \@DECKER, "DECKER");

	$$ar_new_name[0] = $DECKER_NAMES[$$ar_new_index[0]];
	print "\nWill send motor a to $$ar_new_name[0] = ".
	      "$$ar_new_pos[0]\n\n";
	
	print "Configure another wheel?  ";
	my $y_n = get_yn();
	if( $y_n == 0 ){ $ready = 1;}
	
      }elsif( $new_motor =~ m/b/i ){

	( $$ar_new_pos[1], $$ar_new_index[1] )
	  = select_new_position( \@MOS_SLIT_PN_NAMES, 
				 \@PN_MOS_SLIT, "MOS_SLIT");

	$$ar_new_name[1] = $MOS_SLIT_PN_NAMES[$$ar_new_index[1]];
	print "\nWill send motor b to $MOS_SLIT_PN_NAMES[$$ar_new_index[1]]= ".
	      "$$ar_new_pos[1]\n\n";
	
	print "Configure another wheel?  ";
	my $y_n = get_yn();
	if( $y_n == 0 ){ $ready = 1;}

      }elsif( $new_motor =~ m/c/i ){

	( $$ar_new_pos[2], $$ar_new_index[2] )
	  = select_new_position( \@FILTER_NAMES, \@FILTER, "FILTER");

	$$ar_new_name[2] = $FILTER_NAMES[$$ar_new_index[2]];
	print "\nWill send motor c to $FILTER_NAMES[$$ar_new_index[2]]= ".
	      "$$ar_new_pos[2]\n\n";
	
	print "Configure another wheel?  ";
	my $y_n = get_yn();
	if( $y_n == 0 ){ $ready = 1;}

      }elsif( $new_motor =~ m/d/i ){

	( $$ar_new_pos[3], $$ar_new_index[3] )
	  = select_new_position( \@LYOT_NAMES, \@LYOT, "LYOT");

	$$ar_new_name[3] = $LYOT_NAMES[$$ar_new_index[3]];
	print "\nWill send motor d to $LYOT_NAMES[$$ar_new_index[3]]= ".
	      "$$ar_new_pos[3]\n\n";
	
	print "Configure another wheel?  ";
	my $y_n = get_yn();
	if( $y_n == 0 ){ $ready = 1;}
	
      }elsif( $new_motor =~ m/e/i ){

	( $$ar_new_pos[4], $$ar_new_index[4] )
	  = select_new_position( \@GRISM_NAMES, \@GRISM, "GRISM");

	$$ar_new_name[4] = $GRISM_NAMES[$$ar_new_index[4]];
	print "\nWill send motor e to $GRISM_NAMES[$$ar_new_index[4]]= ".
	      "$$ar_new_pos[4]\n\n";
	
	print "Configure another wheel?  ";
	my $y_n = get_yn();
	if( $y_n == 0 ){ $ready = 1;}
	
      }
    }else{
      print "Please enter motor name from the list a-e\n";
    }
  }

}#Endsub query_change_what


sub update_header{
  my ( $ar_new_pos, $ar_new_name, 
       $ar_closest_pos, $ar_closest_name,$header ) = @_;
  my @new_pos = @$ar_new_pos;
  my @new_name = @$ar_new_name;
  my @closest_pos = @$ar_closest_pos;
  my @closest_name = @$ar_closest_name;

  my $num_mot = $ENV{NUM_MOTORS};

  for( my $i = 0; $i < $num_mot; $i++ ){
    if( $new_pos[$i] == PI ){
      #Put unchanged old names and positions in header
      #print "calling update_params with ".
      #	    "$i, $closest_name[$i], $closest_pos[$i]\n";
      update_params( $i, $closest_name[$i], $closest_pos[$i] );
    }elsif( $new_pos[$i] != PI ){
      #Put new target names and positions in header
      #print "calling update_params with  $i, $new_name[$i], $new_pos[$i]\n";
      update_params( $i, $new_name[$i], $new_pos[$i] );
    }
  }
  print "\n";
  writeFitsHeader( $header );

}#Endsub update_header


sub update_params{
  my ( $i, $name, $pos ) = @_;
     
  if( $i == 0 ){
    #decker
    setStringParam(  "DECKER", $name );
    setNumericParam( "POS_A",  $pos );
    print "New header param: DECKER=$name,\t\tPOS_A=$pos\n";
  }elsif( $i == 1 ){
    #mos_slit
    set_MOS_SLIT_params( $name, $pos );
  }elsif( $i == 2 ){
    #filter
    setStringParam(  "FILTER", $name );
    setNumericParam( "POS_C",  $pos );
    print "New header params FILTER=$name,\t\tPOS_C=$pos\n";
  }elsif( $i == 3 ){
    #lyot
    setStringParam(  "LYOT",  $name );
    setNumericParam( "POS_D", $pos );
    print "New header params LYOT  =$name,\t\tPOS_D=$pos\n";
  }elsif( $i == 4 ){
    #grism
    setStringParam(  "GRISM", $name );
    setNumericParam( "POS_E", $pos );
    print "New header params GRISM =$name,\t\tPOS_D=$pos\n";
  }

}#Endsub update_params

sub set_MOS_SLIT_params{
  my ( $name, $pos ) = @_;

  if( $name =~ m/imaging/ ){
    setStringParam( "SLIT", $name );
    setStringParam( "MOS",  $name );
    setNumericParam( "POS_B", $pos );
    print "New header param: SLIT  =$name, ";
    print "\tPOS_B=$pos\n";
    print "New header param: MOS   =$name, ";
    print "\tPOS_B=$pos\n";
  }elsif( $name =~ m/pix/ ){
    setStringParam( "SLIT",   $name );
    setStringParam( "MOS",   'long-slit' );
    setNumericParam( "POS_B", $pos );
    print "New header param: SLIT  =$name, ";
    print "\t\tPOS_B=$pos\n";
  }else{
    setStringParam( "MOS",  $name );
    setStringParam( "SLIT", 'mos' );
    setNumericParam( "POS_B", $pos );
    print "New header param: MOS   =$name, ";
    print "\t\tPOS_B=$pos\n";
  }

}#Endsub set_MOS_SLIT_params


sub build_home_and_move_commands{
  my ( $ar_motors, $ar_new_pos, $ar_home_types ) = @_;
  my @motors  = @$ar_motors;
  my @new_pos = @$ar_new_pos;
  my @home_types = @$ar_home_types;

  my $motion;
  my $ampersand = " & ";

  my $num_to_home_and_move = how_many_to_home_and_move( \@new_pos );

  my $set_i_v = set_i_v( \@new_pos, $num_to_home_and_move );

  my $home_cmds = build_home_commands( \@motors, \@home_types, 
		                        $num_to_home_and_move);

  my $move_cmds = build_move_commands( \@motors, \@home_types,
				        $num_to_home_and_move,
				       \@new_pos );

  $motion = $set_i_v.$ampersand.$home_cmds.$ampersand.$move_cmds;
  #print "final motion string: $motion\n";
  
  return $motion;
}#Endsub build_home_and_move_commands


sub how_many_to_home_and_move{
  my $ar_new_pos = $_[0];
  my @new_pos = @$ar_new_pos;
  my $num_to_home_and_move = 0;

  for( my $i = 0; $i < $ENV{NUM_MOTORS}; $i++ ){
    if( $new_pos[$i] != PI ){
      $num_to_home_and_move++;
    }
  }

  return $num_to_home_and_move;
}#Endsub how_many_to_home_and_move


sub set_i_v{
  my ( $ar_new_pos, $num_to_home_and_move ) = @_;
  my @new_pos = @$ar_new_pos;
  my $ampersand = " & ";
  my $which_not_pie = 1;
  my $set_i_v;

  for( my $i = 0; $i < $ENV{NUM_MOTORS}; $i++ ){
    if( $new_pos[$i] != PI ){
      if( $which_not_pie == 1 ){
	$set_i_v = $ENV{"I_".$i}.$ampersand.$ENV{"V_".$i};	
	$which_not_pie++;
      }elsif( $which_not_pie > 1 and
	      $which_not_pie <= $num_to_home_and_move ){
	$which_not_pie++;
	$set_i_v = $set_i_v.$ampersand.
	           $ENV{"I_".$i}.$ampersand.$ENV{"V_".$i};
      }
    }
  }
  #print "$set_i_v\n";
  return $set_i_v;
}#Endsub set_i_v


sub build_home_commands{
  my ( $ar_motors, $ar_home_types, $num_to_home_and_move ) = @_;
  my @motors  = @$ar_motors;
  my @home_types = @$ar_home_types;
 
  my $num_mot = $ENV{NUM_MOTORS};
  my $which_not_pie = 1;
  my $ampersand = " & ";
  my $home_cmd;

  for( my $i = 0; $i < $num_mot; $i++ ){
    #New positions are != PI; build the home cmd
    if( $new_pos[$i] != PI ){
      if( $which_not_pie == 1 ){
	$home_cmd = interp_home_cmd( $motors[$i], $home_types[$i], $i );
	$which_not_pie++;
      }elsif( $which_not_pie > 1 and
	      $which_not_pie <= $num_to_home_and_move ){
	$which_not_pie++;
	my $tmp = interp_home_cmd( $motors[$i], $home_types[$i], $i );
	$home_cmd = $home_cmd.$ampersand.$tmp;
	            
      }
    }
  }
  #print "$home_cmd\n";
  return $home_cmd;

}#Endsub build_home_commands


sub build_move_commands{
  my ( $ar_motors, $ar_home_types, $num_to_home_and_move, $ar_new_pos ) = @_;
  my @motors  = @$ar_motors;
  my @home_types = @$ar_home_types;
  my @new_pos = @$ar_new_pos;
 
  my $num_mot = $ENV{NUM_MOTORS};
  my $which_not_pie = 1;
  my $ampersand = " & ";
  my $move_cmd;

  for( my $i = 0; $i < $num_mot; $i++ ){
    my $pos = $new_pos[$i];
    my $home_type = $home_types[$i];
    my $mot = $motors[$i];
    my $nh_pos       = $ENV{ "NH_".$i };
    my $adjusted_pos = $pos - $nh_pos;

    #New positions are != PI; build the home cmd
    if( $pos != PI ){
      #print "pos=$pos, nh_pos=$nh_pos, adjpos=$adjusted_pos\n";
    
      if( $which_not_pie == 1 ){
	$which_not_pie++;

	if( $home_type == 1 ){
	  #USE LIMIT SWITCH
	  if( $pos >= 0 ){
	    $move_cmd = $mot."+".$pos;

	  }elsif( $pos < 0 ){
	    my $tmp   = add_backlash( $i, $mot, $pos, 0 );
	    $move_cmd = $tmp;
	  }
	}elsif( $home_type == 0 ){
	  #USE NEAR HOME
	  if( $adjusted_pos >= 0 ){
	    $move_cmd = $mot."+".$adjusted_pos;

	  }elsif( $adjusted_pos < 0 ){
	    my $tmp   = add_backlash( $i, $mot, $adjusted_pos, $nh_pos );
	    $move_cmd = $tmp;
	  }
	}

      }elsif( $which_not_pie > 1 and
	      $which_not_pie <= $num_to_home_and_move ){
	$which_not_pie++;

   	if( $home_type == 1 ){
	  #USE LIMIT SWITCH
	  if( $pos >= 0 ){
	    $move_cmd = $move_cmd.$ampersand.$mot."+".$pos;

	  }elsif( $pos < 0 ){
	    my $tmp   = add_backlash( $i, $mot, $pos, 0 );
	    $move_cmd = $move_cmd.$ampersand.$tmp;
	               
	  }
	}elsif( $home_type == 0 ){
	  #USE NEAR HOME
	  if( $adjusted_pos >= 0 ){
	    $move_cmd = $move_cmd.$ampersand.$mot."+".$adjusted_pos;

	  }elsif( $adjusted_pos < 0 ){
	    my $tmp   = add_backlash( $i, $mot, $adjusted_pos, $nh_pos );
	    $move_cmd = $move_cmd.$ampersand.$tmp;
	                
	  }
	}
      }
    }
  }
  #print "$move_cmd\n";
  return $move_cmd;

}#Endsub build_move_commands


sub add_backlash{
  my ( $i, $mot, $pos, $nh_pos ) = @_;
  my $ampersand = " & ";
  my $backlash_cmd;

  my $backlash = $ENV{ "BACKLASH_".$i };

  if( $pos < 0 ){
    my $move_to_val = abs( $pos ) + $backlash;
    #David's demon doesn't allow sequential moves of the same motor in 
    #one command-line call.  So the following commented lines don't work.
    #$backlash_cmd = $mot."-".$move_to_val.$ampersand.
    #                $mot."+".$backlash;

    $backlash_cmd = $mot."-".$move_to_val;
  }
  
  return $backlash_cmd;
}#Endsub add_backlash


sub build_remove_backlash_cmds{
    my ( $ar_motors, $ar_home_types, $ar_new_pos ) = @_;
    my @motors = @$ar_motors;
    my @home_types = @$ar_home_types;
    my @new_pos = @$ar_new_pos;

    my $num_mot = $ENV{NUM_MOTORS};
    my $which_not_pie = 1;
    my $ampersand = " & ";
    my $remove_backlash_cmd = "nobacklash";

    my $num_to_home_and_move = how_many_to_home_and_move( \@new_pos );

    for( my $i = 0; $i < $num_mot; $i++ ){
      my $pos = $new_pos[$i];
      my $home_type = $home_types[$i];
      my $mot = $motors[$i];
      my $nh_pos       = $ENV{ "NH_".$i };
      my $adjusted_pos = $pos - $nh_pos;

      #print "pos=$pos; ht=$home_type; mot=$mot; ".
      #      "nh=$nh_pos; adjpos=$adjusted_pos\n";
      #New positions are != PI; build the home cmd
      if( $pos != PI ){
	#print "pos=$pos, nh_pos=$nh_pos, adjpos=$adjusted_pos\n";
	
	if( $which_not_pie == 1 ){
	  $which_not_pie++;
	  
	  if( $home_type == 1 ){
	    #USE LIMIT SWITCH
	    if( $pos >= 0 ){
	      #do nothing
	    }elsif( $pos < 0 ){
	      $remove_backlash_cmd = $mot."V50".$ampersand.
		                     $mot."+".$ENV{ "BACKLASH_".$i };
	    }
	  }elsif( $home_type == 0 ){
	    #USE NEAR HOME
	    if( $adjusted_pos >= 0 ){
	      #do nothing
	    }elsif( $adjusted_pos < 0 ){
	      $remove_backlash_cmd = $mot."V50".$ampersand.
		                     $mot."+".$ENV{ "BACKLASH_".$i };
	    }
	  }
	  
	}elsif( $which_not_pie > 1 and
		$which_not_pie <= $num_to_home_and_move ){
	  $which_not_pie++;
	  
	  if( $home_type == 1 ){
	    #USE LIMIT SWITCH
	    if( $pos >= 0 ){
	      #do nothing
	    }elsif( $pos < 0 ){
	      my $tmp   = $mot."V50".$ampersand.
		          $mot."+".$ENV{ "BACKLASH_".$i };
	      $remove_backlash_cmd = $remove_backlash_cmd.$ampersand.$tmp;
	    }
	  }elsif( $home_type == 0 ){
	    #USE NEAR HOME
	    if( $adjusted_pos >= 0 ){
	      #do nothing
	    }elsif( $adjusted_pos < 0 ){
	      my $tmp   = $mot."V50".$ampersand.
		          $mot."+".$ENV{ "BACKLASH_".$i };
	      $remove_backlash_cmd = $remove_backlash_cmd.$ampersand.$tmp;
	    }
	  }
	}
      }
    }      
    
    return $remove_backlash_cmd;
}#Endsub build_remove_backlash_cmds


sub build_remove_backlash_mos_neg_home{
  my ( $motor, $home_type, $mos_home_dir ) = @_;

  my @mos_remove_backlash_cmds;
  if( $mos_home_dir == 1 and $home_type == 1 ){
    #negative direction and limit switch homing
    $mos_remove_backlash_cmds[0] = "$motor-".$ENV{BACKLASH_1}." ";
    $mos_remove_backlash_cmds[1] = $ENV{FV_1}." ";
  }#do nothing if default direction
  else{ $mos_remove_backlash_cmds[0] = "nomosbacklash"; 
        $mos_remove_backlash_cmds[1] = "nomosbacklash";}

  return \@mos_remove_backlash_cmds;
}#Endsub build_remove_backlash_mos_neg_home


sub execute_motion{
  my ( $motion, $max_time_est, $remove_backlash_cmd ) = @_;
  my $space  = " ";
  my $quote  = "\"";

  my $motor_agent = $ENV{UFMOTOR}.$space.
                    $ENV{CLIENT_HOST}.$space.$ENV{HOST}.$space.
		    $ENV{CLIENT_PORT}.$space.$ENV{CLIENT_UFMOTOR_PORT}.$space.
		    $ENV{CLIENT_RAW}.$space;

  my $cmd = $motor_agent.$quote.$motion.$quote;

  my $do_remove_backlash = 0;
  my $do_remove_backlash_cmd = " ";
  if( $remove_backlash_cmd !~ m/nobacklash/ ){
    $do_remove_backlash = 1;
    $do_remove_backlash_cmd = $motor_agent.$quote.$remove_backlash_cmd.$quote;
  }

  #print "\n\nShould be Executing command to home and move wheels:\n$cmd\n\n";

  print "\n\nExecuting command to home and move wheels:\n$cmd\n\n";
  my $reply = `$cmd`;

  move_loop( $max_time_est, $do_remove_backlash, $do_remove_backlash_cmd );
  
}#Endsub execute_motion


sub compute_time_est{
  my $ar_new_pos = $_[0];
  my @new_pos = @$ar_new_pos;
  my $max_time_est;

  my $a = 0; my $b = 0; my $cde = 0;
  for( my $i=0; $i < $ENV{NUM_MOTORS}; $i++ ){
    my $this_pos = $new_pos[$i];
    if( $this_pos != PI ){
      if( $i == 0 ){ 
	$a = 1;
      }elsif( $i == 1 ){
	$b = 1;
      }else{
	$cde = 1;
      }
    }
  }

  my $vel;
  if( $a == 1 and $b == 0 ){
    $vel = substr $ENV{V_0}, 2;
    $max_time_est = 2 * $DECKER_REV / $vel;
  }elsif( $a == 0 and $b == 0 ){
    $vel = substr $ENV{V_2}, 2;
    $max_time_est = 2 * $FILTER_REV / $vel;
  }elsif( $b == 1 ){
    $vel = substr $ENV{V_1}, 2;
    #print "$ENV{V_1}, $vel\n";
    $max_time_est = $MOS_SLIT_REV / $vel;
  }
  #print "max time is $max_time_est\n";
  return $max_time_est;
}#Endsub compute_time_est


sub print_exec_msg{
  
  print "________________________________________________________________\n";

  print "Flamingos has had problems with it's wheels in the past, and\n";
  print "it has been useful to listen to them while they move.  If there's\n";
  print "an intercom or microphone near the dewar, consider turning it on.\n\n";

  print "There are two failure modes:\n";
  print "1) If a wheel moves by first homing to the limit switch, and\n";
  print "the switch fails, then the wheel will turn endlessly.\n\n";

  print "2)  If a wheel binds the griding noise should be audible\n";
  print "(likely candidate for this is the mos_slit wheel).\n\n";

  print ">>>>>  WHAT TO DO IF A FAILURE OCCURS\n";
  print "Type Ctrl-C to kill the script, and then type the following:\n";
  print "ufmotor $ENV{CLIENT_HOST} ".
        "$ENV{HOST} $ENV{CLIENT_PORT} $ENV{CLIENT_UFMOTOR_PORT}\n\n";

  print "At the ufmotor> prompt type, e.g. a@, or whatever motor\n";
  print "name is appropriate (a=decker, b=mos, c=filter, d=lyot, e=grism).\n\n";

  print "In the event that this fails, log into the baytech and\n";
  print "cycle the power on the motor controller:\n";
  print "telnet baytech,  username> guest,  passwd>16Mb,img\n";
  print "reboot 3\n";
  print "logout\n\n";


  print "(Note that cycling the power on the motor controller will reset)\n";
  print "(the current position of all motors to zero.  Any wheels that  )\n";
  print "(use the Near home technique will probably be lost as well as  )\n";
  print "(the motor that requires this drastic action.                  )\n";
  print "________________________________________________________________\n\n";

  
}#Endsub print_exec_msg


sub move_loop{
  my ( $max_time_est, $do_remove_backlash, $do_remove_backlash_cmd ) = @_;

  my $timer = 0;
  my $wait_time = 5; #seconds
  my $stationary_twice = 0;
  my $time_elapsed = 0;
  until( $stationary_twice == 2 or $time_elapsed == 1 ){
    print "\n\n*********************************************************\n";
    print "This should not take longer than $max_time_est seconds.\n";
    print "Have waited $timer seconds so far.\n\n";
    my ( $ar1, $ar2, $ar3, $ar4, $ar5, $ar6, $ar7 ) = get_wheel_info();
    my @motors       = @$ar1;
    my @wheels       = @$ar2;
    my @actual_pos   = @$ar3;
    my @closest_pos  = @$ar4;
    my @closest_name = @$ar5;
    my @difference   = @$ar6;
    my @motion       = @$ar7;
    
    Print_Out( \@motors, \@wheels, 
	       \@actual_pos, \@closest_pos, 
	       \@closest_name, \@difference, \@motion,
	     );
    
    my $number_stationary = get_stationary( \@motion );
    if( $number_stationary == 5 ){
      $stationary_twice += 1;
    }

    if( $timer >= $max_time_est ){
      $time_elapsed = 1;
    }
    sleep $wait_time;
    $timer += $wait_time;
  }

  if( $time_elapsed ){
    print ">>>>>>     WARNING   WARNING  WARNGIN  WARNING  WARNING   <<<<<<\n";
    print ">>>>>>     Maximum estimated time ($max_time_est seconds) <<<<<<\n";
    print ">>>>>>     for wheel(s) to move has elapsed!              <<<<<<\n";

    print_exec_msg();
  }

  if( $do_remove_backlash ){
    remove_backlash_loop( $do_remove_backlash_cmd );
  }

  print "\n\n>>>>>  Wheel motion is done.\n\n";

}#Endsub move_loop


sub remove_backlash_loop{
  my ( $do_remove_backlash_cmd ) = @_;

  my $timer = 0;
  my $wait_time = 5; #seconds
  my $stationary_twice = 0;
  my $time_elapsed = 0;
  my $max_time_est = 17;

  #print "\nShould be Removing backlash by executing\n$do_remove_backlash_cmd\n\n";

  print "\nRemoving backlash by executing\n$do_remove_backlash_cmd\n\n";
  system( $do_remove_backlash_cmd );

  until( $stationary_twice == 2 or $time_elapsed == 1 ){
    print "\n\n*********************************************************\n";
    print "This should not take longer than $max_time_est seconds.\n";
    print "Have waited $timer seconds so far.\n\n";
    my ( $ar1, $ar2, $ar3, $ar4, $ar5, $ar6, $ar7 ) = get_wheel_info();
    my @motors       = @$ar1;
    my @wheels       = @$ar2;
    my @actual_pos   = @$ar3;
    my @closest_pos  = @$ar4;
    my @closest_name = @$ar5;
    my @difference   = @$ar6;
    my @motion       = @$ar7;
    
    Print_Out( \@motors, \@wheels, 
	       \@actual_pos, \@closest_pos, 
	       \@closest_name, \@difference, \@motion,
	     );
    
    my $number_stationary = get_stationary( \@motion );
    if( $number_stationary == 5 ){
      $stationary_twice += 1;
    }

    if( $timer >= $max_time_est ){
      $time_elapsed = 1;
    }
    sleep $wait_time;
    $timer += $wait_time;
  }

  if( $time_elapsed ){
    print ">>>>>>     WARNING   WARNING  WARNGIN  WARNING  WARNING   <<<<<<\n";
    print ">>>>>>     Maximum estimated time ($max_time_est seconds) <<<<<<\n";
    print ">>>>>>     for wheel(s) to move has elapsed!              <<<<<<\n";

    print_exec_msg();
  }

}#Endsub remove_backlash_loop


sub get_stationary{
  my $ar = $_[0];
  my @motion = @$ar;
  my $number_stationary = 0;

  for( my $i = 0; $i < $ENV{NUM_MOTORS}; $i++ ){
    if( $motion[$i] =~ m/Stationary/ ){
      $number_stationary += 1;
    }
  }

  return $number_stationary;
}#Endsub get_stationary


sub get_home_types{
  my $ar_home_types = $_[0];
  my $num_mot = $ENV{NUM_MOTORS};

  for( my $i = 0; $i < $num_mot; $i++ ){
    my $ht_str = "HT_".$i;

    $$ar_home_types[$i] = param_value( $ht_str );

  }
  
}#Endsub get_home_types


sub interp_home_cmd{
  my ( $motor, $input, $i ) = @_;
  my $cmd_str;
  my $ampersand = " & ";
  
  print "actual pos for motor i=$i is $actual_pos[$i]\n";

  if( $input == 0 ){
    #nearhome
    #makes strings like aI100 & aV500 & aN25
    $cmd_str = $motor.$ENV{NH}.$ENV{ "NH_".$i };

  }elsif( $input == 1 ){
    #limit home
    #acquires strings like aI100 & aV500 & aF500
    $cmd_str = $ENV{ "FV_".$i };
    if( $i == 1 ){
      #select direction to home for mos motor b
      if( $actual_pos[$i] > 0 ){
	#home in negative direction if present position is positive
	$cmd_str = $cmd_str . " $neg_mos_home_dir";
      }elsif( $actual_pos[$i] <= 0 ){
	#home is positive direction is present position is negative or zero
	$cmd_str = $cmd_str . " $default_mos_home_dir";
      }
  }
  }

  return $cmd_str;

}#Endsub interp_home_cmd


sub print_selections{
  my ( $ar_motors,       $ar_wheels, 
       $ar_closest_name, $ar_new_name, $ar_home_types ) = @_;

  my @motors = @$ar_motors;
  my @wheels = @$ar_wheels;
  my @closest_name = @$ar_closest_name;
  my @new_name   = @$ar_new_name;
  my @home_types = @$ar_home_types;

  my $num_mot = $ENV{NUM_MOTORS};

  print "The new configuation:\n";
  print "\t\t\tMOVE\t\tMOVE\n";
  print "Motor\tWheel\t\tFROM\t\tTO\n";
  print "_________________________________________________\n";
  for( my $i = 0; $i < $num_mot; $i++ ){
    if( $new_pos[$i] != PI ){
      if( $i == 1 ){
	print "$motors[$i]\t$wheels[$i]\t".
	      "$closest_name[$i]\t\t$new_name[$i]\n";
      }else{
	print "$motors[$i]\t$wheels[$i]\t\t".
	      "$closest_name[$i]\t\t$new_name[$i]\n";
      }
      #print "$home_types[$i]\n";
    }    
  }
  print "________________________________________________\n";

}#Endsub print_selections

sub select_new_position{
  my ( $ar1, $ar2, $wheel_name ) = @_;
  my @POS_NAMES = @$ar1;
  my @POS_VALS  = @$ar2;
  my $num_pos   = @POS_NAMES;
  my $num_pos0  = $num_pos - 1;

  my $new_pos_val = PI;#this is the value returned if choose not to change
  my $new_pos_num = 0;

  my $is_valid = 0;

  print "\n\n";
  print "Known $wheel_name positions\n";
  print "Position #\tPosition name\n";
  for( my $i = 0; $i < $num_pos; $i++ ){
    #print "$i\t\t$POS_NAMES[$i]\n";
    print "$i -------------- $POS_NAMES[$i]\n";
  }

  until( $is_valid ){
    print "Select a position number: ";
    $new_pos_num = <STDIN>; chomp $new_pos_num;
    
    my $input_len = length $new_pos_num;
    if( $input_len == 0 ){
      print "Not changing $wheel_name position.\n";
      $is_valid = 1;
    }else{
      #if( $new_pos_num =~ m/[0-$num_pos0]/ ){
      if( $new_pos_num >= 0 and $new_pos_num < $num_pos ){
	#print "if = $new_pos_num\n";
	$new_pos_val = $POS_VALS[$new_pos_num];
	$is_valid    = 1;
      }else{
	print "Please enter a number between 0 and $num_pos0:\n";
      }
    }
  }#End until is_valid
  return ( $new_pos_val, $new_pos_num );
  
}#Endsub select_new_position

sub configure_motor_b{
  my $num_pos = @MOS_SLIT_NAMES;
  my $num_pos0 = $num_pos - 1;
  my $rpos = PI;#this is the value returned if choose not to change
  my $is_valid = 0;

  print "\n\n";
  print "Known MOS/Slit positions\n";
  print "Position #\tPosition name\n";
  for( my $i = 0; $i < $num_pos; $i++ ){
    print "$i\t\t$MOS_SLIT_NAMES[$i]\n";
  }

  until( $is_valid ){
    print "Select a position number: ";
    my $new_pos_num = <STDIN>; chomp $new_pos_num;
    
    my $input_len = length $new_pos_num;
    if( $input_len == 0 ){
      print "Not changing MOS/Slit position.\n";
      $is_valid = 1;
    }else{
      if( $new_pos_num =~ m/[0-$num_pos0]/ ){
	print "if = $new_pos_num\n";
	$rpos = $MOS_SLIT[$new_pos_num];
	$is_valid = 1;
      }else{
	print "Please enter a number between 0 and $num_pos0:\n";
      }
    }
  }#End until is_valid
  return $rpos;
  
}#Endsub configure_motor_b


sub Print_Out{
  my ( $aref1, $aref2, $aref3, $aref4, $aref5, 
       $aref6, $aref7 ) = @_;
  my @motors       = @$aref1;
  my @wheels       = @$aref2;
  my @actual_pos   = @$aref3;
  my @closest_pos  = @$aref4;
  my @closest_name = @$aref5;
  my @difference   = @$aref6;
  my @motion       = @$aref7;

  #system("clear");
  print "\n\n";
  print 
">>>>>>>>>>>>>>>>>>   PRESENT STATUS OF WHEELS   <<<<<<<<<<<<<<<<\n";
  print 
"________________________________________________________________\n";

  print "\t\t\t ACTUAL\t\tCLOSEST LIKELY\n";
  print "MOTOR\tWHEEL\t\tPOSITION\tPOSITION\  NAME   DIFFERENCE  MOTION\n";
  print "_____\t_____\t\t________\t____________________________________\n";

  #PRINT OUT DECKER INFO
  print " $motors[0]\t$wheels[0]\t\t$actual_pos[0]\t\t";
  print "$closest_pos[0]\t$closest_name[0]\t     $difference[0]\t";
  print "     $motion[0]\n";

  #PRINT OUT MOS_SLIT INFO
  print " $motors[1]\t$wheels[1]\t$actual_pos[1]\t\t";
  print "$closest_pos[1]\t$closest_name[1]\t     $difference[1]\t";
  print "     $motion[1]\n";  
 
  #PRINT OUT FILTER, LYOT, GRISM INFO
  for( my $i = 2; $i < 5; $i++ ){
    print " $motors[$i]\t$wheels[$i]\t\t$actual_pos[$i]\t\t";
    print "$closest_pos[$i]\t$closest_name[$i]\t     $difference[$i]\t";  
    print "     $motion[$i]\n";
  }
  print 
"________________________________________________________________\n";

}#Endsub PrintOut




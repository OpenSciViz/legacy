#!/usr/local/bin/perl -w

use strict;

my $first_line = "#Please do not delete nor edit this file";

print "Enter the absolute pathname of the directory to which data will be writtne\n";
print ">>> CREATE DIR YOURSELF FIRST <<<\n";
print ">>>  INCLUDE TRAILING SLASH   <<<\n";
my $test_input = <STDIN>;
chomp $test_input;
print "test input is $test_input\n";

$test_input = "'" . $test_input . "'";
print "now $test_input\n";

my $new_dir = $test_input;

open OUT, ">original_file_dir.txt" or die "cannot open $!\n";
print OUT $first_line;
print OUT "\n";
print OUT $new_dir;
close OUT;

#rcsId = q($Name:  $ $Id: write_orig_file_dir.pl 14 2008-06-11 01:49:45Z hon $);

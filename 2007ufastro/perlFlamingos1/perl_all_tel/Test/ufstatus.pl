#!/usr/local/bin/perl -w
my $rcsId = q($Name:  $ $Id: ufstatus.pl 14 2008-06-11 01:49:45Z hon $);

BEGIN { $| = 1; print "Initializing\n"; }
END {print "not ok 1\n" unless $loaded;}

$loaded = 1;
#print "ok 1\n";

use strict;
use Getopt::Long;

my $DEBUG = 0;#0 = not debugging; 1 = debugging
GetOptions( 'debug' => \$DEBUG );

use Wheel qw/:all/;

#'Wheel'->require_version(0.01);
#print "Wheel constant is " . Wheel::PI . "\n";
#print "Wheel constant is " . PI . "\n";

my @motors    = qw( a b c d e );
my @wheels    = qw( decker mos_or_slit filter lyot grism );
#The following arrays will have 5 entries that correspond to @wheels
my ( @actual_pos, @closest_pos, @closest_name, @difference, @motion );

my @temps;
my @bias;

my $input;
my $loop = 1;

#system("clear");
while( $loop ){
  print "\nHit enter to go again, (x) to exit ";
  $input = <STDIN>;
  chomp( $input );

  if( $input eq 'x' ){
    print "exiting\n";
    $loop = 0;
  }else{
    #system("clear") unless $DEBUG;#equivalent to next line
    #system("clear") if !$DEBUG;
    #system("clear");

    #PUT flamingos configuration calls made through system here
    print "Asking the motor controllers for the present positions....\n\n";
    my @actual_pos = Get_Positions();
    print "\n\n";
    print "Asking if the motors are moving...\n\n";
    Get_Motion( \@motion );
    print "\n\n";
    print "Asking for array & mos temperatures...\n\n";
    @temps = Get_Temps();
    print "\n\n";
    print "Asking for the array bias...\n\n";
    @bias = Get_Bias();

    #Decker 
    ( $closest_pos[0], $closest_name[0], $difference[0] ) =
      det_nearest_posn( $actual_pos[0], 
			\@DECKER,       \@NEG_DECKER,
			\@DECKER_NAMES, \@DECKER_NEG_NAMES,
			\@DECKER_MDPTS, \@DECKER_NEG_MDPTS, $DECKER_REV );

    #Mos_slit
    ( $closest_pos[1], $closest_name[1], $difference[1] ) =
      det_nearest_posn( $actual_pos[1], 
			\@MOS_SLIT,       \@NEG_MOS_SLIT,
			\@MOS_SLIT_NAMES, \@MOS_SLIT_NEG_NAMES,
			\@MOS_SLIT_MDPTS, \@MOS_SLIT_NEG_MDPTS, $MOS_SLIT_REV );


    #Filter
    ( $closest_pos[2], $closest_name[2], $difference[2] ) =
      det_nearest_posn( $actual_pos[2], 
			\@FILTER,       \@NEG_FILTER,
			\@FILTER_NAMES, \@FILTER_NEG_NAMES,
			\@FILTER_MDPTS, \@FILTER_NEG_MDPTS, $FILTER_REV );
    
    
    #Lyot 
    ( $closest_pos[3], $closest_name[3], $difference[3] ) =
      det_nearest_posn( $actual_pos[3], 
			\@LYOT,       \@NEG_LYOT,
			\@LYOT_NAMES, \@LYOT_NEG_NAMES,
			\@LYOT_MDPTS, \@LYOT_NEG_MDPTS, $LYOT_REV );

    #Grism 
    ( $closest_pos[4], $closest_name[4], $difference[4] ) =
      det_nearest_posn( $actual_pos[4], 
			\@GRISM,       \@NEG_GRISM,
			\@GRISM_NAMES, \@GRISM_NEG_NAMES,
			\@GRISM_MDPTS, \@GRISM_NEG_MDPTS, $GRISM_REV );


    #Nearest_Positions( \@actual_pos, \@closest_pos,
    #                   \@closest_name, \@difference );

    Print_Out( \@motors, \@wheels, 
	       \@actual_pos, \@closest_pos, 
	       \@closest_name, \@difference, \@motion,
	       \@temps,  \@bias);

  }

}#End of while loop

#print "Out of loop\n";

##end of main code
##subs follow

sub Get_Positions{

  my $reply;

  my $test_file  = ( 'wheel.txt');
  my @positions = ( 0, 0 , 0 , 0, 0 );
  my ($line, @list, %positions);

  if( $DEBUG ){
      print "Opening debug file containing motor positions\n\n";
      open( FILE, $test_file ) || die "Cannot find $test_file\n"; 
      while( <FILE> ){
	  #print "Line number $.: $_";
	  $_ =~ s/^(a|b|c|d|e)Z //;
	  chomp;
	  if( $. == 1 ){$positions[0] = $_}
	  if( $. == 3 ){$positions[1] = $_}
	  if( $. == 5 ){$positions[2] = $_}
	  if( $. == 7 ){$positions[3] = $_}
	  if( $. == 9 ){$positions[4] = $_}
      }#end while
      close( FILE );

      for( my $i = 0; $i < 5; $i++){
	  print "$positions[$i] \n";
      }
  }elsif( !$DEBUG ){
    my $str_quote           = "\"";
    my $space               = " ";
    my $cmd_head = $ENV{UFMOTOR}.$space.
                   $ENV{CLIENT_HOST}.$space.$ENV{HOST}.$space.
	           $ENV{CLIENT_PORT}.$space.$ENV{CLIENT_UFMOTOR_PORT}.$space.
		   $ENV{CLIENT_QUIET}.$space;


    #GET MOTOR ALL POSITIONS AT ONCE
    my $cmd_str = $cmd_head . $str_quote."aZ & bZ & cZ & dZ & eZ".$str_quote;
    print "Executing\n$cmd_str\n";
    my $reply = `$cmd_str`;
    sleep 1.0;

    my $a_index = index $reply, 'aZ';
    my $b_index = index $reply, 'bZ';
    my $c_index = index $reply, 'cZ';
    my $d_index = index $reply, 'dZ';
    my $e_index = index $reply, 'eZ';

    my $l_index = length $reply;

    #print "a at $a_index\n".
    #  "b at $b_index\n".
    #	"c at $c_index\n".
    #	  "d at $d_index\n".
    #	    "e at $e_index\n";


    my $a_substr = substr $reply, $a_index, $b_index - $a_index;
    my $b_substr = substr $reply, $b_index, $c_index - $b_index;
    my $c_substr = substr $reply, $c_index, $d_index - $c_index;
    my $d_substr = substr $reply, $d_index, $e_index - $d_index;
    my $e_substr = substr $reply, $e_index, $l_index - $e_index;

    my $a_dot = index $a_substr, "\.";
    my $a_val = substr $a_substr, 2, $a_dot - 2;

    my $b_dot = index $b_substr, "\.";
    my $b_val = substr $b_substr, 2, $b_dot -2;

    my $c_dot = index $c_substr, "\.";
    my $c_val = substr $c_substr, 2, $c_dot -2;

    my $d_dot = index $d_substr, "\.";
    my $d_val = substr $d_substr, 2, $d_dot -2;

    my $e_dot = index $e_substr, "\.";
    my $e_val = substr $e_substr, 2, $e_dot -2;

    #print "a substr is $a_substr\n";
    #print "b substr is $b_substr\n";
    #print "c substr is $c_substr\n";
    #print "d substr is $d_substr\n";
    #print "e substr is $e_substr\n";

    #print "a_val, up to decimal point, is $a_val\n";
    #print "b_val, up to decimal point, is $b_val\n";
    #print "c_val, up to decimal point, is $c_val\n";
    #print "d_val, up to decimal point, is $d_val\n";
    #print "e_val, up to decimal point, is $e_val\n";

    $positions[0] = $a_val;
    $positions[1] = $b_val;
    $positions[2] = $c_val;
    $positions[3] = $d_val;
    $positions[4] = $e_val;

  }#end if debug

  return @positions;
  
}#endsub Get_Positions


sub Get_Motion{
  my ( $aref_moving ) = @_;
  my ( $reply, $interpretation );

  if( $DEBUG ){
    for( my $i = 0; $i < 5; $i++ ){
    $$aref_moving[$i] = "Stationary";
  }
		  
  }elsif( !$DEBUG ){
    my $str_quote           = "\"";
    my $space               = " ";
    my $cmd_head = $ENV{UFMOTOR}.$space.
                   $ENV{CLIENT_HOST}.$space.$ENV{HOST}.$space.
	           $ENV{CLIENT_PORT}.$space.$ENV{CLIENT_UFMOTOR_PORT}.$space.
		   $ENV{CLIENT_QUIET}.$space;

    #GET All MOTOR MOTIONS AT ONCE
    #print "Getting motor a motion\n";
    my $cmd_str = $cmd_head .$str_quote."a^ & b^ & c^ & d^ & e^".$str_quote;
    #print "Doing $cmd_str\n";
    $reply = `$cmd_str`;
    sleep 1.0;
    #print $reply . "\n";

    my $a_index = index $reply, 'a';
    my $b_index = index $reply, 'b';
    my $c_index = index $reply, 'c';
    my $d_index = index $reply, 'd';
    my $e_index = index $reply, 'e';

    my $l_index = length $reply;

    #print "a at $a_index\n".
    #  "b at $b_index\n".
    #	"c at $c_index\n".
    #	  "d at $d_index\n".
    #	    "e at $e_index\n";

    my $a_substr = substr $reply, $a_index, $b_index - $a_index;
    my $b_substr = substr $reply, $b_index, $c_index - $b_index;
    my $c_substr = substr $reply, $c_index, $d_index - $c_index;
    my $d_substr = substr $reply, $d_index, $e_index - $d_index;
    my $e_substr = substr $reply, $e_index, $l_index - $e_index;

    my $a_dot = index $a_substr, "\n";
    my $a_val = substr $a_substr, 2, $a_dot - 2;

    my $b_dot = index $b_substr, "\n";
    my $b_val = substr $b_substr, 2, $b_dot -2;

    my $c_dot = index $c_substr, "\n";
    my $c_val = substr $c_substr, 2, $c_dot -2;

    my $d_dot = index $d_substr, "\n";
    my $d_val = substr $d_substr, 2, $d_dot -2;

    my $e_dot = index $e_substr, "\n";
    my $e_val = substr $e_substr, 2, $e_dot -2;

    #print "a substr is $a_substr\n";
    #print "b substr is $b_substr\n";
    #print "c substr is $c_substr\n";
    #print "d substr is $d_substr\n";
    #print "e substr is $e_substr\n";

    #print "a_val is $a_val\n";
    #print "b_val is $b_val\n";
    #print "c_val is $c_val\n";
    #print "d_val is $d_val\n";
    #print "e_val is $e_val\n";

    my $interpretation;
    $interpretation = Interpret_Motion($a_val);
    $$aref_moving[0] = $interpretation;

    $interpretation = Interpret_Motion($b_val);
    $$aref_moving[1] = $interpretation;

    $interpretation = Interpret_Motion($c_val);
    $$aref_moving[2] = $interpretation;

    $interpretation = Interpret_Motion($d_val);
    $$aref_moving[3] = $interpretation;

    $interpretation = Interpret_Motion($e_val);
    $$aref_moving[4] = $interpretation;

  }#end if debug

}#Endsub Get_Motion


sub Interpret_Motion{
  my $input = $_[0];

  #print "input is $input\n";
  if( $input == 0  ){ return "Stationary" }
  if( $input == 1  ){ return "Moving"     }
  if( $input == 10 ){ return "Homing, Constant Vel" }
  if( $input == 42 ){ return "Homing, Constant Vel, Ramping" }

}#Endsub Interpret_Motion


sub Get_Temps{
  
  my $reply = `uflsc -q 1`;
  chomp( $reply );
  my @temps = split / /, $reply;
  shift @temps;
  $temps[0] =~ s/^1,//;
  $temps[1] =~ s/^6,//;
  #print "array = $temps[0], mos = $temps[1]\n";

  return @temps;

}#Endsub Get_Temps


sub Get_Bias{
  
  my $reply = `ufdc -q dac_dump_bias`;
  #print $reply . "\n";
  my @input_array = split /\.\.\.\./, $reply;
  my @split_input = split /=/, $input_array[1];
  my @resplit_input = split /\(/, $split_input[3];
  my $bin_bias = $resplit_input[0];
  #print( $bin_bias / 255 . "\n" );

  my @bias;
  $bias[0] = $bin_bias / 255;

  if(  $bias[0] == 1    ){ $bias[1] = "imaging" };
  if(  $bias[0] == 0.75 ){ $bias[1] = "spectroscopy" };
  if( ($bias[0] != 1) && ($bias[0] != 0.75) ){ $bias[1] = "User's choice"};

  return @bias;

}


sub det_nearest_posn{
  my ($pos,         $aref_wval, $aref_neg_wval,
      $aref_wnam,   $aref_neg_wnam,
      $aref_wmdpts, $aref_wnegmdpts, $wheel_rev ) = @_;
  my $guess_index = 0;
  my @wheel_vals  = @$aref_wval;
  my @wheel_neg_vals = @$aref_neg_wval;
  my @wheel_names = @$aref_wnam;
  my @wheel_neg_names = @$aref_neg_wnam;
  my @wheel_mdpts = @$aref_wmdpts;
  my @wheel_neg_mdpts = @$aref_wnegmdpts;

  my $num_posn    = @wheel_names;

  my $closest_pos;
  my $closest_name;
  my $diff;

  #print "\npos=$pos, num_pos=$num_posn\n";
  my $actual_pos = $pos;
  if( $pos >= 0 ){
    #print ">>>Checking against positives\n";
    if( $pos > $wheel_rev ){
      my $is_valid=0;
      until( $is_valid ){
	$pos = $pos - $wheel_rev;
	if( $pos <= $wheel_rev ){
	  $is_valid = 1;
	}
      }
    }
    if( $pos >= 0  and $pos < $wheel_mdpts[0] ){
      $guess_index = 0;
    }else{
      for( my $i = 1; $i < $num_posn; $i++ ){
	#Positive vals first
	#print "i=$i, guess_index=$guess_index:  ";
	#print "checking posn=$pos against hp".($i-1).
	#      "=$wheel_mdpts[$i-1] and hp$i=$wheel_mdpts[$i]\n";

	if( ($pos >= $wheel_mdpts[$i - 1]) and 
	    ($pos < $wheel_mdpts[$i] ) ){
	  #print "in if $i ";
	  $guess_index = $i;
	}elsif( $pos > $wheel_mdpts[$num_posn - 1] and 
		$pos <= $wheel_rev ){
	  #This checks between last position and home
	  #print "in last check\n";
	  #print "pos = $pos\n";
	  #print "lastpos = $wheel_neg_mdpts[$num_posn - 1]\n";
	  #print "wrev    = $wheel_rev\n";
	  $guess_index = 0;
	}
      }
    }    

    $closest_pos  = $wheel_vals[$guess_index];
    $closest_name = $wheel_names[$guess_index];
    $diff         = $actual_pos - $wheel_vals[$guess_index];

  }elsif( $pos < 0 ){
    #print ">>>Checking against negatives\n";
    my $neg_wheel_rev = -1 * $wheel_rev;

    if( $pos < $neg_wheel_rev ){
      my $is_valid=0;
      until( $is_valid ){
	$pos = $pos - $neg_wheel_rev;
	if( $pos >= $neg_wheel_rev ){
	  $is_valid = 1;
	}
      }
    }
    #print "pos now=$pos\n";
    if( $pos <= 0  and $pos > $wheel_neg_mdpts[0] ){
      $guess_index = 0;
    }else{
      for( my $i = 1; $i < $num_posn; $i++ ){
	  #print "i=$i, guess_index=$guess_index:  ";
	  #print "checking posn=$pos against hp".($i-1).
	  #  "=$wheel_neg_mdpts[$i-1] and hp$i=$wheel_neg_mdpts[$i]\n";
	if( $pos <= $wheel_neg_mdpts[$i - 1] and 
	    $pos > $wheel_neg_mdpts[$i] ){
	  $guess_index = $i;
	}elsif( $pos < $wheel_neg_mdpts[$num_posn - 1] and 
		$pos >= $wheel_rev ){
	  $guess_index = 0;
	}
      }
    }    

    $closest_pos  = $wheel_neg_vals[$guess_index];
    $closest_name = $wheel_neg_names[$guess_index];
    $diff         = $actual_pos - $wheel_neg_vals[$guess_index];

  }#End ifelse pos and neg



  #print "\n\n$closest_pos=$closest_name, $diff\n\n";

  return( $closest_pos, $closest_name, $diff );

}#Endsub det_nearest_posn


sub Print_Out{
  my ( $aref1, $aref2, $aref3, $aref4, $aref5, 
       $aref6, $aref7, $aref8, $aref9 ) = @_;
  my @motors       = @$aref1;
  my @wheels       = @$aref2;
  my @actual_pos   = @$aref3;
  my @closest_pos  = @$aref4;
  my @closest_name = @$aref5;
  my @difference   = @$aref6;
  my @motion       = @$aref7;
  my @temps        = @$aref8;
  my @bias         = @$aref9;

  #system("clear");

  print "\n\n";
  print " ARRAY BIAS\n";
  print "_____________\n";
  printf("%1\.2f", $bias[0]);
  print " ($bias[1])\n";

  print "\n\n";
  print "    TEMPERATURES\n";
  print "_____________________\n";
  print "ARRAY      =  $temps[0] K\n";
  print "MOS DEWAR  =  $temps[1] K\n";

  print "\n\n";
  print "\t\t\t ACTUAL\t\tCLOSEST LIKELY\n";
  print "MOTOR\tWHEEL\t\tPOSITION\tPOSITION\  NAME   DIFFERENCE  MOTION\n";
  print "_____\t_____\t\t________\t____________________________________\n";

  #PRINT OUT DECKER INFO
  print " $motors[0]\t$wheels[0]\t\t$actual_pos[0]\t\t";
  print "$closest_pos[0]\t$closest_name[0]\t     $difference[0]\t";
  print "     $motion[0]\n";

  #PRINT OUT MOS_SLIT INFO
  print " $motors[1]\t$wheels[1]\t$actual_pos[1]\t\t";
  print "$closest_pos[1]\t$closest_name[1]\t     $difference[1]\t";
  print "     $motion[1]\n";  
 
  #PRINT OUT FILTER, LYOT, GRISM INFO
  for( my $i = 2; $i < 5; $i++ ){
    print " $motors[$i]\t$wheels[$i]\t\t$actual_pos[$i]\t\t";
    print "$closest_pos[$i]\t$closest_name[$i]\t     $difference[$i]\t";  
    print "     $motion[$i]\n";
  }

  print "\n\n";

}#Endsub PrintOut




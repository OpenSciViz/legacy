#!/usr/local/bin/perl -w

my $rcsId = q($Id: z.old.oneimage.newmmt.pl 14 2008-06-11 01:49:45Z hon $);

use strict;
use Getopt::Long;

my $hostnm = `hostname`;
$hostnm =~ s/\n//g;
my $ctlhost = $hostnm;
my $frmhost = $hostnm;


my $reply;

usage();
#die "USAGE: \n"   unless @ARGV > 0;

my $testing = 0;#0 = running; 1 = testing


##########VALID OPTIONS PRESENTED TO USER
my $filenm = "";
my $lut    = "";
my $fits   = "";
my $index  = "";
my $expt   = "";
my $nread  = "";
my $timeout_option = "";
my $do_not_setup_ct = "";

$filenm = argval('-file');
$lut = argval('-lut');
$fits = argval('-fits');
$index = argval('-index');
$expt = argval('-expt');
$nread = argval('-reads');
$timeout_option  = argval( '-timeout');
$do_not_setup_ct = argval( '-ct');
print "\n\nct is $do_not_setup_ct\n";

my $net_edt_timeout = 0;
my $nread_timeout    = 1;
my $fiddle_timeout   = 5;
my $lut_apply_timeout = 30;

if( !$filenm && !$lut && !$fits && !$index && !$expt && !$nread && !$timeout_option && !$do_not_setup_ct){
  usage();
  exit 1;
}

##########SET DEFAULT OPTION VALUES
if( $filenm eq "false" || $filenm eq "true" ) { # use default file name
  $filenm = "/data1/data";
}else{
  #$filenm = $filenm . ".";
}
die "please provide full path for image file!" unless index($filenm, '/') == 0;


if( $lut eq "false" || $lut eq "true" ) { # use default file name
  print "filename tests\n";
  if( $filenm =~ m/^\/data1\// ){
    print "disk matches /data1\n";
    $lut = "/data1/flamingos/Flamingos.lut";
  }elsif( $filenm =~ m/^\/data2\// ){
    print "disk matches /data2\n";
    $lut = "/data2/flamingos/Flamingos.lut";
  }else{
    die "neither data disk (/data1 or /data2) specified as location to write data\n";  
  }

}
die "please provide full path for lut file!" unless index($lut, '/') == 0;


if( $fits eq "false" || $fits eq "true" ) { # use default file name
  print "fit tests\n";
  if( $filenm =~ m/^\/data1\// ){
    print "disk matches /data1\n";
    $fits = "/data1/flamingos/Flamingos.fitsheader";
  }elsif( $filenm =~ m/^\/data2\// ){
    print "disk matches /data2\n";
    $fits = "/data2/flamingos/Flamingos.fitsheader";
  }else{
    die "neither data disk (/data1 or /data2) specified as location to write data\n";  
  }
}
die "please provide full path for fits file!" unless index($fits, '/') == 0;


if( $index eq "false" || $index eq "true"  ) { # use default start index
  $index = 1;
}


if( $expt eq "false" || $expt eq "true" ) { # use default exposure time
  $expt = 2;
}

if( $nread eq "false" || $nread eq "true" ) { # use default read cnt
  $nread = 1;
}

if( $timeout_option eq "false" || $timeout_option eq "true" ){
  $nread_timeout  = $nread;
  #$net_edt_timeout = $expt + $nread_timeout + $lut_apply_timeout + $fiddle_timeout;
  $net_edt_timeout = $expt + $nread_timeout  + $fiddle_timeout;
}

my $setup_ct;
if( $do_not_setup_ct eq "false" || $do_not_setup_ct eq "true" ){
  $setup_ct = 1;
  print "Will setup mce4 cycle types each time\n";
}else{
  $setup_ct = 0;
  print "Will _not_ setup mce4 scycle types each time\n";
}


my $nfrm = argval('-frames');#frames sequentially spit by mce; not a user option
if( $nfrm eq "false" || $nfrm eq "true"  ) { # use default frame cnt
  $nfrm = 1;
}

if( $index > 9999 ) {
    die "Can't take more than 9999 images with the same name\n";
}


##########Have these file names already been used?
my $i=1;
for($i = $index; $i <= $index + ($nfrm - 1); $i += 1){
    my $matchname = $filenm . ".";
  if( $i > 0 && $i <= 9){
    $matchname = $matchname . "000$i.fits";
    if( -e $matchname ){
      die "File $matchname already exists:\n$!\n";
    }
  }elsif( $i > 9 && $i <= 99){
    $matchname = $matchname . "00$i.fits";
    if( -e $matchname ){
      die "File $matchname already exists:\n$!\n";
    }
  }elsif( $i > 99 && $i <= 999 ){
    $matchname = $matchname . "0$i.fits";
    if( -e $matchname ){
      die "File $matchname already exists:\n$!\n";
    }
  }elsif( $i > 999 && $i <= 9999){
    $matchname = $matchname . "$i.fits";
    if( -e $matchname ){
      die "File $matchname already exists:\n$!\n";
    }
  }
}

#print "nread is $nread\n";
#print "expt is $expt\n";
die "reads == 2 not supported\n" unless $nread != 2; 
die "exposure time ( $expt ) too short for reads == $nread\n" unless $expt >= (2 * $nread);

$nread = $nread-1;
my $ct = 42;
if( $nread == 0 ) {
  $ct = 40;
}

##########FITSHEADER STUFF
#Run Frank's code to get tcs info

if( !$testing ){

  use vars qw /$CAMERA_PERL_LIBS $LibFitsHeader/;
  $CAMERA_PERL_LIBS = $ENV{"CAMERA_PERL_LIBS"};
  $LibFitsHeader = $CAMERA_PERL_LIBS . 'LibFitsHeader.pl';
  
  if( -e $LibFitsHeader and -r $LibFitsHeader )
    {
      do $LibFitsHeader;   #compiles subs for manipulating FITS header.
    }
  else { print "\n file $LibFitsHeader does not exist or is not readable\n" }
  
  my $FITSheaderFile = define_FH_file("Flamingos.fitsheader");
  print "\nReading FITS header from: $FITSheaderFile ...\n";
  
  readFitsHeader( $FITSheaderFile );
  
  print "\nFetching telescope information and adding to FITS header...\n";
  my $telescope = param_value("TELESCOP");
  if( ($telescope =~ m/kp2m/i) or ($telescope =~ m/kp4m/i) ){

    get_kptcs_info_into_fh();
    
  }elsif( $telescope =~ m/mmt/i ){

    get_mmt_tcs_info_into_fh();

  }elsif( $telescope =~ m/gem/i ){


  }else{
   print "Not at a known telescope to get any TCS info\n\n";
  }

}else{
  print "Testing. Should be writing tcs info into header\n";
}

my $quote = "\"";
##########STOP PREVIOUS CYCLE TYPE and RESET THE uffrmstatus SEMAPHORES
#my $stop = "/usr/local/uf/bin/ufdc -q -raw " . $quote . "stop" . $quote;
#my $stop = "/usr/local/uf/bin/ufdc -raw " . $quote . "stop" . $quote;
#print "do $stop\n";
#
#if( !$testing ){
#  $reply = `$stop`; 
#  system("ufsleep 1.0");
#}
#else{
#  print "Testing.  Should be stopping previous CT\n";
#}


##########SETUP MCE
my $setmce = "";
#my $setmce_raw = "ufdc -q -host $ctlhost -raw "; 
my $setmce_raw = "ufdc -host $ctlhost -raw "; 
#$setmce = $setmce_raw . $quote . "sim 0 & PBC 10 & LDVAR 0 10 & LDVAR 1 1000 & ";
#$setmce = "$setmce LDVAR 2 $nread & LDVAR 10 $nfrm & TIMER 0 $expt S &";
#$setmce = "$setmce CT $ct" . $quote;
$setmce = $setmce_raw . $quote . "LDVAR 2 $nread & LDVAR 10 $nfrm & TIMER 0 $expt S & ";
$setmce = $setmce . "CT $ct" . $quote;
print $setmce . "\n";


if( $setup_ct ){

  print "submit: $setmce\n";
  
  if( !$testing ){
    $reply = `$setmce`;
    system("ufsleep 1.0");
    print "$reply\n";
  }else{
    print "testing\n";
  }
  
}#if true, setup cycle types, else do nothing

###########INVOKE FRAME GRAB WITH DAVID'S NEW DAEMON

my $acquire = $quote . 
              "start -ds9 -cnt $nfrm -index $index -lut $lut -fits $fits -file $filenm " . 
              "-timeout $net_edt_timeout" .
              $quote;
#-jpeg -png options available, too

my $grab_via_mce = "ufdc -acq $acquire";

print "\nObservation starting.\a\a\a\a\a\n";
if( !$testing ){
  print "\n\n\nExecuting $grab_via_mce\n";
  print "\nIf it hangs here for more than ~6 seconds, type ufdc -q status in another flamingos1b window\n\n\n";
  $reply = `$grab_via_mce`;
  system("ufsleep 0.1");
  print "$reply\n";
}else{
  print "Testing.  Should be issuing grab_via_mce\n";
}

my $uffrmstatus = "uffrmstatus";
my $frame_status_reply = `$uffrmstatus`;
print "\n\n\n";

my $idle_notLutting = 0;
my $sleep_count = 1;
my $lut_timeout = 30;
while( $idle_notLutting == 0 ){
  sleep 1;

  $frame_status_reply = `$uffrmstatus`;
  $idle_notLutting = grep( /idle: true, applyingLUT: false/, $frame_status_reply);

  if( $idle_notLutting == 0 ){
    print ">>>>>.....TAKING AN IMAGE OR APPLYING LUT.....\n";
  }elsif( $idle_notLutting ==1 ){
    print ">>>>>.....EXPOSURE DONE.....\n";
  }

  if( $timeout_option eq "false" || $timeout_option eq "true" ){

    if( $sleep_count < $expt + $nread ){

      print "Have slept $sleep_count seconds out of an exposure time of " . ($expt + $nread) ." seconds\n";
      print "(Exposure time + number of reads = $expt + $nread)\n";

    }elsif( $sleep_count >= $expt + $nread ){

      print "\a\a\a\a\a\n\n";
      print ">>>>>>>>>>..........EXPOSURE TIME ELAPSED..........<<<<<<<<<<\n";
      print "If not applying LUT (in MCE4 window), hit ctrl-Z, type ufstop.pl -stop -clean, then type fg to resume\n";
      print "If the ufstop.pl command hangs, hit ctrl-c, and try ufstop.pl -clean only, then type fg to resume\n";

      print "Have slept " . ($sleep_count - ($expt + $nread)) . " seconds past the exposure time\n";
      print "Applying lut may take an additional 30 seconds\n\n";
      
     }elsif( $sleep_count > $expt + $nread + $lut_timeout ){

       print "\a\a\a\a\a\n\n";
       print ">>>>>>>>>>..........EXITING--IMAGE MAY NOT BE COMPLETE..........<<<<<<<<<<\n";

       if( !$testing ){
	 #stop take
	 my $stop_take = "ufstop.pl -stop";
	 system( $stop_take );
	 #reset mce4 to ct 0
	 system("ufidle.pl");
       }else{
	 print "Testing.  Should be issuing ufstop.pl -stop\n";
	 print ;
       }
       exit;
       
     }#end time expired
    
  }else{
        print "Have slept $sleep_count seconds out of $timeout_option requested with the -timeout option\n";
  }#end timeout true/false

  $sleep_count += 1;

}#end while idle not lutting


print ">>>>>EXPOSURE DONE<<<<< \a\a\a\a\a\n\n\n";

##
##SHOULD RESET MCE4 TO CT 0, AND RUNNING
##Set CT 0
#
#system("ufidle.pl");
#
exit;


######################### subs ################################
sub usage{
 my $msg = "
oneimage.pl [-file aboslute_path/file_name_prefix]    (required)
            [-expt exposure time in integer seconds]  (required)
            [-index start #]                          (defaults to 1)

            [-reads # of reads]                       (optional; default is 1)
            [-frames # frames]                        (not yet used)
";

 my $argc = @ARGV;
 if( $argc eq 0){
   die("USAGE: \n" . $msg);
 }

 my $div2 = $argc / 2;
 my $mod2 = int( $argc / 2);
 my $remainder = $div2 - $mod2;

 if( $remainder != 0 ){
   print "\nSupply values for all options!\n\n";
   die( "USAGE: \n" . $msg );
 }

 my $i;
 my $option;
 my $fileopt   = "-file";
 my $exptopt   = "-expt";
 my $indexopt  = "-index";
 my $readsopt  = "-reads";
 my $framesopt = "-frames";
 my $ctopt     = "-ct";

 #print "argc is $argc\n";
 for( $i = 0; $i <= ($argc - 1); $i += 2){
   $option = $ARGV[$i];
   #print "testing $option\n";
   
   my $reply = grep( m/^$fileopt/, $option );
   $reply += grep( m/^$exptopt/, $option );
   $reply += grep( m/^$indexopt/, $option );
   $reply += grep( m/^$readsopt/, $option );
   $reply += grep( m/^$framesopt/, $option );
   $reply += grep( m/^$ctopt/, $option );
   
   if( $reply == 0 ){
     print "\n\n$option IS NOT A VALID OPTION\n\n";
     die( "USAGE: \n" . $msg );
     #
   }
 }#endfor


}#endsub usage

sub argval {
  my ( $arg ) = @_;
  my $argc = @ARGV;
  my $a;
  my $b;
  my $idx = index($arg, '-');
  die "please use dashes for all args!\n" unless $idx == 0;
  my $i;
  for( $i = 0; $i < $argc; $i++ ) {
    $a = $ARGV[$i];
    if( $a eq $arg ) {
      if( $i == $argc - 1 ) {
	return "true"; # last one
      }
      $b = $ARGV[1 + $i]; # check next for dash
      $idx = index($b, '-');
      if( $idx == 0 ) {
	die "$a must have an associated value"; #DO not allow boolean
      }
      else {
	#print "testing that $b is the correct type of value for $a\n";
	if($a eq "-file" ){
	  return $b; # return value;
	}elsif( $a eq "-expt" || $a eq "-index" || $a eq "-reads" || 
		$a eq "-frames" || $a eq "-ct"){
	  my $gtest = grep( m/^(\d+)$/, $b );
	  #print "grep says gtest  = $gtest\n";
	  if( $gtest == 0  ){
	    return "true";
	    #die "\n$a must have a numeric value\n\n";
	  }elsif($gtest == 1){
	    return $b;
	  }   
	}       
      }#end main else
    }#end if loop
  }#end for loop
  return "false"; # never found it
}#endsub argval


sub get_kptcs_info_into_fh{
  use vars qw /$CAMERA_PERL_LIBS $LibFitsHeader/;
  $CAMERA_PERL_LIBS = $ENV{"CAMERA_PERL_LIBS"};
  $LibFitsHeader = $CAMERA_PERL_LIBS . 'LibFitsHeader.pl';
  
  #if( -e $LibFitsHeader and -r $LibFitsHeader )
  #  {
  #    do $LibFitsHeader;   #compiles subs for manipulating FITS header.
  #  }
  #else { print "\n file $LibFitsHeader does not exist or is not readable\n" }
  
  my $FITSheaderFile = define_FH_file("Flamingos.fitsheader");
  print "\nReading FITS header from: $FITSheaderFile ...\n";
  
  readFitsHeader( $FITSheaderFile );
  
  print "\nFetching telescope information and adding to FITS header...\n";
  my $telescope = param_value("TELESCOP");
  if( index( $telescope, 'kp' ) == 0 ) { $telescope = substr($telescope,2,2) }
  print "Telescope = $telescope\n";
  
  addTCStoFH( $telescope );
  
  print "\nNew parameters in FITS header:\n";
  
  printFitsHeader("RA","DEC","epoch");
  printFitsHeader("RA_APR","DEC_APR","HA");
  printFitsHeader("UTIME","STIME","date","time");
  printFitsHeader("AIRMASS","ZEN_DIST");
  
  print "\nWriting FITS header back to: $FITSheaderFile ...\n";
  writeFitsHeader( $FITSheaderFile );
  
  my $dothis1 = "cp $FITSheaderFile /data1/flamingos ";
  my $dothis2 = "cp $FITSheaderFile /data2/flamingos ";
  system($dothis1);
  print "Copying FITS header to /data1/flamingos \n";
  system($dothis2);
  print "Copying FITS header to /data2/flamingos \n";
  
}#endsub get_tcs_info_into_fh


sub get_mmt_tcs_info_into_fh{

  use IO::Socket;

  ####TCS CONNECT###
  my $ops_sock = tcsops_connect();
  die "Unable to connect to tcsops host (Reason: $!)\n" unless $ops_sock;

  ###GET TCS INFO  
  my @ops_reply;
  @ops_reply = tcs_info(\$ops_sock);
  print "tcsops reply:\n @ops_reply\n";
  close( $ops_sock );

  my $EPOCH    = $ops_reply[0];
  my $RA       = $ops_reply[1]; my $DEC      = $ops_reply[2];
  my $UTIME    = $ops_reply[3]; my $STIME    = $ops_reply[4];
  my $HA       = $ops_reply[5]; my $AIRMASS  = $ops_reply[6]; 
  my $ZEN_DIST = $ops_reply[7]; my $INSTAZ   = $ops_reply[8];
  my $INSTEL   = $ops_reply[9]; my $ROTANGLE = $ops_reply[10]; 
  my $TELFOCUS = $ops_reply[11]; 
  my $TELESCOP = $ops_reply[12]; my $OBSERVAT = $TELESCOP . "O";
  my $PA       = $ops_reply[13]; my $MJD      = $ops_reply[14];

  #use vars qw /$CAMERA_PERL_LIBS $LibFitsHeader/;
  #$CAMERA_PERL_LIBS = $ENV{"CAMERA_PERL_LIBS"};
  #$LibFitsHeader = $CAMERA_PERL_LIBS . 'LibFitsHeader.pl';
  
  #if( -e $LibFitsHeader and -r $LibFitsHeader )
  #  {
  #    do $LibFitsHeader;   #compiles subs for manipulating FITS header.
  #  }
  #else { print "\n file $LibFitsHeader does not exist or is not readable\n" }
  
  my $FITSheaderFile = define_FH_file("Flamingos.fitsheader");
  print "\nReading FITS header from: $FITSheaderFile ...\n";
  
  readFitsHeader( $FITSheaderFile );
  setStringParam( "EPOCH",    $EPOCH    );
  setStringParam( "RA",       $RA       ); 
  setStringParam( "DEC",      $DEC      );
  setStringParam( "UTIME",    $UTIME    ); 
  setStringParam( "STIME",    $STIME    );
  setStringParam( "HA",       $HA       );
  setStringParam( "AIRMASS",  $AIRMASS  ); 
  setStringParam( "ZEN_DIST", $ZEN_DIST );
  setStringParam( "INSTAZ",   $INSTAZ   );
  setStringParam( "INSTEL",   $INSTEL   );
  setStringParam( "ROTANGLE", $ROTANGLE );
  setStringParam( "TELFOCUS", $TELFOCUS );
  setStringParam( "TELESCOP", $TELESCOP );
  setStringParam( "OBSERVAT", $OBSERVAT );
  setStringParam( "PA      ", $PA       );
  setStringParam( "MJD     ", $MJD      );

  my $date = `date -u '+%Y-%m-%d'`;
  chomp( $date );
  setStringParam("DATE-OBS", $date);
  
  my $time = `date -u '+%H:%M:%S'`;
  chomp( $time );
  setStringParam("TIME-OBS",$time);

  #UTC time, LST, Epoch, Hour Angle, Airmass, and Zenith Distance 
  #would be good, too.

  #print "\nNew parameters in FITS header:\n";
  
  printFitsHeader("RA","DEC");
  printFitsHeader("UTIME");
  printFitsHeader("date-obs","time-obs");
    
  print "\nWriting FITS header back to: $FITSheaderFile ...\n";
  writeFitsHeader( $FITSheaderFile );
  
  my $dothis1 = "cp $FITSheaderFile /data1/flamingos ";
  my $dothis2 = "cp $FITSheaderFile /data2/flamingos ";
  system($dothis1);
  print "Copying FITS header to /data1/flamingos \n";
  system($dothis2);
  print "Copying FITS header to /data2/flamingos \n";


}#Endsub get_mmt_tcs_info_into_fh


sub tcsops_connect {
  my $port = 5403; # brian mccloud's service
  #my $host = "packrat";
  #my $host = "128.196.100.5";
  my $host = "hacksaw";
  my $sock = IO::Socket::INET->new (
			PeerAddr => $host,
			PeerPort => $port,
			Proto => 'tcp');

  print "sock is $sock\n";
  return $sock;
}#Endsub tcsops_connect

sub tcs_info {
  #my $sock = $_;
  my ($stref) = @_;
  my $sock = $$stref;
  print "tcs_info sock is $sock\n";

  ###Get the data###
  my $want;
  $want = "1 get epoch";     my $epoch     = tcsops( \$want, $sock );
  $want = "1 get ra";        my $ra        = tcsops( \$want , $sock );
  $want = "1 get dec";       my $dec       = tcsops( \$want, $sock );
  $want = "1 get ut";        my $utime     = tcsops( \$want, $sock );
  $want = "1 get lst";       my $stime     = tcsops( \$want, $sock );
  $want = "1 get ha";        my $ha        = tcsops( \$want, $sock );
  $want = "1 get airmass";   my $airmass   = tcsops( \$want, $sock );
  $want = "1 get thetaz";    my $zen_dist  = tcsops( \$want, $sock );
  $want = "1 get instazoff"; my $instazoff = tcsops( \$want, $sock );
  $want = "1 get insteloff"; my $insteloff = tcsops( \$want, $sock );
  $want = "1 get rot";       my $rot       = tcsops( \$want, $sock );
  $want = "1 get focus";     my $telfocus  = tcsops( \$want, $sock );
  $want = "1 get telname";   my $telname   = tcsops( \$want, $sock );
  $want = "1 get pa     ";   my $parangle  = tcsops( \$want, $sock );
  $want = "1 get mjd    ";   my $mjd       = tcsops( \$want, $sock );

  ###Get rid of extraneous characters###
  my @field; 
  $epoch =~ s/^(\s*)1 ack //;     @field = split( /(\s+)/, $epoch ); 
  $epoch = $field[0] ;

  $ra =~ s/^(\s*)1 ack //;        @field = split( /(\s+)/, $ra );  
  $ra = $field[0];

  $dec =~ s/^(\s*)1 ack //;       @field = split( /(\s+)/, $dec ); 
  $dec = $field[0];

  $utime =~ s/^(\s*)1 ack //;     @field = split( /(\s+)/, $utime );  
  $utime = $field[0];

  $stime =~ s/^(\s*)1 ack //;       @field = split( /(\s+)/, $stime ); 
  $stime = $field[0];

  $ha  =~ s/^(\s*)1 ack //;       @field = split( /(\s+)/, $ha ); 
  $ha  = $field[0];

  $airmass =~ s/^(\s*)1 ack //;   @field = split( /(\s+)/, $airmass ); 
  $airmass = $field[0];

  $zen_dist =~ s/^(\s*)1 ack //;  @field = split( /(\s+)/, $zen_dist ); 
  $zen_dist = $field[0];

  $instazoff =~ s/^(\s*)1 ack //; @field = split( /(\s+)/, $instazoff); 
  $instazoff = $field[0];

  $insteloff =~ s/^(\s*)1 ack //; @field = split( /(\s+)/, $insteloff); 
  $insteloff = $field[0];

  $rot =~ s/^(\s*)1 ack //;       @field = split( /(\s+)/, $rot ); 
  $rot = $field[0];

  $telfocus =~ s/^(\s*)1 ack //;  @field = split( /(\s+)/, $telfocus ); 
  $telfocus = $field[0];

  $telname  =~ s/^(\s*)1 ack //;  @field = split( /(\s+)/, $telname  ); 
  $telname  = $field[0];

  $parangle =~ s/^(\s*)1 ack //;  @field = split( /(\s+)/, $parangle ); 
  $parangle = $field[0];

  $mjd =~ s/^(\s*)1 ack //;  @field = split( /(\s+)/, $mjd ); 
  $mjd = $field[0];

  ###Trucate long number###
  my $dec_loc;
  $dec_loc = index($airmass, ".");
  $airmass = substr $airmass, 0, ($dec_loc + 4);

  $dec_loc = index($zen_dist, ".");
  $zen_dist = substr $zen_dist, 0, ($dec_loc + 4);

  $dec_loc = index($instazoff, ".");
  $instazoff = substr $instazoff, 0, ($dec_loc + 4);

  $dec_loc = index($insteloff, ".");
  $insteloff = substr $insteloff, 0, ($dec_loc + 4);

  $dec_loc = index($rot, ".");   
  $rot = substr $rot, 0, ($dec_loc + 4);

  $dec_loc = index($telfocus, "."); 
  $telfocus = substr $telfocus, 0, ($dec_loc + 3);

  $dec_loc = index($parangle, "."); 
  $parangle = substr $parangle, 0, ($dec_loc + 3);

  my $pos_angle = $parangle - $rot;

  my @return = ($epoch, $ra, $dec, $utime, $stime, $ha, $airmass, $zen_dist,
	        $instazoff, $insteloff, $rot, $telfocus, $telname, 
		$pos_angle, $mjd );

  return @return;

}#Endsub tcs_info


sub tcsops {
  #my ( $cmd ) = @_;
  #my $sock = $ops_sock;

  my( $sref1, $sref2 ) = @_;
  my $cmd = $$sref1;
  my $sock = $$sref2;

  print "submit tcsops: $cmd\n";
  #die "Unable to connect to $host on port $port (Reason: $!)\n" unless $sock;
  return "$!" unless $sock;

  print $sock "$cmd\n";
  my $retval = "";
  while ( defined($_ = <$sock>) ) { 
    $retval = $retval . $_;
    last if /\n/;
    #print;
  }
  return $retval;
}#Endsub tcsops



#rcsId = q($Name:  $ $Id: z.old.oneimage.newmmt.pl 14 2008-06-11 01:49:45Z hon $);

#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


###############################################################################
#
#Configure fits header parameters for next exposure
#object, filebase, exp_time, and nreads
#
##############################################################################
use strict;
use Getopt::Long;
use GetYN;
use ImgTests;
use Fitsheader qw/:all/;

my $DEBUG = 0;#0 = not debugging; 1 = debugging
GetOptions( 'debug' => \$DEBUG );

my $header = select_header($DEBUG);

Find_Fitsheader($header);

#readFitsHeader is exported from Fitsheader.pm
#and loads the following arrays from the input header
#@fh_params @fh_pvalues @fh_comments
readFitsHeader($header);

#print "\n>>>         Present exposure parameters are:                    <<<\n";
#print "_____________________________________________________________________\n";
#printFitsHeader( "FILEBASE", "ORIG_DIR" );
#
my $didmod = 0;
my $redo = 0;
my $change_bias = 0;

#print "\n\nChange any parameter? ";
#my $change = get_yn();
#if( $change == 0 ){
#  die "Exiting\n\n";
#}

until( $redo ){
  $didmod += mod_FILEBASE();

  my $test = mod_ORIG_DIR();
  if( $test ){
    $didmod += 1;
    my $dir_path = param_value( 'ORIG_DIR' );
    my $reply = does_dir_exist( $dir_path );
    print $reply;
  }

  if( $didmod > 0 ){
    print "\n\nThe new set of exposure parameters are:\n";
    printFitsHeader( "FILEBASE", "ORIG_DIR" );

    print "\nAccept changes (y/n)?";
    $redo = get_yn();
  }else{
    $redo = 1;
  }
}

if( $didmod > 0 ){
  if( !$DEBUG ){
    writeFitsHeader( $header );

  }elsif( $DEBUG ){
    #print "\nUpdating FITS header in: $header ...\n";
    writeFitsHeader( $header );
  }
}

print "\n";
###End of main

###subs
sub mod_FILEBASE{
    print "\n";
  print 
"__________________________________________________________________________\n";
  print "Enter template filename (Example 2001aug31 or sa57_a\n";
  print "Output filenames will be, e.g., 2001aug31".
         $ENV{DESIRED_INDEX_SEPARATOR}."####".$ENV{FITS_SUFFIX}."\n";
  print "The length must be less than 28 characters\n\n";

    
  my $nvals = @fh_pvalues;
  my ($new_pvalue);
  my ($vlen);
  my $didmod = 0;
  my ($i);
  my ($iparm);
  
  $iparm = param_index( 'FILEBASE' );
  
  print $fh_comments[$iparm] . "\n";
  my $is_valid = 0;
  until ($is_valid){
    if( $iparm < $nvals ){
      print $fh_params[$iparm ]. '= ' . $fh_pvalues[$iparm] . ' ? ';
      $new_pvalue = <STDIN>;
      chomp( $new_pvalue );
      
      $vlen = length( $new_pvalue );
      if( $vlen > 0 and $vlen <= 28 ){
	$fh_pvalues[$iparm] = '\'' . $new_pvalue . '\'' . (' ' x (28-$vlen));
	print $fh_params[$iparm ]. '= ' . $fh_pvalues[$iparm];
	print "\n";
	$is_valid = 1;
	$didmod = 1;
      }elsif( $vlen > 28 ){
	print "The path entered is > 28 characters long\n";
	print "Sorry, but you have to use a different filename\n";
      }elsif( $vlen <= 0 ){
	print "Not changing the current base filename\n";
	$didmod = 0;
	$is_valid = 1;
      }
    }else{
      print ">>>      WARNGING      WARNGING      <<<\n";
      print "Cannot find FILEBASE keyword in FITS header\n";
      print "NONE of the data taking scripts will work without this field\n";
      $is_valid = 1;
    }
  }
  print "\n";
  return $didmod;

} #Endsub mod_FILEBASE

__END__

=head1 NAME

set.filename.pl

=head1 DESCRIPTION

For FLAMINGOS-1.  Sets the filename prefix and working directory.


=head1 REVISION & LOCKER

$Name:  $

$Id: set.filename.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $

=head1 REQUIRES

All of the other original FLAMINGOS-1 perl scripts & modules.

=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut

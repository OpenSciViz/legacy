#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;

use Getopt::Long;

my $DEBUG = 0;#0 = not debugging; 1 = debugging
GetOptions( 'debug' => \$DEBUG );


my $number = $ARGV[0] or 
  die "\nUsage:\nmore.singleimages.pl #_of_images\n\n";

if( $number !~ m/^[0-9]*$/ ){ die "\nUsage:\nmore.singleimages.pl #_of_images\n\n"};

print "\n\nWill take $number exposures with the current setup:\n\n";

use Fitsheader qw/:all/;

my $header   = Fitsheader::select_header($DEBUG);
my $lut_name = Fitsheader::select_lut($DEBUG);
Fitsheader::Find_Fitsheader($header);
Fitsheader::readFitsHeader($header);

print "----------------------------------------------------------------------------\n";
Fitsheader::printFitsHeader( "OBS_TYPE", "OBJECT" );
print "----------------------------------------------------------------------------\n";
Fitsheader::printFitsHeader( "FILEBASE", "ORIG_DIR" );
print "----------------------------------------------------------------------------\n";
Fitsheader::printFitsHeader( "EXP_TIME",  "NREADS", "BIAS" );
print "----------------------------------------------------------------------------\n";
print "***LAST KNOWN FILTER POSITIONS:\n";
print "***(run config.wheels.pl to check actual positions)\n";
Fitsheader::printFitsHeader( "DECKER", "MOS", "SLIT", "FILTER", "LYOT", "GRISM" );
print "----------------------------------------------------------------------------\n";
print "\n";

use GetYN;
print "\n\nStart exposure sequence?";
my $change = GetYN::get_yn();
if( $change == 0 ){
  die "Exiting\n\n";
}

for( my $i=1; $i <=$number; $i++ ){
  print "\n\n\n\n\n".
        "***********************************************************************\n";
  print "Beginning exposure $i of $number....\n";
  #print "SHOULD BE EXECUTING singleimage.pl\n";
  my $singleimage_script = $ENV{SINGLEIMAGE};
  system( $singleimage_script );
  #system( "singleimage.pl" );
}

print "\a\a\a\a\a\a\a\a\a\a\a\a\a\n\n";
print "more.singleimages.pl done!\n\n";

__END__

=head1 NAME

more.singleimages.pl

=head1 DESCRIPTION

Takes a sequence of images on FLAMINGOS-1.  Useful for darks.

=head1 REVISION & LOCKER

$Name:  $

$Id: more.singleimages.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $

=head1 REQUIRES

All of the other original FLAMINGOS-1 perl scripts & modules.

=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut

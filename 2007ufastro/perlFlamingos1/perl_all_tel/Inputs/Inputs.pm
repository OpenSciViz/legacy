package Inputs;

require 5.005_62;
use strict;
use warnings;

require Exporter;
use AutoLoader qw(AUTOLOAD);

our @ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use Inputs ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.
our %EXPORT_TAGS = ( 'all' => [ qw(
				   &check_alpha
				   &check_integer
				   &check_number_decimals
				   &check_number_signs
) ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw( &check_alpha
		  &check_integer	
);

use vars qw/ $VERSION $LOCKER /;
'$Revision: 0.1 $ ' =~ /.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ ' =~ /.*:\s(.*)\s\$/ && ($LOCKER = $1);



# Preloaded methods go here.
sub check_alpha{
  my $input = $_[0];

  my $reply = 1;
  if( $input =~ m/[a-zA-Z]/ ){
    print "You entered $input.\n";
    print "Please enter an integer value.\n\n";
    $reply = 0;
  }

  return $reply;
}#Endsub check_alpha


sub check_integer{
  my $input = $_[0];

  my $reply = 1;
  if( $input =~ m/\.+/ ){
    print "Please enter an integer (no decimal point).\n\n";
    $reply = 0;
  }

  return $reply;
}#Endsub check_integer


sub check_number_decimals{
  my $offset = $_[0];

  my $first_decimal = index $offset, ".";

  my $reply = 1;
  if( $first_decimal >= 0 ){
    my $trunc = substr $offset, $first_decimal+1;

    my $second_decimal = index $trunc, ".";
    if( $second_decimal >= 0 ){
      print  "\n\nPlease enter only one decimal point\n\n";
      $reply = 0;
    }
  }
  return $reply;
}#Endsub check_number_decimals


sub check_number_signs{
  my $input = $_[0];
  my $sign = "+";
  my $value = $input;

  my $first_plus = index $input, "+";
  my $first_neg  = index $input, "-";

  my $reply = 1;
  if( $first_plus > 0 ){
    print "\n\nEnter a leading plus sign\n\n";
    $reply = 0;
  }
  if( $first_neg  > 0 ){
    print "\n\nEnter a leading minus sign\n\n";
    $reply = 0;
  }

  if( $first_plus == 0 ){
    my $trunc = substr $input, $first_plus+1;
    my $second_plus = index $trunc, "+";
    if( $second_plus >= 0 ){
      print "\n\nEnter only one leading plus sign\n\n";
      $reply = 0;
    }
    $sign = "+";
    $value = $trunc;
  }

  if( $first_neg == 0 ){
    my $trunc = substr $input, $first_neg+1;
    my $second_neg = index $trunc, "-";
    if( $second_neg >= 0 ){
      print "\n\nEnter only one leading minus sign\n\n";
      $reply = 0;
    }
    $sign = "-";
    $value = $trunc;
  }

  return $sign, $value, $reply;
}#Endsub check_number_signs


# Autoload methods go after =cut, and are processed by the autosplit program.

1;
__END__
# Below is stub documentation for your module. You better edit it!

=head1 NAME

Inputs - Perl extension for Flamingos

=head1 SYNOPSIS

  use Inputs qw/:all/;

=head1 DESCRIPTION

  Subroutines for checking input type.
  return value is 1=mathced or 0=no match
  check_alpha(   $input ); #return 1 if  m/[a-zA-Z]
  check_integer( $input ); #return 1 if  m/\.+/

=head2 EXPORT

None by default.


=head1 REVISION & LOCKER

$Name:  $

$Id: Inputs.pm,v 0.1 2003/05/22 15:21:06 raines Exp $

$Locker:  $


=head1 AUTHOR

SNR

=head1 SEE ALSO

perl(1).

=cut

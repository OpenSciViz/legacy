#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


###############################################################################
#
#Configure type of output next exposure
#display to ds9? write a jpeg and/or png file, too? get tcs info?
#
##############################################################################
use strict;
use Getopt::Long;
use GetYN;

my $DEBUG = 0;#0 = not debugging; 1 = debugging
GetOptions( 'debug' => \$DEBUG );

my $header = select_header($DEBUG);

use Fitsheader qw/:all/;
Find_Fitsheader($header);

#readFitsHeader is exported from Fitsheader.pm
#and loads the following arrays from the input header
#@fh_params @fh_pvalues @fh_comments
readFitsHeader($header);

print "\n>>> Present output parameters are: <<<\n";

printFitsHeader( "USE_TCS", "DISP_DS9", "DISPDS92", "JPEG",  "PNG", "UNLOCK", "PAD_DATA" );
print "\n";

my $didmod = 0;
my $redo = 0;

until( $redo ){

  if( modNumericParams( "USE_TCS", "DISP_DS9", "DISPDS92", "JPEG", "PNG", "UNLOCK", "PAD_DATA") > 0 ){
    $didmod = 1 ;
  }

  if( $didmod > 0 ){
    print "\n\nThe output parameters are:\n";
    printFitsHeader( "USE_TCS", "DISP_DS9", "DISPDS92", "JPEG",  "PNG", "UNLOCK", "PAD_DATA" );
    print "\n";

    print "\nAccept changes (y/n)?";
    $redo = get_yn();
  }else{
    $redo = 1;
  }
}

if( $didmod > 0 ){
  if( !$DEBUG ){

    writeFitsHeader( $header );

  }elsif( $DEBUG ){
    #print "\nUpdating FITS header in: $header ...\n";
    writeFitsHeader( $header );
  }
}elsif( $didmod == 0 ){
  print "\nNo changes made to default header.\n\n";
}

#Final print
print "\n";

__END__

=head1 NAME

engineering.config.output.pl

=head1 DESCRIPTION

Configures the template FITS header for FLAMINGOS-1 to
indicate what output data is required.  Examples include
automatically displaying data to ds9, and whether or not
to ask the TCS for TCS information, to be written to the
header.

=head1 REVISION & LOCKER

$Name:  $

$Id: engineering.config.output.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $

=head1 REQUIRES

All of the other original FLAMINGOS-1 perl scripts & modules.

=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut

#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;

##RESET MCE4 TO CT 0 = CONTINUOUS RESETs, AND RUNNING
my $testing = 0;#0 = Code normal, 1 = Code test

my $quote = "\"";my $space = " ";
my $reply;

my $ufdc_cmd = $ENV{UFDC}.$space.$ENV{CLIENT_HOST}.$space.$ENV{HOST}.$space.
	       $ENV{CLIENT_PORT}.$space.$ENV{CLIENT_UFDC_PORT}.$space.
	       $ENV{CLIENT_RAW}.$space;

my $ct0_cmd = $ufdc_cmd.$quote."CT 0".$quote;

my $start_cmd = $ufdc_cmd.$quote."START".$quote;


if( !$testing ){
  print "SETTING UP array controller; EXECUTING:\n$ufdc_cmd\n\n";
  $reply = `$ct0_cmd`;
  print "$reply\n";

  print "Sleep for 2 seconds\n";
  system("ufsleep 2.0");

  print "TRIGGERING CONTINUOUS RESETTING; EXECTUING:\n$start_cmd\n\n";
  $reply = `$start_cmd`;
  print "$reply\n";

  print "Sleep for 2 seconds\n";
  system("ufsleep 2.0");
}else{
  print "TESTING";
  print "SETTING UP array controller; EXECUTING:\n$ufdc_cmd\n\n";

  print "Sleep for 2 seconds\n";
  system("ufsleep 2.0");

  print "TRIGGERING CONTINUOUS RESETTING; EXECTUING:\n$start_cmd\n\n";

  print "Sleep for 2 seconds\n";
  system("ufsleep 2.0");
}

__END__

=head1 NAME

ufidle.pl

=head1 DESCRIPTION

For FLAMINGOS-1.  Sends commands to the MCE-4 demon to put the 
array controller in an idle loop where the array is continuously
reset.

=head1 REVISION & LOCKER

$Name:  $

$Id: ufidle.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $

=head1 REQUIRES

All of the other original FLAMINGOS-1 perl scripts & modules.

=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut


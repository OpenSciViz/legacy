#!/usr/local/bin/perl -w

use strict;


my $bintest = pack( "L", 0 );
print "bintest is $bintest:\n";


my $ascii_file = "/home/raines/Perl/flamingos.headers.lut/z.ascii.txt";
my $bin_file   = "z.bin.txt";
my $big_bin_file = "z.big.bin.txt";

open AF, ">$ascii_file" || die;
print AF "0";
close AF;

open BF, ">$bin_file" || die;
print BF $bintest;
close BF;


open BBF, ">$big_bin_file" || die;
for( my $i = 0; $i < 416; $i++ ){
  print BBF $bintest;
}
close BBF;

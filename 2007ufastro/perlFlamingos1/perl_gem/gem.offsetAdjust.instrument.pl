#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;

use ufgem qw/:all/;

my $cnt = @ARGV;
if( $cnt < 2 ) { # assume -help
   die "\n\tUsage:".
       "\n\tEnter relative offset, in units of arcseconds, ".
       "\n\tin instrument coordinate frame".
       "\n\toffsetAdjust.instrument.pl instr_x_offset instr_y_offset".
       "\n\n";

}else{
  my $x_offset = shift;
  my $y_offset = shift;

  if( $x_offset =~ m/[a-zA-Z]/ or $y_offset =~ m/[a-zA-Z]/ ){
    die "\n\nDid you enter a non-numerical x_offset or y_offset ".
        "by mistake?".
	"\n\nx_offset = $x_offset; y_offset = $y_offset\n\n";

  }else{
    my $soc = ufgem::viiconnect();
    #ufgem::offsetAdjust_instrument( $soc, $x_offset, $y_offset );
    offsetAdjust_instrument( $soc, $x_offset, $y_offset );
    close( $soc );
  }
}#End


__END__

=head1 NAME

gem.offsetAdjust.instrument.pl

=head1 Description

Offsets the telescope in the coordinate
frame of the instrument.


=head1 REVISION & LOCKER

$Name:  $

$Id: gem.offsetAdjust.instrument.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut

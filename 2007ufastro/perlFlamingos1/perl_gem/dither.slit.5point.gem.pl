#!/usr/local/bin/perl -w


use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";

use strict;

use Getopt::Long;
use Fitsheader qw/:all/;
use GetYN;
use IdleLutStatus;
use ImgTests;
use RW_Header_Params qw/:all/;
use Dither_patterns qw/:all/;
use MCE4_acquisition qw/:all/;
use UseTCS qw/:all/;
use GemTCSinfo qw/:all/;
use ufgem qw/:all/;

my $DEBUG = 0;#0 = not debugging; 1 = debugging

#******
my $VIIDO = 1;#0 = don't talk to vii (for safe debugging); 1 = talk to vii
#******

GetOptions( 'debug' => \$DEBUG,
	    'viido' => \$VIIDO);

#>>>Select and load header<<<
my $header   = select_header($DEBUG);
my $lut_name = select_lut($DEBUG);
Find_Fitsheader($header);
readFitsHeader($header);
#this loads fits header array variables into the variable space
#and apparently they're visible to subroutines without explicit passing

my ($use_tcs,
    $telescop) = RW_Header_Params::get_telescope_and_use_tcs_params();

my ($expt,
    $nreads,
    $extra_timeout,
    $net_edt_timeout) = RW_Header_Params::get_expt_params();

my ($num_pat_repeats,
    $nudge,
    $delta_y,
    $use_mnudg,
    $chip_pa_on_sky_for_rot0,
    $pa_rotator,
    $pa_slit_on_chip,
    $chip_pa_on_sky_this_rotator_pa) = RW_Header_Params::get_mos_dither_params();
    #only m_rptpat and m_throw will be used

#### Get Filename info from header
#### Double check existence of absolute path to write data into
my ($orig_dir, $filebase, $file_hint) = RW_Header_Params::get_filename_params();
my $reply      = ImgTests::does_dir_exist( $orig_dir );
my $last_index = ImgTests::whats_last_index($orig_dir, $filebase);
my $this_index = $last_index + 1;
my $next_index = $this_index;
print "This image's index will be $this_index\n";

my $soc = UseTCS::make_socket_connection( $telescop, $VIIDO );
main::prompt_setup();

my $probe_name = ufgem::query_probe_name();
main::prompt_should_be_guiding_beam_A();

ufgem::probeGuideConfig_noda_chopa( $soc, "p2", "on" );
ufgem::probeGuideConfig_noda_chopb( $soc, "p2", "off" );
ufgem::probeGuideConfig_nodb_chopa( $soc, "p2", "off" );
ufgem::probeGuideConfig_nodb_chopb( $soc, "p2", "off" );

#### Setup mce4 only once
#print "SHOULD BE: Configuring the exposure time and cycle type on MCE4\n";
print "Configuring the exposure time and cycle type on MCE4\n";
MCE4_acquisition::setup_mce4( $expt, $nreads );


my $delta_x = 0.0;

my $move_y_AtoB =  2 * $delta_y;
my $move_y_BtoC = -3 * $delta_y;
my $move_y_CtoD =  2 * $delta_y;
my $move_y_DtoE = -3 * $delta_y;
my $move_y_EtoA =  2 * $delta_y;

my @move_y = ($move_y_AtoB, $move_y_BtoC, $move_y_CtoD, $move_y_DtoE, $move_y_EtoA );

my @rel_y_wrtA = ( 0, 2*$delta_y, -1*$delta_y, 1*$delta_y, 2*$delta_y );

my $n_index;
for( my $i = 0; $i < $num_pat_repeats; $i++ ){

  print "\n\n";
  print "       PATTERN ".($i+1)." of $num_pat_repeats\n";
  print "***************************************\n";

  print "Present offsets in arcsec wrt position A\n";
  print "-------(In instrument frame)------------\n";
  print "XOFFSET = $delta_x\tYOFFSET = $rel_y_wrtA[0]\n";
  RW_Header_Params::write_rel_offset_to_header( $header, $delta_x, $rel_y_wrtA[0]);
  print "\n";
  $n_index = $next_index + $i*5;
  take_an_image( $header, $lut_name, $file_hint, $n_index, $net_edt_timeout, $use_tcs );

  print "\n";
  print "A to B: \n";dither_slit_motion( "B", $delta_x, $move_y[0] );
  print "Present offsets in arcsec wrt position A\n";
  print "-------(In instrument frame)------------\n";
  print "XOFFSET = $delta_x\tYOFFSET = $rel_y_wrtA[1]\n";
  RW_Header_Params::write_rel_offset_to_header( $header, $delta_x, $rel_y_wrtA[1]);
  print "\n";
  $n_index = ($next_index + 1) + $i*5;
  take_an_image( $header, $lut_name, $file_hint, $n_index, $net_edt_timeout, $use_tcs );

  print "\n";
  print "B to C: \n";dither_slit_motion( "C", $delta_x, $move_y[1] );
  print "Present offsets in arcsec wrt position A\n";
  print "-------(In instrument frame)------------\n";
  print "XOFFSET = $delta_x\tYOFFSET = $rel_y_wrtA[2]\n";
  RW_Header_Params::write_rel_offset_to_header( $header, $delta_x, $rel_y_wrtA[2]);
  print "\n";
  $n_index = ($next_index + 2) + $i*5;
  take_an_image( $header, $lut_name, $file_hint, $n_index, $net_edt_timeout, $use_tcs );

  print "\n";
  print "C to D: \n";dither_slit_motion( "D", $delta_x, $move_y[2] );
  print "Present offsets in arcsec wrt position A\n";
  print "-------(In instrument frame)------------\n";
  print "XOFFSET = $delta_x\tYOFFSET = $rel_y_wrtA[3]\n";
  RW_Header_Params::write_rel_offset_to_header( $header, $delta_x, $rel_y_wrtA[3]);
  print "\n";
  $n_index = ($next_index + 3) + $i*5;
  take_an_image( $header, $lut_name, $file_hint, $n_index, $net_edt_timeout, $use_tcs );

  print "\n";
  print "D to E: \n";dither_slit_motion( "E",$delta_x, $move_y[3] );
  print "Present offsets in arcsec wrt position A\n";
  print "-------(In instrument frame)------------\n";
  print "XOFFSET = $delta_x\tYOFFSET = $rel_y_wrtA[4]\n";
  RW_Header_Params::write_rel_offset_to_header( $header, $delta_x, $rel_y_wrtA[4]);
  print "\n";
  $n_index = ($next_index + 4 ) + $i*5;
  take_an_image( $header, $lut_name, $file_hint, $n_index, $net_edt_timeout, $use_tcs );

  print "\n";
  print "E to A: \n";dither_slit_motion( "A", $delta_x, $move_y[4] );
  print "Present offsets in arcsec wrt position A\n";
  print "-------(In instrument frame)------------\n";
  print "XOFFSET = $delta_x\tYOFFSET = $rel_y_wrtA[0]\n";
  RW_Header_Params::write_rel_offset_to_header( $header, $delta_x, $rel_y_wrtA[0]);
  print "\n";
}



#subs
sub dither_slit_motion{
  my ( $position, $offset_x, $offset_y ) = @_;

  if( $VIIDO ){
    print "guide off\n";
    ufgem::guideOff( $soc );

    print "offsetAdjust to position $position: $offset_x, $offset_y\n";
    ufgem::offsetAdjust_instrument( $soc, $offset_x, $offset_y );

    ufgem::offset_wait();

    print "guide on\n";
    ufgem::guideOn( $soc );

    ufgem::guide_wait();

  }else{
    print "SHOULD BE: guide off\n";
    print "offsetAdjust to position $position: $offset_x, $offset_y\n";
    print "SHOULD BE: do offsetAdjust frame=instrumentxy off1=$offset_x off2=$offset_y $ufgem::terminate";
    print "SHOULD BE: offset waiting\n";
    print "SHOULD BE: guide on\n";
    print "SHOULD BE: guide waiting\n";
  }
}#Endsub dither_slit_motion


sub prompt_setup{
  print "\n";
  print "*******   Parameters for 5 point slit dither   ********\n";
  print "Spacing between positions (arcsec)= $delta_y\n";
  print "Number of times to repeat pattern = $num_pat_repeats\n";
  print "******* WILL NOT NUDGE BEAM ON PATTERN REPEATS ********\n";
  print "\n\n";
}#Endsub prompt_setup


sub prompt_should_be_guiding_beam_A{

  print "\n\n";
  print ">--------------------------------------------------------------<\n";
  print "The telescope should already be guiding with the Stars centered in their boxes.\n";

  print "WARNING WARNING WARNING\n";
  print "And the offsets should be zero.\n";
  print "(If they are not, move to home or absorb the offsets before running this script\n";
  print "About to begin executing the dither pattern.";

  GetYN::query_ready();
}#Endsub prompt_should_be_guiding_beam_A


sub take_an_image{
  my ($header, $lut_name, $file_hint, $this_index, $net_edt_timeout, $use_tcs ) = @_;

  print "take image with index= $this_index\n";
  if( $VIIDO ){
    UseTCS::use_tcs( $use_tcs, $header, $telescop );
  }else{
    print "SHOULD BE GETTING TCSINFO\n";
  }

  #print "SHOULD BE calling write filename header param with $filebase $this_index\n";
  RW_Header_Params::write_filename_header_param( $filebase, $this_index, $header);

  my $acq_cmd = acquisition_cmd( $header, $lut_name, 
				 $file_hint, $this_index, $net_edt_timeout);

  #print "SHOULD BE EXECUTING:  acq cmd\n";
  #print "acq cmd is \n$acq_cmd\n\n";
  MCE4_acquisition::acquire_image( $acq_cmd );

  #print "SHOULD be executing idle_lut_status loop\n";
  IdleLutStatus::idle_lut_status_slashr( $expt, $nreads);

  my $unlock_param = param_value( 'UNLOCK' );
  if( $unlock_param ){
    ImgTests::make_unlock_file( $file_hint, $this_index );
  }

  #print "SHOULD BE padding data.\n";
  if( Fitsheader::param_value( 'PAD_DATA' ) ){
    ImgTests::pad_data( $file_hint, $this_index );
  }

  #print "SHOULD BE displaying image\n";
  if( Fitsheader::param_value( 'DISPDS92' ) ){
    ImgTests::display_image( $file_hint, $this_index );
  }else{
    print "The better way to display images is off; see config.output.pl\n";
  }

}#Endsub take_an_image


__END__

=head1 NAME

dither.slit.5point.gem.pl

=head1 Description

Dithers up and down the slit at 5 points, stepping
a distance of at least two points per motion.

=head1 REVISION & LOCKER

$Name:  $

$Id: dither.slit.5point.gem.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut

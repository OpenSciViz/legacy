#!/usr/local/bin/perl -w


use strict;

my $test_str = "ack some_command -1 the   wait has timed out";

my $failure_index = -1;
$failure_index = index $test_str, ' -1 ';

if( $failure_index != -1 ){
  print "failure index at $failure_index\n";
  if( $test_str =~ m/the(\s+)wait(\s+)has(\s+)timed(\s+)out/ ){
    print "test_str matched ocswish timeout condition\n";

  }
}

#!/usr/local/bin/perl -w
my $rcsId = q($Name:  $ $Id: testoffset.pl 14 2008-06-11 01:49:45Z hon $);

use strict;

use ufgem qw/:all/;

my $soc = ufgem::viiconnect();
my $cnt = @ARGV;
my $reply;

if( $cnt < 1 ) {
  $reply = ufgem::getOffset_radec($soc);
}
elsif( $cnt < 2 ) { # assume -help
   print "usage (units of arcsec.): gemoffset.pl ra dec\n";
   exit(0);
}
else {
  my $ra = shift;
  my $dec = shift;
  $reply = ufgem::setOffset_radec($soc, $ra, $dec);
}



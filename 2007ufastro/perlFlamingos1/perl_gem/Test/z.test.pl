#!/usr/local/bin/perl -w

use strict;
use ufgem;

my $soc = ufgem::viiconnect();

my $probe_name = "p2";
print "Probe $probe_name follow on\n";

ufgem::probe_follow_on( $soc, $probe_name );
print "guide off\n";
ufgem::guideOff( $soc );

my $X =  7.515;
my $Y = -7.515;

print "offset to position  $X, $Y\n";

ufgem::setOffset_radec( $soc, $X, $Y );
ufgem::offset_wait();
print "guide on\n";
ufgem::guideOn( $soc );
ufgem::guide_wait();


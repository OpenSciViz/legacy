#!/usr/local/bin/perl -w
my $rcsId = q($Name:  $ $Id: mountGuide.pl 14 2008-06-11 01:49:45Z hon $);

use strict;

use ufgem qw/:all/;

my $soc = ufgem::viiconnect();
my $cnt = @ARGV;
my $reply;

if( $cnt < 1 ) { # assume -help
   print "usage mountGuide.pl on|off\n";
   exit(0);
}
else {
  my $state = shift;
  if( $state eq "on" ){
    mountGuideOn( $soc );
  }elsif( $state eq "off" ){
    mountGuideOff( $soc );
  }else{
    die "Unknown command $state\n\n";
  }


}



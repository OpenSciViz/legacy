#!/usr/local/bin/perl -w
my $rcsId = q($Name:  $ $Id: gemnodbeam.pl 14 2008-06-11 01:49:45Z hon $);

use strict;

use ufgem qw/:all/;

my $soc = ufgem::viiconnect();
my $cnt = @ARGV;
my $reply;

if( $cnt < 1 ) { # assume -help
   print "usage testnodbeam.pl A|B\n";
   exit(0);
}
else {
  my $beam = shift;
  ufgem::nodbeam($soc, $beam);
}



#!/usr/local/bin/perl -w
my $rcsId = q($Name:  $ $Id: z.test.ack.done.pl 14 2008-06-11 01:49:45Z hon $);

use strict;

use ufgem qw/:all/;

my $soc = this_viiconnect();
my $cnt = @ARGV;
my $reply;

if( $cnt < 2 ) { # assume -help
   print "usage testnodconfig.pl throw (arcsec) pa (deg)\n";
   exit(0);
}
else {
  my $throw = shift;
  my $pa    = shift;
  $reply = this_nodConfig($soc, $throw, $pa);
  print "$reply\n";
}

sub this_nodConfig{
  my ( $soc, $throw, $throw_pa ) = @_;
  my $nod_mode  = "mode = offset";
  my $nod_type  = "type = tangent plane";
  my $nod_throw = "throw = $throw";
  my $nod_pa    = "pa = $throw_pa";

  my $nod_config_cmd =
    "do nodConfig $nod_mode $nod_type $nod_throw $nod_pa $ufgem::terminate";

  if( !defined $soc || $soc eq "" ) {
    print "?Bad socket connection to Gemini VII server\n";
    return;
  }

  print "Executing $nod_config_cmd\n";
  print $soc "$nod_config_cmd"; 
  $soc->flush();
  my $ack = <$soc>;
  #print "ack is $ack\n";
  parse_gem_ack_or_done( "nodConfig", $ack );

  my $done = <$soc>;
  parse_gem_ack_or_done( "nodConfig", $done);

}#Endsub nodConfig


sub parse_gem_ack_or_done{
  my ( $cmd, $ack_or_done) = @_;

  chomp $ack_or_done;
  {  local $/ = "\r"; chomp $ack_or_done; }

  #index returns -1 on failure to find pattern
  my $failure_index = index $ack_or_done, ' -1 ';
  my $success_index = index $ack_or_done, ' 0 ';
  
  if( $failure_index == -1 and $success_index != -1 ){
    print "$cmd acknowledged or done successfully:\n";
    print "$ack_or_done\n\n";
  }elsif( $failure_index != -1 and $success_index == -1 ){
    print "$cmd not acknowledged or done successfuly:\n";
    print "$ack_or_done\n\n";
    die "Exiting\n\n";
  }else{
    print "Not sure if $cmd executed successfully\n";
    print "$ack_or_done\n";
    print "Continue ";
    use GetYN;
    my $reply = get_yn();
    if( !$reply ){
      die "Exiting\n\n";
    }
  }

}#Endsub parse_gem_ack_or_done


sub this_viiconnect {
#  local $ufgem::host = "vii-sim.hi.gemini.edu";
  local $ufgem::host = "172.17.2.10"; # icarus
  #local $ufgem::host = $ENV{VII_HOST_IP};
  local $ufgem::argc = @_;
  if( $ufgem::argc > 0 ) { $ufgem::host = shift; }
  local $ufgem::port = $ENV{VII_HOST_PORT};
  local $ufgem::soc = IO::Socket::INET->new( PeerAddr => $ufgem::host, 
					     PeerPort => $ufgem::port,
					     Proto => 'tcp', 
					     Type => IO::Socket::SOCK_STREAM );
  print "soc= $ufgem::soc\n";

  if( !defined($ufgem::soc) ) {
    print "failed to connect to $ufgem::host on port $ufgem::port\n";
    exit(0);
  }
  else {
    local $ufgem::ack = <$ufgem::soc>;
    print "connected to $ufgem::host on port $ufgem::port, ";
    print "$ufgem::ack";
  }
  print $ufgem::soc "enable ack $ufgem::terminate";
  print $ufgem::soc "enable done $ufgem::terminate";
  return $ufgem::soc;
}#Endsub this_viiconnect



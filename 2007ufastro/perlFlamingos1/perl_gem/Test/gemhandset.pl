#!/usr/bin/perl
my $rcsId = q($Name:  $ $Id: gemhandset.pl 14 2008-06-11 01:49:45Z hon $);

use strict;

BEGIN { require "/usr/local/flamingos/perl_gem/gemsouth.plib"; }

my $cnt = @ARGV;
my $reply;
my $soc = ufgem::viiconnect();

if( $cnt < 1 ) {
  $reply = ufgem::getOffset_radec($soc);
}
elsif( $cnt < 2 ) { # assume -help
   print "usage (units of arcsec.): gemhandset.pl ra dec\n";
   exit(0);
}
else {
  my $ra = shift;
  my $dec = shift;
  $reply = ufgem::handsetOffset_radec($soc, $ra, $dec);
}

print "$reply";


#!/usr/bin/perl
my $rcsId = q($Name:  $ $Id: gemguide.pl 14 2008-06-11 01:49:45Z hon $);

use strict;

BEGIN { require "/usr/local/flamingos/perl_gem/gemsouth.plib"; }

my $cnt = @ARGV;

if( $cnt < 1 ) {
   print "usage: gemguide on/off\n";
   exit(0);
}

my $gd = shift;
my $reply;
my $soc = ufgem::viiconnect();
if( rindex($gd,"ff") >= 1 ) {
  $reply = ufgem::guideOff($soc);
}
else {
  $reply = ufgem::guideOn($soc);
}

print "$reply";


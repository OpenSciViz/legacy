#!/usr/local/bin/perl -w

## Execute mos object-sky dither patterns at Gemini South
## This script uses nods
##

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;

use Getopt::Long;
use Fitsheader qw/:all/;
use GetYN;
use IdleLutStatus;
use ImgTests;
use RW_Header_Params qw/:all/;
use Dither_patterns qw/:all/;
use MCE4_acquisition qw/:all/;
use UseTCS qw/:all/;
use GemTCSinfo qw/:all/;
use ufgem qw/:all/;

my $DEBUG = 0;#0 = not debugging; 1 = debugging
my $VIIDO = 1;#0 = don't talk to vii (for safe debugging); 1 = talk to vii
GetOptions( 'debug' => \$DEBUG,
	    'viido' => \$VIIDO);

#>>>Select and load header<<<
my $header   = select_header($DEBUG);
my $lut_name = select_lut($DEBUG);
Find_Fitsheader($header);
readFitsHeader($header);
#this loads fits header array variables into the variable space
#and apparently they're visible to subroutines without explicit passing

my ($use_tcs,
    $telescop) = RW_Header_Params::get_telescope_and_use_tcs_params();

my ($expt,
    $nreads,
    $extra_timeout,
    $net_edt_timeout) = RW_Header_Params::get_expt_params();

my $m_rptpat  = Fitsheader::param_value( 'M_RPTPAT' );
my $throw     = Fitsheader::param_value( 'THROW' );
my $throw_pa  = Fitsheader::param_value( 'THROW_PA' );

#### Get Filename info from header
#### Double check existence of absolute path to write data into
my ($orig_dir, $filebase, $file_hint) = RW_Header_Params::get_filename_params();
my $reply      = ImgTests::does_dir_exist( $orig_dir );
my $last_index = ImgTests::whats_last_index($orig_dir, $filebase);
my $this_index = $last_index + 1;
my $next_index = $this_index;
print "This image's index will be $this_index\n";

my ( $beamB_dra, $beamB_ddec ) = main::nod_location();
print_info( $m_rptpat, $throw, $throw_pa, $beamB_dra, $beamB_ddec );

my $soc = UseTCS::make_socket_connection( $telescop, $VIIDO );
main::prompt_setup( $soc );

my $probe_name = ufgem::query_probe_name();
main::prompt_should_be_guiding_beam_A();

ufgem::probeGuideConfig_noda_chopa( $soc, "p2", "on" );
ufgem::probeGuideConfig_noda_chopb( $soc, "p2", "off" );
ufgem::probeGuideConfig_nodb_chopa( $soc, "p2", "off" );
ufgem::probeGuideConfig_nodb_chopb( $soc, "p2", "off" );

main::setup_nod( $soc );

#### Setup mce4 only once
#print "SHOULD BE: Configuring the exposure time and cycle type on MCE4\n";
print "Configuring the exposure time and cycle type on MCE4\n";
MCE4_acquisition::setup_mce4( $expt, $nreads );

if( $VIIDO ){
  print "Probe $probe_name follow on.\n";
  ufgem::probe_follow_on( $soc, $probe_name );
}else{
    print "SHOULD BE: Probe $probe_name follow on.\n";
}

my $nudge_sign = 0;
for( my $i = 1; $i <= $m_rptpat; $i++ ){

    $next_index = do_pattern( $header, $lut_name,
			      $file_hint, $next_index, $net_edt_timeout, $use_tcs );
}#End for loop


print "\n\n***************************************************\n";
print      "*****          DITHER SCRIPT DONE           ******\n";
print     "***************************************************\n";
print "\n";

####SUBS

sub do_pattern{
  my ($header, $lut_name, 
      $file_hint, $next_index, $net_edt_timeout, $use_tcs, $m_throw ) = @_;

  print ">-------------------------------------<\n";
  print ">     Doing ABBA pattern              <\n";
  print "Take 1 Spectrum in Beam A\n";
  print "PRESENT OFFSETS wrt beam A: XOFFSET = 0\n";
  print "                            YOFFSET = 0\n";
  RW_Header_Params::write_rel_offset_to_header( $header, 0, 0);

  $next_index = take_an_image( $header, $lut_name, 1,
  		 $file_hint, $next_index, $net_edt_timeout, $use_tcs);

  print "- - - - - - - - - - - - - - - - - -\n";
  main::move_from_beam_A_to_beam_B();

  print "PRESENT OFFSETS wrt beam A_centered: XOFFSET = $beamB_dra\n";
  print "                                     YOFFSET = $beamB_ddec\n";
  RW_Header_Params::write_rel_offset_to_header( $header, $beamB_dra, $beamB_ddec);

  print "Take 2 Spectra in beam B\n";
  $next_index = take_an_image( $header, $lut_name, 2,
  		 $file_hint, $next_index, $net_edt_timeout, $use_tcs);

  print "- - - - - - - - - - - - - - - - - -\n";
  main::move_from_beam_B_to_beam_A();

  print "PRESENT OFFSETS wrt beam A_centered: XOFFSET = 0\n";
  print "                                     YOFFSET = 0\n";
  RW_Header_Params::write_rel_offset_to_header( $header, 0, 0);

  print "Take 1 Spectrum in Beam A\n";
  $next_index = take_an_image( $header, $lut_name, 1,
  		 $file_hint, $next_index, $net_edt_timeout, $use_tcs);


  return $next_index;
}#Endsub do_pattern


sub my_idle_lut_status{
  my ($expt, $nreads) = @_;
  my $uffrmstatus = "uffrmstatus";
  my $frame_status_reply = `$uffrmstatus`;
  print "\n\n\n";

  my $idle_notLutting = 0;
  my $sleep_count = 1;
  my $lut_timeout = 30;
  while( $idle_notLutting == 0 ){
    sleep 1;

    $frame_status_reply = `$uffrmstatus`;
    $idle_notLutting = 
      grep( /idle: true, applyingLUT: false/, $frame_status_reply);

    if( $idle_notLutting == 0 ){
      print ">>>>>.....TAKING AN IMAGE OR APPLYING LUT.....\n";
    }elsif( $idle_notLutting ==1 ){
      print ">>>>>.....EXPOSURE DONE.....\n";
    }

    if( $sleep_count < $expt + $nreads ){
      print "Have slept $sleep_count seconds out of an exposure time of ".
	     ($expt + $nreads) ." seconds\n";
      print "(Exposure time + number of reads = $expt + $nreads)\n";
    }elsif( $sleep_count >= $expt + $nreads ){
      print "\a\a\a\a\a\n\n";
      print ">>>>>>>>>>..........EXPOSURE TIME ELAPSED..........<<<<<<<<<<\n";
      print "If not applying LUT (in MCE4 window), hit ctrl-Z,\n".
             "type ufstop.pl -stop -clean, then type fg to resume\n";
      print "If the ufstop.pl command hangs, hit ctrl-c,\n".
            "and try ufstop.pl -clean only, then type fg to resume\n";

      print "\nHave slept " . ($sleep_count - ($expt + $nreads)) .
            " seconds past the exposure time\n";
      print "Applying lut may take an additional 30 seconds\n\n";

      if( $sleep_count > $expt + $nreads + $lut_timeout ){

	print "\a\a\a\a\a\n\n";
	print ">>>>>>>>>>..........".
	  "EXITING--IMAGE MAY NOT BE COMPLETE".
            "..........<<<<<<<<<<\n\n";
	print "IF it looks like it has stalled while applying the LUT,\n";
	print "run ufstop.pl -clean, and start over\n\n";
	print "Else if it has stalled sometime before applying the LUT,\n";
	print "run ufstop.pl -stop -clean, and start over\n\n";
	exit;
      }#end time expired
    }
    $sleep_count += 1;

  }#end while idle not lutting

}#Endsub my_idle_lut_status


sub move_from_beam_A_to_beam_B{

  print "NODDING from beam A to beam B\n";
  print "------------------------------------\n";
    if( $VIIDO ){
      print "m1Corrections off\n";
      ufgem::m1CorrectionsOff( $soc );
      print "mountguide off\n";
      ufgem::mountGuideOff( $soc );
      print "guide off\n";
      ufgem::guideOff( $soc );
      print "Probe $probe_name follow off\n";
      ufgem::probe_follow_off( $soc, $probe_name );
      print "probeGuideConfig p1 off\n";
      ufgem::probeGuideConfig( $soc, "p1", "off" );
      print "probeGuideConfig p2 off\n";
      ufgem::probeGuideConfig( $soc, "p2", "off" );
      print "Nodding to beam=B\n";
      #print "position relative to first position:  $X_origin, $Y_origin\n";
      ufgem::nodbeam( $soc, "B" );
      ufgem::nod_wait();
    }else{ print "***Should be doing the following:\n";
	   print "***m1CorrectionsOff\n";
	   print "***mountguide off\n";
	   print "***guide off\n";
	   print "***Probe $probe_name follow off\n";
	   print "***probeGuideConfig off\n";
	   print "***Nodding to beam=B\n";
	   #print "***position relative to first position:  $X_origin, $Y_origin\n";
	   print "***"; ufgem::nod_wait();
   }

}#Endsub move_from_beam_A_to_beam_B


sub move_from_beam_B_to_beam_A{

  print "NODDING to from beam B to beam A\n";
  print "---------------------------------------\n";
    if( $VIIDO ){
      print "Nodding to beam=A\n";
      ufgem::nodbeam( $soc, "A" );
      sleep 1;
      print "probeGuideConfig $probe_name on\n";
      ufgem::probeGuideConfig( $soc, $probe_name, "on" );
      sleep 1;
      print "Probe $probe_name follow on\n";
      ufgem::probe_follow_on( $soc, $probe_name );
      ufgem::probe_wait();
      print "guide on\n";
      ufgem::guideOn( $soc );
      ufgem::guide_wait();
      print "mountguide on\n";
      ufgem::mountGuideOn( $soc );
      print "m1Corrections on\n";
      ufgem::m1CorrectionsOn( $soc );
    }else{ print "***Should be doing the following:\n";
	   print "***Nodding to beam=A\n";
	   print "***probeGuideConfig on\n";
	   print "***Probe $probe_name follow on\n";
	   print "***"; ufgem::probe_wait();
	   print "***guide on\n";
	   print "***"; ufgem::guide_wait();
	   print "***mountguide on\n";
	   print "***m1Corrections\n";
    }

}#Endsub move_from_beam_B_to_beam_A


sub nod_location{
  use Math::Trig;

  my $pa_rad = deg2rad( $throw_pa );
  my $d_alpha = $throw * sin( $pa_rad );
  my $d_delta = $throw * cos( $pa_rad );

  $d_alpha = sprintf "%.2f", $d_alpha;
  $d_delta = sprintf "%.2f", $d_delta;

  return( $d_alpha, $d_delta );
}#Endsub nod_location


sub setup_nod{
  my $soc = $_[0];

  print "Setting up nod beam B with throw = $throw arcsec, PA = $throw_pa\n";
  ufgem::nodConfig( $soc, $throw, $throw_pa );

}#Endsub setup_nod


sub print_info{
  print "\n\n";
  print "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n";
  print "----------------PRESENT PARAMETERS FOR MOS DITHER----------------\n";
  print "\n";

  print "\n";
  print "Number of times through pattern------------------------ $m_rptpat\n";
  print "Distance (arcsec) between A & B beams------------------ $throw\n";
  print "Position angle (deg) to beam B (unguided)-------------- $throw_pa\n";
  print "\n";
  print "Beam B relative location in RA  (arcsec)--------------- $beamB_dra\n";
  print "Beam B relative location in Dec (arcsec)--------------- $beamB_ddec\n";

  print "Will _NOT_ nugde pattern on repeats from center.\n";

  print "-----------------------------------------------------------------\n";

}#Endsub print_info


sub prompt_should_be_guiding_beam_A{

  print "\n\n";
  print ">--------------------------------------------------------------<\n";
  print "The telescope should already be guiding in NOD BEAM=A\n\n";
  print "About to begin executing the dither pattern.";

  GetYN::query_ready();
}#Endsub prompt_setup


sub prompt_setup{
  my $soc = $_[0];

  print "\n";
  print ">------------------------------------------------------------------------------------\n";
  print ">---WARNING--WARNING--WARNING--WARNGING--WARNING--WARNING-WARNING--WARNING--WARNING--\n";
  print ">\n";
  print "> You have to choose what pointing center about which to execute the dither pattern:\n\n";
  print "> Type HOME to continue dithering about the original pointing center\n";
  print ">\n";
  print "> OR,\n";
  print ">\n";
  print "> Type ABSORB to recenter the dither pattern to your present location.\n";
  print ">\n";
  print "> OR,\n";
  print ">\n";
  print "> Type q to quit\n\n";
  print "\n";
  print "> Entry:  ";

  my $absorb_home = <>;chomp $absorb_home;
  my $valid = 0;
  until( $valid ){
    if( $absorb_home =~ m/absorb/i ){
      $valid = 1;
    }elsif( $absorb_home =~ m/home/i ){
      $valid = 1;
    }elsif( $absorb_home =~ m/q/i ){
      die "\n\nExiting\n\n";
    }else{
      print "Please enter either ABSORB or HOME ";
      $absorb_home = <>; chomp $absorb_home;
    }
  }

  if( $absorb_home =~ m/absorb/i ){
    print "\n\nRecentering the dither pattern to the present location\n";
    ufgem::absorb_offsets( $soc );
    ufgem::absorb_adjusts( $soc );

  }elsif( $absorb_home =~ m/home/i ){
    print "\n\nDithering about the original pointing center\n";
  }
  print "\n\n";
}#Endsub prompt_setup


sub take_an_image{
  my ($header, $lut_name, $d_rptpos,
      $file_hint, $this_index, $net_edt_timeout, $use_tcs ) = @_;


  for( my $i = 0; $i < $d_rptpos; $i++ ){
    print "take image with index= $this_index\n";
    if( $VIIDO ){
      UseTCS::use_tcs( $use_tcs, $header, $telescop );
    }else{
      print "SHOULD BE GETTING TCSINFO\n";
    }

    print "Writing filename header param with $filebase $this_index\n";
    RW_Header_Params::write_filename_header_param( $filebase, $this_index, $header);

    my $acq_cmd = acquisition_cmd( $header, $lut_name, 
   		   $file_hint, $this_index, $net_edt_timeout);

    #print "SHOULD BE EXECUTING: acq cmd\n";
    print "acq cmd is \n$acq_cmd\n\n";
    MCE4_acquisition::acquire_image( $acq_cmd );

    #print "SHOULD be executing idle_lut_status loop\n";
    IdleLutStatus::idle_lut_status_slashr( $expt, $nreads);
    main::my_idle_lut_status( $expt, $nreads );

    my $unlock_param = param_value( 'UNLOCK' );
    if( $unlock_param ){
      ImgTests::make_unlock_file( $file_hint, $this_index );
    }

    #print "SHOULD BE padding data.\n";
    if( Fitsheader::param_value( 'PAD_DATA' ) ){
      ImgTests::pad_data( $file_hint, $this_index );
    }

    print "SHOULD BE displaying image\n";
    if( Fitsheader::param_value( 'DISPDS92' ) ){
      ImgTests::display_image( $file_hint, $this_index );
    }else{
      print "The better way to display images is off; see config.output.pl\n";
    }

    $this_index += 1;
  }
  print "\n\n";
  return $this_index;
}#Endsub take_an_image


__END__

=head1 NAME

dither.mos.object.sky.nod.test.pl

=head1 Description

Do an object-sky dither pattern using a nod to move off source.
Richard doesn't believe we should use a nod, but should move the
whole mount to get to the target.

=head1 REVISION & LOCKER

$Name:  $

$Id: dither.mos.object.sky.nod.test.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut

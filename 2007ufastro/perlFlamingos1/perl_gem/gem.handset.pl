#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;
use ufgem qw/:all/;

my $cnt = @ARGV;
if( $cnt < 1 ) {
  #Get present values
  my $soc = ufgem::viiconnect();
  ufgem::getOffset_radec($soc);
  close( $soc );

}elsif( $cnt < 2 ) {
  #Print help message
   die "\n\tUsage:".
       "Enter relative offsets, in units of arcseconds, ".
       "for both ra and dec:\n".
       "gem.handset.pl ra dec\n\n";

}else{
  my $ra = shift;
  my $dec = shift;

  if( $ra =~ m/[a-zA-Z]/ or $dec =~ m/[a-zA-Z]/ ){
    die "\n\nDid you enter a non-numerical ra or dec by mistake?".
        "\n\nra = $ra; dec = $dec\n\n";

  }else{
    #print "ra = $ra; dec = $dec\n"; die;
    my $soc = ufgem::viiconnect();
    ufgem::setHandset_radec($soc, $ra, $dec);
    close( $soc );
  }
}#End


__END__

=head1 NAME

gem.handset.pl

=head1 Description

Does a releative offset of the telescope in ra and dec.


=head1 REVISION & LOCKER

$Name:  $

$Id: gem.handset.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut

#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;
use ufgem qw/:all/;

my $cnt = @ARGV;
if( $cnt < 1 ) {
   die "\n\tUsage:  gem.nodbeam.pl A|B\n\n";

}else{
  my $beam = shift;

  if( $beam =~ m/(a|b)/i ){
    my $soc = ufgem::viiconnect();
    print "Going to beam = $beam\n";
    ufgem::nodbeam($soc, $beam);
    close( $soc );

  }else{
    die "\n\n\tPlease enter gem.nodbeam.pl a or ".
        "gem.nodbeam.pl b\n\n";
  }

}#End


__END__

=head1 NAME

gem.nodbeam.pl

=head1 Description

Selects the nodbeam the telescope is in.

=head1 REVISION & LOCKER

$Name:  $

$Id: gem.nodbeam.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut

#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;
use ufgem qw/:all/;
use IO::Socket;

my $soc = ufgem::viiconnect();

print $soc "get instrPA $ufgem::terminate";
$soc->flush();
my $instrPA = <$soc>;
chomp $instrPA;
{  local $/ = "\r"; chomp $instrPA; }

my $got_str = "got ";
my $len_got_str = length $got_str;
substr $instrPA, 0, $len_got_str, "";
my $len_keyword = length "instrPA";
substr $instrPA, 0, $len_keyword, "";

print "The instrPA is at = $instrPA\n";

print "\n";
print "increment by how many degrees (+/-#.## format)?  ";

my $input = <>;chomp $input;

my $valid = 0;
until( $valid ){
  if( $input =~ m/[0-9]\.[0-9]/i ){
    $valid = 1;
  }else{
    print "Please enter a real number";
    $input = <>; chomp $input;
  }
}

print "Will increment the instrument PA by $input\n";

my $new_position_angle = $instrPA + $input;
print "The new instrument PA angle is $new_position_angle\n";

print $soc "do rotator PA=$new_position_angle $ufgem::terminate";
$soc->flush();
my $reply = <$soc>;
close( $soc );


__END__

=head1 NAME

gem.increment.position.angle.pl

=head1 Description

Use to tweak the instrument PA.


=head1 REVISION & LOCKER

$Name:  $

$Id: gem.increment.position.angle.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut

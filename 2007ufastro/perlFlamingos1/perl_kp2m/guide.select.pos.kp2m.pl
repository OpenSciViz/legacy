#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;
use Gdr_Rpc qw/:all/;
use GetYN qw/:all/;

my $gdrrpc = $ENV{UF_KP2m_GDRRPC};

my $cnt = @ARGV;
if( $cnt < 1 ) {
  usage();
}else{
  my $state = shift;

  if( $state =~ m/^1$/i){
    Gdr_Rpc::guide_off( $gdrrpc );
    Gdr_Rpc::select_guide_pos1( $gdrrpc );

  }elsif( $state =~ m/^2$/i){
    Gdr_Rpc::guide_off( $gdrrpc );
    Gdr_Rpc::select_guide_pos2( $gdrrpc );

  }elsif( $state =~ m/^3$/i){
    Gdr_Rpc::guide_off( $gdrrpc );
    Gdr_Rpc::select_guide_pos3( $gdrrpc );

  }elsif( $state =~ m/^4$/i){
    Gdr_Rpc::guide_off( $gdrrpc );
    Gdr_Rpc::select_guide_pos4( $gdrrpc );

  }else{
    usage();

  }

  print "Turn guiding on at this location? ";
  my $reply = GetYN::get_yn();
  if( !$reply ){
    die "Exiting without enabling guiding.\n";
  }else{
    Gdr_Rpc::guide_on( $gdrrpc );
    main::guide_wait();
  }
}#End


sub usage{

   die "\n\tUsage: guide.select.pos.kp2m.pl 1|2|3|4".
       "\n\tThis will move the guide box to the selected guide position.".
       "\n\tNOTES:  1) Guiding will be turned off.".
       "\n\t        2) This script will not turn guiding on, but will prompt you.\n\n";

}#Endsub usage


__END__

=head1 NAME

guide.select.pos.kp2m.pl

=head1 Description

Select which lock position to guide at.


=head1 REVISION & LOCKER

$Name:  $

$Id: guide.select.pos.kp2m.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut

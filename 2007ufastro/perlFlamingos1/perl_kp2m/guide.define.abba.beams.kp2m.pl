#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;

use Fitsheader qw/:all/;
use Gdr_Rpc qw/:all/;
use Getopt::Long;
use GetYN qw/:all/;
use KPNO_offsets qw/:all/;
use Math::Round qw( round );
use RW_Header_Params qw/:all/;


my $DEBUG = 0;#0 = not debugging; 1 = debugging;
GetOptions( 'debug' => \$DEBUG );

print "\n-----------------------------------------------------------------\n";
#>>>Select and load header<<<
my $header   = Fitsheader::select_header($DEBUG);
my $lut_name = Fitsheader::select_lut($DEBUG);
Fitsheader::Find_Fitsheader($header);
Fitsheader::readFitsHeader($header);
#this loads fits header array variables into the variable space
#and apparently they're visible to subroutines without explicit passing

print_msg();

my $gdrrpc = $ENV{UF_KP2m_GDRRPC};

my ($m_rptpat, $m_ndgsz, $m_throw, $chip_pa_on_sky_for_rot0, $pa_rotator,
    $pa_slit_on_chip, $chip_pa_on_sky_this_rotator_pa) = RW_Header_Params::get_mos_dither_params();

my $pa_slit_on_sky = main::compute_pa_slit_on_sky( $chip_pa_on_sky_this_rotator_pa,
						   $pa_slit_on_chip );

### Convert to guider plate scale
my $gdr_ps = $ENV{GDR_PS};

#testing
#$m_throw = 70.4;

my $beam_separation_pix = $m_throw / $gdr_ps;

my ($lockpos1_x, $lockpos1_y) = get_slit_lock_position( $header );

print "\n";
print "Last known slit center pixel definition on the guider\n";
print "-----------------------------------------------------\n";
print "X = $lockpos1_x\n";
print "Y = $lockpos1_y\n";

### Use these for telescope offsetting
my ( $A_dra_asec, $A_ddec_asec, $B_dra_asec, $B_ddec_asec ) =
   compute_offsets( $pa_slit_on_sky, $m_throw );

print "\n";
print "Telescope offsets in arcsec\n";
print "---------------------------\n";
print "Beam\tdra\tddec\n";
print "A\t$A_dra_asec\t$A_ddec_asec\n";
print "B\t$B_dra_asec\t$B_ddec_asec\n";
print "\n";

### Use these for guide and AOI box offsets
my ( $A_dra_pix, $A_ddec_pix, $B_dra_pix, $B_ddec_pix ) =
   compute_offsets( $pa_slit_on_sky, $beam_separation_pix );

$A_dra_pix  = Math::Round::round( $A_dra_pix );
$A_ddec_pix = Math::Round::round( $A_ddec_pix );
$B_dra_pix  = Math::Round::round( $B_dra_pix );
$B_ddec_pix = Math::Round::round( $B_ddec_pix );

print "Guider box offsets in pixels, rounded\n";
print "-------------------------------------\n";
print "Beam\tdra\tddec\n";
print "A\t$A_dra_pix\t$A_ddec_pix\n";
print "B\t$B_dra_pix\t$B_ddec_pix\n";
print "\n";

GetYN::query_ready();

#### Step 1 Guiding off
#print "<><><><><><><><><><><><><><><><><><><><>\n";
#print "Step 1: Guiding off.\n";
#print "--------------------\n";
#Gdr_Rpc::guide_off( $gdrrpc );

### Step 2 Define beam B
print "\n";
print "Step 2: Define beam B = lock pos3, mark position, get position info.\n";
print "--------------------------------------------------------------------\n";
my $this_x = $lockpos1_x + $B_dra_pix;
my $this_y = $lockpos1_y + $B_ddec_pix;
Gdr_Rpc::set_real_valued_lock_position( $gdrrpc, "3", $this_x, $this_y );
sleep 1;

#Gdr_Rpc::select_guide_pos3( $gdrrpc );
#sleep 1;

#my ($gpos3_x, $gpos3_y) = Gdr_Rpc::get_current_lock_position( $gdrrpc );
#sleep 1;

#my $round_pos3_x = Math::Round::round( $gpos3_x );
#my $round_pos3_y = Math::Round::round( $gpos3_y );

#my $gpos3_marker_y = Gdr_Rpc::flip_y( $round_pos3_y );
#Gdr_Rpc::mark_current_position( $gdrrpc, $round_pos3_x, $gpos3_marker_y );

#print "Should have marked beam B as defpos3.\n";
#main::did_it_work();

### Step 3 Define beam A
print "\n\n\n";
print "Step 3: Define beam A = lock pos2, mark position, get position info.\n";
print "--------------------------------------------------------\n";
$this_x = $lockpos1_x + $A_dra_pix;
$this_y = $lockpos1_y + $A_ddec_pix;
Gdr_Rpc::set_real_valued_lock_position( $gdrrpc, "2", $this_x, $this_y );
sleep 1;

#Gdr_Rpc::select_guide_pos2( $gdrrpc );
#sleep 1;

#my ($gpos2_x, $gpos2_y) = Gdr_Rpc::get_current_lock_position( $gdrrpc );
#sleep 1;

#my $round_pos2_x = Math::Round::round( $gpos2_x );
#my $round_pos2_y = Math::Round::round( $gpos2_y );

#my $gpos2_marker_y = flip_y( $round_pos2_y );
#Gdr_Rpc::mark_current_position( $gdrrpc, $round_pos2_x, $gpos2_marker_y );

#print "Should have marked beam A as defpos2.\n";
#main::did_it_work();

#write_box_positions( $gpos2_x, $gpos2_y, $gpos3_x, $gpos3_y );


### Step 4 Offset telescope to beam A
print "\n";
print "Step 4: Offset telescope to beam A\n";
print "----------------------------------\n";
print "Offset the telescope to beam A? ";

my $reply = GetYN::get_yn();
if( !$reply ){
  print "\n";
  print "Resuming guiding at slit center.\n";
  Gdr_Rpc::select_guide_pos1( $gdrrpc );
  sleep 1;
  Gdr_Rpc::guide_on( $gdrrpc );

  die "\n\nExiting.\n\n";
}

offset_to_beam_A( $A_dra_asec, $A_ddec_asec );
main::offset_wait();

### Step 5 Start guiding in beam A
print "\n";
print "Step 5: Start guiding in beam A? \n";

$reply = GetYN::get_yn();
if( !$reply ){
  die "\n\nExiting.\n\n";
}

Gdr_Rpc::select_guide_pos2( $gdrrpc );
sleep 1;
#main::did_it_work();
Gdr_Rpc::guide_on( $gdrrpc );

####################################
### Subs
sub compute_offsets{
  my ($pa_slit_on_sky, $beam_separation) = @_;
  use Math::Trig;
  my $pa_sky_rad = deg2rad( $pa_slit_on_sky );

  my $A_dra  = 0.5 * $beam_separation * sin( $pa_sky_rad );
  my $A_ddec = 0.5 * $beam_separation * cos( $pa_sky_rad );

  my $B_dra  = -1 * $A_dra;
  my $B_ddec = -1 * $A_ddec;

  $A_dra  = sprintf '%.2f', $A_dra;
  $A_ddec = sprintf '%.2f', $A_ddec;

  $B_dra  = sprintf '%.2f', $B_dra;
  $B_ddec = sprintf '%.2f', $B_ddec;

  return( $A_dra, $A_ddec, $B_dra, $B_ddec );
}#Endsub compute_offsets


sub compute_pa_slit_on_sky{
  my ( $chipsky, $slitchip ) = @_;
  my $slitsky;

  if( $chipsky <= 90 and $slitchip >= -90 and $slitchip <= 90 ){
    $slitsky = $chipsky + $slitchip;

  }elsif( $chipsky > 90 and $chipsky <= 180 ){
    if( $slitchip >= 0 ){
      $slitsky = $chipsky + $slitchip - 180;

    }elsif( $slitchip < 0 ){
      $slitsky = 180 - $chipsky + abs( $slitchip );

    }
  }
  $slitsky = sprintf "%.2f", $slitsky;
  print "PA of slit on sky = $slitsky\n";

  return $slitsky;
}#Endsub compute_pa_slit_on_sky


sub did_it_work{
  print "Did it move the boxes as expected? ";
  my $reply = GetYN::get_yn();

  if( !$reply ){
    print "Continue? ";
    my $reply2 = GetYN::get_yn();
    if( !$reply2 ){
      die "\n\nExiting\n\n";
    }
  }
}#Endsub did_it_work


sub get_slit_lock_position{
  my $header = $_[0];

  my $x = Fitsheader::param_value( 'GPOS1_X' );
  my $y = Fitsheader::param_value( 'GPOS1_Y' );

  return ($x, $y);
}#Endsub get_slit_lock_position


sub offset_to_beam_A{
  my ($A_dra_asec, $A_ddec_asec) = @_;

  my $tcs   = $ENV{UF_KPNO_TCS_PROG};
  my $space = $ENV{SPACE};
  my $head  = "\"tele offset += ";
  #my $head  = $ENV{TCS_OFFSET_HEAD};
  my $sep   = $ENV{TCS_OFFSET_SEP};
  my $tail  = $ENV{TCS_OFFSET_TAIL};

  my $offset = $A_dra_asec.$sep.$A_ddec_asec;

  my $offset_cmd = $tcs.$space.$head.$offset.$tail;
  print "SHOULD BE: EXECUTING OFFSET COMMAND:\n";
  print "EXECUTING OFFSET COMMAND:\n";
  print "$offset_cmd\n";

  my $reply;
  $reply = `$offset_cmd`;
  print "$reply\n";
}#Endsub offset_to_beam_A


sub print_msg{
  print "\n\n";
  print "\tBefore you start:\n";
  print "\t1) Make sure the MOS alignment stars are centered on their boxes.\n";
  print "\t2) The present position is defined as lock position 1.\n";
  print "\t3) The telescope has been guiding.\n";
  print "\t4) Step 4 below will send a 'zero' to the tcs, which will set the present\n";
  print "\t   offsets to zero without moving the telescope.\n";
  print "\n";
  print "\tThis script will define lock positions 2 & 3 as beams A & B,\n";
  print "\tand place markers at each location.\n";
  print "\n";
  print "\tOrder of action:\n";
  print "\t1) Turn off guiding.\n";
  print "\t2) Move guide and AOI boxes to beam B and define as pos3, and place marker.\n";
  print "\t3) Move guide and AOI boxes to beam A and define as pos2, and place marker.\n";
  print "\t4) Offset telescope to beam A.\n";
  print "\t5) Turn on guiding.\n";
  print "\n\n";
}#Endsub print_msg


sub write_box_positions{
  my ($gpos2_x, $gpos2_y, $gpos3_x, $gpos3_y) = @_;

  print "Writing box positions to header.\n";
  print "Beam B:  (GPOS3_X, GPOS3_Y) = ($gpos3_x, $gpos3_y)\n";
  print "Beam A:  (GPOS2_X, GPOS2_Y) = ($gpos2_x, $gpos2_y)\n";

  Fitsheader::setNumericParam( 'GPOS2_X', $gpos2_x );
  Fitsheader::setNumericParam( 'GPOS2_Y', $gpos2_y );

  Fitsheader::setNumericParam( 'GPOS3_X', $gpos3_x );
  Fitsheader::setNumericParam( 'GPOS3_Y', $gpos3_y );

  Fitsheader::writeFitsHeader( $header );
  print "\n";
}#Endsub write_box_positions

__END__

=head1 NAME

guide.define.abba.beams.kp2m.pl

=head1 Description

Use to setup the location of the beams A &
B for mos dithering.


=head1 REVISION & LOCKER

$Name:  $

$Id: guide.define.abba.beams.kp2m.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut

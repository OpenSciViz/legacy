#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;
use Gdr_Rpc qw/:all/;
use GetYN qw/:all/;

my $gdrrpc = $ENV{UF_KP2m_GDRRPC};
print "\n\n";
print "\tThis will center the guide box on the brightest object.\n";
print "\tGuiding must be off.\n";

GetYN::query_ready();

my $warp_cmd = $gdrrpc." guide warp";

print "Executing: $warp_cmd\n";
my $reply = `$warp_cmd`;


__END__

=head1 NAME

guide.warp.kp2m.pl

=head1 Description

Warp to the nearest bright star.
Guiding must be off.
Does not start guiding.

=head1 REVISION & LOCKER

$Name:  $

$Id: guide.warp.kp2m.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut

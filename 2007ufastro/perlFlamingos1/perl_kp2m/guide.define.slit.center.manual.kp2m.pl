#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;

use Fitsheader qw/:all/;
use Gdr_Rpc qw/:all/;
use Getopt::Long;
use GetYN qw/:all/;
use KPNO_offsets qw/:all/;
use Math::Round qw/ round /;

my $DEBUG = 0;#0 = not debugging; 1 = debugging;
GetOptions( 'debug' => \$DEBUG );

print "\n-----------------------------------------------------------------\n";
#>>>Select and load header<<<
my $header   = Fitsheader::select_header($DEBUG);
my $lut_name = Fitsheader::select_lut($DEBUG);
Fitsheader::Find_Fitsheader($header);
Fitsheader::readFitsHeader($header); 
#this loads fits header array variables into the variable space
#and apparently they're visible to subroutines without explicit passing

my $gdrrpc = $ENV{UF_KP2m_GDRRPC};

main::print_msg();
GetYN::query_ready();

print "On the guider gui on Teal:\n";
print "select 'Locks --> Clear lock positions'\n";
print "Done?";
GetYN::get_yn();

print "On the guider gui on Teal:\n";
print "select 'Locks --> Define lock position 1'\n";
print "Done?";
GetYN::get_yn();

print "May not need this step:\n";
print "On the guider gui on Teal:\n";
print "select 'Locks --> Goto lock position 1'\n";
print "Done?";
GetYN::get_yn();

print "On the guider gui on Teal:\n";
print "select 'Markers --> Add marker at current box center'\n";
print "Done?";
GetYN::get_yn();

print "This script next needs to query the guider for the current lock position.\n";
print "This is necessary if we ever decide that nudging the mos observation is\n";
print "possible on repeats of the mos ABBA pattern.\n";
GetYN::query_ready();

my ($slit_box_x, $slit_box_y) = Gdr_Rpc::get_current_lock_position( $gdrrpc );
my $round_box_x = Math::Round::round( $slit_box_x );
my $round_box_y = Math::Round::round( $slit_box_y );

print "\n";
print "Writing guide box locations to header keywords.\n";
print "GPOS1_X = $slit_box_x\n";
print "GPOS1_Y = $slit_box_y\n";
print "\n";
Fitsheader::setNumericParam( 'GPOS1_X', $slit_box_x );
Fitsheader::setNumericParam( 'GPOS1_Y', $slit_box_y );
#print "SHOULD BE WRITING FITS HEADER\n";
Fitsheader::writeFitsHeader( $header );
print "\n";

#print "SHOULDB BE clearing offset\n";
print "\n";
print "The mos dither script requires the slit center have zero offsets.\n";
print "Clear the present offsets (i.e. set to zero)?  ";
my $reply = GetYN::get_yn();
if( !$reply ){
  die "Exiting\n";
}else{
  KPNO_offsets::clear_offsets();
}


### SUBS
sub print_msg{
  print "\n\n";
  print "\tThis script will walk you through the guider gui on Teal, in order\n";
  print "\tto define guide lock postion 1 at the current guiding position.\n";
  print "\n";
  print "\tThe Mos plate alignment stars should be well placed in their boxes,\n";
  print "\tand the Mos objects in the center of their slits.\n";
  print "\n";
  print "\tYou should be guiding.\n";
}#Endsub print_msg

__END__

=head1 NAME

guide.define.slit.center.manual.kp2m.pl

=head1 Description

Yet another script for defining the starting
point before defining the A & B beams for
mos spectroscopy.

=head1 REVISION & LOCKER

$Name:  $

$Id: guide.define.slit.center.manual.kp2m.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut

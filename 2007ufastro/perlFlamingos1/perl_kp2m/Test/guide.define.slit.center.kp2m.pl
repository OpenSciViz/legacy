#!/usr/local/bin/perl -w

my $rcsId = q($Name:  $ $Id: guide.define.slit.center.kp2m.pl 14 2008-06-11 01:49:45Z hon $);

use strict;

use Getopt::Long;
use Gdr_Rpc qw/:all/;
use GetYN qw/:all/;
use Math::Round qw( round );
use Fitsheader qw/:all/;

my $DEBUG = 0;#0 = not debugging; 1 = debugging;
GetOptions( 'debug' => \$DEBUG );

print "\n-----------------------------------------------------------------\n";
#>>>Select and load header<<<
my $header   = Fitsheader::select_header($DEBUG);
my $lut_name = Fitsheader::select_lut($DEBUG);
Fitsheader::Find_Fitsheader($header);
Fitsheader::readFitsHeader($header); 
#this loads fits header array variables into the variable space
#and apparently they're visible to subroutines without explicit passing


my $gdrrpc = $ENV{UF_KP2m_GDRRPC};
print "\n\n";
print "\tThis script will define lock position 1 at the current position,\n";
print "\twhich should have the alignment start well placed in their boxes,\n";
print "\tand the Mos objects in the center of their slits.  A marker will\n";
print "\talso be placed on the screen.\n";
print "\n";
print "\tThe present offsets will be zeroed.\n";
print "\n";
print "\tThe mos dither script will use lock positions 2 & 3 for the A\n";
print "\tand B beams.\n";

GetYN::query_ready();

my $cmd_def_pos    = $gdrrpc." guide defpos1";

print "Executing: $cmd_def_pos\n";
my $reply = `$cmd_def_pos`;

my $cmd_info = $gdrrpc." info";
#print "Doing Fake ask for present info\n";
print "Asking for present info\n";
my $info_reply = `$cmd_info`;
chomp $info_reply;

my @info = split / /,$info_reply;
my $slit_box_x = $info[13];
my $slit_box_y = $info[14];

print "Un-rounded guide-center = $slit_box_x, $slit_box_y\n";
$slit_box_x = Math::Round::round( $slit_box_x );
$slit_box_y = Math::Round::round( $slit_box_y );
print "Rounded guide-center    = $slit_box_x, $slit_box_y\n";

my $fliped_y = Gdr_Rpc::flip_y( $slit_box_y );
Gdr_Rpc::mark_current_position( $gdrrpc, $slit_box_x, $fliped_y );

print "\n";
print "Writing guide box locations to header keywords.\n";
print "GPOS1_X = $slit_box_x\n";
print "GPOS1_Y = $fliped_y\n";
Fitsheader::setNumericParam( 'GPOS1_X', $slit_box_x );
Fitsheader::setNumericParam( 'GPOS1_Y', $slit_box_y );
Fitsheader::writeFitsHeader( $header );
print "\n";

#print "SHOULDB BE clearing offset\n";
main::clear_offset();

### SUBS
sub clear_offset{
  my $tcs  = $ENV{UF_KPNO_TCS_PROG};
  my $space = $ENV{SPACE};
  my $head = $ENV{TCS_OFFSET_HEAD};
  my $sep  = $ENV{TCS_OFFSET_SEP};
  my $tail = $ENV{TCS_OFFSET_TAIL};
  my $zero = "zero";

  my $offset_cmd = $tcs.$space.$head.$zero.$tail;

  print "The mos dither scripts require this position to have zero offsets.\n";
  print "Do you want to zero the offsets now? ";

  my $reply = GetYN::get_yn();
  if( !$reply ){
    die "\n\nExiting.\n\n";
  }

  print "EXECUTING CLEAR OFFSET COMMAND:\n";
  print "$offset_cmd\n";

  $reply = `$offset_cmd`;
  print "$reply\n";
}#Endsub clear_offset



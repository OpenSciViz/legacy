#!/usr/local/bin/perl -w

my $rcsId = q($Name:  $ $Id: guide.define.abba.main.beams.kp2m.pl 14 2008-06-11 01:49:45Z hon $);

use strict;

use Getopt::Long;
use Gdr_Rpc qw/:all/;
use GetYN qw/:all/;
use Math::Round qw( round );
use Fitsheader qw/:all/;
use RW_Header_Params qw/:all/;
use Math::Round qw( round );

my $DEBUG = 0;#0 = not debugging; 1 = debugging;
GetOptions( 'debug' => \$DEBUG );

print "\n-----------------------------------------------------------------\n";
#>>>Select and load header<<<
my $header   = Fitsheader::select_header($DEBUG);
my $lut_name = Fitsheader::select_lut($DEBUG);
Fitsheader::Find_Fitsheader($header);
Fitsheader::readFitsHeader($header);
#this loads fits header array variables into the variable space
#and apparently they're visible to subroutines without explicit passing

print_msg();

my $gdrrpc = $ENV{UF_KP2m_GDRRPC};

my ($m_rptpat, $m_ndgsz, $m_throw, $chip_pa_on_sky_for_rot0, $pa_rotator,
    $pa_slit_on_chip, $chip_pa_on_sky_this_rotator_pa) = RW_Header_Params::get_mos_dither_params();

my $pa_slit_on_sky = main::compute_pa_slit_on_sky( $chip_pa_on_sky_this_rotator_pa,
						   $pa_slit_on_chip );

### Convert to guider plate scale
my $gdr_ps = $ENV{GDR_PS};

#testing
#$m_throw = 70.4;

my $beam_separation_pix = $m_throw / $gdr_ps;

### Use these for telescope offsetting
my ( $A_dra_asec, $A_ddec_asec, $B_dra_asec, $B_ddec_asec ) =
   compute_offsets( $pa_slit_on_sky, $m_throw );

print "\n";
print "Telescope offsets in arcsec\n";
print "---------------------------\n";
print "Beam\tdra\tddec\n";
print "A\t$A_dra_asec\t$A_ddec_asec\n";
print "B\t$B_dra_asec\t$B_ddec_asec\n";
print "\n";

### Use these for guide and AOI box offsets
my ( $A_dra_pix, $A_ddec_pix, $B_dra_pix, $B_ddec_pix ) =
   compute_offsets( $pa_slit_on_sky, $beam_separation_pix );

$A_dra_pix  = Math::Round::round( $A_dra_pix );
$A_ddec_pix = Math::Round::round( $A_ddec_pix );
$B_dra_pix  = Math::Round::round( $B_dra_pix );
$B_ddec_pix = Math::Round::round( $B_ddec_pix );

print "Guider box offsets in pixels, rounded\n";
print "-------------------------------------\n";
print "Beam\tdra\tddec\n";
print "A\t$A_dra_pix\t$A_ddec_pix\n";
print "B\t$B_dra_pix\t$B_ddec_pix\n";
print "\n";

GetYN::query_ready();

### Step 1 Guiding off
print "<><><><><><><><><><><><><><><><><><><><>\n";
print "Step 1: Guiding off.\n";
print "--------------------\n";
Gdr_Rpc::guide_off( $gdrrpc );

### Step 2 Define beam B
print "\n";
print "Step 2: Define beam B = lock pos3, mark position, get position info.\n";
print "--------------------------------------------------------------------\n";
Gdr_Rpc::move_guide_and_aoi_boxes( $gdrrpc, $B_dra_pix, $B_ddec_pix );

print "Should have moved guide and AOI boxes by $B_dra_pix, $B_ddec_pix wrt slit center.\n";
main::did_it_work();

#main::guide_wait();
Gdr_Rpc::define_lock_position( $gdrrpc, "3" );
my ($gpos3_x, $gpos3_y) = Gdr_Rpc::get_current_position( $gdrrpc );

my $gpos3_marker_y = Gdr_Rpc::flip_y( $gpos3_y );

Gdr_Rpc::mark_current_position( $gdrrpc, $gpos3_x, $gpos3_marker_y );

print "Should have marked beam B as defpos3.\n";
main::did_it_work();

### Step 3 Define beam A
print "\n\n\n";
print "Step 3: Define beam A = lock pos2, mark position, get position info.\n";
print "--------------------------------------------------------\n";
Gdr_Rpc::move_guide_and_aoi_boxes( $gdrrpc, 2*$A_dra_pix, 2*$A_ddec_pix );

print "Should have moved guide and AOI boxes to $A_dra_pix, $A_ddec_pix wrt slit center.\n";
main::did_it_work();

#main::guide_wait();
Gdr_Rpc::define_lock_position( $gdrrpc, "2" );
my ($gpos2_x, $gpos2_y) = Gdr_Rpc::get_current_position( $gdrrpc );

my $gpos2_marker_y = flip_y( $gpos2_y );
Gdr_Rpc::mark_current_position( $gdrrpc, $gpos2_x, $gpos2_marker_y );

print "Should have marked beam A as defpos2.\n";
main::did_it_work();

write_box_positions( $gpos2_x, $gpos2_y, $gpos3_x, $gpos3_y );


### Step 4 Offset telescope to beam A
print "\n";
print "Step 4: Offset telescope to beam A\n";
print "----------------------------------\n";
print "Offset the telescope to beam A? ";

my $reply = GetYN::get_yn();
if( !$reply ){
  die "\n\nExiting.\n\n";
}

offset_to_beam_A( $A_dra_asec, $A_ddec_asec );
main::offset_wait();

### Step 5 Start guiding in beam A
print "\n";
print "Step 5: Start guiding in beam A? \n";

$reply = GetYN::get_yn();
if( !$reply ){
  die "\n\nExiting.\n\n";
}

Gdr_Rpc::select_guide_pos2( $gdrrpc );

main::did_it_work();
Gdr_Rpc::guide_on( $gdrrpc );

####################################
### Subs
sub compute_offsets{
  my ($pa_slit_on_sky, $beam_separation) = @_;
  use Math::Trig;
  my $pa_sky_rad = deg2rad( $pa_slit_on_sky );

  my $A_dra  = 0.5 * $beam_separation * sin( $pa_sky_rad );
  my $A_ddec = 0.5 * $beam_separation * cos( $pa_sky_rad );

  my $B_dra  = -1 * $A_dra;
  my $B_ddec = -1 * $A_ddec;

  $A_dra  = sprintf '%.2f', $A_dra;
  $A_ddec = sprintf '%.2f', $A_ddec;

  $B_dra  = sprintf '%.2f', $B_dra;
  $B_ddec = sprintf '%.2f', $B_ddec;

  return( $A_dra, $A_ddec, $B_dra, $B_ddec );
}#Endsub compute_offsets


sub compute_pa_slit_on_sky{
  my ( $chipsky, $slitchip ) = @_;
  my $slitsky;

  if( $chipsky <= 90 and $slitchip >= -90 and $slitchip <= 90 ){
    $slitsky = $chipsky + $slitchip;

  }elsif( $chipsky > 90 and $chipsky <= 180 ){
    if( $slitchip >= 0 ){
      $slitsky = $chipsky + $slitchip - 180;

    }elsif( $slitchip < 0 ){
      $slitsky = 180 - $chipsky + abs( $slitchip );

    }
  }
  $slitsky = sprintf "%.2f", $slitsky;
  print "PA of slit on sky = $slitsky\n";

  return $slitsky;
}#Endsub compute_pa_slit_on_sky


sub did_it_work{
  print "Did it move the boxes as expected? ";
  my $reply = GetYN::get_yn();

  if( !$reply ){
    print "Continue? ";
    my $reply2 = GetYN::get_yn();
    if( !$reply2 ){
      die "\n\nExiting\n\n";
    }
  }
}#Endsub did_it_work


sub guide_wait{
  my $gwait = $ENV{GUIDE_WAIT};
  my $count = 0;

  until( $count eq $gwait ){
    print "Waiting for guider $count of $gwait\n";
    sleep( 1 );
    $count++;
  }
}#Endsub guide_wait


sub offset_to_beam_A{
  my ($A_dra_asec, $A_ddec_asec) = @_;

  my $tcs   = $ENV{UF_KPNO_TCS_PROG};
  my $space = $ENV{SPACE};
  my $head  = "\"tele offset += ";
  #my $head  = $ENV{TCS_OFFSET_HEAD};
  my $sep   = $ENV{TCS_OFFSET_SEP};
  my $tail  = $ENV{TCS_OFFSET_TAIL};

  my $offset = $A_dra_asec.$sep.$A_ddec_asec;

  my $offset_cmd = $tcs.$space.$head.$offset.$tail;
  print "SHOULD BE: EXECUTING OFFSET COMMAND:\n";
  print "EXECUTING OFFSET COMMAND:\n";
  print "$offset_cmd\n";

  my $reply;
  $reply = `$offset_cmd`;
  print "$reply\n";
}#Endsub offset_to_beam_A


sub offset_wait{
  my $gwait = $ENV{OFFSET_WAIT};
  my $count = 0;

  until( $count eq $gwait ){
    print "Waiting for offset $count of $gwait\n";
    sleep( 1 );
    $count++;
  }
}#Endsub offset_wait


sub print_msg{
  print "\n\n";
  print "\tBefore you start:\n";
  print "\t1) Make sure the MOS alignment stars are centered on their boxes.\n";
  print "\t2) The present position is defined as lock position 1.\n";
  print "\t3) The telescope has been guiding.\n";
  print "\t4) Step 4 below will send a 'zero' to the tcs, which will set the present\n";
  print "\t   offsets to zero without moving the telescope.\n";
  print "\n";
  print "\tThis script will define lock positions 2 & 3 as beams A & B,\n";
  print "\tand place markers at each location.\n";
  print "\n";
  print "\tOrder of action:\n";
  print "\t1) Turn off guiding.\n";
  print "\t2) Move guide and AOI boxes to beam B and define as pos3, and place marker.\n";
  print "\t3) Move guide and AOI boxes to beam A and define as pos2, and place marker.\n";
  print "\t4) Offset telescope to beam A.\n";
  print "\t5) Turn on guiding.\n";
  print "\n\n";
}#Endsub print_msg


sub write_box_positions{
  my ($gpos2_x, $gpos2_y, $gpos3_x, $gpos3_y) = @_;

  print "Writing box positions to GPOS#_X and GPOS#_Y header keywords\n";

  Fitsheader::setNumericParam( 'GPOS2_X', $gpos2_x );
  Fitsheader::setNumericParam( 'GPOS2_Y', $gpos2_y );

  Fitsheader::setNumericParam( 'GPOS3_X', $gpos3_x );
  Fitsheader::setNumericParam( 'GPOS3_Y', $gpos3_y );

  Fitsheader::writeFitsHeader( $header );
  print "\n";
}#Endsub write_box_positions

#!/usr/local/bin/perl -w

my $rcsId = q($Name:  $ $Id: guide.mark.current.position.kp2m.pl 14 2008-06-11 01:49:45Z hon $);

use strict;
use Gdr_Rpc qw/:all/;

my $gdrrpc = $ENV{UF_KP2m_GDRRPC};

my ($box_x, $box_y) = Gdr_Rpc::get_current_position( $gdrrpc );

print "Present location of guide box = $box_x, $box_y\n";

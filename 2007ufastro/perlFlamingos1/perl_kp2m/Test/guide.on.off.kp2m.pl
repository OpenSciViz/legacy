#!/usr/local/bin/perl -w

my $rcsId = q($Name:  $ $Id: guide.on.off.kp2m.pl 14 2008-06-11 01:49:45Z hon $);

use strict;
use Gdr_Rpc qw/:all/;

my $gdrrpc = $ENV{UF_KP2m_GDRRPC};

my $cnt = @ARGV;
if( $cnt < 1 ) {
   die "\n\tUsage: guide.on.off.kp2m.pl on|off\n\n";

}else{
  my $state = shift;

  if( $state =~ m/^on$/i) {
    print "Turning guiding on\n";
    Gdr_Rpc::guide_on( $gdrrpc );

  }elsif( $state =~ m/^off$/i) {
    print "Turning guiding off\n";
    Gdr_Rpc::guide_off( $gdrrpc );

  }else{
    die "\n\n\tPlease enter guide.on.off.kp2m.pl on or guide.on.off.kp2m.pl off\n\n";
    
  }
  
}#End

#!/usr/local/bin/perl -w

my $rcsId = q($Name:  $ $Id: guide.mos.offset.kp2m.pl 14 2008-06-11 01:49:45Z hon $);

use strict;

use Getopt::Long;
use Fitsheader qw/:all/;
use Gdr_Rpc qw/:all/;

my $DEBUG = 0;
#GetOptions( 'debug' => \$DEBUG );

my $header   = Fitsheader::select_header($DEBUG);
my $lut_name = Fitsheader::select_lut($DEBUG);
Fitsheader::Find_Fitsheader($header);
Fitsheader::readFitsHeader($header);

my $cnt = @ARGV;
my $gdr_ps = $ENV{GDR_PS};
if( $cnt < 2 ){
  die "\n\n".
      "USAGE:  guide.mos.offset.kp2m.pl ra_offset dec_offset\n".
      "        Offsets are relative to present position, and in arcsec.\n".
      "\n".
      "WHAT IT WILL DO:\n".
      "        1) Turn off guiding\n".
      "        2) Offset the telescope\n".
      "        3) Offset the guide and aoi boxes\n".
      "        4) Resume guiding\n\n".
      "\n".
      "        Note: The minimum size offsets that will be performed by the guider are\n".
      "        guide box: 1 guider pixels = $gdr_ps arcsec.\n".
      "        AOI   box: 2 guider pixels = ".(2*$gdr_ps)." arcsec.\n\n".
      "        The env variable GDR_PS = $gdr_ps tells this script the guider plate scale.\n\n".

      "********NEED to check signs of guider offsets match signs of ra/dec***********\n".
      "********NEED to check GDR_PS value********************************************\n\n";
}

my $ra_offset  = shift;
my $dec_offset = shift;

if( $ra_offset =~ m/[a-zA-Z]/ or $dec_offset =~ m/[a-zA-Z]/ ){
  die "\nPlease enter numerical offsets.\n\n";
}


my $telescop = Fitsheader::param_value( 'TELESCOP' );
my $tlen = length $telescop;
print "telescop is $telescop, len is $tlen\n\n";

my $gdrrpc = $ENV{UF_KP2m_GDRRPC};
Gdr_Rpc::guide_off( $gdrrpc );
do_offset( $ra_offset, $dec_offset );
offset_wait();

my ($ra_offset_pix, $dec_offset_pix) = compute_guide_offset( $gdr_ps, $ra_offset, $dec_offset );
Gdr_Rpc::move_guide_and_aoi_boxes( $gdrrpc, $ra_offset_pix, $dec_offset_pix );

Gdr_Rpc::guide_on( $gdrrpc );

### SUBS
sub compute_guide_offset{
  my ($gdr_ps, $ra_arcsec, $dec_arcsec) = @_;

  my $ra_offset_pix  = $ra_arcsec / $gdr_ps;
  my $dec_offset_pix = $dec_arcsec / $gdr_ps;

  $ra_offset_pix  = sprintf "%.2f", $ra_offset_pix;
  $dec_offset_pix = sprintf "%.2f", $dec_offset_pix;

  #print " ra_offset_pix = $ra_offset_pix\n".
  #      "dec_offset_pix = $dec_offset_pix\n\n";
  
  use Math::Round qw/ round /;
  $ra_offset_pix  = Math::Round::round( $ra_offset_pix  );
  $dec_offset_pix = Math::Round::round( $dec_offset_pix );

  #$ra_offset_pix  = round_input( $ra_offset_pix  );
  #$dec_offset_pix = round_input( $dec_offset_pix );

  #die " ra_offset_pix = $ra_offset_pix\n".
  #    "dec_offset_pix = $dec_offset_pix\n\n";

  ### +RA  = East  = -x for guide box
  ### +DEC = North = -y for guide box
  $ra_offset_pix  = -1*$ra_offset_pix;
  $dec_offset_pix =1*$dec_offset_pix;

  return( $ra_offset_pix, $dec_offset_pix );
}#Endsub compute_guide_offset


sub do_offset{
  my ( $dX, $dY ) = @_;

  my $tcs  = $ENV{UF_KPNO_TCS_PROG};
  my $space = $ENV{SPACE};
  my $head = $ENV{TCS_REL_OFFSET_HEAD};
  my $sep  = $ENV{TCS_OFFSET_SEP};
  my $tail = $ENV{TCS_OFFSET_TAIL};

  my $offset_cmd = $tcs.$space.$head.$dX.$sep.$dY.$tail;
  print "EXECUTING RELATIVE OFFSET COMMAND:\n";
  print "$offset_cmd\n";

  my $reply;
  $reply = `$offset_cmd`;
  print "$reply\n";
}#Endsub do_offset


sub offset_wait{
  my $wait = $ENV{OFFSET_WAIT};

  #print "Waiting $wait sec for offset\n";
  #sleep( $wait );

  my $cnt = 1;
  print "\n";
  until( $cnt > $wait ){
    print "Wait $cnt seconds out of $wait seconds for telescope\n";
    sleep 1;
    $cnt++;
  }
  print "\n\n";
}#Endsub offset_wait



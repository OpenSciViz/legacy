#!/usr/local/bin/perl -w

my $rcsId = q($Name:  $ $Id: guide.warp.kp2m.pl 14 2008-06-11 01:49:45Z hon $);

use strict;
use Gdr_Rpc qw/:all/;
use GetYN qw/:all/;

my $gdrrpc = $ENV{UF_KP2m_GDRRPC};
print "\n\n";
print "\tThis will center the guide box on the brightest object.\n";
print "\tGuiding must be off.\n";

GetYN::query_ready();

my $warp_cmd = $gdrrpc." guide warp";

print "Executing: $warp_cmd\n";
my $reply = `$warp_cmd`;

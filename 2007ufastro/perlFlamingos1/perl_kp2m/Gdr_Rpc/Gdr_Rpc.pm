package Gdr_Rpc;


require 5.005_62;
use strict;
use warnings;

require Exporter;
use AutoLoader qw(AUTOLOAD);

our @ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use Gdr_Rpc ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.
our %EXPORT_TAGS = ( 'all' => [ qw(
				   &clear_all_lock_positions
				   &define_lock_position
				   &flip_y
				   &get_current_position
				   &get_guide_correction
				   &guide_off
				   &guide_on
				   &guide_wait
				   &mark_current_position
				   &move_aoi_box
				   &move_guide_and_aoi_boxes
				   &move_guide_box
				   &select_guide_pos1
				   &select_guide_pos2
				   &select_guide_pos3
				   &select_guide_pos4
				   &send_guide_and_aoi_boxes
				   &set_real_valued_lock_position
				   &warp_boxes_to_star
) ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(
	
);

use vars qw/ $VERSION $LOCKER /;
'$Revision: 0.1 $ ' =~ /.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ ' =~ /.*:\s(.*)\s\$/ && ($LOCKER = $1);



# Preloaded methods go here.
sub clear_all_lock_positions{
  my $gdrrpc = $_[0];

  my $clear_cmd = $gdrrpc . " guide posclear";
  print "Clearing all guide positions!\n";
  print "Executing $clear_cmd\n\n";

  my $reply = `$clear_cmd`;
  print "gdrrpc reply = $reply\n";
  print "Done!\n\n";

}#Endsub clear_all_lock_positions


sub define_lock_position{
  my ($gdrrpc, $position) = @_;

  my $cmd = $gdrrpc." guide defpos".$position;
  print "Executing $cmd\n\n";
  my $reply = `$cmd`;

  print "gdrrpc reply = $reply\n";
}#Endsub define_lock_position


sub flip_y{
  my $in_y = $_[0];

  #The marker coordinate system is flipped in y wrt the guide box coordinate system
  #The guider size is 640 x 480 (x,y)

  my $ref = 240;
  my $out_y;
  my $delta;
  if( $in_y > $ref ){
    $delta = $in_y - $ref;
    $out_y = $ref - $delta;

  }elsif( $in_y < $ref ){
    $delta = $ref - $in_y;
    $out_y = $ref + $delta;

  }elsif( $in_y == $ref ){
    $out_y = 240;
  }

  return $out_y;
}#Endsub flip_y


sub get_current_position{
  my $gdrrpc = $_[0];

  my $cmd_info = $gdrrpc." info";
  print "Asking for present info\n";
  my $info_reply = `$cmd_info`;
  chomp $info_reply;

  my @info = split / /,$info_reply;
  my $box_x = $info[13];
  my $box_y = $info[14];

  use Math::Round qw/round/;
  #print "Un-rounded guide-center = $box_x, $box_y\n";
  $box_x = Math::Round::round( $box_x );
  $box_y = Math::Round::round( $box_y );

  return( $box_x, $box_y );
}#Endsub get_current_position


sub get_current_lock_position{
  my $gdrrpc = $_[0];

  my $cmd_info = $gdrrpc." info";
  print "Asking for present info\n";
  my $info_reply = `$cmd_info`;
  chomp $info_reply;

  print "<><><><><>\n";
  print "guide info query returns:\n";
  print "$info_reply\n\n";
  print "now parsing out the guide info in fields 13 & 14\n";
  print "<><><><><>\n";

  my @info = split / /,$info_reply;
  my $box_x = $info[13];
  my $box_y = $info[14];
  #The returned values will be reals, in the same location in the return string
  #as the box coordinates are when the lock position is not active.
  #The lock position info doesn't change until the next guide pos# is issued.

  return( $box_x, $box_y );
}#Endsub get_current_lock_position


sub get_guide_correction{
  my $gdrrpc = $_[0];

  my $cmd_info = $gdrrpc." info";
  print "Asking for present info\n";
  my $info_reply = `$cmd_info`;
  chomp $info_reply;

  print "<><><><><>\n";
  print "guide info query returns:\n";
  print "$info_reply\n\n";
  print "now parsing out the guide corrections info in fields 1 & 2\n";
  print "<><><><><>\n";

  my @info = split / /,$info_reply;
  my $x_correction = $info[1];
  my $y_correction = $info[2];

  return( $x_correction, $y_correction );
}#Endsub get_guide_correction


sub guide_off{
  my $gdrrpc = $_[0];

  my $cmd = $gdrrpc." guide off";

  #THE FOLLOWING DOES NOT WORK---snr 2002 Dec 15
  #my $tcs = $ENV{UF_KPNO_TCS_PROG};
  #my $cmd = $tcs." \"guider mode=inactive\"";

  print "Executing $cmd\n\n";
  my $reply = `$cmd`;

  #
  #print "tcs2m reply = $reply\n";
}#Endsub guide_off


sub guide_on{
  my $gdrrpc = $_[0];

  my $cmd = $gdrrpc." guide on";

  #THE FOLLOWING DOES NOT WORK----snr 2002 Dec 15
  #my $tcs = $ENV{UF_KPNO_TCS_PROG};
  #my $cmd = $tcs." \"guider mode=on\"";

  print "Executing $cmd\n\n";
  my $reply = `$cmd`;

  #
  #print "tcs2m reply = $reply\n";
}#Endsub guide_on


sub guide_wait{
  my $wait = $ENV{GUIDE_WAIT};

  my $cnt = 1;
  print "\n";
  until( $cnt > $wait ){
    print "Wait $cnt seconds out of $wait seconds for guider.\n";
    sleep 1;
    $cnt++;
  }
  print "\n\n";
}#Endsub guide_wait


sub mark_current_position{
  my ($gdrrpc, $x, $y) = @_;

  my $cmd = $gdrrpc." marker add $x $y";
  print "Executing $cmd\n\n";
  my $reply = `$cmd`;

  print "gdrrpc reply = $reply\n";
}#Endsub mark_current_position


sub move_aoi_box{
  my ($gdrrpc, $ra_offset_pix, $dec_offset_pix) = @_;
  #The input offsets are in pixels

  my $cmd_aoi   = $gdrrpc." aoi offset $ra_offset_pix $dec_offset_pix ";

  print "Offseting AOI box\n";
  print "Executing: $cmd_aoi\n";
  my $reply = `$cmd_aoi`;
  print "\n";

  print "gdrrpc reply = $reply\n";
}#Endsub move_aoi_box


sub move_guide_and_aoi_boxes{
  my ($gdrrpc, $ra_offset_pix, $dec_offset_pix) = @_;
  #The input offsets are in pixels

  move_guide_box( $gdrrpc, $ra_offset_pix, $dec_offset_pix );
  move_aoi_box( $gdrrpc, $ra_offset_pix, $dec_offset_pix );

}#Endsub move_guide_and_aoi_boxes


sub move_guide_box{
  my ($gdrrpc, $ra_offset_pix, $dec_offset_pix) = @_;
  #The input offsets are in pixels

  my $cmd_guide = $gdrrpc." offset $ra_offset_pix $dec_offset_pix guide";

  print "Offseting guide box\n";
  print "Executing: $cmd_guide\n";
  my $reply = `$cmd_guide`;

  print "gdrrpc reply = $reply\n";
}#Endsub move_guide_box


sub select_guide_pos1{
  my $gdrrpc = $_[0];

  my $cmd = $gdrrpc." guide pos1";
  print "Moving guide box to lock position 1.\n\n";
  print "Executing $cmd\n";
  my $reply = `$cmd`;

  print "gdrrpc reply = $reply\n";
}#Endsub guide_pos1


sub select_guide_pos2{
  my $gdrrpc = $_[0];

  my $cmd = $gdrrpc." guide pos2";
  print "Moving guide box to lock position 2.\n\n";
  print "Executing $cmd\n";
  my $reply = `$cmd`;

  print "gdrrpc reply = $reply\n";
}#Endsub guide_pos2


sub select_guide_pos3{
  my $gdrrpc = $_[0];

  my $cmd = $gdrrpc." guide pos3";
  print "Moving guide box to lock position 3.\n\n";
  print "Executing $cmd\n";
  my $reply = `$cmd`;

  print "gdrrpc reply = $reply\n";
}#Endsub guide_pos3


sub select_guide_pos4{
  my $gdrrpc = $_[0];

  my $cmd = $gdrrpc." guide pos4";
  print "Moving guide box to lock position 4.\n\n";
  print "Executing $cmd\n";
  my $reply = `$cmd`;

  print "gdrrpc reply = $reply\n";
}#Endsub guide_pos4


sub send_guide_and_aoi_boxes{
  my ($gdrrpc, $ra_absolute_pix, $dec_absolute_pix) = @_;
  #The input offsets are in pixels

  my $cmd  = $gdrrpc." goto $ra_absolute_pix $dec_absolute_pix ";

  print "Offseting guide & AOI boxes\n";
  print "Executing: $cmd\n";
  my $reply = `$cmd`;

  print "gdrrpc reply = $reply\n";
}#Endsub send_guide_and_aoi_boxes


sub set_real_valued_lock_position{
  my ($gdrrpc, $pos_number, $x, $y) = @_;

  my $cmd = $gdrrpc." guide setpos".$pos_number." $x $y";

  print "Setting real-valued lock position.\n";
  print "The guide boxes will not move until the lock position is selected.\n";
  print "Executing: $cmd\n";
  my $reply = `$cmd`;

  print "gdrrpc reply = $reply\n";
}#Endsub set_real_valued_lock_position


sub warp_boxes_to_star{
  my $gdrrpc = $_[0];

  #print "\tThis will center the guide box on the brightest object.\n";
  #print "\tGuiding must be off.\n";

  my $warp_cmd = $gdrrpc." guide warp";
  print "Executing: $warp_cmd\n";
  my $reply = `$warp_cmd`;
}#Endsub warp_boxes_to_star

# Autoload methods go after =cut, and are processed by the autosplit program.

1;
__END__
# Below is stub documentation for your module. You better edit it!

=head1 NAME

Gdr_Rpc - Perl extension for Flamingos

=head1 SYNOPSIS

  use Gdr_Rpc;


=head1 DESCRIPTION

A library for interacting with the 2.1-m guider software.

=head2 EXPORT

None by default.


=head1 REVISION & LOCKER

$Name:  $

$Id: Gdr_Rpc.pm,v 0.1 2003/05/22 15:45:40 raines Exp $

$Locker:  $


=head1 AUTHOR

SNR.

=head1 SEE ALSO

perl(1).

=cut

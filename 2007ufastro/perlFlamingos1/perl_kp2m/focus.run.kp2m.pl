#!/usr/local/bin/perl -w

use vars qw/ $VERSION $LOCKER/;

'$Revision: 0.1 $ ' =~ m/.*:\s(.*)\s\$/ && ($VERSION = $1);
'$Locker:  $ '   =~ m/.*:\s(.*)\s\$/ && ($LOCKER  = $1);

print "\n";
print "--------------------------\n";
print "Revision number = $VERSION\n";
print "Locked by         $LOCKER \n";
print "--------------------------\n";
print "\n";


use strict;
use Getopt::Long;
use Fitsheader qw/:all/;

my $DEBUG = 0;#0 = not debugging,1 = debugging
my $start = -666;
my $end   = -666;
my $step  = -666;

GetOptions( 'start=i' => \$start,
	    'end=i'   => \$end,
	    'step=i'  => \$step,
	    'debug'   => \$DEBUG);

my $header   = Fitsheader::select_header($DEBUG);
my $lut_name = Fitsheader::select_lut($DEBUG);
Fitsheader::Find_Fitsheader($header);
Fitsheader::readFitsHeader($header);

my ($number_exp, $sign) = check_input_values();

print_info();
print "Will take $number_exp exposures with the above parameters:\n\n";
use GetYN;
print "Start exposure sequence?";
my $change = GetYN::get_yn();
if( $change == 0 ){
  die "Exiting\n\n";
}

my $this_focus = $start;
setNumericParam( "TELFOCUS", $this_focus );
writeFitsHeader( $header );
my $settle_time = $ENV{FOCUS_SETTLE_TIME};
for( my $i=1; $i <=$number_exp; $i++ ){
  print "\n\n\n\n\n".
        "***********************************************************************\n";
  print "Beginning exposure $i of $number_exp....\n";

  change_focus( $this_focus );
  print "Wait $settle_time seconds for focus settling \n";
  sleep( $settle_time );

  #print "SHOULD BE EXECUTING singleimage.pl\n";
  print "EXECUTING singleimage.pl\n";
  my $singleimage_script = $ENV{SINGLEIMAGE};
  system( $singleimage_script );

  $this_focus = next_focus( $this_focus );
  setNumericParam( "TELFOCUS", $this_focus );
  writeFitsHeader( $header );
}

print "\a\a\a\a\a\a\a\a\a\a\a\a\a\n\n";
print "focus.run.kp2m.pl done!\n\n";

####SUBS
sub check_input_values{
  if( $start == -666 ){ 
    usage();
    die "\n\nPlease enter a numerical starting focus\n\n";
  }
  if( $end == -666 ){ 
    usage();
    die "\n\nPlease enter a numerical ending focus\n\n";
  }
  if( $step == -666 ){ 
    usage();
    die "\n\nPlease enter a numerical step in focus\n\n";
  }
  if( $start == $end ){
    usage();
    die "\n\nPlease enter different starting and ending foci\n";
  }
  my $number_exp = abs( ($start - $end)/$step ) + 1;
  my $sign = +1;
  if( $start > $end ){
    $sign = -1;
  }

  return ($number_exp, $sign);
}#Endsub check_input_values


sub usage{
  print "Usage: focus.run.kp2m.pl -start start_focus -end end_focus -step step_focus\n";
}#Endsub usage


sub print_info{
  print "----------------------------------------------------------------------------\n";
  Fitsheader::printFitsHeader( "OBS_TYPE", "OBJECT" );
  print "----------------------------------------------------------------------------\n";
  Fitsheader::printFitsHeader( "FILEBASE", "ORIG_DIR" );
  print "----------------------------------------------------------------------------\n";
  Fitsheader::printFitsHeader( "EXP_TIME",  "NREADS", "BIAS" );
  print "----------------------------------------------------------------------------\n";
  print "***LAST KNOWN FILTER POSITIONS:\n";
  print "***(run config.wheels.pl to check actual positions)\n";
  Fitsheader::printFitsHeader( "DECKER", "MOS", "SLIT", "FILTER", "LYOT", "GRISM" );
  print "----------------------------------------------------------------------------\n";
  print "Focus run parameters:\n";
  print "Start = $start\n";
  print "End   = $end\n";
  print "Step  = $step\n";
  print "----------------------------------------------------------------------------\n";
}#Endsub print_info


sub next_focus{
  my $input_foc = $_[0];
  my $next_foc = $input_foc + $sign*$step;
  return $next_foc;
}#Endsub next_focus


sub change_focus{
  my $this_focus = $_[0];

  my $tcs_client = $ENV{UF_KPNO_TCS_PROG};
  my $focus_head_cmd = $ENV{TCS_FOCUS_HEAD};
  my $tcs_tail_cmd = $ENV{TCS_TAIL};
  my $space = " ";
  my $set_focus_cmd = $tcs_client.$space.$focus_head_cmd.$this_focus.$tcs_tail_cmd;

  #print "SHOULD BE SENDING:\n";
  print "SETING THE FOCUS:\n";
  print "$set_focus_cmd\n";
  system( $set_focus_cmd );
}#Endsub change_focus


__END__

=head1 NAME

focus.run.kp2m.pl

=head1 Description

I do not think this script actually works.


=head1 REVISION & LOCKER

$Name:  $

$Id: focus.run.kp2m.pl 14 2008-06-11 01:49:45Z hon $

$Locker:  $


=head1 AUTHOR

SNR, 2003 May 15

=head1 SEE ALSO

L<perl>

=cut

function MEF_read, MEF_file, hdmain, DISPLAY=display, NODBEAMS=nodbeams, NODSETS=nodsets

	fits_read, MEF_file, d, hdmain, EXTEN=0

	obsmode = sxpar( hdmain, "OBSMODE" )
	savesets = sxpar( hdmain, "SAVESETS" )
	ncoadds = sxpar(hdmain,"FRMCOADD") * sxpar(hdmain,"CHPCOADD")

	if NOT keyword_set( nodsets ) then nodsets = sxpar( hdmain, "NODSETS" )
	if NOT keyword_set( nodbeams ) then nodbeams = sxpar( hdmain, "NNODS" )

	if nodsets LE 0 then nodsets = 1
	if nodbeams LE 0 then nodbeams = 1
	totNodBeams = nodbeams * nodsets

	help,obsmode,ncoadds,savesets,nodsets,nodbeams
	iex=0

	for iset = 1, nodsets do begin
		for ibeam = 1, nodbeams do begin
			iex = iex+1
			fits_read, MEF_file, data, hd, EXTEN=iex
			nodbeam = strtrim( sxpar( hd, "NOD" ), 2 )
			print,"NodBeam=",nodbeam,iset,ibeam,iex
			help,data
			if( iex EQ 1 ) then begin
				s = size( data )
				datafull = Lonarr( s[1],s[2],s[3],s[4], nodbeams, nodsets )
			   endif
			datafull[0,0,0,0,ibeam-1,iset-1] = data
		   endfor
	   endfor

	mkhdr, newhd, datafull
	s = size( datafull )
	hdmain = [ newhd[0:3+s[0]], hdmain[3:*] ]

return, datafull
end

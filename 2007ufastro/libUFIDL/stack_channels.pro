function stack_channels, image, NCHANNELS=nchan

	if N_elements( nchan ) ne 1 then nchan=16
	sz = size( image )
	nx = sz[1]/nchan
	chan_stack = fltarr( nx, sz[2], nchan )

	for ic = 0,nchan-1 do chan_stack[*,*,ic] = image[ ic*nx:(ic+1)*nx-1, * ]

return, chan_stack
end

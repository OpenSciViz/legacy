pro plotchans, sigmas, means, HEADER=fhd

	sz = size( sigmas )
	ndat = sz[2]
	if sz[0] LT 2 then ndat=1
	nchan = sz[1]
	chans = indgen(nchan)+1

	if N_elements( fhd ) gt 1 then begin
		date = sxpar(fhd,"DATE_FH")
		ft = sxpar(fhd,"FRMTIME")
		fc = sxpar(fhd,"FRMCOADD")
		ptit = date + ", FT=" + strtrim(ft,2) + ", FC=" + strtrim(fc,2)
	 endif else ptit=""

	get_window,0
	plot,chans,sigmas[*,0],ps=10, $
		XTIT="Channel #",YTIT="sigma( noise ) ADUs",TIT=ptit
	for j=0,ndat-1 do oplot,chans,sigmas[*,j],ps=10

	get_window,1
	plot,chans,means[*,0],ps=10, $
		XTIT="Channel #",YTIT="Mean Val",TIT=ptit
	for j=0,ndat-1 do oplot,chans,means[*,j],ps=10
end

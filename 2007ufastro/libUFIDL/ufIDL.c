#if !defined(__UFIDL_c__)
#define __UFIDL_c__ "$Name:  $ $Id: ufIDL.c,v 0.59 2003/12/30 23:11:14 varosi beta $"
static const char rcsId[] = __UFIDL_c__;

#include "unistd.h"
#include "stdlib.h"
#include "stdio.h"
#include "dirent.h"
#include "idl/external/export.h"

/* build IDL version of logger here: */
#include "ufLog.c" /* log (info/error messages, status & alarms?) functions */

#include "ufClient.h" /* all socket transaction functions */
#include "ufSerial.h" /* local serial port & annex networked serial ports */

static char _UFerrmsg[MAXNAMLEN + 1];

/***
 * Following functions provide the interface between IDL and libUFClient.so,
 * which then opens/closes sockets, sends/recvs UFLIB objects over sockets, etc.
 * This file (ufIDL.c) should be compiled and linked to create libUFIDL.so,
 * providing IDL Dynamic Load Modules corresponding to libUFIDL.dlm.
 * For IDL to Load it, libUFIDL.so and libUFIDL.dlm must be in current directory
 * or in IDL_DLM_PATH (called !DLM_PATH inside IDL).
 */

/**************************** system functions ************************/

static IDL_VPTR UFhostname(int argc, IDL_VPTR argv[]) {
  /*
   * argv = (none).
   * retval = string: "hostname".
   */ 
  return IDL_StrToSTRING( (char*)ufHostName() );
}/*---------------------------------------------------------------------------*/

static IDL_VPTR UFhostTime(int argc, IDL_VPTR argv[]) {
  /*
   * argv = string: "timezone" (optional input, default=local).
   * retval = string: "timestring".
   */ 
  IDL_VPTR iv_tz;
  IDL_STRING *tp;
  char* tz;
  
  if( argc > 0 ) {
  	iv_tz = argv[0];
  	IDL_ENSURE_STRING( iv_tz );
  	IDL_ENSURE_SCALAR( iv_tz );
  	tp = (IDL_STRING*) &(iv_tz->value.str); 
	if( tp->slen > 0 ) tz = tp->s;  else tz = "";
  } else
  	tz = "";

  return IDL_StrToSTRING( (char*)ufHostTime(tz) );
}/*-----------------------------------------------------------------------------*/

static IDL_VPTR UFcheckfifo(int argc, IDL_VPTR argv[]) {
  /*
   * argv = string: "fifoname".
   * retval = # bytes available.
   */   
  IDL_VPTR f= argv[0];
  IDL_STRING *fp;
  int nb;

  IDL_ENSURE_STRING( f );
  IDL_ENSURE_SCALAR( f );
  
  fp = (IDL_STRING*) &(f->value.str); 
  nb = ufFifoAvailable( IDL_STRING_STR(fp) );
  
  return IDL_GettmpLong(nb);
}/*-----------------------------------------------------------------------------*/

static IDL_VPTR UFchecksocket(int argc, IDL_VPTR argv[]) {
  /*
   * argv = socket F.D. (long int).
   * retval = # bytes available.
   */   
  int sockFd = IDL_LongScalar( argv[0] );
  int nb;

  nb = ufAvailable( sockFd );
  
  return IDL_GettmpLong(nb);
}/*-----------------------------------------------------------------------------*/

static IDL_VPTR UFset_timeout(int argc, IDL_VPTR argv[]) {
  /*
   * argv = timeout for socket recv, in seconds (float or double).
   * retval = max # trys in ufRecv (int).
   */   
  double timeout = IDL_DoubleScalar( argv[0] );
  int maxtry;

  maxtry = ufSetTimeout( (float )timeout );
  
  return IDL_GettmpLong(maxtry);
}

/**************************** uf client services ************************/

static IDL_VPTR UFagentNames(int argc, IDL_VPTR argv[]) {
  /*
   * argv = (none).
   * retval = array of IDL strings containing names of recognized agents.
   */   
  IDL_VPTR vts;
  IDL_STRING *sdata;
  static IDL_LONG one=1, sdims[1];
  int Nagents, n;
  char **AgentNames;   /* do NOT ever free (delete) these strings */

  AgentNames = ufAgents( &Nagents );
  
  if( Nagents <= 0 ) return IDL_GettmpLong( Nagents );

  sdims[0] = (IDL_LONG )Nagents;
  sdata = (IDL_STRING *)IDL_MakeTempArray( IDL_TYP_STRING, one, sdims,
                                           IDL_BARR_INI_INDEX, &vts );

  IDL_StrDelete( sdata, Nagents ); /* note:  sdata is contained in vts */
  
  for( n=0; n<Nagents; n++ ) {
	IDL_StrStore( sdata, *AgentNames );  /* copy strings to IDL descriptors */
  	sdata++;
	AgentNames++;
  }

  return vts;	/* return the IDL var pointer to string array (sdata) */
}/*--------------------------------------------------------------------------*/

static IDL_VPTR UFservices(int argc, IDL_VPTR argv[]) {
  /*
   * argv = string: list of services (output),
   * retval = int, number of agents defined.
   */   
  IDL_VPTR s, sin=argv[0];
  char *tmp;
  int n;

  IDL_ENSURE_STRING( sin );
  IDL_ENSURE_SCALAR( sin );
  
  n = ufServices(&tmp);

  s = IDL_StrToSTRING(tmp);
  IDL_VarCopy( s, sin );    /* this IDL var pointer arg is ready for return */
  
  return IDL_GettmpLong(n);
}/*--------------------------------------------------------------------------*/

static IDL_VPTR UFframeConnect(int argc, IDL_VPTR argv[]) {
  /*
   * argv = string: "server host name",
   *        integer: port number.
   * retval = int socket fd
   */   
  IDL_VPTR h=argv[0];
  IDL_STRING *hp;
  char* host;
  int sockFd, portNo = IDL_LongScalar( argv[1] );

  IDL_ENSURE_STRING( h );
  IDL_ENSURE_SCALAR( h );
  hp = (IDL_STRING*) &(h->value.str);
  
  if( hp->slen > 0 )
    host = hp->s;
  else
    host = (char*)ufHostName();

  sprintf(_UFerrmsg,"Connecting to frame server on host= %s, using portNo= %d", host, portNo);
  IDL_Message(IDL_M_NAMED_GENERIC, IDL_MSG_INFO, _UFerrmsg);

  sockFd = ufConnectAgent( "frame", host, portNo );

  return IDL_GettmpLong( sockFd );
}/*--------------------------------------------------------------------------*/

static IDL_VPTR UFframeClose(int argc, IDL_VPTR argv[])
{
  return IDL_GettmpLong( ufCloseAgent("frame") );
}/*--------------------------------------------------------------------------*/

/************ UF protocol object i/o: ************/

/* The Frame Server responds to following requests:
 *  "FC" : request Frame Config object,
 *  "OC" : request Obs Config object,
 *  "BN" : request buffer names (UFStrings object),
 *  "buffer name" : request frame from buffer (UFFrameConfig and UFInts objects).
 *
 * These client requests are sent to Frame Server by function UF_request,
 *   e.g.: IDL> nbsent = UF_request("FC")
 *
 * The requested protocol object is then sent by the Frame Server to client,
 * and should then be received by the client, using UFrecv* functions below.
 * The UFsend_* functions send the objects to server.
 */

static IDL_VPTR UF_request(int argc, IDL_VPTR argv[]) {
  /*
   * Send a UFTimeStamp request to a server/agent
   *  (calls C-function ufRequest which puts the request into name field of header):
   * argv = string: "request" (input):
   *        string: "agent/server name" (optional input, default="frame").
   * retval = number of bytes sent (= length of UFTimeStamp with request string).
   */
  IDL_VPTR iv_agnt, iv_reqs=argv[0];
  IDL_STRING *agntp; 
  char *agent, *request;
  int nb;
  
  if( argc > 1 ) {
  	iv_agnt = argv[1];
  	IDL_ENSURE_STRING( iv_agnt );
  	IDL_ENSURE_SCALAR( iv_agnt );
	agntp = (IDL_STRING *)&(iv_agnt->value.str); 
	if( agntp->slen > 0 ) agent = agntp->s;  else agent = "frame";
  } else
  	agent = "frame";
  	
  IDL_ENSURE_STRING( iv_reqs );
  IDL_ENSURE_SCALAR( iv_reqs );
  request = IDL_STRING_STR( &(iv_reqs->value.str) ); 
  
  if( argc > 1 ) {
  	sprintf(_UFerrmsg,"Requesting: %s from agent: %s", request, agent );
  	IDL_Message(IDL_M_NAMED_GENERIC, IDL_MSG_INFO, _UFerrmsg);
  }
  
  nb = ufRequest( agent, request );
  
  if( nb <= 0 ) {
    sprintf(_UFerrmsg,"Request for %s from agent %s failed!", request, agent );
    IDL_Message(IDL_M_NAMED_GENERIC, IDL_MSG_INFO, _UFerrmsg);
  }
  return IDL_GettmpLong( nb );  
}/*-------------------------------------------------------------------------*/

static IDL_VPTR UFrecv_FrameConf(int argc, IDL_VPTR argv[]) {
  /*
   * argv = "agent name" (optional input, default = "frame"),
   * retval = IDL structure: Frame Configuration.
   */
  IDL_VPTR iv_agnt, FrameConf;
  IDL_STRING *agntp; 
  char *agent;
  static IDL_LONG one = 1;
  void *sdef;
  int nb;
  static ufFrameConfig uffc;

  static IDL_LONG timestamp_dims[] = { 1, 25 };
  static IDL_LONG name_dims[] = { 1, 83 };
  
  static IDL_STRUCT_TAG_DEF s_tags[] = {
  	{ "LENGTH",	0, (void *) IDL_TYP_LONG },	
	{ "TYPE",	0, (void *) IDL_TYP_LONG  },	
	{ "ELEM",	0, (void *) IDL_TYP_LONG },	
	{ "SEQCNT",	0, (void *) IDL_TYP_UINT },	
	{ "SEQTOT",	0, (void *) IDL_TYP_UINT },	
	{ "DURATION",	0, (void *) IDL_TYP_FLOAT },	
	{ "TIMESTAMP",	timestamp_dims, (void *) IDL_TYP_BYTE },	
	{ "NAME",	name_dims, (void *) IDL_TYP_BYTE },	
	{ "W",		0, (void *) IDL_TYP_LONG },
	{ "H",		0, (void *) IDL_TYP_LONG },		
	{ "D",		0, (void *) IDL_TYP_LONG },		
	{ "LITTLEEND",	0, (void *) IDL_TYP_LONG },	
	{ "DMACNT",	0, (void *) IDL_TYP_LONG },	
	{ "FRAMEGRABCNT", 0, (void *) IDL_TYP_LONG },	
	{ "COADDS",	0, (void *) IDL_TYP_LONG },	
	{ "PIXELSORT",	0, (void *) IDL_TYP_LONG },	
	{ "FRAMEOBSSEQNO",  0, (void *) IDL_TYP_LONG },	
	{ "FRAMEOBSSEQTOT", 0, (void *) IDL_TYP_LONG },
  	{ "CHOPBEAM",	0, (void *) IDL_TYP_LONG },	
  	{ "SAVESET",	0, (void *) IDL_TYP_LONG },	
  	{ "NODBEAM",	0, (void *) IDL_TYP_LONG },	
  	{ "NODSET",	0, (void *) IDL_TYP_LONG },	
  	{ "FRAMEWRITECNT",  0, (void *) IDL_TYP_LONG },	
  	{ "FRAMESENDCNT",   0, (void *) IDL_TYP_LONG },	
  	{ "BG_ADUS",	    0, (void *) IDL_TYP_LONG },	
  	{ "BG_WELL",        0, (void *) IDL_TYP_FLOAT },	
  	{ "SIGMAFRMNOISE",  0, (void *) IDL_TYP_FLOAT },	
  	{ "RD_ADUS",	    0, (void *) IDL_TYP_LONG },	
  	{ "RD_WELL",        0, (void *) IDL_TYP_FLOAT },	
  	{ "SIGMAREADNOISE", 0, (void *) IDL_TYP_FLOAT },	
	{ "FRAMECOADDS",    0, (void *) IDL_TYP_LONG },
	{ "CHOPSETTLEFRMS", 0, (void *) IDL_TYP_LONG },
	{ "CHOPCOADDS",     0, (void *) IDL_TYP_LONG },
	{ "FRAMETIME",      0, (void *) IDL_TYP_FLOAT },
	{ "SAVEPERIOD",     0, (void *) IDL_TYP_FLOAT },
	{ "OFFSET",     0, (void *) IDL_TYP_LONG },
	{ "PIXCNT",     0, (void *) IDL_TYP_LONG },
  	{ "BG_ADU_MIN",	    0, (void *) IDL_TYP_LONG },	
  	{ "BG_ADU_MAX",	    0, (void *) IDL_TYP_LONG },	
  	{ "SIGMAFRM_MIN",  0, (void *) IDL_TYP_FLOAT },	
  	{ "SIGMAFRM_MAX",  0, (void *) IDL_TYP_FLOAT },	
  	{ "RD_ADU_MIN",	    0, (void *) IDL_TYP_LONG },	
  	{ "RD_ADU_MAX",	    0, (void *) IDL_TYP_LONG },	
  	{ "SIGMAREAD_MIN",  0, (void *) IDL_TYP_FLOAT },	
  	{ "SIGMAREAD_MAX",  0, (void *) IDL_TYP_FLOAT },	
	{ 0 }
  };         /* structure and tag names MUST be all uppercase! */

  sdef = IDL_MakeStruct( "UFFRAMECONFIG_v5", s_tags );
  FrameConf = IDL_ImportArray( 1, &one, IDL_TYP_STRUCT, (UCHAR *)&uffc, 0, sdef );
  
  if( argc > 0 ) {
  	iv_agnt = argv[0];
  	IDL_ENSURE_STRING( iv_agnt );
  	IDL_ENSURE_SCALAR( iv_agnt );
	agntp = (IDL_STRING *)&(iv_agnt->value.str); 
	if( agntp->slen > 0 ) agent = agntp->s;  else agent = "frame";
  } else
  	agent = "frame";
  
  nb = ufFrameConfRecv( agent, &uffc );
  
  if( nb != uffc.hdr.length ) {
    sprintf(_UFerrmsg,"Recvd %d bytes of %d in FrameConfig",nb,uffc.hdr.length);
    IDL_Message(IDL_M_NAMED_GENERIC, IDL_MSG_INFO, _UFerrmsg);
  }
  return FrameConf;
}/*--------------------------------------------------------------------------*/

static IDL_VPTR UFsend_FrameConf(int argc, IDL_VPTR argv[]) {
  /*
   * argv = IDL structure: FrameConfig,
   *        IDL string:    "agent name" (optional input, default="frame").
   * retval = # of bytes sent.
   */
  IDL_VPTR iv_agnt, FrameConf = argv[0];
  IDL_STRING *agntp; 
  char *agent;
  int nb;
  ufFrameConfig *uffc;

  IDL_ENSURE_STRUCTURE( FrameConf );
  uffc = (ufFrameConfig *)FrameConf->value.s.arr->data;
  
  if( argc > 1 ) {
  	iv_agnt = argv[1];
  	IDL_ENSURE_STRING( iv_agnt );
  	IDL_ENSURE_SCALAR( iv_agnt );
	agntp = (IDL_STRING *)&(iv_agnt->value.str); 
	if( agntp->slen > 0 ) agent = agntp->s;  else agent = "frame";
  } else
  	agent = "frame";
  	
  sprintf( _UFerrmsg,"Sending FrameConfig to agent: %s %s", agent, uffc->hdr.name );
  IDL_Message(IDL_M_NAMED_GENERIC, IDL_MSG_INFO, _UFerrmsg);

  nb = ufFrameConfSend( agent, uffc );
  
  if( nb != uffc->hdr.length ) {
    sprintf(_UFerrmsg,"Sent %d bytes of %d in FrameConfig",nb,uffc->hdr.length);
    IDL_Message(IDL_M_NAMED_GENERIC, IDL_MSG_INFO, _UFerrmsg);
  }
  return IDL_GettmpLong( nb );
}/*--------------------------------------------------------------------------*/

static IDL_VPTR UFsend_ObsConf(int argc, IDL_VPTR argv[]) {
  /*
   * argv = IDL structure:  ObsConfig (input),
   *        IDL uint array: unsigned 16-bit ObsFlags (input),
   *        IDL string:     "agent name" (optional input, input, default="frame"),
   * retval = # of elements (shorts) of ObsFlags sent.
   */
  IDL_VPTR iv_agnt, ObsConf = argv[0], iv_ObsFlags = argv[1];
  IDL_STRING *agntp; 
  char *agent;
  IDL_ARRAY *obsarr;
  unsigned short *obsFlags;
  int ns;
  ufObsConfig *ufoc;

  IDL_ENSURE_STRUCTURE( ObsConf );
  ufoc = (ufObsConfig *)ObsConf->value.s.arr->data;
  
  if( iv_ObsFlags->type != IDL_TYP_UINT ) {
    IDL_Message(IDL_M_NAMED_GENERIC, IDL_MSG_INFO,
               "ObsFlags array (2nd arg) must be IDL_TYP_UINT (16-bit unsigned)");
    return IDL_GettmpLong(-1);
  }
  IDL_ENSURE_ARRAY( iv_ObsFlags );
  obsarr = iv_ObsFlags->value.arr;
  ufoc->hdr.elem = obsarr->n_elts;
  obsFlags = (unsigned short *)obsarr->data;
  
  if( argc > 2 ) {
  	iv_agnt = argv[2];
  	IDL_ENSURE_STRING( iv_agnt );
  	IDL_ENSURE_SCALAR( iv_agnt );
	agntp = (IDL_STRING *)&(iv_agnt->value.str); 
	if( agntp->slen > 0 ) agent = agntp->s;  else agent = "frame";
  } else
  	agent = "frame";
  	
  sprintf( _UFerrmsg,"Sending ObsConfig to agent: %s", agent );
  IDL_Message(IDL_M_NAMED_GENERIC, IDL_MSG_INFO, _UFerrmsg);

  ns = ufObsConfSend( agent, ufoc, obsFlags );
  
  sprintf(_UFerrmsg,"Sent %d of %d elements in ObsFlags", ns, ufoc->hdr.elem);
  IDL_Message(IDL_M_NAMED_GENERIC, IDL_MSG_INFO, _UFerrmsg);

  return IDL_GettmpLong( ns );
}/*--------------------------------------------------------------------------*/

static IDL_VPTR UFrecv_ObsConf(int argc, IDL_VPTR argv[]) {
  /*
   * argv = IDL int array: unsigned 16-bit ObsFlags (output),
   *        string:       "agent name" (optional input, default="frame"),
   * retval = IDL structure: Observation Configuration.
   */
  IDL_VPTR iv_agnt, ObsConf, iv_ObsFlags;
  IDL_STRING *agntp; 
  char *agent;
  static IDL_LONG one = 1;
  void *sdef;
  int nr;
  short *obsFlags = 0, *obsFcopy = 0;
  static ufObsConfig ufoc;

  static IDL_LONG obsf_dim[1];
  static IDL_LONG timestamp_dims[] = { 1, 25 };
  static IDL_LONG name_dims[] = { 1, 83 };
  static IDL_LONG mode_dims[] = { 1, 12 };
  static IDL_LONG dLab_dims[] = { 1, 40 };
  
  static IDL_STRUCT_TAG_DEF s_tags[] = {
  	{ "LENGTH",	0, (void *) IDL_TYP_LONG },	
	{ "TYPE",	0, (void *) IDL_TYP_LONG  },	
	{ "ELEM",	0, (void *) IDL_TYP_LONG },	
	{ "SEQCNT",	0, (void *) IDL_TYP_UINT },	
	{ "SEQTOT",	0, (void *) IDL_TYP_UINT },	
	{ "DURATION",	0, (void *) IDL_TYP_FLOAT },	
	{ "TIMESTAMP",	timestamp_dims, (void *) IDL_TYP_BYTE },	
	{ "NAME",	name_dims, (void *) IDL_TYP_BYTE },	
  	{ "NODBEAMS",	0, (void *) IDL_TYP_LONG },	
  	{ "CHOPBEAMS",	0, (void *) IDL_TYP_LONG },	
  	{ "SAVESETS",	0, (void *) IDL_TYP_LONG },	
  	{ "NODSETS",	0, (void *) IDL_TYP_LONG },	
  	{ "COADDSPERFRM", 0, (void *) IDL_TYP_LONG },	
	{ "READOUTMODE", mode_dims, (void *) IDL_TYP_BYTE },	
	{ "DATALABEL",   dLab_dims, (void *) IDL_TYP_BYTE },	
	{ 0 }
  };         /* structure and tag names MUST be all uppercase! */

  sdef = IDL_MakeStruct( "UFOBSCONFIG_v4", s_tags );
  ObsConf = IDL_ImportArray( 1, &one, IDL_TYP_STRUCT, (UCHAR *)&ufoc, 0, sdef );
  
  if( argc < 1 ) return ObsConf;

  if( argc > 1 ) {
  	iv_agnt = argv[1];
  	IDL_ENSURE_STRING( iv_agnt );
  	IDL_ENSURE_SCALAR( iv_agnt );
	agntp = (IDL_STRING *)&(iv_agnt->value.str); 
	if( agntp->slen > 0 ) agent = agntp->s;  else agent = "frame";
  } else
  	agent = "frame";
  	
  nr = ufObsConfRecv( agent, &ufoc, &obsFlags );

  if( nr != ufoc.hdr.elem ) {
    sprintf(_UFerrmsg,"Recvd %d of %d elements in ObsFlags", nr, ufoc.hdr.elem);
    IDL_Message(IDL_M_NAMED_GENERIC, IDL_MSG_INFO, _UFerrmsg);
  }

  if( ufoc.hdr.elem > 0  &&  nr > 0 ) {
    /* make copy of the array, so original can be de-alloc later */
    obsFcopy = (short *)calloc( ufoc.hdr.elem, sizeof(short) );
    memcpy( obsFcopy, obsFlags, nr*sizeof(short) );
    obsf_dim[0] = ufoc.hdr.elem;
    iv_ObsFlags = IDL_ImportArray( 1, obsf_dim, IDL_TYP_UINT, (UCHAR *)obsFcopy, 0,0);
    /* this just copies the array pointer ? */
    IDL_VarCopy( iv_ObsFlags, argv[0] );
  }

  free( obsFlags ); /* to avoid memory leakage ? what about obsFcopy ? */
  return ObsConf;
}/*--------------------------------------------------------------------------*/

/* internal utility function: */

static IDL_VPTR UFconv_Phdr_IDL( ufProtocolHeader* ufphdr ) {
  /*
   * Convert ufProtocolHeader C structure to IDL variable structure,
   *         called only by code in this file (ufIDL.c).
   * retval = the IDL pointer to structure ufProtocolHeader.
   */
  IDL_VPTR Phdr;
  void *sdef;
  static IDL_LONG one=1;
  
  static IDL_LONG timestamp_dims[] = { 1, 25 };
  static IDL_LONG name_dims[] = { 1, 83 };
  
  static IDL_STRUCT_TAG_DEF s_tags[] = {
  	{ "LENGTH",	0, (void *) IDL_TYP_LONG },	
	{ "TYPE",	0, (void *) IDL_TYP_LONG  },	
	{ "ELEM",	0, (void *) IDL_TYP_LONG },	
	{ "SEQCNT",	0, (void *) IDL_TYP_UINT },	
	{ "SEQTOT",	0, (void *) IDL_TYP_UINT },	
	{ "DURATION",	0, (void *) IDL_TYP_FLOAT },	
	{ "TIMESTAMP",	timestamp_dims, (void *) IDL_TYP_BYTE },	
	{ "NAME",	name_dims, (void *) IDL_TYP_BYTE },	
	{ 0 }
  };         /* structure and tag names MUST be all uppercase! */

  sdef = IDL_MakeStruct( "UFPROTOCOLHEADER", s_tags );
  Phdr = IDL_ImportArray( 1, &one, IDL_TYP_STRUCT, (UCHAR *) ufphdr, 0, sdef );
  
  return Phdr;
}
/*----------------recv or send frames (images) from/to buffers---------*/
/*---call UF_request("buffer name") and call UFrecv_Ints---------------*/

static IDL_VPTR UFrecv_Ints(int argc, IDL_VPTR argv[]) {
  /*
   * argv = IDL array of Longwords (input, contents overwritten by recv from agent)
   *        structure: ufProtocolHeader (output),
   *        IDL string: agent name (optional input, default="frame").
   * retval = Long int: # pixels fetched.
   */
  IDL_VPTR iv_buf=argv[0], iv_hdr=argv[1], iv_agnt, Phdr;
  IDL_ARRAY *bufarr;
  IDL_STRING *agntp; 
  int *bufptr;
  int Nelrecv=0, Nelbuf=0;
  char *agent;
  static ufProtocolHeader ufphdr;

  Phdr = UFconv_Phdr_IDL( &ufphdr );   /* temp IDL var now points to ufphdr */
  memset( &ufphdr, 0, sizeof(ufProtocolHeader) );
  IDL_VarCopy( Phdr, iv_hdr );	/* this IDL var pointer is ready to return */
  
  if( iv_buf->type != IDL_TYP_LONG ) {
    IDL_Message(IDL_M_NAMED_GENERIC, IDL_MSG_INFO,
               "buffer array (1st arg) must be type LONG");
    return IDL_GettmpLong(-1);
  }
  IDL_ENSURE_ARRAY( iv_buf );
  bufarr = iv_buf->value.arr;
  Nelbuf = bufarr->n_elts;
  bufptr = (int* )bufarr->data;
  
  if( bufptr  == 0 ) {
    IDL_Message(IDL_M_NAMED_GENERIC, IDL_MSG_INFO, "Null buffer ptr?");
    return IDL_GettmpLong(-1);
  }

  if( argc > 2 ) {
  	iv_agnt = argv[2];
  	IDL_ENSURE_STRING( iv_agnt );
  	IDL_ENSURE_SCALAR( iv_agnt );
	agntp = (IDL_STRING *)&(iv_agnt->value.str); 
	if( agntp->slen > 0 ) agent = agntp->s;  else agent = "frame";
  } else
  	agent = "frame";
  
  Nelrecv = ufIntsRecv( agent, bufptr, Nelbuf, &ufphdr );

  return IDL_GettmpLong(Nelrecv);
}/*-------------------------------------------------------------------------*/

static IDL_VPTR UFsend_Ints(int argc, IDL_VPTR argv[]) {
  /*
   * argv = IDL array of Longwords (input: contents are sent to server/agent)
   *        IDL string: array name (optional input, default="UFInts"),
   *        IDL string: agent name (optional input, default="frame").
   * retval = IDL int: number of pixels sent.
   */
  IDL_VPTR iv_bn, iv_buf=argv[0], iv_agnt;
  IDL_ARRAY *bufarr;
  IDL_STRING *agntp; 
  int *bufptr;
  int Nelsent=0, Nelbuf=0;
  char *bufname, *agent;

  if( iv_buf->type != IDL_TYP_LONG ) {
    IDL_Message(IDL_M_NAMED_GENERIC, IDL_MSG_INFO,
               "buffer array (1st arg) must be type LONG");
    return IDL_GettmpLong(-1);
  }
  IDL_ENSURE_ARRAY( iv_buf );
  bufarr = iv_buf->value.arr;
  Nelbuf = bufarr->n_elts;
  bufptr = (int* )bufarr->data;
  
  if( bufptr == 0 ) {
     IDL_Message(IDL_M_NAMED_GENERIC, IDL_MSG_INFO, "Null buffer ptr?");
    return IDL_GettmpLong(-1);
  }
  
  if( argc > 1 ) {
  	iv_bn = argv[1];
  	IDL_ENSURE_STRING( iv_bn );
  	IDL_ENSURE_SCALAR( iv_bn );
	bufname = IDL_STRING_STR( &(iv_bn->value.str) ); 
  } else
  	bufname = "UFInts";

  if( argc > 2 ) {
  	iv_agnt = argv[2];
  	IDL_ENSURE_STRING( iv_agnt );
  	IDL_ENSURE_SCALAR( iv_agnt );
	agntp = (IDL_STRING *)&(iv_agnt->value.str); 
	if( agntp->slen > 0 ) agent = agntp->s;  else agent = "frame";
  } else
  	agent = "frame";

  Nelsent = ufIntsSend( agent, bufptr, Nelbuf, bufname );

  return IDL_GettmpLong(Nelsent);
}/*-------------------------------------------------------------------------*/

/* general "agent" support interface:
 * The UFagentSend/Recv functions send/recv arrays of strings as UFStrings objects.
 */

static IDL_VPTR UFagentConnect(int argc, IDL_VPTR argv[]) {
  /*
   * argv = "agent name" (input, NULL string defaults to "frame")
   *        "agent host name" (input, NULL string defaults to local host),
   *        int port number (input).
   * retval = int sock fd of connection.
   */   
  IDL_VPTR a=argv[0], h=argv[1];
  IDL_STRING *idls;
  int sockFd, portNo = IDL_LongScalar( argv[2] );
  char *agent, *host;
  
  IDL_ENSURE_STRING( a );
  IDL_ENSURE_SCALAR( a );
  idls = (IDL_STRING*) &(a->value.str);
  
  if( idls->slen > 0 )
    agent = idls->s;
  else
    agent = "frame";

  IDL_ENSURE_STRING( h );
  IDL_ENSURE_SCALAR( h );
  idls = (IDL_STRING*) &(h->value.str);
  
  if( idls->slen > 0 )
    host = idls->s;
  else
    host = (char*)ufHostName();

  sprintf(_UFerrmsg,"Connecting to agent=%s, on host=%s, using port=%d",
                                                  agent, host, portNo);
  IDL_Message(IDL_M_NAMED_GENERIC, IDL_MSG_INFO, _UFerrmsg);

  sockFd = ufConnectAgent( agent, host, portNo );

  return IDL_GettmpLong(sockFd);
}/*------------------------------------------------------------------------*/

static IDL_VPTR UFbytesAvailable(int argc, IDL_VPTR argv[]) {
  /*
   * argv = "agent name" (input),
   * retval = number of bytes available to be read from agent.
   */
  IDL_VPTR iv_agnt= argv[0];
  char *agent;
  int sockFd;

  IDL_ENSURE_STRING( iv_agnt );
  IDL_ENSURE_SCALAR( iv_agnt );
  agent = IDL_STRING_STR( &(iv_agnt->value.str) );
  sockFd = ufAgentSocket( agent );

  if( sockFd >= 0 )
    return IDL_GettmpLong( ufAvailable( sockFd ) );
  else
    return IDL_GettmpLong( sockFd );
}/*-------------------------------------------------------------------------*/

static IDL_VPTR UFagentClose(int argc, IDL_VPTR argv[]) {
  /*
   * argv = "agent name" (input),
   * retval = status of close operation.
   */
  IDL_VPTR iv_agnt= argv[0];
  char *agent;

  IDL_ENSURE_STRING( iv_agnt );
  IDL_ENSURE_SCALAR( iv_agnt );
  agent = IDL_STRING_STR( &(iv_agnt->value.str) );
  
  return IDL_GettmpLong( ufCloseAgent(agent) );
}/*-------------------------------------------------------------------------*/

static IDL_VPTR UFagentSend(int argc, IDL_VPTR argv[]) {
  /*
   * Send a string or an array of strings to an agent:
   * argv = IDL string or an IDL array of strings (input),
   *        IDL string: "message name", sent in name field of protocol header
   *                    (optional input, default="IDL").
   *        IDL string: "agent name" (optional input, default="frame").
   * retval = number of strings sent.
   */
  IDL_VPTR vts, iv_agnt, iv_name, iv_strs=argv[0];
  IDL_STRING *agntp, *namp, *sdata; 
  char *agent, *name, **strings, **s;
  int nsent;
  IDL_LONG n, Nstrings;

  if( argc > 2 ) {
  	iv_agnt = argv[2];
  	IDL_ENSURE_STRING( iv_agnt );
  	IDL_ENSURE_SCALAR( iv_agnt );
	agntp = (IDL_STRING *)&(iv_agnt->value.str); 
	if( agntp->slen > 0 ) agent = agntp->s;  else agent = "frame";
  } else
  	agent = "frame";

  if( argc > 1 ) {
  	iv_name = argv[1];
  	IDL_ENSURE_STRING( iv_name );
  	IDL_ENSURE_SCALAR( iv_name );
	namp = (IDL_STRING *)&(iv_name->value.str); 
	if( namp->slen > 0 ) name = namp->s;  else name = "IDL";
  } else
        name = "IDL";

  IDL_ENSURE_STRING( iv_strs );

  if( iv_strs->flags & IDL_V_ARR ) {
  	Nstrings = iv_strs->value.arr->n_elts;
  	sdata = (IDL_STRING *)iv_strs->value.arr->data;	/* string descriptors */
  }
  else {
  	Nstrings = 1;
  	sdata = (IDL_STRING *)&(iv_strs->value.str);
  }
  strings = (char** )IDL_GetScratch( &vts, Nstrings, (IDL_LONG )sizeof(char*) );
  s = strings;
  					
  for( n=0; n<Nstrings; n++ ) {		/* copy pointers from IDL descriptors */
	*s = IDL_STRING_STR( sdata );   /* and put them in C string array */
  	sdata++;
	s++;
  }
  nsent = ufStringsSendAgent( agent, strings, (int )Nstrings, name );

  IDL_Deltmp(vts);  /* this frees the scratch memory allocated for strings */
  
  return IDL_GettmpLong(nsent);
}/*--------------------------------------------------------------------------*/

static IDL_VPTR UFagentRecv(int argc, IDL_VPTR argv[]) {
  /*
   * Recieve a string or an array of strings from an agent:
   * argv = IDL structure (output): UFP hdr,
   *        IDL string: "agent name" (optional input, default="frame").
   * retval = IDL string or IDL array of strings recieved.
   */
  IDL_VPTR vts, Phdr, iv_agnt, iv_hdr=argv[0];
  IDL_STRING *agntp, *sdata;
  char *agent, **strings, **s;
  int n, nbt, Nstrings;
  static IDL_LONG one=1, sdims[1];
  static ufProtocolHeader ufphdr;
  
  if( argc > 1 ) {
  	iv_agnt = argv[1];
  	IDL_ENSURE_STRING( iv_agnt );
  	IDL_ENSURE_SCALAR( iv_agnt );
	agntp = (IDL_STRING *)&(iv_agnt->value.str); 
	if( agntp->slen > 0 ) agent = agntp->s;  else agent = "frame";
  } else
  	agent = "frame";
  	
  Phdr = UFconv_Phdr_IDL( &ufphdr );   /* temp IDL var now points to ufphdr */
  memset( &ufphdr, 0, sizeof(ufProtocolHeader) );
  IDL_VarCopy( Phdr, iv_hdr );	/* this IDL var pointer is ready to return */
  
  strings = ufStringsRecvAgent( agent, &Nstrings, &ufphdr, &nbt );
  
  if( Nstrings <= 0 ) return IDL_GettmpLong( Nstrings );

  sdims[0] = (IDL_LONG )Nstrings;
  sdata = (IDL_STRING *)IDL_MakeTempArray( IDL_TYP_STRING, one, sdims,
                                           IDL_BARR_INI_INDEX, &vts );
  s = strings;
  IDL_StrDelete( sdata, Nstrings ); /* note:  sdata is contained in vts */
  
  for( n=0; n<Nstrings; n++ ) {
	IDL_StrStore( sdata, *s );  /* copy strings to IDL descriptors */
	free(*s);
  	sdata++;
	s++;
  }
  free(strings);
  return vts;	/* return the IDL var pointer to string array (sdata) */
}/*--------------------------------------------------------------------------*/

/**************************** serial port io ************************/

static IDL_VPTR UFttysend(int argc, IDL_VPTR argv[]) {
  /*
   * argv = 0 or 1, "send string"
   * retval = "send string"
   */ 
  IDL_VPTR s = argv[1];
  IDL_STRING *sp;
  int ttyNo = IDL_LongScalar( argv[0] );

  IDL_ENSURE_STRING( s );
  IDL_ENSURE_SCALAR( s );
  sp = (IDL_STRING*) &(s->value.str); 

  if( sp->slen <= 0 ) return IDL_StrToSTRING("empty string?");

  return IDL_StrToSTRING( ufttySend( ttyNo, sp->s ) );
}/*-------------------------------------------------------------------------*/

static IDL_VPTR UFttyrecv(int argc, IDL_VPTR argv[]) {
  /*
   * argv = 0 or 1
   * retval = "recv string"
   */
  int ttyNo = IDL_LongScalar( argv[0] );

  return IDL_StrToSTRING(ufttyRecv(ttyNo));
}/*-------------------------------------------------------------------------*/

/**************************** annex io ************************/
/*
   this forks a child process that creates the name-pipe/fifo, opens it for reading
   and redirects its stdout to the return pipe fd; IDL should
   write to the annex named pipe and read from this returned fd.
*/
static IDL_VPTR UFannexopen(int argc, IDL_VPTR argv[]) {
  /*
   * argv = string: "fifoname",
   *        integer: annexport.
   * retval = pid_t child pid
   */   
  IDL_VPTR f= argv[0], p= argv[1];
  IDL_STRING *fp;
  char* fifoname;
  short portNo = 7008; /* 7002 - 7008, MCE4 is nominally 7008 */
  pid_t child;

  IDL_ENSURE_STRING( f );
  IDL_ENSURE_SCALAR( f );

  fp = (IDL_STRING*) &(f->value.str);
  if( fp->slen > 0 )
    fifoname = fp->s;
  else
    fifoname = "annex8fifo";

  IDL_ENSURE_SCALAR( p );
  if( p->type != IDL_TYP_INT ) {
   IDL_Message(IDL_M_NAMED_GENERIC,IDL_MSG_INFO,"port (2nd arg) must be 16-bit");
    return IDL_GettmpLong(-1);
  }
  portNo = p->value.i;
  
  sprintf(_UFerrmsg,"Attempt to connect to Annex via Fifo= %s, using portNo= %d",
	  fifoname, portNo);
  IDL_Message(IDL_M_NAMED_GENERIC, IDL_MSG_INFO, _UFerrmsg);

  child = ufOpenAnnexFifo(fifoname, portNo);
  return IDL_GettmpLong(child);
}/*---------------------------------------------------------------------------*/

static IDL_VPTR UFannexclose(int argc, IDL_VPTR argv[]) {
  /*
   * argv = string: "fifoname",
   *        pid_t:  child.
   * retval = int 0 or -1
   */   
  IDL_VPTR f= argv[0];
  IDL_STRING *fp;
  char* fifoname;
  pid_t child;

  IDL_ENSURE_STRING( f );
  IDL_ENSURE_SCALAR( f );

  fp = (IDL_STRING*) &(f->value.str);
  if( fp->slen > 0 )
    fifoname = fp->s;
  else
    fifoname = "annex8fifo";

  child = (pid_t)(IDL_LongScalar( argv[1] ));
  
  sprintf(_UFerrmsg,"Attempt to close Annex connection & Fifo= %s, child pid= %d", fifoname, (int)child);
  IDL_Message(IDL_M_NAMED_GENERIC, IDL_MSG_INFO, _UFerrmsg);

  child = ufCloseAnnexFifo(fifoname, child);
  return IDL_GettmpLong(child);
}/*--------------------------------------------------------------------------*/

/**************************** IDL dynamic loader ************************/

/* this must be consistent with libUFIDL.dlm */

#define ARRLEN(a) (sizeof(a)/sizeof(a[0]))

int IDL_Load(void)
{
  static IDL_SYSFUN_DEF2 uffuncs[] = /* function names MUST be uppercase and IN brackets! */
  {
    { {UFhostname},    "UFHOSTNAME",      0, 0, 0, 0 },
    { {UFhostTime},    "UFHOSTTIME",      0, 1, 0, 0 },
    { {UFservices},    "UFSERVICES",      1, 1, 0, 0 },
    { {UFannexopen},   "UFANNEXOPEN",     2, 2, 0, 0 },
    { {UFannexclose},  "UFANNEXCLOSE",	  2, 2, 0, 0 },
    { {UFcheckfifo},   "UFCHECKFIFO",	  1, 1, 0, 0 },
    { {UFchecksocket}, "UFCHECKSOCKET",   1, 1, 0, 0 },
    { {UFset_timeout}, "UFSET_TIMEOUT",   1, 1, 0, 0 },
    { {UFframeConnect},	"UFFRAMECONNECT",  2, 2, 0, 0 },
    { {UFframeClose},	"UFFRAMECLOSE",	   0, 0, 0, 0 },
    { {UFrecv_Ints},	"UFRECV_INTS",	   2, 3, 0, 0 },
    { {UFsend_Ints},	"UFSEND_INTS",	   1, 3, 0, 0 },
    { {UFrecv_FrameConf}, "UFRECV_FRAMECONF", 0, 1, 0, 0 },
    { {UFsend_FrameConf}, "UFSEND_FRAMECONF", 1, 2, 0, 0 },
    { {UFrecv_ObsConf}, "UFRECV_OBSCONF",  0, 2, 0, 0 },
    { {UFsend_ObsConf},	"UFSEND_OBSCONF",  2, 3, 0, 0 },
    { {UF_request},     "UF_REQUEST",      1, 2, 0, 0 },
    { {UFbytesAvailable}, "UFBYTESAVAILABLE", 1, 1, 0, 0 },
    { {UFagentConnect}, "UFAGENTCONNECT",  3, 3, 0, 0 },
    { {UFagentSend},    "UFAGENTSEND",	   1, 3, 0, 0 },
    { {UFagentRecv},    "UFAGENTRECV",	   1, 2, 0, 0 },
    { {UFagentClose},   "UFAGENTCLOSE",	   1, 1, 0, 0 },
    { {UFagentNames},   "UFAGENTNAMES",	   0, 0, 0, 0 },
    { {UFttysend},      "UFTTYSEND",	   2, 2, 0, 0 },
    { {UFttyrecv},      "UFTTYRECV",	   1, 1, 0, 0 }
  };

  int stat = IDL_SysRtnAdd( uffuncs, TRUE, ARRLEN(uffuncs) );
  
  sprintf(_UFerrmsg,"IDL_Load> IDL_SysRtnAdd: stat= %d", stat );
  _uflog(_UFerrmsg);
  return stat;
}

#endif /* __UFIDL_c__ */

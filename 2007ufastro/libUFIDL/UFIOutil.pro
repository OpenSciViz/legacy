;+
; NAME:
;      UFIOutil.pro:
;
; PURPOSE:
;	Functions to req/recv UFLib objects from a server/agent
;	with handling of socket timeouts/errors:
;	UFIO_Fetch_FrameConf sends request to "frame" server,
;	the others assume request was sent to an agent using DLM function UF_request().
;	A ufClient type of socket must be allready open to server/agent (see testfserv.pro).
;
; CATEGORY:
;	UFLib interface.
;
; CALLING SEQUENCE:
;
;	FrameConfig =  UFIO_Fetch_FrameConf( REQUEST=, NEXTRA_TRYS= )
;
;	strings =  UFIO_Recv_Strings( phdr, NBSENT=, NEXTRA_TRYS=, AGENT= )
;
;	Nrecvd =  UFIO_Recv_LongArray( Long_array, phdr, NBSENT=, NEXTRA_TRYS=, AGENT= )
;
; INPUTS:
;	Long_array = array of Longwords large enough to hold the requested UFInts object values.
;			If not large enough, extra values will be discarded.
;
; KEYWORD PARAMETERS:
;
;	REQUEST = string, default = "FC" to request the FrameConfig.
;		If REQUEST = "buffername", then FrameConfig of buffer is recvd,
;		and user must then call UFIO_Recv_LongArray to recv contents of buffer.
;
;	NEXTRA_TRYS = # of times to retry recv of UFLib object after each timeout,
;			until asking user to quit/continue (default=7).
;
;	AGENT = string, name of UFLib agent/server (default = "frame").
;
;	NBSENT = # of bytes that were sent by previous request for UFLib object.
;		Default = 128, assuming a successful send of request had occurred,
;		anything less than 44 bytes indicates an incomplete send of request.
;		Note: UFIO_Fetch_FrameConf sends the request, other functions do not.
; OUTPUTS:
;	phdr = structure, UFLib protocol header.
;
;	Long_array = filled with values of UFLib UFInts object if recv is successful.
;
; EXTERNAL CALLS:
;	DLMs:
;		UFframeConnect, UFframeClose, UFrecv_FrameConf, UFrecv_Ints
;		UFagentSend, UFagentRecv, UF_request, UFset_timeout
;		UFrecv_ObsConf, UFsend_ObsConf
;
;	DLMs are located in /usr/local/uf$USER/lib/libUFIDL.so,
;	and that directory should also contain libUFIDL.dlm,
;	and so to use it must setenv before starting IDL:
;	setenv  IDL_DLM_PATH  /usr/local/uf$USER/lib:/usr/local/rsi/idl/bin/bin.solaris2.sparc
;	Note that libUFIDL.so requires /usr/local/uf$USER/lib/libUFClient.so to be present.
;
; MODIFICATION HISTORY:
;
;  Written May 2001, by:     Frank Varosi
;                            Department of Astronomy
;                            211 Bryant Space Science Center
;                            University of Florida
;                            Gainesville, FL 32611-2055
;                            Phone: (352)392-2052 x225
;                            E-mail: varosi@astro.ufl.edu
;-

;-----------------------------Utility pros:-------------------------------------------------

pro UFIO_print_ProtoHdr, phdr

	print, " Length = " + strtrim( phdr.Length, 2 ) $
		+ ",  type = " + strtrim( phdr.type, 2 ) $
		+ ",  name: " + string( phdr.name )
end

;----------------------------------------------------------------------------------

pro UFIO_print_FrameConf, FrameConfig, VERBOSE=verbose, STATUS=status

	if keyword_set( verbose ) then begin
		print,string( FrameConfig.name ),"> ",string( FrameConfig.timestamp ), $
			": W=",strtrim( FrameConfig.w, 2 ), $
			", H=",strtrim( FrameConfig.h, 2 ), $
			", D=",strtrim( FrameConfig.d, 2 )
	   endif

	print,"Grabbed=",strtrim( FrameConfig.FrameGrabCnt, 2 ), $
		"  Procesd=",strtrim( FrameConfig.FrameObsSeqNo, 2 ), $
		"  CoAdds=",strtrim( FrameConfig.CoAdds, 2 ), $
		"  ChopBeam=",strtrim( FrameConfig.ChopBeam, 2 ), $
		"  NodBeam=",strtrim( FrameConfig.NodBeam, 2 ), $
		"  SaveSet=",strtrim( FrameConfig.SaveSet, 2 ), $
		"  NodSet=",strtrim( FrameConfig.NodSet, 2 ), $
		"  WriteCnt=",strtrim( FrameConfig.FrameWriteCnt, 2 ), $
		"  SendCnt=",strtrim( FrameConfig.FrameSendCnt, 2 )

	if keyword_set( status ) then begin
		print," # DMAs missed = ", $
			strtrim( FrameConfig.DMAcnt - FrameConfig.frameGrabCnt, 2 ), $
			",  # frames to process = ", $
			strtrim( FrameConfig.frameGrabCnt - FrameConfig.FrameObsSeqNo, 2 ), $
			",  # frames to write = ", $
			strtrim( FrameConfig.frameGrabCnt - FrameConfig.FrameWriteCnt, 2 ), $
			",  # frames to send = ", $
			strtrim( FrameConfig.frameGrabCnt - FrameConfig.FrameSendCnt, 2 )
	   endif
end

;---------------------------------------------------------------------------------------------
; Request and recv global FrameConfig from FrameAcqServer,
;  or request a frame a frame buffer,
;  upon which the server first replies with corresponding FrameConfig:
;---------------------------------------------------------------------------------------------

function UFIO_Fetch_FrameConf, NEXTRA_TRYS=NextraTrys, REQUEST=request

  FORWARD_FUNCTION  UF_request, UFrecv_FrameConf

	if N_elements( NextraTrys ) ne 1 then NextraTrys=7
	if N_elements( request ) ne 1 then request = "FC"  ;default is req. processor FrameConfig.

	nbsent = UF_request( request )
	fcon = UFrecv_FrameConf()

	if( fcon.type ne 9 ) then begin

		message,"did not recv FrameConfig object: ",/INFO
		UFIO_print_ProtoHdr, fcon

		if (nbsent gt 44) and (fcon.length LE 0) then begin  ;min UFTimeStamp req Length=44.
			ntry = 0
			maxTrys = NextraTrys
		TRY:	while (ntry LT maxTrys) and (fcon.type ne 9) do begin
				wait,1
				message,"trying again to recv FrameConfig...",/INFO
				fcon = UFrecv_FrameConf()
				ntry = ntry+1
			  endwhile
			if( fcon.type ne 9 ) then begin
				message,"still did not recv FrameConfig object after " $
					+ strtrim( ntry, 2 ) + " trys!" + string(7b),/INFO
				UFIO_print_ProtoHdr, fcon
				command = ""
				read," enter q to quit trying: ",command
				if command ne "q" then begin
					maxTrys = maxTrys + NextraTrys
					goto,TRY
				   endif
			  endif else message," got it after " + strtrim( ntry, 2 ) + " more trys!",/INFO
		   endif
	   endif

return, fcon
end

;-------Recv a UFStrings or UFTimeStamp from an agent:----------------------------------------------

function UFIO_Recv_Strings, phdr, NBSENT=nbsent, NEXTRA_TRYS=NextraTrys, AGENT=agent

  FORWARD_FUNCTION  UFagentRecv

	if N_elements( nbsent ) ne 1 then nbsent=128       ;default is assume that request was sent.
	if N_elements( NextraTrys ) ne 1 then NextraTrys=7
	if N_elements( agent ) ne 1 then agent = ""        ;default agent = "frame" in UFagentRecv().

	strings = UFagentRecv( phdr, agent )

	if (phdr.Length LE 0) or (phdr.type gt 1) or (phdr.type lt 0) then begin

		message,"did not recv UFStrings or UFTimeStamp object: ",/INFO
		UFIO_print_ProtoHdr, phdr

		if (nbsent gt 44) and (phdr.Length LE 0) then begin    ;min UFTimeStamp req Length=44.
			ntry = 0
			maxTrys = NextraTrys
		TRY:	while (ntry LT maxTrys) and (phdr.Length LE 0) do begin
				wait,1
				message,"trying again to recv object...",/INFO
				strings = UFagentRecv( phdr, agent )
				ntry = ntry+1
			  endwhile
			if( phdr.Length LE 0 ) then begin
				message,"still did not recv UFStrings or UFTimeStamp object after " $
					+ strtrim( ntry, 2 ) + " trys!" + string(7b),/INFO
				UFIO_print_ProtoHdr, phdr
				command = ""
				read," enter q to quit trying: ",command
				if command ne "q" then begin
					maxTrys = maxTrys + NextraTrys
					goto,TRY
				   endif
			  endif else message," got it after " + strtrim( ntry, 2 ) + " more trys!",/INFO
		   endif
	   endif

return, strings
end

;-------Recv a UFInts object from an agent (func ret.val. = # Long ints recvd):------------------------

function UFIO_Recv_LongArray, Long_array, phdr, NBSENT=nbsent, NEXTRA_TRYS=NextraTrys, AGENT=agent

  FORWARD_FUNCTION  UFrecv_Ints

	if N_elements( nbsent ) ne 1 then nbsent=128        ;default is assume that request was sent.
	if N_elements( NextraTrys ) ne 1 then NextraTrys=7
	if N_elements( agent ) ne 1 then agent = ""         ;default agent = "frame" in UFrecv_Ints().

	Nrecv = UFrecv_Ints( Long_array, phdr, agent )     ;note that Ints are 32-bit in C and C++.

	if (Nrecv LE 0) or (phdr.type ne 4) then begin

		message,"did not recv UFInts object: ",/INFO
		UFIO_print_ProtoHdr, phdr

		if (phdr.length gt 0) and (phdr.type eq 0) then return,Nrecv ;UFTimeStamp error message.

		if (nbsent gt 44) and (phdr.type ne 4) then begin        ;min UFTimeStamp req Length=44.
			ntry = 0
			maxTrys = NextraTrys
		TRY:	while (ntry LT maxTrys) and (phdr.type ne 4) do begin
				wait,1
				message,"trying again to recv UFInts object...",/INFO
				Nrecv = UFrecv_Ints( Long_array, phdr, agent )
				ntry = ntry+1
			  endwhile
			if( phdr.type ne 4 ) then begin
				message,"still did not recv UFInts object after " $
					+ strtrim( ntry, 2 ) + " trys!" + string(7b),/INFO
				UFIO_print_ProtoHdr, phdr
				command = ""
				read," enter q to quit trying: ",command
				if command ne "q" then begin
					maxTrys = maxTrys + NextraTrys
					goto,TRY
				   endif
			  endif else message," got it after " + strtrim( ntry, 2 ) + " more trys!",/INFO
		   endif
	   endif

return, Nrecv
end

function subMedChan, image, correction, OFF_MEDIAN=offmed, NCHANNELS=nchan

	chanStack = stack_channels( image, NCHAN=nchan )
	sort_stack, chanStack

	imedian = nchan/2
	if nchan MOD 2 eq 0 then imedian = imedian-1
	sz = size( image )
	nx = sz[1]/nchan
	imcorr = image
	if keyword_set( offmed ) then imedian = imedian + offmed
	correction = chanStack[*,*,imedian]

	for ic = 0,nchan-1 do begin
		ix = ic*nx
		Lx = ix + nx - 1
		imcorr[ ix:Lx, * ] = image[ ix:Lx, * ] - correction
	  endfor

return, imcorr
end

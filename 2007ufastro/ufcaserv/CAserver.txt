		[Next] <server-18.html>[Previous] <server-16.html>[Top]
<server-1.html>[Contents] <server-3.html>[Index] <server-19.html>

Channel Access Portable Server Tutorial


  Appendix A : myServer

This is the full listing of the program presented in /Chapter 2: Getting
Started/ <server-9.html#MARKER-9-1>. The actual executable program is
called |myServer|. It's related source files are myServer.h, which
contains all the class declarations, Server.cc, which contains |main()|,
myServer.cc, which contains all the function definitions for the
myServer class, and myPV.cc, which contains all the function definitions
for the |myPV| class.

To compile |myServer|, compile object files for myPV.cc, myServer.cc,
and Server.cc. Then link them along with the libraries libcas.a,
libca.a, libCom.a, and libgdd.a. The libraries should be linked in a
that order:

    1. libcas.a

    2. libca.a

    3. libCom.a

    4. libgdd.a

Another order may cause compiler errors. The program has been compiled
to run on a Sun Sparc workstation.

This version of |myServer| doesn't implement asynchronous IO or access
rights. In Appendix B <server-18.html#MARKER-9-1> is a listing of a
different version of |myServer| that does, though most of the code is
the same.

/*** myServer.h: Contains class declarations. ***/
#include <casdef.h>
#include <osiTimer.h>
#include <gddApps.h>
#include <gddAppFuncTable.h>

class myPV; 
class pvAttr;

// The server class. Provides the pvExistTest() and createPV()
// functions as well as the function table
class myServer : public caServer{
 public:
  myServer(unsigned pvCountEstimate);
  pvExistReturn pvExistTest (const casCtx &ctx, const char *pPVName);
  static const pvAttr *findPV(const char* Name);
  pvCreateReturn createPV(const casCtx &ctx, const char *pPVName);
  static gddAppFuncTableStatus read(myPV &pv, gdd &value)
  {
	return myServer::funcTable.read(pv, value);
  }
 private:
  static const pvAttr pvList[];
  static gddAppFuncTable<myPV> funcTable;
};

// The Process Variable class, myPV. Provides the read() and write()
// functions. The read() function calls the myServer::read() function
// which calls the appropriate function or functions from its function
// table. The actual functions to becalled are members of 
// the myPV class, readStatus() to readUnits(). 
class myPV : public casPV{
 public:
  myPV (const caServer &cas, const pvAttr &attributes);
  caStatus interestRegister()
  {
	interest = aitTrue;
	return S_casApp_success;
  }
  void interestDelete() { interest = aitFalse; }
  caStatus beginTransaction();
  void endTransaction();
  caStatus read(const casCtx &ctx, gdd &prototype);
  caStatus write(const casCtx &ctx, gdd &value);
  aitEnum bestExternalType();
  gddAppFuncTableStatus readStatus(gdd &value);
  gddAppFuncTableStatus readSeverity(gdd &value);
  gddAppFuncTableStatus readPrecision(gdd &value);
  gddAppFuncTableStatus readHopr(gdd &value);
  gddAppFuncTableStatus readLopr(gdd &value);
  gddAppFuncTableStatus readHighAlarm(gdd &value);
  gddAppFuncTableStatus readHighWarn(gdd &value);
  gddAppFuncTableStatus readLowWarn(gdd &value);
  gddAppFuncTableStatus readLowAlarm(gdd &value);
  gddAppFuncTableStatus readHighCtrl(gdd &value);
  gddAppFuncTableStatus readLowCtrl(gdd &value);
  gddAppFuncTableStatus readValue(gdd &value);
  gddAppFuncTableStatus readUnits(gdd &value);
 private:
  static int currentOps;
  const pvAttr& attr;
  aitBool interest;
};

// This class is not part of the server interface, but is only used
// here in order to keep track of PV values. It contains members for
// storing and accessing all the application types needed to satisfy a
// DBR_CTRL request. DBR_STS requests can also be satisfied. Since DM
// makes DBR_CTRL requests for each controller or monitor initially,
// and then makes DBR_STS requests subsequently,this class contains
// the attributes needed to work with DM clients.
class pvAttr {
 public:
  pvAttr (const char *pName) : name(pName)
    {
      pValue = new gddScaler(gddAppType_value, aitEnumFloat64);
      hopr = 100, lopr = 0;
      units = "Jolts";
      high_alarm = 101, high_warning = 95;
      low_warning = 5, low_alarm = -1;
      high_ctrl_limit = 100, low_ctrl_limit = 0;
      precision = 4;
    }

  const aitString &getName () const { return name; }
  double getHopr () const { return this->hopr; }
  double getLopr ()  const { return this->lopr; }
  double getHighAlarm () const { return this->high_alarm; }
  double getHighWarning () const { return this->high_warning; }
  double getLowWarning () const { return this->low_warning; }
  double getLowAlarm () const { return this->low_alarm; }
  double getHighCtrl () const { return this->high_ctrl_limit; }
  double getLowCtrl ()const  { return this->low_ctrl_limit; }
  short getPrec () const { return this->precision; }
  gdd* getVal () const { return this->pValue; }
  aitString getUnits () const { return this->units; }
 private:
  const aitString name;
  double hopr;
  double lopr;
  aitString units;
  double high_alarm;
  double high_warning;
  double low_warning;
  double low_alarm;
  double high_ctrl_limit;
  double low_ctrl_limit;
  short precision;
  gdd *pValue;
};

/*** myServer.cc: contains function for myServer class. ***/
#include "myServer.h"
// Here the myServer::pvList[] static member is initialized with two
// PVs called ProcessVariable1 and ProcessVariable2. 
const pvAttr myServer::pvList[] = {
  pvAttr("ProcessVariable1"),
  pvAttr("ProcessVariable2")};

// Constructor for myServer. After passing arguments to caServer 
// constructor all of the read functions are installed in the function
// table.
 myServer::myServer(unsigned pvCountEstimate) :
 caServer(pvCountEstimate)
{
	funcTable.installReadFunc("status", myPV::readStatus);
  funcTable.installReadFunc("severity", myPV::readSeverity);
  funcTable.installReadFunc("precision", myPV::readPrecision);
  funcTable.installReadFunc("alarmHigh", myPV::readHighAlarm);
  funcTable.installReadFunc("alarmHighWarning", myPV::readHighWarn);
  funcTable.installReadFunc("alarmLowWarning", myPV::readLowWarn);
  funcTable.installReadFunc("alarmLow", myPV::readLowAlarm);
  funcTable.installReadFunc("value", myPV::readValue);
  funcTable.installReadFunc("graphicHigh", myPV::readHopr);
  funcTable.installReadFunc("graphicLow", myPV::readLopr);
  funcTable.installReadFunc("controlHigh", myPV::readHighCtrl);
  funcTable.installReadFunc("controlLow", myPV::readLowCtrl);
  funcTable.installReadFunc("units", myPV::readUnits);
}

pvExistReturn myServer::pvExistTest(const casCtx &ctx,   //CA Context
								   const char *pPVName, // PV name
{
  const pvAttr *pPVAttr;
  // If the PV exists, write its name to the canonical PV name object.
  pPVAttr = myServer::findPV(pPVName);
  if (pPVAttr)
    return pverExistsHere;
  else
    return pverDoesNotExistHere;
}

const pvAttr *myServer::findPV(const char *pName)
{
  const pvAttr *pPVAttr;
  int i;
  short nelem = NELEMENTS(myServer::pvList);
  
  for(pPVAttr = myServer::pvList, i = 0; i < nelem; i++, pPVAttr++){
    if (strcmp(pName, pPVAttr->getName().string()) == 0)
      return pPVAttr;
  }
  return NULL;
}

pvCreateReturn myServer::createPV(const casCtx &ctx, const char *pPVName)
{
  const pvAttr *pAttr;
  myPV *pPV;
  // If PV doesn't exist, return NULL. Otherwise, return a pointer
  // to a new myPV object.
  pAttr = myServer::findPV(pPVName);
  if (!pAttr)
    return S_casApp_pvNotFound;
  pPV = new myPV(*this, *pAttr);
  if(pPV)
	return S_casApp_noMemory;
  else
	return pPV
}
/*** myPV.cc: contains functions for myPV class ***/
#include "myServer.h"
#include <iostream.h>

 myPV::myPV(const caServer &cas, const pvAttr &attributes) :
  attr(attributes),
  casPV(cas),
  interest(aitFalse)
{
  double value;                    // The initial value of the PV.
  gdd *pValue =  attr.getVal();    // Get pointer to gdd object.
  if(!pValue)
    return;

  // rand() is used to generate a random number from 0 to RAND_MAX.
  // This number is made to fit in the 0 - 100 range.
  value = (double)rand();
  while (value < 100.0)
    value/=100.0;

  // Use the gdd::putConvert() function to put the value in the gdd
  // object. Then use the gdd::setStat() and gdd::setSevr() to set
  // the appropriate status and serverity for the PV.
  pValue->putConvert(value);
  if (value >= 95){
    pValue->setStat(epicsAlarmHigh);
    pValue->setSevr(epicsSevMinor);
  }
  else if (value <=5 ){
    pValue->setStat(epicsAlarmLow);
    pValue->setSevr(epicsSevMinor);
  }
  else {
    pValue->setStat(epicsAlarmNone);
    pValue->setSevr(epicsSevNone);
  }
}

caStatus myPV::beginTransaction()
{
  // Trivial implementation that informs the user of the number of
  // current IO operations in progress for the server tool. currentOps
  //  is a static member.
  currentOps++;
  cerr << "Number of current operations = " << currentOps << "\n"; 
  return S_casApp_success;
}

void myPV::endTransaction()
{
  currentOps--;
}

caStatus myPV::read(const casCtx &ctx, gdd &prototype)
{
  // Calls myServer::read() which calls the appropriate function
  // from the application table.
  return myServer::read(*this, prototype);
}

gddAppFuncTableStatus myPV::readStatus(gdd &value)
{
  gdd *pValue = attr.getVal();
  if(pValue)
    value.putConvert(pValue->getStat());
  else
    value.putConvert(epicsAlarmUDF);
  return S_casApp_success;
}

gddAppFuncTableStatus myPV::readSeverity(gdd &value)
{
  gdd *pValue = attr.getVal();
  if(pValue)
    value.putConvert(pValue->getSevr());
  else
    value.putConvert(epicsSevNone);
  return S_casApp_success;
}

gddAppFuncTableStatus myPV::readPrecision(gdd &value)
{
  value.putConvert(attr.getPrec());
  return S_casApp_success;
}

gddAppFuncTableStatus myPV::readHopr(gdd &value)
{
  value.putConvert(attr.getHopr());
  return S_casApp_success;
}

gddAppFuncTableStatus myPV::readLopr(gdd &value)
{
  value.putConvert(attr.getLopr());
  return S_casApp_success;
}

gddAppFuncTableStatus myPV::readHighAlarm(gdd &value)
{
  value.putConvert(attr.getHighAlarm());
  return S_casApp_success;
}

gddAppFuncTableStatus myPV::readHighWarn(gdd &value)
{
  value.putConvert(attr.getHighWarning());
  return S_casApp_success;
}

gddAppFuncTableStatus myPV::readLowWarn(gdd &value)
{
  value.putConvert(attr.getLowWarning());
  return S_casApp_success;
}

gddAppFuncTableStatus myPV::readLowAlarm(gdd &value)
{
  value.putConvert(attr.getLowAlarm());
  return S_casApp_success;
}

gddAppFuncTableStatus myPV::readHighCtrl(gdd &value)
{
  value.putConvert(attr.getHighCtrl());
  return S_casApp_success;
}
gddAppFuncTableStatus myPV::readLowCtrl(gdd &value)
{
  value.putConvert(attr.getLowCtrl());
  return S_casApp_success;
}

gddAppFuncTableStatus myPV::readValue(gdd &value)
{
  // If pvAttr::pValue exists, then use the gdd::get() function to 
  // assign the current value of pValue to currentVal; then use the
  // gdd::putConvert() to write the value into value.
  gdd *pValue = attr.getVal();
  double currentVal;
  if(!pValue)
    return S_casApp_undefined;
  else {
    pValue->getConvert(currentVal);
    value.putConvert(currentVal);
    return S_casApp_success;
  }
}

gddAppFuncTableStatus myPV::readUnits(gdd &value)
{
  value.put(attr.getUnits());
  return S_casApp_success;
}

// bestExternalType() is a virtual function that can redefined to
// return the best type with which to access the PV. Called by the
// server library to respond to client request for the best type.
aitEnum myPV::bestExternalType()
{
  gdd* pValue = attr.getVal();
  if(!pValue)
    return aitEnumInvalid;
  else
    return pValue->primitiveType();
}

caStatus myPV::write(const casCtx &ctx, gdd &value)
{
  struct timespec t;
  osiTime current(osiTime::getCurrent());
  gdd *pValue;
  caServer *pServer = this->getCAS();
  double newVal;

  // Doesn't support writing to arrays or container objects
  // (gddAtomic or gddContainer).
  if(!(value.isScalar()) || !pServer)
    return S_casApp_noSupport;
   
  pValue = attr.getVal();
  // If pValue exists, unreference it, set the pointer to the new gdd
  // object, and reference it.
  if(pValue)
    pValue->unreference();
  pValue = &value;
  pValue->reference();

  // Set the timespec structure to the current time stamp the gdd.
  current.get(t.tv_sec, t.tv_nsec);
  pValue->setTimeStamp(&t);

  // Get the new value and set the severity and status according
  // to its value.
  value.get(newVal);
  if (newVal > 100){
    value.setStat(epicsAlarmHiHi);
    value.setSevr(epicsSevMajor);
  }
  else if (newVal >= 95){
    value.setStat(epicsAlarmHigh);
    value.setSevr(epicsSevMinor);
  }
  else if (newVal <=5 ){
    value.setStat(epicsAlarmLow);
    value.setSevr(epicsSevMinor);
  }
  else if (newVal < 0){
    value.setStat(epicsAlarmLoLo);
    value.setSevr(epicsSevMajor);
  }
  if(interest == aitTrue){
    casEventMask select(pServer->valueEventMask |
						  pServer->alarmEventMask);
    postEvent(select, *pValue);
  }
  return S_casApp_success;
}

/*** Server.cc: contains main(). ***/
#include <fdMgr.h>
#include "myServer.h"

// These static members must be re-declared in this file.
gddAppFuncTable<myPV> myServer::funcTable;
int myPV::currentOps;

main()
{
  myServer *pCAS;
  // Create server object.
  pCAS = new myServer(5u);
  if(!pCAS)
    return;
  pCAS->setDebugLevel(5u);

  // Loop forever
  osiTime delay(1000u, 0u);
  while (aitTrue)
    // fileDescriptorManager is a predeclared object found in fdMgr.h
    fileDescriptorManager.process(delay);
}

Channel Access Portable Server Tutorial - 26 NOV 1997

[Next] <server-18.html>[Previous] <server-16.html>[Top]
<server-1.html>[Contents] <server-3.html>[Index] <server-19.html>

| LANL <http://www.lanl.gov/> | Lansce <http://mesa53.lanl.gov> | UC
<http://www.ucla.edu/ucservers.html> | DOE <http://www.doe.gov/> |

*L O S   A L A M O S   N A T I O N A L    L A B O R A T O R Y*
/Operated by the University of California for the US Department of Energy/

/Copyright � 1996 UC <http://www.lanl.gov/Misc/copyright.html>  
Disclaimer <http://www.lanl.gov/Misc/disclaimer.html>    /



          For problems or questions regarding this web site contact
          George Vaughn <mailto:gvaughn@lanl.gov>.


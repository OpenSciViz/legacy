#if !defined(__UFInts_cc__)
#define __UFInts_cc__ "$Name:  $ $Id: UFInts.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFInts_cc__;

#include "UFInts.h"
__UFInts_H__(__UFInts_cc);

#include "UFBytes.h"
__UFBytes_H__(__UFInts_cc);

#include "cstdio"
#include "sys/types.h"
#include "sys/stat.h"

// public ctors
// for new create signature
UFInts::UFInts(const UFProtocolAttributes& pa) : UFTimeStamp() {
  _paInit(pa);
  if( _pa->_type != _Ints_ ) {
    clog<<"UFInts::> ? type mismatch, _pa->_type = "<< _pa->_type << endl;
    _pa->_type = _Ints_;
  }
  if( _pa->_elem > 0 && !_shared && !_shallow ){
    _pa->_values = (void*) new (nothrow) int[_pa->_elem];
    if( _pa->_values == 0 ) {
      clog<<"UFInts> new alloc failed."<<endl;
      _pa->_elem = 0; _pa->_length = 0;
      return;
    }
  }
}

UFInts::UFInts(bool shared, int length) : UFTimeStamp() {
  _paInit(_Ints_, "UFInts", length);
  _shared = shared; // reset from default
  if( _pa->_elem > 0 && !_shared && !_shallow ) {
    _pa->_values = (void*) new (nothrow) int[_pa->_elem];
    if( _pa->_values == 0 ) {
      clog<<"UFInts> new alloc failed."<<endl;
      _pa->_elem = 0; _pa->_length = 0;
      return;
    }
  }
  _currentTime();
}

UFInts::UFInts(const string& name,
	       bool shared,
	       int length) : UFTimeStamp() {
  _paInit(_Ints_, name, length);
  _shared = shared; // reset from default
  if( _pa->_elem > 0 && !_shared && !_shallow ) {
    _pa->_values = (void*) new (nothrow) int[_pa->_elem];
    if( _pa->_values == 0 ) {
      clog<<"UFInts> new alloc failed."<<endl;
      _pa->_elem = 0; _pa->_length = 0;
      return;
    }
  }
  _currentTime();
}

// this (very odd & very special!) ctor supports shared memory applications
// UFProtocolAttributes* is non-const, indicating we actually want to init its contents here:

UFInts::UFInts(const string& name, UFProtocolAttributes* pa,
	       int elem, bool shared) : UFTimeStamp() {
  if( shared ) { // presumable pa actually point to uninitialized shared mem. seg.
    _paInit(_Ints_, name); //always  provides initial allocation of_pa
    _shared = shared;
    // be sure shared memory _values points to shared memory!
    // insure shared mem. is initialized
    *pa = *_pa;
    // and reset for shared mem.
    pa->_length = minLength() + elem*sizeof(int);
    pa->_elem = elem;
    pa->_values = (void*) ((int*)&pa->_values + 1);
    delete _pa; // free allocation from _paInit
    _pa = pa;
  }
  else { // not shared mem., but assume pa has been initialized?
    _paInit(*pa);
    _pa->_type=_Ints_; 
  }
  //if( _pa->_elem != elem ) {
  // clog<<"UFInts:UFInts> ? error in ctor, not using shared memory?"<<endl;
  //}
}

// these ctor signatures do a shallow copy of the values, for efficiency.
UFInts::UFInts(const string& name, const int* vals,
	       int elem, bool shared) : UFTimeStamp() {
  //clog<<"UFInts::UFInts> shallow ctor..."<<endl;
  _paInit(_Ints_, name);
  _shallow = true; // shallow copy of values (header not!)
  _shared = shared; // shared-memory resident object (header + values)
  _pa->_elem = elem;
  _pa->_length = minLength() + elem*sizeof(int);
  _pa->_values = (void*) vals;
  _currentTime();
}

// shallow copy ctor
UFInts::UFInts(const UFInts& rhs) : UFTimeStamp() {
  _paInit(_Ints_, "UFInts");
  shallowCopy(rhs);
}

//ctor to alloc and zero out vals array:
UFInts::UFInts(const string& name, int elem, bool shared) : UFTimeStamp()
{
  _paInit(_Ints_, name);
  _shared = shared;
  _pa->_elem = elem;
  _pa->_length = minLength() + elem*sizeof(int);
  
  if( elem > 0 ) {
    _pa->_values = (void*) new (nothrow) int[elem];
    if( _pa->_values == 0 ) {
       clog<<"UFInts (zero ctor)> new alloc failed."<<endl;
       _pa->_elem = 0;
       _pa->_length = minLength();
       return;
    }
  }
  
  ::memset( _pa->_values, 0, _pa->_elem*sizeof(int) );
  _currentTime();
}

// Deep vals copy ctor.
// If pointer vals is null, it just sets values to zeros.
// If pointer indexMap is nonzero (default is zero),
// then the array vals are copied with reindexing via index map.

UFInts::UFInts(const string& name, int* vals, int elem,
	       bool shared, int* indexMap, bool invertMap) : UFTimeStamp()
{
  _paInit(_Ints_, name);
  _shared = shared;
  _pa->_elem = elem;
  _pa->_length = minLength() + elem*sizeof(int);
  
  if( elem > 0 ) {
    _pa->_values = (void*) new (nothrow) int[elem];
    if( _pa->_values == 0 ) {
       clog<<"UFInts (deep vals copy ctor)> new alloc failed."<<endl;
       _pa->_elem = 0;
       _pa->_length = minLength();
       return;
    }
  }
  
  if( vals )
    {
      if( indexMap ) {
	if( invertMap )
	  for( int i=0; i<elem; i++ ) ((int*)_pa->_values)[i] = vals[indexMap[i]];
	else
	  for( int i=0; i<elem; i++ ) ((int*)_pa->_values)[indexMap[i]] = vals[i];
      }
      else ::memcpy( _pa->_values, vals, elem*sizeof(int) );
    }
  else // null pointer passed - just set to 0s
    ::memset( _pa->_values, 0, _pa->_elem*sizeof(int) );

  _currentTime();
}

// Deep vals copy with re-indexing ctor.
// If pointer vals is null, it just sets values to zeros.
// If pointers indexMapFrom and indexMapTo are nonzero,
// then the array vals are copied with reindexing via the From and To maps.

UFInts::UFInts(const string& name, int* vals,
	       int* indexMapFrom, int* indexMapTo, int elem) : UFTimeStamp()
{
  _paInit(_Ints_, name);
  _shared = false;
  _pa->_elem = elem;
  _pa->_length = minLength() + elem*sizeof(int);
  
  if( elem > 0 ) {
    _pa->_values = (void*) new (nothrow) int[elem];
    if( _pa->_values == 0 ) {
       clog<<"UFInts (deep vals copy ctor)> new alloc failed."<<endl;
       _pa->_elem = 0;
       _pa->_length = minLength();
       return;
    }
  }
  
  if( vals )
    {
      if( indexMapFrom && indexMapTo )
	for( int i=0; i<elem; i++ )
	  ((int*)_pa->_values)[indexMapTo[i]] = vals[indexMapFrom[i]];
      else
	::memcpy( _pa->_values, vals, elem*sizeof(int) );
    }
  else // null pointer passed - just set to 0s
    ::memset( _pa->_values, 0, _pa->_elem*sizeof(int) );

  _currentTime();
}

// deep copy helper, assumes that _paInit() has created _pa:

void UFInts::deepCopy(const UFProtocol& rhsP) {
  _shallow = false;
  const UFInts& rhs = dynamic_cast<const UFInts&>(rhsP);
  UFProtocolAttributes pas = *_pa; // save current settings
  *_pa = *rhs._pa;

  // support reuse, if current allocation is sufficient;
  // if not, delete current allocation and allocate larger buffer.
  if( !_shared && pas._elem < _pa->_elem ) {
    if( pas._elem > 0 )
      delete (int*)pas._values;
    _pa->_values = (void*) new (nothrow) int[_pa->_elem];
    if( _pa->_values == 0 ) {
      clog<<"UFInts::deepCopy> new alloc failed."<<endl;
      _pa->_elem = 0; _pa->_length = 0;
      return;
    }
  }
  else {
    if( pas._elem > _pa->_elem )
      ::memset( pas._values, 0, pas._elem*sizeof(int) );
    _pa->_values = pas._values;
  }

  ::memcpy( _pa->_values, rhs._pa->_values, _pa->_elem*sizeof(int) );
}

// Utility method for reversing byte order of the array of int values:

int UFInts::reverseByteOrder()
{
  char* bvals = (char* )_pa->_values;
  char tmp[4];
  int i, nbytes = sizeof(int) * _pa->_elem;
      
  for( i = 0; i < nbytes; i += 4 )
    {
      tmp[0] = bvals[i+3]; tmp[1] = bvals[i+2]; tmp[2] = bvals[i+1]; tmp[3] = bvals[i];
      bvals[i] = tmp[0]; bvals[i+1] = tmp[1]; bvals[i+2] = tmp[2]; bvals[i+3] = tmp[3];
    }

  return i;
}

int UFInts::maxVal()
{
  int* vals = (int*)_pa->_values;
  int maxVal = -2147483647;
      
  for( int i = 0; i < _pa->_elem; i++ )
    if( vals[i] > maxVal ) maxVal = vals[i];

  return maxVal;
}

int UFInts::minVal()
{
  int* vals = (int*)_pa->_values;
  int minVal = 2147483647;
      
  for( int i = 0; i < _pa->_elem; i++ )
    if( vals[i] < minVal ) minVal = vals[i];

  return minVal;
}

// write msg out to file descriptor, returns 0 on failure, num. bytes output on success
int UFInts::writeTo(int fd) const {
  int retval=0;
  // check fd
  if( fd < 0 ) {
    clog<<"UFInts::writeTo> bad fd= "<<fd<<endl;
    return retval;
  }
  struct stat st;
  if( fstat(fd, &st) != 0 ) {
    clog<<"UFInts::writeTo> bad stat on fd= "<<fd<<endl;
    return retval;
  }

  // write out "persistent" attributes in order : _length, _type, _pa->_elem, ...
  // _pa->_name, _pa->_values
  retval += writeHeader(fd, *_pa);
  retval += writeValues(fd);

  if( retval < _pa->_length )
    clog<<"UFInts::writeTo> ? bad write on fd= "<<fd<<endl;

  return retval;
}

// this might support compression some day
int UFInts::writeValues(int fd) const {
  return writeFully(fd, (unsigned char*) _pa->_values, _pa->_elem*sizeof(int));
}

int UFInts::readFrom(int fd) {
  int retval=0;
  // check fd
  if( fd < 0 ) {
    clog<<"UFInts::readFrom> bad fd= "<<fd<<endl;
    return retval;
  }
  struct stat st;
  if( fstat(fd, &st) != 0 ) {
    clog<<"UFInts::readFrom> bad stat on fd= "<<fd<<endl;
    return retval;
  }

  UFProtocolAttributes pa;
  int nb = readHeader(fd, pa); // sets everything except _values ptr 
  if( nb <= 0 ) {
    clog<<"UFInts::readFrom> ? read failed, fd= "<<fd<<endl;
    return nb;
  }
  if( pa._type != typeId() ) {
    clog<<"UFInts::readFrom> reuse problem! current type= "<<typeId()
        <<", transmitted type= "<<pa._type<<endl;
    // need to dynamically recast to new type (use convertions ctor)
    // later...
  }
  if( pa._elem <= 0 ) { // more problems with transmitted type?
    return retval;
  }
  else if( _pa->_values == 0 || (_pa->_elem < pa._elem && !_shared) ) {
    delete[] static_cast< int* >( _pa->_values ); // delete and re-allocate
    *_pa = pa; // ressets everything including _values ptr
    _pa->_values = pa._values = (void*) new (nothrow) int[_pa->_elem];
    if( _pa->_values == 0 ) {
      clog<<"UFInts> new alloc failed."<<endl;
      _pa->_elem = 0; _pa->_length = 0;
      return retval;
    }
  }
  else // reuse existing allocation
    _pa->_values = pa._values;

  retval = nb;
  // virtual read
  retval += readValues(fd); // must complete the read otherwise file synch is lost

  if( retval < _pa->_length ) {
    clog<<"UFInts::readValues> bad read on fd= "<<fd<<endl;
  }

  return retval;
}

// read/restore from file descriptor
// someday support decompression
int UFInts::readValues(int fd) {
  return readFully(fd, (unsigned char*) _pa->_values , _pa->_elem*sizeof(int));
}

// write msg out to socket, returns 0 on failure, num. bytes on success
int UFInts::sendTo(UFSocket& soc) const {
  // make use of the socket class i/o & UFprotocolclass methods
  int retval = sendHeader(soc, *_pa);
  if( UFProtocol::_verbose ) 
    clog<<"UFInts::sendTo> sent header, retval: "<<retval<<endl;
  if( retval <= 0 ) 
    return retval;

  if( _pa->_values && _pa->_duration >= 0.0 ) {
    retval += sendValues(soc);
    if( retval < _pa->_length )
      clog<<"UFInts::sendTo> bad send, sent: "<<retval<<", but object length is: "<<_pa->_length<<endl;
      clog<<"UFInts::sendTo> name: "<<_pa->_name<<", on soc: "<<soc.description()<<endl;
  }
  return retval;
}

// once header has been sent out, send the data
// this could optionally send out compressed data:
int UFInts::sendValues(UFSocket& soc) const {
  int last = _pa->_elem-1;
  if( UFProtocol::_verbose ) 
    clog<<"UFInts::sendValues> 0: "<<((int*)_pa->_values)[0]<<", "<<last<<": "<<((int*)_pa->_values)[last]<<endl;
  return soc.send((int*)_pa->_values, _pa->_elem);
}

int UFInts::recvFrom(UFSocket& soc) {
  int retval= 0;
  UFProtocolAttributes pa= *_pa; // initialize to "this"
  int nb = recvHeader(soc, pa); // resets everything except _values ptr 
  if( nb <= 0 ) {
    clog<<"UFInts::recvFrom> socket recv failed?"<<endl;
    return nb;
  }
  if( pa._type != typeId() ) {
    clog<<"UFInts::recvFrom> reuse problem! current type= "<<typeId()
        <<", transmitted type= "<<pa._type<<endl;
    // need to dynamically recast to new type (use a conversion ctor)
    // later...
  }
  if( pa._elem <= 0 ) { // more problems with transmitted type?
    return retval;
  }

  // OK to reset "this" header
  *_pa = pa; 

  if( pa._duration < 0.0 ) { // must be a "request"
    return nb; // and return without further socket input (none expected?)
  }
  else if( _pa->_values == 0 || (_pa->_elem < pa._elem && !_shared) ) {
    delete[] static_cast< int* >( _pa->_values ); // delete and re-allocate
    _pa->_values = (void*) new (nothrow) int[_pa->_elem];
    if( _pa->_values == 0 ) {
      clog<<"UFInts> new alloc failed."<<endl;
      _pa->_elem = 0; _pa->_length = 0;
      return retval;
    }
  }

  // virtual recv
  retval = nb + recvValues(soc); // must complete the recv otherwise socket synch is lost

  if( retval < _pa->_length )
    clog<<"UFInts::recvFrom> ? bad recv on soc= "<<soc.description()<<endl;

  return retval;
}

// read/restore values from socket, assuming header has already been read
// deal with compression later...

int UFInts::recvValues(UFSocket& soc) {
  if( UFProtocol::_verbose ) {
    clog<<"UFInts::recvValues> elem: "<<_pa->_elem<<", duration: "<<_pa->_duration<<endl;
    printHeader(_pa);
  }
  if( _pa->_duration >= 0.0 && _pa->_values )
    return soc.recv((int*)_pa->_values, _pa->_elem);

  return 0;
} 

// Overload the usual arithmetic operators,
//  so that vector arithmetic can be performed on the values of two UFInts.

// Unary operators:
// If rhs is a single element, it is used as a constant operator on lhs array of ints.

UFInts& UFInts::operator+=( const UFInts& rhs )
{
  const UFProtocolAttributes* paL = attributesPtr();
  const UFProtocolAttributes* paR = rhs.attributesPtr();
  
  int* vL = (int*)paL->_values;
  int* vR = (int*)paR->_values;

  if( paR->_elem == 1 )
    for( int i=0; i< paL->_elem; i++ ) *vL++ += *vR;
  else
    for( int i=0; i<min( paL->_elem, paR->_elem ); i++ ) *vL++ += *vR++;

  return *this;
} 

UFInts& UFInts::operator-=( const UFInts& rhs )
{
  const UFProtocolAttributes* paL = attributesPtr();
  const UFProtocolAttributes* paR = rhs.attributesPtr();
  
  int* vL = (int*)paL->_values;
  int* vR = (int*)paR->_values;
  
  if( paR->_elem == 1 )
    for( int i=0; i< paL->_elem; i++ ) *vL++ -= *vR;
  else
    for( int i=0; i<min( paL->_elem, paR->_elem ); i++ ) *vL++ -= *vR++;

  return *this;
} 

UFInts& UFInts::operator*=( const UFInts& rhs )
{
  const UFProtocolAttributes* paL = attributesPtr();
  const UFProtocolAttributes* paR = rhs.attributesPtr();
  
  int* vL = (int*)paL->_values;
  int* vR = (int*)paR->_values;

  if( paR->_elem == 1 )
    for( int i=0; i< paL->_elem; i++ ) *vL++ *= *vR;
  else
    for( int i=0; i<min( paL->_elem, paR->_elem ); i++ ) *vL++ *= *vR++;

  return *this;
}

UFInts& UFInts::operator/=( const UFInts& rhs )
{
  const UFProtocolAttributes* paL = attributesPtr();
  const UFProtocolAttributes* paR = rhs.attributesPtr();
  
  int* vL = (int*)paL->_values;
  int* vR = (int*)paR->_values;
  
  if( paR->_elem == 1 )
    for( int i=0; i< paL->_elem; i++ ) *vL++ /= *vR;
  else
    for( int i=0; i<min( paL->_elem, paR->_elem ); i++ ) *vL++ /= *vR++;

  return *this;
}

int UFInts::sum( const UFInts& Left, const UFInts& Right ) {
  const UFProtocolAttributes* paL = Left.attributesPtr();
  const UFProtocolAttributes* paR = Right.attributesPtr();
  
  int* vi = (int*)_pa->_values;
  int* viL = (int*)paL->_values;
  int* viR = (int*)paR->_values;
  int elem = min( paL->_elem, paR->_elem );
  elem = min(elem, _pa->_elem);
  if( vi == 0 || viL == 0 || viR == 0 || elem <= 0 )
    return 0;

  for( int i=0; i<elem; i++ ) 
   *vi++ = *viL++ + *viR++;

  return elem;
}
int UFInts::diff( const UFInts& Left, const UFInts& Right ) {
  const UFProtocolAttributes* paL = Left.attributesPtr();
  const UFProtocolAttributes* paR = Right.attributesPtr();
  
  int* vi = (int*)_pa->_values;
  int* viL = (int*)paL->_values;
  int* viR = (int*)paR->_values;
  int elem = min( paL->_elem, paR->_elem );
  elem = min(elem, _pa->_elem);
  if( vi == 0 || viL == 0 || viR == 0 || elem <= 0 )
    return 0;

  for( int i=0; i<elem; i++ ) 
   *vi++ = *viL++ - *viR++;

  return elem;
}

// Overloaded binary operators:
// If either Left or Right is a single element,
//  it is used as a constant operator on other array of ints.

UFInts* operator+( const UFInts& Left, const UFInts& Right )
{
  const UFProtocolAttributes* paL = Left.attributesPtr();
  const UFProtocolAttributes* paR = Right.attributesPtr();
  
  int* viL = (int*)paL->_values;
  int* viR = (int*)paR->_values;
  int Nelem = min( paL->_elem, paR->_elem );
  UFInts* Result;

  if( Nelem == 1 ) {
    if( paL->_elem == 1 )
      Result = new (nothrow) UFInts( "UFInts + UFInts", viR, paR->_elem );
    else
      Result = new (nothrow) UFInts( "UFInts + UFInts", viL, paL->_elem );
  }
  else Result = new (nothrow) UFInts( "UFInts + UFInts", viL, Nelem );

  if( Result ) {
    const UFProtocolAttributes* paResult = Result->attributesPtr();
    int* viResult = (int*)paResult->_values;
    if( Nelem == 1 ) {
      if( paL->_elem == 1 )
	for( int i=0; i<paR->_elem; i++ ) *viResult++ += *viL;
      else
	for( int i=0; i<paL->_elem; i++ ) *viResult++ += *viR;
    }
    else
      for( int i=0; i<Nelem; i++ ) *viResult++ += *viR++;
  }

  return Result;
}

UFInts* operator-( const UFInts& Left, const UFInts& Right )
{
  const UFProtocolAttributes* paL = Left.attributesPtr();
  const UFProtocolAttributes* paR = Right.attributesPtr();
  
  int* viL = (int*)paL->_values;
  int* viR = (int*)paR->_values;
  int Nelem = min( paL->_elem, paR->_elem );
  UFInts* Result;

  if( Nelem == 1 ) {
    if( paL->_elem == 1 )
      Result = new (nothrow) UFInts( "UFInts + UFInts", viR, paR->_elem );
    else
      Result = new (nothrow) UFInts( "UFInts + UFInts", viL, paL->_elem );
  }
  else Result = new (nothrow) UFInts( "UFInts + UFInts", viL, Nelem );

  if( Result ) {
    const UFProtocolAttributes* paResult = Result->attributesPtr();
    int* viResult = (int*)paResult->_values;
    if( Nelem == 1 ) {
      if( paL->_elem == 1 )
	for( int i=0; i<paR->_elem; i++ ) *viResult++ -= *viL;
      else
	for( int i=0; i<paL->_elem; i++ ) *viResult++ -= *viR;
    }
    else
      for( int i=0; i<Nelem; i++ ) *viResult++ -= *viR++;
  }

  return Result;
}

#endif // __UFInts_cc__

#if !defined(__UFFlamObsConf_h__)
#define __UFFlamObsConf_h__ "$Name:  $ $Id: UFFlamObsConf.h,v 0.12 2006/02/10 22:05:44 hon Exp $"
#define __UFFlamObsConf_H__(arg) const char arg##FlamObsConf_h__rcsId[] = __UFFlamObsConf_h__;

#include "UFRuntime.h"
#include "UFFloats.h"
#include "string"
#include "vector"
#include "deque"
#include "map"

// this is a merged FrameConfig and ObsConfig that supports Flamingos readout/obs modes:
// (at most 2x2) "science-stare" and "engineering-stare and perhaps "-nod" ...
// nodsperobs == 0 with nodIdx0 == nodIdxF == 0 is congruent with "stare" mode and is
// the default science configuration:
class UFFlamObsConf: public virtual UFFloats {
public:
  enum { SciMode= 1, EngMode= 2 };

  // nodsperobs == 0 and _frmspernod == 1 for standard single image sciende mode
  // nodsperobs == 0 and _frmspernod == 2 for standard dual aimage engineering mode 
  struct Config { // no current plan to use nods > 0 for F2, but perhaps in the future
    float _exptime;
    int _w, _h, _d, _littleEndian, _frmspernod, _nodsperobs, _nodIdx, _nodIdxF;
    inline Config(float exptime= 1.0, int frmmode= SciMode) :
      _exptime(exptime), _w(2048), _h(2048), _d(32), _littleEndian(0),
      _frmspernod(frmmode), _nodsperobs(0), _nodIdx(0), _nodIdxF(0) {
      if( UFRuntime::littleEndian() ) _littleEndian = 1;
    }
  };

  // UFFlamObsConf ctor for create function:
  UFFlamObsConf(); // default ctor
  // construct form min. req. values
  UFFlamObsConf(float exptime, const string& datalabel= "Test", int frmmode= SciMode);
  // construct form command queue
  UFFlamObsConf(const deque< string >& cmds); 
  // construct from transaction
  UFFlamObsConf(const UFProtocolAttributes& pa);
  // (deep) copy ctor 
  UFFlamObsConf(const UFFlamObsConf& oc);

  inline virtual ~UFFlamObsConf() {}

  inline virtual string description() { return string(__UFFlamObsConf_h__); }

  // qlook buffer names:
  inline static int getBufNames(vector< string > & qlnames) {
    qlnames.clear();
    qlnames.push_back("flam:512Image");
    qlnames.push_back("flam:1024Image");
    qlnames.push_back("flam:FullImage");
    return (int) qlnames.size();
  }
  // estimated MCE4 EDT fiber transfer time in seconds (depends on readout mode)
  inline float estXferTime() { return 0.05; }

  // header name element format is expected to be: "directive||datalabel":
  virtual string datalabel() const; // override UFProtocol?
  string directive() const;
  string abortObs(string label= "");
  string stopObs(string label= "");
  string startObs(float exptime= -1, string label= "", int idx0= 0, int idxF= 0, int nods= 0);
  string discardObs(string label= "", int idx0= 0, int idxF= 0, int nods= 0);

  // if name/datalabel contains 'start', this is a new obs:
  bool start() const;  
  // if name/datalabel contains 'abort', this is an aborted obs:
  bool abort() const;
  // if name/datalabel contains 'stop', this is a stopped obs:
  bool stop() const;   
  // if name/datalabel contains 'discard', this obs data is not to be saved:
  // (also indicates start obs)
  bool discard() const;   

  void setConfig(const UFFlamObsConf::Config& c, const string& label= "");
  string getConfig(UFFlamObsConf::Config& c);

  //static int reqConfig(UFSocket& soc, UFFlamObsConf& c); // resets c
  //static int recvConfig(UFSocket& soc, UFFlamObsConf& c); // resets c

  inline float expTime() const { return ((float*) _pa->_values)[0]; }
  inline float setExpTime(float e= 1.0) { ((float*) _pa->_values)[0] = e; return expTime(); }
  inline int width() const { return (int)((float*) _pa->_values)[1]; }
  inline void setWidth(int d= 1) { ((float*) _pa->_values)[1] = d; }
  inline int height() const { return (int) ((float*) _pa->_values)[2]; }
  inline void setHeight(int d= 1) { ((float*) _pa->_values)[2] = d; }
  inline int depth() const { return (int)((float*) _pa->_values)[3]; }
  inline void setDepth(int d= 1) { ((float*) _pa->_values)[3] = d; }
  inline int littleEndian() const { return (int)((float*) _pa->_values)[4]; }
  inline int framesPerNod() const { return (int)((float*) _pa->_values)[5]; }
  inline void setFramesPerNod(int d= 1) { ((float*) _pa->_values)[5] = d; }
  inline int nodsPerObs() const { return (int)((float*) _pa->_values)[6]; }
  inline void setNodsPerObs(int d= 0) { ((float*) _pa->_values)[6] = d; }
  inline int nodIdx() const { return (int)((float*) _pa->_values)[7]; }
  inline void setNodIdx(int d= 0) { ((float*) _pa->_values)[7] = d; }
  inline int nodIdxF() const { return (int)((float*) _pa->_values)[8]; }
  inline void setNodIdxF(int d= 0) { ((float*) _pa->_values)[8] = d; }
  inline int totImgCnt() { int nods = nodsPerObs(); if( nods == 0 ) ++nods; return (int)nods*framesPerNod(); }
  inline int dmaCnt() const { return (int)((float*) _pa->_values)[9]; }
  inline void setDMACnt(int d= 1) { ((float*) _pa->_values)[9] = d; }
  inline int imageCnt() const { return (int)((float*) _pa->_values)[10]; }
  inline void setImageCnt(int d= 1) { ((float*) _pa->_values)[10] = d; }
  inline int pixelSort() const { return (int)((float*) _pa->_values)[11]; }
  inline void setPixelSort(int d= 1) { ((float*) _pa->_values)[11] = d; }
  inline int frameObsSeqNo() const { return (int)((float*) _pa->_values)[12]; }
  inline void setFrameObsSeqNo(int d= 1) { ((float*) _pa->_values)[12] = d; }
  inline int frameObsSeqTot() const { return (int)((float*) _pa->_values)[13]; }
  inline void setFrameObsSeqTot(int d= 1) { ((float*) _pa->_values)[13] = d; }
  inline int frameWriteCnt() const { return (int)((float*) _pa->_values)[14]; }
  inline void setFrameWriteCnt(int d) { ((float*) _pa->_values)[14] = d; }
  inline int frameSendCnt() const { return (int)((float*) _pa->_values)[15]; }
  inline void setFrameSendCnt(int d) { ((float*) _pa->_values)[15] = d; }
  inline int bgADUs() const { return (int)((float*)_pa->_values)[16]; }
  inline void bgADUs(int i) { ((float*)_pa->_values)[16] = i; }
  inline int rdADUs() const { return (int)((float*)_pa->_values)[17]; }
  inline void rdADUs(int i) { ((float*)_pa->_values)[17] = i; }
  inline float bgWellpc() const { return ((float*)_pa->_values)[18]; }
  inline void bgWellpc(float f) { ((float*)_pa->_values)[18] = f; }
  inline float sigmaFrmNoise() const { return ((float*)_pa->_values)[19]; }
  inline void sigmaFrmNoise(float f) { ((float*)_pa->_values)[19] = f; }
  inline int offADUs() const { return (int)((float*)_pa->_values)[20]; }
  inline void offADUs(int i) { ((float*)_pa->_values)[20] = i; }
  inline float offWellpc() const { return ((float*)_pa->_values)[21]; }
  inline void offWellpc(float f) { ((float*)_pa->_values)[21] = f; }
  inline float sigmaReadNoise() const { return ((float*)_pa->_values)[22]; }
  inline void sigmaReadNoise(float f) { ((float*)_pa->_values)[22] = f; }
  inline float frameTime() const { return ((float*)_pa->_values)[23]; }
  inline void frameTime(float f) { ((float*)_pa->_values)[23] = f; }
  inline int offset() const { return (int)((float*) _pa->_values)[24]; }
  inline void setOffset(int o) 
    { if( o < 0 || o >= width() * height() ) o = 0; ((float*) _pa->_values)[24] = o; }
  inline int pixCnt() const { return (int)((float*) _pa->_values)[25]; }
  inline void setPixCnt(int c)
    { if( c <= 0 ) c = width()*height(); ((float*) _pa->_values)[25] = c; }
  inline int bgADUmin() const { return (int)((float*)_pa->_values)[26]; }
  inline void bgADUmin(int i) { ((float*)_pa->_values)[26] = i; }
  inline int bgADUmax() const { return (int)((float*)_pa->_values)[27]; }
  inline void bgADUmax(int i) { ((float*)_pa->_values)[27] = i; }
  inline float sigmaFrmMin() const { return ((float*)_pa->_values)[28]; }
  inline void sigmaFrmMin(float f) { ((float*)_pa->_values)[28] = f; }
  inline float sigmaFrmMax() const { return ((float*)_pa->_values)[29]; }
  inline void sigmaFrmMax(float f) { ((float*)_pa->_values)[29] = f; }
  inline int rdADUmin() const { return (int)((float*)_pa->_values)[30]; }
  inline void rdADUmin(int i) { ((float*)_pa->_values)[30] = i; }
  inline int rdADUmax() const { return (int)((float*)_pa->_values)[31]; }
  inline void rdADUmax(int i) { ((float*)_pa->_values)[31] = i; }
  inline float sigmaReadMin() const { return ((float*)_pa->_values)[32]; }
  inline void sigmaReadMin(float f) { ((float*)_pa->_values)[32] = f; }
  inline float sigmaReadMax() const { return ((float*)_pa->_values)[33]; }
  inline void sigmaReadMax(float f) { ((float*)_pa->_values)[33] = f; }

protected:
  float* _init(float exptime= 1.0, int frmmode= SciMode, int cnt= 34);
};    
#endif // UFFlamObsConf

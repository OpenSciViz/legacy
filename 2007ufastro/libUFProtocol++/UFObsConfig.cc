#if !defined(__UFObsConfig_cc__)
#define __UFObsConfig_cc__ "$Name:  $ $Id: UFObsConfig.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFObsConfig_cc__;

#include "UFObsConfig.h"

#include "strstream"
//#include "bitset"

static UFFrameConfig* _prevfc= 0; // note this is potentially thread-unsafe

// global/static (also potentially thread-unsafe)
vector< string > UFObsConfig::_trecsbufnames;
map< string, UFInts* > UFObsConfig::_trecsbufs;

// get all buffer names:
int UFObsConfig::getBufNames(vector< string >& names) {
  // deep copy
  names.clear();
  int i = 0, cnt = (int)_trecsbufnames.size();
  if( cnt <= 0 ) // nee to init the buf names...
  cnt = initTReCSBufs();
  for( ; i < cnt; ++i )
    names.push_back(_trecsbufnames[i]);

  return i;
}

// get all buffer names prefixed with intrument name:
int UFObsConfig::getBufNames(const string& instrum, vector< string >& names) {
  // deep copy
  names.clear();
  int i = 0, cnt = (int)_trecsbufnames.size();
  if( cnt <= 0 ) // nee to init the buf names...
  cnt = initTReCSBufs();
  for( ; i < cnt; ++i ) {
    string bufnam = instrum + ':' + _trecsbufnames[i];
    names.push_back(bufnam);
  }
  return i;
}

// get buffer by name:
UFInts* UFObsConfig::getBuf(const string& name) {
  map< string, UFInts* >::iterator it = _trecsbufs.find(name);
  if( it == _trecsbufs.end() ) 
    return 0;

  return it->second;
}

// init above static objects
int UFObsConfig::initTReCSBufs(UFFrameConfig* fc) {
  _trecsbufnames.clear();
  if( !_trecsbufs.empty() ) {
    map< string, UFInts* >::iterator it = _trecsbufs.begin();
    do {
      UFInts* buf = it->second;
      delete buf;
    } while( ++it != _trecsbufs.end() );
    _trecsbufs.clear();
  }

  int numpix = 320*240;
  if( fc )
    numpix = fc->pixCnt();

  // insure a zero's initial buffer:
  int trecsimg[(const int) numpix ]; memset(trecsimg, 0, sizeof(trecsimg));

  // these use buf from socket:
  _trecsbufnames.push_back("src1");        //0 == UFObsConfig::buffId(FrameCount) - 1
  _trecsbufs["src1"] = 0;
  _trecsbufnames.push_back("ref1");        //1 == UFObsConfig::buffId(FrameCount) - 1
  _trecsbufs["ref1"] = 0;
  _trecsbufnames.push_back("src2");        //2 == UFObsConfig::buffId(FrameCount) - 1
  _trecsbufs["src2"] = 0;
  _trecsbufnames.push_back("ref2");        //3 == UFObsConfig::buffId(FrameCount) - 1
  _trecsbufs["ref2"] = 0;

  string bufnam = "accum(src1)";
  _trecsbufnames.push_back(bufnam); //4
  _trecsbufs[bufnam] = new (nothrow) UFInts(bufnam, (int*) trecsimg, (const int) numpix); // deep copy ctor

  bufnam = "accum(ref1)";
  _trecsbufnames.push_back(bufnam); //5
  _trecsbufs[bufnam] = new (nothrow) UFInts(bufnam, (int*) trecsimg, (const int) numpix); // deep copy ctor

  bufnam = "accum(src2)";
  _trecsbufnames.push_back(bufnam); //6
  _trecsbufs[bufnam] = new (nothrow) UFInts(bufnam, (int*) trecsimg, (const int) numpix); // deep copy ctor

  bufnam = "accum(ref2)";
  _trecsbufnames.push_back(bufnam); //7
  _trecsbufs[bufnam] = new (nothrow) UFInts(bufnam, (int*) trecsimg, (const int) numpix); // deep copy ctor

  bufnam = "dif1";
  _trecsbufnames.push_back(bufnam);        //8
  _trecsbufs[bufnam] = new (nothrow) UFInts(bufnam, (int*) trecsimg, (const int) numpix); // deep copy ctor

  bufnam = "dif2";
  _trecsbufnames.push_back(bufnam);        //9
  _trecsbufs[bufnam] = new (nothrow) UFInts(bufnam, (int*) trecsimg, (const int) numpix); // deep copy ctor

  bufnam = "accum(dif1)";
  _trecsbufnames.push_back(bufnam); //10
  _trecsbufs[bufnam] = new (nothrow) UFInts(bufnam, (int*) trecsimg, (const int) numpix); // deep copy ctor

  bufnam = "accum(dif2)";
  _trecsbufnames.push_back(bufnam); //11
  _trecsbufs[bufnam] = new (nothrow) UFInts(bufnam, (int*) trecsimg, (const int) numpix); // deep copy ctor

  bufnam = "sig";
  _trecsbufnames.push_back(bufnam);         //12
  _trecsbufs[bufnam] = new (nothrow) UFInts(bufnam, (int*) trecsimg, (const int) numpix); // deep copy ctor

  bufnam = "accum(sig)";
  _trecsbufnames.push_back(bufnam);  //13
  _trecsbufs[bufnam] = new (nothrow) UFInts(bufnam, (int*) trecsimg, (const int) numpix); // deep copy ctor

  return (int) _trecsbufnames.size();
}

// note that the buf names should be congruent with qlook names (i,e, trecs:etc.)
int UFObsConfig::evalTReCS(int frmIdx, UFInts* frame, map< string, UFInts* >& bufs, UFFrameConfig* fc,  bool freeFrm) {
  if( frame == 0 ) {
    clog<<"UFObsConfig::evalTReCS> no data?"<<endl;
    return 0;
  }

  if( _trecsbufnames.empty() )
    initTReCSBufs(fc);

  if( fc ) {
    if( _prevfc == 0 )
      _prevfc = fc;
    if( _prevfc->pixCnt() != fc->pixCnt() ) {
      // must be new obs. and sub-image conf?
      _prevfc = fc;
      initTReCSBufs(fc);
    }
    if( fc->pixCnt() != frame->numVals() ) {
      clog<<"UFObsConfig::evalTReCS> frameconf. and frame size mismatch?"<<endl;
      return 0;
    }
  }
  bufs.clear();

  //int nodfrms = chopBeams() * saveSets();
  int nodpos = nodPosition(frmIdx);
  int chopos = chopPosition(frmIdx);
  string timeframe = frame->timeStamp();

  if( nodpos == 0 ) { // even nod phase
    //if( frmIdx % 2 == 0 ) { // even chop phase for even nod phase is on source
    if( chopos == 0 ) { // even chop phase for even nod phase is on source
      //clog<<"UFObsConfig::evalTReCS> even chop phase for even nod phase, chopos= "<<chopos<<", nodpos= "<<nodpos<<endl;
      if( freeFrm) delete _trecsbufs["src1"];
      bufs["trecs:src1"] = _trecsbufs["src1"] = frame; //frame->rename("src1"); // 0
      UFInts& s1 = *(_trecsbufs["src1"]); s1.stampTime(timeframe);
      UFInts& as1 = *(_trecsbufs["accum(src1)"]); as1.stampTime(timeframe);
      as1 += s1; 
      bufs["trecs:accum(src1)"] = _trecsbufs["accum(src1)"]; // = frame; // 2
    }
    else {
      //clog<<"UFObsConfig::evalTReCS> odd chop phase for even nod phase, chopos= "<<chopos<<", nodpos= "<<nodpos<<endl;
      if( freeFrm) delete _trecsbufs["ref1"];
      bufs["trecs:ref1"] = _trecsbufs["ref1"] = frame; // frame->rename("ref1");// 1
      UFInts& r1 = *(_trecsbufs["ref1"]); r1.stampTime(timeframe);
      UFInts& ar1 = *(_trecsbufs["accum(ref1)"]); ar1.stampTime(timeframe);
      ar1 += r1; 
      bufs["trecs:accum(ref1)"] = _trecsbufs["accum(ref1)"]; // = frame; // 5
    }
  }
  else { // odd nod phase
    if( chopos != 0 ) { // odd chop phase for odd nod phase is on source
      //clog<<"UFObsConfig::evalTReCS> odd chop phase for odd nod phase, chopos= "<<chopos<<", nodpos= "<<nodpos<<endl;
      if( freeFrm) delete _trecsbufs["src2"];
      bufs["trecs:src2"] = _trecsbufs["src2"] = frame;  //frame->rename("src2");// 2
      UFInts& s2 = *(_trecsbufs["src2"]); s2.stampTime(timeframe);
      UFInts& as2 = *(_trecsbufs["accum(src2)"]); as2.stampTime(timeframe);
      as2 += s2; 
      bufs["trecs:accum(src2)"] = _trecsbufs["accum(src2)"]; // = frame; // 6
    }
    else {
      //clog<<"UFObsConfig::evalTReCS> even chop phase for odd nod phase, chopos= "<<chopos<<", nodpos= "<<nodpos<<endl;
      if( freeFrm) delete _trecsbufs["ref2"];
      bufs["trecs:ref2"] = _trecsbufs["ref2"] = frame; //frame->rename("ref2");// 3
      UFInts& r2 = *(_trecsbufs["ref2"]); r2.stampTime(timeframe);
      UFInts& ar2 = *(_trecsbufs["accum(ref2)"]); ar2.stampTime(timeframe);
      ar2 += r2; 
      bufs["trecs:accum(ref2)"] = _trecsbufs["accum(ref2)"]; // = frame; // 7
    }
  }
  if( doDiff1(frmIdx) ) {
    UFInts& s1 = *(_trecsbufs["src1"]);
    UFInts& r1 = *(_trecsbufs["ref1"]);
    UFInts& d1 = *(_trecsbufs["dif1"]);
    d1.diff(s1,r1); d1.stampTime(timeframe);
    bufs["trecs:dif1"] = _trecsbufs["dif1"]; // 8
    //clog<<"UFObsConfig::evalTReCS> d1: "<<d1.name()<<" "<<d1.timeStamp()<<endl;
    if( doAccumDiff1(frmIdx) ) {
      UFInts& ad1 = *(_trecsbufs["accum(dif1)"]);
      ad1 += d1; ad1.stampTime(timeframe);
      bufs["trecs:accum(dif1)"] = _trecsbufs["accum(dif1)"]; // 10
      //clog<<"UFObsConfig::evalTReCS> ad1: "<<ad1.name()<<" "<<ad1.timeStamp()<<endl;
    }
  }
  if( doDiff2(frmIdx) ) {
    UFInts& d2 = *(_trecsbufs["dif2"]);
    UFInts& s2 = *(_trecsbufs["src2"]);
    UFInts& r2 = *(_trecsbufs["ref2"]);
    d2.diff(s2,r2); d2.stampTime(timeframe);
    bufs["trecs:dif2"] = _trecsbufs["dif2"]; // 9
    //clog<<"UFObsConfig::evalTReCS> d2: "<<d2.name()<<" "<<d2.timeStamp()<<endl;
    if( doAccumDiff2(frmIdx) ) {
      UFInts& ad2 = *(_trecsbufs["accum(dif2)"]);
      ad2 += d2; ad2.stampTime(timeframe);
      bufs["trecs:accum(dif2)"] = _trecsbufs["accum(dif2)"]; // 11
      //clog<<"UFObsConfig::evalTReCS> ad2: "<<ad2.name()<<" "<<ad2.timeStamp()<<endl;
    }
  }
  if( doSig(frmIdx) ) {
    UFInts& d1 = *(_trecsbufs["dif1"]);
    UFInts& d2 = *(_trecsbufs["dif2"]);
    UFInts& sig = *(_trecsbufs["sig"]);
    UFInts& asig = *(_trecsbufs["accum(sig)"]);
    sig.sum(d1,d2); sig.stampTime(timeframe);
    asig += sig; asig.stampTime(timeframe);
    bufs["trecs:sig"] = _trecsbufs["sig"]; // 12 
    //clog<<"UFObsConfig::evalTReCS> sig: "<<sig.name()<<" "<<sig.timeStamp()<<endl;
    bufs["trecs:accum(sig)"] = _trecsbufs["accum(sig)"]; // 13    
    //clog<<"UFObsConfig::evalTReCS> asig: "<<asig.name()<<" "<<asig.timeStamp()<<endl;
  }

  return (int) bufs.size();
}

// estimated MCE4 EDT fiber transfer time in seconds.
// about 0.04 secs for one det. readout frame (320x240 pixels)
// so # readouts in frame depends on readout mode (i.e. 1, 2, or 4 samples).

float UFObsConfig::estXferTime() {
  int nfrmPerRdout = 1; //always at least one signal det. on sample.
  string readOutMode = readoutMode();
  if( readOutMode.find("S1R1") != string::npos ||
      readOutMode.find("1S1R") != string::npos ||
      readOutMode.find("s1r1") != string::npos ||
      readOutMode.find("1s1r") != string::npos ) nfrmPerRdout = 2; //one ref sample.
  if( readOutMode.find("S1R3") != string::npos ||
      readOutMode.find("1S3R") != string::npos ||
      readOutMode.find("s1r3") != string::npos ||
      readOutMode.find("1s3r") != string::npos ) nfrmPerRdout = 4; //three ref samples.

  return 0.04 * nfrmPerRdout;
}

void UFObsConfig::beams(const string& obsmode, int& nb, int& nc) {
  nb = nc = 1; // default is Stare mode

  if( (obsmode.find("chop") != string::npos ||
       obsmode.find("Chop") != string::npos ||
       obsmode.find("CHOP") != string::npos) &&
      (obsmode.find("nod") != string::npos ||
       obsmode.find("Nod") != string::npos ||
       obsmode.find("NOD") != string::npos) ) { // Chop-Nod mode
    nb = nc = 2;
    return;
  }
  if( obsmode.find("chop") != string::npos ||
       obsmode.find("Chop") != string::npos ||
       obsmode.find("CHOP") != string::npos ) { // Chop-Only mode
    nb = 1; nc = 2;
    return;
  }
  if( obsmode.find("nod") != string::npos ||
      obsmode.find("Nod") != string::npos ||
      obsmode.find("NOD") != string::npos ) { // Nod-Only mode
    nb = 2; nc = 1;
    return;
  }

  // any other obsmode string reduces to Stare mode...
  return;
}
   
//protected:

// helper func for shallow copy ctors:
// note that if vals=0 then memory is allocated and zeroed.
void UFObsConfig::_initVals( const unsigned short* vals, int elem ) {
  _pa->_elem = elem;
  if( vals ) {
    if( _pa->_values ) delete [] (unsigned short*) _pa->_values;
    _pa->_values = (void*) vals;
  }
  else if( _pa->_values == 0 )
    _initBufSeqFlags();
}

// support shallow ctors
void UFObsConfig::_init(int nb, int cb, int ss, int ns, int cpf, const unsigned short* vals) {
  _pa->_elem = nb*cb*ss*ns;
  string nm = _initname(); // bogus name
  //clog<<"UFObsConfig::_init> _pa->_length= "<<_pa->_length<<endl;

  UFObsConfig::Config ufc;
  ufc.NodBeams = nb;
  ufc.ChopBeams = cb;
  ufc.SaveSets = ss;
  ufc.NodSets = ns;
  ufc.CoaddsPerFrm = cpf;
  ufc.ReadoutMode = "S1";

  string cs = setConfig(ufc); // this resets the name
  //clog<<"UFObsConfig::_init> _pa->_length= "<<_pa->_length<<endl;
  _initVals( vals, _pa->_elem ); // this better not reset the name!
  _pa->_length = headerLength(*_pa) + _pa->_elem*sizeof(short);
  //clog<<"UFObsConfig::_init> _pa->_length= "<<_pa->_length<<endl;
}

// helper func for deep copy ctors:
// note that if no args are given (vals=0) and no current values exist
// then memory is allocated and zeroed based on current _pa attributes.
void UFObsConfig::_initVals(unsigned short* vals, int elem) {
  if( vals ) {
    _pa->_elem = elem;
    if( _pa->_values ) {
      delete [] (unsigned short*) _pa->_values;
      _pa->_values = 0;
    }
    if( elem > 0 ) {
      _pa->_values = (void*) new unsigned short[_pa->_elem];
      ::memcpy( _pa->_values, vals, _pa->_elem*sizeof(short) );
    }
  }
  else if( _pa->_values == 0 ) {
    _initBufSeqFlags();
  }
}

// support deep ctors
void UFObsConfig::_init(int nb, int cb, int ss, int ns, int cpf, unsigned short* vals) {
  _pa->_elem = nb*cb*ss*ns;
  string nm = _initname();
  //clog<<"UFObsConfig::_init> _pa->_length= "<<_pa->_length<<endl;

  UFObsConfig::Config ufc;
  ufc.NodBeams = nb;
  ufc.ChopBeams = cb;
  ufc.SaveSets = ss;
  ufc.NodSets = ns;
  ufc.CoaddsPerFrm = cpf;
  ufc.ReadoutMode = "S1";

  string cs = setConfig(ufc); // this resets the name
  //clog<<"UFObsConfig::_init> _pa->_length= "<<_pa->_length<<endl;

  if( _pa->_values ) delete [] (unsigned short*) _pa->_values;
  _pa->_values = 0;

  _initVals( vals, _pa->_elem );
  _pa->_length = headerLength(*_pa) + _pa->_elem*sizeof(short);
  //clog<<"UFObsConfig::_init> _pa->_length= "<<_pa->_length<<endl;
}

string UFObsConfig::_initname() {
  string c, nm = name();
  size_t posL = nm.find('|');
  if( posL != 0 ) { // poorly initialized object?
    c = "||?|?|?|?|?|?||";
    rename(c+nm);
  }
  return name();
}

// get the config prefix string
string UFObsConfig::_config() const {
  string c, nm = name();
  size_t posL = nm.find_first_of("||");
  size_t posR = nm.find_last_of("||");
  if( posL == posR || posL == string::npos || posR == string::npos )  // poorly initialized object?
    c = "||?|?|?|?|?|?||";
  else
    c = nm.substr( posL, posR+1-posL );

  return c;
}

// get the name postfix string
string UFObsConfig::_name() const {
  string nm = name();
  size_t posR = nm.find_last_of("||");

  if( posR != string::npos )  nm = nm.substr(posR+1);

  return nm;
}

// Compute vector of buffer sequencing flags (the nominal values of this object):
void UFObsConfig::_initBufSeqFlags() {
  unsigned short* pBufSeq = new unsigned short[_pa->_elem];
  _pa->_values = (void*) pBufSeq;
  const UFObsConfig::Config  obscon = config();
  int totalFrames = obscon.ChopBeams * obscon.SaveSets * obscon.NodBeams * obscon.NodSets;

  if( totalFrames <= 0 ) {
    clog<<"UFObsConfig::_initBufSeqFlags> totalFrames = "<<totalFrames
	<<", something is very wrong (_pa->_elem="<<_pa->_elem<<") bailing out!"<<endl;
    return;
  }

  if( totalFrames > _pa->_elem ) {
    clog<<"UFObsConfig::_initBufSeqFlags> totalFrames ("<<totalFrames
	<<") > _pa->_elem ("<<_pa->_elem<<"), inconsistency, bailing out!"<<endl;
    return;
  }

  if( totalFrames < _pa->_elem )
    clog<<"UFObsConfig::_initBufSeqFlags> totalFrames ("<<totalFrames
	<<") < _pa->_elem ("<<_pa->_elem<<"), inconsistency, but proceeding..."<<endl;

  int totFrameNodBeam = obscon.ChopBeams * obscon.SaveSets;
  int totFrameNodSet = obscon.NodBeams * totFrameNodBeam;

  for( int FrameIndex=0; FrameIndex < totalFrames; FrameIndex++ ) {
    int FrameCount = 1 + FrameIndex;
    int SaveSet    = 1 + ( (FrameIndex/obscon.ChopBeams) % obscon.SaveSets );
    unsigned short NodBeam = ( FrameIndex/totFrameNodBeam ) % obscon.NodBeams;
    unsigned short ChpBeam = FrameIndex % obscon.ChopBeams;

    // Initialize buffer sequence vector with buffer index:
    pBufSeq[FrameIndex] = (unsigned short)(( NodBeam * obscon.ChopBeams )
					     + ( FrameIndex % obscon.ChopBeams ));
    // Zero bit registers:
    unsigned short BitClr	= 0;
    unsigned short BitD1	= 0;
    unsigned short BitD2	= 0;
    unsigned short BitD1a	= 0;
    unsigned short BitD2a	= 0;
    unsigned short BitSg	= 0;
    unsigned short BitNB	= 0;
    unsigned short BitCB	= 0;
    unsigned short BitNT	= 0;
		
    if (SaveSet == 1) BitClr = BMC_CLR;

    // Compute Dif1 or Dif2? 
    if ( (obscon.ChopBeams == 2) && ((FrameCount % 2) == 0) ) {
      if (NodBeam == 0) BitD1 = BMC_D1;
      if (NodBeam == 1) BitD2 = BMC_D2;
    }

    // Compute D1A ?
    if (obscon.ChopBeams == 2) {
      if (BitD1 != 0) BitD1a = BMC_D1A;
    }
    else if (obscon.NodBeams == 2) {
      if (FrameCount % (2*obscon.SaveSets) == 0) BitD1a = BMC_D1A;
    }

    if (obscon.ChopBeams == 2) { // Compute D2A?
      if (BitD2 != 0) BitD2a = BMC_D2A;
    }

    if (obscon.NodBeams == 2) { // Compute SG?
      if ((FrameCount % totFrameNodSet) == 0) BitSg = BMC_SG;
    }

    BitNB = NodBeam*BMC_NOD_BEAM; // Indicate nod beam index

    BitCB = ChpBeam*BMC_CHOP_BEAM; // Indicate chop beam index

    if (obscon.NodBeams == 2) {  // Set nod toggle?
      if ((FrameCount % totFrameNodBeam) == 0) BitNT = BMC_NOD_TOGGLE;
    }

    pBufSeq[FrameIndex] += (BitClr + BitD1 + BitD2 + BitD1a + BitD2a + BitSg + BitNB + BitCB + BitNT);

  } // end loop over FrameIndex
} // end _initBufSeqFlags

//public:

//ctor which deep copies vals,
// (because re-casted vals is not declared as const it calls the deep copy _initVals):
UFObsConfig::UFObsConfig( const string& name, const char* vals, int elem, bool shared ) : UFTimeStamp() {
  _paInit(_ObsConfig_, name);
  _shared = shared;
  _initVals( (unsigned short*)vals, elem );
  _currentTime();
}

// convenience functions
UFObsConfig::Config UFObsConfig::config() const {
  string c = _config();
  UFObsConfig::Config ufc;
  size_t posL = 2 + c.find('|');
  size_t posR = c.find('|', posL);
  string NodBeams = c.substr(posL, posR-posL);
  posL = 1 + c.find('|', posR); posR = c.find('|', posL);
  string ChopBeams = c.substr(posL, posR-posL);
  posL = 1 + c.find('|', posR); posR = c.find('|', posL);
  string SaveSets = c.substr(posL, posR-posL);
  posL = 1 + c.find('|', posR); posR = c.find('|', posL);
  string NodSets = c.substr(posL, posR-posL);
  posL = 1 + c.find('|', posR); posR = c.find('|', posL);
  string CoaddsPerFrm = c.substr(posL, posR-posL);
  posL = 1 + c.find('|', posR); posR = c.find('|', posL);
  ufc.ReadoutMode = c.substr(posL, posR-posL);

  ufc.NodBeams = atoi(NodBeams.c_str());
  ufc.ChopBeams = atoi(ChopBeams.c_str());
  ufc.SaveSets = atoi(SaveSets.c_str());
  ufc.NodSets = atoi(NodSets.c_str());
  ufc.CoaddsPerFrm = atoi(CoaddsPerFrm.c_str());

  return ufc;
}

int UFObsConfig::nodBeams() const {
  const UFObsConfig::Config ufc = config();
  return ufc.NodBeams;
}

int UFObsConfig::chopBeams() const {
  const UFObsConfig::Config ufc = config();
  return ufc.ChopBeams;
}

int UFObsConfig::saveSets() const {
  const UFObsConfig::Config ufc = config();
  return ufc.SaveSets;
}

int UFObsConfig::nodSets() const {
  const UFObsConfig::Config ufc = config();
  return ufc.NodSets;
}

int UFObsConfig::coaddsPerFrm() const {
  const UFObsConfig::Config ufc = config();
  return ufc.CoaddsPerFrm;
}

string UFObsConfig::readoutMode() const {
  const UFObsConfig::Config ufc = config();
  return ufc.ReadoutMode;
}

string UFObsConfig::dataLabel() const {
  string nm = name();
  size_t posR = nm.find_last_of("||");
  if( posR != string::npos )
    return nm.substr( ++posR );
  else
    return("?");
}

int UFObsConfig::reLabel(const string& newLabel) {
  rename( _config() + newLabel );
  return strlen(cname());
}

string UFObsConfig::setConfig(const UFObsConfig::Config& c) {  
  setNodBeams(c.NodBeams);
  setChopBeams(c.ChopBeams);
  setSaveSets(c.SaveSets);
  setNodSets(c.NodSets);
  setCoaddsPerFrm(c.CoaddsPerFrm);
  return name();
}

string UFObsConfig::setConfig(const string& cs) {
  // cs must be of the form: "||[0-9]*|[0-9]*|[0-9]*|[0-9]*|[0-9]*|string||"
  // so count # of | (>=9)  for simple verification
  int cnt, pos = cs.find('|');
  if( pos == (int) string::npos )
    return "";

  for( cnt = 1; pos != (int)string::npos; ++cnt)
    pos = cs.find('|', ++pos);

  string nm = _name();
  string c = "||?|?|?|?|?|?||";
  if( cnt < 9 )
    rename(c+nm);
  else
    rename(cs+nm);

  return name();
}  
  
int UFObsConfig::setNodBeams(int nb) {
  string c = _config();
  size_t pos = 2 + c.find("||");
  pos = c.find('|', pos);
  // discard the current NodBeams substr
  strstream s;
  s << "||" << nb << c.substr(pos) << _name() <<ends;
  rename(s.str()); delete s.str();

  return strlen(cname());
}

int UFObsConfig::setChopBeams(int cb) {
  string c = _config();
  size_t pos = 2 + c.find("||");
  pos = 1 + c.find('|', pos);
  // first preserve current ChopBeams substr
  strstream s;
  s << c.substr(0, pos) << cb; // reset the current ChopBeams substr
  // + remainder
  pos = c.find('|', pos);
  s << c.substr(pos) << _name() <<ends;
  rename(s.str()); delete s.str();

  return strlen(cname());
}

int UFObsConfig::setSaveSets(int ss) {
  string c = _config();
  // discard the current SaveSets substr
  // first preserve current NodBeams & ChopBeams substr
  // NodBeams
  size_t pos = 2 + c.find("||");
  pos = 1 + c.find('|', pos);
  // ChopBeams 
  pos = 1 + c.find('|', pos);
  string tmp = c.substr(0, pos);
  strstream s;
  s << tmp << ss; // reset the current SaveSets substr
  // + remainder
  pos = c.find('|', pos);
  tmp = c.substr(pos);
  s << tmp << _name() <<ends;

  rename(s.str()); delete s.str();
  return strlen(cname());
}

int UFObsConfig::setNodSets(int ns) {
  string c = _config();
  // discard the current NodSets substr
  // first preserve current NodBeams & ChopBeams substr
  // NodBeams
  size_t pos = 2 + c.find("||");
  pos = 1 + c.find('|', pos);
  // ChopBeams 
  pos = 1 + c.find('|', pos);
  // SaveSets
  pos = 1 + c.find('|', pos);
  string tmp = c.substr(0,pos);
  strstream s;
  s << tmp << ns; // reset the current NodSets substr
  // + remainder
  pos = c.find('|', pos);
  tmp = c.substr(pos);
  s << tmp << _name() <<ends;

  rename(s.str()); delete s.str();
  return strlen(cname());
}

int UFObsConfig::setCoaddsPerFrm(int cpf) {
  string c = _config();
  // NodBeams
  size_t pos = 2 + c.find("||");
  pos = 1 + c.find('|', pos);
  // ChopBeams 
  pos = 1 + c.find('|', pos);
  // SaveSets
  pos = 1 + c.find('|', pos);
  // NodSets
  pos = 1 + c.find('|', pos);
  string tmp = c.substr(0,pos);
  strstream s;
  s << tmp << cpf; // reset CoaddsPerFrm
  // + remainder
  pos = c.find('|', pos);
  tmp = c.substr(pos);
  s << tmp << _name() <<ends;

  rename(s.str()); delete s.str();
  return strlen(cname());
}

int UFObsConfig::setReadoutMode(const string& rdout) {
  string c = _config();
  // skip NodBeams, ChopBeams, SaveSets, NodSets, and CoaddsPerFrm
  size_t pos = 2 + c.find("||");
  for( int i=0; i<5; i++ ) pos = 1 + c.find('|', pos);
  string tmp = c.substr(0,pos);

  // set ReadoutMode + remainder
  rename( tmp + rdout + "||" + _name() );
  return strlen(cname());
}

// evaluate/set bits:
int UFObsConfig::buffId(int frmIdx) const {
  unsigned short frmseq = *((unsigned short*) valData(frmIdx));
  //bitset< sizeof(unsigned short) > mask(frmseq);
  int bufId = 1 + ((unsigned short)0x03 & frmseq);
  return bufId;
}

bool UFObsConfig::clearBuff(int frmIdx) const {
  unsigned short frmseq = *((unsigned short*) valData(frmIdx));
  //bitset< sizeof(unsigned short) > mask(*frmseq);
  bool bit = (unsigned short)0x04 & frmseq;
  return bit;
}

bool UFObsConfig::doDiff1(int frmIdx) const {
  unsigned short frmseq = *((unsigned short*) valData(frmIdx));
  //bitset< sizeof(unsigned short) > mask(*frmseq);
  bool bit = (unsigned short)0x08 & frmseq;
  return bit;
}

bool UFObsConfig::doDiff2(int frmIdx) const {
  unsigned short frmseq = *((unsigned short*) valData(frmIdx));
  //bitset< sizeof(unsigned short) > mask(*frmseq);
  bool bit = (unsigned short)0x10 & frmseq;
  return bit;
}

bool UFObsConfig::doAccumDiff1(int frmIdx) const {
  unsigned short frmseq = *((unsigned short*) valData(frmIdx));
  //bitset< sizeof(unsigned short) > mask(*frmseq);
  bool bit = (unsigned short)0x20 & frmseq;
  return bit;
}

bool UFObsConfig::doAccumDiff2(int frmIdx) const {
  unsigned short frmseq = *((unsigned short*) valData(frmIdx));
  //bitset< sizeof(unsigned short) > mask(*frmseq);
  bool bit = (unsigned short)0x40 & frmseq;
  return bit;
}

bool UFObsConfig::doSig(int frmIdx) const {
  unsigned short frmseq = *((unsigned short*) valData(frmIdx));
  //bitset< sizeof(unsigned short) > mask(*frmseq);
  bool bit = (unsigned short)0x80 & frmseq;
  return bit;
}

int UFObsConfig::nodPosition(int frmIdx) const {
  unsigned short frmseq = *((unsigned short*) valData(frmIdx));
  //bitset< sizeof(unsigned short) > mask(*frmseq);
  bool bit = (unsigned short)0x100 & frmseq;
  return (int)bit;
}

int UFObsConfig::chopPosition(int frmIdx) const {
  unsigned short frmseq = *((unsigned short*) valData(frmIdx));
  //bitset< sizeof(unsigned short) > mask(*frmseq);
  bool bit = (unsigned short)0x200 & frmseq;
  return (int)bit;
}

int UFObsConfig::setBuffId(int id, int frmIdx) {
  unsigned short* frmseq = (unsigned short*) valData(frmIdx);
  //bitset< sizeof(unsigned short) > mask(*frmseq);
  unsigned short idb = id - 1; // assumes id > 0 !
  unsigned short bufId = (unsigned short)0x03 & idb;
  *frmseq = bufId & *frmseq;
  return id;
}

bool UFObsConfig::setClear(bool s, int frmIdx) {
  unsigned short* frmseq = (unsigned short*) valData(frmIdx);
  //bitset< sizeof(unsigned short) > mask(*frmseq);
  unsigned short sb0 = (unsigned short) (0x01|0x02|0x08|0x10|0x20|0x40|0x80|0x100|0x200);
  unsigned short sb1 = (unsigned short) 0x04;

  if( s )
    *frmseq = sb1 | *frmseq;
  else
    *frmseq = sb0 & *frmseq;

  return true;
}

bool UFObsConfig::setDiff1(bool s, int frmIdx) {
  unsigned short* frmseq = (unsigned short*) valData(frmIdx);
  //bitset< sizeof(unsigned short) > mask(*frmseq);
  unsigned short sb0 = (unsigned short) (0x01|0x02|0x04|0x10|0x20|0x40|0x80|0x100|0x200);
  unsigned short sb1 = (unsigned short) 0x08;

  if( s )
    *frmseq = sb1 | *frmseq;
  else
    *frmseq = sb0 & *frmseq;

  return true;
}

bool UFObsConfig::setDiff2(bool s, int frmIdx) {
  unsigned short* frmseq = (unsigned short*) valData(frmIdx);
  //bitset< sizeof(unsigned short) > mask(*frmseq);
  unsigned short sb0 = (unsigned short) (0x01|0x02|0x04|0x08|0x20|0x40|0x80|0x100|0x200);
  unsigned short sb1 = (unsigned short) 0x10;

  if( s )
    *frmseq = sb1 | *frmseq;
  else
    *frmseq = sb0 & *frmseq;

  return true;
}

bool UFObsConfig::setAccumDiff1(bool s, int frmIdx) {
  unsigned short* frmseq = (unsigned short*) valData(frmIdx);
  //bitset< sizeof(unsigned short) > mask(*frmseq);
  unsigned short sb0 = (unsigned short) (0x01|0x02|0x04|0x08|0x10|0x40|0x80|0x100|0x200);
  unsigned short sb1 = (unsigned short) 0x20;

  if( s )
    *frmseq = sb1 | *frmseq;
  else
    *frmseq = sb0 & *frmseq;

  return true;
}

bool UFObsConfig::setAccumDiff2(bool s, int frmIdx) {
  unsigned short* frmseq = (unsigned short*) valData(frmIdx);
  //bitset< sizeof(unsigned short) > mask(*frmseq);
  unsigned short sb0 = (unsigned short) (0x01|0x02|0x04|0x08|0x10|0x20|0x80|0x100|0x200);
  unsigned short sb1 = (unsigned short) 0x40;

  if( s )
    *frmseq = sb1 | *frmseq;
  else
    *frmseq = sb0 & *frmseq;

  return true;
}

bool UFObsConfig::setSig(bool s, int frmIdx) {
  unsigned short* frmseq = (unsigned short*) valData(frmIdx);
  //bitset< sizeof(unsigned short) > mask(*frmseq);
  unsigned short sb0 = (unsigned short) (0x01|0x02|0x04|0x08|0x10|0x20|0x40|0x100|0x200);
  unsigned short sb1 = (unsigned short) 0x80;

  if( s )
    *frmseq = sb1 | *frmseq;
  else
    *frmseq = sb0 & *frmseq;

  return true;
}

int UFObsConfig::setNodPosition(int pos, int frmIdx) const {
  unsigned short* frmseq = (unsigned short*) valData(frmIdx);
  //bitset< sizeof(unsigned short) > mask(*frmseq);
  unsigned short sb0 = (unsigned short) (0x01|0x02|0x04|0x08|0x10|0x20|0x40|0x80|0x200);
  unsigned short sb1 = (unsigned short) 0x100;

  if( pos )
    *frmseq = sb1 | *frmseq;
  else
    *frmseq = sb0 & *frmseq;

  return pos;
}

int UFObsConfig::setChopPosition(int pos, int frmIdx) const {
  unsigned short* frmseq = (unsigned short*) valData(frmIdx);
  //bitset< sizeof(unsigned short) > mask(*frmseq);
  unsigned short sb0 = (unsigned short) (0x01|0x02|0x04|0x08|0x10|0x20|0x40|0x80|0x100);
  unsigned short sb1 = (unsigned short) 0x200;

  if( pos )
    *frmseq = sb1 | *frmseq;
  else
    *frmseq = sb0 & *frmseq;

  return pos;
}

// additional static function supports configuration info i/o
// between client and server: not intrinsically part of the UFProtocol
// return <= 0 on failure, num. bytes i/o on success
// for use in client
int UFObsConfig::reqConfig(UFSocket& soc, UFObsConfig& c) { // resets c
  // negative duration is used to ID the transaction as a request
  int nb = c.sendAsReqTo(soc);
  if( nb <= 0 ) { 
    clog<<"UFObsConfig::reqConfig> ? unable to send request to server."<<endl;
    return nb;
  }
  return c.recvFrom(soc);
}

// for use in server
int UFObsConfig::recvConfig(UFSocket& soc, UFObsConfig& c) { // resets c
  return c.recvFrom(soc);
}
    
#endif // __UFObsConfig_cc__
    

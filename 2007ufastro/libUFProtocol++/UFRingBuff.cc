#if !defined(__UFRingBuff_cc__)
#define __UFRingBuff_cc__ "$Name:  $ $Id: UFRingBuff.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFRingBuff_cc__;
#include "UFRingBuff.h"
__UFRingBuff_H__(__UFRingBuff_cc);

#include "UFProtocol.h"
#include "UFBytes.h"
#include "UFShorts.h"
#include "UFInts.h"
#include "UFFloats.h"

// protected:

void UFRingBuff::_init(UFProtocol* external, int bufsize, int numbuf) {
  if( bufsize ) 
    _bufsize =  bufsize;

  if( numbuf > size() ) 
    resize(numbuf);
 
  clog<<"UFRingBuff::_init> using external "<<size()<<" buffers of size: "
      << _bufsize<<endl;

  for( _activeIdx = 0; _activeIdx < (int) size(); ++_activeIdx ) {
    _activeBuf = &external[_activeIdx*bufsize];
    (*this)[_activeIdx] = _activeBuf;
  }
  _activeIdx = 0;
  _activeBuf = (*this)[_activeIdx];
}

void UFRingBuff::_init(UFProtocol** external, int bufsize, int numbuf) {
  if( bufsize ) 
	_bufsize =  bufsize;

  if( numbuf > size() ) 
	resize(numbuf);
 
  clog<<"UFRingBuff::_init> using external "<<size()<<" buffers of size: "
	<< _bufsize<<endl;

  for( _activeIdx = 0; _activeIdx <  size();
	++_activeIdx ) {
    _activeBuf = external[_activeIdx];
    (*this)[_activeIdx] = _activeBuf;
  }
  _activeIdx = 0;
  _activeBuf = (*this)[_activeIdx];
}

// public:

UFRingBuff::~UFRingBuff() { 
  if( _externalData ) {
    return;
  }
  clear();
}

UFRingBuff::UFRingBuff(const string& name) : vector< UFProtocol* >(0), _name(name), _externalData(false),
                                             _bufsize(0), _activeIdx(-1), _activeBuf(0) {}

UFRingBuff::UFRingBuff(UFProtocol* external,
		       int bufsize,
		       int numbuf,
		       const string& name) : vector< UFProtocol* >(numbuf), _name(name), _externalData(true),
                                             _bufsize(bufsize), _activeIdx(-1), _activeBuf(0) {
  if( external )
    _init(external, bufsize, numbuf);
}

UFRingBuff::UFRingBuff(UFProtocol** external,
		       int bufsize,
		       int numbuf,
		       const string&  name) : vector< UFProtocol* >(numbuf), _name(name), _externalData(true),
                                     _bufsize(bufsize), _activeIdx(-1), _activeBuf(0) {
  if( external ) 
    _init(external, bufsize, numbuf);
}

// default to 2 buffers off the Heap:
int UFRingBuff::init(vector< string >& names, int elem, int type) {
  if( names.size() == 0 ) {
    names.push_back("#1"); names.push_back("#2");
  }
  int bufcnt = (int)names.size();
  UFProtocol** ufp = new UFProtocol*[bufcnt];

  switch( type ) {
  case UFProtocol::_Bytes_:
    for( int i = 0; i < bufcnt; i++ ) {
      const char* vals = (const char*) new char[elem];
      ufp[i] = new UFBytes(names[i], vals, elem); // shallow ctor sets _values ptr to vals
    }
    break;
  case UFProtocol::_Shorts_:
    for( int i = 0; i < bufcnt; i++ ) {
      const short* vals = (const short*) new short[elem];
      ufp[i] = new UFShorts(names[i], vals, elem); // shallow ctor sets _values ptr to vals
    }
    break;
  case UFProtocol::_Floats_:
    for( int i = 0; i < bufcnt; i++ ) {
      const float* vals = (const float*) new float[elem];
      ufp[i] = new UFFloats(names[i], vals, elem); // shallow ctor sets _values ptr to vals
    }
    break;
  case UFProtocol::_Ints_: // ints are the default
  default:
    for( int i = 0; i < bufcnt; i++ ) {
      const int* vals = (const int*) new int[elem];
      ufp[i] = new UFInts(names[i], vals, elem); // shallow ctor sets _values ptr to vals
    }
    break;
  }

  _init(ufp, elem, names.size());
  return bufcnt;
}

unsigned char** UFRingBuff::bufValPtrs() { // for use by libedt

  static vector<unsigned char*> buffvec;

  static unsigned char** buffarray=0; // retval for libedt

  if( (int) buffvec.size() != size() ) {
    buffvec.resize(size());
    buffarray = new unsigned char*[size()];
  }

  for( int i = 0 ; i < size() ; ++i ) {
     UFProtocol* ufp = (*this)[i];  
     buffvec[i] = buffarray[i] = (unsigned char*) ufp->valData();
  }

  return buffarray;
}

// virtuals
//int UFRingBuff::activeIndex(bool& locked) { locked = false; return _activeIdx; }

int UFRingBuff::nextActiveBuf() {
  bool locked;
  int nextIdx = 1 + activeIndex(locked);
  if( nextIdx >= size() ) 
    nextIdx = 0;

  _activeIdx = nextIdx;
  _activeBuf = (*this)[_activeIdx];
  
  return nextIdx;
}


UFProtocol* UFRingBuff::prevActiveBuf(int& index) {
  vector< UFProtocol* > v =  static_cast< vector< UFProtocol* > >(*this);
  index = _activeIdx - 1;
  if( index < 0 ) index = 0;
  UFProtocol* p = v[index];
  return p;
}

UFProtocol* UFRingBuff::activeBuf(int& index) {
  vector< UFProtocol* > v =  static_cast< vector< UFProtocol* > >(*this);
  index = _activeIdx;
  UFProtocol* p = v[index];
  return p;
}

UFProtocol* UFRingBuff::activeBuf(bool& locked) {
  vector< UFProtocol* > v =  static_cast< vector< UFProtocol* > >(*this);
  UFProtocol* p = v[activeIndex(locked)];
  return p;
}

UFProtocol* UFRingBuff::activeBuf(bool& locked, int& index) {
  index = activeIndex(locked);
  vector< UFProtocol* > v =  static_cast< vector< UFProtocol* > >(*this);
  UFProtocol* p = v[index];
  return p;
}

UFProtocol* UFRingBuff::bufAt(int bufIdx) const {
  vector< UFProtocol* > v =  static_cast< vector< UFProtocol* > >(*this);
  if( bufIdx < 0 ) bufIdx = 0;
  if( bufIdx >= (int) size() ) bufIdx = 0;
  UFProtocol* p= v[bufIdx];
  return p;
}

string UFRingBuff::activeBufName(int& index) {
  index = -1;
  UFProtocol* p = activeBuf(index);
  if(p)
    return p->name(); 

  return string("No Active Buffer!");
}

string UFRingBuff::setBufName(const string& name) {
  return _activeBuf->rename(name);
}

int UFRingBuff::setAllBufNames(const vector<string>& names) {
  vector< UFProtocol* > v =  static_cast< vector< UFProtocol* > >(*this);
  if( names.size() != v.size() ) {
    clog<<"UFRingBuff::setAllBufNames> ? vec. size mismatch."<<endl;
    return 0;
  }
  for( int i = 0; i < (int) v.size(); i++ ) {
    UFProtocol* p = v[i];
    p->rename(names[i]);
  }

  return (int) v.size();
}


// the writes are the only functions that set the activeIdx
// unless a specific index is supplied, this writes then increments the index
int UFRingBuff::write(const UFProtocol* data, int index) {
  // lockActiveBuff also sets _activeIdx to index if the index supplied is >= 0
  _activeBuf = lockActiveBuf(index); // this blocks until the active index mutex is released
  //_activeBuf->deepCopy(*data);
  _activeBuf->shallowCopy(*data);

  return nextActiveBuf();
}

int UFRingBuff::read(UFProtocol* data, int index) {
  if( index < 0 )
    index = _activeIdx;
  else if( index >= size() )
    index = 0;

  data->shallowCopy(*(*this)[index]);

  return index;
}

#endif // __UFRingBuff_cc__

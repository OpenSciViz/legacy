#if !defined(__UFRingBuff_h__)
#define __UFRingBuff_h__ "$Name:  $ $Id: UFRingBuff.h,v 0.2 2004/01/28 20:30:24 drashkin beta $"
#define __UFRingBuff_H__(arg) const char arg##RingBuff_h__rcsId[] = __UFRingBuff_h__;
 
#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx) 
#include "vxWorks.h"
#include "sockLib.h"
#include "taskLib.h"
#include "ioLib.h"
#include "fioLib.h"
#endif // vx

// c++ incs
#include "iostream"
#include "strstream"
#include "cstdio"
#include "cmath"
#include "vector"
#include "string"

#include "UFProtocol.h"
#include "UFBytes.h"
#include "UFShorts.h"
#include "UFInts.h"
#include "UFFloats.h"



const int __128BytePixelBuff_ = 128*128*sizeof(char);


const int __128IntPixelBuff_ = 128*128*sizeof(int);



const int __256BytePixelBuff_ = 256*256*sizeof(char);


const int __256IntPixelBuff_ = 256*256*sizeof(int);



const int __320x240BytePixelBuff_ = 320*240*sizeof(char);


const int __320x240IntPixelBuff_ = 320*240*sizeof(int);



const int __1024BytePixelBuff_ = 1024*1024*sizeof(char);


const int __1024IntPixelBuff_ = 1024*1024*sizeof(int);



const int __2048BytePixelBuff_ = 2048*2048*sizeof(char);


const int __2048IntPixelBuff_ = 2048*2048*sizeof(int);

const int __AiresBytePixelBuff_ = __128BytePixelBuff_;
const int __AiresIntPixelBuff_ = __128IntPixelBuff_;

const int __TrecsBytePixelBuff_ = __256BytePixelBuff_;
const int __TrecsIntPixelBuff_ = __256IntPixelBuff_;

const int __FlamingosBytePixelBuff_ = __2048BytePixelBuff_;
const int __FlamingosIntPixelBuff_ = __2048IntPixelBuff_;

// this is abstract due to the pure virtual methods below
// whose implementations depend on the nature of the sub-class:
// shared-memory rings use semaphores, heap rings use mutexes.
class UFRingBuff : virtual public vector< UFProtocol* > {
protected:
  string _name;
  bool _externalData;
  int _bufsize;

  // current write, or if no current write, next write buffer
  int _activeIdx; 
  UFProtocol* _activeBuf;


  
  virtual void _init(UFProtocol* external, int bufsize, int numbuf = 0);

  
  virtual void _init(UFProtocol** external, int bufsize, int numbuf = 0);

public:

  
  virtual ~UFRingBuff();

  
  UFRingBuff(const string& name= "Anonymous");

  
  UFRingBuff(UFProtocol* external, int bufsize, int numbuf, const string& name= "Anonymous");

  
  UFRingBuff(UFProtocol** external, int bufsize, int numbuf, const string& name= "Anonymous");


  
  inline int size() const { 
    const vector< UFProtocol* > *v = static_cast< const vector< UFProtocol* >* >(this);
    return (int) v->size();
  }

  // mce4 always write 32 bit pixels, so use Ints as the default Protocol type:

  
  virtual int init(vector< string >& names, int elem, int type= UFProtocol::_Ints_);

  // this is virtual because the shared memory ring buff 
  // will need to access the index via shared memory.
  // the active index points to the most recent dma buffer.
  // the locked boolean should be true only while the buffer
  // is being written, not while it is being read.
  // these are abstract in the base class because it does not
  // provide adefault locking mechanism (mutex or semaphore):

  
  virtual int activeIndex(bool& locked) = 0; // set is/not locked boolean

  
  virtual UFProtocol* lockActiveBuf(int& index) = 0; // set index to active index

  
  virtual int unlockActiveBuf() = 0; // return active index
  // convenience func. returns after buffer dma completes

  
  virtual UFProtocol* waitForNewData(int& index) = 0; // set index to active index
  // convenience func. returns buffer used just before current active buffer

  
  virtual UFProtocol* prevActiveBuf(int& index); // set index to prev. val.

  // sets the next buffer for active status

  
  virtual int nextActiveBuf();

  
  virtual UFProtocol* activeBuf(int& index);

  
  virtual UFProtocol* activeBuf(bool& locked);

  
  virtual UFProtocol* activeBuf(bool& locked, int& index);

  
  virtual UFProtocol* bufAt(int bufIdx) const;


  
  inline void setRingName(const string& name) { _name = name; }

  
  inline string getRingName() { return _name; }


  
  string activeBufName(int& index);

  
  string setBufName(const string& name);

  
  int setAllBufNames(const vector<string>& names);

  // for use by libedt, returns all buffer pointers

  
  unsigned char** bufValPtrs();

  // r/w active buf & r/w buf[index]
  // note that write functions set _activeIndex, reads do not.

  
  virtual int write(const UFProtocol* data, int index= -1);

  
  virtual int read(UFProtocol* data, int index=-1);
};

#endif // __UFRingBuff_h__
/**
 *
 */
/**
 *
 */
/**
 *
 */
/**
 *
 */
/**
 *
 */
/**
 *
 */
/**
 *
 */
/**
 *
 */
/**
 *
 */
/**
 *
 */
  /**
   *
   *@param external TBD
   *@param bufsize TBD
   *@param numbuf TBD
   */
  /**
   *
   *@param external TBD
   *@param bufsize TBD
   *@param numbuf TBD
   */
  /**
   *
   */
  /**
   *
   *@param name TBD
   */
  /**
   *
   *@param external TBD
   *@param bufsize TBD
   *@param numbuf TBD
   *@param name TBD
   */
  /**
   *
   *@param external TBD
   *@param bufsize TBD
   *@param numbuf TBD
   *@param name TBD
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@param names TBD
   *@param elem TBD
   *@param type TBD
   *@return TBD
   */
  /**
   *
   *@param locked TBD
   *@return TBD
   */
  /**
   *
   *@param index TBD
   *@return TBD
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@param index TBD
   *@return TBD
   */
  /**
   *
   *@param index TBD
   *@return TBD
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@param index TBD
   *@return TBD
   */
  /**
   *
   *@param locked TBD
   *@return TBD
   */
  /**
   *
   *@param locked TBD
   *@param index TBD
   *@return TBD
   */
  /**
   *
   *@param bufIdx TBD
   *@return TBD
   */
  /**
   *
   *@param name TBD
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@param index TBD
   *@return TBD
   */
  /**
   *
   *@param name TBD
   *@return TBD
   */
  /**
   *
   *@param names TBD
   *@return TBD
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@param data TBD
   *@param index TBD
   *@return TBD
   */
  /**
   *
   *@param data TBD
   *@param index TBD
   *@return TBD
   */

#if !defined(__UFTimeStamp_cc__)
#define __UFTimeStamp_cc__ "$Name:  $ $Id: UFTimeStamp.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFTimeStamp_cc__;
#include "UFTimeStamp.h"
__UFTimeStamp_H__(__UFTimeStamp_cc);

#include "cstdio"
#include "sys/types.h"
#include "sys/stat.h"

#include "UFRuntime.h"
__UFRuntime_H__(_UFTimeStamp_cc);

// ctor for new create signature:
UFTimeStamp::UFTimeStamp(const UFProtocolAttributes& pa) : UFProtocol() {
  _paInit(pa);
  _pa->_type = _TimeStamp_;
}
  
// ctor for shared mem. apps.
UFTimeStamp::UFTimeStamp(bool shared, int length) : UFProtocol() {
  _paInit(_TimeStamp_, "TimeStamp", length);
  _shared = shared; // reset from default
  _currentTime();
}

// ctor initializes to current time
UFTimeStamp::UFTimeStamp(const char* name, bool shared, int length) : UFProtocol() {
  _paInit(_TimeStamp_, name, length);
  _shared = shared; // reset from default
  _currentTime(); // reset the timestamp to curent system time
}

// ctor initializes to current time
UFTimeStamp::UFTimeStamp(const string& name, bool shared, int length) : UFProtocol() {
  _paInit(_TimeStamp_, name, length);
  _shared = shared; // reset from default
  _currentTime(); // reset the timestamp to curent system time
}

// use utc default time zone.
void UFTimeStamp::_currentTime() {
  string t = UFRuntime::currentTime();
  strncpy(_pa->_timestamp, t.data(), min(sizeof(_pa->_timestamp)-1,t.size()));
}

// these must override base class:
// since the time stamp has no values, this can also be shallow
void UFTimeStamp::deepCopy(const UFProtocol& rhs) { shallowCopy(rhs); }

// write msg out to file descriptor, returns 0 on failure, num. bytes output on success
// read/restore from file descriptor (no-op)
int UFTimeStamp::writeTo(int fd) const {
  if( _pa != 0 )
    return writeHeader(fd, *_pa);

  return 0;
}
int UFTimeStamp::writeValues(int fd) const { return 0; }

// read/restore from file descriptor (no-op)
int UFTimeStamp::readFrom(int fd) {
  if( _pa != 0 )
    return readHeader(fd, *_pa);

  return 0;
}
// read/restore from file descriptor (no-op)
int UFTimeStamp::readValues(int fd) { return 0;}

// write msg out to socket, returns 0 on failure, num. bytes on success
int UFTimeStamp::sendTo(UFSocket& soc) const {
  if( _pa == 0 )
    return 0;

  if( UFProtocol::_verbose ) 
    clog<<"UFTimeStamp::sendTo> name= "<<name()<<endl;

  return sendHeader(soc, *_pa);
}

int UFTimeStamp::sendValues(UFSocket& soc) const { return 0; }

 // read/restore from socket,
// this must assume that msg. length and type have already been read from socket!
int UFTimeStamp::recvFrom(UFSocket& soc) {
  if( _pa != 0 )
    return recvHeader(soc, *_pa);

  return 0;
}

int UFTimeStamp::recvValues(UFSocket& soc) { return 0; }

#endif // __UFTimeStamp_cc__

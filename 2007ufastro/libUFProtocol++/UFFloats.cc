#if !defined(__UFFloats_cc__)
#define __UFFloats_cc__ "$Name:  $ $Id: UFFloats.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFFloats_cc__;

#include "UFFloats.h"
__UFFloats_H__(__UFFloats_cc);

#include "cstdio"
#include "sys/types.h"
#include "sys/stat.h"

// public ctors
// for new create signature
UFFloats::UFFloats(const UFProtocolAttributes& pa) : UFTimeStamp() {
  _paInit(pa);
  if( _pa->_type != _Floats_ ) {
    clog<<"UFFloats::> ? type mismatch, _pa->_type = "<< _pa->_type << endl;
    _pa->_type=_Floats_;
  }
  if( _pa->_elem > 0 && !_shared && !_shallow ) _pa->_values = (void*) new float[_pa->_elem];
}

UFFloats::UFFloats(bool shared, int length) : UFTimeStamp() {
  _paInit(_Floats_, "UFFloats", length);
  _shared = shared; // reset from default
  if( _pa->_elem > 0 && !_shared && !_shallow ) _pa->_values = (void*) new float[_pa->_elem];
  _currentTime();
}

UFFloats::UFFloats(const string& name,
	           bool shared,
	           int length) : UFTimeStamp() {
  _paInit(_Floats_, name, length);
  _shared = shared; // reset from default
  if( _pa->_elem > 0 && !_shared && !_shallow ) _pa->_values = (void*) new float[_pa->_elem];
  _currentTime();
}

// this (very odd & very special!) ctor supports shared memory applications
// UFProtocolAttributes* is non-const, indicating we actually want to init its contents here:
UFFloats::UFFloats(const string& name, UFProtocolAttributes* pa,
		   int elem, bool shared) : UFTimeStamp() {
  if( shared ) { // presumable pa actually point to uninitialized shared mem. seg.
    _paInit(_Floats_, name);
    _shared = shared;
    // be sure shared memory _values points to shared memory!
    // insure shared mem. is initialized
    *pa = *_pa;
    // and reset for shared mem.
    pa->_length = minLength() + elem*sizeof(char);
    pa->_elem = elem;
    pa->_values = (void*) ((float*)&pa->_values + 1);
    delete _pa;
    _pa = pa;
  }
  else {// not shared mem., but assume pa has been initialized?
    _paInit(*pa);
    _pa->_type=_Floats_; 
  }
  if( _pa->_elem != elem ) {
    clog<<"UFFloats::UFFloats> ? error in ctor, not using shared memory?"<<endl;
  }
}

// these ctor signatures do a shallow copy of the values, for efficiency
UFFloats::UFFloats(const string& name, const float* vals,
	           int elem, bool shared) : UFTimeStamp() {
  _paInit(_Floats_, name);
  _shallow = true;
  _shared = shared;
  _pa->_elem = elem;
  _pa->_length = minLength() + elem*sizeof(char);
  _pa->_values = (void*) vals;
  _currentTime();
}

// shallow copy ctor
UFFloats::UFFloats(const UFFloats& rhs) : UFTimeStamp() {
  _paInit(_Floats_, "UFFloats");
  shallowCopy(rhs);
}

// deep copy ctor
UFFloats::UFFloats(const string& name,
		   float* vals, int elem,
		   bool shared) : UFTimeStamp() {
  _paInit(_Floats_, name);
  _shared = shared;
  _pa->_elem = elem;
  _pa->_length = minLength() + elem*sizeof(float);
  if( elem > 0 ) {
    _pa->_values = (void*) new (nothrow) float[elem];
    if( _pa->_values == 0 ) clog<<"UFFloats> new alloc failed."<<endl;
    _pa->_elem = 0; _pa->_length = 0;
    return;
  }

  if( vals ) {
    for( int i=0; i<_pa->_elem; i++ )
      ((float*)_pa->_values)[i] = vals[i];
  }
  else { // null pointer passed - just set to 0s
    for( int i=0; i<_pa->_elem; i++ )
      ((float*)_pa->_values)[i] = 0;
  }
}

// deep copy
void UFFloats::deepCopy(const UFProtocol& rhsP) {
  _shallow = false;
  const UFFloats& rhs = dynamic_cast<const UFFloats&>(rhsP);
  UFProtocolAttributes pa;
  pa = *_pa; // save current settings
  *_pa = *rhs._pa;

  // support reuse, if current allocation is sufficient;
  // if not, delete current allocation and allocate larger buffer
  if( !_shared && pa._elem < rhs._pa->_elem ) {
    delete[] static_cast< float* >( _pa->_values ) ;
    _pa->_values = (void*) new float[_pa->_elem];
    _pa->_elem = rhs._pa->_elem;
  }

  for( int i=0; i<_pa->_elem; i++ )
    ((float*)_pa->_values)[i] = ((float*)rhs._pa->_values)[i];
}

// write msg out to file descriptor, returns 0 on failure, num. bytes output on success
int UFFloats::writeTo(int fd) const {
  int retval=0;
  // check fd
  if( fd < 0 ) {
    clog<<"UFFloats::writeTo> bad fd= "<<fd<<endl;
    return retval;
  }
  struct stat st;
  if( fstat(fd, &st) != 0 ) {
    clog<<"UFFloats::writeTo> bad stat on fd= "<<fd<<endl;
    return retval;
  }

  // write out "persistent" attributes in order : _length, _type, _pa->_elem, ...
  // _pa->_name, _pa->_values
  retval += writeHeader(fd, *_pa);
  retval += writeValues(fd);

  if( retval < _pa->_length )
    clog<<"UFFloats::writeTo> ? bad write on fd= "<<fd<<endl;

  return retval;
}

// this might support compression some day
int UFFloats::writeValues(int fd) const {
  return writeFully(fd, (unsigned char*) _pa->_values, _pa->_elem*sizeof(float));
}

int UFFloats::readFrom(int fd) {
  int retval=0;
  // check fd
  if( fd < 0 ) {
    clog<<"UFFloats::readFrom> bad fd= "<<fd<<endl;
    return retval;
  }
  struct stat st;
  if( fstat(fd, &st) != 0 ) {
    clog<<"UFFloats::readFrom> bad stat on fd= "<<fd<<endl;
    return retval;
  }

  UFProtocolAttributes pa;
  int nb = readHeader(fd, pa); // sets everything except _values ptr 
  if( nb <= 0 ) {
    clog<<"UFFloats::readFrom> ? read failed, fd= "<<fd<<endl;
    return nb;
  }
  if( pa._type != typeId() ) {
    clog<<"UFFloats::readFrom> reuse problem! current type= "<<typeId()
        <<", transmitted type= "<<pa._type<<endl;
    // need to dynamically recast to new type (use convertions ctor)
    // later...
  }
  if( pa._elem <= 0 ) { // more problems with transmitted type?
    return retval;
  }
  else if( _pa->_values == 0 || (_pa->_elem < pa._elem && !_shared) ) {
    delete[] static_cast< float* >( _pa->_values ); // delete and re-allocate
    *_pa = pa; // ressets everything including _values ptr
    _pa->_values = pa._values = (void*) new float[_pa->_elem];
  }
  else // reuse existing allocation
    _pa->_values = pa._values;

  retval = nb;
  // virtual read
  retval += readValues(fd); // must complete the read otherwise file synch is lost

  if( retval < _pa->_length ) {
    clog<<"UFFloats::readValues> bad read on fd= "<<fd<<endl;
  }

  return retval;
}


// read/restore from file descriptor
// someday support decompression
int UFFloats::readValues(int fd) {
  return readFully(fd, (unsigned char*) _pa->_values , _pa->_elem*sizeof(float));
}

// write msg out to socket, returns 0 on failure, num. bytes on success
int UFFloats::sendTo(UFSocket& soc) const {
  // make use of the socket class i/o & UFprotocolclass methods
  int retval = sendHeader(soc, *_pa);

  if( retval <= 0 ) 
    return retval;

  // virtual send
  //clog<<"UFFloats::sendTo> elem: "<<_pa->_elem<<", typeId: "<<typeId()<<endl;
  if( _pa->_values && _pa->_duration >= 0.0 ) {
    retval += sendValues(soc);
    if( retval < _pa->_length )
      clog<<"UFFloats::sendTo> bad send on socket= "<<soc.description()<<endl;
  }
  return retval;
}

// once header has been sent out, send the data
// this could optionally send out compressed data:
int UFFloats::sendValues(UFSocket& soc) const {
  return soc.send((float*)_pa->_values, _pa->_elem);
}

int UFFloats::recvFrom(UFSocket& soc) {
  int retval=0;
  UFProtocolAttributes pa= *_pa; // initialize to "this"
  int nb = recvHeader(soc, pa); // resets everything except _values ptr 
  if( nb <= 0 ) {
    clog<<"UFFloats::recvFrom> socket recv failed?"<<endl;
    return nb;
  }
  if( pa._type != typeId() ) {
    clog<<"UFFloats::recvFrom> reuse problem! current type= "<<typeId()
        <<", transmitted type= "<<pa._type<<endl;
    // need to dynamically recast to new type (use convertions ctor)
    // later...
  }
  if( pa._elem <= 0 ) { // more problems with transmitted type?
    return retval;
  }

  // OK to reset "this" header
  *_pa = pa; 

  if( pa._duration < 0.0 ) { // must be a "request"
    return nb; // and return without further socket input (none expected?)
  }
  else if( _pa->_values == 0 || (_pa->_elem < pa._elem && !_shared) ) {
    delete[] static_cast< float* >( _pa->_values ); // delete and re-allocate
     _pa->_values = (void*) new float[_pa->_elem];
  }

  // virtual recv
  clog<<"UFFloats::recvFrom> elem: "<<_pa->_elem<<", typeId: "<<typeId()<<endl;
  retval = nb + recvValues(soc); // must complete the recv otherwise socket synch is lost

  if( retval < _pa->_length )
    clog<<"UFFloats::recvFrom> ? bad recv on soc= "<<soc.description()<<endl;

  return retval;
}


// read/restore values from socket, assuming header has already been read
// deal with compression later...
int UFFloats::recvValues(UFSocket& soc) {
  if( _pa->_duration >= 0.0 && _pa->_values )
    return soc.recv((float*)_pa->_values, _pa->_elem);

  return 0;
} 

#endif // __UFFloats_cc__

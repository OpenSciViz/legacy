#if !defined(__UFClientSocket_cc__)
#define __UFClientSocket_cc__ "$Name:  $ $Id: UFClientSocket.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFClientSocket_cc__;

#include "UFClientSocket.h"
__UFClientSocket_H__(__UFClientSocket_cc);

#include "UFRuntime.h"
__UFRuntime_H__(__UFClientSocket_cc);

#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx) 
#include "vxWorks.h"
#include "sockLib.h"
#include "taskLib.h"
#include "ioLib.h"
#include "fioLib.h"
#endif

#include "cstdio"
#include "cstring"
#include "cerrno"
#include "cstdlib"
#include "unistd.h"
#include "netdb.h"
#include "limits.h"
#include "strings.h"
#include "fcntl.h"
#include "sys/uio.h"
#include "sys/types.h"
#include "sys/stat.h"
#include "sys/socket.h"
#include "arpa/inet.h"
#include "netinet/in.h" // IP_

#if defined(LINUX)
#include "netinet/tcp.h" // TCP_
#include "asm/ioctls.h" // FIONREAD
#endif
#if defined(SOLARIS)
#include "xti_inet.h" // TCP_
#endif

// c++ streams
#include "iostream"
#include "fstream"
#include "strstream"

// former inlines:
UFClientSocket::UFClientSocket(int port) : UFSocket(port), _host("none"), _ipAddr("none") {}
UFClientSocket::UFClientSocket(const UFClientSocket& rhs) : UFSocket((const UFSocket&)rhs),
                                                            _host(rhs._host),
							    _ipAddr(rhs._ipAddr) {}

UFSocket::socketFd 
UFClientSocket::connect(const string& host, int port, bool block) { // connects...
  if( host == "" || host == "none" )
    _host = UFRuntime::hostname() ;
  else
    _host = host ;

  if( port >= 0 ) _portNo = port;
  if( _portNo <= 0 ) {
    clog<<"UFClientSocket::connect> ? portNo= "<<_portNo<<endl;
    return -1;
  } 
  
  _sockinfo.fd = ::socket(AF_INET, SOCK_STREAM, 0);
  if( _sockinfo.fd < 0 ) {
    clog << "UFClientSocket::connect> unable to create new stream socket"
	 << endl;
    return  _sockinfo.fd;
  }

  setSocket(_sockinfo); // allocates _sockinfo.addr

  // theSocket->sin_addr.s_addr = htonl(INADDR_ANY);
  if( isIPAddress(_host) ) { // host string is actually IP addr. string...
    _ipAddr = _host;
  }
  else {
    _ipAddr = ipAddrOf(_host);
    // make sure that the lookup was successfull
    if( _ipAddr == _host ) {
      return -1 ;
    }
  }

  _sockinfo.addr->sin_addr.s_addr = ::inet_addr(_ipAddr.c_str());
  int conStat;
  //clog << "UFClientSocket::connect> attempt to connect to: "
  //     << _host << " (" << _ipAddr << ") " << "using port # "
  //     << _portNo << endl;

  // don't want the connect to block, so reset socket to non-blocking
  // the connect should be in a short retry loop...
  if( block ) {
    if( UFSocket::_verbose )
      clog << "UFClientSocket::connect> blocking..."<<endl;
    _sockinfo.setBlocking();
  }
  else { 
    if( UFSocket::_verbose )
      clog << "UFClientSocket::connect> Non blocking..."<<endl;
    _sockinfo.setBlocking(false);
  }
  // note in the blocking case when connection is refused (no server actually listening),
  // a blocking socket connect returns immediately, so keep trying forever in this special
  // case, rather than checking the try cnt
  int trycnt = 0;
  do {
    if( trycnt++ > 0 ) {
      if( !block ) // still need a short sleep evidently to get EISCONN on 2nd try or later...
        UFPosixRuntime::sleep(0.05);
      else if( errno == ECONNREFUSED ) // sleep a bit more
        UFPosixRuntime::sleep(2.0);
    }
    conStat = ::connect( _sockinfo.fd, reinterpret_cast<const struct sockaddr*>( _sockinfo.addr ), 
			 sizeof(sockaddr_in) );
    if( UFSocket::_verbose )
      clog << "UFClientSocket::connect> trycnt: "<<trycnt<<", stat: "<<conStat
           <<", err:  "<<errno<<" -- "<<strerror(errno)<<endl;
  } while( (block && errno == ECONNREFUSED) || (trycnt < 16 && errno != EISCONN && (conStat < 0 || errno == EINTR) ) );

  if( conStat >= 0 || errno == EISCONN ) {
    if( _sockinfo.fd > _sockinfo.maxFd ) _sockinfo.maxFd = _sockinfo.fd; 
    if( !block ) _sockinfo.setBlocking(); // once connected, socket should be blocking
    if( UFSocket::_verbose )
      clog << "UFClientSocket::connect> trycnt: "<<trycnt<<", fd: "<<_sockinfo.fd<<", err:  "<<strerror(errno)<<endl;
    return _sockinfo.fd;
  }
  if( block )
    clog << "UFClientSocket::connect> unable to connect, trycnt: "<<trycnt<<", host: "
         << _host << ", port # " << _portNo <<"err: "<<errno<<" -- "<< strerror(errno) << endl;

  _sockinfo.close(); // clears the socket attrib. & set fd = -1
  return _sockinfo.fd;
} // connect
 
#endif // __UFClientSocket_cc__

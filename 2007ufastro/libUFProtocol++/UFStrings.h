#if !defined(__UFStrings_h__)
#define __UFStrings_h__ "$Name:  $ $Id: UFStrings.h,v 0.4 2006/04/26 16:22:47 hon Exp $"
#define __UFStrings_H__(arg) const char arg##UFStrings_h__rcsId[] = __UFStrings_h__;

#include "UFTimeStamp.h"
#include "UFRuntime.h"

#include "string"
#include "vector"
#include "deque"

using std::string ;
using std::vector ;
using std::deque ;

class UFStrings: public virtual UFTimeStamp {
protected:
  inline UFStrings() {}
  void _copyVals(const string* vals, int elem=1);
  void _copyVals(const vector< string >& vals);
  void _copyVals(const deque< string >& vals);

public:
  static int upperCase(string& s);
  static int lowerCase(string& s);
  inline static void rmJunk(string& s) { UFRuntime::rmJunk(s); } 
  UFStrings(const UFProtocolAttributes& pa);
  UFStrings(const string& name, int length=_MinLength_);
  UFStrings(const string& name, char* val, int elem=1);
  UFStrings(const string& name, string* vals, int elem=1);
  UFStrings(const string& name, const vector< string >& vals);
  UFStrings(const string& name, const deque< string >& vals);
  UFStrings(const string& name, const string* vals, int elem);
  UFStrings(const UFStrings& rhs);
  UFStrings( UFStrings* Head, UFStrings* Tail=0, int startTail=0, string* name=0 );
  virtual ~UFStrings();

  int clear();
  int upperCase();
  int lowerCase();

  int setWith(const vector< string >& vals, float duration=0.0, int seq=0, int seqT=0);

  inline virtual string description() const { return string(__UFStrings_h__); }

  inline virtual int valSize(int elemIdx=0) const
    { if( numVals() <= 0 ) return 0; return ((string*)_pa->_values)[elemIdx].size(); }

  inline virtual int size() const
    { int sz= _pa->headerSize(); for( int i = 0; i < elements(); i++ ) sz += valSize(i); return sz; }

  inline virtual const string* stringAt(int elemIdx=0) const
    { if( elemIdx < 0 || elemIdx >= elements() ) return 0; return ((string*)_pa->_values)+elemIdx; }

  inline virtual string operator[](int elemIdx) const
    { if( elemIdx < 0 || elemIdx >= elements() ) return ""; return *(stringAt(elemIdx)); }

  inline virtual const char* valData(int elemIdx=0) const
    { if( elemIdx < 0 || elemIdx >= elements() ) return 0; return (stringAt(elemIdx))->c_str(); }

  //virtual void shallowCopy(const UFProtocol& rhs);

  virtual void deepCopy(const UFProtocol& rhs);

  inline virtual void copyVal(char* dst, int elemIdx= 0) {
    if( elemIdx < 0 || elemIdx >= elements() ) return;
    strncpy(dst, ((string*)_pa->_values)[elemIdx].c_str(), ((string*)_pa->_values)[elemIdx].size());
  }  

  virtual int writeTo(int fd) const; 
  virtual int writeValues(int fd) const; 
  virtual int readFrom(int fd); 
  virtual int readValues(int fd); 
  virtual int sendTo(UFSocket& soc) const; 
  virtual int sendValues(UFSocket& soc) const; 
  virtual int recvFrom(UFSocket& soc); 
  virtual int recvValues(UFSocket& soc); 
};

#endif // __UFStrings_h__


/**
 * UFStrings UFProtocolAttributes Msg contains one ore more (variable)
 * length stdc++ strings.
 */
  /**
   * The default constructor is hidden to support the ObjectFactory pattern
   */
  /**
   * Initialized the values array to the given array of strings,
   * of size elem.
   * Performs deep copy, meant to be used only by ctors.  This means that
   * the constructor does the memory allocation.
   * @param vals The array of strings used to initialized this object.
   * @param elem The number of strings in the given vals array.
   */
  /**
   * Initialized the values array to the given vector of strings.
   * Performs deep copy, meant to be used only by ctors.  This means
   * that the constructor does the memory allocation.
   * @param vals The vector of strings used to initialized this object.
   */
  /**
   * Initialized the values array to the given queue of strings,
   * of size elem.
   * Performs deep copy, meant to be used only by ctors.  This means that
   * the constructor does the memory allocation.
   * @param vals The queue of strings used to initialized this object.
   */
  /**
   * Convert a std::string to uppercase.
   * @param s The string to convert
   * @return The length of the string.
   */
  /**
   * Convert a std::string to lowercase.
   * @param s The string to convert
   * @return The length of the string.
   */
  /**
   * Removes all extraneous characters from the given string,
   * such as "\t", "\n", "\r", etc.
   * @param s The string to strip of extraneous characters.
   */
  /**
   * Instantiate a UFStrings object given the UFProtocolAttributes object.
   * This is the constructor used to initialize the UFStrings object by the
   * class factory.
   * @param pa The attributes for initializing this object
   * @return Nothing
   */
  /**
   * This constructor initialized to current time
   * @param name The name of this object, placed into the UFProtocolAttributes object
   * @param length The total length of the UFProtocolAttributes object, with a default
   * value of _MinLength_.
   * @see UFProtocolAttribute
   * @see _MinLength_
   * @return Nothing
   */
  /**
   * This constructor is initialized with a name and an array of values.
   * @param name The name of this object, placed into the UFProtocolAttributes object
   * @param val The array of values to which to initialize this object
   * @param elem The length of the array, default to 1
   * @see UFProtocolAttribute
   * @return Nothing
   */
  /**
   * This constructor is initialized with a name and an array of values.
   * @param name The name of this object, placed into the UFProtocolAttributes object
   * @param vals The array of values to which to initialize this object
   * @param elem The length of the array, default to 1
   * @see UFProtocolAttribute
   * @return Nothing
   */
  /**
   * This constructor is initialized with a name and a vector  of values.
   * @param name The name of this object, placed into the UFProtocolAttributes object
   * @param vals The vector of values to which to initialize this object
   * @see UFProtocolAttribute
   * @return Nothing
   */
  /**
   * This constructor is initialized with a name and a queue of values.
   * @param name The name of this object, placed into the UFProtocolAttributes object
   * @param vals The queue of values to which to initialize this object
   * @see UFProtocolAttribute
   * @return Nothing
   */
  /**
   * This constructor is initialized with a name and an array of values.
   * This method performs a shallow copy of vals
   * @param name The name of this object, placed into the UFProtocolAttributes object
   * @param vals The array of values to which to initialize this object
   * @param elem The length of the array, default to 1
   * @see UFProtocolAttribute
   * @return Nothing
   */
  /**
   * Copy constructor, performs a shallow copy of the given UFStrings object.
   * @param rhs The source UFStrings object to copy into this object.
   */
  /**
   * Concatenate two UFStrings objects into single new UFStrings.
   * Default is to use all the strings in the Tail object, unless startTail is set > 0.
   * If Tail is NULL then new UFStrings is just a deep copy of Head.
   * If name is NULL then name of new UFStrings is same as name of Head.
   */
  /**
   * Destructor.  This method will deallocate all resources used by this object.
   */
  /**
   * Clear the array of strings.  This will perform all deallocations and
   * updated internal variables to reflect the changes made.
   */
  /**
   * Apply upperCase() to each string in this object.
   */
  /**
   * Apply lowerCase() to each string in this object.
   */
  /**
   * Another form of initialization method.
   * @param vals The vector of strings used to initialize this objects
   * values.
   * @param duration The new object duration
   * @param seq The new sequence number for this object
   * @param seqT The sequence total for this sequence.
   */
  /**
   * Return a copy of a C++ STL string describing this object.
   */
  /**
   * Return size of the elements value string (not name string!)
   * @param elemIdx The index into the values array to examine,
   * defaults to 0.
   * @return The size of the element at index elemIdx
   */
  /**
   * Return the element at elemIdx as a string
   * @param elemIdx The index into the values array to examine,
   * defaults to 0.
   * @return A pointer to const string of the element at index elemIdx
   */
  /**
   * Convenience operator method, invokes stringAt()
   * @see stringAt
   */
  /**
   * Return const char* pointer to first byte of element value (data)
   * @param elemIdx The index into the values array to examine,
   * defaults to 0.
   * @return const char* pointer to first byte of element value
   */
  /**
   * Return the total size of this object in memory
   * @return The total size of this object in memory
   */
  /**
   * Perform a deep copy of the given UFProtocol object into this one.
   * @param rhs The source UFProtocol object to copy.
   */
  /**
   * Copy internal values to external buff
   * @param dst The destination buffer to which to copy internal values.
   * @param elemIdx The index into the values array to examine,
   * defaults to 0.
   * @return const char* pointer to first byte of element value
   */
  /**
   * Write out the internal data representation to file descriptor.
   * @param fd The file descriptor to which to write
   * @return 0 on failure, number of bytes output on success
   */
  /**
   * Write out only the values array to the given file descriptor.
   * @param fd The file descriptor to which to write
   * @return 0 on failure, number of bytes written otherwise
   * @return UFTimeStamp always returns 0 since there is no data array.
   */
  /**
   * Read/restore the internal data representation from a file descriptor,
   * supporting reuse of existing object.
   * @param fd The file descriptor from which to read
   * @return 0 on failure, number of bytes read otherwise
   */
  /**
   * Read/restore only the values array from the given file descriptor
   * @param fd The file descriptor from which to read
   * @return 0 on failure, number of bytes read otherwise
   * @return UFTimeStamp always returns 0 since there is no data array.
   */
  /**
   * Write the internal data representation out to a socket
   * @return 0 on failure, number of bytes written on success
   * @param soc The UFSocket to which to write
   * @see UFSocket
   */
  /**
   * Write out only the values array to the given UFSocket.
   * @param soc The UFSocket to which to write
   * @return 0 on failure, number of bytes written on success
   * @see UFSocket
   */
  /**
   * Read/restore only the values array from the given UFSocket
   * @param soc The UFSocket from which to read
   * @return 0 on failure, number of bytes read on success
   * @see UFSocket
   */
  /**
   * Read/restore only the values array from the given UFSocket
   * @param soc The UFSocket from which to read
   * @return 0 on failure, number of bytes read on success
   * @see UFSocket
   */

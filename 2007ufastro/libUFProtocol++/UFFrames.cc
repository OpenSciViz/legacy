#if !defined(__UFFrames_cc__)
#define __UFFrames_cc__ "$Name:  $ $Id: UFFrames.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFFrames_cc__;

#include "UFFrames.h"
__UFFrames_H__(UFFrames_cc__)

#include "UFTimeStamp.h"

// Frames Protocol Msg contains one or more frames
// that is internally stored (shallow) and transmitted as
// either UFBytes or UFShorts or UFInts or UFFloats object

// these are all the currently planned frame buffers.
// a client must now explicitly indicate to the server
// which frames it wants sent in each transaction by name .
// the server should append timestamps to the requested buffer name strings...
static const int _TotBuffCnt__ = 14;
static const char* _AllBuffNames_[_TotBuffCnt__] = { "S1", "R1", "D1", "S2", "R2", "D2",
						     "S1A", "S2A", "D1A", "D2A", "Sig", "SigA" };
static const char* _DefStareModeNames_[2] = { "S1", "S1A" };
static const char* _DefNodModeNames_[2] = { "Sig", "SigA" };
static const char* _DefChopModeNames_[2] = { "D1", "D1A" };
static const char* _DefChopNodModeNames_[3] = { "D1", "D2", "SigA" };

// this is for use by UFProtocol::create function
UFFrames::UFFrames(const UFProtocolAttributes& pa) : UFFrameConfig(), _data(0), _req(0) {
  _paInit(pa);
  if( _pa->_type != _IntFrames_ && _pa->_type != _FloatFrames_ &&
      _pa->_type != _ShortFrames_ && _pa->_type != _ByteFrames_ ) {
    clog<<"UFrames::> ? type mismatch, _pa->_type = "<< _pa->_type << endl;
    _pa->_type= _IntFrames_;
  }
  // although the type must be one of _ByteFrames_, _ShortFrames_, _FloatFrames_, _IntFrames_, or zipped equiv.
  // we store the framedata into _data. here we just want to init parent clase UFFrameConfig stuff: 
  if( _pa->_elem != UFFrameConfig::statusElements() ) {
    clog<<"UFrames::> ? element count mismatch, _pa->_elem = "<< _pa->_elem<< endl;
    _pa->_elem =  UFFrameConfig::statusElements();
  }
  _pa->_values = (void*) new int[_pa->_elem];
}

UFFrames::UFFrames(const UFFrameConfig& c, UFProtocol* ufp) : UFFrameConfig(), _data(ufp), _req(0) {
  if( ufp == 0 ) {
    _paInit(_IntFrames_, c.attributesPtr()); // shallow,shared _pa pointer!
    _currentTime();
    return;
  }
  switch(ufp->typeId()) {
    case _Bytes_: _paInit(_ByteFrames_, c.attributesPtr());
      break;
    case _Shorts_: _paInit(_ShortFrames_, c.attributesPtr());
      break;
    case _Ints_: _paInit(_IntFrames_, c.attributesPtr());
      break;
    case _Floats_: _paInit(_FloatFrames_, c.attributesPtr());
      break;
    default: clog<<"UFFrames::> ? bad protocol object, ufp->type= "<<ufp->typeId()<<", name= "<<ufp->cname()<<endl;
      break;
  }
//  _currentTime();
}

UFFrames::UFFrames(UFProtocol* ufp, int w, int h, int d, bool littleEnd) : UFFrameConfig(), _data(ufp), _req(0) {
  if( ufp == 0 ) { // default to UFInts
    _paInit(_IntFrames_, "UFFrames");
    _pa->_elem = statusElements();
    _pa->_values = (void*) new int[_pa->_elem];
    UFFrameConfig::_init(w, h, d, littleEnd);
    _currentTime();
    return;
  }
  switch(ufp->typeId()) {
    case _Bytes_: _paInit(_ByteFrames_, "UFFrames"); _pa->_elem = statusElements();
      _pa->_values = (void*) new char[_pa->_elem];
      break;
    case _Shorts_: _paInit(_ShortFrames_, "UFFrames"); _pa->_elem = statusElements();
      _pa->_values = (void*) new short[_pa->_elem];
      break;
    case _Ints_: _paInit(_IntFrames_, "UFFrames"); _pa->_elem = statusElements();
      _pa->_values = (void*) new int[_pa->_elem];
      break;
    case _Floats_: _paInit(_FloatFrames_, "UFFrames"); _pa->_elem = statusElements();
      _pa->_values = (void*) new float[_pa->_elem];
     break;
    default: clog<<"UFFrames::> ? bad protocol object, ufp->type= "<<ufp->typeId()<<", name= "<<ufp->cname()<<endl;
      break;
  }
  UFFrameConfig::_init(w, h, d, littleEnd);
}

UFFrames::UFFrames(const UFFrames& rhs) : UFFrameConfig(), _data(rhs._data), _req(rhs._req) {
  _paInit(rhs.attributesRef());
}

// override:
int UFFrames::length() const {
  int len = configLength();
  if( _req ) len += _req->length();
  if( _data ) len += _data->length();
  return len;
}
// fetch name
const char* UFFrames::cname() const {
  if( _data ) return _data->cname(); return 0;
}
string UFFrames::name() const {
  if( _data ) return _data->name(); return "";
}
// reset name
string UFFrames::rename(const string& newname) {
  if( _data ) return _data->rename(newname); return "";
}
// fetch timestamp
const char* UFFrames::timestamp() const {
  if( _data ) return _data->timestamp(); return 0;
}
string UFFrames::timeStamp() const {
  if( _data ) return _data->timeStamp(); return "";
}
// reset timestamp
string UFFrames::stampTime(const string& newtime) {
  if( _data ) return _data->stampTime(newtime); return "";
}
// return # of elements (frame count)
int UFFrames::elements() const {
  int w= UFFrameConfig::width();
  int h= UFFrameConfig::height();
  int d= UFFrameConfig::depth();
  if( _data ) {
    int framesz = w * h * (d / _data->valSize() );
    int elem = framesz / _data->numVals();
    return elem;
  }
  return 0;
}

int UFFrames::numVals() const {
  if( _data ) return _data->numVals(); return 0;
}

int UFFrames::valSize(int elemIdx) const {
  if( _data ) return _data->valSize(elemIdx); return 0;
}

// return int* pointer to first byte of _value (data) array:
const char* UFFrames::valData(int elemIdx) const {
  if( _data ) return _data->valData(elemIdx); return 0;
}

void UFFrames::shallowCopy(const UFProtocol& rhs) {
  if( _data ) _data->shallowCopy(rhs);
}

void UFFrames::deepCopy(const UFProtocol& rhs) {
  if( _data ) _data->deepCopy(rhs);
}

// copy internal values to external buff
void UFFrames::copyVal(char* dst, int elemIdx) { 
  if( _data ) _data->copyVal(dst, elemIdx);
}

// write out to file descriptor, returns 0 on failure, num. bytes output on success
int UFFrames::writeTo(int fd) const {
  // first write config
  //const UFFrameConfig* tmp = dynamic_cast< const UFFrameConfig* > (this);
  const UFFrameConfig* tmp = static_cast< const UFFrameConfig* > (this);
  // use shallow ctor:
  UFFrameConfig c(*tmp);
  int nb = writeConfig(fd, c); // this write header & values of parent class

  int tmpcnt = 0;
  // try to deal with potentially empty parts of this class:
  if( _req ) { // send request info, if any
    tmpcnt = _req->elements();
    nb += writeFully(fd, (unsigned char*)&tmpcnt, sizeof(int));
    if( _req->elements() ) nb += _req->writeTo(fd);
  }
  else { // indicate no request
    tmpcnt = 0;
    nb += writeFully(fd, (unsigned char*)&tmpcnt, sizeof(int));
  }

  if( _data ) { // write any data
    tmpcnt = _data->elements();
    nb += writeFully(fd, (unsigned char*)&tmpcnt, sizeof(int));
    if( _data->elements() ) nb += _data->writeTo(fd);
  } 
  else { // indicate no data
    tmpcnt = 0;
    nb += writeFully(fd, (unsigned char*)&tmpcnt, sizeof(int));
  }

  return nb; 
} 

// this must assume that the header has already been written
// and must complete the wrtie
int UFFrames::writeValues(int fd) const {
  if( _pa->_values == 0  )
    return 0;

  int nb = UFFrameConfig::writeValues(fd); // just write values of parent class

  int tmpcnt = 0;
  // try to deal with potentially empty parts of this class:
  if( _req ) { // send request info, if any
    tmpcnt = _req->elements();
    nb += writeFully(fd, (unsigned char*)&tmpcnt, sizeof(int));
    if( _req->elements() ) nb += _req->writeTo(fd);
  }
  else { // indicate no request
    tmpcnt = 0;
    nb += writeFully(fd,(unsigned char*) &tmpcnt, sizeof(int));
  }

  if( _data ) { // write any data
    tmpcnt = _data->elements();
    nb += writeFully(fd, (unsigned char*)&tmpcnt, sizeof(int));
    if( _data->elements() ) nb += _data->writeTo(fd);
  } 
  else { // indicate no data
    tmpcnt = 0;
    nb += writeFully(fd, (unsigned char*)&tmpcnt, sizeof(int));
  }

  return nb;
}

// read/restore from file descriptor, support reuse of existing object
int UFFrames::readFrom(int fd) {
  // first read config
  //UFFrameConfig* tmp = dynamic_cast< UFFrameConfig* > (this);
  UFFrameConfig* tmp = static_cast< UFFrameConfig* > (this);
  // use shallow ctor:
  UFFrameConfig c(*tmp);
  int nb = readConfig(fd, c); // read header and values of parent class

  // check if there is a request UFStrings and/or data
  int tmpcnt = 0;

  readFully(fd, (unsigned char*)&tmpcnt, sizeof(tmpcnt));
  if( tmpcnt > 0 ) {
    if( _req ) // read req strings
      nb += _req->readFrom(fd);
    else {
      _req = dynamic_cast< UFStrings* > (UFProtocol::createFrom(fd));
       if( _req ) nb += _data->length();
    }
  }

  tmpcnt = 0;
  readFully(fd, (unsigned char*)&tmpcnt, sizeof(tmpcnt));
  if( tmpcnt > 0 ) {
    if( _data ) // read data
      nb += _data->readFrom(fd);
    else {
      _data = UFProtocol::createFrom(fd);
       if( _data ) nb += _data->length();
    }
  }

  return nb; 
} 

// this must assume that the header has already been read
// and must complete the read
int UFFrames::readValues(int fd) {
  if( _pa->_values == 0  )
    return 0;

  int nb = UFFrameConfig::readValues(fd); // read values of parent class

  // check if there is a request UFStrings and/or data
  int tmpcnt = 0;

  readFully(fd, (unsigned char*)&tmpcnt, sizeof(tmpcnt));
  if( tmpcnt > 0 ) {
    if( _req ) // read data
      nb += _req->readFrom(fd);
    else {
      _req = dynamic_cast< UFStrings* > (UFProtocol::createFrom(fd));
       if( _req ) nb += _data->length();
    }
  }

  tmpcnt = 0;
  readFully(fd, (unsigned char*)&tmpcnt, sizeof(tmpcnt));
  if( tmpcnt > 0 ) {
    if( _data ) // read data
      nb += _data->readFrom(fd);
    else {
      _data = UFProtocol::createFrom(fd);
       if( _data ) nb += _data->length();
    }
  }

  return nb;
}

// write msg out to socket, returns 0 on failure, num. bytes on success
int UFFrames::sendTo(UFSocket& soc) const {
  // first write config:
  // this does not work with g++ 2.95; (neither cast), evidently
  // the compiler insists on calling UFFrames::sendTo from UFFrameConfig::sendConfig?
  //const UFFrameConfig* tmp = dynamic_cast< const UFFrameConfig* > (this);
  const UFFrameConfig* tmp = static_cast< const UFFrameConfig* > (this);
  //int nb = sendConfig(soc, *tmp);
  // use shallow ctor:
  UFFrameConfig c(*tmp);
  int nb = sendConfig(soc, c); // send header & values of parent class

  // try to deal with potentially empty parts of this class:
  if( _req ) { // send request info, if any
    nb += soc.send(_req->elements());
    if( _req->elements() ) nb += _req->sendTo(soc);
  }
  else // indicate no request
    nb += soc.send((int) 0);

  if( _data ) { // send  data
    nb += soc.send(_data->elements());
    nb += _data->sendTo(soc);
  } 
  else { // indicate no data
    nb +=  soc.send((int) 0);
  }

  return nb; 
}

// this must assume that the header has already been sent
// and must complete the transaction
int UFFrames::sendValues(UFSocket& soc) const {
  if( _pa->_values == 0  )
    return 0;

  int nb = UFFrameConfig::sendValues(soc); // send values of parent class

  // try to deal with potentially empty parts of this class:
  if( _req ) { // send request info, if any
    nb += soc.send(_req->elements());
    if( _req->elements() ) nb += _req->sendTo(soc);
  }
  else // indicate no request
    nb += soc.send((int) 0);

  if( _data ) { // send  data
    nb += soc.send(_data->elements());
    nb += _data->sendTo(soc);
  } 
  else { // indicate no data
    nb +=  soc.send((int) 0);
  }

  return nb;
}

// support reuse of existing object
// this does not use recValues due to the composite nature
// of this class
int UFFrames::recvFrom(UFSocket& soc) {
  // first read config
  //UFFrameConfig* tmp = dynamic_cast< UFFrameConfig* > (this);
  UFFrameConfig* tmp = static_cast< UFFrameConfig* > (this);
  // use shallow ctor:
  UFFrameConfig c(*tmp);
  int nb = recvConfig(soc, c); // recv header & values of parent class

  // check if there is a request UFStrings sent
  int tmpcnt = 0;

  soc.recv(tmpcnt);
  if( tmpcnt > 0 ) {
    if( _req ) // read data
      nb += _req->recvFrom(soc);
    else {
      _req = dynamic_cast< UFStrings* > (UFProtocol::createFrom(soc));
       if( _req ) nb += _data->length();
    }
  }

  tmpcnt = 0;
  soc.recv(tmpcnt);
  if( tmpcnt > 0 ) {
    if( _data ) // read data
      nb += _data->recvFrom(soc);
    else {
      _data = UFProtocol::createFrom(soc);
       if( _data ) nb += _data->length();
    }
  }

  return nb;
}

// this must assume that the header has already been received
// and must complete the transaction
int UFFrames::recvValues(UFSocket& soc) {
  if( _pa->_values == 0  )
    return 0;

  int nb = UFFrameConfig::recvValues(soc); // send values of parent class
  // check if there is a request UFStrings sent
  int tmpcnt = 0;

  soc.recv(tmpcnt);
  if( tmpcnt > 0 ) {
    if( _req ) { // read data, this relies on re-use logic actually working!
      nb += _req->recvFrom(soc);
    }
    else {
      _req = dynamic_cast< UFStrings* > (UFProtocol::createFrom(soc));
       if( _req ) nb += _data->length();
    }
  }

  tmpcnt = 0;
  soc.recv(tmpcnt);
  if( tmpcnt > 0 ) {
    if( _data ) { // read data, this relies on re-use logic actually working! 
      nb += _data->recvFrom(soc);
    }
    else {
      _data = UFProtocol::createFrom(soc);
       if( _data ) nb += _data->length();
    }
  }

  return nb;
}

// public statics

// for use in client
int UFFrames::reqFrames(UFSocket& soc, UFFrames& f) {
  // negative duration is used to ID the transaction as a request
  int nb = f.sendAsReqTo(soc);
  if( nb <= 0 ) { 
    clog<<"UFFrames::reqFrames> ? unable to send request to server."<<endl;
    return nb;
  }
  return f.recvFrom(soc); // resets f
}

// for use in client
int UFFrames::sendAsReqTo(UFSocket& soc) {
  // request config
  int nb = UFFrameConfig::sendAsReqTo(soc);

  if( _req && _req->elements() > 0 )
    nb += _req->sendTo(soc);

  return nb;
}

// for use in server
int UFFrames::recvReq(UFSocket& soc, UFFrames& f) {
  // recv request config
  UFFrameConfig& c = static_cast< UFFrameConfig& > (f);
  int nb = c.recvFrom(soc); // resets c

  // recv _req strings, should be list of buffer names
  UFStrings& req = (UFStrings&) f.reqNamesRef();
  nb += req.recvFrom(soc); // resets _req

  return nb;
}

int UFFrames::allBuffNames(vector< string >& vreq) {
  vreq.clear();
  for( int i = 0; i < _TotBuffCnt__; i++ )
    vreq.push_back(string(_AllBuffNames_[i]));

  return vreq.size();
}

int UFFrames::defaultStareModeReq(vector< string >& vreq) {
  vreq.clear();
  vreq.push_back(string(_DefStareModeNames_[0]));
  vreq.push_back(string(_DefStareModeNames_[1]));
  return vreq.size();
}
							   
int UFFrames::defaultNodModeReq(vector< string >& vreq) {
  vreq.clear();
  vreq.push_back(string(_DefNodModeNames_[0]));
  vreq.push_back(string(_DefNodModeNames_[1]));
  return vreq.size();
}
							   
int UFFrames::defaultChopModeReq(vector< string >& vreq) {
  vreq.clear();
  vreq.push_back(string(_DefChopModeNames_[0]));
  vreq.push_back(string(_DefChopModeNames_[1]));
  return vreq.size();
}
							   
int UFFrames::defaultChopNodModeReq(vector< string >& vreq){
  vreq.clear();
  vreq.push_back(string(_DefChopNodModeNames_[0]));
  vreq.push_back(string(_DefChopNodModeNames_[1]));
  vreq.push_back(string(_DefChopNodModeNames_[2]));
  return vreq.size();
}
							   
int UFFrames::reqBuffNames(vector< string >& vreq) const {
  vreq.clear();

  if( _req && _req->elements() > 0 ) {
    for( int i = 0; i < _req->elements(); i++ ) {
      const string* tmp = _req->stringAt(i);
      vreq.push_back(*tmp);
    }
  }
  return vreq.size();
}
							   
int UFFrames::setReqBuffNames(vector< string >& vreq) {
  int cnt;

  if( vreq.size() == 0 ) // use all?
    cnt = allBuffNames(vreq);

  if( _req == 0 )
    _req = new UFStrings("UFFrames Request", vreq);
  else
    _req->setWith(vreq);

  return _req->elements();
}

#endif // __UFFrames_cc__



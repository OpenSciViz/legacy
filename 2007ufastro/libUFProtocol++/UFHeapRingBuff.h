#if !defined(__UFHeapRingBuff_h__)
#define __UFHeapRingBuff_h__ "$Name:  $ $Id: UFHeapRingBuff.h,v 0.2 2004/01/28 20:30:24 drashkin beta $"
#define __UFHeapRingBuff_H__(arg) const char arg##HeapRingBuff_h__rcsId[] = __UFHeapRingBuff_h__;
 
#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx) 
#include "vxWorks.h"
#include "sockLib.h"
#include "taskLib.h"
#include "ioLib.h"
#include "fioLib.h"
#endif // vx

#include "UFRingBuff.h"
#include "pthread.h"

class UFHeapRingBuff : public UFRingBuff {
protected:
  // take & release this mutex for read & write access to _theFlag and write access to _activeIdx: 
  pthread_mutex_t _theMutex;
  // much like the shared memory version, this flag is associated with the active index.
  // when the active buff is being written, this flag should be set to
  // the thread id of the thread performing the write, otherwise it should
  // be clear (indicating it is safe to read the active/newest dma buff)
  pthread_t _theFlag;

  
  int _setActiveIndex(int);

  
  int _getActiveIndex(bool& isLocked);

  
  virtual void _init();

  
  virtual int _init(vector<string>& names, int bufsize, int type= UFProtocol::_Ints_);

public:

  
  UFHeapRingBuff(const string& name= "Anonymous");

  
  UFHeapRingBuff(UFProtocol* external, int bufsize, int numbuf, const string& name= "Anonymous");

  
  UFHeapRingBuff(UFProtocol** external, int bufsize, int numbuf, const string& name= "Anonymous");

  
  UFHeapRingBuff(vector<string>& names, int bufsize);

  
  virtual ~UFHeapRingBuff();

  // override these virtuals:

  
  virtual int init(vector< string >& names, int bufsize, int type= UFProtocol::_Ints_);

  
  virtual int activeIndex();

  
  virtual int activeIndex(bool& locked);

  
  virtual UFProtocol* lockActiveBuf(int& index);

  
  virtual int unlockActiveBuf();

  
  virtual int nextActiveBuf();
  // convenience func. returns buffer that was last written

  
  virtual UFProtocol* waitForNewData(int& index);

  
  virtual int write(const UFProtocol* data, int index=-1);

  
  virtual int read(UFProtocol* data, int index=-1);
};

#endif // __UFHeapRingBuff_h__
  /**
   *
   *@return TBD
   */
  /**
   *
   *@param isLocked TBD
   *@return TBD
   */
  /**
   *
   */
  /**
   *
   *@param names TBD
   *@param bufsize TBD
   *@param type TBD
   *@return TBD
   */
  /**
   *
   *@param name TBD
   */
  /**
   *
   *@param external TBD
   *@param bufsize TBD
   *@param numbuf TBD
   *@param name TBD
   */
  /**
   *
   *@param external TBD
   *@param bufsize TBD
   *@param numbuf TBD
   *@param name TBD
   */
  /**
   *
   *@param names TBD
   *@param bufsize TBD
   */
  /**
   *
   */
  /**
   *
   *@param names TBD
   *@param bufsize TBD
   *@param type TBD
   *@return TBD
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@param locked TBD
   *@return TBD
   */
  /**
   *
   *@param index TBD
   *@return TBD
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@param index TBD
   *@return TBD
   */
  /**
   *
   *@param data TBD
   *@param index TBD
   *@return TBD
   */
  /**
   *
   *@param data TBD
   *@param index TBD
   *@return TBD
   */

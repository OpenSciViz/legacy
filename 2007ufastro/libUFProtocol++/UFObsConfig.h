#if !defined(__UFObsConfig_h__)
#define __UFObsConfig_h__ "$Name:  $ $Id: UFObsConfig.h,v 0.12 2004/01/28 20:30:24 drashkin beta $"
#define __UFObsConfig_H__(arg) const char arg##ObsConfig_h__rcsId[] = __UFObsConfig_h__;

#include "UFShorts.h"
#include "UFInts.h"
#include "UFFrameConfig.h"

#include "string"
#include "vector"
#include "map"

class UFObsConfig: public virtual UFShorts {
 public:
  struct Config { int NodBeams; int ChopBeams; int SaveSets; int NodSets;
                  int CoaddsPerFrm; string ReadoutMode; };

  
  static void beams(const string& obsmode, int& nb, int& cb);

  // estimated MCE4 EDT fiber transfer time in seconds (depends on readout mode)

  
  float estXferTime();

  // convenience func. for evaluation of all TReCS image buffers

  
  static int initTReCSBufs(UFFrameConfig* fc= 0);

  
  int evalTReCS(int frmIdx, UFInts* frame, map< string, UFInts* >& bufs, UFFrameConfig* fc= 0, bool freeFrm= true);
  // get all buffer names:

  
  static int getBufNames(const string& instrum, vector< string >& names);

  
  int getBufNames(vector< string >& names);
  // get buffer by name:

  
  UFInts* getBuf(const string& name);

 protected:
  static vector< string > _trecsbufnames;
  static map< string, UFInts* > _trecsbufs;

  //note: the default cpf = 9 is the minimum coadds per frame that MCE will do.
  // support shallow ctors

  
  void _init(int nb=2, int cb=2, int ss=3, int ns=2, int cpf=9, const unsigned short* vals=0);

  
  void _initVals(const unsigned short* vals, int elem);
  // support deep ctors

  
  void _init(int nb=2, int cb=2, int ss=3, int ns=2, int cpf=9, unsigned short* vals=0);

  
  void _initVals(unsigned short* vals=0, int elem=1);

  
  void _initBufSeqFlags();

  // fetch config substr

  
  string _config() const;
  // fetch name substr

  
  string _name() const;
  // insure _name is properly formatted

  
  string _initname();

  // Bit mask code for defining/extracting buffer sequencing information:

  enum BitMaskCode {
    BMC_RAW        =    3,
    BMC_CLR        =    4,
    BMC_D1         =    8,
    BMC_D2         =   16,
    BMC_D1A        =   32,
    BMC_D2A        =   64,
    BMC_SG         =  128,
    BMC_NOD_BEAM   =  256,
    BMC_CHOP_BEAM   =  512,
    BMC_NOD_TOGGLE = 1024,	
  };

 public:
  // UFObsConfig ctor for create function:

  
  inline UFObsConfig(const UFProtocolAttributes& pa) : UFShorts() {
    _paInit(pa);
    if( _pa->_type != _ObsConfig_ ) {
      clog<<"UFObsConfig::> ? type mismatch, _pa->_type = "<< _pa->_type << endl;
      _pa->_type = _ObsConfig_;
    }
    _initVals();
    _currentTime();
  }


  
  inline UFObsConfig(int nb=2, int cb=2, int ss=3, int ns=2, int cpf=9,
		     unsigned short* vals=0): UFShorts() { 
    _paInit(_ObsConfig_);
    _init(nb, cb, ss, ns, cpf, vals);
    _currentTime();
  }


  
  inline UFObsConfig(const string& name,
		     int nb=2, int cb=2, int ss=3, int ns=2, int cpf=9,
		     unsigned short* vals=0) : UFShorts() {
    _paInit(_ObsConfig_, name);
    _init(nb, cb, ss, ns, cpf, vals);
    _currentTime();
  }

  // const UFShorts ctor makes use of shallow copy

  
  inline UFObsConfig(const UFShorts& ufs,
		     int nb=2, int cb=2,
		     int ss=3, int ns=2, int cpf=9) : UFShorts() {
    _paInit(_ObsConfig_, ufs.attributesPtr());
    _init(nb, cb, ss, ns, cpf, (const unsigned short*) ufs.valData());
    _currentTime();
  }


  
  inline UFObsConfig( const UFShorts& ufs ) : UFShorts(ufs) {
    _paInit( _ObsConfig_, ufs.attributesPtr() );
    _currentTime();
  }


  
  inline UFObsConfig( UFShorts& ufs ) : UFShorts(ufs) {
    _paInit( _ObsConfig_, ufs.attributesPtr() );
    _currentTime();
  }

  // UFObsConfig copy ctors:

  
  inline UFObsConfig(const UFObsConfig& ufoc) : UFShorts() { // shallow copy
    _paInit(_ObsConfig_, ufoc.attributesPtr());
    _init(ufoc.nodBeams(), ufoc.chopBeams(), ufoc.saveSets(), ufoc.nodSets(), ufoc.coaddsPerFrm(),
	  (const unsigned short*)0);
    _currentTime();
  }


  
  inline UFObsConfig(UFObsConfig& ufoc) : UFShorts() { // deep copy
    clog << "UFObsConfig::> deep copy!" << endl;
    printHeader(ufoc.attributesPtr());
    _paInit( ufoc.attributesRef() ); // copy helper
    deepCopy( ufoc );
    _currentTime();
    printHeader(_pa);
    clog << "UFObsConfig::> deep copy is done!" << endl;
  }

  // deep copy of vals.

  
  UFObsConfig( const string& name, const char* vals, int elem, bool shared=false );


  
  inline virtual ~UFObsConfig() {}

  // override the inherited UFShorts description:

  
  inline virtual string description() { return string(__UFObsConfig_h__); }

  // convenience functions:
  

  
  inline int totFrameCnt() { if( _pa ) return _pa->_elem; return 0; }
  

  
  UFObsConfig::Config config() const;

  
  string setConfig(const UFObsConfig::Config& c);

  
  string setConfig(const string& c);


  
  int nodBeams() const;

  
  int chopBeams() const;

  
  int saveSets() const;

  
  int nodSets() const;

  
  int coaddsPerFrm() const;

  
  string readoutMode() const;

  
  string dataLabel() const;


  
  int setNodBeams(int nb);

  
  int setChopBeams(int cb);

  
  int setSaveSets(int ss);

  
  int setNodSets(int ns);

  
  int setCoaddsPerFrm(int coadds);

  
  int setReadoutMode(const string& rdout);

  
  int reLabel(const string& newLabel);

  // evaluate/set bits:

  
  int buffId(int frmIdx= 0) const;

  
  bool clearBuff(int frmIdx= 0) const;

  
  bool doDiff1(int frmIdx= 0) const;

  
  bool doDiff2(int frmIdx= 0) const;

  
  bool doAccumDiff1(int frmIdx= 0) const;

  
  bool doAccumDiff2(int frmIdx= 0) const;

  
  bool doSig(int frmIdx= 0) const;

  
  int nodPosition(int frmIdx= 0) const;

  
  int chopPosition(int frmIdx= 0) const;

  
  int detectorId(int frmIdx= 0) const;


  
  int setBuffId(int id, int frmIdx= 0);

  
  bool setClear(bool s, int frmIdx= 0);

  
  bool setDiff1(bool s, int frmIdx= 0);

  
  bool setDiff2(bool s, int frmIdx= 0);

  
  bool setAccumDiff1(bool s, int frmIdx= 0);

  
  bool setAccumDiff2(bool s, int frmIdx= 0);

  
  bool setSig(bool s, int frmIdx= 0);

  
  int setNodPosition(int pos, int frmIdx= 0) const;

  
  int setChopPosition(int pos, int frmIdx= 0) const;

  
  int setDetectorId(int id, int frmIdx= 0) const;

  // additional static function supports configuration info i/o
  // between client and server: not intrinsically part of the UFProtocol
  // return <= 0 on failure, num. bytes i/o on success

  
  static int reqConfig(UFSocket& soc, UFObsConfig& c); // resets c

  
  static int recvConfig(UFSocket& soc, UFObsConfig& c); // resets c
};
    
#endif // UFObsConfig
    
  /**
   *
   *@param obsmode TBD
   *@param nb TBD
   *@param cb TBD
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@param fc TBD
   *@return TBD
   */
  /**
   *
   *@param frmIdx TBD
   *@param frame TBD
   *@param bufs TBD
   *@param fc TBD
   *@param freeFrm TBD
   *@return TBD
   */
  /**
   *
   *@param instrum TBD
   *@param names TBD
   *@return TBD
   */
  /**
   *
   *@param names TBD
   *@return TBD
   */
  /**
   *
   *@param name TBD
   *@return TBD
   */
  /**
   *
   *@param nb TBD
   *@param cb TBD
   *@param ss TBD
   *@param ns TBD
   *@param cpf TBD
   *@param vals TBD
   */
  /**
   *
   *@param vals TBD
   *@param elem TBD
   */
  /**
   *
   *@param nb TBD
   *@param cb TBD
   *@param ss TBD
   *@param ns TBD
   *@param cpf TBD
   *@param vals TBD
   */
  /**
   *
   *@param vals TBD
   *@param elem TBD
   */
  /**
   *
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@param pa TBD
   */
  /**
   *
   *@param nb TBD
   *@param cb TBD
   *@param ss TBD
   *@param ns TBD
   *@param cpf TBD
   *@param vals TBD
   */
  /**
   *
   *@param name TBD
   *@param nb TBD
   *@param cb TBD
   *@param ss TBD
   *@param ns TBD
   *@param cpf TBD
   *@param vals TBD
   */
  /**
   *
   *@param ufs TBD
   *@param nb TBD
   *@param cb TBD
   *@param ss TBD
   *@param ns TBD
   *@param cpf TBD
   */
  /**
   *
   *@param ufs TBD
   */
  /**
   *
   *@param ufs TBD
   */
  /**
   *
   *@param ufoc TBD
   */
  /**
   *
   *@param ufoc TBD
   */
  /**
   *
   *@param name TBD
   *@param vals TBD
   *@param elem TBD
   *@param shared TBD
   */
  /**
   *
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@param c TBD
   *@return TBD
   */
  /**
   *
   *@param c TBD
   *@return TBD
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@param nb TBD
   *@return TBD
   */
  /**
   *
   *@param cb TBD
   *@return TBD
   */
  /**
   *
   *@param ss TBD
   *@return TBD
   */
  /**
   *
   *@param ns TBD
   *@return TBD
   */
  /**
   *
   *@param coadds TBD
   *@return TBD
   */
  /**
   *
   *@param rdout TBD
   *@return TBD
   */
  /**
   *
   *@param newLabel TBD
   *@return TBD
   */
  /**
   *
   *@param frmIdx TBD
   *@return TBD
   */
  /**
   *
   *@param frmIdx TBD
   *@return TBD
   */
  /**
   *
   *@param frmIdx TBD
   *@return TBD
   */
  /**
   *
   *@param frmIdx TBD
   *@return TBD
   */
  /**
   *
   *@param frmIdx TBD
   *@return TBD
   */
  /**
   *
   *@param frmIdx TBD
   *@return TBD
   */
  /**
   *
   *@param frmIdx TBD
   *@return TBD
   */
  /**
   *
   *@param frmIdx TBD
   *@return TBD
   */
  /**
   *
   *@param frmIdx TBD
   *@return TBD
   */
  /**
   *
   *@param frmIdx TBD
   *@return TBD
   */
  /**
   *
   *@param id TBD
   *@param frmIdx TBD
   *@return TBD
   */
  /**
   *
   *@param s TBD
   *@param frmIdx TBD
   *@return TBD
   */
  /**
   *
   *@param s TBD
   *@param frmIdx TBD
   *@return TBD
   */
  /**
   *
   *@param s TBD
   *@param frmIdx TBD
   *@return TBD
   */
  /**
   *
   *@param s TBD
   *@param frmIdx TBD
   *@return TBD
   */
  /**
   *
   *@param s TBD
   *@param frmIdx TBD
   *@return TBD
   */
  /**
   *
   *@param s TBD
   *@param frmIdx TBD
   *@return TBD
   */
  /**
   *
   *@param pos TBD
   *@param frmIdx TBD
   *@return TBD
   */
  /**
   *
   *@param pos TBD
   *@param frmIdx TBD
   *@return TBD
   */
  /**
   *
   *@param id TBD
   *@param frmIdx TBD
   *@return TBD
   */
  /**
   *
   *@param soc TBD
   *@param c TBD
   *@return TBD
   */
  /**
   *
   *@param soc TBD
   *@param c TBD
   *@return TBD
   */

#if !defined(__UFFrames_h__)
#define __UFFrames_h__ "$Name:  $ $Id: UFFrames.h,v 0.2 2004/01/28 20:30:24 drashkin beta $"
#define __UFFrames_H__(arg) const char arg##Frames_h__rcsId[] = __UFFrames_h__;

#include "UFFrameConfig.h"
#include "UFBytes.h"
#include "UFShorts.h"
#include "UFInts.h"
#include "UFFloats.h"
#include "UFStrings.h"

// Frames Protocol Msg contains one or more frames
// that are internally stored and transmitted as
// either an UFBytes or UFShorts or UFInts or UFFloats object
// note that this inherits from UFFRameConfig, but
// overrides some of the standard protocol virtual
// functions to make use of _data info.

class UFFrames: public virtual UFFrameConfig {
protected:
  UFProtocol* _data; // points to "shallow" data
  // data->elements() == _req.elements() count!
  // client designates buffer names, server appends buffer timestamps to names
  // to indicate success or failure to meet request
  UFStrings* _req; // requested buffer names


  
  inline UFFrames() : UFFrameConfig(), _data(0), _req(0) {}

public:

  
  inline const UFStrings& reqNamesRef() const { return (const UFStrings&) *_req; }

  // for use by client

  
  static int reqFrames(UFSocket& soc, UFFrames& f); // resets f
  // for use by server

  
  static int recvReq(UFSocket& soc, UFFrames& f); // resets f
  // helpers for formulating requests

  
  static int allBuffNames(vector< string >& vreq);

  
  static int defaultStareModeReq(vector< string >& vreq);

  
  static int defaultNodModeReq(vector< string >& vreq);

  
  static int defaultChopModeReq(vector< string >& vreq);

  
  static int defaultChopNodModeReq(vector< string >& vreq);

  
  int reqBuffNames(vector< string >& vreq) const;

  
  int setReqBuffNames(vector< string >& vreq);

  // for use by UFProtocol::create

  
  UFFrames(const UFProtocolAttributes& pa);
  // and these can be used with UFBytes, UFInts, UFShorts, and UFFloats

  
  UFFrames(const UFFrameConfig& c, UFProtocol* ufp=0);

  
  UFFrames(UFProtocol* ufp, int w, int h, int d=32, bool littleEnd=false);
  // copy ctor is also shallow

  
  UFFrames(const UFFrames& rhs);

  // UFProtocol dtor now uses switch/case factory 

  
  inline virtual ~UFFrames() { delete _data; delete _req; }

  
  inline virtual string description() const { return string(__UFFrames_h__); }

  // this inherits from UFFrameConfig and thus UFInts (of 4 elements)
  // but override to actually use _data functions
  // need to retain access to the FrameConfig header info:

  
  inline int configLength() const { return UFFrameConfig::length(); }

  
  inline const char* configCname() const { return  UFFrameConfig::cname(); }

  
  inline string configName() const { return UFFrameConfig::name(); }

  
  inline const char* configTimestamp() const { return UFFrameConfig::timestamp(); }

  
  inline string configTimeStamp() const { return UFFrameConfig::timeStamp(); }

  // override these to access _data

  
  virtual int length() const;
  // fetch name

  
  virtual const char* cname() const;

  
  virtual string name() const;
  // reset name

  
  virtual string rename(const string& newname);
  // fetch timestamp

  
  virtual const char* timestamp() const;

  
  virtual string timeStamp() const;
  // reset timestamp

  
  virtual string stampTime(const string& newtime);
  // return # of elements (frame count)

  
  virtual int elements() const;
  // return size of the value element

  
  virtual int valSize(int elemIdx=0) const;
  // return int* pointer to first byte of _value (data) array:

  
  virtual const char* valData(int elemIdx=0) const;
  // number of values should not be confused with number of elements,
  // this must be congruent with the memory allocation addressed by the
  // _values pointer.

  
  virtual int numVals() const;


  
  virtual void shallowCopy(const UFProtocol& rhs);

  
  virtual void deepCopy(const UFProtocol& rhs);
  // copy internal values to external buff

  
  virtual void copyVal(char* dst, int elemIdx=0);

  // write out to file descriptor, returns 0 on failure, num. bytes output on success

  
  virtual int writeTo(int fd) const; 

  
  virtual int writeValues(int fd) const;
  // read/restore from file descriptor, support reuse of existing object

  
  virtual int readFrom(int fd);

  
  virtual int readValues(int fd); 

  // write msg out to socket, returns 0 on failure, num. bytes on success

  
  virtual int sendTo(UFSocket& soc) const; 

  
  virtual int sendValues(UFSocket& soc) const;

  
  virtual int sendAsReqTo(UFSocket& soc);
  // support reuse of existing object

  
  virtual int recvFrom(UFSocket& soc);

  
  virtual int recvValues(UFSocket& soc);
};

#endif // __UFFrames_h__


  /**
   *
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@param soc TBD
   *@param f TBD
   *@return TBD
   */
  /**
   *
   *@param soc TBD
   *@param f TBD
   *@return TBD
   */
  /**
   *
   *@param vreq TBD
   *@return TBD
   */
  /**
   *
   *@param vreq TBD
   *@return TBD
   */
  /**
   *
   *@param vreq TBD
   *@return TBD
   */
  /**
   *
   *@param vreq TBD
   *@return TBD
   */
  /**
   *
   *@param vreq TBD
   *@return TBD
   */
  /**
   *
   *@param vreq TBD
   *@return TBD
   */
  /**
   *
   *@param vreq TBD
   *@return TBD
   */
  /**
   *
   *@param pa TBD
   */
  /**
   *
   *@param c TBD
   *@param ufp TBD
   */
  /**
   *
   *@param ufp TBD
   *@param w TBD
   *@param h TBD
   *@param d TBD
   *@param littleEnd TBD
   */
  /**
   *
   *@param rhs TBD
   */
  /**
   *
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@param newname TBD
   *@return TBD
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@param newtime TBD
   *@return TBD
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@param elemIdx TBD
   *@return TBD
   */
  /**
   *
   *@param elemIdx TBD
   *@return TBD
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@param rhs TBD
   */
  /**
   *
   *@param rhs TBD
   */
  /**
   *
   *@param dst TBD
   *@param elemIdx TBD
   */
  /**
   *
   *@param fd TBD
   *@return TBD
   */
  /**
   *
   *@param fd TBD
   *@return TBD
   */
  /**
   *
   *@param fd TBD
   *@return TBD
   */
  /**
   *
   *@param fd TBD
   *@return TBD
   */
  /**
   *
   *@param soc TBD
   *@return TBD
   */
  /**
   *
   *@param soc TBD
   *@return TBD
   */
  /**
   *
   *@param soc TBD
   *@return TBD
   */
  /**
   *
   *@param soc TBD
   *@return TBD
   */
  /**
   *
   *@param soc TBD
   *@return TBD
   */

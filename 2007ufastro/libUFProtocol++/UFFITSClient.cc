#if !defined(__UFFITSClient_cc__)
#define __UFFITSClient_cc__ "$Name:  $ $Id: UFFITSClient.cc 14 2008-06-11 01:49:45Z hon $";
const char rcsId[] = __UFFITSClient_cc__;

#include "UFFITSClient.h"
#include "UFClientSocket.h"

bool UFFITSClient::_verbose = false;
bool UFFITSClient::_observatory = false;
string UFFITSClient::_host;

#define UFMAXTRY 3

int UFFITSClient::main(const string& name, int argc, char** argv, char** envp) {
  // unit test main 
  UFFITSClient::AgentLoc loc;
  UFFITSClient ufc("unit test", argc, argv, envp);

  int _loop= 1;
  string arg = ufc.findArg("-loop");
  if( arg == "true" ) // _loop == 0 means forever
    _loop = 0;
  else if( arg != "false" )
    _loop = atoi(arg.c_str());

  arg = ufc.findArg("-l");
  if( arg == "true" ) // _loop == 0 means forever
    _loop = 0;
  else if( arg != "false" )
    _loop = atoi(arg.c_str());

  arg = ufc.findArg("-host");
  if( arg != "false" && arg != "true" )
    _host = arg;
  else
    _host = ufc.hostname();
 
  arg = ufc.findArg("-noobs");
  if( arg == "true" )
    _observatory = false;
 
  arg = ufc.findArg("-v");
  if( arg == "true" )
    UFSocket::_verbose = true; 

  int port= -1; // all or single agent invocation
  arg = ufc.findArg("-port");
  if( arg != "true" && arg != "false" )
    port = atoi(arg.c_str());

  if( port > 0 && port < 10 ) // allow shorthand
    port += 52000;

  // reset agent locations with _host:
  ufc.locateAgents(_host, loc, port);

  UFSocket::ConnectTable connections;
  int ncon= 0;
  while( ncon <= 0 ) {
    ncon = ufc.connectAgents(loc, connections, port);
    if( ncon <= 0 ) {
      if( _verbose )
        clog<<"UFFITSClient::main> no connections yet, sleep & retry..."<<endl;
      UFRuntime::mlsleep(0.01);
    }
  }

  int cnt= 0;
  while( _loop == 0 || ++cnt <= _loop ) {
    ufc.fetchAndPrint(connections);
    system("date +%Y:%j:%H:%M:%S");
  }

  return 0;
}

// create a nominal FITS header and write it to stdout
int UFFITSClient::fetchAndPrint(UFSocket::ConnectTable& connections, float timeOut) { 
  int n= 0;
  UFStrings* s = fetchAllFITS(connections, timeOut, _observatory);
  if( s != 0 ) {
    n = s->numVals();
    cout<<"UFFITSClient::fetchAndPrint> numvals: "<<n<<endl;
    for( int i = 0; i < n; ++i )
      if( (*s)[i].find("=") != string::npos )
        cout<<(*s)[i]<<endl;
  }
  delete s;
  return n;
}

UFStrings* UFFITSClient::fetchAllFITS(UFSocket::ConnectTable& connections, float timeOut, bool obs) {
  FITSElements vals, coms;
  UFFITSheader* fits = new UFFITSheader();

  // get the agent stuff
  UFStrings* s = fetchAgentsFITS(connections, timeOut);
  if( s )
    fits->add( s );

  if( obs ) { // set the observatory stuff
    int nobs = observatoryFITS(vals, coms);
    if( nobs > 0 )
      fits->add(vals, coms);
  }

  delete s;
  fits->end();
  s = fits->Strings(name());
  delete fits;

  return s;
}

UFStrings* UFFITSClient::fetchFITS(UFSocket* soc, float timeOut) {
  vector< string > statfits;
  statfits.push_back("status");
  statfits.push_back("fits");
  UFStrings s(name(), statfits);
  
  int n = s.sendTo(*soc);
  if( n <= 0 )
    return 0;

  // this request should result in a UFStrings reply:  
  return dynamic_cast< UFStrings* > ( UFProtocol::createFrom(*soc, timeOut) );
}

UFStrings* UFFITSClient::fetchAgentsFITS(UFSocket::ConnectTable& connections, float timeOut) {
  UFSocket::ConnectTable::iterator it = connections.begin();
  // quicker to first send out all the requests...
  string agent;
  UFSocket* soc= 0;
  vector< string > statfits;
  statfits.push_back("status");
  statfits.push_back("fits");
  UFStrings req(name(), statfits);
  while( it != connections.end() ) {
    agent = it->first;
    soc = it->second;
    if( soc != 0 )
      req.sendTo(*soc);
    ++it;
  }
  // then recv all the replies
  it = connections.begin();
  UFProtocol* ufp= 0;
  UFStrings* s= 0;
  vector< string > all;
  while( it != connections.end() ) {
    agent = it->first;
    soc = it->second;
    ++it;
    if( soc == 0 ) continue;
    ufp = UFProtocol::createFrom(*soc, timeOut);
    if( ufp == 0 ) {
      clog<<"UFFITSClient::fetchAgentsFITS> "<<agent<<" did not respond to FITS request, will sleep a bit and try again..."<<endl;
      int trycnt = UFMAXTRY;
      do {
	if( ufp == 0 && timeOut > 0.00001 ) UFRuntime::mlsleep(timeOut);
	if( ufp == 0 && timeOut < -0.00001 ) UFRuntime::mlsleep(0.01);
        ufp = UFProtocol::createFrom(*soc, timeOut);
      } while ( ufp == 0 && --trycnt > 0 );
    }
    if( ufp == 0 ) {
      clog<<"UFFITSClient::fetchAgentsFITS> "<<agent<<" did not respond to FITS request after "<<UFMAXTRY<<" attempts..."<<endl;
      continue; 
    }
    s = dynamic_cast< UFStrings* > (ufp);
    if( s == 0 ) {
      clog<<"UFFITSClient::fetchAgentsFITS> dynamic cast to UFStrings failed..."<<endl;
      continue; 
    }
    if( _verbose )
      clog<<"UFFITSClient::fetchAgentsFITS> "<<agent<<" returned FITS elements: "<<s->numVals()<<endl;
    for( int i = 0; i < s->numVals(); ++i ) {
      //clog<<agent<<" : "<<(*s)[i]<<endl;
      all.push_back((*s)[i]);
    }
    delete s; s = 0;
  }			    
  if( all.size() <= 0 )
    return 0;

  return new UFStrings(name(), all);
}

int UFFITSClient::locateAgents(const string& host, UFFITSClient::AgentLoc& loc, int port) {
  _host = host;

  AgentLocation* agentloc = new AgentLocation(_host, 52000);
  string agentname = "ufgedtd"; // executive
  //if( port == 52000 || port < 0 )
   // loc[agentname] = agentloc; 
  agentloc = new AgentLocation(_host, 52001);
  agentname = "ufgflam2d"; // executive
  //if( port == 52001 || port < 0 )
   // loc[agentname] = agentloc; 

  agentloc = new AgentLocation(_host, 52002);
  agentname = "ufgls218d"; // lakeshore 218
  if( port == 52002 || port < 0 )
    loc[agentname] = agentloc; 

  agentloc = new AgentLocation(_host, 52003);
  agentname = "ufgls33xd"; // lakeshore 331,2
  if( port == 52003 || port < 0 )
    loc[agentname] = agentloc; 

  agentloc = new AgentLocation(_host, 52004);
  agentname = "ufgpf26xvacd"; // pfeiffer 261,2
  if( port == 52004 || port < 0 )
    loc[agentname] = agentloc; 

  agentloc = new AgentLocation(_host, 52005);
  agentname = "ufgsymbarcd"; // symbol barcode 
  if( port == 52005 || port < 0 )
    loc[agentname] = agentloc;

  agentloc = new AgentLocation(_host, 52006);
  agentname = "ufglvdtd"; // schaevitz lvdt
  if( port == 52006 || port < 0 )
    loc[agentname] = agentloc;

  agentloc = new AgentLocation(_host, 52007);
  agentname = "ufgisplcd"; // automation plc
  if( port == 52007 || port < 0 )
    loc[agentname] = agentloc;

  agentloc = new AgentLocation(_host, 52008);
  agentname = "ufgmce4d"; // gatir vme "mce4"
  if( port == 52008 || port < 0 )
    loc[agentname] = agentloc;

  agentloc = new AgentLocation(_host, 52024);
  agentname = "ufgmotord"; // portescaps
  if( port == 52024 || port < 0 )
    loc[agentname] = agentloc; 

  return (int)loc.size();
}

int UFFITSClient::connectAgents(UFFITSClient::AgentLoc& loc, UFSocket::ConnectTable& connections, int port, bool block) {
  if( loc.empty() ) {
    locateAgents(UFRuntime::hostname(), loc);
  }
  if( _verbose )
    clog<<"UFFITSClient::connectAgents> connecting to agent(s)... "<<endl;
  string agents, name;
  UFFITSClient::AgentLoc::iterator it = loc.begin();
  while( it != loc.end() ) {
    name = it->first;
    agents += name + " ";
    UFFITSClient::AgentLocation* aloc = it->second;
    if( aloc == 0 ) continue;
    if( _verbose )
      clog<<"UFFITSClient::connectAgents> connect to "<<name<<", on port: "<<aloc->port<<endl;
    UFSocket* soc = 0;
    int trycnt = UFMAXTRY;
    if( block ) trycnt = 1;
    do {
      soc = connect(name, *aloc, block); // default block = false
      if( trycnt > 0  && soc == 0 ) UFRuntime::mlsleep(0.01);
    } while( soc == 0 && --trycnt > 0 ); 
    if( soc != 0 ) {
      connections[name] = soc;
      if( _verbose )
        clog<<"UFFITSClient::connectAgents> connected to "<<name<<endl;
    }
    else {
      clog<<"UFFITSClient::connectAgents> failed to connect to "<<name<<endl;
    }
    ++it;
  }

  if( connections.size() > 0 )
    clog<<"UFFITSClient::connectAgents> agents: "<<agents<<endl;

  return connections.size();
}
      
UFSocket* UFFITSClient::connect(const string& agent, const UFFITSClient::AgentLocation& aloc, bool block) {
  UFClientSocket* soc = new UFClientSocket();
  // generally all ufsockets should be set to block=true, with the possible exception
  // of the initial connection 
  // if block is false, this should set the socket accordingly, then reset it to true
  if( _verbose ) 
    clog<<"UFFITSClient::connect> host: "<<aloc.host<<", port: "<<aloc.port<<" , block: "<<block<<endl;
  int fd = soc->connect(aloc.host, aloc.port, block); // default block = false
  if( fd < 0 ) {
    if( block )
      clog<<"UFFITSClient::connect> client soc. connect failed, block= "<<block<<endl;
    delete soc;
    return 0;
  }
  /*
  if( !soc->validConnection() ) {
    delete soc;
    return 0;
  }
  */
  UFTimeStamp greet(name());
  int ns = soc->send(greet);
  ns = soc->recv(greet);
  if( _verbose )
    clog<<"UFFITSClient::connect> greeting: "<< greet.name() << " " << greet.timeStamp() <<endl;

  return static_cast< UFSocket* > (soc);
}

int UFFITSClient::observatoryFITS(FITSElements& vals, FITSElements& comments) {
  clearObservatoryFITS( vals, comments );

  return (int)vals.size();
}

int UFFITSClient::clearObservatoryFITS( FITSElements& vals, FITSElements& comments) {
  // from OSCIR:
  vals.clear(); comments.clear();
  vals["AIRMASS1"] = "Start";
  vals["AIRMASS2"] = "End";
  vals["AZIMUTH1"] ="Start";
  vals["AZIMUTH2"] = "End";
  vals["AZERROR1"] = "Start";
  vals["AZERROR2"] = "End";
  //vals["CHPFREQ"] = "Start";
  vals["CHPPA"] = "Start";
  vals["CHPTHROW"] = "Start";
  vals["DATALAB"] = "Null";
  vals["EL1"] = "Start";
  vals["EL2"] = "End";
  vals["ELERROR1"] = "Start";
  vals["ELERROR2"] = "End";
  vals["TELFOCUS"] = "Start";
  vals["FRAMEPA"] = "Start";
  vals["GEMPRGID"] = "Null";
  vals["HA1"] = "Start";
  vals["HA2"] = "End";
  vals["HUMID1"] = "Start";
  vals["HUMID2"] = "End";
  vals["INSTRAA1"] = "Start";
  vals["INSTRAA2"] = "End";
  vals["INSTRPA1"] = "Start";
  vals["INSTRPA2"] = "End";
  vals["LOCTIME1"] = "Start";
  vals["LOCTIME2"] = "End";
  vals["LST1"] = "Start";
  vals["LST2"] = "End";
  vals["MJD"] = "Start";
  vals["NODMODE"] = "Start";
  vals["NODPA"] = "Start";
  vals["NODTHROW"] = "Start";
  vals["NODTYPE"] = "Start";
  vals["OBSID"] = "Null";
  vals["OBSERVAT"] = "Null";
  vals["OBSERVER"] = "Null";
  vals["DEC_OFF"] = "Start";
  vals["RA_OFF"] = "Start";
  vals["ORIGIN"] = "Null";
  vals["PIXSCALE"] = "Null";
  vals["RAWBG"] = "Null";
  vals["RAWCC"] = "Null";
  vals["RAWIQ"] = "Null";
  vals["RAWGEMQA"] = "Null";
  vals["RAWPIREQ"] = "Null";
  vals["RAWWV"] = "Null";
  vals["ROTATOR1"] = "Start";
  vals["ROTATOR2"] = "End";
  vals["ROTERR1"] = "Start";
  vals["ROTERR2"] = "End";
  vals["SSA"] = "Null";
  vals["OBJECT"] = "Start";
  vals["RA_BASE"] = "Start";
  vals["DEC_BASE"] = "Start";
  vals["EPOCH"] = "Null";
  vals["EQUINOX"] = "Start";
  vals["TARGFRM"] = "Start";
  vals["TARGSYS"] = "Start";
  vals["TELESCOP"] = "Start";
  vals["RA_TEL"] = "Start";
  vals["DEC_TEL"] = "Start";
  vals["USRFOC1"] = "Start";
  vals["USRFOC2"] = "End";
  vals["UTC1"] = "Start";
  vals["UTC2"] = "End";
  vals["DATE-OBS"] = "Start";
  vals["ZD1"] = "Start";
  vals["ZD2"] = "End";

  comments["AIRMASS1"] = "airmass;obs.start";
  comments["AIRMASS2"] = "airmass;obs.end";
  comments["AZIMUTH1"] = "telescope azimuth;obs.start;deg:min:sec";
  comments["AZIMUTH2"] = "telescope azimuth;obs.end;deg:min:sec";
  comments["AZERROR1"] = "azimuth error;obs.start;deg:min:sec";
  comments["AZERROR2"] = "azimuth error;obs.end;deg:min:sec";
  comments["CHPFREQ"] = "secondary chopperfrequency;Hz";
  comments["CHPPA"] = "secondary chopperpa;deg";
  comments["CHPTHROW"] = "secondary chopperthrow;arcsec";
  comments["DATALAB"] = "Gemini data label";
  comments["EL1"] = "telescope elevation;obs.start;deg:min:sec";
  comments["EL2"] = "telescope elevation;obs.end;deg:min:sec";
  comments["ELERROR1"] = "elevation error;obs.start;deg:min:sec";
  comments["ELERROR2"] = "elevation error;obs.end;deg:min:sec";
  comments["TELFOCUS"] = "telescope focus;mm";
  comments["FRAMEPA"] = "frame of reference of instrument pa";
  comments["GEMPRGID"]= "Gemini science program ID";
  comments["HA1"] = "hour angle;obs.start;hr:min:sec";
  comments["HA2"] = "hour angle;obs.end;hr:min:sec";
  comments["HUMID1"] = "%rel.humidity;obs.start";
  comments["HUMID2"] = "%rel.humidity;obs.end";
  comments["INSTRAA1"] = "instrument alignmentangle;obs.start;deg";
  comments["INSTRAA2"] = "instrument alignmentangle;obs.end;deg";
  comments["INSTRPA1"] = "instrumentpa;obs.start;deg";
  comments["INSTRPA2"] = "instrumentpa;obs.start;deg";
  comments["LOCTIME1"] = "hrminsec;obs.start";
  comments["LOCTIME2"] = "hrminsec;obs.end";
  comments["LST1"] = "hrminsec;obs.start";
  comments["LST2"] = "hrminsec;obs.end";
  comments["MJD"]= "mod.juliandate";
  comments["NODMODE"] = "nodmode;e.g. standard or offset";
  comments["NODPA"] = "nodpa;deg";
  comments["NODTHROW"] = "nodthrow;arcsec";
  comments["NODTYPE"] = "nodtype;e.g. radec or azel";
  comments["OBSID"] = "Gemini observation ID";
  comments["OBSERVAT"] = "Gemini observatory";
  comments["OBSERVER"] = "Observers";
  comments["DEC_OFF"] = "dec offset from base;arcsec";
  comments["RA_OFF"] = "raoffsetfrombase;arcsec";
  comments["ORIGIN"] = "Gemini observatory?";
  comments["PIXSCALE"] = "Plate scale at detector;arcsec/pixel";
  comments["RAWBG"] = "Gemini raw background";
  comments["RAWCC"] = "Gemini raw cloud cover";
  comments["RAWIQ"] = "Gemini raw imagequality";
  comments["RAWGEMQA"] = "Gemini raw quality assessment";
  comments["RAWPIREQ"] = "Gemini raw PI requirements met";
  comments["RAWWV"] = "Gemini raw water vapour/transparency";
  comments["ROTATOR1"] = "rotator mech.angle;obs.start;deg";
  comments["ROTATOR2"] = "rotator mech.angle;obs.end;deg";
  comments["ROTERR1"] = "rotator error;obs.start;deg";
  comments["ROTERR2"] = "rotator error;obs.start;deg";
  comments["SSA"] = "Gemini SSAs";
  comments["OBJECT"] = "target name";
  comments["RA_BASE"] = "base ra of target;deg";
  comments["DEC_BASE"] = "base dec of target;deg";
  comments["EPOCH"] = "epoch of target coordinates";
  comments["EQUINOX"] = "equinox of target coordinates";
  comments["TARGFRM"] = "target frame;e.g.FK5";
  comments["TARGSYS"] = "target coordinate frame;e.g.FK5";
  comments["TELESCOP"] = "telescope ID";
  comments["RA_TEL"] = "RA of telescope pointing origin;hr:min:sec";
  comments["DEC_TEL"] = "Dec of telescope pointing origin;deg:min:sec";
  comments["USRFOC1"] = "Offset from nominal focus;obs.start";
  comments["USRFOC2"] = "Offset from nominal focus;obs.end";
  comments["UTC1"] = "UTtime;obs.start;hrmin";
  comments["UTC2"] = "UTtime;obs.end;hrmin";
  comments["DATE-OBS"] = "yyyymmmdd";
  comments["ZD1"] = "zenith distance;obs.start;deg";
  comments["ZD2"] = "zenith distance;obs.end;deg";

  return vals.size();
}

#endif // __UFFITSClient_cc__

#if !defined(__UFStrings_ce__)
#define __UFStrings_cc__ "$Name:  $ $Id: UFStrings.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFStrings_cc__;

#include "UFStrings.h"
__UFStrings_H__(__UFStrings_cc);
#include "UFProtocol.h"
__UFProtocol_H__(__UFProtocol_cc);

#include "cstdio"
#include "sys/types.h"
#include "sys/stat.h"

// static convenience funcs:
int UFStrings::upperCase(string& s) {
  char* cs = (char*) s.c_str();
  char* tmp = cs;
  for( size_t i = 0; i < s.length(); ++i, ++tmp )
    *tmp = ::toupper(*tmp);
 
  s = cs;
  return s.length();
}

int UFStrings::lowerCase(string& s) {
  char* cs = (char*) s.c_str();
  char* tmp = cs;
  for( size_t i = 0; i < s.length(); ++i, ++tmp )
    *tmp = ::tolower(*tmp);
 
  s = cs;
  return s.length();
}

UFStrings::~UFStrings() {}

int UFStrings::upperCase() {
  for(int i = 0; i < numVals(); ++i ) {
    string* s = (string *) stringAt(i);
    upperCase(*s);
  }
  return numVals(); 
}

int UFStrings::lowerCase() {
  for(int i = 0; i < numVals(); ++i ) {
    string* s = (string*) stringAt(i);
    lowerCase(*s);
  }
  return numVals(); 
}
 
int UFStrings::clear() {
  if( _pa == 0 )
    return 0;

  string* p = static_cast< string* > (_pa->_values);
  delete [] p; _pa->_values = 0;
  _pa->_elem = _pa->_seqCnt = _pa->_seqTot =  0;
  _pa->_duration = 0.0;
  _pa->_length = minLength();

  return elements();
}

int UFStrings::setWith(const vector< string >& vals, float duration, int seq, int seqT) {
  if( _pa == 0 ) { // this is a first-time-ever construction
    _paInit(_Strings_, "UFStrings");
    _pa->_elem = (int) vals.size();
    _pa->_values = (void*) new (nothrow) string[_pa->_elem];
  }
  // otherwise assume _pa has been at least partially set at least once
  // and this either a re-use or a createFrom (file or socket)
  else if( _pa->_values != 0 && _pa->_elem < (int) vals.size() ) {
    // clearly need to re-size
    string* p = static_cast< string* > (_pa->_values);
    delete [] p; // ok to delete 0
    _pa->_elem = vals.size();
    _pa->_values = (void*) new (nothrow) string[_pa->_elem];
  } // resize
  else if( vals.size() > 0 || _pa->_values == 0 ) {
    // or first-time create or allocate?
    if(_pa->_elem <= 0 ) // never been set?
      _pa->_elem = (int) vals.size();
    _pa->_values = (void*) new (nothrow) string[_pa->_elem];
  }

  _pa->_seqCnt = seq;
  _pa->_seqTot = seqT;
  _pa->_duration = duration;

  if( _pa->_values == 0 ) { // empty UFStrings, perhaps due to failed alloc. 
    _pa->_elem = 0;
    _currentTime();
    return 0;
  }

  // support "unitialized" settings or partial ctors
  string* p = static_cast< string* > (_pa->_values); 
  int elem = min(_pa->_elem, (int)vals.size());
  for( int i = 0; i < elem; i++ ) {
    p[i] = vals[i];
    // increment transmition length
    _pa->_length += sizeof(int) + vals[i].length();
  }
      
  _currentTime();
  return elements();
}

UFStrings::UFStrings(const UFProtocolAttributes& pa) : UFTimeStamp() {
  _paInit(pa);
  vector< string > vals; // empty vector for "unitialized" settings
  setWith(vals);
}

UFStrings::UFStrings(const string& name, char* val, int elem) {
  _paInit(_Strings_, name);
  vector< string > vals; 
  vals.push_back(val);
  setWith(vals);
  _currentTime();
}

UFStrings::UFStrings(const string& name, int length) : UFTimeStamp() {
  _paInit(_Strings_, name, length);
  vector< string > vals; // empty vector for "unitialized" settings
  setWith(vals);
  _currentTime();
}

// deep copy
void UFStrings::deepCopy(const UFProtocol& rhsP) {
  _shallow = false;
  const UFStrings& rhs = dynamic_cast<const UFStrings&>(rhsP);
  UFProtocolAttributes pa;
  pa = *_pa; // save current header settings, this assigns _elem as well as _values, etc.
  *_pa = *rhs._pa; // new protocal attribute header

  // support reuse, if current allocation is sufficient;
  // if not, delete current allocation and allocate larger buffer
  if( !_shared && pa._elem < _pa->_elem ) {
    delete[] static_cast< string* >( pa._values ) ;
    _pa->_values = (void*) new (nothrow) string[_pa->_elem];
  }

  if( _pa->_values == 0 ) { // empty UFStrings, perhaps due to failed alloc. 
    _pa->_elem = 0;
    return;
  }

  for( int i=0; i < _pa->_elem; i++ )
    ((string*)_pa->_values)[i] = ((string*)rhs._pa->_values)[i];
}

// shallow copy ctors
UFStrings::UFStrings(const UFStrings& rhs) : UFTimeStamp() {
  _paInit(_Strings_);
  _shallow = true;
  *_pa = *rhs._pa;
  vector< string > vals; // empty vector for "unitialized" settings
  setWith(vals);
  _currentTime();
}

UFStrings::UFStrings(const string& name,
		     const string* vals,
		     int elem) : UFTimeStamp() {
  _paInit(_Strings_, name);
  _shallow = true;
  // reset inherited attributes
  _pa->_elem = elem;
  _pa->_length = minLength();

  _pa->_values = (void*) vals;

  for( int i=0; i<_pa->_elem; ++i ) {
    _pa->_length += sizeof(int);
    _pa->_length += ((string*)_pa->_values)[i].length();
  }
  _currentTime();
}

// deep copy, meant to be used only by ctors
// This means that the constructor does the memory allocation
void UFStrings::_copyVals(const string* vals, int elem) {
  _shallow = false;
  _pa->_elem = elem;
  _pa->_length = minLength();
  for( int i=0; i<_pa->_elem; ++i ) {
    ((string*)_pa->_values)[i] = vals[i];
    _pa->_length += sizeof(int);
    _pa->_length += ((string*)_pa->_values)[i].length();
  }
  _currentTime();
}

// deep copy, meant to be used only by ctors
// This means that the constructor does the memory allocation
void UFStrings::_copyVals(const vector< string >& vals) {
  _shallow = false ;
  _pa->_elem = vals.size();
  _pa->_length = minLength();
  for( int i=0; i<_pa->_elem; ++i ) {
    ((string*)_pa->_values)[i] = vals[i];
    _pa->_length += sizeof(int);
    _pa->_length += ((string*)_pa->_values)[i].length();
  }
  _currentTime();
}

void UFStrings::_copyVals(const deque< string >& vals) {
  _shallow = false ;
  _pa->_elem = vals.size();
  _pa->_length = minLength();
  for( int i=0; i<_pa->_elem; ++i ) {
    ((string*)_pa->_values)[i] = vals[i];
    _pa->_length += sizeof(int);
    _pa->_length += ((string*)_pa->_values)[i].length();
  }
  _currentTime();
}

UFStrings::UFStrings(const string& name,
		     string* vals,
		     int elem) : UFTimeStamp() {
  _paInit(_Strings_, name);
  _pa->_values = (void*) new (nothrow) string[elem];
  if( _pa->_values == 0 ) { // empty UFStrings, perhaps due to failed alloc. 
    _pa->_elem = 0;
    _currentTime();
    return;
  }
  _copyVals(vals, elem); // does a deep copy
  _currentTime();
}

UFStrings::UFStrings(const string& name,
		     const vector< string >& vals) : UFTimeStamp() {
  _paInit(_Strings_, name);
  int nelem = (int)vals.size();
  if( nelem <= 0 )
    _pa->_values = 0;
  else {
    _pa->_values = (void*) new (nothrow) string[vals.size()];
    if( _pa->_values == 0 ) { // empty UFStrings, perhaps due to failed alloc. 
      _pa->_elem = 0;
      _currentTime();
      return;
    }
    _copyVals(vals); // does a deep copy
  }
  _currentTime();
}

UFStrings::UFStrings(const string& name,
		     const deque< string >& vals) : UFTimeStamp() {
  _paInit(_Strings_, name);
  int nelem = (int)vals.size();
  if( nelem <= 0 )
    _pa->_values = 0;
  else {
    _pa->_values = (void*) new (nothrow) string[vals.size()];
    if( _pa->_values == 0 ) { // empty UFStrings, perhaps due to failed alloc. 
      _pa->_elem = 0;
      _currentTime();
      return;
    }
    _copyVals(vals); // does a deep copy
  }
  _currentTime();
}

// Concatenate two UFStrings objects into single new UFStrings:
UFStrings::UFStrings( UFStrings* Head,
		      UFStrings* Tail, int startTail, string* name ) : UFTimeStamp() {
  if( name == 0 )
    _paInit( _Strings_, Head->name() );
  else
    _paInit( _Strings_, *name );

  vector< string >vals;
  for( int i=0; i < Head->elements(); i++ ) vals.push_back( *(Head->stringAt(i)) );

  if( Tail != 0) {
    if( startTail < 0 ) startTail = 0;
    for( int i=startTail; i < Tail->elements(); i++ ) vals.push_back( *(Tail->stringAt(i)) );
  }

  setWith(vals);
  _currentTime();
}

// write msg out to file descriptor, returns 0 on failure, num. bytes output on success
int UFStrings::writeTo(int fd) const {
  int retval=0;
  // check fd
  if( fd < 0 ) {
    clog<<"UFBytes::writeTo> bad fd= "<<fd<<endl;
    return retval;
  }
  struct stat st;
  if( fstat(fd, &st) != 0 ) {
    clog<<"UFBytes::writeTo> bad stat on fd= "<<fd<<endl;
    return retval;
  }

  // write out "persistent" attributes in order : _length, _type, _pa->_elem, ...
  // _pa->_name, _pa->_values
  retval += writeHeader(fd, *_pa);
  retval += writeValues(fd);

  if( retval < _pa->_length )
    clog<<"UFBytes::writeTo> ? bad write on fd= "<<fd<<endl;

  return retval;
}

// this might support compression some day
int UFStrings::writeValues(int fd) const {
  int retval=0;
  int slen;
  for( int i = 0; i < elements(); i++ ) {
    slen = htonl(valSize(i));
    retval += writeFully(fd, (unsigned char*) &slen, sizeof(slen));
    retval += writeFully(fd, (unsigned char*) valData(i), valSize(i));
  }
  return retval;
}

// read/restore from file descriptor
int UFStrings::readFrom(int fd) {
  int retval=0;
  // check fd
  if( fd < 0 ) {
    clog<<"UFStrings::readFrom> bad fd= "<<fd<<endl;
    return retval;
  }
  struct stat st;
  if( fstat(fd, &st) != 0 ) {
    clog<<"UFStrings::readFrom> bad stat on fd= "<<fd<<endl;
    return retval;
  }

  UFProtocolAttributes pa;
  int nb = readHeader(fd, pa); // sets everything except _values ptr 
  if( nb <= 0 ) {
    clog<<"UFStrings::readFrom> ? read failed, fd= "<<fd<<endl;
    return nb;
  }
  if( pa._type != typeId() ) {
    clog<<"UFStrings::readFrom> reuse problem! current type= "<<typeId()
        <<", transmitted type= "<<pa._type<<endl;
    // need to dynamically recast to new type (use convertions ctor)
    // later...
  }
  if( pa._elem <= 0 ) { // more problems with transmitted type?
    return retval;
  }
  else if( _pa->_values == 0 || (_pa->_elem < pa._elem && !_shared) ) {
    delete[] static_cast< string* >( _pa->_values ); // delete and re-allocate
    *_pa = pa; // resets everything including _values ptr
    _pa->_values = pa._values = (void*) new (nothrow) string[_pa->_elem];
  }
  else { // reuse existing allocation (string pointers)
    void* vals = _pa->_values; // re-use values alloc.
    *_pa = pa; // resets everything including _values ptr (which is not really set)
    _pa->_values = vals;
  }
  retval = nb;
  // virtual read
  if( _pa->_values == 0 ) { // empty UFStrings, perhaps due to failed alloc. 
    _pa->_elem = 0;
    _currentTime();
    return retval;
  }
  retval += readValues(fd); // must complete the read otherwise file synch is lost

  if( retval < _pa->_length ) {
    clog<<"UFStrings::readValues> bad read on fd= "<<fd<<endl;
  }

  return retval;
}

// read/restore from file descriptor
// someday support decompression
int UFStrings::readValues(int fd) {
  int retval=0;
  int slen;
  char* tmp=0;

  for( int i = 0; i < elements(); i++ ) {
    retval += readFully(fd, (unsigned char*) &slen , sizeof(slen));
    slen = ntohl(slen);
    tmp = new (nothrow) char[slen+1]; memset(tmp, 0, slen+1);
    if( tmp == 0 ) break;
    retval += readFully(fd, (unsigned char*) tmp , slen);
    string s(tmp);
    ((string*) _pa->_values)[i] = s;
    delete tmp;
  }   

  return retval;
}

// write msg out to socket, returns 0 on failure, num. bytes on success
int UFStrings::sendTo(UFSocket& soc) const {
  // make use of the socket class i/o & UFprotocolclass methods
  if( UFProtocol::_verbose ) 
    clog<<"UFStrings::sendTo> elem= "<<elements()<<", name= "<<cname()<<endl;

  int retval = sendHeader(soc, *_pa);

  if( retval <= 0 ) 
    return retval;

  if( _pa->_values && _pa->_elem > 0 && _pa->_duration >= 0.0 ) {
    retval += sendValues(soc);
    if( retval < _pa->_length )
      clog<<"UFStrings::sendTo> bad send on socket= "<<soc.description()<<endl;
  }
  return retval;
}

// once header has been sent out, send the data
// this could optionally send out compressed data:
int UFStrings::sendValues(UFSocket& soc) const {
  // this send signature of UFSocket should send string length (after htonl), then string vals
  int last = _pa->_elem-1;
  //if( _pa->_elem > 1 ) UFProtocol::_sendverbose = true;
  if( UFProtocol::_verbose ) {
    clog<<"UFStrings::sendValues> 0: "<<((string*)_pa->_values)[0]<<", "<<last<<": "<<((string*)_pa->_values)[last]<<endl;
  }
  else if( UFProtocol::_sendverbose ) {
    string* s = (string*)_pa->_values;
    for( int i = 0; i < _pa->_elem; ++i, ++s ) {
      if( s != 0 )
        clog<<"UFStrings::sendValues> "<<i<<" => "<<*s<<endl;
    }
  }
  //if( _pa->_elem > 1 ) UFProtocol::_sendverbose = false;

  return soc.send((string*) _pa->_values, _pa->_elem);
}

int UFStrings::recvFrom(UFSocket& soc) {
  int retval=0;
  UFProtocolAttributes pa;
  int nb = recvHeader(soc, pa); // resets everything except _values ptr 
  if( nb <= 0 ) {
    clog<<"UFStrings::recvFrom> socket recv failed?"<<endl;
    return nb;
  }
  if( pa._type != typeId() ) {
    clog<<"UFStrings::recvFrom> reuse problem! current type= "<<typeId()
        <<", transmitted type= "<<pa._type<<endl;
    // need to dynamically recast to new type (use convertions ctor)
    // later...
  }
  if( pa._elem <= 0 ) { // must be an error?
    return retval;
  }
  if( pa._duration < 0.0 ) { // must be a "request"
    void* vp = _pa->_values;
    *_pa = pa; // reset everything but _values ptr:
    _pa->_values = vp;
    return nb; // and return without further socket input (none expected?)
  }
  else if( _pa->_values == 0 || (_pa->_elem < pa._elem && !_shared) ) {
    delete[] static_cast< string* >( _pa->_values ); // delete and re-allocate
    *_pa = pa; // reset everything including _values ptr
    _pa->_values = pa._values = (void*) new (nothrow) string[_pa->_elem];
  }
  else // reuse existing allocation
    _pa->_values = pa._values;

  retval = nb;
  if( _pa->_values == 0 ) { // empty UFStrings, perhaps due to failed alloc. 
    _pa->_elem = 0;
    _currentTime();
    return retval;
  }
  // virtual recv
  retval += recvValues(soc); // must complete the recv otherwise socket synch is lost

  if( retval < _pa->_length )
    clog<<"UFStrings::recvFrom> ? bad recv on soc= "<<soc.description()<<endl;

  //if( _pa->_elem > 1 ) UFProtocol::_recvverbose = true;
  if( UFProtocol::_recvverbose ) {
    string* s = (string*)_pa->_values;
    for( int i = 0; i < _pa->_elem; ++i, ++s ) {
      if( s != 0 )
        clog<<"UFStrings::recvFrom> "<<i<<" => "<<*s<<endl;
    }
  }
  //if( _pa->_elem > 1 ) UFProtocol::_recvverbose = false;

  return retval;
}

// read/restore values from socket, assuming header has already been read
// deal with compression later...
int UFStrings::recvValues(UFSocket& soc) {
  if(  _pa->_duration < -0.000001 || _pa->_elem <= 0 ) // no-op
    return 0;

  if( _pa->_values == 0 ) // just in case there is yet no allocation...
    _pa->_values = (void*) new (nothrow) string*[_pa->_elem];

  if( _pa->_values == 0 ) { // empty UFStrings, perhaps due to failed alloc. 
    _pa->_elem = 0;
    _currentTime();
    return 0;
  }

  int nr = soc.recv((string*) _pa->_values, _pa->_elem);
  //if( _pa->_elem > 1 ) UFProtocol::_recvverbose = true;
  if( UFProtocol::_recvverbose ) {
    string* s = (string*)_pa->_values;
    for( int i = 0; i < _pa->_elem; ++i, ++s ) {
      if( s != 0 )
        clog<<"UFStrings::recvValues> "<<i<<" => "<<*s<<endl;
    }
  }
  //if( _pa->_elem > 1 ) UFProtocol::_recvverbose = false;
  return nr;
} 

#endif // __UFStrings_cc__

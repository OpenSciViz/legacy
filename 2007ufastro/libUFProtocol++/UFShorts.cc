#if !defined(__UFShorts_cc__)
#define __UFShorts_cc__ "$Name:  $ $Id: UFShorts.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFShorts_cc__;

#include "UFShorts.h"
__UFShorts_H__(__UFShorts_cc);

#include "cstdio"
#include "sys/types.h"
#include "sys/stat.h"

// public ctors
// for new create signature
UFShorts::UFShorts(const UFProtocolAttributes& pa) : UFTimeStamp() { 
  _paInit(pa);
  if( _pa->_type != _Shorts_) {
    clog<<"UFShorts::> ? type mismatch, _pa->_type = "<< _pa->_type << endl;
    _pa->_type = _Shorts_;
  }
  if( _pa->_elem > 0 && !_shared && !_shallow ) _pa->_values = (void*) new short[_pa->_elem];
  //if( UFProtocol::_verbose ) 
    clog<<"UFShorts> _length: "<<_pa->_length<<"_elem: "<<_pa->_elem<<", _duration: "<<_pa->_length<<endl;

}

UFShorts::UFShorts(bool shared, int length) : UFTimeStamp() {
  _paInit(_Shorts_, "UFShorts", length);
  _shared = shared; // reset from default
  if( _pa->_elem > 0 && !_shared && !_shallow ) _pa->_values = (void*) new short[_pa->_elem];
  //if( UFProtocol::_verbose ) 
    clog<<"UFShorts> _length: "<<_pa->_length<<"_elem: "<<_pa->_elem<<", _duration: "<<_pa->_length<<endl;
  _currentTime();
}

UFShorts::UFShorts(const string& name,
	           bool shared,
	           int length) : UFTimeStamp() { 
  _paInit(_Shorts_, name, length);
  _shared = shared; // reset from default
  if( _pa->_elem > 0 && !_shared && !_shallow ) _pa->_values = (void*) new short[_pa->_elem];
  //if( UFProtocol::_verbose ) 
    clog<<"UFShorts> _length: "<<_pa->_length<<"_elem: "<<_pa->_elem<<", _duration: "<<_pa->_length<<endl;
  _currentTime();
}

// this (very odd & very special!) ctor supports shared memory applications
// UFProtocolAttributes* is non-const, indicating we actually want to init its contents here:
UFShorts::UFShorts(const string& name, UFProtocolAttributes* pa,
		   int elem, bool shared) : UFTimeStamp() {
  if( shared ) { // presumable pa actually point to uninitialized shared mem. seg.
    _paInit(_Shorts_, name);
    _shared = shared;
    // be sure shared memory _values points to shared memory!
    // insure shared mem. is initialized
    *pa = *_pa;
    // and reset for shared mem.
    pa->_length = minLength() + elem*sizeof(short);
    pa->_elem = elem;
    pa->_values = (void*) ((short*)&pa->_values + 1);
    delete _pa;
    _pa = pa;
  }
  else {// not shared mem., but assume pa has been initialized?
    _paInit(*pa);
    _pa->_type=_Shorts_; 
  }
  //if( UFProtocol::_verbose ) 
    clog<<"UFShorts> _length: "<<_pa->_length<<"_elem: "<<_pa->_elem<<", _duration: "<<_pa->_length<<endl;
  if( _pa->_elem != elem ) {
    clog<<"UFShorts::UFShorts> ? error in ctor, not using shared memory?"<<endl;
  }
  _currentTime();
}

// these ctor signatures do a shallow copy of the values, for efficiency
UFShorts::UFShorts(const string& name, const short* vals,
	           int elem, bool shared) : UFTimeStamp() {
  _paInit(_Shorts_, name);
  _shared = shared;
  _shallow = true;
  _pa->_elem = elem;
  _pa->_length = minLength() + elem*sizeof(short);
  _pa->_values = (void*) vals;
  //if( UFProtocol::_verbose ) 
    clog<<"UFShorts> _length: "<<_pa->_length<<"_elem: "<<_pa->_elem<<", _duration: "<<_pa->_length<<endl;
  _currentTime();
}

// shallow copy ctor
UFShorts::UFShorts(const UFShorts& rhs) : UFTimeStamp() {
  _paInit(_Shorts_, "UFShorts");
  shallowCopy(rhs);
  //if( UFProtocol::_verbose ) 
    clog<<"UFShorts> _length: "<<_pa->_length<<"_elem: "<<_pa->_elem<<", _duration: "<<_pa->_length<<endl;

  _currentTime();
}

// deep vals copy ctor:
// If pointer vals is null, it just set values to zeros.
UFShorts::UFShorts(const string& name,
		   short* vals, int elem,
		   bool shared) : UFTimeStamp() {
  _paInit(_Shorts_, name);
  _shared = shared;
  _pa->_elem = elem;
  _pa->_length = minLength() + elem*sizeof(short);

  if( elem > 0 ) {
    _pa->_values = (void*) new (nothrow) short[elem];
    if( _pa->_values == 0 ) {
       clog<<"UFShorts (deep vals copy ctor)> new alloc failed."<<endl;
       _pa->_elem = 0;
       _pa->_length = minLength();
       return;
    }
  }

  if( vals ) {
    memcpy( _pa->_values, vals, elem*sizeof(short) );
  }
  else { // null pointer passed - just set to 0s
    memset( _pa->_values, 0, _pa->_elem*sizeof(short) );
  }
  _currentTime();
  //if( UFProtocol::_verbose ) 
    clog<<"UFShorts> _length: "<<_pa->_length<<"_elem: "<<_pa->_elem<<", _duration: "<<_pa->_length<<endl;
}

// deep copy
void UFShorts::deepCopy(const UFProtocol& rhsP) {
  _shallow = false;
  const UFShorts& rhs = dynamic_cast<const UFShorts&>(rhsP);
  UFProtocolAttributes pa;
  pa = *_pa; // save current settings
  *_pa = *rhs._pa;

  // support reuse, if current allocation is sufficient;
  // if not, delete current allocation and allocate larger buffer
  if( !_shared && pa._elem < rhs._pa->_elem ) {
    delete[] static_cast< short* >( _pa->_values ) ;
    _pa->_values = (void*) new short[_pa->_elem];
    _pa->_elem = rhs._pa->_elem;
  }

  for( int i=0; i<_pa->_elem; i++ )
    ((short*)_pa->_values)[i] = ((short*)rhs._pa->_values)[i];

  //if( UFProtocol::_verbose ) 
    clog<<"UFShorts>::deepCopy> _length: "<<_pa->_length<<"_elem: "<<_pa->_elem<<", _duration: "<<_pa->_length<<endl;

}

// write msg out to file descriptor, returns 0 on failure, num. bytes output on success
int UFShorts::writeTo(int fd) const {
  int retval=0;
  // check fd
  if( fd < 0 ) {
    clog<<"UFShorts::writeTo> bad fd= "<<fd<<endl;
    return retval;
  }
  struct stat st;
  if( fstat(fd, &st) != 0 ) {
    clog<<"UFShorts::writeTo> bad stat on fd= "<<fd<<endl;
    return retval;
  }

  // write out "persistent" attributes in order : _length, _type, _pa->_elem, ...
  // _pa->_name, _pa->_values
  retval += writeHeader(fd, *_pa);
  retval += writeValues(fd);

  if( retval < _pa->_length )
    clog<<"UFShorts::writeTo> ? bad write on fd= "<<fd<<endl;

  return retval;
}

// this might support compression some day
int UFShorts::writeValues(int fd) const {
  return writeFully(fd, (unsigned char*) _pa->_values, _pa->_elem*sizeof(short));
}

int UFShorts::readFrom(int fd) {
  int retval=0;
  // check fd
  if( fd < 0 ) {
    clog<<"UFShorts::readFrom> bad fd= "<<fd<<endl;
    return retval;
  }
  struct stat st;
  if( fstat(fd, &st) != 0 ) {
    clog<<"UFShorts::readFrom> bad stat on fd= "<<fd<<endl;
    return retval;
  }

  UFProtocolAttributes pa;
  int nb = readHeader(fd, pa); // sets everything except _values ptr 
  if( nb <= 0 ) {
    clog<<"UFShorts::readFrom> ? read failed, fd= "<<fd<<endl;
    return nb;
  }
  if( pa._type != typeId() ) {
    clog<<"UFShorts::readFrom> reuse problem! current type= "<<typeId()
        <<", transmitted type= "<<pa._type<<endl;
    // need to dynamically recast to new type (use convertions ctor)
    // later...
  }
  if( pa._elem <= 0 ) { // more problems with transmitted type?
    return retval;
  }
  else if( _pa->_values == 0 || (_pa->_elem < pa._elem && !_shared) ) {
    delete[] static_cast< short* >( _pa->_values ); // delete and re-allocate
    *_pa = pa; // ressets everything including _values ptr
    _pa->_values = pa._values = (void*) new short[_pa->_elem];
  }
  else // reuse existing allocation
    _pa->_values = pa._values;

  retval = nb;
  // virtual read
  retval += readValues(fd); // must complete the read otherwise file synch is lost

  if( retval < _pa->_length ) {
    clog<<"UFShorts::readValues> bad read on fd= "<<fd<<endl;
  }

  return retval;
}


// read/restore from file descriptor
// someday support decompression
int UFShorts::readValues(int fd) {
  return readFully(fd, (unsigned char*) _pa->_values , _pa->_elem*sizeof(short));
}

// write msg out to socket, returns 0 on failure, num. bytes on success
int UFShorts::sendTo(UFSocket& soc) const {
  // make use of the socket class i/o & UFprotocolclass methods
  int retval = sendHeader(soc, *_pa);
  if( retval <= 0 ) 
    return retval;

  if( UFProtocol::_verbose )
    clog<<"UFShorts::sendTo> _length: "<<_pa->_length<<"_elem: "<<_pa->_elem<<", sent header: "<<retval<<endl;
  if( _pa->_values && _pa->_duration >= 0.0 ) {
    retval += sendValues(soc);
    if( retval < _pa->_length ) {
      clog<<"UFShorts::sendTo> bad send on socket= "<<soc.description()<<endl;
      clog<<"UFShorts::sendTo> _length: "<<_pa->_length<<"_elem: "<<_pa->_elem<<", sent values: "<<retval<<endl;
    }
    else if( UFProtocol::_verbose ) {
      clog<<"UFShorts::sendTo> _length: "<<_pa->_length<<"_elem: "<<_pa->_elem<<", sent values: "<<retval<<endl;
    }
  }
  return retval;
}

// once header has been sent out, send the data
// this could optionally send out compressed data:
int UFShorts::sendValues(UFSocket& soc) const {
  return soc.send((short*)_pa->_values, _pa->_elem);
}

int UFShorts::recvFrom(UFSocket& soc) {
  int retval=0;
  UFProtocolAttributes pa= *_pa; // initialize to "this"
  int nb = recvHeader(soc, pa); // resets everything except _values ptr 
  if( nb <= 0 ) {
    clog<<"UFShorts::recvFrom> socket recv failed?"<<endl;
    return nb;
  }
  if( pa._type != typeId() ) {
    clog<<"UFShorts::recvFrom> reuse problem! current type= "<<typeId()
        <<", transmitted type= "<<pa._type<<endl;
    // need to dynamically recast to new type (use convertions ctor)
    // later...
  }
  if( pa._elem <= 0 ) { // more problems with transmitted type?
    return retval;
  }

  // OK to reset "this" header
  *_pa = pa; 

  if( pa._duration < 0.0 ) { // must be a "request"
    return nb; // and return without further socket input (none expected?)
  }
  else if( _pa->_values == 0 || (_pa->_elem < pa._elem && !_shared) ) {
    delete[] static_cast< short* >( _pa->_values ); // delete and re-allocate
    _pa->_values = (void*) new short[_pa->_elem];
  }

  // virtual recv
  retval = nb + recvValues(soc); // must complete the recv otherwise socket synch is lost

  if( retval < _pa->_length )
    clog<<"UFShorts::recvFrom> ? bad recv on soc= "<<soc.description()<<endl;

  return retval;
}


// read/restore values from socket, assuming header has already been read
// deal with compression later...
int UFShorts::recvValues(UFSocket& soc) {
  if( _pa->_duration >= 0.0 && _pa->_values )
    return soc.recv((short*)_pa->_values, _pa->_elem);

  return 0;
} 

#endif // __UFShorts_cc__

#if !defined(__UFFITSheader_h__)
#define __UFFITSheader_h__ "$Name:  $ $Id: UFFITSheader.h,v 0.8 2005/04/13 19:59:23 hon Exp $"
#define __UFFITSheader_H__(arg) const char arg##UFFITSheader_h__rcsId[] = __UFFITSheader_h__;

#include "UFRuntime.h"
#include "UFInts.h"
#include "UFStrings.h"
#include "UFSocket.h"
#include "UFFrameConfig.h"
#include "UFObsConfig.h"
#include "UFFlamObsConf.h"

// std c
#include "cstdio" // BUFSIZ, sprintf

// std c++
using namespace std;
#include "string"
#include "deque"
#include "vector"
#include "map"

class UFFITSheader : public deque< string > {
public:
  static int _verbose;
  //ctor to create empty FITS header:
  inline UFFITSheader() { memset( _FITSrecord, 0, BUFSIZ ); }

  //ctor to create starting standard FITS header with "SIMPLE" keyword in first record:
  UFFITSheader( const string& comment, bool simple= true );

  //ctors to create basic FITS header with "SIMPLE" and "NAXIS..." keywords (so file is readable):
  UFFITSheader( UFFrameConfig& fc, const string& comment, bool simple= true );
  UFFITSheader( UFFrameConfig& fc, UFObsConfig& oc, const string& comment, bool simple= true );
  // F2:
  UFFITSheader( UFFlamObsConf& oc, const string& comment= "F2", bool simple= false );

  inline int size() { return (int)deque< string >::size(); }
  inline string description() { return string(__UFFITSheader_h__); }

  // methods to add FITS records (keyword = value / comment) to the FITS header:
  int add( const string& keyword, const string& value, const string& comment );
  int add( const string& keyword, const int value,     const string& comment );
  int add( const string& keyword, const double value,  const string& comment );
  int replace( const string& keyword, const string& value, const string& comment );
  int replace( const string& keyword, const int value,     const string& comment );
  int replace( const string& keyword, const double value,  const string& comment );
  int add( map< string, string >& valhash, map< string, string >& comments );
  int add( map< string, int >& valhash, map< string, string >& comments );
  int add( map< string, double >& valhash, map< string, string >& comments );

  // assuming args are in FITS header entry format
  // truncation or extension to 80 chars may occur:
  int add( string& s );
  int add( UFStrings* ufs );
  int end(); // this adds the "END" record to header.
  int unique(); // ensure each entry is unique, called by end()
  int date( const string& comment="UT of header creation (YYYY:DAY:HH:MM:SS)" );

  // convert UFFITSheader object into UFStrings object for client/server transactions:
  inline UFStrings* Strings( const string& name= "FITSheader" ) {
    return new (nothrow) UFStrings( name, *(static_cast< deque< string >* >(this)) );
  }

  // convenience functions for use by UFDeviceConfig::statusFITS()
  static UFStrings* asStrings(map< string, string >& valhash, map< string, string >& comments );
  static UFStrings* asStrings(map< string, int >& valhash, map< string, string >& comments );
  static UFStrings* asStrings(map< string, double >& valhash, map< string, string >& comments );

  // convenience functions for client connection to an Agent to fetch FITS fragment
  static UFStrings* fetchFITS(const string& clientname, UFSocket& soc); 
  static void rmJunk(string& s) { UFRuntime::rmJunk(s); } 

  // convenience functions for reading and parsing an MEF (multi-extension FITS file):
  // this parses an integer valued by key:
  static int fitsInt(const string& key, char* fitsHdrEntry);

  // this reads the primary header, fseeks to the next 2880 block, and returns
  // the entire header as a ufstrings obj.
  static UFStrings* readPrmHdr(FILE* fs, int& total);
  static UFStrings* readPrmHdr(FILE* fs, int& total, int& nods);
  static UFStrings* readPrmHdr(FILE* fs, int& total, int& nods, int& nodsets);
  static UFStrings* readPrmHdr(FILE* fs, int& w, int& h, int& total, int& nods, int& nodsets);

  // this reads an extension header and the data block that follows it
  // and fseeks to the next 2880 block:
  static UFInts* readExtData(FILE* fs, UFStrings*& exthdr, int& w, int& h);
  static UFInts* readExtData(FILE* fs, UFStrings*& exthdr, int& w, int& h, int& chops, int& savesets);

protected:
  char _FITSrecord[BUFSIZ]; // only 80 bytes will be used (alloc more for safety)

  // helper for ctors: adds one standard rec containing SIMPLE keyword:
  int Simple( const string& comment, bool simple );

  // helper for replace funcs.
  int _replace(const string& key);
};

#endif // __UFFITSheader_h__


  /**
   *
   */
  /**
   *
   *@param comment TBD
   *@param simple TBD
   */
  /**
   *
   *@param fc TBD
   *@param comment TBD
   *@param simple TBD
   */
  /**
   *
   *@param fc TBD
   *@param oc TBD
   *@param comment TBD
   *@param simple TBD
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@param keyword TBD
   *@param value TBD
   *@param comment TBD
   *@return TBD
   */
  /**
   *
   *@param keyword TBD
   *@param value TBD
   *@param comment TBD
   *@return TBD
   */
  /**
   *
   *@param keyword TBD
   *@param value TBD
   *@param comment TBD
   *@return TBD
   */
  /**
   *
   *@param keyword TBD
   *@param value TBD
   *@param comment TBD
   *@return TBD
   */
  /**
   *
   *@param keyword TBD
   *@param value TBD
   *@param comment TBD
   *@return TBD
   */
  /**
   *
   *@param keyword TBD
   *@param value TBD
   *@param comment TBD
   *@return TBD
   */
  /**
   *
   *@param valhash TBD
   *@param comments TBD
   *@return TBD
   */
  /**
   *
   *@param valhash TBD
   *@param comments TBD
   *@return TBD
   */
  /**
   *
   *@param valhash TBD
   *@param comments TBD
   *@return TBD
   */
  /**
   *
   *@param s TBD
   *@return TBD
   */
  /**
   *
   *@param ufs TBD
   *@return TBD
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@return TBD
   */
  /**
   *
   *@param comment TBD
   *@return TBD
   */
  /**
   *
   *@param name TBD
   *@return TBD
   */
  /**
   *
   *@param valhash TBD
   *@param comments TBD
   *@return TBD
   */
  /**
   *
   *@param valhash TBD
   *@param comments TBD
   *@return TBD
   */
  /**
   *
   *@param valhash TBD
   *@param comments TBD
   *@return TBD
   */
  /**
   *
   *@param clientname TBD
   *@param soc TBD
   *@return TBD
   */
  /**
   *
   *@param s TBD
   */
  /**
   *
   *@param key TBD
   *@param fitsHdrEntry TBD
   *@return TBD
   */
  /**
   *
   *@param fs TBD
   *@param total TBD
   *@return TBD
   */
  /**
   *
   *@param fs TBD
   *@param total TBD
   *@param nods TBD
   *@param nodsets TBD
   *@return TBD
   */
  /**
   *
   *@param fs TBD
   *@param w TBD
   *@param h TBD
   *@param total TBD
   *@param nods TBD
   *@param nodsets TBD
   *@return TBD
   */
  /**
   *
   *@param fs TBD
   *@param exthdr TBD
   *@param w TBD
   *@param h TBD
   *@return TBD
   */
  /**
   *
   *@param fs TBD
   *@param exthdr TBD
   *@param w TBD
   *@param h TBD
   *@param chops TBD
   *@param savesets TBD
   *@return TBD
   */
  /**
   *
   *@param comment TBD
   *@param simple TBD
   *@return TBD
   */
  /**
   *
   *@param key TBD
   *@return TBD
   */

#!/usr/bin/perl -wT

use CGI;

BEGIN {
    $ENV{PATH} = '/bin:/usr/bin:/usr/local/bin';
    delete @ENV{ qw( IFS CDPATH ENV BASH_ENV ) };
}


my $q = new CGI;

print $q->header(-Refresh=>'10; URL=http://www.astro.ufl.edu/~drashkin/cgi-bin/logtail.cgi', -type => "text/html");

my $ootput = `tail -n 100 /data/doradus0/data/environment/current`;

$ootput =~ s/\n/<br>/g;

print "$ootput\n";

exit;

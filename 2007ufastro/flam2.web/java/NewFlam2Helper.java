
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;
import java.io.*;
import javax.swing.border.*;

public class NewFlam2Helper {
    JFrame mainFrame ;
    UFHelperPlot [] graphPanels;
    JTextField fileNameField;
    String logName = "/nfs/irdoradus/share/data/environment/current";
    //String logName = "/usr/tmp/newf2Log5-5-2006";
    String defPath = ".";
    UFHelperPlotPanel[] thePanels;

    public NewFlam2Helper(String [] args) {
	mainFrame = new JFrame();
/*
	graphPanels = new UFHelperPlot[3];
	thePanels = new UFHelperPlotPanel[3];
	String [] strs0 = {"CamSetpA","CamSetpB"};
	String [] strs1 = {"MosVac","CamVac"};
	String [] strs2 = {"MosCold1","MosBenc2","MosBenc3","MosBenc4","CamCold5","CamBenc6","CamBenc7","CamBenc8"};
	graphPanels[0] = new UFHelperPlot(strs0,0,logName);
	graphPanels[1] = new UFHelperPlot(strs1,2,logName);
	graphPanels[2] = new UFHelperPlot(strs2,4,logName);
*/
        graphPanels = new UFHelperPlot[2];
        thePanels = new UFHelperPlotPanel[2];
        String [] strs0 = {"MosVac","CamVac"};
        String [] strs1 = {"1 = Stage 1","2 = Stage 2","3 = Bench","4 = Bench (LS Diode)","5 = Stage 1","6 = Cold Finger","7 = Bench","8 = Bench","A = Bench","B = Det"};
        graphPanels[0] = new UFHelperPlot(strs0,0,logName);
        graphPanels[1] = new UFHelperPlot(strs1,2,logName);
	JTabbedPane jtpMain = new JTabbedPane();
	JTabbedPane jtpGraph = new JTabbedPane();
	JPanel overallGraphPanel = new JPanel();
	fileNameField = new JTextField("/nfs/irflam2a/share/data/environment/current");
	JButton saveColorsButton = new JButton("Save Colors");
	saveColorsButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		    try {
			String home = UFExecCommand.getEnvVar("HOME");
			PrintWriter pw = new PrintWriter(new File(home+"/.ufflam2helper"));
			for (int i=0; i<3; i++) {
			    String [] nams = graphPanels[i].getSensorNames();
			    Color [] cols = graphPanels[i].getColors();
			    if (nams.length == cols.length) {
				for (int j=0; j<nams.length; j++) {
				    pw.println(nams[j]+" "+cols[j].getRed()+" "+cols[j].getGreen()+" "+cols[j].getBlue());
				}
			    } else {
				System.err.println("Flam2Helper> Number of sensor names != number of colors!");
			    }
			}
			pw.close();
		    } catch (Exception e) {
			System.err.println("Flam2Helper> Problem saving color prefs: "+e.toString());
		    }
		}
	    });
        String[] labels = {"MOS: LS-218 Channels", "Camera: LS-218 Channels", "Camera: LS-232 Channels"};
	int[] labelPos = {0, 4, 8};
        thePanels[0] = new UFHelperPlotPanel(graphPanels, thePanels, 0);
        thePanels[1] = new UFHelperPlotPanel(graphPanels, thePanels, 1, labels, labelPos);
	jtpGraph.add(thePanels[0],"Pressure");
	jtpGraph.add(thePanels[1],"Cam/Mos");
	overallGraphPanel.setLayout(new BorderLayout()); 
	overallGraphPanel.add(jtpGraph, BorderLayout.CENTER);
        String[] logNames = {logName};
        jtpMain.add(new UFTail(logNames),"Tail");
        if (args.length > 0) {
	   jtpMain.add(new UFTail(args),"Agents");
	}
	jtpMain.add(overallGraphPanel,"Graphs");
	mainFrame.setContentPane(jtpMain);
	mainFrame.setSize(942,726);
	mainFrame.setVisible(true);
	mainFrame.setDefaultCloseOperation(3);
	mainFrame.setTitle("Helpy Helperton");
    }

    public class UFHelperPlotPanel extends JPanel {
      JTextField xMinField, xMaxField, yMinField, yMaxField;
      JTextField bgColorField, axesColorField, logField;
      JTextField[] colorList;
      JCheckBox[] plotSensors;
      JButton plotButton, colorChooser, logBrowse;
      UFHelperPlot thePlot;
      UFHelperPlot[] thePlots;
      UFHelperPlotPanel[] thePanels;
      JComboBox unitsBox;

      public UFHelperPlotPanel(final UFHelperPlot[] plots, UFHelperPlotPanel[] panels, int n) {
	this(plots,panels,n, null, null);
      }

      public UFHelperPlotPanel(final UFHelperPlot[] plots, UFHelperPlotPanel[] panels, int n, String[] labels, int[] labelPos) {
	super();
        this.thePlot = plots[n];
        thePlot.setPlotPanel(this);
        this.thePlots = plots;
        this.thePanels = panels;
        final int numSensors = thePlot.numSensors;
        final String[] sensorNames = thePlot.getSensorNames();
	if (labels != null && labelPos != null) {
	   if (labels.length != labelPos.length) {
	      labels = null;
	      labelPos = null;
	   }
	}
	LinkedHashMap colors = new LinkedHashMap();
        // see if colors are stored in config file
        try {
            String home = UFExecCommand.getEnvVar("HOME");
            File f = new File(home+"/.newufflam2helper");
            if (f.exists()) {
                BufferedReader br = new BufferedReader(new FileReader(f));
                while (br.ready()) {
                    String s = br.readLine();
                    if (s != null && !s.trim().equals("")) {
                        StringTokenizer st = new StringTokenizer(s);
                        if (st.countTokens() == 2) {
                            String sname = st.nextToken().trim();
			    String rgb = st.nextToken().trim();
			    colors.put(sname.replaceAll("_"," "), rgb);
                        }
                    }
                }
            }
        } catch (Exception e) {
            System.err.println("NewFlam2Helper> "+e.toString());
        }
        thePlot.updateLogFile(logName);
	this.setLayout(new BorderLayout());
        this.setPreferredSize(new Dimension(924, 640));
        JPanel leftPanel = new JPanel();
        SpringLayout leftLayout = new SpringLayout();
        leftPanel.setLayout(leftLayout);
        leftPanel.setPreferredSize(new Dimension(284, 512));
	plotButton = new JButton("Plot");
	plotButton.addActionListener(new ActionListener() {
	   public void actionPerformed(ActionEvent ev) {
	      String temp;
	      //thePlot.updateLogFile(logField.getText().trim());
	      String opts = readOpts();
	      String[] oPlotOpts = readOplotOpts(numSensors);
	      thePlot.updatePlotOpts(opts, oPlotOpts);
	      thePlot.updateUnits((String)(unitsBox.getSelectedItem()));
	      thePlot.updatePlot();
	   }
	});
	leftPanel.add(plotButton);
	leftLayout.putConstraint(SpringLayout.WEST, plotButton, 5, SpringLayout.WEST, leftPanel);
        leftLayout.putConstraint(SpringLayout.NORTH, plotButton, 5, SpringLayout.NORTH, leftPanel);
	colorChooser = new JButton("Color Chooser");
	colorChooser.addActionListener(new ActionListener() {
	   public void actionPerformed(ActionEvent ev) {
	      final JDialog retVal = new JDialog();
              retVal.setModal(false);
              retVal.setAlwaysOnTop(true);
              retVal.setSize(200,40*(numSensors+2)+10);
              retVal.setLayout(new GridLayout(0,1));
              for (int i=0; i<numSensors; i++) {
		final int myI = i;
		final JLabel showLabel = new JLabel(sensorNames[i]);
		final JButton colorButton = new JButton();
		final Color tempColor = getColor(colorList[i].getText());
		colorButton.setBackground(tempColor);
		colorButton.addActionListener(new ActionListener(){
                    public void actionPerformed(ActionEvent ae) {
                        Color c = JColorChooser.showDialog(retVal,"Choose Color",tempColor);
                        if (c != null) {
			   colorList[myI].setForeground(c);
			   colorList[myI].setText(""+c.getRed()+","+c.getGreen()+","+c.getBlue());
			   colorButton.setBackground(c);
                           getParent().repaint();
                           plotButton.doClick();
			}
                    }
                });
		
		JPanel pan = new JPanel();
		pan.setLayout(new RatioLayout());
		pan.add("0.01,0.01;0.80,0.99",showLabel);
		pan.add("0.81,0.01;0.19,0.99",colorButton);
		retVal.add(pan);
		retVal.setVisible(true);
	      }
	      final JLabel bgshowLabel = new JLabel("BG Color");
	      final JButton bgcolorButton = new JButton();
	      final Color bgtempColor = getColor(bgColorField.getText());
	      bgcolorButton.setBackground(bgtempColor);
	      bgcolorButton.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent ae) {
		   Color c = JColorChooser.showDialog(retVal,"Choose Color",bgtempColor);
		   if (c != null) {
		      //bgColorField.setForeground(c);
		      bgColorField.setText(""+c.getRed()+","+c.getGreen()+","+c.getBlue());
		      bgcolorButton.setBackground(c);
		      getParent().repaint();
                      plotButton.doClick();
		   }
		}
	      });
              JPanel bgpan = new JPanel();
              bgpan.setLayout(new RatioLayout());
              bgpan.add("0.01,0.01;0.80,0.99",bgshowLabel);
              bgpan.add("0.81,0.01;0.19,0.99",bgcolorButton);
              retVal.add(bgpan);
              final JLabel axshowLabel = new JLabel("Axes Color");
              final JButton axcolorButton = new JButton();
              final Color axtempColor = getColor(axesColorField.getText());
              axcolorButton.setBackground(axtempColor);
              axcolorButton.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent ae) {
                   Color c = JColorChooser.showDialog(retVal,"Choose Color",axtempColor);
                   if (c != null) {
                      //axesColorField.setForeground(c);
                      axesColorField.setText(""+c.getRed()+","+c.getGreen()+","+c.getBlue());
                      axcolorButton.setBackground(c);
                      getParent().repaint();
		      plotButton.doClick();
		   }
                }
              });
              JPanel axpan = new JPanel();
              axpan.setLayout(new RatioLayout());
              axpan.add("0.01,0.01;0.80,0.99",axshowLabel);
              axpan.add("0.81,0.01;0.19,0.99",axcolorButton);
              retVal.add(axpan);
              retVal.setVisible(true);
	   }
	});
        leftPanel.add(colorChooser);
        leftLayout.putConstraint(SpringLayout.WEST, colorChooser, 10, SpringLayout.EAST, plotButton);
        leftLayout.putConstraint(SpringLayout.NORTH, colorChooser, 5, SpringLayout.NORTH, leftPanel);
	JLabel fileLabel = new JLabel("Sensors:");
	leftPanel.add(fileLabel);
        leftLayout.putConstraint(SpringLayout.WEST, fileLabel, 5, SpringLayout.WEST, leftPanel);
        leftLayout.putConstraint(SpringLayout.NORTH, fileLabel, 10, SpringLayout.SOUTH, plotButton);
	plotSensors = new JCheckBox[numSensors];
        colorList = new JTextField[numSensors];
        int maxLength = 0, nmax=0;
	boolean[] hasLabel = new boolean[numSensors];
	JLabel tempLabel = null; 
	for (int j = 0; j < numSensors; j++) {
	   final int myJ = j;
	   hasLabel[j] = false;
	   if (labels != null) for (int l = 0; l < labelPos.length; l++) {
	      if (myJ == labelPos[l]) {
		tempLabel = new JLabel(labels[l]);
		tempLabel.setBorder(new EtchedBorder());
		leftPanel.add(tempLabel);
		leftLayout.putConstraint(SpringLayout.WEST, tempLabel, 5, SpringLayout.WEST, leftPanel);
		if (j == 0) {
		   leftLayout.putConstraint(SpringLayout.NORTH, tempLabel, 10, SpringLayout.SOUTH, fileLabel);
		} else {
		   leftLayout.putConstraint(SpringLayout.NORTH, tempLabel, 10, SpringLayout.SOUTH, plotSensors[j-1]);
		}
		hasLabel[j] = true;
	      }
	   }
	   if (sensorNames[j].length() > maxLength) {
	      maxLength = sensorNames[j].length();
	      nmax = j;
	   }
	   plotSensors[j] = new JCheckBox(sensorNames[j], true);
	   plotSensors[j].addActionListener(new ActionListener() {
	      public void actionPerformed(ActionEvent ev) {
		JCheckBox temp = (JCheckBox)ev.getSource();
		thePlot.showY[myJ] = temp.isSelected();
		thePlot.updatePlot();
	      }
	   });
	   leftPanel.add(plotSensors[j]);
           leftLayout.putConstraint(SpringLayout.WEST, plotSensors[j], 5, SpringLayout.WEST, leftPanel);
	   if (hasLabel[j]) {
              leftLayout.putConstraint(SpringLayout.NORTH, plotSensors[j], 10, SpringLayout.SOUTH, tempLabel);
	   } else if (j == 0) {
	      leftLayout.putConstraint(SpringLayout.NORTH, plotSensors[j], 0, SpringLayout.SOUTH, fileLabel);
	   } else {
              leftLayout.putConstraint(SpringLayout.NORTH, plotSensors[j], 0, SpringLayout.SOUTH, plotSensors[j-1]);
	   }
	}
        JLabel colorLabel = new JLabel("Colors (R,G,B):");
        leftPanel.add(colorLabel);
        leftLayout.putConstraint(SpringLayout.WEST, colorLabel, 10, SpringLayout.EAST, plotSensors[nmax]);
        leftLayout.putConstraint(SpringLayout.NORTH, colorLabel, 10, SpringLayout.SOUTH, plotButton);
        final String[] startingColors = {"0,0,0","255,0,0","0,255,0","0,0,255","180,180,0","180,0,180","0,180,180","128,128,128","0,155,0","255,155,0"};
        for (int j = 0; j < numSensors; j++) {
           colorList[j] = new JTextField(8);
	   Color tempColor;
	   if (colors.containsKey(sensorNames[j])) {
	      String temp = (String)colors.get(sensorNames[j]);
	      colorList[j].setText(temp);
	      tempColor = getColor(temp);
	   } else {
	      colorList[j].setText(startingColors[j]);
	      tempColor = getColor(startingColors[j]);
	   }
	   if (tempColor != null) colorList[j].setForeground(tempColor);
	   final JTextField tempColorList = colorList[j];
	   colorList[j].addFocusListener(new FocusListener() {
	      public void focusGained(FocusEvent fe) {
              }

              public void focusLost(FocusEvent fe) {
		Color tempColor = getColor(tempColorList.getText());
		if (tempColor != null) {
		   tempColorList.setForeground(tempColor);
		   plotButton.doClick();
		}
              }
	   });
           leftPanel.add(colorList[j]);
           leftLayout.putConstraint(SpringLayout.WEST, colorList[j], 10, SpringLayout.EAST, plotSensors[nmax]);
	   if (hasLabel[j]) {
              leftLayout.putConstraint(SpringLayout.NORTH, colorList[j], 4, SpringLayout.NORTH, plotSensors[j]);
	   } else if (j == 0) {
              leftLayout.putConstraint(SpringLayout.NORTH, colorList[j], 4, SpringLayout.SOUTH, fileLabel);
           } else {
              leftLayout.putConstraint(SpringLayout.NORTH, colorList[j], 4, SpringLayout.SOUTH, plotSensors[j-1]);
           }
	}
	JLabel bgLabel = new JLabel("BG Color:");
	leftPanel.add(bgLabel);
	leftLayout.putConstraint(SpringLayout.WEST, bgLabel, 5, SpringLayout.WEST, leftPanel);
	leftLayout.putConstraint(SpringLayout.NORTH, bgLabel, 10, SpringLayout.SOUTH, plotSensors[numSensors-1]);
	bgColorField = new JTextField(8);
	if (colors.containsKey("BG_Color")) {
	   String temp = (String)colors.get("BG_Color");
	   bgColorField.setText(temp);
	} else bgColorField.setText("255,255,255");
	bgColorField.addFocusListener(new FocusListener() {
	   public void focusGained(FocusEvent fe) {
	   }

	   public void focusLost(FocusEvent fe) {
	      Color tempColor = getColor(bgColorField.getText());
	      if (tempColor != null) {
		plotButton.doClick();
	      }
	   }
	});
        leftPanel.add(bgColorField);
        leftLayout.putConstraint(SpringLayout.WEST, bgColorField, 10, SpringLayout.EAST, plotSensors[nmax]);
        leftLayout.putConstraint(SpringLayout.NORTH, bgColorField, 10, SpringLayout.SOUTH, plotSensors[numSensors-1]);

	JLabel axesColorLabel = new JLabel("Axes Color:");
	leftPanel.add(axesColorLabel);
        leftLayout.putConstraint(SpringLayout.WEST, axesColorLabel, 5, SpringLayout.WEST, leftPanel);
        leftLayout.putConstraint(SpringLayout.NORTH, axesColorLabel, 10, SpringLayout.SOUTH, bgColorField); 
	axesColorField = new JTextField(8);
        if (colors.containsKey("Axes_Color")) {
           String temp = (String)colors.get("Axes_Color");
           axesColorField.setText(temp);
        } else axesColorField.setText("0,0,0");
        axesColorField.addFocusListener(new FocusListener() {
           public void focusGained(FocusEvent fe) {
           }

           public void focusLost(FocusEvent fe) {
              Color tempColor = getColor(axesColorField.getText());
              if (tempColor != null) {
                plotButton.doClick();
              }
           }
        });
        leftPanel.add(axesColorField);
        leftLayout.putConstraint(SpringLayout.WEST, axesColorField, 10, SpringLayout.EAST, plotSensors[nmax]);
        leftLayout.putConstraint(SpringLayout.NORTH, axesColorField, 10, SpringLayout.SOUTH, bgColorField); 

	JButton saveColorButton = new JButton("Save Colors");
	saveColorButton.addActionListener(new ActionListener() {
	   public void actionPerformed(ActionEvent ev) {
	      try {
		String home = UFExecCommand.getEnvVar("HOME");
		File f = new File(home+"/.newufflam2helper");
		LinkedHashMap tempcolors = new LinkedHashMap();
		if (f.exists()) {
                   BufferedReader br = new BufferedReader(new FileReader(f));
		   while (br.ready()) {
		      String s = br.readLine();
		      if (s != null && !s.trim().equals("")) {
			StringTokenizer st = new StringTokenizer(s);
			if (st.countTokens() == 2) {
                            String sname = st.nextToken().trim();
                            String rgb = st.nextToken().trim();
                            tempcolors.put(sname, rgb);
                        }
		      }
		   }
		}
		String key, temp;
		for (int j = 0; j < numSensors; j++) {
		   temp = colorList[j].getText();
		   if (!temp.trim().equals("")) {
		      temp = removeWhitespace(temp);
		      if (temp.indexOf(",") == -1) temp = temp.replaceAll(" ", ",");
		   }
		   tempcolors.put(sensorNames[j].replaceAll(" ","_"), temp); 
		}
		temp = bgColorField.getText();
		if (!temp.trim().equals("")) {
		   temp = removeWhitespace(temp);
                   if (temp.indexOf(",") == -1) temp = temp.replaceAll(" ", ",");
		}
		tempcolors.put("BG_Color", temp);
                temp = axesColorField.getText();
                if (!temp.trim().equals("")) {
                   temp = removeWhitespace(temp);
                   if (temp.indexOf(",") == -1) temp = temp.replaceAll(" ", ",");
                }
                tempcolors.put("Axes_Color", temp);
		PrintWriter pw = new PrintWriter(new FileOutputStream(f));
		for (Iterator i = tempcolors.keySet().iterator(); i.hasNext(); ) {
		   key = (String)i.next();
		   temp = (String)tempcolors.get(key);
		   pw.println(key+" "+temp);
		}
		pw.close();
		JOptionPane.showMessageDialog(null, "The colors have been saved.", "Colors Saved", JOptionPane.INFORMATION_MESSAGE);
	     } catch (Exception e) {
	      System.err.println("NewFlam2Helper> "+e.toString());
	     }
	   }
	});
        leftPanel.add(saveColorButton);
        leftLayout.putConstraint(SpringLayout.WEST, saveColorButton, 5, SpringLayout.WEST, leftPanel); 
        leftLayout.putConstraint(SpringLayout.NORTH, saveColorButton, 10, SpringLayout.SOUTH, axesColorField);

        JPanel bottomPanel = new JPanel();
        SpringLayout bottomLayout = new SpringLayout();
        bottomPanel.setLayout(bottomLayout);
        bottomPanel.setPreferredSize(new Dimension(640, 128));

	JLabel xRangeLabel = new JLabel("x-range:");
        bottomPanel.add(xRangeLabel);
        bottomLayout.putConstraint(SpringLayout.WEST, xRangeLabel, 224, SpringLayout.WEST, bottomPanel); 
        bottomLayout.putConstraint(SpringLayout.NORTH, xRangeLabel, 10, SpringLayout.NORTH, bottomPanel);
	xMinField = new JTextField(5);
	xMinField.setText("0");
        bottomPanel.add(xMinField);
        bottomLayout.putConstraint(SpringLayout.WEST, xMinField, 5, SpringLayout.EAST, xRangeLabel);
        bottomLayout.putConstraint(SpringLayout.NORTH, xMinField, 8, SpringLayout.NORTH, bottomPanel);
        JLabel xToLabel = new JLabel("to");
        bottomPanel.add(xToLabel);
        bottomLayout.putConstraint(SpringLayout.WEST, xToLabel, 5, SpringLayout.EAST, xMinField);
        bottomLayout.putConstraint(SpringLayout.NORTH, xToLabel, 10, SpringLayout.NORTH, bottomPanel);
        xMaxField = new JTextField(5);
        bottomPanel.add(xMaxField);
        bottomLayout.putConstraint(SpringLayout.WEST, xMaxField, 5, SpringLayout.EAST, xToLabel);
        bottomLayout.putConstraint(SpringLayout.NORTH, xMaxField, 8, SpringLayout.NORTH, bottomPanel);

        JLabel yRangeLabel = new JLabel("y-range:");
        bottomPanel.add(yRangeLabel);
        bottomLayout.putConstraint(SpringLayout.WEST, yRangeLabel, 10, SpringLayout.EAST, xMaxField);
        bottomLayout.putConstraint(SpringLayout.NORTH, yRangeLabel, 10, SpringLayout.NORTH, bottomPanel);
	yMinField = new JTextField(5);
        bottomPanel.add(yMinField);
        bottomLayout.putConstraint(SpringLayout.WEST, yMinField, 5, SpringLayout.EAST, yRangeLabel);
        bottomLayout.putConstraint(SpringLayout.NORTH, yMinField, 8, SpringLayout.NORTH, bottomPanel);
        JLabel yToLabel = new JLabel("to");
        bottomPanel.add(yToLabel);
        bottomLayout.putConstraint(SpringLayout.WEST, yToLabel, 5, SpringLayout.EAST, yMinField);
        bottomLayout.putConstraint(SpringLayout.NORTH, yToLabel, 10, SpringLayout.NORTH, bottomPanel);
	yMaxField = new JTextField(5);
        bottomPanel.add(yMaxField);
        bottomLayout.putConstraint(SpringLayout.WEST, yMaxField, 5, SpringLayout.EAST, yToLabel);
        bottomLayout.putConstraint(SpringLayout.NORTH, yMaxField, 8, SpringLayout.NORTH, bottomPanel);

        JLabel unitsLabel = new JLabel("Units:");
        bottomPanel.add(unitsLabel);
        bottomLayout.putConstraint(SpringLayout.WEST, unitsLabel, 15, SpringLayout.EAST, yMaxField);
        bottomLayout.putConstraint(SpringLayout.NORTH, unitsLabel, 10, SpringLayout.NORTH, bottomPanel);
        String[] sunits = {"Seconds", "Minutes", "Hours", "Days"};
	unitsBox = new JComboBox(sunits);
        bottomPanel.add(unitsBox);
        bottomLayout.putConstraint(SpringLayout.WEST, unitsBox, 5, SpringLayout.EAST, unitsLabel); 
        bottomLayout.putConstraint(SpringLayout.NORTH, unitsBox, 5, SpringLayout.NORTH, bottomPanel);


        JButton plotButton2 = new JButton("Plot");
        bottomPanel.add(plotButton2);
        bottomLayout.putConstraint(SpringLayout.WEST, plotButton2, 30, SpringLayout.EAST, unitsBox); 
        bottomLayout.putConstraint(SpringLayout.NORTH, plotButton2, 5, SpringLayout.NORTH, bottomPanel);
        plotButton2.addActionListener(new ActionListener() {
           public void actionPerformed(ActionEvent ev) {
	      plotButton.doClick();
	   }
	});

        JLabel logLabel = new JLabel("Logfile:");
        bottomPanel.add(logLabel);
        bottomLayout.putConstraint(SpringLayout.WEST, logLabel, 224, SpringLayout.WEST, bottomPanel);
        bottomLayout.putConstraint(SpringLayout.NORTH, logLabel, 15, SpringLayout.SOUTH, xMinField);
        logField = new JTextField(40);
	logField.setText(logName);
        bottomPanel.add(logField);
        bottomLayout.putConstraint(SpringLayout.WEST, logField, 5, SpringLayout.EAST, logLabel);
        bottomLayout.putConstraint(SpringLayout.NORTH, logField, 13, SpringLayout.SOUTH, xMinField);
	logBrowse = new JButton("Browse");
	logBrowse.addActionListener(new ActionListener() {
	   public void actionPerformed(ActionEvent ev) {
	      JFileChooser jfc = new JFileChooser(defPath);
	      int returnVal = jfc.showOpenDialog((Component)ev.getSource());
	      if (returnVal == JFileChooser.APPROVE_OPTION) {
		defPath = jfc.getCurrentDirectory().getAbsolutePath();
                logName = jfc.getSelectedFile().getAbsolutePath();
		for (int j = 0; j < thePlots.length; j++) {
		   thePanels[j].logField.setText(logName);
		   String opts = readOpts();
		   thePlots[j].updateLogFile(logName);
		}
	      }
           }
	});

        bottomPanel.add(logBrowse);
        bottomLayout.putConstraint(SpringLayout.WEST, logBrowse, 5, SpringLayout.EAST, logField);
        bottomLayout.putConstraint(SpringLayout.NORTH, logBrowse, 8, SpringLayout.SOUTH, xMinField);


        this.add(thePlot, BorderLayout.CENTER);
        this.add(leftPanel, BorderLayout.WEST);
        this.add(bottomPanel, BorderLayout.SOUTH);

	//pass initial options to plot
	String temp;
	thePlot.updateLogFile(logField.getText().trim());
	String opts = readOpts();
	String[] oPlotOpts = readOplotOpts(numSensors);
	thePlot.updatePlotOpts(opts, oPlotOpts);
        thePlot.updateUnits((String)(unitsBox.getSelectedItem()));
	//start reading file
	thePlot.startThread();
      }

      public String removeWhitespace(String s) {
	while (s.indexOf("\t") != -1) s = s.replaceAll("\t"," ");
	while (s.indexOf("  ") != -1) {
	   s = s.replaceAll("  "," ");
	}
	s = s.trim();
	return s;
      }

      public String readOpts() {
	String s = "";
        String temp;
        temp = xMinField.getText();
        if (!temp.trim().equals("")) s+="*xminval="+temp+", ";
        temp = xMaxField.getText();
        if (!temp.trim().equals("")) s+="*xmaxval="+temp+", ";
        temp = yMinField.getText();
        if (!temp.trim().equals("")) s+="*yminval="+temp+", ";
        temp = yMaxField.getText();
        if (!temp.trim().equals("")) s+="*ymaxval="+temp+", ";
	temp = bgColorField.getText();
	if (!temp.trim().equals("")) {
	   temp = removeWhitespace(temp);
	   if (temp.indexOf(",") == -1) temp = temp.replaceAll(" ", ",");
	   s+="*background="+temp+", ";
	   int colorLen = temp.split(",").length;
	   if (colorLen < 3) {
	      for (int l = 0; l < 3-colorLen; l++) s+=temp.substring(temp.lastIndexOf(",")+1)+", ";
	   }
	}
        temp = axesColorField.getText();
        if (!temp.trim().equals("")) {
           temp = removeWhitespace(temp);
           if (temp.indexOf(",") == -1) temp = temp.replaceAll(" ", ",");
           s+="*axescolor="+temp+", ";
           int colorLen = temp.split(",").length;
           if (colorLen < 3) {
              for (int l = 0; l < 3-colorLen; l++) s+=temp.substring(temp.lastIndexOf(",")+1)+", ";
           }
        }
	return s;
      }

      public String[] readOplotOpts(int numSensors) {
        String temp;
        String[] oPlotOpts = new String[numSensors];
        for (int j = 0; j < numSensors; j++) {
           oPlotOpts[j] = "";
           temp = colorList[j].getText();
           if (!temp.trim().equals("")) {
              temp = removeWhitespace(temp);
              if (temp.indexOf(",") == -1) temp = temp.replaceAll(" ", ",");
              oPlotOpts[j]+="*color="+temp+", ";
              int colorLen = temp.split(",").length;
              if (colorLen < 3) {
                for (int l = 0; l < 3-colorLen; l++) oPlotOpts[j]+=temp.substring(temp.lastIndexOf(",")+1)+", ";
              }
           }
        }
        return oPlotOpts;
      }

      public Color getColor(String temp) {
	if (!temp.trim().equals("")) {
	   temp = removeWhitespace(temp);
	}
	if (temp.indexOf(",") == -1) temp = temp.replaceAll(" ", ",");
	String[] temprgb = temp.split(",");
	if (temprgb.length < 3) return Color.BLACK;
	int r = Integer.parseInt(temprgb[0].trim());
        int g = Integer.parseInt(temprgb[1].trim());
        int b = Integer.parseInt(temprgb[2].trim());
	return new Color(r,g,b);
      }

      public void plot(String s) {
	thePlot.updatePlot();
      }
    }


    public static void main(String [] args) {
	new NewFlam2Helper(args);
    }

}

#ifndef INCmenuSimulationH
#define INCmenuSimulationH
typedef enum {
	menuSimulationNONE,
	menuSimulationVSM,
	menuSimulationFAST,
	menuSimulationFULL
}menuSimulation;
#endif /*INCmenuSimulationH*/

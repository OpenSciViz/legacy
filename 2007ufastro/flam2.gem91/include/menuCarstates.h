#ifndef INCmenuCarstatesH
#define INCmenuCarstatesH
typedef enum {
	menuCarstatesIDLE,
	menuCarstatesPAUSED,
	menuCarstatesBUSY,
	menuCarstatesERROR
}menuCarstates;
#endif /*INCmenuCarstatesH*/

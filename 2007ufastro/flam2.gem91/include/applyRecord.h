#include "ellLib.h"
#include "epicsMutex.h"
#include "link.h"
#include "epicsTime.h"
#include "epicsTypes.h"

#ifndef INCmenuDirectiveH
#define INCmenuDirectiveH
typedef enum {
	menuDirectiveMARK,
	menuDirectiveCLEAR,
	menuDirectivePRESET,
	menuDirectiveSTART,
	menuDirectiveSTOP
}menuDirective;
#endif /*INCmenuDirectiveH*/

#ifndef INCmenuApplyStateH
#define INCmenuApplyStateH
typedef enum {
	menuApplyStateIN,
	menuApplyStateOUT
}menuApplyState;
#endif /*INCmenuApplyStateH*/
#ifndef INCapplyH
#define INCapplyH
typedef struct applyRecord {
	char		name[61]; /*Record Name*/
	char		desc[29]; /*Descriptor*/
	char		asg[29]; /*Access Security Group*/
	epicsEnum16	scan;	/*Scan Mechanism*/
	epicsEnum16	pini;	/*Process at iocInit*/
	short		phas;	/*Scan Phase*/
	short		evnt;	/*Event Number*/
	short		tse;	/*Time Stamp Event*/
	DBLINK		tsel;	/*Time Stamp Link*/
	epicsEnum16	dtyp;	/*Device Type*/
	short		disv;	/*Disable Value*/
	short		disa;	/*Disable*/
	DBLINK		sdis;	/*Scanning Disable*/
	epicsMutexId	mlok;	/*Monitor lock*/
	ELLLIST		mlis;	/*Monitor List*/
	unsigned char	disp;	/*Disable putField*/
	unsigned char	proc;	/*Force Processing*/
	epicsEnum16	stat;	/*Alarm Status*/
	epicsEnum16	sevr;	/*Alarm Severity*/
	epicsEnum16	nsta;	/*New Alarm Status*/
	epicsEnum16	nsev;	/*New Alarm Severity*/
	epicsEnum16	acks;	/*Alarm Ack Severity*/
	epicsEnum16	ackt;	/*Alarm Ack Transient*/
	epicsEnum16	diss;	/*Disable Alarm Sevrty*/
	unsigned char	lcnt;	/*Lock Count*/
	unsigned char	pact;	/*Record active*/
	unsigned char	putf;	/*dbPutField process*/
	unsigned char	rpro;	/*Reprocess */
	void		*asp;	/*Access Security Pvt*/
	struct putNotify *ppn;	/*addr of PUTNOTIFY*/
	struct putNotifyRecord *ppnr;	/*pputNotifyRecord*/
	struct scan_element *spvt;	/*Scan Private*/
	struct rset	*rset;	/*Address of RSET*/
	struct dset	*dset;	/*DSET address*/
	void		*dpvt;	/*Device Private*/
	struct dbRecordType *rdes;	/*Address of dbRecordType*/
	struct lockRecord *lset;	/*Lock Set*/
	epicsEnum16	prio;	/*Scheduling Priority*/
	unsigned char	tpro;	/*Trace Processing*/
	char bkpt;	/*Break Point*/
	unsigned char	udf;	/*Undefined*/
	epicsTimeStamp	time;	/*Time*/
	DBLINK		flnk;	/*Forward Process Link*/
	double		vers;	/*Version Number*/
	epicsEnum16	stte;	/*State*/
	epicsInt32		nprc;	/*Num. Proc.*/
	epicsInt32		stfg;	/*Start Flag*/
	short		mark;	/*Mark*/
	epicsInt32		lpro;	/*Link Processing*/
	epicsInt32		top;	/*Top of tree*/
	float		tout;	/*Timeout (sec)*/
	void *rpvt;	/*Record Private*/
	epicsInt32		val;	/*Return Error Code*/
	epicsEnum16	dir;	/*CAD Directive*/
	epicsInt32		clid;	/*Client ID*/
	char		mess[40]; /*Message*/
	char		omss[40]; /*Old Message*/
	DBLINK		outa;	/*Output Link A*/
	DBLINK		outb;	/*Output Link B*/
	DBLINK		outc;	/*Output Link C*/
	DBLINK		outd;	/*Output Link D*/
	DBLINK		oute;	/*Output Link E*/
	DBLINK		outf;	/*Output Link F*/
	DBLINK		outg;	/*Output Link G*/
	DBLINK		outh;	/*Output Link H*/
	DBLINK		ocla;	/*Output CLID Link A*/
	DBLINK		oclb;	/*Output CLID Link B*/
	DBLINK		oclc;	/*Output CLID Link C*/
	DBLINK		ocld;	/*Output CLID Link D*/
	DBLINK		ocle;	/*Output CLID Link E*/
	DBLINK		oclf;	/*Output CLID Link F*/
	DBLINK		oclg;	/*Output CLID Link G*/
	DBLINK		oclh;	/*Output CLID Link H*/
	DBLINK		inpa;	/*Input Link A*/
	DBLINK		inpb;	/*Input Link B*/
	DBLINK		inpc;	/*Input Link C*/
	DBLINK		inpd;	/*Input Link D*/
	DBLINK		inpe;	/*Input Link E*/
	DBLINK		inpf;	/*Input Link F*/
	DBLINK		inpg;	/*Input Link G*/
	DBLINK		inph;	/*Input Link H*/
	DBLINK		inma;	/*Input Msg Link A*/
	DBLINK		inmb;	/*Input Msg Link B*/
	DBLINK		inmc;	/*Input Msg Link C*/
	DBLINK		inmd;	/*Input Msg Link D*/
	DBLINK		inme;	/*Input Msg Link E*/
	DBLINK		inmf;	/*Input Msg Link F*/
	DBLINK		inmg;	/*Input Msg Link G*/
	DBLINK		inmh;	/*Input Msg Link H*/
} applyRecord;
#define applyRecordNAME	0
#define applyRecordDESC	1
#define applyRecordASG	2
#define applyRecordSCAN	3
#define applyRecordPINI	4
#define applyRecordPHAS	5
#define applyRecordEVNT	6
#define applyRecordTSE	7
#define applyRecordTSEL	8
#define applyRecordDTYP	9
#define applyRecordDISV	10
#define applyRecordDISA	11
#define applyRecordSDIS	12
#define applyRecordMLOK	13
#define applyRecordMLIS	14
#define applyRecordDISP	15
#define applyRecordPROC	16
#define applyRecordSTAT	17
#define applyRecordSEVR	18
#define applyRecordNSTA	19
#define applyRecordNSEV	20
#define applyRecordACKS	21
#define applyRecordACKT	22
#define applyRecordDISS	23
#define applyRecordLCNT	24
#define applyRecordPACT	25
#define applyRecordPUTF	26
#define applyRecordRPRO	27
#define applyRecordASP	28
#define applyRecordPPN	29
#define applyRecordPPNR	30
#define applyRecordSPVT	31
#define applyRecordRSET	32
#define applyRecordDSET	33
#define applyRecordDPVT	34
#define applyRecordRDES	35
#define applyRecordLSET	36
#define applyRecordPRIO	37
#define applyRecordTPRO	38
#define applyRecordBKPT	39
#define applyRecordUDF	40
#define applyRecordTIME	41
#define applyRecordFLNK	42
#define applyRecordVERS	43
#define applyRecordSTTE	44
#define applyRecordNPRC	45
#define applyRecordSTFG	46
#define applyRecordMARK	47
#define applyRecordLPRO	48
#define applyRecordTOP	49
#define applyRecordTOUT	50
#define applyRecordRPVT	51
#define applyRecordVAL	52
#define applyRecordDIR	53
#define applyRecordCLID	54
#define applyRecordMESS	55
#define applyRecordOMSS	56
#define applyRecordOUTA	57
#define applyRecordOUTB	58
#define applyRecordOUTC	59
#define applyRecordOUTD	60
#define applyRecordOUTE	61
#define applyRecordOUTF	62
#define applyRecordOUTG	63
#define applyRecordOUTH	64
#define applyRecordOCLA	65
#define applyRecordOCLB	66
#define applyRecordOCLC	67
#define applyRecordOCLD	68
#define applyRecordOCLE	69
#define applyRecordOCLF	70
#define applyRecordOCLG	71
#define applyRecordOCLH	72
#define applyRecordINPA	73
#define applyRecordINPB	74
#define applyRecordINPC	75
#define applyRecordINPD	76
#define applyRecordINPE	77
#define applyRecordINPF	78
#define applyRecordINPG	79
#define applyRecordINPH	80
#define applyRecordINMA	81
#define applyRecordINMB	82
#define applyRecordINMC	83
#define applyRecordINMD	84
#define applyRecordINME	85
#define applyRecordINMF	86
#define applyRecordINMG	87
#define applyRecordINMH	88
#endif /*INCapplyH*/
#ifdef GEN_SIZE_OFFSET
#ifdef __cplusplus
extern "C" {
#endif
#include <epicsExport.h>
static int applyRecordSizeOffset(dbRecordType *pdbRecordType)
{
    applyRecord *prec = 0;
  pdbRecordType->papFldDes[0]->size=sizeof(prec->name);
  pdbRecordType->papFldDes[0]->offset=(short)((char *)&prec->name - (char *)prec);
  pdbRecordType->papFldDes[1]->size=sizeof(prec->desc);
  pdbRecordType->papFldDes[1]->offset=(short)((char *)&prec->desc - (char *)prec);
  pdbRecordType->papFldDes[2]->size=sizeof(prec->asg);
  pdbRecordType->papFldDes[2]->offset=(short)((char *)&prec->asg - (char *)prec);
  pdbRecordType->papFldDes[3]->size=sizeof(prec->scan);
  pdbRecordType->papFldDes[3]->offset=(short)((char *)&prec->scan - (char *)prec);
  pdbRecordType->papFldDes[4]->size=sizeof(prec->pini);
  pdbRecordType->papFldDes[4]->offset=(short)((char *)&prec->pini - (char *)prec);
  pdbRecordType->papFldDes[5]->size=sizeof(prec->phas);
  pdbRecordType->papFldDes[5]->offset=(short)((char *)&prec->phas - (char *)prec);
  pdbRecordType->papFldDes[6]->size=sizeof(prec->evnt);
  pdbRecordType->papFldDes[6]->offset=(short)((char *)&prec->evnt - (char *)prec);
  pdbRecordType->papFldDes[7]->size=sizeof(prec->tse);
  pdbRecordType->papFldDes[7]->offset=(short)((char *)&prec->tse - (char *)prec);
  pdbRecordType->papFldDes[8]->size=sizeof(prec->tsel);
  pdbRecordType->papFldDes[8]->offset=(short)((char *)&prec->tsel - (char *)prec);
  pdbRecordType->papFldDes[9]->size=sizeof(prec->dtyp);
  pdbRecordType->papFldDes[9]->offset=(short)((char *)&prec->dtyp - (char *)prec);
  pdbRecordType->papFldDes[10]->size=sizeof(prec->disv);
  pdbRecordType->papFldDes[10]->offset=(short)((char *)&prec->disv - (char *)prec);
  pdbRecordType->papFldDes[11]->size=sizeof(prec->disa);
  pdbRecordType->papFldDes[11]->offset=(short)((char *)&prec->disa - (char *)prec);
  pdbRecordType->papFldDes[12]->size=sizeof(prec->sdis);
  pdbRecordType->papFldDes[12]->offset=(short)((char *)&prec->sdis - (char *)prec);
  pdbRecordType->papFldDes[13]->size=sizeof(prec->mlok);
  pdbRecordType->papFldDes[13]->offset=(short)((char *)&prec->mlok - (char *)prec);
  pdbRecordType->papFldDes[14]->size=sizeof(prec->mlis);
  pdbRecordType->papFldDes[14]->offset=(short)((char *)&prec->mlis - (char *)prec);
  pdbRecordType->papFldDes[15]->size=sizeof(prec->disp);
  pdbRecordType->papFldDes[15]->offset=(short)((char *)&prec->disp - (char *)prec);
  pdbRecordType->papFldDes[16]->size=sizeof(prec->proc);
  pdbRecordType->papFldDes[16]->offset=(short)((char *)&prec->proc - (char *)prec);
  pdbRecordType->papFldDes[17]->size=sizeof(prec->stat);
  pdbRecordType->papFldDes[17]->offset=(short)((char *)&prec->stat - (char *)prec);
  pdbRecordType->papFldDes[18]->size=sizeof(prec->sevr);
  pdbRecordType->papFldDes[18]->offset=(short)((char *)&prec->sevr - (char *)prec);
  pdbRecordType->papFldDes[19]->size=sizeof(prec->nsta);
  pdbRecordType->papFldDes[19]->offset=(short)((char *)&prec->nsta - (char *)prec);
  pdbRecordType->papFldDes[20]->size=sizeof(prec->nsev);
  pdbRecordType->papFldDes[20]->offset=(short)((char *)&prec->nsev - (char *)prec);
  pdbRecordType->papFldDes[21]->size=sizeof(prec->acks);
  pdbRecordType->papFldDes[21]->offset=(short)((char *)&prec->acks - (char *)prec);
  pdbRecordType->papFldDes[22]->size=sizeof(prec->ackt);
  pdbRecordType->papFldDes[22]->offset=(short)((char *)&prec->ackt - (char *)prec);
  pdbRecordType->papFldDes[23]->size=sizeof(prec->diss);
  pdbRecordType->papFldDes[23]->offset=(short)((char *)&prec->diss - (char *)prec);
  pdbRecordType->papFldDes[24]->size=sizeof(prec->lcnt);
  pdbRecordType->papFldDes[24]->offset=(short)((char *)&prec->lcnt - (char *)prec);
  pdbRecordType->papFldDes[25]->size=sizeof(prec->pact);
  pdbRecordType->papFldDes[25]->offset=(short)((char *)&prec->pact - (char *)prec);
  pdbRecordType->papFldDes[26]->size=sizeof(prec->putf);
  pdbRecordType->papFldDes[26]->offset=(short)((char *)&prec->putf - (char *)prec);
  pdbRecordType->papFldDes[27]->size=sizeof(prec->rpro);
  pdbRecordType->papFldDes[27]->offset=(short)((char *)&prec->rpro - (char *)prec);
  pdbRecordType->papFldDes[28]->size=sizeof(prec->asp);
  pdbRecordType->papFldDes[28]->offset=(short)((char *)&prec->asp - (char *)prec);
  pdbRecordType->papFldDes[29]->size=sizeof(prec->ppn);
  pdbRecordType->papFldDes[29]->offset=(short)((char *)&prec->ppn - (char *)prec);
  pdbRecordType->papFldDes[30]->size=sizeof(prec->ppnr);
  pdbRecordType->papFldDes[30]->offset=(short)((char *)&prec->ppnr - (char *)prec);
  pdbRecordType->papFldDes[31]->size=sizeof(prec->spvt);
  pdbRecordType->papFldDes[31]->offset=(short)((char *)&prec->spvt - (char *)prec);
  pdbRecordType->papFldDes[32]->size=sizeof(prec->rset);
  pdbRecordType->papFldDes[32]->offset=(short)((char *)&prec->rset - (char *)prec);
  pdbRecordType->papFldDes[33]->size=sizeof(prec->dset);
  pdbRecordType->papFldDes[33]->offset=(short)((char *)&prec->dset - (char *)prec);
  pdbRecordType->papFldDes[34]->size=sizeof(prec->dpvt);
  pdbRecordType->papFldDes[34]->offset=(short)((char *)&prec->dpvt - (char *)prec);
  pdbRecordType->papFldDes[35]->size=sizeof(prec->rdes);
  pdbRecordType->papFldDes[35]->offset=(short)((char *)&prec->rdes - (char *)prec);
  pdbRecordType->papFldDes[36]->size=sizeof(prec->lset);
  pdbRecordType->papFldDes[36]->offset=(short)((char *)&prec->lset - (char *)prec);
  pdbRecordType->papFldDes[37]->size=sizeof(prec->prio);
  pdbRecordType->papFldDes[37]->offset=(short)((char *)&prec->prio - (char *)prec);
  pdbRecordType->papFldDes[38]->size=sizeof(prec->tpro);
  pdbRecordType->papFldDes[38]->offset=(short)((char *)&prec->tpro - (char *)prec);
  pdbRecordType->papFldDes[39]->size=sizeof(prec->bkpt);
  pdbRecordType->papFldDes[39]->offset=(short)((char *)&prec->bkpt - (char *)prec);
  pdbRecordType->papFldDes[40]->size=sizeof(prec->udf);
  pdbRecordType->papFldDes[40]->offset=(short)((char *)&prec->udf - (char *)prec);
  pdbRecordType->papFldDes[41]->size=sizeof(prec->time);
  pdbRecordType->papFldDes[41]->offset=(short)((char *)&prec->time - (char *)prec);
  pdbRecordType->papFldDes[42]->size=sizeof(prec->flnk);
  pdbRecordType->papFldDes[42]->offset=(short)((char *)&prec->flnk - (char *)prec);
  pdbRecordType->papFldDes[43]->size=sizeof(prec->vers);
  pdbRecordType->papFldDes[43]->offset=(short)((char *)&prec->vers - (char *)prec);
  pdbRecordType->papFldDes[44]->size=sizeof(prec->stte);
  pdbRecordType->papFldDes[44]->offset=(short)((char *)&prec->stte - (char *)prec);
  pdbRecordType->papFldDes[45]->size=sizeof(prec->nprc);
  pdbRecordType->papFldDes[45]->offset=(short)((char *)&prec->nprc - (char *)prec);
  pdbRecordType->papFldDes[46]->size=sizeof(prec->stfg);
  pdbRecordType->papFldDes[46]->offset=(short)((char *)&prec->stfg - (char *)prec);
  pdbRecordType->papFldDes[47]->size=sizeof(prec->mark);
  pdbRecordType->papFldDes[47]->offset=(short)((char *)&prec->mark - (char *)prec);
  pdbRecordType->papFldDes[48]->size=sizeof(prec->lpro);
  pdbRecordType->papFldDes[48]->offset=(short)((char *)&prec->lpro - (char *)prec);
  pdbRecordType->papFldDes[49]->size=sizeof(prec->top);
  pdbRecordType->papFldDes[49]->offset=(short)((char *)&prec->top - (char *)prec);
  pdbRecordType->papFldDes[50]->size=sizeof(prec->tout);
  pdbRecordType->papFldDes[50]->offset=(short)((char *)&prec->tout - (char *)prec);
  pdbRecordType->papFldDes[51]->size=sizeof(prec->rpvt);
  pdbRecordType->papFldDes[51]->offset=(short)((char *)&prec->rpvt - (char *)prec);
  pdbRecordType->papFldDes[52]->size=sizeof(prec->val);
  pdbRecordType->papFldDes[52]->offset=(short)((char *)&prec->val - (char *)prec);
  pdbRecordType->papFldDes[53]->size=sizeof(prec->dir);
  pdbRecordType->papFldDes[53]->offset=(short)((char *)&prec->dir - (char *)prec);
  pdbRecordType->papFldDes[54]->size=sizeof(prec->clid);
  pdbRecordType->papFldDes[54]->offset=(short)((char *)&prec->clid - (char *)prec);
  pdbRecordType->papFldDes[55]->size=sizeof(prec->mess);
  pdbRecordType->papFldDes[55]->offset=(short)((char *)&prec->mess - (char *)prec);
  pdbRecordType->papFldDes[56]->size=sizeof(prec->omss);
  pdbRecordType->papFldDes[56]->offset=(short)((char *)&prec->omss - (char *)prec);
  pdbRecordType->papFldDes[57]->size=sizeof(prec->outa);
  pdbRecordType->papFldDes[57]->offset=(short)((char *)&prec->outa - (char *)prec);
  pdbRecordType->papFldDes[58]->size=sizeof(prec->outb);
  pdbRecordType->papFldDes[58]->offset=(short)((char *)&prec->outb - (char *)prec);
  pdbRecordType->papFldDes[59]->size=sizeof(prec->outc);
  pdbRecordType->papFldDes[59]->offset=(short)((char *)&prec->outc - (char *)prec);
  pdbRecordType->papFldDes[60]->size=sizeof(prec->outd);
  pdbRecordType->papFldDes[60]->offset=(short)((char *)&prec->outd - (char *)prec);
  pdbRecordType->papFldDes[61]->size=sizeof(prec->oute);
  pdbRecordType->papFldDes[61]->offset=(short)((char *)&prec->oute - (char *)prec);
  pdbRecordType->papFldDes[62]->size=sizeof(prec->outf);
  pdbRecordType->papFldDes[62]->offset=(short)((char *)&prec->outf - (char *)prec);
  pdbRecordType->papFldDes[63]->size=sizeof(prec->outg);
  pdbRecordType->papFldDes[63]->offset=(short)((char *)&prec->outg - (char *)prec);
  pdbRecordType->papFldDes[64]->size=sizeof(prec->outh);
  pdbRecordType->papFldDes[64]->offset=(short)((char *)&prec->outh - (char *)prec);
  pdbRecordType->papFldDes[65]->size=sizeof(prec->ocla);
  pdbRecordType->papFldDes[65]->offset=(short)((char *)&prec->ocla - (char *)prec);
  pdbRecordType->papFldDes[66]->size=sizeof(prec->oclb);
  pdbRecordType->papFldDes[66]->offset=(short)((char *)&prec->oclb - (char *)prec);
  pdbRecordType->papFldDes[67]->size=sizeof(prec->oclc);
  pdbRecordType->papFldDes[67]->offset=(short)((char *)&prec->oclc - (char *)prec);
  pdbRecordType->papFldDes[68]->size=sizeof(prec->ocld);
  pdbRecordType->papFldDes[68]->offset=(short)((char *)&prec->ocld - (char *)prec);
  pdbRecordType->papFldDes[69]->size=sizeof(prec->ocle);
  pdbRecordType->papFldDes[69]->offset=(short)((char *)&prec->ocle - (char *)prec);
  pdbRecordType->papFldDes[70]->size=sizeof(prec->oclf);
  pdbRecordType->papFldDes[70]->offset=(short)((char *)&prec->oclf - (char *)prec);
  pdbRecordType->papFldDes[71]->size=sizeof(prec->oclg);
  pdbRecordType->papFldDes[71]->offset=(short)((char *)&prec->oclg - (char *)prec);
  pdbRecordType->papFldDes[72]->size=sizeof(prec->oclh);
  pdbRecordType->papFldDes[72]->offset=(short)((char *)&prec->oclh - (char *)prec);
  pdbRecordType->papFldDes[73]->size=sizeof(prec->inpa);
  pdbRecordType->papFldDes[73]->offset=(short)((char *)&prec->inpa - (char *)prec);
  pdbRecordType->papFldDes[74]->size=sizeof(prec->inpb);
  pdbRecordType->papFldDes[74]->offset=(short)((char *)&prec->inpb - (char *)prec);
  pdbRecordType->papFldDes[75]->size=sizeof(prec->inpc);
  pdbRecordType->papFldDes[75]->offset=(short)((char *)&prec->inpc - (char *)prec);
  pdbRecordType->papFldDes[76]->size=sizeof(prec->inpd);
  pdbRecordType->papFldDes[76]->offset=(short)((char *)&prec->inpd - (char *)prec);
  pdbRecordType->papFldDes[77]->size=sizeof(prec->inpe);
  pdbRecordType->papFldDes[77]->offset=(short)((char *)&prec->inpe - (char *)prec);
  pdbRecordType->papFldDes[78]->size=sizeof(prec->inpf);
  pdbRecordType->papFldDes[78]->offset=(short)((char *)&prec->inpf - (char *)prec);
  pdbRecordType->papFldDes[79]->size=sizeof(prec->inpg);
  pdbRecordType->papFldDes[79]->offset=(short)((char *)&prec->inpg - (char *)prec);
  pdbRecordType->papFldDes[80]->size=sizeof(prec->inph);
  pdbRecordType->papFldDes[80]->offset=(short)((char *)&prec->inph - (char *)prec);
  pdbRecordType->papFldDes[81]->size=sizeof(prec->inma);
  pdbRecordType->papFldDes[81]->offset=(short)((char *)&prec->inma - (char *)prec);
  pdbRecordType->papFldDes[82]->size=sizeof(prec->inmb);
  pdbRecordType->papFldDes[82]->offset=(short)((char *)&prec->inmb - (char *)prec);
  pdbRecordType->papFldDes[83]->size=sizeof(prec->inmc);
  pdbRecordType->papFldDes[83]->offset=(short)((char *)&prec->inmc - (char *)prec);
  pdbRecordType->papFldDes[84]->size=sizeof(prec->inmd);
  pdbRecordType->papFldDes[84]->offset=(short)((char *)&prec->inmd - (char *)prec);
  pdbRecordType->papFldDes[85]->size=sizeof(prec->inme);
  pdbRecordType->papFldDes[85]->offset=(short)((char *)&prec->inme - (char *)prec);
  pdbRecordType->papFldDes[86]->size=sizeof(prec->inmf);
  pdbRecordType->papFldDes[86]->offset=(short)((char *)&prec->inmf - (char *)prec);
  pdbRecordType->papFldDes[87]->size=sizeof(prec->inmg);
  pdbRecordType->papFldDes[87]->offset=(short)((char *)&prec->inmg - (char *)prec);
  pdbRecordType->papFldDes[88]->size=sizeof(prec->inmh);
  pdbRecordType->papFldDes[88]->offset=(short)((char *)&prec->inmh - (char *)prec);
    pdbRecordType->rec_size = sizeof(*prec);
    return(0);
}
epicsExportRegistrar(applyRecordSizeOffset);
#ifdef __cplusplus
}
#endif
#endif /*GEN_SIZE_OFFSET*/

#
#  T-ReCS filter name to wheel position translation table, with default Window
#


# Filter Name		Filter 1	Filter 2		Window

open      		open     	open			KBrC  
dark     	 	dark		open			KBrC
J-lo     	 	J-lo		open			KBrC
J			J		open			KBrC
H			open		H			KBrC     
Ks			open		Ks			KBrC      
JH			open		JH			KBrC 
HK			open		HK			KBrC 
TBD1			open		TBD1			KBrC 

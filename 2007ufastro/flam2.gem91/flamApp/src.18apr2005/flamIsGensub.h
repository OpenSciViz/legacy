
/* INDENT OFF */

/*+
 *
 * FILENAME
 * -------- 
 * flamIsGensub.h
 *
 * PURPOSE
 * -------
 * declare public functions for the T-Recs instrument sequencer gensub
 * record support code.
 *
 * FUNCTION NAME(S)
 * ----------------
 * 
 * DEPENDENCIES
 * ------------
 *
 * LIMITATIONS
 * -----------
 * 
 * AUTHOR
 * ------
 * William Rambold (wrambold@gemini.edu)
 * 
 * HISTORY
 * -------
 * 2000/12/12  WNR  Initial coding
 *
 */

/* INDENT ON */

/* ===================================================================== */


/*
 *
 *  Public function prototypes
 *
 */

long flamCmdCombineGProcess (genSubRecord * pgs);
long flamIsCopyGProcess (genSubRecord * pgs);
long flamIsInitGInit (genSubRecord * pgs);
long flamIsInitGProcess (genSubRecord * pgs);
long flamIsInsSetupCopy (genSubRecord * pgs);
long flamIsInsSetupTranslate (genSubRecord * pgs);
long flamIsNullGInit (genSubRecord * pgs);
long flamIsRebootGInit (genSubRecord * pgs);
long flamIsRebootGProcess (genSubRecord * pgs);
long flamIsSubSysCombineProcess (genSubRecord * pgs);
long flamIsTranslateGProcess (genSubRecord * pgs);
long flamStateGProcess (genSubRecord * pgs);

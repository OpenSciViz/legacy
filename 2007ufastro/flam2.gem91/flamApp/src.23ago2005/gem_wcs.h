#ifndef GEM_WCS_H
#define GEM_WCS_H

/* for MAX_STRING_SIZE */
#include "epicsTypes.h"

#ifndef GEM_WCS_INTERNAL
   extern long gem_TCSconnection;
   extern SEM_ID semInitWcs;
   extern SEM_ID semComputeWcs;
   extern char trackFrameCurrent[80];
   extern double trackWavelengthCurrent;
   extern char trackEquinoxCurrent[80];
#else
   long gem_TCSconnection=-1;
   SEM_ID semInitWcs;
   SEM_ID semComputeWcs;
   char trackFrameCurrent[80];
   double trackWavelengthCurrent;
   char trackEquinoxCurrent[80];
#endif

#define NPOINTS 100
#define MATRIXSIZE 6

typedef struct
{
    char ctype1[MAX_STRING_SIZE];
    double crpix1;
    double crval1;
    char ctype2[MAX_STRING_SIZE];
    double crpix2;
    double crval2;
    char radecsys[MAX_STRING_SIZE];
    double mjdobs;
    double cd1_1;
    double cd1_2;
    double cd2_1;
    double cd2_2;
    double equinox;

} wcsHeader;

typedef struct
{
    double fpxy[NPOINTS][2];
    double pixij[NPOINTS][2];
    double detij[NPOINTS][2];
    double cij[MATRIXSIZE];
    double pixis;
    double pixjs;
    double perp;
    double orient;
    int numPoints;

} wcsInfoStruct;

int getWcs(char *trackFrameString,double *trackWavelength, char *trackEquinoxString,wcsHeader *wcshdr);
int getWcsCurrent(wcsHeader *wcshdr);

#endif /* GEM_WCS_H */

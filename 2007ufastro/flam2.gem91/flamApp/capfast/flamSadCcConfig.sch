[schematic2]
uniq 72
[tools]
[detail]
s 2624 2064 100 1792 2000/11/12
s 2480 2064 100 1792 NWR
s 2240 2064 100 1792 Initial Layout
s 2016 2064 100 1792 A
s 2512 -240 100 1792 flamSadCcConfig.sch
s 2096 -272 100 1792 Author: NWR
s 2096 -240 100 1792 2001/01/25
s 2320 -240 100 1792 Rev: B
s 2432 -192 100 256 Flamingos CC Configure
s 2096 -176 200 1792 FLAMINGOS
s 2240 -128 100 0 FLAMINGOS
s 2624 2032 100 1792 2001/01/25
s 2480 2032 100 1792 NWR
s 2240 2032 100 1792 Removed EC records
s 2016 2032 100 1792 B
[cell use]
use esirs 2112 423 100 0 esirs#71
xform 0 2320 576
p 2176 384 100 0 1 SCAN:Passive
p 2272 416 100 1024 -1 name:$(top)MOSCIRC2
use esirs -192 1671 100 0 esirs#7
xform 0 16 1824
p -128 1632 100 0 1 SCAN:Passive
p -32 1664 100 1024 -1 name:$(top)DeckerSteps
use esirs -192 1255 100 0 esirs#28
xform 0 16 1408
p -128 1216 100 0 1 SCAN:Passive
p -32 1248 100 1024 -1 name:$(top)Filter2Steps
use esirs -192 839 100 0 esirs#29
xform 0 16 992
p -128 800 100 0 1 SCAN:Passive
p -32 832 100 1024 -1 name:$(top)GrismSteps
use esirs 320 1671 100 0 esirs#31
xform 0 528 1824
p 384 1632 100 0 1 SCAN:Passive
p 480 1664 100 1024 -1 name:$(top)MOSSteps
use esirs 320 1255 100 0 esirs#32
xform 0 528 1408
p 384 1216 100 0 1 SCAN:Passive
p 480 1248 100 1024 -1 name:$(top)Filter1Steps
use esirs 320 839 100 0 esirs#33
xform 0 528 992
p 384 800 100 0 1 SCAN:Passive
p 480 832 100 1024 -1 name:$(top)LyotSteps
use esirs 320 423 100 0 esirs#48
xform 0 528 576
p 384 384 100 0 1 SCAN:Passive
p 528 704 100 1024 -1 name:$(top)Filter2Status
use esirs 768 1671 100 0 esirs#49
xform 0 976 1824
p 832 1632 100 0 1 SCAN:Passive
p 928 1664 100 1024 -1 name:$(top)DeckerStatus
use esirs 768 1255 100 0 esirs#50
xform 0 976 1408
p 832 1216 100 0 1 SCAN:Passive
p 928 1248 100 1024 -1 name:$(top)FocusStatus
use esirs 768 839 100 0 esirs#51
xform 0 976 992
p 832 800 100 0 1 SCAN:Passive
p 928 832 100 1024 -1 name:$(top)GrismStatus
use esirs 1216 1671 100 0 esirs#52
xform 0 1424 1824
p 1280 1632 100 0 1 SCAN:Passive
p 1376 1664 100 1024 -1 name:$(top)MOSStatus
use esirs 1216 1255 100 0 esirs#53
xform 0 1424 1408
p 1280 1216 100 0 1 SCAN:Passive
p 1376 1248 100 1024 -1 name:$(top)Filter1Status
use esirs -192 7 100 0 esirs#55
xform 0 16 160
p -128 -32 100 0 1 SCAN:Passive
p -32 0 100 1024 -1 name:$(top)MOSCIRC1
use esirs 320 7 100 0 esirs#56
xform 0 528 160
p 384 -32 100 0 1 SCAN:Passive
p 480 0 100 1024 -1 name:$(top)MOSPLT01
use esirs -192 423 100 0 esirs#34
xform 0 16 576
p -128 384 100 0 1 SCAN:Passive
p -32 416 100 1024 -1 name:$(top)FocusSteps
use esirs 784 423 100 0 esirs#57
xform 0 992 576
p 848 384 100 0 1 SCAN:Passive
p 944 416 100 1024 -1 name:$(top)LyotSteps
use esirs 1216 839 100 0 esirs#59
xform 0 1424 992
p 1280 800 100 0 1 SCAN:Passive
p 1376 832 100 1024 -1 name:$(top)MOSPLT05
use esirs 1216 423 100 0 esirs#60
xform 0 1424 576
p 1280 384 100 0 1 SCAN:Passive
p 1376 416 100 1024 -1 name:$(top)MOSPLT04
use esirs 1216 7 100 0 esirs#61
xform 0 1424 160
p 1280 -32 100 0 1 SCAN:Passive
p 1376 0 100 1024 -1 name:$(top)MOSPLT03
use esirs 1664 839 100 0 esirs#64
xform 0 1872 992
p 1728 800 100 0 1 SCAN:Passive
p 1824 832 100 1024 -1 name:$(top)MOSPLT08
use esirs 1664 423 100 0 esirs#65
xform 0 1872 576
p 1728 384 100 0 1 SCAN:Passive
p 1824 416 100 1024 -1 name:$(top)MOSPLT09
use esirs 1664 7 100 0 esirs#66
xform 0 1872 160
p 1728 -32 100 0 1 SCAN:Passive
p 1824 0 100 1024 -1 name:$(top)MOSPLT10
use esirs 2144 7 100 0 esirs#67
xform 0 2352 160
p 2208 -32 100 0 1 SCAN:Passive
p 2304 0 100 1024 -1 name:$(top)MOSPLT11
use esirs 784 7 100 0 esirs#68
xform 0 992 160
p 848 -32 100 0 1 SCAN:Passive
p 944 0 100 1024 -1 name:$(top)MOSPLT02
use esirs 1664 1671 100 0 esirs#69
xform 0 1872 1824
p 1728 1632 100 0 1 SCAN:Passive
p 1824 1664 100 1024 -1 name:$(top)MOSPLT06
use esirs 1664 1255 100 0 esirs#70
xform 0 1872 1408
p 1728 1216 100 0 1 SCAN:Passive
p 1824 1248 100 1024 -1 name:$(top)MOSPLT07
use changeBar 1984 2023 100 0 changeBar#46
xform 0 2336 2064
use changeBar 1984 1991 100 0 changeBar#47
xform 0 2336 2032
use bc200 -576 -408 -100 0 frame
xform 0 1104 896
use rb200abc 1984 -313 100 0 rb200abc#0
xform 0 2336 -192
[comments]
RCS: $Name:  $ $Id: flamSadCcConfig.sch,v 0.0 2005/09/01 20:20:10 drashkin Exp $

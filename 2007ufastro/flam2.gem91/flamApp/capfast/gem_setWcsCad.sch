[schematic2]
uniq 242
[tools]
[detail]
w 74 1099 100 0 n#229 ecad4.confwcs.STLK -112 1088 320 1088 320 1440 480 1440 elongouts.confwcsbusy.SLNK
w 930 1483 100 0 n#236 elongouts.confwcsbusy.FLNK 736 1472 1184 1472 1184 1376 1344 1376 ecars.confwcsC.SLNK
w 18 1675 100 0 MESS ecad4.confwcs.MESS -112 1664 208 1664 outhier.MESS.p
w -54 1707 100 0 VAL ecad4.confwcs.VAL -112 1696 64 1696 64 1792 208 1792 208 1796 outhier.VAL.p
w -606 1771 100 0 DIR inhier.DIR.P -640 1760 -512 1760 -512 1696 -432 1696 ecad4.confwcs.DIR
w 402 1483 100 0 n#232 hwin.hwin#231.in 384 1472 480 1472 elongouts.confwcsbusy.DOL
w 898 1419 100 0 n#230 elongouts.confwcsbusy.OUT 736 1408 1120 1408 1120 1568 1344 1568 ecars.confwcsC.IVAL
w 1442 1259 100 0 n#123 hwout.hwout#120.outp 1344 1152 1248 1152 1248 1248 1696 1248 1696 1344 1664 1344 ecars.confwcsC.FLNK
s -880 80 100 0 now try the scan solution
s 144 1392 100 0 busy on start
s -352 1504 100 0 wcs conf file 
s 224 1568 100 0 set the cadC busy, the SNL code will take care of getting it back idle
s -720 1504 100 0 WCS config file
s -864 160 100 0 observe PRESET fwdlink here
[cell use]
use elongouts 1488 -473 100 0 wcsDIM
xform 0 1616 -384
use eaos 1056 -729 100 0 wcsInstrumentPA
xform 0 1184 -640
p 800 -786 100 0 0 PREC:10
p 1168 -736 100 1024 0 name:$(top)wcs:$(I)
use eaos 1056 -473 100 0 wcsPixscale
xform 0 1184 -384
p 800 -530 100 0 0 PREC:10
p 1168 -480 100 1024 0 name:$(top)wcs:$(I)
use estringouts 608 -697 100 0 wcsDEC_used
xform 0 736 -624
p 720 -704 100 1024 0 name:$(top)wcs:$(I)
use estringouts 608 -441 100 0 wcsRA_used
xform 0 736 -368
p 720 -448 100 1024 0 name:$(top)wcs:$(I)
use hwin 192 1431 100 0 hwin#231
xform 0 288 1472
p 195 1464 100 0 -1 val(in):2
use elongouts 480 1351 100 0 confwcsbusy
xform 0 608 1440
p 544 1328 100 0 1 OMSL:closed_loop
use hwout 1344 1111 100 0 hwout#120
xform 0 1440 1152
p 1440 1143 100 0 -1 val(outp):$(top)combCars1
use egenSub -400 71 100 0 astCtx
xform 0 -256 496
p -320 848 100 0 1 FTA:DOUBLE
p -320 800 100 0 1 FTB:STRING
p -320 720 100 0 1 FTC:STRING
p -320 624 100 0 1 FTD:DOUBLE
p -576 592 100 0 0 FTE:DOUBLE
p -16 768 100 0 1 FTVB:STRING
p 0 720 100 0 1 FTVC:STRING
p -320 832 100 0 1 NOA:39
p -304 368 100 0 1 PINI:YES
p -336 -16 100 0 1 SCAN:5 second
p -352 32 100 0 1 SNAM:gem_updateAstCtx
p -784 832 100 0 1 def(INPA):tcs:ak:astCtx.VALA CA
p -864 768 100 0 1 def(INPB):tcs:sad:sourceATrackFrame CA
p -832 704 100 0 1 def(INPC):tcs:sad:sourceATrackEq CA
p -864 640 100 0 1 def(INPD):tcs:sad:sourceAWavelength CA
p -416 842 75 0 -1 palrm(INPA):MS
p -416 778 75 0 -1 palrm(INPB):MS
p -416 714 75 0 -1 palrm(INPC):MS
p -416 650 75 0 -1 palrm(INPD):MS
use inhier -656 1719 100 0 DIR
xform 0 -640 1760
use eaos 1488 -249 100 0 cd22
xform 0 1616 -160
p 1232 -306 100 0 0 PREC:10
p 1600 -256 100 1024 0 name:$(top)wcs:$(I)
use eaos 1488 -9 100 0 cd12
xform 0 1616 80
p 1232 -66 100 0 0 PREC:10
p 1600 -16 100 1024 0 name:$(top)wcs:$(I)
use eaos 1056 -9 100 0 cd11
xform 0 1184 80
p 800 -66 100 0 0 PREC:10
p 1168 -16 100 1024 0 name:$(top)wcs:$(I)
use eaos 1056 -249 100 0 cd21
xform 0 1184 -160
p 800 -306 100 0 0 PREC:10
p 1168 -256 100 1024 0 name:$(top)wcs:$(I)
use eaos 1024 343 100 0 mjdobs
xform 0 1152 432
p 768 286 100 0 0 PREC:2
p 1136 336 100 1024 0 name:$(top)wcs:$(I)
use eaos 1024 535 100 0 equinox
xform 0 1152 624
p 768 478 100 0 0 PREC:2
p 1136 528 100 1024 0 name:$(top)wcs:$(I)
use eaos 608 -9 100 0 crpix2
xform 0 736 80
p 352 -66 100 0 0 PREC:2
p 720 -16 100 1024 0 name:$(top)wcs:$(I)
use eaos 608 -233 100 0 crval2
xform 0 736 -144
p 352 -290 100 0 0 PREC:10
p 720 -240 100 1024 0 name:$(top)wcs:$(I)
use eaos 608 343 100 0 crval1
xform 0 736 432
p 352 286 100 0 0 PREC:10
p 720 336 100 1024 0 name:$(top)wcs:$(I)
use eaos 608 535 100 0 crpix1
xform 0 736 624
p 352 478 100 0 0 PREC:2
p 720 528 100 1024 0 name:$(top)wcs:$(I)
use estringouts 1024 759 100 0 radecsys
xform 0 1152 832
p 1136 752 100 1024 0 name:$(top)wcs:$(I)
use estringouts 608 183 100 0 ctype2
xform 0 736 256
p 720 176 100 1024 0 name:$(top)wcs:$(I)
use estringouts 608 759 100 0 ctype1
xform 0 736 832
p 720 752 100 1024 0 name:$(top)wcs:$(I)
use ecad4 -432 999 100 0 confwcs
xform 0 -272 1376
p -352 1792 100 0 1 SNAM:gemProcConfWcs
p -112 1472 75 768 -1 pproc(OUTA):NPP
use outhier 176 1755 100 0 VAL
xform 0 192 1796
use outhier 176 1623 100 0 MESS
xform 0 192 1664
use ecars 1344 1287 100 0 confwcsC
xform 0 1504 1456
p 1408 1616 100 0 -1 PV:$(top)
[comments]

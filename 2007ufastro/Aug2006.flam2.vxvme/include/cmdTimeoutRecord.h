#include "ellLib.h"
#include "epicsMutex.h"
#include "link.h"
#include "epicsTime.h"
#include "epicsTypes.h"

#ifndef INCmenuDirectiveH
#define INCmenuDirectiveH
typedef enum {
	menuDirectiveMARK,
	menuDirectiveCLEAR,
	menuDirectivePRESET,
	menuDirectiveSTART,
	menuDirectiveSTOP
}menuDirective;
#endif /*INCmenuDirectiveH*/

#ifndef INCmenuCarstatesH
#define INCmenuCarstatesH
typedef enum {
	menuCarstatesIDLE,
	menuCarstatesPAUSED,
	menuCarstatesBUSY,
	menuCarstatesERROR
}menuCarstates;
#endif /*INCmenuCarstatesH*/

#ifndef INCmenuCTOStateH
#define INCmenuCTOStateH
typedef enum {
	menuCTOStateMARK_CMD,
	menuCTOStateMARK_RESP,
	menuCTOStatePRESET_RESP,
	menuCTOStateSTART_RESP,
	menuCTOStateSTOP_RESP,
	menuCTOStateCAR_RESP
}menuCTOState;
#endif /*INCmenuCTOStateH*/
#ifndef INCcmdTimeoutH
#define INCcmdTimeoutH
typedef struct cmdTimeoutRecord {
	char		name[61]; /*Record Name*/
	char		desc[29]; /*Descriptor*/
	char		asg[29]; /*Access Security Group*/
	epicsEnum16	scan;	/*Scan Mechanism*/
	epicsEnum16	pini;	/*Process at iocInit*/
	short		phas;	/*Scan Phase*/
	short		evnt;	/*Event Number*/
	short		tse;	/*Time Stamp Event*/
	DBLINK		tsel;	/*Time Stamp Link*/
	epicsEnum16	dtyp;	/*Device Type*/
	short		disv;	/*Disable Value*/
	short		disa;	/*Disable*/
	DBLINK		sdis;	/*Scanning Disable*/
	epicsMutexId	mlok;	/*Monitor lock*/
	ELLLIST		mlis;	/*Monitor List*/
	unsigned char	disp;	/*Disable putField*/
	unsigned char	proc;	/*Force Processing*/
	epicsEnum16	stat;	/*Alarm Status*/
	epicsEnum16	sevr;	/*Alarm Severity*/
	epicsEnum16	nsta;	/*New Alarm Status*/
	epicsEnum16	nsev;	/*New Alarm Severity*/
	epicsEnum16	acks;	/*Alarm Ack Severity*/
	epicsEnum16	ackt;	/*Alarm Ack Transient*/
	epicsEnum16	diss;	/*Disable Alarm Sevrty*/
	unsigned char	lcnt;	/*Lock Count*/
	unsigned char	pact;	/*Record active*/
	unsigned char	putf;	/*dbPutField process*/
	unsigned char	rpro;	/*Reprocess */
	void		*asp;	/*Access Security Pvt*/
	struct putNotify *ppn;	/*addr of PUTNOTIFY*/
	struct putNotifyRecord *ppnr;	/*pputNotifyRecord*/
	struct scan_element *spvt;	/*Scan Private*/
	struct rset	*rset;	/*Address of RSET*/
	struct dset	*dset;	/*DSET address*/
	void		*dpvt;	/*Device Private*/
	struct dbRecordType *rdes;	/*Address of dbRecordType*/
	struct lockRecord *lset;	/*Lock Set*/
	epicsEnum16	prio;	/*Scheduling Priority*/
	unsigned char	tpro;	/*Trace Processing*/
	char bkpt;	/*Break Point*/
	unsigned char	udf;	/*Undefined*/
	epicsTimeStamp	time;	/*Time*/
	DBLINK		flnk;	/*Forward Process Link*/
	double		vers;	/*Version Number*/
	epicsInt32		pflg;	/*Process flag*/
	epicsInt32		cnt;	/*Number of processes*/
	epicsInt32		lpro;	/*Link Processing*/
	epicsInt32		val;	/*Holds CAD/CAR value*/
	epicsEnum16	stte;	/*State*/
	epicsEnum16	dir;	/*CAD Directive*/
	float		tout;	/*Timeout (sec)*/
	void *rpvt;	/*Record Private*/
	char		mess[40]; /*Current CAD/CAR Message*/
	DBLINK		icdv;	/*Input Link From CAD Val*/
	DBLINK		icdm;	/*Input Link From CAD Message*/
	DBLINK		icrv;	/*Input Link From CAR Val*/
	DBLINK		icrm;	/*Input Link From CAR Message*/
	DBLINK		omss;	/*Output Message Link*/
	DBLINK		oval;	/*Output Value Link*/
	DBLINK		odir;	/*Output Directive Link*/
} cmdTimeoutRecord;
#define cmdTimeoutRecordNAME	0
#define cmdTimeoutRecordDESC	1
#define cmdTimeoutRecordASG	2
#define cmdTimeoutRecordSCAN	3
#define cmdTimeoutRecordPINI	4
#define cmdTimeoutRecordPHAS	5
#define cmdTimeoutRecordEVNT	6
#define cmdTimeoutRecordTSE	7
#define cmdTimeoutRecordTSEL	8
#define cmdTimeoutRecordDTYP	9
#define cmdTimeoutRecordDISV	10
#define cmdTimeoutRecordDISA	11
#define cmdTimeoutRecordSDIS	12
#define cmdTimeoutRecordMLOK	13
#define cmdTimeoutRecordMLIS	14
#define cmdTimeoutRecordDISP	15
#define cmdTimeoutRecordPROC	16
#define cmdTimeoutRecordSTAT	17
#define cmdTimeoutRecordSEVR	18
#define cmdTimeoutRecordNSTA	19
#define cmdTimeoutRecordNSEV	20
#define cmdTimeoutRecordACKS	21
#define cmdTimeoutRecordACKT	22
#define cmdTimeoutRecordDISS	23
#define cmdTimeoutRecordLCNT	24
#define cmdTimeoutRecordPACT	25
#define cmdTimeoutRecordPUTF	26
#define cmdTimeoutRecordRPRO	27
#define cmdTimeoutRecordASP	28
#define cmdTimeoutRecordPPN	29
#define cmdTimeoutRecordPPNR	30
#define cmdTimeoutRecordSPVT	31
#define cmdTimeoutRecordRSET	32
#define cmdTimeoutRecordDSET	33
#define cmdTimeoutRecordDPVT	34
#define cmdTimeoutRecordRDES	35
#define cmdTimeoutRecordLSET	36
#define cmdTimeoutRecordPRIO	37
#define cmdTimeoutRecordTPRO	38
#define cmdTimeoutRecordBKPT	39
#define cmdTimeoutRecordUDF	40
#define cmdTimeoutRecordTIME	41
#define cmdTimeoutRecordFLNK	42
#define cmdTimeoutRecordVERS	43
#define cmdTimeoutRecordPFLG	44
#define cmdTimeoutRecordCNT	45
#define cmdTimeoutRecordLPRO	46
#define cmdTimeoutRecordVAL	47
#define cmdTimeoutRecordSTTE	48
#define cmdTimeoutRecordDIR	49
#define cmdTimeoutRecordTOUT	50
#define cmdTimeoutRecordRPVT	51
#define cmdTimeoutRecordMESS	52
#define cmdTimeoutRecordICDV	53
#define cmdTimeoutRecordICDM	54
#define cmdTimeoutRecordICRV	55
#define cmdTimeoutRecordICRM	56
#define cmdTimeoutRecordOMSS	57
#define cmdTimeoutRecordOVAL	58
#define cmdTimeoutRecordODIR	59
#endif /*INCcmdTimeoutH*/
#ifdef GEN_SIZE_OFFSET
#ifdef __cplusplus
extern "C" {
#endif
#include <epicsExport.h>
static int cmdTimeoutRecordSizeOffset(dbRecordType *pdbRecordType)
{
    cmdTimeoutRecord *prec = 0;
  pdbRecordType->papFldDes[0]->size=sizeof(prec->name);
  pdbRecordType->papFldDes[0]->offset=(short)((char *)&prec->name - (char *)prec);
  pdbRecordType->papFldDes[1]->size=sizeof(prec->desc);
  pdbRecordType->papFldDes[1]->offset=(short)((char *)&prec->desc - (char *)prec);
  pdbRecordType->papFldDes[2]->size=sizeof(prec->asg);
  pdbRecordType->papFldDes[2]->offset=(short)((char *)&prec->asg - (char *)prec);
  pdbRecordType->papFldDes[3]->size=sizeof(prec->scan);
  pdbRecordType->papFldDes[3]->offset=(short)((char *)&prec->scan - (char *)prec);
  pdbRecordType->papFldDes[4]->size=sizeof(prec->pini);
  pdbRecordType->papFldDes[4]->offset=(short)((char *)&prec->pini - (char *)prec);
  pdbRecordType->papFldDes[5]->size=sizeof(prec->phas);
  pdbRecordType->papFldDes[5]->offset=(short)((char *)&prec->phas - (char *)prec);
  pdbRecordType->papFldDes[6]->size=sizeof(prec->evnt);
  pdbRecordType->papFldDes[6]->offset=(short)((char *)&prec->evnt - (char *)prec);
  pdbRecordType->papFldDes[7]->size=sizeof(prec->tse);
  pdbRecordType->papFldDes[7]->offset=(short)((char *)&prec->tse - (char *)prec);
  pdbRecordType->papFldDes[8]->size=sizeof(prec->tsel);
  pdbRecordType->papFldDes[8]->offset=(short)((char *)&prec->tsel - (char *)prec);
  pdbRecordType->papFldDes[9]->size=sizeof(prec->dtyp);
  pdbRecordType->papFldDes[9]->offset=(short)((char *)&prec->dtyp - (char *)prec);
  pdbRecordType->papFldDes[10]->size=sizeof(prec->disv);
  pdbRecordType->papFldDes[10]->offset=(short)((char *)&prec->disv - (char *)prec);
  pdbRecordType->papFldDes[11]->size=sizeof(prec->disa);
  pdbRecordType->papFldDes[11]->offset=(short)((char *)&prec->disa - (char *)prec);
  pdbRecordType->papFldDes[12]->size=sizeof(prec->sdis);
  pdbRecordType->papFldDes[12]->offset=(short)((char *)&prec->sdis - (char *)prec);
  pdbRecordType->papFldDes[13]->size=sizeof(prec->mlok);
  pdbRecordType->papFldDes[13]->offset=(short)((char *)&prec->mlok - (char *)prec);
  pdbRecordType->papFldDes[14]->size=sizeof(prec->mlis);
  pdbRecordType->papFldDes[14]->offset=(short)((char *)&prec->mlis - (char *)prec);
  pdbRecordType->papFldDes[15]->size=sizeof(prec->disp);
  pdbRecordType->papFldDes[15]->offset=(short)((char *)&prec->disp - (char *)prec);
  pdbRecordType->papFldDes[16]->size=sizeof(prec->proc);
  pdbRecordType->papFldDes[16]->offset=(short)((char *)&prec->proc - (char *)prec);
  pdbRecordType->papFldDes[17]->size=sizeof(prec->stat);
  pdbRecordType->papFldDes[17]->offset=(short)((char *)&prec->stat - (char *)prec);
  pdbRecordType->papFldDes[18]->size=sizeof(prec->sevr);
  pdbRecordType->papFldDes[18]->offset=(short)((char *)&prec->sevr - (char *)prec);
  pdbRecordType->papFldDes[19]->size=sizeof(prec->nsta);
  pdbRecordType->papFldDes[19]->offset=(short)((char *)&prec->nsta - (char *)prec);
  pdbRecordType->papFldDes[20]->size=sizeof(prec->nsev);
  pdbRecordType->papFldDes[20]->offset=(short)((char *)&prec->nsev - (char *)prec);
  pdbRecordType->papFldDes[21]->size=sizeof(prec->acks);
  pdbRecordType->papFldDes[21]->offset=(short)((char *)&prec->acks - (char *)prec);
  pdbRecordType->papFldDes[22]->size=sizeof(prec->ackt);
  pdbRecordType->papFldDes[22]->offset=(short)((char *)&prec->ackt - (char *)prec);
  pdbRecordType->papFldDes[23]->size=sizeof(prec->diss);
  pdbRecordType->papFldDes[23]->offset=(short)((char *)&prec->diss - (char *)prec);
  pdbRecordType->papFldDes[24]->size=sizeof(prec->lcnt);
  pdbRecordType->papFldDes[24]->offset=(short)((char *)&prec->lcnt - (char *)prec);
  pdbRecordType->papFldDes[25]->size=sizeof(prec->pact);
  pdbRecordType->papFldDes[25]->offset=(short)((char *)&prec->pact - (char *)prec);
  pdbRecordType->papFldDes[26]->size=sizeof(prec->putf);
  pdbRecordType->papFldDes[26]->offset=(short)((char *)&prec->putf - (char *)prec);
  pdbRecordType->papFldDes[27]->size=sizeof(prec->rpro);
  pdbRecordType->papFldDes[27]->offset=(short)((char *)&prec->rpro - (char *)prec);
  pdbRecordType->papFldDes[28]->size=sizeof(prec->asp);
  pdbRecordType->papFldDes[28]->offset=(short)((char *)&prec->asp - (char *)prec);
  pdbRecordType->papFldDes[29]->size=sizeof(prec->ppn);
  pdbRecordType->papFldDes[29]->offset=(short)((char *)&prec->ppn - (char *)prec);
  pdbRecordType->papFldDes[30]->size=sizeof(prec->ppnr);
  pdbRecordType->papFldDes[30]->offset=(short)((char *)&prec->ppnr - (char *)prec);
  pdbRecordType->papFldDes[31]->size=sizeof(prec->spvt);
  pdbRecordType->papFldDes[31]->offset=(short)((char *)&prec->spvt - (char *)prec);
  pdbRecordType->papFldDes[32]->size=sizeof(prec->rset);
  pdbRecordType->papFldDes[32]->offset=(short)((char *)&prec->rset - (char *)prec);
  pdbRecordType->papFldDes[33]->size=sizeof(prec->dset);
  pdbRecordType->papFldDes[33]->offset=(short)((char *)&prec->dset - (char *)prec);
  pdbRecordType->papFldDes[34]->size=sizeof(prec->dpvt);
  pdbRecordType->papFldDes[34]->offset=(short)((char *)&prec->dpvt - (char *)prec);
  pdbRecordType->papFldDes[35]->size=sizeof(prec->rdes);
  pdbRecordType->papFldDes[35]->offset=(short)((char *)&prec->rdes - (char *)prec);
  pdbRecordType->papFldDes[36]->size=sizeof(prec->lset);
  pdbRecordType->papFldDes[36]->offset=(short)((char *)&prec->lset - (char *)prec);
  pdbRecordType->papFldDes[37]->size=sizeof(prec->prio);
  pdbRecordType->papFldDes[37]->offset=(short)((char *)&prec->prio - (char *)prec);
  pdbRecordType->papFldDes[38]->size=sizeof(prec->tpro);
  pdbRecordType->papFldDes[38]->offset=(short)((char *)&prec->tpro - (char *)prec);
  pdbRecordType->papFldDes[39]->size=sizeof(prec->bkpt);
  pdbRecordType->papFldDes[39]->offset=(short)((char *)&prec->bkpt - (char *)prec);
  pdbRecordType->papFldDes[40]->size=sizeof(prec->udf);
  pdbRecordType->papFldDes[40]->offset=(short)((char *)&prec->udf - (char *)prec);
  pdbRecordType->papFldDes[41]->size=sizeof(prec->time);
  pdbRecordType->papFldDes[41]->offset=(short)((char *)&prec->time - (char *)prec);
  pdbRecordType->papFldDes[42]->size=sizeof(prec->flnk);
  pdbRecordType->papFldDes[42]->offset=(short)((char *)&prec->flnk - (char *)prec);
  pdbRecordType->papFldDes[43]->size=sizeof(prec->vers);
  pdbRecordType->papFldDes[43]->offset=(short)((char *)&prec->vers - (char *)prec);
  pdbRecordType->papFldDes[44]->size=sizeof(prec->pflg);
  pdbRecordType->papFldDes[44]->offset=(short)((char *)&prec->pflg - (char *)prec);
  pdbRecordType->papFldDes[45]->size=sizeof(prec->cnt);
  pdbRecordType->papFldDes[45]->offset=(short)((char *)&prec->cnt - (char *)prec);
  pdbRecordType->papFldDes[46]->size=sizeof(prec->lpro);
  pdbRecordType->papFldDes[46]->offset=(short)((char *)&prec->lpro - (char *)prec);
  pdbRecordType->papFldDes[47]->size=sizeof(prec->val);
  pdbRecordType->papFldDes[47]->offset=(short)((char *)&prec->val - (char *)prec);
  pdbRecordType->papFldDes[48]->size=sizeof(prec->stte);
  pdbRecordType->papFldDes[48]->offset=(short)((char *)&prec->stte - (char *)prec);
  pdbRecordType->papFldDes[49]->size=sizeof(prec->dir);
  pdbRecordType->papFldDes[49]->offset=(short)((char *)&prec->dir - (char *)prec);
  pdbRecordType->papFldDes[50]->size=sizeof(prec->tout);
  pdbRecordType->papFldDes[50]->offset=(short)((char *)&prec->tout - (char *)prec);
  pdbRecordType->papFldDes[51]->size=sizeof(prec->rpvt);
  pdbRecordType->papFldDes[51]->offset=(short)((char *)&prec->rpvt - (char *)prec);
  pdbRecordType->papFldDes[52]->size=sizeof(prec->mess);
  pdbRecordType->papFldDes[52]->offset=(short)((char *)&prec->mess - (char *)prec);
  pdbRecordType->papFldDes[53]->size=sizeof(prec->icdv);
  pdbRecordType->papFldDes[53]->offset=(short)((char *)&prec->icdv - (char *)prec);
  pdbRecordType->papFldDes[54]->size=sizeof(prec->icdm);
  pdbRecordType->papFldDes[54]->offset=(short)((char *)&prec->icdm - (char *)prec);
  pdbRecordType->papFldDes[55]->size=sizeof(prec->icrv);
  pdbRecordType->papFldDes[55]->offset=(short)((char *)&prec->icrv - (char *)prec);
  pdbRecordType->papFldDes[56]->size=sizeof(prec->icrm);
  pdbRecordType->papFldDes[56]->offset=(short)((char *)&prec->icrm - (char *)prec);
  pdbRecordType->papFldDes[57]->size=sizeof(prec->omss);
  pdbRecordType->papFldDes[57]->offset=(short)((char *)&prec->omss - (char *)prec);
  pdbRecordType->papFldDes[58]->size=sizeof(prec->oval);
  pdbRecordType->papFldDes[58]->offset=(short)((char *)&prec->oval - (char *)prec);
  pdbRecordType->papFldDes[59]->size=sizeof(prec->odir);
  pdbRecordType->papFldDes[59]->offset=(short)((char *)&prec->odir - (char *)prec);
    pdbRecordType->rec_size = sizeof(*prec);
    return(0);
}
epicsExportRegistrar(cmdTimeoutRecordSizeOffset);
#ifdef __cplusplus
}
#endif
#endif /*GEN_SIZE_OFFSET*/

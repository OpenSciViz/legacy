#include "ellLib.h"
#include "epicsMutex.h"
#include "link.h"
#include "epicsTime.h"
#include "epicsTypes.h"
#ifndef INClutoutH
#define INClutoutH
typedef struct lutoutRecord {
	char		name[61]; /*Record Name*/
	char		desc[29]; /*Descriptor*/
	char		asg[29]; /*Access Security Group*/
	epicsEnum16	scan;	/*Scan Mechanism*/
	epicsEnum16	pini;	/*Process at iocInit*/
	short		phas;	/*Scan Phase*/
	short		evnt;	/*Event Number*/
	short		tse;	/*Time Stamp Event*/
	DBLINK		tsel;	/*Time Stamp Link*/
	epicsEnum16	dtyp;	/*Device Type*/
	short		disv;	/*Disable Value*/
	short		disa;	/*Disable*/
	DBLINK		sdis;	/*Scanning Disable*/
	epicsMutexId	mlok;	/*Monitor lock*/
	ELLLIST		mlis;	/*Monitor List*/
	unsigned char	disp;	/*Disable putField*/
	unsigned char	proc;	/*Force Processing*/
	epicsEnum16	stat;	/*Alarm Status*/
	epicsEnum16	sevr;	/*Alarm Severity*/
	epicsEnum16	nsta;	/*New Alarm Status*/
	epicsEnum16	nsev;	/*New Alarm Severity*/
	epicsEnum16	acks;	/*Alarm Ack Severity*/
	epicsEnum16	ackt;	/*Alarm Ack Transient*/
	epicsEnum16	diss;	/*Disable Alarm Sevrty*/
	unsigned char	lcnt;	/*Lock Count*/
	unsigned char	pact;	/*Record active*/
	unsigned char	putf;	/*dbPutField process*/
	unsigned char	rpro;	/*Reprocess */
	void		*asp;	/*Access Security Pvt*/
	struct putNotify *ppn;	/*addr of PUTNOTIFY*/
	struct putNotifyRecord *ppnr;	/*pputNotifyRecord*/
	struct scan_element *spvt;	/*Scan Private*/
	struct rset	*rset;	/*Address of RSET*/
	struct dset	*dset;	/*DSET address*/
	void		*dpvt;	/*Device Private*/
	struct dbRecordType *rdes;	/*Address of dbRecordType*/
	struct lockRecord *lset;	/*Lock Set*/
	epicsEnum16	prio;	/*Scheduling Priority*/
	unsigned char	tpro;	/*Trace Processing*/
	char bkpt;	/*Break Point*/
	unsigned char	udf;	/*Undefined*/
	epicsTimeStamp	time;	/*Time*/
	DBLINK		flnk;	/*Forward Process Link*/
	double		vers;	/*Version Number*/
	char		val[40]; /*Input String*/
	char		oval[40]; /*Old Input String*/
	epicsInt32		nval;	/*Number of Values*/
	epicsInt32		onvl;	/*Old Number*/
	epicsInt32		selb;	/*Selection Bits*/
	epicsInt32		prec;	/*Precision*/
	char		fdir[40]; /*Init File Directory*/
	char		fnam[40]; /*Init File Name*/
	void * ltbl;	/*Lookup Table*/
	DBLINK		llnk;	/*Lookup Table Link*/
	epicsInt32		load;	/*Reload Table*/
	DBLINK		outa;	/*Output A*/
	DBLINK		outb;	/*Output B*/
	DBLINK		outc;	/*Output C*/
	DBLINK		outd;	/*Output D*/
	void *vala;	/*Value of Output A*/
	void *valb;	/*Value of Output B*/
	void *valc;	/*Value of Output C*/
	void *vald;	/*Value of Output D*/
	void *olda;	/*Old Value of Output A*/
	void *oldb;	/*Old Value of Output B*/
	void *oldc;	/*Old Value of Output C*/
	void *oldd;	/*Old Value of Output D*/
	epicsEnum16	ftva;	/*Type of Value A*/
	epicsEnum16	ftvb;	/*Type of Value B*/
	epicsEnum16	ftvc;	/*Type of Value C*/
	epicsEnum16	ftvd;	/*Type of Value D*/
} lutoutRecord;
#define lutoutRecordNAME	0
#define lutoutRecordDESC	1
#define lutoutRecordASG	2
#define lutoutRecordSCAN	3
#define lutoutRecordPINI	4
#define lutoutRecordPHAS	5
#define lutoutRecordEVNT	6
#define lutoutRecordTSE	7
#define lutoutRecordTSEL	8
#define lutoutRecordDTYP	9
#define lutoutRecordDISV	10
#define lutoutRecordDISA	11
#define lutoutRecordSDIS	12
#define lutoutRecordMLOK	13
#define lutoutRecordMLIS	14
#define lutoutRecordDISP	15
#define lutoutRecordPROC	16
#define lutoutRecordSTAT	17
#define lutoutRecordSEVR	18
#define lutoutRecordNSTA	19
#define lutoutRecordNSEV	20
#define lutoutRecordACKS	21
#define lutoutRecordACKT	22
#define lutoutRecordDISS	23
#define lutoutRecordLCNT	24
#define lutoutRecordPACT	25
#define lutoutRecordPUTF	26
#define lutoutRecordRPRO	27
#define lutoutRecordASP	28
#define lutoutRecordPPN	29
#define lutoutRecordPPNR	30
#define lutoutRecordSPVT	31
#define lutoutRecordRSET	32
#define lutoutRecordDSET	33
#define lutoutRecordDPVT	34
#define lutoutRecordRDES	35
#define lutoutRecordLSET	36
#define lutoutRecordPRIO	37
#define lutoutRecordTPRO	38
#define lutoutRecordBKPT	39
#define lutoutRecordUDF	40
#define lutoutRecordTIME	41
#define lutoutRecordFLNK	42
#define lutoutRecordVERS	43
#define lutoutRecordVAL	44
#define lutoutRecordOVAL	45
#define lutoutRecordNVAL	46
#define lutoutRecordONVL	47
#define lutoutRecordSELB	48
#define lutoutRecordPREC	49
#define lutoutRecordFDIR	50
#define lutoutRecordFNAM	51
#define lutoutRecordLTBL	52
#define lutoutRecordLLNK	53
#define lutoutRecordLOAD	54
#define lutoutRecordOUTA	55
#define lutoutRecordOUTB	56
#define lutoutRecordOUTC	57
#define lutoutRecordOUTD	58
#define lutoutRecordVALA	59
#define lutoutRecordVALB	60
#define lutoutRecordVALC	61
#define lutoutRecordVALD	62
#define lutoutRecordOLDA	63
#define lutoutRecordOLDB	64
#define lutoutRecordOLDC	65
#define lutoutRecordOLDD	66
#define lutoutRecordFTVA	67
#define lutoutRecordFTVB	68
#define lutoutRecordFTVC	69
#define lutoutRecordFTVD	70
#endif /*INClutoutH*/
#ifdef GEN_SIZE_OFFSET
#ifdef __cplusplus
extern "C" {
#endif
#include <epicsExport.h>
static int lutoutRecordSizeOffset(dbRecordType *pdbRecordType)
{
    lutoutRecord *prec = 0;
  pdbRecordType->papFldDes[0]->size=sizeof(prec->name);
  pdbRecordType->papFldDes[0]->offset=(short)((char *)&prec->name - (char *)prec);
  pdbRecordType->papFldDes[1]->size=sizeof(prec->desc);
  pdbRecordType->papFldDes[1]->offset=(short)((char *)&prec->desc - (char *)prec);
  pdbRecordType->papFldDes[2]->size=sizeof(prec->asg);
  pdbRecordType->papFldDes[2]->offset=(short)((char *)&prec->asg - (char *)prec);
  pdbRecordType->papFldDes[3]->size=sizeof(prec->scan);
  pdbRecordType->papFldDes[3]->offset=(short)((char *)&prec->scan - (char *)prec);
  pdbRecordType->papFldDes[4]->size=sizeof(prec->pini);
  pdbRecordType->papFldDes[4]->offset=(short)((char *)&prec->pini - (char *)prec);
  pdbRecordType->papFldDes[5]->size=sizeof(prec->phas);
  pdbRecordType->papFldDes[5]->offset=(short)((char *)&prec->phas - (char *)prec);
  pdbRecordType->papFldDes[6]->size=sizeof(prec->evnt);
  pdbRecordType->papFldDes[6]->offset=(short)((char *)&prec->evnt - (char *)prec);
  pdbRecordType->papFldDes[7]->size=sizeof(prec->tse);
  pdbRecordType->papFldDes[7]->offset=(short)((char *)&prec->tse - (char *)prec);
  pdbRecordType->papFldDes[8]->size=sizeof(prec->tsel);
  pdbRecordType->papFldDes[8]->offset=(short)((char *)&prec->tsel - (char *)prec);
  pdbRecordType->papFldDes[9]->size=sizeof(prec->dtyp);
  pdbRecordType->papFldDes[9]->offset=(short)((char *)&prec->dtyp - (char *)prec);
  pdbRecordType->papFldDes[10]->size=sizeof(prec->disv);
  pdbRecordType->papFldDes[10]->offset=(short)((char *)&prec->disv - (char *)prec);
  pdbRecordType->papFldDes[11]->size=sizeof(prec->disa);
  pdbRecordType->papFldDes[11]->offset=(short)((char *)&prec->disa - (char *)prec);
  pdbRecordType->papFldDes[12]->size=sizeof(prec->sdis);
  pdbRecordType->papFldDes[12]->offset=(short)((char *)&prec->sdis - (char *)prec);
  pdbRecordType->papFldDes[13]->size=sizeof(prec->mlok);
  pdbRecordType->papFldDes[13]->offset=(short)((char *)&prec->mlok - (char *)prec);
  pdbRecordType->papFldDes[14]->size=sizeof(prec->mlis);
  pdbRecordType->papFldDes[14]->offset=(short)((char *)&prec->mlis - (char *)prec);
  pdbRecordType->papFldDes[15]->size=sizeof(prec->disp);
  pdbRecordType->papFldDes[15]->offset=(short)((char *)&prec->disp - (char *)prec);
  pdbRecordType->papFldDes[16]->size=sizeof(prec->proc);
  pdbRecordType->papFldDes[16]->offset=(short)((char *)&prec->proc - (char *)prec);
  pdbRecordType->papFldDes[17]->size=sizeof(prec->stat);
  pdbRecordType->papFldDes[17]->offset=(short)((char *)&prec->stat - (char *)prec);
  pdbRecordType->papFldDes[18]->size=sizeof(prec->sevr);
  pdbRecordType->papFldDes[18]->offset=(short)((char *)&prec->sevr - (char *)prec);
  pdbRecordType->papFldDes[19]->size=sizeof(prec->nsta);
  pdbRecordType->papFldDes[19]->offset=(short)((char *)&prec->nsta - (char *)prec);
  pdbRecordType->papFldDes[20]->size=sizeof(prec->nsev);
  pdbRecordType->papFldDes[20]->offset=(short)((char *)&prec->nsev - (char *)prec);
  pdbRecordType->papFldDes[21]->size=sizeof(prec->acks);
  pdbRecordType->papFldDes[21]->offset=(short)((char *)&prec->acks - (char *)prec);
  pdbRecordType->papFldDes[22]->size=sizeof(prec->ackt);
  pdbRecordType->papFldDes[22]->offset=(short)((char *)&prec->ackt - (char *)prec);
  pdbRecordType->papFldDes[23]->size=sizeof(prec->diss);
  pdbRecordType->papFldDes[23]->offset=(short)((char *)&prec->diss - (char *)prec);
  pdbRecordType->papFldDes[24]->size=sizeof(prec->lcnt);
  pdbRecordType->papFldDes[24]->offset=(short)((char *)&prec->lcnt - (char *)prec);
  pdbRecordType->papFldDes[25]->size=sizeof(prec->pact);
  pdbRecordType->papFldDes[25]->offset=(short)((char *)&prec->pact - (char *)prec);
  pdbRecordType->papFldDes[26]->size=sizeof(prec->putf);
  pdbRecordType->papFldDes[26]->offset=(short)((char *)&prec->putf - (char *)prec);
  pdbRecordType->papFldDes[27]->size=sizeof(prec->rpro);
  pdbRecordType->papFldDes[27]->offset=(short)((char *)&prec->rpro - (char *)prec);
  pdbRecordType->papFldDes[28]->size=sizeof(prec->asp);
  pdbRecordType->papFldDes[28]->offset=(short)((char *)&prec->asp - (char *)prec);
  pdbRecordType->papFldDes[29]->size=sizeof(prec->ppn);
  pdbRecordType->papFldDes[29]->offset=(short)((char *)&prec->ppn - (char *)prec);
  pdbRecordType->papFldDes[30]->size=sizeof(prec->ppnr);
  pdbRecordType->papFldDes[30]->offset=(short)((char *)&prec->ppnr - (char *)prec);
  pdbRecordType->papFldDes[31]->size=sizeof(prec->spvt);
  pdbRecordType->papFldDes[31]->offset=(short)((char *)&prec->spvt - (char *)prec);
  pdbRecordType->papFldDes[32]->size=sizeof(prec->rset);
  pdbRecordType->papFldDes[32]->offset=(short)((char *)&prec->rset - (char *)prec);
  pdbRecordType->papFldDes[33]->size=sizeof(prec->dset);
  pdbRecordType->papFldDes[33]->offset=(short)((char *)&prec->dset - (char *)prec);
  pdbRecordType->papFldDes[34]->size=sizeof(prec->dpvt);
  pdbRecordType->papFldDes[34]->offset=(short)((char *)&prec->dpvt - (char *)prec);
  pdbRecordType->papFldDes[35]->size=sizeof(prec->rdes);
  pdbRecordType->papFldDes[35]->offset=(short)((char *)&prec->rdes - (char *)prec);
  pdbRecordType->papFldDes[36]->size=sizeof(prec->lset);
  pdbRecordType->papFldDes[36]->offset=(short)((char *)&prec->lset - (char *)prec);
  pdbRecordType->papFldDes[37]->size=sizeof(prec->prio);
  pdbRecordType->papFldDes[37]->offset=(short)((char *)&prec->prio - (char *)prec);
  pdbRecordType->papFldDes[38]->size=sizeof(prec->tpro);
  pdbRecordType->papFldDes[38]->offset=(short)((char *)&prec->tpro - (char *)prec);
  pdbRecordType->papFldDes[39]->size=sizeof(prec->bkpt);
  pdbRecordType->papFldDes[39]->offset=(short)((char *)&prec->bkpt - (char *)prec);
  pdbRecordType->papFldDes[40]->size=sizeof(prec->udf);
  pdbRecordType->papFldDes[40]->offset=(short)((char *)&prec->udf - (char *)prec);
  pdbRecordType->papFldDes[41]->size=sizeof(prec->time);
  pdbRecordType->papFldDes[41]->offset=(short)((char *)&prec->time - (char *)prec);
  pdbRecordType->papFldDes[42]->size=sizeof(prec->flnk);
  pdbRecordType->papFldDes[42]->offset=(short)((char *)&prec->flnk - (char *)prec);
  pdbRecordType->papFldDes[43]->size=sizeof(prec->vers);
  pdbRecordType->papFldDes[43]->offset=(short)((char *)&prec->vers - (char *)prec);
  pdbRecordType->papFldDes[44]->size=sizeof(prec->val);
  pdbRecordType->papFldDes[44]->offset=(short)((char *)&prec->val - (char *)prec);
  pdbRecordType->papFldDes[45]->size=sizeof(prec->oval);
  pdbRecordType->papFldDes[45]->offset=(short)((char *)&prec->oval - (char *)prec);
  pdbRecordType->papFldDes[46]->size=sizeof(prec->nval);
  pdbRecordType->papFldDes[46]->offset=(short)((char *)&prec->nval - (char *)prec);
  pdbRecordType->papFldDes[47]->size=sizeof(prec->onvl);
  pdbRecordType->papFldDes[47]->offset=(short)((char *)&prec->onvl - (char *)prec);
  pdbRecordType->papFldDes[48]->size=sizeof(prec->selb);
  pdbRecordType->papFldDes[48]->offset=(short)((char *)&prec->selb - (char *)prec);
  pdbRecordType->papFldDes[49]->size=sizeof(prec->prec);
  pdbRecordType->papFldDes[49]->offset=(short)((char *)&prec->prec - (char *)prec);
  pdbRecordType->papFldDes[50]->size=sizeof(prec->fdir);
  pdbRecordType->papFldDes[50]->offset=(short)((char *)&prec->fdir - (char *)prec);
  pdbRecordType->papFldDes[51]->size=sizeof(prec->fnam);
  pdbRecordType->papFldDes[51]->offset=(short)((char *)&prec->fnam - (char *)prec);
  pdbRecordType->papFldDes[52]->size=sizeof(prec->ltbl);
  pdbRecordType->papFldDes[52]->offset=(short)((char *)&prec->ltbl - (char *)prec);
  pdbRecordType->papFldDes[53]->size=sizeof(prec->llnk);
  pdbRecordType->papFldDes[53]->offset=(short)((char *)&prec->llnk - (char *)prec);
  pdbRecordType->papFldDes[54]->size=sizeof(prec->load);
  pdbRecordType->papFldDes[54]->offset=(short)((char *)&prec->load - (char *)prec);
  pdbRecordType->papFldDes[55]->size=sizeof(prec->outa);
  pdbRecordType->papFldDes[55]->offset=(short)((char *)&prec->outa - (char *)prec);
  pdbRecordType->papFldDes[56]->size=sizeof(prec->outb);
  pdbRecordType->papFldDes[56]->offset=(short)((char *)&prec->outb - (char *)prec);
  pdbRecordType->papFldDes[57]->size=sizeof(prec->outc);
  pdbRecordType->papFldDes[57]->offset=(short)((char *)&prec->outc - (char *)prec);
  pdbRecordType->papFldDes[58]->size=sizeof(prec->outd);
  pdbRecordType->papFldDes[58]->offset=(short)((char *)&prec->outd - (char *)prec);
  pdbRecordType->papFldDes[59]->size=sizeof(prec->vala);
  pdbRecordType->papFldDes[59]->offset=(short)((char *)&prec->vala - (char *)prec);
  pdbRecordType->papFldDes[60]->size=sizeof(prec->valb);
  pdbRecordType->papFldDes[60]->offset=(short)((char *)&prec->valb - (char *)prec);
  pdbRecordType->papFldDes[61]->size=sizeof(prec->valc);
  pdbRecordType->papFldDes[61]->offset=(short)((char *)&prec->valc - (char *)prec);
  pdbRecordType->papFldDes[62]->size=sizeof(prec->vald);
  pdbRecordType->papFldDes[62]->offset=(short)((char *)&prec->vald - (char *)prec);
  pdbRecordType->papFldDes[63]->size=sizeof(prec->olda);
  pdbRecordType->papFldDes[63]->offset=(short)((char *)&prec->olda - (char *)prec);
  pdbRecordType->papFldDes[64]->size=sizeof(prec->oldb);
  pdbRecordType->papFldDes[64]->offset=(short)((char *)&prec->oldb - (char *)prec);
  pdbRecordType->papFldDes[65]->size=sizeof(prec->oldc);
  pdbRecordType->papFldDes[65]->offset=(short)((char *)&prec->oldc - (char *)prec);
  pdbRecordType->papFldDes[66]->size=sizeof(prec->oldd);
  pdbRecordType->papFldDes[66]->offset=(short)((char *)&prec->oldd - (char *)prec);
  pdbRecordType->papFldDes[67]->size=sizeof(prec->ftva);
  pdbRecordType->papFldDes[67]->offset=(short)((char *)&prec->ftva - (char *)prec);
  pdbRecordType->papFldDes[68]->size=sizeof(prec->ftvb);
  pdbRecordType->papFldDes[68]->offset=(short)((char *)&prec->ftvb - (char *)prec);
  pdbRecordType->papFldDes[69]->size=sizeof(prec->ftvc);
  pdbRecordType->papFldDes[69]->offset=(short)((char *)&prec->ftvc - (char *)prec);
  pdbRecordType->papFldDes[70]->size=sizeof(prec->ftvd);
  pdbRecordType->papFldDes[70]->offset=(short)((char *)&prec->ftvd - (char *)prec);
    pdbRecordType->rec_size = sizeof(*prec);
    return(0);
}
epicsExportRegistrar(lutoutRecordSizeOffset);
#ifdef __cplusplus
}
#endif
#endif /*GEN_SIZE_OFFSET*/

#!/bin/nawk -f

# Usage:    background-copy.awk data-dir

BEGIN{

  if ( ARGC < 2 ) {
    print ( "Not enough arguments" ) ;
    print ( " Syntax:  " ) ;
    print ( "   background-copy.awk data-dir " ) ;
    exit ;
  } else {
    datadir = ARGV[1] ;
  }

  sleep = 120 ;
  repeat = 999999 ;
#  repeat = 10 ;
  filelocal = "locallist" ;
  fileremote = "remotelist" ;

  execlocal  = "ls -1 *fits > " filelocal ;
  execremote = "ls -1 " datadir "*fits > " fileremote ;
  execsleep  = "sleep " sleep ;

  i = 1 ;
  while ( i <= repeat ) {
#    printf ( "Doing loop number %d \n", i ) ;

    system ( execlocal ) ;
    system ( execremote ) ;

# Sleep here, so that you do not copy an incomplete file
    system ( execsleep ) ;

    file = filelocal ;
    iloc = 0 ;
    while ( getline line < file > 0 ) {
      iloc += 1 ;
      split ( line, field ) ;
      imageloc[iloc] = field[1] ;
    }
    close ( file ) ;

    file = fileremote ;
    irem = 0 ;
    while ( getline line < file > 0 ) {
      irem += 1 ;
      split ( line, field ) ;
      gsub ( datadir, "", field[1] ) ;
      imagerem[irem] = field[1] ;
    }
    close ( file ) ;

    for ( ii=1 ; ii<=irem ; ii+=1 ) {
      imatch = 0 ;
      for ( jj=1 ; jj<=iloc ; jj+=1 ) {
	if ( imageloc[jj] == imagerem[ii] ) { imatch += 1 }
      }
      if ( imatch <= 0 ) {
	execcopy = "cp " datadir imagerem[ii] " . " ;
	printf ( "Copying:   %s   .... \n", execcopy ) ;
	system ( execcopy ) ;
      }
    }

    i += 1 ;
  }

}

/* mtpingserv2_producer */

#include <new>
#include <iostream>
#include <string>
#include <strstream>
#include <vector>
#include <fstream>

#include <unistd.h>
#include <cerrno>
#include <cstdlib>
#include <csignal>

#include "UFClientSocket.h"
#include "UFInts.h"

void sigHandler( int ) ;
bool readConfFile() ;

using std::string ;
using std::clog ;
using std::endl ;
using std::strstream ;
using std::ends ;
using std::vector ;
using std::ifstream ;

bool keepRunning = true ;
unsigned int imageWidth = 128 ; // bytes
unsigned int sleepTime = 2 ; // seconds

string uplinkName( "localhost" ) ;
string confFileName( "mtpingserv2.conf" ) ;
unsigned short int uplinkPort = 5555 ;
unsigned short int bitDepth = 32 ;
vector< string > bufferNames ;

UFClientSocket* uplinkSock = 0 ;

void usage( const string& progName )
{
  clog << endl ;
  clog << "Usage: " << progName << " [options]\n" ;
  clog << "-b <bits>: Specify bit depth per pixel ("
       << bitDepth << ")\n" ;
  clog << "-f <filename>: Specify configuration file ("
       << confFileName << ")\n" ;
  clog << "-h: Print help menu\n" ;
  clog << "-p <port num>: Specify port to which to connect ("
       << uplinkPort << ")\n" ;
  clog << "-s <sleep time>: Sleep time between loops (seconds)"
       << " (" << sleepTime << ")\n" ;
  clog << "-u <uplink>: Specify uplink hostname/IP ("
       << uplinkName << ")\n" ;
  clog << "-w <width>: Specify the width of the images (bytes) ("
       << imageWidth << ")\n" ;
  clog << endl ;
}

int main( int argc, char** argv )
{

  int c = EOF ;
  while( (c = ::getopt( argc, argv, "b:f:hp:s:t:u:w:" )) != EOF )
    {
      switch( c )
	{
	case 'b':
	  bitDepth = static_cast< unsigned short int >( ::atoi( optarg ) ) ;
	  break ;
        case 'f':
          confFileName = optarg;
          break ;
	case 'p':
	  uplinkPort = static_cast< unsigned short int >( ::atoi( optarg ) ) ;
	  break ;
	case 's':
	  sleepTime = static_cast< unsigned int >( ::atoi( optarg ) ) ;
	  break ;
	case 'u':
	  uplinkName = optarg ;
	  break ;
	case 'w':
	  imageWidth = static_cast< unsigned int >( ::atoi( optarg ) ) ;
          break ;
	case 'h':
	default:
	  usage( argv[ 0 ] ) ;
	  return 0 ;
	}
    }

  ::srand( time( 0 ) ) ;

  if( SIG_ERR == ::signal( SIGINT, sigHandler ) )
    {
      clog << "main> Error establishing signal handler for SIGINT\n" ;
      return 0 ;
    }
  if( SIG_ERR == ::signal( SIGPIPE, sigHandler ) )
    {
      clog << "main> Error establishing signal handler for SIGPIPE\n" ;
      return 0 ;
    }
  if( SIG_ERR == ::signal( SIGTERM, sigHandler ) )
    {
      clog << "main> Error establishing signal handler for SIGTERM\n" ;
      return 0 ;
    }

  if( !readConfFile() )
    {
      clog << "Failed to read config file: " << confFileName << endl ;
      return 0 ;
    }

  if( bufferNames.empty() )
    {
      clog << "main> Read 0 buffer names, aborting\n" ;
      return 0 ;
    }

  int imageBytes = imageWidth * imageWidth * (bitDepth / 8) ;
  int numInts = imageBytes / sizeof( int ) ;

  clog << "Building images of size " << imageWidth << 'x' << imageWidth
       << 'x' << bitDepth << endl ;
  clog << "Each image is " << imageBytes << " bytes, using "
       << numInts << " integers of size " << sizeof( int )
       << " bytes\n" ;
  clog << "Sleeping for " << sleepTime << " seconds between image "
       << "sends\n" ;
  clog << "Connecting to " << uplinkName << " on port " << uplinkPort << endl ;

  int* image = 0 ;
  try
    {
      uplinkSock = new UFClientSocket ;
      image = new int[ numInts ] ;
    }
  catch( std::bad_alloc )
    {
      clog << "main> Memory allocation failure\n" ;

      if( uplinkSock != NULL )
	{
	  delete uplinkSock ; uplinkSock = 0 ;
	}
      return 0 ;
    }

  if( uplinkSock->connect( uplinkName, uplinkPort ) < 0 )
    {
      clog << "main> Unable to connect to " << uplinkName
	   << ':' << uplinkPort << endl ;
      delete uplinkSock ; uplinkSock = 0 ;
      delete[] image ; image = 0 ;
      return 0 ;
    }

  clog << "Connected to " << uplinkName << ':' << uplinkPort
       << endl ;

  UFInts* ufImage = 0 ;
  int value = 0 ;

  while( keepRunning )
    {

      ::sleep( sleepTime ) ;

      clog << "Building image...\n" ;

      for( int i = 0 ; i < numInts ; ++i )
	{
	  // Catch overflow
	  if( value < 0 )
	    {
	      value = 0 ;
	    }
	  image[ i ] = value++ ;
	}

      vector< string >::size_type randomIndex = rand() % bufferNames.size() ;
      string buffName = bufferNames[ randomIndex ] ;

      try
	{
	  ufImage =  new UFInts( buffName, (const int*) image,
			       numInts ) ;
	}
      catch( std::bad_alloc )
	{
	  clog << "main> Memory allocation failure allocating UFInts\n" ;
	  keepRunning = false ;
	  continue ;
	}

      clog << "Sending image to uplink (buffer: "
	   << buffName << ")\n" ;

      if( uplinkSock->send( ufImage ) <= 0 )
	{
	  clog << "main> Write error to uplink: "
	       << strerror( errno ) << endl ;
	  keepRunning = false ;
	  continue ;
	}

      clog << "Image successfully written to uplink\n" ;
      delete ufImage ; ufImage = 0 ;

    }

  //  delete ufImage ; ufImage = 0 ;
  delete[] image ; image = 0 ;

  clog << "Shutting down...\n" ; 

  uplinkSock->close() ;
  delete uplinkSock ; uplinkSock = 0 ;

  return 0 ;

}

void sigHandler( int )
{
  clog << "Caught signal, shutting down...\n" ;
  keepRunning = false ;
}

bool readConfFile()
{
  ifstream inFile( confFileName.c_str() ) ;
  if( !inFile.is_open() )
    {
      clog << "readConfFile> Unable to open " << confFileName << endl ;
      return false ;
    }

  string line ;
  while( getline( inFile, line ) )
    {
      if( line.empty() || '#' == line[ 0 ] )
	{
	  // Empty or comment
	  continue ;
	}

      // Otherwise just add it
      bufferNames.push_back( line ) ;

    } // while

  clog << "readConfFile> Read " << bufferNames.size() << " buffer names\n" ;
  inFile.close() ;
  return true ;
}

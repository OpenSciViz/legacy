#if !defined(__ufervices_cc__)
#define __ufservices_cc__ "$Name:  $ $Id: ufservices.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __ufservices_cc__;

#include "UFServices.h"

int main(int argc, char** argv) {
  UFServices::main(argc, argv);
}
      
#endif // __ufervices_cc__

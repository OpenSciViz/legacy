/* pingserv.cc */

const char rcsId[] = "$Id: pingserv.cc 14 2008-06-11 01:49:45Z hon $";

#include "iostream"
#include "string"
#include "cctype"
#include "cstring"

#include "UFRuntime.h"
#include "UFServerSocket.h"
#include "UFProtocol.h"
#include "UFTimeStamp.h"
#include "UFStrings.h"
#include "UFBytes.h"
#include "UFShorts.h"
#include "UFInts.h"
#include "UFFloats.h"

int main( int argc, char** argv )
{
  string port("55555");
  
  if( argc > 1 )
    port = argv[ 1 ];

  unsigned short int portNo = (unsigned short int) atoi(port.c_str());
  cout << "portNo = " << portNo << endl;

  // create a listening server
  UFServerSocket _server(portNo);
  clog	<< "pingserv> waiting for client connection on port #"
	<< portNo << endl ;

  while( true )
    {
      clog<<"pingserv> waiting for client connection on: "<<_server.description()<<endl;
      UFSocket _connection = _server.listenAndAccept();

      if( !_connection.validConnection() )
	{
	  clog	<< "pingserv> Unable to establish client connection: "
		<< strerror( errno ) << endl ;
	  sleep( 2 ) ;
	  continue ;
	}

      clog << "pingserv> Got client connection..." << endl ;
      // enter recv/send loop
      unsigned long int msgcnt = 0 ;
      string newnam ;

      while( true )
	{
	  clog	<< "pingserv> msgcnt= " << msgcnt++
		<< ", waiting for client ping mesg..."<< endl ;

	  UFProtocol *ufp = UFProtocol::createFrom( _connection ) ;

	  if( ufp == 0 )
	    {
	      clog<<"pingserv> UFProtocol::createFrom failed?..."<<endl;
	      break ;
	    }

	  clog	<< "pingserv> name= " << ufp->name() << ", time= "
		<< ufp->timestamp() << "\n"
		<< ", type= " << ufp->typeId() << ", desc.: "
		<< ufp->description() << endl ;

	  newnam = "Alive -- " ;
	  newnam += ufp->name() ;
	  ufp->rename( newnam ) ;
	  ufp->stampTime( UFRuntime::currentTime() ) ;

	  if( _connection.writable() > 0 ) {
	    if( _connection.send( ufp ) <= 0 )
	      clog << "pingserv> Write error to client"<<endl;
	  }
	  else {
	    clog<<"pingserv> ? socket not writable"<<endl;
	    _connection.close();
	    delete ufp ;
	    break;
	  }
 
	  delete ufp ;
	} // close inner client while

      clog << "Connection closed.\n" ;

      // Let the socket close properly
      sleep( 2 ) ;

    } // close outer while

  return 0 ;
}


      

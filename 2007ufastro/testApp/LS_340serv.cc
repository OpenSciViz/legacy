const char rcsId[] = "$Id: LS_340serv.cc 14 2008-06-11 01:49:45Z hon $";

// A client should connect to this server to send requests/commands to the
// Lakeshore 340 Temperature Controller.
//
// This client will make sure it is a valid command before passing the command to
// the LS 340.
//
// UFStrings objects are used as communication between the client and this server.
// ASCII text strings are used to communicate with the LS 340.
//
// The LS 340 can be set up to use different line terminators:
//   -- LF, CR, CR/LF, or LF/CR
// (LF = ascii value 10, CR = ascii value 13)
// 


#include "iostream.h"
#include "string"

#include "UFRuntime.h"
#include "UFServerSocket.h"
#include "UFClientSocket.h"
#include "UFProtocol.h"
#include "UFTimeStamp.h"
#include "UFStrings.h"
#include "UFBytes.h"
#include "UFShorts.h"
#include "UFInts.h"
#include "UFFloats.h"
#include "UFFrames.h"

static UFSocket theConnection; // globally accessed by sigHandler
static UFClientSocket _client;

static void lostConnection(int sig) {
  if( sig == SIGPIPE ) {
    cout << "lostConnection> sig == SIGPIPE (int sig: " << sig << ")"<< endl;
    theConnection.close();
    _client.close();
  }
  if( sig == SIGINT ) {
    cout << "lostConnection> sig == SIGPIPE (int sig: " << sig << ")"<< endl;
    theConnection.close();
    _client.close();
    exit(0);
  }
  UFPosixRuntime::defaultSigHandler(sig);
}

char* getData() {
  unsigned char* retVal = 0;
  int numAvail = 0;
  int numRead = 0;

  numAvail = _client.available();
  if (numAvail < 0) {
    cerr<<"getData> Can't retrieve bytes available from _client"<<endl;
  }
  else if (numAvail == 0) {
    cerr<<"getData> No data available from client"<<endl;
  }
  else {
    cerr<<"getData> numAvail = "<<numAvail<<endl;
    retVal = new unsigned char[numAvail+1];
    cerr<<"getData> after allocating buffer for retVal"<<endl;
    numRead = _client.recv(retVal, numAvail);
    cerr<<"getData> numRead = "<<numRead<<endl;
    if (numRead < 0) {
      cerr<<"getData> Error reading from device"<<endl;
      retVal = 0;
    }
    else {
      cerr<<"Setting retVal["<<numAvail<<"] = "<<'\0'<<endl;
      retVal[numAvail] = '\0';
      cerr<<"retVal = "<<retVal<<endl;
    }
  }

  return (char*)retVal;
}

void uflisten(UFServerSocket& server) {
  cerr<<"LS_340serv> waiting for client connection on: "<<server.description()<<endl;
  theConnection = server.listenAndAccept();
  cerr<<"LS_340serv> got client connection..."<<endl;

  theConnection.readable();
  return;
}
  
int main(int argc, char** argv) {

  // define defaults
  string port("55555");
  string annexIP("192.168.111.101");
  string annexPort("7003");

//   if( argc > 1 ) { //port supplied
//     port = argv[1];
//   }

  if( argc < 4) {
    cout << "usage> LS_340serv myPort annexIP annexPort" << endl;
    exit(0);
  }

  // Should we check for arguments on command line and use defaults
  // if arguments not there?  Maybe later...
  port = argv[1];
  annexIP = argv[2];
  annexPort = argv[3];

  int portNo = atoi(port.data());
  int annexPortNo = atoi(annexPort.data());
  cerr << "portNo = " << portNo << endl;
  cerr << "annexIP = " << annexIP << endl;
  cerr << "annexPort = " << annexPortNo << endl;

  UFServerSocket theServer(portNo);

  cerr << "LS_340serv> connecting to Lakeshore 340 at IP " << annexIP
       << " on port " << annexPortNo << endl ;
  if (_client.connect( annexIP.c_str(), annexPortNo ) < 0 ) {
    cout << "LS_340serv> Can't connect to Lakeshore 340" << endl;
    exit(0);
  }

  UFPosixRuntime::setSignalHandler(lostConnection);
  cerr << "established signal handler for SIGPIPE = " << SIGPIPE << endl;
  cerr << "established signal handler for SIGINT = " << SIGINT << endl;
  // enter recv/send loop
  unsigned long msgcnt=0;
  string newnam;
  char sendString[255] = "";  // string to send to annex
  char* retData = 0;     // will hold return data from annex
  do {
    if( !theConnection.validConnection() ) { // test the valid function
      cerr<<"LS_340serv> ? no client connection ? listen for new client"<<endl;
      //theConnection = uflisten(theServer);
      uflisten(theServer);
    }

    if( theConnection.readable() > 0 ) { // something is available in the read buffer
      cerr<<"LS_340serv> msgcnt= "<<msgcnt++<<", reading client ping mesg..."<<endl;
      UFProtocol *ufp = UFProtocol::createFrom(theConnection);

      if( ufp == 0 ) {
        cerr<<"LS_340serv> UFProtocol::createFrom failed?..."<<endl;
        ufp = new UFStrings("Error in LS_340serv> UFProtocol::createFrom failed?...");
      }
      else {
// 	_client.send("YC5\n", _client.getInfo().fd);
// 	char* sendString = "YC5\r\n";
	UFStrings *ufs = dynamic_cast<UFStrings*>(ufp);
	if (ufs->elements() > 0) {
	  strcpy(sendString,ufs->valData(0));
	  clog<<"sendString = "<<sendString<<endl;
	}
	else {
	  strcpy(sendString, "YC5\n");
	}
	_client.send((unsigned char*)sendString, strlen(sendString));
	cerr<<"Before sleeping"<<endl;
	UFPosixRuntime::sleep(4.0); // sleep for 4 seconds
	cerr<<"After sleeping"<<endl;
	if ((retData = getData()) == 0) {
	  cerr<<"Can't get any data from device"<<endl;
	}
	else {
	  cerr<<"Got reply from device: "<<retData<<endl;
	  delete[] retData;
	}
      }

      cerr<<"LS_340serv> name= "<<ufp->name()<<", time= "<<ufp->timestamp()<<"\n"
	  <<", typ= "<<ufp->typeId()<<", desc.: "<<ufp->description()
	  <<" length = "<<ufp->length()<<endl;
      newnam =  ufp->name() + "Reply from LC_340serv";
      ufp->rename(newnam);
      ufp->stampTime(UFRuntime::currentTime());
      cerr<<"LS_340serv> respond, name= "<<ufp->name()<<", time= "<<ufp->timestamp()<<endl;
      if( theConnection.writable() > 0 ) {
        theConnection.send(ufp);
      }
      else {
        cerr<<"LS_340serv> ? socket not writable"<<endl;
	theConnection.close();
      }
      delete ufp;
    }
    else {
      //cerr<<"LS_340serv> sleeping due to lack of available data..."<<endl;
      sleep(1);
    }
  } while( true );
}


      

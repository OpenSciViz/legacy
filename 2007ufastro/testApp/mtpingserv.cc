/* mtpingserv.cc */

#include "iostream"
#include "string"
#include "new"
#include "map"
#include "list"

#include "cstring"
#include "cstdlib"
#include "csignal"
#include "pthread.h"

#include "UFRuntime.h"
#include "UFSocket.h"
#include "UFServerSocket.h"
#include "UFProtocol.h"
#include "UFTimeStamp.h"
#include "UFStrings.h"

using std::string ;
using std::clog ;
using std::endl ;
using std::map ;
using std::list ;

void		sigHandler( int ) ;
void*		handleClient( void* ) ;

volatile bool keepRunning = true ;
unsigned long int sleepTime = 10000 ;
unsigned short int portNum = 55555 ;

typedef map< UFSocket*, pthread_t > threadMapType ;

list< UFSocket* > deadThreadList ;
pthread_mutex_t deadThreadListMutex = PTHREAD_MUTEX_INITIALIZER ;

void removeClient( UFSocket* deadSock )
{
  pthread_mutex_lock( &deadThreadListMutex ) ;
  deadThreadList.push_back( deadSock ) ;
  pthread_mutex_unlock( &deadThreadListMutex ) ;
}

void usage( const string& progName )
{
  clog << endl ;
  clog << "Usage: " << progName << " <options>\n" ;
  clog << "-h: Print this help menu\n" ;
  clog << "-p <port>: Specify port number on which to listen ("
       << portNum << ")\n" ;
  clog << "-s <sleepTime>: Specify delay between loops (ms) ("
       << sleepTime << ")\n" ;
  clog << endl ;
}

int main( int argc, char** argv )
{

  int c = EOF ;
  while( (c = ::getopt( argc, argv, "hp:s:" ) ) != EOF )
    {
      switch( c )
        {
        case 'h':
          usage( argv[ 0 ] ) ;
          return 0 ;
        case 'p':
          portNum = static_cast< unsigned short int >( ::atoi( optarg ) ) ;
          break ;
        case 's':
          sleepTime = static_cast< unsigned long int>( ::atoi( optarg ) ) ;
          break ;
        }
    }

  if( SIG_ERR == ::signal( SIGINT, sigHandler ) )
    {
      clog	<< "Error establishing signal handler for SIGINT: "
		<< strerror( errno ) << endl ;
      return 0 ;
    }
  if( SIG_ERR == ::signal( SIGTERM, sigHandler ) )
    {
      clog	<< "Error establishing signal handler for SIGTERM: "
		<< strerror( errno ) << endl ;
      return 0 ;
    }
  if( SIG_ERR == ::signal( SIGALRM, sigHandler ) )
    {
      clog	<< "Error establishing signal handler for SIGALRM: "
		<< strerror( errno ) << endl ;
      return 0 ;
    }
  if( SIG_ERR == ::signal( SIGPIPE, sigHandler ) )
    {
      clog	<< "Error establishing signal handler for SIGPIPE: "
		<< strerror( errno ) << endl ;
      return 0 ;
    }

  UFServerSocket serverSock ;
  if( serverSock.listen( portNum ).listenFd < 0 )
    {
      clog	<< "Failed to establish listener on port: "
		<< portNum << ", because: "
		<< strerror( errno ) << endl ;
      return 0 ;
    }

  clog << "Waiting for connections on port " << portNum << endl ;

  // This structure is only needed by main()
  threadMapType threadMap ;

  while( keepRunning )
    {

      ::usleep( sleepTime ) ;

      // Check for dead threads
      pthread_mutex_lock( &deadThreadListMutex ) ;

      for( list< UFSocket* >::iterator dtPtr = deadThreadList.begin() ;
           dtPtr != deadThreadList.end() ; ++dtPtr )
        {
          threadMapType::iterator tPtr = threadMap.find( *dtPtr ) ;
          if( tPtr == threadMap.end() )
            {
              clog << "main> Unable to find dead socket: "
                   << (*dtPtr)->description() << endl ;
            }
          else
            {
              // Found the socket/threadID pair
              if( 0 != ::pthread_join( tPtr->second, 0 ) )
                {
                  clog << "main> Failed to join thread: "
                       << tPtr->second << endl ;
                }
              else
                {
                  clog << "main> Joined thread: " << tPtr->second
                       << endl ;
                }

              // Remove from the threadMap
              threadMap.erase( tPtr ) ;
            }

          // Close and deallocate the socket
          (*dtPtr)->close() ;
          delete *dtPtr ;

        } // for()
      deadThreadList.clear() ;

      pthread_mutex_unlock( &deadThreadListMutex ) ;

      int readable = serverSock.pendingIO( 0.0, serverSock.getInfo().listenFd ) ;
      if( readable < 0 )
        {
          clog	<< "Error in readable: " << strerror( errno ) << endl ;
          keepRunning = false ;
          continue ;
        }
      else if( 0 == readable )
        {
          // No big deal
          continue ;
        }

      UFSocket* clientSock = 0 ;
      try
        {
          clientSock = new UFSocket ;
        }
      catch( std::bad_alloc )
        {
          clog	<< "Memory allocation failure\n" ;
          keepRunning = false ;
        }

      int acceptRet = serverSock.accept( *clientSock ) ;
      if( acceptRet < 0 )
        {
          clog	<< "main> Error in accept: "
                << strerror( errno ) << endl ;
          // Non-fatal
          continue ;
        }

      clog << "main> Got client connection, spawning thread\n" ;

      pthread_t threadId = 0 ;
      if( pthread_create( &threadId, NULL, handleClient,
                          static_cast< void* >( clientSock ) ) < 0 )
        {
          clog << "main> Thread creation failure\n" ;
          delete clientSock ;
          keepRunning = false ;
          continue ;
        }

      clog << "Spawned new thread...(id: " << threadId << ")\n" ;
      threadMap.insert( threadMapType::value_type( clientSock, threadId ) ) ;
    }

  // keepRunning == false
  clog	<< "Shutting down...\n" ;
  serverSock.close() ;

  clog << "Waiting for threads to finish...\n" ;

  for( threadMapType::iterator ptr = threadMap.begin() ; ptr != threadMap.end() ;
       ++ptr )
    {
      if( 0 != ::pthread_join( ptr->second, 0 ) )
        {
          clog << "main> Failed to join thread: " << ptr->second
               << endl ;
        }
      else
        {
          clog << "main> Joined thread: " << ptr->second << endl ;
        }

      // Close and deallocate socket
      ptr->first->close() ;
      delete ptr->first ;
    }

  threadMap.clear() ;

  // All is well
  return 0 ;

}

void* handleClient( void* data )
{

  UFSocket* clientSock = static_cast< UFSocket* >( data ) ;
  if( NULL == clientSock )
    {
      clog << "Failed to convert from void* to UFSocket*\n" ;
      return NULL ;
    }

  bool isConnected = true ;

  // Just keep reading from the socket indefinitely
  while( isConnected && keepRunning )
    {

      ::usleep( sleepTime ) ;

      int readable = clientSock->readable() ;
      if( readable < 0 )
        {
          clog << "handleClient> readable() error: "
               << strerror( errno ) << endl ;
          isConnected = false ;
          continue ;
        }
      else if( 0 == readable )
        {
          // No data, no big deal
          continue ;
        }

      // Data available
      UFProtocol* protoPtr = UFProtocol::createFrom( *clientSock ) ;
      if( NULL == protoPtr )
        {
          clog << "handleClient> Read error from client\n" ;
          isConnected = false ;
          continue ;
        }

      clog << "handleClient> Read object, name: "
           << protoPtr->name() << endl ;

      //      clog << "handleClient> Read object..\n" ;
      //      clog << "Loop Count: " << loopCount << endl ;
      //      clog << "Name: " << protoPtr->name() << endl ;
      //      clog << "Type ID: " << protoPtr->typeId() << endl ;
      //      clog << "Description: " << protoPtr->description() << endl ;
      //      clog << "Length: " << protoPtr->length() << endl ;

      string newName( "Alive -- " ) ;
      newName += protoPtr->name() ;
      protoPtr->rename( newName ) ;
      protoPtr->stampTime( UFRuntime::currentTime() ) ;

      if( clientSock->send( protoPtr ) <= 0 )
        {
          clog << "handleClient> Write error to client\n" ;
          delete protoPtr ;
          isConnected = false ;
          continue ;
        }
      else
        {
          clog << "handleClient> Wrote object, name: "
               << protoPtr->name() << endl ;
        }

      delete protoPtr ;

    } // close while

  removeClient( clientSock ) ;
  return NULL ;

}
      
void sigHandler( int whichSig )
{
  clog << "Caught signal, shutting down\n" ;
  keepRunning = false ;
}


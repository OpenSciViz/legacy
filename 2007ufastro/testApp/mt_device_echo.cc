/* mt_device_echo.cc
 * This is a multithreaded trivial serial port server.
 * Invariant: No thread may posess more than one locked semaphore.
 */

#ifndef __mt_device_echo_cc__
#define __mt_device_echo_cc__ "$Name:  $ $Id: mt_device_echo.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __mt_device_echo_cc__ ;

#include	<new>
#include	<string>
#include	<map>
#include	<vector>
#include	<list>
#include	<algorithm>
#include	<iostream>
#include	<fstream>
#include	<queue>

#include	<sys/types.h>
#include	<unistd.h>
#include	<semaphore.h>
#include	<pthread.h>
#include	<cstring>
#include	<cerrno>
#include	<cstdlib>
#include	<csignal>

#include	"UFProtocol.h"
__UFProtocol_H__(__mt_device_echo_cc__) ;

#include	"UFServerSocket.h"
__UFServerSocket_H__(__mt_device_echo_cc__) ;

#include	"UFClientSocket.h"
__UFClientSocket_H__(__mt_device_echo_cc__) ;

#include	"UFStrings.h"
__UFStrings_H__(__mt_device_echo_cc__) ;

#include	"UFStringTokenizer.h"
__UFStringTokenizer_H__(__mt_device_echo_cc__) ;

#include	"mt_device_echo.h"
__mt_device_echo_H__(__mt_device_echo_cc__) ;

using std::string ;
using std::clog ;
using std::endl ;
using std::list ;
using std::priority_queue ;
using std::map ;
using std::vector ;

/**
 * The hostname/IP of the annex.
 */
string		annexHost( "192.168.111.101" ) ;

/**
 * The port on the annex to which to connect.
 */
unsigned int	annexPort = 7005 ;

/**
 * The port on the local host on which to listen for client connections.
 */
unsigned int	localPort = 5551 ;

/**
 * Locking semaphore for the clientList..
 */
sem_t		clientListSem ;

/**
 * Locking semaphore for the clientInputQueue.
 */
sem_t clientInputQueueSem ;

/**
 * Locking semaphore for the clientOutputQueue.
 */
sem_t clientOutputQueueSem ;

/**
 * Locking semaphore for the annexOutputQueue.
 */
sem_t annexOutputQueueSem ;

/**
 * Locking semaphore for the serverCommandQueue.
 */
sem_t serverCommandQueueSem ;

/**
 * Type used to store information about currently connected
 * clients.
 */
typedef list< client* > clientListType ;

/**
 * The structure used to store information about currently
 * connected clients.
 */
clientListType clientList ;

/**
 * Type used to store queued clientMessages.
 */
typedef list< clientMessage* > messageQueueType ;

/**
 * Structure used to store clientMessages read from
 * a client.
 */
messageQueueType	clientInputQueue ;

/**
 * Structure used to store clientMessages that are
 * queued to send to a client.
 */
messageQueueType clientOutputQueue ;

/**
 * Type used to store commands to be executed.
 */
typedef queue< clientCommand* > pQueueType ;

/**
 * Structure to store commands to be executed by
 * the annexThread.
 */
pQueueType		annexOutputQueue ;

/**
 * Type used to store the server command queue.
 */
typedef queue< Command* > commandQueueType ;

/**
 * A structure storing commands to be executed by the server.
 */
commandQueueType serverCommandQueue ;

/**
 * Type used to store the information about the currently
 * available commands.
 */
typedef map< string, commInfo* >	commandMapType ;

/**
 * The structure used to store command information
 * for each of the supported annex devices.
 */
vector< commandMapType* >		commandMaps ;

/**
 * Type used to store commands that the server supports.
 */
typedef map< string, Command* > serverCommandMapType ; 

/**
 * The structure used to store command information
 * about the commands the server currently supports.
 */
serverCommandMapType serverCommandMap ;

/**
 * The type of the server command handlers.
 */
typedef void (*serverCommandHandlerType)( Command* ) ;

/**
 * keepRunning is true while the server should remain
 * running.  It is set to false when the server must
 * shutdown (or possible rehash).
 */
static volatile bool keepRunning = true ;

/**
 * The current sequence number.  This variable is used to
 * uniquely identify each requested message/command from
 * each of the connected clients.
 */
static unsigned int globalSeq = 0 ;

/**
 * A UFStrings representation of the the device file.
 */
UFStrings*		fileStrings = 0 ;

/**
 * A structure used to store information about the currently
 * connected devices.
 */
vector< UFDevice* > devices ;

/**
 * Useful debugging tool, used to output a UFStrings object to
 * a C++ output stream.
 */
std::ostream& operator<<( std::ostream& out, const UFStrings& outStrings )
{
  out	<< "Name: " << outStrings.name() << endl ;
  for( int i = 0 ; i < outStrings.elements() ; ++i )
    {
      out	<< "[" << i << "]: " << outStrings[ i ] << endl ;
    }
  return out ;
}

/**
 * Find a client in the clientList.
 * Precondition:
 *  Calling thread must hold lock on clientList (clientListSem).
 */
bool findClient( const client* theClient )
{
  return std::find( clientList.begin(), clientList.end(), theClient )
    != clientList.end() ;
}

/**
 * Return true if this string from a client is requesting a server
 * command.
 */
bool isServerCommand( const string& theString )
{
  return (!theString.empty() && 'R' == theString[ 0 ]) ;
}

/**
 * Remove a client from the clientList.
 * Precondition:
 *  Calling thread must hold lock on clientList (clientListSem).
 */
bool removeClient( const client* theClient )
{
  for( clientListType::iterator ptr = clientList.begin(), end = clientList.end() ;
       ptr != end ; ++ptr )
    {
      if( *ptr == theClient )
	{
	  clientList.erase( ptr ) ;
	  return true ;
	}
    }
  return false ;
}

int main( int argc, char** argv )
{

  // Take a guess at the device file name.
  string deviceFileName( "device.txt" ) ;

  // Parse command line options.
  int c = EOF ;
  while( (c = getopt( argc, argv, "a:d:hl:r:" )) != EOF )
    {
      switch( c )
	{
	case 'a':
	  annexHost = optarg ;
	  break ;
	case 'd':
	  deviceFileName = optarg ;
	  break ;
	case 'h':
	  usage( argv[ 0 ] ) ;
	  return 0 ;
	case 'l':
	  localPort = atoi( optarg ) ;
	  break ;
	case 'r':
	  annexPort = atoi( optarg ) ;
	  break ;
	default:
	  clog << "Unrecognized option\n" ;
	  usage( argv[ 0 ] ) ;
	  break ;
	}
    }

  // Attempt to read the device file.
  if( !readDeviceFile( deviceFileName ) )
    {
      clog	<< "Unable to read device file: " << deviceFileName << endl ;
      return 1 ;
    }

  // Map SIGPIPE, this might already be done by some part
  // of libUF.
  if( SIG_ERR == signal( SIGPIPE, SIG_IGN ) )
    {
      clog	<< "Failed to map SIGPIPE: " << strerror( errno ) << endl ;
      return 1 ;
    }
  /*
    if( SIG_ERR == signal( SIGINT, SIG_IGN ) )
    {
    clog	<< "Failed to map SIGINT: " << strerror( errno ) << endl ;
    return 0 ;
    }
  */

  // Allocate the command maps.
  // Let's be generous to start, all ASCII characters allowed for
  // device types.
  for( vector< commandMapType >::size_type i = 0 ; i < 127 ; ++i )
    {
      commandMaps.push_back( new commandMapType ) ;
    }

  /*
   * Initialize the semaphores unlocked.
   */
  if( sem_init( &clientListSem, 0, 1 ) < 0 )
    {
      clog	<< "Unable to initiailize clientListSem: "
		<< strerror( errno ) << endl ;
      return 0 ;
    }

  if( sem_init( &clientInputQueueSem, 0, 1 ) < 0 )
    {
      clog	<< "Unable to initiailize clientInputQueueSem: "
		<< strerror( errno ) << endl ;
      return 0 ;
    }

  if( sem_init( &clientOutputQueueSem, 0, 1 ) < 0 )
    {
      clog	<< "Unable to initiailize clientOutputQueueSem: "
		<< strerror( errno ) << endl ;
      return 0 ;
    }

  if( sem_init( &annexOutputQueueSem, 0, 1 ) < 0 )
    {
      clog	<< "Unable to initiailize annexOutputQueueSem: "
		<< strerror( errno ) << endl ;
      return 0 ;
    }

  // Allocate the server and annex sockets.
  UFClientSocket* annexSock = 0 ;
  UFServerSocket* servSock = 0 ;
  try
    {
      annexSock = new UFClientSocket( annexPort ) ;
      servSock = new UFServerSocket( localPort ) ;
    }
  catch( std::bad_alloc )
    {
      clog	<< "Memory allocation failure\n" ;
      return 1 ;
    }

  // Connect to the annex.
  if( annexSock->connect( annexHost, annexPort ) < 0 )
    {
      clog	<< "Unable to connect to annex host [" << annexHost << "] on port "
		<< annexPort << " because: " << strerror( errno ) << endl ;
      return 1 ;
    }

  // Variables storing id's for the various threads.
  pthread_t	annexThread_id,
    servThread_id,
    toClientThread_id,
    fromClientThread_id ;

  /*
   * Instantiate threads and start them running.
   */
  if( pthread_create( &annexThread_id, 0, annexThread,
		      static_cast< void* >( annexSock ) ) != 0 )
    {
      clog	<< "Unable to spawn annexThread: " << strerror( errno ) << endl ;
      return 1 ;
    }

  if( pthread_create( &servThread_id, 0, servThread,
		      static_cast< void* >( servSock ) ) != 0 )
    {
      clog	<< "Unable to spwan servThread: " << strerror( errno ) << endl ;
      return 1 ;
    }

  if( pthread_create( &toClientThread_id, 0, toClientThread, 0 ) != 0 )
    {
      clog	<< "Unable to spawn toClientThread: " << strerror( errno ) << endl ;
      return 1 ;
    }

  if( pthread_create( &fromClientThread_id, 0, fromClientThread, 0 ) != 0 )
    {
      clog	<< "Unable to spawn fromClientThread: " << strerror( errno ) << endl ;
      return 1 ;
    }

  clog << "Got connected!\n" ;

  /*
   * Main Thread:
   * Responsible for dealing with newly acquired client command
   * requests.
   */
  while( keepRunning )
    {

      // Be sure to sleep a bit so we don't sit in a spinning loop.
      usleep( 10000 ) ;

      // Process new messages.
      // This assumes that clientInputQueue.empty() is reasonably
      // thread safe.
      if( clientInputQueue.empty() )
	{
	  // No new messages to process, just continue to the
	  // next loop.
	  continue ;
	}

      // We can't hold the lock on two semaphores at once, so
      // here's what we'll do:
      // Build a local queue into which we will put any new client
      // command requests.
      messageQueueType localQueue ;

      // Obtain lock on clientInputQueue semaphore
      sem_wait( &clientInputQueueSem ) ;

      // Retrieve any commands recently read from any clients
      //	clog << "Read " << clientInputQueue.size() << " objects\n" ;

      // Iterate through the clientInputQueue, removing any items
      // it holds and putting them into the localQueue.
      for( messageQueueType::iterator ptr = clientInputQueue.begin() ;
	   ptr != clientInputQueue.end() ; ++ptr )
	{
	  //		clog << *((*ptr)->theMessage) << endl ;

	  // Copy each object into the localQueue
	  localQueue.push_back( *ptr ) ;
	}

      // Remove all of the commands we have just received.
      // The above loop copied pointers to space allocated on
      // the heap.  Make sure to deallocate the memory when
      // we're finished using the clientMessage's.
      clientInputQueue.clear() ;

      // Unlock the clientInputQueue semaphore.
      sem_post( &clientInputQueueSem ) ;

      // Need to parse the commands here.
      // localQueue is a locally created structure so we
      // don't need to lock it.
      // TODO: Assign each object a sequence number.
      for( messageQueueType::iterator ptr = localQueue.begin(), end = localQueue.end() ;
	   ptr != end ; ++ptr )
	{

	  // We don't need to worry about the validity of theClient
	  // here because the send threads will check for this.
	  client* theClient = (*ptr)->theClient ;
	  UFStrings* writeMe = (*ptr)->theMessage ;

	  // This vector of std::string will hold the reply to the
	  // client.
	  vector< string > stringVector( writeMe->elements() + 1 ) ;

	  // The 0th element will be the name of the command.
	  // Make sure to stamp it with a command id.
	  strstream s ;

	  // This is the only place that globalSeq is modified or
	  // used...It's safe to go ahead and increment it here
	  // without locking.
	  s << globalSeq++ << ends ;
	  stringVector[ 0 ] = writeMe->name() + " " + s.str() ;

	  // Deallocate the memory of the silly strstream
	  delete[] s.str() ;

	  // Fill the vector with the commands read from the client.
	  for( int i = 0 ; i < writeMe->elements() ; ++i )
	    {
	      stringVector[ i ] =  (*writeMe)[ i ] ;
	    }

	  // ALL commands in this string must be OK before any
	  // of them are executed.

	  // Check that this command is valid on all rules we
	  // have defined in the protocol.
	  if( checkCommand( writeMe, stringVector ) )
	    {
	      // This will translate the command, and put it
	      // into the proper queue for handling:
	      // AnnexOutputQueue if a command needs to be issued
	      //  to the physical hardware.
	      // serverCommandQueue if the command can be executed
	      //  by internal lookups/calculations.
	      prepareCommand( theClient, writeMe ) ;

	      // Succeeded, mark as such
	      stringVector[ 0 ] += " accept" ;

	    }
	  else
	    {
	      // Bad command.
	      
	      // This command was rejected.  Mark it with the "reject"
	      // keyword.
	      stringVector[ 1 ] += " reject" ;

	      clog << "Found bad command:\n" << writeMe << endl ;
	     
	    }

	  // Deallocate the clientMessage, no longer needed.
	  delete *ptr ;

	  // writeMe is now invalidated
	  writeMe = 0 ;

	  // Preserve the options string, now with the success/error
	  // tag appended to it.
	  string name = stringVector[ 0 ] ;

	  // Remove the options string from the vector.  The UFStrings constructor
	  // expects its name to be specified separately.
	  stringVector.erase( stringVector.begin() ) ;

	  // Allocate a new UFString object with the given name and values
	  // vector.
	  writeMe = new UFStrings( name, stringVector ) ;
	  
	  clog << "Pushing into clientOutputQueue:\n" << *writeMe << endl ;

	  // Echo the string to the client.  This string may be an
	  // acknowledgement, or it may be an error reply.
	  sem_wait( &clientOutputQueueSem ) ;
	  clientOutputQueue.push_back( new clientMessage( theClient, writeMe ) ) ;
	  sem_post( &clientOutputQueueSem ) ;

	} // close for()

    }

  // When we reach this point, keepRunning has been set to false.
  // Kill the threads.
  clog << "Killing my children...\n" ;

  pthread_join( annexThread_id, 0 ) ;
  clog << "annexThread joined\n" ;

  pthread_join( servThread_id, 0 ) ;
  clog << "servThread joined\n" ;

  pthread_join( toClientThread_id, 0 ) ;
  clog << "toClientThread joined\n" ;

  pthread_join( fromClientThread_id, 0 ) ;
  clog << "fromClientThread joined\n" ;

  // Close and deallocate sockets.
  annexSock->close() ;
  delete annexSock ; annexSock = 0 ;

  servSock->close() ;
  delete servSock ; servSock = 0 ;

  // Destroy the semaphores.
  sem_destroy( &clientListSem ) ;
  sem_destroy( &clientInputQueueSem ) ;
  sem_destroy( &clientOutputQueueSem ) ;
  sem_destroy( &annexOutputQueueSem ) ;

  // Deallocate the command maps for each device.
  for( vector< commandMapType >::size_type i = 0 ; i < commandMaps.size() ; ++i )
    {
      delete commandMaps[ i ] ;
    }
  commandMaps.clear() ;

  // Close and deallocate client sockets.
  for( clientListType::iterator ptr = clientList.begin() ; ptr != clientList.end();
       ++ptr )
    {
      delete *ptr ;
    }

  // Delete the UFStrings object representing the configuration file.
  delete fileStrings ; fileStrings = 0 ;

  return 0 ;

}

/**
 * This thread is responsible for communicating (two-way) with the annex.
 */
void* annexThread( void* data )
{

  // Retrieve the pointer to the socket to the annex, already
  // connected.
  UFSocket* annexSock = static_cast< UFClientSocket* >( data ) ;
  if( NULL == annexSock )
    {
      clog	<< "annexThread> static_cast<> failure\n" ;
      return 0 ;
    }

  // Continue while the keepRunning variable is true.
  while( keepRunning )
    {
      // Don't hog the CPU, yield() doesn't seem to work properly
      // (at least in Linux).
      usleep( 10000 ) ;

      // This assumes that empty() is a reasonably threadsafe method.
      // If the annexOutputQueue is empty, skip the rest of the loop,
      // sleep, and try again.
      if( annexOutputQueue.empty() )
	{
	  continue ;
	}

      // There is some data waiting to be written.
      // Save a possible extraneous semaphore lock/unlock by
      // first checking to see if we have any room in the output
      // buffer to the annex.
      int writable = annexSock->writable() ;
      if( writable < 0 )
	{
	  // Connection error
	  clog	<< "annexThread> Lost connection to annex\n" ;
	  return 0 ;
	}

      if( 0 == writable )
	{
	  // Not available to be written
	  continue ;
	}

      // Otherwise, we can go ahead and write

      // Lock the annexOutputQueue semaphore.
      sem_wait( &annexOutputQueueSem ) ;

      // Retrieve a pointer to the first clientMessage.
      clientCommand* cPtr = annexOutputQueue.front() ;

      // Remove this clientMessage from the queue.
      annexOutputQueue.pop() ;

      // Unlock the annexOutputQueue semaphore.
      sem_post( &annexOutputQueueSem ) ;

      // Obtain pointer to UFStrings object to be written to
      // the annex.
      const string& writeMe = cPtr->getCommand() ;

      // Try to send the data to the annex in binary format.
      if( annexSock->send( reinterpret_cast< const unsigned char* >
			   ( writeMe.c_str() ), writeMe.size() ) < 0 )
	{
	  // Write error to annex
	  clog	<< "annexThread> Write error: "
		<< strerror( errno ) << endl ;
	  delete cPtr ;
	  return 0 ;
	}

      // Write successful.
      clog << "Wrote to annex: " << writeMe << endl ;

      // Wait for a little while for the annex to reply.
      // TODO: Wait for a specified amount of time, depending
      // on command.
      usleep( 500000 ) ;

      // See if there's something ready to be read from the annex.
      int available = annexSock->available() ;
      if( available < 0 )
	{
	  clog	<< "annexThread> Unable to poll annex: "
		<< strerror( errno ) << endl ;
	  delete cPtr ;
	  return 0 ;
	}

      // Is there any data ready to be received?
      if( 0 == available )
	{
	  // No data available, just continue.
	  clog	<< "annexThread> No response from annex\n" ;
	  continue ;
	}

      // Yup, some data ready to be read.

      // Allocate a temp input buffer of the appropriate size, and hope
      // that all data was accounted for by available() (no new data
      // comes along in the meantime).
      // Don't use try/catch here because this has proven to be
      // problematic in a multithreaded environment.
      unsigned char* inBuf = new (nothrow) unsigned char[ available + 1 ] ;
      if( NULL == inBuf )
	{
	  clog	<< "annexThread> Memory allocation error\n" ;
	  return 0 ;
	}

      // Try reading from the annex.
      int bytesRead = annexSock->recv( inBuf, available ) ;
      if( bytesRead < 0 )
	{
	  clog	<< "annexThread> Read error: "
		<< strerror( errno ) << endl ;
	  delete[] inBuf ;
	  delete cPtr ;
	  return 0 ;
	}

      // EOF?
      if( 0 == bytesRead )
	{
	  clog	<< "annexThread> EOF detected\n" ;
	  delete[] inBuf ;
	  delete cPtr ;
	  return 0 ;
	}

      // Null terminate the buffer.
      inBuf[ bytesRead ] = 0 ;
		
      clog << "annexThread> Read: " << inBuf << endl ;

      // Create a UFStrings object to write back to the client as
      // reply.  The validity of the client is left for verification by
      // the clientOutput thread.
      //      UFStrings* retMe = new UFStrings( writeMe->name(), localVector ) ;

      UFStrings* retMe = buildReply( cPtr,
				     reinterpret_cast< const char* >( inBuf ) ) ;

      // Deallocate the temp buffer.
      delete[] inBuf ;

      // Lock the clientOutputQueue.
      sem_wait( &clientOutputQueueSem ) ;

      // Put this reply message onto the end of the clientOutputQueue
      clientOutputQueue.push_back( new clientMessage( cPtr->getClient(), retMe ) ) ;

      // Unlock the clientOutputQueue.
      sem_post( &clientOutputQueueSem ) ;

      clog << "Sending to client:\n" << *retMe << endl ;

      // Deallocate the command written to the annex.
      delete cPtr ;

    } // while( keepRunning )

  return 0 ;

}

/**
 * This is a very simple thread:
 * It is responsible for handling incoming client connections and
 * putting those clients into the list of connected clients.
 * TODO: Authentication.
 */
void* servThread( void* data )
{

  // Obtain pointer to the server socket.
  UFServerSocket* servSock = static_cast< UFServerSocket* >( data ) ;
  if( NULL == servSock )
    {
      clog	<< "servThread> static_cast<> failure\n" ;
      return 0 ;
    }

  // Establish a listener on the specified port
  if( servSock->listen( localPort ).listenFd < 0 )
    {
      clog	<< "servThread> Unable to bind to local port " << localPort
		<< endl ;
      return 0 ;
    }

  // Continue running while main() tells us to.
  while( keepRunning )
    {

      // Don't hog the CPU.
      usleep( 10000 ) ;

      // Check for new connection by examining the socket's readable
      // state.  This is done instead of a blocking accept because
      // of the requirement that the thread be responsive when
      // keepRunning is set to false.
      int retVal = servSock->pendingIO( 0.0, servSock->getInfo().listenFd ) ;
      if( retVal < 0 )
	{
	  clog	<< "servThread> Error in readable: "
		<< strerror( errno ) << endl ;
	  return 0 ;
	}
      else if( 0 == retVal )
	{
	  // No new connections.
	  continue ;
	}

      // There is a possible new connection.

      // Try to capture the new client.
      UFSocket clientSock = servSock->listenAndAccept( localPort ) ;
      if( clientSock.getInfo().fd < 0 )
	{
	  // Nope, it wasn't meant to be...
	  clog	<< "servThread> Connection failure\n" ;
	  continue ;
	}

      clog << "Received new client connection\n" ;

      // Got a new connection, put it into the client connection list.
      sem_wait( &clientListSem ) ;
      clientList.push_back( new client( clientSock ) ) ;
      sem_post( &clientListSem ) ;
      clog << "Added client to clientList\n" ;

      // The client will now be polled by the client input thread.

    } // close while( keepRunning )

  return 0 ;
}

/**
 * Purpose of this thread:
 * Write outgoing data to recipient client(s).
 */
void* toClientThread( void* )
{

  while( keepRunning )
    {

      // Don't hog the CPU.
      usleep( 10000 ) ;

      // This assumes that std::list::empty is reasonably thread
      // safe.
      while( !clientOutputQueue.empty() )
	{

	  // There is data ready to be written, yoohoo!!
	  // Lock the client output queue.
	  sem_wait( &clientOutputQueueSem ) ;

	  // Obtain a pointer to the message to be written.
	  clientMessage* ptr = clientOutputQueue.front() ;

	  // Remove the message from the output queue.
	  clientOutputQueue.erase( clientOutputQueue.begin() ) ;

	  // Unlock the client output queue.
	  sem_post( &clientOutputQueueSem ) ;

	  // Just for convenience.
	  UFSocket& theSocket = ptr->theClient->sock ;
	  UFStrings* theMessage = ptr->theMessage ;

	  // Obtain a lock on the clientList.
	  sem_wait( &clientListSem ) ;

	  // Make sure that the client is still valid.
	  if( find( clientList.begin(), clientList.end(), ptr->theClient )
	      == clientList.end() )
	    {
	      // The client has been invalidated.

	      // Unlock the client list semaphore.
	      sem_post( &clientListSem ) ;

	      // Delete the clientMessage pointer.  This will NOT
	      // attempt to deallocate the client pointer itself,
	      // so no memory faults will occur.
	      delete ptr ;

	      // Skip to the next item in the client output buffer, if
	      // any.
	      continue ;
	    }

	  // Otherwise, the client is still valid.
	  // Retain the lock to the clientList until we are finished
	  // with the clientMessage pointer.

	  // Attempt to send the message.
	  if( theSocket.send( theMessage ) < 0 )
	    {
	      // Write error to client.
	      clog	<< "toClientThread> Write error to client: "
			<< strerror( errno ) << endl ;

	      // Remove the now invalidated client from the clientList.
	      // We meet the precondition of removeClient:
	      // We hold the lock on the clientList.
	      removeClient( ptr->theClient ) ;

	      // Deallocate the client pointer, and set to zero.
	      delete ptr->theClient ; ptr->theClient = 0 ;

	      // Unlock the client list semaphore.
	      sem_post( &clientListSem ) ;

	      // Deallocate the clientMessage.
	      delete ptr ;

	      // Continue with processing the client output queue.
	      continue ;
	    }

	  // We are finished with the client list, unlock it.
	  sem_post( &clientListSem ) ;

	  clog	<< "toClientThread> Sent:\n" << *theMessage << endl ;

	  // Deleting the clientMessage pointer will not invalidate the
	  // client pointer.
	  delete ptr ;

	} // while( !clientOutputQueue.empty() )

    } // while( keepRunning )

  return 0 ;

}

/** 
 * This thread is responsible for reading UFStrings objects from
 * unsuspecting client connections.
 */
void* fromClientThread( void* )
{

  // This vector will hold all input messages once read.
  // This is done because no thread may hold more than one
  // semaphore lock at any time.
  vector< clientMessage* > localVector ;

  while( keepRunning )
    {

      // Don't hog the CPU.
      usleep( 10000 ) ;

      // Lock the client list.
      sem_wait( &clientListSem ) ;

      // Iterate through the client list, testing each connection
      // for readable data.
      clientListType::iterator ptr = clientList.begin() ;
      while( ptr != clientList.end() )
	{

	  // The presence of this client in the client list
	  // means that the client is valid.

	  // Test connection for data available.
	  int avail = (*ptr)->sock.readable() ;
	  if( avail < 0 )
	    {
	      // Socket read error

	      // Deallocate the client pointer.
	      delete *ptr ;

	      // Remove the client from the list and update the iterator.
	      ptr = clientList.erase( ptr ) ;

	      // Don't increment the iterator
	      continue ;
	    }

	  if( 0 == avail )
	    {
	      // No data ready to read

	      // Move to next client and continue.
	      ++ptr ;

	      continue ;
	    }

	  // Attempt to read a UFStrings object from the client.
	  UFStrings* inStrings = dynamic_cast< UFStrings* >
	    ( UFProtocol::createFrom( (*ptr)->sock ) ) ;
	  if( NULL == inStrings )
	    {
	      // Read error from client
	      clog	<< "fromClientThread> Read error: "
			<< strerror( errno ) << endl ;

	      // Deallocate the client.
	      delete *ptr ;

	      // Erase this client from the client list and update
	      // the iterator.
	      ptr = clientList.erase( ptr ) ;

	      // Don't increment iterator
	      continue ;
	    }

	  // Read was successful.
	  clog << "fromClientThread> Read:\n" << *inStrings << endl ;

	  // Got an input UFStrings object.
	  // Add it to the local queue.
	  localVector.push_back( new clientMessage( *ptr, inStrings ) ) ;

	  // Increment to next client in structure.
	  ++ptr ;

	} // close while( ptr != ... )

      // Release the clientList semaphore
      sem_post( &clientListSem ) ;

      if( localVector.empty() )
	{
	  // No data read from any client
	  continue ;
	}

      // Lock the client input queue.
      sem_wait( &clientInputQueueSem ) ;

      // Copy the UFStrings objects read from the clients into the
      // global input queue.
      for( vector< clientMessage* >::size_type i = 0 ; i < localVector.size() ; ++i )
	{
	  clientInputQueue.push_back( localVector[ i ] ) ;
	}

      // Unlock client input queue semaphore.
      sem_post( &clientInputQueueSem ) ;

      // Clear the local vector.
      localVector.clear() ;

    } // while( keepRunning )

  return 0 ;
}

// Output a useful and friendly ERROR message to the user.
void usage( const string& progName )
{
  clog	<< "\nUsage: " << progName << " [options]\n\n" ;
  clog	<< "-h\t\tPrint this menu\n" ;
  clog	<< "-a <hostname>\tSpecify annex host address\n" ;
  clog	<< "-d <file name>\tSpecify device file name\n" ;
  clog	<< "-l <port>\tSpecify local port on which to listen for client connections\n" ;
  clog	<< "-r <port>\tSpecify annex port to connect to\n" ;
  clog	<< endl ;
}

/**
 * Handle asynchronous signals.
 */
void sigHandler( int whichSig )
{
  clog << "Caugh signal: " << whichSig << endl ;

  // For now, halt program execution and shutdown if any
  // signal occurs.
  keepRunning = false ;

  switch( whichSig )
    {
    case SIGINT:
      clog	<< "Caught SIGINT\n" ;
      break ;
    }
}

/**
 * Format of the strings [2,theMessage->elements()) is as follows:
 * c <command name> [args]
 * Where c is a single character representing the type of object
 * in the chain, or 'S' for server command.
 */
bool checkCommand( const UFStrings* theMessage, vector< string >& theStrings )
{

  // theMessage is guaranteed to be non-NULL

  // Elements must be at least 2
  // [0] the name
  // [1] is a string of information/options/etc
  // [2,size) contains commands to be executed
  if( theMessage->elements() < 3 )
    {
      theStrings[ 1 ] += " reject" ;
      return false ;
    }

  bool ok = true ;

  for( int i = 2 ; i < theMessage->elements() ; ++i )
    {
      UFStringTokenizer st( *(theMessage->stringAt( i )) ) ;

      clog << "Checking: " << *theMessage->stringAt( i ) << endl ;

      if( st.empty() )
	{
	  ok = false ;
	  clog << "\tInvalid number of arguments\n" ;
	  theStrings[ i + 2 ] += " reject" ;
	  continue ;
	}

      // First token must be exactly one character, it's the
      // device type identifier.
      if( st[ 0 ].size() != 1 )
	{
	  ok = false ;
	  clog << "\tInvalid specifier\n" ;
	  theStrings[ i + 2 ] += " reject" ;
	  continue ;
	}

      if( st[ 1 ].size() != 1 )
	{
	  ok = false ;
	  clog << "\tInvalid device\n" ;
	  theStrings[ i + 2 ] += " reject" ;
	  continue ;
	}

      //      clog << "st[ 0 ][ 0 ]: " << st[ 0 ][ 0 ] << endl ;
      //      clog << "(int) st[ 0 ][ 0 ]: " << ((int) st[ 0 ][ 0 ]) << endl ;
      //      clog << "commandMaps[ st[ 0 ][ 0 ] ]->size(): " <<
      //	commandMaps[ st[ 0 ][ 0 ] ]->size() << endl ;
      //      clog << "Searching for command: " << st[ 1 ] << endl ;

//       if( commandMaps[ st[ 0 ][ 0 ] ]->find( st[ 1 ] ) ==
// 	  commandMaps[ st[ 0 ][ 0 ] ]->end() )
// 	{
// 	  ok = false ;
// 	  clog << "\tCouldn't find command: " << st[ 1 ] << endl ;
// 	  theStrings[ i + 1 ] += " reject: Unknown command;" ;
// 	  continue ;
// 	}

//       commInfo* theComm = commandMaps[ st[ 0 ][ 0 ] ]->find( st[ 1 ] )->second ;
//       if( theComm->getOption( commInfo::OPT_BAD ) )
// 	{
// 	  ok = false ;
// 	  clog << "\tBad command\n" ;
// 	  theStrings[ i + 1 ] += " reject: Bad command;" ;
// 	  continue ;
// 	}

      // This command is ok.
      clog << "\tOk\n" ;

    } // close for()

  clog << endl ;

  if( !ok )
    {
      theStrings[ 1 ] += " reject" ;
    }

  return ok ;
}

/**
 * Read in the file with specifications on the devices connected to
 * the annex.
 */
bool readDeviceFile( const string& deviceFileName )
{
  ifstream inFile( deviceFileName.c_str() ) ;
  if( !inFile.is_open() )
    {
      clog	<< "Unable to open " << deviceFileName << endl ;
      return false ;
    }

  string	inLine ;
  size_t	lineNumber = 0 ;
  size_t	motorNumber = 0 ;
  size_t        numMotors = 0 ;

  // Get the number of devices
  while( getline( inFile, inLine ) )
    {
      lineNumber++ ;

      if( inLine.empty() || ';' == inLine[ 0 ] )
	{
	  // empty or comment, skip it
	  continue ;
	}

      numMotors = atoi( inLine.c_str() ) ;

      //	clog << lineNumber << ": Got numMotors: " << numMotors << endl ;
      break ;
    }

  // Get info string for each motor
  while( motorNumber++ <= numMotors )
    {
      lineNumber++ ;

      if( !getline( inFile, inLine ) )
	{
	  clog	<< lineNumber << ": End Of File detected\n" ;
	  inFile.close() ;
	  return false ;
	}

      if( inLine.empty() || ';' == inLine[ 0 ] )
	{
	  // empty or comment, skip it
	  continue ;
	}

      UFStringTokenizer st( inLine ) ;
      //	clog << lineNumber << ": Got info line: " << st << endl ;

    }

  // Get named positions for each motor
  motorNumber = 1 ;

  while( motorNumber++ <= numMotors )
    {

      lineNumber++ ;
      getline( inFile, inLine ) ;

      lineNumber++ ;
      getline( inFile, inLine ) ;

      int numPositions = atoi( UFStringTokenizer( inLine )[ 1 ].c_str() ) ;
      //	clog << lineNumber << ": " << numPositions << " named positions\n" ;

      // Read the comment line
      lineNumber++ ;
      getline( inFile, inLine ) ;

      int posNum = 1 ;
      while( posNum++ <= numPositions )
	{
	  lineNumber++ ;
	  getline( inFile, inLine ) ;

	  UFStringTokenizer st( inLine ) ;
	  //		clog << "Read named position: " << st << endl ;
	}

    }

  // Re-read the file for the purposes of building a UFStrings object
  // that may be sent to clients
  vector< string > fileVector ;

  inFile.seekg( 0, ios::beg ) ;
  while( getline( inFile, inLine ) )
    {
      if( inLine.empty() || ';' == inLine[ 0 ] )
	{
	  // comment line
	}
      fileVector.push_back( inLine ) ;
    }

  fileStrings = new UFStrings( deviceFileName, fileVector ) ;

  clog << "Read input file " << deviceFileName << endl ;

  inFile.close() ;
  return true ;
}

/**
 * This method is responsible for breaking up the single UFStrings
 * object sent from the client into theCommand->elements() - 1 separate
 * commands, each translated into the proper format for direct execution
 * by the annex component to which it is intended, or for the server
 * itself.
 * This method must then also place these commands into the appropriate
 * queues for handling.
 */
void prepareCommand( client* theClient, const UFStrings* theCommand )
{

  for( int i = 1 ; i < theCommand->elements() ; i++ )
    {
      if( isServerCommand( (*theCommand)[ i ] ) )
	{
       
	  // It should already be properly formatted
	  // Add it to the serverCommandQueue
	  // The syntax of and the number of arguments for this
	  // command has already been verified.

	  UFStringTokenizer st( (*theCommand)[ i ] ) ;
	  UFStringTokenizer name( theCommand->name() ) ;
	  const string options( (*theCommand)[ 0 ] ) ;

	  Command* newCommand = new (nothrow) Command( theClient,
					      st[ 1 ],
					      st.assemble( 2 ),
					      options ) ;
	  if( NULL == newCommand )
	    {
	      clog << "prepareCommand> Memory allocation error\n" ;
	      continue ;
	    }

	  sem_wait( &serverCommandQueueSem ) ;
	  serverCommandQueue.push( newCommand ) ;
	  sem_post( &serverCommandQueueSem ) ;

	  continue ;
	}

      // Otherwise, it's a device command
      // Need to translate the command into device protocol
      // and place into annexOutputQueue
      //      UFStringTokenizer st( (*theCommand)[ i ] ) ;
      //      char whichMotor = st[ 1 ][ 0 ] ;

      // Let the UFDevice subclass handle the translation
      // No locks are held while calling this method.
      //      string comm =
      //	devices[ whichMotor ]->prepareCommand( (*theCommand)[ i ] ) ;
      //      sem_wait( &annexOutputQueueSem ) ;
      //      annexOutputQueue.push( comm ) ;
      //      sem_post( &annexOutputQueueSem ) ;

      // This is the only place that the global sequence number is
      // modified.
      sem_wait( &annexOutputQueueSem ) ;
      annexOutputQueue.push( new clientCommand( theClient, globalSeq++,
						(*theCommand)[ i ] ) ) ;
      sem_post( &annexOutputQueueSem ) ;

    } // for()

}

/**
 * This method is responsible for constructing a properly formed
 * UFStrings object for writing back to the client.  This is called
 * after a command has been attempted to be executed, either failing
 * or succeeding.
 */
UFStrings* buildReply( clientCommand* theCommand, const string& reply )
{
  return new UFStrings( reply, &reply, 1 ) ;
}

#endif // __mt_device_echo_cc__

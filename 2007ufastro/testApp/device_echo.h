
#ifndef __device_echo_h__
#define __device_echo_h__ "$Name:  $ $Id: device_echo.h,v 0.0 2003/01/24 16:26:55 hon Developmental $"
#define __device_echo_H__(arg) const char arg##device_echo_h__rcsId[] = __device_echo_h__ ;

#include "iostream"
#include "string"

#include "UFStrings.h"
#include "UFSocket.h"

using std::string ;

class commInfo
{

public:

  typedef unsigned int optionType ;
  static const optionType OPT_BAD = 0x01 ;

  string		key ;
  string		format ;
  string		name ;
  int		numArgs ;
  unsigned int  waitTime ;
  optionType    options ;

  commInfo() : numArgs( 0 ), waitTime( 0 ), options( 0 )
    {}

  // Won't be subclasses, no need for virtual destructor.
  ~commInfo()
    {}

  bool getOption( const optionType& whichOpt ) const
  { return (options & whichOpt) ; }

  void parseOptions( const string& ) ;

  friend std::ostream& operator<<( std::ostream& out, const commInfo& rhs )
  {
    out << "Key: " << rhs.key << ", format: " << rhs.format
	<< ", description: " << rhs.name ;
    return out ;
  }

  friend std::ostream& operator<<( std::ostream& out, const commInfo* rhs )
  {
    return (out << *rhs) ;
  }

} ;

class namedPosition
{
 public:
  namedPosition( const int& _position,
		 const string& _name )
    : position( _position ),
    name( _name )
    {}

  // This will not be subclasses, no need to make destructor virtual.
  ~namedPosition()
    {}

  inline const int& getPosition() const
    { return position ; }
  inline const string& getName() const
    { return name ; }

 protected:
  int position ;
  string name ;

} ;

class device
{

 public:
  device( const int& _IS,
	  const int& _TS,
	  const int& _A,
	  const int& _D,
	  const int& _HC,
	  const int& _DC,
	  const char& _deviceID,
	  const string& _deviceName )
    : IS( _IS ),
    TS( _TS ),
    A( _A ),
    D( _D ),
    HC( _HC ),
    DC( _DC ),
    deviceID( _deviceID ),
    deviceName( _deviceName ),
    currentPosition( 0 )
    {}

  typedef vector< namedPosition* > namedPositionsType ;
  typedef namedPositionsType::const_iterator namedPositionsIterator ;

  // Won't be subclasses, no need for virtual destructor.
  ~device()
    {
      for( namedPositionsType::iterator ptr = namedPositions.begin() ;
	   ptr != namedPositions.end() ; ++ptr )
	{
	  delete *ptr ;
	}
      namedPositions.clear() ;
    }

  inline void addNamedPosition( namedPosition* newPosition )
    { namedPositions.push_back( newPosition ) ; }

  inline namedPositionsType::size_type namedPositions_size() const
    { return namedPositions.size() ; }
  inline namedPositionsIterator namedPositions_begin() const
    { return namedPositions.begin() ; }
  inline namedPositionsIterator namedPositions_end() const
    { return namedPositions.end() ; }

  // protected:
  int IS,
    TS,
    A,
    D,
    HC,
    DC ;
  char deviceID ;
  string deviceName ;
  int currentPosition ;
  namedPositionsType namedPositions ;

} ;

/* static const char* errors[] = */
/* { */
/*   "Success", */
/*   "Hazardous command", */
/*   "Invalid syntax", */
/*   "Unknown command", */
/*   "Server Command, that's fine by me" */
/* } ; */

enum errorNum
{
  ESUCCESS,
  EHAZARD,
  ESYNTAX,
  EUNKNOWN,
  ESERVERCOMM, // not actually an error
  ECOMMANDMIX, // can't mix server and motor commands together
  ENODEVICE
} ;

typedef unsigned int jobStatusType ;
static const jobStatusType JOB_PENDING = 0 ;
static const jobStatusType JOB_EXECUTING = 1 ;
static const jobStatusType JOB_COMPLETE = 2 ;
static const jobStatusType JOB_REJECT = 3 ;

const char* jobStatusStrings[] =
{
  "pending",
  "executing",
  "complete",
  "rejecte"
} ;

bool		readCommandFile( const string& ) ;
bool            readParamFile( const string& ) ;
bool            isServerCommand( const string& ) ;
bool            initDevices() ;
bool            getDeviceStates() ;

errorNum	verifyCommands( const UFStrings* ) ;
errorNum        verifyServerCommands( const UFStrings* ) ;

void		sigHandler( int ) ;
void		usage( const string& ) ;
void            handleServerCommand( UFSocket&, const UFStrings* ) ;
void            parseAnnexReply( const string& ) ;

string		formatCommand( const string& ) ;
string          formatHL( const string& ) ;
string          getJobId() ;

unsigned long   waitTime( const string& ) ;

std::ostream& operator<<( std::ostream& out, const UFStrings& strings )
{
  for( int i = 0 ; i < strings.elements() ; ++i )
    {
      out << strings[ i ] << endl ;
    }
  return out ;
}

std::ostream& operator<<( std::ostream& out, const namedPosition* np )
{
  out << np->getPosition() << ' ' << np->getName() ;
  return out ;
}

std::ostream& operator<<( std::ostream& out, const device* devicePtr )
{
out << devicePtr->IS << ' ' << devicePtr->TS << ' '
    << devicePtr->A << ' ' << devicePtr->D << ' '
    << devicePtr->HC << ' ' << devicePtr->DC << ' '
    << devicePtr->deviceID << ' ' << devicePtr->deviceName
    << endl ;

 for( device::namedPositionsIterator ptr = devicePtr->namedPositions_begin() ;
      ptr != devicePtr->namedPositions_end() ; ++ptr )
   {
     out << *ptr << std::endl ;
   }
 return out ;
}

#endif // __device_echo_h__

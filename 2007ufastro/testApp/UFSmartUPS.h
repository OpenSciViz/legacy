#include <iostream>
#include <string>
#include "UFClientSocket.h"

//using namespace std;

struct UPSParams {
  int inpVolt;
  double inpFreq;
  double battVolt;
  int battCap;
  int load;
};

class UFSmartUPS : UFClientSocket {
 public: 
  struct UPSParams params;

 private:
  unsigned int content_len; //length of http tranmission, minus length of http (NOT html) header
  string message; // actual data recieved from website (should be in html format)
  socketFd sock;

 public:
  UFSmartUPS();
  int getResponse() ;
  int requestPage(const char * pagename, const char * hostname);
  int postLogon(const char * username, const char * password, const char * hostname);
  int parseMessage() ;
  bool loginRequired();
  bool loginFailed();
  void reset();
  inline void printMessage() { cout << message << endl; }
  void printParams();
  static int main(int argc, char ** argv);
};

const char rcsId[] = "$Id: pinguf.cc 14 2008-06-11 01:49:45Z hon $";

#include "iostream"
#include "strstream"
#include "string"
#include "algorithm"

#include "unistd.h" // getopt()

#include "cerrno" // strerror()
#include "csignal" // signal()
#include "cstring" // strerror()
#include "cstdlib" // (s)rand()

#include "UFClientSocket.h"
#include "UFPosixRuntime.h"
#include "UFRuntime.h"
#include "UFProtocol.h"
#include "UFTimeStamp.h"
#include "UFStrings.h"
#include "UFBytes.h"
#include "UFShorts.h"
#include "UFInts.h"
#include "UFFloats.h"
#include "UFFrames.h"
#include "UFFrameConfig.h"
#include "UFObsConfig.h"

using namespace std ;

static const int maxVals = 2048*2048;
static UFClientSocket _client;
volatile bool keepRunning = true ;
volatile bool isConnected = false ;

// return diff == echo - ping
double timediff(string& echo, string& ping, int cnt)
{
  // the string time format should always be: "yyyy:ddd:hh:mm:ss.uuuuuu"
  char eval[] = "yyyydddhhmmss.uuuuuu";
  char pval[] = "yyyydddhhmmss.uuuuuu";
  size_t slen = strlen(eval);
  for( size_t n= 0, i= 0; i < slen; ++i, ++n )
    {
      if( echo[n] == ':' ) ++n;
      eval[i] = echo[n];
      pval[i] = ping[n];
    }
  double diff = atof(eval) - atof(pval);
  clog << "timediff> cnt: " << cnt << ", diff= "
       << diff << " --------------------------------" << endl;
  return diff;
}

// test UFTimeStamp 
void timestamp(UFClientSocket& _client, int cnt)
{
  strstream s;
  s << "timestamp> cnt: " << cnt << " " << ends;
  string name = s.str();
  delete[] s.str();
  UFTimeStamp ping(name, UFProtocol::_MinLength_);
  clog << "timestamp> send name= " << ping.cname()
       << ", time= " << ping.timestamp() << endl;

  if( _client.send(ping) <= 0 )
    {
      clog << "timestamp> Write error: "
           << strerror( errno ) << endl ;
      isConnected = false ;
      return ;
    }

  clog << "timestamp> get echo..." << endl;
  UFProtocol *ufp = UFProtocol::createFrom(_client);
  if( 0 == ufp )
    {
      clog << "timestamp> Read error: "
           << strerror( errno ) << endl ;
      isConnected = false ;
      return;
    }

  string echo_t = ufp->timestamp(); string t0 = ping.timestamp();
  //timediff(echo_t, t0, cnt);
  UFTimeStamp *uft = dynamic_cast<UFTimeStamp*>(ufp);
  if( uft )
    {
      clog << "timestamp> received reply:\n name= "
           << uft->cname() << ", time= " << uft->timestamp() << endl;
      //clog<<"timestamp> hit any key to continue..."<<endl;
      //getchar();  //so we can see data on screen
    }
  else
    {
      clog << "timestamp> server did not return UFTimeStamp?, typ= "
           << ufp->typeId() << endl;
    }
  delete uft;
  return;
}

// test UFStrings 
void strings(UFClientSocket& _client, int cnt)
{
  strstream sn;
  sn << "strings> cnt: " << cnt << " " << ends;
  string name = sn.str();
  delete[] sn.str();
  if ( cnt > maxVals ) cnt = maxVals;

  string *vals = new (nothrow) string[cnt];
  if( NULL == vals )
    {
      clog << "strings> Memory allocation failure\n" ;
      keepRunning = false ;
      return ;
    }

  for( int i = 0; i < cnt; ++i )
    {
      strstream s;
      s << i << " -- " << name << ends;
      vals[i] = s.str();
      delete[] s.str();
    }

  UFStrings ping(name, vals, cnt);
  clog << "strings> send name= " << ping.cname() << ", time= "
       << ping.timestamp() << endl;
  clog << "strings> send val[0]= " << *(ping.stringAt(0))
       << ", val[elem-1]= " << *(ping.stringAt(ping.elements()-1))
       << endl;

  if( _client.send(ping) <= 0 )
    {
      clog << "strings> Write error: "
           << strerror( errno ) << endl ;
      isConnected = false ;
      delete[] vals ;
      return ;
    }

  delete[] vals;

  UFProtocol *ufp = UFProtocol::createFrom(_client);
  if( 0 == ufp )
    {
      clog << "strings> Read error: "
           << strerror( errno ) << endl ;
      isConnected = false ;
      return;
    }

  string echo_t = ufp->timestamp(); string t0 = ping.timestamp();
  //timediff(echo_t, t0, cnt);
  UFStrings *ufs = dynamic_cast<UFStrings*>(ufp);
  if( ufs )
    {
      clog << "strings> received reply:\n name= "
           << ufs->cname() << ", time= " << ufs->timestamp()
           << ", elems= " << ufs->elements() << ", nvals= "
           << ufs->numVals() << endl;
      for( int i = 0; i < ufs->elements(); i += 100 )
        {
          clog << "strings> " << ufs->valData(i) << endl;
          //clog<<"strings> hit any key to continue..."<<endl;
          //getchar();  //so we can see data on screen
        }
    }
  else
    {
      clog << "strings> server did not return UFStrings?, typ= "
           << ufp->typeId() << endl;
    }

  delete ufs;
  return;
}

// test UFBytes 
void bytes(UFClientSocket& _client, int cnt)
{
  strstream sn;
  sn << "bytes> cnt: " << cnt << " " << ends;
  string name = sn.str();
  delete[] sn.str();
  if ( cnt > maxVals ) cnt = maxVals;

  unsigned char *vals = new (nothrow) unsigned char[cnt];
  if( NULL == vals )
    {
      clog << "bytes> Memory allocation failure\n" ;
      keepRunning = false ;
      return ;
    }

  for( int i = 0; i < cnt; ++i )
    {
      vals[i] = i % 256;
    }

  bool notshared = false;
  // this is the shallow copy ctor
  UFBytes ping(name, (const char*) vals, cnt, notshared);
  clog << "bytes> _shared = " << ping._shared << " _shallow = "
       << ping._shallow << endl;
  clog << "bytes> send name= " << ping.cname() << ", time= "
       << ping.timestamp() << endl;
  clog << "bytes> length()= " << ping.length() << endl;

  if( _client.send(ping) <= 0 )
    {
      clog << "bytes> Write error: " << strerror( errno ) << endl ;
      isConnected = false ;
      delete[] vals ;
      return ;
    }

  delete [] vals;

  UFProtocol *ufp = UFProtocol::createFrom(_client);
  if( NULL == ufp )
    {
      clog << "bytes> Read error: " << strerror( errno ) << endl ;
      isConnected = false ;
      return ;
    }

  ufp->_shallow = false; // make sure _values is deleted!
  clog << "bytes> after createFrom: _shared = " << ufp->_shared
       << " _shallow = " << ufp->_shallow << endl;

  string echo_t = ufp->timestamp(); string t0 = ping.timestamp();
  //timediff(echo_t, t0, cnt);
  UFBytes *ufb = dynamic_cast<UFBytes*>(ufp);
  if( ufb )
    {
      clog << "bytes> received reply:\n name= "
           << ufb->cname() << ", time= " << ufb->timestamp()
           << ", elems= " << ufb->elements() << ", nvals= "
           << ufb->numVals() << endl;
      clog << "bytes> " << (int)*((char*)ufb->valData(0)) << endl;
      clog << "bytes> " << (int)*((char*)ufb->valData(ufb->elements()/2))
           << endl;
      clog << "bytes> " << (int)*((char*)ufb->valData(ufb->elements()-1))
           << endl;
      //clog<<"bytes> hit any key to continue..."<<endl;
      //getchar();  //so we can see data on screen
    }
  else
    {
      clog << "bytes> server did not return UFBytes?, typ= "
           << ufp->typeId() << endl;
    }

  delete ufb;
  return;
}

// test UFShorts
void shorts(UFClientSocket& _client, int cnt)
{
  strstream sn;
  sn << "shorts> cnt: " << cnt << " " << ends;
  string name = sn.str();
  delete[] sn.str();

  if ( cnt > maxVals ) cnt = maxVals;
  unsigned short *vals = new (nothrow) unsigned short[cnt];
  if( NULL == vals )
    {
      clog << "shorts> Memory allocation failure\n" ;
      keepRunning = false ;
      return ;
    }

  for( int i = 0; i < cnt; ++i )
    {
      vals[i] = i % (USHRT_MAX+1);
    }

  // shallow ctor
  UFShorts ping(name, (const short*) vals, cnt, false);
  clog << "shorts> send name= " << ping.cname() << ", time= "
       << ping.timestamp() << endl;

  if( _client.send(ping) <= 0 )
    {
      clog << "shorts> Write error: " << strerror( errno ) << endl ;
      isConnected = false ;
      delete[] vals ;
      return ;
    }

  delete [] vals;

  UFProtocol *ufp = UFProtocol::createFrom(_client);
  if( NULL == ufp )
    {
      clog << "shorts> Read error: " << strerror( errno ) << endl ;
      isConnected = false ;
      return ;
    }

  string echo_t = ufp->timestamp(); string t0 = ping.timestamp();
  //timediff(echo_t, t0, cnt);
  UFShorts *ufs = dynamic_cast<UFShorts*>(ufp);
  if( ufs )
    {
      clog << "shorts> received reply:\n name= "
           << ufs->cname() << ", time= " << ufs->timestamp()
           << ", elems= " << ufs->elements() << ", nvals= "
           << ufs->numVals() << endl;
      clog << "shorts> " << (int)*((short*)ufs->valData(0)) << endl;
      clog << "shorts> " << (int)*((short*)ufs->valData(ufs->elements()/2))
           << endl;
      clog << "shorts> " << (int)*((short*)ufs->valData(ufs->elements()-1))
           << endl;
      //clog<<"ints> hit any key to continue..."<<endl;
      //getchar();  //so we can see data on screen
    }
  else
    {
      clog << "shorts> server did not return UFShorts?, typ= "
           << ufp->typeId() << endl;
    }

  delete ufs;
  return;
}

// test UFInts
void ints(UFClientSocket& _client, int cnt)
{
  strstream sn;
  sn << "ints> cnt: " << cnt << " " << ends;
  string name = sn.str();
  delete[] sn.str();

  if ( cnt >= maxVals ) cnt = maxVals;

  static int vals[maxVals] = { 0 } ;
  int r = rand();
  for( int i = 0; i < cnt; ++i )
    {
      vals[i] = r + i;
    }

  UFProtocol *ufp= 0;
  UFInts *ufi= 0;
  UFInts ping(name, (const int*) vals, cnt, false);

  clog << "ints> send name= " << ping.cname() << ", time= "
       << ping.timestamp() << endl;

  // send it off:
  if( _client.send(ping) <= 0 )
    {
      clog << "ints> Write error: " << strerror( errno ) << endl ;
      isConnected = false ;
      return ;
    }

  // get it back:
  if( _client.recv(ping) <= 0 )
    {
      clog << "ints> Read error: " << strerror( errno ) << endl ;
      isConnected = false ;
      return ;
    }

  // ufp  = UFProtocol::createFrom(_client);
  //if( ufp == 0 )
  //  return;
  //else
  //  ufi = dynamic_cast<UFInts*>(ufp);

  ufp = ufi = &ping;
  string echo_t = ufp->timestamp(); string t0 = ping.timestamp();
  //timediff(echo_t, t0, cnt);
  if( ufi )
    {
      clog << "ints> received reply:\n name= "
           << ufi->cname() << ", time= " << ufi->timestamp()
           << ", elems= " << ufi->elements() << ", nvals= "
           << ufi->numVals() << endl;
      clog << "ints> " << *((int*)ufi->valData(0)) << endl;
      clog << "ints> " << *((int*)ufi->valData(ufi->elements()/2))
           << endl;
      clog << "ints> " << *((int*)ufi->valData(ufi->elements()-1)) << endl;
      //clog<<"ints> hit any key to continue..."<<endl;
      //getchar();  //so we can see data on screen
    }
  else
    {
      clog << "ints> server did not return UFInts?, typ= "
           << ufp->typeId() << endl;
    }
  //delete ufi;
  return;
}

void floats(UFClientSocket& _client, int cnt)
{
  strstream sn;
  sn << "floats> cnt: " << cnt << " " << ends;
  string name = sn.str();
  delete[] sn.str();
  if ( cnt > maxVals ) cnt = maxVals;

  float *vals = new (nothrow) float[cnt];
  if( NULL == vals )
    {
      clog << "floats> Memory allocation failure\n" ;
      keepRunning = false ;
      return ;
    }

  for( int i = 0; i < cnt; ++i )
    {
      vals[i] = i*1.25;
    }

  UFFloats ping(name, vals, cnt, false);
  clog << "floats> send name= " << ping.cname() << ", time= "
       << ping.timestamp() << endl;
  if( _client.send(ping) <= 0 )
    {
      clog << "floats> Write error: " << strerror( errno ) << endl ;
      isConnected = false ;
      delete[] vals ;
      return ;
    }

  delete [] vals;

  UFProtocol *ufp = UFProtocol::createFrom(_client);
  if( NULL == ufp )
    {
      clog << "floats> Read error: " << strerror( errno ) << endl ;
      isConnected = false ;
      return ;
    }

  string echo_t = ufp->timestamp(); string t0 = ping.timestamp();
  //timediff(echo_t, t0, cnt);
  UFFloats *uff = dynamic_cast<UFFloats*>(ufp);
  if( uff )
    {
      clog << "floats> received reply:\n name= "
           << uff->cname() << ", time= " << uff->timestamp()
           << ", elems= " << uff->elements() << ", nvals= "
           << uff->numVals() << endl;
      clog << "floats> " << *((float*)uff->valData(0)) << endl;
      clog << "floats> " << *((float*)uff->valData(uff->elements()/2))
           << endl;
      clog << "floats> " << *((float*)uff->valData(uff->elements()-1))
           << endl;
      //clog<<"floats> hit any key to continue..."<<endl;
      //getchar();  //so we can see data on screen
    }
  else
    {
      clog << "floats> server did not return UFFloats?, typ= "
           << ufp->typeId() << endl;
    }

  delete uff;
  return;
}

// test UFFrames
void frames(UFClientSocket& _client, int cnt, int type = UFProtocol::_ByteFrames_)
{
  UFFrames* ping = 0;
  strstream sn;
  sn << "frames> cnt: " << cnt << " " << ends;
  string name = sn.str();
  delete[] sn.str();

  cnt = std::min( cnt, 2048 ) ;
  int nvals = cnt*cnt*4 ; // at most 2048x2048x32

  if( type == UFProtocol::_ByteFrames_ )
    {
      unsigned char *vals = new (nothrow) unsigned char[nvals];
      if( NULL == vals )
        {
          clog << "frames> Memory allocation failure\n" ;
          keepRunning = false ;
          return ;
        }
      for( int i = 0; i < nvals; ++i )
        {
          vals[i] = (unsigned char)(i % 256);
        }

      UFBytes tmp(name, (const char*) vals, nvals, false); // shallow ctor
      ping = new (nothrow) UFFrames(&tmp, cnt, cnt);
      if( NULL == ping )
        {
          clog << "frames> Memory allocation failure\n" ;
          keepRunning = false ;
          return ;
        }

      clog << "frames> send name= " << ping->cname() << ", time= "
           << ping->timestamp() << endl;
      if( _client.send(ping) <= 0 )
        {
          clog << "frames> Write error: " << strerror( errno ) << endl ;
          isConnected = false ;
          delete ping ;
          delete[] vals ;
          return ;
        }
      delete [] vals;
    }
    
  if( type == UFProtocol::_ShortFrames_ )
    {
      unsigned short *vals = new (nothrow) unsigned short[nvals];
      if( NULL == vals )
        {
          clog << "frames> Memory allocation failure\n" ;
          keepRunning = false ;
          return ;
        }
      for( int i = 0; i < nvals; ++i )
        {
          vals[i] = (unsigned short)(i % (USHRT_MAX+1));
        }

      UFShorts tmp(name, (const short*) vals, nvals, false); // shallow ctor
      ping = new (nothrow) UFFrames(&tmp, cnt, cnt);
      if( NULL == ping )
        {
          clog << "frames> Memory allocation failure\n" ;
          keepRunning = false ;
          return ;
        }

      clog << "frames> send name= " << ping->cname() << ", time= "
           << ping->timestamp() << endl;
      if( _client.send(ping) <= 0 )
        {
          clog << "frames> Write error: " << strerror( errno ) << endl ;
          isConnected = false ;
          delete ping ;
          delete[] vals ;
          return ;
        }
      delete [] vals;
    }

  if( type == UFProtocol::_IntFrames_ )
    {
      unsigned int *vals = new (nothrow) unsigned int[nvals];
      if( NULL == vals )
        {
          clog << "frames> Memory allocation failure\n" ;
          keepRunning = false ;
          return ;
        }
      for( int i = 0; i < nvals; ++i )
        {
          vals[i] = i;
        }

      UFInts tmp(name, (const int*) vals, nvals, false); // shallow ctor
      ping = new (nothrow) UFFrames(&tmp, cnt, cnt);
      if( NULL == ping )
        {
          clog << "frames> Memory allocation failure\n" ;
          keepRunning = false ;
          return ;
        }

      clog << "frames> send name= " << ping->cname() << ", time= "
           << ping->timestamp() << endl;
      if( _client.send(ping) <= 0 )
        {
          clog << "frames> Write error: " << strerror( errno ) << endl ;
          isConnected = false ;
          delete ping ;
          delete[] vals ;
          return ;
        }
      delete [] vals;
    }

  if( type == UFProtocol::_FloatFrames_ )
    {
      float *vals = new (nothrow) float[nvals];
      if( NULL == vals )
        {
          clog << "frames> Memory allocation failure\n" ;
          keepRunning = false ;
          return ;
        }
      for( int i = 0; i < nvals; ++i )
        {
          vals[i] = i;
        }

      UFFloats tmp(name, (const float*) vals, nvals, false); // shallow ctor
      ping = new (nothrow) UFFrames(&tmp, cnt, cnt);
      if( NULL == ping )
        {
          clog << "frames> Memory allocation failure\n" ;
          keepRunning = false ;
          return ;
        }

      clog << "frames> send name= " << ping->cname() << ", time= "
           << ping->timestamp() << endl;
      if( _client.send(ping) <= 0 )
        {
          clog << "frames> Write error: " << strerror( errno ) << endl ;
          isConnected = false ;
          delete ping ;
          delete[] vals ;
          return ;
        }
      delete [] vals;
    }
    
  clog << "frames> getting echo / response..." << endl;
  UFProtocol *ufp = UFProtocol::createFrom(_client);
  if( NULL == ufp )
    {
      clog << "frames> Read error: " << strerror( errno ) << endl ;
      isConnected = false ;
      return ;
    }

  string echo_t = ufp->timestamp(); string t0 = ping->timestamp();
  //timediff(echo_t, t0, cnt);
  UFFrames *uff = dynamic_cast<UFFrames*>(ufp);
  if( uff )
    {
      clog << "frames> received reply:\n name= "
           << uff->cname() << ", time= " << uff->timestamp()
           << ", elems= " << uff->elements() << ", nvals= "
           << uff->numVals() << endl;
      for( int i = 0; i < uff->elements(); ++i )
        {
          clog << "frames> " << (unsigned int)*((char*)uff->valData(i));
          clog << "frames> address =  " << (unsigned int)uff->valData(i)
               << endl;
        }
      //clog<<"byteframes> hit any key to continue..."<<endl;
      //getchar();  //so we can see data on screen
    }
  else
    {
      clog << "frames> server did not return UFByteFrames?, typ= "
           << ufp->typeId() << endl;
    }

  delete uff;
  return;
}

// test UFObsConfig
void obsconfig(UFClientSocket& _client, int cnt)
{
  strstream s;
  s << "obsconfig> cnt: " << cnt << " " << ends;
  const string name = s.str();
  delete[] s.str();
  unsigned short vals[2*2*3*2];
  unsigned short i= 0;
  while( i < 2*2*3*2 ) vals[i] = i++;
  UFObsConfig ping(name,2,2,3,2,vals);
  UFObsConfig::Config c = ping.config();
  clog << "obsconfig> config= " << c.NodBeams << ", " << c.ChopBeams
       << ", " << c.SaveSets << ", " << c.NodSets << endl;
  clog << "obsconfig> send name= " << ping.cname() << ", time= "
       << ping.timestamp() << endl;
  if( _client.send(ping) <= 0 )
    {
      clog << "obsconfig> Write error: " << strerror( errno ) << endl ;
      isConnected = false ; 
      return ;
    }

  clog << "obsconfig> get echo..." << endl;
  UFProtocol *ufp = UFProtocol::createFrom(_client);
  if( NULL == ufp )
    {
      clog << "obsocnfig> Read error: " << strerror( errno ) << endl ;
      isConnected = false ;
      return ;
    }

  string echo_t = ufp->timestamp(); string t0 = ping.timestamp();
  //timediff(echo_t, t0, cnt);
  UFObsConfig *ufo = dynamic_cast<UFObsConfig*>(ufp);
  if( ufo )
    {
      clog << "obsconfig> received reply:\n name= "
           << ufo->cname() << ", time= " << ufo->timestamp() << endl;
      clog << "obsconfig> config= " << c.NodBeams << ", " << c.ChopBeams
           << ", " << c.SaveSets << ", " << c.NodSets << endl;
      //clog<<"timestamp> hit any key to continue..."<<endl;
      //getchar();  //so we can see data on screen
    }
  else
    {
      clog <<"obsconfig> server did not return  UFObsConfig?, typ= "
           << ufp->typeId() << endl;
    }

  delete ufo;
  return;
}

// test UFFrameConfig
void frameconfig(UFClientSocket& _client, int cnt)
{
  strstream s;
  s << "frameconfig> cnt: " << cnt << " " << ends;
  string name = s.str();
  delete[] s.str();
  UFFrameConfig ping(256,256);
  clog << "frameconfig> send name= " << ping.cname() << ", time= "
       << ping.timestamp() << endl;

  if( _client.send(ping) <= 0 )
    {
      clog << "frameconfig> Write error: " << strerror( errno ) << endl ;
      isConnected = false ;
      return ;
    }

  clog << "frameconfig> get echo..." << endl;

  UFProtocol *ufp = UFProtocol::createFrom(_client);
  if( NULL == ufp )
    {
      clog << "frameconfig> Read error: " << strerror( errno ) << endl ;
      isConnected = false ;
      return ;
    }

  string echo_t = ufp->timestamp(); string t0 = ping.timestamp();
  //timediff(echo_t, t0, cnt);
  UFFrameConfig *ufc = dynamic_cast<UFFrameConfig*>(ufp);
  if( ufc )
    {
      clog << "frameconfig> received reply:\n name= "
           << ufc->cname() << ", time= " << ufc->timestamp() << endl;
      //clog<<"timestamp> hit any key to continue..."<<endl;
      //getchar();  //so we can see data on screen
    }
  else
    {
      clog << "frameconfig> server did not return UFFrameconfig?, typ= "
           << ufp->typeId() << endl;
    }

  delete ufc;
  return;
}

string host = UFRuntime::hostname() ;
unsigned short int port = 55555 ;

bool runAllTests = true ;
bool runTimeStamp = false ;
bool runBytes = false ;
bool runStrings = false ;
bool runInts = false ;
bool runFloats = false ;

void sigHandler( int whichSig )
{
  switch( whichSig )
    {
    case SIGPIPE:
      isConnected = false ;
      break ;
    case SIGINT:
    case SIGTERM:
    case SIGALRM:
      keepRunning = false ;
      clog << "*** Caught signal, shutting down...\n" ;
    }
}

void usage( const string& progName )
{
  clog << endl ;
  clog << "Usage: " << progName << " <options>\n" ;
  clog << "-a: Run all tests (" << runAllTests << ")\n" ;
  clog << "-b: Run UFBytes tests (" << runBytes << ")\n" ;
  clog << "-f: Run UFFloats tests (" << runFloats << ")\n" ;
  clog << "-h: Print this help menu\n" ;
  clog << "-i: Run UFInts tests (" << runInts << ")\n" ;
  clog << "-p <port>: Specify uplink port number ("
       << port << ")\n" ;
  clog << "-s: Run UFStrings tests (" << runStrings << ")\n" ;
  clog << "-t: Run UFTimeStamp tests (" << runTimeStamp << ")\n" ;
  clog << "-u <uplink>: Specify uplink server ("
       << host << ")\n" ;
  clog << endl ;
}

int main(int argc, char** argv)
{

  int c = EOF ;
  while( (c = ::getopt( argc, argv, "hp:u:" )) != EOF )
    {
      switch( c )
        {
        case 'a':
          runAllTests = true ;
          break ;
        case 'b':
          runBytes = true ;
          break ;
        case 'f':
          runFloats = true ;
          break ;
        case 'h':
          usage( argv[ 0 ] ) ;
          return 0 ;
        case 'i':
          runInts = true ;
          break ;
        case 'p':
          port = static_cast< unsigned short int >( ::atoi( optarg ) ) ;
          break ;
        case 's':
          runStrings = true ;
          break ;
        case 't':
          runTimeStamp = true ;
          break ;
        case 'u':
          host = optarg ;
          break ;
        }
    }

  srand( time( 0 ) ) ;

  if( SIG_ERR == ::signal( SIGINT, sigHandler ) )
    {
      clog << "main> Unable to establish signal handler for SIGINT: "
           << strerror( errno ) << endl ;
      return 0 ;
    }
  if( SIG_ERR == ::signal( SIGALRM, sigHandler ) )
    {
      clog << "main> Unable to establish signal handler for SIGALRM: "
           << strerror( errno ) << endl ;
      return 0 ;
    }
  if( SIG_ERR == ::signal( SIGTERM, sigHandler ) )
    {
      clog << "main> Unable to establish signal handler for SIGTERM: "
           << strerror( errno ) << endl ;
      return 0 ;
    }
  if( SIG_ERR == ::signal( SIGPIPE, sigHandler ) )
    {
      clog << "main> Unable to establish signal handler for SIGPIPE: "
           << strerror( errno ) << endl ;
      return 0 ;
    }

  int cnt=0;
  while( keepRunning )
    { 
      if( !isConnected )
        {
          clog << "main> connecting to host: " << host <<
            "on port # " << port << endl;
          int socFd = -1;
          while( (socFd = _client.connect(host.c_str(), port)) <= 0 )
            {
              clog << "main> Unable to connect, sleeping...\n" ;
            sleep(10);
            }
          clog << "main> connected to port # " << port << ", socFd= "
               << socFd << endl;
        }
      cnt += 1000;

      //if( cnt > 100 ) cnt = 1;
      if( runAllTests )
        { // do all tests
          timestamp(_client, cnt);
          strings(_client, cnt);
          bytes(_client, cnt);
          shorts(_client, cnt);
          ints(_client, cnt);
          floats(_client, cnt);
          obsconfig(_client, cnt);
          frameconfig(_client, cnt);
          frames(_client, cnt);
          continue ;
        }

      if( runTimeStamp ) 
        timestamp(_client, cnt);
      if( runStrings ) 
        strings(_client, cnt);
      if( runBytes ) 
        bytes(_client, cnt);
      //    else if( opt == "-bf" ) 
      //      frames(_client, cnt, UFProtocol::_ByteFrames_);
      //    else if( opt == "-sh" ) 
      //      shorts(_client, cnt);
      //    else if( opt == "-shf" ) 
      //      frames(_client, cnt, UFProtocol::_ShortFrames_);
      if( runInts ) 
        ints(_client, cnt);
      //    else if( opt == "-if" ) 
      //      frames(_client, cnt, UFProtocol::_IntFrames_);
      if( runFloats ) 
        floats(_client, cnt);
      //    else if( opt == "-ff" ) 
      //      frames(_client, cnt, UFProtocol::_FloatFrames_);
      //    else if( opt == "-fc" ) 
      //      frameconfig(_client, cnt);
      //    else if( opt == "-oc" ) 
      //      obsconfig(_client, cnt);
    }
  return 0 ;
} // main()


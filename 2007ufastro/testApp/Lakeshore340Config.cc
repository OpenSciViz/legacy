

//#include "UFRuntime.h"
//__UFRuntime_H__(__testStrings_cc__);

//#include "UFStrings.h"

#include "string"

#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

extern int errno;

static char* configFile = "TC340.commands";

int main(int, char**) {

  int test = 5;
  cout << "Print test: " << test << endl;

//   UFStrings ufsFromFile("UFStrings from file",20);

  struct stat myStat;
  if (stat(configFile, &myStat) ==  -1) {
    cout << "Can't stat config file: " << configFile << "." << endl;
    exit(0);
  }

  int filelength = myStat.st_size;
  char* buf = new char[filelength];
  int fdin = 0;
  int numRead = 0;
  if ((fdin = open(configFile, O_RDONLY )) == -1) {
    cout << "Sorry, can't open file " << configFile << ". errno =  " << errno << endl;
  }
  else {
    numRead = read(fdin,buf,filelength);
    cout << "numRead = " << numRead << endl;
    cout << "array of characters read from file: " << buf << endl;
//       numRead = ufsFromFile.readFrom(fdin);
//       cout << "readFrom read " << numRead << " bytes" << endl;
//       close(fdin);
  }

  delete[] buf;

//   numelems = ufsFromFile.elements();
//   for (int i = 0; i < numelems; i++) {
//       cout << "ufsFromFile.stringAt(" << i << ") = " << (ufsFromFile.stringAt(i))->c_str() << endl;
//   }
}


#if !defined(__UFServices_cc__)
#define __UFServices_cc__ "$Name:  $ $Id: UFServices.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFServices_cc__;

#include "UFServices.h"
#include "UFTimeStamp.h"
#include "UFStrings.h"
#include "UFBytes.h"
#include "UFShorts.h"
#include "UFInts.h"
#include "UFFloats.h"
#include "UFObsConfig.h"
#include "UFFrameConfig.h"
#include "UFFrames.h"

// static funcs:
int UFServices::main(int argc, char** argv) {
  UFServices ufs("UFServices", argc, argv); // ctor binds to listen socket
  ufs.exec((void*)&ufs);
  return argc;
}

// non-static stuff:
UFServices::UFServices(int argc, char** argv) : UFRndRobinServ(argc, argv) {}

UFServices::UFServices(const string& name, int argc, char** argv) : UFRndRobinServ(name, argc, argv) {}

int UFServices::servReqs(UFRndRobinServ::MsgTable& clients) {
  // for each client that has an un-read request:
  // recv the req. (UFProtocol) and check its name to decide what to do next.
  // segregate client types into the standard UF architecture:
  UFServices::MsgTable anon;
  UFServices::MsgTable frame;
  UFServices::MsgTable stat;
  UFServices::MsgTable cmd;
  UFServices::MsgTable tscope;
  UFServices::MsgTable mce4;
  UFServices::MsgTable pmotor;
  UFServices::MsgTable vacm;
  UFServices::MsgTable bar;
  int cnt= 0;
  UFRndRobinServ::MsgTable::iterator i = clients.begin();
  for( ; i != clients.end(); ++i, ++cnt ) {
    UFSocket* soc = i->first;
    UFProtocol* req = i->second;
    if( req == 0 ) // skip this client?
      continue;

    string reqstr = req->name();

    if( reqstr.find("anon") != string::npos || reqstr.find("anonymous") != string::npos )
      anon.insert(MsgTable::value_type(soc, req));
    else if( reqstr.find("frm") != string::npos || reqstr.find("fram") != string::npos )
      frame.insert(MsgTable::value_type(soc, req));
    else if( reqstr.find("stat") != string::npos )
      stat.insert(MsgTable::value_type(soc, req));
    else if( reqstr.find("cmd") != string::npos || reqstr.find("com") != string::npos)
      cmd.insert(MsgTable::value_type(soc, req));
    else if( reqstr.find("tele") != string::npos || reqstr.find("tscop") != string::npos)
      tscope.insert(MsgTable::value_type(soc, req));
    else if( reqstr.find("mce") != string::npos )
      mce4.insert(MsgTable::value_type(soc, req));
    else if( reqstr.find("mot") != string::npos || reqstr.find("pmot") != string::npos)
      pmotor.insert(MsgTable::value_type(soc, req));
    else if( reqstr.find("vac") != string::npos || reqstr.find("pres") != string::npos)
      vacm.insert(MsgTable::value_type(soc, req));
    else if( reqstr.find("bar") != string::npos )
      bar.insert(MsgTable::value_type(soc, req));
    else
      clog<<"UFServices::servReqs> unknow request: "<<reqstr<<endl;
  }
  
  // once all current requests have been read off each client soc,
  // process the requests:
  if( frame.size() > 0 )
    frmClients(frame);
  if( stat.size() > 0 )
    statClients(stat);
  if( cmd.size() > 0 )
    cmdClients(cmd);
  if( tscope.size() > 0 )
    tscopeClients(tscope);
  if( mce4.size() > 0 )
    mce4Clients(mce4);
  if( pmotor.size() > 0 )
    pmotorClients(stat);
  if( vacm.size() > 0 )
    vacuumClients(vacm);
  if( bar.size() > 0 )
    barClients(bar);

  // this should clear the req. msg. table:
  clearAllMsgs(clients);
  return cnt;
}

int UFServices::anonClients(UFRndRobinServ::MsgTable& anons) {
  // should delete each protocol object (request) after it is handled, then clear the table
  UFRndRobinServ::MsgTable::iterator i = anons.begin();
  string newnam;
  int cnt= 0;
  while( i != anons.end() ) {
    UFSocket* soc = i->first; 
    UFProtocol* ufp = i->second;
    if( ufp == 0 ) {
      clog<<"UFServices::frmClients> bad UFProtocol pointer?..."<<endl;
      ufp = new UFStrings("Error in ufpingserv> UFProtocol::createFrom failed?...");
    }
    clog<<"UFServices::frmClient> name= "<<ufp->name()<<", time= "<<ufp->timestamp()<<"\n"
        <<", typ= "<<ufp->typeId()<<", desc.: "<<ufp->description()
        <<" length = "<<ufp->length()<<endl;
    newnam = name() + " -- Alive! -- " + ufp->name();
    ufp->rename(newnam);
    ufp->stampTime(UFRuntime::currentTime());
    clog<<"UFServices::frmClient> respond, name= "<<ufp->name()<<", time= "<<ufp->timestamp()<<endl;
    if( soc->writable() > 0 ) {
      soc->send(*ufp);
    }
    else {
      clog<<"UFServices::frmClients> ? socket not writable"<<endl;
      soc->close();
    }
    delete ufp;
    i++;
    cnt++;
  } // while

  if( cnt != (int)anons.size() ) 
    clog<<"UFServices::frmClients> iteration incomplete? cnt= "<<cnt<<", table size= "<<anons.size()<<endl;

  anons.clear();
  return cnt;
} // anonClients

int UFServices::imgClients(UFRndRobinServ::MsgTable& imgs) {
  // image are pixel sorted in some way...
  // should delete each protocol object (request) after it is handled, then clear the table
  UFRndRobinServ::MsgTable::iterator i = imgs.begin();
  string newnam;
  int cnt= 0;
  while( i != imgs.end() ) {
    UFSocket* soc = i->first; 
    UFProtocol* ufp = i->second;
    if( ufp == 0 ) {
      clog<<"UFServices::frmClients> bad UFProtocol pointer?..."<<endl;
      ufp = new UFStrings("Error in ufpingserv> UFProtocol::createFrom failed?...");
    }
    clog<<"UFServices::frmClient> name= "<<ufp->name()<<", time= "<<ufp->timestamp()<<"\n"
        <<", typ= "<<ufp->typeId()<<", desc.: "<<ufp->description()
        <<" length = "<<ufp->length()<<endl;
    newnam = name() + " -- Alive! -- " + ufp->name();
    ufp->rename(newnam);
    ufp->stampTime(UFRuntime::currentTime());
    clog<<"UFServices::frmClient> respond, name= "<<ufp->name()<<", time= "<<ufp->timestamp()<<endl;
    if( soc->writable() > 0 ) {
      soc->send(*ufp);
    }
    else {
      clog<<"UFServices::frmClients> ? socket not writable"<<endl;
      soc->close();
    }
    delete ufp;
    i++;
    cnt++;
  } // while

  if( cnt != (int)imgs.size() ) 
    clog<<"UFServices::frmClients> iteration incomplete? cnt= "<<cnt<<", table size= "<<imgs.size()<<endl;

  imgs.clear();
  return cnt;
} // imgClients

int UFServices::frmClients(UFRndRobinServ::MsgTable& frames) {
  // raw (unsorted?) frames
  // should delete each protocol object (request) after it is handled, then clear the table
  UFRndRobinServ::MsgTable::iterator i = frames.begin();
  string newnam;
  int cnt= 0;
  while( i != frames.end() ) {
    UFSocket* soc = i->first; 
    UFProtocol* ufp = i->second;
    if( ufp == 0 ) {
      clog<<"UFServices::frmClients> bad UFProtocol pointer?..."<<endl;
      ufp = new UFStrings("Error in ufpingserv> UFProtocol::createFrom failed?...");
    }
    clog<<"UFServices::frmClient> name= "<<ufp->name()<<", time= "<<ufp->timestamp()<<"\n"
        <<", typ= "<<ufp->typeId()<<", desc.: "<<ufp->description()
        <<" length = "<<ufp->length()<<endl;
    newnam = name() + " -- Alive! -- " + ufp->name();
    ufp->rename(newnam);
    ufp->stampTime(UFRuntime::currentTime());
    clog<<"UFServices::frmClient> respond, name= "<<ufp->name()<<", time= "<<ufp->timestamp()<<endl;
    if( soc->writable() > 0 ) {
      soc->send(*ufp);
    }
    else {
      clog<<"UFServices::frmClients> ? socket not writable"<<endl;
      soc->close();
    }
    delete ufp;
    i++;
    cnt++;
  } // while

  if( cnt != (int)frames.size() ) 
    clog<<"UFServices::frmClients> iteration incomplete? cnt= "<<cnt<<", table size= "<<frames.size()<<endl;

  frames.clear();
  return cnt;
} // frmClients

int UFServices::statClients(UFRndRobinServ::MsgTable& stats) {
  // should delete each protocol object (request) after it is handled, then clear the table
  UFRndRobinServ::MsgTable::iterator i = stats.begin();
  string newnam;
  int cnt= 0;
  while( i != stats.end() ) {
    UFSocket* soc = i->first; 
    UFProtocol* ufp = i->second;
    if( ufp == 0 ) {
      clog<<"UFServices::frmClients> bad UFProtocol pointer?..."<<endl;
      ufp = new UFStrings("Error in ufpingserv> UFProtocol::createFrom failed?...");
    }
    clog<<"UFServices::frmClient> name= "<<ufp->name()<<", time= "<<ufp->timestamp()<<"\n"
        <<", typ= "<<ufp->typeId()<<", desc.: "<<ufp->description()
        <<" length = "<<ufp->length()<<endl;
    newnam = name() + " -- Alive! -- " + ufp->name();
    ufp->rename(newnam);
    ufp->stampTime(UFRuntime::currentTime());
    clog<<"UFServices::frmClient> respond, name= "<<ufp->name()<<", time= "<<ufp->timestamp()<<endl;
    if( soc->writable() > 0 ) {
      soc->send(*ufp);
    }
    else {
      clog<<"UFServices::frmClients> ? socket not writable"<<endl;
      soc->close();
    }
    delete ufp;
    i++;
    cnt++;
  } // while

  if( cnt != (int)stats.size() ) 
    clog<<"UFServices::frmClients> iteration incomplete? cnt= "<<cnt<<", table size= "<<stats.size()<<endl;

  stats.clear();
  return cnt;
} // statClients

int UFServices::cmdClients(UFRndRobinServ::MsgTable& cmnds) {
  // should delete each protocol object (request) after it is handled, then clear the table
  UFRndRobinServ::MsgTable::iterator i = cmnds.begin();
  string newnam;
  int cnt= 0;
  while( i != cmnds.end() ) {
    UFSocket* soc = i->first; 
    UFProtocol* ufp = i->second;
    if( ufp == 0 ) {
      clog<<"UFServices::frmClients> bad UFProtocol pointer?..."<<endl;
      ufp = new UFStrings("Error in ufpingserv> UFProtocol::createFrom failed?...");
    }
    clog<<"UFServices::frmClient> name= "<<ufp->name()<<", time= "<<ufp->timestamp()<<"\n"
        <<", typ= "<<ufp->typeId()<<", desc.: "<<ufp->description()
        <<" length = "<<ufp->length()<<endl;
    newnam = name() + " -- Alive! -- " + ufp->name();
    ufp->rename(newnam);
    ufp->stampTime(UFRuntime::currentTime());
    clog<<"UFServices::frmClient> respond, name= "<<ufp->name()<<", time= "<<ufp->timestamp()<<endl;
    if( soc->writable() > 0 ) {
      soc->send(*ufp);
    }
    else {
      clog<<"UFServices::frmClients> ? socket not writable"<<endl;
      soc->close();
    }
    delete ufp;
    i++;
    cnt++;
  } // while

  if( cnt != (int)cmnds.size() ) 
    clog<<"UFServices::frmClients> iteration incomplete? cnt= "<<cnt<<", table size= "<<cmnds.size()<<endl;

  cmnds.clear();
  return cnt;
} // cmdClients

int UFServices::tscopeClients(UFRndRobinServ::MsgTable& tscopes) {
  // should delete each protocol object (request) after it is handled, then clear the table
  UFRndRobinServ::MsgTable::iterator i = tscopes.begin();
  string newnam;
  int cnt= 0;
  while( i != tscopes.end() ) {
    UFSocket* soc = i->first; 
    UFProtocol* ufp = i->second;
    if( ufp == 0 ) {
      clog<<"UFServices::frmClients> bad UFProtocol pointer?..."<<endl;
      ufp = new UFStrings("Error in ufpingserv> UFProtocol::createFrom failed?...");
    }
    clog<<"UFServices::frmClient> name= "<<ufp->name()<<", time= "<<ufp->timestamp()<<"\n"
        <<", typ= "<<ufp->typeId()<<", desc.: "<<ufp->description()
        <<" length = "<<ufp->length()<<endl;
    newnam = name() + " -- Alive! -- " + ufp->name();
    ufp->rename(newnam);
    ufp->stampTime(UFRuntime::currentTime());
    clog<<"UFServices::frmClient> respond, name= "<<ufp->name()<<", time= "<<ufp->timestamp()<<endl;
    if( soc->writable() > 0 ) {
      soc->send(*ufp);
    }
    else {
      clog<<"UFServices::frmClients> ? socket not writable"<<endl;
      soc->close();
    }
    delete ufp;
    i++;
    cnt++;
  } // while

  if( cnt != (int)tscopes.size() ) 
    clog<<"UFServices::frmClients> iteration incomplete? cnt= "<<cnt<<", table size= "<<tscopes.size()<<endl;

  tscopes.clear();
  return cnt;
} // tscopeClients

int UFServices::mce4Clients(UFRndRobinServ::MsgTable& mce4s) {
  // should delete each protocol object (request) after it is handled, then clear the table
  UFRndRobinServ::MsgTable::iterator i = mce4s.begin();
  string newnam;
  int cnt= 0;
  while( i != mce4s.end() ) {
    UFSocket* soc = i->first; 
    UFProtocol* ufp = i->second;
    if( ufp == 0 ) {
      clog<<"UFServices::frmClients> bad UFProtocol pointer?..."<<endl;
      ufp = new UFStrings("Error in ufpingserv> UFProtocol::createFrom failed?...");
    }
    clog<<"UFServices::frmClient> name= "<<ufp->name()<<", time= "<<ufp->timestamp()<<"\n"
        <<", typ= "<<ufp->typeId()<<", desc.: "<<ufp->description()
        <<" length = "<<ufp->length()<<endl;
    newnam = name() + " -- Alive! -- " + ufp->name();
    ufp->rename(newnam);
    ufp->stampTime(UFRuntime::currentTime());
    clog<<"UFServices::frmClient> respond, name= "<<ufp->name()<<", time= "<<ufp->timestamp()<<endl;
    if( soc->writable() > 0 ) {
      soc->send(*ufp);
    }
    else {
      clog<<"UFServices::frmClients> ? socket not writable"<<endl;
      soc->close();
    }
    delete ufp;
    i++;
    cnt++;
  } // while

  if( cnt != (int)mce4s.size() ) 
    clog<<"UFServices::frmClients> iteration incomplete? cnt= "<<cnt<<", table size= "<<mce4s.size()<<endl;

  mce4s.clear();
  return cnt;
} // mce4Clients

int UFServices::pmotorClients(UFRndRobinServ::MsgTable& pmotors) {
  // should delete each protocol object (request) after it is handled, then clear the table
  UFRndRobinServ::MsgTable::iterator i = pmotors.begin();
  string newnam;
  int cnt= 0;
  while( i != pmotors.end() ) {
    UFSocket* soc = i->first; 
    UFProtocol* ufp = i->second;
    if( ufp == 0 ) {
      clog<<"UFServices::frmClients> bad UFProtocol pointer?..."<<endl;
      ufp = new UFStrings("Error in ufpingserv> UFProtocol::createFrom failed?...");
    }
    clog<<"UFServices::frmClient> name= "<<ufp->name()<<", time= "<<ufp->timestamp()<<"\n"
        <<", typ= "<<ufp->typeId()<<", desc.: "<<ufp->description()
        <<" length = "<<ufp->length()<<endl;
    newnam = name() + " -- Alive! -- " + ufp->name();
    ufp->rename(newnam);
    ufp->stampTime(UFRuntime::currentTime());
    clog<<"UFServices::frmClient> respond, name= "<<ufp->name()<<", time= "<<ufp->timestamp()<<endl;
    if( soc->writable() > 0 ) {
      soc->send(*ufp);
    }
    else {
      clog<<"UFServices::frmClients> ? socket not writable"<<endl;
      soc->close();
    }
    delete ufp;
    i++;
    cnt++;
  } // while

  if( cnt != (int)pmotors.size() ) 
    clog<<"UFServices::frmClients> iteration incomplete? cnt= "<<cnt<<", table size= "<<pmotors.size()<<endl;

  pmotors.clear();
  return cnt;
} // pmotorClients

int UFServices::vacuumClients(UFRndRobinServ::MsgTable& vacs) {
  // should delete each protocol object (request) after it is handled, then clear the table
  UFRndRobinServ::MsgTable::iterator i = vacs.begin();
  string newnam;
  int cnt= 0;
  while( i != vacs.end() ) {
    UFSocket* soc = i->first; 
    UFProtocol* ufp = i->second;
    if( ufp == 0 ) {
      clog<<"UFServices::frmClients> bad UFProtocol pointer?..."<<endl;
      ufp = new UFStrings("Error in ufpingserv> UFProtocol::createFrom failed?...");
    }
    clog<<"UFServices::frmClient> name= "<<ufp->name()<<", time= "<<ufp->timestamp()<<"\n"
        <<", typ= "<<ufp->typeId()<<", desc.: "<<ufp->description()
        <<" length = "<<ufp->length()<<endl;
    newnam = name() + " -- Alive! -- " + ufp->name();
    ufp->rename(newnam);
    ufp->stampTime(UFRuntime::currentTime());
    clog<<"UFServices::frmClient> respond, name= "<<ufp->name()<<", time= "<<ufp->timestamp()<<endl;
    if( soc->writable() > 0 ) {
      soc->send(*ufp);
    }
    else {
      clog<<"UFServices::frmClients> ? socket not writable"<<endl;
      soc->close();
    }
    delete ufp;
    i++;
    cnt++;
  } // while

  if( cnt != (int)vacs.size() ) 
    clog<<"UFServices::frmClients> iteration incomplete? cnt= "<<cnt<<", table size= "<<vacs.size()<<endl;

  vacs.clear();
  return cnt;
} //vacuumClients

int UFServices::barClients(UFRndRobinServ::MsgTable& bars) {
  // should delete each protocol object (request) after it is handled, then clear the table
  UFRndRobinServ::MsgTable::iterator i = bars.begin();
  string newnam;
  int cnt= 0;
  while( i != bars.end() ) {
    UFSocket* soc = i->first; 
    UFProtocol* ufp = i->second;
    if( ufp == 0 ) {
      clog<<"UFServices::frmClients> bad UFProtocol pointer?..."<<endl;
      ufp = new UFStrings("Error in ufpingserv> UFProtocol::createFrom failed?...");
    }
    clog<<"UFServices::frmClient> name= "<<ufp->name()<<", time= "<<ufp->timestamp()<<"\n"
        <<", typ= "<<ufp->typeId()<<", desc.: "<<ufp->description()
        <<" length = "<<ufp->length()<<endl;
    newnam = name() + " -- Alive! -- " + ufp->name();
    ufp->rename(newnam);
    ufp->stampTime(UFRuntime::currentTime());
    clog<<"UFServices::frmClient> respond, name= "<<ufp->name()<<", time= "<<ufp->timestamp()<<endl;
    if( soc->writable() > 0 ) {
      soc->send(*ufp);
    }
    else {
      clog<<"UFServices::frmClients> ? socket not writable"<<endl;
      soc->close();
    }
    delete ufp;
    i++;
    cnt++;
  } // while

  if( cnt != (int)bars.size() ) 
    clog<<"UFServices::frmClients> iteration incomplete? cnt= "<<cnt<<", table size= "<<bars.size()<<endl;

  bars.clear();
  return cnt;
} // barClients

#endif // UFServices

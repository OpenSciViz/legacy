/* mtpingserv2.cc
 */

#include <new>
#include <iostream>
#include <vector>
#include <string>
#include <list>
#include <map>
#include <fstream>
#include <algorithm>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/time.h>
#include <unistd.h>
#include <pthread.h>

#include <cstdlib>
#include <cstring>
#include <cerrno>

#include "UFServerSocket.h"
#include "UFRingBuff.h"
#include "UFHeapRingBuff.h"
#include "UFShMemRingBuff.h"
#include "UFInts.h"
#include "UFStrings.h"
#include "UFStringTokenizer.h"
#include "UFFrameConfig.h"

using std::vector ;
using std::clog ;
using std::endl ;
using std::string ;
using std::list ;
using std::map ;
using std::ifstream ;

/**
 * The port on which the server will listen for incoming
 * client connections.
 */
unsigned short int clientPort = 5555 ;

/**
 * The time to sleep between loop iterations -- this prevents
 * contention.
 */
unsigned long int sleepTime = 50000 ; // microseconds, 50ms

/**
 * The server will continue to run while this variable is true.
 * Once it is set to false, all threads will end themselves, and
 * main() will clean up the mess.
 */
volatile bool keepRunning = true ;

/**
 * The type used to store information about the currently
 * running threads.  This is needed to be able to join()
 * a thread when it terminates.
 */
typedef map< UFSocket*, pthread_t > clientThreadListType ;

/**
 * The structure used to store information about currently
 * running threads.
 */
clientThreadListType clientThreadList ;

/**
 * The threadID of the thread used to listen for client
 * connections.
 */
pthread_t clientListenThreadID ;

/**
 * The type used to store the UFSocket objects for
 * currently connected clients.
 */
typedef list< UFSocket* > clientListType ;

/**
 * The structure used to store information about newly
 * connected clients.  The main thread will use this
 * structure to construct a new thread for each new
 * client.
 */
clientListType newClientList ;

/**
 * The mutex used to lock the newClientList.
 */
pthread_mutex_t newClientListMutex = PTHREAD_MUTEX_INITIALIZER ;

/**
 * The structure used to store the UFSocket objects of
 * recently terminated client connections.  The main
 * thread will read from this structure and join any
 * dangling threads.
 */
clientListType deadThreadList ;

/**
 * The mutex used to obtain exclusive access to
 * the deadThreadList.
 */
pthread_mutex_t deadThreadListMutex = PTHREAD_MUTEX_INITIALIZER ;

/**
 * This structure is used as a functor to provide case
 * insensitive std::string comparisons for the imageList.
 */
struct noCaseCompare
{
  inline bool operator()( const string& lhs, const string& rhs ) const
  {
    return (::strcasecmp( lhs.c_str(), rhs.c_str() ) < 0) ;
  }
} ;

/**
 * The type used to store the UFInts images.
 */
typedef map< string, list< UFInts* >, noCaseCompare > imageListType ;
// typedef UFShMemRingBuff imageListType ;

/**
 * The structure used to store the UFInts images.
 */
imageListType* imageList ;

/**
 * A mutex used to synchronize access to the imageList.
 */
pthread_mutex_t imageListMutex = PTHREAD_MUTEX_INITIALIZER ;

/**
 * The type used to store frames that are reserved.
 */
typedef list< UFInts* > reservedFramesType ;

/**
 * The structure which holds holds for frames which are reserved
 * (in use by readers) and must not be deallocated or removed
 * from memory.
 */
reservedFramesType reservedFrames ;

/**
 * A mutex used to synchronize access to the reservedFrames
 * structure.
 */
pthread_mutex_t reservedFramesMutex = PTHREAD_MUTEX_INITIALIZER ;

/**
 * This variable is set to true if frames are to be saved to disk.
 */
bool writeFramesToDisk = false ;

/**
 * This variable is true if images are to be written to disk in raw
 * format.
 */
bool useRawFile = true ;

/**
 * Read-only
 * ---------
 * A thread must acquire imageListMutex and reservedFramesMutex (IN THAT ORDER!)
 * to read a frame.  Once both mutexes are acquired, the thread must
 * check for the existence of a particular frame it wants.  If it is found,
 * the thread may put that index into the reservedFrames structure,
 * and it will not be removed from memory until that thread has removed the
 * lock from the reservedFrames structure.  This removal is again done
 * by locking BOTH structures.
 */
/**
 * Write
 * -----
 * To add an image to the imageList, acquire BOTH locks as described above.
 * The writer thread may then just add to the imageList.
 */

/**
 * The width/length of the images to be communicated.
 */
unsigned int imageWidth = 128 ;

/**
 * The number of bits per pixel.
 */
unsigned int bpp = 32 ;

/**
 * The name of the configuration file for the buffers.
 */
string confFileName( "mtpingserv2.conf" ) ;

/**
 * This variable counts the global sequence number.  It is only
 * updated once pairs of frames have been received.
 */
unsigned int globalSeqNum = 0 ;

/**
 * Mutex used to synchronize access to the global sequence counter.
 */
pthread_mutex_t globalSeqNumMutex = PTHREAD_MUTEX_INITIALIZER ;

/**
 * The type used to store frames to be written to disk.
 */
typedef list< UFInts* > writeFramesListType ;

/**
 * The structure used to store pointers to frames to be written
 * to disk.  Any frames in this structure are also locked in
 * the reservedFrames structure.
 */
writeFramesListType writeFramesList ;

/**
 * The mutex used to synchronize access to the writeFrameList.
 */
pthread_mutex_t writeFramesListMutex = PTHREAD_MUTEX_INITIALIZER ;

/**
 * The thread ID for the write frames thread.  The write frames thread
 * may not be run, so this may not be used.
 */
pthread_t writeFramesThreadID = 0 ;

bool readConfFile() ;
bool writeFrameToDisk( UFInts* ) ;

void* clientThread( void* ) ;
void* clientListenThread( void* ) ;
void* writeFramesThread( void* ) ;

void removeOldFrames() ;
void sigHandler( int ) ;
void addImage( UFInts* ) ;
void removeClient( UFSocket* ) ;

// Polymorphism eh?  I don't see your fancy-schmancy polymorphism!
vector< UFProtocol* > processRequest( UFProtocol* ) ;
vector< UFProtocol* > processRequest( UFInts* ) ;
vector< UFProtocol* > processRequest( UFTimeStamp* ) ;
vector< UFProtocol* > processRequest( UFFrameConfig* ) ;

/**
 * Convenience operator function for outputting a UFStrings
 * object to a C++ output stream.
 */
std::ostream& operator<<( std::ostream& out, const UFStrings& rhs )
{
  for( int i = 0 ; i < rhs.elements() ; ++i )
    {
      clog << "[ " << i << " ]> " << rhs[ i ] << endl ;
    }
  return out ;
}

void sigHandler( int )
{
  clog << "Caught signal, shutting down...\n" ;
  keepRunning = false ;
}

/**
 * Output information about how to use this program.
 */
void usage( const string& progName )
{
  clog << endl ;
  clog << "Usage: " << progName << " [options]\n" ;
  clog << "-b <bits>: Specify bits per pixel ("
       << bpp << ")\n" ;
  clog << "-c <port>: Listen on <port> for client connections ("
       << clientPort << ")\n" ;
  clog << "-d: Write frames to disk ("
       << writeFramesToDisk << ")\n" ;
  clog << "-f <filename>: Specify configuration file ("
       << confFileName << ")\n" ;
  clog << "-h: Print help menu\n" ;
  clog << "-r: Write raw files (instead of FITS) ("
       << useRawFile << ")\n" ;
  clog << "-s <time>: Specify the sleep time in microseconds ("
       << sleepTime << ")\n" ;
  clog << "-w <width>: Width (in pixels) of images ("
       << imageWidth << ")\n" ;
  clog << endl ;
}

// Precondition: No mutexes locks held
void removeClient( UFSocket* theClient )
{
  // Let main() know that the thread has exited so it can
  // clean up the mess.
  ::pthread_mutex_lock( &deadThreadListMutex ) ;
  deadThreadList.push_back( theClient ) ;
  ::pthread_mutex_unlock( &deadThreadListMutex ) ;
}

// Precondition: writeFramesListMutex NOT held
void writeFrame( UFInts* theImage )
{
  ::pthread_mutex_lock( &writeFramesListMutex ) ;
  writeFramesList.push_back( theImage ) ;
  ::pthread_mutex_unlock( &writeFramesListMutex ) ;
}

/**
 * The main method of this program.
 */
int main( int argc, char** argv )
{

  int c = EOF ;

  // Parse command line options.
  while( (c = ::getopt( argc, argv, "b:c:df:hl:p:rs:w:" )) != EOF )
    {
      switch( c )
	{
	case 'b':
	  bpp = static_cast< unsigned short int >( ::atoi( optarg ) ) ;
	  break ;
	case 'c':
	  clientPort = static_cast< unsigned short int >( atoi( optarg ) ) ;
	  break ;
        case 'd':
          // Just toggle, we don't really know what the current state
          // of the variable may be.
          writeFramesToDisk = !writeFramesToDisk ;
          break ;
	case 'f':
	  confFileName = optarg ;
	  break ;
	case 'h':
	  usage( argv[ 0 ] ) ;
	  return 0 ;
        case 'r':
          useRawFile = true ;
          break ;
	case 's':
	  sleepTime = static_cast< unsigned long int >( atoi( optarg ) ) ;
	  break ;
	case 'w':
	  imageWidth = static_cast< unsigned int >( atoi( optarg ) ) ;
	  break ;
	}
    }

  if( 0 != (imageWidth % 8) )
    {
      clog << "Error: image width must be a multiple of 8 (byte)\n" ;
      return 0 ;
    }

  // Output information about the server's runtime settings
  clog << "Listening for client connections on port "
       << clientPort << endl ;

  // All images are sizeof(int) pixels deep
  clog << "Each image is " << imageWidth << 'x'
       << imageWidth << 'x' << bpp << " pixels\n" ;

  if( SIG_ERR == ::signal( SIGINT, sigHandler ) )
    {
      clog << "Failed to establish signal handler for SIGINT: "
	   << strerror( errno ) << endl ;
      return 0 ;
    }
  if( SIG_ERR == ::signal( SIGALRM, sigHandler ) )
    {
      clog << "Failed to establish signal handler for SIGALRM: "
	   << strerror( errno ) << endl ;
      return 0 ;
    }
  if( SIG_ERR == ::signal( SIGTERM, sigHandler ) )
    {
      clog << "Failed to establish signal handler for SIGTERM: "
	   << strerror( errno ) << endl ;
      return 0 ;
    }

  // Allocate memory
  UFServerSocket* clientSS = 0 ;
  try
    {
      clientSS = new UFServerSocket( clientPort ) ;

      // Instantiate the imageList.
      // This performs a shallow copy of the data just allocated.
      imageList = new imageListType ;
    }
  catch( std::bad_alloc )
    {
      clog << "main> Memory allocation failure\n" ;
      return 0 ;
    }

  // Read the configuration file.
  if( !readConfFile() )
    {
      clog << "Failed to read configuration file: " << confFileName << endl ;
      return 0 ;
    }

  // Spawn clientListenThread
  if( ::pthread_create( &clientListenThreadID,
			0,
			clientListenThread,
			static_cast< void* >( clientSS ) ) != 0 )
    {
      clog << "main> Error creating clientListenThread: "
	   << strerror( errno ) << endl ;
      return 0 ;
    }

  // Spawn the writeFramesThread, if enabled from the command line.
  if( writeFramesToDisk )
    {
      if( ::pthread_create( &writeFramesThreadID,
                            0,
                            writeFramesThread,
                            0 ) != 0 )
        {
          clog << "main> Error creating writeFramesThread: "
               << strerror( errno ) << endl ;
          return 0 ;
        }
    }

  // Declare a local instance of type clientListType.
  // This structure is used to prevent locking of more
  // than one mutex at a time.
  clientListType localClientList ;

  while( keepRunning )
    {

      //      clog << "main()\n" ;

      // Sleep a short time to prevent contention
      ::usleep( sleepTime ) ;

      // Check the deadThreadList for newly dead threads,
      //  close and deallocate its socket, and pthread_join()
      //  its threadID, remove the <UFSocket*,pthread_t> for
      //  that thread from the clientThreadList
      // Check for dead threads before checking for new clients.
      // Perform two separate iterations to avoid holding
      // more than one lock at a time.
      //
      pthread_mutex_lock( &deadThreadListMutex ) ;
      for( clientListType::iterator ptr = deadThreadList.begin(),
	     end = deadThreadList.end() ; ptr != end ; ++ ptr )
	{
	  //	  clog << "main> Found dead connection\n" ;
	  localClientList.push_back( *ptr ) ;
	}
      deadThreadList.clear() ;
      pthread_mutex_unlock( &deadThreadListMutex ) ;

      // The clientThreadList is only accessed/mutated by the main thread,
      // so no lock needed here.
      for( clientListType::iterator ptr = localClientList.begin(),
	     end = localClientList.end() ; ptr != end ; ++ptr )
	{
	  clientThreadListType::iterator tPtr =
                               clientThreadList.find( *ptr ) ;
	  if( tPtr == clientThreadList.end() )
	    {
	      clog << "main> Unable to find dead client in "
		   << "clientThreadList" ;
	      delete *ptr ;
	    }

	  // Remove this element from the clientThreadList
	  clientThreadList.erase( tPtr ) ;

	  // Wait for the thread to join, should be quickly
	  if( ::pthread_join( tPtr->second, 0 ) != 0 )
	    {
	      clog << "main> Unable to join dead thread\n" ;
	    }
	  clog << "main> Joined dead thread\n" ;

	  // Close and deallocate the dead client's UFSocket.
	  tPtr->first->close() ;
	  delete tPtr->first ;

        }

      // Make sure to clear the localClientList for use later.
      // No continue/break statements have been issued inside
      // the main while loop...i.e., this call to clear() is
      // guaranteed to be executed on each iteration through the
      // while loop.
      localClientList.clear() ;

      // Process new clients.
      // Perform two separate iterations to avoid holding
      // more than one lock at a time.
      pthread_mutex_lock( &newClientListMutex ) ;
      for( clientListType::iterator ptr = newClientList.begin(),
	     end = newClientList.end() ; ptr != end ; ++ptr )
	{
	  //	  clog << "main> Found item in newClientList\n" ;
	  localClientList.push_back( *ptr ) ;
	}
      // Clear the newClientList
      newClientList.clear() ;
      pthread_mutex_unlock( &newClientListMutex ) ;

      // Walk the localClientList, and handle each new connection
      for( clientListType::iterator ptr = localClientList.begin(),
	     end = localClientList.end() ; ptr != end ; ++ptr )
	{

	  //	  clog << "main> Found new connection\n" ;

	  UFSocket* clientSock = *ptr ;
	  pthread_t threadId = 0 ;

	  // Create a new clientThread for this client
	  if( ::pthread_create( &threadId,
				0,
				clientThread,
				static_cast< void* >( clientSock ) ) != 0 )
	    {
	      clog << "main> Failed to spawn new client thread: "
		   << strerror( errno ) << endl ;
	      clientSock->close() ; delete clientSock ;
	      continue ;
	    }

	  // Add this clientThread to the clientThreadList
	  // The main() thread is the only thread which mutates
	  // this structure, so no need for locking.
          // This is ok even if the thread dies immediately.
	  clientThreadList.insert( clientThreadListType::value_type(
			    clientSock,
			    threadId ) ) ;

	} // for( localClientList )

      // Clear the localClientList
      localClientList.clear() ;

      // Task: remove any old frames from the buffers that aren't being
      // written to a client
      removeOldFrames() ;

    } // while( keepRunning )

  // Perform shutdown.
  clog << "main> Shutting down...\n" ;

  clog << "main> Waiting for clientListenThread to terminate...\n" ;
  // Wait for listen threads to join.
  if( ::pthread_join( clientListenThreadID, 0 ) != 0 )
    {
      clog << "main> pthread_join() failed because: "
	   << strerror( errno ) << endl ;
    }

  // Wait for writeFramesThread to terminate
  if( writeFramesToDisk )
    {
      clog << "main> Waiting for writeFramesThread to terminate...\n" ;
      if( ::pthread_join( writeFramesThreadID, 0 ) != 0 )
        {
          clog << "main> pthread_join() failed on writeFramesThread: "
               << strerror( errno ) << endl ;
        }
    }

  clog << "main> Waiting for client threads to terminate...\n" ;
  // keepRunning is already set to false.
  // Wait for all threads to join.
  for( clientThreadListType::iterator ptr = clientThreadList.begin(),
	 end = clientThreadList.end() ; ptr != end ; ++ptr )
    {
      if( ::pthread_join( ptr->second, 0 ) != 0 )
	{
	  clog << "main> pthread_join() failed because: "
	       << strerror( errno ) << endl ;
	}

      // Close and deallocate this client's UFSocket.
      ptr->first->close() ;
      delete ptr->first ;
    }
  clientThreadList.clear() ;

  // There are no clients or threads currently using the existing
  // frames, remove them all.
  for( imageListType::iterator ilPtr = imageList->begin() ;
       ilPtr != imageList->end() ; ++ilPtr )
    {
      for( list< UFInts* >::iterator frPtr = ilPtr->second.begin() ;
           frPtr != ilPtr->second.end() ; ++frPtr )
        {
          delete *frPtr ;
        }
      ilPtr->second.clear() ;
    }
  imageList->clear() ;

  // oldFramesThread will have deallocated all frames
  // Deallocate the image list.
  delete imageList ; imageList = 0 ;

  // TODO: Destroy the mutexes

  // Close and deallocate the server sockets.
  clientSS->close() ;

  delete clientSS ; clientSS = 0 ;

  // Return happily :)
  return 0 ;

}

void removeOldFrames()
{

  list< UFInts* > oldFrames ;

  // Order important for these locks
  ::pthread_mutex_lock( &imageListMutex ) ;
  ::pthread_mutex_lock( &reservedFramesMutex ) ;

  for( imageListType::iterator ilPtr = imageList->begin(),
	 ilEnd = imageList->end() ; ilPtr != ilEnd ; ++ilPtr )
    {
      if( ilPtr->second.size() < 2 )
	{
          // We need at least two images in a buffer to consider
          // removing one of them.
	  continue ;
	}

      // For each buffer, remove old elements if they are not
      // in the reservedFrames list.  The old elements will be
      // toward the end of each buffer
      // Skip over the first frame in each buffer, this is the most
      // current frame; this will work because this buffer has been
      // verified to be non-empty.
      //      clog << "removeOldFrames> Checking " << ilPtr->first
      //   << " for old frames\n" ;

      // The iterator is not incremented here.
      // It must be incremented inside of the for loop itself because
      // if the iterator is removed from the structure that iterator
      // is no longer valid.  Therefore, when an erase is performed,
      // the iterator is assigned to the result of that erase, otherwise
      // it is incremented.
      // Starting from the back here because all new images are ALWAYS
      // added to the front of the list.
      // TODO: Use a reverse iterator here?
      list< UFInts* >::iterator fPtr = ++(ilPtr->second.begin()) ;
      while( fPtr != ilPtr->second.end() )
	{
          //	  clog << "removeOldFrames> Possible old frame: "
          //	       << (*fPtr)->name() << endl ;

	  // If this element is in the reservedFrames list, just ignore it
	  if( std::find( reservedFrames.begin(), reservedFrames.end(),
			 *fPtr ) != reservedFrames.end() )
	    {
	      // It is currently in use, move to next one
	      ++fPtr ;
	      continue ;
	    }

	  clog << "removeOldFrames> Removing frame: "
	       << (*fPtr)->name() << endl ;

	  // Add it to the oldFrames list
	  oldFrames.push_back( *fPtr ) ;

	  // Remove from imageList
	  fPtr = ilPtr->second.erase( fPtr ) ;

	} // frame iterator

    } // buffer iterator

  ::pthread_mutex_unlock( &reservedFramesMutex ) ;
  ::pthread_mutex_unlock( &imageListMutex ) ;

  // Each frame has already been written to disk.  Deallocate the
  // frames which are no longer in use.
  for( list< UFInts* >::iterator ptr = oldFrames.begin() ;
       ptr != oldFrames.end() ; ++ptr )
    {
      delete *ptr ;
    }

}

void* writeFramesThread( void* )
{

  list< UFInts* > localImageList ;

  while( keepRunning )
    {

      ::usleep( 10000 ) ;

      ::pthread_mutex_lock( &reservedFramesMutex ) ;
      ::pthread_mutex_lock( &writeFramesListMutex ) ;

      for( writeFramesListType::iterator ptr = writeFramesList.begin() ;
           ptr != writeFramesList.end() ; ++ptr )
        {
          reservedFrames.push_back( *ptr ) ;
          localImageList.push_back( *ptr ) ;
        }
      writeFramesList.clear() ;

      ::pthread_mutex_unlock( &writeFramesListMutex ) ;
      ::pthread_mutex_unlock( &reservedFramesMutex ) ;

      if( localImageList.empty() )
        {
          continue ;
        }

      // Write the frames to disk
      for( list< UFInts* >::iterator ptr = localImageList.begin() ;
           ptr != localImageList.end() ; ++ptr )
        {
          if( !writeFrameToDisk( *ptr ) )
            {
              clog << "writeFramesThread> Failed to write frame: "
                   << (*ptr)->name() << endl ;
            }
        }

      // Now that the frames have been written to disk, remove the
      // lock in the reservedFrames structure.
      ::pthread_mutex_lock( &reservedFramesMutex ) ;
     
      for( list< UFInts* >::iterator ptr = localImageList.begin() ;
           ptr != localImageList.end() ; ++ptr )
        {
          reservedFramesType::iterator frPtr = std::find( reservedFrames.begin(),
                                                          reservedFrames.end(),
                                                          *ptr ) ;
          if( frPtr == reservedFrames.end() )
            {
              clog << "writeFramesThread> Failed to find image in "
                   << "reservedFrames: " << (*ptr)->name() << endl ;
              continue ;
            }

          // Found the frame, remove it from the reservedFrames.
          reservedFrames.erase( frPtr ) ;
        }

      ::pthread_mutex_unlock( &reservedFramesMutex ) ;

      localImageList.clear() ;

    } // while( keepRunning )


  return 0 ;
}

/**
 * This method will be run in a separate thread, and is
 * responsible for listening for new client connections.
 * When a new client is detected, that client's UFSocket
 * object (pointer) is placed in the newClientList for
 * the main thread to handle.
 */
void* clientListenThread( void* data )
{

  // Obtain the pointer to the server socket
  UFServerSocket* ss = static_cast< UFServerSocket* >( data ) ;
  if( NULL == ss )
    {
      clog << "clientListenThread> static_cast<> failed\n" ;

      // We can't do anything without a valid server socket.
      // Shut down the server.
      keepRunning = false ;
      return 0 ;
    }

  // Listen on that socket for new connections
  if( ss->listen( clientPort ).listenFd < 0 )
    {
      clog << "clientListenThread> Error in listen(): "
	   << strerror( errno ) << endl ;
      keepRunning = false ;
      return 0 ;
    }

  // Continue while keepRunning is true
  while( keepRunning )
    {

      // Sleep for a short time to prevent contention
      ::usleep( sleepTime ) ;

      // Check if any data is available (possible client
      // connection)
      int readable = ss->pendingIO( -1.0, ss->getInfo().listenFd ) ;
      if( readable < 0 )
	{
	  clog << "clientListenThread> Error in pendingIO(): "
	       << strerror( errno ) << endl ;

	  // We can't do anything without a working server socket.
	  // Shut down the server.
	  keepRunning = false ;
	  continue ;
	}
      else if( 0 == readable )
	{
	  // No client available, no problem
	  continue ;
	}

      // Some data is ready, try to do the accept()
      UFSocket clientSock ;
      if( ss->accept( clientSock ) < 0 )
	{
	  clog << "clientListenThread> accept() failed: "
	       << strerror( errno ) << endl ;
	  continue ;
	}

      UFSocket* addMe = new (nothrow) UFSocket( clientSock ) ;
      if( NULL == addMe )
	{
	  clog << "clientListenThread> Memory allocation failure\n" ;
          keepRunning = false ;
	  return 0 ;
	}

      ::pthread_mutex_lock( &newClientListMutex ) ;
      newClientList.push_back( addMe ) ;
      ::pthread_mutex_unlock( &newClientListMutex ) ;

      clog << "clientListenThread> Accepted new connection\n" ;

    }

  // The server socket will be officially closed and deallocated
  // by the main() thread.
  clog << "clientListenThread> Exiting normally\n" ;
  return 0 ;

}

/**
 * A separate thread will run this method for each client that
 * is connected to the server.
 * When this methods ends, the client is no longer connected.
 * This method will schedule the client for removal before
 * returning.
 */
void* clientThread( void* data )
{
  // Obtain a pointer to the client's socket
  UFSocket* clientSock = static_cast< UFSocket* >( data ) ;
  if( NULL == clientSock )
    {
      clog << "clientThread> static_cast<> failed\n" ;
      return 0 ;
    }

  clog << "clientThread> Processing new client\n" ;

  // This variable indicates that the client is currently connected
  bool isConnected = true ;

  // Continue while this client is connected and the server
  // is running
  while( isConnected && keepRunning )
    {

      // Sleep for a short time to prevent contention
      ::usleep( sleepTime ) ;

      // Check if any data is available for reading
      int readable = clientSock->readable() ;
      if( readable < 0 )
	{
	  clog << "clientThread> Error in readable(): "
	       << strerror( errno ) << endl ;

	  // client is no longer connected
	  isConnected = false ;
	  continue ;
	}
      else if( 0 == readable )
        {
          // No data, no big deal
          continue ;
        }

      // Retrieve the request from the client
      UFProtocol* request = UFProtocol::createFrom( *clientSock ) ;
      if( NULL == request )
	{
	  clog << "clientThread> Lost connection: "
	       << strerror( errno ) << endl ;

	  // Read error from the client
	  isConnected = false ;
	  continue ;
	}

      clog << "clientThread> Received client request\n" ;

      // Process the client's request.
      // If data needs to be returned to the client, processRequest()
      // will return a pointer to that object (allocated on the heap).
      // Otherwise, it will return an empty vector.  If this is the case,
      // do NOT deallocate.
      vector< UFProtocol* > reply = processRequest( request ) ;
      if( reply.empty() )
	{
          //	  clog << "clientThread> processRequest() failed\n" ;
	  continue ;
	}

      for( vector< UFProtocol* >::iterator ptr = reply.begin() ;
	   ptr != reply.end() ; ++ptr )
	{
	  // Got the reply, write it back to the client
	  int bytesWritten = clientSock->send( *ptr ) ;

	  // Did the write succeed?
	  if( bytesWritten < 0 )
	    {
	      clog << "clientThread> Write error to client: "
		   << strerror( errno ) << endl ;
	      
	      // Write error, client connection no longer valid
	      isConnected = false ;
	      break ;
	    }
	  
	  clog << "clientThread> Wrote reply to client\n" ;
	}

      // Deallocate memory
      delete request ;

      // The writes are complete, remove any reserved locks for these
      // images, if there are any.
      // Delaying the removal of reserved frames to this point may
      // cost more memory (assuming a single client requests more
      // than one frame at a time), but it makes life easier for
      // implementing and maintaining.
      ::pthread_mutex_lock( &reservedFramesMutex ) ;

	// TODO: Skip if reservedFrames.empty() ?
	// Have to keep the lock, and delete each element

      // Make sure to deallocate all memory no matter how the above for
      // loop ends.
      for( vector< UFProtocol* >::size_type i = 0 ; i < reply.size() ; ++i )
	{
	  // Whether the write succeeds or not, remove this element
	  // from the reservedFrames list if it is in there
	  reservedFramesType::iterator rfPtr = std::find( reservedFrames.begin(),
							  reservedFrames.end(),
							  reply[ i ] ) ;
	  if( rfPtr != reservedFrames.end() )
	    {
	      // Found it
              // There may be multiple instances of this frame in the
              // reservedFrames buffer, just remove one of them (they
              // are identical)
	      reservedFrames.erase( rfPtr ) ;
	    }
          else
            {
              // Only delete this object if it is NOT in the reservedFrames list.
              // Elements in the reservedFrames list refer to images maintained
              // by the main loop and oldFrames loop.  Erasing one of them here
              // causes segmentation faults later when oldFramesThread attempts
              // to flush this frame.
              delete reply[ i ] ;
            }
	}

      ::pthread_mutex_unlock( &reservedFramesMutex ) ;

    } // while( isConnected && keepRunning )

  // Either the client is no longer connected, or the server
  // is shutting down.
  clog << "clientThread> Lost Client\n" ;

  // Remove this client from the server tables.
  // Per prerequisites of removeClient(), no mutex locks
  // are held when calling this method.
  //
  // Do NOT deallocate the clientSock, this is what the main()
  // thread uses to join() this thread.
  //
  removeClient( clientSock ) ;

  return 0 ;

}

/**
 * This method is responsible for handling a request from
 * a client.
 * So much for polymorphic object serialization.
 */
vector< UFProtocol* > processRequest( UFProtocol* request )
{
  vector< UFProtocol* > retMe ;

  switch( request->typeId() )
    {
    case UFProtocol::_TimeStamp_:
      retMe = processRequest( dynamic_cast< UFTimeStamp* >( request ) ) ;
      break ;
    case UFProtocol::_FrameConfig_:
      retMe = processRequest( dynamic_cast< UFFrameConfig* >( request ) ) ;
      break ;
    case UFProtocol::_Ints_:
      retMe = processRequest( dynamic_cast< UFInts* >( request ) ) ;
      break ;
    default:
      clog << "processRequest> Received unknown UFProtocol object: "
	   << request->typeId() << endl ;
      break ;
    }

  return retMe ;
}

vector< UFProtocol* > processRequest( UFInts* newFrame )
{

  // The name field contains the buffer into which to put the new Frame
  ::pthread_mutex_lock( &imageListMutex ) ;
  imageListType::iterator ptr = imageList->find( newFrame->name() ) ;
  if( ptr == imageList->end() )
    {
      // Unable to find a buffer
      clog << "processRequest(UFInts*)> Unable to find a buffer named: "
           << newFrame->name() << endl ;
    }
  else
    {
      // Insert the frame at the front of the list
      ptr->second.push_front( newFrame ) ;

	  // Stamp the image with the time it was received
      UFStringTokenizer st( newFrame->name() ) ;
      strstream s ;

      for( UFStringTokenizer::size_type i = 0 ; i < st.size() ; ++i )
        {
          s << st[ i ] ;
          if( i < (st.size() + 1) )
            {
              s << '.' ;
            }
        }

      // Append the current time
      timeval now ;
      ::gettimeofday( &now, 0 ) ;

      s << now.tv_sec << '.' << now.tv_usec << ends ;
      string name = s.str() ;
      delete[] s.str() ;

      newFrame->rename( name ) ;

      clog << "processRequest(UFInts*)> Added image to buffer: "
           << newFrame->name() << endl ;

      ::pthread_mutex_lock( &globalSeqNumMutex ) ;
      ++globalSeqNum ;
      ::pthread_mutex_unlock( &globalSeqNumMutex ) ;

      if( writeFramesToDisk )
        {
          writeFrame( newFrame ) ;
        }

    }
  ::pthread_mutex_unlock( &imageListMutex ) ;

  // No reply needed
  return vector< UFProtocol* >() ;

}

vector< UFProtocol* > processRequest( UFTimeStamp* request )
{
  //  clog << "processRequest(UFTimeStamp*)> name: "
  //   << request->name() << endl ;

  vector< UFProtocol* > retMe ;

  // Name: empty, send (current) FrameConfig
  // Name: "BN" -> send buffer names to client
  // Name: buffername, send most recent frame from said buffer
  //
  // A UFTimeStamp is sent by the client to request a UFFrameConfig
  // object representing the current state and server info
  //
  if( request->name() == "FC" )
    {
      // Send FrameConfig
      UFFrameConfig* frame = new (nothrow) UFFrameConfig( imageWidth,
							  imageWidth,
							  bpp ) ;
      if( NULL == frame )
	{
	  clog << "processRequest(UFTimeStamp*)> Memory allocation failure\n" ;
	  ::exit( 0 ) ;
	}

      // Lock before accessing globalSeqNum
      ::pthread_mutex_lock( &globalSeqNumMutex ) ;
      frame->setFrameObsSeqNo( globalSeqNum ) ;
      ::pthread_mutex_unlock( &globalSeqNumMutex ) ;

      retMe.push_back( static_cast< UFProtocol* >( frame ) ) ;
    }
  else if( request->name() == "BN" )
    {
      // Send buffer names
      // Shouldn't need to lock imageList here
      vector< string > names ;
      for( imageListType::const_iterator ptr = imageList->begin(),
	     end = imageList->end() ; ptr != end ; ++ptr )
	{
	  names.push_back( ptr->first ) ;
	}
      retMe.push_back( new (nothrow) UFStrings( "BN", names ) ) ;
    }
  else if( imageList->find( request->name() ) != imageList->end() )
    {
      clog << "processRequest(UFTimeStamp*)> Request for buffer: "
           << request->name() << endl ;

      // Buffer name, send latest image from given buffer
      // The order of the locking of the mutexes is important.
      ::pthread_mutex_lock( &imageListMutex ) ;

      // Find the buffer in question
      // No need to verify the existence of the buffer name, we already
      // did that above.
      imageListType::iterator ptr = imageList->find( request->name() ) ;

      // We do however have to make sure that the buffer isn't empty
      if( ptr->second.empty() )
	{
	  // Image not found
	  ::pthread_mutex_unlock( &imageListMutex ) ;
          UFTimeStamp* t = new (nothrow) UFTimeStamp( string( "ERROR Buffer Empty" ) ) ;
          retMe.push_back( t ) ;
	  return retMe ;
	}

      // Found the buffer we want, and there is a frame there.
      // New frames are always pushed onto the front of the list.

      // Add this frame to the reservedFrames list
      ::pthread_mutex_lock( &reservedFramesMutex ) ;
      reservedFrames.push_back( ptr->second.front() ) ;

      // Add this frame pointer to the return vector
      retMe.push_back( ptr->second.front() ) ;

      clog << "processRequest(UFTimeStamp*)> Sending frame: "
           << ptr->second.front()->name() << endl ;

      // Unlock the mutexes
      ::pthread_mutex_unlock( &reservedFramesMutex ) ;
      ::pthread_mutex_unlock( &imageListMutex ) ;

    }
  else
    {
      // Unknown name/command
      clog << "processRequest(UFTimeStamp*)> Unknown command> "
		<< request->name() << endl ;
      retMe.push_back( new UFTimeStamp( string( "ERROR Unknown Command" ) ) ) ;
    }
  return retMe ;
}

vector< UFProtocol* > processRequest( UFFrameConfig* request )
{
  clog << "processRequest(UFFrameConfig*)\n" ;
  return vector< UFProtocol* >() ;
}

bool readConfFile()
{
  ifstream inFile( confFileName.c_str() ) ;
  if( !inFile.is_open() )
    {
      clog << "readConfFile> Unable to open " << confFileName << endl ;
      return false ;
    }

  string line ;
  while( getline( inFile, line ) )
    {
      if( line.empty() || '#' == line[ 0 ] )
	{
	  // Empty or comment
	  continue ;
	}

      if( imageList->find( line ) != imageList->end() )
	{
	  clog << "readConfFile> Found duplicate entry for "
	       << line << endl ;
	  continue ;
	}

      // Otherwise just add it
      imageList->insert( imageListType::value_type( line,
						    list< UFInts* >() ) ) ;
      //      clog << "readConfFile> Added buffer: " << line << endl ;
    } // while

  clog << "readConfFile> Created " << imageList->size() << " buffers\n" ;
  inFile.close() ;
  return true ;
}

bool writeFrameToDisk( UFInts* writeMe )
{

  // Write to disk
  UFStringTokenizer st( writeMe->name() ) ;
  string fileName ;
  for( UFStringTokenizer::size_type i = 0 ; i < st.size() ; ++i )
    {
      fileName += st[ i ] ;
      if( i < (st.size() + 1) )
	{
	  fileName += "." ;
	}
    }

  ofstream outFile( fileName.c_str(), ios::out | ios::bin ) ;
  if( !outFile.is_open() )
    {
      clog << "writeFrame> Failed to open output file: "
	   << fileName << endl ;
      return false ;
    }

  //  clog << "writeFrame> Writing: " << fileName << " of size: "
  //       << (writeMe->numVals() * sizeof(int)) << endl ;
  outFile.write( (const unsigned char*) writeMe->valData(),
		 (writeMe->numVals() * sizeof(int)) ) ;

  outFile.close() ;
  return true ;
}

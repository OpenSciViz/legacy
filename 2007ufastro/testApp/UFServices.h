#if !defined(__UFServices_h__)
#define __UFServices_h__ "$Name:  $ $Id: UFServices.h,v 0.0 2003/01/24 16:26:55 hon Developmental $"
#define __UFServices_H__(arg) const char arg##UFServices_h__rcsId[] = __UFServices_h__;

#include "string"
#include "vector"
#include "iostream.h"

#include "UFRndRobinServ.h"

class UFServices: public UFRndRobinServ {
public:
  static int main(int argc, char** argv);
  UFServices(int argc, char** argv);
  UFServices(const string& name, int argc, char** argv);
  inline virtual ~UFServices() {}

  // override these UFRndRobinServ virtuals:
  virtual int servReqs(UFRndRobinServ::MsgTable& reqtbl);

  // provide tests for each of the standard UF services  
  virtual int anonClients(UFRndRobinServ::MsgTable& reqtbl);
  virtual int imgClients(UFRndRobinServ::MsgTable& reqtbl);
  virtual int frmClients(UFRndRobinServ::MsgTable& reqtbl);
  virtual int statClients(UFRndRobinServ::MsgTable& reqtbl);
  virtual int cmdClients(UFRndRobinServ::MsgTable& reqtbl);
  virtual int tscopeClients(UFRndRobinServ::MsgTable& reqtbl);
  virtual int mce4Clients(UFRndRobinServ::MsgTable& reqtbl);
  virtual int pmotorClients(UFRndRobinServ::MsgTable& reqtbl);
  virtual int vacuumClients(UFRndRobinServ::MsgTable& reqtbl);
  virtual int barClients(UFRndRobinServ::MsgTable& reqtbl);


private:
};

#endif // __UFServices_h__
      

/* ufserial.cc */

#include "UFSerialPort.h"

#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx)
void ufserial(int portNo=2) {
  UFSerialPort::main(portNo);
}
#elif defined(sun) || defined(solaris)|| defined(Solaris) || defined(SOLARIS)\
 || defined( LINUX )
int main(int argc, char** argv) {
  UFSerialPort::main(argc, argv);
}
#endif

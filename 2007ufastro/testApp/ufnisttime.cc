/* ufnisttime.cc */

#if !defined(__UFNistTime_cc__)
#define __UFNistTime_cc__ "$Id: ufnisttime.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFNistTime_cc__;

#include "UFSocket.h" // provides basic send, recv, status methods
__UFSocket_H__(__UFNistTime_cc__);
#include "UFClientSocket.h" // inherits Socket, provides connection method
__UFClientSocket_H__(__UFNistTime_cc__);
#include "UFServerSocket.h" // inherits Socket, provides accept method
__UFServerSocket_H__(__UFNistTime_cc__);

#if defined(vxWorks) || defined(VxWorks) || defined(VXWORKS) || defined(Vx) 
	#include "vxWorks.h"
	#include "taskLib.h"
	#include "ioLib.h"
	#include "fioLib.h"
#endif

#include "unistd.h"
#include "time.h"
#include "cstdio"
#include "cstring"
#include "errno.h"
#include "cstring"
#include "iostream"
#include "sys/types.h"

// Time Portocal RFC 868 port number 32 bit int # of sec. since 1/1/1900
const int TP_RFC868_PortNo = 37; 

// Nist Ascii Text Time port number
const int NistAsciiTime_PortNo = 13;

class UFNistTimeClient : public UFClientSocket {
public:
  UFNistTimeClient() : UFClientSocket() {}
  virtual ~UFNistTimeClient() {}

  // new recv signatures:
  int recvText(char*& timetxt, int fd = -1) {
    if( _socket.fd < 0 && fd > 0 ) 
      _socket.fd = fd;

    if( !pendingIO() ) 
    {
      cerr << "UFNistTimeClient::recvText> ?nothing to read on port# "<< 
	_portNo<< " will block..."<<endl;
      if( !validConnection() ) 
	{
        cerr << "UFNistTimeClient::recvText> invalid connection on port# "
		<< _portNo<<endl;
        return 0;
      }
    }
	
    timetxt = new char[50];
    memset(timetxt, 0, 50);
    int nbtoread = 49;
    int nbresult = 0 ;
    do {
      nbresult = ::recv( _socket.fd, timetxt + nbresult, nbtoread, 0);
      nbtoread -= nbresult;
    } while( nbresult >= 0 && nbtoread > 0 && (errno == 0 || errno == EINTR));
    if( errno && errno != EINTR ) hasError();
    if( nbtoread > 0 ) {
      cerr << "UFNistTimeClient::recvText> bad read from socket, retval= " 
           << nbtoread << endl;
      return 0;
    }
    return strlen(timetxt);
  }
};

unsigned long mergeNistTime(char*& timemsg) {
  UFNistTimeClient* txt = new UFNistTimeClient();

  UFSocket::socketFd ts = txt->connect(string( "time.nist.gov" ), NistAsciiTime_PortNo);
  //UFSocket::socketFd ts = txt->connect("192.43.244.18", NistAsciiTime_PortNo);
  if( ts <= 0 ) {
    cerr << "ufnisttime> unable to connect to time.nist.gov on port # "<<NistAsciiTime_PortNo<<endl;
    exit(1);
  }

  int mlen=0;
  char* msgbuf=0;
  mlen = txt->recvText(msgbuf, ts);
  if( mlen <= 0 || msgbuf == 0) {
    msgbuf = "not time text returned from nist";
    cerr << "ufnisttime> unable to recv text msg from time.nist.gov on port # "<<NistAsciiTime_PortNo<<endl;
  }
  else {
    cout << "ufnisttime> msg returned: "<<msgbuf << endl;
  }
  
  // Try the int # sec.
  UFNistTimeClient* bin = new UFNistTimeClient();
  UFSocket::socketFd bs = bin->connect("time.nist.gov", TP_RFC868_PortNo);
  //UFSocket::socketFd bs = bin>connect("192.43.244.18", TP_RFC868_PortNo);
  if( bs <= 0 ) {
    cerr << "ufnisttime> unable to connect to time.nist.gov on port # "<<TP_RFC868_PortNo<<endl;
    exit(1);
  }

  while( !bin->validConnection() || !bin->pendingIO() ) {
    cerr << "ufnisttime> invalid connection to time.nist.gov on port "<<TP_RFC868_PortNo<<endl;
    sleep(1);
  }
  unsigned long sec=0;
  mlen = bin->recv((int &)sec, bs); // # sec. since 1/1/1900

  if( mlen <= 0 || sec == 0) {
    cerr << "ufnisttime> mlen= "<<mlen<<" sec= "<<sec<<endl;
    cerr << "ufnisttime> unable to recv binary msg from time.nist.gov on port # "<<TP_RFC868_PortNo<<endl;
  }
  else
    cout << "ufnisttime> # sec. = "<< sec << endl;

  if( geteuid() == (uid_t) 0 ) {
    cerr << "ufnisttime> setting time (as root) not yet implemented..."<<endl;
  }

  strstream s;
  s << sec << " == " << msgbuf << ends;
  timemsg = s.str();
  
  delete txt; txt = 0;
  delete bin; bin = 0;

  return sec;
}


int main(int argc, char** argv) {
  while( true ) {
    char* timemsg=0;
    mergeNistTime(timemsg);

    cerr << "ufnisttime> waiting to accept one test connection on port# "<<__DefaultTestServerPortNo_ <<endl;
    UFServerSocket serv;
    UFSocketInfo client = serv.accept();

    cerr<<"ufnisttime> got client connection, sending: "<<timemsg<<endl;
    
    serv.send(timemsg);
    close(client.fd);
    delete timemsg;
  }
  
}

#endif // __NISTTIME_CC__




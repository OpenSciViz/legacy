#if !defined(__UFtest_cc__)
#define __UFtest_cc__ "$Id: uftest.cc 14 2008-06-11 01:49:45Z hon $"
const char rcsId[] = __UFtest_cc__;

#include "UFRuntime.h"
__UFRuntime_H__(__UFtest_cc__);

#include "UFImageFrame.h"
__UFImageFrame_H__(__UFtest_cc__);

int main(int, char**) {
  UFRuntime::sysinfo();
  UFImageFrame imf(256);

  unsigned int tstimg[imf.dimension()][imf.dimension()];
  for( int y=0; y<imf.dimension(); y++ )
    for( int x=0; x<imf.dimension(); x++ )
      tstimg[x][y] = x + y*imf.dimension();

  unsigned int* frm=0;
  imf.imageToFrame((unsigned int*)tstimg, frm, imf.dimension());

  unsigned int* img=0;
  imf.frameToImage(frm, img, imf.dimension());

  imf.writeImageFile((const unsigned int*)tstimg, "image.dat");
  imf.writeFrameFile((const unsigned int*)frm, "frame.dat");
}

#endif // __UFtest_cc__

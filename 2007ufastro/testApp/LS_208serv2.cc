const char rcsId[] = "$Id: LS_208serv2.cc 14 2008-06-11 01:49:45Z hon $";

// A client should connect to this server to send requests/commands to the
// Lakeshore 208 Temperature Controller.
//
// This client will read channel data from stdin from a popen call using
// the LS_208poll2 program to generate the channel data on its stdout.
//
// UFStrings objects are used as communication between the client and this server.
// ASCII text strings are received from the generating program.
//
// 
// 
// 

#include "iostream.h"
#include "string"
#include "vector"

// for popen() call
#include "stdio.h"
#include "stdlib.h"

#include "pthread.h"

#include "UFRuntime.h"
#include "UFServerSocket.h"
#include "UFClientSocket.h"
#include "UFProtocol.h"
#include "UFTimeStamp.h"
#include "UFStrings.h"
#include "UFBytes.h"
#include "UFShorts.h"
#include "UFInts.h"
#include "UFFloats.h"
#include "UFFrames.h"
#include "UFFixedVector.h"

#include "UFStringTokenizer.h"

static UFSocket theConnection; // globally accessed by sigHandler
static UFClientSocket _client;
static FILE *popen_ptr = 0;



UFFixedVector ch1("Channel 1", 10);
UFFixedVector ch2("Channel 2", 10);
UFFixedVector ch3("Channel 3", 10);
UFFixedVector ch4("Channel 4", 10);
UFFixedVector ch5("Channel 5", 10);
UFFixedVector ch6("Channel 6", 10);
UFFixedVector ch7("Channel 7", 10);
UFFixedVector ch8("Channel 8", 10);



void exitProper() {
  kill(0,SIGINT);
//   pclose(popen_ptr);  // this doesn't work since it waits for process to die
  exit(1);
}

static void theSigHdlr(int sig) {
  if( sig == SIGPIPE ) {
    cout << "LS_208serv2:: theSigHdlr> sig == SIGPIPE (int sig: " << sig << ")"<< endl;
    theConnection.close();
    _client.close();
  }
  if( sig == SIGINT ) {
    cout << "LS_208serv2:: theSigHdlr> sig == SIGINT (int sig: " << sig << ")"<< endl;
    theConnection.close();
    _client.close();
    exitProper();
  }
  if( sig == SIGCHLD ) {
    cout << "LS_208serv2:: theSigHdlr> sig == SIGCHLD (int sig: " << sig << ")"<< endl;
    //    cout << "Sleeping for 5 seconds." << endl;
    //    UFPosixRuntime::sleep(5.00); // sleep for 5 seconds

    // child process died, so close ptr
    pclose(popen_ptr);
    // exit(0);
  }
  UFPosixRuntime::defaultSigHandler(sig);
}

// Strip CR's and LF's off end of char array
void stripCR_LF(char* data) {
  cout << "stripCR_LF> in function" << endl;

  if (strlen(data) < 1)
    return;
  while ((data[strlen(data)-1] == 13) || (data[strlen(data)-1] == 10)) {
    data[strlen(data)-1] = '\0';
    if (strlen(data) < 1)
      break;
  }
}

char* getData() {
  unsigned char* retVal = 0;
  int numAvail = 0;
  int numRead = 0;

  numAvail = _client.available();
  if (numAvail < 0) {
    cerr<<"getData> Can't retrieve bytes available from _client"<<endl;
  }
  else if (numAvail == 0) {
    cerr<<"getData> No data available from client"<<endl;
  }
  else {
    cerr<<"getData> numAvail = "<<numAvail<<endl;
    retVal = new unsigned char[numAvail+1];
    cerr<<"getData> after allocating buffer for retVal"<<endl;
    numRead = _client.recv(retVal, numAvail);
    cerr<<"getData> numRead = "<<numRead<<endl;
    if (numRead < 0) {
      cerr<<"getData> Error reading from device"<<endl;
      retVal = 0;
    }
    else {
      cerr<<"Setting retVal["<<numAvail<<"] = "<<'\0'<<endl;
      retVal[numAvail] = '\0';
      cerr<<"retVal = "<<retVal<<endl;
    }
  }

  return (char*)retVal;
}

void printVector(UFFixedVector* uffv) {

  cout << "printVector> " << uffv->name() << endl;
  for (list<UFProtocol*>::iterator ptr = uffv->begin(); ptr != uffv->end(); ptr++) {
    cout << (*ptr)->name()  << "\t" << (*ptr)->timeStamp() << endl;
  }

}

// UFStrings object passed to this function will already contain a name
// and 8 blank strings -- we should fill in strings with the last read
// temperature from each channel
UFStrings* getLastEntries() {

  cout << "getLastEntries" << endl;

  vector<string> v;

  list<UFProtocol*>::iterator ptr;

  if (ch1.size() > 0) {
    ptr = ch1.begin();
    v.push_back((*ptr)->name() + " " + (*ptr)->timeStamp());
  }
  else {
     v.push_back("No data");
  }
  if (ch2.size() > 0) {
    ptr = ch2.begin();
    v.push_back((*ptr)->name() + " " + (*ptr)->timeStamp());
  }
  else {
     v.push_back("No data");
  }
  if (ch3.size() > 0) {
    ptr = ch3.begin();
    v.push_back((*ptr)->name() + " " + (*ptr)->timeStamp());
  }
  else {
     v.push_back("No data");
  }
  if (ch4.size() > 0) {
    ptr = ch4.begin();
    v.push_back((*ptr)->name() + " " + (*ptr)->timeStamp());
  }
  else {
     v.push_back("No data");
  }
  if (ch5.size() > 0) {
    ptr = ch5.begin();
    v.push_back((*ptr)->name() + " " + (*ptr)->timeStamp());
  }
  else {
     v.push_back("No data");
  }
  if (ch6.size() > 0) {
    ptr = ch6.begin();
    v.push_back((*ptr)->name() + " " + (*ptr)->timeStamp());
  }
  else {
     v.push_back("No data");
  }
  if (ch7.size() > 0) {
    ptr = ch7.begin();
    v.push_back((*ptr)->name() + " " + (*ptr)->timeStamp());
  }
  else {
     v.push_back("No data");
  }
  if (ch8.size() > 0) {
    ptr = ch8.begin();
    v.push_back((*ptr)->name() + " " + (*ptr)->timeStamp());
  }
  else {
     v.push_back("No data");
  }

  UFStrings* ufs = new UFStrings("LastEntries", v);

  for (int i = 0; i < 8; i++) {
    cout << "ufs->stringAt(" << i << ") = " << *(ufs->stringAt(i)) << endl;
  }

  return ufs;

}

void addToChannelVector(char* buf) {

  char* pollName = "LS_208poll";

  if (strncmp(buf,pollName,strlen(pollName) != 0)) {
    cout << "LS_208serv2::addToChannelVector> expected: " << pollName <<
      ", but got: " << buf << endl;
    return;
  }
  else {
    cout << "Got correct poll program name of: " << buf << endl;
  }

  // set up string for easier handling
  string s(buf);

  UFStringTokenizer ufst(s,' ');

  if (ufst.size() != 3) {
    cerr << "addToChannelVector> bad data: tokens != 3" << endl;
    return;
  }

  string channel = ufst[1];
  string temp = ufst[2];

  int channelNum = atoi(channel.c_str());

  UFFixedVector* vec;
  switch (channelNum) {
  case 1:  vec = &ch1; break;
  case 2:  vec = &ch2; break;
  case 3:  vec = &ch3; break;
  case 4:  vec = &ch4; break;
  case 5:  vec = &ch5; break;
  case 6:  vec = &ch6; break;
  case 7:  vec = &ch7; break;
  case 8:  vec = &ch8; break;
  default:  cout << "addToChannelVector> default in switch" << endl;
            return;
  }

  vec->push_front(new UFStrings(temp));

}

void uflisten(UFServerSocket& server) {
  cerr<<"LS_208serv2> waiting for client connection on: "<<server.description()<<endl;

//   theConnection = server.listenAndAccept();

  cout << "Waiting for client connection for one second..." << endl;
  server.acceptClient(theConnection, 1.0);
  cout << "Done waiting.  Moving on..." << endl;

  // non-blocking accept
//   int connected = server.acceptClient(theConnection);
//   if (connected > -1)
//     cerr<<"LS_208serv2> got client connection..."<<endl;
//   else
//     cerr<<"LS_208serv2> didn't get client connection..."<<endl;


  theConnection.readable();
  return;
}


int main(int argc, char** argv) {

  cout << "LS_208serv2> getpgrp = " << getpgrp() << endl;

  // define defaults
  string port("55555");

  if( argc < 5) {
    cout << "usage> LS_208serv2 myPort pollExecutable remoteIP remotePort" << endl;
    exit(0);
  }

  string cmdLine(argv[2]);
  cmdLine = cmdLine + " " + argv[3];
  cmdLine = cmdLine + " " + argv[4];
  cmdLine = cmdLine + " 2> pollerr";

  cout << "cmdLine = " << cmdLine << endl;

  // Should we check for arguments on command line and use defaults
  // if arguments not there?  Maybe later...
  port = argv[1];

  int portNo = atoi(port.data());
  cerr << "portNo = " << portNo << endl;

  UFServerSocket theServer(portNo);
  if (theServer.listen(portNo).listenFd < 0) {
    cout << "Can't listen on server socket. Exiting..." << endl;
    exit(0);
  }

  UFPosixRuntime::setSignalHandler(theSigHdlr);
  cerr << "established signal handler for SIGPIPE = " << SIGPIPE << endl;
  cerr << "established signal handler for SIGINT = " << SIGINT << endl;
  cerr << "established signal handler for SIGCHLD = " << SIGCHLD << endl;

  const char *cmd = cmdLine.c_str();
//   char *cmd = "wolf.Linux2.2.14-5.0.gcc2.95.2/LS_208poll2 128.227.184.208 7003 2> pollerr";
  char buf[BUFSIZ];
  fflush(0);
  popen_ptr = popen(cmd, "r");
  cout << "popen_ptr = " << popen_ptr << endl;
  int fdP = fileno(popen_ptr);
  cout << "BUFSIZ " << BUFSIZ << endl;
  cout << "fdP = " << fdP << endl;

  cout << "Sleeping for 5 seconds." << endl;
  UFPosixRuntime::sleep(5.00); // sleep for 5 seconds

  int looking_ndx = 0;
  while (!UFRuntime::available(fdP)) {
    cout << "Looking for OK message from LS_208poll2..." << endl;
    UFPosixRuntime::sleep(1.00);
    looking_ndx++;
    if (looking_ndx == 10) {
      cout << "Loop Timeout. Didn't get OK message from LS_208poll2.  Exiting..." << endl;
      raise(SIGINT);
//       exitProper();
    }
  }

  cout << "Something available.  Looking for OK message..." << endl;
  fgets(buf, BUFSIZ, popen_ptr);
  cout << "LS_208serv2> got message: " << buf << " strlen=" << strlen(buf) << endl;
  string okMsg(buf);
  if (okMsg != "OK\n") {
    cout << "Wrong string. Didn't get OK message from LS_208poll2.  Exiting..." << endl;
    raise(SIGINT);
//     exitProper();
  }

  // enter recv/send loop
  unsigned long msgcnt=0;
  string newnam;
  string sendString = "";
  char* retData = 0;     // will hold return data from annex
  vector<UFStrings*> V;  // Hold history of channel temperature strings
  int Vndx = 1;          // Index number for name of UFStrings in vector V
  int do_loop_index = 1;
  do {
    cout << "LS_208serv2> top of do loop. do_loop_index = " << do_loop_index++ << endl;
    if( UFRuntime::available(fdP) ) {
      cout << "Something available.  Trying to get data..." << endl;
      fgets(buf, BUFSIZ, popen_ptr);
      stripCR_LF(buf);
      cout << "LS_208serv2> got message: " << buf << endl;
      strstream s;
      s << Vndx++;
      string name;
      s >> name ;
      delete[] s.str() ;
      addToChannelVector(buf);
//       V.push_back(new UFStrings(name, new string(buf)));
    }
    if( !theConnection.validConnection() ) { // test the valid function
      cerr<<"LS_208serv2> ? no client connection ? listen for new client"<<endl;
      //theConnection = uflisten(theServer);
      uflisten(theServer);
    }

    if( theConnection.readable() > 0 ) { // something is available in the read buffer
      cerr<<"LS_208serv2> msgcnt= "<<msgcnt++<<", reading client ping mesg..."<<endl;
      UFProtocol *ufp = UFProtocol::createFrom(theConnection);

      if( ufp == 0 ) {
        cerr<<"LS_208serv2> UFProtocol::createFrom failed?..."<<endl;
	UFStrings *ufss = new UFStrings("Error in LS_208serv2> UFProtocol::createFrom failed?");
	ufss->sendTo(theConnection);
	delete ufss;
	theConnection.close();
      }
      else {
	UFStrings *ufs = dynamic_cast<UFStrings*>(ufp);
	if (ufs->elements() > 0) {
	  sendString = ufs->valData(0);
	  clog<<"sendString = "<<sendString<<endl;
	}
	else {
	  sendString = "YC5";
	}

	ufs = getLastEntries();
	ufs->sendTo(theConnection);
	delete ufs;

	cout << "Data currently in vectors:" << endl;
	cout << "==========================" << endl;
	printVector(&ch1);
	printVector(&ch2);
	printVector(&ch3);
	printVector(&ch4);
	printVector(&ch5);
	printVector(&ch6);
	printVector(&ch7);
	printVector(&ch8);

      }
      delete ufp;
    }
    else {
      //cerr<<"LS_208serv2> sleeping due to lack of available data..."<<endl;
      sleep(1);
    }

    // print out contents of vector for debug
//     int Vsize = V.size();
//     cout << "V vector =" << endl;
//     for (int i = 0; i < Vsize; i++) {
//       cout << ((UFStrings*)V[i])->name() << ": " << ((UFStrings*)V[i])->valData() << endl;
//     }
//     cout << "V vector done." << endl;


  } while( true );
}


      
